-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for lofar2_unb2c_ddrctrl.
-- Description:
--   The DUT can be targeted at unb 0, node 3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp lofar2_unb2c_ddrctrl
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for lofar2_unb2c_ddrctrl)
--     . load tb_unb1_minimal simulation by double clicking the tb_lofar2_unb2c_ddrctrl icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--   On command line do:
--     > python $UPE_GEAR/peripherals/util_system_info.py --gn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_ppsh.py --gn 3 -n 1 -v 5 --sim
--

library IEEE, common_lib, unb2c_board_lib, i2c_lib, technology_lib, mm_lib, dp_lib, tech_ddr_lib, lofar2_sdp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use i2c_lib.i2c_dev_unb2_pkg.all;
use i2c_lib.i2c_commander_unb2_pmbus_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use diag_lib.diag_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;

entity tb_lofar2_unb2c_ddrctrl is
    generic (
      g_design_name : string  := "lofar2_unb2c_ddrctrl";
      g_sim_unb_nr  : natural := 0;  -- UniBoard 0
      g_sim_node_nr : natural := 3  -- Node 3
    );
end tb_lofar2_unb2c_ddrctrl;

architecture tb of tb_lofar2_unb2c_ddrctrl is
  constant c_sim             : boolean := true;
  constant c_rd_data_w       : natural := 32;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb2c_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb2c_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_cable_delay     : time := 12 ns;
  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoardi
  constant c_st_clk_period   : time := 5 ns;  -- 200 MHz st clk
  constant c_tb_clk_period   : time := 1 ns;  -- 1000 MHz tb clk
  constant c_mm_clk_period   : time := 10 ns;  -- 100 MHz mm clk
  constant c_pps_period      : natural := 1000;

  constant c_mm_file_reg_bsn_source_v2        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg     : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg_wideband_arr : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG_WIDEBAND_ARR";
  constant c_mm_file_reg_stop_in              : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STOP_IN";
  constant c_mm_file_reg_data_buf             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DATA_BUF";
  constant c_mm_file_ram_data_buf             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_DATA_BUF";
  constant c_mm_file_reg_rx_seq_data          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_RX_SEQ_DATA";
  constant c_mm_file_reg_bsn_buf              : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_BUF";
  constant c_mm_file_ram_bsn_buf              : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_BSN_BUF";
  constant c_mm_file_reg_rx_seq_bsn           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_RX_SEQ_BSN";
  constant c_mm_file_reg_ddrctrl_ctrl_state   : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DDRCTRL_CTRL_STATE";

  -- c_check_vector
  constant  c_tech_ddr        : t_c_tech_ddr            := c_tech_ddr4_sim_16k;
  constant  c_ctrl_data_w     : natural                 := func_tech_ddr_ctlr_data_w(c_tech_ddr);  -- 576
  constant  c_adr_w           : natural                 := func_tech_ddr_ctlr_address_w(c_tech_ddr);  -- the lengt of the address vector, for simulation this is smaller, otherwise the simulation would take to long, 27
  constant  c_max_adr         : natural                 := 2**(c_adr_w) - 1;  -- the maximal address that is possible within the address vector length
  constant  c_block_size      : natural                 := 1024;
  constant  c_nof_streams     : natural                 := 12;
  constant  c_data_w          : natural                 := 14;
  constant  c_bim             : natural                 := (c_max_adr * c_ctrl_data_w) / (c_block_size * c_nof_streams * c_data_w);  -- the amount of whole blocks that fit in memory.
  constant  c_nof_st_in_mem   : natural                 := c_bim * c_block_size;

  constant c_check_vector     : std_logic_vector(c_ctrl_data_w * c_bim * c_block_size-1 downto 0) := (others => '0');  -- the sinewave of one stream for c_bim length

  -- BSN
  constant c_init_bsn             : natural := 17;  -- some recognizable value >= 0
  constant c_nof_block_per_sync   : natural := 16;
  constant c_nof_clk_per_sync     : natural := c_nof_block_per_sync * c_sdp_N_fft;

  constant c_speed            : natural   := 4;

  -- WG
  constant c_wg_phase         : real      := 0.0;  -- WG phase in degrees
  constant c_wg_freq          : real      := 160.0;  -- WG freq
  constant c_wg_ampl          : natural   := natural(1.0 * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_bsn_start_wg     : natural   := c_init_bsn + 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  constant c_subband          : real      := 1.0 * (2**c_speed);

  -- DUT
  signal st_clk               : std_logic := '0';
  signal tb_clk               : std_logic := '0';
  signal tb_end               : std_logic := '0';
  signal pps                  : std_logic := '0';
  signal pps_rst              : std_logic := '0';

  signal rd_data              : std_logic_vector(c_rd_data_w - 1 downto 0)  := (others => '0');
  signal sosi_out_data        : std_logic_vector(c_rd_data_w - 1 downto 0)  := (others => '0');
  signal sosi_out_bsn         : std_logic_vector(c_rd_data_w - 1 downto 0)  := (others => '0');
  signal sosi_out_data_sin    : std_logic_vector(   c_data_w - 1 downto 0)  := (others => '0');
  signal sosi_out_not_bsn     : std_logic_vector(c_rd_data_w - 1 downto 0)  := (others => '0');

  signal out_ddrctrl_ctrl_state : std_logic_vector(c_rd_data_w - 1 downto 0)  := (others => '0');

  signal WDI                  : std_logic;
  signal INTA                 : std_logic;
  signal INTB                 : std_logic;

  signal eth_clk              : std_logic_vector(1 downto 0);
  signal eth_txp              : std_logic_vector(1 downto 0);
  signal eth_rxp              : std_logic_vector(1 downto 0);

  signal VERSION              : std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                   : std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO               : std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

  signal qsfp_led             : std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

  signal current_bsn_wg       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  st_clk     <= not st_clk or tb_end after c_st_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) or tb_end after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, st_clk, pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp(0) <= transport eth_txp(0) after c_cable_delay;

  eth_rxp(1) <= '0';
  eth_txp(1) <= '0';

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
   u_lofar2_unb2c_ddrctrl : entity work.lofar2_unb2c_ddrctrl
    generic map (
      g_sim         => c_sim,
      g_sim_unb_nr  => c_unb_nr,
      g_sim_node_nr => c_node_nr,
      g_design_name => g_design_name
    )
    port map (
      -- GENERAL
      CLK         => st_clk,
      PPS         => pps,
      WDI         => WDI,
      INTA        => INTA,
      INTB        => INTB,

      -- Others
      VERSION     => VERSION,
      ID          => ID,
      TESTIO      => TESTIO,

      -- 1GbE Control Interface
      ETH_clk     => eth_clk,
      ETH_SGIN    => eth_rxp,
      ETH_SGOUT   => eth_txp,

      QSFP_LED    => qsfp_led,

      MB_I_REF_CLK  => st_clk

    );

    -- WG

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk <= not tb_clk or tb_end after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process

  variable v_bsn    : natural  := 0;
  begin
    wait for 1 us;
    ----------------------------------------------------------------------------
    -- Enable BSN
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,         c_init_bsn, tb_clk);  -- Init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                  0, tb_clk);  -- Write high part activates the init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1, c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,       16#00000003#, tb_clk);  -- Enable BSN at PPS

    --mmf_mm_bus_rd(c_mm_file_reg_dp_selector, 0, rd_data, tb_clk);

    ----------------------------------------------------------------------------
    -- Enable and start WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]

    for I in 0 to c_nof_streams - 1 loop
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg_wideband_arr, (I * 4) + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg_wideband_arr, (I * 4) + 1, integer(c_wg_phase), tb_clk);  -- phase offset in degrees
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg_wideband_arr, (I * 4) + 2, integer(real(c_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg_wideband_arr, (I * 4) + 3, integer(real(c_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
    end loop;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0,     0, tb_clk);
    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & natural'image(v_bsn) & " > " & natural'image(c_bsn_start_wg)
      severity ERROR;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, c_bsn_start_wg, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,              0, tb_clk);  -- assume v_bsn < 2**31-1

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 24000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 16;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    mmf_mm_bus_rd(c_mm_file_reg_ddrctrl_ctrl_state, 0, out_ddrctrl_ctrl_state, tb_clk);

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * c_block_size;
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 24000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 30;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * c_block_size;
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 24000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 300;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * c_block_size;
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 240000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 30;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * (c_block_size-500);
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 24000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 30000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * (c_block_size);
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    wait for c_mm_clk_period * 24000;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 1, tb_clk);
    wait for c_mm_clk_period * 30;
    mmf_mm_bus_wr(c_mm_file_reg_stop_in, 0, 0, tb_clk);
    wait for c_mm_clk_period * 50000;

    for I in 0 to c_bim - 1 loop
      for J in 0 to c_nof_streams - 1 loop
        for K in c_block_size - (c_block_size / (2**c_speed)) to c_block_size-1 loop
          mmf_mm_bus_rd(c_mm_file_ram_data_buf, (J * c_block_size) + K,           sosi_out_data(c_rd_data_w - 1 downto 0), tb_clk);
          sosi_out_data_sin(c_data_w - 1 downto 0) <= sosi_out_data(c_data_w - 1 downto 0);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2,        sosi_out_bsn(c_rd_data_w - 1 downto 0), tb_clk);
          mmf_mm_bus_rd( c_mm_file_ram_bsn_buf, ((J * c_block_size) + k) * 2 + 1,  sosi_out_not_bsn(c_rd_data_w - 1 downto 0), tb_clk);
        end loop;
      end loop;
      wait for c_st_clk_period * (c_block_size);
    end loop;

-----------------------------------------------------------------------------------------------------------------------------

    tb_end <= '1';
    assert false
      report "Test: OK"
      severity FAILURE;
    wait;
  end process;
end tb;
