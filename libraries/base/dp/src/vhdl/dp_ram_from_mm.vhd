--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_ram_from_mm is
  generic (
    g_technology       : natural := c_tech_select_default;
    g_ram_wr_nof_words : natural;
    g_ram_rd_dat_w     : natural;
    g_init_file        : string := "UNUSED"
  );
  port (
    mm_rst    : in  std_logic;
    mm_clk    : in  std_logic;

    st_rst    : in  std_logic;
    st_clk    : in  std_logic;

    mm_addr   : in  std_logic_vector(c_mem_address_w - 1 downto 0);
    mm_wr     : in  std_logic;
    mm_wrdata : in  std_logic_vector(c_mem_data_w - 1 downto 0);

    dp_on     : in  std_logic := '0';

    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_ram_from_mm;

architecture rtl of dp_ram_from_mm is
  constant c_mm_ram_wr  : t_c_mem := (latency  => 1,
                                      adr_w    => ceil_log2(g_ram_wr_nof_words),
                                      dat_w    => c_word_w,
                                      nof_dat  => g_ram_wr_nof_words,
                                      init_sl  => '0');

  constant c_ram_rd_nof_words : natural := (c_word_w * g_ram_wr_nof_words) / g_ram_rd_dat_w;

  constant c_mm_ram_rd  : t_c_mem := (latency  => 1,
                                      adr_w    => ceil_log2(c_ram_rd_nof_words),
                                      dat_w    => g_ram_rd_dat_w,
                                      nof_dat  => c_ram_rd_nof_words,
                                      init_sl  => '0');

  type t_state_enum is (s_init, s_wait_for_rdy, s_read);

  signal state         : t_state_enum;
  signal nxt_state     : t_state_enum;

  signal src_out_ctrl_val     : t_dp_sosi;
  signal nxt_src_out_ctrl_val : t_dp_sosi;

  signal rd_en       : std_logic;
  signal rd_val      : std_logic;
  signal rd_data     : std_logic_vector(c_mm_ram_rd.dat_w - 1 downto 0);
  signal rd_addr     : std_logic_vector(c_mm_ram_rd.adr_w - 1 downto 0);
  signal nxt_rd_addr : std_logic_vector(c_mm_ram_rd.adr_w - 1 downto 0);
begin
  p_src_out : process(src_out_ctrl_val, rd_data)
  begin
    src_out <= src_out_ctrl_val;
    src_out.data(c_mm_ram_rd.dat_w - 1 downto 0) <= rd_data;
  end process;

  p_state : process(state, rd_addr, src_in, dp_on, rd_data)
  begin
    nxt_state   <= state;
    nxt_rd_addr <= rd_addr;
    nxt_src_out_ctrl_val <= c_dp_sosi_rst;
    rd_en <= '0';

    case state is
      when s_init =>
        nxt_state <= s_wait_for_rdy;

      when s_wait_for_rdy =>
        -- Only start stream when dp_on = '1'
        if dp_on = '1' then
          if src_in.ready = '1' then
            rd_en <= '1';
            nxt_state <= s_read;
            nxt_rd_addr <= INCR_UVEC(rd_addr, 1);
            nxt_src_out_ctrl_val.valid <= '1';
            nxt_src_out_ctrl_val.sop <= '1';
            if unsigned(rd_addr) = c_ram_rd_nof_words - 1 then
              nxt_src_out_ctrl_val.eop <= '1';
              nxt_state   <= s_wait_for_rdy;
              nxt_rd_addr <= (others => '0');
            end if;
          end if;
        end if;

      when s_read =>
        if src_in.ready = '1' then
          rd_en <= '1';
          nxt_rd_addr <= INCR_UVEC(rd_addr, 1);
          nxt_src_out_ctrl_val.valid <= '1';
          if unsigned(rd_addr) = c_ram_rd_nof_words - 1 then
            nxt_src_out_ctrl_val.eop <= '1';
            nxt_state   <= s_wait_for_rdy;
            nxt_rd_addr <= (others => '0');
            -- Only stop stream after EOP
            if dp_on = '0' then
              nxt_state <= s_wait_for_rdy;
            end if;
          end if;
        end if;

      when others =>
        nxt_state <= s_init;
    end case;
  end process;

  p_clk : process(st_rst, st_clk)
  begin
    if st_rst = '1' then
      state   <= s_init;
      rd_addr <= (others => '0');
      src_out_ctrl_val <= c_dp_sosi_rst;
    elsif rising_edge(st_clk) then
      state   <= nxt_state;
      rd_addr <= nxt_rd_addr;
      src_out_ctrl_val <= nxt_src_out_ctrl_val;
    end if;
  end process;

  u_ram : entity common_lib.common_ram_cr_cw_ratio
  generic map (
    g_technology => g_technology,
    g_ram_wr    => c_mm_ram_wr,
    g_ram_rd    => c_mm_ram_rd,
    g_init_file => g_init_file
  )
  port map (
    -- Write port clock domain
    wr_rst    => mm_rst,
    wr_clk    => mm_clk,
    wr_en     => mm_wr,
    wr_adr    => mm_addr(c_mm_ram_wr.adr_w - 1 downto 0),
    wr_dat    => mm_wrdata(c_mm_ram_wr.dat_w - 1 downto 0),
    -- Read port clock domain
    rd_rst    => st_rst,
    rd_clk    => st_clk,
    rd_en     => rd_en,
    rd_adr    => rd_addr,
    rd_dat    => rd_data,
    rd_val    => open
  );
end rtl;
