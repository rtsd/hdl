################################################################################
#
# Copyright (C) 2011
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
 
# >>> Convert Waveform txt file into unsigned format that can be copy pasted
#     manually into a Quartus mif file using and editor
# . Usage at TCLSH prompt% source wg_to_unsigned.tcl
# . Steps:
#   - wg_waveform.tcl or wg_waveform.m --> .txt
#   - .txt --> wg_to_unsigned.tcl --> .uns
#   - .uns --> Crimson editor paste column --> .mif
#   - .mif --> Quartus read, save as --> .hex

global env

set dir_name  "$env(UNB)/Firmware/modules/Lofar/diag/src/data/"

# Set file properties
set file_name "tcl_sin_1024x8"
set us_offset [expr round(pow(2, 8))]

# Convert txt --> uns
set waveform  [wg_read_file [format "$dir_name%s" "$file_name.txt"]]

set us_waveform {}
foreach i $waveform {
  if {$i>=0} {
    lappend us_waveform $i
  } else {
    lappend us_waveform [expr $i + $us_offset]
  }
}
    
set f            {}
append f         "$dir_name" "$file_name.uns"
wg_write_file    $f $us_waveform
