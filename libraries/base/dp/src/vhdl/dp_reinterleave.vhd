--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . DP wrapper for common_reinterleave
-- Description:
-- . The reinterleaver is an array of deinterleavers that feed an array of interleavers.
--                   _
--      _           | \
--     / |----------| |----
-- ----| |---\   /--|_/
--     \_|--\ \ /    _
--           \ X----| \
--            X     | |----
--      _    / X----|_/
--     / |--/ / \    _
-- ----| |---/   \--| \
--     \_|----------| |----
--                  |_/
-- Remarks:
-- . Requirements:
--   * All input streams must be synchronous;
--   * g_inter_block_size >= g_deint_block_size;
--    . g_deint_block_size = g_block_size out
--    . Input block length (!= g_deint_block_size) / nof_out / g_block_size_x = integer.
--      If it is not an integer number, the input blocks cannot be evenly distributed over
--      the outputs with the chosen block_size_x.

entity dp_reinterleave is
  generic (
    g_dat_w             : natural;  -- Width of the data. Either the width of the datafield or the re and im field combined.
    g_nof_in            : natural;  -- Number of input streams
    g_deint_block_size  : natural;  -- Inputs streams will be de-interleaved using this block size. Should match block_size_out when g_use_ctrl=TRUE
    g_nof_out           : natural;  -- The number of output streams
    g_inter_block_size  : natural;  -- Outputs streams will be interleaved using this block size. Should match block_size_in when g_use_ctrl=TRUE
    g_use_ctrl          : boolean := true;
    g_use_sync_bsn      : boolean := true;  -- forwards (stored) input Sync+BSN to all output streams
    g_use_complex       : boolean;
    g_align_out         : boolean := false;
    g_block_size_output : natural := 1  -- Output block size: The number of samles in the blocks at the output
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_in - 1 downto 0);
    src_out_arr : out t_dp_sosi_arr(g_nof_out - 1 downto 0)
  );
end dp_reinterleave;

architecture wrap of dp_reinterleave is
  constant c_complex_w : natural := g_dat_w / 2;

  signal common_reinterleave_in_dat  : std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);
  signal common_reinterleave_in_val  : std_logic;
  signal common_reinterleave_out_dat : std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
  signal common_reinterleave_out_val : std_logic_vector(g_nof_out - 1 downto 0);

  signal common_reinterleave_src_out_arr : t_dp_sosi_arr(g_nof_out - 1 downto 0);
  signal dp_block_gen_src_out_arr        : t_dp_sosi_arr(g_nof_out - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Map input sosi_arr to SLV
  -----------------------------------------------------------------------------
  gen_wires_in : for i in 0 to g_nof_in - 1 generate
    gen_sosi_dat_in: if g_use_complex = false generate
      -----------------------------------------------------------------------------
      -- Forward SOSI data field
      -----------------------------------------------------------------------------
      common_reinterleave_in_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w) <= snk_in_arr(i).data(g_dat_w - 1 downto 0);
    end generate;

    gen_sosi_compl_in: if g_use_complex = true generate
      -----------------------------------------------------------------------------
      -- Forward concatenated im & re fields
      -----------------------------------------------------------------------------
      common_reinterleave_in_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w) <= snk_in_arr(i).im(c_complex_w - 1 downto 0) & snk_in_arr(i).re(c_complex_w - 1 downto 0);
    end generate;
  end generate;

  common_reinterleave_in_val <= snk_in_arr(0).valid;

  u_common_reinterleave : entity common_lib.common_reinterleave
  generic map (
    g_nof_in           => g_nof_in,
    g_deint_block_size => g_deint_block_size,
    g_nof_out          => g_nof_out,
    g_inter_block_size => g_inter_block_size,
    g_dat_w            => g_dat_w,
    g_align_out        => g_align_out
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_dat     => common_reinterleave_in_dat,
    in_val     => common_reinterleave_in_val,

    out_dat    => common_reinterleave_out_dat,
    out_val    => common_reinterleave_out_val
  );

  -----------------------------------------------------------------------------
  -- Map output SLV to sosi_arr
  -----------------------------------------------------------------------------
  gen_wires_out : for i in 0 to g_nof_out - 1 generate
    gen_sosi_dat_out: if g_use_complex = false generate
      -----------------------------------------------------------------------------
      -- Forward SOSI data field
      -----------------------------------------------------------------------------
      common_reinterleave_src_out_arr(i).data(g_dat_w - 1 downto 0) <= common_reinterleave_out_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
    end generate;

    gen_sosi_compl_out: if g_use_complex = true generate
      -----------------------------------------------------------------------------
      -- Forward concatenated im & re fields
      -----------------------------------------------------------------------------
      common_reinterleave_src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_reinterleave_out_dat( i * g_dat_w + g_dat_w     - 1 downto i * g_dat_w + c_complex_w));
      common_reinterleave_src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_reinterleave_out_dat( i * g_dat_w + c_complex_w - 1 downto i * g_dat_w));
    end generate;

    common_reinterleave_src_out_arr(i).valid <= common_reinterleave_out_val(i);
  end generate;

  -----------------------------------------------------------------------------
  -- Add SOP and EOP to the outputs
  -----------------------------------------------------------------------------
  gen_ctrl : if g_use_ctrl = true generate
    gen_dp_block_gen : for i in 0 to g_nof_out - 1 generate
      u_dp_block_gen : entity work.dp_block_gen
      generic map (
        g_use_src_in       => false,
        g_nof_data         => g_block_size_output,
        g_preserve_sync    => true,
        g_preserve_bsn     => true
      )
      port map(
        rst        => rst,
        clk        => clk,

        snk_in     => common_reinterleave_src_out_arr(i),
        src_out    => dp_block_gen_src_out_arr(i)
      );
    end generate;
  end generate;

  no_ctrl : if g_use_ctrl = false generate
    dp_block_gen_src_out_arr <= common_reinterleave_src_out_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- Re-add input sync + BSN to all output streams
  -----------------------------------------------------------------------------
  align_out : if g_use_sync_bsn = true generate
    gen_dp_fifo_info: for i in 0 to g_nof_out - 1 generate
      u_dp_fifo_info : entity work.dp_fifo_info
      generic map (
        g_use_sync => true,
        g_use_bsn  => true
      )
      port map (
        rst          => rst,
        clk          => clk,

        data_snk_in  => dp_block_gen_src_out_arr(i),  -- delayed snk_in data
        info_snk_in  => snk_in_arr(0),  -- original snk_in info

        src_in       => c_dp_siso_rdy,
        src_out      => src_out_arr(i)
      );
    end generate;
  end generate;

  no_align_out : if g_use_sync_bsn = false generate
    src_out_arr <= dp_block_gen_src_out_arr;
  end generate;
end wrap;
