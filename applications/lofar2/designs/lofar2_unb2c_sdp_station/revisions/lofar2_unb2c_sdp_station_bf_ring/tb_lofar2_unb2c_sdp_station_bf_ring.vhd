-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle (original), E. Kooistra (updates)
-- Purpose:
--   Self-checking testbench for simulating lofar2_unb2c_sdp_station_bf_ring
--   using WG data.
--
-- Description:
--   This tb is a balance between verification coverage and keeping it simple:
--   - Use only one signal input (g_global_sp) with strong WG. Put same remnant
--     weak WG signal on the other g_nof_rn * S_pn - 1 signal inputs.
--   - Use different BF weight for the two beamlet polarizations (g_bf_x_gain,
--     g_bf_x_phase and g_bf_y_phase, g_bf_y_phase) of signal input g_global_sp.
--     Using different BF weights for the N_pol_bf = 2 BF polarizations allows
--     verification of the dual polarization beamlet.
--   - Use same remnant BF weight for the other g_nof_rn * S_pn - 1 signal inputs.
--     The remnant signal inputs and BF weights allow verifying the BF sum if
--     they are not 0. Using the same settings for all remnant SP simplyfies
--     the tb, while still testing the BF sum.
--   - Select one beamlet (g_beamlet) for the subband (g_subband). Selecting
--     one beamlet other than the default beamlet for the subband is sufficient
--     to verify the beamlet subband select.
--   - Use same stimuli for both beamsets.
--
--   MM control actions:
--
--   1) Enable calc mode for WG on signal input (si) g_global_sp via reg_diag_wg with:
--        g_subband = 102 --> 102 * f_sub = 19.921875 MHz
--        g_sp_ampl = 1.0 --> 1.0 yield WG ampl = 2**13
--        g_sp_phase --> subband phase
--      Use g_sp_remnant_ampl = 0.1 and g_sp_remnant_phase = 0.0 for all other
--      g_nof_rn * S_pn - 1 signal inputs, than g_global_sp, that are not used in the BF.
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write
--      reg_bsn_scheduler_wg to trigger start of WG at BSN.
--
--   3) Read and verify subband statistics (SST) for g_global_sp. This also reads the
--      SST of the other signal input of the WPFB that processes g_global_sp.
--
--   4) Select subband g_subband for beamlets in g_beamlet
--
--   5) Apply BF weight to g_beamlet X beam and Y beam, so for example if g_global_sp
--      = 3, then w_x3 = g_bf_x_gain,phase and w_y3 = g_bf_y_gain,phase. The
--      BF weights for all other beamlets (/= g_beamlet) are 0.
--
--      WG          BF               BF
--      si          weight           weight
--                  X                Y
--       0 -----> * w_x0  ..+
--           \--------------|----> * w_y0  ..+
--       1 -----> * w_x1  ..+                |
--           \--------------|----> * w_y1  ..+
--       2 -----> * w_x2  ..+                |
--           \--------------|----> * w_y2  ..+
--       3 -----> * w_x3  ..+                |
--           \--------------|----> * w_y3  ..+
--       4 -----> * w_x4  ..+                |
--           \--------------|----> * w_y4  ..+
--       5 -----> * w_x5  ..+                |
--           \--------------|----> * w_y5  ..+
--       6 -----> * w_x6  ..+                |
--           \--------------|----> * w_y6  ..+
--       7 -----> * w_x7  ..+                |
--           \--------------|----> * w_y7  ..+
--       8 -----> * w_x8  ..+                |
--           \--------------|----> * w_y8  ..+
--       9 -----> * w_x9  ..+                |
--           \--------------|----> * w_y9  ..+
--      10 -----> * w_x10 ..+                |
--           \--------------|----> * w_y10 ..+
--      11 -----> * w_x11 ..+                |
--           \--------------|----> * w_y11 ..+
--                          |                |
--                          \----------------|---> beamlet_x
--                                            \--> beamlet_y
--
--   6) Read and verify beamlet statistics (BST)
--        View sp_sst in Wave window
--        View bst_x_arr, bst_y_arr in Wave window
--
--   7) Verify 10GbE output header and output payload for g_beamlet.
--        View rx_beamlet_sosi
--        View rx_beamlet_cnt (in analog format)
--
-- Remark:
-- * The c_wg_phase_offset and c_subband_phase_offset are used to tune the WG
--   phase reference to 0.0 degrees at the start (sop)
-- * Use g_beamlet_scale = 2**10, for full scale WG and N_ant = 1, see [1]
-- * Using g_beamlet = c_sdp_S_sub_bf-1 = 487 puts g_subband = 102 at the last
--   beamlet in the beamset, so at index 974,975 of rx_beamlet_list_re/im.
-- * Default beamlet 102 also contains g_subband = 102. On HW the BF weights
--   are default 0, but in sim the BF weights in node_sdp_beamformer.vhd
--   are default unit weights. Therefore also write the BF weight for default
--   beamlet 102 to define it value, in case g_beamlet /= 102. In this tb
--   the BF weigth for beamlet = g_subband = 102 is set to 0, so that the
--   g_subband = 102 will only show up in g_beamlet.
-- * A simulation only section in sdp_beamformer_output.vhd disturbs the BSN,
--   to cause a merged payload error, so that sdp_source_info_payload_error
--   can be verified here.
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   # Manually add missing signals and constants using objects in GUI
--   > add wave -position insertpoint  \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/sp_ssts_arr2 \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/bsts_arr2 \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/rx_packet_list_re \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/rx_packet_list_im \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/rx_reordered_list_re \
--     sim:/tb_lofar2_unb2c_sdp_station_bf_ring/rx_reordered_list_im
--   > run -a
--   Takes about 2h 25m when g_read_all_* = FALSE
--
-- References:
-- [1] L4 SDPFW Decision: LOFAR2.0 SDP Firmware Quantization Model,
--     https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L4+SDPFW+Decision%3A+LOFAR2.0+SDP+Firmware+Quantization+Model
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, reorder_lib, lofar2_sdp_lib, wpfb_lib, tech_pll_lib, tr_10GbE_lib, lofar2_unb2c_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use reorder_lib.reorder_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use lofar2_sdp_lib.tb_sdp_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station_pkg.all;

entity tb_lofar2_unb2c_sdp_station_bf_ring is
  generic (
    g_first_gn           : natural := 1;  -- first global node (GN) in ring
    g_nof_rn             : natural := 2;  -- nof ring nodes (RN) in ring
    g_global_sp          : natural := 15;  -- WG global signal path (SP) index in range [c_first_sp : c_last_sp]
    g_sp_ampl            : real := 0.5;  -- WG normalized amplitude
    g_sp_phase           : real := -110.0;  -- WG phase in degrees = subband phase
    g_sp_remnant_ampl    : real := 0.1;  -- WG normalized amplitude for remnant sp
    g_sp_remnant_phase   : real := 15.0;  -- WG phase in degrees for remnant sp
    g_subband            : natural := 102;  -- select g_subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
    g_beamlet            : natural := c_sdp_S_sub_bf - 1;  -- map g_subband to g_beamlet index in beamset in range(c_sdp_S_sub_bf = 488)
    g_beamlet_scale      : real := 1.0 / 2.0**9;  -- g_beamlet output scale factor
    g_bf_x_gain          : real := 0.7;  -- g_beamlet X BF weight normalized gain for g_global_sp
    g_bf_y_gain          : real := 0.6;  -- g_beamlet Y BF weight normalized gain for g_global_sp
    g_bf_x_phase         : real := 30.0;  -- g_beamlet X BF weight phase rotation in degrees for g_global_sp
    g_bf_y_phase         : real := 40.0;  -- g_beamlet Y BF weight phase rotation in degrees for g_global_sp
    g_bf_remnant_x_gain  : real := 0.05;  -- g_beamlet X BF weight normalized gain for remnant sp
    g_bf_remnant_y_gain  : real := 0.04;  -- g_beamlet Y BF weight normalized gain for remnant sp
    g_bf_remnant_x_phase : real := 170.0;  -- g_beamlet X BF weight phase rotation in degrees for remnant sp
    g_bf_remnant_y_phase : real := -135.0;  -- g_beamlet Y BF weight phase rotation in degrees for remnant sp
    g_read_all_sub_sel   : boolean := false;  -- when FALSE only read subband selection for g_beamlet, to save sim time
    g_read_all_SST       : boolean := false;  -- when FALSE only read SST for g_subband, to save sim time
    g_read_all_BST       : boolean := false  -- when FALSE only read BST for g_beamlet, to save sim time
  );
end tb_lofar2_unb2c_sdp_station_bf_ring;

architecture tb of tb_lofar2_unb2c_sdp_station_bf_ring is
  -- Revision parameters
  constant c_design_name         : string := "lofar2_unb2c_sdp_station_bf_ring";
  constant c_revision_select     : t_lofar2_unb2c_sdp_station_config := func_sel_revision_rec(c_design_name);

  constant c_sim                 : boolean := true;
  constant c_first_unb_nr        : natural := g_first_gn / c_quad;  -- c_quad = 4 FPGAs per UniBoard2
  constant c_first_node_nr       : natural := g_first_gn mod c_quad;  -- first node_nr in range(c_quad) = [0:3] on c_first_unb_nr
  constant c_last_rn             : natural := g_nof_rn - 1;  -- first RN has index 0 by definition.
  constant c_last_gn             : natural := g_first_gn + c_last_rn;  -- last global node (GN) in ring
  constant c_last_unb_nr         : natural := c_last_gn / c_quad;
  constant c_last_node_nr        : natural := c_last_gn mod c_quad;
  constant c_global_sp_gn        : natural := g_global_sp / c_sdp_S_pn;  -- global node (GN) of where g_global_sp is located
  constant c_local_sp            : natural := g_global_sp mod c_sdp_S_pn;  -- local SP index of g_global_sp on c_global_sp_gn
  constant c_global_sp_unb_nr    : natural := c_global_sp_gn / c_quad;  -- unb_nr of where g_global_sp is located
  constant c_global_sp_node_nr   : natural := c_global_sp_gn mod c_quad;  -- unb_nr of where g_global_sp is located
  constant c_first_sp            : natural := g_first_gn * c_sdp_S_pn;
  constant c_last_sp             : natural := c_first_sp + g_nof_rn * c_sdp_S_pn - 1;

  constant c_init_bsn            : natural := 17;  -- some recognizable value >= 0
  constant c_nof_lanes           : natural := c_sdp_N_beamsets;
  constant c_use_bdo_transpose   : boolean := c_revision_select.use_bdo_transpose;

  constant c_last_id             : std_logic_vector(7 downto 0) := TO_UVEC(c_last_gn, 8);
  constant c_version             : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version          : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_mm_clk_period       : time := 10 ns;  -- 100 MHz internal mm_clk
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 16;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_stat_data_sz        : natural := c_wpfb_sim.stat_data_sz;  -- = 2

  constant c_stat_percentage     : real := 0.05;  -- +-percentage margin that actual value may differ from expected value
  constant c_stat_lo_factor      : real := 1.0 - c_stat_percentage;  -- lower boundary
  constant c_stat_hi_factor      : real := 1.0 + c_stat_percentage;  -- higher boundary

  constant c_beamlet_output_delta : integer := 2;  -- +-delta margin

  -- header fields
  -- . Use gn = 1 for c_cep_ip_src_addr to have fixed c_exp_ip_header_checksum
  constant c_cep_eth_src_mac     : std_logic_vector(47 downto 0) := c_sdp_cep_eth_src_mac_47_16 & func_sdp_gn_index_to_mac_15_0(c_last_gn);
  constant c_cep_ip_src_addr     : std_logic_vector(31 downto 0) := c_sdp_cep_ip_src_addr_31_16 & func_sdp_gn_index_to_ip_15_0(1);
  constant c_cep_udp_src_port    : std_logic_vector(15 downto 0) := c_sdp_cep_udp_src_port_15_8 & c_last_id;  -- D0 & c_last_id
  -- . The ip_header_checksum depends on src IP, and therefore on the c_last_gn. The expected
  --   value is obtained from rx_sdp_cep_header.ip.header_checksum in wave window for
  --   c_last_gn = 1. Therefore in this tb use func_sdp_gn_index_to_ip_15_0(1) to have fixed
  --   c_exp_ip_header_checksum, independent of actual c_last_gn.
  constant c_exp_ip_header_checksum : natural := 16#5BDD#;

  constant c_exp_beamlet_scale   : natural := natural(g_beamlet_scale * real(c_sdp_unit_beamlet_scale));  -- c_sdp_unit_beamlet_scale = 2**15;
  constant c_exp_beamlet_index   : natural := 0;  -- depends on beamset bset * c_sdp_S_sub_bf
  constant c_beamlet_index_mod   : boolean := true;

  constant c_exp_sdp_info        : t_sdp_info := (
                                     TO_UVEC(3, 6),  -- antenna_field_index
                                     TO_UVEC(601, 10),  -- station_id
                                     '0',  -- antenna_band_index
                                     x"7FFFFFFF",  -- observation_id, use > 0 to avoid Warning: (vsim-151) NUMERIC_STD.TO_INTEGER: Value -2 is not in bounds of subtype NATURAL.
                                     b"01",  -- nyquist_zone_index, 0 = first, 1 = second, 2 = third
                                     '1',  -- f_adc, 0 = 160 MHz, 1 = 200 MHz
                                     '0',  -- fsub_type, 0 = critically sampled, 1 = oversampled
                                     '0',  -- beam_repositioning_flag
                                     x"1400"  -- block_period = 5120
                                   );

  -- Expected transposed indices order by func_reorder_transpose_packet().
  -- Yields same c_reorder_transpose_indices order as:
  -- > python applications/lofar2/libraries/sdp/src/python/test_func_sdp_transpose_packet.py
  constant c_nof_ch                    : natural := c_sdp_cep_nof_beamlets_per_packet * c_sdp_N_pol_bf;
  constant c_reorder_transpose_indices : t_natural_arr(0 to c_nof_ch - 1) :=
    func_reorder_transpose_indices(c_sdp_cep_nof_blocks_per_packet,
                                   c_sdp_cep_nof_beamlets_per_block,
                                   c_sdp_N_pol_bf);

  -- WG
  constant c_bsn_start_wg         : natural := c_init_bsn + 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  -- .ampl
  constant c_wg_ampl              : natural := natural(g_sp_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_wg_remnant_ampl      : natural := natural(g_sp_remnant_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_exp_sp_power         : real := real(c_wg_ampl**2) / 2.0;
  constant c_exp_sp_ast           : real := c_exp_sp_power * real(c_nof_clk_per_sync);
  -- . phase
  constant c_subband_freq         : real := real(g_subband) / real(c_sdp_N_fft);  -- normalized by fs = f_adc = 200 MHz = dp_clk rate
  constant c_wg_latency           : integer := c_diag_wg_latency + c_sdp_shiftram_latency - 0;  -- -0 to account for BSN scheduler start trigger latency
  constant c_wg_phase_offset      : real := 360.0 * real(c_wg_latency) * c_subband_freq;  -- c_diag_wg_latency is in dp_clk cycles
  constant c_wg_phase             : real := g_sp_phase + c_wg_phase_offset;  -- WG phase in degrees
  constant c_wg_remnant_phase     : real := g_sp_remnant_phase + c_wg_phase_offset;  -- WG phase in degrees

  -- WPFB
  constant c_pol_index                    : natural := c_local_sp mod c_sdp_Q_fft;
  constant c_pfb_index                    : natural := c_local_sp / c_sdp_Q_fft;  -- only read used WPFB unit out of range(c_sdp_P_pfb = 6)
  constant c_subband_phase_offset         : real := -90.0;  -- WG with zero phase sinus yields subband with -90 degrees phase (negative Im, zero Re)
  constant c_subband_weight_gain          : real := 1.0;  -- use default unit subband weights
  constant c_subband_weight_phase         : real := 0.0;  -- use default unit subband weights
  constant c_exp_subband_phase            : real := g_sp_phase + c_subband_phase_offset + c_subband_weight_phase;
  constant c_exp_subband_ampl             : real := real(c_wg_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio * c_subband_weight_gain;
  constant c_exp_subband_power            : real := c_exp_subband_ampl**2.0;  -- complex signal ampl, so power is A**2 (not A**2 / 2 as for real)
  constant c_exp_subband_sst              : real := c_exp_subband_power * real(c_nof_block_per_sync);

  constant c_exp_remnant_subband_phase    : real := g_sp_remnant_phase + c_subband_phase_offset + c_subband_weight_phase;
  constant c_exp_remnant_subband_ampl     : real := real(c_wg_remnant_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio * c_subband_weight_gain;

  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_sub - 1);  -- 512
  type t_slv_64_beamlets_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_beamlets_sdp - 1);  -- 2*488 = 976

  -- BF X-pol and Y-pol
  -- . select
  constant c_exp_beamlet_x_index          : natural := g_beamlet * c_sdp_N_pol_bf;  -- X index in beamset 0
  constant c_exp_beamlet_y_index          : natural := g_beamlet * c_sdp_N_pol_bf + 1;  -- Y index in beamset 0
  -- . Beamlet weights for selected g_global_sp
  constant c_bf_x_weight_re               : integer := integer(COMPLEX_RE(g_bf_x_gain * real(c_sdp_unit_bf_weight), g_bf_x_phase));
  constant c_bf_x_weight_im               : integer := integer(COMPLEX_IM(g_bf_x_gain * real(c_sdp_unit_bf_weight), g_bf_x_phase));
  constant c_bf_y_weight_re               : integer := integer(COMPLEX_RE(g_bf_y_gain * real(c_sdp_unit_bf_weight), g_bf_y_phase));
  constant c_bf_y_weight_im               : integer := integer(COMPLEX_IM(g_bf_y_gain * real(c_sdp_unit_bf_weight), g_bf_y_phase));
  constant c_bf_remnant_x_weight_re       : integer := integer(COMPLEX_RE(g_bf_remnant_x_gain * real(c_sdp_unit_bf_weight), g_bf_remnant_x_phase));
  constant c_bf_remnant_x_weight_im       : integer := integer(COMPLEX_IM(g_bf_remnant_x_gain * real(c_sdp_unit_bf_weight), g_bf_remnant_x_phase));
  constant c_bf_remnant_y_weight_re       : integer := integer(COMPLEX_RE(g_bf_remnant_y_gain * real(c_sdp_unit_bf_weight), g_bf_remnant_y_phase));
  constant c_bf_remnant_y_weight_im       : integer := integer(COMPLEX_IM(g_bf_remnant_y_gain * real(c_sdp_unit_bf_weight), g_bf_remnant_y_phase));

  -- Model the SDP (remote) beamformer for one g_global_sp and g_nof_rn * S_pn - 1 remnant signal inputs
  -- . Beamlet internal
  constant c_nof_remnant                  : natural := g_nof_rn * c_sdp_S_pn - 1;
  constant c_exp_beamlet_x_tuple          : t_real_arr(0 to 3) := func_sdp_beamformer(
                                              c_exp_subband_ampl, c_exp_subband_phase, g_bf_x_gain, g_bf_x_phase,
                                              c_exp_remnant_subband_ampl, c_exp_remnant_subband_phase, g_bf_remnant_x_gain, g_bf_remnant_x_phase,
                                              c_nof_remnant);
  constant c_exp_beamlet_x_ampl           : real := c_exp_beamlet_x_tuple(0);
  constant c_exp_beamlet_x_phase          : real := c_exp_beamlet_x_tuple(1);
  constant c_exp_beamlet_x_re             : real := c_exp_beamlet_x_tuple(2);
  constant c_exp_beamlet_x_im             : real := c_exp_beamlet_x_tuple(3);

  constant c_exp_beamlet_y_tuple          : t_real_arr(0 to 3) := func_sdp_beamformer(
                                              c_exp_subband_ampl, c_exp_subband_phase, g_bf_y_gain, g_bf_y_phase,
                                              c_exp_remnant_subband_ampl, c_exp_remnant_subband_phase, g_bf_remnant_y_gain, g_bf_remnant_y_phase,
                                              c_nof_remnant);
  constant c_exp_beamlet_y_ampl           : real := c_exp_beamlet_y_tuple(0);
  constant c_exp_beamlet_y_phase          : real := c_exp_beamlet_y_tuple(1);
  constant c_exp_beamlet_y_re             : real := c_exp_beamlet_y_tuple(2);
  constant c_exp_beamlet_y_im             : real := c_exp_beamlet_y_tuple(3);
  -- . BST
  constant c_exp_beamlet_x_power          : real := c_exp_beamlet_x_ampl**2.0;  -- complex signal ampl, so no divide by 2
  constant c_exp_beamlet_x_bst            : real := c_exp_beamlet_x_power * real(c_nof_block_per_sync);
  constant c_exp_beamlet_y_power          : real := c_exp_beamlet_y_ampl**2.0;  -- complex signal ampl, so no divide by 2
  constant c_exp_beamlet_y_bst            : real := c_exp_beamlet_y_power * real(c_nof_block_per_sync);
  -- . Beamlet output
  constant c_exp_beamlet_x_output_ampl    : real := c_exp_beamlet_x_ampl * g_beamlet_scale;
  constant c_exp_beamlet_x_output_phase   : real := c_exp_beamlet_x_phase;
  constant c_exp_beamlet_x_output_re      : real := c_exp_beamlet_x_re * g_beamlet_scale;
  constant c_exp_beamlet_x_output_im      : real := c_exp_beamlet_x_im * g_beamlet_scale;
  constant c_exp_beamlet_y_output_ampl    : real := c_exp_beamlet_y_ampl * g_beamlet_scale;
  constant c_exp_beamlet_y_output_phase   : real := c_exp_beamlet_y_phase;
  constant c_exp_beamlet_y_output_re      : real := c_exp_beamlet_y_re * g_beamlet_scale;
  constant c_exp_beamlet_y_output_im      : real := c_exp_beamlet_y_im * g_beamlet_scale;

  -- MM
  -- . Address widths of a single MM instance
  --   . c_sdp_S_pn = 12 instances
  constant c_addr_w_reg_diag_wg           : natural := 2;
  --   . c_sdp_N_beamsets = 2 instances
  constant c_addr_w_ram_ss_ss_wide        : natural := ceil_log2(c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_ram_bf_weights        : natural := ceil_log2(c_sdp_N_pol_bf * c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_reg_bf_scale          : natural := 1;
  constant c_addr_w_reg_hdr_dat           : natural := ceil_log2(field_nof_words(c_sdp_cep_hdr_field_arr, c_word_w));
  constant c_addr_w_reg_stat_enable_bst   : natural := c_sdp_reg_stat_enable_addr_w;
  constant c_addr_w_reg_dp_xonoff         : natural := 1;
  constant c_addr_w_ram_st_bst            : natural := ceil_log2(c_sdp_S_sub_bf * c_sdp_N_pol_bf * c_stat_data_sz);
  -- . Address spans of a single MM instance
  --   . c_sdp_S_pn = 12 instances
  constant c_mm_span_reg_diag_wg          : natural := 2**c_addr_w_reg_diag_wg;
  --   . c_sdp_N_beamsets = 2 instances
  constant c_mm_span_ram_ss_ss_wide       : natural := 2**c_addr_w_ram_ss_ss_wide;
  constant c_mm_span_ram_bf_weights       : natural := 2**c_addr_w_ram_bf_weights;
  constant c_mm_span_reg_bf_scale         : natural := 2**c_addr_w_reg_bf_scale;
  constant c_mm_span_reg_hdr_dat          : natural := 2**c_addr_w_reg_hdr_dat;
  constant c_mm_span_reg_stat_enable_bst  : natural := 2**c_addr_w_reg_stat_enable_bst;
  constant c_mm_span_reg_dp_xonoff        : natural := 2**c_addr_w_reg_dp_xonoff;
  constant c_mm_span_ram_st_bst           : natural := 2**c_addr_w_ram_st_bst;

  -- Use c_mm_file_reg_* on c_last_gn for MM accesses to only last node in ring, e.g. for control beamlet output, read BSN, read back
  -- Use mmf_unb_file_prefix() and range(g_nof_rn) for MM access loop to all nodes in ring, e.g. for control BSN source, BF weights
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_BSN_SCHEDULER";  -- read current BSN
  constant c_mm_file_ram_equalizer_gains  : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "RAM_EQUALIZER_GAINS";  -- read default subband weight
  constant c_mm_file_reg_dp_selector      : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_DP_SELECTOR";  -- read sst_weighted_subbands_flag
  constant c_mm_file_ram_st_sst           : string := mmf_unb_file_prefix(c_global_sp_unb_nr, c_global_sp_node_nr) & "RAM_ST_SST";  -- read SST from GN with g_global_sp
  constant c_mm_file_ram_st_bst           : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "RAM_ST_BST";  -- read BST
  constant c_mm_file_reg_stat_enable_bst  : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_STAT_ENABLE_BST";  -- control BST offload
  constant c_mm_file_reg_dp_xonoff        : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_DP_XONOFF";  -- beamlet output
  constant c_mm_file_ram_ss_ss_wide       : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "RAM_SS_SS_WIDE";  -- readback
  constant c_mm_file_ram_bf_weights       : string := mmf_unb_file_prefix(c_global_sp_unb_nr, c_global_sp_node_nr) & "RAM_BF_WEIGHTS";  -- readback from GN with g_global_sp
  constant c_mm_file_reg_bf_scale         : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_BF_SCALE";  -- readback
  constant c_mm_file_reg_hdr_dat          : string := mmf_unb_file_prefix(c_last_unb_nr, c_last_node_nr) & "REG_HDR_DAT";  -- control beamlet output

  -- Tb BSN moments
  constant c_stimuli_done_bsn             : natural := c_init_bsn + c_nof_block_per_sync * 3;
  constant c_verify_rx_beamlet_list_bsn   : natural := c_stimuli_done_bsn - c_nof_block_per_sync + 2;

  -- Tb
  signal stimuli_done        : std_logic := '0';
  signal tb_almost_end       : std_logic := '0';
  signal tb_end              : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);
  signal rd_data_bsn         : std_logic_vector(c_32 - 1 downto 0);

  signal dest_rst            : std_logic := '1';  -- use separate destination rst for Rx 10GbE in tb
  signal pps_rst             : std_logic := '1';  -- use separate reset to release the PPS generator
  signal gen_pps             : std_logic := '0';

  signal in_sync             : std_logic := '0';
  signal in_sync_cnt         : natural := 0;
  signal test_sync_cnt       : integer := 0;

  -- MM
  signal rd_sdp_info         : t_sdp_info := c_sdp_info_rst;
  signal rd_beamlet_scale    : std_logic_vector(15 downto 0);
  signal rd_cep_eth_src_mac  : std_logic_vector(47 downto 0);
  signal rd_cep_eth_dst_mac  : std_logic_vector(47 downto 0);
  signal rd_cep_ip_src_addr  : std_logic_vector(31 downto 0);
  signal rd_cep_ip_dst_addr  : std_logic_vector(31 downto 0);
  signal rd_cep_udp_src_port : std_logic_vector(15 downto 0);
  signal rd_cep_udp_dst_port : std_logic_vector(15 downto 0);

  -- WG
  signal current_bsn_wg      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- FSUB
  -- . Read sp_ssts_arr2 = SST for one WPFB unit that processes g_global_sp
  signal sp_ssts_arr2        : t_slv_64_subbands_arr(c_sdp_N_pol - 1 downto 0);  -- [pol][sub], for X,Y pair of A, B
  signal sp_sst              : real := 0.0;
  signal stat_data           : std_logic_vector(c_longword_w - 1 downto 0);

  -- . Selector
  signal sst_weighted_subbands_flag : std_logic;

  -- . Subband equalizer
  signal sp_subband_weight_re    : integer := 0;
  signal sp_subband_weight_im    : integer := 0;
  signal sp_subband_weight_gain  : real := 0.0;
  signal sp_subband_weight_phase : real := 0.0;

  -- BF
  -- . beamlet subband selection
  signal sp_subband_select         : natural :=  0;
  signal sp_subband_select_arr     : t_integer_arr(0 to c_sdp_S_sub_bf * c_sdp_N_pol - 1) := (others => -1);  -- Q_fft = N_pol = 2

  -- . beamlet X-pol
  signal sp_bf_x_weights_re_arr    : t_integer_arr(c_first_sp to c_last_sp) := (others => 0);
  signal sp_bf_x_weights_im_arr    : t_integer_arr(c_first_sp to c_last_sp) := (others => 0);
  signal sp_bf_x_weights_gain_arr  : t_real_arr(c_first_sp to c_last_sp) := (others => 0.0);
  signal sp_bf_x_weights_phase_arr : t_real_arr(c_first_sp to c_last_sp) := (others => 0.0);
  -- . beamlet Y-pol
  signal sp_bf_y_weights_re_arr    : t_integer_arr(c_first_sp to c_last_sp) := (others => 0);
  signal sp_bf_y_weights_im_arr    : t_integer_arr(c_first_sp to c_last_sp) := (others => 0);
  signal sp_bf_y_weights_gain_arr  : t_real_arr(c_first_sp to c_last_sp) := (others => 0.0);
  signal sp_bf_y_weights_phase_arr : t_real_arr(c_first_sp to c_last_sp) := (others => 0.0);

  -- . BST
  signal bsts_arr2           : t_slv_64_beamlets_arr(c_sdp_N_pol_bf - 1 downto 0);  -- [pol_bf][blet]
  signal bst_x_arr           : t_real_arr(0 to c_sdp_N_beamsets - 1) := (others => 0.0);  -- [bset] for BF X pol
  signal bst_y_arr           : t_real_arr(0 to c_sdp_N_beamsets - 1) := (others => 0.0);  -- [bset] for BF Y pol

  -- CEP model
  -- . 10GbE
  signal tr_10GbE_src_out    : t_dp_sosi;
  signal tr_10GbE_src_in     : t_dp_siso;
  signal tr_ref_clk_312      : std_logic := '0';
  signal tr_ref_clk_156      : std_logic := '0';
  signal tr_ref_rst_156      : std_logic := '0';

  -- . dp_offload_rx
  signal rx_hdr_dat_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal rx_hdr_dat_miso     : t_mem_miso;

  signal rx_hdr_fields_out   : std_logic_vector(1023 downto 0);
  signal rx_hdr_fields_raw   : std_logic_vector(1023 downto 0) := (others => '0');

  -- Beamlets packets header
  signal rx_sdp_cep_header   : t_sdp_cep_header;
  signal exp_sdp_cep_header  : t_sdp_cep_header;
  signal exp_dp_bsn          : natural;
  signal exp_payload_error   : std_logic := '0';

  -- Beamlets packets data
  signal rx_beamlet_data     : std_logic_vector(c_longword_w - 1 downto 0);  -- 64 bit
  signal rx_beamlet_sosi     : t_dp_sosi := c_dp_sosi_rst;
  signal rx_beamlet_sop_cnt  : natural := 0;
  signal rx_beamlet_eop_cnt  : natural := 0;

  -- [0 : 3] =  X, Y, X, Y
  signal rx_beamlet_arr_re   : t_sdp_beamlet_part_arr;
  signal rx_beamlet_arr_im   : t_sdp_beamlet_part_arr;
  signal rx_beamlet_cnt      : natural;
  signal rx_beamlet_valid    : std_logic;

  -- [0 : 4 * 488 * 2 - 1] = [0 : 3903]
  signal rx_packet_list_re      : t_sdp_beamlet_packet_list;
  signal rx_packet_list_im      : t_sdp_beamlet_packet_list;
  signal rx_reordered_list_re   : t_sdp_beamlet_packet_list;
  signal rx_reordered_list_im   : t_sdp_beamlet_packet_list;

  -- Recover original beamlet order per block, either by using
  -- c_use_bdo_transpose = false or by using c_use_bdo_transpose = true
  -- and func_sdp_undo_transpose_beamlet_packet().
  -- List: [0 : 488 * 2 - 1] = [0 : 975]
  -- . X part at even indices
  -- . Y part at odd indices
  signal prev_rx_beamlet_list_re : t_sdp_beamlet_block_list;
  signal prev_rx_beamlet_list_im : t_sdp_beamlet_block_list;
  signal rx_beamlet_list_re      : t_sdp_beamlet_block_list;
  signal rx_beamlet_list_im      : t_sdp_beamlet_block_list;
  signal rx_beamlet_list_val     : std_logic;
  signal verify_rx_beamlet_list  : std_logic := '0';

  signal rx_beamlet_x_output_re : integer;
  signal rx_beamlet_x_output_im : integer;
  signal rx_beamlet_y_output_re : integer;
  signal rx_beamlet_y_output_im : integer;

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal ext_pps             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

  signal SA_CLK              : std_logic := '1';
  signal i_QSFP_0_TX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_QSFP_0_RX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_RING_0_TX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_RING_0_RX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_RING_1_TX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_RING_1_RX         : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));
  signal i_QSFP_1_lpbk       : t_unb2c_board_qsfp_bus_2arr(c_last_rn downto 0) := (others => (others => '0'));

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)
  dest_rst <= '0' after c_ext_clk_period * 10;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, gen_pps);
  jesd204b_sysref <= gen_pps;
  ext_pps <= gen_pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  gen_dut : for RN in 0 to c_last_rn generate
    u_lofar_unb2c_sdp_station_bf : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
    generic map (
      g_design_name            => c_design_name,
      g_design_note            => "",
      g_sim                    => c_sim,
      g_sim_unb_nr             => (g_first_gn + RN) / c_quad,
      g_sim_node_nr            => (g_first_gn + RN) mod c_quad,
      g_wpfb                   => c_wpfb_sim,
      g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync,
      g_scope_selected_subband => g_subband
    )
    port map (
      -- GENERAL
      CLK          => ext_clk,
      PPS          => ext_pps,
      WDI          => WDI,
      INTA         => INTA,
      INTB         => INTB,

      -- Others
      VERSION      => c_version,
      ID           => ( TO_UVEC((g_first_gn + RN) / c_quad, c_unb2c_board_nof_uniboard_w) & TO_UVEC((g_first_gn + RN) mod c_quad, c_unb2c_board_nof_chip_w) ),
      TESTIO       => open,

      -- 1GbE Control Interface
      ETH_CLK      => eth_clk,
      ETH_SGIN     => eth_rxp,
      ETH_SGOUT    => eth_txp,

      -- Transceiver clocks
      SA_CLK       => SA_CLK,
      -- front transceivers for ring
      QSFP_0_RX    => i_QSFP_0_RX(RN),
      QSFP_0_TX    => i_QSFP_0_TX(RN),

      -- ring transceivers
      RING_0_RX    => i_RING_0_RX(RN),
      RING_0_TX    => i_RING_0_TX(RN),
      RING_1_RX    => i_RING_1_RX(RN),
      RING_1_TX    => i_RING_1_TX(RN),

      -- front transceivers for CEP
      QSFP_1_RX    => i_QSFP_1_lpbk(RN),
      QSFP_1_TX    => i_QSFP_1_lpbk(RN),

      -- LEDs
      QSFP_LED     => open,

      -- back transceivers
      JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
      JESD204B_REFCLK      => JESD204B_REFCLK,

      -- jesd204b syncronization signals
      JESD204B_SYSREF => jesd204b_sysref,
      JESD204B_SYNC_N => jesd204b_sync_n
    );
  end generate;

  -- Ring connections
  gen_ring : for RN in 0 to c_last_rn - 1 generate
    -- Connect consecutive nodes with RING interfaces (PCB)
    i_RING_0_RX(RN + 1) <= i_RING_1_TX(RN);
    i_RING_1_RX(RN)     <= i_RING_0_TX(RN + 1);
  end generate;

  -- Connect first and last nodes with QSFP interface.
  i_QSFP_0_RX(0)         <= i_QSFP_0_TX(c_last_rn);
  i_QSFP_0_RX(c_last_rn) <= i_QSFP_0_TX(0);

  ------------------------------------------------------------------------------
  -- CEP model
  ------------------------------------------------------------------------------
  u_unb2_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  port map (
    refclk_644 => SA_CLK,
    rst_in     => dest_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
  generic map (
    g_sim           => true,
    g_sim_level     => 1,
    g_nof_macs      => 1,
    g_use_mdio      => false
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644      => SA_CLK,
    tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

    -- MM interface
    mm_rst              => dest_rst,
    mm_clk              => tb_clk,

    -- DP interface
    dp_rst              => dest_rst,
    dp_clk              => ext_clk,

    serial_rx_arr(0)    => i_QSFP_1_lpbk(c_last_rn)(0),  -- Last RN must be used as end node.

    src_out_arr(0)      => tr_10GbE_src_out,
    src_in_arr(0)       => tr_10GbE_src_in
  );

  u_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => c_longword_w,
    g_symbol_w            => c_octet_w,
    g_hdr_field_arr       => c_sdp_cep_hdr_field_arr,
    g_remove_crc          => false,
    g_crc_nof_words       => 0
  )
  port map (
    mm_rst                => dest_rst,
    mm_clk                => tb_clk,

    dp_rst                => dest_rst,
    dp_clk                => ext_clk,

    reg_hdr_dat_mosi      => rx_hdr_dat_mosi,
    reg_hdr_dat_miso      => rx_hdr_dat_miso,

    snk_in_arr(0)         => tr_10GbE_src_out,
    snk_out_arr(0)        => tr_10GbE_src_in,

    src_out_arr(0)        => rx_beamlet_sosi,

    hdr_fields_out_arr(0) => rx_hdr_fields_out,
    hdr_fields_raw_arr(0) => rx_hdr_fields_raw
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_gn                                   : natural;
    variable v_bsn                                  : natural;
    variable v_data_lo, v_data_hi                   : std_logic_vector(c_word_w - 1 downto 0);
    variable v_stat_data                            : std_logic_vector(c_longword_w - 1 downto 0);
    variable v_len, v_span, v_offset, v_addr, v_sel : natural;  -- address ranges, indices
    variable v_W, v_P, v_PB, v_S, v_A, v_B, v_G     : natural;  -- array indicies
    variable v_re, v_im, v_weight                   : integer;
    variable v_re_exp, v_im_exp                     : real := 0.0;
  begin
    -- Check g_global_sp
    if g_global_sp < c_first_sp or g_global_sp > c_last_sp then
      assert false
        report "g_global_sp not in ring GN range."
        severity ERROR;
      assert false
        report "g_global_sp not in ring GN range."
        severity FAILURE;
    end if;

    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Set and check SDP info
    ----------------------------------------------------------------------------
    --     TYPE t_sdp_info IS RECORD
    --   8   antenna_field_index     : STD_LOGIC_VECTOR(5 DOWNTO 0);
    --   7   station_id              : STD_LOGIC_VECTOR(9 DOWNTO 0);
    --   6   antenna_band_index      : STD_LOGIC;
    --   5   observation_id          : STD_LOGIC_VECTOR(31 DOWNTO 0);
    --   4   nyquist_zone_index      : STD_LOGIC_VECTOR(1 DOWNTO 0);
    --   3   f_adc                   : STD_LOGIC;
    --   2   fsub_type               : STD_LOGIC;
    --   1   beam_repositioning_flag : STD_LOGIC;
    --   0   block_period            : STD_LOGIC_VECTOR(15 DOWNTO 0);
    --     END RECORD;
    -- . Write
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  8, TO_UINT(c_exp_sdp_info.antenna_field_index), tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  7, TO_UINT(c_exp_sdp_info.station_id), tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  6, TO_UINT(slv(c_exp_sdp_info.antenna_band_index)), tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  5, TO_UINT(c_exp_sdp_info.observation_id), tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  4, TO_UINT(c_exp_sdp_info.nyquist_zone_index), tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  1, TO_UINT(slv(c_exp_sdp_info.beam_repositioning_flag)), tb_clk);
      -- . Read
      mmf_mm_bus_rd(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  3, rd_data, tb_clk); rd_sdp_info.f_adc <= rd_data(0);
      mmf_mm_bus_rd(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  2, rd_data, tb_clk); rd_sdp_info.fsub_type <= rd_data(0);
      mmf_mm_bus_rd(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_SDP_INFO",  0, rd_data, tb_clk); rd_sdp_info.block_period <= rd_data(15 downto 0);
    end loop;

    proc_common_wait_some_cycles(tb_clk, 1);
    -- . Verify read
    assert c_exp_sdp_info.f_adc = rd_sdp_info.f_adc
      report "Wrong MM read SDP info f_adc"
      severity ERROR;
    assert c_exp_sdp_info.fsub_type = rd_sdp_info.fsub_type
      report "Wrong MM read SDP info fsub_type"
      severity ERROR;
    assert c_exp_sdp_info.block_period = rd_sdp_info.block_period
      report "Wrong MM read SDP info block_period"
      severity ERROR;

    ------------------------------------------------------------------------------
    ---- Set and check BF per beamset
    ------------------------------------------------------------------------------
    for bset in 0 to c_sdp_N_beamsets - 1 loop
      ----------------------------------------------------------------------------
      -- MM beamlet_scale on last node
      ----------------------------------------------------------------------------
      -- . write
      v_offset := bset * c_mm_span_reg_bf_scale;
      mmf_mm_bus_wr(c_mm_file_reg_bf_scale, v_offset + 0, c_exp_beamlet_scale, tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);

      -- . readback
      mmf_mm_bus_rd(c_mm_file_reg_bf_scale, v_offset + 0, rd_data, tb_clk);
      rd_beamlet_scale <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert TO_UINT(rd_beamlet_scale) = c_exp_beamlet_scale
        report "Wrong MM read beamlet_scale for beamset " & natural'image(bset)
        severity ERROR;

      ----------------------------------------------------------------------------
      -- Set CEP beamlets output MAC,IP,UDP port on last node
      ----------------------------------------------------------------------------
      -- CEP beamlet output header
      --     c_sdp_cep_hdr_field_arr : t_common_field_arr(c_sdp_cep_nof_hdr_fields-1 DOWNTO 0) := (
      --  40   "eth_dst_mac"                        ), "RW", 48, field_default(c_sdp_cep_eth_dst_mac) ),
      --  38   "eth_src_mac"                        ), "RW", 48, field_default(0) ),
      --  37   "eth_type"                           ), "RW", 16, field_default(x"0800") ),
      --
      --  36   "ip_version"                         ), "RW",  4, field_default(4) ),
      --  35   "ip_header_length"                   ), "RW",  4, field_default(5) ),
      --  34   "ip_services"                        ), "RW",  8, field_default(0) ),
      --  33   "ip_total_length"                    ), "RW", 16, field_default(c_sdp_cep_ip_total_length) ),
      --  32   "ip_identification"                  ), "RW", 16, field_default(0) ),
      --  31   "ip_flags"                           ), "RW",  3, field_default(2) ),
      --  30   "ip_fragment_offset"                 ), "RW", 13, field_default(0) ),
      --  29   "ip_time_to_live"                    ), "RW",  8, field_default(127) ),
      --  28   "ip_protocol"                        ), "RW",  8, field_default(17) ),
      --  27   "ip_header_checksum"                 ), "RW", 16, field_default(0) ),
      --  26   "ip_src_addr"                        ), "RW", 32, field_default(0) ),
      --  25   "ip_dst_addr"                        ), "RW", 32, field_default(c_sdp_cep_ip_dst_addr) ),
      --
      --  24   "udp_src_port"                       ), "RW", 16, field_default(0) ),
      --  23   "udp_dst_port"                       ), "RW", 16, field_default(c_sdp_cep_udp_dst_port) ),
      --  22   "udp_total_length"                   ), "RW", 16, field_default(c_sdp_cep_udp_total_length) ),
      --  21   "udp_checksum"                       ), "RW", 16, field_default(0) ),
      --
      --  20   "sdp_marker"                         ), "RW",  8, field_default(c_sdp_marker_beamlets) ),
      --  19   "sdp_version_id"                     ), "RW",  8, field_default(c_sdp_cep_version_id) ),
      --  18   "sdp_observation_id"                 ), "RW", 32, field_default(0) ),
      --  17   "sdp_station_info"                   ), "RW", 16, field_default(0) ),
      --
      --  16   "sdp_source_info_reserved"               ), "RW",  5, field_default(0) ),
      --  15   "sdp_source_info_antenna_band_id"        ), "RW",  1, field_default(0) ),
      --  14   "sdp_source_info_nyquist_zone_id"        ), "RW",  2, field_default(0) ),
      --  13   "sdp_source_info_f_adc"                  ), "RW",  1, field_default(0) ),
      --  12   "sdp_source_info_fsub_type"              ), "RW",  1, field_default(0) ),
      --  11   "sdp_source_info_payload_error"          ), "RW",  1, field_default(0) ),
      --  10   "sdp_source_info_beam_repositioning_flag"), "RW",  1, field_default(0) ),
      --   9   "sdp_source_info_beamlet_width"          ), "RW",  4, field_default(c_sdp_W_beamlet) ),
      --   8   "sdp_source_info_gn_id"                  ), "RW",  8, field_default(0) ),
      --
      --   7   "sdp_reserved"                       ), "RW", 32, field_default(0) ),
      --   6   "sdp_beamlet_scale"                  ), "RW", 16, field_default(c_sdp_unit_beamlet_scale) ),
      --   5   "sdp_beamlet_index"                  ), "RW", 16, field_default(0) ),
      --   4   "sdp_nof_blocks_per_packet"          ), "RW",  8, field_default(c_sdp_cep_nof_blocks_per_packet) ),
      --   3   "sdp_nof_beamlets_per_block"         ), "RW", 16, field_default(c_sdp_cep_nof_beamlets_per_block) ),
      --   2   "sdp_block_period"                   ), "RW", 16, field_default(c_sdp_block_period) ),
      --
      --   0   "dp_bsn"                             ), "RW", 64, field_default(0) )
      --     );

      v_offset := bset * c_mm_span_reg_hdr_dat;
      -- Default destination MAC/IP/UDP = 0
      -- . Read
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 39, rd_data, tb_clk); rd_cep_eth_src_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 38, rd_data, tb_clk); rd_cep_eth_src_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 26, rd_data, tb_clk); rd_cep_ip_src_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 24, rd_data, tb_clk); rd_cep_udp_src_port <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 41, rd_data, tb_clk); rd_cep_eth_dst_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 40, rd_data, tb_clk); rd_cep_eth_dst_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 25, rd_data, tb_clk); rd_cep_ip_dst_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 23, rd_data, tb_clk); rd_cep_udp_dst_port <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      -- . verify read
      assert unsigned(rd_cep_eth_src_mac) = 0
        report "Wrong MM read rd_cep_eth_src_mac != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_ip_src_addr) = 0
        report "Wrong MM read rd_cep_ip_src_addr != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_udp_src_port) = 0
        report "Wrong MM read rd_cep_udp_src_port != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_eth_dst_mac) = 0
        report "Wrong MM read rd_cep_eth_dst_mac != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_ip_dst_addr) = 0
        report "Wrong MM read rd_cep_ip_dst_addr != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_udp_dst_port) = 0
        report "Wrong MM read rd_cep_udp_dst_port != 0 for beamset " & natural'image(bset)
        severity ERROR;

      -- Write tb defaults
      -- . Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on c_last_gn
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 39, TO_UINT(c_cep_eth_src_mac(47 downto 32)), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 38, TO_SINT(c_cep_eth_src_mac(31 downto 0)), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 26, TO_SINT(c_cep_ip_src_addr), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 24, TO_UINT(c_cep_udp_src_port), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 41, TO_UINT(c_sdp_cep_eth_dst_mac(47 downto 32)), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 40, TO_SINT(c_sdp_cep_eth_dst_mac(31 downto 0)), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 25, TO_SINT(c_sdp_cep_ip_dst_addr), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 23, TO_UINT(c_sdp_cep_udp_dst_port), tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);

      -- . Read back
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 39, rd_data, tb_clk); rd_cep_eth_src_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 38, rd_data, tb_clk); rd_cep_eth_src_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 26, rd_data, tb_clk); rd_cep_ip_src_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 24, rd_data, tb_clk); rd_cep_udp_src_port <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 41, rd_data, tb_clk); rd_cep_eth_dst_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 40, rd_data, tb_clk); rd_cep_eth_dst_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 25, rd_data, tb_clk); rd_cep_ip_dst_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 23, rd_data, tb_clk); rd_cep_udp_dst_port <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      -- . verify read back
      assert rd_cep_eth_src_mac = c_cep_eth_src_mac
        report "Wrong MM read rd_cep_eth_src_mac for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_ip_src_addr = c_cep_ip_src_addr
        report "Wrong MM read rd_cep_ip_src_addr for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_udp_src_port = c_cep_udp_src_port
        report "Wrong MM read rd_cep_udp_src_port for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_eth_dst_mac = c_sdp_cep_eth_dst_mac
        report "Wrong MM read rd_cep_eth_dst_mac for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_ip_dst_addr = c_sdp_cep_ip_dst_addr
        report "Wrong MM read rd_cep_ip_dst_addr for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_udp_dst_port = c_sdp_cep_udp_dst_port
        report "Wrong MM read rd_cep_udp_dst_port for beamset " & natural'image(bset)
        severity ERROR;

      ----------------------------------------------------------------------------
      -- Enable BST offload on last node (not verified here, but only for view in Wave window)
      ----------------------------------------------------------------------------

      -- Do not enable BST offload, because MM accesses interfer with MP read
      -- accesses via c_mm_file_ram_st_bst, and causes incorrects BST values.
      --v_offset := bset * c_mm_span_reg_stat_enable_bst;
      --mmf_mm_bus_wr(c_mm_file_reg_stat_enable_bst, v_offset + 0, 1, tb_clk);

      ----------------------------------------------------------------------------
      -- Enable beamlet output on last node (dp_xonoff)
      ----------------------------------------------------------------------------
      v_offset := bset * c_mm_span_reg_dp_xonoff;
      mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff, v_offset + 0, 1, tb_clk);
    end loop;

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SOURCE_V2", 2,         c_init_bsn, tb_clk);  -- Init BSN
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SOURCE_V2", 3,                  0, tb_clk);  -- Write high part a
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SOURCE_V2", 1, c_nof_clk_per_sync, tb_clk);  -- nof_block_per_syn
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SOURCE_V2", 0,       16#00000003#, tb_clk);  -- Enable BS at PPS
    end loop;

    -- Release PPS pulser, to get first PPS now and to start BSN source
    wait for 1 us;
    pps_rst <= '0';

    ----------------------------------------------------------------------------
    -- Ring config
    ----------------------------------------------------------------------------
    -- Write ring configuration to all nodes.
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 2, g_nof_rn, tb_clk);  -- N_rn
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 3, g_first_gn, tb_clk);  -- O_rn
    end loop;

    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      if v_gn = g_first_gn then
        -- Start node specific settings
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 0, 1, tb_clk);  -- use_cable_to_previous_rn = 1
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 1, 0, tb_clk);  -- use_cable_to_next_rn = 0
      elsif v_gn = c_last_gn then
        -- End node specific settings
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 0, 0, tb_clk);  -- use_cable_to_previous_rn = 0
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 1, 1, tb_clk);  -- use_cable_to_next_rn = 1
      else
        -- Use same settings for all nodes in between
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 0, 0, tb_clk);  -- use_cable_to_previous_rn = 0
        mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_INFO", 1, 0, tb_clk);  -- use_cable_to_next_rn = 0
      end if;
    end loop;

    -- Access scheme 1 for beamformer.
    -- Use transport_nof_hops = 0 on last node to stop remote input for first node. No
    -- need to also disable the remote input of the BSN aligner on the first node.
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      for I in 0 to c_nof_lanes - 1 loop
        if RN = c_last_rn then
          -- End RN, so set transport_nof_hops to 0.
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_LANE_INFO_BF", I * 2 + 1, 0, tb_clk);
        else
          -- Set transport_nof_hops to 1 on all nodes.
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_RING_LANE_INFO_BF", I * 2 + 1, 1, tb_clk);
        end if;
      end loop;
    end loop;

    ----------------------------------------------------------------------------
    -- Enable and start WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    -- . Put wanted signal on g_global_sp input and remnant signal on all g_nof_rn * S_pn - 1 other inputs
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      for S in 0 to c_sdp_S_pn - 1 loop
        if v_gn * c_sdp_S_pn + S = g_global_sp then
          -- Strong WG signal at g_global_sp
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 1, integer(c_wg_phase * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 2, integer(real(g_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 3, integer(real(c_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
        else
          -- Weak WG signal on all other (remnant) SP
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 1, integer(c_wg_remnant_phase * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 2, integer(real(g_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
          mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_WG", S * 4 + 3, integer(real(c_wg_remnant_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
        end if;
      end loop;
    end loop;

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;

    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SCHEDULER", 0, c_bsn_start_wg, tb_clk);  -- first write low then high part
      mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "REG_BSN_SCHEDULER", 1,              0, tb_clk);  -- assume v_bsn < 2**31-1
    end loop;

    ----------------------------------------------------------------------------
    -- Read weighted subband selector
    ----------------------------------------------------------------------------
    mmf_mm_bus_rd(c_mm_file_reg_dp_selector, 0, rd_data, tb_clk);
    sst_weighted_subbands_flag <= not rd_data(0);
    proc_common_wait_some_cycles(tb_clk, 1);

    ----------------------------------------------------------------------------
    -- Subband weight
    ----------------------------------------------------------------------------

    -- . MM format: (cint16)RAM_EQUALIZER_GAINS[S_pn/Q_fft]_[Q_fft][N_sub] = [S_pn][N_sub]
    v_addr := c_local_sp * c_sdp_N_sub + g_subband;
    -- . read
    mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains, v_addr, rd_data, tb_clk);
    v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
    v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
    sp_subband_weight_re <= v_re;
    sp_subband_weight_im <= v_im;
    sp_subband_weight_gain <= COMPLEX_RADIUS(v_re, v_im) / real(c_sdp_unit_sub_weight);
    sp_subband_weight_phase <= COMPLEX_PHASE(v_re, v_im);

    -- No need to write subband weight, because default it is unit weight

    ----------------------------------------------------------------------------
    -- Subband select to map subband to beamlet
    ----------------------------------------------------------------------------
    -- . MM format: (uint16)RAM_SS_SS_WIDE[N_beamsets][A_pn]_[S_sub_bf][Q_fft], Q_fft = N_pol = 2

    -- . write selection, only for g_beamlet to save sim time
    v_span := true_log_pow2(c_sdp_N_pol * c_sdp_S_sub_bf);  -- = 1024
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- Same selection for both beamsets
      -- Select beamlet g_beamlet to subband g_subband
      for A in 0 to c_sdp_A_pn - 1 loop
        -- Same selection to all SP
        for P in 0 to c_sdp_N_pol - 1 loop
          v_addr := P + g_beamlet * c_sdp_N_pol + A * v_span + U * c_mm_span_ram_ss_ss_wide;
          v_sel := P + g_subband * c_sdp_N_pol;
          for RN in 0 to c_last_rn loop
            v_gn := g_first_gn + RN;
            mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "RAM_SS_SS_WIDE", v_addr, v_sel, tb_clk);
          end loop;
        end loop;
      end loop;
    end loop;

    -- . read back selection for c_local_sp = c_pfb_index * c_sdp_N_pol + c_pol_index
    v_P := c_pol_index;
    v_A := c_pfb_index;
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- Same selection for both beamsets, so fine to use only one sp_subband_select_arr()
      for B in 0 to c_sdp_S_sub_bf - 1 loop
        if g_read_all_sub_sel or B = g_beamlet then
          -- Same selection for all SP, so fine to only read subband selection for c_local_sp on last node
          v_addr := v_P + B * c_sdp_N_pol + v_A * v_span + U * c_mm_span_ram_ss_ss_wide;
          mmf_mm_bus_rd(c_mm_file_ram_ss_ss_wide, v_addr, rd_data, tb_clk);
          v_sel := (TO_UINT(rd_data) - v_P) / c_sdp_N_pol;
          sp_subband_select_arr(B * c_sdp_N_pol + v_P) <= v_sel;
          sp_subband_select <= v_sel;  -- for time series view in Wave window
        end if;
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    assert sp_subband_select_arr(g_beamlet * c_sdp_N_pol + v_P) = g_subband
      report "Wrong subband select at beamlet index."
      severity ERROR;
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ----------------------------------------------------------------------------
    -- Write beamlet weight for g_beamlet in S_sub_bf
    ----------------------------------------------------------------------------
    -- . MM format: (cint16)RAM_BF_WEIGHTS[N_beamsets][N_pol_bf][A_pn]_[N_pol][S_sub_bf]

    -- . write BF weights, only for g_beamlet to save sim time
    v_span := true_log_pow2(c_sdp_N_pol * c_sdp_S_sub_bf);  -- = 1024
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      for U in 0 to c_sdp_N_beamsets - 1 loop
        -- Same BF weights for both beamsets
        for PB in 0 to c_sdp_N_pol_bf - 1 loop
          -- Different BF weights for both beamlet polarizations
          for A in 0 to c_sdp_A_pn - 1 loop
            for P in 0 to c_sdp_N_pol - 1 loop
              v_S := v_gn * c_sdp_S_pn + A * c_sdp_N_pol + P;
              -- Different BF weights for g_global_sp and the remnant sp
              if v_S = g_global_sp then
                -- use generic BF weight for g_global_sp in g_beamlet
                if PB = 0 then
                  v_weight := pack_complex(re => c_bf_x_weight_re, im => c_bf_x_weight_im, w => c_sdp_W_bf_weight);
                else
                  v_weight := pack_complex(re => c_bf_y_weight_re, im => c_bf_y_weight_im, w => c_sdp_W_bf_weight);
                end if;
              else
                -- use the remnant BF weights for the other SP
                if PB = 0 then
                  v_weight := pack_complex(re => c_bf_remnant_x_weight_re, im => c_bf_remnant_x_weight_im, w => c_sdp_W_bf_weight);
                else
                  v_weight := pack_complex(re => c_bf_remnant_y_weight_re, im => c_bf_remnant_y_weight_im, w => c_sdp_W_bf_weight);
                end if;
              end if;
              -- Only need to set BF weight for g_beamlet, keep other beamlet BF weights at zero rst default.
              v_addr := g_beamlet;  -- beamlet index
              v_addr := v_addr + P * c_sdp_S_sub_bf;  -- antenna input polarization address offset
              v_addr := v_addr + A * v_span;  -- antenna input address offset
              v_addr := v_addr + PB * c_sdp_A_pn * v_span;  -- beamlet polarization address offset
              v_addr := v_addr + U * c_mm_span_ram_bf_weights;  -- beamset address offset
              mmf_mm_bus_wr(mmf_unb_file_prefix(v_gn / c_quad, v_gn mod c_quad) & "RAM_BF_WEIGHTS", v_addr, v_weight, tb_clk);
            end loop;
          end loop;
        end loop;
      end loop;
    end loop;

    -- . read back BF weights for g_beamlet in S_sub_bf
    for RN in 0 to c_last_rn loop
      v_gn := g_first_gn + RN;
      for U in 0 to c_sdp_N_beamsets - 1 loop
        for PB in 0 to c_sdp_N_pol_bf - 1 loop
          for A in 0 to c_sdp_A_pn - 1 loop
            for P in 0 to c_sdp_N_pol - 1 loop
              v_addr := g_beamlet;  -- beamlet index
              v_addr := v_addr + P * c_sdp_S_sub_bf;  -- antenna input polarization address offset
              v_addr := v_addr + A * v_span;  -- antenna input address offset
              v_addr := v_addr + PB * c_sdp_A_pn * v_span;  -- beamlet polarization address offset
              v_addr := v_addr + U * c_mm_span_ram_bf_weights;  -- beamset address offset
              mmf_mm_bus_rd(c_mm_file_ram_bf_weights, v_addr, rd_data, tb_clk);
              v_re := unpack_complex_re(rd_data, c_sdp_W_bf_weight);
              v_im := unpack_complex_im(rd_data, c_sdp_W_bf_weight);
              -- same BF weights for both beamsets and both beamlet polarizations,
              -- so fine to use only one sp_bf_x_weights_*_arr()
              v_S := v_gn * c_sdp_S_pn + A * c_sdp_N_pol + P;
              if PB = 0 then
                sp_bf_x_weights_re_arr(v_S) <= v_re;
                sp_bf_x_weights_im_arr(v_S) <= v_im;
                sp_bf_x_weights_gain_arr(v_S) <= COMPLEX_RADIUS(v_re, v_im) / real(c_sdp_unit_bf_weight);
                sp_bf_x_weights_phase_arr(v_S) <= COMPLEX_PHASE(v_re, v_im);
              else
                sp_bf_y_weights_re_arr(v_S) <= v_re;
                sp_bf_y_weights_im_arr(v_S) <= v_im;
                sp_bf_y_weights_gain_arr(v_S) <= COMPLEX_RADIUS(v_re, v_im) / real(c_sdp_unit_bf_weight);
                sp_bf_y_weights_phase_arr(v_S) <= COMPLEX_PHASE(v_re, v_im);
              end if;
            end loop;
          end loop;
        end loop;
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ----------------------------------------------------------------------------
    -- Wait for enough WG data and start of sync interval
    ----------------------------------------------------------------------------
    -- read BSN low, this is the wait until condition
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,
                            "UNSIGNED", rd_data_bsn, ">=", c_stimuli_done_bsn,
                            c_sdp_T_sub, tb_clk);

    -- Stimuli done, now verify results at end of test
    stimuli_done <= '1';

    ---------------------------------------------------------------------------
    -- Read subband statistics from node with g_global_sp
    ---------------------------------------------------------------------------
    -- . the subband statistics are c_stat_data_sz = 2 word power values.
    -- . there are c_sdp_S_pn = 12 signal inputs A, B, C, D, E, F, G, H, I, J, K, L
    -- . there are c_sdp_N_sub = 512 subbands per signal input (SI, = signal path, SP)
    -- . one complex WPFB can process two real inputs A, B, so there are c_sdp_P_pfb = 6 WPFB units,
    --   but only read for the 1 WPFB unit of the selected g_global_sp, to save sim time
    -- . the outputs for A, B are time multiplexed, c_sdp_Q_fft = 2, assume that they
    --   correspond to the c_sdp_N_pol = 2 signal polarizations
    -- . the subbands are output alternately so A0 B0 A1 B1 ... A511 B511 for input A, B
    -- . the subband statistics multiple WPFB units appear in order in the ram_st_sst address map
    -- . the subband statistics are stored first lo word 0 then hi word 1
    v_len := c_sdp_N_sub * c_sdp_N_pol * c_stat_data_sz;  -- 2048 = 512 * 2 * 64/32
    v_span := true_log_pow2(v_len);  -- = 2048
    for I in 0 to v_len - 1 loop
      v_W := I mod c_stat_data_sz;  -- 0, 1 per statistics word, word index
      v_P := (I / c_stat_data_sz) mod c_sdp_N_pol;  -- 0, 1 per SP pol, polarization index
      v_B := I / (c_sdp_N_pol * c_stat_data_sz);  -- subband index, range(N_sub = 512) per dual pol
      v_addr := I + c_pfb_index * v_span;  -- MM address
      -- Only read SST for g_subband for dual pol SP, to save sim time
      if g_read_all_SST = true or v_B = g_subband then
        if v_W = 0 then
          -- low part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_lo := rd_data;
        else
          -- high part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_hi := rd_data;
          v_stat_data := v_data_hi & v_data_lo;

          sp_ssts_arr2(v_P)(v_B) <= v_stat_data;
          stat_data <= v_stat_data;  -- for time series view in Wave window
        end if;
      end if;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Subband power of g_subband in g_global_sp
    sp_sst <= TO_UREAL(sp_ssts_arr2(c_pol_index)(g_subband));
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ---------------------------------------------------------------------------
    -- Read beamlet statistics from last node
    ---------------------------------------------------------------------------
    -- . the beamlet statistics are c_stat_data_sz = 2 word power values.
    -- . there are c_sdp_S_sub_bf = 488 dual pol beamlets per beamset
    -- . the beamlets are output alternately so X0 Y0 X1 Y1 ... X487 Y487 for polarizations X, Y
    -- . the beamlet statistics for multiple beamsets appear in order in the ram_st_bst address map
    -- . the beamlet statistics are stored first lo word 0 then hi word 1
    v_len := c_sdp_S_sub_bf * c_sdp_N_pol_bf * c_stat_data_sz;  -- = 1952 = 488 * 2 * 64/32
    v_span := true_log_pow2(v_len);  -- = 2048
    for U in 0 to c_sdp_N_beamsets - 1 loop
      for I in 0 to v_len - 1 loop
        v_W := I mod c_stat_data_sz;  -- 0, 1 per statistics word, word index
        v_PB := (I / c_stat_data_sz) mod c_sdp_N_pol_bf;  -- 0, 1 per BF pol, polarization index
        v_B := I / (c_sdp_N_pol_bf * c_stat_data_sz);  -- beamlet index in beamset, range(S_sub_bf = 488) per dual pol
        v_G := v_B + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
        v_addr := I + U * v_span;  -- MM address
        --Only read BST for g_beamlet and dual pol_bf 0 and 1 and for both beamsets, to save sim time
        if g_read_all_BST = true or v_B = g_beamlet then
          if v_W = 0 then
            -- low part
            mmf_mm_bus_rd(c_mm_file_ram_st_bst, v_addr, rd_data, tb_clk);
            v_data_lo := rd_data;
          else
            -- high part
            mmf_mm_bus_rd(c_mm_file_ram_st_bst, v_addr, rd_data, tb_clk);
            v_data_hi := rd_data;
            v_stat_data := v_data_hi & v_data_lo;

            bsts_arr2(v_PB)(v_G) <= v_stat_data;
            stat_data <= v_stat_data;  -- for time series view in Wave window
          end if;
        end if;
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Beamlet power of g_beamlet X and Y, same for both beamsets
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      bst_x_arr(U) <= TO_UREAL(bsts_arr2(0)(v_G));  -- X pol beamlet
      bst_y_arr(U) <= TO_UREAL(bsts_arr2(1)(v_G));  -- Y pol beamlet
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ---------------------------------------------------------------------------
    -- Log WG, subband and beamlet statistics
    ---------------------------------------------------------------------------

    print_str("");
    print_str("* WG:");
    print_str(". c_wg_ampl                            = " & int_to_str(c_wg_ampl));
    print_str(". c_exp_sp_power                       = " & real_to_str(c_exp_sp_power, 20, 1));
    print_str(". c_exp_sp_ast                         = " & real_to_str(c_exp_sp_ast, 20, 1));

    print_str("");
    print_str("* Subband weight:");
    print_str(". sp_subband_weight_gain               = " & real_to_str(sp_subband_weight_gain, 20, 6));
    print_str(". sp_subband_weight_phase              = " & real_to_str(sp_subband_weight_phase, 20, 6));

    print_str("");
    print_str("* SST results:");
    print_str(". sst_weighted_subbands_flag           = " & sl_to_str(sst_weighted_subbands_flag));
    print_str("");
    print_str(". c_exp_subband_ampl                   = " & int_to_str(natural(c_exp_subband_ampl)));
    print_str(". c_exp_subband_power                  = " & real_to_str(c_exp_subband_power, 20, 1));
    print_str(". c_exp_subband_sst                    = " & real_to_str(c_exp_subband_sst, 20, 1));
    print_str("");
    print_str(". sp_sst                               = " & real_to_str(sp_sst, 20, 1));
    print_str(". sp_sst / c_exp_subband_sst           = " & real_to_str(sp_sst / c_exp_subband_sst, 20, 6));

    print_str("");
    print_str("* BST results:");
    print_str(". c_exp_beamlet_x_ampl                   = " & int_to_str(natural(c_exp_beamlet_x_ampl)));
    print_str(". c_exp_beamlet_x_power                  = " & real_to_str(c_exp_beamlet_x_power, 20, 1));
    print_str(". c_exp_beamlet_x_bst                    = " & real_to_str(c_exp_beamlet_x_bst, 20, 1));
    print_str("");
    print_str(". c_exp_beamlet_y_ampl                   = " & int_to_str(natural(c_exp_beamlet_y_ampl)));
    print_str(". c_exp_beamlet_y_power                  = " & real_to_str(c_exp_beamlet_y_power, 20, 1));
    print_str(". c_exp_beamlet_y_bst                    = " & real_to_str(c_exp_beamlet_y_bst, 20, 1));
    print_str("");
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      print_str(". bst_x_arr(" & integer'image(v_G) & ") = " & real_to_str(bst_x_arr(U), 20, 1));
      print_str(". bst_y_arr(" & integer'image(v_G) & ") = " & real_to_str(bst_y_arr(U), 20, 1));
    end loop;
    print_str("");
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      print_str(". bst_x_arr(" & integer'image(v_G) & ") / c_exp_beamlet_x_bst = " & real_to_str(bst_x_arr(U) / c_exp_beamlet_x_bst, 20, 6));
      print_str(". bst_y_arr(" & integer'image(v_G) & ") / c_exp_beamlet_y_bst = " & real_to_str(bst_y_arr(U) / c_exp_beamlet_y_bst, 20, 6));
    end loop;

    print_str("");
    print_str("* Beamlet scale:");
    print_str(". rd_beamlet_scale                     = " & int_to_str(TO_UINT(rd_beamlet_scale)));
    print_str(". c_exp_beamlet_scale                  = " & int_to_str(c_exp_beamlet_scale));
    print_str("");
    print_str("* Beamlet output:");
    print_str("  . c_exp_beamlet_x_output_ampl        = " & int_to_str(natural(c_exp_beamlet_x_output_ampl)));
    print_str("  . c_exp_beamlet_x_output_phase       = " & int_to_str(integer(c_exp_beamlet_x_output_phase)));
    print_str("  . rx_beamlet_x_output_re             = " & int_to_str(rx_beamlet_x_output_re));
    print_str("  . c_exp_beamlet_x_output_re          = " & int_to_str(integer(c_exp_beamlet_x_output_re)));
    print_str("  . rx_beamlet_x_output_im             = " & int_to_str(rx_beamlet_x_output_im));
    print_str("  . c_exp_beamlet_x_output_im          = " & int_to_str(integer(c_exp_beamlet_x_output_im)));
    print_str("");
    print_str("  . c_exp_beamlet_y_output_ampl        = " & int_to_str(natural(c_exp_beamlet_y_output_ampl)));
    print_str("  . c_exp_beamlet_y_output_phase       = " & int_to_str(integer(c_exp_beamlet_y_output_phase)));
    print_str("  . rx_beamlet_y_output_re             = " & int_to_str(rx_beamlet_y_output_re));
    print_str("  . c_exp_beamlet_y_output_re          = " & int_to_str(integer(c_exp_beamlet_y_output_re)));
    print_str("  . rx_beamlet_y_output_im             = " & int_to_str(rx_beamlet_y_output_im));
    print_str("  . c_exp_beamlet_y_output_im          = " & int_to_str(integer(c_exp_beamlet_y_output_im)));
    print_str("");
    print_str("  . c_beamlet_output_delta (+- margin)   = " & int_to_str(integer(c_beamlet_output_delta)));
    print_str("");

    ---------------------------------------------------------------------------
    -- Verify SST
    ---------------------------------------------------------------------------
    -- verify expected subband power based on WG power
    assert sp_sst > c_stat_lo_factor * c_exp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_global_sp)
      severity ERROR;
    assert sp_sst < c_stat_hi_factor * c_exp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_global_sp)
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Verify BST
    ---------------------------------------------------------------------------
    -- verify expected beamlet power based on WG power and BF weigths
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- X-pol
      assert bst_x_arr(U) < c_stat_hi_factor * c_exp_beamlet_x_bst
        report "Wrong beamlet X power in beamset " & natural'image(U)
        severity ERROR;
      assert bst_x_arr(U) > c_stat_lo_factor * c_exp_beamlet_x_bst
        report "Wrong beamlet X power in beamset " & natural'image(U)
        severity ERROR;
      -- Y-pol
      assert bst_y_arr(U) > c_stat_lo_factor * c_exp_beamlet_y_bst
        report "Wrong beamlet Y power in beamset " & natural'image(U)
        severity ERROR;
      assert bst_y_arr(U) < c_stat_hi_factor * c_exp_beamlet_y_bst
        report "Wrong beamlet Y power in beamset " & natural'image(U)
        severity ERROR;
    end loop;

    ---------------------------------------------------------------------------
    -- Verify beamlet output in 10GbE UDP offload
    ---------------------------------------------------------------------------
    -- X-pol
    v_re := rx_beamlet_x_output_re; v_re_exp := c_exp_beamlet_x_output_re;
    v_im := rx_beamlet_x_output_im; v_im_exp := c_exp_beamlet_x_output_im;
    assert v_re > integer(v_re_exp) - c_beamlet_output_delta
      report "Wrong beamlet X output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_re < integer(v_re_exp) + c_beamlet_output_delta
      report "Wrong beamlet X output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_im > integer(v_im_exp) - c_beamlet_output_delta
      report "Wrong beamlet X output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;
    assert v_im < integer(v_im_exp) + c_beamlet_output_delta
      report "Wrong beamlet X output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;
    -- Y-pol
    v_re := rx_beamlet_y_output_re; v_re_exp := c_exp_beamlet_y_output_re;
    v_im := rx_beamlet_y_output_im; v_im_exp := c_exp_beamlet_y_output_im;
    assert v_re > integer(v_re_exp) - c_beamlet_output_delta
      report "Wrong beamlet Y output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_re < integer(v_re_exp) + c_beamlet_output_delta
      report "Wrong beamlet Y output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_im > integer(v_im_exp) - c_beamlet_output_delta
      report "Wrong beamlet Y output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;
    assert v_im < integer(v_im_exp) + c_beamlet_output_delta
      report "Wrong beamlet Y output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    tb_almost_end <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window
    proc_common_stop_simulation(true, ext_clk, tb_almost_end, tb_end);
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verify beamlet offload packet header
  -----------------------------------------------------------------------------

  -- Counters to time expected exp_sdp_cep_header fields per offload packet
  p_test_counters : process(ext_clk)
  begin
    if rising_edge(ext_clk) then
      -- Count rx_beamlet_sosi packets
      if rx_beamlet_sosi.sop = '1' then
        rx_beamlet_sop_cnt <= rx_beamlet_sop_cnt + 1;  -- early count
      end if;
      if rx_beamlet_sosi.eop = '1' then
        rx_beamlet_eop_cnt <= rx_beamlet_eop_cnt + 1;  -- after count
      end if;
    end if;
  end process;

  -- Count sync intervals using in_sosi.sync, because there is no rx_beamlet_sosi.sync
  in_sync_cnt <= in_sync_cnt + 1 when rising_edge(ext_clk) and in_sync = '1';
  test_sync_cnt <= in_sync_cnt - 1;  -- optionally adjust to fit rx_beamlet_sosi

  -- Prepare exp_sdp_cep_header before rx_beamlet_sosi.eop, so that
  -- p_verify_cep_header can verify it at rx_beamlet_sosi.eop.
  exp_sdp_cep_header <= func_sdp_compose_cep_header(c_cep_ip_src_addr,
                                                    c_exp_ip_header_checksum,
                                                    c_exp_sdp_info,
                                                    c_last_gn,
                                                    exp_payload_error,
                                                    c_exp_beamlet_scale,
                                                    c_exp_beamlet_index,
                                                    exp_dp_bsn);

  rx_sdp_cep_header <= func_sdp_map_cep_header(rx_hdr_fields_raw);

  p_verify_cep_header : process
    variable v_pkt_cnt : natural;
    variable v_new_pkt : boolean;
    variable v_error   : std_logic := '0';
    variable v_bsn     : natural := 0;
    variable v_bool    : boolean;
  begin
    wait until rising_edge(ext_clk);
    -- Count packets per beamset
    v_pkt_cnt := rx_beamlet_sop_cnt / c_sdp_N_beamsets;
    v_new_pkt := rx_beamlet_sop_cnt mod c_sdp_N_beamsets = 0;

    -- Prepare exp_sdp_cep_header at sop, so that it can be verified at eop
    if rx_beamlet_sosi.sop = '1' then
      -- Expected BSN increments by c_sdp_cep_nof_blocks_per_packet = 4 blocks per packet,
      -- both beamsets are outputting packets.
      if v_new_pkt then
        -- Default expected
        v_error := '0';
        v_bsn := c_init_bsn + v_pkt_cnt * c_sdp_cep_nof_blocks_per_packet;

        -- Expected due to bsn and payload_error stimuli in sdp_beamformer_output.vhd.
        if v_pkt_cnt = 1 then
          v_error := '1';
        elsif v_pkt_cnt = 2 or v_pkt_cnt = 3 then
          v_bsn := v_bsn + 1;
        elsif v_pkt_cnt = 4 then
          v_bsn := v_bsn + 1;
          v_error := '1';
        end if;

        -- Apply expected values
        exp_payload_error <= v_error;
        exp_dp_bsn <= v_bsn;
      end if;
    end if;

    -- Verify header at eop
    -- . The expected beamlet_index 0 or S_sub_bf = 488 depends on beamset 0
    --   or 1, but the order in which the packets arrive is undetermined.
    --   Therefore accept any beamlet_index MOD c_sdp_S_sub_bf = 0 as correct
    --   in func_sdp_verify_cep_header().
    if rx_beamlet_sosi.eop = '1' then
      v_bool := func_sdp_verify_cep_header(rx_sdp_cep_header,
                                           exp_sdp_cep_header,
                                           c_beamlet_index_mod);
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- CEP Read Rx 10GbE Stream
  -----------------------------------------------------------------------------
  -- Show received beamlets from 10GbE stream in Wave Window
  -- . The packet header is 9.25 longwords wide. The dp_offload_rx has stripped
  --   the header and has realigned the payload at a longword boundary.
  -- . expect c_nof_block_per_sync / c_sdp_cep_nof_blocks_per_packet *
  --   c_sdp_N_beamsets = 16 / 4 * 2 = 4 * 2 = 8 packets per sync interval
  -- . expect c_sdp_cep_nof_beamlets_per_block = c_sdp_S_sub_bf = 488 dual pol
  --   and complex beamlets per packet, so 2 dual pol beamlets/64b data word.
  -- . Beamlets array is stored big endian in the data, so X.real index 0 first
  --   in MSByte of rx_beamlet_sosi.data.
  p_rx_cep_beamlets : process
  begin
    rx_beamlet_cnt <= 0;
    rx_beamlet_valid <= '0';
    -- Wait until start of a beamlet packet
    proc_common_wait_until_high(ext_clk, rx_beamlet_sosi.sop);
    -- c_sdp_nof_beamlets_per_longword = 2 dual pol beamlets (= XY, XY) per 64b data word
    for I in 0 to (c_sdp_cep_nof_beamlets_per_packet / c_sdp_nof_beamlets_per_longword) - 1 loop
      proc_common_wait_until_high(ext_clk, rx_beamlet_sosi.valid);
      rx_beamlet_valid <= '1';
      -- Capture rx beamlets per longword in rx_beamlet_arr, for time series view in Wave window
      rx_beamlet_arr_re(0) <= rx_beamlet_sosi.data(63 downto 56);  -- X
      rx_beamlet_arr_im(0) <= rx_beamlet_sosi.data(55 downto 48);
      rx_beamlet_arr_re(1) <= rx_beamlet_sosi.data(47 downto 40);  -- Y
      rx_beamlet_arr_im(1) <= rx_beamlet_sosi.data(39 downto 32);
      rx_beamlet_arr_re(2) <= rx_beamlet_sosi.data(31 downto 24);  -- X
      rx_beamlet_arr_im(2) <= rx_beamlet_sosi.data(23 downto 16);
      rx_beamlet_arr_re(3) <= rx_beamlet_sosi.data(15 downto 8);  -- Y
      rx_beamlet_arr_im(3) <= rx_beamlet_sosi.data( 7 downto 0);
      -- Capture the beamlets block of each packet in rx_packet_list
      rx_packet_list_re(I * 4 + 0) <= rx_beamlet_sosi.data(63 downto 56);  -- X
      rx_packet_list_im(I * 4 + 0) <= rx_beamlet_sosi.data(55 downto 48);
      rx_packet_list_re(I * 4 + 1) <= rx_beamlet_sosi.data(47 downto 40);  -- Y
      rx_packet_list_im(I * 4 + 1) <= rx_beamlet_sosi.data(39 downto 32);
      rx_packet_list_re(I * 4 + 2) <= rx_beamlet_sosi.data(31 downto 24);  -- X
      rx_packet_list_im(I * 4 + 2) <= rx_beamlet_sosi.data(23 downto 16);
      rx_packet_list_re(I * 4 + 3) <= rx_beamlet_sosi.data(15 downto 8);  -- Y
      rx_packet_list_im(I * 4 + 3) <= rx_beamlet_sosi.data( 7 downto 0);
      proc_common_wait_until_high(ext_clk, rx_beamlet_sosi.valid);
      -- Use at least one WAIT instead of proc_common_wait_some_cycles() to
      -- avoid Modelsim warning: (vcom-1090) Possible infinite loop: Process
      -- contains no WAIT statement.
      wait until rising_edge(ext_clk);
      rx_beamlet_valid <= '0';
      rx_beamlet_cnt   <= (rx_beamlet_cnt + c_sdp_nof_beamlets_per_longword) mod c_sdp_cep_nof_beamlets_per_block;  -- 4 blocks/packet
    end loop;
  end process;

  -- Undo the beamlet output transpose, to have original beamlet order
  p_rx_reordered_list : process
  begin
    -- Wait until end of a beamlet packet
    wait until rising_edge(ext_clk);  -- to avoid Modelsim warning: (vcom-1090)
    proc_common_wait_until_hi_lo(ext_clk, rx_beamlet_sosi.eop);  -- to reduce simulation effort
    rx_reordered_list_re <= func_sdp_undo_transpose_beamlet_packet(rx_packet_list_re);
    rx_reordered_list_im <= func_sdp_undo_transpose_beamlet_packet(rx_packet_list_im);
  end process;

  p_rx_beamlet_list : process
    constant c_N : natural := c_sdp_cep_nof_beamlets_per_block * c_sdp_N_pol_bf;
  begin
    rx_beamlet_list_val <= '0';

    -- Wait until after p_rx_reordered_list has updated
    proc_common_wait_until_hi_lo(ext_clk, rx_beamlet_sosi.eop);
    wait until rising_edge(ext_clk);

    -- Use same rx_beamlet_list to show all 4 blocks of a packet in time, to
    -- ease viewing the blocks in the wave window.
    for blk in 0 to c_sdp_cep_nof_blocks_per_packet - 1 loop
      -- Copy block blk from rx_packet_list into rx_beamlet_list of one block.
      if c_use_bdo_transpose then
        -- undone transposed beamlet output order
        rx_beamlet_list_re <= rx_reordered_list_re(blk * c_N to (blk + 1) * c_N - 1);
        rx_beamlet_list_im <= rx_reordered_list_im(blk * c_N to (blk + 1) * c_N - 1);
      else
        -- identity beamlet output order
        rx_beamlet_list_re <= rx_packet_list_re(blk * c_N to (blk + 1) * c_N - 1);
        rx_beamlet_list_im <= rx_packet_list_im(blk * c_N to (blk + 1) * c_N - 1);
      end if;
      rx_beamlet_list_val <= '1';
      wait until rising_edge(ext_clk);
    end loop;
  end process;

  -- Verify that beamlet values remain stable in time, so same beamlet value in each time block
  verify_rx_beamlet_list <= '1' when unsigned(rd_data_bsn) > c_verify_rx_beamlet_list_bsn else '0';

  p_verify_rx_beamlet_list : process(ext_clk)
  begin
    if rising_edge(ext_clk) then
      -- Wait until p_rx_beamlet_list is valid
      if rx_beamlet_list_val = '1' then
        -- Maintain previous x_beamlet_list for comparision
        prev_rx_beamlet_list_re <= rx_beamlet_list_re;
        prev_rx_beamlet_list_im <= rx_beamlet_list_im;
        -- After some time all rx blocks should have same beamlet values, so
        -- then rx_beamlet_list then does not change in time and the other
        -- blocks should be the same as the first block.
        if verify_rx_beamlet_list = '1' then
          assert rx_beamlet_list_re = prev_rx_beamlet_list_re
            report "Wrong: rx_beamlet_list_re differs from previous block"
            severity ERROR;
          assert rx_beamlet_list_im = prev_rx_beamlet_list_im
            report "Wrong: rx_beamlet_list_im differs from previous block"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -- get rx_beamlet for comparision with c_exp_beamlet
  rx_beamlet_x_output_re <= TO_SINT(rx_beamlet_list_re(c_exp_beamlet_x_index));
  rx_beamlet_x_output_im <= TO_SINT(rx_beamlet_list_im(c_exp_beamlet_x_index));
  rx_beamlet_y_output_re <= TO_SINT(rx_beamlet_list_re(c_exp_beamlet_y_index));
  rx_beamlet_y_output_im <= TO_SINT(rx_beamlet_list_im(c_exp_beamlet_y_index));

  -- To view the 64 bit 10GbE offload data more easily in the Wave window
  rx_beamlet_data <= rx_beamlet_sosi.data(c_longword_w - 1 downto 0);
end tb;
