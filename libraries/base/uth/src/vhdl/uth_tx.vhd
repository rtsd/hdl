--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.uth_pkg.all;

-- Purpose: Assemble an UTH frame for a data block.
-- Usage:
--   This entity has two architectures:
--
--   . rtl_delay  <== better not use this one
--   . rtl_hold   <== PREFERRED
--
--   Remarks:
--   . Both architectures verify correct with tb_uth and tb_tb_uth.
--   . In addition an architecture rtl_adapt could have been made, that would
--     use p_state and an RL 2->1 adapter similar as for uth_rx, but that has
--     not been done, because having rtl_hold is sufficient.
--
-- Description:
--
--   Pass on the block of input data:
--
--                            -----------------------
--                            | data ...            |
--                            -----------------------
--                              sop             eop
--                              val   val ...   val
--
--   into an output UTH frame:
--
--          ------------------------------------------------
--     idle | pre | sfd | tlen | data ...            | crc | idle ...
--          ------------------------------------------------
--            sop                                      eop
--            val   val   val    val ...               val
--
--   The uth_rx does the reverse.
--
-- . The input data is a block of data marked by in_sop and in_eop. If the PHY
--   supports out_val then the input block may have gaps where in_val is '0'
--   else the in_val has to remain active during the block. The minimum block
--   size is 1 data word, whereby the valid, sop and eop are all active.
-- . The uth_tx does support backpressure from the downstream sink.
-- . The uth_tx does give backpressure to the up stream source. If the up
--   stream source does not support the ready then it this can be handled by
--   placing a dp_latency_adapter in between.
-- . If the down stream sink is always ready then uth_tx can continuously
--   output UTH frames, i.e. out_sop directly after out_eop, if sufficient
--   input data blocks are provided.
-- . After the in_sop the uth_tx first outputs the preamble, the sfd (start of
--   frame delimiter) and the tlen (type or length) words.
-- . If the tlen word represents a length then its value must match the nof
--   input data words in a block, because then uth_rx will interpret it as UTH
--   payload length. Hence the tlen length must be known and fixed, it is not
--   dynamically derived from in_sop, in_val and in_eop, because that would
--   require some block store and forward buffering.
-- . Else if the tlen word represents a type, then it can have an arbitrary
--   value, because then uth_rx will know the fixed length that corresponds to
--   that UTH type. Typically the UTH type value should be larger than the
--   largest supported UTH length value, to support both on the same PHY link.
-- . The g_data_w is the width of the PHY link, so typically 16, 32 or 64 bit
--   for transceivers, 256 bit for DDR3. The user input data width must be
--   <= g_data_w, if necessary the input data words get resized.
-- . The internal CRC calculation uses the width c_crc_w. The c_crc_w is derived
--   from g_data_w by func_uth_crc_w() in uth_pkg.vhd. The CRC polynomial can
--   be 8, 16, 32 or 64 bit.
-- . The preamble word just before the sfd is also marked valid and indicated by
--   the out_sop to ensure that there is at least one preamble word between UTH
--   frames. The original intention was to send idle = preamble words between
--   UTH frames, but instead now the uth_tx puts in arbitriary idle words, with
--   out_val = '0' between UTH frames. It is allowed to use idle = preamble,
--   but not using the preamble word between frames decreases the chance on a
--   false sfd detection even more.
-- . The out_val, out_sop and out_eop signals are available to ease subsequent
--   UTH frame scheduling, but they do not have to be used on the actual PHY
--   link.
--
-- Design steps:
-- 1) First made uth_tx, because it is needed to test this uth_rx.
-- 2) ARCHITECTURE rtl_delay:
--    As a first step this uth_rx was implemented based on snk_in.valid and not
--    taking account of src_in.ready, because that is easier. However the fact
--    that this uth_tx has more output data than input data, due to the header
--    overhead makes the use of the RL 6 -> 1 latency adapter less trivial. A
--    way arround this would have been to use a state machine instead of
--    p_frame similar as the state machine in uth_rx(rtl_adapt). The state
--    machine then controls snk_out.ready to stop further input during the
--    overhead insertion and a RL 2 -> 1 latency adapter is then sufficient.
--    can have two valid data after src_in.ready went low, so this then needs
--    to be accounted for by using a RL 2 -> 1 latency adapter, similar as in
--    uth_rx(rtl_adapt).
-- 3) For testing the tb_uth.vhd is used using fixed streaming control.
-- 4) Made uth_rx to be able to test a complete UTH communication link, and
--    included random streaming control.
-- 5) ARCHITECTURE rtl_hold:
--    The RL 2 -> 1 latency adapter at the output was 'replaced' by a
--    dp_hold_input at the input. The p_state machine now maintains the RL = 1
--    by using the outputs from the dp_hold_input.
-- 6) Added duty-cycle pulse streaming control to tb_uth to be able to better
--    test uth_tx(rtl_delay).

entity uth_tx is
  generic (
    g_data_w      : natural := c_uth_data_max_w;
    g_nof_ch      : natural := 1;  -- g_nof_ch, the channels are numbered from 0 TO g_nof_ch-1, each channel represents a type of UTH frame
    g_typ_arr     : t_natural_arr := array_init(0, 1);  -- g_nof_ch number of TLEN type values indexed by sink input channel, use unconstraint type to allow generic g_nof_ch
    g_out_rl      : natural := 6  -- maximum output ready latency, not used by rtl_hold, only applies to rtl_delay
  );
  port (
    rst       : in  std_logic;
    clk       : in  std_logic;
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end uth_tx;

architecture rtl_delay of uth_tx is
  constant c_idle        : std_logic_vector := RESIZE_DP_DATA(c_uth_idle);
  constant c_preamble    : std_logic_vector := RESIZE_DP_DATA(c_uth_preamble);
  constant c_sfd         : std_logic_vector := RESIZE_DP_DATA(c_uth_sfd);

  constant c_channel_w   : natural := ceil_log2(g_nof_ch);  -- could use true_log2, but instead use extra check on g_nof_ch=1 to force channel = 0 then, to avoid null-range (-1 DOWNTO 0) warnings.

  -- Map input t_natural_arr range to range 0 TO g_nof_ch-1
  constant c_typ_arr     : t_natural_arr(0 to g_nof_ch - 1) := g_typ_arr;

  constant c_latency     : natural := 1;  -- Extra latency due to that the p_frame outputs when valid='1', ignoring out_ready

  -- This 'rtl_delay' does not adhere to out_ready. Assume that the upstream
  -- device does adhere to RL = 1, then this 'rtl_delay' causes the output RL
  -- to range between 2 and 6, because:
  -- . the upstream RL = 1 so when the sop arrives this accounts for 1 cycle
  -- . the UTH packet overhead of insterting preamble, sfd, tlen and crc accounts for 4 cycles
  -- . the p_frame control ignores out_ready and outputs when valid='1', this accounts for 1 cycle
  -- Therefore define c_in_rl = 1 and c_out_rl_max = 6.
  -- If the downstream device is a FIFO then the almost full level can be
  -- decreased by c_out_rl_max - c_in_rl = 5 to compensate for the increase in
  -- output RL by this 'rtl_delay'. Use g_out_rl = c_out_rl_max to directly
  -- wire to the output without adapting the RL.
  -- If the downstream device does require the output RL to be 1 then use
  -- g_out_rl = 1 which will then instantiate a dp_latency_adapter.
  constant c_in_rl       : natural := 1;
  constant c_out_rl_max  : natural := c_in_rl + c_uth_nof_overhead + c_latency;

  constant c_crc_w       : natural := func_uth_crc_w(g_data_w);

  signal crc             : std_logic_vector(c_crc_w - 1 downto 0);
  signal nxt_crc         : std_logic_vector(crc'range);

  -- Delayed inputs to create space to prepend preamble, sfd and tlen word and to append crc word
  signal snk_in_dly     : t_dp_sosi_arr(0 to c_uth_nof_overhead);

  signal tx_tlen_index   : natural range 0 to g_nof_ch;
  signal tx_tlen         : std_logic_vector(g_data_w - 1 downto 0);
  signal data_busy       : std_logic;
  signal nxt_data_busy   : std_logic;

  signal adapt_siso      : t_dp_siso;
  signal frm_siso        : t_dp_siso;
  signal frm_sosi        : t_dp_sosi;
  signal nxt_frm_sosi    : t_dp_sosi;
  signal adapt_sosi      : t_dp_sosi;

  -- declare data field slv signals to ease debugging in wave window
  signal frm_data        : std_logic_vector(g_data_w - 1 downto 0);
  signal frm_val         : std_logic;
  signal frm_sop         : std_logic;
  signal frm_eop         : std_logic;
begin
  snk_in_dly(0) <= snk_in;  -- put combinatorial at index [0]

  src_out <= adapt_sosi;

  -- Map to slv to ease monitoring in wave window
  frm_data <= frm_sosi.data(g_data_w - 1 downto 0);
  frm_val  <= frm_sosi.valid;
  frm_sop  <= frm_sosi.sop;
  frm_eop  <= frm_sosi.eop;

  -- Determine tlen
  tx_tlen_index <= TO_UINT(snk_in_dly(2).channel(c_channel_w - 1 downto 0)) when g_nof_ch > 1 else 0;
  tx_tlen <= TO_UVEC(c_typ_arr(tx_tlen_index), g_data_w);

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      snk_in_dly(1 to c_uth_nof_overhead) <= (others => c_dp_sosi_rst);
      crc         <= (others => '1');
      frm_sosi    <= c_dp_sosi_rst;
      data_busy   <= '0';
    elsif rising_edge(clk) then
      snk_in_dly(1 to c_uth_nof_overhead) <= snk_in_dly(0 to c_uth_nof_overhead - 1);
      crc         <= nxt_crc;
      frm_sosi    <= nxt_frm_sosi;
      data_busy   <= nxt_data_busy;
    end if;
  end process;

  p_frame : process(crc, snk_in_dly, tx_tlen, data_busy)
  begin
    -- set default nxt_crc
    nxt_crc <= crc;
    if snk_in_dly(3).valid = '1' then
      nxt_crc <= func_uth_next_crc(snk_in_dly(3).data(g_data_w - 1 downto 0), crc);
    end if;

    -- set default nxt_out_dat
    nxt_frm_sosi.data  <= snk_in_dly(3).data;  -- pass on input data
    if data_busy = '0' then
      nxt_frm_sosi.data  <= c_idle;  -- force idle data output between frames
    end if;
    nxt_frm_sosi.valid <= snk_in_dly(3).valid;
    nxt_frm_sosi.sop   <= '0';
    nxt_frm_sosi.eop   <= '0';

    -- use data_busy to know when a payload is being transmitted or not
    nxt_data_busy <= data_busy;

    -- overrule default nxt_crc, nxt_out_dat
    if snk_in_dly(0).sop = '1' then
      nxt_crc            <= (others => '1');  -- init crc
      nxt_frm_sosi.data  <= c_preamble;  -- prepend preamble word
      nxt_frm_sosi.valid <= '1';
      nxt_frm_sosi.sop   <= '1';  -- output sop
    elsif snk_in_dly(1).sop = '1' then
      nxt_crc            <= (others => '1');  -- init crc
      nxt_frm_sosi.data  <= c_sfd;  -- prepend sfd word
      nxt_frm_sosi.valid <= '1';
    elsif snk_in_dly(2).sop = '1' then
      nxt_crc            <= (others => '1');  -- init crc
      nxt_frm_sosi.data  <= RESIZE_DP_DATA(tx_tlen);  -- prepend tlen word
      nxt_frm_sosi.valid <= '1';
      nxt_data_busy      <= '1';  -- next snk_in_dly(3).valid marks first data
    elsif snk_in_dly(3).eop = '1' then
      nxt_data_busy      <= '0';  -- this snk_in_dly(3).valid marks last data
    elsif snk_in_dly(4).eop = '1' then
      nxt_frm_sosi.data  <= RESIZE_DP_DATA(crc);  -- append calculated tx crc
      nxt_frm_sosi.valid <= '1';
      nxt_frm_sosi.eop   <= '1';  -- output eop
    end if;
  end process;

  snk_out.xon   <= src_in.xon;
  snk_out.ready <= frm_siso.ready and adapt_siso.ready;

  -- The for each UTH packet c_uth_nof_overhead=4 extra words have to be output. The RL adapter buffer can only cover this for
  -- one UTH packet. Therefore the frm_siso.ready has to go low immediatly (i.e. g_and_low=TRUE) when the snk_in.eop is there,
  -- to avoid that p_state already receive a sop for next frame. The frm_siso.ready can be released again when the eop leaves
  -- the RL adapter buffer (i.e. g_or_high=TRUE), because then it is free to buffer for the next UTH packet.
  u_frm_ready : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',
    g_priority_lo  => false,
    g_or_high      => true,  -- combinatorially force out_level high
    g_and_low      => true  -- combinatorially force out_level low
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => adapt_sosi.eop,
    switch_low  => snk_in_dly(0).eop,
    out_level   => frm_siso.ready
  );

  u_out_adapt : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency   => c_out_rl_max,
    g_out_latency  => g_out_rl
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => adapt_siso,
    snk_in       => frm_sosi,
    -- ST source
    src_in       => src_in,
    src_out      => adapt_sosi
  );
end rtl_delay;

architecture rtl_hold of uth_tx is
  constant c_idle        : std_logic_vector := RESIZE_DP_DATA(c_uth_idle);
  constant c_preamble    : std_logic_vector := RESIZE_DP_DATA(c_uth_preamble);
  constant c_sfd         : std_logic_vector := RESIZE_DP_DATA(c_uth_sfd);

  constant c_channel_w   : natural := ceil_log2(g_nof_ch);  -- could use true_log2, but instead use extra check on g_nof_ch=1 to force channel = 0 then, to avoid null-range (-1 DOWNTO 0) warnings.

  -- Map input t_natural_arr range to range 0 TO g_nof_ch-1
  constant c_typ_arr     : t_natural_arr(0 to g_nof_ch - 1) := g_typ_arr;

  constant c_crc_w       : natural := func_uth_crc_w(g_data_w);

  type t_state is (s_idle, s_sfd, s_tlen, s_data, s_crc);

  signal state           : t_state;
  signal nxt_state       : t_state;

  signal crc_init        : std_logic;
  signal crc             : std_logic_vector(c_crc_w - 1 downto 0);
  signal nxt_crc         : std_logic_vector(crc'range);

  signal tx_tlen_index   : natural range 0 to g_nof_ch;
  signal tx_tlen         : std_logic_vector(g_data_w - 1 downto 0);

  signal hold_src_in     : t_dp_siso;
  signal src_buf         : t_dp_sosi;
  signal nxt_src_buf     : t_dp_sosi;
  signal next_src_buf    : t_dp_sosi;
  signal pend_src_buf    : t_dp_sosi;

  signal i_src_out       : t_dp_sosi;
  signal nxt_src_out     : t_dp_sosi;
begin
  src_out <= i_src_out;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      crc            <= (others => '1');
      src_buf        <= c_dp_sosi_rst;
      i_src_out      <= c_dp_sosi_rst;  -- default output zero data after reset
      state          <= s_idle;
    elsif rising_edge(clk) then
      crc            <= nxt_crc;
      src_buf        <= nxt_src_buf;
      i_src_out      <= nxt_src_out;
      state          <= nxt_state;
    end if;
  end process;

  -- Hold input register
  nxt_src_buf <= next_src_buf;

  u_hold : entity dp_lib.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => snk_in,
    -- ST source
    src_in       => hold_src_in,
    next_src_out => next_src_buf,
    pend_src_out => pend_src_buf,
    src_out_reg  => src_buf
  );

  -- Determine tlen
  tx_tlen_index <= TO_UINT(pend_src_buf.channel(c_channel_w - 1 downto 0)) when g_nof_ch > 1 else 0;
  tx_tlen       <= TO_UVEC(c_typ_arr(tx_tlen_index), g_data_w);

  -- Calculate CRC
  p_crc : process(crc, crc_init, next_src_buf)
  begin
    nxt_crc <= crc;
    if crc_init = '1' then
      nxt_crc <= (others => '1');  -- init crc
    elsif next_src_buf.valid = '1' then
      nxt_crc <= func_uth_next_crc(next_src_buf.data(g_data_w - 1 downto 0), crc);
    end if;
  end process;

  -- State machine
  p_state : process(state, src_in, pend_src_buf, i_src_out, tx_tlen, crc)
  begin
    nxt_state         <= state;
    crc_init          <= '0';

    nxt_src_out       <= i_src_out;
    nxt_src_out.valid <= '0';
    nxt_src_out.sop   <= '0';
    nxt_src_out.eop   <= '0';

    -- default flow control
    snk_out     <= src_in;
    hold_src_in <= src_in;

    case state is
      when s_idle =>
        crc_init <= '1';
        snk_out.ready     <= not pend_src_buf.sop;  -- flush until sop
        hold_src_in.ready <= '0';
        nxt_src_out.data  <= c_idle;  -- default force idle data output between frames
        if src_in.ready = '1' then
          if pend_src_buf.sop = '1' then
            nxt_src_out.data  <= c_preamble;
            nxt_src_out.valid <= '1';
            nxt_src_out.sop   <= '1';
            nxt_state <= s_sfd;
          end if;
        end if;
      when s_sfd =>
        crc_init <= '1';
        snk_out.ready     <= '0';
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then
          nxt_src_out.data  <= c_sfd;
          nxt_src_out.valid <= '1';
          nxt_state <= s_tlen;
        end if;
      when s_tlen =>
        crc_init <= '1';
        snk_out.ready     <= '0';
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then
          nxt_src_out.data  <= RESIZE_DP_DATA(tx_tlen);
          nxt_src_out.valid <= '1';
          nxt_state <= s_data;
        end if;
      when s_data =>
        if src_in.ready = '1' then
          nxt_src_out.data  <= pend_src_buf.data;
          nxt_src_out.valid <= pend_src_buf.valid;
          if pend_src_buf.eop = '1' then
            nxt_state <= s_crc;
          end if;
        end if;
      when others =>  -- s_crc
        snk_out.ready     <= not pend_src_buf.sop;  -- flush until next sop
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then
          nxt_src_out.data  <= RESIZE_DP_DATA(crc);
          nxt_src_out.valid <= '1';
          nxt_src_out.eop   <= '1';
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
end rtl_hold;
