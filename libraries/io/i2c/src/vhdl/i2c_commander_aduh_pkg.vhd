-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: I2C commander settings for the ADU Handler

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;
use work.i2c_dev_adu_pkg.all;

package i2c_commander_aduh_pkg is
  ------------------------------------------------------------------------------
  -- Local constants (with prefix 'k_')
  ------------------------------------------------------------------------------

  -- Define the protocol lists for the commander (c_i2c_cmdr_max_nof_protocols=16)
  constant k_protocol_list_0      : t_nat_natural_arr := c_i2c_adu_max1617_protocol_list_read_temp;
  constant k_protocol_list_1      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_adc;
  constant k_protocol_list_2      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_adc_tm;
  constant k_protocol_list_3      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_atten_0_0db;
  constant k_protocol_list_4      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_atten_1_0db;
  constant k_protocol_list_5      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_atten_10_0db;
  constant k_protocol_list_6      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_atten_10_6db;
  constant k_protocol_list_7      : t_nat_natural_arr := c_i2c_adu_pca9555_protocol_list_set_cal;
  constant k_protocol_list_8      : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;  -- use SMBUS_C_END for not used protocol lists
  constant k_protocol_list_9      : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_10     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_11     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_12     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_13     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_14     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;
  constant k_protocol_list_15     : t_nat_natural_arr := c_i2c_cmdr_protocol_list_sample_sda;

  constant k_protocol_ram_init : t_nat_natural_arr := k_protocol_list_0  &
                                                      k_protocol_list_1  &
                                                      k_protocol_list_2  &
                                                      k_protocol_list_3  &
                                                      k_protocol_list_4  &
                                                      k_protocol_list_5  &
                                                      k_protocol_list_6  &
                                                      k_protocol_list_7  &
                                                      k_protocol_list_8  &
                                                      k_protocol_list_9  &
                                                      k_protocol_list_10 &
                                                      k_protocol_list_11 &
                                                      k_protocol_list_12 &
                                                      k_protocol_list_13 &
                                                      k_protocol_list_14 &
                                                      k_protocol_list_15;

  -- Define the corresponding mask words for the result data
  constant k_expected_mask_0      : std_logic_vector := c_i2c_adu_max1617_expected_mask_read_temp;
  constant k_expected_mask_1      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_adc;
  constant k_expected_mask_2      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_adc_tm;
  constant k_expected_mask_3      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_atten_0_0dB;
  constant k_expected_mask_4      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_atten_1_0dB;
  constant k_expected_mask_5      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_atten_10_0dB;
  constant k_expected_mask_6      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_atten_10_6dB;
  constant k_expected_mask_7      : std_logic_vector := c_i2c_adu_pca9555_expected_mask_set_cal;
  constant k_expected_mask_8      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_9      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_10     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_11     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_12     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_13     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_14     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_15     : std_logic_vector := c_i2c_cmdr_expected_mask_sample_sda;

  constant k_expected_mask_arr : t_slv_32_arr(0 to c_i2c_cmdr_max_nof_protocols - 1) := (k_expected_mask_0,
                                                                                       k_expected_mask_1,
                                                                                       k_expected_mask_2,
                                                                                       k_expected_mask_3,
                                                                                       k_expected_mask_4,
                                                                                       k_expected_mask_5,
                                                                                       k_expected_mask_6,
                                                                                       k_expected_mask_7,
                                                                                       k_expected_mask_8,
                                                                                       k_expected_mask_9,
                                                                                       k_expected_mask_10,
                                                                                       k_expected_mask_11,
                                                                                       k_expected_mask_12,
                                                                                       k_expected_mask_13,
                                                                                       k_expected_mask_14,
                                                                                       k_expected_mask_15);

  -- Define the corresponding expected nof read data
  constant k_nof_result_data_0    : natural := c_i2c_adu_max1617_nof_result_data_read_temp;
  constant k_nof_result_data_1    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_2    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_3    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_4    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_5    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_6    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_7    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_8    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_9    : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_10   : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_11   : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_12   : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_13   : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_14   : natural := c_i2c_cmdr_nof_result_data_none;
  constant k_nof_result_data_15   : natural := c_i2c_cmdr_nof_result_data_none;

  constant k_nof_result_data_arr : t_nat_natural_arr := (k_nof_result_data_0,
                                                         k_nof_result_data_1,
                                                         k_nof_result_data_2,
                                                         k_nof_result_data_3,
                                                         k_nof_result_data_4,
                                                         k_nof_result_data_5,
                                                         k_nof_result_data_6,
                                                         k_nof_result_data_7,
                                                         k_nof_result_data_8,
                                                         k_nof_result_data_9,
                                                         k_nof_result_data_10,
                                                         k_nof_result_data_11,
                                                         k_nof_result_data_12,
                                                         k_nof_result_data_13,
                                                         k_nof_result_data_14,
                                                         k_nof_result_data_15);

  -- Define the corresponding protocol list offsets
  constant k_protocol_ofs_0  : natural := 0;
  constant k_protocol_ofs_1  : natural := k_protocol_list_0'length  + k_protocol_ofs_0;
  constant k_protocol_ofs_2  : natural := k_protocol_list_1'length  + k_protocol_ofs_1;
  constant k_protocol_ofs_3  : natural := k_protocol_list_2'length  + k_protocol_ofs_2;
  constant k_protocol_ofs_4  : natural := k_protocol_list_3'length  + k_protocol_ofs_3;
  constant k_protocol_ofs_5  : natural := k_protocol_list_4'length  + k_protocol_ofs_4;
  constant k_protocol_ofs_6  : natural := k_protocol_list_5'length  + k_protocol_ofs_5;
  constant k_protocol_ofs_7  : natural := k_protocol_list_6'length  + k_protocol_ofs_6;
  constant k_protocol_ofs_8  : natural := k_protocol_list_7'length  + k_protocol_ofs_7;
  constant k_protocol_ofs_9  : natural := k_protocol_list_8'length  + k_protocol_ofs_8;
  constant k_protocol_ofs_10 : natural := k_protocol_list_9'length  + k_protocol_ofs_9;
  constant k_protocol_ofs_11 : natural := k_protocol_list_10'length + k_protocol_ofs_10;
  constant k_protocol_ofs_12 : natural := k_protocol_list_11'length + k_protocol_ofs_11;
  constant k_protocol_ofs_13 : natural := k_protocol_list_12'length + k_protocol_ofs_12;
  constant k_protocol_ofs_14 : natural := k_protocol_list_13'length + k_protocol_ofs_13;
  constant k_protocol_ofs_15 : natural := k_protocol_list_14'length + k_protocol_ofs_14;

  constant k_protocol_ofs_arr : t_natural_arr(0 to c_i2c_cmdr_max_nof_protocols - 1) := (k_protocol_ofs_0,
                                                                                       k_protocol_ofs_1,
                                                                                       k_protocol_ofs_2,
                                                                                       k_protocol_ofs_3,
                                                                                       k_protocol_ofs_4,
                                                                                       k_protocol_ofs_5,
                                                                                       k_protocol_ofs_6,
                                                                                       k_protocol_ofs_7,
                                                                                       k_protocol_ofs_8,
                                                                                       k_protocol_ofs_9,
                                                                                       k_protocol_ofs_10,
                                                                                       k_protocol_ofs_11,
                                                                                       k_protocol_ofs_12,
                                                                                       k_protocol_ofs_13,
                                                                                       k_protocol_ofs_14,
                                                                                       k_protocol_ofs_15);

  -- RAM sizes
  constant k_protocol_ram_nof_dat : natural := ceil_div(k_protocol_ram_init'LENGTH, c_i2c_cmdr_mem_block_sz) * c_i2c_cmdr_mem_block_sz;
  constant k_protocol_ram_adr_w   : natural := ceil_log2(k_protocol_ram_nof_dat);
  constant k_result_adr_w         : natural := k_protocol_ram_adr_w - 1;  -- assume < 1 result byte per 2 protocol bytes
  constant k_result_nof_dat       : natural := 2**k_result_adr_w;

  -- Commander settings
  constant k_nof_protocols        : natural := 16;  -- Must be >= actually used nof protocol lists in k_protocol_ram_init and <= c_i2c_cmdr_max_nof_protocols=16 protocols.
  constant k_nof_result_data_max  : natural := 2;  -- Must be >= the maximum nof read bytes in any protocol list in k_protocol_ram_init, choose 2 to also suit I2C read word
  constant k_result_cnt_w         : natural := k_result_adr_w;

  ------------------------------------------------------------------------------
  -- Global constants (with prefix 'c_i2c_cmdr_aduh_')
  ------------------------------------------------------------------------------

  constant c_i2c_cmdr_aduh_protocol_ram_init   : t_nat_natural_arr := k_protocol_ram_init;
  constant c_i2c_cmdr_aduh_nof_result_data_arr : t_nat_natural_arr := k_nof_result_data_arr;

  constant c_i2c_cmdr_aduh_protocol_commander  : t_c_i2c_cmdr_commander := (k_nof_protocols,
                                                                            k_protocol_ofs_arr,
                                                                            k_expected_mask_arr,
                                                                            k_result_cnt_w,
                                                                            k_nof_result_data_max);

  constant k_commander_nof_dat                 : natural := func_i2c_cmdr_mm_reg_nof_dat(c_i2c_cmdr_aduh_protocol_commander);
  constant k_commander_adr_w                   : natural := ceil_log2(k_commander_nof_dat);

  constant c_i2c_cmdr_aduh_i2c_mm              : t_c_i2c_mm := (k_commander_adr_w,  -- use control_adr_w field to pass on k_commander_adr_w = ceil_log2(52) = 6
                                                                k_protocol_ram_adr_w,
                                                                k_protocol_ram_nof_dat,
                                                                k_result_adr_w,
                                                                k_result_nof_dat);

  -- use full memory to avoid "Warning: Address pointed at port A is out of bound!" due to ram_crw_crw MegaWizard model in simulation
  constant c_i2c_cmdr_aduh_i2c_mm_sim          : t_c_i2c_mm := (k_commander_adr_w,  -- use control_adr_w field to pass on k_commander_adr_w = ceil_log2(52) = 6
                                                                k_protocol_ram_adr_w,
                                                                2**k_protocol_ram_adr_w,
                                                                k_result_adr_w,
                                                                2**k_result_adr_w);

-- MM register map:
--
-- . c_i2c_cmdr_aduh_protocol_commander.nof_protocols         = 16
-- . c_i2c_cmdr_aduh_protocol_commander.nof_result_data_max   = 2
-- . c_i2c_cmdr_aduh_i2c_mm.protocol_adr_w                    = 13
-- . c_i2c_cmdr_aduh_i2c_mm.protocol_nof_dat                  = 5120 hw, 8192 sim
-- . c_i2c_cmdr_aduh_i2c_mm.result_adr_w                      = 12
-- . c_i2c_cmdr_aduh_i2c_mm.result_nof_dat                    = 4096
-- . protocol_status_w = ceil_log2(c_i2c_cmdr_state_max)      = 2
-- . result_error_cnt_w = result_adr_w                        = 12
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |         xxx                             write access issues protocol 0|  0
--  |-----------------------------------------------------------------------|
--  |         xxx                             write access issues protocol 1|  1
--  |-----------------------------------------------------------------------|
--  |         xxx                             write access issues protocol 2|  2
--  |-----------------------------------------------------------------------|
--  |                                  ...                                  | ..
--  |-----------------------------------------------------------------------|
--  |         xxx                            write access issues protocol 15| 15
--  |-----------------------------------------------------------------------|
--  |         xxx                      protocol_offset[protocol_adr_w-1:0] 0| 16
--  |-----------------------------------------------------------------------|
--  |         xxx                      protocol_offset[protocol_adr_w-1:0] 1| 17
--  |-----------------------------------------------------------------------|
--  |         xxx                      protocol_offset[protocol_adr_w-1:0] 2| 18
--  |-----------------------------------------------------------------------|
--  |                                  ...                                  | ..
--  |-----------------------------------------------------------------------|
--  |         xxx                     protocol_offset[protocol_adr_w-1:0] 15| 31
--  |-----------------------------------------------------------------------|
--  |         xxx                                    result_expected[31:0] 0| 32
--  |-----------------------------------------------------------------------|
--  |         xxx                                    result_expected[31:0] 1| 33
--  |-----------------------------------------------------------------------|
--  |                                  ...                                  | ..
--  |-----------------------------------------------------------------------|
--  |         xxx                                   result_expected[31:0] 15| 47
--  |-----------------------------------------------------------------------|
--  |         xxx                                      protocol_status[31:0]| 48
--  |-----------------------------------------------------------------------|
--  |         xxx                                     result_error_cnt[31:0]| 49
--  |-----------------------------------------------------------------------|
--  |         xxx                                         result_data[7:0] 0| 50
--  |-----------------------------------------------------------------------|
--  |         xxx                                         result_data[7:0] 0| 51
--  |-----------------------------------------------------------------------|
--
--  k_commander_nof_dat =
--      func_i2c_cmdr_mm_reg_nof_dat(c_i2c_cmdr_aduh_protocol_commander) -->
--      3*nof_protocols + 1 + 1 + nof_result_data_max = 3*16 + 1 + 1 + 2 = 52
--  k_commander_adr_w = ceil_log2(k_commander_nof_dat) = ceil_log2(52) = 6
end i2c_commander_aduh_pkg;

package body i2c_commander_aduh_pkg is
end i2c_commander_aduh_pkg;
