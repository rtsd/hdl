-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.util_heater_pkg.all;

entity tb_util_heater is
end tb_util_heater;

architecture tb of tb_util_heater is
  constant mm_clk_period     : time := 8 ns;
  constant st_clk_period     : time := 5 ns;

  constant c_word_heater_on  : integer := -1;  -- 16#FFFFFFFF#;
  constant c_word_heater_off : integer := 0;

  constant c_time_heater_on  : natural := 100;
  constant c_time_heater_off : natural := 20;

  constant c_nof_mac4        : natural := 1;
  constant c_pipeline        : natural := 1;
  constant c_nof_ram         : natural := 2;
  constant c_nof_logic       : natural := 3;

  constant c_reg_nof_words   : natural := 2;  -- derived from c_nof_mac4 / c_word_w

  signal tb_end        : std_logic := '0';
  signal mm_rst        : std_logic;
  signal mm_clk        : std_logic := '0';
  signal st_rst        : std_logic;
  signal st_clk        : std_logic := '0';

  signal mm_mosi       : t_mem_mosi;
  signal mm_miso       : t_mem_miso;
begin
  -- as 10
  -- run -all

  mm_clk <= not mm_clk or tb_end after mm_clk_period / 2;
  st_clk <= not st_clk or tb_end after st_clk_period / 2;

  mm_rst <= '1', '0' after mm_clk_period * 5;
  st_rst <= '1', '0' after st_clk_period * 5;

  p_mm_stimuli : process
  begin
    mm_miso <= c_mem_miso_rst;
    mm_mosi <= c_mem_mosi_rst;

    -- Use while instead of WAIT UNTIL mm_rst='0' because wait until requires a change
    while mm_rst = '1' loop
      wait until rising_edge(mm_clk);
    end loop;

    for I in 0 to 9 loop
      wait until rising_edge(mm_clk);
    end loop;

    for I in 0 to c_reg_nof_words - 1 loop
      proc_mem_mm_bus_wr(I, c_word_heater_on, mm_clk, mm_miso, mm_mosi);  -- Enable multiplier 31 : 0
    end loop;

    for I in 0 to c_time_heater_on - 1 loop
      wait until rising_edge(mm_clk);
    end loop;

    for I in 0 to c_reg_nof_words - 1 loop
      proc_mem_mm_bus_wr(I, c_word_heater_off, mm_clk, mm_miso, mm_mosi);  -- Disable multiplier 31 : 0
    end loop;

    for I in 0 to c_time_heater_off - 1 loop
      wait until rising_edge(mm_clk);
    end loop;
    tb_end <= '1';
    wait;
  end process;

  dut : entity work.util_heater
  generic map (
    g_nof_mac4   => c_nof_mac4,
    g_pipeline   => c_pipeline,
    g_nof_ram    => c_nof_ram,
    g_nof_logic  => c_nof_logic
  )
  port map (
    mm_rst  => mm_rst,  -- MM is the microprocessor control clock domain
    mm_clk  => mm_clk,
    st_rst  => st_rst,  -- ST is the DSP clock domain
    st_clk  => st_clk,
    -- Memory Mapped Slave
    sla_in  => mm_mosi,
    sla_out => mm_miso
  );
end tb;
