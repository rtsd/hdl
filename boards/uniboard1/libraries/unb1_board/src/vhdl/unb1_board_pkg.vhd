-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

package unb1_board_pkg is
  -- UniBoard
  constant c_unb1_board_nof_node             : natural := 4;  -- nof FN or nof BN on UniBoard
  constant c_unb1_board_nof_node_w           : natural := 2;  -- = ceil_log2(c_unb1_board_nof_node)
  constant c_unb1_board_nof_fn               : natural := c_unb1_board_nof_node;  -- nof Front Node FPGAs on UniBoard
  constant c_unb1_board_nof_bn               : natural := c_unb1_board_nof_node;  -- nof Back Node FPGAs on UniBoard
  constant c_unb1_board_nof_chip             : natural := c_unb1_board_nof_fn + c_unb1_board_nof_bn;  -- = 8
  constant c_unb1_board_nof_chip_w           : natural := 3;  -- = ceil_log2(c_unb1_board_nof_chip)
  constant c_unb1_board_nof_ddr3             : natural := 2;  -- each node has 2 DDR3 modules

  -- Subrack
  constant c_unb1_board_nof_uniboard         : natural := 4;  -- nof UniBoard in a subrack
  constant c_unb1_board_nof_uniboard_w       : natural := 5;  -- Only 2 required for 4 boards; full width is 5.

  -- Clock frequencies
  constant c_unb1_board_ext_clk_freq_200M    : natural := 200 * 10**6;  -- external clock, SMA clock
  constant c_unb1_board_eth_clk_freq         : natural :=  25 * 10**6;  -- fixed 25 MHz ETH XO clock used as reference clock for the PLL in SOPC
  constant c_unb1_board_tse_clk_freq         : natural := 125 * 10**6;  -- fixed 125 MHz TSE reference clock derived from ETH_clk by PLL in SOPC
  constant c_unb1_board_cal_clk_freq         : natural :=  40 * 10**6;  -- fixed 40 MHz IO calibration clock derived from ETH_clk by PLL in SOPC
  constant c_unb1_board_mm_clk_freq_50M      : natural :=  50 * 10**6;  -- SOPC memory mapped bus clock derived from ETH_clk by PLL in SOPC
  constant c_unb1_board_mm_clk_freq_100M     : natural := 100 * 10**6;  -- SOPC memory mapped bus clock derived from ETH_clk by PLL in SOPC
  constant c_unb1_board_mm_clk_freq_125M     : natural := 125 * 10**6;  -- SOPC memory mapped bus clock derived from ETH_clk by PLL in SOPC

  -- PHY interface device numbers
  constant c_unb1_board_phy_id_eth1g         : natural := 1;
  constant c_unb1_board_phy_id_eth10g        : natural := 2;
  constant c_unb1_board_phy_id_tr_mesh       : natural := 3;
  constant c_unb1_board_phy_id_tr_back       : natural := 4;
  constant c_unb1_board_phy_id_ddr3_I        : natural := 5;
  constant c_unb1_board_phy_id_ddr3_II       : natural := 6;
  constant c_unb1_board_phy_id_adc           : natural := 7;

  -- PHY interface error numbers
  constant c_unb1_board_ok                   : natural := 0;  -- value
  constant c_unb1_board_error                : integer := 1;  -- value
  constant c_unb1_board_error_eth1g_bi       : natural := 1;  -- = c_unb1_board_phy_id_eth1g;    -- bit number index
  constant c_unb1_board_error_eth10g_bi      : natural := 2;  -- = c_unb1_board_phy_id_eth10g;
  constant c_unb1_board_error_tr_mesh_bi     : natural := 3;  -- = c_unb1_board_phy_id_tr_mesh;
  constant c_unb1_board_error_tr_back_bi     : natural := 4;  -- = c_unb1_board_phy_id_tr_back;
  constant c_unb1_board_error_ddr3_I_bi      : natural := 5;  -- = c_unb1_board_phy_id_ddr3_I;
  constant c_unb1_board_error_ddr3_II_bi     : natural := 6;  -- = c_unb1_board_phy_id_ddr3_II;
  constant c_unb1_board_error_adc_bi         : natural := 7;  -- = c_unb1_board_phy_id_adc;
  constant c_unb1_board_error_w              : natural := 8;

  -- I2C
  constant c_unb1_board_reg_sens_adr_w       : natural := 3;  -- must match ceil_log2(c_mm_nof_dat) in unb1_board_sens_reg.vhd

  -- 10G
  constant c_unb1_board_nof_mdio             : natural := 3;  -- Number of MD interfaces

  -- CONSTANT RECORD DECLARATIONS ---------------------------------------------

  -- PHY interface instantiation control
  type t_c_unb1_board_use_phy is record
    eth1g    : natural;  -- used to set g_udp_offload for streaming DP offload (or onload), the 1GbE for MM control on hardware is always enabled in ctrl_unb1_board
    tr_front : natural;
    tr_mesh  : natural;
    tr_back  : natural;
    ddr3_I   : natural;
    ddr3_II  : natural;
    adc      : natural;
    wdi      : natural;  -- enable WDI when 1, else disable by leaving unconnected so 'Z'
  end record;

  -- Transceivers
  type t_c_unb1_board_tr is record
    nof_bus                           : natural;  -- = 4;   --    FN-BN, BN-BI
    bus_w                             : natural;  -- = 4;   --    FN-BN, BN-BI
    cntrl_w                           : natural;  -- = 3;   -- SI-FN
    cntrl_lasi_id                     : natural;  -- = 0;   -- SI-FN
    cntrl_mdc_id                      : natural;  -- = 1;   -- SI-FN
    cntrl_mdio_id                     : natural;  -- = 2;   -- SI-FN
  end record;

  constant c_unb1_board_tr                   : t_c_unb1_board_tr := ( 4, 4, 3, 0, 1, 2);  -- UniBoard hardware has 4 bundles of 4 transceivers or 16 individual transceivers (including CMU transceivers)
  constant c_unb1_board_tr_mesh              : t_c_unb1_board_tr := ( 4, 3, 3, 0, 1, 2);  -- the FN-BN mesh   uses 4 bundles of 3 transceivers or 12 individual transceivers (not using the CMU transceivers)
  constant c_unb1_board_tr_back              : t_c_unb1_board_tr := ( 3, 4, 3, 0, 1, 2);  -- the BN-BI back   uses 3 bundles of 4 transceivers or 12 individual transceivers (not using the CMU transceivers)
  constant c_unb1_board_tr_xaui              : t_c_unb1_board_tr := ( 3, 4, 3, 0, 1, 2);  -- FN-XAUI          use  3 bundles of 4 transceivers (the CMU transceivers are not HW connected) and XAUI PHY control for 10GbE

  -- Transceivers network array types for the UniBoard mesh HW and for a backplane HW
  constant c_unb1_board_tr_mesh_hw_nof_bus   : natural := c_unb1_board_nof_node;  -- fixed 4 FN and 4 BN on one UniBoard
  constant c_unb1_board_tr_mesh_hw_bus_w     : natural := 4;  -- effectively 4-1 = 3, because the CMU tranceivers are connected on the mesh, but typically not used
  constant c_unb1_board_tr_back_hw_nof_bus   : natural := c_unb1_board_nof_uniboard;  -- assume 4 UniBoard in a subrack (could be more), so connect to 4 - 1= 3 other UniBoards in subrack. Also allow complete range 3..0 to be used for logical indexing (including the hosting board itself).
  constant c_unb1_board_tr_back_hw_bus_w     : natural := 4;  -- the CMU tranceivers are not connected on the backplane
  constant c_unb1_board_tr_xaui_hw_bus_w     : natural := 4;

  -- TR mesh node IO for 1 to 4 nodes
  type t_unb1_board_mesh_sosi_2arr is array (c_unb1_board_tr_mesh_hw_nof_bus - 1 downto 0) of t_dp_sosi_arr(   c_unb1_board_tr_mesh_hw_bus_w - 1 downto 0);  -- indexing (node id 3,2,1,0)(transceiver lane 3,2,1,0)
  type t_unb1_board_mesh_siso_2arr is array (c_unb1_board_tr_mesh_hw_nof_bus - 1 downto 0) of t_dp_siso_arr(   c_unb1_board_tr_mesh_hw_bus_w - 1 downto 0);  -- indexing (node id 3,2,1,0)(transceiver lane 3,2,1,0)
  type t_unb1_board_mesh_sl_2arr   is array (c_unb1_board_tr_mesh_hw_nof_bus - 1 downto 0) of std_logic_vector(c_unb1_board_tr_mesh_hw_bus_w - 1 downto 0);  -- indexing (node id 3,2,1,0)(transceiver lane 3,2,1,0)

  constant c_unb1_board_mesh_sosi_2arr_rst   : t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
  constant c_unb1_board_mesh_siso_2arr_rst   : t_unb1_board_mesh_siso_2arr := (others => (others => c_dp_siso_rst));
  constant c_unb1_board_mesh_sl_2arr_rst     : t_unb1_board_mesh_sl_2arr   := (others => (others => '0'));

  -- TR backplane node IO for 1 BN to 3 BN each on the 3 other UniBoards
  type t_unb1_board_back_sosi_2arr is array (c_unb1_board_tr_back_hw_nof_bus - 1 downto 0) of t_dp_sosi_arr(   c_unb1_board_tr_back_hw_bus_w - 1 downto 0);  -- indexing (other uniboard 2,1,0)(transceiver lane 3,2,1,0) or indexing (uniboard 3,2,1,0)(transceiver lane 3,2,1,0)
  type t_unb1_board_back_siso_2arr is array (c_unb1_board_tr_back_hw_nof_bus - 1 downto 0) of t_dp_siso_arr(   c_unb1_board_tr_back_hw_bus_w - 1 downto 0);  -- indexing (other uniboard 2,1,0)(transceiver lane 3,2,1,0) or indexing (uniboard 3,2,1,0)(transceiver lane 3,2,1,0)
  type t_unb1_board_back_sl_2arr   is array (c_unb1_board_tr_back_hw_nof_bus - 1 downto 0) of std_logic_vector(c_unb1_board_tr_back_hw_bus_w - 1 downto 0);  -- indexing (other uniboard 2,1,0)(transceiver lane 3,2,1,0)

  type t_unb1_board_xaui_sl_2arr   is array (integer range <>) of std_logic_vector(c_unb1_board_tr_xaui_hw_bus_w - 1 downto 0);

  constant c_unb1_board_back_sosi_2arr_rst   : t_unb1_board_back_sosi_2arr := (others => (others => c_dp_sosi_rst));
  constant c_unb1_board_back_siso_2arr_rst   : t_unb1_board_back_siso_2arr := (others => (others => c_dp_siso_rst));
  constant c_unb1_board_back_sl_2arr_rst     : t_unb1_board_back_sl_2arr   := (others => (others => '0'));

  -- DDR3 (definitions similar as in ug_altmemphy.pdf)
  type t_c_unb1_board_ddr is record
    a_w                               : natural;  -- = 16;
    ba_w                              : natural;  -- = 3;
    dq_w                              : natural;  -- = 64;
    nof_dq_per_dqs                    : natural;  -- = 8;
    dqs_w                             : natural;  -- = 8;  -- = dq_w / nof_dq_per_dqs;
    dm_w                              : natural;  -- = 8;
    cs_w                              : natural;  -- = 2;
    clk_w                             : natural;  -- = 2;
  end record;

  constant c_unb1_board_ddr                  : t_c_unb1_board_ddr := (16, 3, 64, 8, 8, 8, 2, 2);
  constant c_unb1_board_ddr_4g               : t_c_unb1_board_ddr := (15, 3, 64, 8, 8, 8, 2, 2);
  constant c_unb1_board_ddr_1g               : t_c_unb1_board_ddr := (14, 3, 64, 8, 8, 8, 1, 2);

  -- Auxiliary

  -- Test IO Interface
  type t_c_unb1_board_testio is record
    tst_w                             : natural;  -- = nof tst = 4; [tst_w-1 +tst_lo : tst_lo] = [7:4],
    led_w                             : natural;  -- = nof led = 2; [led_w-1 +led_lo : led_lo] = [3:2],
    jmp_w                             : natural;  -- = nof jmp = 2; [jmp_w-1 +jmp_lo : jmp_lo] = [1:0],
    tst_lo                            : natural;  -- = 4;
    led_lo                            : natural;  -- = 2;
    jmp_lo                            : natural;  -- = 0;
  end record;

  constant c_unb1_board_testio               : t_c_unb1_board_testio := (4, 2, 2, 4, 2, 0);
  constant c_unb1_board_testio_led_green     : natural := c_unb1_board_testio.led_lo;
  constant c_unb1_board_testio_led_red       : natural := c_unb1_board_testio.led_lo + 1;

  type t_c_unb1_board_aux is record
    version_w                         : natural;  -- = 2;
    id_w                              : natural;  -- = 8;  -- 5+3 bits wide = total node ID for up to 32 UniBoards in a system and 8 nodes per board
    chip_id_w                         : natural;  -- = 3;  -- board node ID for the 8 FPGA nodes on a UniBoard
    testio_w                          : natural;  -- = 8;
    testio                            : t_c_unb1_board_testio;
  end record;

  constant c_unb1_board_aux                  : t_c_unb1_board_aux := (2, 8, c_unb1_board_nof_chip_w, 8, c_unb1_board_testio);

  type t_e_unb1_board_node is (e_fn, e_bn, e_any);

  -- UniBoard Common Interface
  type t_c_unb1_board_ci is record
    tr                                : t_c_unb1_board_tr;
    ddr                               : t_c_unb1_board_ddr;
    aux                               : t_c_unb1_board_aux;
  end record;

  constant c_unb1_board_ci                   : t_c_unb1_board_ci := (c_unb1_board_tr, c_unb1_board_ddr, c_unb1_board_aux);

  -- ADC Interface
  type t_c_unb1_board_ai is record
    nof_ports                         : natural;  -- = 4;     -- Fixed 4 ADC BI port with names A, B, C, D
    nof_ovr                           : natural;  -- = 2;     -- Fixed 2 overflow signals, one for ports AB and one for ports CD
    port_w                            : natural;  -- = 8;     -- Fixed 8 bit ADC BI port width, the ADC sample width is also 8 bit
    lvds_data_rate                    : natural;  -- = 800;   -- The ADC sample rate is 800 Msps, so the LVDS rate is 800 Mbps per ADC BI data line,
    use_dpa                           : boolean;  -- = TRUE;  -- When TRUE use LVDS_RX with DPA, else used fixed IOE delays and/or lvds_clk_phase instead of DPA
    use_lvds_clk                      : boolean;  -- = TRUE;  -- When TRUE use the one or both ADC BI lvds_clk, else use the single dp_clk to capture the lvds data
    use_lvds_clk_rst                  : boolean;  -- = FALSE; -- When TRUE then support reset pulse to ADU to align the lvds_clk to the dp_clk, else no support
    lvds_clk_phase                    : natural;  -- = 0;     -- Use PLL phase 0 for center aligned. Only for no DPA
    nof_clocks                        : natural;  -- = 2;     -- 1 --> Use ADC BI clock D or dp_clk and 32 bit port ABCD
                                                              -- 2 --> Use ADC BI clock A, D and 16 bit ports AB, CD
    lvds_deser_factor                 : natural;  -- = 2;     -- The ADC sampled data comes in with a DDR lvds_clk, so lvds_data_rate / 2
    dp_deser_factor                   : natural;  -- = 4;     -- The Data Path clock dp_clk frequency is 200 MHz, so lvds_data_rate / 4
  end record;

  constant c_unb1_board_ai                   : t_c_unb1_board_ai := (4, 2, 8, 800, true, true, false, 0, 2, 2, 4);

  type t_unb1_board_fw_version is record
    hi                                : natural;  -- = 0..15
    lo                                : natural;  -- = 0..15, firmware version is: hi.lo
  end record;

  constant c_unb1_board_fw_version           : t_unb1_board_fw_version := (0, 0);

  -- SIGNAL RECORD DECLARATIONS -----------------------------------------------

  -- DDR3 (definitions similar as in ug_altmemphy.pdf)
  type t_unb1_board_ddr_in is record
    evt              : std_logic;
    --nc               : STD_LOGIC;   -- not connect, needed to be able to initialize constant record which has to have more than one field in VHDL
  end record;

  type t_unb1_board_ddr_inout is record
    dq               : std_logic_vector(c_unb1_board_ci.ddr.dq_w - 1 downto 0);  -- data bus
    dqs              : std_logic_vector(c_unb1_board_ci.ddr.dqs_w - 1 downto 0);  -- data strobe bus
    dqs_n            : std_logic_vector(c_unb1_board_ci.ddr.dqs_w - 1 downto 0);
    clk              : std_logic_vector(c_unb1_board_ci.ddr.clk_w - 1 downto 0);  -- clock, positive edge clock
    clk_n            : std_logic_vector(c_unb1_board_ci.ddr.clk_w - 1 downto 0);  -- clock, negative edge clock
    scl              : std_logic;  -- I2C
    sda              : std_logic;
  end record;

  type t_unb1_board_ddr_out is record
    a                : std_logic_vector(c_unb1_board_ci.ddr.a_w - 1 downto 0);  -- row and column address
    ba               : std_logic_vector(c_unb1_board_ci.ddr.ba_w - 1 downto 0);  -- bank address
    dm               : std_logic_vector(c_unb1_board_ci.ddr.dm_w - 1 downto 0);  -- data mask bus
    cas_n            : std_logic;  -- column address strobe
    ras_n            : std_logic;  -- row address strobe
    we_n             : std_logic;  -- write enable signal
    reset_n          : std_logic;  -- reset signal
    odt              : std_logic_vector(c_unb1_board_ci.ddr.cs_w - 1 downto 0);  -- on-die termination control signal
    cke              : std_logic_vector(c_unb1_board_ci.ddr.cs_w - 1 downto 0);  -- clock enable
    cs_n             : std_logic_vector(c_unb1_board_ci.ddr.cs_w - 1 downto 0);  -- chip select
  end record;

  --CONSTANT c_unb1_board_ddr_in_rst    : t_unb1_board_ddr_in    := ('0', 'X');
  constant c_unb1_board_ddr_inout_rst : t_unb1_board_ddr_inout := ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), '0', '0');
  constant c_unb1_board_ddr_out_rst   : t_unb1_board_ddr_out   := ((others => '0'), (others => '0'), (others => '0'), '0', '0', '0', '0', (others => '0'), (others => '0'), (others => '0'));

  -- I2C, MDIO
  -- . If no I2C bus arbitration or clock stretching is needed then the SCL only needs to be output.
  -- . Can also be used for a PHY Management Data IO interface with serial clock MDC and serial data MDIO
  type t_unb1_board_i2c_inout is record
    scl : std_logic;  -- serial clock
    sda : std_logic;  -- serial data
  end record;

  -- System info
  type t_c_unb1_board_system_info is record
    version  : natural;  -- UniBoard board HW version (2 bit value)
    id       : natural;  -- UniBoard FPGA node id (8 bit value)
                         -- Derived ID info:
    bck_id   : natural;  -- = id[7:3], ID part from back plane
    chip_id  : natural;  -- = id[2:0], ID part from UniBoard
    is_bn    : natural;  -- = id[2], 0 for Front Node, 1 for Back Node
    node_id  : natural;  -- = id[1:0], node ID: 0, 1, 2 or 3
    is_bn3   : natural;  -- 1 for Back Node 3, else 0.
  end record;

  function func_unb1_board_system_info(VERSION : in std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
                                       ID      : in std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)) return t_c_unb1_board_system_info;

  function func_unb1_board_chip_id(chip_id   : in std_logic_vector(c_unb1_board_aux.chip_id_w - 1 downto 0);
                                   node_type : in t_e_unb1_board_node) return std_logic_vector;

  -- Connect: out_2arr = in_2arr of different types
  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_back_sosi_2arr;
  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_back_siso_2arr;
  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_mesh_sosi_2arr;
  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_mesh_siso_2arr;

  -- Transpose: out_2arr(J)(I) = in_2arr(I)(J) for different types
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_back_sosi_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_back_siso_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_mesh_sosi_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_mesh_siso_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_mesh_sosi_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_mesh_siso_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_back_sosi_2arr;
  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_back_siso_2arr;
end unb1_board_pkg;

package body unb1_board_pkg is
  function func_unb1_board_system_info(VERSION : in std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
                                       ID      : in std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)) return t_c_unb1_board_system_info is
    variable v_system_info : t_c_unb1_board_system_info;
  begin
    v_system_info.version := to_integer(unsigned(VERSION));
    v_system_info.id      := to_integer(unsigned(ID));
    v_system_info.bck_id  := to_integer(unsigned(ID(7 downto 3)));
    v_system_info.chip_id := to_integer(unsigned(ID(2 downto 0)));
    if ID(2) = '1' then v_system_info.is_bn := 1; else v_system_info.is_bn := 0; end if;
    v_system_info.node_id := to_integer(unsigned(ID(1 downto 0)));
    if unsigned(ID(2 downto 0)) = 7 then v_system_info.is_bn3 := 1; else v_system_info.is_bn3 := 0; end if;
    return v_system_info;
  end;

  function func_unb1_board_chip_id(chip_id   : in std_logic_vector(c_unb1_board_aux.chip_id_w - 1 downto 0);
                                   node_type : in t_e_unb1_board_node) return std_logic_vector is
    variable v_chip_id : std_logic_vector(chip_id'range);  -- [2:0]
  begin
    v_chip_id := chip_id;  -- default for design that can run on either FN or BN
    if node_type = e_fn then
      v_chip_id(2) := '0';  -- design that is intended to run on a FN
    elsif node_type = e_bn then
      v_chip_id(2) := '1';  -- design that is intended to run on a BN
    end if;
    return v_chip_id;
  end;

  ------------------------------------------------------------------------------
  -- Connect mesh 2arr - back 2arr
  ------------------------------------------------------------------------------

  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_back_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4 = c_unb1_board_tr_back_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4 = c_unb1_board_tr_back_hw_bus_w

    variable v_out_2arr : t_unb1_board_back_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(I)(J) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_back_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4 = c_unb1_board_tr_back_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4 = c_unb1_board_tr_back_hw_bus_w

    variable v_out_2arr : t_unb1_board_back_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(I)(J) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_mesh_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4 = c_unb1_board_tr_mesh_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4 = c_unb1_board_tr_mesh_hw_bus_w

    variable v_out_2arr : t_unb1_board_mesh_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(I)(J) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_connect_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_mesh_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4 = c_unb1_board_tr_mesh_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4 = c_unb1_board_tr_mesh_hw_bus_w

    variable v_out_2arr : t_unb1_board_mesh_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(I)(J) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  ------------------------------------------------------------------------------
  -- Transpose mesh 2arr - back 2arr
  ------------------------------------------------------------------------------

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_back_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4 = c_unb1_board_tr_back_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4 = c_unb1_board_tr_back_hw_bus_w

    variable v_out_2arr : t_unb1_board_back_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_back_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4 = c_unb1_board_tr_back_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4 = c_unb1_board_tr_back_hw_bus_w

    variable v_out_2arr : t_unb1_board_back_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_mesh_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4 = c_unb1_board_tr_mesh_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4 = c_unb1_board_tr_mesh_hw_bus_w

    variable v_out_2arr : t_unb1_board_mesh_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_mesh_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4 = c_unb1_board_tr_mesh_hw_nof_bus
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4 = c_unb1_board_tr_mesh_hw_bus_w

    variable v_out_2arr : t_unb1_board_mesh_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  ------------------------------------------------------------------------------
  -- Transpose mesh 2arr -mesh 2arr
  ------------------------------------------------------------------------------

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_sosi_2arr) return t_unb1_board_mesh_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4

    variable v_out_2arr : t_unb1_board_mesh_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_mesh_siso_2arr) return t_unb1_board_mesh_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_mesh_hw_nof_bus;  -- = 4
    constant c_bus_w   : natural := c_unb1_board_tr_mesh_hw_bus_w;  -- = 4

    variable v_out_2arr : t_unb1_board_mesh_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  ------------------------------------------------------------------------------
  -- Transpose back 2arr -back 2arr
  ------------------------------------------------------------------------------

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_sosi_2arr) return t_unb1_board_back_sosi_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4

    variable v_out_2arr : t_unb1_board_back_sosi_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;

  function func_unb1_board_transpose_2arr(in_2arr : t_unb1_board_back_siso_2arr) return t_unb1_board_back_siso_2arr is
    constant c_nof_bus : natural := c_unb1_board_tr_back_hw_nof_bus;  -- = 4
    constant c_bus_w   : natural := c_unb1_board_tr_back_hw_bus_w;  -- = 4

    variable v_out_2arr : t_unb1_board_back_siso_2arr;
  begin
    for I in c_nof_bus - 1 downto 0 loop
      for J in c_bus_w - 1 downto 0 loop
        v_out_2arr(J)(I) := in_2arr(I)(J);
      end loop;
    end loop;
    return v_out_2arr;
  end;
end unb1_board_pkg;
