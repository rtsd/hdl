-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: DP FIFO array for dual clock (= dc) domain wr and rd.
-- Description: See dp_fifo_core_arr.vhd.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_dc_arr is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_nof_streams    : natural := 1;
    g_data_w         : natural := 16;  -- Should be 2 times the c_complex_w if g_use_complex = TRUE
    g_data_signed    : boolean := false;  -- TRUE extends g_data_w bits with the sign bit, FALSE pads g_data_w bits with zeros.
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_aux_w          : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_aux        : boolean := false;
    g_use_ctrl       : boolean := true;  -- sop & eop
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_size      : natural := 512;  -- (16+2) * 512 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1
  );
  port (
    wr_rst      : in  std_logic;
    wr_clk      : in  std_logic;
    rd_rst      : in  std_logic;
    rd_clk      : in  std_logic;
    -- Monitor FIFO filling
    wr_ful      : out std_logic;
    wr_usedw    : out std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    rd_usedw    : out std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    rd_emp      : out std_logic;
    -- ST sink
    snk_out_arr : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    in_aux      : in  std_logic_vector(g_aux_w - 1 downto 0) := (others => '0');
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_aux     : out std_logic_vector(g_aux_w - 1 downto 0)
  );
end dp_fifo_dc_arr;

architecture str of dp_fifo_dc_arr is
begin
  u_dp_fifo_core_arr : entity work.dp_fifo_core_arr
  generic map (
    g_technology     => g_technology,
    g_nof_streams    => g_nof_streams,
    g_use_dual_clock => true,
    g_data_w         => g_data_w,
    g_data_signed    => g_data_signed,
    g_bsn_w          => g_bsn_w,
    g_empty_w        => g_empty_w,
    g_channel_w      => g_channel_w,
    g_error_w        => g_error_w,
    g_aux_w          => g_aux_w,
    g_use_bsn        => g_use_bsn,
    g_use_empty      => g_use_empty,
    g_use_channel    => g_use_channel,
    g_use_error      => g_use_error,
    g_use_sync       => g_use_sync,
    g_use_aux        => g_use_aux,
    g_use_ctrl       => g_use_ctrl,
    g_use_complex    => g_use_complex,
    g_fifo_size      => g_fifo_size,
    g_fifo_af_margin => g_fifo_af_margin,
    g_fifo_af_xon    => g_fifo_af_xon,
    g_fifo_rl        => g_fifo_rl
  )
  port map (
    wr_rst      => wr_rst,
    wr_clk      => wr_clk,
    rd_rst      => rd_rst,
    rd_clk      => rd_clk,
    -- Monitor FIFO filling
    wr_ful      => wr_ful,
    wr_usedw    => wr_usedw,
    rd_usedw    => rd_usedw,
    rd_emp      => rd_emp,
    -- ST sink
    snk_out_arr => snk_out_arr,
    snk_in_arr  => snk_in_arr,
    in_aux      => in_aux,
    -- ST source
    src_in_arr  => src_in_arr,
    src_out_arr => src_out_arr,
    out_aux     => out_aux
  );
end str;
