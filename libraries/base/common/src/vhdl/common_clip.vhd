-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Function:
--   When enabled clip input, else pass input on unchanged. Report clippled
--   output data via overflow bit.
--   . Clip   signed input to range [-g_full_scale +g_full_scale]
--   . Clip unsigned input to range [ 0            +g_full_scale]
-- Remark:
-- . Define g_full_scale as SIGNED to avoid NATURAL limited to <= 2**31-1.
-- . Input and output must have the same width
-- . Use g_full_scale = 2**(c_dat_w-1)-1 to achieve a symmetrical range which
--   allow skipping one sign bit after multiplication. E.g. 18b*18b --> 35b
--   are sufficient for the signed product instead of 36b.

entity common_clip is
  generic (
    g_representation : string  := "SIGNED";  -- SIGNED or UNSIGNED clipping
    g_pipeline       : natural := 1;
    g_full_scale     : unsigned
  );
  port (
    rst      : in  std_logic := '0';
    clk      : in  std_logic;
    clken    : in  std_logic := '1';
    enable   : in  std_logic := '1';
    in_dat   : in  std_logic_vector;
    out_dat  : out std_logic_vector;
    out_ovr  : out std_logic
  );
end;

architecture rtl of common_clip is
  constant c_s_full_scale_w : natural := g_full_scale'length + 1;
  constant c_u_full_scale_w : natural := g_full_scale'length;

  constant c_s_full_scale   :   signed(c_s_full_scale_w - 1 downto 0) := signed('0' & std_logic_vector(g_full_scale));
  constant c_u_full_scale   : unsigned(c_u_full_scale_w - 1 downto 0) := g_full_scale;

  constant c_output_pipe    : natural := sel_a_b(g_pipeline > 1, g_pipeline-1, 0);

  constant c_dat_w          : natural := out_dat'length;

  signal clip_dat     : std_logic_vector(c_dat_w - 1 downto 0);
  signal nxt_clip_dat : std_logic_vector(c_dat_w - 1 downto 0);
  signal clip_ovr     : std_logic;
  signal nxt_clip_ovr : std_logic;

  signal pipe_in      : std_logic_vector(c_dat_w downto 0);
  signal pipe_out     : std_logic_vector(c_dat_w downto 0);
begin
  p_clip : process(in_dat, enable)
  begin
    nxt_clip_dat <= in_dat;
    nxt_clip_ovr <= '0';
    if enable = '1' then
      if g_representation = "SIGNED" then
        if    signed(in_dat) >  c_s_full_scale then
          nxt_clip_dat <= std_logic_vector(RESIZE_NUM( c_s_full_scale, c_dat_w));
          nxt_clip_ovr <= '1';
        elsif signed(in_dat) < - c_s_full_scale then
          nxt_clip_dat <= std_logic_vector(RESIZE_NUM(-c_s_full_scale, c_dat_w));
          nxt_clip_ovr <= '1';
        end if;
      else
        if unsigned(in_dat) > c_u_full_scale then
          nxt_clip_dat <= std_logic_vector(RESIZE_NUM(c_u_full_scale, c_dat_w));
          nxt_clip_ovr <= '1';
        end if;
      end if;
    end if;
  end process;

  no_reg : if g_pipeline = 0 generate
    clip_dat <= nxt_clip_dat;
    clip_ovr <= nxt_clip_ovr;
  end generate;

  gen_reg : if g_pipeline > 0 generate
    p_clk : process (rst, clk)
    begin
      if rst = '1' then
        clip_dat <= (others => '0');
        clip_ovr <= '0';
      elsif rising_edge(clk) then
        if clken = '1' then
          clip_dat <= nxt_clip_dat;
          clip_ovr <= nxt_clip_ovr;
        end if;
      end if;
    end process;
  end generate;

  -- Optional extra pipelining
  pipe_in <= clip_ovr & clip_dat;

  u_output_pipe : entity work.common_pipeline
  generic map (
    g_pipeline       => c_output_pipe,
    g_in_dat_w       => c_dat_w + 1,
    g_out_dat_w      => c_dat_w + 1
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_dat  => pipe_in,
    out_dat => pipe_out
  );

  out_ovr <= pipe_out(pipe_out'high);
  out_dat <= pipe_out(pipe_out'high - 1 downto 0);
end rtl;
