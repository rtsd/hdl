-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer
-- Purpose:
--   IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_mult_component_pkg is
  -----------------------------------------------------------------------------
  -- Stratix IV components
  -----------------------------------------------------------------------------

  component ip_stratixiv_complex_mult is
  port
  (
    aclr          : in std_logic;
    clock         : in std_logic;
    dataa_imag    : in std_logic_vector(17 downto 0);
    dataa_real    : in std_logic_vector(17 downto 0);
    datab_imag    : in std_logic_vector(17 downto 0);
    datab_real    : in std_logic_vector(17 downto 0);
    ena           : in std_logic;
    result_imag   : out std_logic_vector(35 downto 0);
    result_real   : out std_logic_vector(35 downto 0)
  );
  end component;

  component ip_stratixiv_complex_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
  end component;

  component ip_stratixiv_mult is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  component ip_stratixiv_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  component ip_stratixiv_mult_add2_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(2)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub          : string := "ADD";  -- or "SUB"
    g_nof_mult         : integer := 2;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_stratixiv_mult_add4_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- Arria 10 components
  -----------------------------------------------------------------------------

  component ip_arria10_complex_mult is
  port (
    dataa_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(35 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(35 downto 0)  -- .result_imag
  );
  end component;

  component ip_arria10_complex_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
  end component;

  component ip_arria10_complex_mult_rtl_canonical is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
--    g_conjugate_b      : BOOLEAN := FALSE;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
  end component;

  component ip_arria10_mult is
  generic (
    g_in_a_w           : positive := 18;  -- Width of the data A port
    g_in_b_w           : positive := 18;  -- Width of the data B port
    g_out_p_w          : positive := 36;  -- Width of the result port
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  component ip_arria10_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- Arria 10 e3sge3 components
  -----------------------------------------------------------------------------

  component ip_arria10_e3sge3_mult_add4_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- Arria 10 e1sg components
  -----------------------------------------------------------------------------

  component ip_arria10_e1sg_mult_add2_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(2)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub          : string := "ADD";  -- or "SUB"
    g_nof_mult         : integer := 2;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_arria10_e1sg_mult_add4_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_arria10_e1sg_complex_mult is
  port (
    dataa_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(35 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(35 downto 0)  -- .result_imag
  );
  end component;

  component ip_arria10_e1sg_complex_mult_27b is
  port (
    dataa_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(53 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(53 downto 0)  -- .result_imag
  );
  end component;

  -----------------------------------------------------------------------------
  -- Arria 10 e2sg components
  -----------------------------------------------------------------------------

  component ip_arria10_e2sg_mult_add2_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(2)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub          : string := "ADD";  -- or "SUB"
    g_nof_mult         : integer := 2;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_arria10_e2sg_mult_add4_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_arria10_e2sg_complex_mult is
  port (
    dataa_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(35 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(35 downto 0)  -- .result_imag
  );
  end component;

  component ip_arria10_e2sg_complex_mult_27b is
  port (
    dataa_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(53 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(53 downto 0)  -- .result_imag
  );
  end component;

  -----------------------------------------------------------------------------
  -- Agilex 7 (agi027) 1e1v components
  -----------------------------------------------------------------------------

  component ip_agi027_1e1v_mult is
  generic (
    g_in_a_w           : positive := 18;  -- Width of the data A port
    g_in_b_w           : positive := 18;  -- Width of the data B port
    g_out_p_w          : positive := 36;  -- Width of the result port
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_mult_add2_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(2)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub          : string := "ADD";  -- or "SUB"
    g_nof_mult         : integer := 2;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_mult_add4_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_complex_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_complex_mult_rtl_canonical is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
--    g_conjugate_b      : BOOLEAN := FALSE;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_complex_mult is
  port (
    dataa_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(17 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(35 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(35 downto 0)  -- .result_imag
  );
  end component;

  component ip_agi027_1e1v_complex_mult_27b is
  port (
    dataa_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- complex_input.dataa_real
    dataa_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .dataa_imag
    datab_real  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_real
    datab_imag  : in  std_logic_vector(26 downto 0) := (others => '0');  -- .datab_imag
    clock       : in  std_logic                     := '0';  -- .clk
    aclr        : in  std_logic                     := '0';  -- .aclr
    ena         : in  std_logic                     := '0';  -- .ena
    result_real : out std_logic_vector(53 downto 0);  -- complex_output.result_real
    result_imag : out std_logic_vector(53 downto 0)  -- .result_imag
  );
  end component;
end tech_mult_component_pkg;
