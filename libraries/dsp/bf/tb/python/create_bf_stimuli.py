#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# System imports
import sys
import getopt
from tools import *
from common import *

###############################################################################

def main(argv):   

  c_nof_signalpaths = 16
  c_nof_subbands = 96
  
  try:
      opts, args = getopt.getopt(argv,"hs:b:")
  except getopt.GetoptError:
      print 'create_bf_stimuli.py -s <nof signal paths> -b <nof subbands>'
      sys.exit(2)
  if len(opts) == 0:
      print 'Missing arguments!!!!'
      print 'create_bf_stimuli.py -s <nof signal paths> -b <nof subbands>'
      sys.exit()
  for opt, arg in opts:
      if opt == '-h':
         print 'create_bf_stimuli.py -s <nof signal paths> -b <nof subbands>'
         sys.exit()
      elif opt == '-s':
         c_nof_signalpaths = int(arg)
      elif opt == '-b':
         c_nof_subbands = int(arg)
  
  print "Creating files for nof_signalpaths = %d nof_subbands = %d" % (c_nof_signalpaths, c_nof_subbands) 
       
  # Setup
  c_coefs_width = 16
  c_file_prefix = "../data/bf_in_data_s%d_b%d" % (c_nof_signalpaths, c_nof_subbands) 
  ramp_data = 0

  filename = c_file_prefix + ".dat"
  f = file(filename, "w")
  
  for i in range(c_nof_subbands):         # Make the current list
      s = ""
      for j in range(c_nof_signalpaths):  # Make the current list
          s_add = "%d    %d    " % (j, ramp_data) 
          s = s + s_add
          if j == c_nof_signalpaths-1:
              s = s + "\n"
      f.write(s)        
      ramp_data = ramp_data + 1
  f.close()
  
        
if __name__ == "__main__":  
   main(sys.argv[1:])     