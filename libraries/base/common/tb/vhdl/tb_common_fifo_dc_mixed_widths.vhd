-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_fifo_dc_mixed_widths
-- Usage:
--   > as 10
--   > run -all
--   . observe rd_dat in wave window

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_fifo_dc_mixed_widths is
  generic (
    g_wr_clk_freq : positive := 1;  -- normalized write clock frequency
    g_rd_clk_freq : positive := 1;  -- normalized read  clock frequency
    g_wr_dat_w    : natural :=  8;
    g_rd_dat_w    : natural := 16
    --g_rd_dat_w    : NATURAL := 4
  );
end tb_common_fifo_dc_mixed_widths;

architecture tb of tb_common_fifo_dc_mixed_widths is
  constant clk_period           : time := 10 ns;

  constant c_run_interval       : natural := 2**g_wr_dat_w;

  constant c_wr_fifo_nof_words  : natural := 32;

  signal tb_end               : std_logic := '0';
  signal rst                  : std_logic;

  signal wr_clk               : std_logic := '0';
  signal wr_dat               : std_logic_vector(g_wr_dat_w - 1 downto 0);
  signal wr_val               : std_logic;
  signal wr_ful               : std_logic;
  signal wr_usedw             : std_logic_vector(ceil_log2(c_wr_fifo_nof_words) - 1 downto 0);

  signal rd_clk               : std_logic := '0';
  signal rd_dat               : std_logic_vector(g_rd_dat_w - 1 downto 0);
  signal rd_req               : std_logic;
  signal rd_val               : std_logic;
  signal rd_emp               : std_logic;
  signal rd_usedw             : std_logic_vector(ceil_log2(c_wr_fifo_nof_words * g_wr_dat_w / g_rd_dat_w) - 1 downto 0);
begin
  rst <= '1', '0' after clk_period * 7;

  wr_clk  <= not wr_clk or tb_end after g_rd_clk_freq * clk_period / 2;
  rd_clk  <= not rd_clk or tb_end after g_wr_clk_freq * clk_period / 2;

  p_wr_stimuli : process
  begin
    wr_dat <= TO_UVEC(0, g_wr_dat_w);
    wr_val <= '0';
    wait until rst = '0';
    proc_common_wait_some_cycles(wr_clk, 10);

    wr_val <= '1';
    wait until rising_edge(wr_clk);
    for I in 0 to 2 * c_run_interval - 1 loop
      wr_dat <= INCR_UVEC(wr_dat, 1);
      wr_val <= '1';
      wait until rising_edge(wr_clk);
    end loop;
    wr_val <= '0';
    proc_common_wait_some_cycles(wr_clk, 10);

    for I in 0 to 9 loop
      wr_dat <= INCR_UVEC(wr_dat, 1);
      wr_val <= '1';
      wait until rising_edge(wr_clk);
      wr_val <= '0';
      proc_common_wait_some_cycles(wr_clk, 10);
    end loop;

    proc_common_wait_some_cycles(wr_clk, 100);
    proc_common_wait_some_cycles(rd_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  rd_req <= '1';

  u_dut : entity work.common_fifo_dc_mixed_widths
  generic map (
    g_nof_words => c_wr_fifo_nof_words,
    g_wr_dat_w  => g_wr_dat_w,
    g_rd_dat_w  => g_rd_dat_w
  )
  port map (
    rst     => rst,
    wr_clk  => wr_clk,
    wr_dat  => wr_dat,
    wr_req  => wr_val,
    wr_ful  => wr_ful,
    wrusedw => wr_usedw,
    rd_clk  => rd_clk,
    rd_dat  => rd_dat,
    rd_req  => rd_req,
    rd_emp  => rd_emp,
    rdusedw => rd_usedw,
    rd_val  => rd_val
  );
end tb;
