-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi-testbench for mms_diag_block_gen
-- Description:
--   Verify mms_diag_block_gen
-- Usage:
--   > as 4
--   > run -all

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tb_mms_diag_block_gen is
end tb_tb_mms_diag_block_gen;

architecture tb of tb_tb_mms_diag_block_gen is
  constant c_nof_repeat : natural := 2;

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- g_use_usr_input        : BOOLEAN := TRUE;
-- g_use_bg               : BOOLEAN := TRUE;
-- g_use_tx_seq           : BOOLEAN := FALSE;
-- g_use_bg_buffer_ram    : BOOLEAN := TRUE;
-- g_usr_bypass_xonoff    : BOOLEAN := FALSE;
-- g_flow_control_verify  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
-- g_nof_repeat           : NATURAL := 2;
-- g_nof_streams          : NATURAL := 16;
-- g_gap_size             : NATURAL := 160

  u_bg_one_stream               : entity work.tb_mms_diag_block_gen generic map (false,  true, false,  true, false, e_active, 1, 1,   0);
  u_bg_gap_0                    : entity work.tb_mms_diag_block_gen generic map (false,  true, false,  true, false, e_active, 1, 3,   0);
  u_bg_gap_160                  : entity work.tb_mms_diag_block_gen generic map (false,  true, false,  true, false, e_active, 1, 3, 160);

  u_mux_one_stream              : entity work.tb_mms_diag_block_gen generic map ( true,  true, false,  true, false, e_active, 5, 1,   0);
  u_mux_gap_0                   : entity work.tb_mms_diag_block_gen generic map ( true,  true, false,  true, false, e_active, 1, 2,   1);
  u_mux_gap_1                   : entity work.tb_mms_diag_block_gen generic map ( true,  true, false,  true,  true, e_active, 1, 2,   1);

  u_bg_tx_seq                   : entity work.tb_mms_diag_block_gen generic map (false,  true,  true,  true, false, e_active, 2, 2,   0);

  u_usr_bypass_all              : entity work.tb_mms_diag_block_gen generic map ( true, false, false,  true, false, e_active, 1, 2,   0);
  u_usr_tx_seq_random_stream_0  : entity work.tb_mms_diag_block_gen generic map ( true, false,  true,  true, false, e_random, 2, 2,   0);  -- stream 1 is active, so both e_random and e_active are tested

  u_bg_no_buffer_ram            : entity work.tb_mms_diag_block_gen generic map (false,  true, false, false, false, e_active, 1, 2,   0);
end tb;
