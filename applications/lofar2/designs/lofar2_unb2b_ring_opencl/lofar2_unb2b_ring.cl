/* *************************************************************************
* Copyright 2021
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Reinier vd Walle
* Purpose:
* . Implements ring network functionality for UniBoard2b as part of Lofar2 
* *********************************************************************** */

#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

// Directives
#define DIVIDE_AND_ROUND_UP(A,B) (((A)+(B)-1)/(B))
#define FLAG_FIRST	0x01
#define FLAG_LAST	0x02
#define FLAG_SYNC	0x04
#define MASK_BSN 0x7FFFFFFFFFFFFFFF
#define MASK_SYNC 0x8000000000000000
#define LANE_DIRECTION 1
// Nof lanes = 1 - 8
#define NOF_LANES 8
#define USE_DP_LAYER 
#define ETH_HEADER_SIZE 2
#define DP_HEADER_SIZE 3
#ifdef EMULATOR
#define PAYLOAD_SIZE 8
#else
#define PAYLOAD_SIZE 750 // = 750*8 bytes = 6000 bytes
#endif
#ifdef USE_DP_LAYER
#define BLOCK_LENGTH (PAYLOAD_SIZE+DP_HEADER_SIZE)
#else
#define BLOCK_LENGTH (PAYLOAD_SIZE+ETH_HEADER_SIZE)
#endif

#define ERR_BI 6
#define NOF_ERR_COUNTS 7
#define REMOVE_CHANNEL 16 //default

// Mac checks for packet size if type < 0x600, other values the MAC uses 
// are 0x8100 (VLAN) and 0x8808 (Control frames).
#define ETHER_TYPE 0x0600

// mm_channel order enum
enum mm_channel {
  CH_INTERFACE_SELECT,
  CH_BLOCK_VALIDATE_DECODE_0,
  CH_BLOCK_VALIDATE_DECODE_1,
  CH_BLOCK_VALIDATE_DECODE_2,
  CH_BLOCK_VALIDATE_DECODE_3,
  CH_BLOCK_VALIDATE_DECODE_4,
  CH_BLOCK_VALIDATE_DECODE_5,
  CH_BLOCK_VALIDATE_DECODE_6,
  CH_BLOCK_VALIDATE_DECODE_7,
  CH_VALIDATE_CHANNEL,
  CH_LANE_DIRECTION,
  LAST_MM_CHANNEL_ENTRY
};

// M&C parameters definitions
struct param_rx_validate_struct {
  uint block_cnt;
  uint err_cnt[NOF_ERR_COUNTS];
};
union param_rx_validate {
  struct param_rx_validate_struct parameters;
  uint arr[DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))];
};

struct param_interface_select_struct {
  uint input_select;
  uint output_select;
};
union param_interface_select {
  struct param_interface_select_struct parameters;
  uint arr[DIVIDE_AND_ROUND_UP(sizeof(struct param_interface_select_struct),sizeof(uint))];
};

struct param_validate_channel_struct {
  uint transport_nof_hops;
};
union param_validate_channel {
  struct param_validate_channel_struct parameters[NOF_LANES];
  uint arr[DIVIDE_AND_ROUND_UP(NOF_LANES*sizeof(struct param_validate_channel_struct),sizeof(uint))];
};

// register struct
struct reg {
  uint offset;
  uint size;
} __attribute__((packed));

// Channel element structs
struct mm_in {
  uint wrdata;
  uint address;
  bool wr;
} __attribute__((packed));

struct mm_out {
  uint rddata;
} __attribute__((packed));

struct line_10GbE {
  uint64_t data;
  uchar flags;
  uint err;
} __attribute__((packed));

struct line_dp {
  uint64_t data;
  uchar flags;
  uint64_t dp_bsn;
  uint dp_channel;
} __attribute__((packed));

struct line_bs_sosi {
  uint data;
  uchar flags;
  uint64_t dp_bsn;
} __attribute__((packed));


// Ethernet packet definition
struct ethernet_header_struct {
  uchar destination_mac[6], source_mac[6];
  ushort ether_type;
}__attribute__((packed));

struct ethernet_packet_struct {
  struct ethernet_header_struct ethernet_header;
  ushort padding; 
  uint64_t payload[PAYLOAD_SIZE];
}__attribute__((packed)); 

union eth_packet {
  struct ethernet_packet_struct packet;
  uint64_t raw[PAYLOAD_SIZE+ETH_HEADER_SIZE];
} __attribute__((packed));

// DP packet definition
struct dp_header_struct {
  ushort dp_channel;
  uint64_t dp_sync_and_bsn; // 62:0 = bsn, 63=sync
} __attribute__((packed));

struct dp_packet_struct{
  struct ethernet_header_struct ethernet_header;
  struct dp_header_struct dp_header;
  uint64_t payload[PAYLOAD_SIZE];
} __attribute__((packed));

union dp_packet {
  struct dp_packet_struct packet;
  uint64_t raw[PAYLOAD_SIZE+DP_HEADER_SIZE];
} __attribute__((packed));


// IO channels
channel struct mm_in ch_in_mm __attribute__((depth(0))) __attribute__((io("kernel_input_mm")));
channel struct mm_out ch_out_mm __attribute__((depth(0))) __attribute__((io("kernel_output_mm")));

channel struct line_10GbE ch_in_ring_0  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_0")));
channel struct line_10GbE ch_out_ring_0 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_0")));

channel struct line_10GbE ch_in_ring_1  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_1")));
channel struct line_10GbE ch_out_ring_1 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_1")));

channel struct line_10GbE ch_in_ring_2  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_2")));
channel struct line_10GbE ch_out_ring_2 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_2")));

channel struct line_10GbE ch_in_ring_3  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_3")));
channel struct line_10GbE ch_out_ring_3 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_3")));

channel struct line_10GbE ch_in_ring_4  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_4")));
channel struct line_10GbE ch_out_ring_4 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_4")));

channel struct line_10GbE ch_in_ring_5  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_5")));
channel struct line_10GbE ch_out_ring_5 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_5")));

channel struct line_10GbE ch_in_ring_6  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_6")));
channel struct line_10GbE ch_out_ring_6 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_6")));

channel struct line_10GbE ch_in_ring_7  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_ring_7")));
channel struct line_10GbE ch_out_ring_7 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_ring_7")));

channel struct line_10GbE ch_in_qsfp_0  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_qsfp_0")));
channel struct line_10GbE ch_out_qsfp_0 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_qsfp_0")));

channel struct line_10GbE ch_in_qsfp_1  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_qsfp_1")));
channel struct line_10GbE ch_out_qsfp_1 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_qsfp_1")));

channel struct line_10GbE ch_in_qsfp_2  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_qsfp_2")));
channel struct line_10GbE ch_out_qsfp_2 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_qsfp_2")));

channel struct line_10GbE ch_in_qsfp_3  __attribute__((depth(0))) __attribute__((io("kernel_input_10GbE_qsfp_3")));
channel struct line_10GbE ch_out_qsfp_3 __attribute__((depth(0))) __attribute__((io("kernel_output_10GbE_qsfp_3")));

channel struct line_dp ch_out_lane_0 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_0")));
channel struct line_dp ch_out_lane_1 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_1")));
channel struct line_dp ch_out_lane_2 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_2")));
channel struct line_dp ch_out_lane_3 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_3")));
channel struct line_dp ch_out_lane_4 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_4")));
channel struct line_dp ch_out_lane_5 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_5")));
channel struct line_dp ch_out_lane_6 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_6")));
channel struct line_dp ch_out_lane_7 __attribute__((depth(0))) __attribute__((io("kernel_output_lane_7")));

channel struct line_dp ch_in_lane_0 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_0")));
channel struct line_dp ch_in_lane_1 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_1")));
channel struct line_dp ch_in_lane_2 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_2")));
channel struct line_dp ch_in_lane_3 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_3")));
channel struct line_dp ch_in_lane_4 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_4")));
channel struct line_dp ch_in_lane_5 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_5")));
channel struct line_dp ch_in_lane_6 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_6")));
channel struct line_dp ch_in_lane_7 __attribute__((depth(0))) __attribute__((io("kernel_input_lane_7")));

channel struct line_dp ch_out_rx_monitor_0 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_0")));
channel struct line_dp ch_out_rx_monitor_1 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_1")));
channel struct line_dp ch_out_rx_monitor_2 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_2")));
channel struct line_dp ch_out_rx_monitor_3 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_3")));
channel struct line_dp ch_out_rx_monitor_4 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_4")));
channel struct line_dp ch_out_rx_monitor_5 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_5")));
channel struct line_dp ch_out_rx_monitor_6 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_6")));
channel struct line_dp ch_out_rx_monitor_7 __attribute__((depth(0))) __attribute__((io("kernel_output_rx_monitor_7")));

channel struct line_dp ch_out_tx_monitor_0 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_0")));
channel struct line_dp ch_out_tx_monitor_1 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_1")));
channel struct line_dp ch_out_tx_monitor_2 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_2")));
channel struct line_dp ch_out_tx_monitor_3 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_3")));
channel struct line_dp ch_out_tx_monitor_4 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_4")));
channel struct line_dp ch_out_tx_monitor_5 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_5")));
channel struct line_dp ch_out_tx_monitor_6 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_6")));
channel struct line_dp ch_out_tx_monitor_7 __attribute__((depth(0))) __attribute__((io("kernel_output_tx_monitor_7")));

channel struct line_bs_sosi ch_in_bs_sosi __attribute__((depth(0))) __attribute__((io("kernel_input_bs_sosi")));

// Internal channels
channel struct line_10GbE rx_10GbE_channels[NOF_LANES] __attribute__((depth(0)));
channel struct line_dp rx_decoded_channels[NOF_LANES] __attribute__((depth(0)));
channel struct line_dp rx_sosi_channels[NOF_LANES] __attribute__((depth(0)));

channel struct line_dp tx_validated_channels[NOF_LANES] __attribute__((depth(DP_HEADER_SIZE)));
channel struct line_10GbE tx_sosi_channels[NOF_LANES] __attribute__((depth(0)));

channel struct mm_in mm_channel_in[LAST_MM_CHANNEL_ENTRY] __attribute__((depth(0)));
channel struct mm_out mm_channel_out[LAST_MM_CHANNEL_ENTRY+1] __attribute__((depth(0))); // 1 extra channel for undefined addresses

// Constants
__constant uchar destination_mac[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
__constant uchar source_mac[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

__constant uint64_t c_header_out[ETH_HEADER_SIZE] = {(__constant uint64_t) 0xFFFFFFFFFFFF0000, (__constant uint64_t) 0x0000000006000000};

// Regmap table with offset, size. Offsets are chosen to fit the largest sizes when NOF_LANES=8
__constant struct reg regmap[LAST_MM_CHANNEL_ENTRY] = {
  {0 , DIVIDE_AND_ROUND_UP(sizeof(struct param_interface_select_struct),sizeof(uint))},          //CH_INTERFACE_SELECT, size = 2 
  {2 , DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_0 size = 8
  {10, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_1 size = 8
  {18, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_2 size = 8
  {26, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_3 size = 8
  {34, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_4 size = 8
  {42, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_5 size = 8
  {50, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_6 size = 8
  {58, DIVIDE_AND_ROUND_UP(sizeof(struct param_rx_validate_struct),sizeof(uint))},     //CH_BLOCK_VALIDATE_DECODE_7 size = 8
  {66, DIVIDE_AND_ROUND_UP(NOF_LANES*sizeof(struct param_validate_channel_struct),sizeof(uint))},//CH_VALIDATE_CHANNEL size = NOF_LANES*1 
  {74, 8}                                                                                        //CH_LANE_DIRECTION size = 8 
};

// helper functions
void handle_mm_request(const int ch_id, uint *reg_arr, bool ro)
{
  bool mm_valid;
  struct mm_in mm_request = read_channel_nb_intel(mm_channel_in[ch_id], &mm_valid); //non-blocking read
  struct mm_out mm_response;
  if (mm_valid) {
    if(mm_request.wr) //write request
    {
      if(!ro)
        reg_arr[mm_request.address] = mm_request.wrdata;
    } else { //read request
      mm_response.rddata = reg_arr[mm_request.address];
      write_channel_intel(mm_channel_out[ch_id], mm_response);
    }
  }
}

void handle_rw_mm_request(const int ch_id, uint *reg_arr)
{
  handle_mm_request(ch_id, reg_arr, false);
}

void handle_ro_mm_request(const int ch_id, uint *reg_arr)
{
  handle_mm_request(ch_id, reg_arr, true);
}


/* ----- MM Controller ----- */
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void mm_in_controller()
{
  while(1)
  {
    bool undefined = true;
    struct mm_in mm_request = read_channel_intel(ch_in_mm);
    #pragma unroll
    for (int i = 0; i < LAST_MM_CHANNEL_ENTRY; i++)
    {
      if (mm_request.address >= regmap[i].offset && mm_request.address < (regmap[i].offset + regmap[i].size))
      {
        undefined = false;
        struct mm_in local_mm_request;
        local_mm_request.wr = mm_request.wr;
        local_mm_request.wrdata = mm_request.wrdata;
        local_mm_request.address = mm_request.address - regmap[i].offset;
        write_channel_intel(mm_channel_in[i], local_mm_request);
      } 
    }

    if (undefined && mm_request.wr == 0)  { // undefined address
      struct mm_out zero_response;
      zero_response.rddata = 0;
      write_channel_intel(mm_channel_out[LAST_MM_CHANNEL_ENTRY], zero_response);
    }
  }
}


__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void mm_out_controller()
{
  while(1)
  {
    struct mm_out mm_response;
    for (int i = 0; i < LAST_MM_CHANNEL_ENTRY+1; i++)
    {
      bool valid;
      mm_response = read_channel_nb_intel(mm_channel_out[i], &valid);
      if (valid)
      {
        write_channel_intel(ch_out_mm, mm_response);
      }
    }
  }
}
/* ----- End of MM Controller ----- */


/* ----- Constant MM Read only parameters ----- */
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void lane_direction()
{
  uint lane_directions[8] = {1,0,1,0,1,0,1,0};
  for (int i = NOF_LANES; i < 8; i++){
    lane_directions[i] = -1; //force to -1 if lane is unused.
  }
  while(1){
    // handle MM read/write requests
    handle_ro_mm_request(CH_LANE_DIRECTION, lane_directions);
  }
}


/* ----- In/Output select -----*/
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void interface_select()
{
  union param_interface_select reg;
  reg.parameters.input_select = 0; // default input is board input = 0
  reg.parameters.output_select = 0; // default input is board input = 0
#ifdef EMULATOR
  int emu_i = 0;
#endif
  while(1){
    // handle MM read/write requests
    handle_rw_mm_request(CH_INTERFACE_SELECT, reg.arr);
    // Do someting with parameters
    
    struct line_10GbE line_out_ring[8];
    struct line_10GbE line_out_qsfp[4];
    struct line_10GbE line_in_ring[8];
    struct line_10GbE line_in_qsfp[4];
    bool valid_ring_input[8];
    bool valid_qsfp_input[4];
    bool valid_qsfp_output[4] = {};
    bool valid_ring_output[8] = {};

    line_in_qsfp[0] = read_channel_nb_intel(ch_in_qsfp_0, &valid_qsfp_input[0]);
    line_in_qsfp[1] = read_channel_nb_intel(ch_in_qsfp_1, &valid_qsfp_input[1]);
    line_in_qsfp[2] = read_channel_nb_intel(ch_in_qsfp_2, &valid_qsfp_input[2]);
    line_in_qsfp[3] = read_channel_nb_intel(ch_in_qsfp_3, &valid_qsfp_input[3]);

    line_in_ring[0] = read_channel_nb_intel(ch_in_ring_0, &valid_ring_input[0]);
    line_in_ring[1] = read_channel_nb_intel(ch_in_ring_1, &valid_ring_input[1]);
    line_in_ring[2] = read_channel_nb_intel(ch_in_ring_2, &valid_ring_input[2]);
    line_in_ring[3] = read_channel_nb_intel(ch_in_ring_3, &valid_ring_input[3]);
    line_in_ring[4] = read_channel_nb_intel(ch_in_ring_4, &valid_ring_input[4]);
    line_in_ring[5] = read_channel_nb_intel(ch_in_ring_5, &valid_ring_input[5]);
    line_in_ring[6] = read_channel_nb_intel(ch_in_ring_6, &valid_ring_input[6]);
    line_in_ring[7] = read_channel_nb_intel(ch_in_ring_7, &valid_ring_input[7]);

    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++)
    {    
      struct line_10GbE input_10GbE;
      struct line_10GbE output_10GbE;
      bool valid_input;    
      bool valid_output;    
      //read tx channels 
      output_10GbE = read_channel_nb_intel(tx_sosi_channels[i], &valid_output);
      // all even lanes are received from qsfp instead of board (odd lanes received from ring).
      // even lanes are therefore transmitted over ring and odd lanes are transmitted over qsfp.
      if (reg.parameters.input_select == 1 && reg.parameters.output_select == 0)  // RX_input select
      {
        if (i % 2) { // odd
          input_10GbE = line_in_ring[i];
          valid_input = valid_ring_input[i];
          if(valid_output){
            valid_qsfp_output[i/2] = true;
            line_out_qsfp[i/2] = output_10GbE;
          }
        } else { // even
          input_10GbE = line_in_qsfp[i/2];
          valid_input = valid_qsfp_input[i/2];
          if(valid_output){
            valid_ring_output[i] = true;
            line_out_ring[i] = output_10GbE;
          }
        }
      }

      // all even lanes are transmitted to qsfp instead of board (odd lanes transmitted to ring).
      // even lanes are therefore received from ring and odd lanes are received over qsfp.
      else if (reg.parameters.input_select == 0 && reg.parameters.output_select == 1) // TX_output select 
      {
        if (i % 2) { // odd
          input_10GbE = line_in_qsfp[i/2];
          valid_input = valid_qsfp_input[i/2];
          if(valid_output){
            valid_ring_output[i] = true;
            line_out_ring[i] = output_10GbE;
          }
        } else { // even
          input_10GbE = line_in_ring[i];
          valid_input = valid_ring_input[i];
          if(valid_output){
            valid_qsfp_output[i/2] = true;
            line_out_qsfp[i/2] = output_10GbE;
          }
        }

      }
      // All lanes are received from and transmitted to ring
      else { // board input
        input_10GbE = line_in_ring[i];
        valid_input = valid_ring_input[i];
        if(valid_output){
          valid_ring_output[i] = true;
          line_out_ring[i] = output_10GbE;
        }
      }
      // Write rx channels
      if(valid_input)
        write_channel_intel(rx_10GbE_channels[i], input_10GbE);
    }

    // Write channels to output
    if (valid_qsfp_output[0]){write_channel_intel(ch_out_qsfp_0, line_out_qsfp[0]);}
    if (valid_qsfp_output[1]){write_channel_intel(ch_out_qsfp_1, line_out_qsfp[1]);}
    if (valid_qsfp_output[2]){write_channel_intel(ch_out_qsfp_2, line_out_qsfp[2]);}
    if (valid_qsfp_output[3]){write_channel_intel(ch_out_qsfp_3, line_out_qsfp[3]);}

    if (valid_ring_output[0]){write_channel_intel(ch_out_ring_0, line_out_ring[0]);
#ifdef EMULATOR      
      emu_i++;
      if(emu_i >= BLOCK_LENGTH)
        break;
#endif
    }
    if (valid_ring_output[1]){write_channel_intel(ch_out_ring_1, line_out_ring[1]);}
    if (valid_ring_output[2]){write_channel_intel(ch_out_ring_2, line_out_ring[2]);}
    if (valid_ring_output[3]){write_channel_intel(ch_out_ring_3, line_out_ring[3]);}
    if (valid_ring_output[4]){write_channel_intel(ch_out_ring_4, line_out_ring[4]);}          
    if (valid_ring_output[5]){write_channel_intel(ch_out_ring_5, line_out_ring[5]);}         
    if (valid_ring_output[6]){write_channel_intel(ch_out_ring_6, line_out_ring[6]);}         
    if (valid_ring_output[7]){write_channel_intel(ch_out_ring_7, line_out_ring[7]);}          
  }
}
/* ----- End of In/Output select -----*/


/* ----- ring_rx  ----- */
__attribute__((num_compute_units(NOF_LANES), max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void block_validate_decode()
{
  const int laneIndex = get_compute_id(0);
  union param_rx_validate reg;
  reg.parameters.block_cnt = 0;
  for (int x = 0; x < NOF_ERR_COUNTS; x++)
    reg.parameters.err_cnt[x] = 0; 

#ifdef USE_DP_LAYER
  union dp_packet packets[2]; //one to read and one to write
#else
  union eth_packet packets[2]; //one to read and one to write
#endif
  bool valid = false;
  bool canWrite = false;
  uint i = 0; // read iterator
  uint j = 0; // write iterator
  uint1_t readIndex = 0;
  uint1_t writeIndex = 0;
  while(1){
    struct line_10GbE input_10GbE;
    struct line_dp line_out;
    bool ch_valid;
    
    handle_ro_mm_request((laneIndex+CH_BLOCK_VALIDATE_DECODE_0), reg.arr); // handle MM read/write requests

    input_10GbE = read_channel_nb_intel(rx_10GbE_channels[laneIndex], &ch_valid);
    if(ch_valid){
      // validation
      if((input_10GbE.flags & FLAG_LAST) == FLAG_LAST){
        if (i == BLOCK_LENGTH -1 && input_10GbE.err == 0) {
          valid = true;
          reg.parameters.block_cnt += 1;
        }
        else {
          if (i != BLOCK_LENGTH-1)
            reg.parameters.err_cnt[ERR_BI] += 1;
#pragma unroll
          for (int err = 0; err < NOF_ERR_COUNTS; err++){
            if (err != ERR_BI)
              reg.parameters.err_cnt[err] += ((input_10GbE.err & (1 << err)) >> err);
          }
        }       
      } 
      //Packet capturing
      packets[readIndex].raw[i] = input_10GbE.data;
      if (i == BLOCK_LENGTH-1 || (input_10GbE.flags & FLAG_LAST) == FLAG_LAST){
        i = 0; // reset read iterator
      }
      else {
        i++; //only iterate if ch_valid = true
      }
    } 
    
    // Packet decoding
    if (valid) {
      writeIndex = readIndex; // Write the stored packet 
      readIndex = !readIndex; // set read index to the packet which can be overwritten   
      valid = false;
      canWrite = true; // assumes canWrite will be false again before valid is true as outgoing packet is shorter than incoming packet
    }

    if (canWrite){
      line_out.data = packets[writeIndex].packet.payload[j];
      line_out.flags = 0;
      line_out.dp_bsn = 0;
      line_out.dp_channel = 0;
      if (j == 0) {
        line_out.flags |= FLAG_FIRST;
#ifdef USE_DP_LAYER
        line_out.dp_bsn = (packets[writeIndex].packet.dp_header.dp_sync_and_bsn & MASK_BSN); //62:0 = bsn
        line_out.dp_channel = packets[writeIndex].packet.dp_header.dp_channel;
        
        if( 0 != (packets[writeIndex].packet.dp_header.dp_sync_and_bsn & MASK_SYNC))
          line_out.flags |= FLAG_SYNC;
#endif
      }
      if (j == BLOCK_LENGTH-1){
        line_out.flags |= FLAG_LAST;
        j = 0;
        canWrite = false;
      } 
      else {
        j++;  
      }

      write_channel_intel(rx_decoded_channels[laneIndex], line_out);
    }
  }
}



__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void rx_split()
{
  while(1){
    struct line_dp line[NOF_LANES];
    bool valid[NOF_LANES];
    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++){
      line[i] = read_channel_nb_intel(rx_decoded_channels[i], &valid[i]);
      if (valid[i])
        write_channel_intel(rx_sosi_channels[i], line[i]);
    }
    
    if (valid[0]){ write_channel_intel(ch_out_rx_monitor_0, line[0]);} 
    if (valid[1]){ write_channel_intel(ch_out_rx_monitor_1, line[1]);} 
    if (valid[2]){ write_channel_intel(ch_out_rx_monitor_2, line[2]);} 
    if (valid[3]){ write_channel_intel(ch_out_rx_monitor_3, line[3]);} 
    if (valid[4]){ write_channel_intel(ch_out_rx_monitor_4, line[4]);} 
    if (valid[5]){ write_channel_intel(ch_out_rx_monitor_5, line[5]);} 
    if (valid[6]){ write_channel_intel(ch_out_rx_monitor_6, line[6]);} 
    if (valid[7]){ write_channel_intel(ch_out_rx_monitor_7, line[7]);} 
    
  }
}

#ifdef USE_DP_LAYER
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void validate_bsn_at_sync()
{
  bool discard[NOF_LANES] = {};
  uint64_t localBsn = 0;
#ifdef EMULATOR
  int emu_i = 0;
#endif
  while(1){
    struct line_bs_sosi bs_sosi;
    struct line_dp line[NOF_LANES];
    bool valid[NOF_LANES];
    bool bs_sosi_valid;

    bs_sosi = read_channel_nb_intel(ch_in_bs_sosi, &bs_sosi_valid);
    if (bs_sosi_valid && ((bs_sosi.flags & FLAG_SYNC) == FLAG_SYNC)) 
      localBsn = bs_sosi.dp_bsn;

    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++){
      line[i] = read_channel_nb_intel(rx_sosi_channels[i], &valid[i]);
      if (valid[i] && ((line[i].flags & FLAG_SYNC) == FLAG_SYNC))
         discard[i] = (localBsn != line[i].dp_bsn);
    }
     
    if ((!discard[0]) && valid[0]){ write_channel_intel(ch_out_lane_0, line[0]);
#ifdef EMULATOR
      emu_i++;
      if (emu_i >= PAYLOAD_SIZE)
        break;
#endif
    } 
    if ((!discard[1]) && valid[1]){ write_channel_intel(ch_out_lane_1, line[1]);} 
    if ((!discard[2]) && valid[2]){ write_channel_intel(ch_out_lane_2, line[2]);} 
    if ((!discard[3]) && valid[3]){ write_channel_intel(ch_out_lane_3, line[3]);} 
    if ((!discard[4]) && valid[4]){ write_channel_intel(ch_out_lane_4, line[4]);} 
    if ((!discard[5]) && valid[5]){ write_channel_intel(ch_out_lane_5, line[5]);} 
    if ((!discard[6]) && valid[6]){ write_channel_intel(ch_out_lane_6, line[6]);} 
    if ((!discard[7]) && valid[7]){ write_channel_intel(ch_out_lane_7, line[7]);} 

  }
}

#else
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void no_validate_bsn_at_sync()
{
  while(1){
    struct line_dp line[NOF_LANES];
    bool valid[NOF_LANES];
    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++)
      line[i] = read_channel_nb_intel(rx_sosi_channels[i], &valid[i]);
   
    if(valid[0]){ write_channel_intel(ch_out_lane_0, line[0]);} 
    if(valid[1]){ write_channel_intel(ch_out_lane_1, line[1]);} 
    if(valid[2]){ write_channel_intel(ch_out_lane_2, line[2]);} 
    if(valid[3]){ write_channel_intel(ch_out_lane_3, line[3]);} 
    if(valid[4]){ write_channel_intel(ch_out_lane_4, line[4]);} 
    if(valid[5]){ write_channel_intel(ch_out_lane_5, line[5]);} 
    if(valid[6]){ write_channel_intel(ch_out_lane_6, line[6]);} 
    if(valid[7]){ write_channel_intel(ch_out_lane_7, line[7]);} 
  }
}
#endif

/* ----- End of ring_rx -----  */

/* ----- ring_tx -----  */
#ifdef USE_DP_LAYER
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void validate_channel()
{
  union param_validate_channel reg;
  for (int i = 0; i < NOF_LANES; i++){
    reg.parameters[i].transport_nof_hops = REMOVE_CHANNEL;
  }

  bool discard[NOF_LANES] = {0};
  while(1){
    // handle MM read/write requests
    handle_rw_mm_request(CH_VALIDATE_CHANNEL, reg.arr);
    // Do someting with parameters
    bool valid[NOF_LANES];
    struct line_dp line[NOF_LANES];
    line[0] = read_channel_nb_intel(ch_in_lane_0, &valid[0]); 
    line[1] = read_channel_nb_intel(ch_in_lane_1, &valid[1]); 
    line[2] = read_channel_nb_intel(ch_in_lane_2, &valid[2]); 
    line[3] = read_channel_nb_intel(ch_in_lane_3, &valid[3]); 
    line[4] = read_channel_nb_intel(ch_in_lane_4, &valid[4]); 
    line[5] = read_channel_nb_intel(ch_in_lane_5, &valid[5]); 
    line[6] = read_channel_nb_intel(ch_in_lane_6, &valid[6]); 
    line[7] = read_channel_nb_intel(ch_in_lane_7, &valid[7]); 

    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++){
      if (valid[i]){
        if((line[i].flags & FLAG_FIRST) == FLAG_FIRST) 
          discard[i] = (line[i].dp_channel == reg.parameters[i].transport_nof_hops);
        if (!discard[i])
          write_channel_intel(tx_validated_channels[i], line[i]);
      }
    }

    if(valid[0] && !discard[0]){ write_channel_intel(ch_out_tx_monitor_0, line[0]);}
    if(valid[1] && !discard[1]){ write_channel_intel(ch_out_tx_monitor_1, line[1]);}
    if(valid[2] && !discard[2]){ write_channel_intel(ch_out_tx_monitor_2, line[2]);}
    if(valid[3] && !discard[3]){ write_channel_intel(ch_out_tx_monitor_3, line[3]);}
    if(valid[4] && !discard[4]){ write_channel_intel(ch_out_tx_monitor_4, line[4]);}
    if(valid[5] && !discard[5]){ write_channel_intel(ch_out_tx_monitor_5, line[5]);}
    if(valid[6] && !discard[6]){ write_channel_intel(ch_out_tx_monitor_6, line[6]);}
    if(valid[7] && !discard[7]){ write_channel_intel(ch_out_tx_monitor_7, line[7]);}
    
  }
}
#else
__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void no_validate_channel()
{
  uint no_param_arr[8] = [~0, ~0, ~0, ~0, ~0, ~0, ~0, ~0];
  while(1){
    // handle MM read/write requests
    handle_ro_mm_request(CH_VALIDATE_CHANNEL, no_param_arr);

    bool valid[NOF_LANES];
    struct line_dp line[NOF_LANES];
    line[0] = read_channel_nb_intel(ch_in_lane_0, &valid[0]); 
    line[1] = read_channel_nb_intel(ch_in_lane_1, &valid[1]); 
    line[2] = read_channel_nb_intel(ch_in_lane_2, &valid[2]); 
    line[3] = read_channel_nb_intel(ch_in_lane_3, &valid[3]); 
    line[4] = read_channel_nb_intel(ch_in_lane_4, &valid[4]); 
    line[5] = read_channel_nb_intel(ch_in_lane_5, &valid[5]); 
    line[6] = read_channel_nb_intel(ch_in_lane_6, &valid[6]); 
    line[7] = read_channel_nb_intel(ch_in_lane_7, &valid[7]); 

    #pragma unroll
    for (int i = 0; i < NOF_LANES; i++){
      if (valid[i])
        write_channel_intel(tx_validated_channels[i], line[i]);
    }

    if(valid[0]){ write_channel_intel(ch_out_tx_monitor_0, line[0]);}
    if(valid[1]){ write_channel_intel(ch_out_tx_monitor_1, line[1]);}
    if(valid[2]){ write_channel_intel(ch_out_tx_monitor_2, line[2]);}
    if(valid[3]){ write_channel_intel(ch_out_tx_monitor_3, line[3]);}
    if(valid[4]){ write_channel_intel(ch_out_tx_monitor_4, line[4]);}
    if(valid[5]){ write_channel_intel(ch_out_tx_monitor_5, line[5]);}
    if(valid[6]){ write_channel_intel(ch_out_tx_monitor_6, line[6]);}
    if(valid[7]){ write_channel_intel(ch_out_tx_monitor_7, line[7]);}
    
  }
}
#endif

// TODO: make sure the latency is low.
__attribute__((num_compute_units(NOF_LANES), max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void tx_encode()
{
  const int laneIndex = get_compute_id(0);
  while(1){
    struct line_10GbE output_10GbE; 
    struct line_dp input_dp;
    uint64_t dp_sync_and_bsn = 0;
    ushort dp_channel = 0;
    for (int j = 0; j < BLOCK_LENGTH; j++){

#ifdef USE_DP_LAYER
      if(j == 0 || (j > DP_HEADER_SIZE)){
#else
      if(j == 0 || (j > ETH_HEADER_SIZE)){
#endif
        input_dp = read_channel_intel(tx_validated_channels[laneIndex]);
      }
      output_10GbE.flags = 0;
      output_10GbE.err = 0;

      switch(j)
      {
        case 0:
#ifdef USE_DP_LAYER
          dp_channel = input_dp.dp_channel + 1; //Add 1 hop.
          if ((input_dp.flags & FLAG_SYNC)==FLAG_SYNC){
            dp_sync_and_bsn = (input_dp.dp_bsn | MASK_SYNC); // set bsn and sync
          }
          else{
            dp_sync_and_bsn = (input_dp.dp_bsn & MASK_BSN); // set bsn and clear sync (if set)
          }
#endif              
          output_10GbE.flags = FLAG_FIRST; 
          output_10GbE.data = c_header_out[0];
          break;

        case 1:      
#ifdef USE_DP_LAYER
          output_10GbE.data = (c_header_out[1] | ((uint64_t) dp_channel));
#else
          output_10GbE.data = c_header_out[1];
#endif
          break;
      
#ifdef USE_DP_LAYER
        case 2:
          output_10GbE.data = dp_sync_and_bsn;
          break;
#endif

        case (BLOCK_LENGTH-1):
          output_10GbE.flags = FLAG_LAST;
          // no break, we also want to execute the default case. 

        default:
          output_10GbE.data = input_dp.data;
      }
      
      write_channel_intel(tx_sosi_channels[laneIndex], output_10GbE);
    }
  }
}



__attribute__((max_global_work_dim(0)))
__kernel void dummy()
{
}

