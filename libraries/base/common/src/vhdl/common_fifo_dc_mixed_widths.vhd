-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Dual clock - dual width FIFO to exchange clock rate and data width
-- Description:
-- . For g_wr_dat_w=8 and g_rd_dat_w=16 the read result data for writing
--   counter data is:
--     wr_dat:  00, 01, 02, 03, ...
--     rd_dat:          0100,  0302, ...
--   Hence there is only read data available when the whole rd_dat word has
--   been filled by the wr_dat words. Also the read empty remains active until
--   the whole rd_dat word has been filled.
-- . For g_wr_dat_w=8 and g_rd_dat_w=4 the read result data for writing
--   counter data is:
--     wr_dat:  00,    01,    02,    03, ...
--     rd_dat:     0,0,   1,0,   2,0,   3,0, ...
-- Remark:
-- . The MegaWizard dual width FIFO is only available as dual clock FIFO.
--
-- Warning !!!
-- . It appears that the stratix4 architecture does not work OK when g_rd_dat_w
--   is not a power of 2 multiple of g_wr_dat_w. After that g_nof_words have
--   been written to the FIFO the rdusedw will wrap and the output goes wrong.

library IEEE, technology_lib, tech_fifo_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_fifo_dc_mixed_widths is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_note_is_ful : boolean := true;  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
    g_fail_rd_emp : boolean := false;  -- when TRUE report FAILURE when read from an empty FIFO
    g_nof_words   : natural := c_bram_m9k_fifo_depth;  -- FIFO size in nof wr_dat words
    g_wr_dat_w    : natural := 36;  -- 36 * 256 = 1 M9K
    g_rd_dat_w    : natural := 9
  );
  port (
    rst     : in  std_logic;
    wr_clk  : in  std_logic;
    wr_dat  : in  std_logic_vector(g_wr_dat_w - 1 downto 0);
    wr_req  : in  std_logic;
    wr_ful  : out std_logic;
    wrusedw : out std_logic_vector(ceil_log2(g_nof_words) - 1 downto 0);
    rd_clk  : in  std_logic;
    rd_dat  : out std_logic_vector(g_rd_dat_w - 1 downto 0);
    rd_req  : in  std_logic;
    rd_emp  : out std_logic;
    rdusedw : out std_logic_vector(ceil_log2(g_nof_words * g_wr_dat_w / g_rd_dat_w) - 1 downto 0);
    rd_val  : out std_logic
  );
end common_fifo_dc_mixed_widths;

architecture str of common_fifo_dc_mixed_widths is
  constant c_nof_words  : natural := 2**ceil_log2(g_nof_words);  -- ensure size is power of 2 for dual clock FIFO

  signal wr_rst  : std_logic;
  signal wr_init : std_logic;
  signal wr_en   : std_logic;
  signal rd_en   : std_logic;
  signal ful     : std_logic;
  signal emp     : std_logic;

  signal nxt_rd_val : std_logic;
begin
  -- Control logic copied from LOFAR common_fifo_dc(virtex4).vhd

  -- Need to make sure the reset lasts at least 3 cycles (see fifo_generator_ug175.pdf).
  -- Wait at least 4 cycles after reset release before allowing FIFO wr_en (see fifo_generator_ug175.pdf)

  -- Use common_areset to:
  -- . asynchronously detect rst even when the wr_clk is stopped
  -- . synchronize release of rst to wr_clk domain
  -- Using common_areset is equivalent to using common_async with same signal applied to rst and din.
  u_wr_rst : entity work.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => 3,
    g_tree_len  => 0  -- assume if necessary g_tree_len > 0 is covered via input rst
  )
  port map (
    in_rst    => rst,
    clk       => wr_clk,
    out_rst   => wr_rst
  );

  -- Delay wr_init to ensure that FIFO ful has gone low after reset release
  u_wr_init : entity work.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => 4,
    g_tree_len  => 0
  )
  port map (
    in_rst  => wr_rst,
    clk     => wr_clk,
    out_rst => wr_init  -- assume init has finished g_delay_len cycles after release of wr_rst
  );

  -- The FIFO under read and over write protection are kept enabled in the MegaWizard
  wr_en <= wr_req and not wr_init;  -- check on NOT ful is not necessary according to fifo_generator_ug175.pdf
  rd_en <= rd_req;  -- check on NOT emp is not necessary

  nxt_rd_val <= rd_req and not emp;  -- check on NOT emp is necessary for rd_val

  wr_ful <= ful;
  rd_emp <= emp;

  p_rd_clk : process(rd_clk)
  begin
    if rising_edge(rd_clk) then
      rd_val <= nxt_rd_val;
    end if;
  end process;

  u_fifo : entity tech_fifo_lib.tech_fifo_dc_mixed_widths
  generic map (
    g_technology => g_technology,
    g_nof_words  => c_nof_words,
    g_wrdat_w    => g_wr_dat_w,
    g_rddat_w    => g_rd_dat_w
  )
  port map (
    aclr    => wr_rst,  -- MegaWizard fifo_dc_mixed_widths seems to use aclr synchronous with wr_clk
    data    => wr_dat,
    rdclk   => rd_clk,
    rdreq   => rd_en,
    wrclk   => wr_clk,
    wrreq   => wr_en,
    q       => rd_dat,
    rdempty => emp,
    rdusedw => rdusedw,
    wrfull  => ful,
    wrusedw => wrusedw
  );

  proc_common_fifo_asserts("common_fifo_dc_mixed_widths", g_note_is_ful, g_fail_rd_emp, wr_rst, wr_clk, ful, wr_en, rd_clk, emp, rd_en);
end str;
