-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify dp_sync_insert_v2
-- Description: The tb verifies:
--  . data valid gaps between blocks
--  . data valid gaps within blocks
--  . output sop, eop, valid, bsn equal to input
--  . expected output sync consisting of input syncs and inserted syncs
-- Usage:
-- > as 8
-- > run -all
--
-- * The tb is self stopping because tb_end will stop the simulation by
--   stopping the clk and thus all toggling.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_sync_insert_v2 is
  generic (
    g_nof_streams            : natural := 2;
    g_block_size_input       : natural := 16;
    g_nof_blk_per_sync_input : natural := 32;
    g_gap_size_during_block  : natural := 0;
    g_gap_size_between_block : natural := 0;
    g_nof_blk_per_sync       : natural := 8;
    g_nof_blk_per_sync_min   : natural := 2;
    g_nof_repeat             : natural := 14
  );
end tb_dp_sync_insert_v2;

architecture tb of tb_dp_sync_insert_v2 is
  constant c_dut_latency         : natural := 1;
  constant c_nof_replicated_sync : natural := g_nof_blk_per_sync_input / g_nof_blk_per_sync;

  constant c_dp_clk_period       : time := 5 ns;
  constant c_mm_clk_period       : time := 8 ns;

  signal tb_end                    : std_logic := '0';
  signal mm_clk                    : std_logic := '1';
  signal dp_clk                    : std_logic := '1';
  signal rst                       : std_logic := '1';

  -- DUT
  signal ref_sosi                  : t_dp_sosi := c_dp_sosi_rst;
  signal ref_sosi_arr              : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_sosi                  : t_dp_sosi;
  signal out_sosi_arr              : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal reg_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso   : t_mem_miso := c_mem_miso_rst;

  -- Verification
  signal dly_valid_arr             : std_logic_vector(0 to c_dut_latency) := (others => '0');
  signal dly_ref_sosi_arr          : t_dp_sosi_arr(0 to c_dut_latency) := (others => c_dp_sosi_rst);
  signal exp_sync                  : std_logic := '0';
  signal out_hold_sop              : std_logic := '0';
  signal exp_size                  : natural := g_block_size_input;
  signal cnt_size_arr              : t_natural_arr(0 to g_nof_streams - 1);
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- STIMULI
  ------------------------------------------------------------------------------

  p_stimuli : process
  begin
    proc_common_wait_until_low(dp_clk, rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    for I in 0 to g_nof_repeat - 1 loop
      -- Generate first block with sync
      ref_sosi.sync  <= '1';
      ref_sosi.sop   <= '1';
      ref_sosi.valid <= '1';
      ref_sosi.bsn   <= TO_DP_BSN(23 + I);
      proc_common_wait_some_cycles(dp_clk, 1);
      ref_sosi.sync  <= '0';
      ref_sosi.sop   <= '0';

      -- Optionally apply valid='0' during block of data
      ref_sosi.valid <= '0';
      proc_common_wait_some_cycles(dp_clk, g_gap_size_during_block);
      ref_sosi.valid <= '1';

      proc_common_wait_some_cycles(dp_clk, g_block_size_input - 2);
      ref_sosi.eop   <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      ref_sosi.eop   <= '0';
      ref_sosi.valid <= '0';

      -- Optionally apply valid='0' between block of data
      proc_common_wait_some_cycles(dp_clk, g_gap_size_between_block);

      -- Generate next blocks after sync
      for I in 0 to g_nof_blk_per_sync_input - 2 loop
        ref_sosi.sop   <= '1';
        ref_sosi.valid <= '1';
        proc_common_wait_some_cycles(dp_clk, 1);
        ref_sosi.sync  <= '0';
        ref_sosi.sop   <= '0';

        -- Optionally apply valid='0' during block of data
        ref_sosi.valid <= '0';
        proc_common_wait_some_cycles(dp_clk, g_gap_size_during_block);
        ref_sosi.valid <= '1';

        proc_common_wait_some_cycles(dp_clk, g_block_size_input - 2);
        ref_sosi.eop   <= '1';
        proc_common_wait_some_cycles(dp_clk, 1);
        ref_sosi.eop   <= '0';
        ref_sosi.valid <= '0';

        -- Optionally apply valid='0' between block of data
        proc_common_wait_some_cycles(dp_clk, g_gap_size_between_block);
      end loop;
    end loop;

    -- End of stimuli
    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  ref_sosi.data  <= INCR_UVEC(ref_sosi.data, 1) when rising_edge(dp_clk);
  ref_sosi.re    <= INCR_UVEC(ref_sosi.re,   2) when rising_edge(dp_clk);
  ref_sosi.im    <= INCR_UVEC(ref_sosi.im,   3) when rising_edge(dp_clk);

  gen_streams : for I in 0 to g_nof_streams - 1 generate
    ref_sosi_arr(I) <= ref_sosi;
  end generate;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut: entity work.dp_sync_insert_v2
  generic map (
    g_nof_streams          => g_nof_streams,
    g_nof_blk_per_sync     => g_nof_blk_per_sync,
    g_nof_blk_per_sync_min => g_nof_blk_per_sync_min
  )
  port map (
    mm_rst        => rst,
    mm_clk        => mm_clk,
    dp_rst        => rst,
    dp_clk        => dp_clk,

    reg_mosi      => reg_mosi,
    reg_miso      => reg_miso,

    -- Streaming sink
    in_sosi_arr   => ref_sosi_arr,
    -- Streaming source
    out_sosi_arr  => out_sosi_arr
  );

  ------------------------------------------------------------------------------
  -- Verification
  -- . use some DUT specific verification
  -- . use some general Verification means from tb_dp_pkg.vhd, dp_stream_verify.vhd
  ------------------------------------------------------------------------------
  dly_ref_sosi_arr(0)                  <= ref_sosi;
  dly_ref_sosi_arr(1 to c_dut_latency) <= dly_ref_sosi_arr(0 to c_dut_latency - 1) when rising_edge(dp_clk);

  p_exp_sync : process(dp_clk)
    variable blk_cnt : natural := 0;
  begin
    if rising_edge(dp_clk) then
      exp_sync <= '0';
      if dly_ref_sosi_arr(c_dut_latency - 1).sop = '1' then
        if dly_ref_sosi_arr(c_dut_latency - 1).sync = '1' or blk_cnt >= g_nof_blk_per_sync - 1 then
          blk_cnt := 0;
          exp_sync <= '1';
        else
          blk_cnt := blk_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  p_verify_out_sosi : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      for I in 0 to g_nof_streams - 1 loop
        assert out_sosi_arr(I).valid = dly_ref_sosi_arr(c_dut_latency).valid
          report "Wrong out_sosi.valid"
          severity ERROR;
        assert out_sosi_arr(I).sop = dly_ref_sosi_arr(c_dut_latency).sop
          report "Wrong out_sosi.sop"
          severity ERROR;
        assert out_sosi_arr(I).eop = dly_ref_sosi_arr(c_dut_latency).eop
          report "Wrong out_sosi.eop"
          severity ERROR;
        assert out_sosi_arr(I).bsn = dly_ref_sosi_arr(c_dut_latency).bsn
          report "Wrong out_sosi.bsn"
          severity ERROR;
        assert out_sosi_arr(I).sync = exp_sync
          report "Wrong out_sosi.sync"
          severity ERROR;
      end loop;
    end if;
  end process;

  -- Verify output packet ctrl
  gen_verify_ctrl : for I in 0 to g_nof_streams - 1 generate
    proc_dp_verify_sop_and_eop(dp_clk, out_sosi_arr(I).valid, out_sosi_arr(I).sop, out_sosi_arr(I).eop, out_hold_sop);
    -- Verify output packet block size
    proc_dp_verify_block_size(exp_size, dp_clk, out_sosi_arr(I).valid, out_sosi_arr(I).sop, out_sosi_arr(I).eop, cnt_size_arr(I));
  end generate;
end tb;
