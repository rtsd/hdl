-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author : E. Kooistra, 27 jul 2017
-- Purpose : Control force data in a serial stream via MM
-- Description:
--   Address   Data  Access  Description
--     0        [0]     R/W  force enable or default disable for data pass on
--     0        [1]     R/W  force value when '1' else data pass on when at index
--     0        [2]     R/W  force zero when '0' else data pass on when not at index
--     1     [31:0]     R/W  force index range 0:g_index_period-1
--     2     [31:0]     R/W  force value used for sosi data
--     3     [31:0]     R/W  force value used for sosi re
--     4     [31:0]     R/W  force value used for sosi im
--
--   The actual sosi data, re and im width integer is constrained by g_dat_w.
--   Synthesis will optimize away unused bits from the full integer 32b range.
--
--   See description of dp_force_data_serial.vhd.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_force_data_serial is
  generic (
    g_dat_w                : natural := 32;  -- number of bits per force data value, must be <= 32 to fit INTEGER range
    g_index_period         : natural := 10;  -- number of indices in time, must be <= 2*31 to fit in NATURAL range
    g_index_sample_block_n : boolean := false  -- when TRUE sample index in block, else block index in sync interval
  );
  port (
    -- Clocks and reset
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;
    -- MM control
    reg_force_data_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_force_data_miso : out t_mem_miso;
    -- ST sink
    snk_out             : out t_dp_siso;
    snk_in              : in  t_dp_sosi;
    -- ST source
    src_in              : in  t_dp_siso := c_dp_siso_rdy;
    src_out             : out t_dp_sosi
  );
end mms_dp_force_data_serial;

architecture str of mms_dp_force_data_serial is
  constant c_index_w        : natural := ceil_log2(g_index_period);

  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_reg         : t_c_mem := (1, 3, c_word_w, 5, 'X');  -- use c_word_w = 32 to fit both g_dat_w data values and force_index

  constant c_mm_reg_init    : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0');

  signal reg_force_data_wr  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0);
  signal reg_force_data_rd  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');

  signal force_en           : std_logic := '0';
  signal force_value        : std_logic := '0';
  signal force_zero_n       : std_logic := '0';  -- use _n to preserve backwards compatibility when force zero MM control was not supported yet
  signal force_index        : natural := 0;
  signal force_data         : integer := 0;  -- used for sosi.data
  signal force_re           : integer := 0;  -- used for sosi.re
  signal force_im           : integer := 0;  -- used for sosi.im
begin
  -- write wires
  force_en    <= reg_force_data_wr(0);
  force_value <= reg_force_data_wr(1);
  force_zero_n <= reg_force_data_wr(2);
  force_index <= TO_UINT(reg_force_data_wr(2 * c_mm_reg.dat_w - 1 downto 1 * c_mm_reg.dat_w));
  force_data  <= TO_SINT(reg_force_data_wr(3 * c_mm_reg.dat_w - 1 downto 2 * c_mm_reg.dat_w));
  force_re    <= TO_SINT(reg_force_data_wr(4 * c_mm_reg.dat_w - 1 downto 3 * c_mm_reg.dat_w));
  force_im    <= TO_SINT(reg_force_data_wr(5 * c_mm_reg.dat_w - 1 downto 4 * c_mm_reg.dat_w));

  -- read wires
  p_readback : process(reg_force_data_wr)
  begin
    -- default readback what was written
    reg_force_data_rd <= reg_force_data_wr;
    -- read unused bits as '0' to save logic
    reg_force_data_rd(1 * c_mm_reg.dat_w - 1 downto         2 + 0 * c_mm_reg.dat_w) <= (others => '0');
    if c_index_w < c_mm_reg.dat_w then
    reg_force_data_rd(2 * c_mm_reg.dat_w - 1 downto c_index_w + 1 * c_mm_reg.dat_w) <= (others => '0');
    end if;
    if g_dat_w < c_mm_reg.dat_w then
    reg_force_data_rd(3 * c_mm_reg.dat_w - 1 downto   g_dat_w + 2 * c_mm_reg.dat_w) <= (others => '0');
    reg_force_data_rd(4 * c_mm_reg.dat_w - 1 downto   g_dat_w + 3 * c_mm_reg.dat_w) <= (others => '0');
    reg_force_data_rd(5 * c_mm_reg.dat_w - 1 downto   g_dat_w + 4 * c_mm_reg.dat_w) <= (others => '0');
    end if;
  end process;

  u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_mm_reg,
    g_init_reg           => c_mm_reg_init
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_force_data_mosi,
    sla_out        => reg_force_data_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    out_reg        => reg_force_data_wr,  -- readback via ST clock domain
    in_reg         => reg_force_data_rd
  );

  u_dp_force_data_serial : entity work.dp_force_data_serial
  generic map (
    g_dat_w                => g_dat_w,
    g_index_period         => g_index_period,
    g_index_sample_block_n => g_index_sample_block_n
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    -- MM control
    force_en      => force_en,
    force_value   => force_value,
    force_zero_n  => force_zero_n,
    force_data    => force_data,
    force_re      => force_re,
    force_im      => force_im,
    force_index   => force_index,
    -- ST sink
    snk_out       => snk_out,
    snk_in        => snk_in,
    -- ST source
    src_in        => src_in,
    src_out       => src_out
  );
end str;
