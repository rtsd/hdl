-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author : R. van der Walle
-- Purpose:
--   Wrapper for Lofar2 SDP Station beamformer design
-- Description:
--   Unb2c version for lab testing
--   Contains complete AIT input stage with 12 ADC streams, FSUB and BF

library IEEE, common_lib, unb2c_board_lib, diag_lib, dp_lib, tech_jesd204b_lib, lofar2_unb2c_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity lofar2_unb2c_sdp_station_bf is
  generic (
    g_design_name      : string  := "lofar2_unb2c_sdp_station_bf";
    g_design_note      : string  := "Lofar2 SDP station beamformer design";
    g_sim              : boolean := false;  -- Overridden by TB
    g_sim_unb_nr       : natural := 0;
    g_sim_node_nr      : natural := 0;
    g_stamp_date       : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time       : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id      : string := ""  -- revision ID     -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGIN     : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

    -- front transceivers QSFP0 for Ring.
    QSFP_0_RX     : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX     : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- front transceivers
    QSFP_1_RX     : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX     : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');  -- Using qsfp bus width also for ring interfaces
    RING_0_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

     -- back transceivers (note only 12 are used in unb2c)
    BCK_RX       : in    std_logic_vector(c_unb2c_board_nof_tr_jesd204b - 1 downto 0);  -- c_unb2c_board_nof_tr_jesd204b = c_sdp_S_pn = 12
    BCK_REF_CLK  : in    std_logic;  -- Use as JESD204B_REFCLK

    -- jesd204b syncronization signals (4 syncs)
    JESD204B_SYSREF : in    std_logic;
    JESD204B_SYNC_N : out   std_logic_vector(c_unb2c_board_nof_sync_jesd204b - 1 downto 0)  -- c_unb2c_board_nof_sync_jesd204b = c_sdp_N_sync_jesd = 4
  );
end lofar2_unb2c_sdp_station_bf;

architecture str of lofar2_unb2c_sdp_station_bf is
  signal JESD204B_SERIAL_DATA       : std_logic_vector((c_unb2c_board_tr_jesd204b.bus_w * c_unb2c_board_tr_jesd204b.nof_bus) - 1 downto 0);
  signal jesd204b_sync_n_arr        : std_logic_vector(c_unb2c_board_nof_sync_jesd204b - 1 downto 0);
  signal JESD204B_REFCLK            : std_logic;
begin
  -- Mapping between JESD signal names and UNB2B pin/schematic names
  JESD204B_REFCLK      <= BCK_REF_CLK;
  JESD204B_SERIAL_DATA <= BCK_RX;
  JESD204B_SYNC_N(c_unb2c_board_nof_sync_jesd204b - 1 downto 0) <= jesd204b_sync_n_arr(c_unb2c_board_nof_sync_jesd204b - 1 downto 0);

  u_revision : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
  generic map (
    g_design_name => g_design_name,
    g_design_note => g_design_note,
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_revision_id => g_revision_id
  )
  port map (
    -- GENERAL
    CLK          => CLK,
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- 1GbE Control Interface
    ETH_clk      => ETH_clk,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,

    -- front transceivers QSFP0 for Ring.
    QSFP_0_RX    => QSFP_0_RX,
    QSFP_0_TX    => QSFP_0_TX,

    -- front transceivers QSFP1 for 10GbE output to CEP.
    QSFP_1_RX    => QSFP_1_RX,
    QSFP_1_TX    => QSFP_1_TX,
    -- LEDs
    QSFP_LED     => QSFP_LED,

    -- ring transceivers
    RING_0_RX    => RING_0_RX,
    RING_0_TX    => RING_0_TX,
    RING_1_RX    => RING_1_RX,
    RING_1_TX    => RING_1_TX,

    -- back transceivers
    JESD204B_SERIAL_DATA   => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK        => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF        => JESD204B_SYSREF,
    JESD204B_SYNC_N        => jesd204b_sync_n_arr
  );
end str;
