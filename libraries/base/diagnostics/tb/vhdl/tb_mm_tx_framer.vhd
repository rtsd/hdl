-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_mm_tx_framer is
end tb_mm_tx_framer;

architecture tb of tb_mm_tx_framer is
  constant c_mm_clk_period               : time := 8 ns;  -- 125 MHz MM clock, altpll in SOPC will make this from the ETH 25 MHz XO clock
  constant c_tx_clk_period               : time := 6.4 ns;  -- 156.25 MHz

  signal tx_rst                          : std_logic := '1';
  signal tx_clk                          : std_logic := '0';

  signal mm_clk                          : std_logic := '0';
  signal mm_rst                          : std_logic := '1';

  signal ctrl_mosi                      : t_mem_mosi;
  signal ctrl_miso                      : t_mem_miso;

  signal mst_data_mosi                      : t_mem_mosi;
  signal mst_data_miso                      : t_mem_miso;

  signal sla_data_mosi                      : t_mem_mosi;
  signal sla_data_miso                      : t_mem_miso;

  -- Register offsets:
  constant c_ofs_ctrl_availw            : natural := 0;  -- RD
  constant c_ofs_ctrl_release           : natural := 1;  -- WR
  constant c_ofs_data                   : natural := 0;  -- WR

  signal  mst_data_out                      : std_logic_vector(63 downto 0);
  signal  sla_data_out                      : std_logic_vector(31 downto 0);

  signal mst_release : std_logic;
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  tx_rst <= '0' after 200 ns;
  mm_rst <= '0' after 700 ns;

  tx_clk      <= not tx_clk after c_tx_clk_period / 2;
  mm_clk      <= not mm_clk after c_mm_clk_period / 2;  -- MM clock (125 MHz)

  -------------------------------------------------------------------------------------
  -- PROCESS: Assemble 2 frames of equal length and assert 'release'
  -------------------------------------------------------------------------------------
  p_in_stimuli : process
  begin
    ctrl_mosi <= c_mem_mosi_rst;

    wait until mm_rst = '0';
    proc_common_wait_some_cycles(mm_clk, 10);

    -- MASTER's DATA FRAME:
    -- ====================

    -- 0xDEADBEEFCAFEBABE will be our 64-bit start word
    proc_mem_mm_bus_wr(c_ofs_data, x"DEADBEEF", mm_clk, mst_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    proc_mem_mm_bus_wr(c_ofs_data, x"CAFEBABE", mm_clk, mst_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    -- Add some counter data to the frame
    for i in 0 to 49 loop
      proc_mem_mm_bus_wr(c_ofs_data, i, mm_clk, mst_data_mosi);  -- MS portion, 0 - 49
      proc_common_wait_some_cycles(mm_clk, 4);

      proc_mem_mm_bus_wr(c_ofs_data, i + 100, mm_clk, mst_data_mosi);  -- LS portion, 100 - 149
      proc_common_wait_some_cycles(mm_clk, 4);
    end loop;

    -- 0xDEADBEEFCAFEBABE again to end with.
    proc_mem_mm_bus_wr(c_ofs_data, x"DEADBEEF", mm_clk, mst_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    proc_mem_mm_bus_wr(c_ofs_data, x"CAFEBABE", mm_clk, mst_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    -- SLAVE's DATA FRAME:
    -- ====================

    -- 0xAABBCCDDEE will be our 32-bit start word
    proc_mem_mm_bus_wr(c_ofs_data, x"AABBCCDD", mm_clk, sla_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    -- Add some counter data to the frame
    for i in 200 to 249 loop
      proc_mem_mm_bus_wr(c_ofs_data, i, mm_clk, sla_data_mosi);
      proc_common_wait_some_cycles(mm_clk, 4);
    end loop;

    -- 0xFFFFFFFF to end with.
    proc_mem_mm_bus_wr(c_ofs_data, x"FFFFFFFF", mm_clk, sla_data_mosi);
    proc_common_wait_some_cycles(mm_clk, 4);

    -- Frames are set up, now release them onto their buses in sync.
    proc_mem_mm_bus_wr(c_ofs_ctrl_release, 1, mm_clk, ctrl_mosi);

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUTS: Two framers, one master, one slave. The master is MM controlled, its
  -- release signal also connected to the slave so they release their frames
  -- simultaneously.
  ------------------------------------------------------------------------------
  u_mm_tx_framer_mst: entity work.mm_tx_framer
  generic map(
    g_dat_out_w  => 64
  )
  port map(
    tx_rst       => tx_rst,
    tx_clk       => tx_clk,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    ctrl_mosi    => ctrl_mosi,
    ctrl_miso    => ctrl_miso,

    data_mosi    => mst_data_mosi,
    data_miso    => mst_data_miso,

    data_out     => mst_data_out,
    master_release => mst_release
  );

  u_mm_tx_framer_sla: entity work.mm_tx_framer
  generic map(
    g_dat_out_w  => 32
  )
  port map(
    tx_rst       => tx_rst,
    tx_clk       => tx_clk,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    data_mosi    => sla_data_mosi,
    data_miso    => sla_data_miso,

    data_out     => sla_data_out,
    slave_release => mst_release
  );
end tb;
