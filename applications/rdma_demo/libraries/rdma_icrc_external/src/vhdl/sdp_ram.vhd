-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Wrapper for a simple dual port RAM

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
library common_lib;
    use common_lib.common_mem_pkg.all;

entity sdp_ram is
    generic (
        g_REG_OUTPUT : boolean := false;
        g_INIT_FILE  : string  := "UNUSED"
    );
    port (
        i_clk       : in std_logic;
        i_clk_reset : in std_logic;
        i_wr_addr   : in unsigned;
        i_wr_en     : in std_logic;
        i_wr_data   : in std_logic_vector;
        i_rd_addr   : in unsigned;
        i_rd_en     : in std_logic;
        o_rd_data   : out std_logic_vector
    );
end entity sdp_ram;

architecture str of sdp_ram is

  constant c_ram : t_c_mem := (c_mem_ram_rd_latency, i_wr_addr'length,  i_wr_data'length, 2**i_wr_addr'length, '0');

begin
  u_common_ram_r_w : entity common_lib.common_ram_r_w
  generic map (
    g_ram => c_ram,
    g_init_file => g_INIT_FILE
  )
  port map (
    rst    => i_clk_reset,
    clk    => i_clk,
    wr_en  => i_wr_en,
    wr_adr => std_logic_vector(i_wr_addr),
    wr_dat => i_wr_data,
    rd_en  => i_rd_en,
    rd_adr => std_logic_vector(i_rd_addr),
    rd_dat => o_rd_data
  );
end str;
