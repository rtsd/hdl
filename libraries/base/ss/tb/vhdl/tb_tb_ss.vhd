-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_ss is
end tb_tb_ss;

architecture tb of tb_tb_ss is
  constant c_nof_sync       : natural := 20;
  constant c_reverse_ss_map : boolean := true;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 2
  -- > run -all                 --> OK

  -- g_mode_in_en            : NATURAL := 0;  -- 0 = active, 1 = random in_sosi.valid control
  -- g_mode_out_ready        : NATURAL := 0;  -- 0 = active, 1 = toggling, 2 = inverted toggling for out_siso.ready control, toggling requires g_nof_ch_sel <= g_nof_ch_in/2
  -- g_nof_sync              : NATURAL := 100;
  -- g_reverse_ss_map        : BOOLEAN := FALSE;
  -- g_nof_ch_in             : NATURAL := 32;
  -- g_nof_ch_sel            : NATURAL := 31;
  -- g_use_output_rl_adapter : BOOLEAN := FALSE;  -- when true adapt output RL to 1 else the output RL is equal to c_retrieve_lat=2

  u_act_act_32_32              : entity work.tb_ss generic map (0, 0, c_nof_sync, c_reverse_ss_map, 32, 32, false);
  u_act_act_32_24              : entity work.tb_ss generic map (0, 0, c_nof_sync, c_reverse_ss_map, 32, 24, false);
  u_rnd_act_32_32_rl           : entity work.tb_ss generic map (1, 0, c_nof_sync, c_reverse_ss_map, 32, 32, true);
  u_rnd_act_32_32              : entity work.tb_ss generic map (1, 0, c_nof_sync, c_reverse_ss_map, 32, 32, false);
  u_rnd_act_32_24              : entity work.tb_ss generic map (1, 0, c_nof_sync, c_reverse_ss_map, 32, 24, false);

  u_act_act_32_31              : entity work.tb_ss generic map (0, 0, c_nof_sync, c_reverse_ss_map, 32, 31, false);
  u_rnd_act_32_31              : entity work.tb_ss generic map (1, 0, c_nof_sync, c_reverse_ss_map, 32, 31, false);

  u_act_toggle_32_16           : entity work.tb_ss generic map (0, 1, c_nof_sync, c_reverse_ss_map, 32, 16, false);
  u_rnd_toggle_32_16           : entity work.tb_ss generic map (1, 1, c_nof_sync, c_reverse_ss_map, 32, 16, false);

  u_act_toggle_32_15           : entity work.tb_ss generic map (0, 1, c_nof_sync, c_reverse_ss_map, 32, 15, false);
  u_rnd_toggle_32_15           : entity work.tb_ss generic map (0, 1, c_nof_sync, c_reverse_ss_map, 32, 15, false);

  u_rnd_toggle_32_15_rl        : entity work.tb_ss generic map (0, 1, c_nof_sync, c_reverse_ss_map, 32, 15, true);

  u_act_invert_toggle_32_16    : entity work.tb_ss generic map (0, 2, c_nof_sync, c_reverse_ss_map, 32, 16, false);
  u_rnd_invert_toggle_32_16    : entity work.tb_ss generic map (1, 2, c_nof_sync, c_reverse_ss_map, 32, 16, false);
end tb;
