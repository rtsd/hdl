-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Tristate buffer

entity common_inout is
  port (
    dat_inout        : inout std_logic;
    dat_in_from_line : out   std_logic;
    dat_out_to_line  : in    std_logic := '0';  -- default drive INOUT low when output enabled
    dat_out_en       : in    std_logic := '0'  -- output enable, default use INOUT as tristate input
  );
end common_inout;

architecture rtl of common_inout is
begin
  dat_inout <= 'Z' when dat_out_en = '0' else dat_out_to_line;

  dat_in_from_line <= not (not dat_inout);  -- do via NOT(NOT) for simulation to force 'H' -> '1' and 'L' --> '0'
end rtl;
