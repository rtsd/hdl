-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose : Monitor signal path statistics
-- Description :
--   For one input signal path provide MM access to:
--   . ADC mean  via reg_miso
--   . ADC power via reg_miso
--   . ADC data buffer via buf_miso
-- Remarks:

entity mms_aduh_monitor is
  generic (
    g_cross_clock_domain   : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_symbol_w             : natural := 8;
    g_nof_symbols_per_data : natural := 4;  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    : natural := 800 * 10**6;  -- integration time in symbols, defines internal accumulator widths
    g_buffer_nof_symbols   : natural := 1024;
    g_buffer_use_sync      : boolean := false  -- when TRUE start filling the buffer after the in_sync, else after the last word was read
  );
  port (
    -- Memory-mapped clock domain
    mm_rst     : in  std_logic;
    mm_clk     : in  std_logic;

    reg_mosi   : in  t_mem_mosi;  -- read only access to the mean_sum and power_sum
    reg_miso   : out t_mem_miso;
    buf_mosi   : in  t_mem_mosi;  -- read and overwrite access to the data buffer
    buf_miso   : out t_mem_miso;

    -- Streaming clock domain
    st_rst     : in  std_logic;
    st_clk     : in  std_logic;

    in_sosi    : in t_dp_sosi  -- Signal path with data g_nof_symbols_per_data=4 8bit samples in time per one 32bit word
  );
end mms_aduh_monitor;

architecture str of mms_aduh_monitor is
  -- Monitor outputs
  signal mon_mean_sum  : std_logic_vector(c_longword_w - 1 downto 0);  -- use fixed 64 bit sum width
  signal mon_power_sum : std_logic_vector(c_longword_w - 1 downto 0);  -- use fixed 64 bit sum width
  signal mon_sync      : std_logic;  -- at the mon_sync there are new mean_sum and pwr_sum statistics available
begin
  u_mm_reg : entity work.aduh_monitor_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    st_rst            => st_rst,
    st_clk            => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in            => reg_mosi,
    sla_out           => reg_miso,

    -- MM registers in st_clk domain
    st_mon_mean_sum   => mon_mean_sum,
    st_mon_power_sum  => mon_power_sum,
    st_mon_sync       => mon_sync
  );

  u_monitor : entity work.aduh_monitor
  generic map (
    g_symbol_w             => g_symbol_w,
    g_nof_symbols_per_data => g_nof_symbols_per_data,
    g_nof_accumulations    => g_nof_accumulations,
    g_buffer_nof_symbols   => g_buffer_nof_symbols,
    g_buffer_use_sync      => g_buffer_use_sync
  )
  port map (
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,

    buf_mosi       => buf_mosi,
    buf_miso       => buf_miso,

    -- Streaming inputs
    st_rst         => st_rst,
    st_clk         => st_clk,

    in_sosi        => in_sosi,

    -- Monitor outputs
    stat_mean_sum  => mon_mean_sum,
    stat_pwr_sum   => mon_power_sum,
    stat_sync      => mon_sync
  );
end str;
