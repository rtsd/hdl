-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Description:
-- . Store input frames in a FIFO
-- . The sink is always ready, so a frame gets flushed when the FIFO is almost full
-- . Output a frame from the FIFO on request when available

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;
use technology_lib.technology_select_pkg.all;

entity eth_buffer is
  generic (
    g_technology         : natural := c_tech_select_default
  );
  port (
    -- Clocks and reset
    rst             : in  std_logic;  -- reset synchronous with clk
    clk             : in  std_logic;  -- packet stream clock

    -- Streaming Sink
    snk_in          : in  t_dp_sosi;
    snk_out         : out t_dp_siso := c_dp_siso_rdy;  -- internal u_fifo is always ready for packet data

    -- Streaming Source
    -- . The src_rd, src_ack and src_done act instead of src_in.ready to have src_in ready per frame instead of per data word
    src_rd          : in  std_logic;  -- a pulse causes the next frame to sourced out from the internal u_fifo if available
    src_ack         : out std_logic;  -- acknowledge the src_rd pulse
    src_done        : out std_logic;  -- frame received
    src_out         : out t_dp_sosi;

    -- Monitoring
    flushed_frm_cnt : out std_logic_vector(c_word_w - 1 downto 0)
  );
end eth_buffer;

architecture str of eth_buffer is
  constant c_nof_frames       : natural := 2;  -- support at least 2 max size frames in the FIFO
  constant c_fifo_nof_words   : natural := 2**(ceil_log2(c_nof_frames * c_eth_frame_nof_words));
  constant c_fifo_almost_full : natural := c_fifo_nof_words - 32;  -- little margin is sufficient, because flush u_fifo will start immediately

  type t_state_enum is (
    s_idle,
    s_frame,
    s_flush
  );

  -- Packet FIFO
  signal fifo_rd_siso         : t_dp_siso;
  signal fifo_rd_sosi         : t_dp_sosi;
  signal fifo_usedw           : std_logic_vector(ceil_log2(c_fifo_nof_words) - 1 downto 0);
  signal fifo_almost_full     : std_logic;
  signal nxt_fifo_almost_full : std_logic;

  signal frm_req              : std_logic;
  signal nxt_frm_req          : std_logic;
  signal frm_flush            : std_logic;
  signal nxt_frm_flush        : std_logic;
  signal frm_ack              : std_logic;
  signal frm_busy             : std_logic;
  signal frm_done             : std_logic;
  signal i_flushed_frm_cnt    : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_flushed_frm_cnt  : std_logic_vector(c_word_w - 1 downto 0);

  -- Output state machine
  signal state                : t_state_enum;
  signal nxt_state            : t_state_enum;
  signal nxt_src_ack          : std_logic;
  signal nxt_src_done         : std_logic;
begin
  flushed_frm_cnt <= i_flushed_frm_cnt;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      fifo_almost_full <= '0';
      state            <= s_idle;
      frm_req          <= '0';
      frm_flush        <= '0';
      src_ack          <= '0';
      src_done         <= '0';
      i_flushed_frm_cnt  <= (others => '0');
    elsif rising_edge(clk) then
      fifo_almost_full <= nxt_fifo_almost_full;
      state            <= nxt_state;
      frm_req          <= nxt_frm_req;
      frm_flush        <= nxt_frm_flush;
      src_ack          <= nxt_src_ack;
      src_done         <= nxt_src_done;
      i_flushed_frm_cnt  <= nxt_flushed_frm_cnt;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Input frame FIFO
  ------------------------------------------------------------------------------

  nxt_fifo_almost_full <= '1' when unsigned(fifo_usedw) > c_fifo_almost_full else '0';

  u_fifo : entity dp_lib.dp_fifo_sc
  generic map (
    g_technology  => g_technology,
    g_data_w      => c_eth_data_w,
    g_empty_w     => c_eth_empty_w,
    g_channel_w   => 1,
    g_error_w     => 1,
    g_use_empty   => true,
    g_use_channel => false,
    g_use_error   => false,
    g_use_ctrl    => true,
    g_fifo_size   => c_fifo_nof_words,
    g_fifo_rl     => 1
  )
  port map (
    rst      => rst,
    clk      => clk,
    -- ST sink
    snk_out  => OPEN,  -- the frame FIFO can not get full, because frames will get flushed when it is almost full
    snk_in   => snk_in,
    usedw    => fifo_usedw,
    -- ST source
    src_in   => fifo_rd_siso,
    src_out  => fifo_rd_sosi
  );

  u_frame_rd : entity dp_lib.dp_frame_rd
  generic map (
    g_dat_w         => c_eth_data_w,
    g_empty_w       => c_eth_empty_w
  )
  port map (
    rst       => rst,
    clk       => clk,

    frm_req   => frm_req,
    frm_flush => frm_flush,
    frm_ack   => frm_ack,
    frm_busy  => frm_busy,
    frm_err   => OPEN,
    frm_done  => frm_done,

    rd_req    => fifo_rd_siso.ready,
    rd_dat    => fifo_rd_sosi.data(c_eth_data_w - 1 downto 0),
    rd_empty  => fifo_rd_sosi.empty(c_eth_empty_w - 1 downto 0),
    rd_val    => fifo_rd_sosi.valid,
    rd_sof    => fifo_rd_sosi.sop,
    rd_eof    => fifo_rd_sosi.eop,

    out_dat   => src_out.data(c_eth_data_w - 1 downto 0),
    out_empty => src_out.empty(c_eth_empty_w - 1 downto 0),
    out_val   => src_out.valid,
    out_sof   => src_out.sop,
    out_eof   => src_out.eop
  );

  ------------------------------------------------------------------------------
  -- Output state machine
  ------------------------------------------------------------------------------

  p_state : process(state, src_rd, fifo_almost_full, frm_ack, frm_busy, frm_done, i_flushed_frm_cnt)
  begin
    nxt_frm_req   <= '0';
    nxt_frm_flush <= '0';
    nxt_state     <= state;
    nxt_src_ack   <= '0';
    nxt_src_done  <= '0';
    nxt_flushed_frm_cnt <= i_flushed_frm_cnt;
    case state is
      when s_idle =>
        if src_rd = '1' then
          -- . Handle external frame read request pulse
          nxt_frm_req   <= '1';
          nxt_state     <= s_frame;
        elsif fifo_almost_full = '1' then
          -- . Flush a frame if the FIFO is almost full
          nxt_frm_req   <= '1';
          nxt_frm_flush <= '1';
          nxt_state     <= s_flush;
        end if;
      when s_frame =>
        if frm_ack = '1' then
          nxt_src_ack <= '1';
          if frm_busy = '0' then
            nxt_state <= s_idle;  -- the u_fifo was empty, allow host to request src_rd again
          end if;
        end if;
        if frm_done = '1' then
          nxt_src_done <= '1';  -- indicate that new rx frame is available
          nxt_state    <= s_idle;
        end if;
      when others =>  -- s_flush
        if frm_done = '1' then
          nxt_src_ack <= '1';  -- acknowledge a src_rd that may have occured during flush, allow host to request src_rd again
          nxt_flushed_frm_cnt <= INCR_UVEC(i_flushed_frm_cnt, 1);
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
end str;
