--------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_diag_tx_frm is
end tb_diag_tx_frm;

architecture tb of tb_diag_tx_frm is
  constant c_period      : time    := 100 ns;
  constant c_nof_cycles  : natural := 83;  -- use prime number to avoid periodicity with sequence data, which can confuse interpretating the data

  constant c_sel         : std_logic := '1';  -- select COUNTER sequence
  constant c_init        : natural := 2;  -- test sequence init value
  constant c_frame_len   : natural := 20;  -- >= 2, test frame length
  constant c_frame_len_w : natural := ceil_log2(c_frame_len);
  constant c_dat_w       : natural := c_frame_len_w;

  signal rst            : std_logic;
  signal clk            : std_logic := '1';

  signal diag_sel       : std_logic;
  signal diag_frame_len : std_logic_vector(c_frame_len_w - 1 downto 0) := TO_UVEC(c_frame_len, c_frame_len_w);
  signal diag_init      : std_logic_vector(c_dat_w - 1 downto 0);
  signal diag_ready     : std_logic;
  signal diag_sop       : std_logic;

  signal seq_req        : std_logic;
  signal seq_dat        : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev_seq_dat   : std_logic_vector(c_dat_w - 1 downto 0) := TO_UVEC(c_init + c_frame_len - 1, c_dat_w);
  signal seq_val        : std_logic;
  signal seq_sop        : std_logic;
  signal seq_eop        : std_logic;
begin
  rst <= '1', '0' after c_period / 10;
  clk <= not clk after c_period / 2;

  stimuli: process
  -- run 100 us
  begin
    diag_sel <= c_sel;
    diag_init <= TO_UVEC(c_init, c_dat_w);
    diag_sop <= '0';
    seq_req  <= '1';
    wait for 10 * c_period;

    -- Keep seq_req='1'
    seq_req  <= '1';
    wait for c_period;
    diag_sop <= '1';  -- Generate one frame
    wait for c_period;
    diag_sop <= '0';
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;

    -- Keep seq_req='1'
    seq_req  <= '1';
    diag_init <= TO_UVEC(0, c_dat_w);
    wait for 10 * c_period;
    diag_sop <= '1';  -- Generate one frame with actual c_init only during diag_sop
    diag_init <= TO_UVEC(c_init, c_dat_w);
    wait for c_period;
    diag_sop <= '0';
    diag_init <= TO_UVEC(0, c_dat_w);
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;
    diag_init <= TO_UVEC(c_init, c_dat_w);

    -- Keep diag_sop='1' to immediately request a next frame again
    seq_req  <= '1';
    diag_sop <= '1';  -- Immediatly generate a frame when generator indicates ready
    for I in 0 to 3 * c_frame_len loop
      wait for c_period;
    end loop;
    diag_sop <= '0';
    wait for c_frame_len * c_period;
    wait for 10 * c_period;

    -- Use diag_ready to immediately request a next frame again
    seq_req <= '1';
    for I in 0 to 3 * c_frame_len loop
      diag_sop <= diag_ready;  -- Immediatly generate a frame when generator indicates ready
      wait for c_period;
    end loop;
    diag_sop <= '0';
    wait for c_frame_len * c_period;
    wait for 10 * c_period;

    -- Toggle seq_req='1'
    seq_req  <= '1';
    wait for c_period;
    diag_sop  <= '1';  -- Generate one frame
    wait for c_period;
    diag_sop <= '0';
    wait for 10 * c_period;
    seq_req  <= '0';  -- One inactive request cycle
    wait for c_period;
    seq_req  <= '1';
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;

    -- Make seq_req='0' near end of frame
    seq_req  <= '1';
    wait for c_period;
    diag_sop  <= '1';  -- Generate one frame
    wait for c_period;
    diag_sop <= '0';
    wait for (c_frame_len - 1) * c_period;
    seq_req  <= '0';  -- Some inactive request cycles
    wait for 5 * c_period;
    seq_req  <= '1';
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;

    -- Keep diag_sop='1' and make seq_req='0' near end of frame
    seq_req  <= '1';
    wait for c_period;
    diag_sop <= '1';  -- Generate one frame
    wait for c_period;
    diag_sop <= '1';
    wait for (c_frame_len - 1) * c_period;
    seq_req  <= '0';  -- Some inactive request cycles
    wait for 5 * c_period;
    seq_req  <= '1';
    wait for 5 * c_period;
    diag_sop <= '0';
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;

    wait;
  end process;

  u_diag_tx_frm : entity work.diag_tx_frm
  generic map (
    g_sel       => c_sel,
    g_init      => c_init,
    g_frame_len => c_frame_len,
    g_dat_w     => c_dat_w
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    -- Static control input (connect via MM or leave open to use default)
    diag_sel       => diag_sel,
    diag_frame_len => diag_frame_len,
    -- Dynamic control input (connect via MM or via ST input or leave open to use defaults)
    diag_ready     => diag_ready,
    diag_init      => diag_init,
    diag_sop       => diag_sop,
    -- ST output
    out_ready      => seq_req,
    out_dat        => seq_dat,
    out_val        => seq_val,
    out_sop        => seq_sop,
    out_eop        => seq_eop
  );

  prev_seq_dat <= seq_dat when rising_edge(clk) and seq_val = '1';

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if seq_sop = '1' then assert seq_val = '1' and unsigned(seq_dat) = c_init               report "Unexpected seq_sop";  end if;
      if seq_eop = '1' then assert seq_val = '1' and unsigned(seq_dat) = c_init + c_frame_len - 1 report "Unexpected seq_eop";  end if;

      if seq_val = '1' and unsigned(seq_dat) = c_init               then assert seq_sop = '1' report "Missing seq_sop";  end if;
      if seq_val = '1' and unsigned(seq_dat) = c_init + c_frame_len - 1 then assert seq_eop = '1' report "Missing seq_eop";  end if;

      if seq_val = '1' and unsigned(seq_dat) = c_init then assert unsigned(prev_seq_dat) = c_init + c_frame_len - 1  report "Wrong last seq_dat";  end if;
      if seq_val = '1' and unsigned(seq_dat) > c_init then assert unsigned(prev_seq_dat) = unsigned(seq_dat) - 1  report "Wrong seq_dat";       end if;
    end if;
  end process;
end tb;
