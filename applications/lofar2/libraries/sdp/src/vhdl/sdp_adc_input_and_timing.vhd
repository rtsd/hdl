-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Authors : J Hargreaves, L Hiemstra, R van der Walle, E. Kooistra
-- Purpose:
--   AIT - ADC / WG input, timing and associated diagnostic blocks
-- Description:
--   Contains all the signal processing blocks to receive and time the ADC input data
--   See https://support.astron.nl/confluence/display/STAT/L5+SDPFW+DD%3A+ADC+data+input+and+timestamp

library IEEE, common_lib, diag_lib, aduh_lib, dp_lib, tech_jesd204b_lib, st_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.sdp_pkg.all;

entity sdp_adc_input_and_timing is
  generic (
    g_no_st_histogram         : boolean := true;  -- when false use input histogram, else not to save block RAM
    g_buf_nof_data            : natural := c_sdp_V_si_db;
    g_bsn_nof_clk_per_sync    : natural := c_sdp_N_clk_per_sync;  -- Default 200M, overide for short simulation
    g_sim                     : boolean := false
  );
  port (
    -- clocks and resets
    mm_clk                         : in std_logic;
    mm_rst                         : in std_logic;
    rx_clk                         : in std_logic;
    rx_rst                         : in std_logic;
    rx_sysref                      : in std_logic;

    -- input samples
    rx_sosi_arr                    : in t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);

    -- processing status
    bs_sosi_valid                  : out std_logic;

    -- MM control buses
    -- Shiftram (applies per-antenna delay)
    reg_dp_shiftram_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_shiftram_miso           : out t_mem_miso := c_mem_miso_rst;

    -- bsn source
    reg_bsn_source_v2_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_source_v2_miso         : out t_mem_miso := c_mem_miso_rst;

    -- bsn scheduler
    reg_bsn_scheduler_wg_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_scheduler_wg_miso      : out t_mem_miso := c_mem_miso_rst;

    -- WG
    reg_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_wg_miso                    : out t_mem_miso := c_mem_miso_rst;
    ram_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_wg_miso                    : out t_mem_miso := c_mem_miso_rst;

    -- BSN MONITOR
    reg_bsn_monitor_input_mosi     : in  t_mem_mosi;
    reg_bsn_monitor_input_miso     : out t_mem_miso;

    -- Data buffer for framed samples (variable depth)
    ram_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    ram_diag_data_buf_bsn_miso     : out t_mem_miso;
    reg_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    reg_diag_data_buf_bsn_miso     : out t_mem_miso;

    -- ST Histogram
    ram_st_histogram_mosi          : in  t_mem_mosi;
    ram_st_histogram_miso          : out t_mem_miso;

    -- Aduh (statistics) monitor
    reg_aduh_monitor_mosi          : in  t_mem_mosi;
    reg_aduh_monitor_miso          : out t_mem_miso;

    -- Streaming data output
    out_sosi_arr                   : out t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0);
    rx_bsn_source_restart          : out std_logic;  -- for dp_sync_recover in WPFB
    rx_bsn_source_new_interval     : out std_logic;  -- for SST and BST statistics offload, XST uses new_interval
                                                     -- based on xst_processing_enable
    rx_bsn_source_nof_clk_per_sync : out std_logic_vector(c_word_w - 1 downto 0)  -- for RSN source in transient buffer
  );
end sdp_adc_input_and_timing;

architecture str of sdp_adc_input_and_timing is
  -- Waveform Generator
  constant c_wg_buf_directory       : string := "data/";
  constant c_wg_buf_dat_w           : natural := 18;  -- default value of WG that fits 14 bits of ADC data
  constant c_wg_buf_addr_w          : natural := 10;  -- default value of WG for 1024 samples;
  signal trigger_wg                 : std_logic;

  -- Frame parameters
  constant c_bs_sync_timeout        : natural := g_bsn_nof_clk_per_sync + g_bsn_nof_clk_per_sync / 10;  -- +10% margin
  constant c_bs_bsn_w               : natural := 64;  -- > 51;
  constant c_bs_aux_w               : natural := 2;
  constant c_bs_block_size          : natural := c_sdp_N_fft;  -- =1024;
  constant c_dp_fifo_dc_size        : natural := 64;

  -- Timestamp and streaming data arrays
  signal bs_sosi                    : t_dp_sosi := c_dp_sosi_rst;
  signal wg_sosi_arr                : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ant_sosi_arr               : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal mux_sosi_arr               : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nxt_mux_sosi_arr           : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal inp_sosi_arr               : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal st_sosi_arr                : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
begin
  out_sosi_arr <= st_sosi_arr;

  -----------------------------------------------------------------------------
  -- Timestamp
  -----------------------------------------------------------------------------
  u_bsn_source_v2 : entity dp_lib.mms_dp_bsn_source_v2
  generic map (
    g_cross_clock_domain     => true,
    g_block_size             => c_bs_block_size,
    g_nof_clk_per_sync       => g_bsn_nof_clk_per_sync,
    g_bsn_w                  => c_bs_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,
    dp_pps            => rx_sysref,

    -- Memory-mapped clock domain
    reg_mosi          => reg_bsn_source_v2_mosi,
    reg_miso          => reg_bsn_source_v2_miso,

    -- Streaming clock domain
    bs_sosi           => bs_sosi,

    bs_restart        => rx_bsn_source_restart,
    bs_new_interval   => rx_bsn_source_new_interval,
    bs_nof_clk_per_sync => rx_bsn_source_nof_clk_per_sync
  );

  bs_sosi_valid <= bs_sosi.valid;

  u_bsn_trigger_wg : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => true,
    g_bsn_w              => c_bs_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_wg_mosi,
    reg_miso    => reg_bsn_scheduler_wg_miso,

    -- Streaming clock domain
    dp_rst      => rx_rst,
    dp_clk      => rx_clk,

    snk_in      => bs_sosi,  -- only uses eop (= block sync), bsn[]
    trigger_out => trigger_wg
  );

  -----------------------------------------------------------------------------
  -- WG (Waveworm Generator)
  -----------------------------------------------------------------------------
  u_wg_arr : entity diag_lib.mms_diag_wg_wideband_arr
  generic map (
    g_nof_streams        => c_sdp_S_pn,
    g_cross_clock_domain => true,
    g_buf_dir            => c_wg_buf_directory,

    -- Wideband parameters
    g_wideband_factor    => 1,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          => c_wg_buf_dat_w,
    g_buf_addr_w         => c_wg_buf_addr_w,
    g_calc_support       => true,
    g_calc_gain_w        => 1,
    g_calc_dat_w         => c_sdp_W_adc
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_wg_mosi,
    reg_miso            => reg_wg_miso,

    buf_mosi            => ram_wg_mosi,
    buf_miso            => ram_wg_miso,

    -- Streaming clock domain
    st_rst              => rx_rst,
    st_clk              => rx_clk,
    st_restart          => trigger_wg,

    out_sosi_arr        => wg_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- ADC data
  -----------------------------------------------------------------------------
  p_rx_rewire_data : process(rx_sosi_arr)
  begin
    ant_sosi_arr <= rx_sosi_arr;
    for I in 0 to c_sdp_S_pn - 1 loop
      -- ADC data is stored in the upper 14 bits of the jesd rx_sosi.
      ant_sosi_arr(I).data <= RESIZE_DP_SDATA(
                                rx_sosi_arr(I).data(c_sdp_W_adc_jesd - 1 downto (c_sdp_W_adc_jesd - c_sdp_W_adc))
                              );
      ant_sosi_arr(I).valid <= rx_sosi_arr(I).valid;  -- connect, but not used
    end loop;
  end process;

  -----------------------------------------------------------------------------
  -- ADC/WG Mux (Input Select)
  -----------------------------------------------------------------------------
  gen_mux : for I in 0 to c_sdp_S_pn - 1 generate
    p_mux : process(ant_sosi_arr, wg_sosi_arr)
    begin
      -- Default use the ADC data
      nxt_mux_sosi_arr(I).data <= ant_sosi_arr(I).data;
      if wg_sosi_arr(I).valid = '1' then
        -- Valid WG data overrules ADC data
        nxt_mux_sosi_arr(I).data <= wg_sosi_arr(I).data;
      end if;
      -- Force valid = '1' to maintain synchronous processing, independent of
      -- whether the ADC/WG data values are valid
      nxt_mux_sosi_arr(I).valid <= '1';
    end process;
  end generate;

  mux_sosi_arr <= nxt_mux_sosi_arr when rising_edge(rx_clk);

  -----------------------------------------------------------------------------
  -- Time delay: dp_shiftram
  -- . copied from unb1_bn_capture_input (apertif)
  --   Array range reversal is not done because everything is DOWNTO
  -- . the input valid is always '1', even when there is no data, to avoid
  --   shift misalignment between different signal inputs on different nodes
  -- . the sync_in ensures that a new shift setting takes effect at the
  --   bs_sosi.sync
  -----------------------------------------------------------------------------
  u_dp_shiftram : entity dp_lib.dp_shiftram
  generic map (
    g_nof_streams => c_sdp_S_pn,
    g_nof_words   => c_sdp_V_sample_delay,
    g_data_w      => c_sdp_W_adc,
    g_use_sync_in => true
  )
  port map (
    dp_rst   => rx_rst,
    dp_clk   => rx_clk,
    mm_rst   => mm_rst,
    mm_clk   => mm_clk,

    sync_in  => bs_sosi.sync,

    reg_mosi => reg_dp_shiftram_mosi,
    reg_miso => reg_dp_shiftram_miso,

    snk_in_arr => mux_sosi_arr,
    src_out_arr => inp_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Concatenate input data streams with BSN framing
  -----------------------------------------------------------------------------
  gen_concat : for I in 0 to c_sdp_S_pn - 1 generate
    p_concat : process(inp_sosi_arr, bs_sosi)
    begin
      st_sosi_arr(I)       <= bs_sosi;  -- timestamp
      st_sosi_arr(I).data  <= inp_sosi_arr(I).data;
    end process;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Diagnostics on the BSN-framed data
  ---------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------
  -- BSN monitor, for sync, BSN and nof samples per sync
  ---------------------------------------------------------------------------------------
  u_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => 1,  -- They're all the same
    g_sync_timeout       => c_bs_sync_timeout,
    g_bsn_w              => c_bs_bsn_w,
    g_log_first_bsn      => false
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_input_mosi,
    reg_miso    => reg_bsn_monitor_input_miso,

    -- Streaming clock domain
    dp_rst      => rx_rst,
    dp_clk      => rx_clk,
    in_sosi_arr => st_sosi_arr(0 downto 0)
  );

  -----------------------------------------------------------------------------
  -- Monitor ADU/WG output, for mean sum and power sum
  -----------------------------------------------------------------------------
  u_aduh_monitor : entity aduh_lib.mms_aduh_monitor_arr
  generic map (
    g_cross_clock_domain   => true,
    g_nof_streams          => c_sdp_S_pn,
    g_symbol_w             => c_sdp_W_adc,
    g_nof_symbols_per_data => 1,  -- Wideband factor is 1
    g_nof_accumulations    => g_bsn_nof_clk_per_sync
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,

    reg_mosi       => reg_aduh_monitor_mosi,
    reg_miso       => reg_aduh_monitor_miso,
    buf_mosi       => c_mem_mosi_rst,  -- Unused
    buf_miso       => OPEN,

    -- Streaming clock domain
    st_rst         => rx_rst,
    st_clk         => rx_clk,

    in_sosi_arr    => st_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Diagnostic Data Buffer, for capturing raw time series data
  -----------------------------------------------------------------------------
  u_diag_data_buffer_bsn : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_sdp_S_pn,
    g_data_w       => c_sdp_W_adc,
    g_buf_nof_data => g_buf_nof_data,
    g_buf_use_sync => true  -- when TRUE start filling the buffer at the in_sync, else after the last word was read
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,

    ram_data_buf_mosi => ram_diag_data_buf_bsn_mosi,
    ram_data_buf_miso => ram_diag_data_buf_bsn_miso,
    reg_data_buf_mosi => reg_diag_data_buf_bsn_mosi,
    reg_data_buf_miso => reg_diag_data_buf_bsn_miso,

    in_sosi_arr       => st_sosi_arr,
    in_sync           => st_sosi_arr(0).sync
  );

  -----------------------------------------------------------------------------
  -- ST Histogram
  -----------------------------------------------------------------------------
  no_st_histogram : if g_no_st_histogram = true generate
    ram_st_histogram_miso <= c_mem_miso_rst;
  end generate;

  gen_st_histogram : if g_no_st_histogram = false generate
    u_st_histogram : entity st_lib.mmp_st_histogram
    generic map (
      g_nof_instances          => c_sdp_S_pn,
      g_data_w                 => c_sdp_W_adc,
      g_nof_bins               => c_sdp_V_si_histogram,
      g_nof_data_per_sync      => g_bsn_nof_clk_per_sync,
      g_nof_data_per_sync_diff => c_sdp_N_fft / 2
    )
    port map (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => rx_rst,
      dp_clk            => rx_clk,

      ram_copi          => ram_st_histogram_mosi,
      ram_cipo          => ram_st_histogram_miso,

      snk_in_arr        => st_sosi_arr
    );
  end generate;
end str;
