-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Polyphase FIR filter
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.pfs_pkg.all;

entity pfs is
  generic (
    g_nof_bands         : natural := 1024;
    g_nof_taps          : natural := 16 * 1024;
    g_in_dat_w          : natural := 12;
    g_out_dat_w         : natural := 18;
    g_coef_dat_w        : natural := c_pfs_coef_w;  -- = 16, should match coefs in g_coefs_file
    g_coefs_file        : string  := c_pfs_coefs_file  -- = "data/pfs_coefsbuf_1024.hex"
  );
  port (
    in_dat_x            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_y            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val              : in  std_logic;
    in_sync             : in  std_logic;
    out_dat_x           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_dat_y           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val             : out std_logic;
    out_sync            : out std_logic;
    clk                 : in  std_logic;
    rst                 : in  std_logic;
    restart             : in  std_logic
  );
end pfs;

architecture str of pfs is
  constant c_nof_bands_w    : natural := ceil_log2(g_nof_bands);
  constant c_nof_fir_taps   : natural := g_nof_taps / g_nof_bands;
  constant c_nof_fir_taps_w : natural := ceil_log2(c_nof_fir_taps);

  signal taps_rdaddr        : std_logic_vector(c_nof_bands_w - 1 downto 0);
  signal taps_wraddr        : std_logic_vector(c_nof_bands_w - 1 downto 0);
  signal taps_wren          : std_logic;
  signal taps_in_x          : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_in_y          : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_out_x         : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_out_y         : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal coefs              : std_logic_vector(g_coef_dat_w * c_nof_fir_taps - 1 downto 0);
begin
  ctrl : entity work.pfs_ctrl
  generic map (
    g_nof_bands_w     => c_nof_bands_w,
    g_nof_taps        => c_nof_fir_taps,
    g_nof_taps_w      => c_nof_fir_taps_w,
    g_taps_w          => g_in_dat_w
  )
  port map (
    clk               => clk,
    rst               => rst,
    restart           => restart,
    in_x              => in_dat_x,
    in_y              => in_dat_y,
    in_val            => in_val,
    in_sync           => in_sync,
    taps_rdaddr       => taps_rdaddr,
    taps_wraddr       => taps_wraddr,
    taps_wren         => taps_wren,
    taps_in_x         => taps_in_x,
    taps_in_y         => taps_in_y,
    taps_out_x        => taps_out_x,
    taps_out_y        => taps_out_y,
    out_val           => out_val,
    out_sync          => out_sync
  );

  firx : entity work.pfs_filter
  generic map (
    g_coef_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => g_in_dat_w,
    g_nof_taps        => c_nof_fir_taps
  )
  port map(
    clk               => clk,
    taps              => taps_out_x,
    coefs             => coefs,
    result            => out_dat_x
  );

  firy : entity work.pfs_filter
  generic map (
    g_coef_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => g_in_dat_w,
    g_nof_taps        => c_nof_fir_taps
  )
  port map (
    clk               => clk,
    taps              => taps_out_y,
    coefs             => coefs,
    result            => out_dat_y
  );

  tapsbufx : entity work.pfs_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w * c_nof_fir_taps,
    g_nof_words       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    wrdata            => taps_out_x,
    wren              => taps_wren,
    wraddr            => taps_wraddr,
    rdaddr            => taps_rdaddr,
    rddata            => taps_in_x,
    clk               => clk,
    rst               => rst
  );

  tapsbufy : entity work.pfs_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w * c_nof_fir_taps,
    g_nof_words       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    wrdata            => taps_out_y,
    wren              => taps_wren,
    wraddr            => taps_wraddr,
    rdaddr            => taps_rdaddr,
    rddata            => taps_in_y,
    clk               => clk,
    rst               => rst
  );

  coefsbuf : entity work.pfs_coefsbuf
  generic map (
    g_data_w          => g_coef_dat_w * c_nof_fir_taps,
    g_coefs_file      => g_coefs_file,
    g_nof_coefs       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    addr              => taps_rdaddr,
    data              => coefs,
    clk               => clk,
    rst               => rst
  );
end str;
