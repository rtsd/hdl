-------------------------------------------------------------------------------
-- Title      : CRC generator
-- Project    :
-------------------------------------------------------------------------------
-- File       : crc_generator.vhd
-- Author     : William Kamp  <william.kamp@aut.ac.nz>
-- Company    :
-- Created    : 2016-02-16
-- Last update: 2016-07-26
-- Platform   :
-- Standard   : VHDL'2008
-------------------------------------------------------------------------------
-- Description:
-- Calculates the CRC for an AXI stream.
--
-- Area vs Speed Tradeoff.
--   The CRC algorithm can be parallelised by slicing the data into segments and
--   computing the CRC of each segment of the bus and XORing the results.
--
--   This uses the result that CRC(msg_A) xor CRC(msg_B) = CRC(msg_A xor msg_B).
--   The kicker is that msg_A'length must equal msg_B'length.
--
--   The message on the input bus is separated into segments, e.g.
--    msg   = 0x 1234_5678
--    msg_A = 0x 1234|0000
--    msg_B = 0x 0000|5678
--   such that each message has one 'active' segment and all other segments = '0'.
--
--   The data bits in the non-active segments for each CRC are replaced with a
--   constant '0'. The synthesis engine can then optimise away part of the
--   CRC XOR-tree, since A xor '0' = A, thus reducing the critical timing path.
--   This of course has
--   a limit, as no reduction can be made on the CRC feedback path, which will
--   have an average fan in of half the CRC bits, plus half the data bits in the
--   slice. Additionally as g_AREA_SPEED_TRADEOFF_FACTOR is increased, the size
--   of the final combining XOR tree will also grow. However this can be pipelined!
--   If you double the number of CRC slices used, then the area of each CRC will reduce,
--   initially by about a third, and then with diminishing returns. Therefore, with
--   g_AREA_SPEED_TRADEOFF_FACTOR = 2, will result in an area increase of
--   approximately 2*2/3 = 1.33, or 33%.
--   If you double the number of CRCs, then the fan-in of each CRC will be reduced
--   by half the data width. If the original data width is 64b, and the CRC is 32b,
--   and g_AREA_SPEED_TRADEOFF_FACTOR = 2 then the average fan-in for each CRC bit
--   is (64/gASTF + 32/2) = 48.
--
--   When keep /= (others => '1'), then the lengths of msg_A and msg_B are not equal.
--   Initially the part CRCs are calculated assuming that the bus is full and using
--   keep to set the corresponding bytes to zero. This means extra zero bytes are
--   included in the messages.
--   The part CRCs are then combined with an XOR.
--   Finally, the extra zero bytes are removed from the CRC by running the LFSR
--   backwards by the number of extra zero bytes in the original message.
--
-- Error Forwarding:
--   If i_stream.error is set with i_stream.last then the CRC result is inverted.
--   You can distinguish between a transmission error and a forwarded error at the
--   receiver, by comparing the recalculated crc with the original and its inverse.
--
-- g_INVALID_CYCLES_BETWEEN_PACKETS:
--   This parameter is the number of guaranteed invalid cycles between packets.
--   If this value is greater than 0, then the logic area is approximately halved.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2016
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author     Description
-- 2016-02-16  1.0      will       Created
-- 2019-10-15  1.1      Will Kamp  Added support for i_stream.keep, generics to
--                                 control bit reversal and CRC output XOR.
-- 2024-02-27  1.2      R vd Walle Removed byte swap when G_BIT_REVERSE_OUT = 
--                      (ASTRON)   true, only perform bit swap, no byte swap.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use work.crc_pkg.all;
    use work.PCK_CRC32_D1_reversed.all; -- prevCRC32_D1
    use work.misc_tools_pkg.all; -- bit_swap, byte_swap

use work.bit_width_config_pkg.all;

-- vsg_off entity_008 entity_012

entity crc_generator is
    generic (
        G_AREA_SPEED_TRADEOFF_FACTOR     : natural range 1 to 32 := 1;
        -- defines the parallelisation of the implementation. Larger  =  more area,  but more speed.
        G_INIT_CRC                       : std_logic_vector       := (31 downto 0 => '1');
        -- Initial value of the CRC calc.
        G_BIT_REVERSE_IN                 : boolean                := true;
        -- Reverse the bit order within each input byte before applying CRC.
        G_BIT_REVERSE_OUT                : boolean                := true;
        -- Reverse the bit order within each CRC output byte,
        G_XOR_CRC_OUT                    : std_logic_vector       := (31 downto 0 => '1');
        -- XOR the CRC output with this value.
        G_INVALID_CYCLES_BETWEEN_PACKETS : natural                := 0
        -- guaranteed number of cycles between packets.
    );
    port (
        i_clk       : in    std_logic;
        i_clk_reset : in    std_logic;

        i_stream    : in    AXI4_STREAMING_RDMA_t;
        o_stop      : out   std_logic;
        o_crc       : out   std_logic_vector;
        o_crc_vld   : out   std_logic;
        i_stop2     : in    std_logic
    );
end entity crc_generator;

architecture rtl of crc_generator is

    constant C_TOTAL_WIDTH : natural := i_stream.data'length;
    constant C_CRC_WIDTH   : natural := o_crc'length;

    function f_slice_width (
        constant slice_idx : natural)
        return natural is
    begin
        -- calculates irregular divisions of the bus width, e.g. by 3.
        return (c_TOTAL_WIDTH + slice_idx) / g_AREA_SPEED_TRADEOFF_FACTOR;
    end function f_slice_width;

    function f_slice_lo (
        constant slice_idx : natural)
        return natural is
        variable v_lo : natural;
    begin -- function f_slice_lo
        v_lo := 0;
        for i in 1 to slice_idx loop
            v_lo := v_lo + f_slice_width(i);
        end loop;
        return v_lo;
    end function f_slice_lo;

    type t_slv_crc_a is array (natural range <>) of std_logic_vector(o_crc'range);
    signal part_crc         : t_slv_crc_a(g_AREA_SPEED_TRADEOFF_FACTOR - 1 downto 0);
    signal part_crc_vld     : std_logic_vector(g_AREA_SPEED_TRADEOFF_FACTOR - 1 downto 0);
    signal part_keep        : std_logic_vector(i_stream.keep'range);
    signal err_fwd          : std_logic;

    signal crc_combined     : std_logic_vector(o_crc'range);
    signal crc_combined_vld : std_logic;
    signal comb_keep        : std_logic_vector(i_stream.keep'range);

begin     -- architecture rtl

    o_stop <= i_stop2;

    G_SLICE : for idx in 0 to g_AREA_SPEED_TRADEOFF_FACTOR - 1 generate
        -- calculate the range of a slice of the data. This will cope with irregular divisors.
        constant C_SLICE_WIDTH : natural := f_slice_width(idx);
        constant C_SLICE_LO    : natural := f_slice_lo(idx);
        constant C_SLICE_HI    : natural := C_SLICE_LO + C_SLICE_WIDTH - 1;
        signal slice_crc       : std_logic_vector(o_crc'range);
        signal sop             : std_logic;
    begin

        P_CRC : process (i_clk) is
            variable v_data   : std_logic_vector(i_stream.data'range);
            variable v_crc_fb : std_logic_vector(o_crc'range);
        begin -- process
            if rising_edge(i_clk) then
                v_data := (others => '0');
                for byte in C_SLICE_HI / 8 downto C_SLICE_LO / 8 loop
                    -- extract a slice of the data bus.
                    -- validate each byte using keep, set to zero if invalid.
                    v_data(byte * 8 + 7 downto byte * 8) := i_stream.data(byte * 8 + 7 downto byte * 8)
                                                            and i_stream.keep(byte);
                end loop;
                if g_BIT_REVERSE_IN then
                    v_data := byte_swap(bit_swap(v_data));
                end if;
                if i_stream.valid then
                    sop <= i_stream.last;
                    if sop then
                        v_crc_fb := g_INIT_CRC when C_SLICE_HI = i_stream.data'high
                else (others => '0');
                    else
                        v_crc_fb := slice_crc;
                    end if;
                    -- compute the slice_crc for this slice.
                    slice_crc <= f_next_crc(v_data, v_crc_fb);
                end if;
                part_crc_vld(idx) <= i_stream.valid and i_stream.last;
                if i_clk_reset then
                    sop <= '1';
                end if;
            end if;
        end process P_CRC;

        part_crc(idx) <= slice_crc;

    end generate G_SLICE;

    P_PIPE_CONTROL : process (i_clk) is
    begin -- process
        if rising_edge(i_clk) then
            err_fwd   <= i_stream.error;
            part_keep <= i_stream.keep;
        end if;
    end process P_PIPE_CONTROL;

    -- XOR all the part CRC together.
    -- If this is large (g_AREA_SPEED_TRADEOFF_FACTOR > 5), could add a pipeline or two.
    P_COMBINE : process (i_clk) is
        variable v_crc_comb : std_logic_vector(o_crc'range);
    begin                                      -- process
        if rising_edge(i_clk) then
            v_crc_comb := (others => err_fwd); -- Invert the result of the CRC.
            for idx in 0 to g_AREA_SPEED_TRADEOFF_FACTOR - 1 loop
                v_crc_comb := part_crc(idx) xor v_crc_comb;
            end loop;
            crc_combined     <= v_crc_comb;
            crc_combined_vld <= part_crc_vld(0);
            comb_keep        <= part_keep;
        end if;
    end process P_COMBINE;

    -- CRC has been calculated to the full width of the bus.
    -- Now roll back the extra zero bytes that were left over.
    -- To achieve more speed, possibly should do this in each slice before combining the part CRCs.
    P_ROLLBACK : process (i_clk) is
        variable v_rb_crc  : std_logic_vector(o_crc'range);
        variable v_xor_crc : std_logic_vector(o_crc'range);
    begin
        if rising_edge(i_clk) then
            v_rb_crc := crc_combined;
            if crc_combined_vld then
                for byte in comb_keep'low to comb_keep'high loop
                    if not comb_keep(byte) then
                        for b in 0 to 7 loop
                            v_rb_crc := prevCRC32_D1('0', v_rb_crc);
                        end loop; -- b
                    else
                        assert (and comb_keep(comb_keep'high downto byte))
                            report "Expected the high bits of the keep to all be the same. Got: " & to_string(comb_keep)
                            severity error;
                        exit;     -- exit loop prematurely.
                    end if;
                end loop;         --byte
            end if;

            v_xor_crc := v_rb_crc xor g_XOR_CRC_OUT;

            if g_BIT_REVERSE_OUT then
                o_crc <= bit_swap(v_xor_crc);
            else
                o_crc <= v_xor_crc;
            end if;

            o_crc_vld <= crc_combined_vld;
        end if;
    end process P_ROLLBACK;

end architecture RTL;
