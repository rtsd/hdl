-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra
-- Date: 17 April 2023
-- Purpose: Calculate CRC per block of data
-- Description:
--   The calculated blk_crc of a block is available immediately after the
--   snk_in.eop of that block.

library IEEE, common_lib, easics_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

use easics_lib.PCK_CRC8_D8.all;
use easics_lib.PCK_CRC16_D16.all;
use easics_lib.PCK_CRC28_D28.all;
use easics_lib.PCK_CRC32_D32.all;
use easics_lib.PCK_CRC32_D64.all;

entity dp_calculate_crc is
  generic (
    g_data_w  : natural := 32;
    g_crc_w   : natural := 32
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_in       : in  t_dp_sosi;
    blk_crc      : out std_logic_vector(g_crc_w - 1 downto 0)
  );
end dp_calculate_crc;

architecture rtl of dp_calculate_crc is
  constant c_crc_init : std_logic_vector(g_crc_w - 1 downto 0) := (others => '1');

  function func_next_crc(data, crc : std_logic_vector) return std_logic_vector is
    variable v_crc : std_logic_vector(g_crc_w - 1 downto 0) := c_crc_init;
  begin
    if    g_data_w =  8 and g_crc_w =  8 then v_crc := nextCRC8_D8(data, crc);
    elsif g_data_w = 16 and g_crc_w = 16 then v_crc := nextCRC16_D16(data, crc);
    elsif g_data_w = 28 and g_crc_w = 28 then v_crc := nextCRC28_D28(data, crc);
    elsif g_data_w = 32 and g_crc_w = 32 then v_crc := nextCRC32_D32(data, crc);
    elsif g_data_w = 64 and g_crc_w = 32 then v_crc := nextCRC32_D64(data, crc);
    else
      report "Data width and CRC width combination not supported (yet)"
        severity FAILURE;
    end if;
    return v_crc;
  end func_next_crc;

  signal data         : std_logic_vector(g_data_w - 1 downto 0);
  signal calc_crc     : std_logic_vector(g_crc_w - 1 downto 0);
  signal nxt_calc_crc : std_logic_vector(g_crc_w - 1 downto 0);
  signal i_blk_crc    : std_logic_vector(g_crc_w - 1 downto 0);
  signal nxt_blk_crc  : std_logic_vector(g_crc_w - 1 downto 0);
begin
  data <= snk_in.data(g_data_w - 1 downto 0);

  blk_crc <= i_blk_crc;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      calc_crc   <= c_crc_init;
      i_blk_crc  <= c_crc_init;
    elsif rising_edge(clk) then
      calc_crc   <= nxt_calc_crc;
      i_blk_crc  <= nxt_blk_crc;
    end if;
  end process;

  p_crc : process(data, calc_crc, i_blk_crc, snk_in)
    variable v_crc : std_logic_vector(g_crc_w - 1 downto 0);
  begin
    v_crc := func_next_crc(data, calc_crc);

    -- Calculate CRC per block
    nxt_calc_crc <= calc_crc;
    if snk_in.sop = '1' then  -- restart CRC at begin of block
      nxt_calc_crc <= func_next_crc(data, c_crc_init);
    elsif snk_in.valid = '1' then  -- calculate CRC during block
      nxt_calc_crc <= v_crc;
    end if;

    -- Hold CRC of previous block, available after eop
    nxt_blk_crc <= i_blk_crc;
    if snk_in.eop = '1' then
      nxt_blk_crc <= v_crc;
    end if;
  end process;
end rtl;
