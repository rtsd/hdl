-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose : Recover DP control signals (sync, valid, sop, eop, bsn) from input of a processing component and the out valid of that component.
-- Description: dp_sync_recover generates the control signals based on in_sosi.sync, in_sosi.bsn and recover_val.
--  dp_sync_recover is used in combination with a processing component that doesn't keep the control signals. By connecting the input of the
--  processing component to in_sosi and the valid of the output of the processing component to recover_val, dp_sync_recover will generate the
--  missing control signals at out_sosi.
--  . A data counter is used to count the valids from the input "recover_val" and compare to g_nof_data_per_block to generate sop/eop.
--  . A block counter is used generate the BSN
--  . The BSN at sync of in_sosi is captured to determine when to generate the sync at the output.
--  . IN recover_val is used to indicate when to start outputting the control signals. recover_val is also used for out_sosi.valid, these are connected as wires, so no latency.
--  . IN restart is used to restart the counters. On the restart pulse the in_sosi.bsn is used as the initial value for the bsn counter.
-- Remarks:
--  . The recover_val input signal should be connected to the valid of the output of the related processing component. It determines when
--    the first block will start after a (re)start.
--  . It is assumed that the restart pulse is synchronous with in_sosi.sop (if it would have a sop)
--  . IN recover_val should have at least a latency of 1 compared to in_sosi.
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_sync_recover is
  generic (
    g_nof_data_per_block : positive := 1
  );
  port (
    -- Clocks and reset
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;

    in_sosi     : in  t_dp_sosi := c_dp_sosi_rst;
    recover_val : in  std_logic;  -- valid of the out_sosi that needs to be recoverd.
    restart     : in  std_logic := '0';  -- pulse to restart bsn counter

    out_sosi    : out t_dp_sosi
  );
end dp_sync_recover;

architecture rtl of dp_sync_recover is
  type t_reg is record  -- local registers
    bsn_at_sync        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);  -- bsn to store at which to generate a sync pulse.
    bsn_before_restart : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);  -- bsn to store at which to restart te bsn counter.
    bsn_at_restart     : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);  -- bsn to store inital bsn after (re)start.
    restart            : std_logic;
    started            : std_logic;
    data_cnt           : natural range 0 to g_nof_data_per_block;
    out_bsn            : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    out_sosi           : t_dp_sosi;
  end record;

  constant c_reg_rst  : t_reg := ( (others => '0'), (others => '0'), (others => '0'), '0', '0', 0, (others => '0'), c_dp_sosi_rst);

  -- Define the local registers in t_reg record
  signal r         : t_reg;
  signal nxt_r     : t_reg;
begin
  out_sosi <= nxt_r.out_sosi;

  p_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, in_sosi, recover_val, restart)
    variable v : t_reg;
  begin
    v := r;
    v.out_sosi       := c_dp_sosi_rst;
    v.out_sosi.valid := recover_val;
    v.out_sosi.bsn   := r.out_sosi.bsn;

    if r.restart = '0' and restart = '0' then  -- keep track of last bsn before restart
      v.bsn_before_restart := in_sosi.bsn;
    end if;

    if in_sosi.sync = '1' then  -- capture bsn at sync
      v.bsn_at_sync  := in_sosi.bsn;
    end if;

    if recover_val = '1' then
      v.data_cnt := r.data_cnt + 1;
      if r.data_cnt = 0 then  -- generate sop + bsn
        v.out_sosi.sop := '1';
        v.out_bsn := std_logic_vector(unsigned(r.out_bsn) + 1);  -- increase block counter
        v.out_sosi.bsn := r.out_bsn;
        if r.out_bsn = r.bsn_at_sync then  -- generate sync pulse
          v.out_sosi.sync := '1';
        end if;
      end if;
      if r.data_cnt = g_nof_data_per_block - 1 then  -- reset data counter and generate eop.
        v.data_cnt := 0;
        v.out_sosi.eop := '1';
      end if;
    end if;

    -- overwrite v.out_bsn if a restart has occurd.
    if restart = '1' then  -- Capture BSN at restart
      v.bsn_at_restart  := in_sosi.bsn;  -- initial bsn value
      v.restart := '1';
      if r.started = '0' or r.out_sosi.bsn = r.bsn_before_restart then  -- if restarted for the first time or if the restart should happen at the current bsn use the in_sosi.bsn immidiatly.
        v.started := '1';
        v.restart := '0';
        v.out_bsn := in_sosi.bsn;
      end if;
    end if;

    -- If the latency is larger than one block, it is needed wait with the restart until the last block has arrived.
    if r.restart = '1' and r.out_sosi.bsn = r.bsn_before_restart then  -- set bsn to initial value after restart
      v.out_bsn := r.bsn_at_restart;
      v.restart := '0';
    end if;

    nxt_r <= v;
  end process;
end rtl;
