--------------------------------------------------------------------------------
--
-- Author: Daniel van der Schuur
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.corr_permutor_pkg.all;

-- Purpose:
-- . Outputs all unique pair permutations of the words at the inputs.
-- Description:
-- .

entity corr_permutor is
  generic (
    g_nof_inputs  : natural;  -- e.g. 24 inputs yield (24*(24+1))/2=300 visibilities
    g_nof_outputs : natural;  -- = nof_visibilities/g_nof_cycles
    g_nof_cycles  : natural;  -- = nof_visibilities/g_nof_outputs
    g_data_w      : natural
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    snk_out_arr    : out t_dp_siso_arr(g_nof_inputs - 1 downto 0);  -- snk_out_arr(i).ready is low during processing when g_nof_cycles>1

    src_out_2arr_2 : out t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0)  -- Array of pairs
  );
end corr_permutor;

architecture rtl of corr_permutor is
  constant c_nof_permutations   : natural := g_nof_inputs * (g_nof_inputs + 1) / 2;
  constant c_permutation_table  : t_corr_permutation_table := corr_permute(g_nof_inputs, g_nof_cycles);  -- Returns 3D array of dimensions nof_folding_cycles*g_nof_outputs*2 input indices

  type t_common_paged_reg_arr is array(g_nof_inputs - 1 downto 0) of std_logic_vector(g_data_w - 1 downto 0);

  signal common_paged_reg_wr_re_arr       : t_common_paged_reg_arr;
  signal common_paged_reg_wr_im_arr       : t_common_paged_reg_arr;
  signal common_paged_reg_out_re_arr      : t_common_paged_reg_arr;
  signal common_paged_reg_out_im_arr      : t_common_paged_reg_arr;
  signal common_paged_reg_src_out_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal cycle_cnt                        : std_logic_vector(ceil_log2(g_nof_cycles) - 1 downto 0);
  signal nxt_cycle_cnt                    : std_logic_vector(ceil_log2(g_nof_cycles) - 1 downto 0);

  signal perm_2arr_2                      : t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0);
  signal nxt_perm_2arr_2                  : t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0);

  signal nxt_snk_out_arr                  : t_dp_siso_arr(g_nof_inputs - 1 downto 0);

  signal common_pulse_extend_ep_out_valid : std_logic;
  signal common_pulse_extend_ep_out_sync  : std_logic;
begin
  -----------------------------------------------------------------------------
  -- One set of input values is valid for only one clock cycle. We need to
  -- preserve the input data for an additional g_nof_cycles-1 clock
  -- cycles while the process p_permute puts the input data on the outputs.
  -----------------------------------------------------------------------------
  gen_common_paged_reg : if g_nof_cycles > 1 generate
    gen_common_paged_reg_i : for i in 0 to g_nof_inputs - 1 generate
      common_paged_reg_wr_re_arr(i) <= snk_in_arr(i).re(g_data_w - 1 downto 0);
      common_paged_reg_wr_im_arr(i) <= snk_in_arr(i).im(g_data_w - 1 downto 0);

      u_common_paged_reg_re : entity common_lib.common_paged_reg
      generic map (
        g_data_w    => g_data_w,
        g_nof_pages => 1
      )
      port map (
        rst          => rst,
        clk          => clk,
        wr_en(0)     => snk_in_arr(0).valid,
        wr_dat       => common_paged_reg_wr_re_arr(i),
        out_dat      => common_paged_reg_out_re_arr(i)
      );

      u_common_paged_reg_im : entity common_lib.common_paged_reg
      generic map (
        g_data_w    => g_data_w,
        g_nof_pages => 1
      )
      port map (
        rst          => rst,
        clk          => clk,
        wr_en(0)     => snk_in_arr(0).valid,
        wr_dat       => common_paged_reg_wr_im_arr(i),
        out_dat      => common_paged_reg_out_im_arr(i)
      );

      common_paged_reg_src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_paged_reg_out_re_arr(i));
      common_paged_reg_src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_paged_reg_out_im_arr(i));
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- Count folding clock cycles. As all valid inputs are presented in one
  -- clock cycle, we always reset the counter at that moment and and spend the
  -- following clock cycles putting the folded data on the outputs.
  -----------------------------------------------------------------------------
  nxt_cycle_cnt <= (others => '0') when unsigned(cycle_cnt) = g_nof_cycles - 1 else INCR_UVEC(cycle_cnt, 1) when common_pulse_extend_ep_out_valid = '1' or snk_in_arr(0).valid = '1' else cycle_cnt;

  -----------------------------------------------------------------------------
  -- For each output, select the inputs with indices provided by
  -- c_permutation_table. A different set of inputs indices is used for each
  -- folding clock cycle.
  -----------------------------------------------------------------------------
  p_permute: process(snk_in_arr, cycle_cnt, common_paged_reg_src_out_arr)
  begin
    for i in 0 to g_nof_outputs - 1 loop
      if snk_in_arr(0).valid = '1' then  -- cycle_cnt=0
        -- First (or only) (folding) cycle; Take data present on snk_in_arr
        nxt_perm_2arr_2(i)(0) <= snk_in_arr(c_permutation_table(0, i)(0));
        nxt_perm_2arr_2(i)(1) <= snk_in_arr(c_permutation_table(0, i)(1));
      elsif g_nof_cycles > 1 then
        -- We're folding the permutation pairs; different outputs on next clock cycle(s).
        -- . Now take the data from common_paged_reg as snk_in_arr is no longer valid
        for j in 1 to g_nof_cycles - 1 loop
          if TO_UINT(cycle_cnt) = j then
            nxt_perm_2arr_2(i)(0) <= common_paged_reg_src_out_arr(c_permutation_table(j, i)(0));
            nxt_perm_2arr_2(i)(1) <= common_paged_reg_src_out_arr(c_permutation_table(j, i)(1));
          end if;
        end loop;
      end if;
    end loop;
  end process;

  -----------------------------------------------------------------------------
  -- Assert snk_out_arr(i).ready when we're folding.
  -----------------------------------------------------------------------------
  gen_snk_out_arr : for i in 0 to g_nof_inputs - 1 generate
    snk_out_arr(i).ready <= '0' when snk_in_arr(i).valid = '1' or (unsigned(cycle_cnt) < g_nof_cycles - 1 and unsigned(cycle_cnt) > 0) else '1';
  end generate;

  -----------------------------------------------------------------------------
  -- Both our input valid and sync need to be extended according to our folding
  -- level.
  -----------------------------------------------------------------------------
  u_common_pulse_extend_sync : entity common_lib.common_pulse_extend
  generic map (
    g_nof_cycles => g_nof_cycles
  )
  port map (
    clk    => clk,
    rst    => rst,

    p_in   => snk_in_arr(0).sync,
    ep_out => common_pulse_extend_ep_out_sync
  );

  u_common_pulse_extend_valid : entity common_lib.common_pulse_extend
  generic map (
    g_nof_cycles => g_nof_cycles
  )
  port map (
    clk    => clk,
    rst    => rst,

    p_in   => snk_in_arr(0).valid,
    ep_out => common_pulse_extend_ep_out_valid
  );

  -----------------------------------------------------------------------------
  -- Output
  -----------------------------------------------------------------------------
  p_output: process(perm_2arr_2, common_pulse_extend_ep_out_sync, common_pulse_extend_ep_out_valid)
  begin
    src_out_2arr_2 <= (others => (others => c_dp_sosi_rst));
    for i in 0 to g_nof_outputs - 1 loop
      src_out_2arr_2(i)(0).re    <= perm_2arr_2(i)(0).re;
      src_out_2arr_2(i)(0).im    <= perm_2arr_2(i)(0).im;
      src_out_2arr_2(i)(0).valid <= common_pulse_extend_ep_out_valid;
      src_out_2arr_2(i)(0).sync  <= common_pulse_extend_ep_out_sync;

      src_out_2arr_2(i)(1).re    <= perm_2arr_2(i)(1).re;
      src_out_2arr_2(i)(1).im    <= perm_2arr_2(i)(1).im;
      src_out_2arr_2(i)(1).valid <= common_pulse_extend_ep_out_valid;
      src_out_2arr_2(i)(1).sync  <= common_pulse_extend_ep_out_sync;
    end loop;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      cycle_cnt   <= (others => '0');
      perm_2arr_2 <= (others => (others => c_dp_sosi_rst));
--      snk_out_arr <= (OTHERS=>c_dp_siso_rst);
    elsif rising_edge(clk) then
      cycle_cnt   <= nxt_cycle_cnt;
      perm_2arr_2 <= nxt_perm_2arr_2;
--      snk_out_arr <= nxt_snk_out_arr;
    end if;
  end process;
end rtl;
