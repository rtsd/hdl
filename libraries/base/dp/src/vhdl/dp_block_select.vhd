-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author: Eric Kooistra, 14 Dec 2018
-- Purpose:
--   Pass on a range of blocks within a sync interval and drop the blocks
--   outside this range.
-- Description:
--   The blocks are counted from the sync using the eop. The blocks with index >= g_index_lo
--   and index <= g_index_hi are passed on. The blocks with index outside this range are
--   skipped by making there sosi control fields zero.
--   If g_index_lo > 0 then the sosi.sync and (global) sosi.bsn will be passed on via the
--   first output block. The sosi.bsn for the other blocks is not changed, so the (local)
--   sosi.bsn for the output is not restarted.
--   If g_index_hi >= g_nof_blocks_per_sync interval, then no trailing blocks are dropped.
--   The g_nof_blocks_per_sync is used to determine the size of the internal block counter.
--
-- Usage:
--   This component can be used to select a specific range of blocks within a sync interval
--   e.g. to capture one in a data buffer or to reduce the output data rate.
--
-- Remarks:
-- - This component supports flow control and was designed by keeping the functional
--   state registers and the pipeline registers seperate. Therefore the function is
--   implemented using combinatorial logic and local state registers to keep its
--   state. The combinatorial function output preserves the snk_in ready latency and
--   is pipelined using dp_pipeline to ease timing closure on the output.
--   A similar approach is also used with dp_block_resize.vhd.

entity dp_block_select is
  generic (
    g_pipeline             : natural := 1;
    g_nof_blocks_per_sync  : natural;
    g_index_lo             : natural := 0;
    g_index_hi             : natural
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- Control
    index_lo     : in natural := g_index_lo;
    index_hi     : in natural := g_index_hi;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_block_select;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

architecture rtl of dp_block_select is
  signal cnt_reg       : natural range 0 to true_log_pow2(g_nof_blocks_per_sync);
  signal cnt           : natural;
  signal sync_sosi_reg : t_dp_sosi;  -- hold snk_in.bsn
  signal sync_sosi     : t_dp_sosi;  -- hold snk_in.bsn
  signal block_sosi    : t_dp_sosi;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      cnt_reg       <= 0;
      sync_sosi_reg <= c_dp_sosi_rst;
    elsif rising_edge(clk) then
      cnt_reg       <= cnt;
      sync_sosi_reg <= sync_sosi;
    end if;
  end process;

  -- Preserve snk_in @ sync
  sync_sosi <= snk_in when snk_in.sync = '1' else sync_sosi_reg;

  -- Count blocks at sop per sync interval and restart count at sync
  p_cnt : process(snk_in, cnt_reg)
  begin
    cnt <= cnt_reg;
    if snk_in.sync = '1' then
      cnt <= 0;
    elsif snk_in.sop = '1' then
      cnt <= cnt_reg + 1;
    end if;
  end process;

  -- Resize snk_in combinatorially into block_sosi, so no impact on ready latency (RL)
  p_block_sosi : process(snk_in, cnt, index_lo, index_hi, sync_sosi)
  begin
    -- Default keep snk_in info and data fields
    block_sosi <= snk_in;
    -- Check for out of range indices
    if index_lo > index_hi or index_lo > g_nof_blocks_per_sync - 1 then
      block_sosi.sync  <= '0';
      block_sosi.sop   <= '0';
      block_sosi.eop   <= '0';
      block_sosi.valid <= '0';
    else
      if snk_in.valid = '1' then
        -- Drop blocks outside selected range
        if cnt < index_lo or cnt > index_hi then
          block_sosi.sync  <= '0';
          block_sosi.sop   <= '0';
          block_sosi.eop   <= '0';
          block_sosi.valid <= '0';
        end if;
        -- Move input sync and bsn to first output block
        if cnt = index_lo then
          block_sosi.sync <= snk_in.sop;
          block_sosi.bsn  <= sync_sosi.bsn;  -- bsn is valid at sop
        end if;
      end if;
    end if;
  end process;

  -- Register block_sosi to easy timing closure
  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => g_pipeline  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => block_sosi,
    -- ST source
    src_in       => src_in,
    src_out      => src_out
  );
end rtl;
