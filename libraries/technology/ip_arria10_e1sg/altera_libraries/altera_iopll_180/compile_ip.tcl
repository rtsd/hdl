#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

vmap  altera_iopll_180           ./work/


set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_pll_clk25/sim"
  vlog  "$IP_DIR/../altera_iopll_180/sim/ip_arria10_e1sg_pll_clk25_altera_iopll_180_fp6fpla.vo"  -work altera_iopll_180         

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_pll_clk125/sim"
  vlog  "$IP_DIR/../altera_iopll_180/sim/ip_arria10_e1sg_pll_clk125_altera_iopll_180_abkdtja.vo" -work altera_iopll_180          

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_pll_clk200/sim"
  vlog  "$IP_DIR/../altera_iopll_180/sim/ip_arria10_e1sg_pll_clk200_altera_iopll_180_qkytlfy.vo" -work altera_iopll_180          

# # Refreshing /home/hiemstra/git/hdl/build/unb2b/modelsim/ip_arria10_e1sg_jesd204b/work.ip_arria10_e1sg_jesd204b_rx_core_pll_200mhz(rtl)
# Loading ip_arria10_e1sg_jesd204b_lib.ip_arria10_e1sg_jesd204b_rx_core_pll_200mhz(rtl)
# Refreshing /home/hiemstra/git/hdl/build/unb2b/modelsim/ip_arria10_e1sg_altera_iopll_180/work.ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz_altera_iopll_180_4sgpama
# Loading altera_iopll_180.ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz_altera_iopll_180_4sgpama
# Loading altera_lnsim_ver.altera_iopll
# ** Error (suppressible): (vsim-3584) /home/hiemstra/git/hdl/build/unb2b/qsys-generate/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz/sim/../altera_iopll_180/sim/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz_altera_iopll_180_4sgpama.vo(155): Module parameter 'prot_mode' not found for override.
#    Time: 0 fs  Iteration: 0  Instance: /tb_tech_jesd204b/u_jesd204b/gen_ip_arria10_e1sg/u0/u_ip_arria10_e1sg_jesd204b/gen_jesd204b_rx/gen_jesd204b_rx_corepll_freqsel/u_ip_arria10_e1sg_jesd204b_rx_corepll_200MHz/iopll_0 File: /home/hiemstra/git/hdl/build/unb2b/qsys-generate/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz/sim/../altera_iopll_180/sim/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz_altera_iopll_180_4sgpama.vo

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz/sim"
  vlog "$IP_DIR/../altera_iopll_180/sim/ip_arria10_e1sg_jesd204b_rx_core_pll_200MHz_altera_iopll_180_4sgpama.vo" -work altera_iopll_180          

