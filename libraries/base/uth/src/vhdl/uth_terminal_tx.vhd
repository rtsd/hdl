--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Transmit SOSI data from n input streams via m UTH output streams.
-- Description:
-- Data flow:
--       snk--> dp_fifo_fill
--        in--> dp_packet_enc
--       pkt--> dp_distribute n --> m
--      dist--> dp_uth_tx
--       out--> dp_fifo_fill
--          -->src
-- Remark:
-- . The FIFO inside dp_distribute is not used, instead FIFOs directly at the
--   snk input can be instantiated.
-- . The input FIFO can be used if the sink does not support backpressure,
--   while the dp_packet_enc does apply backbressure to be able to insert the
--   packet header.
-- . The output FIFO can be used if the PHY link does not support data valid.
--   By filling the FIFO sufficiently it can be ensured that the UTH packets
--   are then output without data not valid gaps during the packet.

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity uth_terminal_tx is
  generic (
    g_technology            : natural := c_tech_select_default;
    -- Terminal IO
    g_nof_input             : natural := 4;  -- >= 1
    g_nof_output            : natural := 3;  -- >= 1
    g_mux_mode              : natural := 0;  -- default use 0 for non-blocking mux in dp_distribute, use 1 to preserve input order
    -- . DP Packet
    g_use_complex           : boolean := false;  -- when TRUE transport snk_in_arr im & re via DP data, else transport snk_in_arr data via DP data
    g_data_w                : natural := 32;  -- DP data width
    g_packet_data_w         : natural := 32;  -- DP packet data width, must be >= g_data_w, same width is also used for the Uthernet packet data (fixed packing ratio 1/1)
    -- . Uthernet
    g_uth_nof_ch            : natural := 2;  -- selects UTH TLEN based on the snk_in channel_lo part that is not in the DP Packet CHAN field
    g_uth_typ_ofs           : natural := 256;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
    g_uth_typ_arr           : t_natural_arr := array_init(256, 2, 1);  -- = (256, 257), array length must match g_uth_nof_ch
    -- Input FIFO passes DP sosi data
    g_input_use_fifo        : boolean := false;
    g_input_bsn_w           : natural := 1;
    g_input_empty_w         : natural := 1;
    g_input_channel_w       : natural := 1;
    g_input_error_w         : natural := 1;
    g_input_use_bsn         : boolean := false;
    g_input_use_empty       : boolean := false;
    g_input_use_channel     : boolean := false;
    g_input_use_error       : boolean := false;
    g_input_use_sync        : boolean := false;
    g_input_fifo_af_margin  : natural := c_fifo_afull_margin;  -- = 4, nof words below max (full) at which fifo is considered almost full
    g_input_fifo_fill       : natural := 0;
    g_input_fifo_size       : natural := c_bram_m9k_fifo_depth;  -- = 256
    -- Output FIFO passes DP/Uthernet packets
    g_output_use_fifo       : boolean := false;
    g_output_fifo_af_margin : natural := c_fifo_afull_margin;  -- = 4, nof words below max (full) at which fifo is considered almost full
    g_output_fifo_fill      : natural := 0;
    g_output_fifo_size      : natural := c_bram_m9k_fifo_depth  -- = 256
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(g_nof_input - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_input - 1 downto 0);
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(g_nof_output - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr : out t_dp_sosi_arr(g_nof_output - 1 downto 0)
  );
end uth_terminal_tx;

architecture str of uth_terminal_tx is
  constant c_packet_channel_lo : natural := true_log2(g_uth_nof_ch);

  constant c_complex_w         : natural := g_data_w / 2;

  signal sel_dat_sosi_arr : t_dp_sosi_arr(g_nof_input - 1 downto 0);

  signal in_siso_arr      : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal in_sosi_arr      : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal pkt_siso_arr     : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal pkt_sosi_arr     : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal dist_siso_arr    : t_dp_siso_arr(g_nof_output - 1 downto 0);
  signal dist_sosi_arr    : t_dp_sosi_arr(g_nof_output - 1 downto 0);
  signal out_siso_arr     : t_dp_siso_arr(g_nof_output - 1 downto 0);
  signal out_sosi_arr     : t_dp_sosi_arr(g_nof_output - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Forward sosi.data or complex fields as data
  ------------------------------------------------------------------------------
  p_snk_in_arr : process(snk_in_arr)
  begin
    sel_dat_sosi_arr <= snk_in_arr;
    if g_use_complex = true then
      for I in g_nof_input - 1 downto 0 loop
        sel_dat_sosi_arr(I).data <= (others => '0');
        sel_dat_sosi_arr(I).data(2 * c_complex_w - 1 downto c_complex_w) <= snk_in_arr(I).im(c_complex_w - 1 downto 0);
        sel_dat_sosi_arr(I).data(  c_complex_w - 1 downto 0)           <= snk_in_arr(I).re(c_complex_w - 1 downto 0);
      end loop;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Input FIFO (g_input_use_fifo)
  ------------------------------------------------------------------------------
  no_input_fifo : if g_input_use_fifo = false generate
    snk_out_arr <= in_siso_arr;
    in_sosi_arr <= sel_dat_sosi_arr;
  end generate;

  gen_input_fifo : for I in g_nof_input - 1 downto 0 generate
    gen_fifo : if g_input_use_fifo = true generate
      u_fifo_fill : entity dp_lib.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_data_w         => g_data_w,
        g_bsn_w          => g_input_bsn_w,
        g_empty_w        => g_input_empty_w,
        g_channel_w      => g_input_channel_w,
        g_error_w        => g_input_error_w,
        g_use_bsn        => g_input_use_bsn,
        g_use_empty      => g_input_use_empty,
        g_use_channel    => g_input_use_channel,
        g_use_error      => g_input_use_error,
        g_use_sync       => g_input_use_sync,
        g_fifo_fill      => g_input_fifo_fill,
        g_fifo_size      => g_input_fifo_size,
        g_fifo_af_margin => g_input_fifo_af_margin,
        g_fifo_rl        => 1
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sink
        snk_out   => snk_out_arr(I),
        snk_in    => sel_dat_sosi_arr(I),
        -- ST source
        src_in    => in_siso_arr(I),
        src_out   => in_sosi_arr(I)
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- DP Packet encoder
  ------------------------------------------------------------------------------
  gen_input : for I in g_nof_input - 1 downto 0 generate
    u_enc : entity dp_lib.dp_packet_enc
    generic map (
      g_data_w      => g_packet_data_w,
      g_channel_lo  => c_packet_channel_lo
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => in_siso_arr(I),
      snk_in    => in_sosi_arr(I),
      -- ST source
      src_in    => pkt_siso_arr(I),
      src_out   => pkt_sosi_arr(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- DP distribute: g_nof_input --> g_nof_output
  ------------------------------------------------------------------------------
  u_distribute : entity dp_lib.dp_distribute
  generic map (
    g_technology      => g_technology,
    -- Distribution IO
    g_tx              => true,
    g_nof_input       => g_nof_input,
    g_nof_output      => g_nof_output,
    g_transpose       => false,
    g_code_channel_lo => true,
    g_data_w          => g_packet_data_w,
    -- Scheduling
    g_tx_mux_mode     => g_mux_mode,  -- default 0 for non-blocking mux, but 1 also works in simulation and on hardware provided that the inputs are garantueed to arrive
    -- Input FIFO
    g_use_fifo        => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => pkt_siso_arr,
    snk_in_arr  => pkt_sosi_arr,
    -- ST source
    src_in_arr  => dist_siso_arr,
    src_out_arr => dist_sosi_arr
  );

  ------------------------------------------------------------------------------
  -- UTHernet transmitter
  ------------------------------------------------------------------------------
  gen_output : for I in g_nof_output - 1 downto 0 generate
    --u_uth_tx : ENTITY work.uth_tx(rtl_delay)  -- requires using g_fifo_af_margin >= 10
    u_uth_tx : entity work.uth_tx(rtl_hold)  -- can use default g_fifo_af_margin >= 4
    generic map (
      g_data_w      => g_packet_data_w,
      g_nof_ch      => g_uth_nof_ch,  -- g_nof_ch, the channels are numbered from 0 TO g_nof_ch-1, each channel represents a type of UTH frame
      g_typ_arr     => g_uth_typ_arr  -- g_nof_ch number of TLEN type values indexed by sink input channel, use unconstraint type to allow generic g_nof_ch
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => dist_siso_arr(I),
      snk_in    => dist_sosi_arr(I),
      -- ST source
      src_in    => out_siso_arr(I),
      src_out   => out_sosi_arr(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Output FIFO (g_output_use_fifo)
  ------------------------------------------------------------------------------
  no_output_fifo : if g_output_use_fifo = false generate
    out_siso_arr <= src_in_arr;
    src_out_arr  <= out_sosi_arr;
  end generate;

  gen_output_fifo : for I in g_nof_output - 1 downto 0 generate
    gen_fifo : if g_output_use_fifo = true generate
      -- Output FIFO passes DP/Uthernet packets, so the sosi control fields (sync, bsn, empty, channel and error) are encoded in the packet data
      u_fifo_fill : entity dp_lib.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_data_w         => g_packet_data_w,
        g_fifo_fill      => g_output_fifo_fill,
        g_fifo_size      => g_output_fifo_size,
        g_fifo_af_margin => g_output_fifo_af_margin,
        g_fifo_rl        => 1
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sink
        snk_out   => out_siso_arr(I),
        snk_in    => out_sosi_arr(I),
        -- ST source
        src_in    => src_in_arr(I),
        src_out   => src_out_arr(I)
      );
    end generate;
  end generate;
end str;
