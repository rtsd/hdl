-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2_board_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2_board_lib.unb2_board_pkg.all;
use unb2_board_lib.unb2_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_unb2_minimal_pkg.all;

entity mmm_unb2_minimal is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    reg_unb_pmbus_mosi       : out t_mem_mosi;
    reg_unb_pmbus_miso       : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_mosi           : out t_mem_mosi;
    eth1g_tse_miso           : in  t_mem_miso;
    eth1g_reg_mosi           : out t_mem_mosi;
    eth1g_reg_miso           : in  t_mem_miso;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_mosi           : out t_mem_mosi;
    eth1g_ram_miso           : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso
  );
end mmm_unb2_minimal;

architecture str of mmm_unb2_minimal is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  signal i_reset_n         : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                               port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_unb_pmbus       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_PMBUS")
                                               port map(mm_rst, mm_clk, reg_unb_pmbus_mosi, reg_unb_pmbus_miso );

    u_mm_file_reg_fpga_temp_sens  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;

  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_unb2_minimal
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_unb_sens_reset_export                 => OPEN,
      reg_unb_sens_clk_export                   => OPEN,
      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_reset_export                => OPEN,
      reg_unb_pmbus_clk_export                  => OPEN,
      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0)
      );
  end generate;
end str;
