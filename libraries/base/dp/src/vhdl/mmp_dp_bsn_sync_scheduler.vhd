-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author : E. Kooistra, 6 aug 2021
-- Purpose : MM peripheral interface for dp_bsn_sync_scheduler.vhd
-- Description: MM port register logic
--
--  wi    Bits  Access     Type   Name
--   0     [0]      RW  boolean   ctrl_enable, '1' is on, '0' is FALSE is off
--   1  [31:0]      RW   uint32   W: ctrl_interval_size
--                                R: mon_output_interval_size
--   2  [31:0]      RW   uint64   ctrl_start_bsn[31:0]
--   3  [31:0]      RW            ctrl_start_bsn[63:32]
--   4  [31:0]      RO   uint64   mon_current_input_bsn[31:0]
--   5  [31:0]      RO            mon_current_input_bsn[63:32]
--   6  [31:0]      RO   uint64   mon_input_bsn_at_sync[31:0]
--   7  [31:0]      RO            mon_input_bsn_at_sync[63:32]
--   8     [0]      RO  boolean   mon_output_enable, '1' is on, '0' is FALSE is off
--   9  [31:0]      RO   uint64   mon_output_sync_bsn[31:0]
--  10  [31:0]      RO            mon_output_sync_bsn[63:32]
--  11  [31:0]      RO   uint32   block_size[31:0]
--
-- * Do not use reg_wr_arr[0] in dp_clk domain to detect ctrl_enable_evt,
--   because reg_wr_arr pulses occur before the reg_wr data arrives in the
--   dp_clk domain. Therefore instead use change in wr_ctrl_enable value
--   to detect ctrl_enable_evt. Hence a re-enable via MM now requires a
--   disable via MM first.
--
-- Remarks:
-- * The actual bsn width is constrained by g_bsn_w.
-- * The MM interface accesses 64 bit words in two 32 bit MM word accesses.
--   This can result in a BSN value that is wrong if it happens to have
--   an increment carry from the low word to the high word. The BSN carry
--   occurs after every 2**32 blocks, so e.g. with a block period of 5.12 us
--   this is once every 6.1 hours. It is very unlikely that this will cause
--   a problem and if it does then a next attempt will succeed. Therefore
--   it is not necessary to ensure that the 64 values are accesses more
--   robustly (e.g. by only accepting them when the high word is accessed),
--   and therefore it is fine to use common_reg_r_w_dc for 64 bit BSN words.
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mmp_dp_bsn_sync_scheduler is
  generic (
    g_bsn_w                  : natural := c_dp_stream_bsn_w;
    g_block_size             : natural := 256;  -- = number of data valid per BSN block, must be >= 2
    g_ctrl_interval_size_min : natural := 1  -- Minimum interval size to use if MM write interval size is set too small.
  );
  port (
    -- Clocks and reset
    mm_rst          : in  std_logic;
    mm_clk          : in  std_logic;
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;

    -- MM control
    reg_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_miso        : out t_mem_miso;
    reg_ctrl_interval_size : out natural;

    -- Streaming
    in_sosi            : in t_dp_sosi;
    out_sosi           : out t_dp_sosi;
    out_start          : out std_logic;
    out_start_interval : out std_logic;
    out_enable         : out std_logic
  );
end mmp_dp_bsn_sync_scheduler;

architecture str of mmp_dp_bsn_sync_scheduler is
  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_reg         : t_c_mem := (1, 4, c_word_w, 12, '0');

  signal reg_wr_arr                : std_logic_vector(c_mm_reg.nof_dat               - 1 downto 0);
  signal reg_wr                    : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0);
  signal reg_rd                    : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');

  signal wr_ctrl_enable            : std_logic;
  signal wr_ctrl_enable_evt        : std_logic;
  signal ctrl_enable               : std_logic := '0';
  signal ctrl_enable_evt           : std_logic := '0';
  signal ctrl_interval_size        : natural;
  signal ctrl_start_bsn            : std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
  signal mon_current_input_bsn     : std_logic_vector(g_bsn_w - 1 downto 0);
  signal mon_input_bsn_at_sync     : std_logic_vector(g_bsn_w - 1 downto 0);
  signal mon_output_enable         : std_logic;
  signal mon_output_interval_size  : natural;
  signal mon_output_sync_bsn       : std_logic_vector(g_bsn_w - 1 downto 0);

  -- Resize BSN values to 64 bit
  signal wr_start_bsn_64           : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal rd_current_input_bsn_64   : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal rd_input_bsn_at_sync_64   : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal rd_output_sync_bsn_64     : std_logic_vector(2 * c_word_w - 1 downto 0);
begin
  ctrl_start_bsn <= wr_start_bsn_64(g_bsn_w - 1 downto 0);

  rd_current_input_bsn_64 <= RESIZE_UVEC(mon_current_input_bsn, 2 * c_word_w);
  rd_input_bsn_at_sync_64 <= RESIZE_UVEC(mon_input_bsn_at_sync, 2 * c_word_w);
  rd_output_sync_bsn_64   <= RESIZE_UVEC(mon_output_sync_bsn,   2 * c_word_w);

  reg_ctrl_interval_size <= ctrl_interval_size when rising_edge(dp_clk);

  -- Register mapping
  -- . Write
  wr_ctrl_enable                                  <=         reg_wr(                              0);
  ctrl_interval_size                              <= TO_UINT(reg_wr( 2 * c_word_w - 1 downto 1 * c_word_w)) when
                          g_ctrl_interval_size_min < TO_UINT(reg_wr( 2 * c_word_w - 1 downto 1 * c_word_w)) else g_ctrl_interval_size_min;
  wr_start_bsn_64(  c_word_w - 1 downto          0) <=         reg_wr( 3 * c_word_w - 1 downto 2 * c_word_w);  -- low word
  wr_start_bsn_64(2 * c_word_w - 1 downto 1 * c_word_w) <=         reg_wr( 4 * c_word_w - 1 downto 3 * c_word_w);  -- high word

  -- Derive ctrl_enable_evt from change in wr_ctrl_enable, instead of using
  -- reg_wr_arr(0), see description
  u_common_evt : entity common_lib.common_evt
  generic map (
    g_evt_type   => "BOTH",
    g_out_reg    => true
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    in_sig   => wr_ctrl_enable,
    out_evt  => wr_ctrl_enable_evt
  );

  ctrl_enable     <= wr_ctrl_enable     when rising_edge(dp_clk) and wr_ctrl_enable_evt = '1';
  ctrl_enable_evt <= wr_ctrl_enable_evt when rising_edge(dp_clk);

  -- . Read
  reg_rd(                               0) <= ctrl_enable;  -- read back internal ctrl_enable
  reg_rd( 2 * c_word_w - 1 downto  1 * c_word_w) <= TO_UVEC(mon_output_interval_size, c_word_w);
  reg_rd( 3 * c_word_w - 1 downto  2 * c_word_w) <= wr_start_bsn_64(          c_word_w - 1 downto        0);  -- low word
  reg_rd( 4 * c_word_w - 1 downto  3 * c_word_w) <= wr_start_bsn_64(        2 * c_word_w - 1 downto c_word_w);  -- high word
  reg_rd( 5 * c_word_w - 1 downto  4 * c_word_w) <= rd_current_input_bsn_64(  c_word_w - 1 downto        0);  -- low word
  reg_rd( 6 * c_word_w - 1 downto  5 * c_word_w) <= rd_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w);  -- high word
  reg_rd( 7 * c_word_w - 1 downto  6 * c_word_w) <= rd_input_bsn_at_sync_64(  c_word_w - 1 downto        0);  -- low word
  reg_rd( 8 * c_word_w - 1 downto  7 * c_word_w) <= rd_input_bsn_at_sync_64(2 * c_word_w - 1 downto c_word_w);  -- high word
  reg_rd(                      8 * c_word_w) <= mon_output_enable;
  reg_rd(10 * c_word_w - 1 downto  9 * c_word_w) <= rd_output_sync_bsn_64(    c_word_w - 1 downto        0);  -- low word
  reg_rd(11 * c_word_w - 1 downto 10 * c_word_w) <= rd_output_sync_bsn_64(  2 * c_word_w - 1 downto c_word_w);  -- high word
  reg_rd(12 * c_word_w - 1 downto 11 * c_word_w) <= TO_UVEC(g_block_size, c_word_w);

  u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain   => true,
    g_readback             => false,
    g_reg                  => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_mosi,
    sla_out        => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => reg_wr_arr,
    reg_rd_arr     => OPEN,
    out_reg        => reg_wr,  -- readback via ST clock domain
    in_reg         => reg_rd
  );

  u_dp_bsn_sync_scheduler : entity work.dp_bsn_sync_scheduler
  generic map (
    g_bsn_w                  => g_bsn_w,
    g_block_size             => g_block_size,
    g_ctrl_interval_size_min => g_ctrl_interval_size_min,
    g_pipeline               => 1
  )
  port map (
    rst                      => dp_rst,
    clk                      => dp_clk,

    -- M&C
    ctrl_enable              => ctrl_enable,
    ctrl_enable_evt          => ctrl_enable_evt,
    ctrl_interval_size       => ctrl_interval_size,
    ctrl_start_bsn           => ctrl_start_bsn,
    mon_current_input_bsn    => mon_current_input_bsn,
    mon_input_bsn_at_sync    => mon_input_bsn_at_sync,
    mon_output_enable        => mon_output_enable,
    mon_output_interval_size => mon_output_interval_size,
    mon_output_sync_bsn      => mon_output_sync_bsn,

    -- Streaming
    in_sosi                  => in_sosi,
    out_sosi                 => out_sosi,
    out_start                => out_start,
    out_start_interval       => out_start_interval,
    out_enable               => out_enable
  );
end str;
