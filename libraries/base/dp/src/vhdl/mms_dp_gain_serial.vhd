-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose : Set serial gains for one stream via MM
-- Description:
--   Wrapper of mms_dp_gain_serial_arr with g_nof_streams=1. See
--   mms_dp_gain_serial_arr for details.
-- Usage:
-- . See tb_mms_dp_gain_serial_arr which also tests this mms_dp_gain_serial

library IEEE, common_lib, common_mult_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_dp_gain_serial is
  generic (
    g_technology                    : natural := c_tech_select_default;
    g_nof_gains                     : natural := 256;  -- number of gains in series per stream
    g_complex_data                  : boolean := true;
    g_complex_gain                  : boolean := false;
    g_gain_w                        : natural := 16;  -- to fit 32b MM data use <= 32 for real, <= 16 for complex
    g_in_dat_w                      : natural := 9;
    g_out_dat_w                     : natural := 24;  -- 16 + 9 - 1 = 24 (-1 to skip double sign bit in product)
    g_gains_file_name               : string  := "UNUSED";  -- "UNUSED" or relative path to some "gains_#.hex" file, where # is the stream index
    g_gains_write_only              : boolean := false;  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode to save memory. When FALSE it is True Dual Port.

    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_product    : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_output     : natural := 1;  -- >= 0
    -- . complex multiplier
    g_pipeline_complex_mult_input   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_product : natural := 0;  -- 0 or 1
    g_pipeline_complex_mult_adder   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_output  : natural := 1  -- >= 0
  );
  port (
    -- System
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;

    -- MM interface
    ram_gains_mosi          : in  t_mem_mosi := c_mem_mosi_rst;  -- write side
    ram_gains_miso          : out t_mem_miso;

    -- ST interface
    gains_rd_address        : in  std_logic_vector(ceil_log2(g_nof_gains) - 1 downto 0);  -- read side, same read address for all streams

    in_sosi                 : in  t_dp_sosi;
    out_sosi                : out t_dp_sosi
  );
end mms_dp_gain_serial;

architecture str of mms_dp_gain_serial is
  signal in_sosi_arr     : t_dp_sosi_arr(0 downto 0);
  signal out_sosi_arr    : t_dp_sosi_arr(0 downto 0);
begin
  -- wires
  in_sosi_arr(0) <= in_sosi;
  out_sosi       <= out_sosi_arr(0);

  u_one : entity work.mms_dp_gain_serial_arr
  generic map (
    g_technology                    => g_technology,
    g_nof_streams                   => 1,
    g_nof_gains                     => g_nof_gains,
    g_complex_data                  => g_complex_data,
    g_complex_gain                  => g_complex_gain,
    g_gain_w                        => g_gain_w,
    g_in_dat_w                      => g_in_dat_w,
    g_out_dat_w                     => g_out_dat_w,
    g_gains_file_name               => g_gains_file_name,
    g_gains_write_only              => g_gains_write_only,

    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      => g_pipeline_real_mult_input,
    g_pipeline_real_mult_product    => g_pipeline_real_mult_product,
    g_pipeline_real_mult_output     => g_pipeline_real_mult_output,
    -- . complex multiplier
    g_pipeline_complex_mult_input   => g_pipeline_complex_mult_input,
    g_pipeline_complex_mult_product => g_pipeline_complex_mult_product,
    g_pipeline_complex_mult_adder   => g_pipeline_complex_mult_adder,
    g_pipeline_complex_mult_output  => g_pipeline_complex_mult_output
  )
  port map (
    -- System
    mm_rst                  => mm_rst,
    mm_clk                  => mm_clk,
    dp_rst                  => dp_rst,
    dp_clk                  => dp_clk,

    -- MM interface
    ram_gains_mosi          => ram_gains_mosi,
    ram_gains_miso          => ram_gains_miso,

    -- ST interface
    gains_rd_address        => gains_rd_address,

    in_sosi_arr             => in_sosi_arr,
    out_sosi_arr            => out_sosi_arr
  );
end str;
