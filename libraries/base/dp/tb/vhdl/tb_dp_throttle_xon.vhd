-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 13 Apr 2017
-- Purpose:
-- . Test bench for dp_throttle_xon
-- Description:
-- Usage:
-- > as 10
-- > run -all
--
-- Remarks:
-- . Derived from tb_dp_xonoff.vhd, tb_dp_example_no_dut.vhd
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_throttle_xon is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active or random flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active or random flow control
    -- specific
    g_restart_at_sync    : boolean := false;  -- if true restart xon control at each snk_in_sync, else and start at first valid and never restart
    g_block_size         : natural := 10;  -- number of valid data per block marked by sop and eop
    g_nof_block_on       : natural := 13;  -- number of blocks that snk_out.xon is active
    g_nof_block_per_sync : natural := 10;
    g_nof_clk_off        : natural := 37  -- must be > g_block_size, number of clock cycles that snk_out.xon is kept inactive
  );
end tb_dp_throttle_xon;

architecture tb of tb_dp_throttle_xon is
  constant c_nof_sync                 : natural := 20;
  constant c_nof_valid_on             : natural := g_nof_block_on * g_block_size;
  constant c_nof_clk_off              : natural := sel_a_b(g_nof_clk_off > g_block_size, g_nof_clk_off, 0);
  constant c_exp_nof_clk_off          : natural := sel_a_b(c_nof_clk_off > 0, c_nof_clk_off - g_block_size+1, 0);

  constant c_data_w                   : natural := 16;
  constant c_nof_repeat               : natural := c_nof_sync * g_nof_block_per_sync;

  constant c_init_bsn                 : natural := 0;  -- will increment
  constant c_init_data                : integer := 1;  -- fixed
  constant c_init_err                 : natural := 4;  -- fixed
  constant c_init_channel             : integer := 5;  -- fixed

  signal tb_end                     : std_logic := '0';
  signal tb_stimuli_end             : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal sl1                        : std_logic := '1';

  signal random_0                   : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1                   : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;

  signal prev_verify_snk_out        : t_dp_siso;
  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal prev_verify_snk_in         : t_dp_sosi := c_dp_sosi_rst;

  signal verify_hold_sop            : std_logic := '0';
  signal verify_en_sop              : std_logic := '0';
  signal verify_en_bsn              : std_logic := '0';  -- used as inout
  signal verify_done                : std_logic := '0';

  signal expected_verify_snk_in     : t_dp_sosi;
  signal exp_size                   : natural;
  signal cnt_size                   : natural;
  signal cnt_replicated_global_bsn  : natural;
  signal prev_out_bsn_global        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal prev_out_bsn_local         : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  signal xon_cnt                    : natural;  -- count nof valid='1' cycles
  signal xoff_cnt                   : natural;  -- count nof valid='0' cycles
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  stimuli_en           <= random_0(random_0'high) when g_flow_control_stimuli = e_random else '1';
  verify_snk_out.ready <= random_1(random_1'high) when g_flow_control_verify = e_random  else '1';

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    v_sosi.bsn                     := TO_DP_BSN(c_init_bsn);
    expected_verify_snk_in.data    <= TO_DP_SDATA(c_init_data);
    expected_verify_snk_in.channel <= TO_DP_CHANNEL(c_init_channel);
    expected_verify_snk_in.err     <= TO_DP_ERROR(c_init_err);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to c_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.sync := sel_a_b((unsigned(v_sosi.bsn) mod g_nof_block_per_sync) = 0, '1', '0');  -- insert sync every g_nof_block_per_sync

      -- Send packet
      proc_common_wait_until_high(clk, stimuli_src_in.xon);  -- wait for XON to avoid gaps in BSN increment
      proc_dp_gen_block_data(c_data_w, c_init_data, g_block_size, c_init_channel, c_init_err, v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      v_sosi.bsn := INCR_UVEC(v_sosi.bsn, 1);
    end loop;
    tb_stimuli_end <= '1';

    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 100);
    proc_common_gen_pulse(clk, verify_done);
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Start verify after first valid, sop or eop
  verify_en_sop   <= '1' when verify_snk_in.sop = '1' and rising_edge(clk);

  -- Verify that the stimuli have been applied at all
  proc_dp_verify_value("verify_snk_in.channel", e_equal, clk, verify_done, expected_verify_snk_in.channel, verify_snk_in.channel);
  proc_dp_verify_value("verify_snk_in.err",     e_equal, clk, verify_done, expected_verify_snk_in.err,     verify_snk_in.err);

  -- Verify that the output sync occurs when expected
  proc_dp_verify_sync(g_nof_block_per_sync, 0, clk, verify_en_sop, verify_snk_in.sync, verify_snk_in.sop, verify_snk_in.bsn);

  -- Verify incrementing global BSN
  proc_dp_verify_bsn(false, 1, 1, g_nof_block_per_sync,
                     clk, verify_snk_in.sync, verify_snk_in.sop, verify_snk_in.bsn,
                     verify_en_bsn, cnt_replicated_global_bsn, prev_out_bsn_global, prev_out_bsn_local);

  p_verify_xonxoff : process(clk)
  begin
    if g_flow_control_stimuli = e_active and g_flow_control_verify = e_active then
      if rising_edge(clk) then
        -- detect change in valid
        prev_verify_snk_in <= verify_snk_in;
        -- maintain counters
        xon_cnt <= 0;
        if verify_snk_in.valid = '1' then
          xon_cnt <= xon_cnt + 1;
        end if;
        xoff_cnt <= 0;
        if verify_snk_in.valid = '0' then
          xoff_cnt <= xoff_cnt + 1;
        end if;

        -- verify counts
        if g_restart_at_sync = false then
          if verify_en_sop = '1' and tb_stimuli_end = '0' then
            if prev_verify_snk_in.valid = '1' and verify_snk_in.valid = '0' then
              assert xon_cnt = c_nof_valid_on
                report "Wrong XON time"
                severity ERROR;
            end if;
            if prev_verify_snk_in.valid = '0' and verify_snk_in.valid = '1' then
              assert xoff_cnt = c_exp_nof_clk_off
                report "Wrong XOFF time"
                severity ERROR;
            end if;
          end if;
        else
          -- no verification yet for restart at sync, add when necessary
        end if;
      end if;
    else
      -- no verification needed, because the ready flow control does not affect the xonoff
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  u_dut : entity work.dp_throttle_xon
  generic map (
    g_restart_at_sync => g_restart_at_sync,
    g_block_size      => g_block_size,
    g_nof_block_on    => g_nof_block_on,
    g_nof_clk_off     => c_nof_clk_off
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- Frame in
    snk_out       => stimuli_src_in,
    snk_in        => stimuli_src_out,
    -- Frame out
    src_in        => verify_snk_out,  -- flush control via out_siso.xon
    src_out       => verify_snk_in
  );
end tb;
