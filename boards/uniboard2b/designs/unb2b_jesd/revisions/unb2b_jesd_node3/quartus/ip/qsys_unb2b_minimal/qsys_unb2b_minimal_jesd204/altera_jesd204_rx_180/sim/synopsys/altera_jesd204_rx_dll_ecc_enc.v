// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
gmKqY6PwIk/KEtG4PHCm3JZw6nrnfaJReItlkFSoZFpZm/aD/umzQhC7axpG6vwd
pUEXfsEuWjVSRaYgrzD564xeaJFnwzJzZbnPMtHtnN6VZQDv4bXBwd+Cf6TM9HA1
ru/4wlHq/74+NIU4v8fGUU/GCNbD6sBVvvDOOkjK0lY=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 4864 )
`pragma protect data_block
Y2o5biDExuhbKORw0KuNboSppcu5/SeMPEuhPgrjL1G+DEg+2iKTqXcB2bjQAKR5
bfxevfbb8YE1ngsnsxiNRitD2XU32n54dtnTdDMo3Cke9NWWWA3PnOyHJQiWwTwH
KpPndVxjyJiS0nYhShd9ZQHYbKTInDMzi9gsTMQdobGy/03qGxuJtk8OTPHAOfkY
LepzkXRSHzWlPUNiyKiUH9Dhh7L6AU3rywr73ziF+ysrtqzlY0a4gD9EXz6Y3syR
0ydtZQ+VbtFr7/ZcgpSVYN+UkIADzj/+CxbJBnrLgOaS0RI1RrOk08BdeRWI+nVa
1+8hXugogB0d6OxW9AxwmfFuIy4ZNtvfrK69tFg/56R7MgRdzgU4kOSVUPLSjMju
KGNZM+HgieNuM75bVkUrd7I8eyB7JfjrEeqpV+B2+va2vdof3s8bdFQr70uomzaF
lKI9SbqPhA9n5PGyx8QjB4yyNcw+CcMTXebhEJljEy6vMpK9DZuf4Tmy5FT7LbWV
F5UoKNifuCEiKS9R2M6Hyk3cQGa6+HIT0TtiqydWl+Q430U49rwnzJeJmGnXFz9t
d02qd5wtlP8NFsu85ApgAI9S2hvoK7bJ6WD2lITrHgiJIYlBXKuYp1ztngyoliWn
nM5w/B/k9gWgZ2fjEiHuIkF2/kL7B5vCRzqdzWNfzq9Xdda9Mb+rOhdLM8U62qr/
5AMTtGTSoJ2nCKG2fsBauJGI22xbKhvLtTuVi69PW1SgvHw+U0uWeeTvBJBRac86
n6OrnMl10DmN+22rIgvYeQ/E42q+ZO0i/SMFQ37MHiRsq5qb6MjFRlT8X6vgYqbJ
1uFhKxOZg3bD8CPsFpq70A4OcIXOzGb+colVuZ81V8jHLgfdDZtHVzg2bT2JlmfS
pbL1bUJ7RdsR7y0JwoZmuVR05SAKz35KMOQpp6GPCQgrg3vJDgFpjKPdeZL+ADmC
m3VCSqSYZxr+WpPaqIrxRKmtGE9kU4QtNzhcmauWUH3/beWFO3DAfo7YkU40aOwe
4SjvEYqmNtEDP/nZUK1e69ZGsnN5jOLuoy/ZD8VWNA534VdMD9v9mm2BH/rHEJmS
V00lebiTrIKesKgQGgpWD8wAsIXl4PWWRqp5BD+w2mPq+zCzJNMwwcdhshrSoCWx
Uz5P4rZ9onmfgvRJAFhL1eOT7sSXRY98aXnrAJ4ycfdwd6OwGdeg6zAdNMGJ3f7x
9PAfxBqFs+5FPVTTMB/5k7iD2zmeNtl0QUvcO59dNtVhsDgnzO7aarxSSrXiutWm
w2XEHZHhORJa+eUy5+Hdq/AIRYxyMXY+1NVNSzvgWE4gk3XfJEM4/mLDlE/bJvC+
ksAupRbQLJ5+SM0k1oKfWZgEAAtFTdxn+Fbf80UayqSQseIS4JDj9JULxhZp4FbS
jWmvnNyu0TBtDhainfH2yUQR+1gX/28xEk+LLiZGM1Dtw9g+z5jcHoLgiMDkdI0f
5QmMXVQNQqkBuJZmufmQsFmRFw40qn6tvtlRBhZ9n2kvJSW1cwhmoGQ194h0Cn3I
T2vMqsErboSKXUfk54OnweUZi3Uar/RnlIVVFgeA3cbT1416DHMSvC1Ylf8vwwbp
UNKmO7QGDVZZNR06mFb1bqqLlceVG/sV4zv7ZjHU8pxF7Ik7NYOqvVCCT2BjK69D
mZxvytTN8noI7R3HfOX6E8hd1xDm3lpEt51Wb31a64mavjlmKQ3iFcKec2R+HUtA
ft5j2TBEZABtim5cSLpvJyt8y6Jn5YtVH3yDP4l/aaoip8v6xaJ+WV8ISMsinfGZ
OXYcSQktJvZ4kXgAa5Xt5QGwEcOgMwtNaU5R4MUgc+ORVM4687X+zBxSXjl9Th/A
cw0x5wVMRP4Z44iDoS1eOskD6aMcBlZS7iOqJHZ/qIB95OSeYRJ4WyXjzO0U7EHi
Ko4BiU6QALhsvv/etBVTzsL4DBgA21KhaPdIzEPqlKLwM8dFHPVl3TKaGuvUH/H6
IIG9/JCFiTq/uXoCxqJWqnrf6/m1xZAeRLdDdVREoHlXS9hkOCgA/TxIy9M2JSP5
2iODPZSBlyVrd0V6olr6OxlFnsWbbwZj9R9MU7rn5N9Y73+yRHznM4DKTsZUikqI
e9NzZlxmM1LlnDPuiffDMDlx3zIYBnQk+2KTg4IT0aW7T02kScbcMUB69x/3JD03
Ct2dHm5KarHW1AxqbwQ/GUI2lucSsGguEYQkTH3ERR5nWzBKjFMi+QheYe4xFNWq
FmSO66rnDTQrpnlhN/dH/x/I+gaSsuiyaVtZ1KJT+xeUPsb8O/gTuIKX2Ni6tdXu
6taE+LWxNny6ppjD9EeUsbEMRWzmX0e4WJLfAbhScQ+qirngER9xud8CfHvCEGGU
pXEcR0709UEovd4kQljFqcb5w0z34YjxQQbi92JsNNBTo3FW3yP3Rcxu9xNWYCh6
vdCsOYtT5vjoSwbyvEHYbsGA5dmSQ63h1eNoziqVIyPi8WWcmqXZCCfDXb03FYO/
3rHUzH7iWaxxectbSk73O4VDYAUIgWbuxY0M1LwH7E9WOGiksEMlqBiohfRATQI8
BU0lYsGjCGL1znzt0ewVH+lbIk6a0RxFd5reQASLCDaPf4/IE6tSDsGrmBWjZVd6
BNDnFgs8a+Qq5LM3b2mH3weKWOw/ZFWtPNlcsY51bH+9LaxwehjMQySWnEUqiNeE
wgD6MghB4e9/sZV8FF/VdsFmnnwcW+DQL0IPpS5pea6wqy7SVd68SIrnE3rB5WYC
Lbon66zRGLJ5/6ge/xo/Sr1Ul9A4tuEkGoWEDvoKHbqjZAUmGQ5vgM0HUuPhLGTn
em7Dic2xTxg07q3CWm3JLlApkL/qSNs+4+MXi9bUNejwcztkvqywgpYzx0d2Vdnu
O0b4pK/qr+PWkv/6NGyjsNDcsVqC9fSGwa8B15+Y3+ncjRpF4tMIDGL1cyMdKLy2
/98uCI9xfP5zHFv5ZcDECjlwem/UQX6JfextsS7TfKHzLGM7eb90nYdxDvNaXOnZ
s42QqbclKR8yY2HB0N9c2WB9emYfCWf/42dOapVrzFfmfqwspIiy/ZPyGJLU8txC
AR642Ow1ZrcRGItIn+0NjFltU9sB8NSeoROrtTzKeN8egHQRg2qLvg3XPG6vvaxY
FGSZ9flzLkSmObm6RyLyY1N6mEyFm/osLpDkNVRu85QB1diJk5ybJ6vcFplm71kC
7aj8YmFVSimL2FNrTOrggEimIQ4c7ih6ZrV80T6Byh+6Mo+Ls4nLDr3fNbMjuqhO
MLeFOeASNNlG1MFJxIxIelT3mPlsCbDfvd0Y/cuTD8EJbNJskibc3anmXxGob9F/
B3YBcw6isFNuWAdmsbBXtc4SElHZSAwSUAmgi979mYwiVrn6BXGD31PSe8MUb9Ec
LOjGZiiYSPIp2mAeAqGCLOhSdiEnHE/If7jjzzrCNs4dz+EuiVv4KXLvSbomr4DM
+B/PRMSWKeAlxTjLAE9JDKUqUKyn5vgsEB2eNrwjxiz+I0YQxrrdvf88ojenyU/b
7BkALV6Qn6UojOfZmoazXjpeC+GL3dHQr+GTaOOClJ0q34Y3rXwcFOuC4bUn1iO9
mhXYtttcvj+CdGUKecswIo7em4wMLHlUOx0TL/aZYn1Qhr/0tVdYOakSb2cDsx81
hG2tdpFbUKa0keBvlFVMAheQzB+YLQvX731sQJb1S0pFr/oALqRODs8gzyQXSDna
UXhBozSFWEKFPQh+Ckg2cEG21NgBohiQ1khtVO8soQrGrpNNf+2kPXlo+g4uTwEu
3JjGcp878LD1ETFKhbmvbFQTyA0dZpr97zucH1+AwB7zS83ADv72Kw1qVeDw/Ts+
mNGvSfbRyZ2rFJ39OBjp7yvzruDcdxXUSx90zSElMCTRLWB6O8tA5YgPQKvrJubP
1Mamo8feSXUHUJ/2COlpDZQyraDM5p1MjgPjRE22gfaWSIUGU4Ss+Jaw3MvYCelA
XuZ5KP5SUg7bLMOIi/SNJK6Q02tTxWYR+RsS9PlR9cyjEswHmtjPDtybmMxVjnrY
n/dsxxnOHQrp0fDHjad2x/K6OSMHBhHPbWxyjcHtBlVd+0ufaFpaE8P0SHeYZ9Ee
F0rfmtbm1/hkUPjGycAOBmAH0DF8OpeTA7qKdvjUUkbI6Qr4CWZshppkU9ICYrOb
ez2rEgiU5jafkafIPzGpL5wwK6ZuY85jRnF/LnVs8rq/7fnbqQYRfVCNqLl4V2y3
YhxBSP8B5dZWmat855GOLMgwQ06GiZAE76Bqy/VCRk8hRCBUu2K9W7PeqoPNJrVS
JgJV0tPTUYVmhBNzAG6gRe8XKE6rNxXJnmxM1JQQK0doJO4Pb8TfkzmOodMVvbZz
q/9j8stKB/W16mb3h+trDUhQXHZVcTCP/gsDoZVhalLzdFoLXiG/pDRf7z1qc0R+
hOTs3HvtY+JZ6QV0a9w+eFBUfNUAjrcJREyEJe8CxPKCWDuUWUeuW/kn+HIY0SQi
q3FTshlp1kdI3o/ieXazc4tsvYj36smjZ8Uj+gXQdiO/a1dgYWbqM0kkJd4QwJKl
BMwHT60urh1DLJ7Eq3s3QI4K/yk96ONZERH363qbHAUdDmlCTSI9PUjfIAaWLvFl
8W4xEUHAlu6rJFSctSnfYNoEuL31x08TQp77Bg87hDJjQ9mf3Wo9zq+LlBd6wZll
O9+7PT4boUxnTHetFday8Vt9qaUXWTk0oGyZ0dT6ph2AfuIVzcNELHjWAT92XyOz
lLCSd90T/ZNPl1pp/WkdWhiy8lxOhUF7Z6N1NVDT32imTdkXJg7d5SK0bhNle64v
nvmT4qjQrhy/ugjzi8c+XfiqnJbOw+6Hoy+IXPTopCqmYzMps6uiOX0Jg9Cg7P2v
C0SiPWCCWN09joRGImEanIbZ+1H6W8zhJG87c4RrE49ixXv1Kxf2d9EMXSukAjvJ
b3j/kWo/WHl2CUlj35TDGgjGcoY1wdc4xNeLno20+6ivDL6hVNAdmbVSKSH+QbTE
/v6PpViYnYeSmn1htvJIPR54Nlc5TYkRVuP1XGWqXDFRwDfcpEr2Jn1F0BlcjMap
15yCDj662pAsI301NZt6AI6Fpob7Q87sKYSdPpgXedMbe0weHvk05YP1FSJcFdU7
O6z3u39hins4hYUnJXv8+Q7l37uoVxgZ0Y30zfoUGg+lEdfVyJzOnb7ANWzcDe58
N6aPldSQAGpOs2IOI2uSHXyOVu4tigrtnqnEuufd9m9agKkkShLnyRAn5fM8n23i
1y60BHRqxpNNqK3UasR4l5pkt9d3AX8hB4McjzdhtYugSEpzY1yyZ1llZ0nbbPRo
+X669AsJUpij3wem6ZbiI6Nxv7dbGLA3oPQm3s6RKadd530Pax23ShBanfmfZGOQ
eLV+hZcA7xzoK1dOxjZkVuTPOug//u0IX/8X+SxbHCXA30cZnncYe6bZp/NPpjsX
SQvBykbyAhG2cqgL88gccLEA0QA2i9KiunDKYKA2zkqcs22ppaKLx0zynS6sYraf
VZZ79DvDt3O0n2pMoX/UvgAzpWytxFbglGxNIvpgdbYXnaH6BsXkvjcfCc4OwJ1o
DqKQNd4f5Y6WDzXkj49YMHyGl8eXboWQ72sNqWqShwOhzGkSLbHv5+NsAIQ3jKHL
Aakbg/WzqrCfKhPvIxPr0z2jz7INoAQVIcHdlbnmBMBCb1nSzFJ3DYxhZ8siaQuw
wvYT4hSEydktUb/uBJNphlrsrLEXK1n2Tvm1RuA4nzb4A4thRRhCUZSQuzKqfE2D
ktMUYRsEz3btFsQ5Gbt8HLCTEqxh95QQCIsKVdP/UTbLQwu19u65KfVndwr99xXt
mH5GsC68dDTzm0ZA7wLIi7U0UzIf7fiDQGtQlOx8kQX3mNfr2DBQcIRryQyvdGWB
cSuUiRaelEtILaOGH0ChXmQZDvmzmyuE7hPu/a6+QCfGwNhb6D34C+jqUYzpLqy3
3/WrTRKfi2YE4SQWAcZzrttP15B7tu8aky3cYtQL5LrQDIgdoshKSJTig7mjLYt+
4ykzyTOVeo4sdHMoh2Qh+mA99WFUp6inl0Vb3sVO4x7lOdO48awXOk3E9TcOJcH4
zhPiuOqq0bH+YidVHDKaJu6k8foYzrAo8KwahiLWEcRPK+qi0ZMxc93FDBoZREHz
s+OvuNfGSmJx3xrgWHdEjlANVsxtYgF6QJj8BagL+Iv6971QnUk5gk9Y0RK+oNtj
JO1zy6kJJH31j53bwqKCK9XDbK3VLrC3RDtsUmC9Erbn+9stGHdIDptEBy6XAbr+
6i/Q5Q+oYZlM5g/kJiEW9zja+GLeT3TCIueHuU/RdhXo8G3oTEiCuuZRUtQIImfp
Zqd7+cWYgR+PgmeP7zX5XE3qceoa7HHIEUxD07LhJO4KJ/vyib0eFMjK8eSm8xrr
HqiQ7C9zGQQG+SyyXBHtDh8qb55BeZ4uD9+BnIQqGJalpgfrIaE3p7ln2QqsKv8U
Ks7hlFEHJhnj9hgELKbhAg==

`pragma protect end_protected
