Title: How to use OpenCL with UNB2b

Author: Reinier vd Walle


INDEX
1. OVERVIEW
2. SETUP ONCE
3. COMPILING EXAMPLE OPENCL APPLICATION FOR EMULATION
4. COMPILING EXAMPLE OPENCL APPLICATION FOR UNB2B
5. CREATING NEW OPENCL APPLICATION
6. FLASH SOF TO FPGA
7. FLASH RBF TO FPGA
8. CREATING A NEW BSP


1. OVERVIEW
The ta2 project folder contains 4 sub-folders:
- doc
    . The folder that contains this file.
- designs
    . This folder contains all TA2 OpenCL applications.
- bsp
    . This folder contains OpenCL board support packages.
    . The sub-folder "hardware" contains the list of available BSPs
- ip
    . This folder contains VHDL libraries that are used in BSPs


2. SETUP ONCE
- Install Quartus 19.2 with arria10 dependencies and OpenCL dependencies. A later Quartus version may be used 
  for OpenCL compilation. However, the current BSPs are created for 19.2.
- Aquire the RadioHDL library from GIT or other source.
- Export the following environment variables in your .bashrc and source the radiohdl init script as follows:

    INTEL_ROOTDIR=/home/software/Altera/19.2 # For example
    export QUARTUS_ROOTDIR=$INTEL_ROOTDIR/quartus
    export QSYS_ROOTDIR=$INTEL_ROOTDIR/quartus/sopc_builder/bin

    export HDL_WORK=${GIT}/hdl
    export RADIOHDL_CONFIG=${GIT}/radiohdl/config
    export HDL_BUILD_DIR=${HDL_WORK}/build

    export INTELOCLSDKROOT=$INTEL_ROOTDIR/hld
    export INTELFPGAOCLSDKROOT=$INTEL_ROOTDIR/hld
    export AOCL_BOARD_PACKAGE_ROOT=$HDL_WORK/applications/ta2/bsp
    export PATH=$PATH:$INTEL_ROOTDIR/hld/bin
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INTELOCLSDKROOT/host/linux64/lib
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$AOCL_BOARD_PACKAGE_ROOT/linux64/lib
    export PATH=$PATH:$QUARTUS_ROOTDIR/bin
    export LIBGL_ALWAYS_SOFTWARE=1
    export LIBGL_ALWAYS_INDIRECT=1

    # Altera, Mentor and modelsim_altera_libs dir
    export ALTERA_DIR=/home/software/Altera
    export MENTOR_DIR=/home/software/Mentor

    . ${GIT}/radiohdl/init_radiohdl.sh


- Make sure the hdl_buildset_unb2b.cfg is correctly configured for this Quartus version. 
- Generate all IP by executing:
    -> generate_ip_libs unb2b


3. COMPILING EXAMPLE OPENCL APPLICATION FOR EMULATION
The example application used is "ta2_unb2b_qsfp_demo", this application generates UDP packets and outputs them 
to 40GbE and 10GbE. In emulation, this design is verified by comparing the 10GbE and 40GbE outputs to check if 
the data is identical. If that is the case, the output will state "PASSED".

- Navigate to $HDL_WORK/applications/ta2/designs/ta2_unb2b_qsfp_demo
- First we need to compile the OpenCL application for emulation and compile the host code. This can be done by
  the provided Makefile. Execute the command:
    -> make ta2_unb2b_qsfp_demo
- This creates the executable "host" and the aocx file "ta2_unb2b_qsfp_demo.aocx" located at:
  $HDL_BUILD_DIR/unb2b/OpenCL/ta2_unb2b_qsfp_demo/bin
- First navigate to that directory and then run this executable using the following commands:
    -> cd $HDL_BUILD_DIR/unb2b/OpenCL/ta2_unb2b_qsfp_demo/bin
    -> CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 ./host
- At the end of the application output you should see "PASSED".
  

4. COMPILING EXAMPLE OPENCL APPLICATION FOR UNB2B
The example application used is "ta2_unb2b_qsfp_demo", this application generates UDP packets and outputs them 
to 40GbE and 10GbE. On hardware, you can verify the design by connecting a PC to the 40GbE (QSFP1) or 
10GbE (QSFP0) output and check for incoming packets using tcpdump.
- Navigate to $HDL_WORK/applications/ta2/designs/ta2_unb2b_qsfp_demo
- This example design uses the "ta2_unb2b_bsp" OpenCL BSP. If you did not initialize the BSP already, 
  execute the commands below.
    -> quartus_config unb2b; run_qsys unb2b name_of_your_BSP board.qsys;
- The OpenCL application can be compiled for UniBoard2b using the provided Makefile, this is done by executing:
    -> make ta2_unb2b_qsfp_demo.sof ta2_unb2b_qsfp_demo.rbf
- After a long compilation time (can take hours) you will find the files ta2_unb2b_qsfp_demo.sof and
  ta2_unb2b_qsfp_demo.rbf in the current directory.


5. CREATING NEW OPENCL APPLICATION
- Start by copying the example application located in $HDL_WORK/applications/ta2/designs/ta2_unb2b_qsfp_demo
- rename the folder and ta2_unb2b_qsfp_demo.cl
- If you need to use a specific OpenCL BSP, open the Makefile and change the BSP Name into the name of the BSP 
  you want to use (see OVERVIEW).
- If you did not initialize the BSP, execute the commands below.
    quartus_config unb2b; run_qsys unb2b name_of_your_BSP board.qsys;
- Now you can create your application in the *.cl file.
- With the provided makefile you can compile your application for UniBoard2b by executing:
  make myApp.sof myApp.rbf
    where "myApp" is the name of your .cl file
- Note that by default your application is build in $HDL_BUILD_DIR/unb2b/OpenCL/myApp
  where "myApp" is the name of your design directory file. This directory includes the 
  Quartus project for analysis using the Quartus GUI.
- For emulation, you need to modify the host code located in host/src/main.cpp to fit your design. 


6. FLASH SOF TO FPGA
The quickest way to program the FPGA is to use a JTAG connection and program the FPGA with the Quartus
programmer, writing the .sof file.
- To configure a jtagserver you can use the command:
  jtagconfig -addserver <server name> <password>
  
- To program the FPGA use the following command:
quartus_pgm -c USB-BLASTERII -m jtag -o p\;my_app.sof@1

The above command will program FPGA 1 of Uniboard2 with my_app.sof.
FPGAs 2, 3, and 4 can also be programmed by changing the FPGA ID. For example
For FPGA 2 -> quartus_pgm -c USB-BLASTERII -m jtag -o p\;my_app.sof@2
For FPGA 3 -> quartus_pgm -c USB-BLASTERII -m jtag -o p\;my_app.sof@3
For FPGA 4 -> quartus_pgm -c USB-BLASTERII -m jtag -o p\;my_app.sof@4

Multiple FPGAs can be targeted simultaniously for example, programming FPGAs 1,2 and 4:
quartus_pgm -c USB-BLASTERII -m jtag -o p\;my_app.sof@1 -o p\;my_app.sof@2 -o p\;my_app.sof@4


7. FLASH RBF TO FPGA
If a JTAG connection is not available or you want your application to stay in flash, the application can be 
written using the .rbf file over a 1GbE connection. This is achieved by running the util_unb2.py peripheral 
script in $UPE_GEAR/peripherals


8. CREATING A NEW BSP
- BSPs are located in "$AOCL_BOARD_PACKAGE_ROOT/hardware" which is defined as
  "$HDL_WORK/applications/ta2/bsp/hardware"
- To create a new BSP it is easiest to copy an existing one. In this example we would make a copy of the 
  directory "ta2_unb2b_bsp" and rename it to example_bsp.
- inside the new folder "example_bsp" we need to change 2 files: board_spec.xml and hdllib.cfg
- In board_spec.xml replace the two occurrences of "ta2_unb2b_bsp" into "example_bsp" (lines 2 and 5).
- In hdllib.cfg replace "ta2_unb2b_bsp" into "example_bsp" and "ta2_unb2b_bsp_lib" into "example_bsp_lib"
- Now we can make changes to this BSP by editing top.vhd, this is very similar to normal unb2b top-level 
  files. The difference is that top.vhd uses the "board" component defined in top_components_pkg.vhd 
  instead of the usual mmm component. You will also find another component "freeze_wrapper" is used which 
  is defined in top_components_pkg.vhd.
- freeze_wrapper is the OpenCL kernel wrapper. which is located at ip/freeze_wrapper.v. "freeze_wrapper" 
  instantiates pr_region.v which instantiates the generated "kernel_system" (OpenCL kernel).
- IO channels are defined in board_spec.xml between <channels>  ...  </channels>. These lines define 
  what in- and outputs the OpenCL kernel will generate. When you change these IO channel definitions, you 
  need to modify the files ip/pr_region.v, ip/freeze_wrapper.v, top_components_pkg.vhd and top.vhd accordingly.














