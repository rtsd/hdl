-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mmd_slave is
  generic (
    g_st        : std_logic_vector(1 downto 0) := "00";
    g_prtad     : std_logic_vector(4 downto 0) := "00000";
    g_devad     : std_logic_vector(4 downto 0) := "00001";
    g_quick     : boolean := false;  -- TRUE to support MDIO quick access using devad[4:0] as register address
    g_reg_addr  : natural := 16#A001#;  -- support only this one PHY register address
    g_reg_dat   : natural := 16#1234#;  -- initial PHY register value
    g_reg_w     : natural := 16
  );
  port(
    mdc         : in    std_logic;
    mdio        : inout std_logic;
    addr        : out   std_logic_vector(g_reg_w - 1 downto 0);  -- access address
    reg_dat     : out   std_logic_vector(g_reg_w - 1 downto 0)  -- the one data register
  );
end mmd_slave;

architecture beh of mmd_slave is
  constant c_preamble_len       : natural := 32;
  constant c_preamble_timeout   : natural := 1;  -- >= 0
  constant c_header_st_len      : natural := 2;
  constant c_header_op_len      : natural := 2 + 2;
  constant c_header_prtad_len   : natural := 2 + 2 + 5;
  constant c_header_devad_len   : natural := 2 + 2 + 5 + 5;
  constant c_header_ta_len      : natural := 2 + 2 + 5 + 5 + 2;
  constant c_data_len           : natural := 16;

  constant c_hdr_op_addr        : std_logic_vector(1 downto 0) := "00";  -- operation code address (typically not used when g_quick = TRUE)
  constant c_hdr_op_wr          : std_logic_vector(1 downto 0) := "01";  -- operation code write
  constant c_hdr_op_rd          : std_logic_vector(1 downto 0) := "11";  -- operation code read
  constant c_hdr_op_rd_incr     : std_logic_vector(1 downto 0) := "10";  -- operation code read increment

  type t_mdio_state is (s_idle, s_preamble, s_st, s_header, s_header_ta_rd, s_write, s_read);

  signal mdin           : std_logic;

  signal state          : t_mdio_state;
  signal op_ad          : std_logic;
  signal op_wr          : std_logic;
  signal op_rd          : std_logic;

  signal i_addr         : std_logic_vector(g_reg_w - 1 downto 0) := (others => '1');
  signal i_reg_dat      : std_logic_vector(g_reg_w - 1 downto 0) := std_logic_vector(to_unsigned(g_reg_dat, g_reg_w));
begin
  addr <= i_addr;
  reg_dat <= i_reg_dat;

  mdin <= not(not(mdio));  -- force 'L' to '0' and 'H' to '1' to ensure proper value interpretation

  p_mmd : process (mdc)
    variable v_bit_cnt : natural := 0;
    variable v_st      : std_logic_vector(1 downto 0) := (others => '1');
    variable v_op      : std_logic_vector(1 downto 0) := (others => '1');
    variable v_prtad   : std_logic_vector(4 downto 0) := (others => '1');
    variable v_devad   : std_logic_vector(4 downto 0) := (others => '1');
    variable v_dat     : std_logic_vector(15 downto 0) := (others => '1');
    variable v_mdout   : std_logic;
    variable v_match   : boolean;
  begin
    case state is
      when s_idle =>
        v_mdout := 'Z';

        op_ad <= '0';
        op_wr <= '0';
        op_rd <= '0';

        v_match := false;
        v_bit_cnt := 0;

        if falling_edge(mdc) then
          state <= s_preamble;
        end if;

      when s_preamble =>
        if rising_edge(mdc) then
          v_bit_cnt := v_bit_cnt + 1;

          if v_bit_cnt = c_preamble_len then
            v_bit_cnt := 0;
            state <= s_st;
          end if;
        end if;

      when s_st =>
        if rising_edge(mdc) then
          v_st    := v_st(v_st'high - 1 downto 0) & mdin;

          v_bit_cnt := v_bit_cnt + 1;

          if v_bit_cnt >= c_header_st_len then
            if v_bit_cnt <= c_header_st_len + c_preamble_timeout then
              if v_st = g_st then
                v_bit_cnt := c_header_st_len;
                state <= s_header;
                v_match := true;
              end if;
            else
              report "Preamble too long, not supported by this slave model"
                severity FAILURE;
              v_match := false;
              state <= s_idle;
            end if;
          end if;
        end if;

      when s_header =>
        if rising_edge(mdc) then
          v_op    := v_op(      v_op'high - 1 downto 0) & mdin;
          v_prtad := v_prtad(v_prtad'high - 1 downto 0) & mdin;
          v_devad := v_devad(v_devad'high - 1 downto 0) & mdin;

          v_bit_cnt := v_bit_cnt + 1;

          case v_bit_cnt is
            when c_header_op_len =>
              case v_op is
                when c_hdr_op_addr => op_ad <= '1';
                when c_hdr_op_wr   => op_wr <= '1';
                when c_hdr_op_rd   => op_rd <= '1';
                when others        => op_rd <= '1';  -- no support for op rd incr
              end case;
            when c_header_prtad_len =>
              if v_prtad /= g_prtad then
                v_match := false;
              end if;
            when c_header_devad_len =>
              if v_devad /= g_devad then
                v_match := false;
              end if;
              if g_quick = true then
                i_addr                <= (others => '0');
                i_addr(g_devad'range) <= v_devad;
              end if;
            when c_header_ta_len - 1 =>
              if op_rd = '1' then
                v_bit_cnt := 0;
                state <= s_header_ta_rd;
              end if;
            when c_header_ta_len =>
              v_bit_cnt := 0;
              state <= s_write;
            when others => null;  -- nothing to do, continue
          end case;
        end if;

      when s_header_ta_rd =>
        if falling_edge(mdc) then
          v_mdout := '0';
        elsif rising_edge(mdc) then
          v_dat := i_reg_dat;
          state <= s_read;
        end if;

      when s_write =>
        if rising_edge(mdc) then
          v_dat := v_dat(v_dat'high - 1 downto 0) & mdin;

          v_bit_cnt := v_bit_cnt + 1;

          if v_bit_cnt = c_data_len then
            if op_ad = '1' then
              i_addr <= v_dat;
            else  -- op_wr='1'
              if g_quick = false then
                if unsigned(i_addr) = to_unsigned(g_reg_addr, g_reg_w) then
                  i_reg_dat <= v_dat;
                end if;
              else
                if unsigned(i_addr) = unsigned(g_devad) then
                  i_reg_dat <= v_dat;
                end if;
              end if;
            end if;
            state <= s_idle;
          end if;
        end if;

      when s_read =>
        if falling_edge(mdc) then
          v_mdout := v_dat(c_data_len - 1 - v_bit_cnt);

          v_bit_cnt := v_bit_cnt + 1;

          if v_bit_cnt = c_data_len then
            state <= s_idle;
          end if;
        end if;

      when others =>  -- can not occur
        assert false
          report "Unknown MDIO state."
          severity FAILURE;
    end case;

    if v_match = false then
      mdio <= 'Z';
    else
      mdio <= v_mdout;
    end if;
  end process;
end beh;
