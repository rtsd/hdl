-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity i2c_slv_device is
  generic (
    g_address : std_logic_vector(6 downto 0)  -- Slave I2C address
  );
  port (
    scl     : in    std_logic;  -- I2C Serial Clock Line
    sda     : inout std_logic;  -- I2C Serial Data Line
    en      : out   std_logic;  -- '1' : new access, wr_dat may be a cmd byte, '0' : wr_dat is a data byte,
    p       : out   std_logic;  -- rising edge indicates end of access (stop)
    wr_dat  : out   std_logic_vector(7 downto 0);
    wr_val  : out   std_logic;  -- rising edge indicates valid wr_dat
    rd_req  : out   std_logic;  -- rising edge request new rd_dat
    rd_dat  : in    std_logic_vector(7 downto 0)
  );
end i2c_slv_device;

architecture beh of i2c_slv_device is
  -- The code reuses the RTL code of i2cslave(rtl).vhd written by A.W. Gunst.

  function strong( sl : in std_logic ) return std_logic is
  begin
    if sl = 'H'THEN
      return '1';
    elsif sl = 'L' then
      return '0';
    else
      return sl;
    end if;
  end;

  constant g_nof_ctrl_bytes : natural := 1;

  type t_dev_state is (
    ST_IDLE,
    ST_CMD_OR_DATA,
    ST_READ_CMD,
    ST_READ_DATA
  );

  signal rs             : std_logic;  -- behavioral restart
  signal prev_rw        : std_logic := '0';  -- behavioral
  signal rd_first       : std_logic := '0';  -- behavioral
  signal rd_next        : std_logic := '0';  -- behavioral
  signal dev_state      : t_dev_state := ST_IDLE;  -- behavioral

  type   state  is (reset, read_addr, read_data, write_data, acknowledge, nacknowledge, wacknowledge, wnacknowledge);

  signal RST            : std_logic;  -- behavioral
  signal CTRL_REG       : std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0);

  signal start          : std_logic := '0';  -- behavioral
  signal stop           : std_logic := '1';  -- behavioral

  signal am             : std_logic := '0';  -- behavioral address match

  signal streset        : std_logic;  -- bit to reset the start and stop flip flops
  signal latch_ctrl     : std_logic := '0';  -- behavioral
  signal latch_ctrl_dly : std_logic := '0';  -- behavioral
  signal ctrl_tmp       : std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0);  -- register to read and write to temporarily
  signal i_ctrl_reg     : std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0);  -- output register
  signal rw             : std_logic := '0';  -- bit to indicate a read or a write, behavioral
  signal ctrladr        : std_logic_vector(6 downto 0);  -- I2C address register
  signal bitcnt         : natural range 0 to 7;  -- bitcnt for reading bits
  signal bytecnt        : natural range 0 to g_nof_ctrl_bytes - 1;  -- bytenct for reading bytes
  signal current_state  : state;

  signal tri_en         : std_logic;  -- signal to enable the tristate buffer for the SDA line
  signal sda_int        : std_logic;  -- internal SDA line to drive the SDA pin
  signal wbitcnt        : natural range 0 to 8;  -- bitcnt for writing bits
  signal wbytecnt       : natural range 0 to g_nof_ctrl_bytes - 1;  -- bytenct for writing bytes
  signal zeroedge_state : state;
begin
  -- Mostly behavioral code:

  RST    <= '0';
  wr_dat <= ctrl_tmp;

  p_start : process (start)
  begin
    rs <= start;
    if rising_edge(start) then
      rs <= not stop;
    end if;
  end process;

  rd_first <= rw and not prev_rw;
  p_dev_read : process(SCL)
  begin
    if rising_edge(SCL) then
      prev_rw <= rw;
    end if;
  end process;

  p_dev_state : process (am, rs, stop)
  begin
    case dev_state is
      when ST_IDLE =>
        if rising_edge(am) then
          dev_state <= ST_CMD_OR_DATA;  -- address match so expect cmd or data
        end if;
      when ST_CMD_OR_DATA =>
        if rising_edge(stop) then
          dev_state <= ST_IDLE;  -- end of direct data access (write with or without cmd, or read without cmd)
        elsif falling_edge(rs) then
          dev_state <= ST_READ_CMD;  -- read cmd so continue with address
        end if;
      when ST_READ_CMD =>
        if rising_edge(am) then
          dev_state <= ST_READ_DATA;  -- address match so continue with read data
        elsif rising_edge(stop) then
          dev_state <= ST_IDLE;  -- no address match occured
        end if;
      when ST_READ_DATA =>
        if rising_edge(stop) then
          dev_state <= ST_IDLE;  -- end of cmd read data access
        end if;
    end case;
  end process;

  --output control signals
  en     <= '1'                 when dev_state = ST_CMD_OR_DATA                           else '0';
  wr_val <= latch_ctrl_dly      when dev_state = ST_CMD_OR_DATA                           else '0';
  rd_req <= rd_first or rd_next when dev_state = ST_CMD_OR_DATA or dev_state = ST_READ_DATA else '0';
  p      <= stop;  -- output p is can be used to distinghuis beteen direct write data or cmd write data.
                   --  if at p n bytes were written, then it was a direct write,
                   --  else if at p 1+n bytes were written then it the first byte was the cmd.

  -- Mostly RTL code:

  CTRL_REG <= i_ctrl_reg;

  startcontrol:
  process(SDA, SCL, streset, RST)
  begin
    if falling_edge(SDA) then
      if strong(SCL) = '1' then
        start <= '1';
      else
        start <= '0';
      end if;
    end if;

    if streset = '1' or RST = '1' then
      start <= '0';
    end if;
  end process;

  stopcontrol:
  process(SDA, SCL, streset, RST)
  begin
    if rising_edge(SDA) then
      if strong(SCL) = '1' then
        stop <= '1';
      else
        stop <= '0';
      end if;
    end if;

    if streset = '1' or RST = '1' then
      stop <= '0';
    end if;
  end process;

  control:  -- i2c slave
  process(SCL, RST)
  begin
    if RST = '1' then
      --reset input connected to bit 17 of CTRL register, hence default for CTRL[17] must be '0' so RST will act as a spike.
      --if the spike is strong enough, then this works also in hardware for the rest of the logic connected to RST.

      -- Rising edge registers:
      streset        <= '0';
      latch_ctrl     <= '0';
      latch_ctrl_dly <= '0';
      ctrl_tmp       <= (others => '0');
      i_ctrl_reg     <= (others => '0');
      rw             <= '0';
      ctrladr        <= (others => '0');
      bitcnt         <= 0;
      bytecnt        <= 0;
      current_state  <= reset;
      am             <= '0';

      -- Falling edge registers:
      sda_int        <= 'H';
      tri_en         <= '0';
      wbitcnt        <= 0;
      wbytecnt       <= 0;
      zeroedge_state <= reset;
      rd_next        <= '0';

    elsif rising_edge(SCL) then

      -- Latch CTRL register
      latch_ctrl_dly <= latch_ctrl;
      i_ctrl_reg <= rd_dat;
      if latch_ctrl = '1' then  -- latch ctrl register
        i_ctrl_reg <= ctrl_tmp;
      end if;

      -- Statemachine
      --   default assignments (others keep their value accross states during the access)
      streset       <= '0';
      latch_ctrl    <= '0';
      bitcnt        <= 0;
      current_state <= zeroedge_state;
      am            <= '0';
      if start = '1' then
        streset       <= '1';  -- reset start bit
        ctrladr       <= ctrladr(5 downto 0) & strong(SDA);  -- first commands of read_addr state should immediately be executed
        bitcnt        <= 1;
        bytecnt       <= 0;
        current_state <= read_addr;
      elsif stop = '1' then  -- only recognized if there occurs an SCL edge between I2C Stop and Start
        streset       <= '1';  -- reset stop bit
        bytecnt       <= 0;
        current_state <= reset;
      else
        case zeroedge_state is
          when reset => null;  -- only a Start gets the statemachines out of reset, all SCL edges are ignored until then
          when read_addr =>
            if bitcnt < 7 then
              ctrladr <= ctrladr(5 downto 0) & strong(SDA);  -- shift the data to the left
                                                            --shift a new bit in (MSB first)
              bitcnt <= bitcnt + 1;
            else
              rw <= strong(SDA);  -- last bit indicates a read or a write
              if ctrladr = g_address then
                am <= '1';
                current_state <= acknowledge;
              else
                current_state <= reset;
              end if;
            end if;
          when read_data =>
            if bitcnt < 7 then
              ctrl_tmp(8 * g_nof_ctrl_bytes - 1 downto 1) <= ctrl_tmp(8 * g_nof_ctrl_bytes - 2 downto 0);  -- shift the data to the left
              ctrl_tmp(0) <= strong(SDA);  -- shift a new bit in (MSB first)
              bitcnt <= bitcnt + 1;
            else  -- reading the last bit and going immediately to the ack state
              ctrl_tmp(8 * g_nof_ctrl_bytes - 1 downto 1) <= ctrl_tmp(8 * g_nof_ctrl_bytes - 2 downto 0);  -- shift the data to the left
              ctrl_tmp(0) <= strong(SDA);  -- shift a new bit in (MSB first)
              if bytecnt < g_nof_ctrl_bytes - 1 then  -- first bytes
                bytecnt <= bytecnt + 1;
                current_state <= acknowledge;  -- acknowledge the successfull read of a byte
              else  -- last byte
                latch_ctrl <= '1';  -- latch at g_nof_ctrl_bytes-th byte (or a multiple of that, due to bytecnt wrap)
                bytecnt <= 0;  -- wrap byte count
                current_state <= acknowledge;  -- acknowledge also for the last byte
              end if;
            end if;
          when write_data => null;
          when acknowledge =>
            if rw = '0' then
              current_state <= read_data;  -- acknowledge state is one clock period active
            else
              current_state <= write_data;
            end if;
          when nacknowledge => null;
            --When the master addresses another slave, then the statemachine directly goes into reset state.
            --The slave always answers with ACK on a master write data, so the nacknowledge state is never
            --reached.
          when wacknowledge =>
            if strong(SDA) = '0' then
              current_state <= write_data;  -- write went OK, continue writing bytes to the master
            else
              current_state <= reset;  -- write failed, so abort the transfer
            end if;
          when wnacknowledge =>
            --NACK is used to signal the end of a master read access. The slave can not known whether
            --the write transfer to the master was succesful and it does not need to know anyway.
            if strong(SDA) = '1' then
              current_state <= reset;  -- last write went OK, no more bytes to write to the master
            else
              --By making current_state <= reset the CTRL register can only be read once in one transfer.
              --If more bytes are read then the master will see them as 0xFF (due to SDA pull up). By
              --making current_state <= write_data the CTRL register wraps if it is read more than once.
              --current_state <= reset;      --last write went OK, abort further transfer
              current_state <= write_data;  -- last write went OK, wrap and continue writing bytes to the master
            end if;
          when others =>
            current_state <= reset;
        end case;
      end if;

    elsif falling_edge(SCL) then

      -- Statemachine
      --   default assignments (others keep their value accross states during the access)
      sda_int        <= 'H';
      tri_en         <= '0';
      wbitcnt        <= 0;
      zeroedge_state <= current_state;
      rd_next        <= '0';
      if start = '1' then
        wbytecnt       <= 0;
        zeroedge_state <= reset;
      elsif stop = '1' then  -- only recognized if there occurs an SCL edge between I2C Stop and Start
        wbytecnt       <= 0;
        zeroedge_state <= reset;
      else
        case current_state is
          when reset => null;
          when read_addr => null;
          when read_data => null;
          when write_data =>
            if wbitcnt < 8 then
              tri_en <= '1';  -- enable tri-state buffer to write SDA
              if i_ctrl_reg(8 * g_nof_ctrl_bytes - 1 - (8 * wbytecnt + wbitcnt)) = '0' then
                sda_int <= '0';  -- copy one bit to SDA (MSB first), else default to 'H'
              end if;
              wbitcnt <= wbitcnt + 1;
            else
              if wbytecnt < g_nof_ctrl_bytes - 1 then  -- first bytes
                wbytecnt <= wbytecnt + 1;
                zeroedge_state <= wacknowledge;  -- wait till master acknowledges the successfull read of a byte
              else  -- last byte
                rd_next <= '1';
                wbytecnt <= 0;  -- wrap byte count
                zeroedge_state <= wnacknowledge;  -- wait till master nacknowledges the successfull read of a byte
              end if;
            end if;
          when acknowledge =>
            tri_en <= '1';  -- enable tri-state buffer to write SDA
            sda_int <= '0';  -- acknowledge data
          when nacknowledge => null;
            --This state is never reached.
            --To answer an NACK the tri_en can remain '0', because the default SDA is 'H' due to the pull up.
          when wacknowledge => null;
          when wnacknowledge => null;
          when others =>
            zeroedge_state <= reset;
        end case;
      end if;
    end if;
  end process;

  --control the tri state buffer
  SDA <= sda_int when tri_en = '1' else 'Z';
end beh;
