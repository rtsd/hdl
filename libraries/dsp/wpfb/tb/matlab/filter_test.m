%% Test R2SDF output for given input signal
% 
%-------------------------------------------------------------------------%
%   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
%   Copyright (C) 2009-2010
%   ASTRON (Netherlands Institute for Radio Astronomy)
%   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
% 
%   This file is part of the UniBoard software suite.
%   The file is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------%
%%

if(1)
  clear all; close all; clc;

  nof_slices        = 8; 
  nof_subbands      = 64;
  fft_size          = nof_subbands;
  c_nof_taps        = 8;
  c_fil_in_dat_w    = 8;
  c_fil_out_dat_w   = 11;
  c_pf_coef_w       = 16;  
  c_quantize_factor = c_fil_in_dat_w + c_pf_coef_w - c_fil_out_dat_w;

  %--------------------
  NOISE    = 1;
  SINUS    = 0;
  SINNOISE = 0;
  IMPULSE  = 0;
  TWO_REAL = 1;
  REORDER  = 0;        % optional Reorder enable for the vhdl output. Use only in case reordering in VHDL is not done! 
  SEPARATE = TWO_REAL; % Separate function only applicable when TWO_REAL inputs are used. 
  FIGURE   = 1;
  
  RE= 1;
  IM= 2;

  coefsFileName = '../data/Coeffs16384Kaiser-quant.dat';
  all_coefs = dlmread(coefsFileName);
  
  coefs = zeros(nof_subbands, c_nof_taps);
  for ii=1:nof_subbands
    for jj=1:c_nof_taps  
      index = ((ii-1)+(jj-1)*nof_subbands)*floor(16384/(nof_subbands*c_nof_taps))+1;
      coefs(nof_subbands-ii+1,jj) = all_coefs(((ii-1)+(jj-1)*nof_subbands)*floor(16384/(nof_subbands*c_nof_taps))+1);
    end;
  end;

  filterOutputFile = '../data/uniNoise_filter_out.txt';
  
  x=zeros(fft_size*nof_slices,2); %in_re,  in_img
  y=zeros(fft_size*nof_slices,2); %out_re, out_img
  if TWO_REAL
    if NOISE        
      inFileName=   '../data/uniNoise_in.txt';
      outFileName=  '../data/uniNoise_out.txt';
    end;
  else                                            
    if NOISE        
      inFileName=  '../data/uniNoise_in.txt';
      outFileName= '../data/uniNoise_out.txt';
    end;
  end;   
    
  'reading Model sim inputs and outputs'
  inFileName
  outFileName
  X= dlmread(inFileName,',');
  Y= dlmread(outFileName,',');
  F= dlmread(filterOutputFile, ',');

%  xSize= size(X)
  ySize= size(Y)
  
  % read only first slice from input file and copy that slice for nof_slices times.
  new=1;
  for ii=1: nof_slices
    for cnt=1: fft_size
      x(new,:)=X(cnt,:);
      new=new+1;
    end;
  end;
  size(x);
                     
  % read valid outputs
  new=1;
  for cnt=1: ySize(1)
    y(new,:)=Y(cnt,:);
    new=new+1;
  end;
  size(y);
  
  if(REORDER)
    % re-order y
    r=bitrevorder([0:fft_size-1]);
    for ff=0:fft_size:(nof_slices-1)*fft_size
      for jj=1:size(r,2)
        y1(ff+jj,:)=y(ff+r(jj)+1,:);
      end;
    end;
  else
    % retain fft
    y1=y;
  end;
  size(y1);  
  
  %Pre-filter
  for ii=1:nof_subbands
    data_re = downsample(x(:,RE), nof_subbands, ii-1);   % Downsample to generate a list of samples that represent the same subband
    data_im = downsample(x(:,IM), nof_subbands, ii-1);
    fil_dat_re(:,ii) = filter(coefs(ii,:),1,data_re);
    fil_dat_im(:,ii) = filter(coefs(ii,:),1,data_im);
  end; 
  
  % Shuffle the data in order to make it fit for the fft.
  % Also apply the quantization factor that corresponds to the quantization step in VHDL.  
  for ii=1: nof_subbands
    for jj=0: nof_slices-1
      fft_in_unquantized_re(ii+jj*nof_subbands,:) = fil_dat_re(jj+1,ii); 
      fft_in_unquantized_im(ii+jj*nof_subbands,:) = fil_dat_im(jj+1,ii);
      fft_in_re(ii+jj*nof_subbands,:) = fil_dat_re(jj+1,ii)/2^c_quantize_factor;
      fft_in_im(ii+jj*nof_subbands,:) = fil_dat_im(jj+1,ii)/2^c_quantize_factor;
    end;
  end; 
    
  % FFT
  for ff=0:fft_size:(nof_slices-1)*fft_size
    tmp(ff+1:ff+fft_size,:)=fft(fft_in_re(ff+1:ff+fft_size)+i*fft_in_im(ff+1:ff+fft_size),fft_size);
  end;
  expFFT(:,RE)=real(tmp); 
  expFFT(:,IM)=imag(tmp);
  
  pow_a = zeros(fft_size/2,1);
  pow_b = zeros(fft_size/2,1);
       
  % Separate
  if(SEPARATE)  
    for ff=0:fft_size:(nof_slices-1)*fft_size
      newaFFT(1+ff,RE) = expFFT(1+ff,RE);
      newbFFT(1+ff,RE) = expFFT(1+ff,IM);
      for jj=1:1:fft_size/2
        newaFFT(jj+ff+1,RE) = (expFFT(ff+fft_size-jj+1,RE) + expFFT(ff+jj+1,RE))/2;
        newbFFT(jj+ff+1,RE) = (expFFT(ff+fft_size-jj+1,IM) + expFFT(ff+jj+1,IM))/2;
        newaFFT(jj+ff+1,IM) = (expFFT(ff+jj+1,IM) - expFFT(ff+fft_size-jj+1,IM))/2;
        newbFFT(jj+ff+1,IM) = (expFFT(ff+fft_size-jj+1,RE) - expFFT(ff+jj+1,RE))/2;
      end;          
      for jj=0:1:fft_size/2-1          
        expFFT(ff+2*jj+1,RE) = newaFFT(ff+jj+1,RE); 
        expFFT(ff+2*jj+2,RE) = newbFFT(ff+jj+1,RE);
        expFFT(ff+2*jj+1,IM) = newaFFT(ff+jj+1,IM); 
        expFFT(ff+2*jj+2,IM) = newbFFT(ff+jj+1,IM);
        pow_a(jj+1, 1) = pow_a(jj+1, 1) + real((newaFFT(ff+jj+1,RE) + i*newaFFT(ff+jj+1,IM)) * (newaFFT(ff+jj+1,RE) - i*newaFFT(ff+jj+1,IM))); 
        pow_b(jj+1, 1) = pow_b(jj+1, 1) + real((newbFFT(ff+jj+1,RE) + i*newbFFT(ff+jj+1,IM)) * (newbFFT(ff+jj+1,RE) - i*newbFFT(ff+jj+1,IM))); 
      end;
    end;
  end;    
  expFFT;

  % Error
  z    = expFFT(:,RE)+i*expFFT(:,IM);
  zQ   = y1(:,RE)+i*y1(:,IM);
  qErr = z-zQ; 

  if(FIGURE)
    subplot(4,1,1);
    plot(x(1:nof_slices*fft_size,RE),'B*-'); grid on; hold on;
    plot(x(1:nof_slices*fft_size,IM),'r*-'); hold off;
    legend ('Input real','Input imaginary');

    subplot(4,1,2); grid on;
    plot(expFFT(:,RE),'B*-'); grid on; hold on;
    plot(expFFT(:,IM),'r*-'); hold off;
    legend ('MATLAB FFT output real','MATLAB FFT output imaginary');

    subplot(4,1,3); grid on;
    plot(y1(:,RE),'B*-'); grid on; hold on;
    plot(y1(:,IM),'r*-'); hold off;
    legend ('VHDL output real','VHDL output imaginary');

    subplot(4,1,4); grid on;
    %figure;
    plot(real(qErr),'B*-'); grid on; hold on;
    plot(imag(qErr),'r*-'); hold off;
    legend ('error real','error imaginary');

    if(SEPARATE)  
      figure
      subplot(2,1,1); grid on;
      plot(log10(pow_a(1:fft_size/2)),'B*-'); grid on; hold on;
      legend ('MATLAB FFT output channel A');
      subplot(2,1,2); grid on;
      plot(log10(pow_b(1:fft_size/2)),'B*-'); grid on; hold on;
      legend ('MATLAB FFT output channel B');      
    end; 
    
  end;
clear tmp expFFT x y Y z F pow_a pow_b;    
end; 


