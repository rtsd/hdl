
set_location_assignment PIN_K15 -to CLK
set_location_assignment PIN_J15 -to "CLK(n)"
set_location_assignment PIN_N12 -to ETH_CLK
set_location_assignment PIN_K14 -to PPS
set_location_assignment PIN_J14 -to "PPS(n)"

# enable 100 ohm termination:
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to CLK
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to PPS

#set_location_assignment PIN_AT33 -to CFG_DATA[0]
#set_location_assignment PIN_AT32 -to CFG_DATA[1]
#set_location_assignment PIN_BB33 -to CFG_DATA[2]
#set_location_assignment PIN_BA33 -to CFG_DATA[3]




# IO Standard Assignments from Gijs (excluding memory)
set_instance_assignment -name IO_STANDARD "1.8 V" -to ETH_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[1](n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[6]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[7]
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTA
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTB
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SC
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SD
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to WDI

# locations changed 30 sept
set_location_assignment PIN_P16 -to ID[0]
set_location_assignment PIN_P15 -to ID[1]
set_location_assignment PIN_K13 -to ID[2]
set_location_assignment PIN_L13 -to ID[3]
set_location_assignment PIN_N16 -to ID[4]
set_location_assignment PIN_N14 -to ID[5]
set_location_assignment PIN_U13 -to ID[6]

set_location_assignment PIN_T13 -to ID[7]
set_location_assignment PIN_AU31 -to INTA
set_location_assignment PIN_AR30 -to INTB

set_location_assignment PIN_BC31 -to SENS_SC
set_location_assignment PIN_BB31 -to SENS_SD

set_location_assignment PIN_BA25 -to PMBUS_SC
set_location_assignment PIN_BD25 -to PMBUS_SD
set_location_assignment PIN_BD26 -to PMBUS_ALERT
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SC
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SD
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_ALERT


set_location_assignment PIN_AN32 -to TESTIO[0]
set_location_assignment PIN_AP32 -to TESTIO[1]
set_location_assignment PIN_AT30 -to TESTIO[2]
set_location_assignment PIN_BD31 -to TESTIO[3]
set_location_assignment PIN_AU30 -to TESTIO[4]
set_location_assignment PIN_BD30 -to TESTIO[5]

set_location_assignment PIN_AB12 -to VERSION[0]
set_location_assignment PIN_AB13 -to VERSION[1]
set_location_assignment PIN_BB30 -to WDI

set_location_assignment PIN_K12 -to ETH_SGIN[0]
set_location_assignment PIN_J12 -to "ETH_SGIN[0](n)"
set_location_assignment PIN_AF33 -to ETH_SGIN[1]
set_location_assignment PIN_AE33 -to "ETH_SGIN[1](n)"
set_location_assignment PIN_H13 -to ETH_SGOUT[0]
set_location_assignment PIN_H12 -to "ETH_SGOUT[0](n)"
set_location_assignment PIN_AW31 -to ETH_SGOUT[1]
set_location_assignment PIN_AV31 -to "ETH_SGOUT[1](n)"

set_instance_assignment -name IO_STANDARD LVDS -to PPS
set_instance_assignment -name IO_STANDARD LVDS -to "PPS(n)"
set_instance_assignment -name IO_STANDARD LVDS -to CLK
set_instance_assignment -name IO_STANDARD LVDS -to "CLK(n)"

# Enable internal termination for LVDS inputs
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to PPS
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[1]

set_location_assignment PIN_AG31 -to altera_reserved_tms
set_location_assignment PIN_AJ31 -to altera_reserved_tck
set_location_assignment PIN_AK18 -to altera_reserved_tdi
set_location_assignment PIN_AH31 -to altera_reserved_ntrst
set_location_assignment PIN_AM29 -to altera_reserved_tdo
#set_location_assignment PIN_AV33 -to ~ALTERA_DATA0~


set_location_assignment PIN_BA33 -to QSFP_LED[0]
set_location_assignment PIN_BA30 -to QSFP_LED[1]
set_location_assignment PIN_BB33 -to QSFP_LED[2]
set_location_assignment PIN_AU33 -to QSFP_LED[3]
set_location_assignment PIN_AV32 -to QSFP_LED[4]
set_location_assignment PIN_AW30 -to QSFP_LED[5]
set_location_assignment PIN_AP31 -to QSFP_LED[6]
set_location_assignment PIN_AP30 -to QSFP_LED[7]
set_location_assignment PIN_AT33 -to QSFP_LED[8]
set_location_assignment PIN_AG32 -to QSFP_LED[9]
set_location_assignment PIN_AF32 -to QSFP_LED[10]
set_location_assignment PIN_AE32 -to QSFP_LED[11]



