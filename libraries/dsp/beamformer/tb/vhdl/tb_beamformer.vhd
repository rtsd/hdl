--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Authors:
-- . Daniel van der Schuur
-- . Pieter Donker
-- Purpose:
-- .
-- Description:
-- .

library IEEE, common_lib, dp_lib, technology_lib, diag_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use common_lib.tb_common_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;

entity tb_beamformer is
  generic (
    g_tb_index       : natural := 0;  -- use different index to avoid MM file conflict in multi tb
    --g_technology     : NATURAL := c_tech_select_default;
    g_nof_inputs     : natural := 2;
    g_nof_weights    : natural := 32;
    g_data_w         : natural := 8;  -- 8b complex input data
    g_use_weight_ram : boolean := false;
    g_use_weight_reg : boolean := true
  );
end tb_beamformer;

architecture tb of tb_beamformer is
  -----------------------------------------------------------------------------
  -- General
  -----------------------------------------------------------------------------
  constant c_dp_clk_period   : time :=  5 ns;
  constant c_mm_clk_period   : time := 25 ns;

  -- Input data cont
  constant c_in_block_size          : natural := g_nof_weights;
  constant c_in_block_interval      : natural := c_in_block_size;
  constant c_in_nof_blocks_per_sync : natural := g_nof_weights;
  constant c_in_sync_interval       : natural := c_in_block_interval * c_in_nof_blocks_per_sync;

  signal tb_end         : std_logic := '0';
  signal dp_clk         : std_logic := '1';
  signal dp_rst         : std_logic;
  signal mm_clk         : std_logic := '1';
  signal mm_rst         : std_logic;
  signal OK             : std_logic := '1';
  signal mm_ok          : std_logic := '1';
  signal addr_ok        : std_logic := '1';
  signal data_ok        : std_logic := '1';
  signal verify_mm_en   : std_logic := '1';
  signal verify_addr_en : std_logic := '0';
  signal verify_data_en : std_logic := '0';

  constant min_snk_in_val: integer := -(2**g_data_w / 2);
  constant max_snk_in_val: integer := (2**g_data_w / 2) - 1;

  -- MM
  constant c_unb_nr                 : natural := 0;
  constant c_node_nr                : natural := 0;  -- choose node 0 is FN 0
  constant c_mm_file_ram_beamformer : string  := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_BEAMFORMER_" & int_to_str(g_tb_index);

  signal snk_in_enable : std_logic := '0';
  signal snk_in        : t_dp_sosi := c_dp_sosi_rst;

  -----------------------------------------------------------------------------
  -- beamformer
  -----------------------------------------------------------------------------
  signal ram_beamformer_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_beamformer_miso : t_mem_miso := c_mem_miso_rst;

  signal beamformer_snk_in_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal beamformer_src_out    : t_dp_sosi;

  signal beamformer_weight_addr    : std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0);
  signal addr_stimulus_weight_addr : std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0) := (others => '0');
  signal data_stimulus_weight_addr : std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0) := (others => '0');

  signal rd_data_mm_stimuli     : std_logic_vector(c_word_w - 1 downto 0);
  signal rd_data_addr_stimuli   : std_logic_vector(c_word_w - 1 downto 0);

  -- used to set snk_in re data
  signal snk_re_data            : std_logic_vector(g_data_w - 1 downto 0) := TO_SVEC(1, g_data_w);
  signal snk_im_data            : std_logic_vector(g_data_w - 1 downto 0) := TO_SVEC(0, g_data_w);
  begin
  -----------------------------------------------------------------------------
  -- Clocks and reset
  -------------------------------------------------------------------------------
  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  ----------------------------------------------------------------------------
  -- Stimuli snk_in
  ----------------------------------------------------------------------------
  -- sync, valid, sop, eop
  proc_common_gen_duty_pulse(0,                 1, c_in_sync_interval,  '1', dp_rst, dp_clk, snk_in_enable, snk_in.sync);
  proc_common_gen_duty_pulse(0,   c_in_block_size, c_in_block_interval, '1', dp_rst, dp_clk, snk_in_enable, snk_in.valid);
  proc_common_gen_duty_pulse(0,                 1, c_in_block_interval, '1', dp_rst, dp_clk, snk_in_enable, snk_in.sop);
  proc_common_gen_duty_pulse(c_in_block_size-1, 1, c_in_block_interval, '1', dp_rst, dp_clk, snk_in_enable, snk_in.eop);  -- DUT uses eop
  -- bsn
  snk_in.bsn <= INCR_UVEC(snk_in.bsn, 1) when rising_edge(dp_clk) and snk_in.eop = '1';
  -- re/im data
  snk_in.re <= TO_DP_DSP_DATA(TO_SINT(snk_re_data));
  snk_in.im <= TO_DP_DSP_DATA(TO_SINT(snk_im_data));

  -- set master OK
  OK <= mm_ok and addr_ok and data_ok;

  -- change beamformer weights address from multiple processes
  set_weights : process(verify_addr_en, addr_stimulus_weight_addr, verify_data_en, data_stimulus_weight_addr)
  begin
    beamformer_weight_addr <= TO_UVEC(0, ceil_log2(g_nof_weights));

    if verify_addr_en = '1' then
      beamformer_weight_addr <= addr_stimulus_weight_addr;
    end if;

    if verify_data_en = '1' then
      beamformer_weight_addr <= data_stimulus_weight_addr;
    end if;
  end process;

  -- first check, write/read weight to/from RAM
  mm_stimulus: process
  begin
    if verify_mm_en = '1' then
      -- wait for reset
      proc_common_wait_until_low(mm_clk, mm_rst);
      proc_common_wait_until_low(dp_clk, dp_rst);
      proc_common_wait_some_cycles(dp_clk, 10);

      -- enable snk_in generator
      snk_in_enable <= '1';
      proc_common_wait_until_hi_lo(dp_clk, snk_in.sync);

      report "Start mm stumulus";
      for I in 0 to g_nof_inputs - 1 loop
        for J in 0 to g_nof_weights - 1 loop
          -- write MM page
          mmf_mm_bus_wr(c_mm_file_ram_beamformer, I * g_nof_weights + J, (J + 1) * 2**16 + J + 1, mm_clk);
        end loop;
      end loop;

      for I in 0 to g_nof_inputs - 1 loop
        for J in 0 to g_nof_weights - 1 loop
          -- read back MM page
          mmf_mm_bus_rd(c_mm_file_ram_beamformer, I * g_nof_weights + J, rd_data_mm_stimuli, mm_clk);
          if TO_SINT(rd_data_mm_stimuli(c_word_w / 2 - 1 downto 0)) /= J + 1 then
            assert false
              report "MM Wrong weight for re" &
                     ", expected=" & int_to_str(J + 1) &
                     ", readback=" & int_to_str(integer(TO_UINT(rd_data_mm_stimuli(c_word_w / 2 - 1 downto 0))))
              severity ERROR;
            mm_ok <= '0';
          end if;

          if TO_SINT(rd_data_mm_stimuli(c_word_w - 1 downto c_word_w / 2)) /= J + 1 then
            assert false
              report "MM Wrong weight for im" &
                     ", expected=" & int_to_str(J + 1) &
                     ", readback=" & int_to_str(integer(TO_UINT(rd_data_mm_stimuli(c_word_w - 1 downto c_word_w / 2))))
              severity ERROR;
            mm_ok <= '0';
          end if;
        end loop;
      end loop;
      verify_addr_en <= '1';  -- enable second check
    end if;
    wait;
  end process;

  -- second check, activate weights and check effect on output
  -- for this test snk_in must be (1+0j)
  addr_stimulus: process
  begin
    proc_common_wait_until_high(dp_clk, verify_addr_en);
    report "Start weights addr stimulus";
    -- loop over all weights adresses
    for I in 0 to g_nof_inputs - 1 loop
      for J in 0 to g_nof_weights - 1 loop
        addr_stimulus_weight_addr <= TO_UVEC(J, ceil_log2(g_nof_weights));
        proc_common_wait_some_cycles(dp_clk, 4 + ceil_log2(g_nof_inputs) + 1);

        mmf_mm_bus_rd(c_mm_file_ram_beamformer, I * g_nof_weights + J, rd_data_addr_stimuli, mm_clk);
        if TO_SINT(rd_data_addr_stimuli(c_word_w / 2 - 1 downto 0)) * g_nof_inputs * TO_SINT(snk_in.re) /= TO_SINT(beamformer_src_out.re) then
          assert false
            report "Beamformer wrong weights for re" &
                   ", expected=" & int_to_str(TO_SINT(rd_data_addr_stimuli(c_word_w / 2 - 1 downto 0)) * g_nof_inputs * TO_SINT(snk_in.re)) &
                   ", readback=" & int_to_str(TO_SINT(beamformer_src_out.re))
            severity ERROR;
          addr_ok <= '0';
        end if;
        if TO_SINT(rd_data_addr_stimuli(c_word_w - 1 downto c_word_w / 2)) * g_nof_inputs * TO_SINT(snk_in.re) /= TO_SINT(beamformer_src_out.im) then
          assert false
            report "Beamformer wrong weights for im" &
                   ", expected=" & int_to_str(TO_SINT(rd_data_addr_stimuli(c_word_w - 1 downto c_word_w / 2)) * g_nof_inputs * TO_SINT(snk_in.im)) &
                   ", readback=" & int_to_str(TO_SINT(beamformer_src_out.im))
            severity ERROR;
          addr_ok <= '0';
        end if;
      end loop;
    end loop;
    verify_data_en <= '1';  -- enable third check
    wait;
  end process;

  -- third check, change input data and check effect on output
  data_stimulus: process
  begin
    proc_common_wait_until_high(dp_clk, verify_data_en);

    report "Start data stimulus";
    -- set weights for test on addr 0 and 1 for all inputs
    -- with weight (1+1j) output: re=0, im=(snk_in.re+snk_in.im)*nof_inputs
    -- with weight (1+0j) output: re=snk_in.re*nof_inputs, im=snk_in.im*nof_inputs
    for I in 0 to g_nof_inputs loop
      mmf_mm_bus_wr(c_mm_file_ram_beamformer, I * g_nof_weights, 1 * 2**16 + 1, mm_clk);  -- (1+1j)
      mmf_mm_bus_wr(c_mm_file_ram_beamformer, I * g_nof_weights + 1, 1, mm_clk);  -- (1+0j)
    end loop;
    proc_common_wait_some_cycles(dp_clk, 1);

    -- loop over weight address 0..1
    for I in 0 to 1 loop
      data_stimulus_weight_addr <= TO_UVEC(I, ceil_log2(g_nof_weights));
      proc_common_wait_some_cycles(dp_clk, 1);
      -- loop over input data
      for J in min_snk_in_val to max_snk_in_val loop
        snk_re_data <= TO_SVEC(J, g_data_w);
        snk_im_data <= TO_SVEC(J, g_data_w);

        proc_common_wait_some_cycles(dp_clk, 4 + ceil_log2(g_nof_inputs) + 1);
        if I * g_nof_inputs * J /= TO_SINT(beamformer_src_out.re) then
          assert false
            report "Beamformer wrong data for re" &
                   ", expected=" & int_to_str(TO_SINT(beamformer_weight_addr) * g_nof_inputs * J) &
                   ", readback=" & int_to_str(TO_SINT(beamformer_src_out.re))
            severity ERROR;
          data_ok <= '0';
        end if;
        -- (2-I): (I=0), doubles value for weight (1+1j)
        if g_nof_inputs * J * (2 - I) /= TO_SINT(beamformer_src_out.im) then
          assert false
            report "Beamformer wrong data for im" &
                   ", expected=" & int_to_str(g_nof_inputs * J * (2 - I)) &
                   ", readback=" & int_to_str(TO_SINT(beamformer_src_out.im))
            severity ERROR;
          data_ok <= '0';
        end if;
      end loop;  -- input data
    end loop;  -- weigth address

    proc_common_wait_some_cycles(dp_clk, 1);
    if OK = '0' then
      report "TEST FAILED.";
    else
      report "Test passed.";
    end if;

    tb_end <= '1';  -- end test
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- beamformer for each TAB
  -----------------------------------------------------------------------------
  u_mm_file_ram_beamformer : entity mm_lib.mm_file generic map(c_mm_file_ram_beamformer) port map(mm_rst, mm_clk, ram_beamformer_mosi, ram_beamformer_miso);

  -- assign input_source to snk_in_arr
  beamformer_snk_in_arr <=  (others => snk_in);

  u_beamformer : entity work.beamformer
  generic map (

    g_technology     => c_tech_select_default,
    g_nof_inputs     => g_nof_inputs,
    g_nof_weights    => g_nof_weights,
    g_data_w         => g_data_w,
    g_weights_file   => "UNUSED",
    g_use_weight_ram => g_use_weight_ram,
    g_use_weight_reg => g_use_weight_reg
  )
  port map (
    dp_clk      => dp_clk,
    dp_rst      => dp_rst,
    mm_clk      => mm_clk,
    mm_rst      => mm_rst,

    ram_mosi    => ram_beamformer_mosi,
    ram_miso    => ram_beamformer_miso,

    weight_addr => beamformer_weight_addr,

    snk_in_arr  => beamformer_snk_in_arr,
    src_out     => beamformer_src_out
  );
end tb;
