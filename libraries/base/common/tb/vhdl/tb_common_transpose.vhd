-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

-- Purpose: Test bench for common_transpose.vhd
-- Usage:
-- > as 3
-- > run -all
--   p_verify self-checks the data output and the addr output of two times the
--   transpose.

entity tb_common_transpose is
  generic (
    g_pipeline_shiftreg  : natural := 0;
    g_pipeline_transpose : natural := 0;
    g_pipeline_hold      : natural := 0;
    g_pipeline_select    : natural := 1;
    g_nof_data           : natural := 3;
    g_data_w             : natural := 12;
    g_addr_w             : natural := 9;
    g_addr_offset        : natural := 10  -- default use fixed offset, in_offset * g_nof_data must fit in g_addr_w address range
  );
end tb_common_transpose;

architecture tb of tb_common_transpose is
  constant clk_period     : time := 10 ns;

  constant c_pipeline     : natural := g_pipeline_shiftreg + g_pipeline_transpose + g_pipeline_select;
  constant c_repeat       : natural := 10;
  constant c_lo           : natural := 0;
  constant c_interval_len : natural := 100;
  constant c_blk_len      : natural := 10;
  constant c_gap_len      : natural := 50;

  constant c_symbol_w     : natural := g_data_w / g_nof_data;

  constant c_frame_len    : natural :=  7 * g_nof_data;
  constant c_frame_eop    : natural := (c_frame_len - 1) mod c_frame_len;

  procedure proc_align_eop(signal clk      : in  std_logic;
                           signal stimuli_phase : in  std_logic;
                           signal in_val   : out std_logic) is
  begin
    while stimuli_phase = '0' loop
      in_val <= '1';
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    in_val <= '0';
  end proc_align_eop;

  -- Stimuli
  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '1';

  signal stimuli_end    : std_logic;
  signal stimuli_data   : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal stimuli_phase  : std_logic;

  signal in_offset      : std_logic_vector(g_addr_w - 1 downto 0) := TO_UVEC(g_addr_offset, g_addr_w);
  signal in_addr        : std_logic_vector(g_addr_w - 1 downto 0) := (others => '0');
  signal in_data        : std_logic_vector(g_data_w - 1 downto 0) := (others => '0');
  signal in_val         : std_logic;
  signal in_eop         : std_logic;

  -- DUT output
  signal trans_offset   : std_logic_vector(g_addr_w - 1 downto 0) := TO_SVEC(-g_addr_offset, g_addr_w);  -- use -g_addr_offset as inverse operation
  signal trans_addr     : std_logic_vector(g_addr_w - 1 downto 0);
  signal trans_data     : std_logic_vector(g_data_w - 1 downto 0);
  signal trans_val      : std_logic;
  signal trans_eop      : std_logic;

  signal out_addr       : std_logic_vector(g_addr_w - 1 downto 0);
  signal out_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_eop        : std_logic;

  -- Verify
  signal verify_en      : std_logic := '0';
  signal ready          : std_logic := '1';
  signal prev_out_addr  : std_logic_vector(g_addr_w - 1 downto 0) := (others => '1');
  signal prev_out_data  : std_logic_vector(g_data_w - 1 downto 0) := (others => '1');
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  p_stimuli : process
  begin
    in_val <= '0';
    in_eop <= '0';
    verify_en <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);
    verify_en <= '1';

    -- Verify pulsed valid
    for J in 0 to 2 * g_nof_data loop
      for I in 1 to c_blk_len + J loop
        proc_common_val_duty(1, 1, clk, in_val);
      end loop;
      proc_align_eop(clk, stimuli_phase, in_val);
      proc_common_eop(clk, in_val, in_eop);
      proc_common_wait_some_cycles(clk, 2 * c_blk_len);
    end loop;

    for J in 0 to 2 * g_nof_data loop
      for I in 1 to c_blk_len + J loop
        proc_common_val_duty(1, 1, clk, in_val);
      end loop;
      proc_align_eop(clk, stimuli_phase, in_val);
      proc_common_wait_some_cycles(clk, 1);
      proc_common_eop(clk, in_val, in_eop);
      proc_common_wait_some_cycles(clk, 2 * c_blk_len);
    end loop;

    -- Verify muli pulsed valid
    for P in 0 to 1 loop
      for I in c_lo to c_repeat loop
        for J in c_lo to c_repeat loop
          if P = 0 then
            proc_common_val_duty(I, J, clk, in_val);
          else
            proc_common_val_duty(J, I, clk, in_val);
          end if;
        end loop;
      end loop;
      proc_common_wait_some_cycles(clk, c_gap_len);
      for I in c_lo to c_repeat loop
        for J in c_repeat downto c_lo loop
          if P = 0 then
            proc_common_val_duty(I, J, clk, in_val);
          else
            proc_common_val_duty(J, I, clk, in_val);
          end if;
        end loop;
      end loop;
      proc_common_wait_some_cycles(clk, c_gap_len);
      for I in c_repeat downto c_lo loop
        for J in c_lo to c_repeat loop
          if P = 0 then
            proc_common_val_duty(I, J, clk, in_val);
          else
            proc_common_val_duty(J, I, clk, in_val);
          end if;
        end loop;
      end loop;
      proc_common_wait_some_cycles(clk, c_gap_len);
      for I in c_repeat downto c_lo loop
        for J in c_repeat downto c_lo loop
          if P = 0 then
            proc_common_val_duty(I, J, clk, in_val);
          else
            proc_common_val_duty(J, I, clk, in_val);
          end if;
        end loop;
      end loop;
      proc_align_eop(clk, stimuli_phase, in_val);
      proc_common_eop(clk, in_val, in_eop);
      proc_common_wait_some_cycles(clk, c_gap_len);
    end loop;

    -- Verify active valid
    in_val <= '1';
    proc_common_wait_until_lo_hi(clk, stimuli_end);
    proc_common_wait_some_cycles(clk, c_interval_len);
    proc_align_eop(clk, stimuli_phase, in_val);
    proc_common_eop(clk, in_val, in_eop);
    in_val <= '0';
    proc_common_wait_some_cycles(clk, c_gap_len);

    tb_end <= '1';
    wait;
  end process;

  stimuli_end <= '1' when signed(in_data) = -1 else '0';

  in_addr <= stimuli_data(g_addr_w - 1 downto 0);
  in_data <= stimuli_data(g_data_w - 1 downto 0);

  stimuli_data  <= INCR_UVEC(stimuli_data, 1) when rising_edge(clk) and in_val = '1';
  stimuli_phase <= in_val when TO_UINT(stimuli_data) mod g_nof_data = g_nof_data - 2 else '0';

  -- DUT
  u_transpose_in : entity common_lib.common_transpose
  generic map (
    g_pipeline_shiftreg  => g_pipeline_shiftreg,
    g_pipeline_transpose => g_pipeline_transpose,
    g_pipeline_hold      => g_pipeline_hold,
    g_pipeline_select    => g_pipeline_select,
    g_nof_data           => g_nof_data,
    g_data_w             => g_data_w,
    g_addr_w             => g_addr_w,
    g_addr_offset        => g_addr_offset
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_offset  => in_offset,
    in_addr    => in_addr,
    in_data    => in_data,
    in_val     => in_val,
    in_eop     => in_eop,

    out_addr   => trans_addr,
    out_data   => trans_data,
    out_val    => trans_val,
    out_eop    => trans_eop
  );

  u_transpose_out : entity common_lib.common_transpose
  generic map (
    g_pipeline_shiftreg  => g_pipeline_shiftreg,
    g_pipeline_transpose => g_pipeline_transpose,
    g_pipeline_hold      => g_pipeline_hold,
    g_pipeline_select    => g_pipeline_select,
    g_nof_data           => g_nof_data,
    g_data_w             => g_data_w,
    g_addr_w             => g_addr_w,
    g_addr_offset        => g_addr_offset
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_offset  => trans_offset,
    in_addr    => trans_addr,
    in_data    => trans_data,
    in_val     => trans_val,
    in_eop     => trans_eop,

    out_addr   => out_addr,
    out_data   => out_data,
    out_val    => out_val,
    out_eop    => out_eop
  );

  -- Verification p_verify
  proc_common_verify_data(1, clk, verify_en, ready, out_val, out_addr, prev_out_addr);
  proc_common_verify_data(1, clk, verify_en, ready, out_val, out_data, prev_out_data);
end tb;
