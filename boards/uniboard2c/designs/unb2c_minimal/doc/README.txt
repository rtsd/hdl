This is the minimal design for Uniboard2c.

Here are quick steps to compile and use designs in RadionHDL.
-------------------------------------------------------------


Steps to go:
------------

-> At all times run:

     cd ${GIT}/hdl
     . init_hdl.sh

-> In case of a new installation, the IP's have to be generated for Arria10. 

     echo $HDL_BUILD_DIR         # should be something like /home/user/git/hdl/build
     rm -r $HDL_BUILD_DIR/unb2c  # optional
     compile_altera_simlibs unb2c
     generate_ip_libs unb2c


1. Start with the Oneclick Commands:
     modelsim_config unb2c
     quartus_config unb2c


2. Generate MMM for QSYS (select one of these revisions):
    run_qsys_pro unb2c unb2c_minimal



3. -> From here either continue to Modelsim (simulation) or Quartus (synthesis)

Simulation
----------
    run_modelsim unb2c




Synthesis
---------
Quartus instructions: (select one of these revisions):
    run_qcomp unb2c unb2c_minimal


Or compile using the Quartus GUI:
    run_quartus unb2c
load the project now from the build directory.




4. Load firmware
----------------
Using JTAG: Start the Quartus GUI and open: tools->programmer.
            Then click auto-detect; (click 4x ok)
            Use 'change file' to select the correct .sof file (in $HDL_WORK/build/unb2c/quartus/unb2c_test_...) for each FPGA
            Select the FPGA(s) which has to be programmed
            Click 'start'
Using EPCS: See step 6 below.


