-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose: Monitor the sosi and siso control of a stream per sync interval
-- Description:
--   The mon_evt pulses when new monitor output is available regarding the
--   previous in_sosi.sync interval:
--   . mon_sync_timeout        = '1' when the in_sosi.sync did not occur
--   . mon_ready_stable        = '1' when ready was always '1' during last sync interval
--   . mon_xon_stable          = '1' when xon   was always '1' during last sync interval
--   . mon_bsn_at_sync         = BSN at sync
--   . mon_nof_sop             = number of sop during last sync interval
--   . mon_nof_err             = number of err at eop during last sync interval
--   . mon_nof_valid           = number of valid during last sync interval;
--   . mon_latency             = number of clock cycles between ref_sync and in_sosi.sync;
--
-- Remarks:
-- . Assumes RL > 0 so each active valid indicates a new data (with RL = 0 the
--   valid remains active until an acknowledge by ready)
-- . If mon_sync_timeout = '1', all output vectors are forced -1 (all ones) to
--   indicate they are not valid.
-- . The single bit siso status signals mon_ready_stable and mon_xon_stable
--   are less useful, because they do not support an invalid value.
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_bsn_monitor_v2 is
  generic (
    g_sync_timeout  : natural := 200 * 10**6;  -- choose >= nof clk cycles per sync period
    g_error_bi      : natural := 0
  );
  port (
    rst                     : in  std_logic := '0';
    clk                     : in  std_logic;

    -- ST interface
    in_siso                 : in  t_dp_siso;
    in_sosi                 : in  t_dp_sosi;
    ref_sync                : in  std_logic := '0';  -- reference sync input
    -- MM interface
    -- . control
    mon_evt                 : out std_logic;  -- pulses when new monitor output is available regarding the previous sync interval
    mon_sync                : out std_logic;  -- pulses every in_sosi.sync interval
    mon_sync_timeout        : out std_logic;
    -- . siso
    mon_ready_stable        : out std_logic;
    mon_xon_stable          : out std_logic;
    -- . sosi
    mon_bsn_at_sync         : out std_logic_vector;
    mon_nof_sop             : out std_logic_vector;
    mon_nof_err             : out std_logic_vector;
    mon_nof_valid           : out std_logic_vector;
    mon_latency             : out std_logic_vector
  );
end dp_bsn_monitor_v2;

architecture rtl of dp_bsn_monitor_v2 is
  constant c_sync_timeout_w : natural := ceil_log2(g_sync_timeout);
  constant c_bsn_w          : natural := mon_bsn_at_sync'length;
  constant c_cnt_sop_w      : natural := mon_nof_sop'length;
  constant c_cnt_valid_w    : natural := mon_nof_valid'length;
  constant c_cnt_latency_w  : natural := mon_latency'length;

  constant inv_mon_bsn_at_sync       : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '1');
  constant inv_mon_nof_sop           : std_logic_vector(c_cnt_sop_w - 1 downto 0) := (others => '1');
  constant inv_mon_nof_err           : std_logic_vector(c_cnt_sop_w - 1 downto 0) := (others => '1');
  constant inv_mon_nof_valid         : std_logic_vector(c_cnt_valid_w - 1 downto 0) := (others => '1');
  constant inv_mon_latency           : std_logic_vector(c_cnt_latency_w - 1 downto 0) := (others => '1');

  signal ready                       : std_logic;
  signal nxt_ready                   : std_logic;
  signal ready_stable                : std_logic;
  signal xon                         : std_logic;
  signal nxt_xon                     : std_logic;
  signal xon_stable                  : std_logic;

  signal err                         : std_logic;
  signal nxt_err                     : std_logic;
  signal valid                       : std_logic;
  signal nxt_valid                   : std_logic;
  signal sop                         : std_logic;
  signal nxt_sop                     : std_logic;
  signal sync_reg2                   : std_logic;
  signal sync_reg                    : std_logic;
  signal sync                        : std_logic;
  signal nxt_sync                    : std_logic;
  signal bsn                         : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_bsn                     : std_logic_vector(c_bsn_w - 1 downto 0);
  signal cnt_sop                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nof_sop                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal cnt_err                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);  -- use c_cnt_sop_w, because at maximium all frames have an error
  signal nof_err                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal cnt_valid                   : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal nof_valid                   : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal cnt_latency                 : std_logic_vector(c_cnt_latency_w - 1 downto 0);
  signal latency                     : std_logic_vector(c_cnt_latency_w - 1 downto 0);
  signal ref_sync_reg                : std_logic := '0';

  signal i_mon_ready_stable          : std_logic;
  signal i_mon_xon_stable            : std_logic;
  signal i_mon_bsn_at_sync           : std_logic_vector(c_bsn_w - 1 downto 0);
  signal i_mon_nof_sop               : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal i_mon_nof_err               : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal i_mon_nof_valid             : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal i_mon_latency               : std_logic_vector(c_cnt_latency_w - 1 downto 0);
  signal i_current_bsn               : std_logic_vector(c_bsn_w - 1 downto 0);

  signal sync_timeout_cnt            : std_logic_vector(c_sync_timeout_w - 1 downto 0);
  signal sync_timeout                : std_logic;
  signal sync_timeout_n              : std_logic;
  signal nxt_sync_timeout            : std_logic;
  signal sync_timeout_revt           : std_logic;

  signal nxt_mon_evt                 : std_logic;
  signal nxt_mon_ready_stable        : std_logic;
  signal nxt_mon_xon_stable          : std_logic;
  signal nxt_mon_bsn_at_sync         : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_mon_nof_sop             : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nxt_mon_nof_err             : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nxt_mon_nof_valid           : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal nxt_mon_latency             : std_logic_vector(c_cnt_latency_w - 1 downto 0);
  signal nxt_current_bsn             : std_logic_vector(c_bsn_w - 1 downto 0);
begin
  mon_sync                <= sync;
  mon_sync_timeout        <= sync_timeout;
  mon_ready_stable        <= i_mon_ready_stable;
  mon_xon_stable          <= i_mon_xon_stable;
  mon_bsn_at_sync         <= i_mon_bsn_at_sync  when sync_timeout = '0' else inv_mon_bsn_at_sync;
  mon_nof_sop             <= i_mon_nof_sop      when sync_timeout = '0' else inv_mon_nof_sop;
  mon_nof_err             <= i_mon_nof_err      when sync_timeout = '0' else inv_mon_nof_err;
  mon_nof_valid           <= i_mon_nof_valid    when sync_timeout = '0' else inv_mon_nof_valid;
  mon_latency             <= i_mon_latency      when sync_timeout = '0' else inv_mon_latency;

  nxt_mon_evt          <= sync or sync_timeout_revt;
  nxt_mon_ready_stable <= ready_stable when sync = '1' else i_mon_ready_stable;
  nxt_mon_xon_stable   <= xon_stable   when sync = '1' else i_mon_xon_stable;
  nxt_mon_bsn_at_sync  <= bsn          when sync = '1' else i_mon_bsn_at_sync;
  nxt_mon_nof_sop      <= nof_sop      when sync = '1' else i_mon_nof_sop;
  nxt_mon_nof_err      <= nof_err      when sync = '1' else i_mon_nof_err;
  nxt_mon_nof_valid    <= nof_valid    when sync = '1' else i_mon_nof_valid;
  nxt_mon_latency      <= latency      when sync_reg2 = '1' else i_mon_latency;  -- use sync_reg2 due to ref_sync_reg

  nof_sop   <= cnt_sop;
  nof_err   <= cnt_err;
  nof_valid <= cnt_valid;
  latency   <= cnt_latency;

  -- Register ref_sync to ease timing closure for ref_sync fanout to (many) dp_bsn_monitor_v2
  -- instances. The ref_sync_reg is used to restart cnt_latency. Therefore the sync_reg2
  -- (instead of sync) is needed to capture latency (= cnt_latency) not too early, in case
  -- ref_sync = in_sosi.sync, so capture cnt_latency at or after the cnt_latency has restarted.
  ref_sync_reg <= ref_sync when rising_edge(clk);

  u_sync_timeout_cnt : entity common_lib.common_counter
  generic map (
    g_width => c_sync_timeout_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => sync_timeout_n,
    count   => sync_timeout_cnt
  );

  sync_timeout_n <= not nxt_sync_timeout;

  nxt_sync_timeout <= '1' when unsigned(sync_timeout_cnt) >= g_sync_timeout else '0';

  u_sync_timeout_revt : entity common_lib.common_evt
  generic map (
    g_evt_type   => "RISING",
    g_out_invert => false,
    g_out_reg    => false
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => sync_timeout,
    out_evt  => sync_timeout_revt
  );

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      -- internal
      ready                     <= '0';
      xon                       <= '0';
      valid                     <= '0';
      sop                       <= '0';
      err                       <= '0';
      sync                      <= '0';
      sync_reg                  <= '0';
      sync_reg2                 <= '0';
      bsn                       <= (others => '0');
      -- output
      mon_evt                   <= '0';
      sync_timeout              <= '0';
      i_mon_ready_stable        <= '0';
      i_mon_xon_stable          <= '0';
      i_mon_bsn_at_sync         <= (others => '0');
      i_mon_nof_sop             <= (others => '0');
      i_mon_nof_err             <= (others => '0');
      i_mon_nof_valid           <= (others => '0');
      i_mon_latency             <= (others => '0');
      i_current_bsn             <= (others => '0');
    elsif rising_edge(clk) then
      -- internal
      ready                     <= nxt_ready;
      xon                       <= nxt_xon;
      valid                     <= nxt_valid;
      sop                       <= nxt_sop;
      err                       <= nxt_err;
      sync                      <= nxt_sync;
      sync_reg                  <= sync;
      sync_reg2                 <= sync_reg;
      bsn                       <= nxt_bsn;
      -- output
      mon_evt                   <= nxt_mon_evt;
      sync_timeout              <= nxt_sync_timeout;
      i_mon_ready_stable        <= nxt_mon_ready_stable;
      i_mon_xon_stable          <= nxt_mon_xon_stable;
      i_mon_bsn_at_sync         <= nxt_mon_bsn_at_sync;
      i_mon_nof_sop             <= nxt_mon_nof_sop;
      i_mon_nof_err             <= nxt_mon_nof_err;
      i_mon_nof_valid           <= nxt_mon_nof_valid;
      i_mon_latency             <= nxt_mon_latency;
      i_current_bsn             <= nxt_current_bsn;
    end if;
  end process;

  -- siso
  nxt_ready <= in_siso.ready;
  nxt_xon   <= in_siso.xon;

  u_ready_stable : entity common_lib.common_stable_monitor
  port map (
    rst          => rst,
    clk          => clk,
    -- MM
    r_in         => ready,
    r_stable     => ready_stable,
    r_stable_ack => sync
  );

  u_xon_stable : entity common_lib.common_stable_monitor
  port map (
    rst          => rst,
    clk          => clk,
    -- MM
    r_in         => xon,
    r_stable     => xon_stable,
    r_stable_ack => sync
  );

  -- Sample the BSN, because BSN is only valid during sop.
  nxt_current_bsn <= in_sosi.bsn(c_bsn_w - 1 downto 0) when in_sosi.sop = '1' else i_current_bsn;

  -- sosi
  -- . no need to AND sop, eop with valid, because can only be active when valid = '1'
  -- . no need to AND sync     with sop,   because can only be active when sop   = '1'
  nxt_valid                   <= in_sosi.valid;
  nxt_sop                     <= in_sosi.sop;
  nxt_sync                    <= in_sosi.sync;
  nxt_err                     <= in_sosi.err(g_error_bi)         when in_sosi.eop = '1' else '0';  -- assume sosi.err(g_error_bi) = '1' at eop indicates an error
  nxt_bsn                     <= in_sosi.bsn(c_bsn_w - 1 downto 0) when in_sosi.sop = '1' else bsn;  -- keep bsn as defined at sop

  u_cnt_sop : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_sop_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_ld  => sync,  -- using cnt_ld instead of cnt clr to reset to 1 as the sop at the sync counts too.
    cnt_en  => sop,
    load    => TO_SVEC(1, c_cnt_sop_w),
    count   => cnt_sop
  );

  u_nof_err : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_sop_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => err,
    count   => cnt_err
  );

  u_cnt_valid : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_valid_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_ld  => sync,  -- using cnt_ld instead of cnt clr to reset to 1 as the valid at the sync counts too.
    cnt_en  => valid,
    load    => TO_SVEC(1, c_cnt_valid_w),
    count   => cnt_valid
  );

  u_cnt_latency : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_latency_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => ref_sync_reg,
    cnt_en  => '1',
    count   => cnt_latency
  );
end rtl;
