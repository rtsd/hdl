--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   The dp_repack_data works both as packer and as unpacker.
--
-- Block diagram:
--
-- A) Functional
--   The drawing shows g_in_nof_words=4 and g_out_nof_words=2 as example:
--
--                dp_repack_in               dp_repack_out
--                     ___                        ___
--                    |   |      pack_sosi       |   |--> src_out
--                    | 3 |--------------------->| 1 |
--                    |   |                      |   |
--                    | 2 | ^                    |   | ^
--                    |   | |valid               |   | |shift
--                    | 1 | |flush               | 0 | |
--                    |   | |                    |   | |
--                    | 0 |                      |   |
--   snk_in        -->|___|      pack_siso       |___|
--   snk_out.ready <--     <---------------------     <-- src_in.ready
--   snk_out.xon   <------------------------------------- src_in.xon
--
--
-- B) Flow control
--
--      RL=1                        RL=1               RL=1
--        .                           .                  .
--        .        /-----------------------------------------\
--        .        |                  .           _____  .   |
--        .        |   /------\     nxt_r        |     | r   |
--        .        \-->|      |---*-------*----->|p_reg|-----*---> src_out
--        .            |      |   |       |      |_____|
--     snk_in -------->|p_comb|<--|-------|--------------*-------- src_in
--                     |      |   |       |              |
--                     |      |   |       v              |
--                     |      |   |   /-------\          |
--                     |      |   |   |p_flow |          |
--                     \------/   |   \-------/          |
--                                |       |              |
--           nxt_r.hold_out.valid |       |              |
--                                v       |              |
--                                /|      |r_snk_out     |
--                               |0|------/              |
--    snk_out <------------------| |                     |
--                               |1|---------------------/
--                                \|
--
-- Description:
--   The dp_repack_data repacks g_in_nof_words of width g_in_dat_w into
--   g_out_nof_words of width g_out_dat_w.
--
-- . g_in_bypass, g_out_bypass
--   The dp_repack_in and dp_repack_out can be bypassed to save logic and to
--   avoid the pipeline stage. Default both are FALSE, but they can be set
--   TRUE if:
--
--   . g_in_bypass =TRUE if g_in_nof_words=g_out_nof_words or g_in_nof_words=1
--   . g_out_bypass=TRUE if g_in_nof_words=g_out_nof_words or g_out_nof_words=1
--
--   Both the dp_repack_in and dp_repack_out stage do work correctly independent
--   of the g_*_bypass setting. When g_*_bypass=FALSE then they merely
--   add a transparant pipeline delay. It is important that they also work for
--   g_*_bypass=FALSE because that gives confidence that their implementation
--   structure is ok.
--
-- . g_in_nof_words and input block size
--   The input block size in words is indicated by snk_in.sop and snk_in.eop.
--   Each subsection of g_in_nof_words is packed into g_out_nof_words. The
--   input block size does not have to be a multiple of g_in_nof_words. When
--   the snk_in.eop occurs the last repack is initiated without need for input
--   data padding. If the block length is an integer multiple of
--   g_in_nof_words then the dp_repack_data introduces no gaps between blocks.
--   If the block length is a fractional multiple of g_in_nof_words then there
--   will be a gap after the block due to that the dp_repack_in needs to
--   shift up the last subsection for the 'missing' input words.
--
-- . g_in_dat_w*g_in_nof_words <, =, > g_in_dat_w*g_in_nof_words
--   . = : no subsection zero padding
--   . < : the subsections will be zero padded
--   . > : then the input must have sufficient zero padded bits per
--         subsection that can be stripped without data loss.
--
-- . Resolution of the empty field
--   The g_in_symbol_w is used to define the resolution of snk_in.empty and
--   the g_out_symbol_w is used to define the resolution of src_out.empty. If
--   they are 1 then the resolution is in number of bits, because the symbol
--   size is then 1 bit. Their value has no effect on the packed data it self,
--   only on the meaning of the empty field. Hence if the empty field is not
--   used, then the setting of g_in_symbol_w and g_out_symbol_w is dont care.
--
-- Remarks:
-- . Originally reused from LOFAR rad_repack.vhd and rad_repack(rtl).vhd. This
--   dp_repack_data still uses the shift in input register in and the shift out
--   output register, but no longer the intermediate buffer register.
--   Using shift in and shift out may ease timing closure because the routing
--   is more local compared to using a demultiplexer to put the input data in
--   the input register and a multiplexer to get the data directly from the
--   output register. For the demultiplexer / multiplexer it would be possible
--   to only use one internal register.
--   Using shift up is sufficient, the shift down option is not needed. With
--   shift up the data is input a [0] and output the high index.
--   Instead of requiring an snk_in.valid duty cycle this dp_repack_data uses
--   snk_out.ready flow control and can handle src_in.ready flow control.
--
-- . To pack ETH/IP/UDP header slv of 14 + 20 + 8 = 42 octets into 32 bit words
--   use:
--     u_dp_repack_data : ENTITY dp_lib.dp_repack_data
--     GENERIC MAP (
--       g_in_bypass         => TRUE,
--       g_in_dat_w          => 8 * 42,
--       g_in_nof_words      => 1,
--       g_in_symbol_w       => 8,
--       g_out_bypass        => FALSE,
--       g_out_dat_w         => 32,
--       g_out_nof_words     => 11,
--       g_out_symbol_w      => 8
--     )
--   The src_out.empty will be 2, because:
--     (g_out_dat_w*g_out_nof_words-g_in_dat_w*g_in_nof_words)/g_out_symbol_w
--      = (32*11 - 42*8*1)/ 8 = 2 octet symbols
-- . Instead of using dp_sosi.data for intermediate results in dp_repack_data,
--   a seperate std_logic_vector is used to carry the sosi data as the required
--   vector width can become larger than c_dp_stream_data_w. Note that for the
--   in/out sosi of dp_repack_data, the dp_sosi.data field is still used such
--   that there is no added complexity for the user of dp_repack_data.
--
-- Design steps:
-- * In total the development took 5 days. On day 3 I was in distress because
--   I could not get it to work so I needed to rethink. After changing to the
--   new flow control scheme that uses nxt_r the design was gradually improved
--   by getting the dp_repack_data instances in tb_tb_dp_repack_data to work one
--   by one. First only for e_active stimuli and later also for e_random and
--   e_pulse. Initially about 80 % of the functionality was implemented but
--   and subsequently each feature was verified starting with the basic
--   features and then themore detailed features. This step by step approach
--   makes that the bugs appear one by one instead of all together. Without a
--   step by step approach the bugs are too big to solve.
--   . First cases with g_in_nof_words=1 and g_out_nof_words were made to work
--     for different g_pkt_len <, =, > g_in_nof_words.
--   . Then the empty functionality for g_pkt_len MOD g_in_nof_words /= 0 was
--     added.
--   . Tried g_out_dat_w=1 which makes dp_repack_data a serializer/deserializer.
--   . Then apply external flow control using c_dp_flow_control_enum_arr in
--     the tb_tb_dp_repack_data was verified resulting in small corrections.
--   . Then verified g_in_dat_w * g_in_nof_words > or < g_out_dat_w *
--     g_out_nof_words which require padding in the subsection. The > case
--     occurs for packing and the < case then occurs for unpacking.
--   . Added g_bypass to force using wires instead of a void dp_repack_in or
--     dp_repack_out stage.
--   . Verified g_in_symbol_w and g_out_symbol_w /= 1.
-- * The development used the tb_dp_repack_data testbench that does a pack and
--   an unpack to be able to verify the data. The c_no_unpack and
--   c_enable_repack_in and c_enable_repack_out parameters in the tb are
--   useful to be able to isolate a component for debugging.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_repack_data is
  generic (
    g_use_repack_in_load : boolean := false;
    g_enable_repack_in   : boolean := true;
    g_enable_repack_out  : boolean := true;
    g_in_bypass          : boolean := false;
    g_in_dat_w           : natural;
    g_in_nof_words       : natural;
    g_in_symbol_w        : natural := 1;  -- default 1 for snk_in.empty in nof bits, else use power of 2
    g_out_bypass         : boolean := false;
    g_out_dat_w          : natural;
    g_out_nof_words      : natural;
    g_out_symbol_w       : natural := 1;  -- default 1 for src_out.empty in nof bits, else use power of 2
    g_pipeline_ready     : boolean := false  -- TRUE to pipeline ready from src_in to snk_out.
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    snk_out          : out t_dp_siso;
    snk_in           : in  t_dp_sosi;

    src_in           : in  t_dp_siso := c_dp_siso_rdy;
    src_out          : out t_dp_sosi
  );
end dp_repack_data;

architecture str of dp_repack_data is
  constant c_in_buf_dat_w      : natural := g_in_dat_w * g_in_nof_words;

  signal snk_in_data       : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal i_snk_out         : t_dp_siso;
  signal i_snk_in          : t_dp_sosi;

  signal pack_siso         : t_dp_siso;
  signal pack_sosi         : t_dp_sosi;
  signal pack_sosi_data    : std_logic_vector(c_in_buf_dat_w - 1 downto 0);

  signal i_src_out         : t_dp_sosi;
  signal src_out_data      : std_logic_vector(g_out_dat_w - 1 downto 0);

  signal snk_out_ready_reg : std_logic_vector(0 to c_dp_stream_rl);
  signal pack_ready_reg    : std_logic_vector(0 to c_dp_stream_rl);
begin
  gen_dp_pipeline_ready: if g_pipeline_ready = true generate
    u_dp_pipeline_ready : entity dp_lib.dp_pipeline_ready
    port map (
      rst     => rst,
      clk     => clk,

      snk_out => snk_out,
      snk_in  => snk_in,

      src_in  => i_snk_out,
      src_out => i_snk_in
    );
  end generate;

  gen_no_dp_pipeline_ready: if g_pipeline_ready = false generate
    i_snk_in <= snk_in;
    snk_out  <= i_snk_out;
  end generate;

  src_out <= i_src_out;

  snk_in_data    <= i_snk_in.data(g_in_dat_w - 1 downto 0);
  src_out_data   <= i_src_out.data(g_out_dat_w - 1 downto 0);

  no_dp_repack_in : if g_enable_repack_in = false generate
    i_snk_out <= pack_siso;
    pack_sosi <= i_snk_in;
    pack_sosi_data <= RESIZE_UVEC(snk_in_data, c_in_buf_dat_w);
  end generate;

  gen_dp_repack_in : if g_enable_repack_in = true generate
    u_dp_repack_in : entity work.dp_repack_data_in
    generic map (
      g_use_in_load  => g_use_repack_in_load,
      g_bypass       => g_in_bypass,
      g_in_dat_w     => g_in_dat_w,
      g_in_nof_words => g_in_nof_words,
      g_in_symbol_w  => g_in_symbol_w
    )
    port map (
      rst      => rst,
      clk      => clk,

      snk_out  => i_snk_out,
      snk_in   => i_snk_in,

      src_in   => pack_siso,
      src_out  => pack_sosi,
      src_out_data => pack_sosi_data
    );
  end generate;

  no_dp_repack_out : if g_enable_repack_out = false generate
    pack_siso <= src_in;
    i_src_out <= pack_sosi;
  end generate;

  gen_dp_repack_out : if g_enable_repack_out = true generate
    u_dp_repack_out : entity work.dp_repack_data_out_shift
    generic map (
      g_bypass        => g_out_bypass,
      g_in_buf_dat_w  => c_in_buf_dat_w,
      g_out_dat_w     => g_out_dat_w,
      g_out_nof_words => g_out_nof_words,
      g_out_symbol_w  => g_out_symbol_w
    )
    port map (
      rst      => rst,
      clk      => clk,

      snk_out  => pack_siso,
      snk_in   => pack_sosi,
      snk_in_data => pack_sosi_data,

      src_in   => src_in,
      src_out  => i_src_out
    );
  end generate;

  -- Simulation only: internal stream RL verification
  proc_dp_siso_alert(clk, i_snk_in, i_snk_out, snk_out_ready_reg);
  proc_dp_siso_alert(clk, pack_sosi, pack_siso, pack_ready_reg);
end str;
