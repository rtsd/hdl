# Author : Eric Kooistra
# Purpose: Calculate cost of fiber resources for data output by SDP in LOFAR2
# Descriptions:
# . See https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Allocation+of+10GbE+ports+in+SDP
# Usage:
# . python station_sdp_fiber_cost.py

# Number of stations in LOFAR2
nof_core = 24
nof_remote = 14
nof_international = 14

# Component prices in Euro incl. VAT
QSFP_optic     = 47   # [RD-15]
MTP8_cable     = 121  # [RD-13]
breakout_box   = 229  # [RD-10]
rack_mount     = 80   # [RD-11]
LC2_cable      = 6    # [RD-14]
breakout_cable = 71   # [RD-12]
SFP_optic      = 33   # [RD-16]

# Cost per station
BaseLine_STAT_core =  3 * QSFP_optic +  3 * MTP8_cable + 1 * breakout_box +   1 * rack_mount +  3 * LC2_cable +  3 * SFP_optic
QuickBlitz_A1_core = 21 * QSFP_optic + 21 * MTP8_cable + 7 * breakout_box +   1 * rack_mount + 24 * LC2_cable + 24 * SFP_optic
QuickBlitz_A2_core = 24 * QSFP_optic +                  24 * breakout_cable + 1 * rack_mount +                  24 * SFP_optic
QuickBlitz_B1_core =  6 * QSFP_optic +  6 * MTP8_cable + 2 * breakout_box +                    24 * LC2_cable + 24 * SFP_optic
QuickBlitz_B2_core =  6 * QSFP_optic +                   6 * breakout_cable +                                   24 * SFP_optic

BaseLine_STAT_remote =  2 * QSFP_optic +  2 * MTP8_cable + 1 * breakout_box +   1 * rack_mount +  2 * LC2_cable +  2 * SFP_optic
QuickBlitz_A1_remote = 22 * QSFP_optic + 22 * MTP8_cable + 7 * breakout_box +   1 * rack_mount + 24 * LC2_cable + 24 * SFP_optic
QuickBlitz_A2_remote = 24 * QSFP_optic +                  24 * breakout_cable + 1 * rack_mount +                  24 * SFP_optic
QuickBlitz_B1_remote =  6 * QSFP_optic +  6 * MTP8_cable + 2 * breakout_box +                    24 * LC2_cable + 24 * SFP_optic
QuickBlitz_B2_remote =  6 * QSFP_optic +                   6 * breakout_cable +                                   24 * SFP_optic

BaseLine_STAT_int =  2 * QSFP_optic +  2 * MTP8_cable +  1 * breakout_box +   1 * rack_mount +  2 * LC2_cable +  2 * SFP_optic
QuickBlitz_A1_int = 30 * QSFP_optic + 30 * MTP8_cable + 10 * breakout_box +   2 * rack_mount + 32 * LC2_cable + 32 * SFP_optic
QuickBlitz_A2_int = 32 * QSFP_optic +                   32 * breakout_cable + 2 * rack_mount +                  32 * SFP_optic
QuickBlitz_B1_int =  8 * QSFP_optic +  8 * MTP8_cable +  3 * breakout_box +                    32 * LC2_cable + 32 * SFP_optic
QuickBlitz_B2_int =  8 * QSFP_optic +                    8 * breakout_cable +                                   32 * SFP_optic

# Cost in kEuro
BaseLine_STAT_core = BaseLine_STAT_core / 1000
QuickBlitz_A1_core = QuickBlitz_A1_core / 1000
QuickBlitz_A2_core = QuickBlitz_A2_core / 1000
QuickBlitz_B1_core = QuickBlitz_B1_core / 1000
QuickBlitz_B2_core = QuickBlitz_B2_core / 1000

BaseLine_STAT_remote = BaseLine_STAT_remote / 1000
QuickBlitz_A1_remote = QuickBlitz_A1_remote / 1000
QuickBlitz_A2_remote = QuickBlitz_A2_remote / 1000
QuickBlitz_B1_remote = QuickBlitz_B1_remote / 1000
QuickBlitz_B2_remote = QuickBlitz_B2_remote / 1000

BaseLine_STAT_int = BaseLine_STAT_int / 1000
QuickBlitz_A1_int = QuickBlitz_A1_int / 1000
QuickBlitz_A2_int = QuickBlitz_A2_int / 1000
QuickBlitz_B1_int = QuickBlitz_B1_int / 1000
QuickBlitz_B2_int = QuickBlitz_B2_int / 1000

# Cost for sum of stations in LOFAR2
BaseLine_STAT_cores = BaseLine_STAT_core * nof_core
QuickBlitz_A1_cores = QuickBlitz_A1_core * nof_core
QuickBlitz_A2_cores = QuickBlitz_A2_core * nof_core
QuickBlitz_B1_cores = QuickBlitz_B1_core * nof_core
QuickBlitz_B2_cores = QuickBlitz_B2_core * nof_core

BaseLine_STAT_remotes = BaseLine_STAT_remote * nof_remote
QuickBlitz_A1_remotes = QuickBlitz_A1_remote * nof_remote
QuickBlitz_A2_remotes = QuickBlitz_A2_remote * nof_remote
QuickBlitz_B1_remotes = QuickBlitz_B1_remote * nof_remote
QuickBlitz_B2_remotes = QuickBlitz_B2_remote * nof_remote

BaseLine_STAT_ints = BaseLine_STAT_int * nof_international
QuickBlitz_A1_ints = QuickBlitz_A1_int * nof_international
QuickBlitz_A2_ints = QuickBlitz_A2_int * nof_international
QuickBlitz_B1_ints = QuickBlitz_B1_int * nof_international
QuickBlitz_B2_ints = QuickBlitz_B2_int * nof_international

BaseLine_STAT_lofar = BaseLine_STAT_cores + BaseLine_STAT_remotes + BaseLine_STAT_ints
QuickBlitz_A1_lofar = QuickBlitz_A1_cores + QuickBlitz_A1_remotes + QuickBlitz_A1_ints
QuickBlitz_A2_lofar = QuickBlitz_A2_cores + QuickBlitz_A2_remotes + QuickBlitz_A2_ints
QuickBlitz_B1_lofar = QuickBlitz_B1_cores + QuickBlitz_B1_remotes + QuickBlitz_B1_ints
QuickBlitz_B2_lofar = QuickBlitz_B2_cores + QuickBlitz_B2_remotes + QuickBlitz_B2_ints

print('BaseLine_STAT_core = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (BaseLine_STAT_core, nof_core, BaseLine_STAT_cores))
print('QuickBlitz_A1_core = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A1_core, nof_core, QuickBlitz_A1_cores))
print('QuickBlitz_A2_core = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A2_core, nof_core, QuickBlitz_A2_cores))
print('QuickBlitz_B1_core = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B1_core, nof_core, QuickBlitz_B1_cores))
print('QuickBlitz_B2_core = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B2_core, nof_core, QuickBlitz_B2_cores))
print()
print('BaseLine_STAT_remote = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (BaseLine_STAT_remote, nof_remote, BaseLine_STAT_remotes))
print('QuickBlitz_A1_remote = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A1_remote, nof_remote, QuickBlitz_A1_remotes))
print('QuickBlitz_A2_remote = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A2_remote, nof_remote, QuickBlitz_A2_remotes))
print('QuickBlitz_B1_remote = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B1_remote, nof_remote, QuickBlitz_B1_remotes))
print('QuickBlitz_B2_remote = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B2_remote, nof_remote, QuickBlitz_B2_remotes))
print()
print('BaseLine_STAT_int  = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (BaseLine_STAT_int, nof_international, BaseLine_STAT_ints))
print('QuickBlitz_A1_int  = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A1_int, nof_international, QuickBlitz_A1_ints))
print('QuickBlitz_A2_int  = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_A2_int, nof_international, QuickBlitz_A2_ints))
print('QuickBlitz_B1_int  = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B1_int, nof_international, QuickBlitz_B1_ints))
print('QuickBlitz_B2_int  = %5.2f kEuro * %2d = %6.1f kEuro incl. VAT' % (QuickBlitz_B2_int, nof_international, QuickBlitz_B2_ints))
print()
print()
print('BaseLine_STAT_lofar  = %6.0f kEuro incl. VAT' % BaseLine_STAT_lofar)
print('QuickBlitz_A1_lofar  = %6.0f kEuro incl. VAT' % QuickBlitz_A1_lofar)
print('QuickBlitz_A2_lofar  = %6.0f kEuro incl. VAT' % QuickBlitz_A2_lofar)
print('QuickBlitz_B1_lofar  = %6.0f kEuro incl. VAT' % QuickBlitz_B1_lofar)
print('QuickBlitz_B2_lofar  = %6.0f kEuro incl. VAT' % QuickBlitz_B2_lofar)
