-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 14 feb 2023
-- Purpose: Verify multiple variations of tb_dp_reverse_n_data
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_reverse_n_data_fc is
end tb_tb_dp_reverse_n_data_fc;

architecture tb of tb_tb_dp_reverse_n_data_fc is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_rep_act   : natural :=   3;
  constant c_rep_rnd   : natural := 100;
begin
-- -- general
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always e_active, e_random or e_pulse flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
-- -- specific
-- g_pipeline               : NATURAL := 1;  -- 0 for combinatorial, > 0 for registers
-- g_nof_repeat             : NATURAL := 5;
-- g_reverse_len            : NATURAL := 4;
-- g_pkt_gap                : NATURAL := 0

--            g_pipeline
--            |  g_reverse_len
--            |  | g_pkt_gap
--            |  | |
  u_act_act_pipe_1_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_active, e_active, 1, c_rep_act, 1, 0);
  u_act_act_pipe_2_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_active, e_active, 1, c_rep_act, 2, 0);
  u_act_act_pipe_4_7  : entity work.tb_dp_reverse_n_data_fc generic map(e_active, e_active, 1, c_rep_act, 4, 7);
  u_act_act_comb_4_7  : entity work.tb_dp_reverse_n_data_fc generic map(e_active, e_active, 0, c_rep_act, 4, 7);

  u_rnd_rnd_pipe_1_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 1, c_rep_rnd, 1, 0);
  u_rnd_rnd_pipe_2_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 1, c_rep_rnd, 2, 0);
  u_rnd_rnd_pipe_4_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 1, c_rep_rnd, 4, 0);
  u_rnd_rnd_pipe_4_7  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 1, c_rep_rnd, 4, 7);

  u_pls_pls_comb_3_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_pulse,  e_pulse,  0, c_rep_rnd, 3, 0);
  u_pls_pls_pipe_3_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_pulse,  e_pulse,  1, c_rep_rnd, 3, 0);
  u_rnd_rnd_comb_3_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 0, c_rep_rnd, 3, 0);
  u_rnd_rnd_pipe_3_0  : entity work.tb_dp_reverse_n_data_fc generic map(e_random, e_random, 1, c_rep_rnd, 3, 0);
end tb;
