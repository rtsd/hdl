-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Test tb_st_histogram in with several parameter sets
-- Usage
-- . as 8
-- . run -all
-- . Testbenches are self-checking.
-- . The sine wave test benches are best for verification by eye in the wave window.
--   . tb_st_histogram uses a sine wave as input by default

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_st_histogram is
end tb_tb_st_histogram;

architecture tb of tb_tb_st_histogram is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--  g_nof_sync     : NATURAL := 4;
--  g_data_w       : NATURAL := 8;
--  g_nof_bins     : NATURAL := 256;
--  g_nof_data     : NATURAL := 1024;
--  g_stimuli_mode : STRING  := "dc";
--  g_data_type    : STRING  := "unsigned";
--  g_lock_sine    : BOOLEAN := TRUE

-- Counter data
u_tb_st_histogram_0 : entity work.tb_st_histogram generic map ( 7,  8,  256, 1024, "counter", "unsigned");  -- Incoming data repeats 1024/ 256= 4 times: Bin count =  4
u_tb_st_histogram_1 : entity work.tb_st_histogram generic map ( 6, 10,  256, 4096, "counter", "unsigned");  -- Incoming data repeats 4096/ 256=16 times: Bin count = 16
u_tb_st_histogram_2 : entity work.tb_st_histogram generic map ( 5, 12,  512, 4096, "counter", "unsigned");  -- Incoming data repeats 4096/ 512= 8 times: Bin count =  8
u_tb_st_histogram_3 : entity work.tb_st_histogram generic map ( 4, 13, 1024, 8192, "counter", "unsigned");  -- Incoming data repeats 8192/1024= 8 times: Bin count =  8
u_tb_st_histogram_4 : entity work.tb_st_histogram generic map (20,  6,   64,  128, "counter", "unsigned");  -- Incoming data repeats  128/  64= 2 times: Bin count =  2

-- DC signal
u_tb_st_histogram_5 : entity work.tb_st_histogram generic map ( 2,  8,  256, 1000, "dc", "unsigned");
u_tb_st_histogram_6 : entity work.tb_st_histogram generic map ( 6, 10,  256, 4000, "dc", "unsigned");
u_tb_st_histogram_7 : entity work.tb_st_histogram generic map ( 5, 12,  512, 4000, "dc", "unsigned");
u_tb_st_histogram_8 : entity work.tb_st_histogram generic map ( 4, 13, 1024, 8000, "dc", "unsigned");
u_tb_st_histogram_9 : entity work.tb_st_histogram generic map (11,  6,   64,  100, "dc", "unsigned");

-- Locked Sine wave
u_tb_st_histogram_10: entity work.tb_st_histogram generic map ( 4,  3,    8,   20, "sine", "signed");
u_tb_st_histogram_11: entity work.tb_st_histogram generic map ( 8,  6,   64,  200, "sine", "signed");
u_tb_st_histogram_12: entity work.tb_st_histogram generic map (12,  8,  256, 2000, "sine", "signed");
u_tb_st_histogram_13: entity work.tb_st_histogram generic map (17, 10,  256, 3455, "sine", "signed");
u_tb_st_histogram_14: entity work.tb_st_histogram generic map (21, 14, 1024, 8111, "sine", "signed");

-- Drifting Sine wave
u_tb_st_histogram_15: entity work.tb_st_histogram generic map ( 4,  3,    8,   20, "sine", "signed", false);
u_tb_st_histogram_16: entity work.tb_st_histogram generic map ( 8,  6,   64,  200, "sine", "signed", false);
u_tb_st_histogram_17: entity work.tb_st_histogram generic map (12,  8,  256, 2000, "sine", "signed", false);
u_tb_st_histogram_18: entity work.tb_st_histogram generic map (17, 10,  256, 3455, "sine", "signed", false);
u_tb_st_histogram_19: entity work.tb_st_histogram generic map (21, 14, 1024, 8111, "sine", "signed", false);

-- Random
u_tb_st_histogram_20: entity work.tb_st_histogram generic map ( 4,  3,    8,   20, "random", "signed");
u_tb_st_histogram_21: entity work.tb_st_histogram generic map ( 6,  6,   64,  200, "random", "signed");
u_tb_st_histogram_22: entity work.tb_st_histogram generic map ( 9,  8,  256, 2000, "random", "signed");
u_tb_st_histogram_23: entity work.tb_st_histogram generic map (17, 10,  256, 3455, "random", "signed");
u_tb_st_histogram_24: entity work.tb_st_histogram generic map (13, 14, 1024, 8111, "random", "signed");
end tb;
