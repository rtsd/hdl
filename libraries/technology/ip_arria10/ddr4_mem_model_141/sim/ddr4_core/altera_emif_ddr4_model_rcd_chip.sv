// (C) 2001-2014 Altera Corporation. All rights reserved.
// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


///////////////////////////////////////////////////////////////////////////////
// Basic simulation model of DDR4 Registering Clock Driver used by RDIMM and LRDIMM
//
///////////////////////////////////////////////////////////////////////////////
module altera_emif_ddr4_model_rcd_chip # (
   parameter ADDRESS_MIRRORING  = 1
) (
   input             [1:0]   DCKE,
   input             [1:0]   DODT,
   input             [3:0]   DCS_n,
   input             [2:0]   DC,

   input            [17:0]   DA,
   input             [1:0]   DBA,
   input             [1:0]   DBG,
   input                     DACT_n,

   input                     CK_t,
   input                     CK_c,

   input                     DRST_n,

   input                     DPAR,

   input                     ERROR_IN_n,

   output                    BODT,
   output                    BCKE,
   output            [3:0]   BCOM,
   output                    BCK_t,
   output                    BCK_c,
   output                    BVrefCA,

   output   logic    [1:0]   QACKE,
   output   logic    [1:0]   QBCKE,
   output   logic    [1:0]   QAODT,
   output   logic    [1:0]   QBODT,
   output   logic    [3:0]   QACS_n,
   output   logic    [3:0]   QBCS_n,
   output   logic    [1:0]   QAC,
   output   logic    [1:0]   QBC,

   output   logic   [17:0]   QAA,
   output   logic   [17:0]   QBA,
   output   logic    [1:0]   QABA,
   output   logic    [1:0]   QABG,
   output   logic    [1:0]   QBBA,
   output   logic    [1:0]   QBBG,
   output   logic            QAACT_n,
   output   logic            QBACT_n,

   output            [3:0]   Y_c,
   output            [3:0]   Y_t,

   output   logic            QRST_n,

   output   logic            QAPAR,
   output   logic            QBPAR,

   output   logic            ALERT_n,

   inout                     SDA,
   input             [2:0]   SA,
   input                     SCL,
   input                     BFUNC,
   input                     VDDSPD,

   input                     VDD,
   input                     VSS,
   input                     AVDD,
   input                     PVDD,
   input                     PVSS
);

   timeunit 1ps;
   timeprecision 1ps;

   assign Y_t = {4{CK_t}};
   assign Y_c = {4{CK_c}};
   
   assign QRST_n = DRST_n;
   
   reg               [1:0]   DCKE_1; 
   reg               [1:0]   DCKE_2; 
   
   assign QACKE = DCKE_1 || DCKE_2;
   assign QBCKE = DCKE_1 || DCKE_2;

   assign ALERT_n = ERROR_IN_n;

   always @ (*)
   begin
      QAA     <= repeat(2) @(negedge CK_t) DA;
      QBA     <= repeat(2) @(negedge CK_t) ((ADDRESS_MIRRORING) ? ({~DA[17], DA[16:14], ~DA[11], DA[12], ~DA[13], DA[10], ~DA[9], ~DA[7], ~DA[8], ~DA[5], ~DA[6], ~DA[3], ~DA[4], DA[2:0]}) : ({~DA[17], DA[16:14], ~DA[13], DA[12], ~DA[11], DA[10], ~DA[9:3], DA[2:0]}));
      QABA    <= repeat(2) @(negedge CK_t) DBA;
      QBBA    <= repeat(2) @(negedge CK_t) ((ADDRESS_MIRRORING) ? ({~DBA[0], ~DBA[1]}) : (~DBA));
      QABG    <= repeat(2) @(negedge CK_t) DBG;
      QBBG    <= repeat(2) @(negedge CK_t) ((ADDRESS_MIRRORING) ? ({~DBG[0], ~DBG[1]}) : (~DBG));
      QAACT_n <= repeat(2) @(negedge CK_t) DACT_n;
      QBACT_n <= repeat(2) @(negedge CK_t) DACT_n;
      QACS_n  <= repeat(2) @(negedge CK_t) DCS_n;
      QBCS_n  <= repeat(2) @(negedge CK_t) DCS_n;
      QAC     <= repeat(2) @(negedge CK_t) DC;
      QBC     <= repeat(2) @(negedge CK_t) DC;
      QAODT   <= repeat(2) @(negedge CK_t) DODT;
      QBODT   <= repeat(2) @(negedge CK_t) DODT;
     
      DCKE_2  <= repeat(2) @(negedge CK_t) DCKE;
      DCKE_1  <= repeat(1) @(negedge CK_t) DCKE;

      QAPAR   <= repeat(1) @(negedge CK_t) DPAR;
      QBPAR   <= repeat(1) @(negedge CK_t) ~DPAR; 
   end

endmodule

