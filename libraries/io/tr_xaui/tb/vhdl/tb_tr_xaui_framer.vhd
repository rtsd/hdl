-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tr_xaui_framer is
end tb_tr_xaui_framer;

architecture tb of tb_tr_xaui_framer is
  constant c_clk_period   : time    := 10 ns;
  constant c_rl           : natural := 1;
  constant c_tx_init      : natural := 0;

  signal rst              : std_logic;
  signal clk              : std_logic := '1';

  signal tx_enable        : std_logic;

  signal tx_siso          : t_dp_siso;
  signal tx_sosi          : t_dp_sosi;

  signal gap_siso         : t_dp_siso;
  signal gap_sosi         : t_dp_sosi;

  signal xgmii_tx_d       : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal xgmii_tx_c       : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);
begin
  rst <= '1', '0' after c_clk_period * 7;
  clk <= not clk after c_clk_period / 2;

  p_stim : process
  begin
    tx_enable <= '0';

    wait for c_clk_period * 100;
    tx_enable <= '1';

    wait;
  end process;

   -- Generate tx_sosi for DUT using counter data generator
  proc_dp_gen_data(c_rl, c_xgmii_data_w, c_tx_init, rst, clk, tx_enable, tx_siso, tx_sosi);

  -- Introduce some gaps in the streaming data to make sure framer handles them correctly.
  u_dp_gap : entity dp_lib.dp_gap
  generic map (
    g_dat_len    => 40,
    g_gap_len    => 20
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => tx_siso,
    snk_in    => tx_sosi,

    src_in    => gap_siso,
    src_out   => gap_sosi
  );

  dut : entity work.tr_xaui_framer
  generic map (
    g_dat_len => 100,
    g_gap_len => 5
   )
  port map (
    tx_rst     => rst,
    tx_clk     => clk,

    snk_out    => gap_siso,
    snk_in     => gap_sosi,

    xgmii_tx_d => xgmii_tx_d,
    xgmii_tx_c => xgmii_tx_c

  );
end tb;
