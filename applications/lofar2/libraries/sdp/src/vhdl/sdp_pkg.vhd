-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle, E. Kooistra
-- Purpose:
-- . This package contains sdp specific constants.
-- Description: See [1]
-- References:
-- . [1] https://support.astron.nl/confluence/display/L2M/L3+SDP+Decision%3A+SDP+Parameter+definitions
-------------------------------------------------------------------------------
library IEEE, common_lib, rTwoSDF_lib, fft_lib, filter_lib, wpfb_lib, diag_lib, tech_jesd204b_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.math_real.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_field_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use diag_lib.diag_pkg.all;
  use rTwoSDF_lib.rTwoSDFPkg.all;
  use fft_lib.fft_pkg.all;
  use filter_lib.fil_pkg.all;
  use wpfb_lib.wpfb_pkg.all;
  use tech_jesd204b_lib.tech_jesd204b_pkg.all;

package sdp_pkg is
  -------------------------------------------------
  -- SDP info record as defined in:
  --  LOFAR2-9258-SDP info per antenna band
  -------------------------------------------------
  type t_sdp_info is record
    antenna_field_index     : std_logic_vector(5 downto 0);  -- = station_info[15:10]
    station_id              : std_logic_vector(9 downto 0);  -- = station_info[9:0]
    antenna_band_index      : std_logic;
    observation_id          : std_logic_vector(31 downto 0);
    nyquist_zone_index      : std_logic_vector(1 downto 0);
    f_adc                   : std_logic;
    fsub_type               : std_logic;
    beam_repositioning_flag : std_logic;
    block_period            : std_logic_vector(15 downto 0);
  end record;

  constant c_sdp_info_rst : t_sdp_info :=
      ( (others => '0'), (others => '0'), '0', (others => '0'), (others => '0'),
        '0', '0', '0',
        (others => '0') );

  -------------------------------------------------
  -- SDP specific parameters as defined in [1]
  constant c_sdp_f_adc_MHz                 : natural := 200;
  constant c_sdp_N_band                    : natural := 2;  -- 2 antenna bands, LB and HB
  constant c_sdp_N_beamsets                : natural := 2;  -- = N_beamsets_sdp in doc
  constant c_sdp_N_cross_sets_sdp          : natural := 1;
  constant c_sdp_N_crosslets_max           : natural := 7;
  constant c_sdp_N_fft                     : natural := 1024;
  constant c_sdp_N_pn_max                  : natural := 16;  -- max 16 PN per ring = per antenna band
  constant c_sdp_N_pol                     : natural := 2;
  constant c_sdp_N_pol_bf                  : natural := 2;
  constant c_sdp_N_rings_sdp               : natural := 1;  -- number of QSFP rings in SDP, each has N_lane = 8 lanes
  constant c_sdp_N_ring_lanes_max          : natural := 8;  -- = N_lane in doc
  constant c_sdp_N_sub                     : natural := 512;
  constant c_sdp_N_sync_rcu                : natural := 1;
  constant c_sdp_N_taps                    : natural := 16;
  constant c_sdp_P_sq                      : natural := 9;  -- = N_pn / 2 + 1 square correlator cells for XST
  constant c_sdp_P_sum                     : natural := 2;  -- sums of two in ring beamformer adder tree
  constant c_sdp_Q_fft                     : natural := 2;
  constant c_sdp_S_pn                      : natural := 12;
  constant c_sdp_S_rcu                     : natural := 3;
  constant c_sdp_S_sub_bf                  : natural := 488;
  constant c_sdp_R_os                      : natural := 2;  -- Oversampling factor of 2 for PFB.
  constant c_sdp_V_ring_pkt_len_max        : natural := 48;  -- for 16 nodes
  constant c_sdp_V_sample_delay            : natural := 1024;  -- 1024 uses 12 M20K, 2048 24 M20K and 4096 36 M20K
  constant c_sdp_V_si_db                   : natural := 1024;
  constant c_sdp_V_si_db_large             : natural := 131072;
  constant c_sdp_V_si_histogram            : natural := 512;
  constant c_sdp_W_adc                     : natural := 14;
  constant c_sdp_W_adc_jesd                : natural := 16;
  constant c_sdp_W_fir_coef                : natural := 16;
  constant c_sdp_W_subband                 : natural := 18;
  constant c_sdp_W_crosslet                : natural := 16;
  constant c_sdp_W_beamlet_sum             : natural := 18;
  constant c_sdp_W_beamlet                 : natural := 8;
  constant c_sdp_W_gn_id                   : natural := 8;  -- = UniBoard2 ID[7:0]
  constant c_sdp_W_statistic               : natural := 64;
  constant c_sdp_W_statistic_sz            : natural := 2;  -- = c_sdp_W_statistic / c_word_w
  constant c_sdp_W_sub_weight              : natural := 16;  -- = w in s(w, p), s = signed
  constant c_sdp_W_sub_weight_fraction     : natural := 14;  -- = p in s(w, p)
  constant c_sdp_W_sub_weight_magnitude    : natural := c_sdp_W_sub_weight - c_sdp_W_sub_weight_fraction - 1;  -- = 1
  constant c_sdp_W_beamlet_scale           : natural := 16;  -- = w in u(w, p), u = unsigned
  constant c_sdp_W_beamlet_scale_fraction  : natural := 15;  -- = p in u(w, p)
  constant c_sdp_W_beamlet_scale_magnitude : natural := c_sdp_W_beamlet_scale - c_sdp_W_beamlet_scale_fraction;  -- = 1
  constant c_sdp_W_bf_weight               : natural := 16;  -- = w in s(w, p), s = signed
  constant c_sdp_W_bf_weight_fraction      : natural := 14;  -- = p in s(w, p)
  constant c_sdp_W_bf_weight_magnitude     : natural := c_sdp_W_bf_weight - c_sdp_W_bf_weight_fraction - 1;  -- = 1
  constant c_sdp_W_local_oscillator           : natural := 16;  -- = w in s(w, p), s = signed
  constant c_sdp_W_local_oscillator_fraction  : natural := 15;  -- = p in s(w, p)
  constant c_sdp_W_local_oscillator_magnitude : natural := c_sdp_W_local_oscillator -
                                                           c_sdp_W_local_oscillator_fraction - 1;  -- = 0
  constant c_sdp_N_ring_nof_mac10g            : natural := 3;  -- for sdp_station_xsub_ring design.

  -- Derived constants
  constant c_sdp_FS_adc                  : natural := 2**(c_sdp_W_adc - 1);  -- full scale FS corresponds to amplitude
                                                                             -- 1.0, will just cause clipping of +FS
                                                                             -- to +FS-1
  constant c_sdp_wg_ampl_lsb             : real := c_diag_wg_ampl_unit / real(c_sdp_FS_adc);  -- WG amplitude in number
                                                                             -- of LSbit resolution steps
  constant c_sdp_wg_subband_freq_unit    : real := c_diag_wg_freq_unit / real(c_sdp_N_fft);  -- subband freq = Fs/1024
                                                                             -- = 200 MSps/1024 = 195312.5 Hz sinus
  constant c_sdp_N_clk_per_second        : natural := c_sdp_f_adc_MHz * 10**6;  -- Default 200M clock cycles per second
  constant c_sdp_N_clk_per_sync          : natural := c_sdp_N_clk_per_second;  -- Default 200M clock cycles per sync
                                                                               -- interval of 1 second
  constant c_sdp_N_clk_per_sync_max      : natural := c_sdp_N_clk_per_second * 10;  -- 10 seconds
  constant c_sdp_N_clk_per_sync_min      : natural := c_sdp_N_clk_per_second / 10;  -- 0.1 second
  constant c_sdp_N_clk_sync_timeout      : natural := c_sdp_N_clk_per_second + c_sdp_N_clk_per_second / 10;  -- 10%
                                                                                                             -- margin.
  constant c_sdp_N_clk_sync_timeout_xsub : natural := 2147483647;  -- = 2**31 - 1 = largest value for NATURAL for 10.7
                                                                   -- seconds. Do not use 2*31 to avoid Modelsim
                                                                   -- natural overflow warning.
  constant c_sdp_N_sync_jesd             : natural := c_sdp_S_pn * c_sdp_N_sync_rcu / c_sdp_S_rcu;  -- = 4, nof JESD IP
                                                                   -- sync outputs per PN
  constant c_sdp_f_sub_Hz                : real := real(c_sdp_N_clk_per_second) / real(c_sdp_N_fft);  -- = 195312.5
  constant c_sdp_N_int                   : natural := c_sdp_N_clk_per_second;  -- nof ADC sample periods per 1 s
                                                                               -- integration interval
  constant c_sdp_N_int_sub               : real := c_sdp_f_sub_Hz;  -- nof subband sample periods per 1 s integration
                                                                    -- interval
  constant c_sdp_N_int_sub_lo            : natural := natural(FLOOR(c_sdp_N_int_sub));  -- = 195312
  constant c_sdp_N_int_sub_hi            : natural := natural(CEIL(c_sdp_N_int_sub));  -- = 195313
  constant c_sdp_A_pn                    : natural := c_sdp_S_pn / c_sdp_N_pol;  -- = 6 dual pol antenna per PN, is 6
                                                                                 -- signal input pairs
  constant c_sdp_P_pfb                   : natural := c_sdp_S_pn / c_sdp_Q_fft;  -- = 6 PFB units, for 6 signal input
                                                                                 -- pairs
  constant c_sdp_T_adc                   : time    := (10**6 / c_sdp_f_adc_MHz) * 1 ps;  -- = 5 ns @ 200MHz
  constant c_sdp_T_sub                   : time    := c_sdp_N_fft * c_sdp_T_adc;  -- = 5.12 us @ 200MHz
  constant c_sdp_X_sq                    : natural := c_sdp_S_pn * c_sdp_S_pn;  -- = 144
  constant c_sdp_block_period            : natural := c_sdp_N_fft * 1000 / c_sdp_f_adc_MHz;  -- = 5120 [ns]
  constant c_sdp_N_beamlets_sdp          : natural := c_sdp_N_beamsets * c_sdp_S_sub_bf;  -- = 976
  constant c_sdp_W_dual_pol_beamlet      : natural := c_sdp_N_pol_bf * c_nof_complex * c_sdp_W_beamlet;  -- 2 * 2 * 8
                                                                                                         -- = 32b

  constant c_sdp_nof_words_per_beamlet     : natural := 1;  -- 1 dual pol, complex, 8bit beamlet (Xre, Xim, Yre, Yim)
                                                            -- per 32b word
  constant c_sdp_nof_beamlets_per_longword : natural := 2;  -- 2 dual pol, complex, 8bit beamlets fit in 1 64bit
                                                            -- longword
  constant c_sdp_nof_beamlets_per_block    : natural := c_sdp_S_sub_bf;  -- number of dual pol beamlets per block
  constant c_sdp_nof_beamlets_per_block_w  : natural := ceil_log2(c_sdp_nof_beamlets_per_block + 1);

  -- . unit weights
  constant c_sdp_unit_sub_weight      : natural := 2**c_sdp_W_sub_weight_fraction;  -- 2**13, so range +-4.0 for 16 bit
                                                                                    -- signed weight
  constant c_sdp_unit_bf_weight       : natural := 2**c_sdp_W_bf_weight_fraction;  -- 2**14, so range +-2.0 for 16 bit
                                                                                   -- signed weight
  constant c_sdp_unit_beamlet_scale   : natural := 2**c_sdp_W_beamlet_scale_fraction;  -- 2**15, so range +-1.0 for 16
                                                                                       -- bit signed weight

  -- One dual polarization beamlet fits in a 32b word:
  -- [0:3] = [Xre, Xim, Yre, Yim] parts of c_sdp_W_beamlet = 8 bit, so
  -- c_sdp_N_pol_bf * c_nof_complex * c_sdp_W_beamlet = 2 * 2 = 4 octets
  subtype t_sdp_dual_pol_beamlet_in_word is t_slv_8_arr(0 to 3);

  -- Two dual polarization beamlets fit in a 64b longword:
  -- [0:7] = [0:3,4:7] = [Xre, Xim, Yre, Yim,  Xre, Xim, Yre, Yim], so
  -- c_sdp_nof_beamlets_per_longword * c_sdp_N_pol_bf * c_nof_complex = 2 * 2 * 2 = 8 octets
  subtype t_sdp_dual_pol_beamlet_in_longword is t_slv_8_arr(0 to 7);

  -----------------------------------------------------------------------------
  -- Signal input delay buffer (dp_shiftram)
  -----------------------------------------------------------------------------
  constant c_sdp_input_mux_latency : natural := 1;
  constant c_sdp_shiftram_latency  : natural := c_shiftram_latency;  -- + c_sdp_input_mux_latency;

  -----------------------------------------------------------------------------
  -- PFB
  -----------------------------------------------------------------------------

  -- In SDP c_nof_channels = 2**nof_chan = 1 and wb_factor = 1,
  -- therefore these parameters are not explicitly used in calculation of derived constants
  -- LTS 2020_11_23:
  --CONSTANT c_sdp_wpfb_subbands : t_wpfb :=
  -- (1, c_sdp_N_fft, 0, c_sdp_P_pfb,
  -- c_sdp_N_taps, 1, c_sdp_W_adc, 16, c_sdp_W_fir_coef,
  -- true, false, true, 16, c_sdp_W_subband, 1, 18, 2, true, 54, 2, 195313,
  -- c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);

  -- LTS 2021-02-03, changes based on results from u_wpfb_stage22 in tb_tb_verify_pfb_wg.vhd:
  -- . fil_backoff_w = 0 (was 1)
  -- . fil_out_dat_w = fft_in_dat_w = 17 (was 16)
  -- . g_fft_out_gain_w = 0 (was 1)
  -- . g_fft_stage_dat_w = 22 (was 18)
  -- . g_fft_guard_w = 1 (was 2)
  --CONSTANT c_sdp_wpfb_subbands : t_wpfb :=
  --  (1, c_sdp_N_fft, 0, c_sdp_P_pfb,
  --   c_sdp_N_taps, 0, c_sdp_W_adc, 17, c_sdp_W_fir_coef,
  --   true, false, true,
  --   17, c_sdp_W_subband, 0, 22, 1, true,
  --   54, c_sdp_W_statistic_sz, 195313,
  --   c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);  -- = c_wpfb_lofar2_subbands_lts_2021

  -- DTS 2022-04-04, changes based on results from  in tb_tb_verify_pfb_wg.vhd:
  -- . fil_backoff_w = 1
  -- . fil_out_dat_w = fft_in_dat_w = 0 (use g_fft_stage_dat_w - g_fft_guard_w)
  -- . g_fft_out_gain_w = 1 (compensate for fil_backoff_w = 1)
  -- . g_fft_stage_dat_w = 24
  -- . g_fft_guard_w = 1
  --CONSTANT c_sdp_wpfb_subbands : t_wpfb :=
  --  (1, c_sdp_N_fft, 0, c_sdp_P_pfb,
  --   c_sdp_N_taps, 1, c_sdp_W_adc, 23, c_sdp_W_fir_coef,
  --   true, false, true,
  --   23, c_sdp_W_subband, 1, 24, 1, true,
  --   54, c_sdp_W_statistic_sz, 195313,
  --   c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);  -- = c_wpfb_lofar2_subbands_dts_18b

  -- L2TS
  -- . Use fft_guard_w = 1, instead of 2 to avoid overflow in first FFT stage,
  --   because fil_backoff_w = 1 already provides sufficient FFT input margin
  -- . Use fft_out_gain_w = 1 + 1 = 2. One to compensate for fil_backoff_w
  --   = 1 and one to preserve the W_fft_proc = 5b while using fft_out_dat_w
  --   = c_sdp_W_subband = 18b instead of 19b, to fit a 18x19 multiplier for
  --   SST.
  -- . From hdl/libraries/base/common/python/try_round_weight.py it follows
  --   that using -r = 6 extra internal bits per stage is sufficient to have
  --   < 1% disturbance on the sigma of the subband noise. The disturbance
  --   on the sigma is about proportional to 1/2**r, so with -r = 4 it is
  --   about < 4%. Therefore use fft_stage_dat_w = fft_out_dat_w +
  --   fft_out_gain_w + 6b = 26b.
  -- . The raw_dat_w for FFT output of real input is fft_stage_dat_w + 1,
  --   because the use_separate in the FFT feature does not divide by 2.
  --   This implies that preferrably fft_stage_dat_w <= 26, to fit the 27b
  --   multiplier resources.
  -- . Resource usage for fft_stage_dat_w = 24b, 25b, 26b:
  --               24b           25b           26b
  --     FFT       6 x  27 M20K, 6 x  27 M20K, 6 x  28 M20K, due to separate
  --     BF        2 x 397 M20K, 2 x 403 M20K, 2 x 403 M20K, due to reorder_col
  --     u_revison    1738 M20K,    1750 M20K,    1756 M20K,
  --                    611 DSP,      611 DSP,      611 DSP, same for all
  --                  332324 FF,    335097 FF,    336262 FF,
  --     where 6 = c_sdp_P_pfb and 2 = c_sdp_N_beamsets.
  --   The total increase for u_revison is:
  --   . for 25b / 24b : 12 m20K = 0.7 % and ~2800 FF = 0.83 %.
  --   . for 26b / 24b : 18 m20K = 1.0 % and ~4000 FF = 1.2 %.
  constant c_sdp_W_fil_backoff             : natural := 1;
  constant c_sdp_W_fft_guard               : natural := 1;
  constant c_sdp_W_fft_stage_dat           : natural := 25;
  constant c_sdp_W_fft_in_dat              : natural := c_sdp_W_fft_stage_dat - c_sdp_W_fft_guard;
  constant c_sdp_W_fft_out_gain            : natural := 2;
  constant c_sdp_W_stat_data               : natural := c_sdp_W_subband * 2 + ceil_log2(c_sdp_N_int_sub_hi);  -- = 54

  constant c_sdp_wpfb_subbands : t_wpfb :=
    (1, c_sdp_N_fft, 0, c_sdp_P_pfb,
     c_sdp_N_taps, c_sdp_W_fil_backoff, c_sdp_W_adc, c_sdp_W_fft_in_dat, c_sdp_W_fir_coef,
     true, false, true,
     c_sdp_W_fft_in_dat, c_sdp_W_subband, c_sdp_W_fft_out_gain, c_sdp_W_fft_stage_dat, c_sdp_W_fft_guard, true,
     c_sdp_W_stat_data, c_sdp_W_statistic_sz, c_sdp_N_int_sub_hi,
     c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);  -- = c_wpfb_lofar2_subbands_l2ts_18b

  constant c_sdp_wpfb_complex_subbands : t_wpfb :=
                                         func_wpfb_map_real_input_wpfb_parameters_to_complex_input(c_sdp_wpfb_subbands);

  -- DC gain of WPFB FIR filter obtained from applications/lofar2/model/run_pfir_coef.m using application =
  -- 'lofar_subband'. Not used in RTL, only used in test benches to verify expected suband levels
  constant c_sdp_wpfb_fir_filter_dc_gain    : real := c_fil_lofar1_fir_filter_dc_gain;  -- = 0.994817, almost unit DC
                                                                                        -- gain
  constant c_sdp_wpfb_subband_sp_ampl_ratio : real :=
                                             func_wpfb_subband_gain(c_sdp_wpfb_subbands, c_sdp_wpfb_fir_filter_dc_gain);

  -----------------------------------------------------------------------------
  -- Subband Equalizer
  -----------------------------------------------------------------------------
  constant c_sdp_subband_equalizer_latency : natural := 11;  -- 11 = 3 reverse + 1 sum + 5 weight + 2 requant

  -----------------------------------------------------------------------------
  -- Statistics offload
  -----------------------------------------------------------------------------

  -- The statistics offload uses the same 1GbE port as the NiosII for M&C. The 1GbE addresses defined in SW and here
  -- in FW.
  -- See NiosII code:
  --   https://git.astron.nl/rtsd/hdl/-/blob/master/libraries/unb_osy/unbos_eth.h
  --   https://git.astron.nl/rtsd/hdl/-/blob/master/libraries/unb_osy/unbos_eth.c
  -- and g_base_ip = x"0A63" in:
  --   https://git.astron.nl/rtsd/hdl/-/blob/master/boards/uniboard2b/libraries/unb2b_board/src/vhdl/ctrl_unb2b_board.vhd

  -- Can use same offload time for all statistics, because 1GbE mux will combine them
  -- see https://support.astron.nl/confluence/display/L2M/L3+SDP+Testing+Notebook%3A+Statistics+offload
  -- Intra node time of 600000 * 5 ns = 3 ms, so gn 31 starts after 31 * 3 = 93 ms
  constant c_sdp_offload_node_time    : natural := 600000;
  -- Inter packet time of 3100 * 5 ns = 15.5 us, so maximum 9 * 7 = 63 XST packets will yield total gap time of
  -- about 63 * 15.5 us = 0.98 ms. The 63 XST packets take about 63 * 2344 octets * 8 bits / 1GbE = 1.2 ms.
  -- Hence in total the XST offload takes about 0.98 + 1.2 = 2.2 ms, which fits in the budgetted 3 ms per node.
  constant c_sdp_offload_packet_time  : natural := 3100;
  -- Scale factor to implement offload time dependent on ctrl_interval_size, see sdp_statistics_offload.vhd
  -- for more description.
  constant c_sdp_offload_scale_w : natural := 15;

  -- packet lengths, see ICD SC-SDP
  constant c_sdp_nof_bytes_per_statistic : natural := 8;  -- c_sdp_W_statistic_sz * c_word_sz = 2 * 4 = 8

  constant c_sdp_stat_app_header_len    : natural := 32;

  -- MAC : 001B217176B9 = DOP36-enp2s0
  -- MAC_47_16 : 00:22:86:08:pp:qq = UNB_ETH_SRC_MAC_BASE in libraries/unb_osy/unbos_eth.h
  -- IP : 0A6300FE = '10.99.0.254' = DOP36-enp2s0
  -- IP_31_16: 10.99.xx.yy = g_base_ip in ctrl_unb2#_board.vhd used in libraries/unb_osy/unbos_eth.c
  -- UDP_SRC_PORT[15:8] = gn_id (= ID[7:0] = backplane[5:0] & node[1:0])
  constant c_sdp_stat_eth_dst_mac       : std_logic_vector(47 downto 0) := x"001B217176B9";
  constant c_sdp_stat_eth_src_mac_47_16 : std_logic_vector(31 downto 0) := x"00228608";
  constant c_sdp_stat_ip_dst_addr       : std_logic_vector(31 downto 0) := x"0A6300FE";
  constant c_sdp_stat_ip_src_addr_31_16 : std_logic_vector(15 downto 0) := x"0A63";
  constant c_sdp_stat_udp_dst_port      : std_logic_vector(15 downto 0) := TO_UVEC(5001, 16);  -- 0x1389 = 5001
  constant c_sdp_sst_udp_src_port_15_8  : std_logic_vector( 7 downto 0) := x"D0";  -- TBC
  constant c_sdp_bst_udp_src_port_15_8  : std_logic_vector( 7 downto 0) := x"D1";  -- TBC
  constant c_sdp_xst_udp_src_port_15_8  : std_logic_vector( 7 downto 0) := x"D2";  -- TBC

  constant c_sdp_stat_version_id        : natural := 5;
  constant c_sdp_stat_nof_hdr_fields    : natural := 1 + 3 + 12 + 4 + 4 + 8 + 7 + 1;  -- 608b; 19 32b words

  -- hdr_field_sel bit selects where the hdr_field value is set:
  -- . 0 = data path controlled, value is set in sdp_statistics_offload.vhd, so field_default() is not used.
  -- . 1 = MM controlled, value is set via MM or by the field_default(), so any data path setting in
  --       sdp_statistics_offload.vhd is not used.
  -- Remarks:
  -- . For constant values it is convenient to use MM controlled, because then the field_default()
  --   is used that can be set here in c_sdp_stat_hdr_field_arr.
  -- . For reserved values it is convenient to use MM controlled, because then in future they
  --   could still be changed via MM without having to recompile the FW.
  -- . Typically only use data path controlled if the value has to be set dynamically, so dependent
  --   on the state of the FW.
  -- . If a data path controlled field is not set in the FW, then it defaults to 0 by declaring
  --   hdr_fields_in_arr with all 0. Hence e.g. udp_checksum = 0 can be achieve via data path
  --
  --   and default hdr_fields_in_arr = 0 or via MM controlled and field_default(0).
  --         eth     ip               udp      app
  constant c_sdp_stat_hdr_field_sel     : std_logic_vector(c_sdp_stat_nof_hdr_fields - 1 downto 0) :=
      "1" & "101" & "111011111001" & "0100" & "0100" & "00000000" & "1000000" & "0";  -- current
--constant c_sdp_stat_hdr_field_sel     : STD_LOGIC_VECTOR(c_sdp_stat_nof_hdr_fields-1 DOWNTO 0) :=
--    "1" & "101" & "111011111001" & "0101" & "0100" & "00000000" & "0000100" & "0";  -- previous 26 nov 2021
--constant c_sdp_stat_hdr_field_sel     : STD_LOGIC_VECTOR(c_sdp_stat_nof_hdr_fields-1 DOWNTO 0) :=
--    "0" & "100" & "000000010001" & "0100" & "0100" & "00000000" & "1000000" & "0";  -- initial

  -- Default use destination MAC/IP/UDP = 0, so these have to be MM programmed before
  -- statistics offload packets can be send.
  constant c_sdp_stat_hdr_field_arr : t_common_field_arr(c_sdp_stat_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("word_align" ), "RW", 16, field_default(0) ),  -- Tx TSE IP will strip these 2 padding bytes
      ( field_name_pad("eth_dst_mac"), "RW", 48, field_default(0) ),  -- c_sdp_stat_eth_dst_mac
      ( field_name_pad("eth_src_mac"), "RW", 48, field_default(0) ),
      ( field_name_pad("eth_type"   ), "RW", 16, field_default(x"0800") ),

      ( field_name_pad("ip_version"        ), "RW",  4, field_default(4) ),
      ( field_name_pad("ip_header_length"  ), "RW",  4, field_default(5) ),
      ( field_name_pad("ip_services"       ), "RW",  8, field_default(0) ),
      ( field_name_pad("ip_total_length"   ), "RW", 16, field_default(0) ),  -- differs for SST, BST, XST so set by
                                                                             -- data path
      ( field_name_pad("ip_identification" ), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_flags"          ), "RW",  3, field_default(2) ),
      ( field_name_pad("ip_fragment_offset"), "RW", 13, field_default(0) ),
      ( field_name_pad("ip_time_to_live"   ), "RW",  8, field_default(127) ),
      ( field_name_pad("ip_protocol"       ), "RW",  8, field_default(17) ),
      ( field_name_pad("ip_header_checksum"), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_src_addr"       ), "RW", 32, field_default(0) ),
      ( field_name_pad("ip_dst_addr"       ), "RW", 32, field_default(0) ),  -- c_sdp_stat_ip_dst_addr

      ( field_name_pad("udp_src_port"    ), "RW", 16, field_default(0) ),
      ( field_name_pad("udp_dst_port"    ), "RW", 16, field_default(0) ),  -- c_sdp_stat_udp_dst_port
      ( field_name_pad("udp_total_length"), "RW", 16, field_default(0) ),  -- differs for SST, BST, XST so set by
                                                                           -- data path
      ( field_name_pad("udp_checksum"    ), "RW", 16, field_default(0) ),

      ( field_name_pad("sdp_marker"        ), "RW",  8, field_default(0) ),  -- differs for SST, BST, XST so set by
                                                                             -- data path
      ( field_name_pad("sdp_version_id"    ), "RW",  8, field_default(c_sdp_stat_version_id) ),
      ( field_name_pad("sdp_observation_id"), "RW", 32, field_default(0) ),
      ( field_name_pad("sdp_station_info"  ), "RW", 16, field_default(0) ),

      ( field_name_pad("sdp_source_info_antenna_band_id"        ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_nyquist_zone_id"        ), "RW",  2, field_default(0) ),
      ( field_name_pad("sdp_source_info_f_adc"                  ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_fsub_type"              ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_payload_error"          ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_beam_repositioning_flag"), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_weighted_subbands_flag" ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_gn_id"                  ), "RW",  8, field_default(0) ),

      ( field_name_pad("sdp_reserved"                 ), "RW",  8, field_default(0) ),
      ( field_name_pad("sdp_integration_interval"     ), "RW", 24, field_default(0) ),
      ( field_name_pad("sdp_data_id"                  ), "RW", 32, field_default(0) ),
      ( field_name_pad("sdp_nof_signal_inputs"        ), "RW",  8, field_default(0) ),
      ( field_name_pad("sdp_nof_bytes_per_statistic"  ), "RW",  8, field_default(c_sdp_nof_bytes_per_statistic) ),
      ( field_name_pad("sdp_nof_statistics_per_packet"), "RW", 16, field_default(0) ),
      ( field_name_pad("sdp_block_period"             ), "RW", 16, field_default(c_sdp_block_period) ),

      ( field_name_pad("dp_bsn"), "RW", 64, field_default(0) )
  );
  constant c_sdp_reg_stat_hdr_dat_addr_w : natural := ceil_log2(field_nof_words(c_sdp_stat_hdr_field_arr, c_word_w));

  type t_sdp_network_stat_header is record
    sdp_marker                              : std_logic_vector( 7 downto 0);
    sdp_version_id                          : std_logic_vector( 7 downto 0);
    sdp_observation_id                      : std_logic_vector(31 downto 0);
    sdp_station_info                        : std_logic_vector(15 downto 0);

    sdp_source_info_antenna_band_id         : std_logic_vector( 0 downto 0);
    sdp_source_info_nyquist_zone_id         : std_logic_vector( 1 downto 0);
    sdp_source_info_f_adc                   : std_logic_vector( 0 downto 0);
    sdp_source_info_fsub_type               : std_logic_vector( 0 downto 0);
    sdp_source_info_payload_error           : std_logic_vector( 0 downto 0);
    sdp_source_info_beam_repositioning_flag : std_logic_vector( 0 downto 0);
    sdp_source_info_weighted_subbands_flag  : std_logic_vector( 0 downto 0);
    sdp_source_info_gn_id                   : std_logic_vector( 7 downto 0);

    sdp_reserved                            : std_logic_vector( 7 downto 0);
    sdp_integration_interval                : std_logic_vector(23 downto 0);
    sdp_data_id                             : std_logic_vector(31 downto 0);
    sdp_data_id_sst_signal_input_index      : std_logic_vector( 7 downto  0);  -- sdp_data_id sub field
    sdp_data_id_bst_beamlet_index           : std_logic_vector(15 downto  0);  -- sdp_data_id sub field
    sdp_data_id_xst_subband_index           : std_logic_vector(24 downto 16);  -- sdp_data_id sub field
    sdp_data_id_xst_signal_input_A_index    : std_logic_vector(15 downto  8);  -- sdp_data_id sub field
    sdp_data_id_xst_signal_input_B_index    : std_logic_vector( 7 downto  0);  -- sdp_data_id sub field
    sdp_nof_signal_inputs                   : std_logic_vector( 7 downto 0);
    sdp_nof_bytes_per_statistic             : std_logic_vector( 7 downto 0);
    sdp_nof_statistics_per_packet           : std_logic_vector(15 downto 0);
    sdp_block_period                        : std_logic_vector(15 downto 0);

    dp_bsn                                  : std_logic_vector(63 downto 0);
  end record;

  type t_sdp_stat_data_id is record
    sst_signal_input_index      : natural range 0 to 2**8 - 1;  -- < 192 = c_sdp_N_pn_max * c_sdp_S_pn
    bst_beamlet_index           : natural range 0 to 2**16 - 1;  -- < 976 = c_sdp_N_beamsets * c_sdp_S_sub_bf
    xst_subband_index           : natural range 0 to 2**9 - 1;  -- < 512 = c_sdp_N_sub
    xst_signal_input_A_index    : natural range 0 to 2**8 - 1;  -- < 192 = c_sdp_N_pn_max * c_sdp_S_pn
    xst_signal_input_B_index    : natural range 0 to 2**8 - 1;  -- < 192 = c_sdp_N_pn_max * c_sdp_S_pn
  end record;

  type t_sdp_stat_header is record
    eth : t_network_eth_header;
    ip  : t_network_ip_header;
    udp : t_network_udp_header;
    app : t_sdp_network_stat_header;
  end record;

  -----------------------------------------------------------------------------
  -- Beamlet data output (BDO) via 10GbE to CEP (= central processor, see ICD STAT-CEP)
  -----------------------------------------------------------------------------
  constant c_sdp_cep_version_id        : natural := 5;
  constant c_sdp_marker_beamlets       : natural := 98;  -- = x"62" = 'b'

  constant c_sdp_cep_eth_dst_mac       : std_logic_vector(47 downto 0) := x"00074306C700";  -- 00074306C700 = DOP36-eth0
  constant c_sdp_cep_eth_src_mac_47_16 : std_logic_vector(31 downto 0) := x"00228608";  -- 47:16, 15:8 = backplane,
                                                                                        -- 7:0 = node
  constant c_sdp_cep_ip_dst_addr       : std_logic_vector(31 downto 0) := x"C0A80001";  -- C0A80001 = '192.168.0.1' =
                                                                                        -- DOP36-eth0
  constant c_sdp_cep_ip_src_addr_31_16 : std_logic_vector(15 downto 0) := x"C0A8";  -- 31:16, 15:8 = backplane, 7:0 =
                                                                                    -- node + 1 = 192.168.xx.yy
  constant c_sdp_cep_ip_total_length   : std_logic_vector(15 downto 0) := TO_UVEC(7868, 16);  -- see ICD STAT-CEP
  constant c_sdp_cep_udp_total_length  : std_logic_vector(15 downto 0) := TO_UVEC(7848, 16);  -- see ICD STAT-CEP
  constant c_sdp_cep_udp_dst_port      : std_logic_vector(15 downto 0) := TO_UVEC(5000, 16);  -- 0x1388 = 5000
  constant c_sdp_cep_udp_src_port_15_8 : std_logic_vector( 7 downto 0) := x"D0";  -- 15:8, 7:0 = gn_id (= ID[7:0] =
                                                                                  -- backplane[5:0] & node[1:0])

  constant c_sdp_cep_app_header_len    : natural := 32;  -- octets, see ICD STAT-CEP
  constant c_sdp_cep_header_len        : natural := 14 + 20 + 8 + c_sdp_cep_app_header_len;  -- = eth + ip + udp + app
                                                                                       -- = 74 octets, see ICD STAT-CEP

  constant c_sdp_cep_nof_blocks_per_packet   : natural := 4;  -- number of time blocks of beamlets per output packet
  constant c_sdp_cep_nof_beamlets_per_block  : natural := c_sdp_nof_beamlets_per_block;  -- number of dual pol
                                                                                       -- beamlets (c_sdp_N_pol_bf = 2)
  constant c_sdp_cep_nof_beamlets_per_packet : natural := c_sdp_cep_nof_blocks_per_packet *
                                                          c_sdp_cep_nof_beamlets_per_block;
  constant c_sdp_cep_payload_nof_longwords   : natural := c_sdp_cep_nof_beamlets_per_packet /
                                                          c_sdp_nof_beamlets_per_longword;  -- = 976
  constant c_sdp_cep_packet_nof_longwords    : natural := ceil_div(c_sdp_cep_header_len, c_longword_sz) +
                                                          c_sdp_cep_payload_nof_longwords;  -- without tail CRC, the
                                                                                         -- CRC is applied by 10GbE MAC

  -- CEP packet header
  constant c_sdp_cep_nof_hdr_fields : natural := 3 + 12 + 4 + 4 + 9 + 6 + 1;  -- = 39 fields
  -- c_sdp_cep_header_len / c_longword_sz = 74 / 8 = 9.25 64b words = 592b
  -- hdr_field_sel bit selects where the hdr_field value is set:
  -- . 0 = data path controlled, value is set in sdp_beamformer_output.vhd, so field_default() is not used.
  -- . 1 = MM controlled, value is set via MM or by the field_default(), so any data path setting in
  --       sdp_beamformer_output.vhd is not used.
  -- Remarks: see remarks at c_sdp_stat_nof_hdr_fields.
  --   eth     ip               udp      app
  constant c_sdp_cep_hdr_field_sel  : std_logic_vector(c_sdp_cep_nof_hdr_fields - 1 downto 0) :=
      "111" & "111111111011" & "1110" & "1100" & "100000010" & "100000" & "0";  -- current
--constant c_sdp_cep_hdr_field_sel  : std_logic_vector(c_sdp_cep_nof_hdr_fields - 1 downto 0) :=
--    "111" & "111111111011" & "1110" & "1100" & "100000010" & "100110" & "0";  -- 18 sep 2023
--constant c_sdp_cep_hdr_field_sel  : STD_LOGIC_VECTOR(c_sdp_cep_nof_hdr_fields-1 downto 0) :=
--    "101" & "111111111001" & "0111" & "1100" & "100000010" & "000110" & "0";  -- previous 27 sep 2022
--constant c_sdp_cep_hdr_field_sel  : STD_LOGIC_VECTOR(c_sdp_cep_nof_hdr_fields-1 downto 0) :=
--    "100" & "000000010001" & "0100" & "0100" & "100000000" & "101000"&"0";  -- initial

  -- Default use source MAC/IP/UDP = 0 and destination MAC/IP/UDP = 0, so these have to be MM programmed
  -- before beamlet output packets can be send.
  constant c_sdp_cep_hdr_field_arr : t_common_field_arr(c_sdp_cep_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("eth_dst_mac"                        ), "RW", 48, field_default(0) ),  -- c_sdp_cep_eth_dst_mac
      ( field_name_pad("eth_src_mac"                        ), "RW", 48, field_default(0) ),
      ( field_name_pad("eth_type"                           ), "RW", 16, field_default(x"0800") ),

      ( field_name_pad("ip_version"                         ), "RW",  4, field_default(4) ),
      ( field_name_pad("ip_header_length"                   ), "RW",  4, field_default(5) ),
      ( field_name_pad("ip_services"                        ), "RW",  8, field_default(0) ),
      ( field_name_pad("ip_total_length"                    ), "RW", 16, field_default(c_sdp_cep_ip_total_length) ),
      ( field_name_pad("ip_identification"                  ), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_flags"                           ), "RW",  3, field_default(2) ),
      ( field_name_pad("ip_fragment_offset"                 ), "RW", 13, field_default(0) ),
      ( field_name_pad("ip_time_to_live"                    ), "RW",  8, field_default(127) ),
      ( field_name_pad("ip_protocol"                        ), "RW",  8, field_default(17) ),
      ( field_name_pad("ip_header_checksum"                 ), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_src_addr"                        ), "RW", 32, field_default(0) ),
      ( field_name_pad("ip_dst_addr"                        ), "RW", 32, field_default(0) ),  -- c_sdp_cep_ip_dst_addr

      ( field_name_pad("udp_src_port"                       ), "RW", 16, field_default(0) ),
      ( field_name_pad("udp_dst_port"                       ), "RW", 16, field_default(0) ),  -- c_sdp_cep_udp_dst_port
      ( field_name_pad("udp_total_length"                   ), "RW", 16, field_default(c_sdp_cep_udp_total_length) ),
      ( field_name_pad("udp_checksum"                       ), "RW", 16, field_default(0) ),

      ( field_name_pad("sdp_marker"                         ), "RW",  8, field_default(c_sdp_marker_beamlets) ),
      ( field_name_pad("sdp_version_id"                     ), "RW",  8, field_default(c_sdp_cep_version_id) ),
      ( field_name_pad("sdp_observation_id"                 ), "RW", 32, field_default(0) ),
      ( field_name_pad("sdp_station_info"                   ), "RW", 16, field_default(0) ),

      ( field_name_pad("sdp_source_info_reserved"               ), "RW",  5, field_default(0) ),
      ( field_name_pad("sdp_source_info_antenna_band_id"        ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_nyquist_zone_id"        ), "RW",  2, field_default(0) ),
      ( field_name_pad("sdp_source_info_f_adc"                  ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_fsub_type"              ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_payload_error"          ), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_beam_repositioning_flag"), "RW",  1, field_default(0) ),
      ( field_name_pad("sdp_source_info_beamlet_width"          ), "RW",  4, field_default(c_sdp_W_beamlet) ),
      ( field_name_pad("sdp_source_info_gn_id"                  ), "RW",  8, field_default(0) ),

      ( field_name_pad("sdp_reserved"              ), "RW", 32, field_default(0) ),
      ( field_name_pad("sdp_beamlet_scale"         ), "RW", 16, field_default(c_sdp_unit_beamlet_scale) ),
      ( field_name_pad("sdp_beamlet_index"         ), "RW", 16, field_default(0) ),
      ( field_name_pad("sdp_nof_blocks_per_packet" ), "RW",  8, field_default(c_sdp_cep_nof_blocks_per_packet) ),
      ( field_name_pad("sdp_nof_beamlets_per_block"), "RW", 16, field_default(c_sdp_cep_nof_beamlets_per_block) ),
      ( field_name_pad("sdp_block_period"          ), "RW", 16, field_default(c_sdp_block_period) ),

      ( field_name_pad("dp_bsn"), "RW", 64, field_default(0) )
  );
  constant c_sdp_reg_cep_hdr_dat_addr_w : natural := ceil_log2(field_nof_words(c_sdp_cep_hdr_field_arr, c_word_w));

  type t_sdp_network_cep_header is record
    sdp_marker                              : std_logic_vector( 7 downto 0);
    sdp_version_id                          : std_logic_vector( 7 downto 0);
    sdp_observation_id                      : std_logic_vector(31 downto 0);
    sdp_station_info                        : std_logic_vector(15 downto 0);

    sdp_source_info_reserved                : std_logic_vector( 4 downto 0);
    sdp_source_info_antenna_band_id         : std_logic_vector( 0 downto 0);
    sdp_source_info_nyquist_zone_id         : std_logic_vector( 1 downto 0);
    sdp_source_info_f_adc                   : std_logic_vector( 0 downto 0);
    sdp_source_info_fsub_type               : std_logic_vector( 0 downto 0);
    sdp_source_info_payload_error           : std_logic_vector( 0 downto 0);
    sdp_source_info_beam_repositioning_flag : std_logic_vector( 0 downto 0);
    sdp_source_info_beamlet_width           : std_logic_vector( 3 downto 0);
    sdp_source_info_gn_id                   : std_logic_vector( 7 downto 0);

    sdp_reserved                            : std_logic_vector(31 downto 0);
    sdp_beamlet_scale                       : std_logic_vector(15 downto 0);
    sdp_beamlet_index                       : std_logic_vector(15 downto 0);
    sdp_nof_blocks_per_packet               : std_logic_vector( 7 downto 0);
    sdp_nof_beamlets_per_block              : std_logic_vector(15 downto 0);
    sdp_block_period                        : std_logic_vector(15 downto 0);

    dp_bsn                                  : std_logic_vector(63 downto 0);
  end record;

  type t_sdp_cep_header is record
    eth : t_network_eth_header;
    ip  : t_network_ip_header;
    udp : t_network_udp_header;
    app : t_sdp_network_cep_header;
  end record;

  -----------------------------------------------------------------------------
  -- MM: address width to fit span of number of 32b words
  -----------------------------------------------------------------------------
  -- BSN monitor V2 address width
  constant c_sdp_reg_bsn_monitor_v2_addr_w  : natural := ceil_Log2(7);
  -- BSN align address width
  constant c_sdp_reg_bsn_align_v2_addr_w    : natural := ceil_log2(2);
  -- 10GbE MM address widths
  constant c_sdp_reg_bf_hdr_dat_addr_w      : natural := ceil_log2(c_sdp_N_beamsets) + c_sdp_reg_cep_hdr_dat_addr_w;
  constant c_sdp_reg_nw_10GbE_mac_addr_w    : natural := 13;
  constant c_sdp_reg_nw_10GbE_eth10g_addr_w : natural := 1;

  -- JESD204B
  constant c_sdp_jesd204b_freq             : string := "200MHz";
  constant c_sdp_mm_jesd_ctrl_reg          : t_c_mem := (latency  => 1,
                                                         adr_w    => 1,
                                                         dat_w    => c_word_w,
                                                         nof_dat  => 1,
                                                         init_sl  => '0');  -- PIO_JESD_CTRL

  -- AIT MM address widths
  constant c_sdp_jesd204b_addr_w               : natural := ceil_log2(c_sdp_S_pn) + tech_jesd204b_port_span_w;
                                                            -- = 4 + 8
  constant c_sdp_jesd_ctrl_addr_w              : natural := c_sdp_mm_jesd_ctrl_reg.adr_w;  -- = 1
  constant c_sdp_jesd_ctrl_reset_bi            : natural := 31;
  constant c_sdp_jesd_ctrl_enable_w            : natural := 31;
  constant c_sdp_reg_bsn_monitor_input_addr_w  : natural := 8;
  constant c_sdp_reg_wg_addr_w                 : natural := ceil_log2(c_sdp_S_pn) + 2;
  constant c_sdp_ram_wg_addr_w                 : natural := ceil_log2(c_sdp_S_pn) + 10;
  constant c_sdp_reg_dp_shiftram_addr_w        : natural := ceil_log2(c_sdp_S_pn) + 1;
  constant c_sdp_reg_bsn_source_v2_addr_w      : natural := 3;
  constant c_sdp_reg_bsn_scheduler_addr_w      : natural := 1;
  constant c_sdp_ram_diag_data_buf_bsn_addr_w  : natural := ceil_log2(c_sdp_S_pn) + ceil_log2(c_sdp_V_si_db_large);
                                                            -- Dimension DB address range for largest DB, so that both
                                                            -- the large and the default small DB fit.
  constant c_sdp_reg_diag_data_buf_bsn_addr_w  : natural := ceil_log2(c_sdp_S_pn) + 1;
  constant c_sdp_ram_st_histogram_addr_w       : natural := ceil_log2(c_sdp_S_pn) + ceil_log2(c_sdp_V_si_histogram);
  constant c_sdp_reg_aduh_monitor_addr_w       : natural := ceil_log2(c_sdp_S_pn) + 2;

  -- FSUB MM address widths
  constant c_sdp_ram_fil_coefs_addr_w       : natural := ceil_log2(c_sdp_R_os) + ceil_log2(c_sdp_N_fft * c_sdp_N_taps);
  constant c_sdp_ram_st_sst_addr_w          : natural := ceil_log2(c_sdp_R_os * c_sdp_P_pfb) +
                                                         ceil_log2(c_sdp_N_sub * c_sdp_Q_fft * c_sdp_W_statistic_sz);
  constant c_sdp_reg_si_addr_w              : natural := 1;  -- enable/disable
  constant c_sdp_ram_equalizer_gains_addr_w : natural := ceil_log2(c_sdp_R_os * c_sdp_P_pfb) +
                                                         ceil_log2(c_sdp_N_sub * c_sdp_Q_fft);
  constant c_sdp_reg_dp_selector_addr_w     : natural := 1;  -- Select input 0 or 1.
  constant c_sdp_reg_bsn_monitor_v2_sst_offload_addr_w : natural := c_sdp_reg_bsn_monitor_v2_addr_w;

  -- STAT UDP offload MM address widths
  constant c_sdp_reg_stat_enable_addr_w     : natural  := 1;

  -- BF MM address widths
  constant c_sdp_reg_sdp_info_addr_w                         : natural := 4;
  constant c_sdp_ram_ss_ss_wide_addr_w                       : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          ceil_log2(c_sdp_P_pfb *
                                                                                    c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_sdp_ram_bf_weights_addr_w                       : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          ceil_log2(c_sdp_N_pol_bf * c_sdp_P_pfb *
                                                                                    c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_sdp_reg_bf_scale_addr_w                         : natural := ceil_log2(c_sdp_N_beamsets) + 1;
  constant c_sdp_reg_dp_xonoff_addr_w                        : natural := ceil_log2(c_sdp_N_beamsets) + 1;
  constant c_sdp_ram_st_bst_addr_w                           : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          ceil_log2(c_sdp_S_sub_bf * c_sdp_N_pol_bf *
                                                                                    c_sdp_W_statistic_sz);
  constant c_sdp_reg_stat_enable_bst_addr_w                  : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_stat_enable_addr_w;
  constant c_sdp_reg_stat_hdr_dat_bst_addr_w                 : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_stat_hdr_dat_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_bst_offload_addr_w       : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_beamlet_output_addr_w    : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_align_v2_bf_addr_w                  : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          ceil_log2(c_sdp_P_sum) +
                                                                          c_sdp_reg_bsn_align_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_rx_align_bf_addr_w       : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          ceil_log2(c_sdp_P_sum) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_aligned_bf_addr_w        : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_ring_lane_info_bf_addr_w                : natural := ceil_log2(c_sdp_N_beamsets) + 1;
  constant c_sdp_reg_bsn_monitor_v2_ring_rx_bf_addr_w        : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_ring_tx_bf_addr_w        : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_dp_block_validate_err_bf_addr_w         : natural := ceil_log2(c_sdp_N_beamsets) + 4;
  constant c_sdp_reg_dp_block_validate_bsn_at_sync_bf_addr_w : natural := ceil_log2(c_sdp_N_beamsets) + 2;
  constant c_sdp_reg_bdo_destinations_info_w_one             : natural := ceil_log2(256);
  constant c_sdp_reg_bdo_destinations_info_w                 : natural := ceil_log2(c_sdp_N_beamsets) +
                                                                          c_sdp_reg_bdo_destinations_info_w_one;

  -- XSUB
  constant c_sdp_crosslets_index_w          : natural := ceil_log2(c_sdp_N_sub);
  constant c_sdp_mm_reg_crosslets_info : t_c_mem := (latency  => 1,
                                                     adr_w    => 4,
                                                     dat_w    => c_sdp_crosslets_index_w,
                                                     nof_dat  => 16,  -- 15 offsets + 1 step
                                                     init_sl  => '0');
  constant c_sdp_crosslets_info_reg_w       : natural := c_sdp_mm_reg_crosslets_info.nof_dat *
                                                         c_sdp_mm_reg_crosslets_info.dat_w;
  constant c_sdp_crosslets_info_nof_offsets : natural := c_sdp_mm_reg_crosslets_info.nof_dat - 1;

  type t_sdp_crosslets_info is record
    offset_arr : t_natural_arr(0 to c_sdp_crosslets_info_nof_offsets - 1);
    step       : natural;
  end record;

  constant c_sdp_crosslets_info_rst : t_sdp_crosslets_info := (offset_arr => (others => 0), step => 0);

  constant c_sdp_mm_reg_nof_crosslets  : t_c_mem := (latency  => 1,
                                                     adr_w    => 1,
                                                     dat_w    => ceil_log2(c_sdp_N_crosslets_max + 1),
                                                     nof_dat  => 1,
                                                     init_sl  => '0');  -- Default = 1
  constant c_sdp_nof_crosslets_reg_w : natural := c_sdp_mm_reg_nof_crosslets.nof_dat *
                                                  c_sdp_mm_reg_nof_crosslets.dat_w;

  -- XSUB MM address widths
  constant c_sdp_reg_crosslets_info_addr_w          : natural := c_sdp_mm_reg_crosslets_info.adr_w;
  constant c_sdp_reg_nof_crosslets_addr_w           : natural := c_sdp_mm_reg_nof_crosslets.adr_w;
  constant c_sdp_reg_bsn_sync_scheduler_xsub_addr_w : natural := 4;
  constant c_sdp_ram_st_xsq_addr_w                  : natural := ceil_log2(c_sdp_N_crosslets_max * c_sdp_X_sq *
                                                                           c_nof_complex * c_sdp_W_statistic_sz);
  constant c_sdp_ram_st_xsq_arr_addr_w              : natural := ceil_log2(c_sdp_P_sq) + c_sdp_ram_st_xsq_addr_w;
  constant c_sdp_reg_bsn_align_v2_xsub_addr_w       : natural := ceil_log2(c_sdp_P_sq) + c_sdp_reg_bsn_align_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_rx_align_xsub_addr_w       : natural := ceil_log2(c_sdp_P_sq) +
                                                                            c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_aligned_xsub_addr_w        : natural := c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_xst_offload_addr_w         : natural := c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_ring_lane_info_xst_addr_w                 : natural := 1;
  constant c_sdp_reg_bsn_monitor_v2_ring_rx_xst_addr_w         : natural := ceil_log2(c_sdp_N_pn_max) +
                                                                            c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_ring_tx_xst_addr_w         : natural := ceil_log2(c_sdp_N_pn_max) +
                                                                            c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_dp_block_validate_err_xst_addr_w          : natural := 4;
  constant c_sdp_reg_dp_block_validate_bsn_at_sync_xst_addr_w  : natural := 2;

  -- RING MM address widths
  constant c_sdp_reg_bsn_monitor_v2_ring_rx_addr_w        : natural := ceil_log2(c_sdp_N_ring_lanes_max) +
                                                                       ceil_log2(c_sdp_N_pn_max) +
                                                                       c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_bsn_monitor_v2_ring_tx_addr_w        : natural := ceil_log2(c_sdp_N_ring_lanes_max) +
                                                                       ceil_log2(c_sdp_N_pn_max) +
                                                                       c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_sdp_reg_ring_lane_info_addr_w                : natural := ceil_log2(c_sdp_N_ring_lanes_max) + 1;
  constant c_sdp_reg_dp_xonoff_lane_addr_w                : natural := ceil_log2(c_sdp_N_ring_lanes_max) + 1;
  constant c_sdp_reg_dp_xonoff_local_addr_w               : natural := ceil_log2(c_sdp_N_ring_lanes_max) + 1;
  constant c_sdp_reg_dp_block_validate_err_addr_w         : natural := ceil_log2(c_sdp_N_ring_lanes_max) + 4;
  constant c_sdp_reg_dp_block_validate_bsn_at_sync_addr_w : natural := ceil_log2(c_sdp_N_ring_lanes_max) + 2;
  constant c_sdp_reg_ring_info_addr_w                     : natural := 2;
  constant c_sdp_reg_tr_10GbE_mac_addr_w                  : natural := ceil_log2(c_sdp_N_ring_nof_mac10g) + 13;
  constant c_sdp_reg_tr_10GbE_eth10g_addr_w               : natural := ceil_log2(c_sdp_N_ring_nof_mac10g) + 1;
  constant c_sdp_reg_diag_bg_addr_w                       : natural := 3;
  constant c_sdp_ram_diag_bg_addr_w                       : natural := 7;

  -------------------------------------------------
  -- SDP simulation constants record, to use instead of HW default when g_sim = TRUE
  -------------------------------------------------
  type t_sdp_sim is record
    N_clk_per_sync       : natural;  -- for statistics offload timing
    N_clk_per_sync_min   : natural;  -- for statistics offload timing
    offload_node_time    : natural;  -- select > 0 and gn_index > 0 to see effect on statistics offload
    offload_packet_time  : natural;  -- select > 0 to see effect on statistics offload
    offload_scale_w      : natural;  -- for statistics offload timing
    sync_timeout         : natural;
    unb_nr               : natural;
    node_nr              : natural;
  end record;

  constant c_sdp_sim : t_sdp_sim := (2, 1, 10, 5, 1, 3 * 1024, 0, 0);

  -------------------------------------------------
  -- SDP functions
  -------------------------------------------------

  function func_sdp_gn_index_to_pn_index(gn_index : natural) return natural;
  function func_sdp_modulo_N_sub(sub_index : natural) return natural;

  function func_sdp_get_stat_marker(g_statistics_type : string) return natural;
  function func_sdp_get_stat_nof_signal_inputs(g_statistics_type : string) return natural;

  -- nof_statistics_per_packet = mm_nof_data * mm_data_size / c_sdp_W_statistic_sz
  function func_sdp_get_stat_from_mm_user_size(g_statistics_type : string) return natural;
  function func_sdp_get_stat_from_mm_data_size(g_statistics_type : string) return natural;
  function func_sdp_get_stat_from_mm_step_size(g_statistics_type : string) return natural;
  function func_sdp_get_stat_from_mm_nof_data(g_statistics_type : string) return natural;
  function func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type : string) return natural;

  function func_sdp_get_stat_app_total_length(g_statistics_type : string) return natural;
  function func_sdp_get_stat_udp_total_length(g_statistics_type : string) return natural;
  function func_sdp_get_stat_ip_total_length(g_statistics_type : string) return natural;
  function func_sdp_get_stat_udp_src_port(g_statistics_type : string; gn_index : natural) return std_logic_vector;
  function func_sdp_get_stat_nof_packets(g_statistics_type : string; S_pn, P_sq, N_crosslets : natural) return natural;
  function func_sdp_get_stat_nof_packets(g_statistics_type : string) return natural;  -- use c_sdp_S_pn, c_sdp_P_sq,
                                                                                      -- c_sdp_N_crosslets_max

  function func_sdp_map_stat_header(hdr_fields_raw : std_logic_vector) return t_sdp_stat_header;
  function func_sdp_map_cep_header(hdr_fields_raw : std_logic_vector) return t_sdp_cep_header;

  -- Select header destination MAC, IP, UDP fields from DP (sl = 0) or from MM (sl = '1') in dp_offload_tx_v3
  function func_sdp_cep_hdr_field_sel_dst(sl : std_logic) return std_logic_vector;

  function func_sdp_map_stat_data_id(g_statistics_type : string; data_id_slv : std_logic_vector)
      return t_sdp_stat_data_id;
  function func_sdp_map_stat_data_id(g_statistics_type : string; data_id_rec : t_sdp_stat_data_id)
      return std_logic_vector;

  function func_sdp_map_crosslets_info(info_slv : std_logic_vector)
      return t_sdp_crosslets_info;  -- map all c_sdp_N_crosslets_max offsets
  function func_sdp_map_crosslets_info(info_rec : t_sdp_crosslets_info)
      return std_logic_vector;  -- map all c_sdp_N_crosslets_max offsets
  function func_sdp_step_crosslets_info(info_rec : t_sdp_crosslets_info)
      return t_sdp_crosslets_info;  -- step all c_sdp_N_crosslets_max offsets
end package sdp_pkg;

package body sdp_pkg is
  function func_sdp_gn_index_to_pn_index(gn_index : natural) return natural is
    -- Determine PN index that starts at 0 per antenna band. For LOFAR2 SDP
    -- each antenna_band has c_sdp_N_pn_max = 16 PN. The pn_index defines the
    -- PN index within an antenna_band:
    --
    --   pn_index = gn_index MOD c_sdp_N_pn_max
    --
    -- The c_sdp_N_pn_max = 16 fits the LB and HB of LOFAR2:
    -- . The LB starts at GN index = 0 and has c_sdp_N_pn_max = 16 nodes.
    -- . The HB starts at GN index = c_sdp_N_pn_max, and has 8 or 16 nodes
    --   dependent on the type of station.
    --
    -- The fact that c_sdp_N_pn_max = 16 implies that instead of implementing
    -- MOD it is possible to do:
    --
    --   pn_index = gn_index[3:0], because log2(16) = 4
    constant c_w  : natural := ceil_log2(c_sdp_N_pn_max);  -- = 4

    variable v_index : std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(gn_index, c_word_w);
  begin
    return TO_UINT(v_index(c_w - 1 downto 0));
  end func_sdp_gn_index_to_pn_index;

  function func_sdp_modulo_N_sub(sub_index : natural) return natural is
  begin
    assert sub_index < 2 * c_sdp_N_sub
      report "func_sdp_modulo_N_sub: sub_index too large"
      severity FAILURE;
    if sub_index < c_sdp_N_sub - 1 then
      return sub_index;
    else
      return sub_index - c_sdp_N_sub;
    end if;
  end func_sdp_modulo_N_sub;

  function func_sdp_get_stat_marker(g_statistics_type : string) return natural is
    constant c_marker_sst : natural := 83;  -- = 0x53 = 'S'
    constant c_marker_bst : natural := 66;  -- = 0x42 = 'B'
    constant c_marker_xst : natural := 88;  -- = 0x58 = 'X'
  begin
    return sel_a_b(g_statistics_type = "BST", c_marker_bst,
           sel_a_b(g_statistics_type = "XST", c_marker_xst,
                                            c_marker_sst));  -- SST, SST_OS
  end func_sdp_get_stat_marker;

  function func_sdp_get_stat_nof_signal_inputs(g_statistics_type : string) return natural is
  begin
    return sel_a_b(g_statistics_type = "BST", 0,  -- not applicable for BST, so use 0,
           sel_a_b(g_statistics_type = "XST", c_sdp_S_pn,
                                            1));  -- SST, SST_OS
  end func_sdp_get_stat_nof_signal_inputs;

  function func_sdp_get_stat_from_mm_user_size(g_statistics_type : string) return natural is
  -- see sdp_statistics_offload.vhd for description
  begin
    return sel_a_b(g_statistics_type = "BST", c_sdp_W_statistic_sz,  -- = 2, so preserve X, Y order
           sel_a_b(g_statistics_type = "XST", c_sdp_W_statistic_sz,  -- = 2, so preserve Re, Im order
                                            c_sdp_W_statistic_sz));  -- = 2, SST, SST_OS
  end func_sdp_get_stat_from_mm_user_size;

  function func_sdp_get_stat_from_mm_data_size(g_statistics_type : string) return natural is
  begin
    return sel_a_b(g_statistics_type = "BST", c_sdp_N_pol_bf * c_sdp_W_statistic_sz,  -- = 4
           sel_a_b(g_statistics_type = "XST", c_nof_complex  * c_sdp_W_statistic_sz,  -- = 4
                                                             c_sdp_W_statistic_sz));  -- = 2, SST, SST_OS
  end func_sdp_get_stat_from_mm_data_size;

  function func_sdp_get_stat_from_mm_step_size(g_statistics_type : string) return natural is
    constant c_data_size : natural := func_sdp_get_stat_from_mm_data_size(g_statistics_type);
  begin
    return sel_a_b(g_statistics_type = "BST", c_data_size,  -- = 4
           sel_a_b(g_statistics_type = "XST", c_data_size,  -- = 4
                                            c_data_size * c_sdp_Q_fft));  -- = 4, SST, SST_OS
  end func_sdp_get_stat_from_mm_step_size;

  function func_sdp_get_stat_from_mm_nof_data(g_statistics_type : string) return natural is
  begin
    return sel_a_b(g_statistics_type = "BST", c_sdp_S_sub_bf,  -- = 488
           sel_a_b(g_statistics_type = "XST", c_sdp_X_sq,  -- = 144
                                            c_sdp_N_sub));  -- = 512, SST, SST_OS
  end func_sdp_get_stat_from_mm_nof_data;

  -- nof_statistics_per_packet = mm_nof_data * mm_data_size / c_sdp_W_statistic_sz
  function func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type : string) return natural is
  begin
    return sel_a_b(g_statistics_type = "BST", c_sdp_S_sub_bf * c_sdp_N_pol_bf,  -- = 976
           sel_a_b(g_statistics_type = "XST", c_sdp_X_sq * c_nof_complex,  -- = 288
                                            c_sdp_N_sub));  -- = 512, SST, SST_OS
  end func_sdp_get_stat_nof_statistics_per_packet;

  function func_sdp_get_stat_app_total_length(g_statistics_type : string) return natural is
    constant c_nof_statistics_per_packet : natural := func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type);
  begin
    -- RETURN:
    -- . SST : 4128 (= 4096 + 32)
    -- . BST : 7840 (= 7808 + 32)
    -- . XST : 2336 (= 2304 + 32)
    return c_nof_statistics_per_packet * c_sdp_nof_bytes_per_statistic + c_sdp_stat_app_header_len;
  end func_sdp_get_stat_app_total_length;

  function func_sdp_get_stat_udp_total_length(g_statistics_type : string) return natural is
    constant c_sdp_app_total_length : natural := func_sdp_get_stat_app_total_length(g_statistics_type);
  begin
    -- RETURN:
    -- . SST : 4136 (= 4128 + 8)
    -- . BST : 7848 (= 7840 + 8)
    -- . XST : 2344 (= 2336 + 8)
    return c_sdp_app_total_length + c_network_udp_header_len;
  end func_sdp_get_stat_udp_total_length;

  function func_sdp_get_stat_ip_total_length(g_statistics_type : string) return natural is
    constant c_sdp_udp_total_length : natural := func_sdp_get_stat_udp_total_length(g_statistics_type);
  begin
    -- RETURN:
    -- . SST : 4156 (= 4136 + 20)
    -- . BST : 7868 (= 7848 + 20)
    -- . XST : 2364 (= 2344 + 20)
    return c_sdp_udp_total_length + c_network_ip_header_len;
  end func_sdp_get_stat_ip_total_length;

  function func_sdp_get_stat_udp_src_port(g_statistics_type : string; gn_index : natural) return std_logic_vector is
    constant c_gn_index : std_logic_vector(7 downto 0) := TO_UVEC(gn_index, 8);
  begin
    return sel_a_b(g_statistics_type = "BST", c_sdp_bst_udp_src_port_15_8 & c_gn_index,  -- BST = 0xD1 & gn_index
           sel_a_b(g_statistics_type = "XST", c_sdp_xst_udp_src_port_15_8 & c_gn_index,  -- XST = 0xD2 & gn_index
                                              c_sdp_sst_udp_src_port_15_8 & c_gn_index));  -- SST = 0xD0 & gn_index,
                                                                                           -- SST_OS
  end func_sdp_get_stat_udp_src_port;

  function func_sdp_get_stat_nof_packets(g_statistics_type : string; S_pn, P_sq, N_crosslets : natural)
      return natural is
  begin
    return sel_a_b(g_statistics_type = "BST", 1,
           sel_a_b(g_statistics_type = "XST", P_sq * N_crosslets,
           sel_a_b(g_statistics_type = "SST", S_pn,
                                            c_sdp_R_os * S_pn)));  -- SST_OS
  end func_sdp_get_stat_nof_packets;

  function func_sdp_get_stat_nof_packets(g_statistics_type : string) return natural is
  begin
    return func_sdp_get_stat_nof_packets(g_statistics_type, c_sdp_S_pn, c_sdp_P_sq, c_sdp_N_crosslets_max);
  end func_sdp_get_stat_nof_packets;

  function func_sdp_map_stat_header(hdr_fields_raw : std_logic_vector) return t_sdp_stat_header is
    constant c_stat_hfa : t_common_field_arr(c_sdp_stat_hdr_field_arr'range) := c_sdp_stat_hdr_field_arr;
    variable v : t_sdp_stat_header;
  begin
    -- eth header
    v.eth.dst_mac  := hdr_fields_raw(field_hi(c_stat_hfa, "eth_dst_mac") downto field_lo(c_stat_hfa, "eth_dst_mac"));
    v.eth.src_mac  := hdr_fields_raw(field_hi(c_stat_hfa, "eth_src_mac") downto field_lo(c_stat_hfa, "eth_src_mac"));
    v.eth.eth_type := hdr_fields_raw(field_hi(c_stat_hfa, "eth_type")    downto field_lo(c_stat_hfa, "eth_type"));

    -- ip header
    v.ip.version         := hdr_fields_raw(field_hi(c_stat_hfa, "ip_version") downto
                                           field_lo(c_stat_hfa, "ip_version"));
    v.ip.header_length   := hdr_fields_raw(field_hi(c_stat_hfa, "ip_header_length") downto
                                           field_lo(c_stat_hfa, "ip_header_length"));
    v.ip.services        := hdr_fields_raw(field_hi(c_stat_hfa, "ip_services") downto
                                           field_lo(c_stat_hfa, "ip_services"));
    v.ip.total_length    := hdr_fields_raw(field_hi(c_stat_hfa, "ip_total_length") downto
                                           field_lo(c_stat_hfa, "ip_total_length"));
    v.ip.identification  := hdr_fields_raw(field_hi(c_stat_hfa, "ip_identification") downto
                                           field_lo(c_stat_hfa, "ip_identification"));
    v.ip.flags           := hdr_fields_raw(field_hi(c_stat_hfa, "ip_flags") downto
                                           field_lo(c_stat_hfa, "ip_flags"));
    v.ip.fragment_offset := hdr_fields_raw(field_hi(c_stat_hfa, "ip_fragment_offset") downto
                                           field_lo(c_stat_hfa, "ip_fragment_offset"));
    v.ip.time_to_live    := hdr_fields_raw(field_hi(c_stat_hfa, "ip_time_to_live") downto
                                           field_lo(c_stat_hfa, "ip_time_to_live"));
    v.ip.protocol        := hdr_fields_raw(field_hi(c_stat_hfa, "ip_protocol") downto
                                           field_lo(c_stat_hfa, "ip_protocol"));
    v.ip.header_checksum := hdr_fields_raw(field_hi(c_stat_hfa, "ip_header_checksum") downto
                                           field_lo(c_stat_hfa, "ip_header_checksum"));
    v.ip.src_ip_addr     := hdr_fields_raw(field_hi(c_stat_hfa, "ip_src_addr") downto
                                           field_lo(c_stat_hfa, "ip_src_addr"));
    v.ip.dst_ip_addr     := hdr_fields_raw(field_hi(c_stat_hfa, "ip_dst_addr") downto
                                           field_lo(c_stat_hfa, "ip_dst_addr"));

    -- udp header
    v.udp.src_port       := hdr_fields_raw(field_hi(c_stat_hfa, "udp_src_port") downto
                                           field_lo(c_stat_hfa, "udp_src_port"));
    v.udp.dst_port       := hdr_fields_raw(field_hi(c_stat_hfa, "udp_dst_port") downto
                                           field_lo(c_stat_hfa, "udp_dst_port"));
    v.udp.total_length   := hdr_fields_raw(field_hi(c_stat_hfa, "udp_total_length") downto
                                           field_lo(c_stat_hfa, "udp_total_length"));
    v.udp.checksum       := hdr_fields_raw(field_hi(c_stat_hfa, "udp_checksum") downto
                                           field_lo(c_stat_hfa, "udp_checksum"));

    -- app header
    v.app.sdp_marker         := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_marker") downto
                                               field_lo(c_stat_hfa, "sdp_marker"));
    v.app.sdp_version_id     := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_version_id") downto
                                               field_lo(c_stat_hfa, "sdp_version_id"));
    v.app.sdp_observation_id := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_observation_id") downto
                                               field_lo(c_stat_hfa, "sdp_observation_id"));
    v.app.sdp_station_info   := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_station_info") downto
                                               field_lo(c_stat_hfa, "sdp_station_info"));

    v.app.sdp_source_info_antenna_band_id := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_antenna_band_id")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_antenna_band_id"));
    v.app.sdp_source_info_nyquist_zone_id := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_nyquist_zone_id")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_nyquist_zone_id"));
    v.app.sdp_source_info_f_adc           := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_f_adc")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_f_adc"));
    v.app.sdp_source_info_fsub_type       := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_fsub_type")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_fsub_type"));
    v.app.sdp_source_info_payload_error   := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_payload_error")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_payload_error"));
    v.app.sdp_source_info_beam_repositioning_flag := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_beam_repositioning_flag")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_beam_repositioning_flag"));
    v.app.sdp_source_info_weighted_subbands_flag  := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_weighted_subbands_flag")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_weighted_subbands_flag"));
    v.app.sdp_source_info_gn_id                 := hdr_fields_raw(
                                                    field_hi(c_stat_hfa, "sdp_source_info_gn_id")
                                             downto field_lo(c_stat_hfa, "sdp_source_info_gn_id"));

    v.app.sdp_reserved                            := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_reserved")
                                                             downto field_lo(c_stat_hfa, "sdp_reserved"));
    v.app.sdp_integration_interval                := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_integration_interval")
                                                             downto field_lo(c_stat_hfa, "sdp_integration_interval"));
    v.app.sdp_data_id                             := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_data_id")
                                                             downto field_lo(c_stat_hfa, "sdp_data_id"));
    v.app.sdp_data_id_sst_signal_input_index      := v.app.sdp_data_id( 7 downto  0);
    v.app.sdp_data_id_bst_beamlet_index           := v.app.sdp_data_id(15 downto  0);
    v.app.sdp_data_id_xst_subband_index           := v.app.sdp_data_id(24 downto 16);
    v.app.sdp_data_id_xst_signal_input_A_index    := v.app.sdp_data_id(15 downto  8);
    v.app.sdp_data_id_xst_signal_input_B_index    := v.app.sdp_data_id( 7 downto  0);
    v.app.sdp_nof_signal_inputs         := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_nof_signal_inputs")
                                                   downto field_lo(c_stat_hfa, "sdp_nof_signal_inputs"));
    v.app.sdp_nof_bytes_per_statistic   := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_nof_bytes_per_statistic")
                                                   downto field_lo(c_stat_hfa, "sdp_nof_bytes_per_statistic"));
    v.app.sdp_nof_statistics_per_packet := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_nof_statistics_per_packet")
                                                   downto field_lo(c_stat_hfa, "sdp_nof_statistics_per_packet"));
    v.app.sdp_block_period              := hdr_fields_raw(field_hi(c_stat_hfa, "sdp_block_period")
                                                   downto field_lo(c_stat_hfa, "sdp_block_period"));

    v.app.dp_bsn := hdr_fields_raw(field_hi(c_stat_hfa, "dp_bsn") downto field_lo(c_stat_hfa, "dp_bsn"));
    return v;
  end func_sdp_map_stat_header;

  function func_sdp_map_cep_header(hdr_fields_raw : std_logic_vector) return t_sdp_cep_header is
    constant c_cep_hfa : t_common_field_arr(c_sdp_cep_hdr_field_arr'range) := c_sdp_cep_hdr_field_arr;
    variable v : t_sdp_cep_header;
  begin
    -- eth header
    v.eth.dst_mac  := hdr_fields_raw(field_hi(c_cep_hfa, "eth_dst_mac") downto field_lo(c_cep_hfa, "eth_dst_mac"));
    v.eth.src_mac  := hdr_fields_raw(field_hi(c_cep_hfa, "eth_src_mac") downto field_lo(c_cep_hfa, "eth_src_mac"));
    v.eth.eth_type := hdr_fields_raw(field_hi(c_cep_hfa, "eth_type")    downto field_lo(c_cep_hfa, "eth_type"));

    -- ip header
    v.ip.version         := hdr_fields_raw(field_hi(c_cep_hfa, "ip_version") downto
                                           field_lo(c_cep_hfa, "ip_version"));
    v.ip.header_length   := hdr_fields_raw(field_hi(c_cep_hfa, "ip_header_length") downto
                                           field_lo(c_cep_hfa, "ip_header_length"));
    v.ip.services        := hdr_fields_raw(field_hi(c_cep_hfa, "ip_services") downto
                                           field_lo(c_cep_hfa, "ip_services"));
    v.ip.total_length    := hdr_fields_raw(field_hi(c_cep_hfa, "ip_total_length") downto
                                           field_lo(c_cep_hfa, "ip_total_length"));
    v.ip.identification  := hdr_fields_raw(field_hi(c_cep_hfa, "ip_identification") downto
                                           field_lo(c_cep_hfa, "ip_identification"));
    v.ip.flags           := hdr_fields_raw(field_hi(c_cep_hfa, "ip_flags") downto
                                           field_lo(c_cep_hfa, "ip_flags"));
    v.ip.fragment_offset := hdr_fields_raw(field_hi(c_cep_hfa, "ip_fragment_offset") downto
                                           field_lo(c_cep_hfa, "ip_fragment_offset"));
    v.ip.time_to_live    := hdr_fields_raw(field_hi(c_cep_hfa, "ip_time_to_live") downto
                                           field_lo(c_cep_hfa, "ip_time_to_live"));
    v.ip.protocol        := hdr_fields_raw(field_hi(c_cep_hfa, "ip_protocol") downto
                                           field_lo(c_cep_hfa, "ip_protocol"));
    v.ip.header_checksum := hdr_fields_raw(field_hi(c_cep_hfa, "ip_header_checksum") downto
                                           field_lo(c_cep_hfa, "ip_header_checksum"));
    v.ip.src_ip_addr     := hdr_fields_raw(field_hi(c_cep_hfa, "ip_src_addr") downto
                                           field_lo(c_cep_hfa, "ip_src_addr"));
    v.ip.dst_ip_addr     := hdr_fields_raw(field_hi(c_cep_hfa, "ip_dst_addr") downto
                                           field_lo(c_cep_hfa, "ip_dst_addr"));

    -- udp header
    v.udp.src_port       := hdr_fields_raw(field_hi(c_cep_hfa, "udp_src_port") downto
                                           field_lo(c_cep_hfa, "udp_src_port"));
    v.udp.dst_port       := hdr_fields_raw(field_hi(c_cep_hfa, "udp_dst_port") downto
                                           field_lo(c_cep_hfa, "udp_dst_port"));
    v.udp.total_length   := hdr_fields_raw(field_hi(c_cep_hfa, "udp_total_length") downto
                                           field_lo(c_cep_hfa, "udp_total_length"));
    v.udp.checksum       := hdr_fields_raw(field_hi(c_cep_hfa, "udp_checksum") downto
                                           field_lo(c_cep_hfa, "udp_checksum"));

    -- app header
    v.app.sdp_marker         := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_marker") downto
                                               field_lo(c_cep_hfa, "sdp_marker"));
    v.app.sdp_version_id     := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_version_id") downto
                                               field_lo(c_cep_hfa, "sdp_version_id"));
    v.app.sdp_observation_id := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_observation_id") downto
                                               field_lo(c_cep_hfa, "sdp_observation_id"));
    v.app.sdp_station_info   := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_station_info") downto
                                               field_lo(c_cep_hfa, "sdp_station_info"));

    v.app.sdp_source_info_reserved                := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_reserved")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_reserved"));
    v.app.sdp_source_info_antenna_band_id         := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_antenna_band_id")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_antenna_band_id"));
    v.app.sdp_source_info_nyquist_zone_id         := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_nyquist_zone_id")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_nyquist_zone_id"));
    v.app.sdp_source_info_f_adc                   := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_f_adc")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_f_adc"));
    v.app.sdp_source_info_fsub_type               := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_fsub_type")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_fsub_type"));
    v.app.sdp_source_info_payload_error           := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_payload_error")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_payload_error"));
    v.app.sdp_source_info_beam_repositioning_flag := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_beam_repositioning_flag")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_beam_repositioning_flag"));
    v.app.sdp_source_info_beamlet_width           := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_beamlet_width")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_beamlet_width"));
    v.app.sdp_source_info_gn_id                   := hdr_fields_raw(
                                                       field_hi(c_cep_hfa, "sdp_source_info_gn_id")
                                                downto field_lo(c_cep_hfa, "sdp_source_info_gn_id"));

    v.app.sdp_reserved               := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_reserved") downto
                                                       field_lo(c_cep_hfa, "sdp_reserved"));
    v.app.sdp_beamlet_scale          := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_beamlet_scale") downto
                                                       field_lo(c_cep_hfa, "sdp_beamlet_scale"));
    v.app.sdp_beamlet_index          := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_beamlet_index") downto
                                                       field_lo(c_cep_hfa, "sdp_beamlet_index"));
    v.app.sdp_nof_blocks_per_packet  := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_nof_blocks_per_packet") downto
                                                       field_lo(c_cep_hfa, "sdp_nof_blocks_per_packet"));
    v.app.sdp_nof_beamlets_per_block := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_nof_beamlets_per_block") downto
                                                       field_lo(c_cep_hfa, "sdp_nof_beamlets_per_block"));
    v.app.sdp_block_period           := hdr_fields_raw(field_hi(c_cep_hfa, "sdp_block_period") downto
                                                       field_lo(c_cep_hfa, "sdp_block_period"));

    v.app.dp_bsn := hdr_fields_raw(field_hi(c_cep_hfa, "dp_bsn") downto field_lo(c_cep_hfa, "dp_bsn"));
    return v;
  end func_sdp_map_cep_header;

  function func_sdp_cep_hdr_field_sel_dst(sl : std_logic) return std_logic_vector is
    variable v_sel : std_logic_vector(c_sdp_cep_nof_hdr_fields - 1 downto 0) := c_sdp_cep_hdr_field_sel;
  begin
    -- Select header destination MAC, IP, UDP field from DP or from MM in dp_offload_tx_v3
    v_sel(38) := sl;  -- eth_dst_mac
    v_sel(24) := sl;  -- ip_dst_addr
    v_sel(22) := sl;  -- udp_dst_port
    return v_sel;
  end func_sdp_cep_hdr_field_sel_dst;

  function func_sdp_map_stat_data_id(g_statistics_type : string; data_id_slv : std_logic_vector)
      return t_sdp_stat_data_id is
    variable v_rec : t_sdp_stat_data_id;
  begin
    if g_statistics_type = "BST" then
      v_rec.bst_beamlet_index := TO_UINT(data_id_slv(15 downto 0));
    elsif g_statistics_type = "XST" then
      v_rec.xst_subband_index := TO_UINT(data_id_slv(24 downto 16));
      v_rec.xst_signal_input_A_index := TO_UINT(data_id_slv(15 downto 8));
      v_rec.xst_signal_input_B_index := TO_UINT(data_id_slv(7 downto 0));
    else  -- SST, SST_OS
      v_rec.sst_signal_input_index := TO_UINT(data_id_slv(7 downto 0));
    end if;
    return v_rec;
  end func_sdp_map_stat_data_id;

  function func_sdp_map_stat_data_id(g_statistics_type : string; data_id_rec : t_sdp_stat_data_id)
      return std_logic_vector is
    variable v_slv : std_logic_vector(31 downto 0) := x"00000000";
  begin
    if g_statistics_type = "BST" then
      v_slv(15 downto 0) := TO_UVEC(data_id_rec.bst_beamlet_index, 16);
    elsif g_statistics_type = "XST" then
      v_slv(24 downto 16) := TO_UVEC(data_id_rec.xst_subband_index, 9);
      v_slv(15 downto 8) := TO_UVEC(data_id_rec.xst_signal_input_A_index, 8);
      v_slv(7 downto 0) := TO_UVEC(data_id_rec.xst_signal_input_B_index, 8);
    else  -- SST, SST_OS
      v_slv(7 downto 0) := TO_UVEC(data_id_rec.sst_signal_input_index, 8);
    end if;
    return v_slv;
  end func_sdp_map_stat_data_id;

  function func_sdp_map_crosslets_info(info_slv : std_logic_vector) return t_sdp_crosslets_info is
    variable v_info : t_sdp_crosslets_info;
  begin
    for I in 0 to c_sdp_crosslets_info_nof_offsets - 1 loop  -- map al offsets
      v_info.offset_arr(I) := TO_UINT(info_slv((I + 1) * c_sdp_crosslets_index_w - 1 downto
                                               I * c_sdp_crosslets_index_w));
    end loop;
    v_info.step := TO_UINT(info_slv(c_sdp_crosslets_info_reg_w - 1 downto
                                    c_sdp_crosslets_info_reg_w - c_sdp_crosslets_index_w));
    return v_info;
  end func_sdp_map_crosslets_info;

  function func_sdp_map_crosslets_info(info_rec : t_sdp_crosslets_info) return std_logic_vector is
    variable v_info : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0);
  begin
    for I in 0 to c_sdp_crosslets_info_nof_offsets - 1 loop  -- map all offsets
      v_info((I + 1) * c_sdp_crosslets_index_w - 1 downto I * c_sdp_crosslets_index_w) :=
                                                               TO_UVEC(info_rec.offset_arr(I), c_sdp_crosslets_index_w);
    end loop;
    v_info(c_sdp_crosslets_info_reg_w - 1 downto c_sdp_crosslets_info_reg_w - c_sdp_crosslets_index_w) :=
                                                                        TO_UVEC(info_rec.step, c_sdp_crosslets_index_w);
    return v_info;
  end func_sdp_map_crosslets_info;

  function func_sdp_step_crosslets_info(info_rec : t_sdp_crosslets_info) return t_sdp_crosslets_info is
    variable v_info : t_sdp_crosslets_info := info_rec;
  begin
    for I in 0 to c_sdp_crosslets_info_nof_offsets - 1 loop  -- step all offsets
      -- c_sdp_N_sub = 512 is power of 2, so MOD should be fine in synthesis (simpel skips MSbits, no need for
      -- division).
      v_info.offset_arr(I) := (v_info.offset_arr(I) + v_info.step) mod c_sdp_N_sub;
    end loop;
    return v_info;
  end func_sdp_step_crosslets_info;
end sdp_pkg;
