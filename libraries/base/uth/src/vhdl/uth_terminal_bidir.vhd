-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Instantiate a pair of uthernet TX and RX terminals
-- Description:
--   The uth_terminal_bidir provides an compact component for instantiating
--   a duplex or simplex uthernet terminal.
-- Remarks:
-- . Some of the constants can be changed into generics when some future
--   application needs so.

library IEEE, common_lib, technology_lib, dp_lib, uth_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use work.uth_pkg.all;
use technology_lib.technology_select_pkg.all;

entity uth_terminal_bidir is
  generic (
    g_technology          : natural := c_tech_select_default;
    -- User
    g_usr_nof_streams     : natural := 4;  -- number of user streams per bus
    g_usr_use_complex     : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
    g_usr_data_w          : natural := 32;
    g_usr_frame_len       : natural := 20;
    -- DP/UTH packet
    g_packet_data_w       : natural := 32;  -- packet data width for DP packet and for UTH packet, must be >= g_usr_data_w
    -- Phy
    g_phy_nof_serial      : natural := 4;  -- up to 4 serial lanes per bus
    -- Tx
    g_use_tx              : boolean := true;
    g_tx_input_use_fifo   : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
    g_tx_input_fifo_size  : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
    g_tx_input_fifo_fill  : natural := 0;
    g_tx_mux_mode         : natural := 0;  -- can use 0 for non-blocking tx mux in dp_distribute or use 1 to preserve input order for tx
    -- Rx
    g_use_rx              : boolean := true;
    g_rx_mux_mode         : natural := 0;  -- must use 0 for non-blocking rx mux in dp_distribute (can not use 1 to preserve input order for rx, because some rx frames may go lost)
    g_rx_output_use_fifo  : boolean := true;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
    g_rx_output_fifo_size : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
    g_rx_output_fifo_fill : natural := 0;
    g_rx_timeout_w        : natural := 0;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
    -- UTH
    g_uth_len_max         : natural := 255;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
    g_uth_typ_ofs         : natural := 256  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
  );
  port (
    dp_rst                : in  std_logic;
    dp_clk                : in  std_logic;

    -- usr side interface
    tx_dp_sosi_arr        : in  t_dp_sosi_arr(g_usr_nof_streams - 1 downto 0);
    tx_dp_siso_arr        : out t_dp_siso_arr(g_usr_nof_streams - 1 downto 0);

    rx_dp_sosi_arr        : out t_dp_sosi_arr(g_usr_nof_streams - 1 downto 0);
    rx_dp_siso_arr        : in  t_dp_siso_arr(g_usr_nof_streams - 1 downto 0);

    -- phy side interface
    tx_uth_sosi_arr       : out t_dp_sosi_arr(g_phy_nof_serial - 1 downto 0);
    tx_uth_siso_arr       : in  t_dp_siso_arr(g_phy_nof_serial - 1 downto 0);

    rx_uth_sosi_arr       : in  t_dp_sosi_arr(g_phy_nof_serial - 1 downto 0);
    rx_uth_siso_arr       : out t_dp_siso_arr(g_phy_nof_serial - 1 downto 0);

    -- monitoring interface
    rx_mon_pkt_sosi_arr   : out t_dp_sosi_arr(g_phy_nof_serial - 1 downto 0);
    rx_mon_dist_sosi_arr  : out t_dp_sosi_arr(g_usr_nof_streams - 1 downto 0)
  );
end uth_terminal_bidir;

architecture str of uth_terminal_bidir is
  -- DP/UTH packet
  constant c_dp_packet_len            : natural := func_dp_packet_overhead_len(g_packet_data_w) + g_usr_frame_len;
  constant c_uth_packet_len           : natural := c_dp_packet_len + c_uth_nof_overhead;

  constant c_uth_nof_ch               : natural := 1;  -- support only one type of UTH frames
  constant c_use_uth_err              : boolean := true;  -- when TRUE insert the one bit CRC status from uth_rx in the received DP packet err field, else pass on the received DP packet err field unchanged
  constant c_uth_err_bi               : natural := 0;  -- bit index for where to insert the UTH err bit

  -- Tx
  constant c_tx_input_use_empty       : boolean := false;  -- assume all words are full with symbols, so no need for empty
  constant c_tx_input_use_channel     : boolean := false;  -- support no multiplexed channels per DP input and only one kind of DP frame length per input, so no need for channel
  constant c_tx_input_use_error       : boolean := false;  -- No support (yet) for error field from usr input
  constant c_tx_input_empty_w         : natural := c_dp_stream_empty_w;  -- actual width will be: true_log2(nof symbols per data)
  constant c_tx_input_channel_w       : natural := c_dp_stream_channel_w;  -- actual width will be: true_log2(nof multiplexed channels per input) + true_log2(c_uth_nof_ch)
  constant c_tx_input_error_w         : natural := c_dp_stream_error_w;  -- actual width will be: e.g. c_unb_error_w
  constant c_tx_input_bsn_w           : natural := c_dp_stream_bsn_w;

  constant c_tx_output_use_fifo       : boolean := false;  -- No need for Tx output FIFO because the SOSI and PHY (tr_nonbonded) serial outputs support passing on data valid
  constant c_tx_output_fifo_fill      : natural := c_uth_packet_len;  -- When used set the FIFO fill level to the size of one UTH packet
  constant c_tx_output_fifo_size      : natural := ceil_div(c_tx_output_fifo_fill + c_fifo_afull_margin, c_bram_m9k_fifo_depth);  -- Add margin for the FIFO size and fully use the BRAM blocks

  -- Rx
  constant c_rx_input_use_fifo        : boolean := false;  -- Note that mms_tr_nonbonded also already contains an clock-domain crossing FIFO for which the size can be set via c_rx_phy_fifo_size.
                                                           -- Furthermore typically Rx does not need slack because frame headers and tails are stripped.
  constant c_rx_input_fifo_fill       : natural := 0;
  constant c_rx_input_fifo_size       : natural := c_bram_m9k_fifo_depth;  -- choose to use full BRAM size = 256 for FIFO depth
begin
    gen_tx : if g_use_tx = true generate
      u_uth_terminal_tx : entity work.uth_terminal_tx
      generic map (
        g_technology            => g_technology,
        -- Terminal IO
        g_nof_input             => g_usr_nof_streams,
        g_nof_output            => g_phy_nof_serial,
        g_mux_mode              => g_tx_mux_mode,
        -- . DP Packet
        g_use_complex           => g_usr_use_complex,
        g_data_w                => g_usr_data_w,
        g_packet_data_w         => g_packet_data_w,
        -- . Uthernet
        g_uth_nof_ch            => c_uth_nof_ch,
        g_uth_typ_ofs           => g_uth_typ_ofs,
        g_uth_typ_arr           => array_init(g_uth_typ_ofs, c_uth_nof_ch, 1),
        -- Input FIFO
        g_input_use_fifo        => g_tx_input_use_fifo,
        g_input_bsn_w           => c_tx_input_bsn_w,
        g_input_empty_w         => c_tx_input_empty_w,
        g_input_channel_w       => c_tx_input_channel_w,
        g_input_error_w         => c_tx_input_error_w,
        g_input_use_bsn         => true,  -- support timing
        g_input_use_empty       => c_tx_input_use_empty,
        g_input_use_channel     => c_tx_input_use_channel,
        g_input_use_error       => c_tx_input_use_error,
        g_input_use_sync        => true,  -- support timing
        g_input_fifo_af_margin  => c_fifo_afull_margin,
        g_input_fifo_fill       => g_tx_input_fifo_fill,
        g_input_fifo_size       => g_tx_input_fifo_size,
        -- Output FIFO
        g_output_use_fifo       => c_tx_output_use_fifo,
        g_output_fifo_af_margin => c_fifo_afull_margin,
        g_output_fifo_fill      => c_tx_output_fifo_fill,
        g_output_fifo_size      => c_tx_output_fifo_size
      )
      port map (
        rst         => dp_rst,
        clk         => dp_clk,
        -- ST sinks
        snk_out_arr => tx_dp_siso_arr,
        snk_in_arr  => tx_dp_sosi_arr,
        -- ST source
        src_in_arr  => tx_uth_siso_arr,
        src_out_arr => tx_uth_sosi_arr
      );
    end generate;

    gen_rx : if g_use_rx = true generate
      u_uth_terminal_rx : entity work.uth_terminal_rx
      generic map (
        g_technology            => g_technology,
        -- Terminal IO
        g_nof_input             => g_phy_nof_serial,
        g_nof_output            => g_usr_nof_streams,
        g_mux_mode              => g_rx_mux_mode,
        -- . DP Packet
        g_data_w                => g_usr_data_w,
        g_packet_data_w         => g_packet_data_w,
        -- . Uthernet
        g_uth_nof_ch            => c_uth_nof_ch,
        g_uth_len_max           => g_uth_len_max,
        g_uth_typ_arr           => array_init(g_uth_typ_ofs, c_uth_nof_ch, 1),
        g_uth_len_arr           => array_init(c_dp_packet_len, c_uth_nof_ch),
        g_use_uth_err           => c_use_uth_err,
        g_uth_err_bi            => c_uth_err_bi,
        g_uth_timeout_w         => g_rx_timeout_w,
        -- Input FIFO
        g_input_use_fifo        => c_rx_input_use_fifo,
        g_input_fifo_af_margin  => c_fifo_afull_margin,
        g_input_fifo_fill       => c_rx_input_fifo_fill,
        g_input_fifo_size       => c_rx_input_fifo_size,
        -- Output FIFO
        g_output_use_fifo       => g_rx_output_use_fifo,
        g_output_fifo_af_margin => c_fifo_afull_margin,
        g_output_fifo_fill      => g_rx_output_fifo_fill,
        g_output_fifo_size      => g_rx_output_fifo_size
      )
      port map (
        rst               => dp_rst,
        clk               => dp_clk,
        -- ST sinks
        snk_out_arr       => rx_uth_siso_arr,
        snk_in_arr        => rx_uth_sosi_arr,
        -- ST source
        src_in_arr        => rx_dp_siso_arr,
        src_out_arr       => rx_dp_sosi_arr,
        -- Monitor outputs
        mon_pkt_sosi_arr  => rx_mon_pkt_sosi_arr,
        mon_dist_sosi_arr => rx_mon_dist_sosi_arr
      );
    end generate;
end str;
