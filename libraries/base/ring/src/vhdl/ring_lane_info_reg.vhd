-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose:
-- . Ring lane info register
-- Description:
-- See ring_pkg.vhd
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.ring_pkg.all;

entity ring_lane_info_reg is
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock

    dp_clk     : in  std_logic;
    dp_rst     : in  std_logic;

    reg_mosi : in  t_mem_mosi;
    reg_miso : out t_mem_miso;

    -- sdp info
    lane_info_ro : in  t_ring_lane_info;  -- ro = read only
    lane_info    : out t_ring_lane_info
  );
end ring_lane_info_reg;

architecture str of ring_lane_info_reg is
  signal mm_fields_in  : std_logic_vector(field_slv_in_len(c_ring_lane_info_field_arr) - 1 downto 0);
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_ring_lane_info_field_arr) - 1 downto 0);

  signal lane_info_rd : t_ring_lane_info;
  signal lane_info_wr : t_ring_lane_info;
begin
  lane_info    <= lane_info_rd;

  p_lane_info_rd : process(lane_info_wr, lane_info_ro)
  begin
    -- default write assign all fields
    lane_info_rd <= lane_info_wr;

    -- overrule the read only fields
    lane_info_rd.lane_direction <= lane_info_ro.lane_direction;
  end process;

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_use_slv_in_val  => false,  -- use FALSE to save logic when always slv_in_val='1'
    g_field_arr       => c_ring_lane_info_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_mosi,
    mm_miso    => reg_miso,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_in,
    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  -- add "RO" fields to mm_fields
  mm_fields_in(field_hi(c_ring_lane_info_field_arr, "lane_direction") downto field_lo(c_ring_lane_info_field_arr, "lane_direction")) <= slv(lane_info_rd.lane_direction);

  -- get "RW" fields from mm_fields
  lane_info_wr.transport_nof_hops <= mm_fields_out(field_hi(c_ring_lane_info_field_arr, "transport_nof_hops") downto field_lo(c_ring_lane_info_field_arr, "transport_nof_hops"));
end str;
