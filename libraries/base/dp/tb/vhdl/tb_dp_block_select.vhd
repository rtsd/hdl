-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 26 Apr 2018
-- Purpose:
-- . Test bench for dp_block_resize.
-- Description:
--   Block diagram:
--
--    stimuli --> dp_block_resize -- > verify
--
-- Remark:
-- . Pipelining and flow control are already covered by tb_dp_pipeline.vhd.
--
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_select is
  generic (
    g_dut_pipeline         : natural := 1;
    g_nof_blocks_per_sync  : natural := 5;
    g_index_lo             : natural := 0;
    g_index_hi             : natural := 3
  );
end tb_dp_block_select;

architecture tb of tb_dp_block_select is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period  : time := 5 ns;

  constant c_nof_sync                 : natural :=  5;
  constant c_gap_size                 : natural :=  4;
  constant c_nof_data_per_blk         : natural :=  9;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal tb_end         : std_logic := '0';

  signal stimuli_end         : std_logic;
  signal stimuli_sosi        : t_dp_sosi;
  signal stimuli_siso        : t_dp_siso;
  signal stimuli_blk_cnt_reg : natural;
  signal stimuli_blk_cnt     : natural;
  signal verify_sosi         : t_dp_sosi;
  signal verify_siso         : t_dp_siso := c_dp_siso_rdy;
  signal reference_blk_cnt   : natural;
  signal reference_sosi      : t_dp_sosi;
  signal reference_siso      : t_dp_siso := c_dp_siso_rdy;
  signal sync_sosi           : t_dp_sosi;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => g_nof_blocks_per_sync * c_nof_sync,
    g_pkt_len     => c_nof_data_per_blk,
    g_pkt_gap     => c_gap_size
  )
  port map (
    rst               => rst,
    clk               => clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_select : entity work.dp_block_select
  generic map (
    g_pipeline            => g_dut_pipeline,
    g_nof_blocks_per_sync => g_nof_blocks_per_sync,
    g_index_lo            => g_index_lo,
    g_index_hi            => g_index_hi
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- Control
    index_lo     => g_index_lo,
    index_hi     => g_index_hi,
    -- ST sink
    snk_out      => stimuli_siso,
    snk_in       => stimuli_sosi,
    -- ST source
    src_in       => verify_siso,
    src_out      => verify_sosi
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => g_dut_pipeline
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => reference_siso,
    src_out     => reference_sosi
  );

  stimuli_blk_cnt_reg <= stimuli_blk_cnt when rising_edge(clk);
  stimuli_blk_cnt <= 0                       when stimuli_sosi.sync = '1' else
                     stimuli_blk_cnt_reg + 1 when stimuli_sosi.sop = '1' else
                     stimuli_blk_cnt_reg;

  -- Only support g_dut_pipeline = 0 or 1
  reference_blk_cnt <= stimuli_blk_cnt when g_dut_pipeline = 0 else
                       stimuli_blk_cnt when rising_edge(clk);

  -- Keep BSN at sync
  sync_sosi <= reference_sosi when  rising_edge(clk) and reference_sosi.sync = '1';

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if reference_sosi.valid = '1' then
        if reference_blk_cnt >= g_index_lo and reference_blk_cnt <= g_index_hi and reference_blk_cnt < g_nof_blocks_per_sync then
          ---------------------------------------------------------------------
          -- Selected blocks
          ---------------------------------------------------------------------
          if g_index_lo = 0 then
            -- Direct sosi verification because sync and BSN are not moved when g_index_lo=0
            assert verify_sosi = reference_sosi
              report "Wrong selected subsequent blocks"
              severity ERROR;
          else
            -- Account for that sync and BSN of first block are moved to g_index_lo>0
            if reference_blk_cnt = g_index_lo then
              -- First block
              -- . verify moved sync and bsn
              if reference_sosi.sop = '1' then
                assert verify_sosi.sync = sync_sosi.sync
                  report "Missing selected first block sync"
                  severity ERROR;
                assert verify_sosi.bsn = sync_sosi.bsn
                  report "Wrong selected first block bsn"
                  severity ERROR;
              else
                assert verify_sosi.sync = '0'
                  report "Unexpected selected first block sync"
                  severity ERROR;
              end if;
              -- . verify other sosi fields
              assert verify_sosi.data = reference_sosi.data
                report "Wrong selected first block data"
                severity ERROR;
              assert verify_sosi.re = reference_sosi.re
                report "Wrong selected first block re"
                severity ERROR;
              assert verify_sosi.im = reference_sosi.im
                report "Wrong selected first block im"
                severity ERROR;
              assert verify_sosi.channel = reference_sosi.channel
                report "Wrong selected first block channel"
                severity ERROR;
              assert verify_sosi.empty = reference_sosi.empty
                report "Wrong selected first block empty"
                severity ERROR;
              assert verify_sosi.valid = reference_sosi.valid
                report "Wrong selected first block valid"
                severity ERROR;
              assert verify_sosi.sop = reference_sosi.sop
                report "Wrong selected first block sop"
                severity ERROR;
              assert verify_sosi.eop = reference_sosi.eop
                report "Wrong selected first block eop"
                severity ERROR;
            else
              -- Subsequent blocks
              assert verify_sosi = reference_sosi
                report "Wrong selected subsequent blocks"
                severity ERROR;
            end if;
          end if;
        else
          ---------------------------------------------------------------------
          -- Skipped blocks
          ---------------------------------------------------------------------
          assert verify_sosi.sync = '0'
            report "Wrong skipped sync"
            severity ERROR;
          assert verify_sosi.valid = '0'
            report "Wrong skipped valid"
            severity ERROR;
          assert verify_sosi.sop = '0'
            report "Wrong skipped sop"
            severity ERROR;
          assert verify_sosi.eop = '0'
            report "Wrong skipped eop"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  tb_end <= '0', stimuli_end after (1 + 10 * g_dut_pipeline) * c_clk_period;
end tb;
