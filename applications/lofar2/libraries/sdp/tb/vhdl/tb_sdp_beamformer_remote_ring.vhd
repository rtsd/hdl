-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
-- . Test bench for multiple sdp_beamformer_remote.vhd + ring_lane.vhd +
--   tr_10GbE in a ring
-- Description:
-- . https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Beamformer
--
-- . This tb is inspired by tb_lofar2_unb2c_sdp_station_bf_ring.vhd, however
--   here the purpose is to simulate the memory usage of the circular buffer
--   in the bsn_aligner_v2 at each node.
--
-- . Block diagram:
--   * tb can use one instance of tr_10Gbe to model Rx from ring and Tx to ring.
--   * Ring lane serial links for ring nodes RN = 0 to c_last_rn:
--
--     tr_10gbe_ring_serial_tx_arr --> tr_10gbe_ring_serial_rx_arr after c_cable_delay
--
--         /<-------------------------------------------------------------\
--         \---> 0 ---> RN - 1  --->  RN  --->  RN + 1 ---> c_last_rn --->/
--                                    |^
--    tr_10gbe_ring_serial_tx_arr(RN) || tr_10gbe_ring_serial_tx_arr(RN)
--                                    v|
--                                  tr_10Gbe
--                                    |^
--      tr_10gbe_ring_rx_sosi_arr(RN) || tr_10gbe_ring_tx_sosi_arr(RN)
--                                    v|
--                                 ring_lane
--                                    |^
--               from_ri_sosi_arr(RN) || to_ri_sosi_arr(RN)
--                                    v|
--        local_bf_sosi --> sdp_beamformer_remote --> bf_sum_sosi_arr(RN)
--                                                    bf_sum_sosi
--   * BSN monitors:
--                                            RN
--                                            |^
--              ring_lane/ring_rx             || ring_lane/ring_tx
--              FPGA_bf_ring_rx_latency_R(RN) || FPGA_bf_ring_tx_latency_R(RN)
--                                            ||
--      dp_bsn_align_v2 P_sum = 2 inputs      ||
--      FPGA_bf_rx_align_latency_R(RN)(P_sum) ||
--                                            ||
--           dp_bsn_align_v2 aligned output   ||
--              FPGA_bf_aligned_latency_R(RN) v|
--
-- . BF latency results from SDP-ARTS HW
--   - with 16 ring nodes (GN = 64 is RN = 0)
--   - 2024-03-02T21.16.33_d601da896_lofar2_unb2b_sdp_station_full_wg
--
--   Node:  bf_ring_rx    bf_rx_align        bf_aligned      bf_ring_tx
--          _latency:     _latency:          _latency:       _latency:
--   64:    -1            ( 1    -1    )     2053            3114
--   65:    4898          ( 1    3880  )     4101            5162
--   66:    6949          ( 1    5916  )     6149            7210
--   67:    8998          ( 1    7960  )     8197            9258
--   68:    11048         ( 1    10003 )     10245           11306
--   69:    13093         ( 1    12063 )     12293           13354
--   70:    15135         ( 1    14105 )     14341           15402
--   71:    17174         ( 1    16154 )     16389           17450
--   72:    19261         ( 1    18229 )     18437           19498
--   73:    21288         ( 1    20261 )     20485           21546
--   74:    23319         ( 1    22292 )     22533           23594
--   75:    25367         ( 1    24359 )     24581           25642
--   76:    27448         ( 1    26417 )     26629           27690
--   77:    29471         ( 1    28453 )     28677           29738
--   78:    31512         ( 1    30481 )     30725           31786
--   79:    33567         ( 1    32537 )     32773           -1
--
--   Simulation latency results with this tb
--
--   # c_cable_delay = 0 * 6.4 ns (c_bsn_latency_first_node = 2)
--   #
--   # Node:  bf_ring_rx   bf_rx_align       bf_aligned     bf_ring_tx
--   #        _latency:    _latency:         _latency:      _latency:
--   # 0:     -1           ( 1   0 )         2053           2075
--   # 1:     3824         ( 1   3837 )      4101           4123
--   # 2:     5876         ( 1   5889 )      6149           6171
--   # 3:     7926         ( 1   7939 )      8197           8219
--   # 4:     9977         ( 1   9990 )      10245          10267
--   # 5:     12029        ( 1   12042 )     12293          12315
--   # 6:     14079        ( 1   14092 )     14341          14363
--   # 7:     16108        ( 1   16121 )     16389          16411
--   # 8:     18159        ( 1   18172 )     18437          18459
--   # 9:     20211        ( 1   20224 )     20485          20507
--   # 10:    22261        ( 1   22274 )     22533          22555
--   # 11:    24312        ( 1   24325 )     24581          24603
--   # 12:    26363        ( 1   26376 )     26629          26651
--   # 13:    28414        ( 1   28427 )     28677          28699
--   # 14:    30465        ( 1   30478 )     30725          30747
--   # 15:    32493        ( 1   32506 )     32773          -1
--
--   # c_cable_delay = 30 * 6.4 ns (c_bsn_latency_first_node = 2)
--   #
--   # Node:  bf_ring_rx   bf_rx_align       bf_aligned     bf_ring_tx
--   #        _latency:    _latency:         _latency:      _latency:
--   # 0:     -1           ( 1    0 )        2053           2075
--   # 1:     3862         ( 1    3875 )     4101           4123
--   # 2:     5914         ( 1    5927 )     6149           6171
--   # 3:     7965         ( 1    7978 )     8197           8219
--   # 4:     10015        ( 1    10028 )    10245          10267
--   # 5:     12067        ( 1    12080 )    12293          12315
--   # 6:     14118        ( 1    14131 )    14341          14363
--   # 7:     16146        ( 1    16159 )    16389          16411
--   # 8:     18197        ( 1    18210 )    18437          18459
--   # 9:     20249        ( 1    20262 )    20485          20507
--   # 10:    22299        ( 1    22312 )    22533          22555
--   # 11:    24350        ( 1    24363 )    24581          24603
--   # 12:    26402        ( 1    26415 )    26629          26651
--   # 13:    28452        ( 1    28465 )    28677          28699
--   # 14:    30503        ( 1    30516 )    30725          30747
--   # 15:    32532        ( 1    32545 )    32773          -1
--
--   # c_cable_delay = 30 * 6.4 ns (c_bsn_latency_first_node = 1)
--   #
--   # Node:  bf_ring_rx   bf_rx_align       bf_aligned      bf_ring_tx
--   #        _latency:    _latency:         _latency:       _latency:
--   # 0:     -1           ( 1    0 )        1029            1051
--   # 1:     2837         ( 1    2850 )     3077            3099
--   # 2:     4888         ( 1    4901 )     5125            5147
--   # 3:     6939         ( 1    6952 )     7173            7195
--   # 4:     8990         ( 1    9003 )     9221            9243
--   # 5:     11041        ( 1    11054 )    11269           11291
--   # 6:     13092        ( 1    13105 )    13317           13339
--   # 7:     15143        ( 1    15156 )    15365           15387
--   # 8:     17172        ( 1    17185 )    17413           17435
--   # 9:     19222        ( 1    19235 )    19461           19483
--   # 10:    21274        ( 1    21287 )    21509           21531
--   # 11:    23325        ( 1    23338 )    23557           23579
--   # 12:    25375        ( 1    25388 )    25605           25627
--   # 13:    27427        ( 1    27440 )    27653           27675
--   # 14:    29478        ( 1    29491 )    29701           29723
--   # 15:    31507        ( 1    31520 )    31749           -1
--
--   - The dp_bsn_align_v2 BSN latency monitor results agree between sim an HW.
--   - The bf_aligned_latency is exactly equal in sim and on HW, because the
--     mmp_dp_bsn_align_v2 uses the ref_sync for the BSN monitor and also to
--     release its BSN aligned output, so the latency only depends on internal
--     FW buffering and latency.
--   . The bf_aligned_latency and bf_ring_tx_latency do not depend on cable
--     delays and are constant when read again in sim or on HW, because they
--     only depend on fixed internal FW buffering and latency.
--   - The ring_lane BSN latency monitor results differ between sim and HW, it
--     is unclear why:
--     . the ring_rx and ring_tx BSN latency monitor results are about one
--       block of 1024 larger on HW.
--     . on the same HW node, the bf_ring_rx_latency is about one block of 1024
--       larger than the bf_rx_align_latency, even though they are taken at
--       nearly the same place in the ring_rx signal path.
--     . on the same HW node, the bf_ring_tx_latency is about one block of 1024
--       larger than the bf_align_latency, even though they are taken at nearly
--       the same place in the tx signal path.
--     . the ring_rx and ring_tx BSN latency monitor results for XST do not
--       show a one block is 1024 offset.
--     TODO:
--     . Assume the ring_lane latencies are one block is 1024 too high, and
--       assume that the bf_rx_align_latency is correct and reflects the actual
--       packet latency.
--     . The ring_rx and ring_tx both use func_ring_nof_hops_to_source_rn() and
--       hops = sosi.channel to get monitor_sosi, maybe there occurs an offset
--       there.
--     . The ring_rx and ring_tx both use dp_demux.vhd, maybe that causes a one
--       block is 1024 shift in sosi.sync.
--
-- Usage:
-- > as 3 or more
-- > add wave -position insertpoint sim:/tb_sdp_beamformer_remote_ring/bf_sum_sosi_arr
-- > run -a
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, ring_lib, tr_10GbE_lib, tech_pll_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;
use ring_lib.ring_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use work.sdp_pkg.all;
use work.tb_sdp_pkg.all;

entity tb_sdp_beamformer_remote_ring is
  generic (
    g_nof_rn    : natural := 4;  -- number of nodes in the ring
    g_nof_sync  : natural := 2
  );
end tb_sdp_beamformer_remote_ring;

architecture tb of tb_sdp_beamformer_remote_ring is
  constant c_dp_clk_period : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period : time := 1 ns;  -- fast MM clk to speed up simulation
  constant c_sa_clk_period : time := tech_pll_clk_644_period;  -- 644MHz

  -- Apply cable delay in tech_pll_clk_156_period units, to remain aligned with tr_10GbE sim model
  -- . Choose c_cable_delay = 30 * 6.4 ~= 192 ns ~= 38 dp_clk of 5 ns, to match delay seen on HW
  -- . Maximum c_cable_delay <= 186 * 6.4 = 1210 ns ~= 242 dp_clk of 5 ns in simulation with
  --   g_nof_rn = 16. For larger c_cable_delay the bf_sum_sosi.data goes wrong. The maximum
  --   c_cable_delay depends a little bit on g_nof_rn, for g_nof_rn = 2 the data goes wrong when
  --   c_cable_delay >= 190.
  constant c_clk_156_period  : time := tech_pll_clk_156_period;  -- 6.400020 ns ~= 156.25 MHz
  constant c_nof_delay       : natural := 30; --286;
  constant c_cable_delay     : time := c_clk_156_period * c_nof_delay;

  -- BF data
  constant c_block_period              : natural := c_sdp_N_fft;
  constant c_block_size                : natural := c_sdp_S_sub_bf * c_sdp_N_pol_bf;
  constant c_gap_size                  : natural := c_block_period - c_block_size;
  -- choose sync interval somewhat longer than maximum BF ring latency
  constant c_nof_blocks_per_sync       : natural := largest(10, (g_nof_rn + 1) * 2);
  constant c_local_bf_re               : integer := 1;
  constant c_local_bf_im               : integer := 2;

  -- Ring lane packets
  constant c_last_rn                   : natural := g_nof_rn - 1;  -- first ring node has index RN = 0 by definition.
  constant c_use_cable                 : std_logic := '1';  -- '0' ring via PCB traces, '1' ring via QSFP cables
  constant c_lane_payload_nof_longwords_bf  : natural := (c_block_size * 9) / 16;  -- beamlet block size repacked
                                              -- from 36b to 64b (9/16 = 36/64), 488 * 2 * 9 / 16 = 549 longwords
  constant c_lane_packet_nof_longwords_max  : natural := c_lane_payload_nof_longwords_bf + c_ring_dp_hdr_field_size;
                                              -- = 549 + 3 = 552
  constant c_fifo_tx_fill_margin       : natural := 10;  -- >= c_fifo_fill_margin = 6 that is used in dp_fifo_fill_eop
  constant c_fifo_tx_size_ring : natural := true_log_pow2(c_lane_packet_nof_longwords_max + c_fifo_tx_fill_margin);
                                            -- = 552 + 6 --> 1024
  constant c_fifo_tx_fill_ring : natural := c_fifo_tx_size_ring - c_fifo_tx_fill_margin;
                                            -- = maximum fill level, so rely on eop
  constant c_err_bi                    : natural := 0;
  constant c_nof_err_counts            : natural := 8;
  constant c_bsn_at_sync_check_channel : natural := 1;
  constant c_validate_channel          : boolean := true;
  constant c_validate_channel_mode     : string  := "=";
  constant c_sync_timeout              : natural := c_block_period * (c_nof_blocks_per_sync + 1);

  -- Timeout tb if there is no output bf_sum_sosi
  constant c_tb_timeout                : time := (g_nof_sync + 1) * c_sync_timeout * c_dp_clk_period;

  -- Address widths of a single MM instance
  constant c_addr_w_reg_ring_lane_info_bf          : natural := 1;

  signal mm_init                : std_logic := '1';
  signal tb_end                 : std_logic := '0';
  signal dp_clk                 : std_logic := '1';
  signal dp_rst                 : std_logic;
  signal mm_clk                 : std_logic := '1';
  signal mm_rst                 : std_logic;
  signal SA_CLK                 : std_logic := '1';
  signal tr_ref_clk_312         : std_logic := '0';
  signal tr_ref_clk_156         : std_logic := '0';
  signal tr_ref_rst_156         : std_logic := '0';

  signal stimuli_rst            : std_logic;
  signal stimuli_end            : std_logic;

  signal stimuli_sosi           : t_dp_sosi;
  signal local_bf_sosi          : t_dp_sosi;
  signal bf_bs_sosi             : t_dp_sosi;
  signal from_ri_sosi_arr       : t_dp_sosi_arr(c_last_rn downto 0);
  signal to_ri_sosi_arr         : t_dp_sosi_arr(c_last_rn downto 0);
  signal bf_sum_sosi_arr        : t_dp_sosi_arr(c_last_rn downto 0);
  signal bf_sum_sosi            : t_dp_sosi;

  -- 10GbE ring
  signal tr_10gbe_ring_rx_sosi_arr    : t_dp_sosi_arr(c_last_rn downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_ring_tx_sosi_arr    : t_dp_sosi_arr(c_last_rn downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_ring_serial_rx_arr  : std_logic_vector(c_last_rn downto 0) := (others => '0');
  signal tr_10gbe_ring_serial_tx_arr  : std_logic_vector(c_last_rn downto 0) := (others => '0');

  -- BF ring MM registers
  signal reg_ring_lane_info_bf_copi_arr         : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_ring_lane_info_bf_cipo_arr         : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_ring_lane_info_bf_copi             : t_mem_copi := c_mem_copi_rst;
  signal reg_ring_lane_info_bf_cipo             : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_ring_rx_bf_copi_arr : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_rx_bf_cipo_arr : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_ring_rx_bf_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_ring_rx_bf_cipo     : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_ring_tx_bf_copi_arr : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_tx_bf_cipo_arr : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_ring_tx_bf_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_ring_tx_bf_cipo     : t_mem_cipo := c_mem_cipo_rst;
  signal reg_dp_block_validate_err_bf_copi_arr  : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_dp_block_validate_err_bf_cipo_arr  : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_dp_block_validate_err_bf_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_block_validate_err_bf_cipo      : t_mem_cipo := c_mem_cipo_rst;
  signal reg_dp_block_validate_bsn_at_sync_bf_copi_arr : t_mem_copi_arr(c_last_rn downto 0) :=
                                                         (others => c_mem_copi_rst);
  signal reg_dp_block_validate_bsn_at_sync_bf_cipo_arr : t_mem_cipo_arr(c_last_rn downto 0) :=
                                                         (others => c_mem_cipo_rst);
  signal reg_dp_block_validate_bsn_at_sync_bf_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_block_validate_bsn_at_sync_bf_cipo     : t_mem_cipo := c_mem_cipo_rst;
  -- BF ring MM points
  signal FPGA_bf_ring_nof_transport_hops_R       : t_natural_arr(c_last_rn downto 0);
  signal FPGA_bf_ring_rx_latency_R               : t_integer_arr(c_last_rn downto 0);
  signal FPGA_bf_ring_tx_latency_R               : t_integer_arr(c_last_rn downto 0);

  -- BSN aligner MM registers
  signal reg_bsn_align_v2_bf_copi_arr            : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_align_v2_bf_cipo_arr            : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_align_v2_bf_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_align_v2_bf_cipo                : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_bf_rx_align_copi_arr : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_bf_rx_align_cipo_arr : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_bf_rx_align_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_bf_rx_align_cipo     : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_bf_aligned_copi_arr  : t_mem_copi_arr(c_last_rn downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_bf_aligned_cipo_arr  : t_mem_cipo_arr(c_last_rn downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_bf_aligned_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_bf_aligned_cipo      : t_mem_cipo := c_mem_cipo_rst;
  -- BSN aligner Monitor Points
  signal FPGA_bf_rx_align_latency_R               : t_integer_2arr_2(c_last_rn downto 0);  -- c_sdp_P_sum = 2
  signal FPGA_bf_aligned_latency_R                : t_integer_arr(c_last_rn downto 0);
begin
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  -- Wait for tr_10GbE to be active
  stimuli_rst <= '1', '0' after 15 us;

  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)

  -- Generate local BF stream, use same for all nodes
  u_stimuli : entity dp_lib.dp_stream_stimuli
  generic map (
    g_sync_period => c_nof_blocks_per_sync,
    g_nof_repeat  => c_nof_blocks_per_sync * g_nof_sync,
    g_pkt_len     => c_block_size,
    g_pkt_gap     => c_gap_size
  )
  port map (
    rst               => stimuli_rst,
    clk               => dp_clk,
    -- Generate stimuli
    src_out           => stimuli_sosi,
    -- End of stimuli
    tb_end            => stimuli_end
  );

  -- Use constant beamlet data to ease verification of (intermediate) beamlet sums at each node
  p_local_bf_sosi : process(stimuli_sosi)
  begin
    local_bf_sosi <= stimuli_sosi;
    local_bf_sosi.data <= TO_DP_SDATA(0);
    local_bf_sosi.re <= TO_DP_DSP_DATA(c_local_bf_re);
    local_bf_sosi.im <= TO_DP_DSP_DATA(c_local_bf_im);
    local_bf_sosi.channel <= TO_DP_CHANNEL(0);
    local_bf_sosi.err <= TO_DP_ERROR(0);
  end process;

  bf_bs_sosi <= local_bf_sosi;
  bf_sum_sosi <= bf_sum_sosi_arr(c_last_rn);

  p_mm : process
    variable v_span               : natural;
    variable v_span_node          : natural;
    variable v_offset             : natural;
    variable v_transport_nof_hops : natural;
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_dp_clk_period,
                                                c_common_cross_clock_domain_latency * 2);
    ---------------------------------------------------------------------------
    -- Setup transport nof hops for RN = 0:15 to [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]
    ---------------------------------------------------------------------------
    -- Write FPGA_bf_ring_nof_transport_hops_RW = ring_lane_info.transport_nof_hops
    v_span := 2**c_addr_w_reg_ring_lane_info_bf;
    for RN in 0 to c_last_rn loop
      v_offset := 1 + RN * v_span;
      v_transport_nof_hops := 1;
      if RN = c_last_rn then
        v_transport_nof_hops := 0;
      end if;
      proc_mem_mm_bus_wr(v_offset, v_transport_nof_hops, mm_clk,
                         reg_ring_lane_info_bf_cipo, reg_ring_lane_info_bf_copi);
    end loop;
    proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_dp_clk_period,
                                                c_common_cross_clock_domain_latency * 2);
    -- Readback FPGA_bf_ring_nof_transport_hops_R
    for RN in 0 to c_last_rn loop
      v_offset := 1 + RN * v_span;
      proc_mem_mm_bus_rd(v_offset, mm_clk, reg_ring_lane_info_bf_cipo, reg_ring_lane_info_bf_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      FPGA_bf_ring_nof_transport_hops_R(RN) <= TO_UINT(reg_ring_lane_info_bf_cipo.rddata(c_word_w - 1 downto 0));
    end loop;

    ---------------------------------------------------------------------------
    -- Wait until second bf_sum_sosi.sync
    ---------------------------------------------------------------------------
    proc_common_wait_until_hi_lo(dp_clk, bf_sum_sosi.sync);
    proc_common_wait_until_hi_lo(dp_clk, bf_sum_sosi.sync);

    ---------------------------------------------------------------------------
    -- Read BSN monitors
    ---------------------------------------------------------------------------
    v_span := 2**c_sdp_reg_bsn_monitor_v2_addr_w;
    -- Read FPGA_bf_ring_rx_latency_R
    for RN in 0 to c_last_rn loop
      v_offset := 6 + RN * v_span;
      proc_mem_mm_bus_rd(v_offset, mm_clk, reg_bsn_monitor_v2_ring_rx_bf_cipo, reg_bsn_monitor_v2_ring_rx_bf_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      FPGA_bf_ring_rx_latency_R(RN) <= TO_SINT(reg_bsn_monitor_v2_ring_rx_bf_cipo.rddata(c_word_w - 1 downto 0));
    end loop;
    -- Read FPGA_bf_rx_align_latency_R, for both c_sdp_P_sum = 2 inputs per RN
    v_span_node := true_log_pow2(c_sdp_P_sum) * v_span;
    for RN in 0 to c_last_rn loop
      for P in 0 to c_sdp_P_sum - 1 loop
        v_offset := 6 + RN * v_span_node + P * v_span;
        proc_mem_mm_bus_rd(v_offset, mm_clk, reg_bsn_monitor_v2_bf_rx_align_cipo, reg_bsn_monitor_v2_bf_rx_align_copi);
        proc_mem_mm_bus_rd_latency(1, mm_clk);
        FPGA_bf_rx_align_latency_R(RN)(P) <= TO_SINT(reg_bsn_monitor_v2_bf_rx_align_cipo.rddata(c_word_w - 1 downto 0));
      end loop;
    end loop;
    -- Read FPGA_bf_aligned_latency_R
    for RN in 0 to c_last_rn loop
      v_offset := 6 + RN * v_span;
      proc_mem_mm_bus_rd(v_offset, mm_clk, reg_bsn_monitor_v2_bf_aligned_cipo, reg_bsn_monitor_v2_bf_aligned_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      FPGA_bf_aligned_latency_R(RN) <= TO_SINT(reg_bsn_monitor_v2_bf_aligned_cipo.rddata(c_word_w - 1 downto 0));
    end loop;
    -- Read FPGA_bf_ring_tx_latency_R
    for RN in 0 to c_last_rn loop
      v_offset := 6 + RN * v_span;
      proc_mem_mm_bus_rd(v_offset, mm_clk, reg_bsn_monitor_v2_ring_tx_bf_cipo, reg_bsn_monitor_v2_ring_tx_bf_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      FPGA_bf_ring_tx_latency_R(RN) <= TO_SINT(reg_bsn_monitor_v2_ring_tx_bf_cipo.rddata(c_word_w - 1 downto 0));
    end loop;

    ---------------------------------------------------------------------------
    -- Wait until end of simulation
    ---------------------------------------------------------------------------
    mm_init <= '0';

    proc_common_wait_until_high(dp_clk, stimuli_end);
    proc_common_wait_some_cycles(dp_clk, 1000);

    ---------------------------------------------------------------------------
    -- Print latency results
    ---------------------------------------------------------------------------
    print_str("c_cable_delay = " & int_to_str(c_nof_delay) & " * 6.4 ns");
    print_str("");
    print_str("Node:  bf_ring_rx    bf_rx_align          bf_aligned      bf_ring_tx");
    print_str("       _latency:     _latency:            _latency:       _latency:");
    for RN in 0 to c_last_rn loop
       print_str(int_to_str(RN) & ":     " &
                 int_to_str(FPGA_bf_ring_rx_latency_R(RN)) & "          ( " &
                 int_to_str(FPGA_bf_rx_align_latency_R(RN)(0)) & "    " &
                 int_to_str(FPGA_bf_rx_align_latency_R(RN)(1)) & " )        " &
                 int_to_str(FPGA_bf_aligned_latency_R(RN)) & "            " &
                 int_to_str(FPGA_bf_ring_tx_latency_R(RN)));
    end loop;

    tb_end <= '1';
    wait;
  end process;

  -- End the tb simulation
  proc_common_timeout_failure(c_tb_timeout, tb_end);  -- ERROR: end simulation if it fails to end in time
  proc_common_stop_simulation(tb_end);  -- OK: end simulation

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  gen_dut : for RN in 0 to c_last_rn generate
    -- Connect ring wires between the nodes
    wire_ring : if RN > 0 generate
      tr_10gbe_ring_serial_rx_arr(RN) <= transport tr_10gbe_ring_serial_tx_arr(RN - 1) after c_cable_delay;
    end generate;
    close_ring : if RN = 0 generate
      tr_10gbe_ring_serial_rx_arr(0) <= transport tr_10gbe_ring_serial_tx_arr(c_last_rn) after c_cable_delay;
    end generate;

    -- tr_10GbE access at each node, all via front_io QSFP[0]
    u_tr_10GbE_ring: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => true,
      g_sim_level     => 1,
      g_nof_macs      => 1,
      g_direction     => "TX_RX",
      g_tx_fifo_fill  => c_fifo_tx_fill_ring,
      g_tx_fifo_size  => c_fifo_tx_size_ring
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644        => SA_CLK,
      tr_ref_clk_312        => tr_ref_clk_312,
      tr_ref_clk_156        => tr_ref_clk_156,
      tr_ref_rst_156        => tr_ref_rst_156,

      -- MM interface
      mm_rst                => mm_rst,
      mm_clk                => mm_clk,

      reg_mac_mosi          => c_mem_copi_rst,
      reg_mac_miso          => open,
      reg_eth10g_mosi       => c_mem_copi_rst,
      reg_eth10g_miso       => open,

      -- DP interface
      dp_rst                => dp_rst,
      dp_clk                => dp_clk,

      src_out_arr           => tr_10gbe_ring_rx_sosi_arr(RN downto RN),
      snk_in_arr            => tr_10gbe_ring_tx_sosi_arr(RN downto RN),

      -- Serial IO
      serial_tx_arr         => tr_10gbe_ring_serial_tx_arr(RN downto RN),
      serial_rx_arr         => tr_10gbe_ring_serial_rx_arr(RN downto RN)
    );

    -- Ring lane access at each node
    u_ring_lane_bf : entity ring_lib.ring_lane
      generic map (
        g_lane_direction            => 1,  -- transport in positive RN direction.
        g_lane_data_w               => c_longword_w,
        g_lane_packet_length        => c_lane_payload_nof_longwords_bf,
        g_lane_total_nof_packets_w  => 32,
        g_use_dp_layer              => true,
        g_nof_rx_monitors           => 1,
        g_nof_tx_monitors           => 1,
        g_err_bi                    => c_err_bi,
        g_nof_err_counts            => c_nof_err_counts,
        g_bsn_at_sync_check_channel => c_bsn_at_sync_check_channel,
        g_validate_channel          => c_validate_channel,
        g_validate_channel_mode     => c_validate_channel_mode,
        g_sync_timeout              => c_sync_timeout
      )
      port map (
        mm_rst => mm_rst,
        mm_clk => mm_clk,
        dp_clk => dp_clk,
        dp_rst => dp_rst,

        from_lane_sosi     => from_ri_sosi_arr(RN),
        to_lane_sosi       => to_ri_sosi_arr(RN),
        lane_rx_cable_sosi => tr_10gbe_ring_rx_sosi_arr(RN),
        lane_rx_board_sosi => c_dp_sosi_rst,
        lane_tx_cable_sosi => tr_10gbe_ring_tx_sosi_arr(RN),
        lane_tx_board_sosi => open,
        bs_sosi            => bf_bs_sosi,  -- used for bsn and sync

        reg_ring_lane_info_copi                => reg_ring_lane_info_bf_copi_arr(RN),
        reg_ring_lane_info_cipo                => reg_ring_lane_info_bf_cipo_arr(RN),
        reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_bf_copi_arr(RN),
        reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_bf_cipo_arr(RN),
        reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_bf_copi_arr(RN),
        reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_bf_cipo_arr(RN),
        reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_bf_copi_arr(RN),
        reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_bf_cipo_arr(RN),
        reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_bf_copi_arr(RN),
        reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_bf_cipo_arr(RN),

        this_rn   => to_uvec(RN, c_byte_w),
        N_rn      => to_uvec(g_nof_rn, c_byte_w),
        rx_select => c_use_cable,
        tx_select => c_use_cable
      );

    -- Intermediate BF alignment and summation at each node
    u_sdp_beamformer_remote : entity work.sdp_beamformer_remote
      generic map (
        g_nof_aligners_max  => g_nof_rn
      )
      port map (
        dp_clk        => dp_clk,
        dp_rst        => dp_rst,

        rn_index      => RN,

        local_bf_sosi => local_bf_sosi,  -- all nodes use same local reference data
        from_ri_sosi  => from_ri_sosi_arr(RN),
        to_ri_sosi    => to_ri_sosi_arr(RN),
        bf_sum_sosi   => bf_sum_sosi_arr(RN),

        mm_rst        => mm_rst,
        mm_clk        => mm_clk,

        reg_bsn_align_copi                       => reg_bsn_align_v2_bf_copi_arr(RN),
        reg_bsn_align_cipo                       => reg_bsn_align_v2_bf_cipo_arr(RN),
        reg_bsn_monitor_v2_bsn_align_input_copi  => reg_bsn_monitor_v2_bf_rx_align_copi_arr(RN),
        reg_bsn_monitor_v2_bsn_align_input_cipo  => reg_bsn_monitor_v2_bf_rx_align_cipo_arr(RN),
        reg_bsn_monitor_v2_bsn_align_output_copi => reg_bsn_monitor_v2_bf_aligned_copi_arr(RN),
        reg_bsn_monitor_v2_bsn_align_output_cipo => reg_bsn_monitor_v2_bf_aligned_cipo_arr(RN)
      );
  end generate;  -- gen_dut

  ------------------------------------------------------------------------------
  -- Verify bf_sum_sosi_arr at every node, to check that no packets were lost
  ------------------------------------------------------------------------------
  p_verify_bf_sum : process(dp_clk)
  begin
    for RN in 0 to c_last_rn loop
      if bf_sum_sosi_arr(RN).valid = '1' then
        assert TO_SINT(bf_sum_sosi_arr(RN).re) = (RN + 1) * c_local_bf_re
          report "Wrong BF re sum at node " & int_to_str(RN)
          severity error;
        assert TO_SINT(bf_sum_sosi_arr(RN).im) = (RN + 1) * c_local_bf_im
          report "Wrong BF im sum at node " & int_to_str(RN)
          severity error;
      end if;
    end loop;
  end process;

  ------------------------------------------------------------------------------
  -- 10GbE clocks
  ------------------------------------------------------------------------------
  u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  port map (
    refclk_644 => SA_CLK,
    rst_in     => mm_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  ------------------------------------------------------------------------------
  -- MM bus multiplexers
  ------------------------------------------------------------------------------
  -- Use common_mem_mux to avoid (vcom-1450) Actual (indexed name) for formal "mm_miso" is not a static signal name.
  -- Use downto range for _arr, to match downto range of mosi_arr.
  u_mem_mux_reg_ring_lane_info_bf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_addr_w_reg_ring_lane_info_bf
  )
  port map (
    mosi     => reg_ring_lane_info_bf_copi,
    miso     => reg_ring_lane_info_bf_cipo,
    mosi_arr => reg_ring_lane_info_bf_copi_arr,
    miso_arr => reg_ring_lane_info_bf_cipo_arr
  );

  u_mem_mux_reg_bsn_monitor_v2_ring_rx_bf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
  )
  port map (
    mosi     => reg_bsn_monitor_v2_ring_rx_bf_copi,
    miso     => reg_bsn_monitor_v2_ring_rx_bf_cipo,
    mosi_arr => reg_bsn_monitor_v2_ring_rx_bf_copi_arr,
    miso_arr => reg_bsn_monitor_v2_ring_rx_bf_cipo_arr
  );

  u_mem_mux_reg_bsn_monitor_v2_ring_tx_bf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
  )
  port map (
    mosi     => reg_bsn_monitor_v2_ring_tx_bf_copi,
    miso     => reg_bsn_monitor_v2_ring_tx_bf_cipo,
    mosi_arr => reg_bsn_monitor_v2_ring_tx_bf_copi_arr,
    miso_arr => reg_bsn_monitor_v2_ring_tx_bf_cipo_arr
  );

  u_mem_mux_reg_bsn_monitor_v2_bf_rx_align : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w + ceil_log2(c_sdp_P_sum)
  )
  port map (
    mosi     => reg_bsn_monitor_v2_bf_rx_align_copi,
    miso     => reg_bsn_monitor_v2_bf_rx_align_cipo,
    mosi_arr => reg_bsn_monitor_v2_bf_rx_align_copi_arr,
    miso_arr => reg_bsn_monitor_v2_bf_rx_align_cipo_arr
  );

  u_mem_mux_reg_bsn_monitor_v2_bf_aligned : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
  )
  port map (
    mosi     => reg_bsn_monitor_v2_bf_aligned_copi,
    miso     => reg_bsn_monitor_v2_bf_aligned_cipo,
    mosi_arr => reg_bsn_monitor_v2_bf_aligned_copi_arr,
    miso_arr => reg_bsn_monitor_v2_bf_aligned_cipo_arr
  );

  u_mem_mux_reg_bsn_align_v2_bf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_rn,
    g_mult_addr_w => c_sdp_reg_bsn_align_v2_addr_w
  )
  port map (
    mosi     => reg_bsn_align_v2_bf_copi,
    miso     => reg_bsn_align_v2_bf_cipo,
    mosi_arr => reg_bsn_align_v2_bf_copi_arr,
    miso_arr => reg_bsn_align_v2_bf_cipo_arr
  );
end tb;
