-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_fifo_info
-- Description:
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_dp_fifo_info is
end tb_tb_dp_fifo_info;

architecture tb of tb_tb_dp_fifo_info is
  constant c_nof_repeat            : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --  -- general
  --  g_flow_control_stimuli   : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
  --  g_flow_control_verify    : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
  --  -- specific
  --  g_info_use_sync          : BOOLEAN := TRUE;
  --  g_info_use_bsn           : BOOLEAN := TRUE;
  --  g_info_use_channel       : BOOLEAN := TRUE;
  --  g_info_use_empty         : BOOLEAN := TRUE;
  --  g_info_use_error         : BOOLEAN := TRUE;
  --  g_info_fifo_size         : NATURAL := 8;      -- >= ceil_div(g_data_delay, g_data_block_len)
  --  g_nof_data_block         : NATURAL := 25;
  --  g_data_block_len         : NATURAL := 10;
  --  g_data_delay             : NATURAL := 35

  -- Try flow control
  u_act_act         : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  4, 25, 10, 35);
  u_rnd_rnd         : entity work.tb_dp_fifo_info generic map ( e_random, e_random, true,  true,  true,  true,  true,  4, 25, 10, 35);
  u_rnd_pls         : entity work.tb_dp_fifo_info generic map ( e_random, e_pulse,  true,  true,  true,  true,  true,  4, 25, 10, 35);
  u_pls_pls         : entity work.tb_dp_fifo_info generic map ( e_pulse,  e_pulse,  true,  true,  true,  true,  true,  4, 25, 10, 35);
  u_pls_rnd         : entity work.tb_dp_fifo_info generic map ( e_pulse,  e_random, true,  true,  true,  true,  true,  4, 25, 10, 35);

  -- Try different fifo sizes
  u_act_act_size_1  : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  1, 25, 10,  7);
  u_act_act_size_2  : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  2, 25, 10, 17);
  u_act_act_size_3  : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  3, 25, 10, 27);
  u_act_act_size_4  : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  4, 25, 10, 37);
  u_act_act_size_5  : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  true,  true,  true,  true,  5, 25, 10, 47);

  -- Try different info
  u_act_act_no_info : entity work.tb_dp_fifo_info generic map ( e_active, e_active, false, false, false, false, false, 8, 25, 10, 35);
  u_act_act_sync    : entity work.tb_dp_fifo_info generic map ( e_active, e_active, true,  false, false, false, false, 8, 25, 10, 35);
  u_act_act_bsn     : entity work.tb_dp_fifo_info generic map ( e_active, e_active, false, true,  false, false, false, 8, 25, 10, 35);
  u_act_act_channel : entity work.tb_dp_fifo_info generic map ( e_active, e_active, false, false, true,  false, false, 8, 25, 10, 35);
  u_act_act_empty   : entity work.tb_dp_fifo_info generic map ( e_active, e_active, false, false, false, true,  false, 8, 25, 10, 35);
  u_act_act_error   : entity work.tb_dp_fifo_info generic map ( e_active, e_active, false, false, false, false, true,  8, 25, 10, 35);
end tb;
