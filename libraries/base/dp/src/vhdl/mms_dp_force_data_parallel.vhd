-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author : E. Kooistra, 7 apr 2017
-- Purpose : Control force data per parallel stream via MM
-- Description:
--   Address   Data  Access  Description
--     0        [0]     R/W  force enable or default disable for data pass on
--     1     [31:0]     R/W  force sosi data
--     2     [31:0]     R/W  force sosi re
--     3     [31:0]     R/W  force sosi im
--
--   The actual sosi data, re and im width integer is constrained by g_dat_w.
--   Synthesis will optimize away unused bits from the full integer 32b range.
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_force_data_parallel is
  generic (
    g_dat_w                 : natural := 32;  -- must be <= 32 to fit INTEGER range
    g_increment_data        : integer := 0;
    g_increment_re          : integer := 0;
    g_increment_im          : integer := 0;
    g_increment_data_on_sop : boolean := false;
    g_increment_re_on_sop   : boolean := false;
    g_increment_im_on_sop   : boolean := false;
    g_restart_data_on_sync  : boolean := false;
    g_restart_re_on_sync    : boolean := false;
    g_restart_im_on_sync    : boolean := false;
    g_restart_data_on_sop   : boolean := false;
    g_restart_re_on_sop     : boolean := false;
    g_restart_im_on_sop     : boolean := false
  );
  port (
    -- Clocks and reset
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;
    -- MM control
    reg_force_data_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_force_data_miso : out t_mem_miso;
    -- ST sink
    snk_out             : out t_dp_siso;
    snk_in              : in  t_dp_sosi;
    -- ST source
    src_in              : in  t_dp_siso := c_dp_siso_rdy;
    src_out             : out t_dp_sosi
  );
end mms_dp_force_data_parallel;

architecture str of mms_dp_force_data_parallel is
  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_reg         : t_c_mem := (1, 2, g_dat_w, 4, 'X');

  constant c_mm_reg_init    : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0');

  signal reg_force_data_wr  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0);
  signal reg_force_data_rd  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');

  signal force_en           : std_logic := '0';
  signal force_data         : integer := 0;  -- used for sosi.data
  signal force_re           : integer := 0;  -- used for sosi.re
  signal force_im           : integer := 0;  -- used for sosi.im
begin
  -- wires
  force_en             <= reg_force_data_wr(0);
  reg_force_data_rd(0) <= reg_force_data_wr(0);  -- other bits are not used and will read '0'
  force_data                                                    <= TO_SINT(reg_force_data_wr(2 * c_mm_reg.dat_w - 1 downto   c_mm_reg.dat_w));
  reg_force_data_rd(2 * c_mm_reg.dat_w - 1 downto   c_mm_reg.dat_w) <=         reg_force_data_wr(2 * c_mm_reg.dat_w - 1 downto   c_mm_reg.dat_w);
  force_re                                                      <= TO_SINT(reg_force_data_wr(3 * c_mm_reg.dat_w - 1 downto 2 * c_mm_reg.dat_w));
  reg_force_data_rd(3 * c_mm_reg.dat_w - 1 downto 2 * c_mm_reg.dat_w) <=         reg_force_data_wr(3 * c_mm_reg.dat_w - 1 downto 2 * c_mm_reg.dat_w);
  force_im                                                      <= TO_SINT(reg_force_data_wr(4 * c_mm_reg.dat_w - 1 downto 3 * c_mm_reg.dat_w));
  reg_force_data_rd(4 * c_mm_reg.dat_w - 1 downto 3 * c_mm_reg.dat_w) <=         reg_force_data_wr(4 * c_mm_reg.dat_w - 1 downto 3 * c_mm_reg.dat_w);

  u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_mm_reg,
    g_init_reg           => c_mm_reg_init
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_force_data_mosi,
    sla_out        => reg_force_data_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    out_reg        => reg_force_data_wr,  -- readback via ST clock domain
    in_reg         => reg_force_data_rd
  );

  u_dp_force_data_parallel : entity work.dp_force_data_parallel
  generic map (
    g_dat_w                 => g_dat_w,
    g_increment_data        => g_increment_data,
    g_increment_re          => g_increment_re,
    g_increment_im          => g_increment_im,
    g_increment_data_on_sop => g_increment_data_on_sop,
    g_increment_re_on_sop   => g_increment_re_on_sop,
    g_increment_im_on_sop   => g_increment_im_on_sop,
    g_restart_data_on_sync  => g_restart_data_on_sync,
    g_restart_re_on_sync    => g_restart_re_on_sync,
    g_restart_im_on_sync    => g_restart_im_on_sync,
    g_restart_data_on_sop   => g_restart_data_on_sop,
    g_restart_re_on_sop     => g_restart_re_on_sop,
    g_restart_im_on_sop     => g_restart_im_on_sop
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    -- MM control
    force_en      => force_en,
    force_data    => force_data,
    force_re      => force_re,
    force_im      => force_im,
    -- ST sink
    snk_out       => snk_out,
    snk_in        => snk_in,
    -- ST source
    src_in        => src_in,
    src_out       => src_out
  );
end str;
