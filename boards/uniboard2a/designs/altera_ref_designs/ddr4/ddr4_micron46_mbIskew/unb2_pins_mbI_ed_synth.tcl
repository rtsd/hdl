
set_location_assignment PIN_K15 -to CLK
set_location_assignment PIN_J15 -to "CLK(n)"
set_location_assignment PIN_N12 -to ETH_CLK
set_location_assignment PIN_K14 -to PPS
set_location_assignment PIN_J14 -to "PPS(n)"
set_location_assignment PIN_Y36 -to SA_CLK
set_location_assignment PIN_Y35 -to "SA_CLK(n)"
set_location_assignment PIN_AH9 -to SB_CLK
set_location_assignment PIN_AH10 -to "SB_CLK(n)"


# Memory pins read back from quartus chip planner
set_location_assignment PIN_AP20 -to emif_0_example_design_mem_mem_a[0]
set_location_assignment PIN_AR20 -to emif_0_example_design_mem_mem_a[1]
set_location_assignment PIN_AP19 -to emif_0_example_design_mem_mem_a[2]
set_location_assignment PIN_AR19 -to emif_0_example_design_mem_mem_a[3]
set_location_assignment PIN_AR18 -to emif_0_example_design_mem_mem_a[4]
set_location_assignment PIN_AT17 -to emif_0_example_design_mem_mem_a[5]
set_location_assignment PIN_AU19 -to emif_0_example_design_mem_mem_a[6]
set_location_assignment PIN_AT18 -to emif_0_example_design_mem_mem_a[7]
set_location_assignment PIN_AL17 -to emif_0_example_design_mem_mem_a[8]
set_location_assignment PIN_AM18 -to emif_0_example_design_mem_mem_a[9]
set_location_assignment PIN_AM19 -to emif_0_example_design_mem_mem_a[10]
set_location_assignment PIN_AN19 -to emif_0_example_design_mem_mem_a[11]
set_location_assignment PIN_BA17 -to emif_0_example_design_mem_mem_a[12]
set_location_assignment PIN_BD17 -to emif_0_example_design_mem_mem_a[13]
set_location_assignment PIN_AY18 -to emif_0_example_design_mem_mem_act_n[0]
set_location_assignment PIN_AV29 -to emif_0_example_design_mem_mem_alert_n[0]
set_location_assignment PIN_BB16 -to emif_0_example_design_mem_mem_ba[0]
set_location_assignment PIN_BD16 -to emif_0_example_design_mem_mem_ba[1]
set_location_assignment PIN_BC16 -to emif_0_example_design_mem_mem_bg[0]
set_location_assignment PIN_AW19 -to emif_0_example_design_mem_mem_bg[1]
set_location_assignment PIN_BA15 -to emif_0_example_design_mem_mem_a[15]
set_location_assignment PIN_BC21 -to emif_0_example_design_mem_mem_dq[64]
set_location_assignment PIN_BA22 -to emif_0_example_design_mem_mem_dq[65]
set_location_assignment PIN_BD21 -to emif_0_example_design_mem_mem_dq[66]
set_location_assignment PIN_BB20 -to emif_0_example_design_mem_mem_dq[67]
set_location_assignment PIN_BA20 -to emif_0_example_design_mem_mem_dq[68]
set_location_assignment PIN_BD20 -to emif_0_example_design_mem_mem_dq[69]
set_location_assignment PIN_AY20 -to emif_0_example_design_mem_mem_dq[70]
set_location_assignment PIN_AY22 -to emif_0_example_design_mem_mem_dq[71]
set_location_assignment PIN_AU18 -to emif_0_example_design_mem_mem_ck[0]
set_location_assignment PIN_AV18 -to emif_0_example_design_mem_mem_ck_n[0]
set_location_assignment PIN_AT16 -to emif_0_example_design_mem_mem_CK[1]
set_location_assignment PIN_AU16 -to emif_0_example_design_mem_mem_CK_n[1]

set_location_assignment PIN_BB19 -to emif_0_example_design_mem_mem_cke[0]

set_location_assignment PIN_AP16 -to emif_0_example_design_mem_mem_cke[1]
#set_location_assignment PIN_AP16 -to cke1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to cke1_export

set_location_assignment PIN_AY19 -to emif_0_example_design_mem_mem_cs_n[0]

set_location_assignment PIN_AN16 -to emif_0_example_design_mem_mem_cs_n[1]
#set_location_assignment PIN_AN16 -to cs1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to cs1_export

set_location_assignment PIN_BD19 -to emif_0_example_design_mem_mem_odt[0]

set_location_assignment PIN_AR17 -to emif_0_example_design_mem_mem_odt[1]
#set_location_assignment PIN_AR17 -to odt1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to odt1_export

set_location_assignment PIN_BC29 -to emif_0_example_design_mem_mem_dbi_n[0]
set_location_assignment PIN_AR27 -to emif_0_example_design_mem_mem_dbi_n[1]
set_location_assignment PIN_BD24 -to emif_0_example_design_mem_mem_dbi_n[2]
set_location_assignment PIN_AM23 -to emif_0_example_design_mem_mem_dbi_n[3]
set_location_assignment PIN_AU12 -to emif_0_example_design_mem_mem_dbi_n[4]
set_location_assignment PIN_AU13 -to emif_0_example_design_mem_mem_dbi_n[5]
set_location_assignment PIN_AM14 -to emif_0_example_design_mem_mem_dbi_n[6]
set_location_assignment PIN_AM16 -to emif_0_example_design_mem_mem_dbi_n[7]
set_location_assignment PIN_BA21 -to emif_0_example_design_mem_mem_dbi_n[8]
set_location_assignment PIN_BA28 -to emif_0_example_design_mem_mem_dqs[0]
set_location_assignment PIN_AM28 -to emif_0_example_design_mem_mem_dqs[1]
set_location_assignment PIN_AV24 -to emif_0_example_design_mem_mem_dqs[2]
set_location_assignment PIN_AN24 -to emif_0_example_design_mem_mem_dqs[3]
set_location_assignment PIN_BC14 -to emif_0_example_design_mem_mem_dqs[4]
set_location_assignment PIN_AW14 -to emif_0_example_design_mem_mem_dqs[5]
set_location_assignment PIN_AN12 -to emif_0_example_design_mem_mem_dqs[6]
set_location_assignment PIN_AK15 -to emif_0_example_design_mem_mem_dqs[7]
set_location_assignment PIN_BC22 -to emif_0_example_design_mem_mem_dqs[8]


set_location_assignment PIN_BC18 -to emif_0_example_design_mem_mem_par[0]
set_location_assignment PIN_BB15 -to emif_0_example_design_mem_mem_a[16]

set_location_assignment PIN_AW17 -to emif_0_example_design_pll_ref_clk_clk
#set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_pll_ref_clk_clk
set_instance_assignment -name IO_STANDARD "1.2 V" -to emif_0_example_design_pll_ref_clk_clk

set_location_assignment PIN_AV19 -to emif_0_example_design_mem_mem_reset_n[0]

set_location_assignment PIN_AY17 -to oct_oct_rzqin
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_oct_rzqin
#set_instance_assignment -name IO_STANDARD "SSTL-12" -to oct_oct_rzqin

set_location_assignment PIN_BC17 -to emif_0_example_design_mem_mem_a[14]
# qsfp leds
set_location_assignment PIN_BA33 -to emif_0_example_design_status_local_cal_fail
set_location_assignment PIN_BB33 -to emif_0_example_design_status_local_cal_success
set_location_assignment PIN_AV32 -to emif_0_example_design_tg_0_traffic_gen_fail
set_location_assignment PIN_AP31 -to emif_0_example_design_tg_0_traffic_gen_pass
set_location_assignment PIN_AT33 -to emif_0_example_design_tg_0_traffic_gen_timeout
set_location_assignment PIN_AF32 -to pll_locked_pll_locked
# testio(0)
set_location_assignment PIN_AN32 -to global_reset_reset_n 
# testio(1)
set_location_assignment PIN_AP32 -to testio1 
set_instance_assignment -name IO_STANDARD "1.8 V" -to testio1

# IO Standard Assignments from Gijs (excluding memory)
set_instance_assignment -name IO_STANDARD "1.8 V" -to ETH_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SA_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SB_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to WDI

#set_location_assignment PIN_AN32 -to TESTIO[0]
set_location_assignment PIN_AP32 -to TESTIO[1]
set_location_assignment PIN_AT30 -to TESTIO[2]
set_location_assignment PIN_BD31 -to TESTIO[3]
set_location_assignment PIN_AU30 -to TESTIO[4]
set_location_assignment PIN_BD30 -to TESTIO[5]

set_location_assignment PIN_AB12 -to VERSION[0]
set_location_assignment PIN_AB13 -to VERSION[1]
set_location_assignment PIN_BB30 -to WDI

# locations changed 30 sept
set_location_assignment PIN_K12 -to ETH_SGIN[0]
set_location_assignment PIN_J12 -to "ETH_SGIN[0](n)"
set_location_assignment PIN_AF33 -to ETH_SGIN[1]
set_location_assignment PIN_AE33 -to "ETH_SGIN[1](n)"
set_location_assignment PIN_H13 -to ETH_SGOUT[0]
set_location_assignment PIN_H12 -to "ETH_SGOUT[0](n)"
set_location_assignment PIN_AW31 -to ETH_SGOUT[1]
set_location_assignment PIN_AV31 -to "ETH_SGOUT[1](n)"

set_instance_assignment -name IO_STANDARD LVDS -to PPS
set_instance_assignment -name IO_STANDARD LVDS -to "PPS(n)"
set_instance_assignment -name IO_STANDARD LVDS -to CLK
set_instance_assignment -name IO_STANDARD LVDS -to "CLK(n)"

# Enable internal termination for LVDS inputs
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to PPS
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[1]

set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"
set_location_assignment PIN_AL32 -to CLKUSR
set_location_assignment PIN_M16 -to MB_EVENT





set_location_assignment PIN_AU29 -to emif_0_example_design_mem_mem_dq[0]
set_location_assignment PIN_BC28 -to emif_0_example_design_mem_mem_dq[1]
set_location_assignment PIN_AY29 -to emif_0_example_design_mem_mem_dq[2]
set_location_assignment PIN_BB28 -to emif_0_example_design_mem_mem_dq[3]
set_location_assignment PIN_BB29 -to emif_0_example_design_mem_mem_dq[4]
set_location_assignment PIN_AW29 -to emif_0_example_design_mem_mem_dq[5]
set_location_assignment PIN_BC27 -to emif_0_example_design_mem_mem_dq[6]
set_location_assignment PIN_BD29 -to emif_0_example_design_mem_mem_dq[7]
set_location_assignment PIN_AR28 -to emif_0_example_design_mem_mem_dq[8]
set_location_assignment PIN_AR29 -to emif_0_example_design_mem_mem_dq[9]
set_location_assignment PIN_AV27 -to emif_0_example_design_mem_mem_dq[10]
set_location_assignment PIN_AU28 -to emif_0_example_design_mem_mem_dq[11]
set_location_assignment PIN_AW27 -to emif_0_example_design_mem_mem_dq[12]
set_location_assignment PIN_AT28 -to emif_0_example_design_mem_mem_dq[13]
set_location_assignment PIN_AV28 -to emif_0_example_design_mem_mem_dq[14]
set_location_assignment PIN_AP27 -to emif_0_example_design_mem_mem_dq[15]
set_location_assignment PIN_BC24 -to emif_0_example_design_mem_mem_dq[16]
set_location_assignment PIN_BB24 -to emif_0_example_design_mem_mem_dq[17]
set_location_assignment PIN_BB23 -to emif_0_example_design_mem_mem_dq[18]
set_location_assignment PIN_AW22 -to emif_0_example_design_mem_mem_dq[19]
set_location_assignment PIN_BA23 -to emif_0_example_design_mem_mem_dq[20]
set_location_assignment PIN_BC23 -to emif_0_example_design_mem_mem_dq[21]
set_location_assignment PIN_AY23 -to emif_0_example_design_mem_mem_dq[22]
set_location_assignment PIN_AY24 -to emif_0_example_design_mem_mem_dq[23]
set_location_assignment PIN_AP22 -to emif_0_example_design_mem_mem_dq[24]
set_location_assignment PIN_AN23 -to emif_0_example_design_mem_mem_dq[25]
set_location_assignment PIN_AR23 -to emif_0_example_design_mem_mem_dq[26]
set_location_assignment PIN_AT23 -to emif_0_example_design_mem_mem_dq[27]
set_location_assignment PIN_AU23 -to emif_0_example_design_mem_mem_dq[28]
set_location_assignment PIN_AV23 -to emif_0_example_design_mem_mem_dq[29]
set_location_assignment PIN_AR24 -to emif_0_example_design_mem_mem_dq[30]
set_location_assignment PIN_AP24 -to emif_0_example_design_mem_mem_dq[31]
set_location_assignment PIN_AV12 -to emif_0_example_design_mem_mem_dq[32]
set_location_assignment PIN_AY13 -to emif_0_example_design_mem_mem_dq[33]
set_location_assignment PIN_BD14 -to emif_0_example_design_mem_mem_dq[34]
set_location_assignment PIN_AY12 -to emif_0_example_design_mem_mem_dq[35]
set_location_assignment PIN_BA13 -to emif_0_example_design_mem_mem_dq[36]
set_location_assignment PIN_BA12 -to emif_0_example_design_mem_mem_dq[37]
set_location_assignment PIN_AW12 -to emif_0_example_design_mem_mem_dq[38]
set_location_assignment PIN_BB13 -to emif_0_example_design_mem_mem_dq[39]
set_location_assignment PIN_AV13 -to emif_0_example_design_mem_mem_dq[40]
set_location_assignment PIN_AR13 -to emif_0_example_design_mem_mem_dq[41]
set_location_assignment PIN_AR15 -to emif_0_example_design_mem_mem_dq[42]
set_location_assignment PIN_AP15 -to emif_0_example_design_mem_mem_dq[43]
set_location_assignment PIN_AT15 -to emif_0_example_design_mem_mem_dq[44]
set_location_assignment PIN_AU14 -to emif_0_example_design_mem_mem_dq[45]
set_location_assignment PIN_AU15 -to emif_0_example_design_mem_mem_dq[46]
set_location_assignment PIN_AV14 -to emif_0_example_design_mem_mem_dq[47]
set_location_assignment PIN_AM13 -to emif_0_example_design_mem_mem_dq[48]
set_location_assignment PIN_AT13 -to emif_0_example_design_mem_mem_dq[49]
set_location_assignment PIN_AT12 -to emif_0_example_design_mem_mem_dq[50]
set_location_assignment PIN_AP14 -to emif_0_example_design_mem_mem_dq[51]
set_location_assignment PIN_AN13 -to emif_0_example_design_mem_mem_dq[52]
set_location_assignment PIN_AK13 -to emif_0_example_design_mem_mem_dq[53]
set_location_assignment PIN_AM12 -to emif_0_example_design_mem_mem_dq[54]
set_location_assignment PIN_AL13 -to emif_0_example_design_mem_mem_dq[55]
set_location_assignment PIN_AH13 -to emif_0_example_design_mem_mem_dq[56]
set_location_assignment PIN_AL15 -to emif_0_example_design_mem_mem_dq[57]
set_location_assignment PIN_AM15 -to emif_0_example_design_mem_mem_dq[58]
set_location_assignment PIN_AJ14 -to emif_0_example_design_mem_mem_dq[59]
set_location_assignment PIN_AJ12 -to emif_0_example_design_mem_mem_dq[60]
set_location_assignment PIN_AL16 -to emif_0_example_design_mem_mem_dq[61]
set_location_assignment PIN_AK12 -to emif_0_example_design_mem_mem_dq[62]
set_location_assignment PIN_AH14 -to emif_0_example_design_mem_mem_dq[63]
set_location_assignment PIN_AY28 -to emif_0_example_design_mem_mem_dqs_n[0]
set_location_assignment PIN_AN28 -to emif_0_example_design_mem_mem_dqs_n[1]
set_location_assignment PIN_AU24 -to emif_0_example_design_mem_mem_dqs_n[2]
set_location_assignment PIN_AM24 -to emif_0_example_design_mem_mem_dqs_n[3]
set_location_assignment PIN_BB14 -to emif_0_example_design_mem_mem_dqs_n[4]
set_location_assignment PIN_AY14 -to emif_0_example_design_mem_mem_dqs_n[5]
set_location_assignment PIN_AP12 -to emif_0_example_design_mem_mem_dqs_n[6]
set_location_assignment PIN_AK14 -to emif_0_example_design_mem_mem_dqs_n[7]
set_location_assignment PIN_BD22 -to emif_0_example_design_mem_mem_dqs_n[8]
set_location_assignment PIN_AJ31 -to altera_reserved_tck
set_location_assignment PIN_AK18 -to altera_reserved_tdi
set_location_assignment PIN_AH31 -to altera_reserved_ntrst
set_location_assignment PIN_AM29 -to altera_reserved_tdo
#set_location_assignment PIN_AV33 -to ~ALTERA_DATA0~



set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[2]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[3]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[4]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[5]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[6]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[7]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[8]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[9]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[10]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[11]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[12]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[13]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_act_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_ba[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_ba[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_bg[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_bg[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[15]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V SSTL" -to emif_0_example_design_mem_mem_ck[0]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V SSTL" -to emif_0_example_design_mem_mem_CK[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_cke[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_cs_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_par[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[16]
set_instance_assignment -name IO_STANDARD "1.2 V" -to emif_0_example_design_mem_mem_reset_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_A[14]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_odt[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_alert_n[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[64]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[65]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[66]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[67]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[68]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[69]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[70]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[71]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[1]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[2]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[3]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[4]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[5]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[6]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[7]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[8]

set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[0]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[1]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[2]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[3]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[4]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[5]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[6]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[7]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[8]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[1]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[2]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[3]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[4]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[5]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[6]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[7]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[8]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[9]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[10]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[11]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[12]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[13]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[14]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[15]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[16]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[17]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[18]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[19]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[20]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[21]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[22]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[23]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[24]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[25]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[26]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[27]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[28]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[29]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[30]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[31]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[32]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[33]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[34]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[35]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[36]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[37]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[38]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[39]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[40]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[41]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[42]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[43]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[44]

set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[45]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[46]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[47]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[48]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[49]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[50]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[51]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[52]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[53]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[54]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[55]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[56]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[57]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[58]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[59]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[60]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[61]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[62]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[63]

