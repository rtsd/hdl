------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.unb1_board_pkg.all;

entity unb1_board_front_io is
  generic (
    g_nof_xaui : natural
  );
  port (
    xaui_tx_arr       : in  t_unb1_board_xaui_sl_2arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr       : out t_unb1_board_xaui_sl_2arr(g_nof_xaui - 1 downto 0);

    mdio_mdc_arr      : in  std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_in_arr  : out std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_oen_arr : in  std_logic_vector(g_nof_xaui - 1 downto 0);

    -- Serial I/O
    -- . hard IP transceivers busses
    SI_FN_0_TX      : out std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0);
    SI_FN_0_RX      : in  std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0) := (others => '0');
    SI_FN_1_TX      : out std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0);
    SI_FN_1_RX      : in  std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0) := (others => '0');
    SI_FN_2_TX      : out std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0);
    SI_FN_2_RX      : in  std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0) := (others => '0');
    -- . soft IP transceivers bus (typically not used on UniBoard)
    SI_FN_3_TX      : out std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0);
    SI_FN_3_RX      : in  std_logic_vector(c_unb1_board_tr_xaui.bus_w - 1 downto 0) := (others => '0');

    SI_FN_0_CNTRL   : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_1_CNTRL   : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_2_CNTRL   : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_3_CNTRL   : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0)
  );
end unb1_board_front_io;

architecture str of unb1_board_front_io is
  -- help signals so we can iterate through buses
  signal si_fn_tx_arr : t_unb1_board_xaui_sl_2arr(c_unb1_board_tr_xaui.bus_w - 1 downto 0) := (others => (others => '0'));
  signal si_fn_rx_arr : t_unb1_board_xaui_sl_2arr(c_unb1_board_tr_xaui.bus_w - 1 downto 0);
begin
  -- XAUI buses
  SI_FN_0_TX      <= si_fn_tx_arr(0);
  SI_FN_1_TX      <= si_fn_tx_arr(1);
  SI_FN_2_TX      <= si_fn_tx_arr(2);
  SI_FN_3_TX      <= si_fn_tx_arr(3);

  si_fn_rx_arr(0) <= SI_FN_0_RX;
  si_fn_rx_arr(1) <= SI_FN_1_RX;
  si_fn_rx_arr(2) <= SI_FN_2_RX;
  si_fn_rx_arr(3) <= SI_FN_3_RX;

  wires : for i in 0 to g_nof_xaui - 1 generate
    si_fn_tx_arr(i) <= xaui_tx_arr(i);
    xaui_rx_arr(i)  <= si_fn_rx_arr(i);
  end generate;

  -- MDIO buffers
  gen_iobuf_0 : if g_nof_xaui > 0 generate
    u_iobuf_0 : entity common_lib.common_inout
    port map (
      dat_inout        => SI_FN_0_CNTRL(c_unb1_board_ci.tr.cntrl_mdio_id),
      dat_in_from_line => mdio_mdat_in_arr(0),
      dat_out_to_line  => '0',
      dat_out_en       => mdio_mdat_oen_arr(0)
    );

    SI_FN_0_CNTRL(c_unb1_board_ci.tr.cntrl_mdc_id) <= mdio_mdc_arr(0);
  end generate;

  gen_iobuf_1 : if g_nof_xaui > 1 generate
    u_iobuf_1 : entity common_lib.common_inout
    port map (
      dat_inout        => SI_FN_1_CNTRL(c_unb1_board_ci.tr.cntrl_mdio_id),
      dat_in_from_line => mdio_mdat_in_arr(1),
      dat_out_to_line  => '0',
      dat_out_en       => mdio_mdat_oen_arr(1)
    );

    SI_FN_1_CNTRL(c_unb1_board_ci.tr.cntrl_mdc_id) <= mdio_mdc_arr(1);
  end generate;

  gen_iobuf_2 : if g_nof_xaui > 2 generate
    u_iobuf_2 : entity common_lib.common_inout
    port map (
      dat_inout        => SI_FN_2_CNTRL(c_unb1_board_ci.tr.cntrl_mdio_id),
      dat_in_from_line => mdio_mdat_in_arr(2),
      dat_out_to_line  => '0',
      dat_out_en       => mdio_mdat_oen_arr(2)
    );

    SI_FN_2_CNTRL(c_unb1_board_ci.tr.cntrl_mdc_id) <= mdio_mdc_arr(2);
  end generate;

  gen_iobuf_3 : if g_nof_xaui > 3 generate
    u_iobuf_3 : entity common_lib.common_inout
    port map (
      dat_inout        => SI_FN_3_CNTRL(c_unb1_board_ci.tr.cntrl_mdio_id),
      dat_in_from_line => mdio_mdat_in_arr(3),
      dat_out_to_line  => '0',
      dat_out_en       => mdio_mdat_oen_arr(3)
    );

    SI_FN_3_CNTRL(c_unb1_board_ci.tr.cntrl_mdc_id) <= mdio_mdc_arr(3);
  end generate;
end;
