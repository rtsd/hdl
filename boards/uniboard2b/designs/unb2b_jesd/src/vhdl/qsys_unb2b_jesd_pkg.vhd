-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb2b_jesd_pkg is
    -----------------------------------------------------------------------------
    -- this component declaration is copy-pasted from Quartus v14 QSYS builder
    -----------------------------------------------------------------------------

component qsys_unb2b_jesd is
        port (
            altjesd_reset_seq_irq_irq                                    : out std_logic;  -- irq
            altjesd_reset_seq_pll_locked_in_reset1_dsrt_qual             : in  std_logic                     := 'X';  -- reset1_dsrt_qual
            altjesd_reset_seq_pll_reset_reset                            : out std_logic;  -- reset
            altjesd_reset_seq_reset_in0_reset                            : in  std_logic                     := 'X';  -- reset
            altjesd_reset_seq_rx_xcvr_ready_in_reset5_dsrt_qual          : in  std_logic                     := 'X';  -- reset5_dsrt_qual
            altjesd_reset_seq_tx_xcvr_ready_in_reset2_dsrt_qual          : in  std_logic                     := 'X';  -- reset2_dsrt_qual
            altjesd_rx_csr_cf_export                                     : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_csr_cs_export                                     : out std_logic_vector(1 downto 0);  -- export
            altjesd_rx_csr_f_export                                      : out std_logic_vector(7 downto 0);  -- export
            altjesd_rx_csr_hd_export                                     : out std_logic;  -- export
            altjesd_rx_csr_k_export                                      : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_csr_l_export                                      : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_csr_lane_powerdown_export                         : out std_logic_vector(0 downto 0);  -- export
            altjesd_rx_csr_m_export                                      : out std_logic_vector(7 downto 0);  -- export
            altjesd_rx_csr_n_export                                      : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_csr_np_export                                     : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_csr_rx_testmode_export                            : out std_logic_vector(3 downto 0);  -- export
            altjesd_rx_csr_s_export                                      : out std_logic_vector(4 downto 0);  -- export
            altjesd_rx_dev_sync_n_export                                 : out std_logic;  -- export
            altjesd_rx_jesd204_rx_dlb_data_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            altjesd_rx_jesd204_rx_dlb_data_valid_export                  : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- export
            altjesd_rx_jesd204_rx_dlb_disperr_export                     : in  std_logic_vector(3 downto 0)  := (others => 'X');  -- export
            altjesd_rx_jesd204_rx_dlb_errdetect_export                   : in  std_logic_vector(3 downto 0)  := (others => 'X');  -- export
            altjesd_rx_jesd204_rx_dlb_kchar_data_export                  : in  std_logic_vector(3 downto 0)  := (others => 'X');  -- export
            altjesd_rx_jesd204_rx_frame_error_export                     : in  std_logic                     := 'X';  -- export
            altjesd_rx_jesd204_rx_int_irq                                : out std_logic;  -- irq
            altjesd_rx_jesd204_rx_link_data                              : out std_logic_vector(31 downto 0);  -- data
            altjesd_rx_jesd204_rx_link_valid                             : out std_logic;  -- valid
            altjesd_rx_jesd204_rx_link_ready                             : in  std_logic                     := 'X';  -- ready
            altjesd_rx_rx_serial_data_rx_serial_data                     : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- rx_serial_data
            altjesd_rx_rxlink_rst_n_reset_n                              : in  std_logic                     := 'X';  -- reset_n
            altjesd_rx_rxphy_clk_export                                  : out std_logic_vector(0 downto 0);  -- export
            altjesd_rx_sof_export                                        : out std_logic_vector(3 downto 0);  -- export
            altjesd_rx_somf_export                                       : out std_logic_vector(3 downto 0);  -- export
            altjesd_rx_sysref_export                                     : in  std_logic                     := 'X';  -- export
            altjesd_ss_rx_corepll_locked_export                          : out std_logic;  -- export
            altjesd_ss_rx_xcvr_reset_control_pll_powerdown_pll_powerdown : out std_logic_vector(0 downto 0);  -- pll_powerdown
            altjesd_ss_rx_xcvr_reset_control_rx_ready_rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready
            altjesd_ss_rx_frame_reset_out_reset_reset_n                  : out std_logic;  -- reset_n
            altjesd_ss_rx_link_reset_out_reset_reset_n                   : out std_logic;  -- reset_n
            avs_eth_0_clk_export                                         : out std_logic;  -- export
            avs_eth_0_irq_export                                         : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export                                 : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export                                    : out std_logic;  -- export
            avs_eth_0_ram_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export                                   : out std_logic;  -- export
            avs_eth_0_ram_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export                                 : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export                                    : out std_logic;  -- export
            avs_eth_0_reg_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export                                   : out std_logic;  -- export
            avs_eth_0_reg_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                                       : out std_logic;  -- export
            avs_eth_0_tse_address_export                                 : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export                                    : out std_logic;  -- export
            avs_eth_0_tse_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export                             : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export                                   : out std_logic;  -- export
            avs_eth_0_tse_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                                      : in  std_logic                     := 'X';  -- clk
            device_clk_clk                                               : in  std_logic                     := 'X';  -- clk
            device_clk_reset_reset_n                                     : in  std_logic                     := 'X';  -- reset_n
            frame_clk_clk                                                : in  std_logic                     := 'X';  -- clk
            frame_clk_reset_reset_n                                      : in  std_logic                     := 'X';  -- reset_n
            link_clk_clk                                                 : in  std_logic                     := 'X';  -- clk
            link_clk_reset_reset_n                                       : in  std_logic                     := 'X';  -- reset_n
            pio_pps_address_export                                       : out std_logic_vector(0 downto 0);  -- export
            pio_pps_clk_export                                           : out std_logic;  -- export
            pio_pps_read_export                                          : out std_logic;  -- export
            pio_pps_readdata_export                                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                                         : out std_logic;  -- export
            pio_pps_write_export                                         : out std_logic;  -- export
            pio_pps_writedata_export                                     : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export                               : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export                                   : out std_logic;  -- export
            pio_system_info_read_export                                  : out std_logic;  -- export
            pio_system_info_readdata_export                              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export                                 : out std_logic;  -- export
            pio_system_info_write_export                                 : out std_logic;  -- export
            pio_system_info_writedata_export                             : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export                           : out std_logic;  -- export
            pll_out_frame_clk_clk                                        : out std_logic;  -- clk
            pll_out_link_clk_clk                                         : out std_logic;  -- clk
            reg_dpmm_ctrl_address_export                                 : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                                     : out std_logic;  -- export
            reg_dpmm_ctrl_read_export                                    : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export                                   : out std_logic;  -- export
            reg_dpmm_ctrl_write_export                                   : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export                                 : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                                     : out std_logic;  -- export
            reg_dpmm_data_read_export                                    : out std_logic;  -- export
            reg_dpmm_data_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export                                   : out std_logic;  -- export
            reg_dpmm_data_write_export                                   : out std_logic;  -- export
            reg_dpmm_data_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                                      : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                                          : out std_logic;  -- export
            reg_epcs_read_export                                         : out std_logic;  -- export
            reg_epcs_readdata_export                                     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                                        : out std_logic;  -- export
            reg_epcs_write_export                                        : out std_logic;  -- export
            reg_epcs_writedata_export                                    : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_address_export                            : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_clk_export                                : out std_logic;  -- export
            reg_fpga_temp_sens_read_export                               : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export                              : out std_logic;  -- export
            reg_fpga_temp_sens_write_export                              : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export                          : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_address_export                         : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_clk_export                             : out std_logic;  -- export
            reg_fpga_voltage_sens_read_export                            : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export                           : out std_logic;  -- export
            reg_fpga_voltage_sens_write_export                           : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export                       : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export                                 : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                                     : out std_logic;  -- export
            reg_mmdp_ctrl_read_export                                    : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export                                   : out std_logic;  -- export
            reg_mmdp_ctrl_write_export                                   : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export                                 : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                                     : out std_logic;  -- export
            reg_mmdp_data_read_export                                    : out std_logic;  -- export
            reg_mmdp_data_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export                                   : out std_logic;  -- export
            reg_mmdp_data_write_export                                   : out std_logic;  -- export
            reg_mmdp_data_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                                      : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                                          : out std_logic;  -- export
            reg_remu_read_export                                         : out std_logic;  -- export
            reg_remu_readdata_export                                     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                                        : out std_logic;  -- export
            reg_remu_write_export                                        : out std_logic;  -- export
            reg_remu_writedata_export                                    : out std_logic_vector(31 downto 0);  -- export
            reg_unb_pmbus_address_export                                 : out std_logic_vector(5 downto 0);  -- export
            reg_unb_pmbus_clk_export                                     : out std_logic;  -- export
            reg_unb_pmbus_read_export                                    : out std_logic;  -- export
            reg_unb_pmbus_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_pmbus_reset_export                                   : out std_logic;  -- export
            reg_unb_pmbus_write_export                                   : out std_logic;  -- export
            reg_unb_pmbus_writedata_export                               : out std_logic_vector(31 downto 0);  -- export
            reg_unb_sens_address_export                                  : out std_logic_vector(5 downto 0);  -- export
            reg_unb_sens_clk_export                                      : out std_logic;  -- export
            reg_unb_sens_read_export                                     : out std_logic;  -- export
            reg_unb_sens_readdata_export                                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_sens_reset_export                                    : out std_logic;  -- export
            reg_unb_sens_write_export                                    : out std_logic;  -- export
            reg_unb_sens_writedata_export                                : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                                       : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                                           : out std_logic;  -- export
            reg_wdi_read_export                                          : out std_logic;  -- export
            reg_wdi_readdata_export                                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                                         : out std_logic;  -- export
            reg_wdi_write_export                                         : out std_logic;  -- export
            reg_wdi_writedata_export                                     : out std_logic_vector(31 downto 0);  -- export
            reset_reset_n                                                : in  std_logic                     := 'X';  -- reset_n
            rom_system_info_address_export                               : out std_logic_vector(9 downto 0);  -- export
            rom_system_info_clk_export                                   : out std_logic;  -- export
            rom_system_info_read_export                                  : out std_logic;  -- export
            rom_system_info_readdata_export                              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export                                 : out std_logic;  -- export
            rom_system_info_write_export                                 : out std_logic;  -- export
            rom_system_info_writedata_export                             : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buf_jesd_address_export                        : out std_logic_vector(16 downto 0);  -- export
            ram_diag_data_buf_jesd_clk_export                            : out std_logic;  -- export
            ram_diag_data_buf_jesd_read_export                           : out std_logic;  -- export
            ram_diag_data_buf_jesd_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buf_jesd_reset_export                          : out std_logic;  -- export
            ram_diag_data_buf_jesd_write_export                          : out std_logic;  -- export
            ram_diag_data_buf_jesd_writedata_export                      : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buf_jesd_address_export                        : out std_logic_vector(5 downto 0);  -- export
            reg_diag_data_buf_jesd_clk_export                            : out std_logic;  -- export
            reg_diag_data_buf_jesd_read_export                           : out std_logic;  -- export
            reg_diag_data_buf_jesd_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buf_jesd_reset_export                          : out std_logic;  -- export
            reg_diag_data_buf_jesd_write_export                          : out std_logic;  -- export
            reg_diag_data_buf_jesd_writedata_export                      : out std_logic_vector(31 downto 0)  -- export
        );
    end component qsys_unb2b_jesd;
end qsys_unb2b_jesd_pkg;
