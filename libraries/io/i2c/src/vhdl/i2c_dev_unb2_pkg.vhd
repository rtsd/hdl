-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Uniboard2 version derived from i2c_dev_unb_pkg

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.i2c_smbus_pkg.all;
use work.i2c_dev_max1617_pkg.all;
use work.i2c_dev_max6652_pkg.all;
use work.i2c_commander_pkg.all;

package i2c_dev_unb2_pkg is
  -- I2C slave addresses of the devices on the I2C sens bus on UniBoard2
  constant I2C_UNB2_SENS_TEMP_MAX1617_ADR     : natural := MAX1617_ADR_MID_LOW;  -- temperature sensor, slave address is 0x29
  constant I2C_UNB2_SENS_DCDC_BMR456_ADR      : natural := 16#2C#;  -- dc/dc converter, slave address is 0x2c
  constant I2C_UNB2_SENS_PIM_PIM4328PD_ADR    : natural := 16#2b#;  -- power input module, slave address is 0x2b
  constant I2C_UNB2_SENS_1V2_BMR461_ADR       : natural := 16#0f#;  -- 1.2V power supply, slave address is 0x0f  (CHECKED OK: ONLY on FN2)
  constant I2C_UNB2_SENS_3V3_BMR461_ADR       : natural := 16#0e#;  -- 3.3V power supply, slave address is 0x0e  (CHECKED OK: ONLY on FN2)
  constant I2C_UNB2_SENS_CLK_BMR461_ADR       : natural := 16#0d#;  -- clock power supply, slave address is 0x0d (CHECKED OK: ONLY on FN2)
  constant I2C_UNB2_SENS_QSFP0_BMR464_ADR     : natural := 16#01#;  -- qsfp0 power supply, slave address is 0x01 (CHECKED OK: ONLY on FN2)
  constant I2C_UNB2_SENS_QSFP1_BMR464_ADR     : natural := 16#02#;  -- qsfp1 power supply, slave address is 0x02 (CHECKED OK: ONLY on FN2)
  constant I2C_UNB2_SENS_EEPROM_CAT24C02_ADR  : natural := 16#50#;  -- eeprom , slave address is 0x50            (CHECKED OK: on all nodes)
  constant I2C_UNB2_SENS_TEMP_TMP451_ADR      : natural := 16#4c#;  -- temperature sensor, slave address is 0x4c (CHECKED OK: ONLY on FN2)

  -- I2C slave addresses of the devices on the I2C pm bus on UniBoard2
  constant I2C_UNB2_PMB_CORE_BMR464_ADR       : natural := 16#01#;  -- core supply, slave address is 0x01         (CHECKED OK)
  constant I2C_UNB2_PMB_VCCRAM_BMR461_ADR     : natural := 16#0d#;  -- vcc ram supply, slave address is 0x0d      (CHECKED OK)
  constant I2C_UNB2_PMB_TCVR0_BMR461_ADR      : natural := 16#0e#;  -- transceiver0 supply, slave address is 0x0e (CHECKED OK)
  constant I2C_UNB2_PMB_TCVR1_BMR461_ADR      : natural := 16#0f#;  -- transceiver1 supply, slave address is 0x0f (CHECKED OK)
  constant I2C_UNB2_PMB_CTRL_BMR461_ADR       : natural := 16#10#;  -- control supply, slave address is 0x10      (CHECKED OK)
  constant I2C_UNB2_PMB_FPGAIO_BMR461_ADR     : natural := 16#11#;  -- fpga io supply, slave address is 0x11      (CHECKED OK)

  -- I2C slave addresses of the devices on the I2C ddr4 memory bus on UniBoard2
  constant I2C_UNB2_MB_I_DDR4_ADR             : natural := 16#18#;  -- ddr4 module I, slave address is 0x18
  constant I2C_UNB2_MB_II_DDR4_ADR            : natural := 16#19#;  -- ddr4 module II, slave address is 0x19

  constant c_i2c_unb_temp_high     : natural := 85;

  -- commands
  -- these can later go into device specific packages
  constant PMBUS_REG_READ_VOUT_MODE          : natural := 16#20#;  -- common to all PMB devices (CHECKED OK)
  constant PMBUS_REG_READ_VIN                : natural := 16#88#;  -- common to all PMB devices
  constant PMBUS_REG_READ_VCAP               : natural := 16#8a#;  -- used in the PIM4328PD
  constant PMBUS_REG_READ_VOUT               : natural := 16#8b#;  -- common to all PMB devices (CHECKED OK)
  constant PMBUS_REG_READ_IOUT               : natural := 16#8c#;  -- common to all PMB devices (CHECKED OK)
  constant PMBUS_REG_READ_TEMP               : natural := 16#8d#;  -- common to all PMB devices (CHECKED OK)

  constant c_i2c_unb2_nof_protocol_lists     : natural := 4;  -- for now we allow four protocol lists for each i2c interface

  -- Commander protocol lists for UNB sens bus

  -- > Temperature sensor:
  --       . c_i2c_unb_protocol_list_read_config
  --       . c_i2c_unb_protocol_list_read

  constant c_i2c_unb2_sens_max1617_expected_mask_read_config   : std_logic_vector := c_i2c_cmdr_expected_mask_read_one_byte;
  constant c_i2c_unb2_sens_max1617_nof_result_data_read_config : natural := c_i2c_cmdr_nof_result_data_read_one_byte;
  constant c_i2c_unb2_sens_max1617_protocol_list_read_config   : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_TEMP_MAX1617_ADR, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_WRITE_BYTE, I2C_UNB2_SENS_TEMP_MAX1617_ADR, MAX1617_CMD_WRITE_CONFIG, MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, I2C_UNB2_SENS_TEMP_MAX1617_ADR, MAX1617_CMD_WRITE_REMOTE_HIGH, c_i2c_unb_temp_high,
    SMBUS_C_END
  );

  constant c_i2c_unb2_sens_max1617_expected_mask_read_temp   : std_logic_vector := c_i2c_cmdr_expected_mask_read_one_byte;
  constant c_i2c_unb2_sens_max1617_nof_result_data_read_temp : natural := c_i2c_cmdr_nof_result_data_read_one_byte;
  constant c_i2c_unb2_sens_max1617_protocol_list_read_temp   : t_nat_natural_arr := (
    SMBUS_READ_BYTE, I2C_UNB2_SENS_TEMP_MAX1617_ADR, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_C_END
  );

  constant c_i2c_unb2_sens_expected_mask_read_all   : std_logic_vector := RESIZE_UVEC("001010101010101010101010101010101010101010101", c_word_w);
  constant c_i2c_unb2_sens_nof_result_data_read_all : natural := 22;
  constant c_i2c_unb2_sens_protocol_list_read_all   : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_TEMP_MAX1617_ADR, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_DCDC_BMR456_ADR, PMBUS_REG_READ_VIN,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_DCDC_BMR456_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_DCDC_BMR456_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_PIM_PIM4328PD_ADR, PMBUS_REG_READ_VIN,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_PIM_PIM4328PD_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_PIM_PIM4328PD_ADR, PMBUS_REG_READ_VCAP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_1V2_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_1V2_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_1V2_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_3V3_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_3V3_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_3V3_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_CLK_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_CLK_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_CLK_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP0_BMR464_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP0_BMR464_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP0_BMR464_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP1_BMR464_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP1_BMR464_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_SENS_QSFP1_BMR464_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_C_END
  );

  -- Commander protocol lists for UNB2 PMBUS

  constant c_i2c_unb2_pmbus_expected_mask_read_all   : std_logic_vector := RESIZE_UVEC("0010101010101010101010101010101010101", c_word_w);
  constant c_i2c_unb2_pmbus_nof_result_data_read_all : natural := 18;
  constant c_i2c_unb2_pmbus_protocol_list_read_all   : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_TEMP,
    SMBUS_C_END
  );
end package;
