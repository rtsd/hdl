-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra 2009, updated in 2021 using common_round_tb.py
-- Purpose: Test bench for common_round.vhd
-- Description:
--   The signal names in this VHDL tb are the same as in the document [1] and
--   in the Python tb [2].
--   The verification uses expected results that were obtained and copied
--   from [2]. The tb_tb_round.vhd tries different settings for w and r.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L4+SDPFW+Decision%3A+Number+representation%2C+resizing+and+rounding
-- [2] common_round_tb.py
-- Usage:
-- > as 5
--   and manually set the signed data output signals to radix decimal
-- > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_round is
  generic (
    g_in_dat_w        : natural := 4;  -- w = g_in_dat_w, in [2]
    g_out_dat_w       : natural := 1  -- r = g_in_dat_w - g_out_dat_w, in [2]
  );
end tb_round;

architecture tb of tb_round is
  constant clk_period   : time    := 10 ns;

  constant c_pipeline_input   : natural := 0;
  constant c_pipeline_output  : natural := 0;
  constant c_pipeline         : natural := c_pipeline_input + c_pipeline_output;

  constant c_round_w          : integer := g_in_dat_w - g_out_dat_w;

  -- Expected rounded results from [2] for w = g_in_dat_w = 4 and r = c_round_w = 1
  constant c_exp_w4_r1_sreal_fixed_point             : t_real_arr(0 to 15)    := (-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5);
  constant c_exp_w4_r1_signed_truncate               : t_integer_arr(0 to 15) := (-4,   -4,   -3,   -3,   -2,   -2,   -1,   -1,   0,   0,   1,   1,   2,   2,   3,   3);
  constant c_exp_w4_r1_signed_round_half_away        : t_integer_arr(0 to 15) := (-4,   -4,   -3,   -3,   -2,   -2,   -1,   -1,   0,   1,   1,   2,   2,   3,   3,  -4);
  constant c_exp_w4_r1_signed_round_half_away_clip   : t_integer_arr(0 to 15) := (-4,   -4,   -3,   -3,   -2,   -2,   -1,   -1,   0,   1,   1,   2,   2,   3,   3,   3);
  constant c_exp_w4_r1_signed_round_half_even        : t_integer_arr(0 to 15) := (-4,   -4,   -3,   -2,   -2,   -2,   -1,    0,   0,   0,   1,   2,   2,   2,   3,  -4);
  constant c_exp_w4_r1_signed_round_half_even_clip   : t_integer_arr(0 to 15) := (-4,   -4,   -3,   -2,   -2,   -2,   -1,    0,   0,   0,   1,   2,   2,   2,   3,   3);

  constant c_exp_w4_r1_unsigned_fixed_point          : t_real_arr(0 to 15)    := (0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5);
  constant c_exp_w4_r1_unsigned_truncate             : t_natural_arr(0 to 15) := (0,   0,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5,   6,   6,   7,   7);
  constant c_exp_w4_r1_unsigned_round_half_up        : t_natural_arr(0 to 15) := (0,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5,   6,   6,   7,   7,   0);
  constant c_exp_w4_r1_unsigned_round_half_up_clip   : t_natural_arr(0 to 15) := (0,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5,   6,   6,   7,   7,   7);
  constant c_exp_w4_r1_unsigned_round_half_even      : t_natural_arr(0 to 15) := (0,   0,   1,   2,   2,   2,   3,   4,   4,   4,   5,   6,   6,   6,   7,   0);
  constant c_exp_w4_r1_unsigned_round_half_even_clip : t_natural_arr(0 to 15) := (0,   0,   1,   2,   2,   2,   3,   4,   4,   4,   5,   6,   6,   6,   7,   7);

  -- Expected rounded results from [2] for w = g_in_dat_w = 4 and r = c_round_w = 2
  constant c_exp_w4_r2_sreal_fixed_point           : t_real_arr(0 to 15)    := (-2.0, -1.75, -1.5, -1.25, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75);
  constant c_exp_w4_r2_signed_truncate             : t_integer_arr(0 to 15) := (-2,   -2,    -2,   -2,    -1,   -1,    -1,   -1,    0,   0,    0,   0,    1,   1,    1,   1);
  constant c_exp_w4_r2_signed_round_half_away      : t_integer_arr(0 to 15) := (-2,   -2,    -2,   -1,    -1,   -1,    -1,    0,    0,   0,    1,   1,    1,   1,   -2,  -2);
  constant c_exp_w4_r2_signed_round_half_away_clip : t_integer_arr(0 to 15) := (-2,   -2,    -2,   -1,    -1,   -1,    -1,    0,    0,   0,    1,   1,    1,   1,    1,   1);
  constant c_exp_w4_r2_signed_round_half_even      : t_integer_arr(0 to 15) := (-2,   -2,    -2,   -1,    -1,   -1,     0,    0,    0,   0,    0,   1,    1,   1,   -2,  -2);
  constant c_exp_w4_r2_signed_round_half_even_clip : t_integer_arr(0 to 15) := (-2,   -2,    -2,   -1,    -1,   -1,     0,    0,    0,   0,    0,   1,    1,   1,    1,   1);

  constant c_exp_w4_r2_unsigned_fixed_point          : t_real_arr(0 to 15)    := (0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75);
  constant c_exp_w4_r2_unsigned_truncate             : t_natural_arr(0 to 15) := (0,   0,    0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3);
  constant c_exp_w4_r2_unsigned_round_half_up        : t_natural_arr(0 to 15) := (0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    0,   0);
  constant c_exp_w4_r2_unsigned_round_half_up_clip   : t_natural_arr(0 to 15) := (0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    3,   3);
  constant c_exp_w4_r2_unsigned_round_half_even      : t_natural_arr(0 to 15) := (0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,    0,   0);
  constant c_exp_w4_r2_unsigned_round_half_even_clip : t_natural_arr(0 to 15) := (0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,    3,   3);

  -- Expected rounded results from [2] for w = g_in_dat_w = 4 and r = c_round_w = 3
  constant c_exp_w4_r3_sreal_fixed_point           : t_real_arr(0 to 15)    := (-1.0, -0.875, -0.75, -0.625, -0.5, -0.375, -0.25, -0.125, 0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875);
  constant c_exp_w4_r3_signed_truncate             : t_integer_arr(0 to 15) := (-1,   -1,     -1,    -1,     -1,   -1,     -1,    -1,     0,   0,     0,    0,     0,   0,     0,    0);
  constant c_exp_w4_r3_signed_round_half_away      : t_integer_arr(0 to 15) := (-1,   -1,     -1,    -1,     -1,    0,      0,     0,     0,   0,     0,    0,    -1,  -1,    -1,   -1);
  constant c_exp_w4_r3_signed_round_half_away_clip : t_integer_arr(0 to 15) := (-1,   -1,     -1,    -1,     -1,    0,      0,     0,     0,   0,     0,    0,     0,   0,     0,    0);
  constant c_exp_w4_r3_signed_round_half_even      : t_integer_arr(0 to 15) := (-1,   -1,     -1,    -1,      0,    0,      0,     0,     0,   0,     0,    0,     0,  -1,    -1,   -1);
  constant c_exp_w4_r3_signed_round_half_even_clip : t_integer_arr(0 to 15) := (-1,   -1,     -1,    -1,      0,    0,      0,     0,     0,   0,     0,    0,     0,   0,     0,    0);

  constant c_exp_w4_r3_unsigned_fixed_point          : t_real_arr(0 to 15)    := (0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0, 1.125, 1.25, 1.375, 1.5, 1.625, 1.75, 1.875);
  constant c_exp_w4_r3_unsigned_truncate             : t_natural_arr(0 to 15) := (0,   0,     0,    0,     0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1);
  constant c_exp_w4_r3_unsigned_round_half_up        : t_natural_arr(0 to 15) := (0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     0,   0,     0,    0);
  constant c_exp_w4_r3_unsigned_round_half_up_clip   : t_natural_arr(0 to 15) := (0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     1,   1,     1,    1);
  constant c_exp_w4_r3_unsigned_round_half_even      : t_natural_arr(0 to 15) := (0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,     0,   0,     0,    0);
  constant c_exp_w4_r3_unsigned_round_half_even_clip : t_natural_arr(0 to 15) := (0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,     1,   1,     1,    1);

  -- Expected rounded results from [2] for w = g_in_dat_w = 5 and r = c_round_w = 2
  constant c_exp_w5_r2_sreal_fixed_point             : t_real_arr(0 to 31)    := (-4.0, -3.75, -3.5, -3.25, -3.0, -2.75, -2.5, -2.25, -2.0, -1.75, -1.5, -1.25, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75);
  constant c_exp_w5_r2_signed_truncate               : t_integer_arr(0 to 31) := (-4,   -4,    -4,   -4,    -3,   -3,    -3,   -3,    -2,   -2,    -2,   -2,    -1,   -1,    -1,   -1,    0,   0,    0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3);
  constant c_exp_w5_r2_signed_round_half_away        : t_integer_arr(0 to 31) := (-4,   -4,    -4,   -3,    -3,   -3,    -3,   -2,    -2,   -2,    -2,   -1,    -1,   -1,    -1,    0,    0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,   -4,  -4);
  constant c_exp_w5_r2_signed_round_half_away_clip   : t_integer_arr(0 to 31) := (-4,   -4,    -4,   -3,    -3,   -3,    -3,   -2,    -2,   -2,    -2,   -1,    -1,   -1,    -1,    0,    0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    3,   3);
  constant c_exp_w5_r2_signed_round_half_even        : t_integer_arr(0 to 31) := (-4,   -4,    -4,   -3,    -3,   -3,    -2,   -2,    -2,   -2,    -2,   -1,    -1,   -1,     0,    0,    0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,   -4,  -4);
  constant c_exp_w5_r2_signed_round_half_even_clip   : t_integer_arr(0 to 31) := (-4,   -4,    -4,   -3,    -3,   -3,    -2,   -2,    -2,   -2,    -2,   -1,    -1,   -1,     0,    0,    0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,    3,   3);

  constant c_exp_w5_r2_unsigned_fixed_point          : t_real_arr(0 to 31)    := (0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5, 4.75, 5.0, 5.25, 5.5, 5.75, 6.0, 6.25, 6.5, 6.75, 7.0, 7.25, 7.5, 7.75);
  constant c_exp_w5_r2_unsigned_truncate             : t_natural_arr(0 to 31) := (0,   0,    0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    4,   4,    4,   4,    5,   5,    5,   5,    6,   6,    6,   6,    7,   7,    7,   7);
  constant c_exp_w5_r2_unsigned_round_half_up        : t_natural_arr(0 to 31) := (0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    4,   4,    4,   4,    5,   5,    5,   5,    6,   6,    6,   6,    7,   7,    7,   7,    0,   0);
  constant c_exp_w5_r2_unsigned_round_half_up_clip   : t_natural_arr(0 to 31) := (0,   0,    1,   1,    1,   1,    2,   2,    2,   2,    3,   3,    3,   3,    4,   4,    4,   4,    5,   5,    5,   5,    6,   6,    6,   6,    7,   7,    7,   7,    7,   7);
  constant c_exp_w5_r2_unsigned_round_half_even      : t_natural_arr(0 to 31) := (0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,    4,   4,    4,   4,    4,   5,    5,   5,    6,   6,    6,   6,    6,   7,    7,   7,    0,   0);
  constant c_exp_w5_r2_unsigned_round_half_even_clip : t_natural_arr(0 to 31) := (0,   0,    0,   1,    1,   1,    2,   2,    2,   2,    2,   3,    3,   3,    4,   4,    4,   4,    4,   5,    5,   5,    6,   6,    6,   6,    6,   7,    7,   7,    7,   7);

  -- Expected rounded results from [2] for w = g_in_dat_w = 5 and r = c_round_w = 3
  constant c_exp_w5_r3_sreal_fixed_point           : t_real_arr(0 to 31)    := (-2.0, -1.875, -1.75, -1.625, -1.5, -1.375, -1.25, -1.125, -1.0, -0.875, -0.75, -0.625, -0.5, -0.375, -0.25, -0.125, 0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0, 1.125, 1.25, 1.375, 1.5, 1.625, 1.75, 1.875);
  constant c_exp_w5_r3_signed_truncate             : t_integer_arr(0 to 31) := (-2,   -2,     -2,    -2,     -2,   -2,     -2,    -2,     -1,   -1,     -1,    -1,     -1,   -1,     -1,    -1,     0,   0,     0,    0,     0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1);
  constant c_exp_w5_r3_signed_round_half_away      : t_integer_arr(0 to 31) := (-2,   -2,     -2,    -2,     -2,   -1,     -1,    -1,     -1,   -1,     -1,    -1,     -1,    0,      0,     0,     0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,    -2,  -2,    -2,   -2);
  constant c_exp_w5_r3_signed_round_half_away_clip : t_integer_arr(0 to 31) := (-2,   -2,     -2,    -2,     -2,   -1,     -1,    -1,     -1,   -1,     -1,    -1,     -1,    0,      0,     0,     0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     1,   1,     1,    1);
  constant c_exp_w5_r3_signed_round_half_even      : t_integer_arr(0 to 31) := (-2,   -2,     -2,    -2,     -2,   -1,     -1,    -1,     -1,   -1,     -1,    -1,      0,    0,      0,     0,     0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,    -2,  -2,    -2,   -2);
  constant c_exp_w5_r3_signed_round_half_even_clip : t_integer_arr(0 to 31) := (-2,   -2,     -2,    -2,     -2,   -1,     -1,    -1,     -1,   -1,     -1,    -1,      0,    0,      0,     0,     0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,     1,   1,     1,    1);

  constant c_exp_w5_r3_unsigned_fixed_point          : t_real_arr(0 to 31)    := (0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0, 1.125, 1.25, 1.375, 1.5, 1.625, 1.75, 1.875, 2.0, 2.125, 2.25, 2.375, 2.5, 2.625, 2.75, 2.875, 3.0, 3.125, 3.25, 3.375, 3.5, 3.625, 3.75, 3.875);
  constant c_exp_w5_r3_unsigned_truncate             : t_natural_arr(0 to 31) := (0,   0,     0,    0,     0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     2,   2,     2,    2,     2,   2,     2,    2,     3,   3,     3,    3,     3,   3,     3,    3);
  constant c_exp_w5_r3_unsigned_round_half_up        : t_natural_arr(0 to 31) := (0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     2,   2,     2,    2,     2,   2,     2,    2,     3,   3,     3,    3,     3,   3,     3,    3,     0,   0,     0,    0);
  constant c_exp_w5_r3_unsigned_round_half_up_clip   : t_natural_arr(0 to 31) := (0,   0,     0,    0,     1,   1,     1,    1,     1,   1,     1,    1,     2,   2,     2,    2,     2,   2,     2,    2,     3,   3,     3,    3,     3,   3,     3,    3,     3,   3,     3,    3);
  constant c_exp_w5_r3_unsigned_round_half_even      : t_natural_arr(0 to 31) := (0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,     2,   2,     2,    2,     2,   2,     2,    2,     2,   3,     3,    3,     3,   3,     3,    3,     0,   0,     0,    0);
  constant c_exp_w5_r3_unsigned_round_half_even_clip : t_natural_arr(0 to 31) := (0,   0,     0,    0,     0,   1,     1,    1,     1,   1,     1,    1,     2,   2,     2,    2,     2,   2,     2,    2,     2,   3,     3,    3,     3,   3,     3,    3,     3,   3,     3,    3);

  signal tb_end               : std_logic := '0';
  signal clk                  : std_logic := '1';

  -- Input data
  signal in_val               : std_logic;
  signal in_dat               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_val              : std_logic;
  signal reg_dat              : std_logic_vector(g_in_dat_w - 1 downto 0);

  -- Signed output data
  -- . view as radix decimal in Wave window
  signal fs_signed_integer                : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal fs_signed_truncate               : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_signed_round_half_away        : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_signed_round_half_away_clip   : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_signed_round_half_even        : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_signed_round_half_even_clip   : std_logic_vector(g_out_dat_w - 1 downto 0);

  signal S_w4                                    : natural;  -- lookup index for signed
  signal exp_w4_r1_signed_truncate               : integer;
  signal exp_w4_r1_signed_round_half_away        : integer;
  signal exp_w4_r1_signed_round_half_away_clip   : integer;
  signal exp_w4_r1_signed_round_half_even        : integer;
  signal exp_w4_r1_signed_round_half_even_clip   : integer;

  signal exp_w4_r2_signed_truncate               : integer;
  signal exp_w4_r2_signed_round_half_away        : integer;
  signal exp_w4_r2_signed_round_half_away_clip   : integer;
  signal exp_w4_r2_signed_round_half_even        : integer;
  signal exp_w4_r2_signed_round_half_even_clip   : integer;

  signal exp_w4_r3_signed_truncate               : integer;
  signal exp_w4_r3_signed_round_half_away        : integer;
  signal exp_w4_r3_signed_round_half_away_clip   : integer;
  signal exp_w4_r3_signed_round_half_even        : integer;
  signal exp_w4_r3_signed_round_half_even_clip   : integer;

  signal S_w5                                    : natural;  -- lookup index for signed
  signal exp_w5_r2_signed_truncate               : integer;
  signal exp_w5_r2_signed_round_half_away        : integer;
  signal exp_w5_r2_signed_round_half_away_clip   : integer;
  signal exp_w5_r2_signed_round_half_even        : integer;
  signal exp_w5_r2_signed_round_half_even_clip   : integer;

  signal exp_w5_r3_signed_truncate               : integer;
  signal exp_w5_r3_signed_round_half_away        : integer;
  signal exp_w5_r3_signed_round_half_away_clip   : integer;
  signal exp_w5_r3_signed_round_half_even        : integer;
  signal exp_w5_r3_signed_round_half_even_clip   : integer;

  -- . show as real in Wave window
  signal fs_sreal_fixed_point             : real := 0.0;
  signal fs_sreal_truncate                : real := 0.0;
  signal fs_sreal_round_half_away         : real := 0.0;
  signal fs_sreal_round_half_away_clip    : real := 0.0;
  signal fs_sreal_round_half_even         : real := 0.0;
  signal fs_sreal_round_half_even_clip    : real := 0.0;

  -- Unsigned output data
  -- . view as radix unsigned in Wave window
  signal fs_unsigned_integer              : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal fs_unsigned_truncate             : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_unsigned_round_half_up        : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_unsigned_round_half_up_clip   : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_unsigned_round_half_even      : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal fs_unsigned_round_half_even_clip : std_logic_vector(g_out_dat_w - 1 downto 0);

  signal U_w4                                    : natural;  -- lookup index for unsigned
  signal exp_w4_r1_unsigned_truncate             : integer;
  signal exp_w4_r1_unsigned_round_half_up        : integer;
  signal exp_w4_r1_unsigned_round_half_up_clip   : integer;
  signal exp_w4_r1_unsigned_round_half_even      : integer;
  signal exp_w4_r1_unsigned_round_half_even_clip : integer;

  signal exp_w4_r2_unsigned_truncate             : integer;
  signal exp_w4_r2_unsigned_round_half_up        : integer;
  signal exp_w4_r2_unsigned_round_half_up_clip   : integer;
  signal exp_w4_r2_unsigned_round_half_even      : integer;
  signal exp_w4_r2_unsigned_round_half_even_clip : integer;

  signal exp_w4_r3_unsigned_truncate             : integer;
  signal exp_w4_r3_unsigned_round_half_up        : integer;
  signal exp_w4_r3_unsigned_round_half_up_clip   : integer;
  signal exp_w4_r3_unsigned_round_half_even      : integer;
  signal exp_w4_r3_unsigned_round_half_even_clip : integer;

  signal U_w5                                    : natural;  -- lookup index for unsigned
  signal exp_w5_r2_unsigned_truncate             : integer;
  signal exp_w5_r2_unsigned_round_half_up        : integer;
  signal exp_w5_r2_unsigned_round_half_up_clip   : integer;
  signal exp_w5_r2_unsigned_round_half_even      : integer;
  signal exp_w5_r2_unsigned_round_half_even_clip : integer;

  signal exp_w5_r3_unsigned_truncate             : integer;
  signal exp_w5_r3_unsigned_round_half_up        : integer;
  signal exp_w5_r3_unsigned_round_half_up_clip   : integer;
  signal exp_w5_r3_unsigned_round_half_even      : integer;
  signal exp_w5_r3_unsigned_round_half_even_clip : integer;

  -- . show as real in Wave window
  signal fs_ureal_fixed_point             : real := 0.0;
  signal fs_ureal_truncate                : real := 0.0;
  signal fs_ureal_round_half_up           : real := 0.0;
  signal fs_ureal_round_half_up_clip      : real := 0.0;
  signal fs_ureal_round_half_even         : real := 0.0;
  signal fs_ureal_round_half_even_clip    : real := 0.0;
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;

  p_stimuli : process
  begin
    in_val <= '0';
    in_dat <= (others => '0');
    for I in 0 to 3 loop wait until rising_edge(clk); end loop;
    in_val <= '1';
    wait until rising_edge(clk);
    for I in 0 to 2**g_in_dat_w - 1 loop
      in_dat <= INCR_UVEC(in_dat, 1);
      wait until rising_edge(clk);
    end loop;
    in_val <= '0';
    in_dat <= (others => '0');
    for I in 0 to 3 loop wait until rising_edge(clk); end loop;
    tb_end <= '1';
    wait;
  end process;

  -- Delay input as much as DUT output, assume c_pipeline = 0
  reg_val <= in_val;
  reg_dat <= in_dat;

  fs_signed_integer <= reg_dat;
  fs_unsigned_integer <= reg_dat;

  -----------------------------------------------------------------------------
  -- SIGNED DUTs
  -----------------------------------------------------------------------------

  s_truncate : entity work.common_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => false,
    g_round_clip      => false,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_signed_truncate
  );

  s_round_half_away : entity work.common_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_signed_round_half_away
  );

  s_round_half_away_clip : entity work.common_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_signed_round_half_away_clip
  );

  s_round_half_even : entity work.common_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_round_even      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_signed_round_half_even
  );

  s_round_half_even_clip : entity work.common_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => true,
    g_round_even      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_signed_round_half_even_clip
  );

  -----------------------------------------------------------------------------
  -- UNSIGNED DUTs
  -----------------------------------------------------------------------------

  -- DUT for truncate
  u_truncate : entity work.common_round
  generic map (
    g_representation  => "UNSIGNED",
    g_round           => false,
    g_round_clip      => false,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_unsigned_truncate
  );

  u_round_half_up : entity work.common_round
  generic map (
    g_representation  => "UNSIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_unsigned_round_half_up
  );

  u_round_half_up_clip : entity work.common_round
  generic map (
    g_representation  => "UNSIGNED",
    g_round           => true,
    g_round_clip      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_unsigned_round_half_up_clip
  );

  u_round_half_even : entity work.common_round
  generic map (
    g_representation  => "UNSIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_round_even      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_unsigned_round_half_even
  );

  u_round_half_even_clip : entity work.common_round
  generic map (
    g_representation  => "UNSIGNED",
    g_round           => true,
    g_round_clip      => true,
    g_round_even      => true,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => fs_unsigned_round_half_even_clip
  );

  -- Observe fixed point SLV values as REAL
  -- . signed
  fs_sreal_fixed_point          <= TO_SREAL(fs_signed_integer, c_round_w);
  fs_sreal_truncate             <= TO_SREAL(fs_signed_truncate, 0);
  fs_sreal_round_half_away      <= TO_SREAL(fs_signed_round_half_away, 0);
  fs_sreal_round_half_away_clip <= TO_SREAL(fs_signed_round_half_away_clip, 0);
  fs_sreal_round_half_even      <= TO_SREAL(fs_signed_round_half_even, 0);
  fs_sreal_round_half_even_clip <= TO_SREAL(fs_signed_round_half_even_clip, 0);

  -- . unsigned
  fs_ureal_fixed_point          <= TO_UREAL(fs_unsigned_integer, c_round_w);
  fs_ureal_truncate             <= TO_UREAL(fs_unsigned_truncate, 0);
  fs_ureal_round_half_up        <= TO_UREAL(fs_unsigned_round_half_up, 0);
  fs_ureal_round_half_up_clip   <= TO_UREAL(fs_unsigned_round_half_up_clip, 0);
  fs_ureal_round_half_even      <= TO_UREAL(fs_unsigned_round_half_even, 0);
  fs_ureal_round_half_even_clip <= TO_UREAL(fs_unsigned_round_half_even_clip, 0);

  -- Expected rounded values
  -- . w = 4
  S_w4 <= (TO_UINT(in_dat) + 8) mod 16;  -- 2**4 = 16
  U_w4 <= TO_UINT(in_dat) mod 16;

  -- . w = 4, r = 1
  exp_w4_r1_signed_truncate             <= c_exp_w4_r1_signed_truncate(S_w4);
  exp_w4_r1_signed_round_half_away      <= c_exp_w4_r1_signed_round_half_away(S_w4);
  exp_w4_r1_signed_round_half_away_clip <= c_exp_w4_r1_signed_round_half_away_clip(S_w4);
  exp_w4_r1_signed_round_half_even      <= c_exp_w4_r1_signed_round_half_even(S_w4);
  exp_w4_r1_signed_round_half_even_clip <= c_exp_w4_r1_signed_round_half_even_clip(S_w4);

  exp_w4_r1_unsigned_truncate             <= c_exp_w4_r1_unsigned_truncate(U_w4);
  exp_w4_r1_unsigned_round_half_up        <= c_exp_w4_r1_unsigned_round_half_up(U_w4);
  exp_w4_r1_unsigned_round_half_up_clip   <= c_exp_w4_r1_unsigned_round_half_up_clip(U_w4);
  exp_w4_r1_unsigned_round_half_even      <= c_exp_w4_r1_unsigned_round_half_even(U_w4);
  exp_w4_r1_unsigned_round_half_even_clip <= c_exp_w4_r1_unsigned_round_half_even_clip(U_w4);

  -- . w = 4, r = 2
  exp_w4_r2_signed_truncate             <= c_exp_w4_r2_signed_truncate(S_w4);
  exp_w4_r2_signed_round_half_away      <= c_exp_w4_r2_signed_round_half_away(S_w4);
  exp_w4_r2_signed_round_half_away_clip <= c_exp_w4_r2_signed_round_half_away_clip(S_w4);
  exp_w4_r2_signed_round_half_even      <= c_exp_w4_r2_signed_round_half_even(S_w4);
  exp_w4_r2_signed_round_half_even_clip <= c_exp_w4_r2_signed_round_half_even_clip(S_w4);

  exp_w4_r2_unsigned_truncate             <= c_exp_w4_r2_unsigned_truncate(U_w4);
  exp_w4_r2_unsigned_round_half_up        <= c_exp_w4_r2_unsigned_round_half_up(U_w4);
  exp_w4_r2_unsigned_round_half_up_clip   <= c_exp_w4_r2_unsigned_round_half_up_clip(U_w4);
  exp_w4_r2_unsigned_round_half_even      <= c_exp_w4_r2_unsigned_round_half_even(U_w4);
  exp_w4_r2_unsigned_round_half_even_clip <= c_exp_w4_r2_unsigned_round_half_even_clip(U_w4);

  -- . w = 4, r = 3
  exp_w4_r3_signed_truncate             <= c_exp_w4_r3_signed_truncate(S_w4);
  exp_w4_r3_signed_round_half_away      <= c_exp_w4_r3_signed_round_half_away(S_w4);
  exp_w4_r3_signed_round_half_away_clip <= c_exp_w4_r3_signed_round_half_away_clip(S_w4);
  exp_w4_r3_signed_round_half_even      <= c_exp_w4_r3_signed_round_half_even(S_w4);
  exp_w4_r3_signed_round_half_even_clip <= c_exp_w4_r3_signed_round_half_even_clip(S_w4);

  exp_w4_r3_unsigned_truncate             <= c_exp_w4_r3_unsigned_truncate(U_w4);
  exp_w4_r3_unsigned_round_half_up        <= c_exp_w4_r3_unsigned_round_half_up(U_w4);
  exp_w4_r3_unsigned_round_half_up_clip   <= c_exp_w4_r3_unsigned_round_half_up_clip(U_w4);
  exp_w4_r3_unsigned_round_half_even      <= c_exp_w4_r3_unsigned_round_half_even(U_w4);
  exp_w4_r3_unsigned_round_half_even_clip <= c_exp_w4_r3_unsigned_round_half_even_clip(U_w4);

  -- . w = 5
  S_w5 <= (TO_UINT(in_dat) + 16) mod 32;  -- 2**5 = 32
  U_w5 <= TO_UINT(in_dat) mod 32;

  -- . w = 5, r = 2
  exp_w5_r2_signed_truncate             <= c_exp_w5_r2_signed_truncate(S_w5);
  exp_w5_r2_signed_round_half_away      <= c_exp_w5_r2_signed_round_half_away(S_w5);
  exp_w5_r2_signed_round_half_away_clip <= c_exp_w5_r2_signed_round_half_away_clip(S_w5);
  exp_w5_r2_signed_round_half_even      <= c_exp_w5_r2_signed_round_half_even(S_w5);
  exp_w5_r2_signed_round_half_even_clip <= c_exp_w5_r2_signed_round_half_even_clip(S_w5);

  exp_w5_r2_unsigned_truncate             <= c_exp_w5_r2_unsigned_truncate(U_w5);
  exp_w5_r2_unsigned_round_half_up        <= c_exp_w5_r2_unsigned_round_half_up(U_w5);
  exp_w5_r2_unsigned_round_half_up_clip   <= c_exp_w5_r2_unsigned_round_half_up_clip(U_w5);
  exp_w5_r2_unsigned_round_half_even      <= c_exp_w5_r2_unsigned_round_half_even(U_w5);
  exp_w5_r2_unsigned_round_half_even_clip <= c_exp_w5_r2_unsigned_round_half_even_clip(U_w5);

  -- . w = 5, r = 3
  exp_w5_r3_signed_truncate             <= c_exp_w5_r3_signed_truncate(S_w5);
  exp_w5_r3_signed_round_half_away      <= c_exp_w5_r3_signed_round_half_away(S_w5);
  exp_w5_r3_signed_round_half_away_clip <= c_exp_w5_r3_signed_round_half_away_clip(S_w5);
  exp_w5_r3_signed_round_half_even      <= c_exp_w5_r3_signed_round_half_even(S_w5);
  exp_w5_r3_signed_round_half_even_clip <= c_exp_w5_r3_signed_round_half_even_clip(S_w5);

  exp_w5_r3_unsigned_truncate             <= c_exp_w5_r3_unsigned_truncate(U_w5);
  exp_w5_r3_unsigned_round_half_up        <= c_exp_w5_r3_unsigned_round_half_up(U_w5);
  exp_w5_r3_unsigned_round_half_up_clip   <= c_exp_w5_r3_unsigned_round_half_up_clip(U_w5);
  exp_w5_r3_unsigned_round_half_even      <= c_exp_w5_r3_unsigned_round_half_even(U_w5);
  exp_w5_r3_unsigned_round_half_even_clip <= c_exp_w5_r3_unsigned_round_half_even_clip(U_w5);

  -- Verification
  p_verify : process
  begin
    wait until rising_edge(clk);
    if reg_val = '1' then
      if c_round_w <= 0 then
        -- Without rounding the expected value is same as input value
        -- . signed
        assert signed(fs_signed_truncate ) = signed(reg_dat)
          report "Wrong wired fs_signed_truncate"
          severity ERROR;
        assert signed(fs_signed_round_half_away ) = signed(reg_dat)
          report "Wrong wired fs_signed_round_half_away"
          severity ERROR;
        assert signed(fs_signed_round_half_away_clip) = signed(reg_dat)
          report "Wrong wired fs_signed_round_half_away_clip"
          severity ERROR;
        assert signed(fs_signed_round_half_even ) = signed(reg_dat)
          report "Wrong wired fs_signed_round_half_even"
          severity ERROR;
        assert signed(fs_signed_round_half_even_clip) = signed(reg_dat)
          report "Wrong wired fs_signed_round_half_even_clip"
          severity ERROR;
        -- . unsigned
        assert unsigned(fs_unsigned_truncate ) = unsigned(reg_dat)
          report "Wrong wired fs_unsigned_truncate"
          severity ERROR;
        assert unsigned(fs_unsigned_round_half_up ) = unsigned(reg_dat)
          report "Wrong wired fs_unsigned_round_half_up"
          severity ERROR;
        assert unsigned(fs_unsigned_round_half_up_clip ) = unsigned(reg_dat)
          report "Wrong wired fs_unsigned_round_half_up_clip"
          severity ERROR;
        assert unsigned(fs_unsigned_round_half_even ) = unsigned(reg_dat)
          report "Wrong wired fs_unsigned_round_half_even"
          severity ERROR;
        assert unsigned(fs_unsigned_round_half_even_clip) = unsigned(reg_dat)
          report "Wrong wired fs_unsigned_round_half_even_clip"
          severity ERROR;
      else
        -- With rounding then compare with expected list of values from common_round_tb.py
        if g_in_dat_w = 4 and c_round_w = 1 then
          -- . signed
          assert signed(fs_signed_truncate ) = exp_w4_r1_signed_truncate
            report "Wrong exp_w4_r1_signed_truncate"
            severity ERROR;
          assert signed(fs_signed_round_half_away ) = exp_w4_r1_signed_round_half_away
            report "Wrong exp_w4_r1_signed_round_half_away"
            severity ERROR;
          assert signed(fs_signed_round_half_away_clip) = exp_w4_r1_signed_round_half_away_clip
            report "Wrong exp_w4_r1_signed_round_half_away_clip"
            severity ERROR;
          assert signed(fs_signed_round_half_even ) = exp_w4_r1_signed_round_half_even
            report "Wrong exp_w4_r1_signed_round_half_even"
            severity ERROR;
          assert signed(fs_signed_round_half_even_clip) = exp_w4_r1_signed_round_half_even_clip
            report "Wrong exp_w4_r1_signed_round_half_even_clip"
            severity ERROR;
          -- . unsigned
          assert unsigned(fs_unsigned_truncate ) = exp_w4_r1_unsigned_truncate
            report "Wrong exp_w4_r1_unsigned_truncate"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up ) = exp_w4_r1_unsigned_round_half_up
            report "Wrong exp_w4_r1_unsigned_round_half_up"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up_clip ) = exp_w4_r1_unsigned_round_half_up_clip
            report "Wrong exp_w4_r1_unsigned_round_half_up_clip"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even ) = exp_w4_r1_unsigned_round_half_even
            report "Wrong exp_w4_r1_unsigned_round_half_even"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even_clip) = exp_w4_r1_unsigned_round_half_even_clip
            report "Wrong exp_w4_r1_unsigned_round_half_even_clip"
            severity ERROR;
        end if;
        if g_in_dat_w = 4 and c_round_w = 2 then
          -- . signed
          assert signed(fs_signed_truncate ) = exp_w4_r2_signed_truncate
            report "Wrong exp_w4_r2_signed_truncate"
            severity ERROR;
          assert signed(fs_signed_round_half_away ) = exp_w4_r2_signed_round_half_away
            report "Wrong exp_w4_r2_signed_round_half_away"
            severity ERROR;
          assert signed(fs_signed_round_half_away_clip) = exp_w4_r2_signed_round_half_away_clip
            report "Wrong exp_w4_r2_signed_round_half_away_clip"
            severity ERROR;
          assert signed(fs_signed_round_half_even ) = exp_w4_r2_signed_round_half_even
            report "Wrong exp_w4_r2_signed_round_half_even"
            severity ERROR;
          assert signed(fs_signed_round_half_even_clip) = exp_w4_r2_signed_round_half_even_clip
            report "Wrong exp_w4_r2_signed_round_half_even_clip"
            severity ERROR;
          -- . unsigned
          assert unsigned(fs_unsigned_truncate ) = exp_w4_r2_unsigned_truncate
            report "Wrong exp_w4_r2_unsigned_truncate"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up ) = exp_w4_r2_unsigned_round_half_up
            report "Wrong exp_w4_r2_unsigned_round_half_up"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up_clip ) = exp_w4_r2_unsigned_round_half_up_clip
            report "Wrong exp_w4_r2_unsigned_round_half_up_clip"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even ) = exp_w4_r2_unsigned_round_half_even
            report "Wrong exp_w4_r2_unsigned_round_half_even"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even_clip) = exp_w4_r2_unsigned_round_half_even_clip
            report "Wrong exp_w4_r2_unsigned_round_half_even_clip"
            severity ERROR;
        end if;
        if g_in_dat_w = 4 and c_round_w = 3 then
          -- . signed
          assert signed(fs_signed_truncate ) = exp_w4_r3_signed_truncate
            report "Wrong exp_w4_r3_signed_truncate"
            severity ERROR;
          assert signed(fs_signed_round_half_away ) = exp_w4_r3_signed_round_half_away
            report "Wrong exp_w4_r3_signed_round_half_away"
            severity ERROR;
          assert signed(fs_signed_round_half_away_clip) = exp_w4_r3_signed_round_half_away_clip
            report "Wrong exp_w4_r3_signed_round_half_away_clip"
            severity ERROR;
          assert signed(fs_signed_round_half_even ) = exp_w4_r3_signed_round_half_even
            report "Wrong exp_w4_r3_signed_round_half_even"
            severity ERROR;
          assert signed(fs_signed_round_half_even_clip) = exp_w4_r3_signed_round_half_even_clip
            report "Wrong exp_w4_r3_signed_round_half_even_clip"
            severity ERROR;
          -- . unsigned
          assert unsigned(fs_unsigned_truncate ) = exp_w4_r3_unsigned_truncate
            report "Wrong exp_w4_r3_unsigned_truncate"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up ) = exp_w4_r3_unsigned_round_half_up
            report "Wrong exp_w4_r3_unsigned_round_half_up"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up_clip ) = exp_w4_r3_unsigned_round_half_up_clip
            report "Wrong exp_w4_r3_unsigned_round_half_up_clip"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even ) = exp_w4_r3_unsigned_round_half_even
            report "Wrong exp_w4_r3_unsigned_round_half_even"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even_clip) = exp_w4_r3_unsigned_round_half_even_clip
            report "Wrong exp_w4_r3_unsigned_round_half_even_clip"
            severity ERROR;
        end if;
        if g_in_dat_w = 5 and c_round_w = 2 then
          -- . signed
          assert signed(fs_signed_truncate ) = exp_w5_r2_signed_truncate
            report "Wrong exp_w5_r2_signed_truncate"
            severity ERROR;
          assert signed(fs_signed_round_half_away ) = exp_w5_r2_signed_round_half_away
            report "Wrong exp_w5_r2_signed_round_half_away"
            severity ERROR;
          assert signed(fs_signed_round_half_away_clip) = exp_w5_r2_signed_round_half_away_clip
            report "Wrong exp_w5_r2_signed_round_half_away_clip"
            severity ERROR;
          assert signed(fs_signed_round_half_even ) = exp_w5_r2_signed_round_half_even
            report "Wrong exp_w5_r2_signed_round_half_even"
            severity ERROR;
          assert signed(fs_signed_round_half_even_clip) = exp_w5_r2_signed_round_half_even_clip
            report "Wrong exp_w5_r2_signed_round_half_even_clip"
            severity ERROR;
          -- . unsigned
          assert unsigned(fs_unsigned_truncate ) = exp_w5_r2_unsigned_truncate
            report "Wrong exp_w5_r2_unsigned_truncate"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up ) = exp_w5_r2_unsigned_round_half_up
            report "Wrong exp_w5_r2_unsigned_round_half_up"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up_clip ) = exp_w5_r2_unsigned_round_half_up_clip
            report "Wrong exp_w5_r2_unsigned_round_half_up_clip"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even ) = exp_w5_r2_unsigned_round_half_even
            report "Wrong exp_w5_r2_unsigned_round_half_even"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even_clip) = exp_w5_r2_unsigned_round_half_even_clip
            report "Wrong exp_w5_r2_unsigned_round_half_even_clip"
            severity ERROR;
        end if;
        if g_in_dat_w = 5 and c_round_w = 3 then
          -- . signed
          assert signed(fs_signed_truncate ) = exp_w5_r3_signed_truncate
            report "Wrong exp_w5_r3_signed_truncate"
            severity ERROR;
          assert signed(fs_signed_round_half_away ) = exp_w5_r3_signed_round_half_away
            report "Wrong exp_w5_r3_signed_round_half_away"
            severity ERROR;
          assert signed(fs_signed_round_half_away_clip) = exp_w5_r3_signed_round_half_away_clip
            report "Wrong exp_w5_r3_signed_round_half_away_clip"
            severity ERROR;
          assert signed(fs_signed_round_half_even ) = exp_w5_r3_signed_round_half_even
            report "Wrong exp_w5_r3_signed_round_half_even"
            severity ERROR;
          assert signed(fs_signed_round_half_even_clip) = exp_w5_r3_signed_round_half_even_clip
            report "Wrong exp_w5_r3_signed_round_half_even_clip"
            severity ERROR;
          -- . unsigned
          assert unsigned(fs_unsigned_truncate ) = exp_w5_r3_unsigned_truncate
            report "Wrong exp_w5_r3_unsigned_truncate"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up ) = exp_w5_r3_unsigned_round_half_up
            report "Wrong exp_w5_r3_unsigned_round_half_up"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_up_clip ) = exp_w5_r3_unsigned_round_half_up_clip
            report "Wrong exp_w5_r3_unsigned_round_half_up_clip"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even ) = exp_w5_r3_unsigned_round_half_even
            report "Wrong exp_w5_r3_unsigned_round_half_even"
            severity ERROR;
          assert unsigned(fs_unsigned_round_half_even_clip) = exp_w5_r3_unsigned_round_half_even_clip
            report "Wrong exp_w5_r3_unsigned_round_half_even_clip"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;
end tb;
