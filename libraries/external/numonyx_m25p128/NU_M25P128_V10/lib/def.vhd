--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
--
-- generic definition for all VHDL model
LIBRARY IEEE;
    Use IEEE.std_logic_1164.all;

package Def is 
  Constant HIGH     : Integer := 1;
  Constant LOW      : Integer := 0;
  Constant UNLOCKED : Std_Logic := '0';
  Constant LOCKED   : Std_Logic := '1';
  Constant BUSY     : Std_Logic := '0';
  Constant READY    : Std_Logic := '1';
  Subtype  BYTE_range is Integer range 7 downto 0;
  Subtype  WORD_range is Integer range 15 downto 0;
  Subtype  HIGH_range is Integer range 15 downto 8;
  Subtype  LOW_range  is Integer range 7 downto 0;
  Constant BYTENP   : Integer := 16#FF#;
end;

