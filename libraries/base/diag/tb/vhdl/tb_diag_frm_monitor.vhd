--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for diag_frm_monitor
-- Usage:
--   > as 5
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_diag_frm_monitor is
end tb_diag_frm_monitor;

architecture tb of tb_diag_frm_monitor is
  constant c_period       : time    := 100 ns;
  constant c_nof_cycles   : natural := 200;

  constant c_sel          : std_logic := '1';  -- select COUNTER sequence
  constant c_frame_len    : natural := 20;  -- >= 2, test frame length
  constant c_frame_len_w  : natural := ceil_log2(c_frame_len);
  constant c_sof_period   : natural := 35;
  constant c_sof_period_w : natural := ceil_log2(c_sof_period);
  constant c_frame_cnt_w  : natural := 32;
  constant c_dat_w        : natural := c_frame_len_w;

  signal tb_end              : std_logic := '0';
  signal rst                 : std_logic;
  signal clk                 : std_logic := '1';

  signal gen_diag_en         : std_logic;
  signal gen_diag_sel        : std_logic := c_sel;
  signal gen_diag_frame_len  : std_logic_vector(c_frame_len_w - 1 downto 0) := TO_UVEC(c_frame_len, c_frame_len_w);
  signal gen_diag_sof_period : std_logic_vector(c_sof_period_w - 1 downto 0) := TO_UVEC(c_sof_period, c_sof_period_w);
  signal gen_diag_frame_cnt  : std_logic_vector(c_frame_cnt_w - 1 downto 0);

  signal seq_req             : std_logic;
  signal seq_dat             : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev_seq_dat        : std_logic_vector(c_dat_w - 1 downto 0) := TO_UVEC(c_frame_len - 1, c_dat_w);
  signal seq_val             : std_logic;
  signal seq_sop             : std_logic;
  signal seq_eop             : std_logic;
  signal seq_error           : std_logic;

  signal mon_diag_en         : std_logic;
  signal mon_diag_frame_cnt  : std_logic_vector(c_frame_cnt_w - 1 downto 0);
  signal mon_diag_error_cnt  : std_logic_vector(c_frame_cnt_w - 1 downto 0);

  signal hld_frame_cnt       : natural;

  signal init_dat            : std_logic_vector(c_dat_w - 1 downto 0);
begin
  rst <= '1', '0' after c_period / 10;
  clk <= not clk or tb_end after c_period / 2;

  stimuli: process
  -- run 200 us
  begin
    gen_diag_sel       <= c_sel;
    gen_diag_frame_len <= TO_UVEC(c_frame_len, c_frame_len_w);
    gen_diag_en        <= '0';
    seq_req            <= '1';
    seq_error          <= '0';
    mon_diag_en        <= '0';
    wait for 10 * c_period;

    -- Keep seq_req='1'
    wait for c_period;
    gen_diag_en  <= '1';  -- Enable frames generator
    mon_diag_en  <= '1';  -- Enable frames monitor
    wait for c_nof_cycles * c_period;
    gen_diag_en  <= '0';  -- Disable frames generator
    mon_diag_en  <= '0';  -- Disable frames monitor
    wait for c_nof_cycles * c_period;
    assert unsigned(gen_diag_frame_cnt) = unsigned(mon_diag_frame_cnt)
      report "wrong gen or mon count.";
    wait for 10 * c_period;

    -- Again
    wait for c_period;
    gen_diag_en  <= '1';  -- Enable frames generator
    mon_diag_en  <= '1';  -- Enable frames monitor
    wait for c_nof_cycles * c_period;
    mon_diag_en  <= '0';  -- Disable frames monitor
    wait for 10 * c_period;
    assert unsigned(mon_diag_frame_cnt) = unsigned(gen_diag_frame_cnt)
      report "Wrong gen or mon count.";
    assert unsigned(mon_diag_error_cnt) = 0
      report "Error count should be 0.";
    hld_frame_cnt <= TO_UINT(gen_diag_frame_cnt);
    mon_diag_en  <= '1';  -- Enable frames monitor
    wait for 10 * c_period;
    seq_error    <= '1';  -- force error for at leat one rx frame
    wait for 2 * c_sof_period * c_period;
    wait for 10 * c_period;
    seq_error    <= '0';
    wait for 5 * c_nof_cycles * c_period;
    gen_diag_en  <= '0';  -- Disable frames generator
    mon_diag_en  <= '0';  -- Disable frames monitor
    assert unsigned(mon_diag_frame_cnt) = unsigned(gen_diag_frame_cnt) - hld_frame_cnt
      report "Wrong gen or mon count.";
    assert unsigned(mon_diag_error_cnt) /= 0
      report "Error count should be > 0.";
    wait for 10 * c_period;

    wait for c_nof_cycles * c_period;
    tb_end <= '1';
    wait;
  end process;

  u_frm_gen : entity work.diag_frm_generator
  generic map (
    g_sel         => c_sel,
    g_frame_len   => c_frame_len,
    g_sof_period  => c_sof_period,
    g_frame_cnt_w => c_frame_cnt_w,
    g_dat_w       => c_dat_w,
    g_symbol_w    => c_dat_w,
    g_empty       => 0
  )
  port map (
    rst             => rst,
    clk             => clk,
    clken           => '1',

    -- Static control input (connect via MM or leave open to use default)
    diag_en         => gen_diag_en,
    diag_sel        => gen_diag_sel,
    diag_frame_len  => gen_diag_frame_len,
    diag_sof_period => gen_diag_sof_period,
    diag_frame_cnt  => gen_diag_frame_cnt,

    -- ST output
    out_ready       => seq_req,
    out_dat         => seq_dat,
    out_val         => seq_val,
    out_sop         => seq_sop,
    out_eop         => seq_eop
  );

  u_frm_mon : entity work.diag_frm_monitor
  generic map (
    g_frame_cnt_w => c_frame_cnt_w
  )
  port map (
    rst             => rst,
    clk             => clk,
    clken           => '1',

    -- Static control input (connect via MM)
    diag_en         => mon_diag_en,
    diag_frame_cnt  => mon_diag_frame_cnt,
    diag_error_cnt  => mon_diag_error_cnt,

    -- ST input
    in_eop          => seq_eop,
    in_error        => seq_error
  );

  prev_seq_dat <= seq_dat when rising_edge(clk) and seq_val = '1';

  p_verify_generator : process(clk)
  begin
    if rising_edge(clk) then
      if gen_diag_en = '0' and seq_val = '0' then
        init_dat <= (others => '0');
      elsif seq_eop = '1' then
        init_dat <= INCR_UVEC(init_dat, 1);
      end if;

      if seq_sop = '1' then assert seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat)               report "Unexpected seq_sop";  end if;
      if seq_eop = '1' then assert seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat) + c_frame_len - 1 report "Unexpected seq_eop";  end if;

      if seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat)               then assert seq_sop = '1' report "Missing seq_sop";  end if;
      if seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat) + c_frame_len - 1 then assert seq_eop = '1' report "Missing seq_eop";  end if;

      if seq_sop = '1'                                          then assert unsigned(seq_dat) = unsigned(init_dat)       report "Wrong first seq_dat"; end if;
      if seq_val = '1' and unsigned(seq_dat) > unsigned(init_dat) then assert unsigned(prev_seq_dat) = unsigned(seq_dat) - 1 report "Wrong seq_dat";       end if;
    end if;
  end process;
end tb;
