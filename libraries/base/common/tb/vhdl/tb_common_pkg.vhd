-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Collection of commonly used base funtions for simulations
-- Interface:
-- . [n/a]
-- Description:
-- . More information can be found in the comments near the code.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;  -- for boolean, integer file IO
use IEEE.std_logic_textio.all;  -- for std_logic, std_logic_vector file IO
use work.common_pkg.all;

package tb_common_pkg is
  -- Constants

  -- For common_reg_r_w_dc.vhd with c_meta_delay_len = 3 and internal g_readback = TRUE a
  -- c_common_cross_clock_domain_latency = 20 is enough. With g_readback = FALSE somewhat
  -- more is needed, because the read register value has to cross the clock domain back
  -- again, so then use proc_common_wait_cross_clock_domain_latency() with c_nof_cycles
  -- = c_common_cross_clock_domain_latency * 2.
  constant c_common_cross_clock_domain_latency : natural := 20;

  -- Wait for some time or until
  procedure proc_common_wait_some_cycles(signal clk          : in  std_logic;
                                                c_nof_cycles : in  natural);

  procedure proc_common_wait_some_cycles(signal clk          : in  std_logic;
                                                c_nof_cycles : in  real);

  procedure proc_common_wait_some_cycles(signal clk_in       : in  std_logic;
                                         signal clk_out      : in  std_logic;
                                                c_nof_cycles : in  natural);

  procedure proc_common_wait_some_pulses(signal clk          : in  std_logic;
                                         signal pulse        : in  std_logic;
                                                c_nof_pulses : in  natural);

  procedure proc_common_wait_until_evt(signal clk    : in  std_logic;
                                       signal level  : in  std_logic);

  procedure proc_common_wait_until_evt(signal clk    : in  std_logic;
                                       signal level  : in  integer);

  procedure proc_common_wait_until_evt(constant c_timeout : in  natural;
                                       signal   clk       : in  std_logic;
                                       signal   level     : in  std_logic);

  procedure proc_common_wait_until_high(constant c_timeout : in  natural;
                                        signal   clk       : in  std_logic;
                                        signal   level     : in  std_logic);

  procedure proc_common_wait_until_high(signal clk    : in  std_logic;
                                        signal level  : in  std_logic);

  procedure proc_common_wait_until_clk_and_high(signal clk    : in  std_logic;
                                                signal level  : in  std_logic);

  procedure proc_common_wait_until_low(constant c_timeout : in  natural;
                                       signal   clk       : in  std_logic;
                                       signal   level     : in  std_logic);

  procedure proc_common_wait_until_low(signal clk    : in  std_logic;
                                       signal level  : in  std_logic);

  procedure proc_common_wait_until_clk_and_low(signal clk    : in  std_logic;
                                               signal level  : in  std_logic);

  procedure proc_common_wait_until_hi_lo(constant c_timeout : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   level     : in  std_logic);

  procedure proc_common_wait_until_hi_lo(signal clk    : in  std_logic;
                                         signal level  : in  std_logic);

  procedure proc_common_wait_until_lo_hi(constant c_timeout : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   level     : in  std_logic);

  procedure proc_common_wait_until_lo_hi(signal clk    : in  std_logic;
                                         signal level  : in  std_logic);

  procedure proc_common_wait_until_value(constant c_value : in  integer;
                                         signal clk       : in  std_logic;
                                         signal level     : in  integer);

  procedure proc_common_wait_until_value(constant c_value : in  integer;
                                         signal clk       : in  std_logic;
                                         signal level     : in  std_logic_vector);

  procedure proc_common_wait_until_value(constant c_timeout : in  natural;
                                         constant c_value   : in  integer;
                                         signal clk         : in  std_logic;
                                         signal level       : in  std_logic_vector);

  -- Wait until absolute simulation time NOW = c_time
  procedure proc_common_wait_until_time(signal clk      : in  std_logic;
                                        constant c_time : in  time);

  -- Exit simulation on timeout failure
  procedure proc_common_timeout_failure(constant c_timeout : in time;
                                        signal tb_end      : in std_logic);

  -- Stop simulation using severity FAILURE when g_tb_end=TRUE, else for use in multi tb report as severity NOTE
  procedure proc_common_stop_simulation(signal tb_end : in std_logic);

  procedure proc_common_stop_simulation(constant g_tb_end  : in boolean;
                                        constant g_latency : in natural;  -- latency between tb_done and tb_)end
                                        signal clk         : in std_logic;
                                        signal tb_done     : in std_logic;
                                        signal tb_end      : out std_logic);

  procedure proc_common_stop_simulation(constant g_tb_end  : in boolean;
                                        signal clk         : in std_logic;
                                        signal tb_done     : in std_logic;
                                        signal tb_end      : out std_logic);

  -- Handle stream ready signal, only support ready latency c_rl = 0 or 1.
  procedure proc_common_ready_latency(constant c_rl      : in  natural;
                                      signal   clk       : in  std_logic;
                                      signal   enable    : in  std_logic;  -- when '1' then active output when ready
                                      signal   ready     : in  std_logic;
                                      signal   out_valid : out std_logic);

  -- Wait for clock domain crossing latency, e.g. for MM readback after MM write
  procedure proc_common_wait_cross_clock_domain_latency(signal mm_clk : in  std_logic;
                                                        signal st_clk : in  std_logic;
                                                        constant c_nof_cycles : in  natural);

  procedure proc_common_wait_cross_clock_domain_latency(signal mm_clk : in  std_logic;
                                                        signal st_clk : in  std_logic);

  procedure proc_common_wait_cross_clock_domain_latency(constant c_mm_clk_period : in time;
                                                        constant c_st_clk_period : in time;
                                                        constant c_nof_cycles : in  natural);

  procedure proc_common_wait_cross_clock_domain_latency(constant c_mm_clk_period : in time;
                                                        constant c_st_clk_period : in time);

  -- Generate a single active, inactive pulse
  procedure proc_common_gen_pulse(constant c_active : in  natural;  -- pulse active for nof clk
                                  constant c_period : in  natural;  -- pulse period for nof clk
                                  constant c_level  : in  std_logic;  -- pulse level when active
                                  signal   clk      : in  std_logic;
                                  signal   pulse    : out std_logic);

  -- Pulse forever after rst was released
  procedure proc_common_gen_pulse(constant c_active : in  natural;  -- pulse active for nof clk
                                  constant c_period : in  natural;  -- pulse period for nof clk
                                  constant c_level  : in  std_logic;  -- pulse level when active
                                  signal   rst      : in  std_logic;
                                  signal   clk      : in  std_logic;
                                  signal   pulse    : out std_logic);

  -- Generate a single '1', '0' pulse
  procedure proc_common_gen_pulse(signal clk   : in  std_logic;
                                  signal pulse : out std_logic);

  -- Generate a periodic pulse with arbitrary duty cycle
  procedure proc_common_gen_duty_pulse(constant c_delay     : in  natural;  -- delay pulse for nof_clk after enable
                                       constant c_active    : in  natural;  -- pulse active for nof clk
                                       constant c_period    : in  natural;  -- pulse period for nof clk
                                       constant c_level     : in  std_logic;  -- pulse level when active
                                       signal   rst         : in  std_logic;
                                       signal   clk         : in  std_logic;
                                       signal   enable      : in  std_logic;  -- once enabled, the pulse remains enabled
                                       signal   pulse       : out std_logic);

  procedure proc_common_gen_duty_pulse(constant c_active    : in  natural;  -- pulse active for nof clk
                                       constant c_period    : in  natural;  -- pulse period for nof clk
                                       constant c_level     : in  std_logic;  -- pulse level when active
                                       signal   rst         : in  std_logic;
                                       signal   clk         : in  std_logic;
                                       signal   enable      : in  std_logic;  -- once enabled, the pulse remains enabled
                                       signal   pulse       : out std_logic);

  -- Generate counter data with valid and arbitrary increment or fixed increment=1
  procedure proc_common_gen_data(constant c_rl        : in  natural;  -- 0, 1 are supported by proc_common_ready_latency()
                                 constant c_init      : in  integer;
                                 constant c_incr      : in  integer;
                                 signal   rst         : in  std_logic;
                                 signal   clk         : in  std_logic;
                                 signal   enable      : in  std_logic;  -- when '0' then no valid output even when ready='1'
                                 signal   ready       : in  std_logic;
                                 signal   out_data    : out std_logic_vector;
                                 signal   out_valid   : out std_logic);

  procedure proc_common_gen_data(constant c_rl        : in  natural;  -- 0, 1 are supported by proc_common_ready_latency()
                                 constant c_init      : in  integer;
                                 signal   rst         : in  std_logic;
                                 signal   clk         : in  std_logic;
                                 signal   enable      : in  std_logic;  -- when '0' then no valid output even when ready='1'
                                 signal   ready       : in  std_logic;
                                 signal   out_data    : out std_logic_vector;
                                 signal   out_valid   : out std_logic);

  -- Generate frame control
  procedure proc_common_sop(signal clk     : in  std_logic;
                            signal in_val  : out std_logic;
                            signal in_sop  : out std_logic);

  procedure proc_common_eop(signal clk     : in  std_logic;
                            signal in_val  : out std_logic;
                            signal in_eop  : out std_logic);

  procedure proc_common_val(constant c_val_len : in natural;
                            signal   clk       : in  std_logic;
                            signal   in_val    : out std_logic);

  procedure proc_common_val_duty(constant c_hi_len  : in natural;
                                 constant c_lo_len  : in natural;
                                 signal   clk       : in  std_logic;
                                 signal   in_val    : out std_logic);

  procedure proc_common_eop_flush(constant c_flush_len : in natural;
                                  signal   clk         : in  std_logic;
                                  signal   in_val      : out std_logic;
                                  signal   in_eop      : out std_logic);

  -- Verify the DUT output incrementing data, only support ready latency c_rl = 0 or 1.
  procedure proc_common_verify_data(constant c_rl            : in    natural;
                                    signal   clk             : in    std_logic;
                                    signal   verify_en       : in    std_logic;
                                    signal   ready           : in    std_logic;
                                    signal   out_valid       : in    std_logic;
                                    signal   out_data        : in    std_logic_vector;
                                    signal   prev_out_data   : inout std_logic_vector);

  -- Verify the DUT output valid for ready latency, only support ready latency c_rl = 0 or 1.
  procedure proc_common_verify_valid(constant c_rl        : in    natural;
                                     signal   clk         : in    std_logic;
                                     signal   verify_en   : in    std_logic;
                                     signal   ready       : in    std_logic;
                                     signal   prev_ready  : inout std_logic;
                                     signal   out_valid   : in    std_logic);

  -- Verify the DUT input to output latency for SL ctrl signals
  procedure proc_common_verify_latency(constant c_str         : in    string;  -- e.g. "valid", "sop", "eop"
                                       constant c_latency     : in    natural;
                                       signal   clk           : in    std_logic;
                                       signal   verify_en     : in    std_logic;
                                       signal   in_ctrl       : in    std_logic;
                                       signal   pipe_ctrl_vec : inout std_logic_vector;  -- range [0:c_latency]
                                       signal   out_ctrl      : in    std_logic);

  -- Verify the DUT input to output latency for SLV data signals
  procedure proc_common_verify_latency(constant c_str         : in    string;  -- e.g. "data"
                                       constant c_latency     : in    natural;
                                       signal   clk           : in    std_logic;
                                       signal   verify_en     : in    std_logic;
                                       signal   in_data       : in    std_logic_vector;
                                       signal   pipe_data_vec : inout std_logic_vector;  -- range [0:(1 + c_latency)*c_data_w-1]
                                       signal   out_data      : in    std_logic_vector);

  -- Verify the expected value, e.g. to check that a test has ran at all
  procedure proc_common_verify_value(constant mode : in natural;
                                     signal   clk  : in std_logic;
                                     signal   en   : in std_logic;
                                     signal   exp  : in std_logic_vector;
                                     signal   res  : in std_logic_vector);
  -- open, read line, close file
  procedure proc_common_open_file(file_status  : inout FILE_OPEN_STATUS;
                                  file in_file : TEXT;
                                  file_name    : in string;
                                  file_mode    : in FILE_OPEN_KIND);

  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_value_0 : out integer);

  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_value_0 : out integer;
                                      read_value_1 : out integer);

  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      value_array  : out t_integer_arr;
                                      nof_reads    : in  integer);

  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_slv     : out std_logic_vector);

  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      res_string   : out string);

  procedure proc_common_close_file(file_status  : inout FILE_OPEN_STATUS;
                                   file in_file : TEXT);

  -- read entire file
  procedure proc_common_read_integer_file(file_name              : in  string;
                                          nof_header_lines       : natural;
                                          nof_row                : natural;
                                          nof_col                : natural;
                                          signal return_array    : out t_integer_arr);

  procedure proc_common_read_mif_file(file_name           : in  string;
                                      signal return_array : out t_integer_arr);

  -- Complex multiply function with conjugate option for input b
  function func_complex_multiply(in_ar, in_ai, in_br, in_bi : std_logic_vector; conjugate_b : boolean; str : string; g_out_dat_w : natural) return std_logic_vector;

  function func_decstring_to_integer(in_string: string) return integer;

  function func_hexstring_to_integer(in_string: string) return integer;

  function func_find_char_in_string(in_string: string; find_char: character) return integer;

  function func_find_string_in_string(in_string: string; find_string: string) return boolean;
end tb_common_pkg;

package body tb_common_pkg is
  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait some clock cycles
  ------------------------------------------------------------------------------
  procedure proc_common_wait_some_cycles(signal clk          : in  std_logic;
                                                c_nof_cycles : in  natural) is
  begin
    for I in 0 to c_nof_cycles - 1 loop wait until rising_edge(clk); end loop;
  end proc_common_wait_some_cycles;

  procedure proc_common_wait_some_cycles(signal clk          : in  std_logic;
                                                c_nof_cycles : in  real) is
  begin
    proc_common_wait_some_cycles(clk, natural(c_nof_cycles));
  end proc_common_wait_some_cycles;

  procedure proc_common_wait_some_cycles(signal clk_in       : in  std_logic;
                                         signal clk_out      : in  std_logic;
                                                c_nof_cycles : in  natural) is
  begin
    proc_common_wait_some_cycles(clk_in, c_nof_cycles);
    proc_common_wait_some_cycles(clk_out, c_nof_cycles);
  end proc_common_wait_some_cycles;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait some pulses
  ------------------------------------------------------------------------------
  procedure proc_common_wait_some_pulses(signal clk          : in  std_logic;
                                         signal pulse        : in  std_logic;
                                                c_nof_pulses : in  natural) is
  begin
    for I in 0 to c_nof_pulses - 1 loop
      proc_common_wait_until_hi_lo(clk, pulse);
    end loop;
  end proc_common_wait_some_pulses;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait until the level input event
  -- PROCEDURE: Wait until the level input is high
  -- PROCEDURE: Wait until the level input is low
  -- PROCEDURE: Wait until the       input is equal to c_value
  ------------------------------------------------------------------------------
  procedure proc_common_wait_until_evt(signal clk    : in  std_logic;
                                       signal level  : in  std_logic) is
    variable v_level : std_logic := level;
  begin
    wait until rising_edge(clk);
    while v_level = level loop
      v_level := level;
      wait until rising_edge(clk);
    end loop;
  end proc_common_wait_until_evt;

  procedure proc_common_wait_until_evt(signal clk    : in  std_logic;
                                       signal level  : in  integer) is
    variable v_level : integer := level;
  begin
    wait until rising_edge(clk);
    while v_level = level loop
      v_level := level;
      wait until rising_edge(clk);
    end loop;
  end proc_common_wait_until_evt;

  procedure proc_common_wait_until_evt(constant c_timeout : in  natural;
                                       signal   clk       : in  std_logic;
                                       signal   level     : in  std_logic) is
    variable v_level : std_logic := level;
    variable v_I     : natural := 0;
  begin
    wait until rising_edge(clk);
    while v_level = level loop
      v_level := level;
      wait until rising_edge(clk);
      v_I := v_I + 1;
      if v_I >= c_timeout - 1 then
        report "COMMON : level evt timeout"
          severity ERROR;
        exit;
      end if;
    end loop;
  end proc_common_wait_until_evt;

  procedure proc_common_wait_until_high(signal clk    : in  std_logic;
                                        signal level  : in  std_logic) is
  begin
    if level /= '1' then
      wait until rising_edge(clk) and level = '1';
    end if;
  end proc_common_wait_until_high;

  procedure proc_common_wait_until_clk_and_high(signal clk    : in  std_logic;
                                                signal level  : in  std_logic) is
  begin
    wait until rising_edge(clk) and level = '1';
  end proc_common_wait_until_clk_and_high;

  procedure proc_common_wait_until_high(constant c_timeout : in  natural;
                                        signal   clk       : in  std_logic;
                                        signal   level     : in  std_logic) is
  begin
    for I in 0 to c_timeout - 1 loop
      if level = '1' then
        exit;
      else
        if I = c_timeout - 1 then
          report "COMMON : level high timeout"
            severity ERROR;
        end if;
        wait until rising_edge(clk);
      end if;
    end loop;
  end proc_common_wait_until_high;

  procedure proc_common_wait_until_low(signal clk    : in  std_logic;
                                       signal level  : in  std_logic) is
  begin
    if level /= '0' then
      wait until rising_edge(clk) and level = '0';
    end if;
  end proc_common_wait_until_low;

  procedure proc_common_wait_until_clk_and_low(signal clk    : in  std_logic;
                                              signal level  : in  std_logic) is
  begin
    wait until rising_edge(clk) and level = '0';
  end proc_common_wait_until_clk_and_low;

  procedure proc_common_wait_until_low(constant c_timeout : in  natural;
                                       signal   clk       : in  std_logic;
                                       signal   level     : in  std_logic) is
  begin
    for I in 0 to c_timeout - 1 loop
      if level = '0' then
        exit;
      else
        if I = c_timeout - 1 then
          report "COMMON : level low timeout"
            severity ERROR;
        end if;
        wait until rising_edge(clk);
      end if;
    end loop;
  end proc_common_wait_until_low;

  procedure proc_common_wait_until_hi_lo(signal clk    : in  std_logic;
                                         signal level  : in  std_logic) is
  begin
    if level /= '1' then
      proc_common_wait_until_high(clk, level);
    end if;
    proc_common_wait_until_low(clk, level);
  end proc_common_wait_until_hi_lo;

  procedure proc_common_wait_until_hi_lo(constant c_timeout : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   level     : in  std_logic) is
  begin
    if level /= '1' then
      proc_common_wait_until_high(c_timeout, clk, level);
    end if;
    proc_common_wait_until_low(c_timeout, clk, level);
  end proc_common_wait_until_hi_lo;

  procedure proc_common_wait_until_lo_hi(signal clk    : in  std_logic;
                                         signal level  : in  std_logic) is
  begin
    if level /= '0' then
      proc_common_wait_until_low(clk, level);
    end if;
    proc_common_wait_until_high(clk, level);
  end proc_common_wait_until_lo_hi;

  procedure proc_common_wait_until_lo_hi(constant c_timeout : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   level     : in  std_logic) is
  begin
    if level /= '0' then
      proc_common_wait_until_low(c_timeout, clk, level);
    end if;
    proc_common_wait_until_high(c_timeout, clk, level);
  end proc_common_wait_until_lo_hi;

  procedure proc_common_wait_until_value(constant c_value : in  integer;
                                         signal clk       : in  std_logic;
                                         signal level     : in  integer) is
  begin
    while level /= c_value loop
      wait until rising_edge(clk);
    end loop;
  end proc_common_wait_until_value;

  procedure proc_common_wait_until_value(constant c_value : in  integer;
                                         signal clk       : in  std_logic;
                                         signal level     : in  std_logic_vector) is
  begin
    while signed(level) /= c_value loop
      wait until rising_edge(clk);
    end loop;
  end proc_common_wait_until_value;

  procedure proc_common_wait_until_value(constant c_timeout : in  natural;
                                         constant c_value   : in  integer;
                                         signal clk         : in  std_logic;
                                         signal level       : in  std_logic_vector) is
  begin
    for I in 0 to c_timeout - 1 loop
      if signed(level) = c_value then
        exit;
      else
        if I = c_timeout - 1 then
          report "COMMON : level value timeout"
            severity ERROR;
        end if;
        wait until rising_edge(clk);
      end if;
    end loop;
  end proc_common_wait_until_value;

  procedure proc_common_wait_until_time(signal clk      : in  std_logic;
                                        constant c_time : in  time) is
  begin
    while NOW < c_time loop
      wait until rising_edge(clk);
    end loop;
  end procedure;

  procedure proc_common_timeout_failure(constant c_timeout : in time;
                                        signal tb_end      : in std_logic) is
  begin
    while tb_end = '0' loop
      assert NOW < c_timeout
        report "Test bench timeout."
        severity ERROR;
      assert NOW < c_timeout
        report "Test bench timeout."
        severity FAILURE;
      wait for 1 us;
    end loop;
  end procedure;

  procedure proc_common_stop_simulation(signal tb_end : in std_logic) is
  begin
    wait until tb_end = '1';
    -- For modelsim_regression_test_vhdl.py:
    -- The tb_end will stop the test verification bases on error or failure. The wait is necessary to
    -- stop the simulation using failure, without causing the test to fail.
    wait for 1 ns;
    report "Tb simulation finished."
      severity FAILURE;
    wait;
  end procedure;

  procedure proc_common_stop_simulation(constant g_tb_end  : in boolean;
                                        constant g_latency : in natural;
                                        signal clk         : in std_logic;
                                        signal tb_done     : in std_logic;
                                        signal tb_end      : out std_logic) is
  begin
    -- Wait until simulation indicates done
    proc_common_wait_until_high(clk, tb_done);

    -- Wait some more cycles
    proc_common_wait_some_cycles(clk, g_latency);

    -- Stop the simulation or only report NOTE
    tb_end <= '1';
    -- For modelsim_regression_test_vhdl.py:
    -- The tb_end will stop the test verification bases on error or failure. The wait is necessary to
    -- stop the simulation using failure, without causing the test to fail.
    wait for 1 ns;
    if g_tb_end = false then
      report "Tb Simulation finished."
        severity NOTE;
    else
      report "Tb Simulation finished."
        severity FAILURE;
    end if;
    wait;
  end procedure;

  procedure proc_common_stop_simulation(constant g_tb_end  : in boolean;
                                        signal clk         : in std_logic;
                                        signal tb_done     : in std_logic;
                                        signal tb_end      : out std_logic) is
  begin
    proc_common_stop_simulation(g_tb_end, 0, clk, tb_done, tb_end);
  end procedure;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Handle stream ready signal for data valid
  -- . output active when ready='1' and enable='1'
  -- . only support ready latency c_rl = 0 or 1
  ------------------------------------------------------------------------------
  procedure proc_common_ready_latency(constant c_rl      : in  natural;
                                      signal   clk       : in  std_logic;
                                      signal   enable    : in  std_logic;
                                      signal   ready     : in  std_logic;
                                      signal   out_valid : out std_logic) is
  begin
    -- skip ready cycles until enable='1'
    out_valid <= '0';
    while enable = '0' loop
      if c_rl = 0 then
        wait until rising_edge(clk);
        while ready /= '1' loop
          wait until rising_edge(clk);
        end loop;
      end if;
      if c_rl = 1 then
        while ready /= '1' loop
          wait until rising_edge(clk);
        end loop;
        wait until rising_edge(clk);
      end if;
    end loop;
    -- active output when ready
    if c_rl = 0 then
      out_valid <= '1';
      wait until rising_edge(clk);
      while ready /= '1' loop
        wait until rising_edge(clk);
      end loop;
    end if;
    if c_rl = 1 then
      while ready /= '1' loop
        out_valid <= '0';
        wait until rising_edge(clk);
      end loop;
      out_valid <= '1';
      wait until rising_edge(clk);
    end if;
  end proc_common_ready_latency;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait for clock domain crossing latency, e.g. for MM readback after MM write
  ------------------------------------------------------------------------------

  procedure proc_common_wait_cross_clock_domain_latency(signal mm_clk : in  std_logic;
                                                        signal st_clk : in  std_logic;
                                                        constant c_nof_cycles : in  natural) is
  begin
    proc_common_wait_some_cycles(mm_clk, c_nof_cycles);
    proc_common_wait_some_cycles(st_clk, c_nof_cycles);
  end proc_common_wait_cross_clock_domain_latency;

  procedure proc_common_wait_cross_clock_domain_latency(signal mm_clk : in  std_logic;
                                                        signal st_clk : in  std_logic) is
  begin
    proc_common_wait_some_cycles(mm_clk, c_common_cross_clock_domain_latency);
    proc_common_wait_some_cycles(st_clk, c_common_cross_clock_domain_latency);
  end proc_common_wait_cross_clock_domain_latency;

  procedure proc_common_wait_cross_clock_domain_latency(constant c_mm_clk_period : in time;
                                                        constant c_st_clk_period : in time;
                                                        constant c_nof_cycles : in  natural) is
  begin
    wait for c_nof_cycles * c_mm_clk_period;
    wait for c_nof_cycles * c_st_clk_period;
  end proc_common_wait_cross_clock_domain_latency;

  procedure proc_common_wait_cross_clock_domain_latency(constant c_mm_clk_period : in time;
                                                        constant c_st_clk_period : in time) is
  begin
    wait for c_common_cross_clock_domain_latency * c_mm_clk_period;
    wait for c_common_cross_clock_domain_latency * c_st_clk_period;
  end proc_common_wait_cross_clock_domain_latency;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate a single active, inactive pulse
  ------------------------------------------------------------------------------
  procedure proc_common_gen_pulse(constant c_active  : in  natural;  -- pulse active for nof clk
                                  constant c_period  : in  natural;  -- pulse period for nof clk
                                  constant c_level   : in  std_logic;  -- pulse level when active
                                  signal   clk       : in  std_logic;
                                  signal   pulse     : out std_logic) is
    variable v_cnt : natural range 0 to c_period := 0;
  begin
    while v_cnt < c_period loop
      if v_cnt < c_active then
        pulse <= c_level;
      else
        pulse <= not c_level;
      end if;
      v_cnt := v_cnt + 1;
      wait until rising_edge(clk);
    end loop;
  end proc_common_gen_pulse;

  -- Pulse forever after rst was released
  procedure proc_common_gen_pulse(constant c_active  : in  natural;  -- pulse active for nof clk
                                  constant c_period  : in  natural;  -- pulse period for nof clk
                                  constant c_level   : in  std_logic;  -- pulse level when active
                                  signal   rst       : in  std_logic;
                                  signal   clk       : in  std_logic;
                                  signal   pulse     : out std_logic) is
    variable v_cnt : natural range 0 to c_period := 0;
  begin
    pulse <= not c_level;
    if rst = '0' then
      wait until rising_edge(clk);
      while true loop
        proc_common_gen_pulse(c_active, c_period, c_level, clk, pulse);
      end loop;
    end if;
  end proc_common_gen_pulse;

  -- pulse '1', '0'
  procedure proc_common_gen_pulse(signal clk   : in  std_logic;
                                  signal pulse : out std_logic) is
  begin
    proc_common_gen_pulse(1, 2, '1', clk, pulse);
  end proc_common_gen_pulse;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate a periodic pulse with arbitrary duty cycle
  ------------------------------------------------------------------------------
  procedure proc_common_gen_duty_pulse(constant c_delay     : in  natural;  -- delay pulse for nof_clk after enable
                                       constant c_active    : in  natural;  -- pulse active for nof clk
                                       constant c_period    : in  natural;  -- pulse period for nof clk
                                       constant c_level     : in  std_logic;  -- pulse level when active
                                       signal   rst         : in  std_logic;
                                       signal   clk         : in  std_logic;
                                       signal   enable      : in  std_logic;
                                       signal   pulse       : out std_logic) is
    variable v_cnt : natural range 0 to c_period - 1 := 0;
  begin
    pulse <= not c_level;
    if rst = '0' then
      proc_common_wait_until_high(clk, enable);  -- if enabled then continue immediately else wait here
      proc_common_wait_some_cycles(clk, c_delay);  -- apply initial c_delay. Once enabled, the pulse remains enabled
      while true loop
        wait until rising_edge(clk);
        if v_cnt < c_active then
          pulse <= c_level;
        else
          pulse <= not c_level;
        end if;
        if v_cnt < c_period - 1 then
          v_cnt := v_cnt + 1;
        else
          v_cnt := 0;
        end if;
      end loop;
    end if;
  end proc_common_gen_duty_pulse;

  procedure proc_common_gen_duty_pulse(constant c_active    : in  natural;  -- pulse active for nof clk
                                       constant c_period    : in  natural;  -- pulse period for nof clk
                                       constant c_level     : in  std_logic;  -- pulse level when active
                                       signal   rst         : in  std_logic;
                                       signal   clk         : in  std_logic;
                                       signal   enable      : in  std_logic;
                                       signal   pulse       : out std_logic) is
  begin
    proc_common_gen_duty_pulse(0, c_active, c_period, c_level, rst, clk, enable, pulse);
  end proc_common_gen_duty_pulse;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate counter data with valid
  -- . Output counter data dependent on enable and ready
  ------------------------------------------------------------------------------
  -- arbitrary c_incr
  procedure proc_common_gen_data(constant c_rl        : in  natural;  -- 0, 1 are supported by proc_common_ready_latency()
                                 constant c_init      : in  integer;
                                 constant c_incr      : in  integer;
                                 signal   rst         : in  std_logic;
                                 signal   clk         : in  std_logic;
                                 signal   enable      : in  std_logic;  -- when '0' then no valid output even when ready='1'
                                 signal   ready       : in  std_logic;
                                 signal   out_data    : out std_logic_vector;
                                 signal   out_valid   : out std_logic) is
    constant c_data_w  : natural := out_data'length;
    variable v_data    : std_logic_vector(c_data_w - 1 downto 0) := TO_SVEC(c_init, c_data_w);
  begin
    out_valid <= '0';
    out_data  <= v_data;
    if rst = '0' then
      wait until rising_edge(clk);
      while true loop
        out_data <= v_data;
        proc_common_ready_latency(c_rl, clk, enable, ready, out_valid);
        v_data := INCR_UVEC(v_data, c_incr);
      end loop;
    end if;
  end proc_common_gen_data;

  -- c_incr = 1
  procedure proc_common_gen_data(constant c_rl        : in  natural;  -- 0, 1 are supported by proc_common_ready_latency()
                                 constant c_init      : in  integer;
                                 signal   rst         : in  std_logic;
                                 signal   clk         : in  std_logic;
                                 signal   enable      : in  std_logic;  -- when '0' then no valid output even when ready='1'
                                 signal   ready       : in  std_logic;
                                 signal   out_data    : out std_logic_vector;
                                 signal   out_valid   : out std_logic) is
  begin
    proc_common_gen_data(c_rl, c_init, 1, rst, clk, enable, ready, out_data, out_valid);
  end proc_common_gen_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate frame control
  ------------------------------------------------------------------------------
  procedure proc_common_sop(signal clk     : in  std_logic;
                            signal in_val  : out std_logic;
                            signal in_sop  : out std_logic) is
  begin
    in_val <= '1';
    in_sop <= '1';
    proc_common_wait_some_cycles(clk, 1);
    in_sop <= '0';
  end proc_common_sop;

  procedure proc_common_eop(signal clk     : in  std_logic;
                            signal in_val  : out std_logic;
                            signal in_eop  : out std_logic) is
  begin
    in_val <= '1';
    in_eop <= '1';
    proc_common_wait_some_cycles(clk, 1);
    in_val <= '0';
    in_eop <= '0';
  end proc_common_eop;

  procedure proc_common_val(constant c_val_len : in natural;
                            signal   clk       : in  std_logic;
                            signal   in_val    : out std_logic) is
  begin
    in_val <= '1';
    proc_common_wait_some_cycles(clk, c_val_len);
    in_val <= '0';
  end proc_common_val;

  procedure proc_common_val_duty(constant c_hi_len  : in natural;
                                 constant c_lo_len  : in natural;
                                 signal   clk       : in  std_logic;
                                 signal   in_val    : out std_logic) is
  begin
    in_val <= '1';
    proc_common_wait_some_cycles(clk, c_hi_len);
    in_val <= '0';
    proc_common_wait_some_cycles(clk, c_lo_len);
  end proc_common_val_duty;

  procedure proc_common_eop_flush(constant c_flush_len : in natural;
                                  signal   clk         : in  std_logic;
                                  signal   in_val      : out std_logic;
                                  signal   in_eop      : out std_logic) is
  begin
    -- . eop
    proc_common_eop(clk, in_val, in_eop);
    -- . flush after in_eop to empty the shift register
    proc_common_wait_some_cycles(clk, c_flush_len);
  end proc_common_eop_flush;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify incrementing data
  ------------------------------------------------------------------------------
  procedure proc_common_verify_data(constant c_rl            : in    natural;
                                    signal   clk             : in    std_logic;
                                    signal   verify_en       : in    std_logic;
                                    signal   ready           : in    std_logic;
                                    signal   out_valid       : in    std_logic;
                                    signal   out_data        : in    std_logic_vector;
                                    signal   prev_out_data   : inout std_logic_vector) is
    variable v_exp_data : std_logic_vector(out_data'range);
  begin
    if rising_edge(clk) then
      -- out_valid must be active, because only the out_data will it differ from the previous out_data
      if out_valid = '1' then
        -- for ready_latency = 1 out_valid indicates new data
        -- for ready_latency = 0 out_valid only indicates new data when it is confirmed by ready
        if c_rl = 1 or (c_rl = 0 and ready = '1') then
          prev_out_data <= out_data;
          v_exp_data := INCR_UVEC(prev_out_data, 1);  -- increment first then compare to also support increment wrap around
          if verify_en = '1' and unsigned(out_data) /= unsigned(v_exp_data) then
            report "COMMON : Wrong out_data count"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_common_verify_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output valid
  -- . only support ready latency c_rl = 0 or 1
  ------------------------------------------------------------------------------
  procedure proc_common_verify_valid(constant c_rl        : in    natural;
                                     signal   clk         : in    std_logic;
                                     signal   verify_en   : in    std_logic;
                                     signal   ready       : in    std_logic;
                                     signal   prev_ready  : inout std_logic;
                                     signal   out_valid   : in    std_logic) is
  begin
    if rising_edge(clk) then
      -- for ready latency c_rl = 1 out_valid may only be asserted after ready
      -- for ready latency c_rl = 0 out_valid may always be asserted
      prev_ready <= '0';
      if c_rl = 1 then
        prev_ready <= ready;
        if verify_en = '1' and out_valid = '1' then
          if prev_ready /= '1' then
            report "COMMON : Wrong ready latency between ready and out_valid"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_common_verify_valid;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT input to output latency
  ------------------------------------------------------------------------------
  -- for SL ctrl
  procedure proc_common_verify_latency(constant c_str         : in    string;  -- e.g. "valid", "sop", "eop"
                                       constant c_latency     : in    natural;
                                       signal   clk           : in    std_logic;
                                       signal   verify_en     : in    std_logic;
                                       signal   in_ctrl       : in    std_logic;
                                       signal   pipe_ctrl_vec : inout std_logic_vector;  -- range [0:c_latency]
                                       signal   out_ctrl      : in    std_logic) is
  begin
    if rising_edge(clk) then
      pipe_ctrl_vec <= in_ctrl & pipe_ctrl_vec(0 to c_latency - 1);  -- note: pipe_ctrl_vec(c_latency) is a dummy place holder to avoid [0:-1] range
      if verify_en = '1' then
        if c_latency = 0 then
          if in_ctrl /= out_ctrl then
            report "COMMON : Wrong zero latency between input " & c_str & " and output " & c_str
              severity ERROR;
          end if;
        else
          if pipe_ctrl_vec(c_latency - 1) /= out_ctrl then
            report "COMMON : Wrong latency between input " & c_str & " and output " & c_str
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_common_verify_latency;

  -- for SLV data
  procedure proc_common_verify_latency(constant c_str         : in    string;  -- e.g. "data"
                                       constant c_latency     : in    natural;
                                       signal   clk           : in    std_logic;
                                       signal   verify_en     : in    std_logic;
                                       signal   in_data       : in    std_logic_vector;
                                       signal   pipe_data_vec : inout std_logic_vector;  -- range [0:(1 + c_latency)*c_data_w-1]
                                       signal   out_data      : in    std_logic_vector) is
    constant c_data_w     : natural := in_data'length;
    constant c_data_vec_w : natural := pipe_data_vec'length;  -- = (1 + c_latency) * c_data_w
  begin
    if rising_edge(clk) then
      pipe_data_vec <= in_data & pipe_data_vec(0 to c_data_vec_w - c_data_w - 1);  -- note: pipe_data_vec(c_latency) is a dummy place holder to avoid [0:-1] range
      if verify_en = '1' then
        if c_latency = 0 then
          if unsigned(in_data) /= unsigned(out_data) then
            report "COMMON : Wrong zero latency between input " & c_str & " and output " & c_str
              severity ERROR;
          end if;
        else
          if unsigned(pipe_data_vec(c_data_vec_w - c_data_w - c_data_w to c_data_vec_w - c_data_w - 1)) /= unsigned(out_data) then
            report "COMMON : Wrong latency between input " & c_str & " and output " & c_str
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_common_verify_latency;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the expected value
  -- . e.g. to check that a test has ran at all
  ------------------------------------------------------------------------------
  procedure proc_common_verify_value(constant mode : in natural;
                                     signal   clk  : in std_logic;
                                     signal   en   : in std_logic;
                                     signal   exp  : in std_logic_vector;
                                     signal   res  : in std_logic_vector) is
  begin
    if rising_edge(clk) then
      if en = '1' then
        if mode = 0 and unsigned(res) /= unsigned(exp) then
          report "COMMON : Wrong result value"
            severity ERROR;  -- == (equal)
        end if;
        if mode = 1 and unsigned(res) < unsigned(exp) then
          report "COMMON : Wrong result value too small"
            severity ERROR;  -- >= (at least)
        end if;
      end if;
    end if;
  end proc_common_verify_value;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Opens a file for access and reports fail or success of opening.
  ------------------------------------------------------------------------------
  procedure proc_common_open_file( file_status  : inout FILE_OPEN_STATUS;
                                   file in_file : TEXT;
                                   file_name    : in    string;
                                   file_mode    : in    FILE_OPEN_KIND) is
  begin
    if file_status = OPEN_OK then
      file_close(in_file);
    end if;
    file_open (file_status, in_file, file_name, file_mode);
    if file_status = OPEN_OK then
      report "COMMON : file opened "
        severity NOTE;
    else
      report "COMMON : Unable to open file "
        severity FAILURE;
    end if;
  end proc_common_open_file;

 ------------------------------------------------------------------------------
  -- PROCEDURE: Reads an integer from a file.
  ------------------------------------------------------------------------------
  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_value_0 : out integer) is
     variable v_line : LINE;
     variable v_good : boolean;
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file is not opened "
        severity FAILURE;
    else
      if ENDFILE(in_file) then
        report "COMMON : end of file "
          severity NOTE;
      else
        READLINE(in_file, v_line);
        READ(v_line, read_value_0, v_good);
        if v_good = false then
          report "COMMON : Read from line unsuccessful "
            severity FAILURE;
        end if;
      end if;
    end if;
  end proc_common_readline_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Reads two integers from two columns in a file.
  ------------------------------------------------------------------------------
  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_value_0 : out integer;
                                      read_value_1 : out integer) is
     variable v_line : LINE;
     variable v_good : boolean;
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file is not opened "
        severity FAILURE;
    else
      if ENDFILE(in_file) then
        report "COMMON : end of file "
          severity NOTE;
      else
        READLINE(in_file, v_line);
        READ(v_line, read_value_0, v_good);
        if v_good = false then
          report "COMMON : Read from line unsuccessful "
            severity FAILURE;
        end if;
        READ(v_line, read_value_1, v_good);
        if v_good = false then
          report "COMMON : Read from line unsuccessful "
            severity FAILURE;
        end if;
      end if;
    end if;
  end proc_common_readline_file;

------------------------------------------------------------------------------
  -- PROCEDURE: Reads an array of integer from a file.
  ------------------------------------------------------------------------------
  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      value_array  : out t_integer_arr;
                                      nof_reads    : in  integer) is
     variable v_line : LINE;
     variable v_good : boolean;
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file is not opened "
        severity FAILURE;
    else
      if ENDFILE(in_file) then
        report "COMMON : end of file "
          severity NOTE;
      else
        READLINE(in_file, v_line);
        for I in 0 to nof_reads - 1 loop
          READ(v_line, value_array(I), v_good);
          if v_good = false then
            report "COMMON : Read from line unsuccessful "
              severity FAILURE;
          end if;
        end loop;
      end if;
    end if;
  end proc_common_readline_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Reads an std_logic_vector from a file
  ------------------------------------------------------------------------------
  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      read_slv     : out std_logic_vector) is
     variable v_line : LINE;
     variable v_good : boolean;
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file is not opened "
        severity FAILURE;
    else
      if ENDFILE(in_file) then
        report "COMMON : end of file "
          severity NOTE;
      else
        READLINE(in_file, v_line);
        READ(v_line, read_slv, v_good);
        if v_good = false then
          report "COMMON : Read from line unsuccessful "
            severity FAILURE;
        end if;
      end if;
    end if;
  end proc_common_readline_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Reads a string of any length from a file pointer.
  ------------------------------------------------------------------------------
  procedure proc_common_readline_file(file_status  : inout FILE_OPEN_STATUS;
                                      file in_file : TEXT;
                                      res_string   : out string) is
    variable v_line    : LINE;
    variable v_char    : character;
    variable is_string : boolean;
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file is not opened "
        severity FAILURE;
    else
      if ENDFILE(in_file) then
        report "COMMON : end of file "
          severity NOTE;
      else
        readline(in_file, v_line);
        -- clear the contents of the result string
        for I in res_string'range loop
            res_string(I) := ' ';
        end loop;
        -- read all characters of the line, up to the length
        -- of the results string
        for I in res_string'range loop
           read(v_line, v_char, is_string);
           if not is_string then  -- found end of line
              exit;
           end if;
           res_string(I) := v_char;
        end loop;
      end if;
    end if;
  end proc_common_readline_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Closes a file.
  ------------------------------------------------------------------------------
  procedure proc_common_close_file(file_status  : inout FILE_OPEN_STATUS;
                                   file in_file : TEXT) is
  begin
    if file_status /= OPEN_OK then
      report "COMMON : file was not opened "
        severity WARNING;
    end if;
    FILE_CLOSE(in_file);
    report "COMMON : file closed "
      severity NOTE;
  end proc_common_close_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Reads the integer data from nof_rows with nof_col values per
  --            row from a file and returns it row by row in an array of
  --            integers.
  ------------------------------------------------------------------------------
  procedure proc_common_read_integer_file(file_name              : in  string;
                                          nof_header_lines       : natural;
                                          nof_row                : natural;
                                          nof_col                : natural;
                                          signal return_array    : out t_integer_arr) is
    variable v_file_status : FILE_OPEN_STATUS;
    file     v_in_file     : TEXT;
    variable v_input_line  : LINE;
    variable v_string      : string(1 to 80);
    variable v_row_arr     : t_integer_arr(0 to nof_col - 1);
  begin
    if file_name /= "UNUSED" and file_name /= "unused" then
      -- Open the file for reading
      proc_common_open_file(v_file_status, v_in_file, file_name, READ_MODE);
      -- Read and skip the header
      for J in 0 to nof_header_lines - 1  loop
        proc_common_readline_file(v_file_status, v_in_file, v_string);
      end loop;
      for J in 0 to nof_row - 1  loop
        proc_common_readline_file(v_file_status, v_in_file, v_row_arr, nof_col);
        for I in 0 to nof_col - 1 loop
          return_array(J * nof_col + I) <= v_row_arr(I);  -- use loop to be independent of t_integer_arr downto or to range
        end loop;
        if ENDFILE(v_in_file) then
          if J /= nof_row - 1 then
            report "COMMON : Unexpected end of file"
              severity FAILURE;
          end if;
          exit;
        end if;
      end loop;
      -- Close the file
      proc_common_close_file(v_file_status, v_in_file);
    else
      return_array <= (return_array'range => 0);
    end if;
  end proc_common_read_integer_file;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Reads the data column from a .mif file and returns it in an
  --            array of integers
  ------------------------------------------------------------------------------
  procedure proc_common_read_mif_file(        file_name    : in  string;
                                       signal return_array : out t_integer_arr) is
    variable v_file_status : FILE_OPEN_STATUS;
    file     v_in_file     : TEXT;
    variable v_input_line  : LINE;
    variable v_string      : string(1 to 80);
    variable v_mem_width   : natural := 0;
    variable v_mem_depth   : natural := 0;
    variable v_up_bound    : natural := 0;
    variable v_low_bound   : natural := 0;
    variable v_end_header  : boolean := false;
    variable v_char        : character;
  begin
    -- Open the .mif file for reading
    proc_common_open_file(v_file_status, v_in_file, file_name, READ_MODE);
    -- Read the header.
    while not v_end_header loop
      proc_common_readline_file(v_file_status, v_in_file, v_string);
      if(func_find_string_in_string(v_string, "WIDTH=")) then  -- check for "WIDTH="
        v_up_bound  := func_find_char_in_string(v_string, ';');
        v_low_bound := func_find_char_in_string(v_string, '=');
        v_mem_width := func_decstring_to_integer(v_string(v_low_bound + 1 to v_up_bound - 1));
      elsif(func_find_string_in_string(v_string, "DEPTH=")) then  -- check for "DEPTH="
        v_up_bound  := func_find_char_in_string(v_string, ';');
        v_low_bound := func_find_char_in_string(v_string, '=');
        v_mem_depth := func_decstring_to_integer(v_string(v_low_bound + 1 to v_up_bound - 1));
      elsif(func_find_string_in_string(v_string, "CONTENT BEGIN")) then
        v_end_header := true;
      end if;
    end loop;
    -- Read the data
    for I in 0 to v_mem_depth - 1  loop
      proc_common_readline_file(v_file_status, v_in_file, v_string);  -- Read the next line from the file.
      v_low_bound     := func_find_char_in_string(v_string, ':');  -- Find the left position of the string that contains the data field
      v_up_bound      := func_find_char_in_string(v_string, ';');  -- Find the right position of the string that contains the data field
      return_array(I) <= func_hexstring_to_integer(v_string(v_low_bound + 1 to v_up_bound - 1));
    end loop;
    -- Close the file
    proc_common_close_file(v_file_status, v_in_file);
  end proc_common_read_mif_file;

  ------------------------------------------------------------------------------
  -- FUNCTION: Complex multiply with conjugate option for input b
  ------------------------------------------------------------------------------
  function func_complex_multiply(in_ar, in_ai, in_br, in_bi : std_logic_vector; conjugate_b : boolean; str : string; g_out_dat_w : natural) return std_logic_vector is
    -- Function: Signed complex multiply
    --   p = a * b       when g_conjugate_b = FALSE
    --     = (ar + j ai) * (br + j bi)
    --     =  ar*br - ai*bi + j ( ar*bi + ai*br)
    --
    --   p = a * conj(b) when g_conjugate_b = TRUE
    --     = (ar + j ai) * (br - j bi)
    --     =  ar*br + ai*bi + j (-ar*bi + ai*br)
    -- From mti_numeric_std.vhd follows:
    -- . SIGNED * --> output width = 2 * input width
    -- . SIGNED + --> output width = largest(input width)
    constant c_in_w      : natural := in_ar'length;  -- all input have same width
    constant c_res_w     : natural := 2 * c_in_w + 1;  -- *2 for multiply, +1 for sum of two products
    variable v_ar        : signed(c_in_w - 1 downto 0);
    variable v_ai        : signed(c_in_w - 1 downto 0);
    variable v_br        : signed(c_in_w - 1 downto 0);
    variable v_bi        : signed(c_in_w - 1 downto 0);
    variable v_result_re : signed(c_res_w - 1 downto 0);
    variable v_result_im : signed(c_res_w - 1 downto 0);
  begin
    -- Calculate expected result
    v_ar := RESIZE_NUM(signed(in_ar), c_in_w);
    v_ai := RESIZE_NUM(signed(in_ai), c_in_w);
    v_br := RESIZE_NUM(signed(in_br), c_in_w);
    v_bi := RESIZE_NUM(signed(in_bi), c_in_w);
    if conjugate_b = false then
      v_result_re := RESIZE_NUM(v_ar * v_br, c_res_w) - v_ai * v_bi;
      v_result_im := RESIZE_NUM(v_ar * v_bi, c_res_w) + v_ai * v_br;
    else
      v_result_re := RESIZE_NUM(v_ar * v_br, c_res_w) + v_ai * v_bi;
      v_result_im := RESIZE_NUM(v_ai * v_br, c_res_w) - v_ar * v_bi;
    end if;
    -- Note that for the product needs as many bits as the sum of the input widths. However the
    -- sign bit is then only needed for the case that both inputs have the largest negative
    -- values, only then the MSBits will be "01". For all other inputs the MSbits will always
    -- be "00" for positive numbers or "11" for negative numbers. MSbits "10" can not occur.
    -- For largest negative inputs the complex multiply result becomes:
    --
    --   3b inputs                --> 6b products     --> c_res_w = 7b
    --     -4 *   -4 +   -4 *   -4 =     +16 +     +16 =      +64       -- most negative valued inputs
    --   b100 * b100 + b100 * b100 = b010000 + b010000 = b0100000
    --
    --   --> if g_out_dat_w = 6b then
    --       a) IEEE unsigned resizing skips   the MSbits so b0100000 = +64 becomes b_100000 = -64
    --       b) IEEE signed resizing preserves the MSbit  so b0100000 = +64 becomes b0_00000 = 0
    --       c) detect MSbits = "01" to clip max positive to get                    _b011111 = +63
    -- Option a) seems to map best on the FPGA hardware multiplier IP.
    if str = "RE" then
      return std_logic_vector(RESIZE_NUM(v_result_re, g_out_dat_w));  -- conform option a)
    else
      return std_logic_vector(RESIZE_NUM(v_result_im, g_out_dat_w));  -- conform option a)
    end if;
  end;

  ------------------------------------------------------------------------------
  -- FUNCTION: Converts the decimal value represented in a string to an integer value.
  ------------------------------------------------------------------------------
  function func_decstring_to_integer(in_string: string) return integer is
    constant c_nof_digits : natural := in_string'length;  -- Define the length of the string
    variable v_char       : character;
    variable v_weight     : integer := 1;
    variable v_return_int : integer := 0;
  begin
    -- Walk through the string character by character.
    for I in c_nof_digits - 1 downto 0 loop
      v_char := in_string(I + in_string'low);
      case v_char is
         when '0' => v_return_int := v_return_int + 0 * v_weight;
         when '1' => v_return_int := v_return_int + 1 * v_weight;
         when '2' => v_return_int := v_return_int + 2 * v_weight;
         when '3' => v_return_int := v_return_int + 3 * v_weight;
         when '4' => v_return_int := v_return_int + 4 * v_weight;
         when '5' => v_return_int := v_return_int + 5 * v_weight;
         when '6' => v_return_int := v_return_int + 6 * v_weight;
         when '7' => v_return_int := v_return_int + 7 * v_weight;
         when '8' => v_return_int := v_return_int + 8 * v_weight;
         when '9' => v_return_int := v_return_int + 9 * v_weight;
         when others => null;
      end case;
      if (v_char /= ' ') then  -- Only increment the weight when the character is NOT a spacebar.
        v_weight := v_weight * 10;  -- Addapt the weight for the next decimal digit.
      end if;
    end loop;
    return(v_return_int);
  end function func_decstring_to_integer;

  ------------------------------------------------------------------------------
  -- FUNCTION: Converts the hexadecimal value represented in a string to an integer value.
  ------------------------------------------------------------------------------
  function func_hexstring_to_integer(in_string: string) return integer is
    constant c_nof_digits : natural := in_string'length;  -- Define the length of the string
    variable v_char       : character;
    variable v_weight     : integer := 1;
    variable v_return_int : integer := 0;
  begin
    -- Walk through the string character by character.
    for I in c_nof_digits - 1 downto 0 loop
      v_char := in_string(I + in_string'low);
      case v_char is
         when '0' => v_return_int := v_return_int + 0 * v_weight;
         when '1' => v_return_int := v_return_int + 1 * v_weight;
         when '2' => v_return_int := v_return_int + 2 * v_weight;
         when '3' => v_return_int := v_return_int + 3 * v_weight;
         when '4' => v_return_int := v_return_int + 4 * v_weight;
         when '5' => v_return_int := v_return_int + 5 * v_weight;
         when '6' => v_return_int := v_return_int + 6 * v_weight;
         when '7' => v_return_int := v_return_int + 7 * v_weight;
         when '8' => v_return_int := v_return_int + 8 * v_weight;
         when '9' => v_return_int := v_return_int + 9 * v_weight;
         when 'A' | 'a' => v_return_int := v_return_int + 10 * v_weight;
         when 'B' | 'b' => v_return_int := v_return_int + 11 * v_weight;
         when 'C' | 'c' => v_return_int := v_return_int + 12 * v_weight;
         when 'D' | 'd' => v_return_int := v_return_int + 13 * v_weight;
         when 'E' | 'e' => v_return_int := v_return_int + 14 * v_weight;
         when 'F' | 'f' => v_return_int := v_return_int + 15 * v_weight;
         when others => null;
      end case;
      if (v_char /= ' ') then  -- Only increment the weight when the character is NOT a spacebar.
        v_weight := v_weight * 16;  -- Addapt the weight for the next hexadecimal digit.
      end if;
    end loop;
    return(v_return_int);
  end function func_hexstring_to_integer;

  ------------------------------------------------------------------------------
  -- FUNCTION: Finds the first instance of a given character in a string
  --           and returns its position.
  ------------------------------------------------------------------------------
  function func_find_char_in_string(in_string: string; find_char: character) return integer is
    variable v_char_position : integer := 0;
  begin
    for I in 1 to in_string'length loop
      if(in_string(I) = find_char) then
        v_char_position := I;
      end if;
    end loop;
    return(v_char_position);
  end function func_find_char_in_string;

  ------------------------------------------------------------------------------
  -- FUNCTION: Checks if a string(find_string) is part of a larger string(in_string).
  --           The result is returned as a BOOLEAN.
  ------------------------------------------------------------------------------
  function func_find_string_in_string(in_string: string; find_string: string) return boolean is
    constant c_in_length     : natural := in_string'length;  -- Define the length of the string to search in
    constant c_find_length   : natural := find_string'length;  -- Define the length of the string to be find
    variable v_found_it      : boolean := false;
  begin
    for I in 1 to c_in_length - c_find_length loop
      if(in_string(I to (I + c_find_length - 1)) = find_string) then
        v_found_it := true;
      end if;
    end loop;
    return(v_found_it);
  end function func_find_string_in_string;
end tb_common_pkg;
