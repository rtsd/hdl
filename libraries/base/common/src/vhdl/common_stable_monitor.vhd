-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Monitor whether r_in did not go low since the previous ack
-- Description:
--   When r_in = '0' then r_stable = '0'. The r_stable is '1' if r_in = '1' and
--   remains '1'. The r_stable measurement interval restarts after an pulse on
--   r_stable_ack. Hence by pulsing r_stable_ack the user can start a new fresh
--   interval for monitoring r_stable.
-- Remarks:

entity common_stable_monitor is
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- MM
    r_in         : in  std_logic;
    r_stable     : out std_logic;
    r_stable_ack : in  std_logic
  );
end common_stable_monitor;

architecture rtl of common_stable_monitor is
  signal nxt_r_stable  : std_logic;
  signal r_evt         : std_logic;
  signal r_evt_occured : std_logic;
begin
  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      r_stable <= '0';
    elsif rising_edge(clk) then
      r_stable <= nxt_r_stable;
    end if;
  end process;

  nxt_r_stable <= r_in and not r_evt_occured;

  u_r_evt : entity work.common_evt
  generic map (
    g_evt_type => "BOTH",
    g_out_reg  => false
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => r_in,
    out_evt  => r_evt
  );

  u_r_evt_occured : entity work.common_switch
  generic map (
    g_rst_level    => '0',
    g_priority_lo  => false,
    g_or_high      => true,
    g_and_low      => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => r_evt,
    switch_low  => r_stable_ack,
    out_level   => r_evt_occured
  );
end rtl;
