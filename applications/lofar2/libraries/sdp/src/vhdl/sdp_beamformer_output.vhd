-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle, E. Kooistra
-- Purpose:
-- The beamformer data output (BDO) packetizes the beamlet data into UDP/IP packets.
-- Description: see references
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Beamformer
-- [2] https://support.astron.nl/confluence/display/L2M/L4+SDPFW+Decision%3A+Multiple+beamlet+output+destinations
-- [3] https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L1%20Interface%20Control%20Documents/STAT%20to%20CEP%20ICD
-- Remark:
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, reorder_lib, tr_10GbE_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_field_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use reorder_lib.reorder_pkg.all;
  use work.sdp_pkg.all;
  use work.sdp_bdo_pkg.all;

entity sdp_beamformer_output is
  generic (
    g_beamset_id           : natural := 0;
    -- For g_nof_destinations_max > 1 the transpose is always used
    -- For g_nof_destinations_max = 1 then the transpose is only used
    -- when g_use_transpose = true, else the identity is used
    g_use_transpose        : boolean := false;
    g_nof_destinations_max : natural := 1;
    g_sim_force_bsn_error  : boolean := true
  );
  port (
    dp_clk   : in  std_logic;
    dp_rst   : in  std_logic;

    mm_clk   : in  std_logic;
    mm_rst   : in  std_logic;

    reg_hdr_dat_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso      : out t_mem_miso;
    reg_destinations_copi : in  t_mem_copi := c_mem_mosi_rst;
    reg_destinations_cipo : out t_mem_cipo;
    reg_dp_xonoff_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_xonoff_miso    : out t_mem_miso;

    in_sosi        : in  t_dp_sosi;
    out_sosi       : out t_dp_sosi;
    out_siso       : in t_dp_siso;

    sdp_info       : in t_sdp_info;
    beamlet_scale  : in std_logic_vector(c_sdp_W_beamlet_scale-1 downto 0);
    gn_id          : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);

    -- Source MAC/IP/UDP are not used, c_sdp_cep_hdr_field_sel selects MM programmable instead
    eth_src_mac    : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr    : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port   : in std_logic_vector(c_network_udp_port_w - 1 downto 0);

    hdr_fields_out : out std_logic_vector(1023 downto 0)
  );
end sdp_beamformer_output;

architecture str of sdp_beamformer_output is
  constant c_data_w                 : natural := c_nof_complex * c_sdp_W_beamlet;  -- 16b
  constant c_beamset_beamlet_index  : natural := g_beamset_id * c_sdp_S_sub_bf;  -- call beamset 'id' and
                                                                                 -- beamlet 'index'

  -- Use c_fifo_fill = c_fifo_size - margin so that FIFO does not get read out too soon.
  -- The dp_fifo_fill_eop takes care that the FIFO gets read out whenever there is an
  -- eop in the FIFO, so that no payload gets stuck in the FIFO. Thanks to the use of eop
  -- it possible to pass on blocks of variable length.
  -- Make c_fifo_size large enough for adding header, muxing c_sdp_N_beamsets beamsets and
  -- delaying output to be able to realign snk_in.err field from snk_in.eop to src_out.sop.
  constant c_nof_words_multi     : natural := largest(
                               func_sdp_bdo_nof_ch_per_packet_first_destinations_look_up_table(g_nof_destinations_max));
  constant c_nof_longwords_one   : natural := c_sdp_cep_payload_nof_longwords;  -- = 976
  constant c_nof_longwords_multi : natural := c_nof_words_multi / 2;
  constant c_fifo_size_one       : natural := true_log_pow2(c_nof_longwords_one) * c_sdp_N_beamsets;  -- = 2048
  constant c_fifo_size_multi     : natural := true_log_pow2(c_nof_longwords_multi) * c_sdp_N_beamsets;  -- = 2048
  constant c_fifo_size           : natural := sel_a_b(g_nof_destinations_max = 1, c_fifo_size_one, c_fifo_size_multi);
  -- Rely on input eop instead of FIFO fill level, therefore set c_fifo_fill =
  -- g_fifo_size - (g_fifo_af_margin + 2) as explained in dp_fifo_fill_eop
  constant c_fifo_fill           : natural := c_fifo_size - 6;

  -- Multi destination info (mdi)
  constant c_nof_destinations_w  : natural := ceil_log2(g_nof_destinations_max + 1);

  constant c_beamlet_index_per_destination_mat : t_natural_matrix(1 to g_nof_destinations_max,
                                                                  0 to g_nof_destinations_max - 1) :=
                                      func_sdp_bdo_beamlet_index_per_destination_look_up_matrix(g_nof_destinations_max);

  -- . field_sel = '0' for DP (dynamic), '1' for MM (fixed or programmable via MM of dp_offload_tx_v3)
  constant c_cep_hdr_field_sel : std_logic_vector(c_sdp_cep_nof_hdr_fields - 1 downto 0) :=
                                   sel_a_b(g_nof_destinations_max = 1, func_sdp_cep_hdr_field_sel_dst('1'),
                                                                       func_sdp_cep_hdr_field_sel_dst('0'));

  -- BDO packet size control
  -- . One 32b word contains 1 dual pol beamlet of 4 octets (Xre, Xim, Yre, Yim).
  -- . The transposed output packet will have nof_blocks_per_packet time slots
  --   per beamlet and nof_beamlets_per_block dual pol beamlets per time slot.
  signal nof_blocks_per_packet      : natural;
  signal nof_beamlets_per_block     : natural;
  signal beamlet_index              : natural;

  signal snk_in_concat              : t_dp_sosi;
  signal snk_in_concat_data         : std_logic_vector(c_data_w - 1 downto 0);
  signal snk_in_concat_re           : std_logic_vector(c_sdp_W_beamlet - 1 downto 0);
  signal snk_in_concat_im           : std_logic_vector(c_sdp_W_beamlet - 1 downto 0);
  signal dp_repack_beamlet_src_out  : t_dp_sosi;
  signal dp_repack_beamlet_word     : t_sdp_dual_pol_beamlet_in_word;
  signal dp_packet_reorder_src_out  : t_dp_sosi;
  signal dp_packet_reorder_word     : t_sdp_dual_pol_beamlet_in_word;
  signal dp_repack_longword_src_out : t_dp_sosi;
  signal dp_repack_longword         : t_sdp_dual_pol_beamlet_in_longword;
  signal dp_fifo_data_src_out       : t_dp_sosi;
  signal dp_fifo_data_src_in        : t_dp_siso;
  signal dp_pipeline_data_src_out   : t_dp_sosi;
  signal dp_pipeline_data_src_in    : t_dp_siso;
  signal dp_offload_tx_src_out      : t_dp_sosi;
  signal dp_offload_tx_src_in       : t_dp_siso;
  signal ip_checksum_src_out        : t_dp_sosi;
  signal ip_checksum_src_in         : t_dp_siso;
  signal dp_pipeline_ready_src_out  : t_dp_sosi;
  signal dp_pipeline_ready_src_in   : t_dp_siso;

  signal dbg_force_bsn_error  : std_logic := '0';
  signal payload_err          : std_logic_vector(0 downto 0);
  signal station_info         : std_logic_vector(15 downto 0) := (others => '0');

  -- Multiple destinations info (mdi)
  signal multi_destinations_info    : t_sdp_bdo_destinations_info;
  signal s_DN                       : natural := 1;  -- number of destinations
  signal s_DI                       : natural := 0;  -- destination index
  signal mdi_eth_dst_mac            : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal mdi_ip_dst_addr            : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal mdi_udp_dst_port           : std_logic_vector(c_network_udp_port_w - 1 downto 0);

  signal mdi_nof_blocks_per_packet                      : natural;
  signal mdi_nof_beamlets_per_block_first_destinations  : natural;
  signal mdi_nof_beamlets_per_block_last_destination    : natural;
  signal mdi_nof_beamlets_per_block_per_destination     : natural;
  signal mdi_beamlet_index_per_destination              : natural;

  -- Default set all data path driven header fields to 0
  signal dp_offload_tx_hdr_fields : std_logic_vector(1023 downto 0) := (others => '0');
  signal dp_offload_tx_header     : t_sdp_cep_header;  -- to view dp_offload_tx_hdr_fields in Wave window
begin
  -----------------------------------------------------------------------------
  -- Input rewiring: concatenate input complex fields to data field
  -- . dp_repack_data works with data fields only
  -- . send beamlet data big endian with X.re part first, then X.im, Y.re,
  --   and Y.im, conform ICD STAT-CEP [3].
  -----------------------------------------------------------------------------

  -- Debug signals for view in Wave window
  snk_in_concat_data <= snk_in_concat.data(c_data_w - 1 downto 0);
  snk_in_concat_re <= in_sosi.re(c_sdp_W_beamlet - 1 downto 0);
  snk_in_concat_im <= in_sosi.re(c_sdp_W_beamlet - 1 downto 0);

  p_snk_in_arr : process(in_sosi)
    variable v_ref_time : time := 0 ns;
  begin
    snk_in_concat <= in_sosi;
    snk_in_concat.data(c_data_w - 1 downto 0) <= in_sosi.re(c_sdp_W_beamlet - 1 downto 0) &
                                                 in_sosi.im(c_sdp_W_beamlet - 1 downto 0);

    ---------------------------------------------------------------------------
    -- synthesis translate_off
    -- Force BSN error in simulation to verify payload error in
    -- tb_lofar2_unb2c_sdp_station_bf.vhd, this will cause two times payload
    -- errors, one when BSN goes wrong and one when BSN goes ok again.
    if g_sim_force_bsn_error = true then
      dbg_force_bsn_error <= '0';
      if v_ref_time = 0 ns then
        if in_sosi.sop = '1' then
          -- Use start of input as reference time, rather than e.g. fixed 50 us,
          -- to be independent of how long it takes for the tb to deliver the
          -- first block.
          v_ref_time := NOW;
          -- Offset the v_ref_time to the second block of the
          -- c_sdp_cep_nof_blocks_per_packet = 4 blocks that will be merged.
          v_ref_time := v_ref_time + c_sdp_block_period * 1 ns;
        end if;
      elsif NOW > v_ref_time + 1 * c_sdp_cep_nof_blocks_per_packet * c_sdp_block_period * 1 ns and
            NOW < v_ref_time + 4 * c_sdp_cep_nof_blocks_per_packet * c_sdp_block_period * 1 ns then
        -- Disturb BSN to cause merged payload error. Expected results for the
        -- merged blocks:
        -- . index 0 : First merged block bsn ok and payload_error = '0'.
        -- . index 1 : bsn still ok, but payload error = '1', due to bsn++
        --             after first block
        -- . index 2,3 : bsn wrong due to bsn++, but payload error = '0',
        --               because all 4 merged blocks have incrementing bsn
        -- . index 4 : bsn still wrong due to bsn++, and payload error = '1',
        --             because the bsn is restored after first block, so the
        --             merged blocks do not have incrementing bsn
        -- . index >= 5 : bsn ok and payload_error = '0'.
        dbg_force_bsn_error <= '1';
        snk_in_concat.bsn <= INCR_UVEC(in_sosi.bsn, 1);
      end if;
    end if;
    -- synthesis translate_on
    ---------------------------------------------------------------------------
  end process;

  -----------------------------------------------------------------------------
  -- dp_repack_data
  -- . Repack 16b -> 32b, to get dual polarization beamlets of N_pol_bf *
  --   N_complex * W_beamlet = 2 * 2 * 8 = 32b words
  -- . No need to flow control the source, because repack into wider words
  -----------------------------------------------------------------------------
  u_dp_repack_data_beamlet : entity dp_lib.dp_repack_data
  generic map (
    g_in_dat_w      => c_data_w,  -- = 16b
    g_in_nof_words  => c_sdp_N_pol_bf,  -- = 2
    g_out_dat_w     => c_sdp_W_dual_pol_beamlet,  -- = 32b
    g_out_nof_words => 1
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,

    snk_in  => snk_in_concat,
    snk_out => OPEN,

    src_out => dp_repack_beamlet_src_out,
    src_in  => c_dp_siso_rdy
  );

  -- Debug signals for view in Wave window
  -- [0:3] = [Xre, Xim, Yre, Yim]
  dp_repack_beamlet_word <= unpack_data(dp_repack_beamlet_src_out.data(c_sdp_W_dual_pol_beamlet - 1 downto 0));

  gen_one_destination : if g_nof_destinations_max = 1 generate
    -----------------------------------------------------------------------------
    -- Merge and reorder beamlet data for one destination from:
    --     (int8) [t] [N_blocks_per_packet][S_sub_bf] [N_pol_bf][N_complex]
    --   to:
    --     (int8) [t] [S_sub_bf][N_blocks_per_packet] [N_pol_bf][N_complex]
    --
    -- . where (int8) [N_pol_bf][N_complex] = c_sdp_W_dual_pol_beamlet = 32b
    --   dual polarization beamlet word
    -----------------------------------------------------------------------------
    u_sdp_bdo_one_destination : entity work.sdp_bdo_one_destination
      generic map (
        g_use_transpose => g_use_transpose
      )
      port map (
        dp_clk   => dp_clk,
        dp_rst   => dp_rst,

        snk_in   => dp_repack_beamlet_src_out,
        src_out  => dp_packet_reorder_src_out
      );
  end generate;

  gen_multiple_destinations : if g_nof_destinations_max > 1 generate
    -----------------------------------------------------------------------------
    -- Merge, reorder and unmerge beamlet data for N_destinations >= 1 from:
    --     (int8) [t] [N_blocks_per_packet][S_sub_bf / N_destinations] [N_pol_bf][N_complex]
    --   to:
    --     (int8) [t] [S_sub_bf / N_destinations][N_blocks_per_packet] [N_pol_bf][N_complex]
    --
    -- . where (int8) [N_pol_bf][N_complex] = c_sdp_W_dual_pol_beamlet = 32b
    --   dual polarization beamlet word
    -- . where N_destinations packets together transport the S_sub_bf beamlets.
    -----------------------------------------------------------------------------
    u_sdp_bdo_multiple_destinations : entity work.sdp_bdo_multiple_destinations
      generic map (
        g_nof_destinations_max => g_nof_destinations_max,
        g_beamset_id           => g_beamset_id
      )
      port map (
        mm_clk   => mm_clk,
        mm_rst   => mm_rst,

        dp_clk   => dp_clk,
        dp_rst   => dp_rst,

        reg_destinations_copi => reg_destinations_copi,
        reg_destinations_cipo => reg_destinations_cipo,

        destinations_info => multi_destinations_info,

        snk_in   => dp_repack_beamlet_src_out,
        src_out  => dp_packet_reorder_src_out,

        -- Streaming data output info dependent on DN = nof_destinations
        nof_blocks_per_packet                     => mdi_nof_blocks_per_packet,
        nof_beamlets_per_block_first_destinations => mdi_nof_beamlets_per_block_first_destinations,
        nof_beamlets_per_block_last_destination   => mdi_nof_beamlets_per_block_last_destination
      );

    -----------------------------------------------------------------------------
    -- Look up table values and and output info dependent on DN and DI
    -----------------------------------------------------------------------------
    -- Register output info dependent on DN and DI to ease timing closure
    p_reg : process(dp_clk)
      variable v_DI : natural;  -- destination index
    begin
      if rising_edge(dp_clk) then
        -- Register number of destinations (DN)
        s_DN <= multi_destinations_info.nof_destinations_act;

        -- Capture destination index (DI) from channel field, valid at sop
        if dp_fifo_data_src_out.sop = '1' then
          v_DI := to_uint(dp_fifo_data_src_out.channel);
          s_DI <= v_DI;  -- for view in Wave window

          -- Variable values that depend on DN set via MM and/or on current DI
          -- from dp_packet_unmerged that is passed on via
          -- dp_fifo_data_src_out.channel.
          -- . Use s_DN to ease timing closure. Use v_DI to ensure that the mdi
          --   values are valid at dp_pipeline_data_src_out.sop
          mdi_eth_dst_mac  <= multi_destinations_info.eth_destination_mac_arr(v_DI);
          mdi_ip_dst_addr  <= multi_destinations_info.ip_destination_address_arr(v_DI);
          mdi_udp_dst_port <= multi_destinations_info.udp_destination_port_arr(v_DI);

          -- . Account for beamset offset in beamlet index
          mdi_beamlet_index_per_destination <= c_beamset_beamlet_index +
                                               c_beamlet_index_per_destination_mat(s_DN, v_DI);
          -- . In total there are S_sub_bf = 488 beamlets for nof_destinations.
          --   The packet for last destination contains the same number of
          --   beamlets as the first destinations, or less beamlets.
          if v_DI < s_DN - 1 then
            mdi_nof_beamlets_per_block_per_destination <= mdi_nof_beamlets_per_block_first_destinations;
          else
            mdi_nof_beamlets_per_block_per_destination <= mdi_nof_beamlets_per_block_last_destination;
          end if;
        end if;
      end if;
    end process;
  end generate;

  -- Debug signals for view in Wave window
  dp_packet_reorder_word <= unpack_data(dp_packet_reorder_src_out.data(c_sdp_W_dual_pol_beamlet - 1 downto 0));

  -----------------------------------------------------------------------------
  -- dp_repack_data
  -- . Repack 32b -> 64b, to get 64b longwords for network packet data
  -- . No need to flow control the source, because repack into wider words
  -----------------------------------------------------------------------------
  u_dp_repack_data_longword : entity dp_lib.dp_repack_data
  generic map (
    g_in_dat_w      => c_sdp_W_dual_pol_beamlet,  -- = 32b
    g_in_nof_words  => c_sdp_nof_beamlets_per_longword,  -- = 2
    g_out_dat_w     => c_longword_w,  -- = 64b
    g_out_nof_words => 1
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,

    snk_in  => dp_packet_reorder_src_out,
    snk_out => OPEN,

    src_out => dp_repack_longword_src_out,
    src_in  => c_dp_siso_rdy
  );

  dp_repack_longword <= unpack_data(dp_repack_longword_src_out.data(c_longword_w - 1 downto 0));

  -----------------------------------------------------------------------------
  -- FIFO: to be able to insert header in u_dp_offload_tx_v3
  -----------------------------------------------------------------------------
  -- Pass on dp_repack_longword_src_out.err field not here, but via separate
  -- u_common_fifo_err.
  u_dp_fifo_data : entity dp_lib.dp_fifo_fill_eop_sc
  generic map (
    g_data_w         => c_longword_w,
    g_empty_w        => c_byte_w,
    g_use_empty      => true,
    g_use_bsn        => true,
    g_use_channel    => true,
    g_bsn_w          => 64,
    g_channel_w      => c_nof_destinations_w,
    g_use_sync       => true,
    g_fifo_size      => c_fifo_size,
    g_fifo_fill      => c_fifo_fill,
    g_fifo_rl        => 1
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,
    snk_in  => dp_repack_longword_src_out,
    src_out => dp_fifo_data_src_out,
    src_in  => dp_fifo_data_src_in
  );

  -- FIFO: to store and align payload error bit from eop to sop
  -- . The payload error bit is set when dp_packet_merge detects an BSN
  --   increment error. The dual page mechanism in reorder_col_select makes
  --   that the sosi.err bit will be valid during the entire output packet,
  --   so that it can be passed on at the sop via the application header.
  --   For g_nof_destinations > 1 each destination packet will have its
  --   payload error bit set as well, because dp_packet_unmerge applies the
  --   input sosi.err at the sop to all unmerged output packets.
  -- . Simple fifo to store the payload error bit at eop of FIFO input to be
  --   used at sop of FIFO output, so that payload_err can then be used in
  --   the packet header. Typically the u_dp_fifo_data will store between 0
  --   and c_sdp_N_beamsets = 2 packets. Choose g_nof_words > c_sdp_N_beamsets
  --   to have some margin compared to c_fifo_size of the data FIFO.
  -- . No need to account for g_nof_destinations_max > 1 in FIFO g_nof_words,
  --   because the BDO packets are multiplexed round-robin with fair chance
  --   per beamset by the dp_mux in sdp_station.vhd
  u_common_fifo_err : entity common_lib.common_fifo_sc
  generic map (
    g_dat_w => 1,
    g_nof_words => c_sdp_N_beamsets + 2
  )
  port map (
    rst    => dp_rst,
    clk    => dp_clk,
    wr_dat => dp_repack_longword_src_out.err(0 downto 0),
    wr_req => dp_repack_longword_src_out.eop,
    rd_dat => payload_err,
    rd_req => dp_fifo_data_src_out.sop
  );

  -- Pipeline dp_fifo_data_src_out to align payload_err at dp_pipeline_data_src_out.sop
  u_pipeline_data : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline => 1
  )
  port map  (
    rst        => dp_rst,
    clk        => dp_clk,
    -- ST sink
    snk_out    => dp_fifo_data_src_in,
    snk_in     => dp_fifo_data_src_out,
    -- ST source
    src_in     => dp_pipeline_data_src_in,
    src_out    => dp_pipeline_data_src_out
  );

  -----------------------------------------------------------------------------
  -- Assemble offload info
  -----------------------------------------------------------------------------
  -- Whether the dp_offload_tx_hdr_fields value is actually used in the Tx
  -- header depends on c_sdp_cep_hdr_field_sel:
  -- . c_sdp_cep_hdr_field_sel = "111"&"111111111011"&"1110"&"1100"&"00000010"&"100110"&"0";
  --                              eth   ip             udp    app
  --   where 0 = data path, 1 = MM controlled. The '0' fields are assigned here
  --   via dp_offload_tx_hdr_fields. In order:
  --     access   field
  --     MM,DP    eth_dst_mac
  --     MM       eth_src_mac
  --     MM       eth_type
  --
  --     MM       ip_version
  --     MM       ip_header_length
  --     MM       ip_services
  --     MM       ip_total_length
  --     MM       ip_identification
  --     MM       ip_flags
  --     MM       ip_fragment_offset
  --     MM       ip_time_to_live
  --     MM       ip_protocol
  --        DP    ip_header_checksum --> added by u_tr_10GbE_ip_checksum
  --     MM       ip_src_addr
  --     MM,DP    ip_dst_addr
  --
  --     MM       udp_src_port
  --     MM,DP    udp_dst_port
  --     MM       udp_total_length
  --        DP    udp_checksum --> default fixed 0, so not used, not calculated
  --                               here or in tr_10GbE because would require
  --                               store and forward
  --
  --     MM       sdp_marker
  --     MM       sdp_version_id
  --        DP    sdp_observation_id
  --        DP    sdp_station_info
  --
  --        DP    sdp_source_info_reserved
  --        DP    sdp_source_info_antenna_band_id
  --        DP    sdp_source_info_nyquist_zone_id
  --        DP    sdp_source_info_f_adc
  --        DP    sdp_source_info_fsub_type
  --        DP    sdp_source_info_payload_error
  --        DP    sdp_source_info_beam_repositioning_flag
  --     MM       sdp_source_info_beamlet_width
  --        DP    sdp_source_info_gn_id
  --
  --     MM       sdp_reserved
  --        DP    sdp_beamlet_scale
  --        DP    sdp_beamlet_index
  --        DP    sdp_nof_blocks_per_packet
  --        DP    sdp_nof_beamlets_per_block
  --        DP    sdp_block_period
  --
  --        DP    dp_bsn

  p_assemble_offload_info : process(mdi_nof_blocks_per_packet,
                                    mdi_nof_beamlets_per_block_per_destination,
                                    mdi_beamlet_index_per_destination)
  begin
    if g_nof_destinations_max = 1 then
      -- Use constant defaults for beamlet data output to one destination.
      nof_blocks_per_packet  <= c_sdp_cep_nof_blocks_per_packet;  -- = 4;
      nof_beamlets_per_block <= c_sdp_S_sub_bf;  -- = 488 dual pol beamlets;
      beamlet_index          <= c_beamset_beamlet_index;
    else
      -- Use dynamic sizes for beamlet data output to multiple destination.
      nof_blocks_per_packet  <= mdi_nof_blocks_per_packet;
      nof_beamlets_per_block <= mdi_nof_beamlets_per_block_per_destination;
      beamlet_index          <= mdi_beamlet_index_per_destination;
    end if;
  end process;

  station_info <= sdp_info.antenna_field_index & sdp_info.station_id;

  -- Use MM programmable source MAC/IP/UDP instead of source MAC/IP/UDP based
  -- on node ID. This is necessary because beamlet packets from different
  -- stations must have different source MAC/IP/UDP. Hence the eth_src_mac,
  -- udp_src_port and ip_src_addr are ignored, because c_sdp_cep_hdr_field_sel
  -- selects MM control, but keep the code to be able to enable using them by
  -- just changing the selection bit.
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "eth_src_mac") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "eth_src_mac")) <= eth_src_mac;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "ip_src_addr") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "ip_src_addr")) <= ip_src_addr;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "udp_src_port") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "udp_src_port")) <= udp_src_port;

  -- Use MM programmable destination MAC/IP/UDP from dp_offload_tx_v3 for one destination
  -- Use DP programmable destination MAC/IP/UDP from sdp_bdo_destinations_reg for multiple destinations
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "eth_dst_mac") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "eth_dst_mac")) <= mdi_eth_dst_mac;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "ip_dst_addr") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "ip_dst_addr")) <= mdi_ip_dst_addr;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "udp_dst_port") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "udp_dst_port")) <= mdi_udp_dst_port;

  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_observation_id") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_observation_id")) <= sdp_info.observation_id;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_station_info") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_station_info")) <= station_info;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_antenna_band_id") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_antenna_band_id")) <=
                                                                                       SLV(sdp_info.antenna_band_index);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_nyquist_zone_id") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_nyquist_zone_id")) <=
                                                                                            sdp_info.nyquist_zone_index;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_f_adc") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_f_adc")) <= SLV(sdp_info.f_adc);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_fsub_type") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_fsub_type")) <= SLV(sdp_info.fsub_type);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_payload_error") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_payload_error")) <= payload_err;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_beam_repositioning_flag") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_beam_repositioning_flag")) <=
                                                                                  SLV(sdp_info.beam_repositioning_flag);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_source_info_gn_id") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_source_info_gn_id")) <= gn_id;

  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_beamlet_scale") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_beamlet_scale")) <= beamlet_scale;
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_beamlet_index") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_beamlet_index")) <=
                                                                                   TO_UVEC(beamlet_index, c_halfword_w);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_nof_blocks_per_packet") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_nof_blocks_per_packet")) <=
                                                                              TO_UVEC(nof_blocks_per_packet, c_octet_w);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_nof_beamlets_per_block") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_nof_beamlets_per_block")) <=
                                                                          TO_UVEC(nof_beamlets_per_block, c_halfword_w);
  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "sdp_block_period") downto
                           field_lo(c_sdp_cep_hdr_field_arr, "sdp_block_period")) <= sdp_info.block_period;

  dp_offload_tx_hdr_fields(field_hi(c_sdp_cep_hdr_field_arr, "dp_bsn" ) downto
                           field_lo(c_sdp_cep_hdr_field_arr, "dp_bsn" )) <= dp_pipeline_data_src_out.bsn(63 downto 0);

  -- For viewing the header fields in wave window
  dp_offload_tx_header <= func_sdp_map_cep_header(dp_offload_tx_hdr_fields);

  -----------------------------------------------------------------------------
  -- dp_offload_tx_v3
  -----------------------------------------------------------------------------
  u_dp_offload_tx_v3 : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams   => 1,
    g_data_w        => c_longword_w,
    g_symbol_w      => c_byte_w,
    g_hdr_field_arr => c_sdp_cep_hdr_field_arr,
    g_hdr_field_sel => c_cep_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => reg_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_hdr_dat_miso,

    snk_in_arr(0)         => dp_pipeline_data_src_out,
    snk_out_arr(0)        => dp_pipeline_data_src_in,

    src_out_arr(0)        => dp_offload_tx_src_out,
    src_in_arr(0)         => dp_offload_tx_src_in,

    hdr_fields_in_arr(0)  => dp_offload_tx_hdr_fields,
    hdr_fields_out_arr(0) => hdr_fields_out
  );

  -----------------------------------------------------------------------------
  -- tr_10GbE_ip_checksum
  -----------------------------------------------------------------------------
  u_tr_10GbE_ip_checksum : entity tr_10GbE_lib.tr_10GbE_ip_checksum
  port map (
    rst     => dp_rst,
    clk     => dp_clk,

    snk_in  => dp_offload_tx_src_out,
    snk_out => dp_offload_tx_src_in,

    src_out => ip_checksum_src_out,
    src_in  => ip_checksum_src_in
  );

  -----------------------------------------------------------------------------
  -- dp_pipeline_ready to ease timing closure
  -----------------------------------------------------------------------------
  u_dp_pipeline_ready : entity dp_lib.dp_pipeline_ready
  port map(
    rst => dp_rst,
    clk => dp_clk,

    snk_out => ip_checksum_src_in,
    snk_in  => ip_checksum_src_out,
    src_in  => dp_pipeline_ready_src_in,
    src_out => dp_pipeline_ready_src_out
  );

  -----------------------------------------------------------------------------
  -- mms_dp_xonoff
  -----------------------------------------------------------------------------
  u_mms_dp_xonoff : entity dp_lib.mms_dp_xonoff
  generic map(
    g_default_value => '0'
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_dp_xonoff_mosi,
    reg_miso    => reg_dp_xonoff_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    -- ST sinks
    snk_out_arr(0) => dp_pipeline_ready_src_in,
    snk_in_arr(0)  => dp_pipeline_ready_src_out,
    -- ST source
    src_in_arr(0)  => out_siso,
    src_out_arr(0) => out_sosi
  );
end str;
