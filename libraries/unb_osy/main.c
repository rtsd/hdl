/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2010
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

#include "system.h"
#include "sys/alt_irq.h"

#if defined(COMPILE_FOR_QSYS)
#  include "altera_nios2_qsys_irq.h"
#elif defined(COMPILE_FOR_GEN2_UNB2)
#  include "altera_nios2_gen2_irq.h"
#else
#  include "altera_nios2_irq.h"
#endif

#include "osx_timer.h"
#include "unbosx_types.h"
#include "unbos_eth.h"
#include "unbos_i2c.h"
#include "packet.h"
#include "osx_stdio.h"
#include "altera_avalon_jtag_uart_regs.h"
/* Compile switches. */

#define BUILD 1
#undef ATEST
void main(void) __attribute__((weak, alias("alt_main")));


/*-----------------------------------------------------------------------------
 * Function name:  main
 * Parameters:     -
 * Return:         -
 * Description:    Deal with incoming command packets
 * Remark:         Use the 1ms system tick to initiate actions in a loop.
 *                 The tasks in the loop are blocking.
 *-----------------------------------------------------------------------------
 */
#if defined(COMPILE_FOR_QSYS)
#warning "Compiling for QSYS"
ALTERA_NIOS2_QSYS_IRQ_INSTANCE ( CPU_0, cpu_0);
#elif defined(COMPILE_FOR_GEN2_UNB2)
#warning "Compiling for GEN2_UNB2"
ALTERA_NIOS2_GEN2_IRQ_INSTANCE ( CPU_0, cpu_0);
#else
#warning "Compiling for SOPC"
ALTERA_NIOS2_IRQ_INSTANCE ( CPU_0, cpu_0);
#endif

void alt_main(void) {
	clock_t nof_ticks_wdi;
#ifdef ATEST
	alt_u32 atest = 0, oatest = 0;
#endif
#if defined(COMPILE_FOR_QSYS)
    ALTERA_NIOS2_QSYS_IRQ_INIT ( CPU_0, cpu_0);
#elif defined(COMPILE_FOR_GEN2_UNB2)
    ALTERA_NIOS2_GEN2_IRQ_INIT ( CPU_0, cpu_0);
#else
    ALTERA_NIOS2_IRQ_INIT ( CPU_0, cpu_0);
#endif
    alt_irq_cpu_enable_interrupts();
	OSX_TIMER_INIT ( TIMER_0, timer_0);

	// Clear the AC(cess) bit in the JTAG UART.
	// If there *is* JTAG activity (ie a host-PC
	// reading out the JTAG) then it will be set to
	// '1'. Our output routine will detect activity
	// (or lack thereof).
	// We must write '1' to clear the bit.
	IOWR_ALTERA_AVALON_JTAG_UART_CONTROL(JTAG_UART_0_BASE, ALTERA_AVALON_JTAG_UART_CONTROL_AC_MSK);

    osx_printf("unb_osy %d\n", BUILD);
#ifdef ATEST
	osx_printf("%x\n", &atest);
#endif

    // Set up networking environment
    UNBOSX_NET_Setup();

    // Take care of setting up I2C - works wether we 
    // have it or not
    UNBOS_i2c_init();

	// force watchdog reset asap
	nof_ticks_wdi = osx_clock();
    // prepare for entering mainloop
    while( 1 ) {
        clock_t       now = osx_clock();

        // Reset WDI every 1000 clockticks (1 second)
        if( (now-nof_ticks_wdi) >= 1000 ) {
            UNBOSX_ResetWatchdog();
            nof_ticks_wdi = now;
        }

		// Task: Ethernet traffic
        if( RXAVAIL(eth_status) ) {
			unsigned int replysize;
            if( (replysize=handlePacket())>0 )
				UNBOSX_NET_Send(replysize+2);
			else
				UNBOSX_NET_Discard();
        }
#if ATEST
		if( oatest!=atest ) {
			osx_printf("%x -> %x\n", oatest, atest);
			oatest = atest;
		}
#endif
    }
}
