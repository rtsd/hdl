-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide DP tx and DP rx interface to 10GbE IO.
-- Description
--
-- . Block diagram:
--                                         - XAUI
--                       ST                - 10GBASE-R
--   dp_rst -->           |                    |
--   dp_clk -->           |                    |
--                        |                    |
--    snk_in  --> fifo ---|--> tech_eth_10g ---|-->  i/o pins
--    src_out <--      <--|---              <--|---
--                        |        ^ |         |
--                                 | |
--                          _______|_v______
--       tr_ref_clk_644 --> |              |
--       tr_ref_clk_312 --> | tech_eth_10g |
--       tr_ref_clk_156 --> | clocks       |
--       tr_ref_rst_156 --> |______________|
--
-- . If the average DP data rate > 10GbE data rate then the g_tx_fifo_fill
--   can be small, otherwise:
--
--      g_tx_fifo_fill > (1-(DP data rate)/156.25MHz) * largest packet size
--
--   to avoid that the packet transmission will get a gap that will abort it.
--   The average DP data rate depends on dp_clk and on the DP data valid.
--
--   g_xon_backpressure can be enabled to set xon = 0 when the TX fill fifo is
--   full. This also makes use of an extra fifo of size g_tx_fifo_size to
--   buffer the last incoming frame when xon = 0 to prevent corrupting the frame.
--
-- Remark
--  . Note that the snk_out_arr().xon is used to indicate when the MAC cannot
--    receive new frames. If xon = 0 is ignored, the TX FIFO can overflow.
--  . xon can become low when the MAC has no link. When g_xon_backpressure
--    = TRUE, xon = 0 can also occur when the ready of the MAC is 0 for a long
--    time filling up the TX fifo. If this fifo is almost full xon is set to 0,
--    the remainder of the incoming frame is captured by an extra fifo. Therefore
--    using g_xon_backpressure = TRUE uses extra RAM.

library IEEE, common_lib, dp_lib, diag_lib, technology_lib, tech_mac_10g_lib, tech_eth_10g_lib, tr_xaui_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use technology_lib.technology_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity tr_10GbE is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_sim                    : boolean;
    g_sim_level              : natural := 1;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_macs               : natural;
    g_direction              : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_use_loopback           : boolean := false;
    g_use_mdio               : boolean := false;
    g_mdio_epcs_dis          : boolean := false;  -- TRUE disables EPCS on init; e.g. to target a 10GbE card in PC that does not support it
    g_tx_fifo_fill           : natural := 10;  -- Release tx packet only when sufficiently data is available,
    g_tx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_rx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_word_alignment_padding : boolean := false;
    g_xon_backpressure       : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R
    tr_ref_clk_312   : in  std_logic := '0';  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156   : in  std_logic := '0';  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156   : in  std_logic := '0';  -- for 10GBASE-R or for XAUI

    -- Calibration & reconfig clock
    cal_rec_clk         : in  std_logic := '0';  -- for XAUI;

    -- MM interface
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;

    reg_mac_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mac_miso        : out t_mem_miso;

    xaui_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso           : out t_mem_miso;

    reg_eth10g_mosi     : in  t_mem_mosi := c_mem_mosi_rst;  -- ETH10G (link status register)
    reg_eth10g_miso     : out t_mem_miso;

    mdio_mosi_arr       : in  t_mem_mosi_arr(g_nof_macs - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr       : out t_mem_miso_arr(g_nof_macs - 1 downto 0);

    reg_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_10gbase_r_24_miso   : out t_mem_miso;

    -- DP interface
    dp_rst              : in  std_logic := '0';
    dp_clk              : in  std_logic := '0';

    snk_out_arr         : out t_dp_siso_arr(g_nof_macs - 1 downto 0);
    snk_in_arr          : in  t_dp_sosi_arr(g_nof_macs - 1 downto 0) := (others => c_dp_sosi_rst);

    src_in_arr          : in  t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr         : out t_dp_sosi_arr(g_nof_macs - 1 downto 0);

    -- Serial XAUI IO
    xaui_tx_arr         : out t_xaui_arr(g_nof_macs - 1 downto 0);
    xaui_rx_arr         : in  t_xaui_arr(g_nof_macs - 1 downto 0) := (others => (others => '0'));

    -- Serial IO
    serial_tx_arr       : out std_logic_vector(g_nof_macs - 1 downto 0);
    serial_rx_arr       : in  std_logic_vector(g_nof_macs - 1 downto 0) := (others => '0');

    -- MDIO interface
    mdio_rst            : out std_logic;
    mdio_mdc_arr        : out std_logic_vector(g_nof_macs - 1 downto 0);
    mdio_mdat_in_arr    : in  std_logic_vector(g_nof_macs - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr   : out std_logic_vector(g_nof_macs - 1 downto 0)
  );
end tr_10GbE;

architecture str of tr_10GbE is
  signal tx_rst_arr_out                 : std_logic_vector(g_nof_macs - 1 downto 0);
  signal rx_clk_arr_out                 : std_logic_vector(g_nof_macs - 1 downto 0);
  signal rx_rst_arr_out                 : std_logic_vector(g_nof_macs - 1 downto 0);

  signal eth_ref_clk_644                : std_logic;
  signal eth_ref_clk_312                : std_logic;
  signal eth_ref_clk_156                : std_logic;
  signal eth_ref_rst_156                : std_logic;

  signal eth_tx_clk_arr                 : std_logic_vector(g_nof_macs - 1 downto 0);
  signal eth_tx_rst_arr                 : std_logic_vector(g_nof_macs - 1 downto 0);
  signal eth_rx_clk_arr                 : std_logic_vector(g_nof_macs - 1 downto 0);
  signal eth_rx_rst_arr                 : std_logic_vector(g_nof_macs - 1 downto 0);

  signal dp_fifo_sc_tx_src_out_arr      : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal dp_fifo_sc_tx_src_in_arr       : t_dp_siso_arr(g_nof_macs - 1 downto 0);

  signal dp_fifo_fill_tx_src_out_arr    : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal dp_fifo_fill_tx_src_in_arr     : t_dp_siso_arr(g_nof_macs - 1 downto 0);
  signal dp_fifo_fill_tx_snk_out_arr    : t_dp_siso_arr(g_nof_macs - 1 downto 0);

  signal mac_10g_src_out_arr            : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal mac_10g_src_in_arr             : t_dp_siso_arr(g_nof_macs - 1 downto 0);

  signal dp_fifo_dc_rx_src_out_arr      : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal dp_fifo_dc_rx_src_in_arr       : t_dp_siso_arr(g_nof_macs - 1 downto 0);

  component tr_xaui_mdio is
  generic (
    g_sim                   : boolean := false;
    g_nof_xaui              : natural := 1;  -- Up to 3 (hard XAUI only) supported
    g_mdio_epcs_dis         : boolean := false  -- TRUE disables EPCS on init; e.g. to target a 10GbE card in PC that does not support it
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                  : in  std_logic;
    tr_rst                  : in  std_logic;

    -- MM clock for register of optional MDIO master
    mm_clk                  : in  std_logic := '0';
    mm_rst                  : in  std_logic := '0';

    -- MDIO master = mm slave
    mdio_mosi_arr           : in  t_mem_mosi_arr(g_nof_xaui - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr           : out t_mem_miso_arr(g_nof_xaui - 1 downto 0);

    -- MDIO External clock and serial data.
    mdio_rst                : out std_logic;
    mdio_mdc_arr            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_in_arr        : in  std_logic_vector(g_nof_xaui - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr       : out std_logic_vector(g_nof_xaui - 1 downto 0)
  );
  end component;
begin
  ---------------------------------------------------------------------------------------
  -- Clocks and reset
  ---------------------------------------------------------------------------------------

  -- Apply the clocks from top level down such that they have their rising_edge() aligned without any delta-delay
  u_tech_eth_10g_clocks : entity tech_eth_10g_lib.tech_eth_10g_clocks
  generic map (
    g_technology     => g_technology,
    g_nof_channels   => g_nof_macs
  )
  port map (
    -- Input clocks
    -- . Reference
    tr_ref_clk_644    => tr_ref_clk_644,
    tr_ref_clk_312    => tr_ref_clk_312,
    tr_ref_clk_156    => tr_ref_clk_156,
    tr_ref_rst_156    => tr_ref_rst_156,

    -- . XAUI
    tx_rst_arr        => tx_rst_arr_out,
    rx_clk_arr        => rx_clk_arr_out,
    rx_rst_arr        => rx_rst_arr_out,

    -- Output clocks
    -- . Reference
    eth_ref_clk_644   => eth_ref_clk_644,
    eth_ref_clk_312   => eth_ref_clk_312,
    eth_ref_clk_156   => eth_ref_clk_156,
    eth_ref_rst_156   => eth_ref_rst_156,

    -- . Data
    eth_tx_clk_arr    => eth_tx_clk_arr,
    eth_tx_rst_arr    => eth_tx_rst_arr,

    eth_rx_clk_arr    => eth_rx_clk_arr,
    eth_rx_rst_arr    => eth_rx_rst_arr
  );

  ---------------------------------------------------------------------------------------
  -- TX FIFO for buffering last packet when xon = 0 to prevent corrupt frames.
  ---------------------------------------------------------------------------------------
  gen_xon_backpressure : if g_xon_backpressure generate
    gen_dp_fifo_sc_tx : for i in 0 to g_nof_macs - 1 generate
      u_dp_fifo_sc_tx : entity dp_lib.dp_fifo_sc
      generic map (
        g_technology  => g_technology,
        g_data_w      => c_xgmii_data_w,
        g_empty_w     => c_tech_mac_10g_empty_w,
        g_use_empty   => true,
        g_fifo_size   => g_tx_fifo_size
      )
      port map (
        rst         => dp_rst,
        clk         => dp_clk,

        snk_out     => snk_out_arr(i),
        snk_in      => snk_in_arr(i),

        src_in      => dp_fifo_sc_tx_src_in_arr(i),
        src_out     => dp_fifo_sc_tx_src_out_arr(i)
      );
    end generate;

    -- When MAC receives pause frames, it's ready signal is low for a long time
    -- Set xon to (xon AND ready) to enable flushing frames using external dp_xonoff.
    p_fifo_sc_tx : process(dp_fifo_fill_tx_snk_out_arr)
    begin
      dp_fifo_sc_tx_src_in_arr <= dp_fifo_fill_tx_snk_out_arr;
      for i in 0 to g_nof_macs - 1 loop
        dp_fifo_sc_tx_src_in_arr(i).xon <= dp_fifo_fill_tx_snk_out_arr(i).xon and dp_fifo_fill_tx_snk_out_arr(i).ready;
      end loop;
    end process;
  end generate;

  gen_no_xon_backpressure : if not g_xon_backpressure generate
    dp_fifo_sc_tx_src_out_arr <= snk_in_arr;
    snk_out_arr <= dp_fifo_fill_tx_snk_out_arr;
  end generate;

  ---------------------------------------------------------------------------------------
  -- TX: FIFO: dp_clk -> tx_clk and with fill level/eop trigger so we can deliver packets to the MAC fast enough
  ---------------------------------------------------------------------------------------
  gen_dp_fifo_fill_eop : for i in 0 to g_nof_macs - 1 generate
    u_dp_fifo_fill_eop : entity dp_lib.dp_fifo_fill_eop
    generic map (
      g_technology     => g_technology,
      g_use_dual_clock => true,
      g_data_w         => c_xgmii_data_w,
      g_empty_w        => c_tech_mac_10g_empty_w,
      g_use_empty      => true,
      g_fifo_fill      => g_tx_fifo_fill,
      g_fifo_size      => g_tx_fifo_size
    )
    port map (
      wr_rst      => dp_rst,
      wr_clk      => dp_clk,
      rd_rst      => eth_tx_rst_arr(i),
      rd_clk      => eth_tx_clk_arr(i),

      snk_out     => dp_fifo_fill_tx_snk_out_arr(i),
      snk_in      => dp_fifo_sc_tx_src_out_arr(i),

      src_in      => dp_fifo_fill_tx_src_in_arr(i),
      src_out     => dp_fifo_fill_tx_src_out_arr(i)
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- ETH MAC + PHY
  ---------------------------------------------------------------------------------------
  u_tech_eth_10g : entity tech_eth_10g_lib.tech_eth_10g
  generic map (
    g_technology          => g_technology,
    g_sim                 => g_sim,
    g_sim_level           => g_sim_level,  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        => g_nof_macs,
    g_direction           => g_direction,
    g_use_loopback        => g_use_loopback,
    g_pre_header_padding  => g_word_alignment_padding
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   => eth_ref_clk_644,  -- 644.531250 MHz for 10GBASE-R
    tr_ref_clk_312   => eth_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156   => eth_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156   => eth_ref_rst_156,  -- for 10GBASE-R or for XAUI

    -- Calibration & reconfig clock
    cal_rec_clk      => cal_rec_clk,  -- for XAUI;

    -- XAUI clocks
    tx_clk_arr_in    => eth_tx_clk_arr,  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    tx_rst_arr_out   => tx_rst_arr_out,
    rx_clk_arr_out   => rx_clk_arr_out,
    rx_clk_arr_in    => eth_rx_clk_arr,  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr_out   => rx_rst_arr_out,

    -- MM
    mm_clk           => mm_clk,
    mm_rst           => mm_rst,

    mac_mosi         => reg_mac_mosi,  -- MAG_10G (CSR), aggregated for all g_nof_channels
    mac_miso         => reg_mac_miso,

    xaui_mosi        => xaui_mosi,  -- XAUI control
    xaui_miso        => xaui_miso,

    reg_eth10g_mosi  => reg_eth10g_mosi,  -- ETH10G (link status register)
    reg_eth10g_miso  => reg_eth10g_miso,

    reg_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
    reg_10gbase_r_24_miso => reg_10gbase_r_24_miso,

    -- ST
    tx_snk_in_arr    => dp_fifo_fill_tx_src_out_arr,  -- 64 bit data @ 156 MHz
    tx_snk_out_arr   => dp_fifo_fill_tx_src_in_arr,

    rx_src_out_arr   => mac_10g_src_out_arr,  -- 64 bit data @ 156 MHz
    rx_src_in_arr    => mac_10g_src_in_arr,

    -- PHY serial IO
    -- . 10GBASE-R (single lane)
    serial_tx_arr    => serial_tx_arr,
    serial_rx_arr    => serial_rx_arr,

    -- . XAUI (four lanes)
    xaui_tx_arr      => xaui_tx_arr,
    xaui_rx_arr      => xaui_rx_arr
  );

  ---------------------------------------------------------------------------
  -- MDIO
  ---------------------------------------------------------------------------
  gen_mdio: if g_use_mdio = true generate
    u_tr_xaui_mdio : tr_xaui_mdio  -- ENTITY tr_xaui_lib.tr_xaui_mdio
    generic map (
      g_sim           => g_sim,
      g_nof_xaui      => g_nof_macs,
      g_mdio_epcs_dis => g_mdio_epcs_dis
    )
    port map (
      -- Transceiver PLL reference clock
      tr_clk            => eth_ref_clk_156,
      tr_rst            => eth_ref_rst_156,

      -- MM clock for register of optional MDIO master
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,

      -- MDIO master = mm slave
      mdio_mosi_arr     => mdio_mosi_arr,
      mdio_miso_arr     => mdio_miso_arr,

      -- MDIO External clock and serial data.
      mdio_rst          => mdio_rst,
      mdio_mdc_arr      => mdio_mdc_arr,
      mdio_mdat_in_arr  => mdio_mdat_in_arr,
      mdio_mdat_oen_arr => mdio_mdat_oen_arr
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- RX FIFO: rx_clk -> dp_clk
  ---------------------------------------------------------------------------------------
  gen_dp_fifo_dc_rx : for i in g_nof_macs - 1 downto 0 generate
    u_dp_fifo_dc_rx : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => g_technology,
      g_data_w      => c_xgmii_data_w,
      g_empty_w     => c_tech_mac_10g_empty_w,
      g_use_empty   => true,
      g_fifo_size   => g_rx_fifo_size
    )
    port map (
      wr_rst      => eth_rx_rst_arr(i),
      wr_clk      => eth_rx_clk_arr(i),
      rd_rst      => dp_rst,
      rd_clk      => dp_clk,

      snk_out     => mac_10g_src_in_arr(i),
      snk_in      => mac_10g_src_out_arr(i),

      src_in      => dp_fifo_dc_rx_src_in_arr(i),
      src_out     => dp_fifo_dc_rx_src_out_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- RX XON frame control
  -----------------------------------------------------------------------------
  gen_dp_xonoff : for i in g_nof_macs - 1 downto 0 generate
    u_dp_xonoff : entity dp_lib.dp_xonoff
    port map (
      rst      => dp_rst,
      clk      => dp_clk,

      in_siso  => dp_fifo_dc_rx_src_in_arr(i),
      in_sosi  => dp_fifo_dc_rx_src_out_arr(i),

      out_siso => src_in_arr(i),
      out_sosi => src_out_arr(i)
    );
  end generate;
end str;
