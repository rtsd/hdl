--------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.diag_pkg.all;

-- Purpose: test bench for diag_rx_seq control
-- Usage:
--   > as 5
--   > run -all
-- . default should run OK i.e. no mismatches reported
-- . force errors by briefly using different tx sequence at the end of the
--   stimuli process, these mismatches are reported
-- Remarks:
-- . The tb can verify rx_steps other than +1 by using g_tx_cnt_incr, it can
--   only verify one of the c_diag_seq_rx_reg_nof_steps at a time.

entity tb_diag_rx_seq is
  generic (
    g_tx_cnt_incr    : integer := -7;
    g_tx_mod         : integer := 0;
    g_rx_use_steps   : boolean := true;
    g_rx_steps_arr   : t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0) := (1, 0, -7, 3);
    g_seq_dat_w      : natural := 16  -- = 12b for Lofar RCU test data, 16b for Lofar TBBI test data
  );
end tb_diag_rx_seq;

architecture tb of tb_diag_rx_seq is
  constant c_period         : time    := 100 ns;
  constant c_nof_cycles     : natural := 100;
  constant c_latency        : natural := 2;

  constant c_rx_nof_steps   : natural := c_diag_seq_rx_reg_nof_steps;
  constant c_diag_res_w     : natural := g_seq_dat_w + 1;

  constant c_diag_res_ok    : std_logic_vector(c_diag_res_w - 1 downto 0) := '0' & TO_UVEC(               0, c_diag_res_w - 1);
  constant c_diag_res_wrong : std_logic_vector(c_diag_res_w - 1 downto 0) := '0' & TO_UVEC(2**g_seq_dat_w - 1, c_diag_res_w - 1);

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '1';
  signal rst            : std_logic;

  -- Tx test data
  signal tx_diag_en     : std_logic;
  signal tx_diag_sel    : std_logic;
  signal tx_diag_mod    : std_logic_vector(g_seq_dat_w - 1 downto 0) := TO_UVEC(g_tx_mod, g_seq_dat_w);
  signal tx_diag_req    : std_logic;
  signal seq_dat        : std_logic_vector(g_seq_dat_w - 1 downto 0);
  signal seq_val        : std_logic;

  -- Rx test data
  signal rx_diag_en        : std_logic;
  signal rx_diag_sel       : std_logic;
  signal rx_diag_steps_arr : t_integer_arr(c_rx_nof_steps - 1 downto 0) := g_rx_steps_arr;
  signal diag_res          : std_logic_vector(c_diag_res_w - 1 downto 0);
  signal diag_res_val      : std_logic;

  signal expected_res      : std_logic_vector(c_diag_res_w - 1 downto 0);
begin
  rst <= '1', '0' after c_period / 10;
  clk <= not clk or tb_end after c_period / 2;

  stimuli: process
  -- run 100 us
  begin
    tx_diag_en  <= '0';
    rx_diag_en  <= '0';
    tx_diag_sel <= '0';
    rx_diag_sel <= '0';
    tx_diag_req <= '1';

    -- COUNTER
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_ok, c_diag_res_ok);
    wait for c_period;
    tx_diag_sel <= '1';
    rx_diag_sel <= '1';
    wait for c_period;
    tx_diag_en  <= '1';
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    tx_diag_req <= '0';  -- inactive request cycle
    wait for c_period;
    tx_diag_req <= '1';
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 1 * c_period;
    tx_diag_req <= '1';
    wait for 2 * c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 3 * c_period;
    tx_diag_req <= '1';
    wait for 4 * c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 5 * c_period;
    tx_diag_req <= '1';
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- PRSG
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_wrong, c_diag_res_ok);
    wait for c_period;
    tx_diag_sel <= '0';
    rx_diag_sel <= '0';
    wait for c_period;
    tx_diag_en  <= '1';
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- COUNTER
    wait for c_latency * c_period;
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_ok, c_diag_res_ok);
    tx_diag_en  <= '0';  -- stop tx
    wait for c_period;
    tx_diag_sel <= '1';
    rx_diag_sel <= '1';
    wait for 10 * c_period;  -- wait until no more valid data arrives from tx
    rx_diag_en  <= '1';  -- restart diag_res before tx is on
    wait for 10 * c_period;
    tx_diag_en  <= '1';  -- restart tx
    wait for c_nof_cycles * c_period;
    tx_diag_req <= '0';  -- inactive request cycle
    wait for c_period;
    tx_diag_req <= '1';
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- COUNTER
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_ok, c_diag_res_ok);
    tx_diag_en  <= '0';  -- stop tx
    tx_diag_req <= '0';  -- inactive tx request
    wait for c_period;
    tx_diag_sel <= '1';
    rx_diag_sel <= '1';
    wait for 10 * c_period;  -- wait until no more valid data arrives from tx
    rx_diag_en  <= '1';  -- restart diag_res before tx is on
    wait for 10 * c_period;
    tx_diag_en  <= '1';  -- restart tx
    tx_diag_req <= '0';  -- inactive request cycle
    wait for c_period;
    tx_diag_req <= '1';
    wait for c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 4 * c_period;
    tx_diag_req <= '1';
    wait for c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 4 * c_period;
    tx_diag_req <= '1';
    wait for c_period;
    tx_diag_req <= '0';  -- inactive request cycles
    wait for 4 * c_period;
    tx_diag_req <= '1';
    wait for c_period;
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- PRSG
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_wrong, c_diag_res_ok);
    wait for c_period;
    tx_diag_sel <= '0';
    rx_diag_sel <= '0';
    wait for c_period;
    tx_diag_en  <= '1';
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    wait for c_period;
    tx_diag_en  <= '0';  -- stop tx
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- PRSG
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_wrong, c_diag_res_ok);
    wait for c_period;
    tx_diag_sel <= '0';
    rx_diag_sel <= '0';
    tx_diag_en  <= '1';
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for 1 * c_period;
    rx_diag_en  <= '1';  -- fast restart diag_res
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- slow restart diag_res
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- slow restart diag_res
    wait for c_nof_cycles * c_period;
    tx_diag_en  <= '0';  -- stop tx
    wait for c_nof_cycles * c_period;
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    -- COUNTER
    expected_res <= sel_a_b(g_rx_use_steps, c_diag_res_ok, c_diag_res_ok);
    wait for c_period;
    tx_diag_sel <= '1';
    rx_diag_sel <= '1';
    wait for c_period;
    tx_diag_en  <= '1';
    wait for 10 * c_period;
    rx_diag_en  <= '1';  -- restart diag_res
    wait for c_nof_cycles * c_period;
    --tx_diag_sel <= '0';             -- uncomment this line to force errors by using a different tx sequence
    wait for 5 * c_period;  -- for multiple cycles to show that all bits get in error state
    tx_diag_sel <= '1';
    wait for c_nof_cycles * c_period;  -- some more cylces to show that the diag_res error state is preserved
    tx_diag_en  <= '0';  -- stop tx
    rx_diag_en  <= '0';  -- stop diag_res
    wait for c_latency * c_period;

    wait for c_nof_cycles * c_period;
    tb_end <= '1';
    wait for c_nof_cycles * c_period;
    wait;
  end process;

  u_diag_tx_seq : entity work.diag_tx_seq
  generic map (
    g_cnt_incr => g_tx_cnt_incr,
    g_dat_w    => g_seq_dat_w
  )
  port map (
    clk      => clk,
    rst      => rst,
    diag_en  => tx_diag_en,
    diag_sel => tx_diag_sel,
    diag_mod => tx_diag_mod,
    diag_req => tx_diag_req,
    out_dat  => seq_dat,
    out_val  => seq_val
  );

  u_diag_rx_seq : entity work.diag_rx_seq
  generic map (
    g_use_steps  => g_rx_use_steps,
    g_nof_steps  => c_rx_nof_steps,
    g_dat_w      => g_seq_dat_w,
    g_diag_res_w => c_diag_res_w
  )
  port map (
    clk            => clk,
    rst            => rst,
    in_dat         => seq_dat,
    in_val         => seq_val,
    diag_en        => rx_diag_en,
    diag_sel       => rx_diag_sel,
    diag_steps_arr => rx_diag_steps_arr,
    diag_res       => diag_res,
    diag_res_val   => diag_res_val
  );

  p_report : process (clk)
  begin
    if rising_edge(clk) then
      if diag_res_val = '1' then
        assert unsigned(diag_res) = unsigned(expected_res)
          report "DIAG sequence mismatch occured.";
      end if;
    end if;
  end process;
end tb;
