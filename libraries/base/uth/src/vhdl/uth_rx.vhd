--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.uth_pkg.all;

-- Purpose: Extract the data block from an UTH frame.
-- Usage:
--   This entity has two architectures:
--   . rtl_adapt
--   . rtl_hold   <== PREFERRED, although both are suitable
--
--   Remarks:
--   . Both architectures verify correct with tb_uth and tb_tb_uth.
--   . Running tb_tb_uth with random ready for c_repeat=10 shows that
--     verify_done occurs 1.006 later when using rtl_hold than when using
--     rtl_adapt.
--   . If the sink does not need flow control then the RL adapter can be
--     omitted using g_use_src_in=FALSE.
--   . The RL 2->1 adapter costs 3 registers, this compares to the src_buf
--     with dp_hold_input that takes 1 register. Hence g_use_src_in=TRUE then
--     the 'rtl_hold' architecture will take less logic resources. However
--     when g_use_src_in=FALSE then it depends on whether the synthesis tool
--     can optimize such that it notices that src_hld is equivalent to src_buf.
--
-- Description:
--   From the input UTH frame stream:
--
--              ------------------------------------------------
--     ... idle | pre | sfd | tlen | data ...            | crc | idle ...
--              ------------------------------------------------
--
--   detect and output the block data:
--
--                            -----------------------
--                            | data ...            |
--                            -----------------------
--                              sop             eop
--                              val   val ...   val
--                                              err
--                              channel ...
--
--   Mark the sop and eop of each frame and check the received CRC.
--
-- . An UTH frame consists of preable, sfd, tlen, data and crc. The UTH frame
--   starts with one c_uth_preamble word. The last data word is held during the
--   crc and output at the eop. The received crc is passed on via the SOSI
--   err field, where 0 indicates OK and 1 indicates an crc error. The SOSI
--   err field is active at the output eop.
-- . The sop is detected based on a c_uth_preamble to c_uth_sfd word transition
--   and the eop is then determined by counting nof_data data words. The UTH
--   frame overhead words are stripped, only the payload data is output,
--   indicated by the output sop and eop.
-- . While counting valid data words the component is insensitive to possible
--   c_uth_preamble/c_uth_sfd words in the data. Hence the output is always a
--   full block, if for some reason the input UTH framing got corrupted, then
--   this will reflect in the crc so the SOSI err field will note error.
--   Another UTH frame may even get lost, but the UTH frame reception will
--   recover on a next sop detection.
-- . Without backpressure (so ready='1') and continuous valid data the UTH Tx
--   frame is valid from sop to eop (so no gaps) and als between UTH Tx frames
--   there are no gaps if their there are sufficient input blocks.
--   For the Rx payloads there is a 1 cycle gap just before the eop, due to the
--   processing of the CRC. Between Rx payloads there is a gap of 3 cycles
--   between the eop and the sop due to the 3 UTH frame header words (preamble,
--   sfd and tlen) that get removed from the UTH Rx packets.
-- . The uth_rx supports packet level flow control by flushing packets from
--   the sink when src_in.xon is XOFF. Therefore the indication to the sink is
--   always XON.
-- . Typically choose the rx timeout g_timeout_w=0 if a link is fixed after
--   power up (e.g. for UniBoard mesh). If the link can stop and start
--   asynchronously (e.g. changing cables, transmitter restart) then choose
--   g_timeout_w sufficient to fit the frame period (e.g. for UniBoard back
--   via CX4 instead of via backplane).
-- . In p_state the rx timeout restart after every active snk_in.valid, this in
--   principle allows using a smaller timeout that is sufficient to span the
--   inactive gaps of snk_in.valid, so then g_timeout_w <= frame period is
--   sufficient. An alternative implementation for the timeout would be to let
--   the timeout run from the sop, the timeout must then at least span the
--   frame period. The frame period can vary somewhat due to mutliplexing
--   so g_timeout_w would then have to be set to ceil_log2(frame period)+1.
--
-- Remarks:
-- . Determine nxt_crc outside the p_state machine process, because that makes
--   the code more clear and more similar to uth_tx.vhd.
-- . Do not use:
--     IF valid='1' THEN
--       CASE state IS ... etc ...
--   but instead do:
--     CASE state IS ... etc ... and evaluate valid='1' per '=>' clause,
--   because that provides better control over what to do when valid='0'
--   and it makes the code more clear.
--
-- Design steps:
-- 1) First made uth_tx, because it is needed to test this uth_rx.
-- 2) ARCHITECTURE rtl_adapt:
--    As a first step this uth_rx was implemented based on snk_in.valid and not
--    taking account of src_in.ready, because that is easier. The src_out then
--    can have two valid data after src_in.ready went low, so this then needs
--    to be accounted for by using a RL 2 -> 1 latency adapter.
-- 3) For testing the tb_uth.vhd is used, with the streaming control signals
--    always active or some random or all random, what ever is appropriate.
-- 4) Initially only g_use_this_siso=FALSE was used and later on also the TRUE
--    option was implemented.
--    . When g_use_this_siso=TRUE then in addition uth_rx can also be ready
--      when it is receiving idle or the header to increase the throughput.
--    . When g_use_this_siso=FALSE then uth_rx does not need to control snk_out
--      and it is ready when the downstream sink is ready, this may ease
--      achieving timing closure.
-- 5) ARCHITECTURE rtl_hold:
--    Finally the RL 2 -> 1 latency adapter at the output was 'replaced' by a
--    dp_hold_input at the input. The p_state machine now maintains the RL = 1
--    by using the outputs from the dp_hold_input.

entity uth_rx is
  generic (
    g_data_w        : natural := c_uth_data_max_w;
    g_len_max       : natural := 2**15 - 1;  -- g_len_max defines the maximum nof data in the payload
    g_nof_ch        : natural := 1;  -- g_nof_ch, the channels are numbered from 0 TO g_nof_ch-1, each channel represents a type of UTH frame
    g_typ_arr       : t_natural_arr := array_init(0, 1);  -- g_nof_ch = g_typ_arr'LENGTH
    g_len_arr       : t_natural_arr := array_init(1, 1);  -- g_nof_ch = g_len_arr'LENGTH
    g_use_this_siso : boolean := true;  -- default use TRUE for best throughput performance
    g_use_src_in    : boolean := true;  -- default use TRUE for src_in.ready flow control else use FALSE to avoid RL adapter when src_in.ready='1' fixed
    g_timeout_w     : natural := 0  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive snk_in.valid
  );
  port (
    rst       : in  std_logic;
    clk       : in  std_logic;
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso := c_dp_siso_rdy;
    src_out   : out t_dp_sosi
  );
end uth_rx;

architecture rtl_adapt of uth_rx is
  constant c_channel_w      : natural := ceil_log2(g_nof_ch);

  constant c_preamble       : std_logic_vector := c_uth_preamble(g_data_w - 1 downto 0);
  constant c_sfd            : std_logic_vector := c_uth_sfd( g_data_w - 1 downto 0);

  constant c_crc_w          : natural := func_uth_crc_w(g_data_w);
  constant c_crc_ok         : std_logic_vector := TO_DP_ERROR(c_uth_crc_ok);
  constant c_crc_err        : std_logic_vector := TO_DP_ERROR(c_uth_crc_err);

  constant c_timeout_cnt_w  : natural := g_timeout_w + 1;

  type t_state is (s_sfd, s_tlen, s_sop, s_data, s_crc, s_eop, s_flush);

  -- handle rx timeout
  procedure proc_handle_rx_timeout(signal   valid       : in    std_logic;
                                   signal   timeout_evt : in    std_logic;
                                   signal   clr         : out   std_logic;
                                   variable v_state     : inout t_state) is  -- use variable v_state instead of signal to avoid getting latches
  begin
    if valid = '1' then
      clr <= '1';  -- restart timeout_cnt during frame rx and remain in current state
    else
      clr <= '0';  -- let valid inactive timeout count increment
      if timeout_evt = '1' then  -- can only occur when g_timeout_w>0
        v_state := s_flush;  -- exit to flush state to finish the current rx frame with void data
      end if;
    end if;
  end proc_handle_rx_timeout;

  signal timeout_cnt          : std_logic_vector(c_timeout_cnt_w - 1 downto 0);
  signal timeout_cnt_clr      : std_logic;
  signal nxt_timeout_cnt_clr  : std_logic;
  signal timeout_evt          : std_logic;

  signal crc_init         : std_logic;
  signal crc              : std_logic_vector(c_crc_w - 1 downto 0);
  signal nxt_crc          : std_logic_vector(crc'range);

  signal rx_tlen          : std_logic_vector(g_data_w - 1 downto 0);
  signal nof_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal nof_data_hld     : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_nof_data_hld : std_logic_vector(g_data_w - 1 downto 0);
  signal channel          : std_logic_vector(c_channel_w - 1 downto 0);
  signal channel_hld      : std_logic_vector(c_channel_w - 1 downto 0);
  signal nxt_channel_hld  : std_logic_vector(c_channel_w - 1 downto 0);

  signal cnt              : natural range 0 to g_len_max + 1;  -- payload data count
  signal nxt_cnt          : natural;

  -- declare data field slv signals to ease debugging in wave window
  signal in_data          : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_in_data     : std_logic_vector(g_data_w - 1 downto 0);
  signal hold_in_data     : std_logic_vector(g_data_w - 1 downto 0);
  signal blk_data         : std_logic_vector(g_data_w - 1 downto 0);

  signal this_siso        : t_dp_siso;

  signal snk_in_hld       : t_dp_sosi;
  signal nxt_snk_in_hld   : t_dp_sosi;

  signal blk_sosi         : t_dp_sosi;
  signal nxt_blk_sosi     : t_dp_sosi;

  signal xon              : std_logic;
  signal nxt_xon          : std_logic;

  signal state            : t_state;
  signal nxt_state        : t_state;
begin
  -- Map to slv to ease monitoring in wave window
  in_data      <= snk_in.data(g_data_w - 1 downto 0);
  hold_in_data <= snk_in_hld.data(g_data_w - 1 downto 0);
  blk_data     <= blk_sosi.data(g_data_w - 1 downto 0);

  -- Input flow control
  p_snk_out : process(src_in, this_siso)
  begin
    snk_out <= src_in;  -- default
    if g_use_this_siso = true then
      snk_out <= this_siso;  -- better throughput
    end if;
    snk_out.xon <= '1';  -- force XON to sink, because uth_rx takes care of packet flush
  end process;

  no_src_in : if g_use_src_in = false generate
    src_out <= blk_sosi;
  end generate;

  use_src_in : if g_use_src_in = true generate
    u_rl_adapt : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 2,  -- uth_rx does cause one cycle latency, so using dp_latency_adapter to fit current src_in.ready again (using dp_pipeline does not apply here)
      g_out_latency => 1
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => OPEN,
      snk_in       => blk_sosi,
      -- ST source
      src_in       => src_in,
      src_out      => src_out
    );
  end generate;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      timeout_cnt_clr <= '1';
      crc          <= (others => '1');
      cnt          <= 0;
      snk_in_hld   <= c_dp_sosi_rst;
      nof_data_hld <= (others => '0');
      channel_hld  <= (others => '0');
      blk_sosi     <= c_dp_sosi_rst;
      xon          <= '0';
      state        <= s_sfd;
    elsif rising_edge(clk) then
      timeout_cnt_clr <= nxt_timeout_cnt_clr;
      crc          <= nxt_crc;
      cnt          <= nxt_cnt;
      snk_in_hld   <= nxt_snk_in_hld;
      nof_data_hld <= nxt_nof_data_hld;
      channel_hld  <= nxt_channel_hld;
      blk_sosi     <= nxt_blk_sosi;
      xon          <= nxt_xon;
      state        <= nxt_state;
    end if;
  end process;

  -- Rx timeout control
  no_timeout : if g_timeout_w = 0 generate
    timeout_evt <= '0';
  end generate;

  gen_timeout : if g_timeout_w > 0 generate
    u_timeout_cnt : entity common_lib.common_counter
    generic map (
      g_width => c_timeout_cnt_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      cnt_clr => timeout_cnt_clr,
      count   => timeout_cnt
    );

    timeout_evt <= timeout_cnt(g_timeout_w);  -- check MSbit for timeout of 2**g_timeout_w clk cycles

    assert not(rising_edge(clk) and timeout_evt = '1')
      report "uth_rx(rtl_adapt) timeout occurred!"
      severity WARNING;
  end generate;

  -- XON/XOFF packet flow control
  nxt_xon <= src_in.xon;  -- register src_in.xon to ease timing closure, because src_in.xon may come from far on the chip

  -- Hold snk_in
  nxt_snk_in_hld <= snk_in when snk_in.valid = '1' else snk_in_hld;

  -- Derive nof_data and channel number from tlen
  rx_tlen <= snk_in.data(g_data_w - 1 downto 0);

  u_rx_tlen : entity work.uth_rx_tlen
  generic map (
    g_data_w     => g_data_w,
    g_nof_ch     => g_nof_ch,
    g_typ_arr    => g_typ_arr,
    g_len_arr    => g_len_arr
  )
  port map (
    tlen      => rx_tlen,
    nof_data  => nof_data,
    channel   => channel
  );

  -- Calculate CRC
  p_crc : process(crc, crc_init, snk_in)
  begin
    nxt_crc <= crc;
    if crc_init = '1' then
      nxt_crc <= (others => '1');  -- init crc
    elsif snk_in.valid = '1' then
      nxt_crc <= func_uth_next_crc(snk_in.data(g_data_w - 1 downto 0), crc);
    end if;
  end process;

  -- State machine
  p_state : process(state, timeout_evt, src_in, xon, crc, cnt, snk_in, snk_in_hld, nof_data, nof_data_hld, channel, channel_hld)
    variable v_nxt_state : t_state;  -- use variable instead of signal to avoid getting latches with proc_handle_rx_timeout()
  begin
    nxt_timeout_cnt_clr  <= '1';  -- default no timeout_cnt

    v_nxt_state          := state;
    this_siso            <= src_in;
    crc_init             <= '0';
    nxt_cnt              <= cnt;

    nxt_nof_data_hld     <= nof_data_hld;
    nxt_channel_hld      <= channel_hld;

    nxt_blk_sosi         <= snk_in;  -- default pass on input dat
    nxt_blk_sosi.valid   <= '0';
    nxt_blk_sosi.sop     <= '0';
    nxt_blk_sosi.eop     <= '0';
    nxt_blk_sosi.channel <= RESIZE_DP_CHANNEL(channel_hld);  -- use channel derived from 'tlen' field
    nxt_blk_sosi.err     <= c_crc_ok;  -- default assume CRC will be OK

    case state is
      when s_sfd =>
        this_siso <= c_dp_siso_rdy;
        crc_init <= '1';
        nxt_cnt <= 0;
        if snk_in.valid = '1' and xon = '1' then  -- flush any input packet if src_in.xon is XOFF
          if snk_in.data(g_data_w - 1 downto 0) = c_sfd and snk_in_hld.data(g_data_w - 1 downto 0) = c_preamble then
            v_nxt_state := s_tlen;
          end if;
        end if;
      when s_tlen =>
        this_siso <= c_dp_siso_rdy;
        crc_init <= '1';
        nxt_cnt <= 0;
        if snk_in.valid = '1' then
          if unsigned(nof_data) = 0 then  -- use nof_data derived combinatorially from 'tlen' field
            v_nxt_state := s_sfd;  -- flush not supported UTH frame 'tlen'
          else
            nxt_nof_data_hld <= nof_data;  -- hold nof_data derived from 'tlen' field
            nxt_channel_hld  <= channel;  -- hold channel  derived from 'tlen' field
            this_siso <= src_in;
            if unsigned(nof_data) = 1 then  -- use nof_data derived combinatorially from 'tlen' field
              v_nxt_state := s_crc;
            else
              v_nxt_state := s_sop;
            end if;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_sop =>
        if snk_in.valid = '1' then
          nxt_blk_sosi.valid <= '1';
          nxt_blk_sosi.sop   <= '1';
          nxt_cnt <= cnt + 1;  -- first payload data
          if unsigned(nof_data_hld) = 2 then
            v_nxt_state := s_crc;
          else
            v_nxt_state := s_data;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_data =>
        if snk_in.valid = '1' then
          nxt_blk_sosi.valid <= '1';
          nxt_cnt <= cnt + 1;  -- count payload data
          if cnt = unsigned(nof_data_hld) - 2 then
            v_nxt_state := s_crc;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_crc =>
        if snk_in.valid = '1' then  -- hold the last payload data and calculate the final crc
          v_nxt_state := s_eop;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_eop =>
        if snk_in.valid = '1' then
          nxt_blk_sosi.data  <= snk_in_hld.data;
          nxt_blk_sosi.valid <= '1';
          nxt_cnt <= cnt + 1;  -- output the last payload data
          if unsigned(nof_data_hld) = 1 then
            nxt_blk_sosi.sop <= '1';  -- output sop in case the payload has only one data
          end if;
          nxt_blk_sosi.eop <= '1';
          if c_crc_w > g_data_w then  -- set in SOSI error field based on final crc result
            if snk_in.data(g_data_w - 1 downto 0) /= crc(g_data_w - 1 downto 0) then
              nxt_blk_sosi.err <= c_crc_err;
            end if;
          else
            if snk_in.data(c_crc_w - 1 downto 0) /= crc then
              nxt_blk_sosi.err <= c_crc_err;
            end if;
          end if;
          this_siso <= c_dp_siso_rdy;  -- force this_siso.ready='1', thanks to RL 2->1 we do not have to wait for active src_in.ready
          v_nxt_state := s_sfd;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when others =>  -- s_flush                    -- this state can only be reached if g_timeout_w>0
        if src_in.ready = '1' then
          nxt_blk_sosi.valid <= '1';  -- flush rx frame with void data
          nxt_cnt <= cnt + 1;  -- count payload data
          if cnt >= unsigned(nof_data_hld) - 1 then
            nxt_blk_sosi.eop <= '1';
            nxt_blk_sosi.err <= c_crc_err;  -- fill in c_crc_err for flushed rx frame
            v_nxt_state := s_sfd;
          end if;
        end if;
    end case;

    nxt_state <= v_nxt_state;
  end process;
end rtl_adapt;

architecture rtl_hold of uth_rx is
  constant c_channel_w      : natural := ceil_log2(g_nof_ch);

  constant c_preamble       : std_logic_vector := c_uth_preamble(g_data_w - 1 downto 0);
  constant c_sfd            : std_logic_vector := c_uth_sfd( g_data_w - 1 downto 0);

  constant c_crc_w          : natural := func_uth_crc_w(g_data_w);
  constant c_crc_ok         : std_logic_vector := TO_DP_ERROR(c_uth_crc_ok);
  constant c_crc_err        : std_logic_vector := TO_DP_ERROR(c_uth_crc_err);

  constant c_timeout_cnt_w  : natural := g_timeout_w + 1;

  type t_state is (s_sfd, s_tlen, s_sop, s_data, s_crc, s_eop, s_flush);

  -- handle rx timeout
  procedure proc_handle_rx_timeout(signal   valid       : in    std_logic;
                                   signal   timeout_evt : in    std_logic;
                                   signal   clr         : out   std_logic;
                                   variable v_state     : inout t_state) is  -- use variable v_state instead of signal to avoid getting latches
  begin
    if valid = '1' then
      clr <= '1';  -- restart timeout_cnt during frame rx and remain in current state
    else
      clr <= '0';  -- let valid inactive timeout count increment
      if timeout_evt = '1' then  -- can only occur when g_timeout_w>0
        v_state := s_flush;  -- exit to flush state to finish the current rx frame with void data
      end if;
    end if;
  end proc_handle_rx_timeout;

  signal timeout_cnt          : std_logic_vector(c_timeout_cnt_w - 1 downto 0);
  signal timeout_cnt_clr      : std_logic;
  signal nxt_timeout_cnt_clr  : std_logic;
  signal timeout_evt          : std_logic;

  -- declare data field slv signals to ease debugging in wave window
  signal in_data          : std_logic_vector(g_data_w - 1 downto 0);
  signal buf_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal out_data         : std_logic_vector(g_data_w - 1 downto 0);

  signal crc_init         : std_logic;
  signal crc              : std_logic_vector(c_crc_w - 1 downto 0);
  signal nxt_crc          : std_logic_vector(crc'range);

  signal data_hld         : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_data_hld     : std_logic_vector(g_data_w - 1 downto 0);

  signal rx_tlen          : std_logic_vector(g_data_w - 1 downto 0);
  signal nof_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal nof_data_hld     : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_nof_data_hld : std_logic_vector(g_data_w - 1 downto 0);
  signal channel          : std_logic_vector(c_channel_w - 1 downto 0);
  signal channel_hld      : std_logic_vector(c_channel_w - 1 downto 0);
  signal nxt_channel_hld  : std_logic_vector(c_channel_w - 1 downto 0);

  signal cnt              : natural range 0 to g_len_max + 1;  -- payload data count
  signal nxt_cnt          : natural;

  signal this_siso        : t_dp_siso;
  signal hold_src_in      : t_dp_siso;
  signal src_buf          : t_dp_sosi;
  signal nxt_src_buf      : t_dp_sosi;
  signal next_src_buf     : t_dp_sosi;
  signal pend_src_buf     : t_dp_sosi;

  signal i_src_out        : t_dp_sosi;
  signal nxt_src_out      : t_dp_sosi;

  signal xon              : std_logic;
  signal nxt_xon          : std_logic;

  signal state            : t_state;
  signal nxt_state        : t_state;
begin
  src_out <= i_src_out;

  -- Map to slv to ease monitoring in wave window
  in_data     <= snk_in.data(g_data_w - 1 downto 0);
  buf_data    <= src_buf.data(g_data_w - 1 downto 0);
  out_data    <= i_src_out.data(g_data_w - 1 downto 0);

  -- Input flow control
  p_snk_out : process(src_in, this_siso)
  begin
    snk_out <= src_in;  -- default
    if g_use_this_siso = true then
      snk_out <= this_siso;  -- better throughput
    end if;
    snk_out.xon <= '1';  -- force XON to sink, because uth_rx takes care of packet flush
  end process;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      timeout_cnt_clr <= '1';
      crc          <= (others => '1');
      cnt          <= 0;
      src_buf      <= c_dp_sosi_rst;
      data_hld     <= (others => '0');
      nof_data_hld <= (others => '0');
      channel_hld  <= (others => '0');
      i_src_out    <= c_dp_sosi_rst;
      xon          <= '0';
      state        <= s_sfd;
    elsif rising_edge(clk) then
      timeout_cnt_clr <= nxt_timeout_cnt_clr;
      crc          <= nxt_crc;
      cnt          <= nxt_cnt;
      src_buf      <= nxt_src_buf;
      data_hld     <= nxt_data_hld;
      nof_data_hld <= nxt_nof_data_hld;
      channel_hld  <= nxt_channel_hld;
      i_src_out    <= nxt_src_out;
      xon          <= nxt_xon;
      state        <= nxt_state;
    end if;
  end process;

  -- Rx timeout control
  no_timeout : if g_timeout_w = 0 generate
    timeout_evt <= '0';
  end generate;

  gen_timeout : if g_timeout_w > 0 generate
    u_timeout_cnt : entity common_lib.common_counter
    generic map (
      g_width => c_timeout_cnt_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      cnt_clr => timeout_cnt_clr,
      count   => timeout_cnt
    );

    timeout_evt <= timeout_cnt(g_timeout_w);  -- check MSbit for timeout of 2**g_timeout_w clk cycles

    assert not(rising_edge(clk) and timeout_evt = '1')
      report "uth_rx(rtl_hold) timeout occurred!"
      severity WARNING;
  end generate;

  -- XON/XOFF packet flow control
  nxt_xon <= src_in.xon;  -- register src_in.xon to ease timing closure, because src_in.xon may come from far on the chip

  -- Hold input stream register
  nxt_src_buf <= next_src_buf;

  u_hold : entity dp_lib.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => snk_in,
    -- ST source
    src_in       => hold_src_in,
    next_src_out => next_src_buf,
    pend_src_out => pend_src_buf,
    src_out_reg  => src_buf
  );

  -- Derive nof_data and channel number from tlen
  rx_tlen <= pend_src_buf.data(g_data_w - 1 downto 0);  -- can use pend_src_buf instead of next_src_buf because no need to depend on this_siso

  u_rx_tlen : entity work.uth_rx_tlen
  generic map (
    g_data_w     => g_data_w,
    g_nof_ch     => g_nof_ch,
    g_typ_arr    => g_typ_arr,
    g_len_arr    => g_len_arr
  )
  port map (
    tlen      => rx_tlen,
    nof_data  => nof_data,
    channel   => channel
  );

  -- Calculate CRC
  p_crc : process(crc, crc_init, next_src_buf)
  begin
    nxt_crc <= crc;
    if crc_init = '1' then
      nxt_crc <= (others => '1');  -- init crc
    elsif next_src_buf.valid = '1' then
      nxt_crc <= func_uth_next_crc(next_src_buf.data(g_data_w - 1 downto 0), crc);
    end if;
  end process;

  -- State machine
  p_state : process(state, timeout_evt, src_in, xon, i_src_out, crc, cnt, snk_in, next_src_buf, data_hld, nof_data, nof_data_hld, channel, channel_hld)
    variable v_nxt_state : t_state;  -- use variable instead of signal to avoid getting latches with proc_handle_rx_timeout()
  begin
    nxt_timeout_cnt_clr  <= '1';  -- default no timeout_cnt

    v_nxt_state          := state;
    this_siso            <= src_in;
    hold_src_in          <= src_in;
    crc_init             <= '0';
    nxt_cnt              <= cnt;

    nxt_data_hld         <= next_src_buf.data(g_data_w - 1 downto 0);  -- default data_hld = src_buf
    nxt_nof_data_hld     <= nof_data_hld;
    nxt_channel_hld      <= channel_hld;

    nxt_src_out          <= i_src_out;
    nxt_src_out.valid    <= '0';
    nxt_src_out.sop      <= '0';
    nxt_src_out.eop      <= '0';
    nxt_src_out.channel  <= RESIZE_DP_CHANNEL(channel_hld);  -- use channel derived from 'tlen' field
    nxt_src_out.err      <= c_crc_ok;  -- default assume CRC will be OK

    case state is
      when s_sfd =>
        this_siso   <= c_dp_siso_rdy;
        hold_src_in <= c_dp_siso_rdy;
        crc_init    <= '1';
        nxt_cnt     <= 0;
        -- For both the preamble and the sfd the valid must be active, between the preamble and the sfd there may occur invalid cycles
        if next_src_buf.valid = '1' and xon = '1' then  -- flush any input packet if src_in.xon is XOFF
          if next_src_buf.data(g_data_w - 1 downto 0) = c_sfd and data_hld(g_data_w - 1 downto 0) = c_preamble then
            v_nxt_state := s_tlen;
          end if;
        end if;
      when s_tlen =>
        this_siso   <= c_dp_siso_rdy;
        hold_src_in <= c_dp_siso_rdy;
        crc_init    <= '1';
        nxt_cnt     <= 0;
        if next_src_buf.valid = '1' then
          if unsigned(nof_data) = 0 then  -- use nof_data derived combinatorially from 'tlen' field
            v_nxt_state := s_sfd;  -- flush not supported UTH frame 'tlen'
          else
            nxt_nof_data_hld <= nof_data;  -- hold nof_data derived from 'tlen' field
            nxt_channel_hld  <= channel;  -- hold channel  derived from 'tlen' field
            this_siso <= src_in;  -- maintain active hold_src_in and now use the src_in for this_siso
            if unsigned(nof_data) = 1 then  -- use nof_data derived combinatorially from 'tlen' field
              v_nxt_state := s_crc;
            else
              v_nxt_state := s_sop;
            end if;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_sop =>
        if next_src_buf.valid = '1' then
          nxt_src_out.data  <= next_src_buf.data;
          nxt_src_out.valid <= '1';
          nxt_src_out.sop   <= '1';
          nxt_cnt <= cnt + 1;  -- first payload data
          if unsigned(nof_data_hld) = 2 then
            v_nxt_state := s_crc;
          else
            v_nxt_state := s_data;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_data =>
        if next_src_buf.valid = '1' then
          nxt_src_out.data  <= next_src_buf.data;
          nxt_src_out.valid <= '1';
          nxt_cnt <= cnt + 1;  -- count payload data
          if cnt = unsigned(nof_data_hld) - 2 then
            v_nxt_state := s_crc;
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_crc =>
        if next_src_buf.valid = '1' then  -- last payload data and calculate the final crc
          v_nxt_state := s_eop;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_eop =>
        -- We need data_hld to hold the snk_in.data and can not use src_buf for this, because src_buf will get the CRC value.
        -- However if after the CRC the src_in.ready goes low then we can not output the data yet.
        nxt_data_hld <= data_hld;  -- hold the last payload data
        if next_src_buf.valid = '1' then  -- the payload crc
          nxt_src_out.data  <= RESIZE_DP_DATA(data_hld);
          nxt_src_out.valid <= '1';
          nxt_cnt <= cnt + 1;  -- output the last payload data
          if unsigned(nof_data_hld) = 1 then
            nxt_src_out.sop <= '1';  -- output sop in case the payload has only one data
          end if;
          nxt_src_out.eop <= '1';
          if c_crc_w > g_data_w then  -- set in SOSI error field based on final crc result
            if next_src_buf.data(g_data_w - 1 downto 0) /= crc(g_data_w - 1 downto 0) then
              nxt_src_out.err <= c_crc_err;
            end if;
          else
            if next_src_buf.data(c_crc_w - 1 downto 0) /= crc then
              nxt_src_out.err <= c_crc_err;
            end if;
          end if;
          v_nxt_state := s_sfd;  -- no need to force this_siso.ready='1', because it is already '1' due to active src_in.ready
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(snk_in.valid, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when others =>  -- s_flush                    -- this state can only be reached if g_timeout_w>0
        if src_in.ready = '1' then
          nxt_src_out.valid <= '1';  -- flush rx frame with void data
          nxt_cnt <= cnt + 1;  -- count payload data
          if cnt >= unsigned(nof_data_hld) - 1 then
            nxt_src_out.eop <= '1';
            nxt_src_out.err <= c_crc_err;  -- fill in c_crc_err for flushed rx frame
            v_nxt_state := s_sfd;
          end if;
        end if;
    end case;

    nxt_state <= v_nxt_state;
  end process;
end rtl_hold;
