-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Bridge between AXI4 stream and DP stream interfaces.
-- Description:
-- . This core consists of:
--   . Combinatorial translation of one interface to the other.
--   . SOP insertion for AXI4 -> DP as AXI4 does not have a SOP equivalent.
--   . Implementation for deriving dp.empty from axi4.tkeep (see details).
--   . Ready latency adapters.
-- . Details:
--   . g_nof_bytes is used to indicate the number of bytes in the data field. Only
--     used for deriving the DP empty field.
--   . g_use_empty: when true, it will derive the DP empty field from the AXI4 tkeep signal.
--     tkeep is the AXI4 signal that indicates with a bit for each byte of the data whether
--     it is considered a valid or null byte. '1' for a valid byte or '0' for a null byte.--
--     The DP empty field is derived by counting the number of '0' bits in
--     tkeep(g_nof_bytes DOWNTO 0). This means that it is only possible to derive the empty
--     field correctly when tkeep only has bits set to '0' at the end of the vector. For example:
--     . tkeep = "11111000" is valid and will translate to empty = 0011 (3 Decimal).
--     . tkeep = "11011101" is not valid and will result in a wrong empty signal of 0010 (2 Decimal).
--     . Note that AXI4 always uses a symbol width of 8 (= byte) and thus the only DP symbol width
--       used by the empty signal that is supported is also 8.
--   . g_axi4_rl is the ready latency of the axi4_in/out interfaces.
--   . g_dp_rl is the ready latency of the dp_in/out interfaces.
--   . g_active_low_rst should be set to TRUE when in_rst is active low. This is useful as an
--     AXI4 interface often comes with an active-low reset while DP comes with an active-high
--     reset.
-- Remarks:
-- . AXI4 does not have a DP Xon or sync equivalent.

library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.axi4_stream_pkg.all;

entity axi4_stream_dp_bridge is
  generic (
    g_nof_bytes      : natural := 64;  -- Max = 64 bytes
    g_use_empty      : boolean := false;  -- When True the dp empty field is derived from axi4 tkeep.
    g_axi4_rl        : natural := 0;
    g_dp_rl          : natural := 1;
    g_active_low_rst : boolean := false  -- When True, in_rst is interpreted as active-low.
  );
  port (
    in_clk  : in std_logic := '0';
    in_rst  : in std_logic := is_true(g_active_low_rst);  -- Default state is "not in reset".

    aresetn : out std_logic := '1';  -- AXI4 active-low reset
    dp_rst  : out std_logic := '0';  -- DP active-high reset

    axi4_in_sosi  : in  t_axi4_sosi := c_axi4_sosi_rst;
    axi4_in_siso  : out t_axi4_siso := c_axi4_siso_rst;

    axi4_out_sosi : out t_axi4_sosi := c_axi4_sosi_rst;
    axi4_out_siso : in  t_axi4_siso := c_axi4_siso_rst;

    dp_in_sosi    : in  t_dp_sosi   := c_dp_sosi_rst;
    dp_in_siso    : out t_dp_siso   := c_dp_siso_rst;

    dp_out_sosi   : out t_dp_sosi   := c_dp_sosi_rst;
    dp_out_siso   : in  t_dp_siso   := c_dp_siso_rst
  );
end axi4_stream_dp_bridge;

architecture str of axi4_stream_dp_bridge is
  signal i_rst : std_logic := '0';  -- Internal active high reset.

  signal axi4_from_dp_sosi : t_dp_sosi;
  signal axi4_from_dp_siso : t_dp_siso;
  signal dp_from_axi4_sosi : t_dp_sosi;
  signal dp_from_axi4_siso : t_dp_siso;

  type t_reg is record
    r_eop             : std_logic;
    dp_from_axi4_sosi : t_dp_sosi;
  end record;

  constant c_reg_init         : t_reg := ('1', c_dp_sosi_rst);

  -- Registers
  signal d_reg                : t_reg := c_reg_init;
  signal q_reg                : t_reg := c_reg_init;
begin
  i_rst   <= not in_rst when g_active_low_rst else in_rst;
  aresetn <= not i_rst;
  dp_rst  <= i_rst;

  ----------------------------
  -- Translate DP to AXI4
  ----------------------------
  axi4_out_sosi <= func_axi4_stream_from_dp_sosi(axi4_from_dp_sosi);
  axi4_from_dp_siso <= func_axi4_stream_to_dp_siso(axi4_out_siso);

  -- Adapt Ready Latency
  u_dp_latency_adapter_dp_to_axi : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => g_dp_rl,
    g_out_latency => g_axi4_rl
  )
  port map (
    clk       => in_clk,
    rst       => i_rst,

    snk_in    => dp_in_sosi,
    snk_out   => dp_in_siso,

    src_out   => axi4_from_dp_sosi,
    src_in    => axi4_from_dp_siso
  );

  ----------------------------
  -- Translate AXI4 to DP
  ----------------------------
  q_reg <= d_reg when rising_edge(in_clk);

  p_axi4_to_dp : process(i_rst, q_reg, axi4_in_sosi, dp_from_axi4_siso.ready)
    variable v : t_reg := c_reg_init;
  begin
    v := q_reg;
    v.dp_from_axi4_sosi := func_axi4_stream_to_dp_sosi(axi4_in_sosi);  -- Does not have sop or empty yet.

    -- Generate sop on first valid after eop.
    if axi4_in_sosi.tvalid = '1' and g_axi4_rl = 1 then  -- axi4 ready latency = 1
      if axi4_in_sosi.tlast = '1' then
        v.r_eop := '1';
      elsif q_reg.r_eop = '1' then
        v.dp_from_axi4_sosi.sop := '1';
        v.r_eop := '0';
      else
        v.dp_from_axi4_sosi.sop := '0';
      end if;

    elsif axi4_in_sosi.tvalid = '1' and g_axi4_rl = 0 then  -- axi4 ready latency = 0
      if axi4_in_sosi.tlast = '1' and dp_from_axi4_siso.ready = '1' then
        v.r_eop := '1';
      elsif q_reg.r_eop = '1' and dp_from_axi4_siso.ready = '1' then
        v.r_eop := '0';
      end if;
      v.dp_from_axi4_sosi.sop := q_reg.r_eop;
    end if;

    -- Derive Empty from tkeep
    if g_use_empty then
      v.dp_from_axi4_sosi.empty := func_axi4_stream_tkeep_to_dp_empty(axi4_in_sosi.tkeep(g_nof_bytes - 1 downto 0));
    end if;

    -- Reset
    if i_rst = '1' then
      v := c_reg_init;
    end if;

    d_reg <= v;
  end process;

  dp_from_axi4_sosi <= d_reg.dp_from_axi4_sosi;
  axi4_in_siso <= func_axi4_stream_from_dp_siso(dp_from_axi4_siso);

  -- Adapt Ready Latency
  u_dp_latency_adapter_axi_to_dp : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => g_axi4_rl,
    g_out_latency => g_dp_rl
  )
  port map (
    clk       => in_clk,
    rst       => i_rst,

    snk_in    => dp_from_axi4_sosi,
    snk_out   => dp_from_axi4_siso,

    src_out   => dp_out_sosi,
    src_in    => dp_out_siso
  );
end str;
