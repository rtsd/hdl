-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package bf_pkg is
  constant c_bf_note_fifo_is_ful : boolean  := true;
  constant c_bf_burst_nof_blocks : positive := 4;  -- Use 1 for no initial burst, use > 1 to support initial blocks arriving in a burst (= c_nof_no_gap+1 in tb_bf.vhd)
  constant c_bf_max_nof_bf_units : natural  := 4;  -- Setting the maximum number of bf_units in a FPGA.

  -- TYPE DECLARATIONS --------------------------------------------------------

  -- Record definition that conatins all the generics that are used in the bf_unit
  type t_c_bf is record
    nof_signal_paths  : positive;  -- = 64
    nof_input_streams : positive;  -- = 16
    nof_subbands      : positive;  -- = 24
    nof_weights       : positive;  -- = 256
    nof_bf_units      : positive;  -- = 4
    in_dat_w          : positive;  -- = 16
    in_weight_w       : positive;  -- = 16
    bst_gain_w        : integer;  -- = 1
    bst_dat_w         : positive;  -- = 16
    out_gain_w        : integer;  -- = -5
    out_dat_w         : positive;  -- = 8
    stat_data_w       : positive;  -- = 56
    stat_data_sz      : positive;  -- = 2
  end record;

  --CONSTANT c_bf : t_c_bf := (64, 16, 24, 256, 4, 16, 16,  1, 16, -5, 8, 56, 2);  -- bst_gain_w= 1 for 16b and out_gain_w=-5 for 8b as in Fig 10 of RP1377 v0.41
  constant c_bf : t_c_bf := (64, 16, 24, 256, 4, 16, 16,  0, 16, -6, 18, 56, 2);  -- preserve 1 more LSbits for the bst_dat and out_dat outputs
end bf_pkg;

package body bf_pkg is
end bf_pkg;
