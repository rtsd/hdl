#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
"""
On execution, this script will start ModelSim and within ModelSim:
. unb_dp_offload will be compiled;
. The 3 revisions of unb_dp_offload will be loaded sequentially and tested using
  the same tc_unb_dp_offload.py script.
    
If --hold is not passed, the running ModelSim simulation window will be killed
after completion.
"""

import sys, os
sys.path.append(os.environ['UNB']+'/Firmware/sim/python')
from auto_sim import *

LIBRARY_NAME = 'unb_dp_offload'
TB_NAME      = 'tb_unb_dp_offload'
TARGET_NODES = ' --unb 0 --fn 0 '
STREAMS      = ' -r 0:2'
COMMANDS     = ['tc_unb_dp_offload.py' + TARGET_NODES + STREAMS]

# Define an inital delay between start of the simulation and the start of the COMMANDS scripts
INIT_DELAY_NS = 0 

# Generics: design name
DESIGN_NAMES = ["unb_dp_offload", "unb_dp_offload_1GbE", "fn_dp_offload_10GbE"] 

# Sequentially run the test case against the 3 design revisions
generics = {}
results = []
for design_name in DESIGN_NAMES:
    generics['g_design_name'] = design_name
    other=None
    if design_name=='fn_dp_offload_10GbE':
        # Designs that include xaui/10GbE require the following explicitely passed libraries for reasons unknown.
        other = " -L lpm_ver -L sgate_ver -L stratixiv_hssi_ver -L stratixiv_ver"
    results.append( auto_sim(os.environ['UNB'], LIBRARY_NAME, TB_NAME, COMMANDS, INIT_DELAY_NS, generics, other) )  

# Return the result
if all_equal_to(results, 'PASSED')==True:
    result = 'PASSED'
else:
    result = 'FAILED'

sys.exit(result)
