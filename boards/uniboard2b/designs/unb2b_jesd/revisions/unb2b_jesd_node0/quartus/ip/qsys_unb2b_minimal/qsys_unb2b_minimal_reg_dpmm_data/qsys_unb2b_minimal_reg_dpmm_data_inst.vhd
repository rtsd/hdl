  component qsys_unb2b_minimal_reg_dpmm_data is
    generic (
      g_adr_w : natural := 5;
      g_dat_w : natural := 32
    );
    port (
      coe_address_export   : out std_logic_vector(g_adr_w - 1 downto 0);  -- export
      coe_clk_export       : out std_logic;  -- export
      avs_mem_address      : in  std_logic_vector(g_adr_w - 1 downto 0) := (others => 'X');  -- address
      avs_mem_write        : in  std_logic                            := 'X';  -- write
      avs_mem_writedata    : in  std_logic_vector(g_dat_w - 1 downto 0) := (others => 'X');  -- writedata
      avs_mem_read         : in  std_logic                            := 'X';  -- read
      avs_mem_readdata     : out std_logic_vector(g_dat_w - 1 downto 0);  -- readdata
      coe_read_export      : out std_logic;  -- export
      coe_readdata_export  : in  std_logic_vector(g_dat_w - 1 downto 0) := (others => 'X');  -- export
      coe_reset_export     : out std_logic;  -- export
      csi_system_clk       : in  std_logic                            := 'X';  -- clk
      csi_system_reset     : in  std_logic                            := 'X';  -- reset
      coe_write_export     : out std_logic;  -- export
      coe_writedata_export : out std_logic_vector(g_dat_w - 1 downto 0)  -- export
    );
  end component qsys_unb2b_minimal_reg_dpmm_data;

  u0 : component qsys_unb2b_minimal_reg_dpmm_data
    generic map (
      g_adr_w => NATURAL_VALUE_FOR_g_adr_w,
      g_dat_w => NATURAL_VALUE_FOR_g_dat_w
    )
    port map (
      coe_address_export   => CONNECTED_TO_coe_address_export,  -- address.export
      coe_clk_export       => CONNECTED_TO_coe_clk_export,  -- clk.export
      avs_mem_address      => CONNECTED_TO_avs_mem_address,  -- mem.address
      avs_mem_write        => CONNECTED_TO_avs_mem_write,  -- .write
      avs_mem_writedata    => CONNECTED_TO_avs_mem_writedata,  -- .writedata
      avs_mem_read         => CONNECTED_TO_avs_mem_read,  -- .read
      avs_mem_readdata     => CONNECTED_TO_avs_mem_readdata,  -- .readdata
      coe_read_export      => CONNECTED_TO_coe_read_export,  -- read.export
      coe_readdata_export  => CONNECTED_TO_coe_readdata_export,  -- readdata.export
      coe_reset_export     => CONNECTED_TO_coe_reset_export,  -- reset.export
      csi_system_clk       => CONNECTED_TO_csi_system_clk,  -- system.clk
      csi_system_reset     => CONNECTED_TO_csi_system_reset,  -- system_reset.reset
      coe_write_export     => CONNECTED_TO_coe_write_export,  -- write.export
      coe_writedata_export => CONNECTED_TO_coe_writedata_export  -- writedata.export
    );
