-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for eth_ihl_to_20.vhd
-- Description:
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_network_layers_pkg.all;
use work.eth_pkg.all;

entity tb_eth_IHL_to_20 is
end tb_eth_IHL_to_20;

architecture tb of tb_eth_IHL_to_20 is
  constant clk_period : time := 5 ns;  -- 100 MHz

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;

  signal snk_in         : t_dp_sosi := c_dp_sosi_rst;
  signal snk_out        : t_dp_siso;

  signal src_in         : t_dp_siso := c_dp_siso_rdy;
  signal src_out        : t_dp_sosi;

  type int_arr is array (natural range <>) of integer;
  constant c_IHL_to_test : int_arr(1 to 11) := (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
  constant c_len_to_test : int_arr(1 to 5) := (0, 1, 16, 20, 3000);

  procedure gen_eth_frame (constant IHL             : natural;
                           constant UDP_payload_len : natural;
                           signal   clk             : in std_logic;
                           signal   snk_in          : inout t_dp_sosi) is
  begin
    snk_in.sop   <= '1';
    snk_in.valid <= '1';
    snk_in.data  <= RESIZE_DP_DATA(X"0000FFFF");  -- Eth header
    wait until rising_edge(clk);
    snk_in.sop   <= '0';
    snk_in.data  <= RESIZE_DP_DATA(X"FFFFFFFF");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"10FA0004");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"00000800");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"4" &  -- IPv4 header
                                   TO_UVEC(IHL, c_network_ip_header_length_w) &
                                   X"00" &
                                   TO_UVEC((IHL + UDP_payload_len + 2) * 4, c_network_ip_total_length_w));
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"00004000");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"80110000");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"0A0B0001");
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(X"0A0B00FF");
    wait until rising_edge(clk);

    for I in 6 to IHL loop
      snk_in.data  <= RESIZE_DP_DATA(X"BAD000" & TO_UVEC(I, c_network_ip_header_length_w));  -- optionnal option word
      wait until rising_edge(clk);
    end loop;

    snk_in.data  <= RESIZE_DP_DATA(X"10FA10FA");  -- UDP header
    wait until rising_edge(clk);
    snk_in.data  <= RESIZE_DP_DATA(TO_UVEC((UDP_payload_len + 2) * 4, c_network_udp_total_length_w) & X"0000");
    wait until rising_edge(clk);

    for I in 0 to UDP_payload_len - 1 loop  -- UDP payload
      snk_in.data  <= RESIZE_DP_DATA(X"BEEF" & TO_UVEC(I, 16));
      wait until rising_edge(clk);
    end loop;

    snk_in.data  <= RESIZE_DP_DATA(X"CCCCCCCC");  -- Eth CRC
    snk_in.eop   <= '1';
    wait until rising_edge(clk);

    snk_in.data  <= (others => '0');
    snk_in.valid <= '0';
    snk_in.eop   <= '0';
    wait until rising_edge(clk);
  end procedure gen_eth_frame;

  procedure check_eth_frame ( constant UDP_payload_len : natural;
                              signal   clk             : in std_logic;
                              signal   src_out         : in t_dp_sosi) is
    constant c_IHL : natural := 5;
  begin
     -- Eth header
    wait until falling_edge(clk) and src_out.valid = '1' and src_out.sop = '1';
    assert src_out.data(31 downto 0) = X"0000FFFF"
      report "Wrong word align and Destination MAC"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"FFFFFFFF"
      report "Wrong Destination MAC"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"10FA0004"
      report "Wrong Source MAC"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"00000800"
      report "Wrong Source MAC and EtherType"
      severity FAILURE;

    -- IPv4 header
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"4" &
             TO_UVEC(c_IHL, c_network_ip_header_length_w) &
             X"00" &
             TO_UVEC((c_IHL + UDP_payload_len + 2) * 4, c_network_ip_total_length_w)
      report "Wrong Version / IHL / Total Length"
      severity FAILURE;

    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"00004000"
      report "Wrong identification / Flags / Frag Offset"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"80110000"
      report "Wrong TTL / Protocol / Checksum"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"0A0B0001"
      report "Wrong Source IP"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"0A0B00FF"
      report "Wrong Dest IP"
      severity FAILURE;
    -- No options Here

    -- UDP header
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = X"10FA10FA"
      report "Wrong UDP ports"
      severity FAILURE;
    wait until falling_edge(clk) and src_out.valid = '1';
    assert src_out.data(31 downto 0) = TO_UVEC((UDP_payload_len + 2) * 4, c_network_udp_total_length_w) &
             X"0000"
      report "Wrong UDP length / CRC"
      severity FAILURE;
    -- UDP payload
    for I in 0 to UDP_payload_len - 1 loop
      wait until falling_edge(clk) and src_out.valid = '1';
      assert src_out.data(31 downto 0) = X"BEEF" & TO_UVEC(I, 16)
        report "Wrong UDP Payload"
        severity FAILURE;
--      ASSERT src_out.data(31 downto 0) = X"BEEF" & TO_UVEC(I,16) REPORT "Wrong UDP Payload: 0xBEEF" & TO_UVEC(I,16)'IMAGE                SEVERITY FAILURE;
    end loop;

    -- Eth CRC
    wait until falling_edge(clk) and src_out.valid = '1' and src_out.eop   <= '1';
    assert src_out.data(31 downto 0) = X"CCCCCCCC"
      report "Wrong Eth CRC"
      severity FAILURE;
  end procedure check_eth_frame;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  gen_frame: process
  begin
    wait until rst = '0';

    for len_n in c_len_to_test'range loop
      for IHL_n in c_IHL_to_test'range loop
        wait for 50 ns; wait until rising_edge(clk);
        gen_eth_frame (c_IHL_to_test(IHL_n),
                       c_len_to_test(len_n),
                       clk, snk_in);
      end loop;
    end loop;

    wait for 1 ms;
    if tb_end = '0' then
      assert false
        report "ERROR: Processing was too long. DUT is stuck"
        severity FAILURE;
    end if;
    wait;
  end process;

  dut : entity work.eth_IHL_to_20
  generic map (
    incoming_IHL => 24
  )
  port map (
    rst         => rst,
    clk         => clk,

    -- Streaming Sink
    snk_in      => snk_in,
    snk_out     => snk_out,

    -- Streaming Source
    src_in      => src_in,
    src_out     => src_out
  );

  check_frame: process
  begin
    for len_n in c_len_to_test'range loop
      for IHL_n in c_IHL_to_test'range loop
        check_eth_frame (c_len_to_test(len_n), clk, src_out);
      end loop;
    end loop;

    wait for 1 us;
    tb_end <= '1';
    assert false
      report "Simulation tb_eth_IHL_to_20 finished."
      severity NOTE;
    wait;
  end process;
end tb;
