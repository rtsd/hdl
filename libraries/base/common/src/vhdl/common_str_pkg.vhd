-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Collection of commonly used string funtions
-- Interface:
-- . [n/a]
-- Description:
-- . None

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.TEXTIO.all;
use IEEE.std_logic_textio.all;
use common_lib.common_pkg.all;

package common_str_pkg is
  type t_str_4_arr is array (integer range <>) of string(1 to 4);

  function nof_digits(number: natural) return natural;
  function nof_digits_int(number: integer) return natural;

  function bool_to_str(bool : boolean) return string;
  function time_to_str(in_time : time) return string;
  function str_to_time(in_str : string) return time;
  function slv_to_str(slv : std_logic_vector) return string;
  function sl_to_str(sl : std_logic) return string;
  function str_to_hex(str : string) return string;
  function slv_to_hex(slv : std_logic_vector) return string;
  function hex_to_slv(str : string) return std_logic_vector;

  function hex_nibble_to_slv(c: character) return std_logic_vector;

  function int_to_str(int: integer) return string;
  function int_to_str(int, w: integer) return string;
  function real_to_str(re: real; width : integer; digits : integer) return string;

  procedure print_str(str : string);
  procedure print_str(str: string; enable: boolean);

  function str_to_ascii_integer_arr(s: string) return t_integer_arr;
  function str_to_ascii_slv_8_arr(  s: string) return t_slv_8_arr;
  function str_to_ascii_slv_32_arr( s: string) return t_slv_32_arr;
  function str_to_ascii_slv_32_arr( s: string; arr_size : natural) return t_slv_32_arr;
end common_str_pkg;

package body common_str_pkg is
  function nof_digits(number: natural) return natural is
  -- Returns number of digits in a natural number. Only used in string processing, so defined here.
  -- log10(0) is not allowed so:
  -- . nof_digits(0) = 1
  -- We're adding 1 so:
  -- . nof_digits(1) = 1
  -- . nof_digits(9) = 1
  -- . nof_digits(10) = 2
  begin
    if number > 0 then
      return floor_log10(number) + 1;
    else
      return 1;
    end if;

  end;

  function nof_digits_int(number: integer) return natural is
  -- Returns number of digits in a natural number. Only used in string processing, so defined here.
  -- log10(0) is not allowed so:
  -- . nof_digits(0) = 1
  -- We're adding 1 so:
  -- . nof_digits(1) = 1
  -- . nof_digits(9) = 1
  -- . nof_digits(10) = 2
  -- . nof_digits(1) = 2
  begin
    if number = 0 then
      return 1;
    else
      if number > 0 then
        return floor_log10(number) + 1;
      else
        return floor_log10(-1 * number) + 2;
      end if;
    end if;
  end;

  function bool_to_str(bool : boolean) return string is
    constant c_max_len_bool : natural := 5;  -- "TRUE", "FALSE"
    variable v_line: LINE;
    variable v_str: string(1 to c_max_len_bool) := (others => ' ');
  begin
    STD.TEXTIO.WRITE(v_line, bool);
    v_str(v_line.ALL'range) := v_line.all;
    deallocate(v_line);
    return v_str;
  end;

  function time_to_str(in_time : time) return string is
    constant c_max_len_time : natural := 20;
    variable v_line         : LINE;
    variable v_str          : string(1 to c_max_len_time) := (others => ' ');
  begin
    write(v_line, in_time);
    v_str(v_line.ALL'range) := v_line.all;
    deallocate(v_line);
    return v_str;
  end;

  function str_to_time(in_str : string) return time is
  begin
    return time'value(in_str);
  end;

  function slv_to_str(slv : std_logic_vector) return string is
    variable v_line : LINE;
    variable v_str  : string(1 to slv'length) := (others => ' ');
  begin
     write(v_line, slv);
     v_str(v_line.ALL'range) := v_line.all;
     deallocate(v_line);
     return v_str;
  end;

  function sl_to_str(sl : std_logic) return string is
  begin
     return slv_to_str(slv(sl));
  end;

  function str_to_hex(str : string) return string is
    constant c_nof_nibbles : natural := ceil_div(str'LENGTH, c_nibble_w);
    variable v_nibble_arr  : t_str_4_arr(0 to c_nof_nibbles - 1) := (others => (others => '0'));
    variable v_hex         : string(1 to c_nof_nibbles) := (others => '0');
  begin
    for i in 0 to v_hex'right - 1 loop
      v_nibble_arr(i) := slice_up(str, c_nibble_w, i, '0');

      case v_nibble_arr(i) is
        when "0000" => v_hex(i + 1) := '0';
        when "0001" => v_hex(i + 1) := '1';
        when "0010" => v_hex(i + 1) := '2';
        when "0011" => v_hex(i + 1) := '3';
        when "0100" => v_hex(i + 1) := '4';
        when "0101" => v_hex(i + 1) := '5';
        when "0110" => v_hex(i + 1) := '6';
        when "0111" => v_hex(i + 1) := '7';
        when "1000" => v_hex(i + 1) := '8';
        when "1001" => v_hex(i + 1) := '9';
        when "1010" => v_hex(i + 1) := 'A';
        when "1011" => v_hex(i + 1) := 'B';
        when "1100" => v_hex(i + 1) := 'C';
        when "1101" => v_hex(i + 1) := 'D';
        when "1110" => v_hex(i + 1) := 'E';
        when "1111" => v_hex(i + 1) := 'F';
        when others => v_hex(i + 1) := 'X';
      end case;
    end loop;
    return v_hex;
  end;

  function slv_to_hex(slv :std_logic_vector) return string is
  begin
    return str_to_hex(slv_to_str(slv));
  end;

  function hex_to_slv(str: string) return std_logic_vector is
  constant c_length : natural := str'length;
  variable v_str    : string(1 to str'length) := str;  -- Keep local copy of str to prevent range mismatch
  variable v_result : std_logic_vector(c_length * 4 - 1 downto 0);
  begin
   for i in c_length downto 1 loop
       v_result(3 + (c_length - i) * 4  downto (c_length - i) * 4) := hex_nibble_to_slv(v_str(i));
   end loop;
   return v_result;
  end;

  function hex_nibble_to_slv(c: character) return std_logic_vector is
    variable v_result : std_logic_vector(3 downto 0);
  begin
    case c is
      when '0' => v_result :=  "0000";
      when '1' => v_result :=  "0001";
      when '2' => v_result :=  "0010";
      when '3' => v_result :=  "0011";
      when '4' => v_result :=  "0100";
      when '5' => v_result :=  "0101";
      when '6' => v_result :=  "0110";
      when '7' => v_result :=  "0111";
      when '8' => v_result :=  "1000";
      when '9' => v_result :=  "1001";
      when 'A' => v_result :=  "1010";
      when 'B' => v_result :=  "1011";
      when 'C' => v_result :=  "1100";
      when 'D' => v_result :=  "1101";
      when 'E' => v_result :=  "1110";
      when 'F' => v_result :=  "1111";
      when 'a' => v_result :=  "1010";
      when 'b' => v_result :=  "1011";
      when 'c' => v_result :=  "1100";
      when 'd' => v_result :=  "1101";
      when 'e' => v_result :=  "1110";
      when 'f' => v_result :=  "1111";
      when 'x' => v_result :=  "XXXX";
      when 'X' => v_result :=  "XXXX";
      when 'z' => v_result :=  "ZZZZ";
      when 'Z' => v_result :=  "ZZZZ";
    when others => v_result := "0000";
    end case;
    return v_result;
  end hex_nibble_to_slv;

  function int_to_str(int: integer) return string is
    variable v_line: LINE;
    variable v_str: string(1 to nof_digits_int(int)) := (others => ' ');
  begin
    STD.TEXTIO.WRITE(v_line, int);
    v_str(v_line.ALL'range) := v_line.all;
    deallocate(v_line);
    return v_str;
  end;

  function int_to_str(int, w: integer) return string is
    constant c_len: natural := nof_digits_int(int);
    variable v_line: LINE;
    variable v_str: string(1 to c_len) := (others => ' ');
    variable v_ret: string(1 to w) := (others => ' ');
  begin
    STD.TEXTIO.WRITE(v_line, int);
    v_str(v_line.ALL'range) := v_line.all;
    deallocate(v_line);
    v_ret(w - c_len + 1 to w) := v_str;  -- right align v_str in v_ret
    return v_ret;
  end;

  function real_to_str(re: real; width : integer; digits : integer) return string is
    -- . The number length is width + 1, with +1 for the . in the floating point number.
    --   However if width is too small to fit the number, then it will use more characters.
    --   Therefore define sufficiently large v_str(20) and return actual number width or
    --   use the requested width if that fits.
    -- . The digits defines the number of digits after the . in floating point number.
    --   Use digits = 0 for exponent xxxxxe-yy notation.
    variable v_line: LINE;
    variable v_str: string(1 to 20) := (others => ' ');
    variable v_len: natural;
  begin
    STD.TEXTIO.WRITE(v_line, re, right, width, digits);
    v_str(v_line.ALL'range) := v_line.all;
    v_len := v_line.ALL'length;
    deallocate(v_line);
    if width > v_len then
      return v_str(1 to width);
    else
      return v_str(1 to v_len);
    end if;
  end;

  procedure print_str(str: string) is
    variable v_line: LINE;
  begin
    write(v_line, str);
    writeline(output, v_line);
    deallocate(v_line);
  end;

  procedure print_str(str: string; enable: boolean) is
    variable v_line: LINE;
  begin
    if enable then
      print_str(str);
    end if;
  end;

  function str_to_ascii_integer_arr(s: string) return t_integer_arr is
    variable r: t_integer_arr(0 to s'right - 1);
  begin
    for i in s'range loop
      r(i - 1) := character'pos(s(i));
    end loop;
    return r;
  end;

  function str_to_ascii_slv_8_arr(s: string) return t_slv_8_arr is
    variable r: t_slv_8_arr(0 to s'right - 1);
  begin
    for i in s'range loop
      r(i - 1) := TO_UVEC(str_to_ascii_integer_arr(s)(i - 1), 8);
    end loop;
    return r;
  end;

  -- Returns minimum array size required to fit the string
  function str_to_ascii_slv_32_arr(s: string) return t_slv_32_arr is
    constant c_slv_8: t_slv_8_arr(0 to s'right - 1) := str_to_ascii_slv_8_arr(s);
    constant c_bytes_per_word : natural := 4;
    -- Initialize all elements to (OTHERS=>'0') so any unused bytes become a NULL character
    variable r: t_slv_32_arr(0 to ceil_div(s'right * c_byte_w, c_word_w) - 1) := (others => (others => '0'));
  begin
    for word in r'range  loop  -- 0, 1
      for byte in 0 to c_bytes_per_word - 1 loop  -- 0,1,2,3
        if byte + c_bytes_per_word * word <= c_slv_8'right then
          r(word)(byte * c_byte_w + c_byte_w - 1 downto byte * c_byte_w) := c_slv_8(byte + c_bytes_per_word * word);
        end if;
      end loop;
    end loop;

    return r;
  end;

  -- Overloaded version to match array size to arr_size
  function str_to_ascii_slv_32_arr(s: string; arr_size: natural) return t_slv_32_arr is
    constant slv_32: t_slv_32_arr(0 to ceil_div(s'right * c_byte_w, c_word_w) - 1) := str_to_ascii_slv_32_arr(s);
    variable r: t_slv_32_arr(0 to arr_size-1) := (others => (others => '0'));
  begin
    for word in slv_32'range  loop
      r(word) := slv_32(word);
    end loop;
    return r;
  end;
end common_str_pkg;
