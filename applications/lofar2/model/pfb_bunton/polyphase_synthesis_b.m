function out = polyphase_synthesis_b(in,filt,step)

%polyphase synthesis on blocks of filterbank data
%       John Bunton original version Oct 2003

% in = input data, 2D array of multiple blocks of FFT data.
%          block = number if frequency channel
% filter = prototype lowpass filter 
% step = increment along data at output of IFFT for critical sampling this
%           equal to block


s=size(in);
block=s(2); %length of block
os=block/step % oversapling ratio
nblocks=s(1);%number of FFT blocks
phases=ceil(length(filt)/block)   ;

f=(1:block*phases)*0;
f(1:length(filt))=filt(1:length(filt));

for n=1:nblocks
    temp(n,1:block)=real(ifft(in(n,:)));
end

out=(1:nblocks*step + block*phases)*0;
for n=1:nblocks
    temp2=(1:block*phases)*0;
    
    for m=0:phases-1
        
        temp2(1+m*block:(m+1)*block)= temp(n,1:block);
        
    end
    temp2=temp2.*filt;
    range=1+(n-1)*step :block*phases + (n-1)*step;
    out(range)=out(range)+temp2; % for continuous time output
    %out(n,:)=temp2; %for output in blocks
end