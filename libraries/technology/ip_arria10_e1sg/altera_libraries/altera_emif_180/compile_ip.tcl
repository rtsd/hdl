#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist
#
vmap  altera_avalon_mm_bridge_180         ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vlog      "$IP_DIR/../altera_avalon_mm_bridge_180/sim/altera_avalon_mm_bridge.v"  -work altera_avalon_mm_bridge_180


vmap altera_emif_arch_nf_180 ./work/
# ddr4_4g_1600
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"

  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_emif_arch_nf_180_ud6bb7y_top.sv"                -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_emif_arch_nf_180_ud6bb7y_io_aux.sv"             -work altera_emif_arch_nf_180
  vcom      "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_emif_arch_nf_180_ud6bb7y.vhd"                   -work altera_emif_arch_nf_180

# ddr4_4g_2000
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"

  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_emif_arch_nf_180_n4j75iy_top.sv"                -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_emif_arch_nf_180_n4j75iy_io_aux.sv"             -work altera_emif_arch_nf_180
  vcom      "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_emif_arch_nf_180_n4j75iy.vhd"                   -work altera_emif_arch_nf_180

# ddr4_8g_1600
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"

  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_emif_arch_nf_180_7cl5ama_top.sv"                -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_emif_arch_nf_180_7cl5ama_io_aux.sv"             -work altera_emif_arch_nf_180
  vcom      "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_emif_arch_nf_180_7cl5ama.vhd"                   -work altera_emif_arch_nf_180

# ddr4_8g_2400
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"

  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_top.sv"                -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_io_aux.sv"             -work altera_emif_arch_nf_180
  vcom      "$IP_DIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i.vhd"                   -work altera_emif_arch_nf_180

# common dependencies
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_bufs.sv"                                                        -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_se_i.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_se_o.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_df_i.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_df_o.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_cp_i.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_bdir_df.sv"                                                 -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_bdir_se.sv"                                                 -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_unused.sv"                                                  -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_cal_counter.sv"                                                 -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll.sv"                                                         -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll_fast_sim.sv"                                                -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll_extra_clks.sv"                                              -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_oct.sv"                                                         -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_core_clks_rsts.sv"                                              -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hps_clks_rsts.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles_wrap.sv"                                               -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles.sv"                                                    -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles_abphy.sv"                                              -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_abphy_mux.sv"                                                   -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_avl_if.sv"                                                  -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_sideband_if.sv"                                             -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_mmr_if.sv"                                                  -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_amm_data_if.sv"                                             -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_ast_data_if.sv"                                             -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_afi_if.sv"                                                      -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_seq_if.sv"                                                      -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_regs.sv"                                                        -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_oct.sv"                                                                      -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_oct_um_fsm.sv"                                                               -work altera_emif_arch_nf_180
  vlog      "$IP_DIR/../altera_emif_arch_nf_180/sim/altera_std_synchronizer_nocut.v"                                                    -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/mem_array_abphy.sv"                                                                 -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_abphy.sv"                                                       -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_encrypted_abphy.sv"                                             -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_nf5es_encrypted_abphy.sv"                                       -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/io_12_lane_bcm__nf5es_abphy.sv"                                                     -work altera_emif_arch_nf_180
  vlog -sv  "$IP_DIR/../altera_emif_arch_nf_180/sim/io_12_lane__nf5es_abphy.sv"                                                         -work altera_emif_arch_nf_180

vmap  altera_reset_controller_180         ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vlog      "$IP_DIR/../altera_reset_controller_180/sim/mentor/altera_reset_controller.v"                                               -work altera_reset_controller_180
  vlog      "$IP_DIR/../altera_reset_controller_180/sim/mentor/altera_reset_synchronizer.v"                                             -work altera_reset_controller_180

vmap  altera_mm_interconnect_180          ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_ibrpcbq.vhd"             -work altera_mm_interconnect_180
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_mtvmp4i.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180

vmap  altera_avalon_onchip_memory2_180    ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vcom         "$IP_DIR/../altera_avalon_onchip_memory2_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_avalon_onchip_memory2_180_xymx6za.vhd" -work altera_avalon_onchip_memory2_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"
  vcom         "$IP_DIR/../altera_avalon_onchip_memory2_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_avalon_onchip_memory2_180_xymx6za.vhd" -work altera_avalon_onchip_memory2_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"
  vcom         "$IP_DIR/../altera_avalon_onchip_memory2_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_avalon_onchip_memory2_180_xymx6za.vhd" -work altera_avalon_onchip_memory2_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"
  vcom         "$IP_DIR/../altera_avalon_onchip_memory2_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_avalon_onchip_memory2_180_xymx6za.vhd" -work altera_avalon_onchip_memory2_180

vmap  altera_emif_cal_slave_nf_180        ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vcom      "$IP_DIR/../altera_emif_cal_slave_nf_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_emif_cal_slave_nf_180_efslyyq.vhd"           -work altera_emif_cal_slave_nf_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"
  vcom      "$IP_DIR/../altera_emif_cal_slave_nf_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_emif_cal_slave_nf_180_efslyyq.vhd"           -work altera_emif_cal_slave_nf_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"
  vcom      "$IP_DIR/../altera_emif_cal_slave_nf_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_emif_cal_slave_nf_180_efslyyq.vhd"           -work altera_emif_cal_slave_nf_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"
  vcom      "$IP_DIR/../altera_emif_cal_slave_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_cal_slave_nf_180_efslyyq.vhd"           -work altera_emif_cal_slave_nf_180



vmap  altera_emif_180                     ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vcom      "$IP_DIR/../altera_emif_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_emif_180_dzobyri.vhd"                                     -work altera_emif_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"
  vcom      "$IP_DIR/../altera_emif_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_emif_180_lwknerq.vhd"                                     -work altera_emif_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"
  vcom      "$IP_DIR/../altera_emif_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_emif_180_gt57qoa.vhd"                                     -work altera_emif_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"
  vcom      "$IP_DIR/../altera_emif_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_180_nz3mdxa.vhd"                                     -work altera_emif_180
