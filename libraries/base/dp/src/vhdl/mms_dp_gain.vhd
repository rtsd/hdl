-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author : E. Kooistra, 29 mar 2017
-- Purpose : Set gain for one stream via MM
-- Description:
--   Wrapper of mms_dp_gain_arr with g_nof_streams=1. See mms_dp_gain_arr
--   for details.
-- Usage:
-- . See tb_mms_dp_gain_arr which also tests this mms_dp_gain

library IEEE, common_lib, common_mult_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_dp_gain is
  generic (
    g_technology                    : natural := c_tech_select_default;
    g_complex_data                  : boolean := true;
    g_complex_gain                  : boolean := false;
    g_gain_init_re                  : integer := 1;
    g_gain_init_im                  : integer := 0;
    g_gain_w                        : natural := 16;
    g_in_dat_w                      : natural := 9;
    g_out_dat_w                     : natural := 24;  -- 16 + 9 - 1 = 24 (-1 to skip double sign bit in product)

    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_product    : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_output     : natural := 1;  -- >= 0
    -- . complex multiplier
    g_pipeline_complex_mult_input   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_product : natural := 0;  -- 0 or 1
    g_pipeline_complex_mult_adder   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_output  : natural := 1  -- >= 0
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;

    -- MM access to gain
    reg_gain_re_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_re_miso  : out t_mem_miso;
    reg_gain_im_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_im_miso  : out t_mem_miso;

    reg_gain_re       : out std_logic_vector(g_gain_w - 1 downto 0);
    reg_gain_im       : out std_logic_vector(g_gain_w - 1 downto 0);

    -- ST
    in_sosi           : in  t_dp_sosi;
    out_sosi          : out t_dp_sosi
  );
end mms_dp_gain;

architecture str of mms_dp_gain is
  signal in_sosi_arr     : t_dp_sosi_arr(0 downto 0);
  signal out_sosi_arr    : t_dp_sosi_arr(0 downto 0);
begin
  -- wires
  in_sosi_arr(0) <= in_sosi;
  out_sosi       <= out_sosi_arr(0);

  u_one : entity work.mms_dp_gain_arr
  generic map (
    g_technology                    => g_technology,
    -- functional
    g_nof_streams                   => 1,
    g_complex_data                  => g_complex_data,
    g_complex_gain                  => g_complex_gain,
    g_gain_init_re                  => g_gain_init_re,
    g_gain_init_im                  => g_gain_init_im,
    g_gain_w                        => g_gain_w,
    g_in_dat_w                      => g_in_dat_w,
    g_out_dat_w                     => g_out_dat_w,
    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      => g_pipeline_real_mult_input,
    g_pipeline_real_mult_product    => g_pipeline_real_mult_product,
    g_pipeline_real_mult_output     => g_pipeline_real_mult_output,
    -- . complex multiplier
    g_pipeline_complex_mult_input   => g_pipeline_complex_mult_input,
    g_pipeline_complex_mult_product => g_pipeline_complex_mult_product,
    g_pipeline_complex_mult_adder   => g_pipeline_complex_mult_adder,
    g_pipeline_complex_mult_output  => g_pipeline_complex_mult_output
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM access to gain
    reg_gain_re_mosi  => reg_gain_re_mosi,
    reg_gain_re_miso  => reg_gain_re_miso,
    reg_gain_im_mosi  => reg_gain_im_mosi,
    reg_gain_im_miso  => reg_gain_im_miso,

    reg_gain_re       => reg_gain_re,
    reg_gain_im       => reg_gain_im,
    -- ST
    in_sosi_arr       => in_sosi_arr,
    out_sosi_arr      => out_sosi_arr
  );
end str;
