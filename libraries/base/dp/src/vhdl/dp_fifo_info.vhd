-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Realign the snk_in info with the snk_in data that got delayed
-- Description:
--   The snk_in info consists of the BSN and channel fields at the sop and the
--   empty and err fields at the eop. Dependent on the number of blocks that
--   the data gets delayed these info fields need to be buffered to be able to
--   realign them again with the delayed data.
--   The data gets delayed e.g. due to processing latency some DSP function
--   or because it is passed through a FIFO.
--   The info typically only needs to be passed along with the same delay as
--   the data. To save logic and RAM and to not burden the data function with
--   the handling of the info this dp_fifo_info can realign the original
--   snk_in info with the delayed snk_in data.
--   The dp_fifo_info uses the info_snk_in.sop and info_snk_in.eop to write
--   the info_snk_in info to the FIFO and it uses the data_snk_in.sop and
--   data_snk_in.eop to read the info from the FIFO. Therefore the DSP
--   component must pass on the snk_in.sop and snk_in.eop, to be able to
--   attach the info_snk_in sop info and eop info again at the output. The
--   g_fifo_size must be sufficient to fit the maximum latency in number of
--   blocks (sop's) that the DSP component can introduce.
--
-- Usage:
--                           data_snk_out
--                           data_snk_in
--                               .
--                   |-----|     .         |------|
--   snk_out <------ |     | <------------ |      | <---- src_in
--   snk_in  ---*--> | DP/ | ------------> | dp   | ----> src_out
--              |    | DSP |               | fifo |
--              |    |-----|               | info |
--              |            info_snk_in   |      |
--              \------------------------> |------|
--
--
-- Remark:
-- . See also dp/doc/sketch_dp_fifo_info.pdf
-- . The data is passed on unchanged by dp_fifo_info.
-- . The dp_fifo_info preserves the RL=1 but is does delay the data by one
--   cycle.
-- . If the DSP component does not pass on the sop and eop, then it can be
--   possible to recreate these at the output based on the valid using e.g.
--   dp_block_gen. This assumes that the DSP does pass on the valid, that the
--   block size is known and that the first valid at the output corresponds
--   to a sop.
-- . These are related components that try to pass on sosi info from begin to
--   end, without having to pass it on through each step in the sosi data
--   processing.
--   - dp_paged_sop_eop_reg
--   - dp_fifo_info.vhd
--   - dp_block_gen_valid_arr

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_info is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_use_sync       : boolean := false;
    g_use_bsn        : boolean := false;
    g_use_channel    : boolean := false;
    g_use_empty      : boolean := false;
    g_use_error      : boolean := false;
    g_bsn_w          : natural := c_dp_stream_bsn_w;
    g_empty_w        : natural := c_dp_stream_empty_w;
    g_channel_w      : natural := c_dp_stream_channel_w;
    g_error_w        : natural := c_dp_stream_error_w;
    g_fifo_size      : natural := 4
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- Monitor info FIFO filling
    fifo_wr_ful  : out std_logic;  -- corresponds to the carry bit of usedw when FIFO is full
    fifo_usedw   : out std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    fifo_rd_emp  : out std_logic;
    -- ST sink
    data_snk_out : out t_dp_siso;
    data_snk_in  : in  t_dp_sosi;  -- delayed snk_in data
    info_snk_in  : in  t_dp_sosi;  -- original snk_in info
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_fifo_info;

architecture str of dp_fifo_info is
  constant c_fifo_sop_dat_w   : natural := func_slv_concat_w(g_use_sync, g_use_bsn, g_use_channel, 1, g_bsn_w, g_channel_w);  -- concat sop info via FIFO
  constant c_fifo_eop_dat_w   : natural := func_slv_concat_w(g_use_empty, g_use_error, g_empty_w, g_error_w);  -- concat eop info via FIFO
  constant c_fifo_info_dat_w  : natural := c_fifo_sop_dat_w + c_fifo_eop_dat_w;

  signal dp_pipeline_data_src_in    : t_dp_siso;
  signal dp_pipeline_data_src_out   : t_dp_sosi;

  signal info_src_out               : t_dp_sosi;

  signal wr_sync                    : std_logic_vector(0 downto 0);
  signal rd_sync                    : std_logic_vector(0 downto 0);

  signal dp_latency_adapter_snk_out : t_dp_siso;
  signal dp_latency_adapter_snk_in  : t_dp_sosi;

  signal fifo_sop_wr_dat            : std_logic_vector(c_fifo_sop_dat_w - 1 downto 0);
  signal fifo_sop_wr_req            : std_logic;
  signal fifo_sop_wr_ful            : std_logic;
  signal fifo_sop_usedw             : std_logic_vector(fifo_usedw'range);
  signal fifo_sop_rd_dat            : std_logic_vector(c_fifo_sop_dat_w - 1 downto 0) := (others => '0');
  signal fifo_sop_rd_val            : std_logic;
  signal fifo_sop_rd_req            : std_logic;
  signal fifo_sop_rd_emp            : std_logic;

  signal fifo_eop_wr_dat            : std_logic_vector(c_fifo_eop_dat_w - 1 downto 0);
  signal fifo_eop_wr_req            : std_logic;
  signal fifo_eop_wr_ful            : std_logic;
  signal fifo_eop_usedw             : std_logic_vector(fifo_usedw'range);
  signal fifo_eop_rd_dat            : std_logic_vector(c_fifo_eop_dat_w - 1 downto 0) := (others => '0');
  signal fifo_eop_rd_val            : std_logic;
  signal fifo_eop_rd_req            : std_logic;
  signal fifo_eop_rd_emp            : std_logic;
begin
  no_info : if c_fifo_info_dat_w = 0 generate
    data_snk_out <= src_in;
    src_out      <= data_snk_in;
  end generate;

  gen_info : if c_fifo_info_dat_w > 0 generate
    -- Output monitor FIFO filling
    fifo_wr_ful <= fifo_sop_wr_ful when c_fifo_sop_dat_w > 0 else fifo_eop_wr_ful;
    fifo_usedw  <= fifo_sop_usedw  when c_fifo_sop_dat_w > 0 else fifo_eop_usedw;
    fifo_rd_emp <= fifo_sop_rd_emp when c_fifo_sop_dat_w > 0 else fifo_eop_rd_emp;

    -- Data pipeline register to compensate for the fifo rd_req to rd_val latency of 1
    u_dp_pipeline : entity work.dp_pipeline
    generic map (
      g_pipeline => 1
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => data_snk_out,
      snk_in       => data_snk_in,
      -- ST source
      src_in       => dp_pipeline_data_src_in,
      src_out      => dp_pipeline_data_src_out
    );

    -- Buffer sop info
    gen_info_sop : if c_fifo_sop_dat_w > 0 generate
      fifo_sop_wr_req <= info_snk_in.sop;
      fifo_sop_rd_req <= data_snk_in.sop;

      -- concatenate the sop data to use a single FIFO
      wr_sync(0) <= info_snk_in.sync;
      fifo_sop_wr_dat <= func_slv_concat(g_use_sync, g_use_bsn, g_use_channel,
                                         wr_sync,
                                         info_snk_in.bsn(g_bsn_w - 1 downto 0),
                                         info_snk_in.channel(g_channel_w - 1 downto 0));

      u_common_fifo_sc : entity common_lib.common_fifo_sc
      generic map (
        g_technology  => g_technology,
        g_note_is_ful => false,  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
        g_use_lut     => true,  -- when TRUE then force using LUTs via Altera eab="OFF",
        g_dat_w       => c_fifo_sop_dat_w,
        g_nof_words   => g_fifo_size
      )
      port map (
        rst      => rst,
        clk      => clk,
        wr_dat   => fifo_sop_wr_dat,
        wr_req   => fifo_sop_wr_req,
        wr_ful   => fifo_sop_wr_ful,
        rd_dat   => fifo_sop_rd_dat,
        rd_req   => fifo_sop_rd_req,
        rd_emp   => fifo_sop_rd_emp,
        rd_val   => fifo_sop_rd_val,
        usedw    => fifo_sop_usedw
      );

      -- Extract the sop data from the FIFO output SLV
      info_src_out.sync <= rd_sync(0);
      rd_sync                                      <= func_slv_extract(g_use_sync, g_use_bsn, g_use_channel, 1, g_bsn_w, g_channel_w, fifo_sop_rd_dat, 0);
      info_src_out.bsn(        g_bsn_w - 1 downto 0) <= func_slv_extract(g_use_sync, g_use_bsn, g_use_channel, 1, g_bsn_w, g_channel_w, fifo_sop_rd_dat, 1);
      info_src_out.channel(g_channel_w - 1 downto 0) <= func_slv_extract(g_use_sync, g_use_bsn, g_use_channel, 1, g_bsn_w, g_channel_w, fifo_sop_rd_dat, 2);
    end generate;

    -- Buffer eop info
    gen_info_eop : if c_fifo_eop_dat_w > 0 generate
      fifo_eop_wr_req <= info_snk_in.eop;
      fifo_eop_rd_req <= data_snk_in.eop;

      -- concatenate the eop data to use a single FIFO
      fifo_eop_wr_dat <= func_slv_concat(g_use_empty, g_use_error,
                                         info_snk_in.empty(g_empty_w - 1 downto 0),
                                         info_snk_in.err(g_error_w - 1 downto 0));

      u_common_fifo_sc : entity common_lib.common_fifo_sc
      generic map (
        g_technology  => g_technology,
        g_note_is_ful => false,  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
        g_use_lut     => true,  -- when TRUE then force using LUTs via Altera eab="OFF",
        g_dat_w       => c_fifo_eop_dat_w,
        g_nof_words   => g_fifo_size
      )
      port map (
        rst      => rst,
        clk      => clk,
        wr_dat   => fifo_eop_wr_dat,
        wr_req   => fifo_eop_wr_req,
        wr_ful   => fifo_eop_wr_ful,
        rd_dat   => fifo_eop_rd_dat,
        rd_req   => fifo_eop_rd_req,
        rd_emp   => fifo_eop_rd_emp,
        rd_val   => fifo_eop_rd_val,
        usedw    => fifo_eop_usedw
      );

      -- Extract the eop data from the FIFO output SLV
      info_src_out.empty(g_empty_w - 1 downto 0) <= func_slv_extract(g_use_empty, g_use_error, g_empty_w, g_error_w, fifo_eop_rd_dat, 0);
      info_src_out.err(g_error_w - 1 downto 0)   <= func_slv_extract(g_use_empty, g_use_error, g_empty_w, g_error_w, fifo_eop_rd_dat, 1);
    end generate;

    -- Realign the info with the data
    dp_pipeline_data_src_in <= src_in;
    src_out                 <= func_dp_stream_combine_info_and_data(info_src_out, dp_pipeline_data_src_out);
  end generate;
end str;
