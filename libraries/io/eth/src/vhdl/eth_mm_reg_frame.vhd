-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use work.eth_pkg.all;

entity eth_mm_reg_frame is
  port (
    -- Clocks and reset
    rst             : in  std_logic;  -- reset synchronous with clk
    clk             : in  std_logic;  -- packet stream clock

    -- Inputs need for the frame register
    hdr_fields      : in  t_network_total_header;  -- Header info
    hdr_status      : in  t_eth_hdr_status;  -- Header info
    erc_word        : in  std_logic_vector(c_eth_data_w - 1 downto 0);  -- Extracted CRC word (enumerate: 0=OK, >0 AND odd = Error)
    reg_config      : in  t_eth_mm_reg_config;  -- Config setting

    -- Frame register
    reg_frame       : out t_eth_mm_reg_frame
  );
end eth_mm_reg_frame;

architecture str of eth_mm_reg_frame is
  signal nxt_reg_frame : t_eth_mm_reg_frame;
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      reg_frame <= c_eth_mm_reg_frame_rst;
    elsif rising_edge(clk) then
      reg_frame <= nxt_reg_frame;
    end if;
  end process;

  nxt_reg_frame.is_dhcp           <= hdr_status.is_dhcp;
  nxt_reg_frame.is_udp_ctrl_port  <= '1' when hdr_status.is_udp = '1' and unsigned(hdr_status.udp_port) = unsigned(reg_config.udp_port) else '0';
  nxt_reg_frame.is_udp            <= hdr_status.is_udp;
  nxt_reg_frame.is_icmp           <= hdr_status.is_icmp;
  nxt_reg_frame.ip_address_match  <= '1' when unsigned(hdr_fields.ip.dst_ip_addr) = unsigned(reg_config.ip_address) else '0';
  nxt_reg_frame.ip_checksum_is_ok <= hdr_status.ip_checksum_is_ok;
  nxt_reg_frame.is_ip             <= hdr_status.is_ip;
  nxt_reg_frame.is_arp            <= hdr_status.is_arp;
  nxt_reg_frame.mac_address_match <= '1' when unsigned(hdr_fields.eth.dst_mac) = unsigned(reg_config.mac_address) else '0';
  nxt_reg_frame.eth_mac_error     <= erc_word(c_eth_error_w - 1 downto 0);
end str;
