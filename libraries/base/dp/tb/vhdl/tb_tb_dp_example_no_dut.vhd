-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_example_no_dut
-- Description:
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_dp_example_no_dut is
end tb_tb_dp_example_no_dut;

architecture tb of tb_tb_dp_example_no_dut is
  constant c_nof_repeat            : natural := 10;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;   -- always active, random or pulse flow control
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  -- -- specific
  -- g_in_dat_w               : NATURAL := 32;
  -- g_in_nof_words           : NATURAL := 1;
  -- g_nof_repeat             : NATURAL := 5;
  -- g_pkt_len                : NATURAL := 16;  -- must be a multiple of g_in_nof_words
  -- g_pkt_gap                : NATURAL := 4

  u_act_act      : entity work.tb_dp_example_no_dut generic map ( e_active, e_active, 16, 1, c_nof_repeat, 16, 4);
  u_rnd_act      : entity work.tb_dp_example_no_dut generic map ( e_random, e_active, 16, 1, c_nof_repeat, 16, 4);
  u_rnd_rnd      : entity work.tb_dp_example_no_dut generic map ( e_random, e_random, 16, 1, c_nof_repeat, 16, 4);
  u_pls_pls      : entity work.tb_dp_example_no_dut generic map ( e_pulse,  e_pulse,  16, 1, c_nof_repeat, 16, 4);
end tb;
