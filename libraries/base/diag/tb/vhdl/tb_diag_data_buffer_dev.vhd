-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the bf_unit.
--           To be used in conjunction with python testscript: ../python/tc_bf_unit.py
--
-- The testbench can be used in two modes: auto-mode and non-auto-mode.

--
-- Usage in auto-mode
--   > Run python script in separate terminal: "python bf_unit.py "
--
-- Usage in non-auto-mode
--   > as 5
--   > run 500 ns
--   > run python script in separate terminal: "python tc_bf_unit.py --unb 0 --fn 0 --sim"
--   > run 20 us or run -all
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute the WAVE window.

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.diag_pkg.all;

entity tb_diag_data_buffer_dev is
  generic(
    g_nof_streams : positive :=  2;  -- = 16
    g_nof_data    : positive := 128;
    g_data_w      : positive :=  16  -- = 16
  );
end tb_diag_data_buffer_dev;

architecture tb of tb_diag_data_buffer_dev is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 20 ns;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_sclk_period        : time := 1250 ps;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  signal SCLK                   : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_mosi : t_mem_mosi;
  signal ram_diag_data_buf_miso : t_mem_miso;

  signal reg_diag_data_buf_mosi : t_mem_mosi;
  signal reg_diag_data_buf_miso : t_mem_miso;

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := g_nof_data;
  constant c_db_block_len           : natural  := g_nof_data;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := g_nof_streams;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_data_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "UNUSED";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, g_nof_streams, 1);

  constant c_block_size             : natural := c_bg_block_len;
  constant c_bg_gapsize             : natural := 0;
  constant c_bg_nof_blocks_per_sync : natural := 8;
  constant c_bg_mem_high_addr       : natural := g_nof_data - 1;

  constant c_bg_ctrl                : t_diag_block_gen := ( '0',  -- enable: On by default in simulation; MM enable required on hardware.
                                                            '0',  -- enable_sync
                                                TO_UVEC(            c_block_size, c_diag_bg_samples_per_packet_w),
                                                TO_UVEC(c_bg_nof_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                TO_UVEC(            c_bg_gapsize, c_diag_bg_gapsize_w),
                                                TO_UVEC(                       0, c_diag_bg_mem_low_adrs_w),
                                                TO_UVEC(      c_bg_mem_high_addr, c_diag_bg_mem_high_adrs_w),
                                                TO_UVEC(                       0, c_diag_bg_bsn_init_w));

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := g_nof_streams;
  constant c_db_data_w              : positive := c_diag_db_max_data_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := true;
  constant c_db_data_type_re        : t_diag_data_type_enum := e_real;
  constant c_db_data_type_im        : t_diag_data_type_enum := e_imag;

  signal bg_siso_arr                : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);

  signal s_init                     : std_logic;
  signal nof_valids                 : natural;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  SCLK   <= not SCLK after c_sclk_period / 2;
  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- Stimuli: Block gen data send to memory
  ----------------------------------------------------------------------------
  p_mm_block_gen_stimuli : process
  begin
    s_init <= '0';
    reg_diag_bg_mosi <= c_mem_mosi_rst;
    ram_diag_bg_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 10);
    for I in 0 to g_nof_data - 1 loop
      proc_mem_mm_bus_wr( I, I, mm_clk, ram_diag_bg_mosi);
    end loop;
    proc_mem_mm_bus_wr( 0, 1, mm_clk, reg_diag_bg_mosi);
    s_init <= '1';
    wait;
  end process;

  p_mm_data_buffer : process
  begin
    reg_diag_data_buf_mosi <= c_mem_mosi_rst;
    ram_diag_data_buf_mosi <= c_mem_mosi_rst;
    wait until s_init = '1';

    -- Wait until the buffer is full.
    proc_common_wait_some_cycles(dp_clk, g_nof_data + 30);

    -- Read out all streams of the databuffer
    for I in 0 to g_nof_streams - 1 loop
      for J in 0 to g_nof_data - 1 loop
        proc_mem_mm_bus_rd(I * g_nof_data + J, mm_clk, ram_diag_data_buf_mosi);
      end loop;
    end loop;

    --Set delay for all streams.
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_wr(I * c_diag_db_reg_nof_dat + 3, 4, mm_clk, reg_diag_data_buf_mosi);
    end loop;

    proc_common_wait_some_cycles(mm_clk, 10);

    -- Arm all databuffers
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_wr(I * c_diag_db_reg_nof_dat + 2, 1, mm_clk, reg_diag_data_buf_mosi);
    end loop;

    wait until bg_sosi_arr(0).sync = '1';
    proc_common_wait_some_cycles(dp_clk, g_nof_data + 30);

    -- Read out all streams of the databuffer
    for I in 0 to g_nof_streams - 1 loop
      for J in 0 to g_nof_data - 1 loop
        proc_mem_mm_bus_rd(I * g_nof_data + J, mm_clk, ram_diag_data_buf_mosi);
      end loop;
    end loop;

    nof_valids <= 1;
--    WHILE nof_valids /= 0 LOOP
      proc_mem_mm_bus_rd( 2, mm_clk, reg_diag_data_buf_mosi);
      nof_valids <= TO_UINT(reg_diag_data_buf_miso.rddata(c_word_w - 1 downto 0));
--    END LOOP;
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Arm the databuffer
    proc_mem_mm_bus_wr( 3, 1, mm_clk, reg_diag_data_buf_mosi);

    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity work.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_diag_block_gen_rst => c_bg_ctrl,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.mms_diag_data_buffer_dev
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type_re,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
     -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,
    -- ST interface
    in_sync           => bg_sosi_arr(0).sync,
    in_sosi_arr       => bg_sosi_arr
  );
end tb;
