library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library pft2_lib;
use pft2_lib.pft_pkg.all;

entity pft_top is
  generic (
    g_fft_size_w   : natural := 10;
    g_in_dat_w     : natural := 18;
    g_out_dat_w    : natural := 18;
    g_mode         : PFT_MODE_TYPE := PFT_MODE_BITREV
  );
  port (
    in_re          : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_im          : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val         : in  std_logic;
    in_sync        : in  std_logic;
    switch_en      : in  std_logic := '1';
    out_re         : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_im         : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val        : out std_logic;
    out_sync       : out std_logic;
    clk            : in  std_logic;
    rst            : in  std_logic
  );
end pft_top;
