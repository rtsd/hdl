#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

set IP_DIR   "$env(HDL_BUILD_DIR)/unb2/qsys-generate/sim"
set IP_TBDIR "$env(HDL_BUILD_DIR)/unb2/qsys-generate/sim"

#vlib ./work/         ;# Assume library work already exists

vmap ip_arria10_mac_10g_alt_em10g32_150 ./work/

  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/alt_em10g32.v"                                                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/alt_em10g32unit.v"                                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_clk_rst.v"                                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_clock_crosser.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_crc32.v"                                                               -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_crc32_gf_mult32_kc.v"                                                  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_creg_map.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_creg_top.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_frm_decoder.v"                                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_gmii_mii_layer.v"                                                -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_pipeline_base.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_reset_synchronizer.v"                                                  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rr_clock_crosser.v"                                                    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rst_cnt.v"                                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_fctl_filter_crcpad_rem.v"                                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_fctl_overflow.v"                                                    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_fctl_preamble.v"                                                    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_frm_control.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_pfc_flow_control.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_pfc_pause_conversion.v"                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_pkt_backpressure_control.v"                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_gmii16b.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_gmii16b_top.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_gmii_mii.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_layer.v"                                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_xgmii.v"                                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_status_aligner.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_top.v"                                                              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_stat_mem.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_stat_reg.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_data_frm_gen.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_srcaddr_inserter.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_err_aligner.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_flow_control.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_frm_arbiter.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_frm_muxer.v"                                                        -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_pause_beat_conversion.v"                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_pause_frm_gen.v"                                                    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_pause_req.v"                                                        -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_pfc_frm_gen.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rr_buffer.v"                                                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_gmii16b.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_gmii16b_top.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_layer.v"                                                         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_xgmii_layer.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_sc_fifo.v"                                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_top.v"                                                              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_gmii_decoder.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_gmii_decoder_dfa.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_gmii_encoder.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_gmii_encoder_dfa.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_gmii_mii_decoder_if.v"                                              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_gmii_mii_encoder_if.v"                                              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_mm_adapter/altera_eth_avalon_mm_adapter.v"                  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/altera_eth_avalon_st_adapter.v"                  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/avalon_st_adapter_avalon_st_rx.v"                -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/avalon_st_adapter_avalon_st_tx.v"                -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/avalon_st_adapter.v"                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/alt_em10g32_vldpkt_rddly.v"                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/sideband_adapter_rx.v"                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/sideband_adapter_tx.v"                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/sideband_adapter.v"                              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/altera_eth_sideband_crosser.v"                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_avalon_st_adapter/altera_eth_sideband_crosser_sync.v"              -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_width_adaptor/alt_em10g_32_64_xgmii_conversion.v"            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_width_adaptor/alt_em10g_32_to_64_xgmii_conversion.v"         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_width_adaptor/alt_em10g_64_to_32_xgmii_conversion.v"         -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_width_adaptor/alt_em10g_dcfifo_32_to_64_xgmii_conversion.v"  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_width_adaptor/alt_em10g_dcfifo_64_to_32_xgmii_conversion.v"  -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_data_format_adapter/alt_em10g32_xgmii_32_to_64_adapter.v"    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_data_format_adapter/alt_em10g32_xgmii_64_to_32_adapter.v"    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/adapters/altera_eth_xgmii_data_format_adapter/alt_em10g32_xgmii_data_format_adapter.v" -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_altsyncram_bundle.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_altsyncram.v"                                                          -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_dc_fifo_lat_calc.v"                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_dc_fifo_hecc.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_dc_fifo_secc.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_sc_fifo.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_sc_fifo_hecc.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avalon_sc_fifo_secc.v"                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_ecc_dec_18_12.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_ecc_dec_39_32.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_ecc_enc_12_18.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_ecc_enc_32_39.v"                                                       -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_rs_xgmii_layer_ultra.v"                                             -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_rs_xgmii_ultra.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_avst_to_gmii_if.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_gmii_to_avst_if.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_gmii_tsu.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_lpm_mult.v"                                                            -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_ptp_aligner.v"                                                      -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_ptp_detector.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_rx_ptp_top.v"                                                          -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_gmii_crc_inserter.v"                                                -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_gmii_ptp_inserter.v"                                                -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_ptp_processor.v"                                                    -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_ptp_top.v"                                                          -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_xgmii_crc_inserter.v"                                               -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_tx_xgmii_ptp_inserter.v"                                               -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_xgmii_tsu.v"                                                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_crc328generator.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_crc32ctl8.v"                                                           -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_crc32galois8.v"                                                        -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/mentor/rtl/alt_em10g32_gmii_crc_inserter.v"                                                   -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/alt_em10g32_avalon_dc_fifo.v"                                                                 -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/alt_em10g32_dcfifo_synchronizer_bundle.v"                                                     -work ip_arria10_mac_10g_alt_em10g32_150
  vlog "$IP_DIR/../alt_em10g32_150/sim/alt_em10g32_std_synchronizer.v"                                                               -work ip_arria10_mac_10g_alt_em10g32_150
  vcom "$IP_DIR/ip_arria10_mac_10g.vhd"                                                                                                                                      
