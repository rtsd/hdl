-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 2
-- > run -all --> OK

entity tb_tb2_dp_mux is
end tb_tb2_dp_mux;

architecture tb of tb_tb2_dp_mux is
  constant c_nof_repeat : natural := 10;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Select next input when eop has occurred or when this input has had an active ready (so it has had a fair chance)
  --                                                         in_en, src_in.ready, g_mux_mode, g_mux_use_fifo, g_mux_fifo_fill, g_combined_demux, nof repeat
  u0_act_act         : entity work.tb2_dp_mux generic map (e_active, e_active,    0,          false,          0,               false,            c_nof_repeat);
  u0_act_rnd         : entity work.tb2_dp_mux generic map (e_active, e_random,    0,          false,          0,               false,            c_nof_repeat);
  u0_act_pls         : entity work.tb2_dp_mux generic map (e_active, e_pulse,     0,          false,          0,               false,            c_nof_repeat);

  u0_rnd_act         : entity work.tb2_dp_mux generic map (e_random, e_active,    0,          false,          0,               false,            c_nof_repeat);
  u0_rnd_rnd         : entity work.tb2_dp_mux generic map (e_random, e_random,    0,          false,          0,               false,            c_nof_repeat);
  u0_rnd_rnd_comb    : entity work.tb2_dp_mux generic map (e_random, e_random,    0,          false,          0,               true,             c_nof_repeat);
  u0_rnd_rnd_fifo    : entity work.tb2_dp_mux generic map (e_random, e_random,    0,          true,           0,               false,            c_nof_repeat);
  u0_rnd_rnd_fifo_10 : entity work.tb2_dp_mux generic map (e_random, e_random,    0,          true,          10,               false,            c_nof_repeat);
  u0_rnd_pls         : entity work.tb2_dp_mux generic map (e_random, e_pulse,     0,          false,          0,               false,            c_nof_repeat);

  u0_pls_act         : entity work.tb2_dp_mux generic map (e_pulse,  e_active,    0,          false,          0,               false,            c_nof_repeat);
  u0_pls_rnd         : entity work.tb2_dp_mux generic map (e_pulse,  e_random,    0,          false,          0,               false,            c_nof_repeat);
  u0_pls_pls         : entity work.tb2_dp_mux generic map (e_pulse,  e_pulse,     0,          false,          0,               false,            c_nof_repeat);

  -- Select next input when eop has occurred
  --                                                         in_en, src_in.ready, g_mux_mode, nof repeat
  u1_act_act         : entity work.tb2_dp_mux generic map (e_active, e_active,    1,          false,          0,               false,            c_nof_repeat);
  u1_act_rnd         : entity work.tb2_dp_mux generic map (e_active, e_random,    1,          false,          0,               false,            c_nof_repeat);
  u1_act_pls         : entity work.tb2_dp_mux generic map (e_active, e_pulse,     1,          false,          0,               false,            c_nof_repeat);

  u1_rnd_act         : entity work.tb2_dp_mux generic map (e_random, e_active,    1,          false,          0,               false,            c_nof_repeat);
  u1_rnd_rnd         : entity work.tb2_dp_mux generic map (e_random, e_random,    1,          false,          0,               false,            c_nof_repeat);
  u1_rnd_rnd_comb    : entity work.tb2_dp_mux generic map (e_random, e_random,    1,          false,          0,               true,             c_nof_repeat);
  u1_rnd_rnd_fifo    : entity work.tb2_dp_mux generic map (e_random, e_random,    1,          true,           0,               false,            c_nof_repeat);
  u1_rnd_rnd_fifo_10 : entity work.tb2_dp_mux generic map (e_random, e_random,    1,          true,          10,               false,            c_nof_repeat);
  u1_rnd_pls         : entity work.tb2_dp_mux generic map (e_random, e_pulse,     1,          false,          0,               false,            c_nof_repeat);

  u1_pls_act         : entity work.tb2_dp_mux generic map (e_pulse,  e_active,    1,          false,          0,               false,            c_nof_repeat);
  u1_pls_rnd         : entity work.tb2_dp_mux generic map (e_pulse,  e_random,    1,          false,          0,               false,            c_nof_repeat);
  u1_pls_pls         : entity work.tb2_dp_mux generic map (e_pulse,  e_pulse,     1,          false,          0,               false,            c_nof_repeat);
end tb;
