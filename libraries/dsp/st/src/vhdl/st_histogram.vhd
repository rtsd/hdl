------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Count incoming data values and keep the counts in RAM as a histogram
-- Description:
--  . See st_histogram.txt for the original design description.
--  . The histogram of all input data per sync interval (g_nof_data_per_sync)
--    is kept as count values in g_nof_bins RAM addresses.
--    g_nof_data_per_sync determines the required width of the data in RAM.
--  . All valid data of the input contributes to the histogram, no data is
--    lost. This applies to any input type including DC.
--  . Internally 2 RAM pages are used and are swapped on a sync. One
--    (inactive) page contains the histogram of the last sync interval
--    while the new data is written to the other (active) page. The contents
--    of the inactive RAM are cleared automatically just before the next sync
--    interval. This way no data is lost and all valid g_nof_data_per_sync
--    samples contribute to the histogram.
--  . The block schematic below shows the data flow from snk_in to ram_mosi:
--    . snk_in.data is reduced by combining up to 3 consecutive identical
--      samples into 1 and carrying the count (1..3) in the channel field.
--      . This is to prevent simultaneous read/writes to the RAM from/on
--        the same address. The read,increment,write sequence takes 3 cycles,
--        hence combining up to 3 consecutive samples (=addresses) into 1
--        rules out this issue.
--    . snk_in_reg.data is interpreted as address (bin) to read from RAM by
--      bin_reader.
--      . a RAM pointer 0 or 1 is kept as MS part of the address.
--        . snk_in_reg.sync determines the RAM pointer 0 or 1.
--    . The data read from that adress, the bin count, is incremented with
--      the count carried in the channel field (1..3) and written back to
--      the RAM by bin_writer.
--    . Upon request (ram_miso), the bin counts (=the histogram) are output
--      on ram_mosi.
--                                                  bin_reader_miso
--             ______                 __________    |   _________
--            |      |               |          |   |  |         |
-- --snk_in-->|reduce|--snk_in_reg-->|bin_reader|<--+--|         |
--            |______|               |__________|      |         |
--                                        |            |         |
--                                        |            |         |
--                           bin_reader_to_writer_mosi |RAM(1..0)|--ram_mosi-->
--                                        |            |         |
--                                    ____v_____       |         |
--                                   |          |      |         |
--                                   |bin_writer|--+-->|         |
--                                   |__________|  |   |_________|
--                                                 |
--                                           bin_writer_mosi
-- Usage:
-- . The ram_mosi input applies to the RAM page that is inactive (not
--   being written to from data path) *at that time*. The user should take care to
--   time these controls such that the active RAM page does not swap before these
--   operation (ram_mosi readout) has finished, otherwise the read histogram will
--   contain data from both the current and the previous sync periods.
-- Remarks:
-- . The RAM block we use basically needs 3 ports:
--   1 - read port in dp_clk domain to read current bin value
--   2 - write port in dp_clk domain to write back incremented bin value
--   3 - read port in mm_clk domain to read the inactive page
-- . common_ram_r_w
--   . Why common_ram_r_w was selected: it uses a single clock
--     . We need to read and write back bins in the dp_clk clock domain, so our RAM
--       block needs to have 2 separate address inputs - but in the same clock domain.
--       . The other, dual clock, RAM blocks (e.g. common_ram_cr_cw) are based on
--         common_ram_crw_crw and use 2 address inputs (adr_a,adr_b), each in
--         its own (clk_a,clk_b) clock domain, which is not what we need here.
--   . Downside of common_ram_r_w: it uses a single clock
--     . This st_histogram.vhd operates in dp_clk domain only, so we need to
--       provide MM access to the user, in the mm_clk domain, elsewhere. This
--       has been done in mmp_st_histogram.vhd.
--   . Possibly common_paged_ram_r_w could be used instead of 2 common_ram_r_w
--     instances. However that modification would not add an MM clk domain for
--     readout.

library IEEE, common_lib, mm_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity st_histogram is
  generic (
    g_data_w                 : natural := 8;
    g_nof_bins               : natural := 256;  -- <= 2^g_data_w (having more bins than possible values is not useful)
    g_nof_data_per_sync      : natural := 1024;
    g_nof_data_per_sync_diff : natural := 0;  -- Allow +- g_nof_data_per_sync_diff samples per sync interval
    g_data_type              : string  := "unsigned"  -- unsigned or signed
  );
  port (
    dp_clk       : in  std_logic;
    dp_rst       : in  std_logic;

    snk_in       : in  t_dp_sosi;  -- Active RAM page swaps on snk_in.sync

    ram_mosi     : in  t_mem_mosi;  -- MM access to the inactive RAM page
    ram_miso     : out t_mem_miso
  );
end st_histogram;

architecture rtl of st_histogram is
  -------------------------------------------------------------------------------
  -- Main Constants
  -------------------------------------------------------------------------------
  constant c_ram_adr_w : natural := ceil_log2(g_nof_bins);
  constant c_adr_low   : natural := g_data_w - c_ram_adr_w;
  constant c_ram_dat_w : natural := ceil_log2(g_nof_data_per_sync + g_nof_data_per_sync_diff + 1);

  -------------------------------------------------------------------------------
  -- snk_in.data help signal
  -------------------------------------------------------------------------------
  signal snk_in_data : std_logic_vector(c_ram_adr_w - 1 downto 0);

  -------------------------------------------------------------------------------
  -- snk_in_reg_arr
  -------------------------------------------------------------------------------
  constant c_ram_rd_wr_latency : natural := 3;  -- RAM read,incr,write cycle latency
  constant c_shiftreg_depth    : natural := c_ram_rd_wr_latency + 1;

  signal snk_in_reg_arr     : t_dp_sosi_arr(c_shiftreg_depth - 1 downto 0);
  signal nxt_snk_in_reg_arr : t_dp_sosi_arr(c_shiftreg_depth - 1 downto 0);
  signal snk_in_reg         : t_dp_sosi;

  -------------------------------------------------------------------------------
  -- ram_pointer
  -------------------------------------------------------------------------------
  signal ram_pointer            : std_logic;
  signal prv_ram_pointer        : std_logic;

  -------------------------------------------------------------------------------
  -- bin_reader
  -------------------------------------------------------------------------------
  signal bin_reader_mosi     : t_mem_mosi;
  signal bin_reader_miso     : t_mem_miso;
  signal prv_bin_reader_mosi : t_mem_mosi;

  -------------------------------------------------------------------------------
  -- bin_writer
  -------------------------------------------------------------------------------
  signal bin_reader_to_writer_mosi      : t_mem_mosi;
  signal bin_reader_to_writer_count     : natural;
  signal nxt_bin_reader_to_writer_count : natural;

  signal nxt_bin_writer_mosi            : t_mem_mosi;
  signal bin_writer_mosi                : t_mem_mosi;

  -------------------------------------------------------------------------------
  -- 2x RAM (common_ram_r_w) instances
  -------------------------------------------------------------------------------
  constant c_nof_ram_pages     : natural := 2;
  constant c_ram               : t_c_mem := (latency  => 1,
                                             adr_w    => c_ram_adr_w,
                                             dat_w    => c_ram_dat_w,
                                             nof_dat  => g_nof_bins,
                                             init_sl  => '0');

  signal bin_writer_ram_pointer     : std_logic;
  signal bin_reader_ram_pointer     : std_logic;
  signal prv_bin_reader_ram_pointer : std_logic;

  signal common_ram_r_w_wr_mosi_arr : t_mem_mosi_arr(1 downto 0);
  signal common_ram_r_w_rd_mosi_arr : t_mem_mosi_arr(1 downto 0);
  signal common_ram_r_w_rd_miso_arr : t_mem_miso_arr(1 downto 0);

  signal histogram_wr_mosi          : t_mem_mosi;
  signal histogram_rd_mosi          : t_mem_mosi;
  signal histogram_rd_miso          : t_mem_miso;

  -------------------------------------------------------------------------------
  -- ram_clear
  -------------------------------------------------------------------------------
  constant c_data_cnt_w        : natural := ceil_log2(g_nof_data_per_sync + g_nof_data_per_sync_diff);
  constant c_clear_sample_cnt  : natural := g_nof_data_per_sync - g_nof_data_per_sync_diff;  -- Clear sooner (-g_nof_data_per_sync_diff) rather than later

  signal data_cnt              : std_logic_vector(c_data_cnt_w - 1 downto 0);
  signal nxt_data_cnt          : std_logic_vector(c_data_cnt_w - 1 downto 0);

  signal ram_clear             : std_logic;
  signal ram_clearing          : std_logic;

  signal ram_clear_address     : std_logic_vector(c_ram_adr_w - 1 downto 0);
  signal nxt_ram_clear_address : std_logic_vector(c_ram_adr_w - 1 downto 0);

  signal nxt_ram_clearing      : std_logic;
begin
  -------------------------------------------------------------------------------
  -- Select range from snk_in.data and interpret as (un)signed
  -------------------------------------------------------------------------------
  gen_unsigned: if g_data_type /= "signed" generate
    snk_in_data <= snk_in.data(g_data_w - 1 downto c_adr_low);
  end generate;

  -- Use offset_binary() from common_pkg.vhd, to swap the lower half and
  -- upper half of the bins in case the input data is signed. The signed
  -- input values can be two-complement or offset binary, dependent on how
  -- they were sampled by the ADC or generated by an waveform generator.
  -- The difference is in the details. For example with 8 bit data:
  --
  --                          bin:    0    127    128      255
  --  signed two-complement value: -128     -1      0     +127
  --  signed offset binary value:  -127.5   -0.5   +0.5   +127.5
  --  unsigned value:                 0    127    128      255
  gen_signed: if g_data_type = "signed" generate
    snk_in_data <= offset_binary(snk_in.data(g_data_w - 1 downto c_adr_low));
  end generate;

  -------------------------------------------------------------------------------
  -- Slightly reduce the incoming data to prevent simultineous read/write
  -- . Take out every 2nd and 3rd duplicate data value (set valid='0')
  -- . Put the number of duplicates in the channel field to be applied downstream
  -- . With a RAM read->write latency of 3 cycles (c_ram_rd_wr_latency), we need
  --   a shift register of 4 words (0,1,2,3) deep to prevent simultaneous
  --   read/writes on the RAM.
  --   . Element 3 is only an output register, elements 0,1,2 are compared for
  --     matching data.
  -- . A sequence of duplicate data could cross a sync period:
  --   . Don't count across sync periods to ensure exactly correct bin values
  --     in each sync interval.
  --   . We can still get a read on cycle n and a write on cycle n+2 on the
  --     same address, but that does not matter as the read,write will be on
  --     different RAM blocks (1 RAM block per sync period).
  -- . Input : snk_in, snk_in_data
  -- . Output: snk_in_reg
  -------------------------------------------------------------------------------
  p_nxt_snk_in_reg_arr: process(snk_in, snk_in_data, snk_in_reg_arr) is
  begin
    for i in 0 to c_shiftreg_depth - 1 loop
      nxt_snk_in_reg_arr(i) <= c_dp_sosi_rst;
    end loop;

    if snk_in.valid = '1' then
      -- The base function is a shift register
      nxt_snk_in_reg_arr(0) <= snk_in;
      nxt_snk_in_reg_arr(0).data(c_ram_adr_w - 1 downto 0) <= snk_in_data;  -- Use the ranged data
      nxt_snk_in_reg_arr(1) <= snk_in_reg_arr(0);
      nxt_snk_in_reg_arr(2) <= snk_in_reg_arr(1);
      nxt_snk_in_reg_arr(3) <= snk_in_reg_arr(2);

      -- Assign a count of 1 to valid data
      nxt_snk_in_reg_arr(0).channel <= TO_DP_CHANNEL(1);

      -- Overwrite channel field (=count) when duplicate data is found
      -- . Check all possible matches apart from 0==1: simply wait until 0==1 shifts to 1==2.
      if func_dp_data_match(snk_in_reg_arr(0), snk_in_reg_arr(1), snk_in_reg_arr(2), g_data_w) and (snk_in_reg_arr(0).sync = '0' and snk_in_reg_arr(1).sync = '0') then
          nxt_snk_in_reg_arr(1).valid   <= '0';
          nxt_snk_in_reg_arr(1).channel <= TO_DP_CHANNEL(0);
          nxt_snk_in_reg_arr(2).valid   <= '0';
          nxt_snk_in_reg_arr(2).channel <= TO_DP_CHANNEL(0);
          nxt_snk_in_reg_arr(3).channel <= TO_DP_CHANNEL(3);  -- 0,1,2 match: put count=3 here
      elsif func_dp_data_match(snk_in_reg_arr(1), snk_in_reg_arr(2), g_data_w) and snk_in_reg_arr(1).sync = '0' then
          nxt_snk_in_reg_arr(2).valid   <= '0';
          nxt_snk_in_reg_arr(2).channel <= TO_DP_CHANNEL(0);
          nxt_snk_in_reg_arr(3).channel <= TO_DP_CHANNEL(2);  -- 1,2 match: put count=2 here
      elsif func_dp_data_match(snk_in_reg_arr(0), snk_in_reg_arr(2), g_data_w) and (snk_in_reg_arr(0).sync = '0' and snk_in_reg_arr(1).sync = '0') then
          nxt_snk_in_reg_arr(1).valid   <= '0';
          nxt_snk_in_reg_arr(1).channel <= TO_DP_CHANNEL(0);
          nxt_snk_in_reg_arr(3).channel <= TO_DP_CHANNEL(2);  -- 0,2 match: put count=2 here
      end if;
    end if;
  end process;

  snk_in_reg <= snk_in_reg_arr(3);

  -- Registers
  p_snk_in_reg_arr: process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      snk_in_reg_arr <= (others => c_dp_sosi_rst);
    elsif rising_edge(dp_clk) then
      snk_in_reg_arr <= nxt_snk_in_reg_arr;
    end if;
  end process;

  -------------------------------------------------------------------------------
  -- ram_pointer: Keep track of what RAM to target
  -- . Target either RAM 0 or 1 per sync period
  -- . RD/WR sides of RAM have shifted sync periods due to rd>wr latency
  --   . e.g. a new sync period is read while an old sync period is written
  --   . Solution: treat the RAM pointer as MS address bit in separate RD/WR buses
  --   . ram_pointer is synchronous to snk_in_reg.sync
  -------------------------------------------------------------------------------
  p_ram_pointer : process(dp_rst, dp_clk) is
  begin
    if dp_rst = '1' then
      prv_ram_pointer    <= '1';
    elsif rising_edge(dp_clk) then
      prv_ram_pointer    <= ram_pointer;
    end if;
  end process;

  -- Toggle the RAM pointer on the sync
  ram_pointer <= not prv_ram_pointer when snk_in_reg.sync = '1' else prv_ram_pointer;

  -------------------------------------------------------------------------------
  -- bin_reader : reads bin from RAM, sends bin to bin_writer.
  -- . Input  : snk_in_reg      (input data stream)
  --            bin_reader_miso (reply to RAM read request: rddata = bin count)
  --            ram_pointer (to put in MOSI buses as MS address bit)
  -- . Output : bin_reader_mosi (RAM read request, address = bin)
  --            bin_reader_to_writer_mosi (address = bin, wrdata = bin count)
  -------------------------------------------------------------------------------
  -- Fetch the bin from RAM
  bin_reader_mosi.wrdata  <= (others => '0');
  bin_reader_mosi.wr      <= '0';
  bin_reader_mosi.rd      <= snk_in_reg.valid;
  bin_reader_mosi.address <= RESIZE_UVEC(ram_pointer & snk_in_reg.data(c_ram_adr_w - 1 downto 0), c_word_w);

  p_prv_bin_reader_mosi : process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      prv_bin_reader_mosi        <= c_mem_mosi_rst;
      bin_reader_to_writer_count <= 0;
    elsif rising_edge(dp_clk) then
      prv_bin_reader_mosi        <= bin_reader_mosi;
      bin_reader_to_writer_count <= nxt_bin_reader_to_writer_count;
    end if;
  end process;

  -- Forward the read bin + count to bin writer
  bin_reader_to_writer_mosi.wr      <= bin_reader_miso.rdval;
  bin_reader_to_writer_mosi.wrdata  <= RESIZE_UVEC(bin_reader_miso.rddata(c_ram_dat_w - 1 downto 0), c_mem_data_w);
  bin_reader_to_writer_mosi.address <= prv_bin_reader_mosi.address;
  nxt_bin_reader_to_writer_count    <= TO_UINT(snk_in_reg.channel);  -- Use register (_nxt) to align count with rdval instead of snk_in_reg.valid

  -------------------------------------------------------------------------------
  -- bin_writer : Increment the bin, do write request
  -- . Input  : bin_reader_to_writer_mosi (from bin_reader = bin + bin count)
  -- . Output : bin_writer_mosi
  -------------------------------------------------------------------------------
  nxt_bin_writer_mosi.rd      <= '0';
  nxt_bin_writer_mosi.wr      <= bin_reader_to_writer_mosi.wr;
  nxt_bin_writer_mosi.address <= bin_reader_to_writer_mosi.address;
  nxt_bin_writer_mosi.wrdata  <= INCR_UVEC(bin_reader_to_writer_mosi.wrdata, bin_reader_to_writer_count) when bin_reader_to_writer_mosi.wr = '1' else bin_writer_mosi.wrdata;

  -- Register the outputs (above we have a combinational adder = propagation delay)
  p_bin_writer_mosi : process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      bin_writer_mosi     <= c_mem_mosi_rst;
    elsif rising_edge(dp_clk) then
      bin_writer_mosi     <= nxt_bin_writer_mosi;
    end if;
  end process;

  -------------------------------------------------------------------------------
  -- Two RAM (common_ram_r_w) instances. The user can read the histogram from the
  -- instance that is not being written to by the bin_writer.
  -- . Input:  bin_writer_mosi (writes bins)
  --           bin_reader_mosi (requests to read bins to increment bin count)
  --           histogram_rd_mosi (requests to read the bins on the user side)
  --           histogram_wr_mosi (on user side, auto clears RAM every sync)
  -- . Output: histogram_rd_miso (carries the bins the user wants to read)
  --           bin_reader_miso (carries then bins the bin_reader wants to read)
  -- . Note: the ram_pointer is carried (with different latencies) as MSbit in:
  --         . bin_writer_mosi.address
  --         . bin_reader_mosi.address
  -------------------------------------------------------------------------------
  bin_writer_ram_pointer <= bin_writer_mosi.address(c_ram_adr_w);
  bin_reader_ram_pointer <= bin_reader_mosi.address(c_ram_adr_w);

  -- Store the previous RAM pointer of the read bus
  p_prv_ram_pointer : process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      prv_bin_reader_ram_pointer <= '0';
    elsif rising_edge(dp_clk) then
      prv_bin_reader_ram_pointer <= bin_reader_ram_pointer;
    end if;
  end process;

  -- Let bin_writter write RAM 0 while user reads RAM 1 and vice versa
  common_ram_r_w_wr_mosi_arr(0) <= bin_writer_mosi when bin_writer_ram_pointer = '0' else histogram_wr_mosi;
  common_ram_r_w_rd_mosi_arr(0) <= bin_reader_mosi when bin_reader_ram_pointer = '0' else histogram_rd_mosi;
  common_ram_r_w_wr_mosi_arr(1) <= bin_writer_mosi when bin_writer_ram_pointer = '1' else histogram_wr_mosi;
  common_ram_r_w_rd_mosi_arr(1) <= bin_reader_mosi when bin_reader_ram_pointer = '1' else histogram_rd_mosi;

  -- Let bin_reader read RAM 0 while user reads RAM 1 and vice versa
  -- . We always want the MISO bus to switch 1 cycle later than the MOSI (such that the MM operation can finish); hence using prv_bin_reader_ram_pointer.
  bin_reader_miso   <= common_ram_r_w_rd_miso_arr(0) when prv_bin_reader_ram_pointer = '0' else common_ram_r_w_rd_miso_arr(1);
  histogram_rd_miso <= common_ram_r_w_rd_miso_arr(1) when prv_bin_reader_ram_pointer = '0' else common_ram_r_w_rd_miso_arr(0);

  gen_common_ram_r_w : for i in 0 to c_nof_ram_pages - 1 generate
    u_common_ram_r_w : entity common_lib.common_ram_r_w
    generic map (
      g_technology     => c_tech_select_default,
      g_ram            => c_ram,
      g_init_file      => "UNUSED"
    )
    port map (
      rst      => dp_rst,
      clk      => dp_clk,
      clken    => '1',
      wr_en    => common_ram_r_w_wr_mosi_arr(i).wr,
      wr_adr   => common_ram_r_w_wr_mosi_arr(i).address(c_ram_adr_w - 1 downto 0),
      wr_dat   => common_ram_r_w_wr_mosi_arr(i).wrdata(c_ram_dat_w - 1 downto 0),
      rd_en    => common_ram_r_w_rd_mosi_arr(i).rd,
      rd_adr   => common_ram_r_w_rd_mosi_arr(i).address(c_ram_adr_w - 1 downto 0),
      rd_dat   => common_ram_r_w_rd_miso_arr(i).rddata(c_ram_dat_w - 1 downto 0),
      rd_val   => common_ram_r_w_rd_miso_arr(i).rdval
    );
  end generate;

  -------------------------------------------------------------------------------
  -- Clear the RAM just before the next sync interval
  -------------------------------------------------------------------------------
  -- Count input data for automatic RAM clear before next sync interval
  nxt_data_cnt <= (others => '0') when snk_in.sync = '1' else INCR_UVEC(data_cnt, 1) when snk_in.valid = '1' else data_cnt;

  -- Clear all g_nof_bins RAM addresses just before the next sync
  ram_clear <= '1' when TO_UINT(data_cnt) = c_clear_sample_cnt - g_nof_bins - 1 else '0';

  -- Signal to indicate when RAM is being cleared
  nxt_ram_clearing <= '1' when ram_clear = '1' else '0' when TO_UINT(ram_clear_address) = g_nof_bins - 1 else ram_clearing;

  -- Address counter: 0 to g_nof_bins-1.
  nxt_ram_clear_address <= INCR_UVEC(ram_clear_address, 1) when ram_clearing = '1' else (others => '0');

  histogram_wr_mosi.wr                              <= ram_clearing;
  histogram_wr_mosi.address(c_ram_adr_w - 1 downto 0) <= ram_clear_address;
  histogram_wr_mosi.wrdata                          <= (others => '0');
  histogram_wr_mosi.rd                              <= '0';

  -- Registers
  p_ram_clearing : process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      ram_clear_address <= (others => '0');
      ram_clearing      <= '0';
      data_cnt          <= (others => '0');
    elsif rising_edge(dp_clk) then
      ram_clear_address <= nxt_ram_clear_address;
      ram_clearing      <= nxt_ram_clearing;
      data_cnt          <= nxt_data_cnt;
    end if;
  end process;

  -------------------------------------------------------------------------------
  -- Expose the MM buses to the user
  -------------------------------------------------------------------------------
  ram_miso <= histogram_rd_miso;
  histogram_rd_mosi <= ram_mosi;
end rtl;
