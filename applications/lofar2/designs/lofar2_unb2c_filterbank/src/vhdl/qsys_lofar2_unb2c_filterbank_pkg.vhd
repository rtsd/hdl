-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_lofar2_unb2c_filterbank_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus platform designer:
  -----------------------------------------------------------------------------
   component qsys_lofar2_unb2c_filterbank is
        port (
            avs_eth_0_clk_export                    : out std_logic;  -- export
            avs_eth_0_irq_export                    : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export            : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export               : out std_logic;  -- export
            avs_eth_0_ram_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export              : out std_logic;  -- export
            avs_eth_0_ram_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export            : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export               : out std_logic;  -- export
            avs_eth_0_reg_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export              : out std_logic;  -- export
            avs_eth_0_reg_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                  : out std_logic;  -- export
            avs_eth_0_tse_address_export            : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export               : out std_logic;  -- export
            avs_eth_0_tse_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export        : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export              : out std_logic;  -- export
            avs_eth_0_tse_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                 : in  std_logic                     := 'X';  -- clk
            jesd204b_address_export                 : out std_logic_vector(11 downto 0);  -- export
            jesd204b_clk_export                     : out std_logic;  -- export
            jesd204b_read_export                    : out std_logic;  -- export
            jesd204b_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            jesd204b_reset_export                   : out std_logic;  -- export
            jesd204b_write_export                   : out std_logic;  -- export
            jesd204b_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            pio_pps_address_export                  : out std_logic_vector(0 downto 0);  -- export
            pio_pps_clk_export                      : out std_logic;  -- export
            pio_pps_read_export                     : out std_logic;  -- export
            pio_pps_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                    : out std_logic;  -- export
            pio_pps_write_export                    : out std_logic;  -- export
            pio_pps_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export          : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export              : out std_logic;  -- export
            pio_system_info_read_export             : out std_logic;  -- export
            pio_system_info_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export            : out std_logic;  -- export
            pio_system_info_write_export            : out std_logic;  -- export
            pio_system_info_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export      : out std_logic;  -- export
            ram_aduh_monitor_address_export         : out std_logic_vector(12 downto 0);  -- export
            ram_aduh_monitor_clk_export             : out std_logic;  -- export
            ram_aduh_monitor_read_export            : out std_logic;  -- export
            ram_aduh_monitor_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_aduh_monitor_reset_export           : out std_logic;  -- export
            ram_aduh_monitor_write_export           : out std_logic;  -- export
            ram_aduh_monitor_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buf_bsn_address_export    : out std_logic_vector(13 downto 0);  -- export
            ram_diag_data_buf_bsn_clk_export        : out std_logic;  -- export
            ram_diag_data_buf_bsn_read_export       : out std_logic;  -- export
            ram_diag_data_buf_bsn_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buf_bsn_reset_export      : out std_logic;  -- export
            ram_diag_data_buf_bsn_write_export      : out std_logic;  -- export
            ram_diag_data_buf_bsn_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buf_jesd_address_export   : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buf_jesd_clk_export       : out std_logic;  -- export
            ram_diag_data_buf_jesd_read_export      : out std_logic;  -- export
            ram_diag_data_buf_jesd_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buf_jesd_reset_export     : out std_logic;  -- export
            ram_diag_data_buf_jesd_write_export     : out std_logic;  -- export
            ram_diag_data_buf_jesd_writedata_export : out std_logic_vector(31 downto 0);  -- export
            ram_equalizer_gains_address_export      : out std_logic_vector(12 downto 0);  -- export
            ram_equalizer_gains_clk_export          : out std_logic;  -- export
            ram_equalizer_gains_read_export         : out std_logic;  -- export
            ram_equalizer_gains_readdata_export     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_equalizer_gains_reset_export        : out std_logic;  -- export
            ram_equalizer_gains_write_export        : out std_logic;  -- export
            ram_equalizer_gains_writedata_export    : out std_logic_vector(31 downto 0);  -- export
            ram_fil_coefs_address_export            : out std_logic_vector(13 downto 0);  -- export
            ram_fil_coefs_clk_export                : out std_logic;  -- export
            ram_fil_coefs_read_export               : out std_logic;  -- export
            ram_fil_coefs_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_fil_coefs_reset_export              : out std_logic;  -- export
            ram_fil_coefs_write_export              : out std_logic;  -- export
            ram_fil_coefs_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            ram_scrap_address_export                : out std_logic_vector(8 downto 0);  -- export
            ram_scrap_clk_export                    : out std_logic;  -- export
            ram_scrap_read_export                   : out std_logic;  -- export
            ram_scrap_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_scrap_reset_export                  : out std_logic;  -- export
            ram_scrap_write_export                  : out std_logic;  -- export
            ram_scrap_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            ram_st_sst_address_export               : out std_logic_vector(13 downto 0);  -- export
            ram_st_sst_clk_export                   : out std_logic;  -- export
            ram_st_sst_read_export                  : out std_logic;  -- export
            ram_st_sst_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_st_sst_reset_export                 : out std_logic;  -- export
            ram_st_sst_write_export                 : out std_logic;  -- export
            ram_st_sst_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            ram_wg_address_export                   : out std_logic_vector(13 downto 0);  -- export
            ram_wg_clk_export                       : out std_logic;  -- export
            ram_wg_read_export                      : out std_logic;  -- export
            ram_wg_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_wg_reset_export                     : out std_logic;  -- export
            ram_wg_write_export                     : out std_logic;  -- export
            ram_wg_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reg_aduh_monitor_address_export         : out std_logic_vector(5 downto 0);  -- export
            reg_aduh_monitor_clk_export             : out std_logic;  -- export
            reg_aduh_monitor_read_export            : out std_logic;  -- export
            reg_aduh_monitor_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_aduh_monitor_reset_export           : out std_logic;  -- export
            reg_aduh_monitor_write_export           : out std_logic;  -- export
            reg_aduh_monitor_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_input_address_export    : out std_logic_vector(7 downto 0);  -- export
            reg_bsn_monitor_input_clk_export        : out std_logic;  -- export
            reg_bsn_monitor_input_read_export       : out std_logic;  -- export
            reg_bsn_monitor_input_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_input_reset_export      : out std_logic;  -- export
            reg_bsn_monitor_input_write_export      : out std_logic;  -- export
            reg_bsn_monitor_input_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_scheduler_address_export        : out std_logic_vector(0 downto 0);  -- export
            reg_bsn_scheduler_clk_export            : out std_logic;  -- export
            reg_bsn_scheduler_read_export           : out std_logic;  -- export
            reg_bsn_scheduler_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_scheduler_reset_export          : out std_logic;  -- export
            reg_bsn_scheduler_write_export          : out std_logic;  -- export
            reg_bsn_scheduler_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_source_address_export           : out std_logic_vector(1 downto 0);  -- export
            reg_bsn_source_clk_export               : out std_logic;  -- export
            reg_bsn_source_read_export              : out std_logic;  -- export
            reg_bsn_source_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_source_reset_export             : out std_logic;  -- export
            reg_bsn_source_write_export             : out std_logic;  -- export
            reg_bsn_source_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buf_bsn_address_export    : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buf_bsn_clk_export        : out std_logic;  -- export
            reg_diag_data_buf_bsn_read_export       : out std_logic;  -- export
            reg_diag_data_buf_bsn_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buf_bsn_reset_export      : out std_logic;  -- export
            reg_diag_data_buf_bsn_write_export      : out std_logic;  -- export
            reg_diag_data_buf_bsn_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buf_jesd_address_export   : out std_logic_vector(1 downto 0);  -- export
            reg_diag_data_buf_jesd_clk_export       : out std_logic;  -- export
            reg_diag_data_buf_jesd_read_export      : out std_logic;  -- export
            reg_diag_data_buf_jesd_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buf_jesd_reset_export     : out std_logic;  -- export
            reg_diag_data_buf_jesd_write_export     : out std_logic;  -- export
            reg_diag_data_buf_jesd_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_selector_address_export          : out std_logic_vector(0 downto 0);  -- export
            reg_dp_selector_clk_export              : out std_logic;  -- export
            reg_dp_selector_read_export             : out std_logic;  -- export
            reg_dp_selector_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_selector_reset_export            : out std_logic;  -- export
            reg_dp_selector_write_export            : out std_logic;  -- export
            reg_dp_selector_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_dp_shiftram_address_export          : out std_logic_vector(4 downto 0);  -- export
            reg_dp_shiftram_clk_export              : out std_logic;  -- export
            reg_dp_shiftram_read_export             : out std_logic;  -- export
            reg_dp_shiftram_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_shiftram_reset_export            : out std_logic;  -- export
            reg_dp_shiftram_write_export            : out std_logic;  -- export
            reg_dp_shiftram_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_address_export            : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                : out std_logic;  -- export
            reg_dpmm_ctrl_read_export               : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export              : out std_logic;  -- export
            reg_dpmm_ctrl_write_export              : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export            : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                : out std_logic;  -- export
            reg_dpmm_data_read_export               : out std_logic;  -- export
            reg_dpmm_data_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export              : out std_logic;  -- export
            reg_dpmm_data_write_export              : out std_logic;  -- export
            reg_dpmm_data_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                 : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                     : out std_logic;  -- export
            reg_epcs_read_export                    : out std_logic;  -- export
            reg_epcs_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                   : out std_logic;  -- export
            reg_epcs_write_export                   : out std_logic;  -- export
            reg_epcs_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_address_export       : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_clk_export           : out std_logic;  -- export
            reg_fpga_temp_sens_read_export          : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export         : out std_logic;  -- export
            reg_fpga_temp_sens_write_export         : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export     : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_address_export    : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_clk_export        : out std_logic;  -- export
            reg_fpga_voltage_sens_read_export       : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export      : out std_logic;  -- export
            reg_fpga_voltage_sens_write_export      : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export            : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                : out std_logic;  -- export
            reg_mmdp_ctrl_read_export               : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export              : out std_logic;  -- export
            reg_mmdp_ctrl_write_export              : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export            : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                : out std_logic;  -- export
            reg_mmdp_data_read_export               : out std_logic;  -- export
            reg_mmdp_data_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export              : out std_logic;  -- export
            reg_mmdp_data_write_export              : out std_logic;  -- export
            reg_mmdp_data_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                 : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                     : out std_logic;  -- export
            reg_remu_read_export                    : out std_logic;  -- export
            reg_remu_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                   : out std_logic;  -- export
            reg_remu_write_export                   : out std_logic;  -- export
            reg_remu_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_si_address_export                   : out std_logic_vector(0 downto 0);  -- export
            reg_si_clk_export                       : out std_logic;  -- export
            reg_si_read_export                      : out std_logic;  -- export
            reg_si_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_si_reset_export                     : out std_logic;  -- export
            reg_si_write_export                     : out std_logic;  -- export
            reg_si_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reg_unb_pmbus_address_export            : out std_logic_vector(5 downto 0);  -- export
            reg_unb_pmbus_clk_export                : out std_logic;  -- export
            reg_unb_pmbus_read_export               : out std_logic;  -- export
            reg_unb_pmbus_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_pmbus_reset_export              : out std_logic;  -- export
            reg_unb_pmbus_write_export              : out std_logic;  -- export
            reg_unb_pmbus_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_unb_sens_address_export             : out std_logic_vector(5 downto 0);  -- export
            reg_unb_sens_clk_export                 : out std_logic;  -- export
            reg_unb_sens_read_export                : out std_logic;  -- export
            reg_unb_sens_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_sens_reset_export               : out std_logic;  -- export
            reg_unb_sens_write_export               : out std_logic;  -- export
            reg_unb_sens_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                  : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                      : out std_logic;  -- export
            reg_wdi_read_export                     : out std_logic;  -- export
            reg_wdi_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                    : out std_logic;  -- export
            reg_wdi_write_export                    : out std_logic;  -- export
            reg_wdi_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_wg_address_export                   : out std_logic_vector(5 downto 0);  -- export
            reg_wg_clk_export                       : out std_logic;  -- export
            reg_wg_read_export                      : out std_logic;  -- export
            reg_wg_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wg_reset_export                     : out std_logic;  -- export
            reg_wg_write_export                     : out std_logic;  -- export
            reg_wg_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reset_reset_n                           : in  std_logic                     := 'X';  -- reset_n
            rom_system_info_address_export          : out std_logic_vector(9 downto 0);  -- export
            rom_system_info_clk_export              : out std_logic;  -- export
            rom_system_info_read_export             : out std_logic;  -- export
            rom_system_info_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export            : out std_logic;  -- export
            rom_system_info_write_export            : out std_logic;  -- export
            rom_system_info_writedata_export        : out std_logic_vector(31 downto 0)  -- export
        );
    end component qsys_lofar2_unb2c_filterbank;
end qsys_lofar2_unb2c_filterbank_pkg;
