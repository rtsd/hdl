-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_common_duty_cycle is
end tb_common_duty_cycle;

architecture tb of tb_common_duty_cycle is
  constant c_clk_period   : time    := 10 ns;

  constant c_dc_max_period_cnt : natural := 30;

  signal tb_end       : std_logic := '0';
  signal rst          : std_logic := '1';
  signal clk          : std_logic := '1';

  signal dc_per_cnt   : std_logic_vector(ceil_log2(c_dc_max_period_cnt + 1) - 1 downto 0);
  signal dc_act_cnt   : std_logic_vector(ceil_log2(c_dc_max_period_cnt + 1) - 1 downto 0);

  signal dc_out_en    : std_logic;
  signal dc_out       : std_logic;
begin
  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  rst <= '1', '0' after c_clk_period * 7;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -- MM control
  p_mm : process
  begin
    tb_end    <= '0';

    dc_per_cnt <= TO_UVEC(10, ceil_log2(c_dc_max_period_cnt + 1));
    dc_act_cnt <= TO_UVEC(3,  ceil_log2(c_dc_max_period_cnt + 1));

    -- Basic output enable control.
    dc_out_en <= '0';

    -- ** Disabled ** --

    wait for 400 ns;
    wait until clk = '1';
    dc_out_en <= '1';
    wait for 1 us;
    wait until clk = '1';
    dc_out_en <= '0';

    -- ** Disabled ** --

    wait for 1 us;

    -- Now actively control DC active level
    for i in 0 to 10 loop
      dc_act_cnt <= TO_UVEC(i, ceil_log2(c_dc_max_period_cnt + 1));
      dc_out_en <= '1';
      wait for 100 ns;
      wait until clk = '1';
    end loop;

    wait for 80 ns;
    dc_out_en <= '0';

    -- ** Disabled ** --

    wait for 1 us;
    wait until clk = '1';

    -- Now actively control DC active level for 2x the previous period count => twice the resolution.
    dc_per_cnt <= TO_UVEC(20, ceil_log2(c_dc_max_period_cnt + 1));

    for i in 0 to 20 loop
      dc_act_cnt <= TO_UVEC(i, ceil_log2(c_dc_max_period_cnt + 1));
      dc_out_en <= '1';
      wait for 100 ns;
      wait until clk = '1';
    end loop;

    wait for 1 us;

    tb_end   <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: common_duty_cycle
  -----------------------------------------------------------------------------

  dut : entity work.common_duty_cycle
  generic map (
    g_rst_lvl => '0',
    g_dis_lvl => '0',
    g_act_lvl => '1',
    g_per_cnt => c_dc_max_period_cnt,
    g_act_cnt => 10
  )
  port map (
    rst         => rst,
    clk         => clk,

    dc_per_cnt  => dc_per_cnt,
    dc_act_cnt  => dc_act_cnt,

    dc_out_en   => dc_out_en,
    dc_out      => dc_out
   );
end tb;
