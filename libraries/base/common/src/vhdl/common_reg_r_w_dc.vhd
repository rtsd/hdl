-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide dual clock domain crossing to common_reg_r_w.vhd
-- Description:
-- . Write vector to out_reg
-- . Read vector from in_reg or readback from out_reg
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                              data[31:0]                               |  0
--  |-----------------------------------------------------------------------|
--  |                              data[63:32]                              |  1
--  |-----------------------------------------------------------------------|
--
-- . g_readback
--   When g_readback is TRUE then the written data is read back from the st_clk
--   domain directly into the mm_clk domain, so without ST --> MM clock domain
--   crossing logic. This is allowed because the read back value is stable.
--   For readback the out_reg needs to be connected to in_reg, independent of
--   the g_readback setting, because the readback value is read back from the
--   st_clk domain. In this way the readback value also reveals that the
--   written value is indeed available in the st_clk domain (ie. this shows
--   that the st_clk is active). If g_cross_clock_domain=FALSE, then g_readback
--   is don't care.
--   In fact g_readback could better be called g_st_readback. An alternative
--   g_mm_readback could define direct read back in the MM clock domain and
--   would allow leaving the in_reg not connected.
-- . reg_wr_arr
--   Provides write access pulse in dp_clk domain. However the pulse may arrive
--   before the out_reg data, due to that the pulse and data do not cross the
--   clock domain in the same way. A solution would be to use a single
--   common_reg_cross_domain instance to transfer both the wr access info and
--   the data.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

entity common_reg_r_w_dc is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_in_new_latency     : natural := 0;  -- >= 0
    g_readback           : boolean := false;  -- must use FALSE for write/read or read only register when g_cross_clock_domain=TRUE
    --g_readback           : BOOLEAN := TRUE;   -- can use TRUE for write and readback register
    g_reg                : t_c_mem := c_mem_reg;
    g_init_reg           : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0')
  );
  port (
    -- Clocks and reset
    mm_rst      : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk      : in  std_logic;  -- memory-mapped bus clock
    st_rst      : in  std_logic;  -- reset synchronous with st_clk
    st_clk      : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in      : in  t_mem_mosi;  -- actual ranges defined by g_reg
    sla_out     : out t_mem_miso;  -- actual ranges defined by g_reg

    -- MM registers in st_clk domain
    reg_wr_arr  : out std_logic_vector(            g_reg.nof_dat - 1 downto 0);
    reg_rd_arr  : out std_logic_vector(            g_reg.nof_dat - 1 downto 0);
    in_new      : in  std_logic := '1';
    in_reg      : in  std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0);
    out_reg     : out std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0);
    out_new     : out std_logic  -- Pulses '1' when new data has been written.
  );
end common_reg_r_w_dc;

architecture str of common_reg_r_w_dc is
  -- Registers in mm_clk domain
  signal vector_wr_arr   : std_logic_vector(            g_reg.nof_dat - 1 downto 0);
  signal vector_rd_arr   : std_logic_vector(            g_reg.nof_dat - 1 downto 0);
  signal out_vector      : std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0);
  signal in_vector       : std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0);

  -- Initialize output to avoid Warning: (vsim-8684) No drivers exist on out port *, and its initial value is not used
  signal i_sla_out       : t_mem_miso := c_mem_miso_rst;

  signal reg_wr_arr_i    : std_logic_vector(            g_reg.nof_dat - 1 downto 0);
  signal wr_pulse        : std_logic;
  signal toggle          : std_logic;
  signal out_new_i       : std_logic;
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  ------------------------------------------------------------------------------

  sla_out <= i_sla_out;

  u_reg : entity work.common_reg_r_w
  generic map (
    g_reg      => g_reg,
    g_init_reg => g_init_reg
  )
  port map (
    rst         => mm_rst,
    clk         => mm_clk,
    -- control side
    wr_en       => sla_in.wr,
    wr_adr      => sla_in.address(g_reg.adr_w - 1 downto 0),
    wr_dat      => sla_in.wrdata(g_reg.dat_w - 1 downto 0),
    rd_en       => sla_in.rd,
    rd_adr      => sla_in.address(g_reg.adr_w - 1 downto 0),
    rd_dat      => i_sla_out.rddata(g_reg.dat_w - 1 downto 0),
    rd_val      => i_sla_out.rdval,
    -- data side
    reg_wr_arr  => vector_wr_arr,
    reg_rd_arr  => vector_rd_arr,
    out_reg     => out_vector,
    in_reg      => in_vector
  );

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate
    in_vector   <= in_reg;
    out_reg     <= out_vector;
    reg_wr_arr  <= vector_wr_arr;
    reg_rd_arr  <= vector_rd_arr;
    out_new     <= vector_wr_arr(0);
  end generate;  -- no_cross

  gen_cross : if g_cross_clock_domain = true generate
    gen_rdback : if g_readback = true generate
      in_vector <= in_reg;
    end generate;

    gen_rd : if g_readback = false generate
      u_in_vector : entity work.common_reg_cross_domain
      generic map (
        g_in_new_latency => g_in_new_latency
      )
      port map (
        in_rst      => st_rst,
        in_clk      => st_clk,
        in_new      => in_new,
        in_dat      => in_reg,
        in_done     => OPEN,
        out_rst     => mm_rst,
        out_clk     => mm_clk,
        out_dat     => in_vector,
        out_new     => open
      );
    end generate;

    u_out_reg : entity work.common_reg_cross_domain
    generic map(
      g_out_dat_init => g_init_reg
    )
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => out_vector,
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => out_reg,
      out_new     => out_new_i
    );

    u_toggle : entity work.common_switch
    generic map (
      g_rst_level    => '0',
      g_priority_lo  => false,
      g_or_high      => false,
      g_and_low      => false
    )
    port map (
      rst         => st_rst,
      clk         => st_clk,
      switch_high => wr_pulse,
      switch_low  => out_new_i,
      out_level   => toggle
    );

    wr_pulse   <= '0' when vector_or(reg_wr_arr_i) = '0' else '1';
    out_new    <= out_new_i and toggle;
    reg_wr_arr <= reg_wr_arr_i;

    gen_access_evt : for I in 0 to g_reg.nof_dat - 1 generate
      u_reg_wr_arr : entity work.common_spulse
      port map (
        in_rst    => mm_rst,
        in_clk    => mm_clk,
        in_pulse  => vector_wr_arr(I),
        in_busy   => OPEN,
        out_rst   => st_rst,
        out_clk   => st_clk,
        out_pulse => reg_wr_arr_i(I)
      );

      u_reg_rd_arr : entity work.common_spulse
      port map (
        in_rst    => mm_rst,
        in_clk    => mm_clk,
        in_pulse  => vector_rd_arr(I),
        in_busy   => OPEN,
        out_rst   => st_rst,
        out_clk   => st_clk,
        out_pulse => reg_rd_arr(I)
      );
    end generate;

  end generate;  -- gen_cross
end str;
