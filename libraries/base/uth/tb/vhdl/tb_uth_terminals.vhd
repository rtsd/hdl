-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify uth_terminal_tx and uth_terminal_rx using random stimuli
-- Description:
-- . TB stimuli and verify are identical to tb_dp_distribute.
-- . Data flow:
--     in_sosi_arr(0:n-1) --> uth_terminal_tx(n,m) --> serial(0:m-1)
--                        --> uth_terminal_rx(m,n) --> out_sosi_arr(0:n-1)
--                                                 <-- out_siso_arr(0:n-1)
--
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct output
--
-- Remark:

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_uth_terminals is
  generic (
    -- Try flow control
    g_in_en            : t_dp_flow_control_enum := e_pulse;  -- always active, random or pulse flow control
    g_out_ready        : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_nof_repeat       : natural := 1000;  -- >= 1, number of frames
    -- Try PHY model
    g_phy_fifo_size    : natural := 0;  -- when 0 model PHY link as wire, else using a FIFO with that size
    -- Try rx timeout to recover from link disconnect by tb_uth_terminals.do
    g_uth_rx_timeout_w : natural := 5;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
    -- Try input FIFO for Tx distribute
    g_tx_use_fifo      : boolean := true;
    g_tx_fifo_fill     : natural := 15;
    -- Try input FIFO for Rx distribute
    g_rx_use_fifo      : boolean := false;
    g_rx_fifo_fill     : natural := 15;
    -- Try sync and BSN through FIFOs
    g_fifo_use_sync    : boolean := true;
    g_fifo_use_bsn     : boolean := true;
    -- Try distribute settings
    g_nof_input        : natural := 4;  -- = n
    g_nof_serial       : natural := 3  -- = m
  );
end tb_uth_terminals;

architecture tb of tb_uth_terminals is
  constant c_rl                          : natural := 1;
  constant c_data_init                   : integer := 0;
  constant c_frame_len                   : integer := 10;  -- 96
  constant c_gap_len                     : integer := 0;  -- 256 - c_frame_len;
  constant c_pulse_active                : natural := 1;
  constant c_pulse_period                : natural := 3;
  constant c_nof_repeat_extra            : natural := sel_a_b(g_tx_use_fifo, 1, 0) * (ceil_div(g_tx_fifo_fill, c_frame_len) + 1) + sel_a_b(g_rx_use_fifo, 1, 0) * (ceil_div(g_rx_fifo_fill, c_frame_len) + 1);
  constant c_in_nof_repeat               : natural := g_nof_repeat + c_nof_repeat_extra;
  constant c_nof_expected                : natural := g_nof_repeat;

  constant c_use_fifos                   : boolean := g_tx_use_fifo or g_rx_use_fifo;
  constant c_use_sync                    : boolean := not c_use_fifos or (c_use_fifos and g_fifo_use_sync);
  constant c_use_bsn                     : boolean := not c_use_fifos or (c_use_fifos and g_fifo_use_bsn);
  constant c_use_channel                 : boolean := true;
  constant c_sync_period                 : natural := 7;
  constant c_sync_offset                 : natural := 2;

  constant c_data_w                      : natural := 16;
  constant c_bsn_w                       : natural := c_data_w;
  constant c_channel_w                   : natural := c_data_w;
  constant c_fifo_size                   : natural := c_bram_m9k_fifo_depth;  -- = 256
  constant c_fifo_af_margin              : natural := c_fifo_afull_margin;  -- nof words below max (full) at which fifo is considered almost full. For UTH (rtl_hold) default c_fifo_afull_margin = 4 is OK for the other architectures more margin is needed e.g. 10

  constant c_data_init_offset            : integer := 13;

  -- DP packet
  constant c_link_channel_lo             : natural := ceil_log2(g_nof_serial);
  constant c_code_channel_lo             : boolean := true;  -- Code channel_lo in DP packet CHAN field

  -- UTH
  constant c_uth_frame_len               : natural := func_dp_packet_overhead_len(c_data_w) + c_frame_len;
  constant c_uth_nof_ch                  : natural := 1;  -- selects UTH TLEN based on the snk_in channel_lo part that is not in the DP Packet CHAN field
  constant c_uth_len_max                 : natural := 255;
  constant c_uth_typ_ofs                 : natural := c_uth_len_max + 1;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
  constant c_uth_typ_arr                 : t_natural_arr := array_init(  c_uth_typ_ofs, c_uth_nof_ch, 1);
  constant c_uth_len_arr                 : t_natural_arr := array_init(c_uth_frame_len, c_uth_nof_ch);

  subtype t_data_arr   is t_slv_16_arr(g_nof_input - 1 downto 0);  -- width 16 must match c_data_w
  type    t_rl_vec_arr is array (g_nof_input - 1 downto 0) of std_logic_vector(0 to c_rl);

  signal tb_end_vec        : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal tb_end            : std_logic;
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso_arr       : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal in_sosi_arr       : t_dp_sosi_arr(g_nof_input - 1 downto 0);

  signal tx_serial_siso_arr   : t_dp_siso_arr(g_nof_serial - 1 downto 0);
  signal tx_serial_sosi_arr   : t_dp_sosi_arr(g_nof_serial - 1 downto 0);

  signal rx_serial_siso_arr   : t_dp_siso_arr(g_nof_serial - 1 downto 0);
  signal rx_serial_sosi_arr   : t_dp_sosi_arr(g_nof_serial - 1 downto 0);

  signal out_siso          : t_dp_siso;
  signal out_siso_arr      : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal out_sosi_arr      : t_dp_sosi_arr(g_nof_input - 1 downto 0);

  signal verify_en         : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal verify_done       : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal verify_end        : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal count_eop         : t_integer_arr(g_nof_input - 1 downto 0) := (others => 0);
  signal prev_count_eop    : t_integer_arr(g_nof_input - 1 downto 0) := (others => 0);

  signal prev_out_ready    : t_rl_vec_arr;
  signal prev_out_data     : t_data_arr := array_init(c_data_init - 1, g_nof_input, c_data_init_offset);
  signal out_data          : t_data_arr;
  signal out_bsn           : t_data_arr;
  signal out_sync          : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_val           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_sop           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_eop           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_channel       : t_data_arr;
  signal hold_out_sop      : std_logic_vector(g_nof_input - 1 downto 0);
  signal expected_out_data    : t_data_arr;
  signal expected_out_bsn     : t_data_arr;
  signal expected_out_channel : t_data_arr;
begin
  tb_end <= vector_and(tb_end_vec);

  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;
  out_siso.xon   <= '1';

  out_siso_arr <= (others => out_siso);

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_input : for I in g_nof_input - 1 downto 0 generate
    p_stimuli : process
      variable v_data_init  : natural;
      variable v_data_value : natural;
      variable v_frame_len  : natural;
      variable v_channel    : natural;
      variable v_error      : natural;
      variable v_sync       : std_logic;
    begin
      v_data_init := c_data_init + I * c_data_init_offset;  -- offset the frame data   for each input, so the inputs can be recognized at the output
      v_frame_len := c_frame_len;  -- same frame length for each input, so only one UTH TLEN type is needed
      v_channel   := I;
      v_error     := 0;
      in_sosi_arr(I) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);
      proc_common_wait_some_cycles(clk, 5);

      -- Pre-calculate the expected out_data for at the end of the stimuli
      expected_out_data(I)    <= TO_UVEC(v_data_init + v_frame_len * c_nof_expected - 1, c_data_w);
      expected_out_bsn(I)     <= TO_UVEC(                        c_nof_expected - 1, c_data_w);
      expected_out_channel(I) <= TO_UVEC(I, c_data_w);

      -- Begin of stimuli
      v_data_value := v_data_init;
      for R in 0 to c_in_nof_repeat - 1 loop
        v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_value, 0, 0, v_frame_len, v_channel, v_error, v_sync, TO_DP_BSN(R), clk, in_en, in_siso_arr(I), in_sosi_arr(I));
        proc_common_wait_some_cycles(clk, c_gap_len);
        v_data_value := v_data_value + v_frame_len;
      end loop;

      -- End of stimuli
      proc_common_wait_until_high(clk, verify_end(I));  -- in case of using FIFOs verify_done is issued somewhat before the output will stop due to c_nof_expected
      proc_common_wait_some_cycles(clk, 10 * c_frame_len);  -- continue some more after verify_done to see that the output has stopped
      tb_end_vec(I) <= '1';
      wait;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  gen_verify : for I in g_nof_input - 1 downto 0 generate
    -- Verification logistics
    verify_en(I) <= '1'               when rising_edge(clk) and out_sosi_arr(I).sop = '1';  -- enable verify after first output sop
    count_eop(I) <= count_eop(I) + 1    when rising_edge(clk) and out_sosi_arr(I).eop = '1';  -- count number of output eop
    prev_count_eop(I) <= count_eop(I) when rising_edge(clk);
    verify_done(I) <= '1'             when rising_edge(clk) and count_eop(I) = c_nof_expected and count_eop(I) /= prev_count_eop(I) else
                      '0'             when rising_edge(clk);  -- signal verify done after c_nof_expected frames, use pulse to have only one assert from proc_dp_verify_value()
    verify_end(I) <= '1'              when rising_edge(clk) and verify_done(I) = '1';  -- signal verify_end after verify_done pulse, use level to ensure tb_end even if the stimuli are still busy

    -- Actual verification of the output streams
    proc_dp_verify_data("out_sosi_arr.data", c_rl, clk, verify_en(I), out_siso_arr(I).ready, out_sosi_arr(I).valid, out_data(I), prev_out_data(I));  -- Verify that the output is incrementing data, like the input stimuli
    proc_dp_verify_valid(c_rl, clk, verify_en(I), out_siso_arr(I).ready, prev_out_ready(I), out_sosi_arr(I).valid);  -- Verify that the output valid fits with the output ready latency
    proc_dp_verify_sop_and_eop(clk, out_sosi_arr(I).valid, out_sosi_arr(I).sop, out_sosi_arr(I).eop, hold_out_sop(I));  -- Verify that sop and eop come in pairs

    gen_verify_sync : if c_use_sync and c_use_bsn generate
      proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en(I), out_sosi_arr(I).sync, out_sosi_arr(I).sop, out_sosi_arr(I).bsn);

      -- Use at e_equal because at verify done the result is independ of whether there are still frames in the FIFO
      proc_dp_verify_value(e_equal, clk, verify_done(I), expected_out_bsn(I), out_bsn(I));  -- Verify that the stimuli have been applied at all
    end generate;

    gen_verify_channel : if c_use_channel generate
      proc_dp_verify_value(e_equal, clk, verify_en(I), expected_out_channel(I), out_channel(I));
    end generate;

    -- Use at e_equal because at verify done the result is independ of whether there are still frames in the FIFO
    proc_dp_verify_value(e_equal, clk, verify_done(I), expected_out_data(I), prev_out_data(I));  -- Verify that the stimuli have been applied at all

    -- Monitoring
    out_data(I)    <= out_sosi_arr(I).data(c_data_w - 1 downto 0);
    out_bsn(I)     <= out_sosi_arr(I).bsn(c_data_w - 1 downto 0);
    out_sync(I)    <= out_sosi_arr(I).sync;
    out_val(I)     <= out_sosi_arr(I).valid;
    out_sop(I)     <= out_sosi_arr(I).sop;
    out_eop(I)     <= out_sosi_arr(I).eop;
    out_channel(I) <= out_sosi_arr(I).channel(c_data_w - 1 downto 0);
  end generate;

  ------------------------------------------------------------------------------
  -- DUT uth_terminal_tx
  ------------------------------------------------------------------------------

  -- n --> m
  tx : entity work.uth_terminal_tx
  generic map (
    -- Terminal IO
    g_nof_input             => g_nof_input,
    g_nof_output            => g_nof_serial,
    -- . DP Packet
    g_data_w                => c_data_w,
    g_packet_data_w         => c_data_w,
    -- . Uthernet
    g_uth_nof_ch            => c_uth_nof_ch,
    g_uth_typ_ofs           => c_uth_typ_ofs,
    g_uth_typ_arr           => c_uth_typ_arr,
    -- Input FIFO
    g_input_use_fifo        => g_tx_use_fifo,
    g_input_bsn_w           => c_bsn_w,
    g_input_channel_w       => c_channel_w,
    g_input_use_bsn         => c_use_bsn,
    g_input_use_channel     => c_use_channel,
    g_input_use_sync        => c_use_sync,
    g_input_fifo_af_margin  => c_fifo_af_margin,
    g_input_fifo_fill       => g_tx_fifo_fill,
    g_input_fifo_size       => c_fifo_size,
    -- Output FIFO
    g_output_use_fifo       => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => in_siso_arr,
    snk_in_arr  => in_sosi_arr,
    -- ST source
    src_in_arr  => tx_serial_siso_arr,
    src_out_arr => tx_serial_sosi_arr
  );

  -- Model PHY link by a FIFO
  no_phy_fifo : if g_phy_fifo_size = 0 generate
    tx_serial_siso_arr <= rx_serial_siso_arr;
    rx_serial_sosi_arr <= tx_serial_sosi_arr;
  end generate;

  gen_phy_fifo : if g_phy_fifo_size > 0 generate
    gen_nof_serial : for I in 0 to g_nof_serial - 1 generate
      u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
      generic map (
        g_data_w         => c_data_w,
        g_bsn_w          => 1,
        g_empty_w        => 1,
        g_channel_w      => 1,
        g_error_w        => 1,
        g_use_bsn        => false,
        g_use_empty      => false,
        g_use_channel    => false,
        g_use_error      => false,
        g_use_sync       => false,
        g_use_ctrl       => false,
        g_use_complex    => false,
        g_fifo_size      => g_phy_fifo_size,
        g_fifo_af_margin => 4,
        g_fifo_rl        => 1
      )
      port map (
        rst         => rst,
        clk         => clk,
        -- Monitor FIFO filling
        wr_ful      => OPEN,
        usedw       => OPEN,
        rd_emp      => OPEN,
        -- ST sink
        snk_out     => tx_serial_siso_arr(I),
        snk_in      => tx_serial_sosi_arr(I),
        -- ST source
        src_in      => rx_serial_siso_arr(I),
        src_out     => rx_serial_sosi_arr(I)
      );
    end generate;
  end generate;

  -- m --> n
  rx : entity work.uth_terminal_rx
  generic map (
    -- Terminal IO
    g_nof_input             => g_nof_serial,
    g_nof_output            => g_nof_input,
    -- . DP Packet
    g_data_w                => c_data_w,
    g_packet_data_w         => c_data_w,
    -- . Uthernet
    g_uth_nof_ch            => c_uth_nof_ch,
    g_uth_len_max           => c_uth_len_max,
    g_uth_typ_arr           => c_uth_typ_arr,
    g_uth_len_arr           => c_uth_len_arr,
    g_use_uth_err           => false,
    g_uth_err_bi            => 0,
    g_uth_timeout_w         => g_uth_rx_timeout_w,
    -- Input FIFO
    g_input_use_fifo        => g_rx_use_fifo,
    g_input_fifo_af_margin  => c_fifo_af_margin,
    g_input_fifo_fill       => g_rx_fifo_fill,
    g_input_fifo_size       => c_fifo_size,
    -- Output FIFO
    g_output_use_fifo       => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => rx_serial_siso_arr,
    snk_in_arr  => rx_serial_sosi_arr,
    -- ST source
    src_in_arr  => out_siso_arr,
    src_out_arr => out_sosi_arr
  );
end tb;
