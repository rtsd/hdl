-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose:
-- . Ring lane info register
-- Description:
-- See ring_pkg.vhd
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.ring_pkg.all;

entity ring_lane_info is
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk             : in  std_logic;  -- memory-mapped bus clock

    dp_clk             : in  std_logic;
    dp_rst             : in  std_logic;

    reg_mosi           : in  t_mem_mosi;
    reg_miso           : out t_mem_miso;

    -- inputs from other blocks
    lane_direction     : in std_logic;

    -- sdp info
    lane_info          : out t_ring_lane_info
  );
end ring_lane_info;

architecture str of ring_lane_info is
  signal lane_info_ro: t_ring_lane_info;  -- ro = read only
begin
  u_mm_fields: entity work.ring_lane_info_reg
  port map (

    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    reg_mosi  => reg_mosi,
    reg_miso  => reg_miso,

    lane_info_ro => lane_info_ro,
    lane_info    => lane_info
  );

  lane_info_ro.lane_direction <= lane_direction;
end str;
