-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 2
-- > run -all --> OK

entity tb_tb2_dp_demux is
end tb_tb2_dp_demux;

architecture tb of tb_tb2_dp_demux is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --                                                         in_en,    src_in.ready, in_channel, nof_repeat, nof_streams, mode_demux, mode_mux, use_channel_lo, combined_demux
  -- Framed mode 0
  u0_rnd_rnd         : entity work.tb2_dp_demux generic map (e_random, e_random,     1,          30,         3,           0,          0,        true,           false);
  u0_rnd_pls         : entity work.tb2_dp_demux generic map (e_random, e_pulse,      1,          30,         3,           0,          0,        true,           false);

  -- Framed mode 1
  u1_rnd_rnd         : entity work.tb2_dp_demux generic map (e_random, e_random,     1,          30,         3,           1,          1,        false,          false);
  u1_rnd_pls         : entity work.tb2_dp_demux generic map (e_random, e_pulse,      1,          30,         3,           1,          1,        false,          false);

  -- Unframed control mode 2
  u2_rnd_rnd         : entity work.tb2_dp_demux generic map (e_random, e_random,     1,          30,         3,           2,          2,        false,          false);
  u2_rnd_rnd_comb    : entity work.tb2_dp_demux generic map (e_random, e_random,     1,          30,         3,           2,          2,        false,          true);
  u2_rnd_pls         : entity work.tb2_dp_demux generic map (e_random, e_pulse,      1,          30,         3,           2,          2,        false,          false);
end tb;
