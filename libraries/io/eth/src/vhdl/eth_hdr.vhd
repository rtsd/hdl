-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Description:
-- . Store the 11 word header and make the header info available for monitoring
-- . Direct snk to src via g_header_store_and_forward = FALSE
-- . Store and forward the header via g_header_store_and_forward = TRUE to allow:
--   1) IP header checksum insertion for IP frames when g_ip_header_checksum_calculate = TRUE
--   2) Discard frames that are shorter than 11 words (the minimal header size)
--   3) Support option to discard frames based on their header info

entity eth_hdr is
  generic (
    g_header_store_and_forward     : boolean := true;
    g_ip_header_checksum_calculate : boolean := true
  );
  port (
    -- Clocks and reset
    rst               : in  std_logic;  -- reset synchronous with clk
    clk               : in  std_logic;  -- packet stream clock

    -- Streaming Sink
    snk_in            : in  t_dp_sosi;
    snk_out           : out t_dp_siso;

    -- Streaming Source
    src_in            : in  t_dp_siso := c_dp_siso_rdy;
    src_out           : out t_dp_sosi;

    -- Frame control
    frm_discard       : in  std_logic := '0';
    frm_discard_val   : in  std_logic := '1';

    -- Header info
    hdr_words_arr     : out t_network_total_header_32b_arr;
    hdr_words_arr_val : out std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    hdr_fields        : out t_network_total_header;
    hdr_fields_val    : out std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    hdr_data          : out std_logic_vector(c_word_w - 1 downto 0);
    hdr_data_val      : out std_logic;
    hdr_status          : out t_eth_hdr_status;
    hdr_status_complete : out std_logic
  );
end eth_hdr;

architecture str of eth_hdr is
  -- Internal sink ready latency and source ready latency of this component
  constant c_this_snk_latency : natural := 1;
  constant c_this_src_latency : natural := 1;

  signal snk_in_word_cnt     : natural range 0 to c_network_total_header_32b_nof_words;

  -- Extract total header
  signal i_hdr_words_arr     : t_network_total_header_32b_arr;
  signal i_hdr_words_arr_val : std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
  signal i_hdr_fields        : t_network_total_header;
  signal i_hdr_fields_val    : std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
  signal i_hdr_data          : std_logic_vector(c_word_w - 1 downto 0);
  signal i_hdr_data_val      : std_logic;
  signal i_hdr_status        : t_eth_hdr_status;
begin
  -- Make all header info available
  hdr_words_arr     <= i_hdr_words_arr;
  hdr_words_arr_val <= i_hdr_words_arr_val;
  hdr_fields        <= i_hdr_fields;
  hdr_fields_val    <= i_hdr_fields_val;
  hdr_data          <= i_hdr_data;
  hdr_data_val      <= i_hdr_data_val;
  hdr_status        <= i_hdr_status;

  ------------------------------------------------------------------------------
  -- IP header checksum control
  ------------------------------------------------------------------------------

  gen_ctrl : if g_header_store_and_forward = true generate
    -- Replace IP header checksum with the calculated checksum in case of IPv4, else pass on unchanged but discard frames shorter than 11 words
    u_ip_hdr_ctrl : entity work.eth_hdr_ctrl
    port map (
      -- Clocks and reset
      rst             => rst,
      clk             => clk,

      -- Stored header
      hdr_words_arr   => i_hdr_words_arr,
      hdr_status      => i_hdr_status,

      frm_discard     => frm_discard,
      frm_discard_val => frm_discard_val,

      -- ST interface
      snk_in_word_cnt => snk_in_word_cnt,
      snk_in          => snk_in,
      snk_out         => snk_out,
      src_in          => src_in,
      src_out         => src_out
    );
  end generate;

  no_ctrl : if g_header_store_and_forward = false generate
    -- Avoid the store and forward logic of eth_hdr_ctrl if it is not needed, although it would work OK to use it
    src_out <= snk_in;
    snk_out <= src_in;
  end generate;

  ------------------------------------------------------------------------------
  -- Extract total header
  ------------------------------------------------------------------------------

  -- Store 11 header words
  u_hdr_store : entity work.eth_hdr_store
  port map (
    rst               => rst,
    clk               => clk,
    -- Streaming Sink
    snk_in            => snk_in,
    snk_in_word_cnt   => snk_in_word_cnt,
    -- Total header
    hdr_words_arr     => i_hdr_words_arr,
    hdr_words_arr_val => i_hdr_words_arr_val,
    hdr_fields        => i_hdr_fields,
    hdr_fields_val    => i_hdr_fields_val,
    hdr_data          => i_hdr_data,
    hdr_data_val      => i_hdr_data_val
  );

  -- Determine header status
  u_hdr_status : entity work.eth_hdr_status
  generic map (
    g_ip_header_checksum_calculate => g_ip_header_checksum_calculate
  )
  port map (
    rst               => rst,
    clk               => clk,
    -- Total header
    hdr_words_arr     => i_hdr_words_arr,
    hdr_words_arr_val => i_hdr_words_arr_val,
    hdr_fields        => i_hdr_fields,
    hdr_fields_val    => i_hdr_fields_val,
    hdr_data          => i_hdr_data,
    hdr_data_val      => i_hdr_data_val,
    -- Header status
    hdr_status          => i_hdr_status,
    hdr_status_complete => hdr_status_complete
  );
end str;
