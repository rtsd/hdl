--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Concat two frames into one frame.
-- Description:
--   When both frames are present at the snk_in_arr ports, then the output frame
--   starts with the head frame from sink port 0 and concatenates it with the
--   tail frame from sink port 1.
-- Remark:
-- . Both frames must have the same data width and symbol width.
-- . The concatenation is done at symbol level and the output frame will have
--   the aggregated empty.
-- . The concatenated frame gets the channel and error field of the tail frame
--   from sink port 1.
-- . The ready control can easily result in combinatorial loop errors (the loop
--   does not end in a stable state) or synthesis warning. To avoid that:
--   - when src_in.ready = '1' then use c_dp_siso_rdy instead of src_in for
--     flow control assignments to avoid combinatorial loop warning at
--     synthesis
--   - do not use next_src_out_arr in conditional statements but explicitely
--     use pend_src_out_arr AND src_in.ready
--
-- Design steps that were taken for dp_concat (took about 3 days):
--   1 aim is a component that can add a header to a frame
--   2 generalize to a streaming component that can concat two arbitrary frames
--   3 support c_nof_symbols_per_data >= 1
--   4 draft dp_concat that uses dp_hold_input and implements the p_state state machine, key aspects are:
--     . it is not necessary to use dp_shiftreg, using the combination of dp_hold_input and prev_tail is appropriate
--     . state s_sop introduces an idle output cycle, this is no limitation, but makes the state machine easier
--     . states s_head, s_tail and s_eop do not introduce extra output idle cycles, this ensures that the output frame remains
--       active if possible (dependent on snk_in_arr().valid and src_in.ready). It also makes the statemachine more complex and
--       makes it necessary to independently register the hold input streams in src_out_buf_arr in addition to i_src_out. Note that
--       this is different in dp_mux, because dp_mux uses i_src_out for all hold inputs, which causes an idle cycle between
--       dp_mux output frames.
--     . support head and or tail frame with only one symbol
--   5 made proc_dp_gen_frame(), func_dp_data_init() and func_dp_data_incr() to generate frames with symbol data
--   6 made test bench tb_dp_concat and test with ready and valid both active for various head and tail frame lengths
--   7 made tb_dp_concat self checking for data for c_nof_symbols_per_data = 1
--   8 test with ready sometimes active and sometimes inactive using random function from diag module
--   9 test with input valid some times inactive when ready is active using random function from diag module
--  10 made tb_dp_concat self checking for data for c_nof_symbols_per_data > 1
--  11 iterate these steps until the dp_concat works OK

entity dp_concat is
  generic (
    g_data_w      : natural := 16;  -- >= 1
    g_symbol_w    : natural := 8  -- >= 1, and g_data_w/g_symbol_w must be an integer
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(0 to 1);
    snk_in_arr  : in  t_dp_sosi_arr(0 to 1);
    -- ST source
    src_in      : in  t_dp_siso;
    src_out     : out t_dp_sosi
  );
end dp_concat;

architecture rtl of dp_concat is
  constant c_nof_symbols_per_data  : natural := g_data_w / g_symbol_w;  -- nof symbols per data
  constant c_empty_w               : natural := ceil_log2(c_nof_symbols_per_data);
  constant c_bsn_w                 : natural := c_dp_stream_bsn_w;
  constant c_channel_w             : natural := c_dp_stream_channel_w;

  constant c_head                  : natural := 0;  -- head frame snk_in_arr port number
  constant c_tail                  : natural := 1;  -- tail frame snk_in_arr port number

  type t_state is (s_sop, s_head, s_tail, s_eop);

  signal state               : t_state;
  signal nxt_state           : t_state;

  signal i_snk_out_arr       : t_dp_siso_arr(0 to 1);
  signal flush_arr           : t_dp_siso_arr(0 to 1);
  signal hold_src_in_arr     : t_dp_siso_arr(0 to 1);
  signal next_src_out_arr    : t_dp_sosi_arr(0 to 1);
  signal pend_src_out_arr    : t_dp_sosi_arr(0 to 1);
  signal src_out_buf_arr     : t_dp_sosi_arr(0 to 1);
  signal nxt_src_out_buf_arr : t_dp_sosi_arr(0 to 1);

  signal head_bsn            : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_head_bsn        : std_logic_vector(c_bsn_w - 1 downto 0);
  signal head_channel        : std_logic_vector(c_channel_w - 1 downto 0);
  signal nxt_head_channel    : std_logic_vector(c_channel_w - 1 downto 0);
  signal head_empty          : std_logic_vector(c_empty_w - 1 downto 0);
  signal head_empty_reg      : std_logic_vector(c_empty_w - 1 downto 0);
  signal nxt_head_empty_reg  : std_logic_vector(c_empty_w - 1 downto 0);
  signal tail_empty          : std_logic_vector(c_empty_w - 1 downto 0);
  signal out_empty_reg       : std_logic_vector(c_empty_w - 1 downto 0);
  signal nxt_out_empty_reg   : std_logic_vector(c_empty_w - 1 downto 0);

  signal prev_tail           : t_dp_sosi;
  signal nxt_prev_tail       : t_dp_sosi;

  signal i_src_out           : t_dp_sosi;
  signal nxt_src_out         : t_dp_sosi;
begin
  assert (g_data_w mod g_symbol_w) = 0
    report "dp_concat : g_data_w/g_symbol_w must be an integer"
    severity FAILURE;

  snk_out_arr <= i_snk_out_arr;
  src_out <= i_src_out;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      state           <= s_sop;
      head_bsn        <= (others => '0');
      head_channel    <= (others => '0');
      head_empty_reg  <= (others => '0');
      out_empty_reg   <= (others => '0');
      prev_tail       <= c_dp_sosi_rst;
      src_out_buf_arr <= (others => c_dp_sosi_rst);
      i_src_out       <= c_dp_sosi_rst;
    elsif rising_edge(clk) then
      state           <= nxt_state;
      head_bsn        <= nxt_head_bsn;
      head_channel    <= nxt_head_channel;
      head_empty_reg  <= nxt_head_empty_reg;
      out_empty_reg   <= nxt_out_empty_reg;
      prev_tail       <= nxt_prev_tail;
      src_out_buf_arr <= nxt_src_out_buf_arr;
      i_src_out       <= nxt_src_out;
    end if;
  end process;

  gen_input : for I in c_head to c_tail generate
    -- Hold the sink input to be able to register the source output
    u_hold : entity work.dp_hold_input
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => OPEN,
      snk_in       => snk_in_arr(I),
      -- ST source
      src_in       => hold_src_in_arr(I),
      next_src_out => next_src_out_arr(I),
      pend_src_out => pend_src_out_arr(I),
      src_out_reg  => src_out_buf_arr(I)
    );
  end generate;

  -- default ready for hold input when ready for sink input or also ready for hold input when the eop is there
  hold_src_in_arr(c_head).ready <= i_snk_out_arr(c_head).ready or flush_arr(c_head).ready;
  hold_src_in_arr(c_tail).ready <= i_snk_out_arr(c_tail).ready or flush_arr(c_tail).ready;

  -- Hold the two input streams in two separate buffer registers
  nxt_src_out_buf_arr <= next_src_out_arr;

  -- Keep track of head empty and final output empty
  head_empty <= pend_src_out_arr(c_head).empty(c_empty_w - 1 downto 0);
  tail_empty <= pend_src_out_arr(c_tail).empty(c_empty_w - 1 downto 0);

  p_state : process(state, src_in, i_src_out, pend_src_out_arr, prev_tail, head_bsn, head_channel, head_empty, head_empty_reg, tail_empty, out_empty_reg)
    variable v_out_empty : std_logic_vector(c_empty_w - 1 downto 0);
  begin
    i_snk_out_arr <= (others => c_dp_siso_rst);  -- default not ready for input
    flush_arr     <= (others => c_dp_siso_rst);  -- default not ready to flush_arr hold input

    nxt_src_out       <= i_src_out;  -- default no output
    nxt_src_out.empty <= TO_DP_EMPTY(0);
    nxt_src_out.sync  <= '0';
    nxt_src_out.valid <= '0';
    nxt_src_out.sop   <= '0';
    nxt_src_out.eop   <= '0';

    nxt_head_bsn       <= head_bsn;
    nxt_head_channel   <= head_channel;
    nxt_head_empty_reg <= head_empty_reg;
    nxt_out_empty_reg  <= out_empty_reg;
    v_out_empty        := out_empty_reg;

    nxt_prev_tail      <= prev_tail;

    nxt_state <= state;

    case state is
      when s_sop =>
        nxt_head_empty_reg <= (others => '0');
        nxt_out_empty_reg  <= (others => '0');

        -- Wait for pair of frames to have arrived and then concatenate them independent of their arrival order
        if pend_src_out_arr(c_head).sop = '0' then i_snk_out_arr(c_head) <= src_in; end if;
        if pend_src_out_arr(c_tail).sop = '0' then i_snk_out_arr(c_tail) <= src_in; end if;
        if pend_src_out_arr(c_head).sop = '1' and pend_src_out_arr(c_tail).sop = '1' then
          nxt_head_bsn     <= pend_src_out_arr(c_head).bsn;  -- keep head bsn during frame, the sync is defined at sop, so no need to keep sync during frame
          nxt_head_channel <= pend_src_out_arr(c_head).channel;  -- keep head channel during frame
          nxt_state <= s_head;
        end if;

      when s_head =>
        -- Begin or continue outputing the head frame

        -- Input ready contol during head frame
        if pend_src_out_arr(c_head).eop = '0' then
          i_snk_out_arr(c_head) <= src_in;  -- head request until the eop has arrived
        else
          flush_arr(c_head)     <= src_in;  -- also ready for hold input when the head eop is there
        end if;

        if src_in.ready = '1' then
          -- Data output
          nxt_src_out <= pend_src_out_arr(c_head);  -- head output (default data and control)
          if pend_src_out_arr(c_head).eop = '1' then
            -- Concatenate last head data with the first tail data if the head has empty symbols, use head for control
            nxt_src_out <= func_dp_data_shift_first(pend_src_out_arr(c_head), pend_src_out_arr(c_tail), g_symbol_w, c_nof_symbols_per_data, TO_UINT(head_empty));
          end if;
          nxt_src_out.empty <= TO_DP_EMPTY(0);  -- default keep output empty 0 when it is not known yet

          -- Input ready contol at end of head frame
          if pend_src_out_arr(c_head).eop = '1' then
            -- End of head frame will be output
            nxt_head_empty_reg <= head_empty;  -- capture the last head empty field (needed in s_tail and s_eop)
            nxt_prev_tail      <= pend_src_out_arr(c_tail);  -- capture the first tail data

            -- Begin the tail frame
            nxt_src_out.eop <= '0';  -- default assume the head eop will not be the output eop
            if pend_src_out_arr(c_tail).eop = '0' then  -- use pend_src_out_arr because it shows the active control independent of hold_scr_in().ready
              i_snk_out_arr(c_tail) <= c_dp_siso_rdy;  -- tail request until the eop has arrived
              nxt_state <= s_tail;
            else
              -- Support tail frame of 1 word
              if TO_UINT(head_empty) + TO_UINT(tail_empty) >= c_nof_symbols_per_data then  -- Output frame also fits in 1 word
                nxt_src_out.empty <= RESIZE_DP_EMPTY(func_dp_empty_concat(head_empty, tail_empty, c_nof_symbols_per_data));
                nxt_src_out.eop   <= '1';  -- output eop
                i_snk_out_arr <= (others => c_dp_siso_rdy);  -- request next pair of frames
                nxt_state <= s_sop;
              else  -- Output frame needs 2 words
                flush_arr(c_tail) <= c_dp_siso_rdy;  -- also ready for hold input when the tail eop is there
                v_out_empty := func_dp_empty_concat(head_empty_reg, tail_empty, c_nof_symbols_per_data);
                nxt_out_empty_reg <= v_out_empty;  -- capture the output empty field (needed in s_eop)
                nxt_state <= s_eop;
              end if;
            end if;
          end if;
        end if;

      when s_tail =>
        -- Begin, continue or end outputing the tail frame

        -- Input ready contol during tail frame
        if pend_src_out_arr(c_tail).eop = '0' then
          i_snk_out_arr(c_tail) <= src_in;  -- tail request until the eop has arrived
        else
          flush_arr(c_tail)     <= src_in;  -- also ready for hold input when the tail eop is there
        end if;

        if src_in.ready = '1' then
          -- Data output
          if pend_src_out_arr(c_tail).valid = '1' then
            -- Tail data will be passed on when src_in was ready and snk_in_arr() is valid, but the data part needs shift if head_empty_reg /= 0
            nxt_prev_tail <= pend_src_out_arr(c_tail);  -- capture the tail data
            -- Shift tail output if head had empty symbols, default prev_tail for head_empty_reg = 0
            nxt_src_out      <= func_dp_data_shift(prev_tail, pend_src_out_arr(c_tail), g_symbol_w, c_nof_symbols_per_data, TO_UINT(head_empty_reg));
          end if;
          nxt_src_out.empty <= TO_DP_EMPTY(0);  -- default keep output empty 0 when it is not known yet

          -- Input ready contol at end of tail frame
          if pend_src_out_arr(c_tail).eop = '1' then
            v_out_empty := func_dp_empty_concat(head_empty_reg, tail_empty, c_nof_symbols_per_data);
            nxt_out_empty_reg <= v_out_empty;  -- capture the output empty field (needed in s_eop)
            -- End of tail frame will be output
            if TO_UINT(head_empty_reg) + TO_UINT(tail_empty) >= c_nof_symbols_per_data then
              -- last tail frame data also fits in this output frame data
              nxt_src_out.empty <= RESIZE_DP_EMPTY(v_out_empty);
              i_snk_out_arr <= (others => c_dp_siso_rdy);  -- request next pair of frames
              nxt_state <= s_sop;
            else
              -- output frame needs one more word
              nxt_src_out.eop <= '0';  -- no eop yet
              nxt_state <= s_eop;
            end if;
          end if;
        end if;

        -- Surpress sop related fields from tail frame, use the sop info from the head frame
        nxt_src_out.sop     <= '0';
        nxt_src_out.sync    <= '0';
        nxt_src_out.bsn     <= head_bsn;
        nxt_src_out.channel <= head_channel;

      when others =>  -- s_eop
        -- Tail output last symbols (default inactive control, use data from prev_tail when src_in ready)
        if src_in.ready = '1' then
          -- Shift tail output if head had empty symbols, default prev_tail for head_empty_reg = 0
          nxt_src_out       <= func_dp_data_shift(prev_tail, prev_tail, g_symbol_w, c_nof_symbols_per_data, TO_UINT(head_empty_reg));
          nxt_src_out.empty <= RESIZE_DP_EMPTY(v_out_empty);
          i_snk_out_arr <= (others => c_dp_siso_rdy);  -- request next pair of frames
          nxt_state <= s_sop;
        end if;

        -- Surpress sop related fields from tail frame, use the sop info from the head frame
        nxt_src_out.sop     <= '0';
        nxt_src_out.sync    <= '0';
        nxt_src_out.bsn     <= head_bsn;
        nxt_src_out.channel <= head_channel;
    end case;

    -- Pass on frame level flow control
    i_snk_out_arr(0).xon <= src_in.xon;
    i_snk_out_arr(1).xon <= src_in.xon;
    flush_arr(0).xon     <= src_in.xon;
    flush_arr(1).xon     <= src_in.xon;
  end process;
end rtl;
