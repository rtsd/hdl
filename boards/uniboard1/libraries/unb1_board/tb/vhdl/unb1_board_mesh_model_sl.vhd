-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Model the links between the FN and BN in the UniBoard TR mesh at
--          serial transceiver STD_LOGIC level.
-- Description: See unb1_board_mesh_reorder_bidir.vhd

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;
use work.tb_unb1_board_pkg.all;

entity unb1_board_mesh_model_sl is
  generic (
    g_reorder   : boolean := true
  );
  port (
    -- FN to BN
    fn_tx_sl_3arr  : in  t_unb1_board_mesh_sl_3arr := (others => (others => (others => '0')));  -- _3arr = (node id 3,2,1,0)(bus id 3,2,1,0)(tr lane 3,2,1,0)
    bn_rx_sl_3arr  : out t_unb1_board_mesh_sl_3arr;

    -- BN to FN
    bn_tx_sl_3arr  : in  t_unb1_board_mesh_sl_3arr := (others => (others => (others => '0')));
    fn_rx_sl_3arr  : out t_unb1_board_mesh_sl_3arr
  );
end unb1_board_mesh_model_sl;

architecture beh of unb1_board_mesh_model_sl is
begin
  -- Functional mesh connect for transpose
  no_reorder : if g_reorder = false generate
    -- BN(i)(j) <= FN(j)(i)
    -- FN(i)(j) <= BN(j)(i)
    gen_i : for i in 0 to 3 generate
      gen_j : for j in 0 to 3 generate
        bn_rx_sl_3arr(i)(j) <= fn_tx_sl_3arr(j)(i);
        fn_rx_sl_3arr(i)(j) <= bn_tx_sl_3arr(j)(i);
      end generate;
    end generate;
  end generate;

  -- Actual UniBoard PCB mesh connect for transpose
  gen_reorder : if g_reorder = true generate
                                        -- BN, phy  <= FN, phy
    bn_rx_sl_3arr(0)(0) <= fn_tx_sl_3arr(3)(1);  -- 0,0 <= 3,1
    bn_rx_sl_3arr(0)(1) <= fn_tx_sl_3arr(2)(0);  -- 0,1 <= 2,0
    bn_rx_sl_3arr(0)(2) <= fn_tx_sl_3arr(1)(0);  -- 0,2 <= 1,0
    bn_rx_sl_3arr(0)(3) <= fn_tx_sl_3arr(0)(0);  -- 0,3 <= 0,0

    bn_rx_sl_3arr(1)(0) <= fn_tx_sl_3arr(3)(0);  -- 1,0 <= 3,0
    bn_rx_sl_3arr(1)(1) <= fn_tx_sl_3arr(2)(1);  -- 1,1 <= 2,1
    bn_rx_sl_3arr(1)(2) <= fn_tx_sl_3arr(1)(1);  -- 1,2 <= 1,1
    bn_rx_sl_3arr(1)(3) <= fn_tx_sl_3arr(0)(3);  -- 1,3 <= 0,3

    bn_rx_sl_3arr(2)(0) <= fn_tx_sl_3arr(3)(2);  -- 2,0 <= 3,2
    bn_rx_sl_3arr(2)(1) <= fn_tx_sl_3arr(2)(2);  -- 2,1 <= 2,2
    bn_rx_sl_3arr(2)(2) <= fn_tx_sl_3arr(0)(2);  -- 2,2 <= 0,2
    bn_rx_sl_3arr(2)(3) <= fn_tx_sl_3arr(1)(3);  -- 2,3 <= 1,3

    bn_rx_sl_3arr(3)(0) <= fn_tx_sl_3arr(3)(3);  -- 3,0 <= 3,3
    bn_rx_sl_3arr(3)(1) <= fn_tx_sl_3arr(0)(1);  -- 3,1 <= 0,1
    bn_rx_sl_3arr(3)(2) <= fn_tx_sl_3arr(2)(3);  -- 3,2 <= 2,3
    bn_rx_sl_3arr(3)(3) <= fn_tx_sl_3arr(1)(2);  -- 3,3 <= 1,2

                                         -- FN, phy <= BN, phy
    fn_rx_sl_3arr(0)(0) <= bn_tx_sl_3arr(0)(3);  -- 0,0 <= 0,3
    fn_rx_sl_3arr(0)(1) <= bn_tx_sl_3arr(3)(1);  -- 0,1 <= 3,1
    fn_rx_sl_3arr(0)(2) <= bn_tx_sl_3arr(2)(2);  -- 0,2 <= 2,2
    fn_rx_sl_3arr(0)(3) <= bn_tx_sl_3arr(1)(3);  -- 0,3 <= 1,3

    fn_rx_sl_3arr(1)(0) <= bn_tx_sl_3arr(0)(2);  -- 1,0 <= 0,2
    fn_rx_sl_3arr(1)(1) <= bn_tx_sl_3arr(1)(2);  -- 1,1 <= 1,2
    fn_rx_sl_3arr(1)(2) <= bn_tx_sl_3arr(3)(3);  -- 1,2 <= 3,3
    fn_rx_sl_3arr(1)(3) <= bn_tx_sl_3arr(2)(3);  -- 1,3 <= 2,3

    fn_rx_sl_3arr(2)(0) <= bn_tx_sl_3arr(0)(1);  -- 2,0 <= 0,1
    fn_rx_sl_3arr(2)(1) <= bn_tx_sl_3arr(1)(1);  -- 2,1 <= 1,1
    fn_rx_sl_3arr(2)(2) <= bn_tx_sl_3arr(2)(1);  -- 2,2 <= 2,1
    fn_rx_sl_3arr(2)(3) <= bn_tx_sl_3arr(3)(2);  -- 2,3 <= 3,2

    fn_rx_sl_3arr(3)(0) <= bn_tx_sl_3arr(1)(0);  -- 3,0 <= 1,0
    fn_rx_sl_3arr(3)(1) <= bn_tx_sl_3arr(0)(0);  -- 3,1 <= 0,0
    fn_rx_sl_3arr(3)(2) <= bn_tx_sl_3arr(2)(0);  -- 3,2 <= 2,0
    fn_rx_sl_3arr(3)(3) <= bn_tx_sl_3arr(3)(0);  -- 3,3 <= 3,0
  end generate;
end beh;
