###############################################################################
# Copyright 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
#
# Author: E. Kooistra, 11 May 2022
# Purpose: Synthesis of dp_bsn_align_v2 to investigate M20K BRAM usage
#
# Description:
# . Created dp_bsn_align_v2.qpf and qsf with GUI using run_quartus unb2c &
# . Use virtual pins so that it fits the FPGA, as explained in [1].
#
# References:
# [1] "Virtual Pin Assignments in a Partial Design", Apr 17, 2021,
#     https://www.youtube.com/watch?v=QET0lC-jdAQ

set_global_assignment -name TOP_LEVEL_ENTITY dp_bsn_align_v2
set_global_assignment -name ORIGINAL_QUARTUS_VERSION 19.4.0
set_global_assignment -name PROJECT_CREATION_TIME_DATE "13:49:06  MAY 11, 2022"
set_global_assignment -name LAST_QUARTUS_VERSION "19.4.0 Pro Edition"
set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
set_global_assignment -name DEVICE 10AX115N2F45E2SG
set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 2
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"

# Used GUI to add the qip files that dp_bsn_align_v2 depends on:
set_global_assignment -name QIP_FILE ../../ip_arria10_e2sg_ram/ip_arria10_e2sg_ram_lib.qip
set_global_assignment -name QIP_FILE ../../tech_mult/tech_mult_lib.qip
set_global_assignment -name QIP_FILE ../../common_mult/common_mult_lib.qip
set_global_assignment -name QIP_FILE ../../tech_iobuf/tech_iobuf_lib.qip
set_global_assignment -name QIP_FILE ../../mm/mm_lib.qip
set_global_assignment -name QIP_FILE ../../tech_fifo/tech_fifo_lib.qip
set_global_assignment -name QIP_FILE ../../easics/easics_lib.qip
set_global_assignment -name QIP_FILE ../../technology/technology_lib.qip
set_global_assignment -name QIP_FILE ../../tech_memory/tech_memory_lib.qip
set_global_assignment -name QIP_FILE ../../common/common_lib.qip
set_global_assignment -name QIP_FILE ../dp_lib.qip

# Used GUI to add the ports of dp_bsn_align_v2 as virtual pins so that it fits the FPGA [1].
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].eop
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].sop
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].sync
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].valid
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].eop
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].sop
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].sync
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].valid
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[0].rdval
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[0].waitrequest
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[1].rdval
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[1].waitrequest
set_instance_assignment -name VIRTUAL_PIN ON -to mm_copi.rd
set_instance_assignment -name VIRTUAL_PIN ON -to mm_copi.wr
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.eop
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.sop
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.sync
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.valid
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].eop
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].sop
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].sync
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].valid
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].eop
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].sop
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].sync
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].valid
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].bsn
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].channel
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].data
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].empty
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].err
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].im
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[0].re
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].bsn
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].channel
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].data
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].empty
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].err
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].im
set_instance_assignment -name VIRTUAL_PIN ON -to in_sosi_arr[1].re
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[0].rddata
set_instance_assignment -name VIRTUAL_PIN ON -to mm_cipo_arr[1].rddata
set_instance_assignment -name VIRTUAL_PIN ON -to mm_copi.address
set_instance_assignment -name VIRTUAL_PIN ON -to mm_copi.wrdata
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.bsn
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.channel
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.data
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.empty
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.err
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.im
set_instance_assignment -name VIRTUAL_PIN ON -to mm_sosi.re
set_instance_assignment -name VIRTUAL_PIN ON -to node_index
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].bsn
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].channel
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].data
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].empty
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].err
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].im
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[0].re
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].bsn
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].channel
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].data
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].empty
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].err
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].im
set_instance_assignment -name VIRTUAL_PIN ON -to out_sosi_arr[1].re
set_instance_assignment -name VIRTUAL_PIN ON -to stream_en_arr
set_instance_assignment -name VIRTUAL_PIN ON -to stream_replaced_cnt_arr
set_instance_assignment -name VIRTUAL_PIN ON -to dp_rst
