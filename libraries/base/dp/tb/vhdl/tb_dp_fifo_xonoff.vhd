-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Date: 26 Apr 2023
-- Purpose:
-- . Test bench for dp_fifos to verify xon flow control when FIFO runs full.
--   The dp_xonoff will discard blocks when its out_siso.xon = '0', to avoid
--   downstream overflow. The dp_xonoff needs to be applied when the updstream
--   source does not listen to flow control by siso.ready or by sisi.xon.
-- Description:
--   Verify that the dp_mux will not cause input FIFO overflow when the input
--   load is too large for the output capacity.
--
--             g_use_in_xonoff                g_use_out_xonoff
--   proc          |                              |
--   dp_gen --> xonoff --> fifo_sc --> mux --> xonoff --> fifo <-- out_siso
--   frame   |          |  arr      |       |          |  fill
--           |          |           |       |          |  sc
--           |          |           |       |          |
--       bg_sosi_arr    |           |       |          |
--             fifo_in_sosi_arr     |   mux_out_sosi   |
--             fifo_in_siso_arr     |   mux_in_siso    |
--                            mux_in_sosi_arr      fifo_fill_in_sosi
--                            mux_in_siso_arr      fifo_fill_in_siso
--
--   Tb stimuli:
--   . Enable input in_en after reset for 4*c_tb_nof_clk_cycles and disable it
--     before tb_end, to read FIFOs empty in c_tb_nof_clk_cycles until tb_end.
--   . Make external siso.xon = '0' for c_tb_nof_clk_cycles, to check xon flow
--     control.
--                    ______________________________________
--   in_en        ___|                                      |_________
--                _____________________            ___________________
--   out_siso.xon                      |__________|
--
--   tb_end       ___________________________________________________|
--
-- Remark:
-- . If g_gap_size is too small to fit g_nof_inputs on a multiplexed output,
--   then g_use_in_xonoff needs to be TRUE to avoid u_in_fifo overflow.
-- . It appears that the dp_xonoff at the mux output is not needed
--   (g_use_out_xonoff = FALSE), because siso.ready or siso.xon at dp_xonoff
--   of BG outputs can provide sufficient flow control to avoid u_out_fifo
--   overflow.
-- Usage:
-- . as 12
-- . run -all
-- Observe in Wave Window:
-- . near BG inputs:
--   - fifo_in_siso_arr
--   - dp_out_en_arr, to view dp_xonoff flushing
--   - in_fifo_usedw_arr in analogue format
-- . at MUX output:
--   - fifo_fill_in_siso
--   - dp_out_en, to view dp_xonoff flushing
--   - out_fifo_usedw in analogue format
-- Self test result is OK when there is no FAILURE due to FIFO overflow.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_fifo_xonoff is
  generic (
    g_nof_inputs     : natural := 2;
    g_nof_blocks     : natural := 500;
    g_block_size     : natural := 10;
    g_gap_size       : natural := 0;
    g_use_in_xonoff  : boolean := true;
    g_use_out_xonoff : boolean := false
  );
end tb_dp_fifo_xonoff;

architecture tb of tb_dp_fifo_xonoff is
  constant c_clk_period         : time := 5 ns;

  constant c_tb_nof_clk_cycles  : natural := g_nof_blocks * g_block_size;

  -- Use extrnal FIFO for per input of dp_mux
  -- c_fifo_af_ready >= 4, nof words below max (full) at which fifo is considered almost full for snk_out.ready
  -- c_fifo_af_xon   >= 0, nof words below max (full) at which fifo is considered almost full for snk_out.xon
  constant c_fifo_af_ready      : natural := 4;
  constant c_fifo_af_xon        : natural := g_block_size + 50;

  constant c_in_fifo_size       : natural := g_block_size * 20;

  constant c_out_fifo_size      : natural := g_block_size * 20;
  constant c_out_fifo_fill      : natural := g_block_size;

  constant c_ready_latency      : natural := 1;
  constant c_data_w             : natural := 16;
  constant c_symbol_w           : natural := c_data_w;
  constant c_symbol_init        : natural := 0;
  constant c_nof_symbols        : natural := g_block_size;
  constant c_bsn                : natural := 0;
  constant c_sync               : std_logic := '0';

  constant c_nof_input_w        : natural := ceil_log2(g_nof_inputs);

  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';
  signal in_en             : std_logic := '0';
  signal tb_end            : std_logic := '0';

  signal bg_siso           : t_dp_siso := c_dp_siso_rdy;
  signal bg_sosi_arr       : t_dp_sosi_arr(0 to g_nof_inputs - 1);
  signal dp_out_en_arr     : std_logic_vector(0 to g_nof_inputs - 1);
  signal fifo_in_siso_arr  : t_dp_siso_arr(0 to g_nof_inputs - 1);
  signal fifo_in_sosi_arr  : t_dp_sosi_arr(0 to g_nof_inputs - 1);
  signal mux_in_siso_arr   : t_dp_siso_arr(0 to g_nof_inputs - 1);
  signal mux_in_sosi_arr   : t_dp_sosi_arr(0 to g_nof_inputs - 1);

  signal mux_out_siso      : t_dp_siso := c_dp_siso_rdy;
  signal mux_out_sosi      : t_dp_sosi;
  signal dp_out_en         : std_logic;
  signal fifo_fill_in_siso : t_dp_siso := c_dp_siso_rdy;
  signal fifo_fill_in_sosi : t_dp_sosi;
  signal out_siso          : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi          : t_dp_sosi;

  -- Monitor FIFO filling
  type t_in_fifo_usedw_slv_arr is array (integer range <>) of std_logic_vector(ceil_log2(c_out_fifo_size) - 1 downto 0);

  signal in_fifo_wr_ful_arr  : std_logic_vector(0 to g_nof_inputs - 1);
  signal in_fifo_usedw_arr   : t_in_fifo_usedw_slv_arr(0 to g_nof_inputs - 1);
  signal in_fifo_rd_emp_arr  : std_logic_vector(0 to g_nof_inputs - 1);
  signal out_fifo_wr_ful     : std_logic;
  signal out_fifo_usedw      : std_logic_vector(ceil_log2(c_out_fifo_size) - 1 downto 0);
  signal out_fifo_rd_emp     : std_logic;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------
  gen_bg_arr : for I in 0 to g_nof_inputs - 1 generate
    p_bg_arr : process
    begin
      while true loop
        proc_dp_gen_frame(c_ready_latency,
                          c_data_w,
                          c_symbol_w,
                          c_symbol_init,
                          c_nof_symbols,
                          c_bsn + I,  -- use bsn to identify the inputs
                          c_sync,
                          clk,
                          in_en,
                          bg_siso,
                          bg_sosi_arr(I));
        wait for g_gap_size * c_clk_period;
      end loop;
      -- Use WHILE LOOP and WAIT to avoid warning (vcom-1090) Possible infinite loop:
      -- Process contains no WAIT statement.
      wait;
    end process;

    gen_dp_xonoff : if g_use_in_xonoff generate
      u_dp_xonoff : entity work.dp_xonoff
      port map (
        rst         => rst,
        clk         => clk,
        -- Frame in
        in_siso     => OPEN,
        in_sosi     => bg_sosi_arr(I),
        -- Frame out
        out_siso    => fifo_in_siso_arr(I),
        out_sosi    => fifo_in_sosi_arr(I),
        out_en      => dp_out_en_arr(I)
      );
    end generate;

    no_dp_xonoff : if not g_use_in_xonoff generate
      fifo_in_sosi_arr <= bg_sosi_arr;
      dp_out_en_arr <= (others => '1');
    end generate;

    u_in_fifo : entity work.dp_fifo_sc
    generic map (
      g_data_w         => c_data_w,
      g_bsn_w          => c_nof_input_w,
      g_use_bsn        => true,  -- use bsn to identify the inputs
      g_use_ctrl       => true,  -- sop & eop
      g_fifo_size      => c_in_fifo_size,
      g_fifo_af_margin => c_fifo_af_ready,
      g_fifo_af_xon    => c_fifo_af_xon,
      g_fifo_rl        => c_ready_latency
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- Monitor FIFO filling
      wr_ful      => in_fifo_wr_ful_arr(I),
      usedw       => in_fifo_usedw_arr(I),
      rd_emp      => in_fifo_rd_emp_arr(I),
      -- ST sink
      snk_out     => fifo_in_siso_arr(I),  -- flush control via out_siso.xon
      snk_in      => fifo_in_sosi_arr(I),
      -- ST source
      src_in      => mux_in_siso_arr(I),
      src_out     => mux_in_sosi_arr(I)
    );
  end generate;

  -- Enable input after reset and disable it before tb_end, to read FIFOs empty
  in_en <= '0', '1' after c_clk_period * 17,
                '0' after 4 * c_tb_nof_clk_cycles * c_clk_period;

  -- Also verify toggling external siso.xon flow control
  out_siso.xon <= '1',
                  '0' after 2 * c_tb_nof_clk_cycles * c_clk_period,
                  '1' after 3 * c_tb_nof_clk_cycles * c_clk_period;

  -- End test
  tb_end <= '0', '1' after 5 * c_tb_nof_clk_cycles * c_clk_period;

  ------------------------------------------------------------------------------
  -- Multiplexer
  ------------------------------------------------------------------------------
  u_dp_mux : entity work.dp_mux
  generic map (
    g_nof_input   => g_nof_inputs,
    g_fifo_size   => array_init(1024, g_nof_inputs),  -- must match g_nof_input, even when g_use_fifo=FALSE
    g_fifo_fill   => array_init(   0, g_nof_inputs)  -- must match g_nof_input, even when g_use_fifo=FALSE
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => mux_in_siso_arr,
    snk_in_arr  => mux_in_sosi_arr,
    -- ST source
    src_in      => mux_out_siso,
    src_out     => mux_out_sosi
  );

  ------------------------------------------------------------------------------
  -- Output
  ------------------------------------------------------------------------------

  gen_dp_xonoff : if g_use_out_xonoff generate
    u_dp_xonoff : entity work.dp_xonoff
    port map (
      rst         => rst,
      clk         => clk,
      -- Frame in
      in_siso     => mux_out_siso,
      in_sosi     => mux_out_sosi,
      -- Frame out
      out_siso    => fifo_fill_in_siso,
      out_sosi    => fifo_fill_in_sosi,
      out_en      => dp_out_en
    );
  end generate;

  no_dp_xonoff : if not g_use_out_xonoff generate
    fifo_fill_in_sosi <= mux_out_sosi;
    mux_out_siso <= fifo_fill_in_siso;
    dp_out_en <= '1';
  end generate;

  u_out_fifo : entity work.dp_fifo_fill_sc
  --u_out_fifo : ENTITY work.dp_fifo_fill_eop_sc
  generic map (
    g_data_w         => c_data_w,
    g_bsn_w          => c_nof_input_w,
    g_use_bsn        => true,  -- use bsn to identify the inputs
    g_fifo_fill      => c_out_fifo_fill,
    g_fifo_size      => c_out_fifo_size,
    g_fifo_af_margin => c_fifo_af_ready,
    g_fifo_af_xon    => c_fifo_af_xon,
    g_fifo_rl        => c_ready_latency
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- Monitor FIFO filling
    wr_ful      => out_fifo_wr_ful,
    usedw       => out_fifo_usedw,
    rd_emp      => out_fifo_rd_emp,
    -- ST sink
    snk_out     => fifo_fill_in_siso,
    snk_in      => fifo_fill_in_sosi,
    -- ST source
    src_in      => out_siso,
    src_out     => out_sosi
  );
end tb;
