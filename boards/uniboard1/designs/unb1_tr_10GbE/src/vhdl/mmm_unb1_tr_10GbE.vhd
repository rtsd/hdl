--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, mm_lib, eth_lib, technology_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity mmm_unb1_tr_10GbE is
  generic (
    g_sim   : boolean := false
  );
  port (
    mm_clk                          : in  std_logic := '1';
    mm_rst                          : in  std_logic := '1';
    pout_wdi                        : out std_logic := '1';
    reg_wdi_mosi                    : out t_mem_mosi := c_mem_mosi_rst;
    reg_wdi_miso                    : in  t_mem_miso := c_mem_miso_rst;
    reg_unb_system_info_mosi        : out t_mem_mosi := c_mem_mosi_rst;
    reg_unb_system_info_miso        : in  t_mem_miso := c_mem_miso_rst;
    rom_unb_system_info_mosi        : out t_mem_mosi := c_mem_mosi_rst;
    rom_unb_system_info_miso        : in  t_mem_miso := c_mem_miso_rst;
    reg_unb_sens_mosi               : out t_mem_mosi := c_mem_mosi_rst;
    reg_unb_sens_miso               : in  t_mem_miso := c_mem_miso_rst;
    reg_ppsh_mosi                   : out t_mem_mosi := c_mem_mosi_rst;
    reg_ppsh_miso                   : in  t_mem_miso := c_mem_miso_rst;
    eth1g_mm_rst                    : out std_logic;
    eth1g_reg_interrupt             : in  std_logic;
    eth1g_ram_mosi                  : out t_mem_mosi := c_mem_mosi_rst;
    eth1g_ram_miso                  : in  t_mem_miso := c_mem_miso_rst;
    eth1g_reg_mosi                  : out t_mem_mosi := c_mem_mosi_rst;
    eth1g_reg_miso                  : in  t_mem_miso := c_mem_miso_rst;
    eth1g_tse_mosi                  : out t_mem_mosi := c_mem_mosi_rst;
    eth1g_tse_miso                  : in  t_mem_miso := c_mem_miso_rst;
    reg_diag_bg_mosi          : out t_mem_mosi := c_mem_mosi_rst;
    reg_diag_bg_miso          : in  t_mem_miso := c_mem_miso_rst;
    ram_diag_bg_mosi          : out t_mem_mosi := c_mem_mosi_rst;
    ram_diag_bg_miso          : in  t_mem_miso := c_mem_miso_rst;
    reg_mdio_0_mosi                 : out t_mem_mosi := c_mem_mosi_rst;
    reg_mdio_0_miso                 : in  t_mem_miso := c_mem_miso_rst;
    reg_mdio_1_mosi                 : out t_mem_mosi := c_mem_mosi_rst;
    reg_mdio_1_miso                 : in  t_mem_miso := c_mem_miso_rst;
    reg_mdio_2_mosi                 : out t_mem_mosi := c_mem_mosi_rst;
    reg_mdio_2_miso                 : in  t_mem_miso := c_mem_miso_rst;
    reg_dp_offload_rx_hdr_dat_mosi  : out t_mem_mosi := c_mem_mosi_rst;
    reg_dp_offload_rx_hdr_dat_miso  : in  t_mem_miso := c_mem_miso_rst;
    reg_dp_offload_tx_hdr_dat_mosi  : out t_mem_mosi := c_mem_mosi_rst;
    reg_dp_offload_tx_hdr_dat_miso  : in  t_mem_miso := c_mem_miso_rst;
    reg_tr_10gbe_mosi               : out t_mem_mosi := c_mem_mosi_rst;
    reg_tr_10gbe_miso               : in  t_mem_miso := c_mem_miso_rst;
    reg_tr_xaui_mosi                : out t_mem_mosi := c_mem_mosi_rst;
    reg_tr_xaui_miso                : in  t_mem_miso := c_mem_miso_rst;
    reg_bsn_monitor_mosi      : out t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_monitor_miso      : in  t_mem_miso := c_mem_miso_rst
  );
end entity mmm_unb1_tr_10GbE;

architecture str of mmm_unb1_tr_10GbE is
  signal mm_rst_n : std_logic;

  component qsys_unb1_tr_10GbE is
    port (
      eth1g_reg_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_bsn_monitor_readdata_export       : in  std_logic_vector(31 downto 0) := (others => '0');
      reset_in_reset_n                            : in  std_logic := '0';
      pio_system_info_address_export              : out std_logic_vector(4 downto 0);
      ram_diag_bg_write_export              : out std_logic;
      eth1g_tse_writedata_export                  : out std_logic_vector(31 downto 0);
      reg_mdio_1_clk_export                       : out std_logic;
      reg_tr_10gbe_write_export                   : out std_logic;
      eth1g_ram_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_ram_address_export                    : out std_logic_vector(9 downto 0);
      reg_dp_offload_rx_hdr_dat_clk_export        : out std_logic;
      eth1g_reg_writedata_export                  : out std_logic_vector(31 downto 0);
      reg_unb_sens_reset_export                   : out std_logic;
      reg_tr_xaui_write_export                    : out std_logic;
      eth1g_tse_address_export                    : out std_logic_vector(9 downto 0);
      ram_diag_bg_writedata_export          : out std_logic_vector(31 downto 0);
      reg_unb_sens_read_export                    : out std_logic;
      eth1g_tse_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_unb_sens_clk_export                     : out std_logic;
      reg_wdi_read_export                         : out std_logic;
      reg_dp_offload_rx_hdr_dat_write_export      : out std_logic;
      ram_diag_bg_read_export               : out std_logic;
      reg_dp_offload_rx_hdr_dat_read_export       : out std_logic;
      reg_tr_10gbe_read_export                    : out std_logic;
      reg_dp_offload_rx_hdr_dat_reset_export      : out std_logic;
      reg_unb_sens_readdata_export                : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_2_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => '0');
      pio_system_info_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_bsn_monitor_read_export           : out std_logic;
      reg_diag_bg_writedata_export          : out std_logic_vector(31 downto 0);
      reg_wdi_address_export                      : out std_logic;
      ram_diag_bg_reset_export              : out std_logic;
      pio_system_info_write_export                : out std_logic;
      reg_tr_10gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_diag_bg_clk_export                : out std_logic;
      rom_system_info_read_export                 : out std_logic;
      reg_bsn_monitor_reset_export          : out std_logic;
      reg_dp_offload_rx_hdr_dat_address_export    : out std_logic_vector(6 downto 0);
      reg_dp_offload_rx_hdr_dat_writedata_export  : out std_logic_vector(31 downto 0);
      reg_mdio_2_address_export                   : out std_logic_vector(2 downto 0);
      reg_mdio_1_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_1_read_export                      : out std_logic;
      reg_unb_sens_writedata_export               : out std_logic_vector(31 downto 0);
      reg_mdio_2_writedata_export                 : out std_logic_vector(31 downto 0);
      reg_diag_bg_address_export            : out std_logic_vector(2 downto 0);
      pio_system_info_reset_export                : out std_logic;
      reg_wdi_writedata_export                    : out std_logic_vector(31 downto 0);
      reg_bsn_monitor_address_export        : out std_logic_vector(6 downto 0);
      reg_dp_offload_tx_hdr_dat_readdata_export   : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_mm_rst_export                         : out std_logic;
      reg_bsn_monitor_clk_export            : out std_logic;
      reg_tr_10gbe_clk_export                     : out std_logic;
      reg_dp_offload_tx_hdr_dat_read_export       : out std_logic;
      rom_system_info_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_wdi_write_export                        : out std_logic;
      reg_bsn_monitor_write_export          : out std_logic;
      reg_tr_10gbe_address_export                 : out std_logic_vector(14 downto 0);
      pio_pps_writedata_export                    : out std_logic_vector(31 downto 0);
      eth1g_tse_waitrequest_export                : in  std_logic := '0';
      reg_mdio_0_writedata_export                 : out std_logic_vector(31 downto 0);
      reg_tr_10gbe_waitrequest_export             : in  std_logic := '0';
      eth1g_mm_clk_export                         : out std_logic;
      reg_mdio_0_reset_export                     : out std_logic;
      rom_system_info_address_export              : out std_logic_vector(9 downto 0);
      reg_mdio_0_address_export                   : out std_logic_vector(2 downto 0);
      reg_dp_offload_tx_hdr_dat_write_export      : out std_logic;
      ram_diag_bg_readdata_export           : in  std_logic_vector(31 downto 0) := (others => '0');
      pio_pps_address_export                      : out std_logic;
      pio_pps_reset_export                        : out std_logic;
      pio_pps_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_0_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => '0');
      pio_system_info_writedata_export            : out std_logic_vector(31 downto 0);
      reg_wdi_reset_export                        : out std_logic;
      reg_tr_xaui_writedata_export                : out std_logic_vector(31 downto 0);
      clk_in_clk                                  : in  std_logic := '0';
      rom_system_info_clk_export                  : out std_logic;
      reg_unb_sens_write_export                   : out std_logic;
      reg_dp_offload_rx_hdr_dat_readdata_export   : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_0_clk_export                       : out std_logic;
      eth1g_ram_write_export                      : out std_logic;
      reg_tr_xaui_address_export                  : out std_logic_vector(10 downto 0);
      reg_dp_offload_tx_hdr_dat_writedata_export  : out std_logic_vector(31 downto 0);
      reg_mdio_0_write_export                     : out std_logic;
      eth1g_ram_read_export                       : out std_logic;
      reg_diag_bg_write_export              : out std_logic;
      eth1g_reg_read_export                       : out std_logic;
      eth1g_tse_write_export                      : out std_logic;
      pio_pps_clk_export                          : out std_logic;
      reg_tr_xaui_waitrequest_export              : in  std_logic := '0';
      eth1g_reg_address_export                    : out std_logic_vector(3 downto 0);
      rom_system_info_writedata_export            : out std_logic_vector(31 downto 0);
      reg_tr_xaui_reset_export                    : out std_logic;
      reg_dp_offload_tx_hdr_dat_reset_export      : out std_logic;
      reg_dp_offload_tx_hdr_dat_address_export    : out std_logic_vector(5 downto 0);
      reg_mdio_2_read_export                      : out std_logic;
      reg_mdio_1_writedata_export                 : out std_logic_vector(31 downto 0);
      reg_mdio_2_reset_export                     : out std_logic;
      reg_tr_10gbe_writedata_export               : out std_logic_vector(31 downto 0);
      pio_pps_write_export                        : out std_logic;
      rom_system_info_write_export                : out std_logic;
      reg_dp_offload_tx_hdr_dat_clk_export        : out std_logic;
      reg_mdio_2_clk_export                       : out std_logic;
      eth1g_tse_read_export                       : out std_logic;
      reg_tr_xaui_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_ram_writedata_export                  : out std_logic_vector(31 downto 0);
      out_port_from_the_pio_debug_wave            : out std_logic_vector(31 downto 0);
      reg_mdio_1_address_export                   : out std_logic_vector(2 downto 0);
      reg_tr_xaui_read_export                     : out std_logic;
      ram_diag_bg_clk_export                : out std_logic;
      pio_system_info_read_export                 : out std_logic;
      reg_mdio_1_reset_export                     : out std_logic;
      reg_wdi_clk_export                          : out std_logic;

      ram_diag_bg_address_export            : out std_logic_vector(10 downto 0);
      reg_tr_10gbe_reset_export                   : out std_logic;
      reg_bsn_monitor_writedata_export      : out std_logic_vector(31 downto 0);
      out_port_from_the_pio_wdi                   : out std_logic;
      eth1g_reg_write_export                      : out std_logic;
      reg_mdio_1_write_export                     : out std_logic;
      reg_diag_bg_read_export               : out std_logic;
      reg_tr_xaui_clk_export                      : out std_logic;
      reg_wdi_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_2_write_export                     : out std_logic;
      pio_pps_read_export                         : out std_logic;
      pio_system_info_clk_export                  : out std_logic;
      reg_diag_bg_reset_export              : out std_logic;
      rom_system_info_reset_export                : out std_logic;
      reg_unb_sens_address_export                 : out std_logic_vector(2 downto 0);
      eth1g_irq_export                            : in  std_logic := '0';
      reg_diag_bg_readdata_export           : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_mdio_0_read_export                      : out std_logic
    );
  end component qsys_unb1_tr_10GbE;
begin
  gen_sim : if g_sim = true generate
    reg_wdi_mosi                    <= c_mem_mosi_rst;
    reg_unb_system_info_mosi        <= c_mem_mosi_rst;
    rom_unb_system_info_mosi        <= c_mem_mosi_rst;
    reg_unb_sens_mosi               <= c_mem_mosi_rst;
    reg_ppsh_mosi                   <= c_mem_mosi_rst;
    eth1g_ram_mosi                  <= c_mem_mosi_rst;
    reg_diag_bg_mosi          <= c_mem_mosi_rst;
    ram_diag_bg_mosi          <= c_mem_mosi_rst;
    reg_mdio_0_mosi                 <= c_mem_mosi_rst;
    reg_mdio_1_mosi                 <= c_mem_mosi_rst;
    reg_mdio_2_mosi                 <= c_mem_mosi_rst;
    reg_dp_offload_rx_hdr_dat_mosi  <= c_mem_mosi_rst;
    reg_dp_offload_tx_hdr_dat_mosi  <= c_mem_mosi_rst;
    reg_tr_10gbe_mosi               <= c_mem_mosi_rst;
    reg_tr_xaui_mosi                <= c_mem_mosi_rst;
    reg_bsn_monitor_mosi      <= c_mem_mosi_rst;
  end generate;

  ----------------------------------------------------------------------------
  -- SOPC or QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys_unb1_tr_10GbE : if g_sim = false generate
    mm_rst_n <= not(mm_rst);

    u_qsys_unb1_tr_10GbE : qsys_unb1_tr_10GbE
      port map(
      clk_in_clk                                  => mm_clk,
      eth1g_irq_export                            => eth1g_reg_interrupt,
      eth1g_mm_clk_export                         => OPEN,
      eth1g_mm_rst_export                         => eth1g_mm_rst,
      eth1g_ram_address_export                    => eth1g_ram_mosi.address(9 downto 0),
      eth1g_ram_read_export                       => eth1g_ram_mosi.rd,
      eth1g_ram_readdata_export                   => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      eth1g_ram_write_export                      => eth1g_ram_mosi.wr,
      eth1g_ram_writedata_export                  => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      eth1g_reg_address_export                    => eth1g_reg_mosi.address(3 downto 0),
      eth1g_reg_read_export                       => eth1g_reg_mosi.rd,
      eth1g_reg_readdata_export                   => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      eth1g_reg_write_export                      => eth1g_reg_mosi.wr,
      eth1g_reg_writedata_export                  => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      eth1g_tse_address_export                    => eth1g_tse_mosi.address(9 downto 0),
      eth1g_tse_read_export                       => eth1g_tse_mosi.rd,
      eth1g_tse_readdata_export                   => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      eth1g_tse_waitrequest_export                => eth1g_tse_miso.waitrequest,
      eth1g_tse_write_export                      => eth1g_tse_mosi.wr,
      eth1g_tse_writedata_export                  => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      out_port_from_the_pio_debug_wave            => OPEN,
      out_port_from_the_pio_wdi                   => pout_wdi,
      pio_pps_address_export                      => reg_ppsh_mosi.address(0),
      pio_pps_clk_export                          => OPEN,
      pio_pps_read_export                         => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                     => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      pio_pps_reset_export                        => OPEN,
      pio_pps_write_export                        => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                    => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_address_export              => reg_unb_system_info_mosi.address(4 downto 0),
      pio_system_info_clk_export                  => OPEN,
      pio_system_info_read_export                 => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export             => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      pio_system_info_reset_export                => OPEN,
      pio_system_info_write_export                => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export            => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_bg_address_export            => ram_diag_bg_mosi.address(10 downto 0),
      ram_diag_bg_clk_export                => OPEN,
      ram_diag_bg_read_export               => ram_diag_bg_mosi.rd,
      ram_diag_bg_readdata_export           => ram_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_reset_export              => OPEN,
      ram_diag_bg_write_export              => ram_diag_bg_mosi.wr,
      ram_diag_bg_writedata_export          => ram_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_address_export        => reg_bsn_monitor_mosi.address(6 downto 0),
      reg_bsn_monitor_clk_export            => OPEN,
      reg_bsn_monitor_read_export           => reg_bsn_monitor_mosi.rd,
      reg_bsn_monitor_readdata_export       => reg_bsn_monitor_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_reset_export          => OPEN,
      reg_bsn_monitor_write_export          => reg_bsn_monitor_mosi.wr,
      reg_bsn_monitor_writedata_export      => reg_bsn_monitor_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_address_export            => reg_diag_bg_mosi.address(2 downto 0),
      reg_diag_bg_clk_export                => OPEN,
      reg_diag_bg_read_export               => reg_diag_bg_mosi.rd,
      reg_diag_bg_readdata_export           => reg_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_reset_export              => OPEN,
      reg_diag_bg_write_export              => reg_diag_bg_mosi.wr,
      reg_diag_bg_writedata_export          => reg_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dp_offload_rx_hdr_dat_address_export    => reg_dp_offload_rx_hdr_dat_mosi.address(6 downto 0),
      reg_dp_offload_rx_hdr_dat_clk_export        => OPEN,
      reg_dp_offload_rx_hdr_dat_read_export       => reg_dp_offload_rx_hdr_dat_mosi.rd,
      reg_dp_offload_rx_hdr_dat_readdata_export   => reg_dp_offload_rx_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_rx_hdr_dat_reset_export      => OPEN,
      reg_dp_offload_rx_hdr_dat_write_export      => reg_dp_offload_rx_hdr_dat_mosi.wr,
      reg_dp_offload_rx_hdr_dat_writedata_export  => reg_dp_offload_rx_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_hdr_dat_address_export    => reg_dp_offload_tx_hdr_dat_mosi.address(5 downto 0),
      reg_dp_offload_tx_hdr_dat_clk_export        => OPEN,
      reg_dp_offload_tx_hdr_dat_read_export       => reg_dp_offload_tx_hdr_dat_mosi.rd,
      reg_dp_offload_tx_hdr_dat_readdata_export   => reg_dp_offload_tx_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_hdr_dat_reset_export      => OPEN,
      reg_dp_offload_tx_hdr_dat_write_export      => reg_dp_offload_tx_hdr_dat_mosi.wr,
      reg_dp_offload_tx_hdr_dat_writedata_export  => reg_dp_offload_tx_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mdio_0_address_export                   => reg_mdio_0_mosi.address(2 downto 0),
      reg_mdio_0_clk_export                       => OPEN,
      reg_mdio_0_read_export                      => reg_mdio_0_mosi.rd,
      reg_mdio_0_readdata_export                  => reg_mdio_0_miso.rddata(c_word_w - 1 downto 0),
      reg_mdio_0_reset_export                     => OPEN,
      reg_mdio_0_write_export                     => reg_mdio_0_mosi.wr,
      reg_mdio_0_writedata_export                 => reg_mdio_0_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mdio_1_address_export                   => reg_mdio_1_mosi.address(2 downto 0),
      reg_mdio_1_clk_export                       => OPEN,
      reg_mdio_1_read_export                      => reg_mdio_1_mosi.rd,
      reg_mdio_1_readdata_export                  => reg_mdio_1_miso.rddata(c_word_w - 1 downto 0),
      reg_mdio_1_reset_export                     => OPEN,
      reg_mdio_1_write_export                     => reg_mdio_1_mosi.wr,
      reg_mdio_1_writedata_export                 => reg_mdio_1_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mdio_2_address_export                   => reg_mdio_2_mosi.address(2 downto 0),
      reg_mdio_2_clk_export                       => OPEN,
      reg_mdio_2_read_export                      => reg_mdio_2_mosi.rd,
      reg_mdio_2_readdata_export                  => reg_mdio_2_miso.rddata(c_word_w - 1 downto 0),
      reg_mdio_2_reset_export                     => OPEN,
      reg_mdio_2_write_export                     => reg_mdio_2_mosi.wr,
      reg_mdio_2_writedata_export                 => reg_mdio_2_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_address_export                 => reg_tr_10gbe_mosi.address(14 downto 0),
      reg_tr_10gbe_clk_export                     => OPEN,
      reg_tr_10gbe_read_export                    => reg_tr_10gbe_mosi.rd,
      reg_tr_10gbe_readdata_export                => reg_tr_10gbe_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_reset_export                   => OPEN,
      reg_tr_10gbe_waitrequest_export             => reg_tr_10gbe_miso.waitrequest,
      reg_tr_10gbe_write_export                   => reg_tr_10gbe_mosi.wr,
      reg_tr_10gbe_writedata_export               => reg_tr_10gbe_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_xaui_address_export                  => reg_tr_xaui_mosi.address(10 downto 0),
      reg_tr_xaui_clk_export                      => OPEN,
      reg_tr_xaui_read_export                     => reg_tr_xaui_mosi.rd,
      reg_tr_xaui_readdata_export                 => reg_tr_xaui_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_xaui_reset_export                    => OPEN,
      reg_tr_xaui_waitrequest_export              => reg_tr_xaui_miso.waitrequest,
      reg_tr_xaui_write_export                    => reg_tr_xaui_mosi.wr,
      reg_tr_xaui_writedata_export                => reg_tr_xaui_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_address_export                 => reg_unb_sens_mosi.address(2 downto 0),
      reg_unb_sens_clk_export                     => OPEN,
      reg_unb_sens_read_export                    => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export                => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      reg_unb_sens_reset_export                   => OPEN,
      reg_unb_sens_write_export                   => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export               => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_address_export                      => reg_wdi_mosi.address(0),
      reg_wdi_clk_export                          => OPEN,
      reg_wdi_read_export                         => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                     => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      reg_wdi_reset_export                        => OPEN,
      reg_wdi_write_export                        => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                    => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reset_in_reset_n                            => mm_rst_n,
      rom_system_info_address_export              => rom_unb_system_info_mosi.address(9 downto 0),
      rom_system_info_clk_export                  => OPEN,
      rom_system_info_read_export                 => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export             => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      rom_system_info_reset_export                => OPEN,
      rom_system_info_write_export                => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export            => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
