-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker

-- Purpose:
-- . test bench for sdp_info.vhd (and sdp_info_reg.vhd)
-- Description:
--
-- https://plm.astron.nl/polarion/#/project/LOFAR2System/workitem?id=LOFAR2-9258
-- https://plm.astron.nl/polarion/#/project/LOFAR2System/workitem?id=LOFAR2-8855
--
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.sdp_pkg.all;

entity tb_sdp_info is
end tb_sdp_info;

architecture tb of tb_sdp_info is
  constant c_dp_clk_period        : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period        : time := 20 ns;  -- 50 MHz

  -- used mm_adresses on mm bus
  constant c_mm_addr_block_period            : natural := 0;
  constant c_mm_addr_beam_repositioning_flag : natural := 1;
  constant c_mm_addr_fsub_type               : natural := 2;
  constant c_mm_addr_f_adc                   : natural := 3;
  constant c_mm_addr_nyquist_zone_index      : natural := 4;
  constant c_mm_addr_observation_id          : natural := 5;
  constant c_mm_addr_antenna_band_index      : natural := 6;
  constant c_mm_addr_station_id              : natural := 7;
  constant c_mm_addr_antenna_field_index     : natural := 8;

  signal tb_end              : std_logic := '0';
  signal tb_mm_reg_end       : std_logic := '0';

  signal dp_clk              : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_rst              : std_logic;

  signal mm_clk              : std_logic := '1';  -- MM control clock = 50 MHz
  signal mm_rst              : std_logic;

  signal reg_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso            : t_mem_miso;

  -- signals used to change settings of sdp_info
  signal f_adc     : std_logic := '0';
  signal fsub_type : std_logic := '0';

  signal sdp_info  : t_sdp_info;

  -- signals used for response of mm bus
  signal mm_natural_response : natural;
begin
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  p_mm_reg_stimuli : process
  begin
    reg_mosi <= c_mem_mosi_rst;

    -- initialyze
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 100);

    -- default all register hold value 0, try to write 1 in all registers
    proc_mem_mm_bus_wr(c_mm_addr_block_period,           11, mm_clk, reg_miso, reg_mosi);  -- RO
    proc_mem_mm_bus_wr(c_mm_addr_beam_repositioning_flag, 1,  mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_fsub_type,              1,  mm_clk, reg_miso, reg_mosi);  -- RO
    proc_mem_mm_bus_wr(c_mm_addr_f_adc,                  1,  mm_clk, reg_miso, reg_mosi);  -- RO
    proc_mem_mm_bus_wr(c_mm_addr_nyquist_zone_index,     3,  mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_observation_id,         16, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_antenna_band_index,     1,  mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_station_id,             17, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_antenna_field_index,    15, mm_clk, reg_miso, reg_mosi);
    proc_common_wait_cross_clock_domain_latency(dp_clk, mm_clk);

    proc_mem_mm_bus_rd(c_mm_addr_block_period, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response /= 11
      report "Wrong block_period (not read only)"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_beam_repositioning_flag, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 1
      report "Wrong beam_repositioning_flag"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_fsub_type, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);  proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response /= 1
      report "Wrong fsub_type (not read only)"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_f_adc, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response /= 1
      report "Wrong f_adc (not read only)"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_nyquist_zone_index, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 3
      report "Wrong nyquist_zone_index"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_observation_id, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 16
      report "Wrong observation_id"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_antenna_band_index, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 1
      report "Wrong antenna_band_index"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_station_id, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 17
      report "Wrong station_id"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_antenna_field_index, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 15
      report "Wrong antenna_field_index"
      severity ERROR;

    -- check block_period if f_adc and fsub_type are changed
    -- f_adc = '0' and fsub_type = '0' => block_period = 6400
    -- f_adc = '1' and fsub_type = '0' => block_period = 5120
    -- f_adc = '0' and fsub_type = '1' => block_period = 5400
    -- f_adc = '1' and fsub_type = '1' => block_period = 4320
    f_adc <= '0'; fsub_type <= '0';
    proc_common_wait_cross_clock_domain_latency(dp_clk, mm_clk);
    proc_mem_mm_bus_rd(c_mm_addr_block_period, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata(15 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 6400
      report "wrong block_period, expected 6400"
      severity ERROR;
    f_adc <= '1'; fsub_type <= '0';
    proc_common_wait_cross_clock_domain_latency(dp_clk, mm_clk);
    proc_mem_mm_bus_rd(c_mm_addr_block_period, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata(15 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 5120
      report "wrong block_period, expected 5120"
      severity ERROR;
    f_adc <= '0'; fsub_type <= '1';
    proc_common_wait_cross_clock_domain_latency(dp_clk, mm_clk);
    proc_mem_mm_bus_rd(c_mm_addr_block_period, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata(15 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 5400
      report "wrong block_period, expected 5400"
      severity ERROR;
    f_adc <= '1'; fsub_type <= '1';
    proc_common_wait_cross_clock_domain_latency(dp_clk, mm_clk);
    proc_mem_mm_bus_rd(c_mm_addr_block_period, mm_clk, reg_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata(15 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 4320
      report "wrong block_period, expected 4320"
      severity ERROR;

    proc_common_wait_some_cycles(mm_clk, 100);
    tb_mm_reg_end <= '1';
    wait;
  end process;

  -- check if values in sdp_info match with expected values
  p_sdp_info_stimuli : process
  begin
    proc_common_wait_until_high(mm_clk, tb_mm_reg_end);  -- wait for p_mm_reg_stimuli done

    assert TO_UINT(sdp_info.block_period) = 4320
      report "wrong sdp_info.block_period value"
      severity ERROR;
    assert sdp_info.beam_repositioning_flag = '1'
      report "wrong sdp_info.beam_repositioning_flag value"
      severity ERROR;
    assert sdp_info.fsub_type = '1'
      report "wrong sdp_info.fsub_type value"
      severity ERROR;
    assert sdp_info.f_adc = '1'
      report "wrong sdp_info.f_adc value"
      severity ERROR;
    assert TO_UINT(sdp_info.nyquist_zone_index) = 3
      report "wrong sdp_info.nyquist_zone_index value"
      severity ERROR;
    assert TO_UINT(sdp_info.observation_id) = 16
      report "wrong sdp_info.observation_id value"
      severity ERROR;
    assert sdp_info.antenna_band_index = '1'
      report "wrong sdp_info.antenna_band_index value"
      severity ERROR;
    assert TO_UINT(sdp_info.station_id) = 17
      report "wrong sdp_info.station_id value"
      severity ERROR;
    assert TO_UINT(sdp_info.antenna_field_index) = 15
      report "wrong sdp_info.antenna_field_index value"
      severity ERROR;

    proc_common_wait_some_cycles(mm_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- SDP info
  u_dut: entity work.sdp_info
    port map (
      mm_clk    => mm_clk,
      mm_rst    => mm_rst,

      dp_clk    => dp_clk,
      dp_rst    => dp_rst,

      reg_mosi  => reg_mosi,
      reg_miso  => reg_miso,

      f_adc     => f_adc,
      fsub_type => fsub_type,

      sdp_info  => sdp_info
    );
end tb;
