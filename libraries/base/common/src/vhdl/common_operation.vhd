-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity common_operation is
  generic (
    g_operation       : string  := "MAX";  -- supported operations "MAX", "MIN"
    g_representation  : string  := "SIGNED";  -- or "UNSIGNED"
    g_pipeline_input  : natural := 0;  -- 0 or 1
    g_pipeline_output : natural := 1;  -- >= 0
    g_dat_w           : natural := 8
  );
  port (
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    in_a    : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_b    : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_en_a : in  std_logic := '1';
    in_en_b : in  std_logic := '1';
    result  : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end common_operation;

architecture rtl of common_operation is
  function func_default(operation, representation : string; w : natural) return std_logic_vector is
    constant c_smin : std_logic_vector(w - 1 downto 0) := '1' & c_slv0(w - 2 downto 0);
    constant c_umin : std_logic_vector(w - 1 downto 0) :=       c_slv0(w - 1 downto 0);
    constant c_smax : std_logic_vector(w - 1 downto 0) := '0' & c_slv1(w - 2 downto 0);
    constant c_umax : std_logic_vector(w - 1 downto 0) :=       c_slv1(w - 1 downto 0);
  begin
    -- return don't care default value
    if representation = "SIGNED" then
      if operation = "MIN" then return c_smax; end if;
      if operation = "MAX" then return c_smin; end if;
    else
      if operation = "MIN" then return c_umax; end if;
      if operation = "MAX" then return c_umin; end if;
    end if;
    assert true
      report "Operation not supported"
      severity FAILURE;
    return c_umin;  -- void return statement to avoid compiler warning on missing return
  end;

  function func_operation(operation, representation : string; a, b : std_logic_vector) return std_logic_vector is
  begin
    if representation = "SIGNED" then
      if operation = "MIN" then if signed(a) < signed(b) then return a; else return b; end if; end if;
      if operation = "MAX" then if signed(a) > signed(b) then return a; else return b; end if; end if;
    else
      if operation = "MIN" then if unsigned(a) < unsigned(b) then return a; else return b; end if; end if;
      if operation = "MAX" then if unsigned(a) > unsigned(b) then return a; else return b; end if; end if;
    end if;
    assert true
      report "Operation not supported"
      severity FAILURE;
    return a;  -- void return statement to avoid compiler warning on missing return
  end;

  signal nxt_a       : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_b       : std_logic_vector(g_dat_w - 1 downto 0);
  signal a           : std_logic_vector(g_dat_w - 1 downto 0);
  signal b           : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_result  : std_logic_vector(g_dat_w - 1 downto 0);
begin
  nxt_a <= in_a when in_en_a = '1' else func_default(g_operation, g_representation, g_dat_w);
  nxt_b <= in_b when in_en_b = '1' else func_default(g_operation, g_representation, g_dat_w);

  no_input_reg : if g_pipeline_input = 0 generate  -- wired input
    a <= nxt_a;
    b <= nxt_b;
  end generate;
  gen_input_reg : if g_pipeline_input > 0 generate  -- register input
    p_reg : process(clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          a <= nxt_a;
          b <= nxt_b;
        end if;
      end if;
    end process;
  end generate;

  nxt_result <= func_operation(g_operation, g_representation, a, b);

  u_output_pipe : entity work.common_pipeline  -- pipeline output
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline_output,  -- 0 for wires, >0 for register stages
    g_in_dat_w       => g_dat_w,
    g_out_dat_w      => g_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => nxt_result,
    out_dat => result
  );
end rtl;
