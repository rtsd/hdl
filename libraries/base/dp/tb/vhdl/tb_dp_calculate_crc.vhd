-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Date: 17 Apr 2023
-- Purpose:
-- . Test bench for dp_calculate_crc.
-- Description:
--   Verify that CRC is the same if the data in the blocks is the same, for
--   one specific combination of g_data_w, g_crc_w and c_nof_data_per_blk.
--   Assume that CRC calculation itself is OK, because it is used before.
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_calculate_crc is
  generic (
    g_data_w      : natural := 28;
    g_crc_w       : natural := 28;
    g_gap_size    : natural := 10
  );
end tb_dp_calculate_crc;

architecture tb of tb_dp_calculate_crc is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period   : time := 5 ns;

  constant c_nof_sync            : natural :=  1;
  constant c_nof_blk_per_sync    : natural :=  5;
  constant c_nof_data_per_blk    : natural :=  9;

  signal clk              : std_logic := '1';
  signal rst              : std_logic := '1';
  signal stimuli_end      : std_logic := '0';
  signal tb_end           : std_logic := '0';

  signal stimuli_sosi     : t_dp_sosi;
  signal snk_in           : t_dp_sosi;

  signal blk_crc          : std_logic_vector(g_crc_w - 1 downto 0);

  signal new_crc          : std_logic;

  -- Expected specific value obtained from simulation
  signal exp_crc_28       : std_logic_vector(28 - 1 downto 0) := X"ABDE9EB";
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => c_nof_blk_per_sync,
    g_nof_repeat  => c_nof_blk_per_sync * c_nof_sync,
    g_pkt_len     => c_nof_data_per_blk,
    g_pkt_gap     => g_gap_size
  )
  port map (
    rst           => rst,
    clk           => clk,

    -- Generate stimuli
    src_out       => stimuli_sosi,

    -- End of stimuli
    tb_end        => stimuli_end
  );

  -- Use same dat for every block to verify restart of CRC calculation
  p_snk_in : process(stimuli_sosi)
  begin
    snk_in      <= stimuli_sosi;
    snk_in.data <= TO_DP_DATA(TO_UINT(stimuli_sosi.data) mod c_nof_data_per_blk);
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_crc : entity work.dp_calculate_crc
  generic map (
    g_data_w => g_data_w,
    g_crc_w  => g_crc_w
  )
  port map (
    rst      => rst,
    clk      => clk,
    -- ST sink
    snk_in   => snk_in,
    blk_crc  => blk_crc
  );

  ------------------------------------------------------------------------------
  -- Verifycation
  ------------------------------------------------------------------------------
  new_crc <= snk_in.eop when rising_edge(clk);

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if new_crc = '1' then
        --IF g_data_w = 28 AND g_crc_w = 28 AND c_nof_data_per_blk = 9 THEN
          if blk_crc = exp_crc_28 then
            report "OK CRC value."
              severity NOTE;
          else
            report "Wrong CRC value."
              severity ERROR;
          end if;
        --END IF;
      end if;
    end if;
  end process;

  tb_end <= '0', stimuli_end after 10 * c_clk_period;
end tb;
