-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
--   Validate the length of a DP block.
-- Description:
-- The dp_block_validate_length.vhd checks whether the in_sosi block has the
-- expected length given by g_expected_length. The block length is defined by
-- the number of valid from sop to eop. The tasks of the
-- dp_block_validate_length.vhd are:
-- . Default all in_sosi fields are passed on to the out_sosi.
-- . If the input block length differs from g_expected_length, then the bit at
--   bit index g_err_bi in the out_sosi.err field is forced to 1, else the
--   out_sosi.err field passes on the in_sosi.err field.
-- . If the input block length > g_expected_length, then the out_sosi block
--   length is restricted to g_expected_length, by inserting an eop and
--   discarding the remaining data and eop information from the in_sosi.
-- Remarks:
-- - This component supports flow control and was designed by keeping the functional
--   state registers and the pipeline registers seperate. Therefore the function is
--   implemented using combinatorial logic and local state registers to keep its
--   state. The combinatorial function output preserves the snk_in ready latency and
--   is pipelined using dp_pipeline to ease timing closure on the output.
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

entity dp_block_validate_length is
  generic (
    g_err_bi          : natural := 0;  -- bit index in error field
    g_expected_length : natural := 255
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_block_validate_length;

architecture rtl of dp_block_validate_length is
  signal cnt_reg      : natural;
  signal cnt          : natural;
  signal block_sosi   : t_dp_sosi;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      cnt_reg      <= 0;
    elsif rising_edge(clk) then
      cnt_reg      <= cnt;
    end if;
  end process;

  -- Count valid per block
  p_cnt : process(snk_in, cnt_reg)
  begin
    cnt <= cnt_reg;
    if snk_in.sop = '1' then
      cnt <= 0;
    elsif snk_in.valid = '1' then
      cnt <= cnt_reg + 1;
    end if;
  end process;

  -- Resize snk_in combinatorially into block_sosi, so no impact on RL
  p_block_sosi : process(snk_in, cnt)
  begin
    -- Default keep snk_in info and data fields
    block_sosi       <= snk_in;
    if snk_in.valid = '1' then
      -- Set output eop, info @ eop gets lost if g_expected_length < actual block size
      if snk_in.eop = '1' xor cnt = g_expected_length - 1 then
        block_sosi.err(g_err_bi) <= '1';
      end if;

      if cnt = g_expected_length - 1 then
        block_sosi.eop   <= '1';
      end if;

      if cnt > g_expected_length - 1 then
        block_sosi <= c_dp_sosi_rst;
      end if;
    end if;
  end process;

  -- Register block_sosi to easy timing closure
  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => block_sosi,
    -- ST source
    src_in       => src_in,
    src_out      => src_out
  );
end rtl;
