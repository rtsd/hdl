-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Pipeline MM mosi
-- Description:
--   The mm_pipeline mosi registers the in_mosi if g_pipeline = TRUE, else it
--   defaults to wires.
--
-- Background information
--   The MM waitrequest resembles the behaviour of the streaming backpressure
--   ready for ready latency RL = 0. For RL = 0 the ready acts as an
--   acknowledge to pending data. For RL > 0 the ready acts as a request for
--   new data. The miso.waitrequest is defined for RL = 0 but for analysis
--   the timing diagrams below show an example of both RL = 0 and RL = 1. The
--   miso.waitrequest is equivalent to NOT sosi.ready.
--
--   * RL=1
--                 _   _   _   _   _   _   _   _   _   _   _   _
--     clk       _| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_
--
--     in_dat     |a  |b          |c          |d
--               _________         ___         ___
--     in_val             |_______|   |_______|   |_______________
--               _____         ___     _______         ___________
--     ready          |_______|   |___|...    |_______|...........
--               _________         ___     _______         _______
--     reg_ready          |_______|   |___|...    |_______|.......
--
--     reg_dat        |a  |b          |c          |d
--               _____________________________     ___________
--     reg_val                                |___|           |___
--               _________         ___     ___             ___
--     out_val        |a  |_______|b  |___|c  |___________|d  |___
--
--
--   * RL=0
--                 _   _   _   _   _   _   _   _   _   _   _   _   _
--     clk       _| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_
--
--     in_dat     |a  |b          |c      |d              |e
--               _________         _______________         ___
--     in_val             |_______|               |_______|   |_______
--               _____________         ___     _______     ___________
--     ack                    |_______|   |___|       |___|
--
--     reg_dat        |a  |b              |c      |d          |e
--               _____________             ___________         ___
--     reg_val                |___________|           |_______|   |___
--               _____________                 _______         ___
--     out_val        |a  |b  |_______________|c  |d  |_______|e  |___
--
--   In these timing diagrams the out_ready is wired to the in_ready, so
--   therefore they are identical and called ready.
--   The ready for RL = 0 or the reg_ready for RL = 1 is used to gate the
--   out_val. The ready/reg_ready is used and not the in_val, because by
--   using the ready/reg_ready the pipeline register is emptied as soon
--   as the ready is active, rather than to wait for a next in_val to push
--   it out. The ready/reg_ready have the same latency as the in_val,
--   because they are both derived using the same RL.
--
-- Remark:
-- * The mm_pipeline could be optimized regarding the miso.waitrequest flow
--   control if it would be implemented similar as dp_pipeline.vhd. This
--   involves using the pipeline register to accept an access when it is
--   empty. In this way the waitrequest to the in_mosi only needs to apply
--   when the out_miso is not ready and the pipeline is full. This would
--   achieve the maximum throughput. The advantage of simply registering
--   in_mosi and wiring in_miso is that it is simpler and does not put extra
--   logic into the combinatorial miso.waitrequest path. It is better to
--   keep it simpler and with less logic, then to try to win the last few
--   percent of throughput.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_pipeline is
  generic (
    g_pipeline    : boolean := true
  );
  port (
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;
    in_mosi       : in  t_mem_mosi;
    in_miso       : out t_mem_miso;
    out_mosi      : out t_mem_mosi;
    out_miso      : in  t_mem_miso
  );
end mm_pipeline;

architecture rtl of mm_pipeline is
  signal mosi_reg     : t_mem_mosi := c_mem_mosi_rst;
  signal nxt_mosi_reg : t_mem_mosi;
  signal ready        : std_logic;
begin
  -- Pass on miso
  in_miso <= out_miso;

  -- Pipeline the mosi when g_pipeline = TRUE, else default to wires
  gen_wires : if g_pipeline = false generate
    out_mosi <= in_mosi;
  end generate;

  gen_pipeline : if g_pipeline = true generate
    --p_reg : PROCESS(mm_rst, mosi_reg, mm_clk)   -- todo: check with synthesis that it is not necessary to have mosi_reg here
    p_reg : process(mm_rst, mm_clk)
    begin
      if mm_rst = '1' then
        mosi_reg <= RESET_MEM_MOSI_CTRL(mosi_reg);  -- todo: check with synthesis that mosi_reg data fields remain wires
      elsif rising_edge(mm_clk) then
        mosi_reg <= nxt_mosi_reg;
      end if;
    end process;

    ready <= not out_miso.waitrequest;

    nxt_mosi_reg <= in_mosi when ready = '1' else mosi_reg;

    p_out_mosi : process(mosi_reg, ready)
    begin
      out_mosi <= mosi_reg;
      if ready /= '1' then
        out_mosi.wr <= '0';  -- out_mosi.wr = mosi_reg.wr AND ready
        out_mosi.rd <= '0';  -- out_mosi.rd = mosi_reg.rd AND ready
      end if;
    end process;
  end generate;
end rtl;
