-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Tb for common_mult architectures
-- Description:
--   The tb is self verifying.
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_common_mult is
  generic (
    g_in_dat_w         : natural := 7;
    g_out_dat_w        : natural := 11;  -- = 2*g_in_dat_w, or smaller to truncate MSbits, or larger to extend MSbits
    g_nof_mult         : natural := 2;
    g_pipeline_input   : natural := 1;
    g_pipeline_product : natural := 1;
    g_pipeline_output  : natural := 1
  );
end tb_common_mult;

architecture tb of tb_common_mult is
  constant clk_period    : time := 10 ns;
  constant c_pipeline    : natural := g_pipeline_input + g_pipeline_product + g_pipeline_output;
  constant c_nof_mult    : natural := 2;  -- fixed

  constant c_max_p       : integer :=  2**(g_in_dat_w - 1) - 1;
  constant c_min         : integer := -c_max_p;
  constant c_max_n       : integer := -2**(g_in_dat_w - 1);

  constant c_technology  : natural := c_tech_select_default;

  function func_sresult(in_a, in_b : std_logic_vector) return std_logic_vector is
    constant c_res_w  : natural := 2 * g_in_dat_w;  -- use sufficiently large result width
    variable v_a      : std_logic_vector(g_in_dat_w - 1 downto 0);
    variable v_b      : std_logic_vector(g_in_dat_w - 1 downto 0);
    variable v_result : signed(c_res_w - 1 downto 0);
  begin
    -- Calculate expected result
    v_a      := RESIZE_SVEC(in_a, g_in_dat_w);
    v_b      := RESIZE_SVEC(in_b, g_in_dat_w);
    v_result := RESIZE_NUM(signed(v_a) * signed(v_b), c_res_w);
    return RESIZE_SVEC(std_logic_vector(v_result), g_out_dat_w);  -- Truncate MSbits or sign extend MSBits
  end;

  function func_uresult(in_a, in_b : std_logic_vector) return std_logic_vector is
    constant c_res_w  : natural := 2 * g_in_dat_w;  -- use sufficiently large result width
    variable v_a      : std_logic_vector(g_in_dat_w - 1 downto 0);
    variable v_b      : std_logic_vector(g_in_dat_w - 1 downto 0);
    variable v_result : unsigned(c_res_w - 1 downto 0);
  begin
    -- Calculate expected result
    v_a      := RESIZE_UVEC(in_a, g_in_dat_w);
    v_b      := RESIZE_UVEC(in_b, g_in_dat_w);
    v_result := RESIZE_NUM(unsigned(v_a) * unsigned(v_b), c_res_w);
    return RESIZE_UVEC(std_logic_vector(v_result), g_out_dat_w);  -- Truncate MSbits or zero extend MSBits
  end;

  signal rst                  : std_logic;
  signal clk                  : std_logic := '0';
  signal tb_end               : std_logic := '0';

  -- Input signals
  signal in_a                 : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b                 : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_a_p               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b_p               : std_logic_vector(g_in_dat_w - 1 downto 0);

  -- Product signals
  signal sresult_expected     : std_logic_vector(g_out_dat_w - 1 downto 0);  -- pipelined expected result
  signal sresult_rtl          : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal sresult_ip           : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal uresult_expected     : std_logic_vector(g_out_dat_w - 1 downto 0);  -- pipelined expected result
  signal uresult_rtl          : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal uresult_ip           : std_logic_vector(g_out_dat_w - 1 downto 0);

  -- auxiliary signals
  signal in_a_arr             : std_logic_vector(g_nof_mult * g_in_dat_w - 1 downto 0);
  signal in_b_arr             : std_logic_vector(g_nof_mult * g_in_dat_w - 1 downto 0);
  signal out_sresult          : std_logic_vector(g_out_dat_w - 1 downto 0);  -- combinatorial expected result
  signal out_uresult          : std_logic_vector(g_out_dat_w - 1 downto 0);  -- combinatorial expected result
  signal sresult_arr_expected : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
  signal uresult_arr_expected : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
  signal sresult_arr_rtl      : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
  signal uresult_arr_rtl      : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
  signal sresult_arr_ip       : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
  signal uresult_arr_ip       : std_logic_vector(g_nof_mult * g_out_dat_w - 1 downto 0);
begin
  clk  <= not clk or tb_end after clk_period / 2;

  -- run 1 us
  p_in_stimuli : process
  begin
    rst <= '1';
    in_a <= TO_SVEC(0, g_in_dat_w);
    in_b <= TO_SVEC(0, g_in_dat_w);
    proc_common_wait_some_cycles(clk, 10);
    rst <= '0';
    proc_common_wait_some_cycles(clk, 10);

    -- Some special combinations
    in_a <= TO_SVEC(2, g_in_dat_w);
    in_b <= TO_SVEC(3, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*p = pp
    in_b <= TO_SVEC(c_max_p, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(c_max_n, g_in_dat_w);  -- -p*-p = pp
    in_b <= TO_SVEC(c_max_n, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*-p =  = -pp
    in_b <= TO_SVEC(c_max_n, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*(-p-1) = -pp - p
    in_b <= TO_SVEC(c_min, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(c_max_n, g_in_dat_w);  -- -p*(-p-1) = pp + p
    in_b <= TO_SVEC(c_min, g_in_dat_w);
    wait until rising_edge(clk);

    proc_common_wait_some_cycles(clk, 50);

    -- All combinations
    for I in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
      for J in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
        in_a <= TO_SVEC(I, g_in_dat_w);
        in_b <= TO_SVEC(J, g_in_dat_w);
        wait until rising_edge(clk);
      end loop;
    end loop;

    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  -- pipeline inputs to ease comparison in the Wave window
  u_in_a_pipeline : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_in_dat_w,
    g_out_dat_w      => g_in_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => in_a,
    out_dat => in_a_p
  );

  u_in_b_pipeline : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_in_dat_w,
    g_out_dat_w      => g_in_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => in_b,
    out_dat => in_b_p
  );

  gen_wires : for I in 0 to g_nof_mult - 1 generate
    -- use same input for all multipliers
    in_a_arr((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= in_a;
    in_b_arr((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= in_b;
    -- calculate expected output only for one multiplier
    out_sresult <= func_sresult(in_a, in_b);
    out_uresult <= func_uresult(in_a, in_b);
    -- copy the expected result for all multipliers
    sresult_arr_expected((I + 1) * g_out_dat_w - 1 downto I * g_out_dat_w) <= sresult_expected;
    uresult_arr_expected((I + 1) * g_out_dat_w - 1 downto I * g_out_dat_w) <= uresult_expected;
  end generate;

  -- use mult(0) to observe result in Wave window
  sresult_rtl <= sresult_arr_rtl(g_out_dat_w - 1 downto 0);
  uresult_rtl <= uresult_arr_rtl(g_out_dat_w - 1 downto 0);
  sresult_ip  <= sresult_arr_ip(g_out_dat_w - 1 downto 0);
  uresult_ip  <= uresult_arr_ip(g_out_dat_w - 1 downto 0);

  u_sresult : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_out_dat_w,
    g_out_dat_w      => g_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => out_sresult,
    out_dat => sresult_expected
  );

  u_uresult : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_out_dat_w,
    g_out_dat_w      => g_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => out_uresult,
    out_dat => uresult_expected
  );

  u_sdut_rtl : entity work.common_mult
  generic map (
    g_technology       => c_technology,
    g_variant          => "RTL",
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_out_p_w          => g_out_dat_w,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_output  => g_pipeline_output,
    g_representation   => "SIGNED"
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => '1',
    in_a    => in_a_arr,
    in_b    => in_b_arr,
    out_p   => sresult_arr_rtl
  );

  u_udut_rtl : entity work.common_mult
  generic map (
    g_technology       => c_technology,
    g_variant          => "RTL",
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_out_p_w          => g_out_dat_w,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_output  => g_pipeline_output,
    g_representation   => "UNSIGNED"
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => '1',
    in_a    => in_a_arr,
    in_b    => in_b_arr,
    out_p   => uresult_arr_rtl
  );

  u_sdut_ip : entity work.common_mult
  generic map (
    g_technology       => c_technology,
    g_variant          => "IP",
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_out_p_w          => g_out_dat_w,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_output  => g_pipeline_output,
    g_representation   => "SIGNED"
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => '1',
    in_a    => in_a_arr,
    in_b    => in_b_arr,
    out_p   => sresult_arr_ip
  );

  u_udut_ip : entity work.common_mult
  generic map (
    g_technology       => c_technology,
    g_variant          => "IP",
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_out_p_w          => g_out_dat_w,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_output  => g_pipeline_output,
    g_representation   => "UNSIGNED"
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => '1',
    in_a    => in_a_arr,
    in_b    => in_b_arr,
    out_p   => uresult_arr_ip
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        -- verify all multipliers in parallel
        assert sresult_arr_rtl = sresult_arr_expected
          report "Error: wrong signed RTL result"
          severity ERROR;
        assert uresult_arr_rtl = uresult_arr_expected
          report "Error: wrong unsigned RTL result"
          severity ERROR;
        assert sresult_arr_ip = sresult_arr_expected
          report "Error: wrong signed IP result"
          severity ERROR;
        assert uresult_arr_ip = uresult_arr_expected
          report "Error: wrong unsigned IP result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
