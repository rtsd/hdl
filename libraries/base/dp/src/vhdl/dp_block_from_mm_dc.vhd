-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . Read a block of data from memory mapped (MM) location and stream it as a block of data,
--   this is a dual-clock wrapper around dp_block_from_mm.vhd.
-- Description:
-- . https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
-- . see dp_block_from_mm.vhd
-- . read block - output block synchronisation:
--   The start_pulse causes dp_block_from_mm to read a block from memory. The
--   mm_done = mm_fifo_sosi.eop signals that the last word was read from
--   memory and is about to enter the u_dp_fifo_fill_eop. The
--   u_dp_fifo_fill_eop can only output a new packet when it has got the eop,
--   so then the out_sop = dp_out_sosi.sop will appear some cycles later at the
--   u_dp_fifo_fill_eop output, due to the mm_clk - dp_clk domain crossing.
--   The out_sop thus indicates that the complete block available in the
--   u_dp_fifo_fill_eop and is being output. Now a new start_pulse can be
--   accepted by dp_block_from_mm_dc to read a next block. Thanks to this
--   handshake the start_pulse and out_sop always follow in pairs.
--
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_from_mm_dc is
  generic (
    g_user_size          : natural;
    g_data_size          : natural;
    g_step_size          : natural;
    g_nof_data           : natural;
    g_word_w             : natural := c_word_w;
    g_reverse_word_order : boolean := false;
    g_bsn_w              : natural := 1;
    g_bsn_incr_enable    : boolean := true
  );
  port (
    -- mm_clk domain
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;
    mm_mosi       : out t_mem_mosi;
    mm_miso       : in  t_mem_miso;
    -- dp_clk domain
    dp_rst        : in  std_logic;
    dp_clk        : in  std_logic;
    start_pulse   : in  std_logic;
    sync_in       : in  std_logic := '0';  -- Must be syncronous with start_pulse.
    bsn_at_sync   : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    start_address : in  natural;
    out_sop       : out std_logic;  -- = out_sosi.sop
    out_sosi      : out t_dp_sosi;
    out_siso      : in  t_dp_siso
  );
end dp_block_from_mm_dc;

architecture str of dp_block_from_mm_dc is
  constant c_packet_size  : natural := g_nof_data * g_data_size;

  -- Fit one packet in FIFO, and less than two, to avoid filling the FIFO with
  -- multiple packets in case writing FIFO (mm_clk) is faster than reading
  -- FIFO (dp_clk). It also works if the FIFO can fit multiple packets,
  -- because the handshake between out_sop and start_pulse takes care that a
  -- new packet will only be input when the previous packet is being output.
  constant c_fifo_fill    : natural := c_packet_size;
  constant c_fifo_size    : natural := c_packet_size + c_packet_size / 2;
  constant c_start_addr_w : natural := c_natural_w;
  constant c_delay_len    : natural := c_meta_delay_len;

  signal dp_out_sosi          : t_dp_sosi := c_dp_sosi_rst;
  signal dp_out_siso          : t_dp_siso;
  signal mm_fifo_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal mm_fifo_siso         : t_dp_siso;
  signal mm_start_pulse       : std_logic := '0';
  signal mm_sync_hi           : std_logic := '0';
  signal mm_sync_level        : std_logic := '0';
  signal mm_sync              : std_logic := '0';
  signal mm_done              : std_logic := '0';  -- not used, instead use out_sop
  signal mm_bsn_at_sync       : std_logic_vector(g_bsn_w - 1 downto 0)        := (others => '0');
  signal start_address_slv    : std_logic_vector(c_start_addr_w - 1 downto 0) := (others => '0');
  signal mm_start_address_slv : std_logic_vector(c_start_addr_w - 1 downto 0) := (others => '0');
  signal mm_start_address     : natural := 0;
begin
  -- Use g_delay_len = c_meta_delay_len + 4 for start_pulse and g_delay_len =
  -- c_meta_delay_len for the other signals. The + 4 ensures that the other
  -- signals are already stable in the mm_clk domain, when the mm_start_pulse
  -- occurs in the mm_clk domain. Use + 4 to still have 2 clock cycles margin
  -- if mm_start_pulse arrive one clock cycle early and mm_sync_hi arrives one
  -- clock cycle late.
  u_common_spulse_start_pulse : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len + 4
  )
  port map (
    in_rst      => dp_rst,
    in_clk      => dp_clk,
    in_pulse    => start_pulse,
    out_rst     => mm_rst,
    out_clk     => mm_clk,
    out_pulse   => mm_start_pulse
  );

  -- The synchronous start_pulse and sync_in in the dp_clk domain cannot be
  -- passed on via two separate common_spulse instances, because then they may
  -- appear at different clock cycles in the mm_clk domain, due to that the
  -- dp_clk and mm_clk are asynchronous on HW. Therefore use mm_sync_level to
  -- pass on sync_in.
  u_common_spulse_sync : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst      => dp_rst,
    in_clk      => dp_clk,
    in_pulse    => sync_in,
    out_rst     => mm_rst,
    out_clk     => mm_clk,
    out_pulse   => mm_sync_hi
  );

  p_mm_sync : process(mm_clk)
  begin
    if rising_edge(mm_clk) then
      if mm_sync_hi = '1' then
        mm_sync_level <= '1';
      end if;
      if mm_start_pulse = '1' then
        mm_sync_level <= '0';
      end if;
    end if;
  end process;

  mm_sync <= mm_sync_level and mm_start_pulse;  -- synchronous with mm_start_pulse

  start_address_slv <= TO_UVEC(start_address, c_start_addr_w);
  mm_start_address <= TO_UINT(mm_start_address_slv);

  u_common_async_slv_start_address : entity common_lib.common_async_slv
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => start_address_slv,
    dout => mm_start_address_slv
  );

  u_common_async_slv_bsn : entity common_lib.common_async_slv
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => bsn_at_sync,
    dout => mm_bsn_at_sync
  );

  u_dp_fifo_fill_eop : entity work.dp_fifo_fill_eop
  generic map (
    g_use_dual_clock => true,
    g_data_w         => c_word_w,
    g_bsn_w          => g_bsn_w,
    g_use_bsn        => true,
    g_use_sync       => true,
    g_fifo_fill      => c_fifo_fill,
    g_fifo_size      => c_fifo_size
  )
  port map (
    wr_rst      => mm_rst,
    wr_clk      => mm_clk,
    rd_rst      => dp_rst,
    rd_clk      => dp_clk,
    -- ST sink
    snk_in      => mm_fifo_sosi,
    snk_out     => mm_fifo_siso,
    -- ST source
    src_out     => dp_out_sosi,
    src_in      => dp_out_siso
  );

  u_dp_block_from_mm : entity work.dp_block_from_mm
  generic map (
    g_user_size          => g_user_size,
    g_data_size          => g_data_size,
    g_step_size          => g_step_size,
    g_nof_data           => g_nof_data,
    g_word_w             => g_word_w,
    g_reverse_word_order => g_reverse_word_order,
    g_bsn_w              => g_bsn_w,
    g_bsn_incr_enable    => g_bsn_incr_enable
  )
  port map (
    clk         => mm_clk,
    rst         => mm_rst,

    start_pulse   => mm_start_pulse,
    sync_in       => mm_sync,
    bsn_at_sync   => mm_bsn_at_sync,
    start_address => mm_start_address,
    mm_done       => mm_done,  -- = mm_fifo_sosi.eop
    mm_mosi       => mm_mosi,
    mm_miso       => mm_miso,
    out_sosi      => mm_fifo_sosi,
    out_siso      => mm_fifo_siso
  );

  -- Wire output
  out_sop <= dp_out_sosi.sop;

  out_sosi <= dp_out_sosi;
  dp_out_siso <= out_siso;
end str;
