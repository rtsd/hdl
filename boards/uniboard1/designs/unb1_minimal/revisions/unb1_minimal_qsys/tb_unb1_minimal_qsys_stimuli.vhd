-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb1_minimal_qsys using MM stimuli via file IO.
-- Description:
--   - Verify UNB_SENS
--   - Verify PPSH
--   - The tb is done when tb_end goes high.
--
--   The tb is self checking, but not self stopping when run-all is used,
--   because some internal DUT signals keep toggling. However in the
--   modelsim_regression_test_vhdl.py test tb_end high is used to stop the
--   simulation.
--
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb1_minimal_qsys
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb1_minimal_qsys)
--     . load tb_unb1_minimal_qsys_stimuli simulation by double clicking the tb_unb1_minimal_qsys_stimuli icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us
--
library IEEE, common_lib, mm_lib, unb1_board_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;

entity tb_unb1_minimal_qsys_stimuli is
end tb_unb1_minimal_qsys_stimuli;

architecture tb of tb_unb1_minimal_qsys_stimuli is
  constant c_sim             : boolean := true;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 7;  -- Back node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb1_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb1_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb1_board_fw_version := (1, 0);

  constant c_cable_delay     : time := 12 ns;
  constant c_tb_clk_period   : time := 10 ns;
  constant c_eth_clk_period  : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_ext_clk_period  : time := 5 ns;  -- 200 MHz
  constant c_ext_pps_period  : natural := 1000;

  constant c_cross_clock_domain_delay : natural := 50;
  constant c_mm_file_reg_ppsh         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_unb_sens     : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_UNB_SENS";

  -- tb
  signal tb_end               : std_logic := '0';
  signal tb_clk               : std_logic := '0';
  signal wr_data              : std_logic_vector(c_32 - 1 downto 0);
  signal rd_data              : std_logic_vector(c_32 - 1 downto 0);
  signal rd_fpga_temp         : integer;
  signal rd_eth_temp          : integer;
  signal rd_voltage           : integer;
  signal rd_current           : integer;
  signal rd_sens_err          : integer;
  signal rd_temp_high         : integer;

  signal rd_ppsh_toggle       : std_logic;
  signal rd_ppsh_stable       : std_logic;
  signal rd_ppsh_capture_cnt  : natural;
  signal rd_ppsh_edge         : std_logic;
  signal rd_ppsh_expected_cnt : natural;

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal ext_rst             : std_logic := '1';
  signal ext_pps             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;

  -- Model I2C sensor slaves as on the UniBoard
  constant c_fpga_temp_address   : std_logic_vector(6 downto 0) := "0011000";  -- MAX1618 address LOW LOW
  constant c_fpga_temp           : integer := 60;
  constant c_eth_temp_address    : std_logic_vector(6 downto 0) := "0101001";  -- MAX1618 address MID LOW
  constant c_eth_temp            : integer := 40;
  constant c_hot_swap_address    : std_logic_vector(6 downto 0) := "1000100";  -- LTC4260 address L L L
  constant c_hot_swap_R_sense    : real := 0.01;  -- = 10 mOhm on UniBoard
  constant c_temp_high           : integer := 85;

  constant c_uniboard_current    : real := 5.0;  -- = assume 5.0 A on UniBoard
  constant c_uniboard_supply     : real := 48.0;  -- = assume 48.0 V on UniBoard
  constant c_uniboard_adin       : real := -1.0;  -- = NC on UniBoard

  constant c_extpected_cnt       : natural := (1000 ms) / c_ext_clk_period;
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk or tb_end after c_ext_clk_period / 2;  -- External clock (200 MHz)
  ext_rst <= '1', '0' after c_ext_clk_period * 10;

  eth_clk <= not eth_clk or tb_end after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_ext_pps_period, '1', ext_rst, ext_clk, ext_pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp <= transport eth_txp after c_cable_delay;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb1_minimal_qsys : entity work.unb1_minimal_qsys
    generic map (
      g_sim         => c_sim,
      g_sim_unb_nr  => c_unb_nr,
      g_sim_node_nr => c_node_nr
    )
    port map (
      -- GENERAL
      CLK         => ext_clk,
      PPS         => ext_pps,
      WDI         => WDI,
      INTA        => INTA,
      INTB        => INTB,

      sens_sc     => sens_scl,
      sens_sd     => sens_sda,

      -- Others
      VERSION     => VERSION,
      ID          => ID,
      TESTIO      => TESTIO,

      -- 1GbE Control Interface
      ETH_clk     => eth_clk,
      ETH_SGIN    => eth_rxp,
      ETH_SGOUT   => eth_txp
    );

  ------------------------------------------------------------------------------
  -- UniBoard sensors
  ------------------------------------------------------------------------------
  -- I2C slaves that are available for each FPGA
  u_fpga_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_fpga_temp_address
  )
  port map (
    scl  => sens_scl,
    sda  => sens_sda,
    temp => c_fpga_temp
  );

  -- I2C slaves that are available only via FPGA back node 3
  u_eth_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_eth_temp_address
  )
  port map (
    scl  => sens_scl,
    sda  => sens_sda,
    temp => c_eth_temp
  );

  u_power : entity i2c_lib.dev_ltc4260
  generic map (
    g_address => c_hot_swap_address,
    g_R_sense => c_hot_swap_R_sense
  )
  port map (
    scl               => sens_scl,
    sda               => sens_sda,
    ana_current_sense => c_uniboard_current,
    ana_volt_source   => c_uniboard_supply,
    ana_volt_adin     => c_uniboard_adin
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk <= not tb_clk or tb_end after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn : natural;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Read board sensors
    ----------------------------------------------------------------------------
    wait for 30 us;

    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 0, rd_data, tb_clk); rd_fpga_temp <= TO_SINT(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 1, rd_data, tb_clk); rd_eth_temp  <= TO_SINT(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 2, rd_data, tb_clk); rd_voltage   <= TO_SINT(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 3, rd_data, tb_clk); rd_current   <= TO_SINT(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 4, rd_data, tb_clk); rd_sens_err  <= TO_SINT(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_unb_sens, 5, rd_data, tb_clk); rd_temp_high <= TO_SINT(rd_data);

    proc_common_wait_some_cycles(tb_clk, 1);
    assert rd_fpga_temp = c_fpga_temp
      report "REG_UNB_SENS wrong FPGA temperature"
      severity ERROR;
    assert rd_eth_temp = c_eth_temp
      report "REG_UNB_SENS wrong ETH temperature"
      severity ERROR;
    assert rd_sens_err = 0
      report "REG_UNB_SENS I2C access went wrong"
      severity ERROR;
    assert rd_temp_high = c_temp_high
      report "REG_UNB_SENS wrong high temperature"
      severity ERROR;

    ----------------------------------------------------------------------------
    -- Read PPSH
    ----------------------------------------------------------------------------

    -- verify PPS toggle
    proc_common_wait_until_hi_lo(ext_clk, ext_pps);
    mmf_mm_bus_rd(c_mm_file_reg_ppsh, 0, rd_data, tb_clk);
    rd_ppsh_toggle <= rd_data(31);
    proc_common_wait_until_hi_lo(ext_clk, ext_pps);
    mmf_mm_bus_rd(c_mm_file_reg_ppsh, 0, rd_data, tb_clk);

    proc_common_wait_some_cycles(tb_clk, 1);
    assert rd_ppsh_toggle = not rd_data(31)
      report "REG_PPHS wrong toggle"
      severity ERROR;

    -- verify PPS edge, stable and capture cnt and expected cnt
    -- The capture cnt and expected cnt cannot be equal in this simulation so therefore PPS will not become stable
    proc_common_wait_until_hi_lo(ext_clk, ext_pps);
    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    mmf_mm_bus_rd(c_mm_file_reg_ppsh, 0, rd_data, tb_clk);
    rd_ppsh_toggle <= rd_data(31);
    rd_ppsh_stable <= rd_data(30);
    rd_ppsh_capture_cnt <= TO_UINT(rd_data(29 downto 0));
    mmf_mm_bus_rd(c_mm_file_reg_ppsh, 1, rd_data, tb_clk);
    rd_ppsh_edge <= rd_data(31);
    rd_ppsh_expected_cnt <= TO_UINT(rd_data(30 downto 0));

    proc_common_wait_some_cycles(tb_clk, 1);
    assert rd_ppsh_stable = '0'
      report "REG_PPHS wrong stable"
      severity ERROR;
    assert rd_ppsh_capture_cnt = c_ext_pps_period
      report "REG_PPHS wrong capture count"
      severity ERROR;
    assert rd_ppsh_edge = '0'
      report "REG_PPHS wrong edge"
      severity ERROR;
    assert rd_ppsh_expected_cnt = c_extpected_cnt
      report "REG_PPHS wrong capture count"
      severity ERROR;

    proc_common_wait_some_cycles(tb_clk, 10);
    tb_end <= '1';
    wait;
  end process;
end tb;
