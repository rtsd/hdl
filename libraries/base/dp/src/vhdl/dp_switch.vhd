-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Switch one of g_nof_inputs input streams to the output.
-- Description:
-- . Respects frame boundaries (SOP..EOP).
-- . This is basically an MM-controlled dp_mux (with g_mode=4).
-- . There is always one input enabled. If undesired, add your own input and
--   assign c_dp_sosi_rst to it. Switching to that input will disable the output.
-- Remark:
-- . dp_mux requires a minimum invaled gap of 1 cycle to switch to a new input!
--   . So this does not work for continuous streams!

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

entity dp_switch is
  generic (
    g_nof_inputs      : natural;  -- Number of inputs
    g_default_enabled : natural := 0;  -- This input number 0..g_nof_inputs-1 will be enabled by default
    g_use_fifo        : boolean := false;  -- This and the generics below are forwarded to dp_mux only
    g_bsn_w           : natural := 16;
    g_data_w          : natural := 16;
    g_empty_w         : natural := 1;
    g_in_channel_w    : natural := 1;
    g_error_w         : natural := 1;
    g_use_bsn         : boolean := false;
    g_use_empty       : boolean := false;
    g_use_in_channel  : boolean := false;
    g_use_error       : boolean := false;
    g_use_sync        : boolean := false;
    g_fifo_af_margin  : natural := 4;  -- Nof words below max (full) at which fifo is considered almost full
    g_fifo_size       : natural := 1024;
    g_fifo_fill       : natural := 0
  );
  port (
    dp_clk      : in  std_logic;
    dp_rst      : in  std_logic;

    mm_clk      : in  std_logic;
    mm_rst      : in  std_logic;

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    snk_out_arr : out t_dp_siso_arr(g_nof_inputs - 1 downto 0);

    src_out     : out t_dp_sosi;
    src_in      : in  t_dp_siso := c_dp_siso_rdy;

    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso
  );
end dp_switch;

architecture str of dp_switch is
  constant c_field_arr : t_common_field_arr(0 downto 0) := (0 => ( field_name_pad("input_select"), "RW", ceil_log2(g_nof_inputs), field_default(g_default_enabled) ));

  signal mm_fields_out       : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);
  signal dp_mux_sel_ctrl_req : natural;
  signal dp_mux_sel_ctrl     : natural;

  signal xonoff_src_out_arr  : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal xonoff_src_in_arr   : t_dp_siso_arr(g_nof_inputs - 1 downto 0);

  signal inverted_snk_in_arr : t_dp_sosi_arr(0 to g_nof_inputs - 1);
  signal inverted_snk_out_arr: t_dp_siso_arr(0 to g_nof_inputs - 1);
begin
  ------------------------------------------------------------------------------
  -- A single MM register contains input to select
  ------------------------------------------------------------------------------
  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_field_arr => c_field_arr
  )
  port map (
    mm_clk  => mm_clk,
    mm_rst  => mm_rst,

    mm_mosi => reg_mosi,
    mm_miso => reg_miso,

    slv_clk => dp_clk,
    slv_rst => dp_rst,

    slv_out => mm_fields_out
  );

  ------------------------------------------------------------------------------
  -- put dp_xonoff block inbetween data path to control data flow.
  ------------------------------------------------------------------------------
  gen_dp_xonoff_arr : for i in 0 to g_nof_inputs - 1 generate
    u_dp_xonoff: entity work.dp_xonoff
    port map (
      clk         => dp_clk,
      rst         => dp_rst,
      -- Frame in
      in_sosi     => snk_in_arr(i),
      in_siso     => snk_out_arr(i),
      -- Frame out
      out_siso    => xonoff_src_in_arr(i),  -- flush control done by dp_mux.snk_out_arr
      out_sosi    => xonoff_src_out_arr(i)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- invert snk_in_arr, dp_mux uses a TO array.
  ------------------------------------------------------------------------------
  gen_dp_switch_snk_in_arr : for i in 0 to g_nof_inputs - 1 generate
    inverted_snk_in_arr(i) <= xonoff_src_out_arr(i);
    xonoff_src_in_arr(i) <= inverted_snk_out_arr(i);
  end generate;

  ------------------------------------------------------------------------------
  -- Don't allow user to select a non-existent input
  -- . If user requests non-existent input, the default input is forwarded instead.
  ------------------------------------------------------------------------------
  dp_mux_sel_ctrl_req <= TO_UINT(mm_fields_out(field_hi(c_field_arr, "input_select") downto field_lo(c_field_arr, "input_select")));
  dp_mux_sel_ctrl <= dp_mux_sel_ctrl_req when dp_mux_sel_ctrl_req < g_nof_inputs else g_default_enabled;

  ------------------------------------------------------------------------------
  -- DP mux forwards input based on dp_mux_sel_ctrl
  ------------------------------------------------------------------------------
  u_dp_mux : entity work.dp_mux
  generic map (
    g_mode            => 4,  -- Use sel_ctrl
    g_sel_ctrl_invert => false,  -- Dont invert, data is already reverse mapped from DOWNTO >> TO arrays.
    g_nof_input       => g_nof_inputs,
    g_use_fifo        => g_use_fifo,
    g_bsn_w           => g_bsn_w,
    g_data_w          => g_data_w,
    g_empty_w         => g_empty_w,
    g_in_channel_w    => g_in_channel_w,
    g_error_w         => g_error_w,
    g_use_bsn         => g_use_bsn,
    g_use_empty       => g_use_empty,
    g_use_in_channel  => g_use_in_channel,
    g_use_error       => g_use_error,
    g_use_sync        => g_use_sync,
    g_fifo_af_margin  => g_fifo_af_margin,
    g_fifo_size       => array_init(g_fifo_size, g_nof_inputs),
    g_fifo_fill       => array_init(g_fifo_fill, g_nof_inputs)
  )
  port map (
    clk         => dp_clk,
    rst         => dp_rst,

    sel_ctrl    => dp_mux_sel_ctrl,

    snk_in_arr  => inverted_snk_in_arr,
    snk_out_arr => inverted_snk_out_arr,

    src_out     => src_out,
    src_in      => src_in
  );
end str;
