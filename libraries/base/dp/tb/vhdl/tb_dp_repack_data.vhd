-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench for dp_repack_data
-- Description:
--                                                       c_no_unpack
--                                                          .
--             g_in_dat_w             g_pack_dat_w          .   g_in_dat_w
--             g_in_nof_words         g_pack_nof_words      .   g_in_nof_words
--                 .                       .                .       .
--                 .   u_pack              .       u_unpack .       .
--                 .   ______________      .       ______________   .
--                 .  |dp_repack_data|     .      |dp_repack_data|  .
--   stimuli_src ---->|              |----------->|              |----> verify_snk
--                    | in       out |  pack_src  | in       out |
--                    |______________|            |______________|
--
-- Usage:
-- > as 10
-- > run -all
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_repack_data is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    -- specific
    g_use_repack_in_load     : boolean := false;
    g_in_dat_w               : natural := 36;
    g_in_nof_words           : natural := 16;
    g_pack_dat_w             : natural := 64;
    g_pack_nof_words         : natural := 9;
    g_in_bypass              : boolean := false;  -- can use TRUE when g_in_nof_words=1  or g_in_nof_words=g_out_nof_words
    g_pack_bypass            : boolean := false;  -- can use TRUE when g_out_nof_words=1 or g_in_nof_words=g_out_nof_words
    g_in_symbol_w            : natural := 1;  -- default 1 for snk_in.empty  in nof bits, else use power of 2
    g_pack_symbol_w          : natural := 1;  -- default 1 for src_out.empty in nof bits, else use power of 2
    g_nof_repeat             : natural := 10;
    g_pkt_len                : natural := 64;  -- if not a multiple of g_in_nof_words then the input stage flush creates gap between blocks
    g_pkt_gap                : natural := 10
  );
end tb_dp_repack_data;

architecture tb of tb_dp_repack_data is
  constant c_no_unpack                : boolean := false;
  constant c_enable_repack_in         : boolean := true;
  constant c_enable_repack_out        : boolean := true;

  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;  -- g_in_nof_words;
  constant c_stimuli_pulse_period     : natural := 7;

  constant c_data_init                : natural := 0;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init                 : natural := 247;
  constant c_channel_init             : natural := 5;  -- fixed

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_data_max                 : unsigned(g_in_dat_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(g_in_dat_w - 1 downto 0) := (others => '1');

  --CONSTANT c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_rst;  -- default 0 is no wrap
  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  constant c_expected_pkt_len         : natural := sel_a_b(c_no_unpack, g_pkt_len * g_pack_nof_words / g_in_nof_words, g_pkt_len);
  -- both
  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(g_in_dat_w - 1 downto 0);

  -- specific
  signal pack_src_in                : t_dp_siso;
  signal pack_src_out               : t_dp_sosi;
  signal pack_src_out_data          : std_logic_vector(g_pack_dat_w - 1 downto 0);

  signal unpack_src_in              : t_dp_siso;
  signal unpack_src_out             : t_dp_sosi;
  signal unpack_src_out_data        : std_logic_vector(g_in_dat_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_data_init      => c_data_init,
    g_bsn_init       => c_bsn_init,
    g_err_init       => c_err_init,
    g_channel_init   => c_channel_init,
    -- specific
    g_in_dat_w       => g_in_dat_w,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => g_pkt_len,
    g_pkt_gap        => g_pkt_gap
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1' when c_no_unpack = false else '0';
  verify_snk_in_enable.re      <= '0';
  verify_snk_in_enable.im      <= '0';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '1';
  verify_snk_in_enable.err     <= '1';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;
  verify_last_snk_in_evt.data    <= last_snk_in_evt when c_no_unpack = false else '0';
  verify_last_snk_in_evt.re      <= '0';
  verify_last_snk_in_evt.im      <= '0';
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= last_snk_in_evt;
  verify_last_snk_in_evt.err     <= last_snk_in_evt;

  u_dp_stream_verify : entity work.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => g_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => g_in_dat_w,
    g_pkt_len        => c_expected_pkt_len
  )
  port map (
    rst                        => rst,
    clk                        => clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- DUT Pack
  ------------------------------------------------------------------------------

  u_pack : entity work.dp_repack_data
  generic map (
    g_use_repack_in_load => g_use_repack_in_load,
    g_enable_repack_in   => c_enable_repack_in,
    g_enable_repack_out  => c_enable_repack_out,
    g_in_bypass          => g_in_bypass,
    g_in_dat_w           => g_in_dat_w,
    g_in_nof_words       => g_in_nof_words,
    g_in_symbol_w        => g_in_symbol_w,
    g_out_bypass         => g_pack_bypass,
    g_out_dat_w          => g_pack_dat_w,
    g_out_nof_words      => g_pack_nof_words,
    g_out_symbol_w       => g_pack_symbol_w
  )
  port map (
    rst              => rst,
    clk              => clk,

    snk_out          => stimuli_src_in,
    snk_in           => stimuli_src_out,

    src_in           => pack_src_in,
    src_out          => pack_src_out
  );

  pack_src_out_data <= pack_src_out.data(g_pack_dat_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- DUT Unpack
  ------------------------------------------------------------------------------

  no_unpack : if c_no_unpack = true generate
    pack_src_in    <= unpack_src_in;
    unpack_src_out <= pack_src_out;
  end generate;

  gen_unpack : if c_no_unpack = false generate
    u_unpack : entity work.dp_repack_data
    generic map (
      g_use_repack_in_load => g_use_repack_in_load,
      g_enable_repack_in   => c_enable_repack_out,
      g_enable_repack_out  => c_enable_repack_in,
      g_in_bypass          => g_pack_bypass,
      g_in_dat_w           => g_pack_dat_w,
      g_in_nof_words       => g_pack_nof_words,
      g_in_symbol_w        => g_pack_symbol_w,
      g_out_bypass         => g_in_bypass,
      g_out_dat_w          => g_in_dat_w,
      g_out_nof_words      => g_in_nof_words,
      g_out_symbol_w       => g_in_symbol_w
    )
    port map (
      rst              => rst,
      clk              => clk,

      snk_out          => pack_src_in,
      snk_in           => pack_src_out,

      src_in           => unpack_src_in,
      src_out          => unpack_src_out
    );
  end generate;

  unpack_src_out_data <= unpack_src_out.data(g_in_dat_w - 1 downto 0);

  unpack_src_in <= verify_snk_out;
  verify_snk_in <= unpack_src_out;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(g_in_dat_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(g_in_dat_w - 1 downto 0);
end tb;
