-------------------------------------------------------------------------------
--
-- Copyright (C) 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Purpose:
--   Provide Ethernet access to a node for one UDP stream.
-- Description:
-- * This eth_stream_udp.vhd contains the IP/UDP related components of eth.vhd
--   that are needed to send or receive an UDP stream via 1GbE.
--   . support only only UDP offload/onload stream.
--   . the IP checksum is filled in for Tx and checked or Rx.
--   . the Tx only contains UDP stream data, so no need for a dp_mux.
--   . the Rx may contain other packet types, because the 1GbE connects to
--     a network. All Rx packets that are not UDP for g_rx_udp_port are
--     discarded.
-- * Use eth_stream.vhd to have eth_stream_udp in combination with the TSE.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

entity eth_stream_udp is
  generic (
    g_rx_udp_port : natural
  );
  port (
    -- Clocks and reset
    st_rst        : in  std_logic;
    st_clk        : in  std_logic;

    -- User UDP interface
    -- . Tx
    udp_tx_sosi   : in  t_dp_sosi;
    udp_tx_siso   : out t_dp_siso;
    -- . Rx
    udp_rx_sosi   : out t_dp_sosi;
    udp_rx_siso   : in  t_dp_siso := c_dp_siso_rdy;

    -- PHY interface
    -- . Tx
    tse_tx_sosi   : out t_dp_sosi;
    tse_tx_siso   : in  t_dp_siso;
    -- . Rx
    tse_rx_sosi   : in  t_dp_sosi;
    tse_rx_siso   : out t_dp_siso
  );
end eth_stream_udp;

architecture str of eth_stream_udp is
  -- ETH Tx
  signal eth_tx_siso            : t_dp_siso;
  signal eth_tx_sosi            : t_dp_sosi;

  -- ETH Rx
  signal rx_adapt_siso          : t_dp_siso;
  signal rx_adapt_sosi          : t_dp_sosi;

  signal rx_hdr_status          : t_eth_hdr_status;
  signal rx_hdr_status_complete : std_logic;

  signal rx_eth_discard         : std_logic;
  signal rx_eth_discard_val     : std_logic;
begin
  ------------------------------------------------------------------------------
  -- TX
  ------------------------------------------------------------------------------

  -- Insert IP header checksum
  u_tx_ip : entity work.eth_hdr
  generic map (
    g_header_store_and_forward     => true,
    g_ip_header_checksum_calculate => true
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => udp_tx_sosi,
    snk_out         => udp_tx_siso,

    -- Streaming Source
    src_in          => tse_tx_siso,
    src_out         => tse_tx_sosi  -- with err field value 0 for OK
  );

  ------------------------------------------------------------------------------
  -- RX
  ------------------------------------------------------------------------------

  -- Adapt the TSE RX source ready latency from 2 to 1
  u_adapt : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => c_eth_rx_ready_latency,  -- = 2
    g_out_latency => c_eth_ready_latency  -- = 1
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    -- ST sink
    snk_out => tse_rx_siso,
    snk_in  => tse_rx_sosi,
    -- ST source
    src_in  => rx_adapt_siso,
    src_out => rx_adapt_sosi
  );

  -- Pass on UDP stream for g_rx_udp_port
  -- . Verify IP header checksum for IP
  u_rx_udp : entity work.eth_hdr
  generic map (
    g_header_store_and_forward     => true,
    g_ip_header_checksum_calculate => true
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => rx_adapt_sosi,
    snk_out         => rx_adapt_siso,

    -- Streaming Source
    src_in          => udp_rx_siso,
    src_out         => udp_rx_sosi,

    -- Frame control
    frm_discard     => rx_eth_discard,
    frm_discard_val => rx_eth_discard_val,

    -- Header info
    hdr_status          => rx_hdr_status,
    hdr_status_complete => rx_hdr_status_complete
  );

  -- Discard all Rx data that is not UDP for g_rx_udp_port
  p_rx_discard : process(st_rst, st_clk)
  begin
    if st_rst = '1' then
      rx_eth_discard <= '1';  -- default discard
      rx_eth_discard_val <= '0';
    elsif rising_edge(st_clk) then
      -- Default keep rx_eth_discard status (instead of '1'), to more clearly
      -- see when a change occurs
      if rx_hdr_status_complete = '1' then
        rx_eth_discard <= '1';  -- default discard
        if rx_hdr_status.is_ip = '1' and
           rx_hdr_status.is_udp = '1' and
           TO_UINT(rx_hdr_status.udp_port) = g_rx_udp_port then
          rx_eth_discard <= '0';  -- pass on IP/UDP stream for g_rx_udp_port
        end if;
      end if;

      rx_eth_discard_val <= rx_hdr_status_complete;
    end if;
  end process;
end str;
