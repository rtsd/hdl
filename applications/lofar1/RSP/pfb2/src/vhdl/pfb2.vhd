-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Combine LOFAR1 pfs + pft2 into pfb2 with statistics and streaming
--          interfaces, similar as wpfb_unit_dev
-- Description:
--
-- Remark:
-- . Convert between LOFAR1 sync timing 1 clk before sop and streaming data path
--   (DP) sync timing at sop.
-- . g_switch_en = '1' decorrelates rounding crosstalk between the X and Y output
--   that occurs due to shared complex FFT and seperate in PFT_MODE_REAL2.

library IEEE, common_lib, dp_lib, pfs_lib, pft2_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use pfs_lib.pfs_pkg.all;
use pft2_lib.pft_pkg.all;

entity pfb2 is
  generic (
    g_nof_points      : natural := 1024;

    -- pfs
    g_pfs_bypass      : boolean := false;
    g_pfs_nof_taps    : natural := 16;
    g_pfs_in_dat_w    : natural := 12;
    g_pfs_out_dat_w   : natural := 18;
    g_pfs_coef_dat_w  : natural := c_pfs_coef_w;  -- = 16, should match coefs in g_pfs_coefs_file
    g_pfs_coefs_file  : string  := c_pfs_coefs_file;  -- = "data/pfs_coefsbuf_1024.hex"

    -- pft2
    g_pft_mode        : PFT_MODE_TYPE := PFT_MODE_REAL2;
    g_pft_switch_en   : std_logic := '1';
    g_pft_stage_dat_w : natural := c_pft_stage_dat_w;  -- c_pft_stage_dat_w = 20 in pft_pkg.vhd
    g_pft_out_dat_w   : natural := 18
  );
  port (
    dp_rst        : in  std_logic;
    dp_clk        : in  std_logic;
    in_sosi       : in  t_dp_sosi;
    fil_sosi      : out t_dp_sosi;
    out_sosi      : out t_dp_sosi
  );
end pfb2;

architecture str of pfb2 is
  constant c_nof_coeffs     : natural := g_pfs_nof_taps * g_nof_points;

  signal pfs_in_dat_x       : std_logic_vector(g_pfs_in_dat_w - 1 downto 0);
  signal pfs_in_dat_y       : std_logic_vector(g_pfs_in_dat_w - 1 downto 0);
  signal pfs_in_val         : std_logic;
  signal pfs_in_sync        : std_logic;
  signal fil_out_dat_x      : std_logic_vector(g_pfs_out_dat_w - 1 downto 0);
  signal fil_out_dat_y      : std_logic_vector(g_pfs_out_dat_w - 1 downto 0);
  signal fil_out_val        : std_logic;
  signal fil_out_sync       : std_logic;

  signal pft_out_dat_re     : std_logic_vector(g_pft_out_dat_w - 1 downto 0);
  signal pft_out_dat_im     : std_logic_vector(g_pft_out_dat_w - 1 downto 0);
  signal pft_out_val        : std_logic;
  signal pft_out_sync       : std_logic;
begin
  -- Delay in_sosi data with respect to sync to fit LOFAR1 sync timing
  pfs_in_dat_x <= in_sosi.re(g_pfs_in_dat_w - 1 downto 0) when rising_edge(dp_clk);
  pfs_in_dat_y <= in_sosi.im(g_pfs_in_dat_w - 1 downto 0) when rising_edge(dp_clk);
  pfs_in_val   <= in_sosi.valid                         when rising_edge(dp_clk);
  pfs_in_sync  <= in_sosi.sync;

  gen_pfs : if g_pfs_bypass = false generate
    u_pfs : entity pfs_lib.pfs
    generic map (
      g_nof_bands              => g_nof_points,
      g_nof_taps               => c_nof_coeffs,
      g_in_dat_w               => g_pfs_in_dat_w,
      g_out_dat_w              => g_pfs_out_dat_w,
      g_coef_dat_w             => g_pfs_coef_dat_w,
      g_coefs_file             => g_pfs_coefs_file
    )
    port map (
      in_dat_x                 => pfs_in_dat_x,
      in_dat_y                 => pfs_in_dat_y,
      in_val                   => pfs_in_val,
      in_sync                  => pfs_in_sync,
      out_dat_x                => fil_out_dat_x,
      out_dat_y                => fil_out_dat_y,
      out_val                  => fil_out_val,
      out_sync                 => fil_out_sync,
      clk                      => dp_clk,
      rst                      => dp_rst,
      restart                  => '0'
    );
  end generate;

  no_pfs : if g_pfs_bypass = true generate
    fil_out_dat_x <= SHIFT_SVEC(RESIZE_SVEC(pfs_in_dat_x, g_pfs_out_dat_w), g_pfs_in_dat_w - g_pfs_out_dat_w);  -- < 0 is shift left, > 0 is shift right
    fil_out_dat_y <= SHIFT_SVEC(RESIZE_SVEC(pfs_in_dat_y, g_pfs_out_dat_w), g_pfs_in_dat_w - g_pfs_out_dat_w);  -- < 0 is shift left, > 0 is shift right
    fil_out_val   <= pfs_in_val;
    fil_out_sync  <= pfs_in_sync;
  end generate;

  fil_sosi.re    <= RESIZE_DP_DSP_DATA(fil_out_dat_x);
  fil_sosi.im    <= RESIZE_DP_DSP_DATA(fil_out_dat_y);
  fil_sosi.valid <= fil_out_val;
  fil_sosi.sync  <= fil_out_sync;

  u_pft : entity pft2_lib.pft
  generic map (
    g_fft_size_w             => ceil_log2(g_nof_points),
    g_in_dat_w               => g_pfs_out_dat_w,
    g_out_dat_w              => g_pft_out_dat_w,
    g_stage_dat_w            => g_pft_stage_dat_w,
    g_mode                   => PFT_MODE_REAL2
  )
  port map (
    in_re                    => fil_out_dat_x,
    in_im                    => fil_out_dat_y,
    in_val                   => fil_out_val,
    in_sync                  => fil_out_sync,
    switch_en                => g_pft_switch_en,
    out_re                   => pft_out_dat_re,
    out_im                   => pft_out_dat_im,
    out_val                  => pft_out_val,
    out_sync                 => pft_out_sync,
    clk                      => dp_clk,
    rst                      => dp_rst
  );

  -- Delay pft sync with respect pft data to fit DP sync timing
  out_sosi.re    <= RESIZE_DP_DSP_DATA(pft_out_dat_re);
  out_sosi.im    <= RESIZE_DP_DSP_DATA(pft_out_dat_im);
  out_sosi.valid <= pft_out_val;
  out_sosi.sync  <= pft_out_sync when rising_edge(dp_clk);
end str;
