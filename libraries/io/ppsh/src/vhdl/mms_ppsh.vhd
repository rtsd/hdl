-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for ppsh
-- Description: See ppsh.vhd

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_ppsh is
  generic (
    g_technology         : natural := c_tech_select_default;
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_st_clk_freq        : natural := 200 * 10**6  -- clk frequency in Hz
  );
  port (
    -- Clocks and reset
    mm_rst           : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk           : in  std_logic;  -- memory-mapped bus clock
    st_rst           : in  std_logic;  -- reset synchronous with st_clk
    st_clk           : in  std_logic;  -- streaming clock domain clock
    pps_ext          : in  std_logic;  -- with unknown but constant phase to st_clk

    -- Memory-mapped clock domain
    reg_mosi         : in  t_mem_mosi := c_mem_mosi_rst;  -- actual ranges defined by c_mm_reg
    reg_miso         : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- PIO support (for backwards compatibility with pin_pps on ctrl_unb_common)
    pin_pps          : out std_logic_vector(c_word_w - 1 downto 0);

    -- Streaming clock domain
    pps_sys          : out std_logic  -- pps pulse in st_clk domain (i.e. 1 clk cycle pulse per second)
  );
end mms_ppsh;

architecture str of mms_ppsh is
  signal st_pps_sys         : std_logic;
  signal mm_pps_sys         : std_logic;

  -- MM registers in st_clk domain
  signal st_pps_toggle      : std_logic;
  signal st_pps_stable      : std_logic;
  signal st_capture_cnt     : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between subsequent pps_ext pulses
  signal st_offset_cnt      : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between now and last pps_ext pulse

  signal st_pps_stable_ack  : std_logic;

  signal st_capture_edge    : std_logic;
  signal st_expected_cnt    : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);  -- expected number of clk clock cycles between subsequent pps_ext pulses

  -- MM registers in mm_clk domaim for pin_pps support
  signal mm_pps_toggle      : std_logic;
  signal nxt_mm_pps_toggle  : std_logic;
  signal mm_capture_cnt     : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);
  signal nxt_mm_capture_cnt : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);
begin
  pps_sys <= st_pps_sys;

  u_ppsh : entity work.ppsh
  generic map (
    g_technology     => g_technology,
    g_clk_freq       => g_st_clk_freq
  )
  port map (
    rst              => st_rst,
    clk              => st_clk,
    -- PPS
    pps_ext          => pps_ext,
    pps_sys          => st_pps_sys,
    -- MM control
    pps_toggle       => st_pps_toggle,
    pps_stable       => st_pps_stable,
    capture_cnt      => st_capture_cnt,
    offset_cnt       => st_offset_cnt,
    pps_stable_ack   => st_pps_stable_ack,
    capture_edge     => st_capture_edge,
    expected_cnt     => st_expected_cnt
  );

  ------------------------------------------------------------------------------
  -- New MM interface via avs_common_mm
  ------------------------------------------------------------------------------

  u_mm_reg : entity work.ppsh_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_st_clk_freq        => g_st_clk_freq
  )
  port map (
    -- Clocks and reset
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,
    st_rst              => st_rst,
    st_clk              => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in              => reg_mosi,
    sla_out             => reg_miso,

    -- MM registers in st_clk domain
    st_pps_toggle       => st_pps_toggle,
    st_pps_stable       => st_pps_stable,
    st_pps_stable_ack   => st_pps_stable_ack,
    st_capture_cnt      => st_capture_cnt,
    st_offset_cnt       => st_offset_cnt,
    st_capture_edge     => st_capture_edge,
    st_expected_cnt     => st_expected_cnt
  );

  ------------------------------------------------------------------------------
  -- Old MM interface via PIO
  ------------------------------------------------------------------------------

  p_mm_clk : process (mm_clk, mm_rst)
  begin
    if mm_rst = '1' then
      mm_pps_toggle  <= '0';
      mm_capture_cnt <= (others => '1');
    elsif rising_edge(mm_clk) then
      mm_pps_toggle  <= nxt_mm_pps_toggle;
      mm_capture_cnt <= nxt_mm_capture_cnt;
    end if;
  end process;

  -- Use mm_pps to get the PPS info from u_pps into the MM clock domain,
  -- because after the pps_pulse the pps_toggle and capture_cnt are stable
  u_mm_pps : entity common_lib.common_spulse
  port map (
    in_rst       => st_rst,
    in_clk       => st_clk,
    in_pulse     => st_pps_sys,
    out_rst      => mm_rst,
    out_clk      => mm_clk,
    out_pulse    => mm_pps_sys
  );

  nxt_mm_pps_toggle  <= st_pps_toggle  when mm_pps_sys = '1' else mm_pps_toggle;
  nxt_mm_capture_cnt <= st_capture_cnt when mm_pps_sys = '1' else mm_capture_cnt;

  pin_pps <= mm_pps_toggle & '0' & RESIZE_UVEC(mm_capture_cnt, 30);  -- pin_pps did not support pps_stable yet
end str;
