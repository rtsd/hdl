-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, mm_lib, eth_lib, technology_lib, tech_tse_lib, io_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use work.qsys_unb1_test_pkg.all;
use work.unb1_test_pkg.all;

entity mmm_unb1_test is
  generic (
    g_sim               : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_nof_streams_1GbE  : natural;
    g_nof_streams_10GbE : natural;
    g_nof_streams_ddr   : natural;
    g_bg_block_size     : natural
  );
  port (
    mm_rst                               : in  std_logic;
    mm_clk                               : in  std_logic;

    pout_wdi                             : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi                         : out t_mem_mosi;
    reg_wdi_miso                         : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi             : out t_mem_mosi;
    reg_unb_system_info_miso             : in  t_mem_miso;
    rom_unb_system_info_mosi             : out t_mem_mosi;
    rom_unb_system_info_miso             : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi                    : out t_mem_mosi;
    reg_unb_sens_miso                    : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi                        : out t_mem_mosi;
    reg_ppsh_miso                        : in  t_mem_miso;

    -- eth1g
    eth1g_mm_rst                         : out std_logic;
    eth1g_tse_mosi                       : out t_mem_mosi;
    eth1g_tse_miso                       : in  t_mem_miso;
    eth1g_reg_mosi                       : out t_mem_mosi;
    eth1g_reg_miso                       : in  t_mem_miso;
    eth1g_reg_interrupt                  : in  std_logic;
    eth1g_ram_mosi                       : out t_mem_mosi;
    eth1g_ram_miso                       : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi                   : out t_mem_mosi;
    reg_dpmm_data_miso                   : in  t_mem_miso;
    reg_dpmm_ctrl_mosi                   : out t_mem_mosi;
    reg_dpmm_ctrl_miso                   : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi                   : out t_mem_mosi;
    reg_mmdp_data_miso                   : in  t_mem_miso;
    reg_mmdp_ctrl_mosi                   : out t_mem_mosi;
    reg_mmdp_ctrl_miso                   : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi                        : out t_mem_mosi;
    reg_epcs_miso                        : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi                        : out t_mem_mosi;
    reg_remu_miso                        : in  t_mem_miso;

    -- block gen
    ram_diag_bg_1GbE_mosi                : out t_mem_mosi;
    ram_diag_bg_1GbE_miso                : in  t_mem_miso;
    reg_diag_bg_1GbE_mosi                : out t_mem_mosi;
    reg_diag_bg_1GbE_miso                : in  t_mem_miso;
    reg_diag_tx_seq_1GbE_mosi            : out t_mem_mosi;
    reg_diag_tx_seq_1GbE_miso            : in  t_mem_miso;

    ram_diag_bg_10GbE_mosi               : out t_mem_mosi;
    ram_diag_bg_10GbE_miso               : in  t_mem_miso;
    reg_diag_bg_10GbE_mosi               : out t_mem_mosi;
    reg_diag_bg_10GbE_miso               : in  t_mem_miso;
    reg_diag_tx_seq_10GbE_mosi           : out t_mem_mosi;
    reg_diag_tx_seq_10GbE_miso           : in  t_mem_miso;

    -- dp_offload_tx
    reg_dp_offload_tx_1GbE_mosi          : out t_mem_mosi;
    reg_dp_offload_tx_1GbE_miso          : in  t_mem_miso;
    reg_dp_offload_tx_1GbE_hdr_dat_mosi  : out t_mem_mosi;
    reg_dp_offload_tx_1GbE_hdr_dat_miso  : in  t_mem_miso;

    reg_dp_offload_tx_10GbE_mosi         : out t_mem_mosi;
    reg_dp_offload_tx_10GbE_miso         : in  t_mem_miso;
    reg_dp_offload_tx_10GbE_hdr_dat_mosi : out t_mem_mosi;
    reg_dp_offload_tx_10GbE_hdr_dat_miso : in  t_mem_miso;

    -- dp_offload_rx
    reg_dp_offload_rx_1GbE_hdr_dat_mosi  : out t_mem_mosi;
    reg_dp_offload_rx_1GbE_hdr_dat_miso  : in  t_mem_miso;
    reg_dp_offload_rx_10GbE_hdr_dat_mosi : out t_mem_mosi;
    reg_dp_offload_rx_10GbE_hdr_dat_miso : in  t_mem_miso;

    -- bsn
    reg_bsn_monitor_1GbE_mosi            : out t_mem_mosi;
    reg_bsn_monitor_1GbE_miso            : in  t_mem_miso;
    reg_bsn_monitor_10GbE_mosi           : out t_mem_mosi;
    reg_bsn_monitor_10GbE_miso           : in  t_mem_miso;

    -- databuffer
    ram_diag_data_buf_1GbE_mosi          : out t_mem_mosi;
    ram_diag_data_buf_1GbE_miso          : in  t_mem_miso;
    reg_diag_data_buf_1GbE_mosi          : out t_mem_mosi;
    reg_diag_data_buf_1GbE_miso          : in  t_mem_miso;
    reg_diag_rx_seq_1GbE_mosi            : out t_mem_mosi;
    reg_diag_rx_seq_1GbE_miso            : in  t_mem_miso;

    ram_diag_data_buf_10GbE_mosi         : out t_mem_mosi;
    ram_diag_data_buf_10GbE_miso         : in  t_mem_miso;
    reg_diag_data_buf_10GbE_mosi         : out t_mem_mosi;
    reg_diag_data_buf_10GbE_miso         : in  t_mem_miso;
    reg_diag_rx_seq_10GbE_mosi           : out t_mem_mosi;
    reg_diag_rx_seq_10GbE_miso           : in  t_mem_miso;

    -- tr_10GbE
    reg_tr_10GbE_mosi                    : out t_mem_mosi;
    reg_tr_10GbE_miso                    : in  t_mem_miso;
    reg_tr_xaui_mosi                     : out t_mem_mosi;
    reg_tr_xaui_miso                     : in  t_mem_miso;

    -- DDR3 : MB I
    reg_io_ddr_MB_I_mosi                 : out t_mem_mosi;
    reg_io_ddr_MB_I_miso                 : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_I_mosi        : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_I_miso        : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_I_mosi        : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_I_miso        : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_I_mosi      : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_I_miso      : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_I_mosi      : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_I_miso      : in  t_mem_miso;

    -- DDR3 : MB II
    reg_io_ddr_MB_II_mosi                : out t_mem_mosi;
    reg_io_ddr_MB_II_miso                : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso
  );
end mmm_unb1_test;

architecture str of mmm_unb1_test is
  -- Block generator
  constant c_ram_diag_bg_1GbE_addr_w                     : natural := ceil_log2(g_nof_streams_1GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_bg_10GbE_addr_w                    : natural := ceil_log2(g_nof_streams_10GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_ddr_addr_w              : natural := ceil_log2(2                   * pow2(ceil_log2(g_bg_block_size)));

  -- dp_offload
  constant c_reg_dp_offload_tx_adr_w                     : natural := 1;  -- Dev note: add to c_unb1_board_peripherals_mm_reg_default
  constant c_reg_dp_offload_tx_1GbE_multi_adr_w          : natural := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_tx_adr_w));
  constant c_reg_dp_offload_tx_10GbE_multi_adr_w         : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_reg_dp_offload_tx_adr_w));

  constant c_reg_dp_offload_tx_1GbE_hdr_dat_nof_words    : natural := field_nof_words(c_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_tx_1GbE_hdr_dat_adr_w        : natural := ceil_log2(c_reg_dp_offload_tx_1GbE_hdr_dat_nof_words);
  constant c_reg_dp_offload_tx_1GbE_hdr_dat_multi_adr_w  : natural := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_tx_1GbE_hdr_dat_adr_w));

  constant c_reg_dp_offload_tx_10GbE_hdr_dat_nof_words   : natural := field_nof_words(c_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_tx_10GbE_hdr_dat_adr_w       : natural := ceil_log2(c_reg_dp_offload_tx_10GbE_hdr_dat_nof_words);
  constant c_reg_dp_offload_tx_10GbE_hdr_dat_multi_adr_w : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_reg_dp_offload_tx_10GbE_hdr_dat_adr_w));

  constant c_reg_dp_offload_rx_1GbE_hdr_dat_nof_words    : natural := field_nof_words(c_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_rx_1GbE_hdr_dat_adr_w        : natural := ceil_log2(c_reg_dp_offload_rx_1GbE_hdr_dat_nof_words);
  constant c_reg_dp_offload_rx_1GbE_hdr_dat_multi_adr_w  : natural := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_rx_1GbE_hdr_dat_adr_w));

  constant c_reg_dp_offload_rx_10GbE_hdr_dat_nof_words   : natural := field_nof_words(c_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_rx_10GbE_hdr_dat_adr_w       : natural := ceil_log2(c_reg_dp_offload_rx_10GbE_hdr_dat_nof_words);
  constant c_reg_dp_offload_rx_10GbE_hdr_dat_multi_adr_w : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_reg_dp_offload_rx_10GbE_hdr_dat_adr_w));

  -- tr_10GbE
  constant c_reg_tr_10GbE_adr_w                          : natural := 13;
  constant c_reg_tr_10GbE_multi_adr_w                    : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_reg_tr_10GbE_adr_w));

  -- tr_xaui
  constant c_reg_tr_xaui_adr_w                           : natural := 9;
  constant c_reg_tr_xaui_multi_adr_w                     : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_reg_tr_xaui_adr_w));

  -- BSN monitors
  constant c_reg_rsp_bsn_monitor_1GbE_adr_w              : natural := ceil_log2(g_nof_streams_1GbE * pow2(c_unb1_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));
  constant c_reg_rsp_bsn_monitor_10GbE_adr_w             : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_unb1_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));

  constant c_dp_reg_mm_nof_words                         : natural := 1;
  constant c_dp_reg_mm_adr_w                             : natural := ceil_log2(c_eth_nof_udp_ports * pow2(ceil_log2(c_dp_reg_mm_nof_words)));

  -- Simulation
  constant c_sim_node_type                               : string(1 to 2) := sel_a_b(g_sim_node_nr < 4, "FN", "BN");
  constant c_sim_node_nr                                 : natural := sel_a_b(c_sim_node_type = "BN", g_sim_node_nr - 4, g_sim_node_nr);

  constant c_sim_eth_src_mac                             : std_logic_vector(c_network_eth_mac_slv'range) := X"00228608" & TO_UVEC(g_sim_unb_nr, c_byte_w) & TO_UVEC(g_sim_node_nr, c_byte_w);
  constant c_sim_eth_control_rx_en                       : natural := 2**c_eth_mm_reg_control_bi.rx_en;

  signal sim_eth_mm_bus_switch                           : std_logic;
  signal sim_eth_psc_access                              : std_logic;
  signal i_eth1g_reg_mosi                                : t_mem_mosi;
  signal i_eth1g_reg_miso                                : t_mem_miso;
  signal sim_eth1g_reg_mosi                              : t_mem_mosi;

  signal i_reset_n                                       : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    eth1g_mm_rst <= mm_rst;

    u_mm_file_reg_unb_system_info             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                                           port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso);

    u_mm_file_rom_unb_system_info             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                                           port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso);

    u_mm_file_reg_wdi                         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                                           port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso);

    u_mm_file_reg_unb_sens                    : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                                           port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso);

    u_mm_file_reg_ppsh                        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                                           port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso);

    u_mm_file_reg_diag_bg_1GbE                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_1GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_bg_1GbE_mosi, reg_diag_bg_1GbE_miso);
    u_mm_file_ram_diag_bg_1GbE                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG_1GBE")
                                                           port map(mm_rst, mm_clk, ram_diag_bg_1GbE_mosi, ram_diag_bg_1GbE_miso);
    u_mm_file_reg_diag_tx_seq_1GbE            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_1GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_1GbE_mosi, reg_diag_tx_seq_1GbE_miso);

    u_mm_file_reg_diag_bg_10GbE               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_10GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_bg_10GbE_mosi, reg_diag_bg_10GbE_miso);
    u_mm_file_ram_diag_bg_10GbE               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG_10GBE")
                                                           port map(mm_rst, mm_clk, ram_diag_bg_10GbE_mosi, ram_diag_bg_10GbE_miso);
    u_mm_file_reg_diag_tx_seq_10GbE           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_10GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_10GbE_mosi, reg_diag_tx_seq_10GbE_miso);

    u_mm_file_reg_dp_offload_tx_1GbE          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_1GBE")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_tx_1GbE_mosi, reg_dp_offload_tx_1GbE_miso);
    u_mm_file_reg_dp_offload_tx_10GbE         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_10GBE")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_tx_10GbE_mosi, reg_dp_offload_tx_10GbE_miso);

    u_mm_file_reg_dp_offload_tx_1GbE_hdr_dat  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_1GBE_HDR_DAT")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_tx_1GbE_hdr_dat_mosi, reg_dp_offload_tx_1GbE_hdr_dat_miso);
    u_mm_file_reg_dp_offload_tx_10GbE_hdr_dat : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_10GBE_HDR_DAT")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_tx_10GbE_hdr_dat_mosi, reg_dp_offload_tx_10GbE_hdr_dat_miso);

    u_mm_file_reg_dp_offload_rx_1GbE_hdr_dat  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_RX_1GBE_HDR_DAT")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_rx_1GbE_hdr_dat_mosi, reg_dp_offload_rx_1GbE_hdr_dat_miso);
    u_mm_file_reg_dp_offload_rx_10GbE_hdr_dat : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_RX_10GBE_HDR_DAT")
                                                           port map(mm_rst, mm_clk, reg_dp_offload_rx_10GbE_hdr_dat_mosi, reg_dp_offload_rx_10GbE_hdr_dat_miso);

    u_mm_file_reg_bsn_monitor_1GbE            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_1GBE")
                                                           port map(mm_rst, mm_clk, reg_bsn_monitor_1GbE_mosi, reg_bsn_monitor_1GbE_miso);
    u_mm_file_reg_bsn_monitor_10GbE           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_10GBE")
                                                           port map(mm_rst, mm_clk, reg_bsn_monitor_10GbE_mosi, reg_bsn_monitor_10GbE_miso);

    u_mm_file_reg_diag_data_buffer_1GbE       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_1GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_1GbE_mosi, reg_diag_data_buf_1GbE_miso);
    u_mm_file_ram_diag_data_buffer_1GbE       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_1GBE")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_1GbE_mosi, ram_diag_data_buf_1GbE_miso);
    u_mm_file_reg_diag_rx_seq_1GbE            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_1GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_1GbE_mosi, reg_diag_rx_seq_1GbE_miso);

    u_mm_file_reg_diag_data_buffer_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_10GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_10GbE_mosi, reg_diag_data_buf_10GbE_miso);
    u_mm_file_ram_diag_data_buffer_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_10GBE")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_10GbE_mosi, ram_diag_data_buf_10GbE_miso);
    u_mm_file_reg_diag_rx_seq_10GbE           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_10GBE")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_10GbE_mosi, reg_diag_rx_seq_10GbE_miso);

    u_mm_file_reg_io_ddr_MB_I                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_I_mosi, reg_io_ddr_MB_I_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_I_mosi, reg_diag_tx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_I_mosi, reg_diag_rx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_I_mosi, reg_diag_data_buf_ddr_MB_I_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_I_mosi, ram_diag_data_buf_ddr_MB_I_miso);

    u_mm_file_reg_io_ddr_MB_II                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_II_mosi, reg_io_ddr_MB_II_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_II_mosi, reg_diag_tx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_II_mosi, reg_diag_rx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_II_mosi, reg_diag_data_buf_ddr_MB_II_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_II_mosi, ram_diag_data_buf_ddr_MB_II_miso);

    u_mm_file_eth1g_reg                       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                                           port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );
    u_mm_file_eth1g_ram                       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_RAM")
                                                           port map(mm_rst, mm_clk, eth1g_ram_mosi, eth1g_ram_miso );
    u_mm_file_eth1g_tse                       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_TSE")
                                                           port map(mm_rst, mm_clk, eth1g_tse_mosi, eth1g_tse_miso );

    u_mm_file_reg_tr_10GbE                    : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE")  -- , c_mm_clk_period, FALSE, 0)
                                                           port map(mm_rst, mm_clk, reg_tr_10GbE_mosi, reg_tr_10GbE_miso);

    u_mm_file_reg_tr_xaui                     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_XAUI")  -- , c_mm_clk_period, FALSE, 0)
                                                           port map(mm_rst, mm_clk, reg_tr_xaui_mosi, reg_tr_xaui_miso);

    ----------------------------------------------------------------------------
    -- 1GbE setup sequence normally performed by unb_os@NIOS
    ----------------------------------------------------------------------------
    p_eth_setup : process
    begin
      sim_eth_mm_bus_switch <= '1';

      eth1g_tse_mosi.wr <= '0';
      eth1g_tse_mosi.rd <= '0';
      wait for 400 ns;
      wait until rising_edge(mm_clk);
      proc_tech_tse_setup(c_tech_stratixiv, false, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency, c_sim_eth_src_mac, sim_eth_psc_access, mm_clk, eth1g_tse_miso, eth1g_tse_mosi);

      -- Enable RX
      proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_sim_eth_control_rx_en, mm_clk, eth1g_reg_miso, sim_eth1g_reg_mosi);  -- control rx en
      sim_eth_mm_bus_switch <= '0';

      wait;
    end process;

    p_switch : process(sim_eth_mm_bus_switch, sim_eth1g_reg_mosi, i_eth1g_reg_mosi)
    begin
      if sim_eth_mm_bus_switch = '1' then
          eth1g_reg_mosi <= sim_eth1g_reg_mosi;
        else
          eth1g_reg_mosi <= i_eth1g_reg_mosi;
        end if;
    end process;

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  ----------------------------------------------------------------------------
  -- QSYS
  ----------------------------------------------------------------------------
  i_reset_n <= not mm_rst;

  gen_qsys : if g_sim = false generate
    u_qsys : qsys_unb1_test
    port map (
      clk_0                                         => mm_clk,
      reset_n                                       => i_reset_n,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0             => OPEN,
      coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_reg_unb_sens
      coe_clk_export_from_the_reg_unb_sens          => OPEN,
      coe_reset_export_from_the_reg_unb_sens        => OPEN,
      coe_address_export_from_the_reg_unb_sens      => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_unb_sens         => reg_unb_sens_mosi.rd,
      coe_readdata_export_to_the_reg_unb_sens       => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_unb_sens        => reg_unb_sens_mosi.wr,
      coe_writedata_export_from_the_reg_unb_sens    => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dpmm_data
      coe_clk_export_from_the_reg_dpmm_data         => OPEN,
      coe_reset_export_from_the_reg_dpmm_data       => OPEN,
      coe_address_export_from_the_reg_dpmm_data     => reg_dpmm_data_mosi.address(0 downto 0),
      coe_read_export_from_the_reg_dpmm_data        => reg_dpmm_data_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_data      => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_data       => reg_dpmm_data_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_data   => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dpmm_ctrl
      coe_clk_export_from_the_reg_dpmm_ctrl         => OPEN,
      coe_reset_export_from_the_reg_dpmm_ctrl       => OPEN,
      coe_address_export_from_the_reg_dpmm_ctrl     => reg_dpmm_ctrl_mosi.address(0 downto 0),
      coe_read_export_from_the_reg_dpmm_ctrl        => reg_dpmm_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_ctrl      => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_ctrl       => reg_dpmm_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_ctrl   => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_data
      coe_clk_export_from_the_reg_mmdp_data         => OPEN,
      coe_reset_export_from_the_reg_mmdp_data       => OPEN,
      coe_address_export_from_the_reg_mmdp_data     => reg_mmdp_data_mosi.address(0 downto 0),
      coe_read_export_from_the_reg_mmdp_data        => reg_mmdp_data_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_data      => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_data       => reg_mmdp_data_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_data   => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_ctrl
      coe_clk_export_from_the_reg_mmdp_ctrl         => OPEN,
      coe_reset_export_from_the_reg_mmdp_ctrl       => OPEN,
      coe_address_export_from_the_reg_mmdp_ctrl     => reg_mmdp_ctrl_mosi.address(0 downto 0),
      coe_read_export_from_the_reg_mmdp_ctrl        => reg_mmdp_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_ctrl      => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_ctrl       => reg_mmdp_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_ctrl   => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_epcs
      coe_clk_export_from_the_reg_epcs              => OPEN,
      coe_reset_export_from_the_reg_epcs            => OPEN,
      coe_address_export_from_the_reg_epcs          => reg_epcs_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_epcs             => reg_epcs_mosi.rd,
      coe_readdata_export_to_the_reg_epcs           => reg_epcs_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_epcs            => reg_epcs_mosi.wr,
      coe_writedata_export_from_the_reg_epcs        => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_remu
      coe_clk_export_from_the_reg_remu              => OPEN,
      coe_reset_export_from_the_reg_remu            => OPEN,
      coe_address_export_from_the_reg_remu          => reg_remu_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_remu             => reg_remu_mosi.rd,
      coe_readdata_export_to_the_reg_remu           => reg_remu_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_remu            => reg_remu_mosi.wr,
      coe_writedata_export_from_the_reg_remu        => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_pps
      coe_clk_export_from_the_pio_pps               => OPEN,
      coe_reset_export_from_the_pio_pps             => OPEN,
      coe_address_export_from_the_pio_pps           => reg_ppsh_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),  -- 1 bit address width so must use (0) instead of (0 DOWNTO 0)
      coe_read_export_from_the_pio_pps              => reg_ppsh_mosi.rd,
      coe_readdata_export_to_the_pio_pps            => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_pps             => reg_ppsh_mosi.wr,
      coe_writedata_export_from_the_pio_pps         => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_system_info: actually a avs_common_mm instance
      coe_clk_export_from_the_pio_system_info       => OPEN,
      coe_reset_export_from_the_pio_system_info     => OPEN,
      coe_address_export_from_the_pio_system_info   => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_pio_system_info      => reg_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_pio_system_info    => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_system_info     => reg_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_pio_system_info => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_rom_system_info
      coe_clk_export_from_the_rom_system_info       => OPEN,
      coe_reset_export_from_the_rom_system_info     => OPEN,
      coe_address_export_from_the_rom_system_info   => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_rom_system_info      => rom_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_rom_system_info    => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_rom_system_info     => rom_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_rom_system_info => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb1_board.
      out_port_from_the_pio_wdi                     => pout_wdi,

      -- the_reg_wdi: Manual WDI override; causes FPGA reconfiguration if WDI is enabled (g_use_phy).
      coe_clk_export_from_the_reg_wdi               => OPEN,
      coe_reset_export_from_the_reg_wdi             => OPEN,
      coe_address_export_from_the_reg_wdi           => reg_wdi_mosi.address(0 downto 0),
      coe_read_export_from_the_reg_wdi              => reg_wdi_mosi.rd,
      coe_readdata_export_to_the_reg_wdi            => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_wdi             => reg_wdi_mosi.wr,
      coe_writedata_export_from_the_reg_wdi         => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_tr_10GbE
      coe_clk_export_from_the_reg_tr_10GbE          => OPEN,
      coe_reset_export_from_the_reg_tr_10GbE        => OPEN,
      coe_address_export_from_the_reg_tr_10GbE      => reg_tr_10GbE_mosi.address(c_reg_tr_10GbE_multi_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_tr_10GbE         => reg_tr_10GbE_mosi.rd,
      coe_readdata_export_to_the_reg_tr_10GbE       => reg_tr_10GbE_miso.rddata(c_word_w - 1 downto 0),
      coe_waitrequest_export_to_the_reg_tr_10GbE    => reg_tr_10GbE_miso.waitrequest,
      coe_write_export_from_the_reg_tr_10GbE        => reg_tr_10GbE_mosi.wr,
      coe_writedata_export_from_the_reg_tr_10GbE    => reg_tr_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_tr_xaui
      coe_clk_export_from_the_reg_tr_xaui           => OPEN,
      coe_reset_export_from_the_reg_tr_xaui         => OPEN,
      coe_address_export_from_the_reg_tr_xaui       => reg_tr_xaui_mosi.address(c_reg_tr_xaui_multi_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_tr_xaui          => reg_tr_xaui_mosi.rd,
      coe_readdata_export_to_the_reg_tr_xaui        => reg_tr_xaui_miso.rddata(c_word_w - 1 downto 0),
      coe_waitrequest_export_to_the_reg_tr_xaui     => reg_tr_xaui_miso.waitrequest,
      coe_write_export_from_the_reg_tr_xaui         => reg_tr_xaui_mosi.wr,
      coe_writedata_export_from_the_reg_tr_xaui     => reg_tr_xaui_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_diag_bg_1GbE
      reg_diag_bg_1GbE_address_export               => reg_diag_bg_1GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      reg_diag_bg_1GbE_clk_export                   => OPEN,
      reg_diag_bg_1GbE_read_export                  => reg_diag_bg_1GbE_mosi.rd,
      reg_diag_bg_1GbE_readdata_export              => reg_diag_bg_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_1GbE_reset_export                 => OPEN,
      reg_diag_bg_1GbE_write_export                 => reg_diag_bg_1GbE_mosi.wr,
      reg_diag_bg_1GbE_writedata_export             => reg_diag_bg_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_ram_diag_bg_1GbE
      ram_diag_bg_1GbE_address_export               => ram_diag_bg_1GbE_mosi.address(c_ram_diag_bg_1GbE_addr_w - 1 downto 0),
      ram_diag_bg_1GbE_clk_export                   => OPEN,
      ram_diag_bg_1GbE_read_export                  => ram_diag_bg_1GbE_mosi.rd,
      ram_diag_bg_1GbE_readdata_export              => ram_diag_bg_1GbE_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_1GbE_reset_export                 => OPEN,
      ram_diag_bg_1GbE_write_export                 => ram_diag_bg_1GbE_mosi.wr,
      ram_diag_bg_1GbE_writedata_export             => ram_diag_bg_1GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_diag_bg_10GbE
      reg_diag_bg_10GbE_address_export              => reg_diag_bg_10GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      reg_diag_bg_10GbE_clk_export                  => OPEN,
      reg_diag_bg_10GbE_read_export                 => reg_diag_bg_10GbE_mosi.rd,
      reg_diag_bg_10GbE_readdata_export             => reg_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_10GbE_reset_export                => OPEN,
      reg_diag_bg_10GbE_write_export                => reg_diag_bg_10GbE_mosi.wr,
      reg_diag_bg_10GbE_writedata_export            => reg_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_ram_diag_bg_10GbE
      ram_diag_bg_10GbE_address_export              => ram_diag_bg_10GbE_mosi.address(c_ram_diag_bg_10GbE_addr_w - 1 downto 0),
      ram_diag_bg_10GbE_clk_export                  => OPEN,
      ram_diag_bg_10GbE_read_export                 => ram_diag_bg_10GbE_mosi.rd,
      ram_diag_bg_10GbE_readdata_export             => ram_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_10GbE_reset_export                => OPEN,
      ram_diag_bg_10GbE_write_export                => ram_diag_bg_10GbE_mosi.wr,
      ram_diag_bg_10GbE_writedata_export            => ram_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dp_offload_tx_1GbE
      reg_dp_offload_tx_1GbE_address_export         => reg_dp_offload_tx_1GbE_mosi.address(c_reg_dp_offload_tx_1GbE_multi_adr_w - 1 downto 0),
      reg_dp_offload_tx_1GbE_clk_export             => OPEN,
      reg_dp_offload_tx_1GbE_read_export            => reg_dp_offload_tx_1GbE_mosi.rd,
      reg_dp_offload_tx_1GbE_readdata_export        => reg_dp_offload_tx_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_1GbE_reset_export           => OPEN,
      reg_dp_offload_tx_1GbE_write_export           => reg_dp_offload_tx_1GbE_mosi.wr,
      reg_dp_offload_tx_1GbE_writedata_export       => reg_dp_offload_tx_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_reg_dp_offload_tx_10GbE
      reg_dp_offload_tx_10GbE_address_export        => reg_dp_offload_tx_10GbE_mosi.address(c_reg_dp_offload_tx_10GbE_multi_adr_w - 1 downto 0),
      reg_dp_offload_tx_10GbE_clk_export            => OPEN,
      reg_dp_offload_tx_10GbE_read_export           => reg_dp_offload_tx_10GbE_mosi.rd,
      reg_dp_offload_tx_10GbE_readdata_export       => reg_dp_offload_tx_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_10GbE_reset_export          => OPEN,
      reg_dp_offload_tx_10GbE_write_export          => reg_dp_offload_tx_10GbE_mosi.wr,
      reg_dp_offload_tx_10GbE_writedata_export      => reg_dp_offload_tx_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dp_offload_tx_1GbE_hdr_dat
      reg_dp_offload_tx_1GbE_hdr_dat_address_export    => reg_dp_offload_tx_1GbE_hdr_dat_mosi.address(c_reg_dp_offload_tx_1GbE_hdr_dat_multi_adr_w - 1 downto 0),
      reg_dp_offload_tx_1GbE_hdr_dat_clk_export        => OPEN,
      reg_dp_offload_tx_1GbE_hdr_dat_read_export       => reg_dp_offload_tx_1GbE_hdr_dat_mosi.rd,
      reg_dp_offload_tx_1GbE_hdr_dat_readdata_export   => reg_dp_offload_tx_1GbE_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_1GbE_hdr_dat_reset_export      => OPEN,
      reg_dp_offload_tx_1GbE_hdr_dat_write_export      => reg_dp_offload_tx_1GbE_hdr_dat_mosi.wr,
      reg_dp_offload_tx_1GbE_hdr_dat_writedata_export  => reg_dp_offload_tx_1GbE_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_reg_dp_offload_tx_10GbE_hdr_dat
      reg_dp_offload_tx_10GbE_hdr_dat_address_export   => reg_dp_offload_tx_10GbE_hdr_dat_mosi.address(c_reg_dp_offload_tx_10GbE_hdr_dat_multi_adr_w - 1 downto 0),
      reg_dp_offload_tx_10GbE_hdr_dat_clk_export       => OPEN,
      reg_dp_offload_tx_10GbE_hdr_dat_read_export      => reg_dp_offload_tx_10GbE_hdr_dat_mosi.rd,
      reg_dp_offload_tx_10GbE_hdr_dat_readdata_export  => reg_dp_offload_tx_10GbE_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_tx_10GbE_hdr_dat_reset_export     => OPEN,
      reg_dp_offload_tx_10GbE_hdr_dat_write_export     => reg_dp_offload_tx_10GbE_hdr_dat_mosi.wr,
      reg_dp_offload_tx_10GbE_hdr_dat_writedata_export => reg_dp_offload_tx_10GbE_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dp_offload_rx_1GbE_hdr_dat
      reg_dp_offload_rx_1GbE_hdr_dat_address_export    => reg_dp_offload_rx_1GbE_hdr_dat_mosi.address(c_reg_dp_offload_rx_1GbE_hdr_dat_multi_adr_w - 1 downto 0),
      reg_dp_offload_rx_1GbE_hdr_dat_clk_export        => OPEN,
      reg_dp_offload_rx_1GbE_hdr_dat_read_export       => reg_dp_offload_rx_1GbE_hdr_dat_mosi.rd,
      reg_dp_offload_rx_1GbE_hdr_dat_readdata_export   => reg_dp_offload_rx_1GbE_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_rx_1GbE_hdr_dat_reset_export      => OPEN,
      reg_dp_offload_rx_1GbE_hdr_dat_write_export      => reg_dp_offload_rx_1GbE_hdr_dat_mosi.wr,
      reg_dp_offload_rx_1GbE_hdr_dat_writedata_export  => reg_dp_offload_rx_1GbE_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_reg_dp_offload_rx_10GbE_hdr_dat
      reg_dp_offload_rx_10GbE_hdr_dat_address_export   => reg_dp_offload_rx_10GbE_hdr_dat_mosi.address(c_reg_dp_offload_rx_10GbE_hdr_dat_multi_adr_w - 1 downto 0),
      reg_dp_offload_rx_10GbE_hdr_dat_clk_export       => OPEN,
      reg_dp_offload_rx_10GbE_hdr_dat_read_export      => reg_dp_offload_rx_10GbE_hdr_dat_mosi.rd,
      reg_dp_offload_rx_10GbE_hdr_dat_readdata_export  => reg_dp_offload_rx_10GbE_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_offload_rx_10GbE_hdr_dat_reset_export     => OPEN,
      reg_dp_offload_rx_10GbE_hdr_dat_write_export     => reg_dp_offload_rx_10GbE_hdr_dat_mosi.wr,
      reg_dp_offload_rx_10GbE_hdr_dat_writedata_export => reg_dp_offload_rx_10GbE_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_bsn_monitor_1GbE
      reg_bsn_monitor_1GbE_address_export              => reg_bsn_monitor_1GbE_mosi.address(c_reg_rsp_bsn_monitor_1GbE_adr_w - 1 downto 0),
      reg_bsn_monitor_1GbE_clk_export                  => OPEN,
      reg_bsn_monitor_1GbE_read_export                 => reg_bsn_monitor_1GbE_mosi.rd,
      reg_bsn_monitor_1GbE_readdata_export             => reg_bsn_monitor_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_1GbE_reset_export                => OPEN,
      reg_bsn_monitor_1GbE_write_export                => reg_bsn_monitor_1GbE_mosi.wr,
      reg_bsn_monitor_1GbE_writedata_export            => reg_bsn_monitor_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_reg_bsn_monitor_10GbE
      reg_bsn_monitor_10GbE_address_export             => reg_bsn_monitor_10GbE_mosi.address(c_reg_rsp_bsn_monitor_10GbE_adr_w - 1 downto 0),
      reg_bsn_monitor_10GbE_clk_export                 => OPEN,
      reg_bsn_monitor_10GbE_read_export                => reg_bsn_monitor_10GbE_mosi.rd,
      reg_bsn_monitor_10GbE_readdata_export            => reg_bsn_monitor_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_10GbE_reset_export               => OPEN,
      reg_bsn_monitor_10GbE_write_export               => reg_bsn_monitor_10GbE_mosi.wr,
      reg_bsn_monitor_10GbE_writedata_export           => reg_bsn_monitor_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_ram_diag_data_buffer_1GbE
      ram_diag_data_buffer_1GbE_address_export         => ram_diag_data_buf_1GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_diag_db_adr_w - 1 downto 0),
      ram_diag_data_buffer_1GbE_clk_export             => OPEN,
      ram_diag_data_buffer_1GbE_read_export            => ram_diag_data_buf_1GbE_mosi.rd,
      ram_diag_data_buffer_1GbE_readdata_export        => ram_diag_data_buf_1GbE_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_1GbE_reset_export           => OPEN,
      ram_diag_data_buffer_1GbE_write_export           => ram_diag_data_buf_1GbE_mosi.wr,
      ram_diag_data_buffer_1GbE_writedata_export       => ram_diag_data_buf_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_ram_diag_data_buffer_10GbE
      ram_diag_data_buffer_10GbE_address_export        => ram_diag_data_buf_10GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_diag_db_adr_w - 1 downto 0),
      ram_diag_data_buffer_10GbE_clk_export            => OPEN,
      ram_diag_data_buffer_10GbE_read_export           => ram_diag_data_buf_10GbE_mosi.rd,
      ram_diag_data_buffer_10GbE_readdata_export       => ram_diag_data_buf_10GbE_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_10GbE_reset_export          => OPEN,
      ram_diag_data_buffer_10GbE_write_export          => ram_diag_data_buf_10GbE_mosi.wr,
      ram_diag_data_buffer_10GbE_writedata_export      => ram_diag_data_buf_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_diag_data_buffer_1GbE
      reg_diag_data_buffer_1GbE_address_export         => reg_diag_data_buf_1GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_1GbE_clk_export             => OPEN,
      reg_diag_data_buffer_1GbE_read_export            => reg_diag_data_buf_1GbE_mosi.rd,
      reg_diag_data_buffer_1GbE_readdata_export        => reg_diag_data_buf_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_1GbE_reset_export           => OPEN,
      reg_diag_data_buffer_1GbE_write_export           => reg_diag_data_buf_1GbE_mosi.wr,
      reg_diag_data_buffer_1GbE_writedata_export       => reg_diag_data_buf_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      -- the_reg_diag_data_buffer_10GbE
      reg_diag_data_buffer_10GbE_address_export        => reg_diag_data_buf_10GbE_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_10GbE_clk_export            => OPEN,
      reg_diag_data_buffer_10GbE_read_export           => reg_diag_data_buf_10GbE_mosi.rd,
      reg_diag_data_buffer_10GbE_readdata_export       => reg_diag_data_buf_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_10GbE_reset_export          => OPEN,
      reg_diag_data_buffer_10GbE_write_export          => reg_diag_data_buf_10GbE_mosi.wr,
      reg_diag_data_buffer_10GbE_writedata_export      => reg_diag_data_buf_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- reg_diag_tx_seq_10GbE
      reg_diag_tx_seq_10GbE_address_export             => reg_diag_tx_seq_10GbE_mosi.address(4 - 1 downto 0),
      reg_diag_tx_seq_10GbE_clk_export                 => OPEN,
      reg_diag_tx_seq_10GbE_read_export                => reg_diag_tx_seq_10GbE_mosi.rd,
      reg_diag_tx_seq_10GbE_readdata_export            => reg_diag_tx_seq_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_10GbE_reset_export               => OPEN,
      reg_diag_tx_seq_10GbE_write_export               => reg_diag_tx_seq_10GbE_mosi.wr,
      reg_diag_tx_seq_10GbE_writedata_export           => reg_diag_tx_seq_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- reg_diag_rx_seq_10GbE
      reg_diag_rx_seq_10GbE_address_export             => reg_diag_rx_seq_10GbE_mosi.address(5 - 1 downto 0),
      reg_diag_rx_seq_10GbE_clk_export                 => OPEN,
      reg_diag_rx_seq_10GbE_read_export                => reg_diag_rx_seq_10GbE_mosi.rd,
      reg_diag_rx_seq_10GbE_readdata_export            => reg_diag_rx_seq_10GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_10GbE_reset_export               => OPEN,
      reg_diag_rx_seq_10GbE_write_export               => reg_diag_rx_seq_10GbE_mosi.wr,
      reg_diag_rx_seq_10GbE_writedata_export           => reg_diag_rx_seq_10GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- reg_diag_tx_seq_1GbE
      reg_diag_tx_seq_1GbE_address_export              => reg_diag_tx_seq_1GbE_mosi.address(2 - 1 downto 0),
      reg_diag_tx_seq_1GbE_clk_export                  => OPEN,
      reg_diag_tx_seq_1GbE_read_export                 => reg_diag_tx_seq_1GbE_mosi.rd,
      reg_diag_tx_seq_1GbE_readdata_export             => reg_diag_tx_seq_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_1GbE_reset_export                => OPEN,
      reg_diag_tx_seq_1GbE_write_export                => reg_diag_tx_seq_1GbE_mosi.wr,
      reg_diag_tx_seq_1GbE_writedata_export            => reg_diag_tx_seq_1GbE_mosi.wrdata(c_word_w - 1 downto 0),

      -- reg_diag_rx_seq_1GbE
      reg_diag_rx_seq_1GbE_address_export              => reg_diag_rx_seq_1GbE_mosi.address(3 - 1 downto 0),
      reg_diag_rx_seq_1GbE_clk_export                  => OPEN,
      reg_diag_rx_seq_1GbE_read_export                 => reg_diag_rx_seq_1GbE_mosi.rd,
      reg_diag_rx_seq_1GbE_readdata_export             => reg_diag_rx_seq_1GbE_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_1GbE_reset_export                => OPEN,
      reg_diag_rx_seq_1GbE_write_export                => reg_diag_rx_seq_1GbE_mosi.wr,
      reg_diag_rx_seq_1GbE_writedata_export            => reg_diag_rx_seq_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_I_address_export                   => reg_io_ddr_MB_I_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_I_clk_export                       => OPEN,
      reg_io_ddr_MB_I_read_export                      => reg_io_ddr_MB_I_mosi.rd,
      reg_io_ddr_MB_I_readdata_export                  => reg_io_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_I_reset_export                     => OPEN,
      reg_io_ddr_MB_I_write_export                     => reg_io_ddr_MB_I_mosi.wr,
      reg_io_ddr_MB_I_writedata_export                 => reg_io_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),

      reg_io_ddr_MB_II_address_export                  => reg_io_ddr_MB_II_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_II_clk_export                      => OPEN,
      reg_io_ddr_MB_II_read_export                     => reg_io_ddr_MB_II_mosi.rd,
      reg_io_ddr_MB_II_readdata_export                 => reg_io_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_II_reset_export                    => OPEN,
      reg_io_ddr_MB_II_write_export                    => reg_io_ddr_MB_II_mosi.wr,
      reg_io_ddr_MB_II_writedata_export                => reg_io_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_I_reset_export            => OPEN,
      reg_diag_tx_seq_ddr_MB_I_clk_export              => OPEN,
      reg_diag_tx_seq_ddr_MB_I_address_export          => reg_diag_tx_seq_ddr_MB_I_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_write_export            => reg_diag_tx_seq_ddr_MB_I_mosi.wr,
      reg_diag_tx_seq_ddr_MB_I_writedata_export        => reg_diag_tx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_read_export             => reg_diag_tx_seq_ddr_MB_I_mosi.rd,
      reg_diag_tx_seq_ddr_MB_I_readdata_export         => reg_diag_tx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_II_reset_export           => OPEN,
      reg_diag_tx_seq_ddr_MB_II_clk_export             => OPEN,
      reg_diag_tx_seq_ddr_MB_II_address_export         => reg_diag_tx_seq_ddr_MB_II_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_write_export           => reg_diag_tx_seq_ddr_MB_II_mosi.wr,
      reg_diag_tx_seq_ddr_MB_II_writedata_export       => reg_diag_tx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_read_export            => reg_diag_tx_seq_ddr_MB_II_mosi.rd,
      reg_diag_tx_seq_ddr_MB_II_readdata_export        => reg_diag_tx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_I_reset_export            => OPEN,
      reg_diag_rx_seq_ddr_MB_I_clk_export              => OPEN,
      reg_diag_rx_seq_ddr_MB_I_address_export          => reg_diag_rx_seq_ddr_MB_I_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_write_export            => reg_diag_rx_seq_ddr_MB_I_mosi.wr,
      reg_diag_rx_seq_ddr_MB_I_writedata_export        => reg_diag_rx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_read_export             => reg_diag_rx_seq_ddr_MB_I_mosi.rd,
      reg_diag_rx_seq_ddr_MB_I_readdata_export         => reg_diag_rx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_II_reset_export           => OPEN,
      reg_diag_rx_seq_ddr_MB_II_clk_export             => OPEN,
      reg_diag_rx_seq_ddr_MB_II_address_export         => reg_diag_rx_seq_ddr_MB_II_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_write_export           => reg_diag_rx_seq_ddr_MB_II_mosi.wr,
      reg_diag_rx_seq_ddr_MB_II_writedata_export       => reg_diag_rx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_read_export            => reg_diag_rx_seq_ddr_MB_II_mosi.rd,
      reg_diag_rx_seq_ddr_MB_II_readdata_export        => reg_diag_rx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_I_reset_export       => OPEN,
      reg_diag_data_buffer_ddr_MB_I_clk_export         => OPEN,
      reg_diag_data_buffer_ddr_MB_I_address_export     => reg_diag_data_buf_ddr_MB_I_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_write_export       => reg_diag_data_buf_ddr_MB_I_mosi.wr,
      reg_diag_data_buffer_ddr_MB_I_writedata_export   => reg_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_read_export        => reg_diag_data_buf_ddr_MB_I_mosi.rd,
      reg_diag_data_buffer_ddr_MB_I_readdata_export    => reg_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_II_reset_export      => OPEN,
      reg_diag_data_buffer_ddr_MB_II_clk_export        => OPEN,
      reg_diag_data_buffer_ddr_MB_II_address_export    => reg_diag_data_buf_ddr_MB_II_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_write_export      => reg_diag_data_buf_ddr_MB_II_mosi.wr,
      reg_diag_data_buffer_ddr_MB_II_writedata_export  => reg_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_read_export       => reg_diag_data_buf_ddr_MB_II_mosi.rd,
      reg_diag_data_buffer_ddr_MB_II_readdata_export   => reg_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_I_clk_export         => OPEN,
      ram_diag_data_buffer_ddr_MB_I_reset_export       => OPEN,
      ram_diag_data_buffer_ddr_MB_I_address_export     => ram_diag_data_buf_ddr_MB_I_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_write_export       => ram_diag_data_buf_ddr_MB_I_mosi.wr,
      ram_diag_data_buffer_ddr_MB_I_writedata_export   => ram_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_read_export        => ram_diag_data_buf_ddr_MB_I_mosi.rd,
      ram_diag_data_buffer_ddr_MB_I_readdata_export    => ram_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_II_clk_export        => OPEN,
      ram_diag_data_buffer_ddr_MB_II_reset_export      => OPEN,
      ram_diag_data_buffer_ddr_MB_II_address_export    => ram_diag_data_buf_ddr_MB_II_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_write_export      => ram_diag_data_buf_ddr_MB_II_mosi.wr,
      ram_diag_data_buffer_ddr_MB_II_writedata_export  => ram_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_read_export       => ram_diag_data_buf_ddr_MB_II_mosi.rd,
      ram_diag_data_buffer_ddr_MB_II_readdata_export   => ram_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0)
      );
  end generate;
end str;
