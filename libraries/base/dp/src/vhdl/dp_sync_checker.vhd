--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Checks if the number of SOPs within a sync interval is as expected and
--          corrects if sync is too early or too late.

--
-- Description: The dp_sync_checker distinguishes three situations:
--
--              1. SYNC is ok
--              In case the sync interval is as expected the input data is simply
--              passed through to the output.
--
--              2. SYNC is too early
--              When the sync is too early the data is still passed to the output,
--              unitl the specief number of packets (g_nof_blk_per_sync) have been
--              send. The SYNC signal that was too early is suppressed and will not
--              be visible at the output. Once the g_nof_blk_per_sync packets have
--              been send to the output the dp_sync_checker will wait until a new
--              sync has arrived.
--
--              3. SYNC is too late
--              When an expected SYNC is not found at the input the dp_sync_checker
--              will stop passing data to the output and wait for the next SYNC on
--              the input.
--
-- Remarks: Parsing of backpressure is not yet implemented,  since it was not
--          required for the first purpose of this block (in reorder_transpose).
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_sync_checker is
  generic(
    g_nof_blk_per_sync : natural := 16
  );
  port (
    dp_clk                : in  std_logic;
    dp_rst                : in  std_logic;

    -- ST sinks
    snk_out               : out t_dp_siso := c_dp_siso_rdy;
    snk_in                : in  t_dp_sosi;
    -- ST source
    src_in                : in  t_dp_siso := c_dp_siso_rdy;
    src_out               : out t_dp_sosi;

    nof_early_syncs       : out std_logic_vector(c_word_w - 1 downto 0);
    nof_late_syncs        : out std_logic_vector(c_word_w - 1 downto 0);

    clear_nof_early_syncs : in  std_logic := '0';
    clear_nof_late_syncs  : in  std_logic := '0'
  );
end dp_sync_checker;

architecture str of dp_sync_checker is
  constant c_nof_blk_per_sync : natural := g_nof_blk_per_sync - 1;

  type reg_type is record
    snk_in             : t_dp_sosi;
    src_out            : t_dp_sosi;
    cnt_sop            : natural;
    sync_too_early     : std_logic;
    wait_for_next_sync : std_logic;
    nof_early_syncs    : natural;
    nof_late_syncs     : natural;
  end record;

  signal r, rin      : reg_type;
begin
  snk_out <= c_dp_siso_rdy;

  p_comb : process(r, dp_rst, snk_in )
    variable v : reg_type;
  begin
    v := r;

    v.snk_in := snk_in;

    if(snk_in.sync = '1') then
      v.wait_for_next_sync := '0';
      v.sync_too_early     := '0';
    end if;

    -- Check on incoming SOP if things are OK
    if (snk_in.sop = '1') then
      if(r.wait_for_next_sync = '0') then
        v.cnt_sop := r.cnt_sop + 1;
      end if;
      if(r.cnt_sop = c_nof_blk_per_sync) then
        v.cnt_sop := 0;
        if(r.sync_too_early = '1') then
          -- Too early
          v.wait_for_next_sync := '1';
        elsif(snk_in.sync = '0') then
          -- Too late
          v.wait_for_next_sync := '1';
          v.nof_late_syncs     := r.nof_late_syncs + 1;
        end if;
      end if;
    end if;

    -- Detect SYNC that is too early
    if (snk_in.sync = '1' and r.cnt_sop < c_nof_blk_per_sync and r.wait_for_next_sync = '0') then
      v.sync_too_early  := '1';
      v.snk_in.sync     := '0';  -- Remove sync from input.
      v.nof_early_syncs := r.nof_early_syncs + 1;
    end if;

    -- Only pass input to output when not waiting for valid SYNC.
    if(r.wait_for_next_sync = '0') then
      v.src_out := r.snk_in;
    else
      v.src_out := c_dp_sosi_rst;
    end if;

    -- Reset the early and late sync counter
    if(clear_nof_early_syncs = '1') then
      v.nof_early_syncs := 0;
    end if;

    if(clear_nof_late_syncs = '1') then
      v.nof_late_syncs := 0;
    end if;

    if(dp_rst = '1') then
      v.snk_in             := c_dp_sosi_rst;
      v.src_out            := c_dp_sosi_rst;
      v.cnt_sop            := 0;
      v.sync_too_early     := '0';
      v.wait_for_next_sync := '1';
      v.nof_early_syncs    := 0;
      v.nof_late_syncs     := 0;
    end if;

    rin <= v;
  end process;

  p_regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  src_out         <= r.src_out;
  nof_early_syncs <= TO_UVEC(r.nof_early_syncs, c_word_w);
  nof_late_syncs  <= TO_UVEC(r.nof_late_syncs, c_word_w);
end str;
