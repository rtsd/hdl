-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.i2c_smbus_pkg.all;
use work.i2c_dev_max1617_pkg.all;
use work.i2c_dev_max6652_pkg.all;
use work.i2c_commander_pkg.all;

package i2c_dev_unb_pkg is
  -- I2C slave addresses of the devices on the I2C bus on UniBoard
  constant I2C_UNB_MAX1617_ADR     : natural := MAX1617_ADR_LOW_LOW;  -- temperature sensor, slave address is "0011000"
  constant I2C_UNB_MAX6652_ADR     : natural := MAX6652_ADR_GND;  -- temperature-voltage sensor, slave address is "0010100"

  constant c_i2c_unb_temp_high     : natural := 85;

  -- Commander protocol lists for UNB control

  -- > Temperature sensor:
  --       . c_i2c_unb_protocol_list_read_config
  --       . c_i2c_unb_protocol_list_read

  constant c_i2c_unb_max1617_expected_mask_read_config   : std_logic_vector := c_i2c_cmdr_expected_mask_read_one_byte;
  constant c_i2c_unb_max1617_nof_result_data_read_config : natural := c_i2c_cmdr_nof_result_data_read_one_byte;
  constant c_i2c_unb_max1617_protocol_list_read_config   : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  I2C_UNB_MAX1617_ADR, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_WRITE_BYTE, I2C_UNB_MAX1617_ADR, MAX1617_CMD_WRITE_CONFIG, MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, I2C_UNB_MAX1617_ADR, MAX1617_CMD_WRITE_REMOTE_HIGH, c_i2c_unb_temp_high,
    SMBUS_C_END
  );

  constant c_i2c_unb_max1617_expected_mask_read_temp   : std_logic_vector := c_i2c_cmdr_expected_mask_read_one_byte;
  constant c_i2c_unb_max1617_nof_result_data_read_temp : natural := c_i2c_cmdr_nof_result_data_read_one_byte;
  constant c_i2c_unb_max1617_protocol_list_read_temp   : t_nat_natural_arr := (
    SMBUS_READ_BYTE, I2C_UNB_MAX1617_ADR, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_C_END
  );

  -- > Temperature-Voltage sensor:
  --       . c_i2c_unb_max6652_protocol_list_read_config

  constant c_i2c_unb_max6652_expected_mask_read_config   : std_logic_vector := RESIZE_UVEC("001010101", c_word_w);
  constant c_i2c_unb_max6652_nof_result_data_read_config : natural := 4;
  constant c_i2c_unb_max6652_protocol_list_read_config   : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  I2C_UNB_MAX6652_ADR, MAX6652_REG_READ_VIN_2_5,
    SMBUS_READ_BYTE,  I2C_UNB_MAX6652_ADR, MAX6652_REG_READ_VIN_3_3,
    SMBUS_READ_BYTE,  I2C_UNB_MAX6652_ADR, MAX6652_REG_READ_VCC,
    SMBUS_READ_BYTE,  I2C_UNB_MAX6652_ADR, MAX6652_REG_READ_TEMP,
    SMBUS_WRITE_BYTE, I2C_UNB_MAX6652_ADR, MAX6652_REG_CONFIG, MAX6652_CONFIG_LINE_FREQ_SEL + MAX6652_CONFIG_START,
    SMBUS_C_END
  );
end package;
