-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: J Hargreaves, E. Kooistra
-- Purpose: Tb for tech_jesd204b IP
-- Description:
--   Includes 3 JESD transmit sources to test multichannel syncronization
--   Relative delays between TX and RX channels can be varied by editing c_delay_*
--   Used default technology e1sg
-- Remark:
--   The self checking is minimal. It only checks that Tx data from the DAC in
--   the tb does come out the JESD Rx. Some more data and strobe testing would
--   be nice, but is not needed because it works on HW.
-- Usage:
--   Load sim    # check that design can load in vsim
--   > as 3      # check that the hierarchy for g_design_name is complete (or use do wave_tb_tech_jesd204b.do)
--   > run -a    # enough time to reset and syncronize the JESD IP

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_jesd204b_component_pkg.all;
use work.tech_jesd204b_pkg.all;

entity tb_tech_jesd204b is
end tb_tech_jesd204b;

architecture tb of tb_tech_jesd204b is
  -- FALSE is use mm_clk in JESD IP, TRUE is use rxlink_clk as mm_clk in JESD IP
  constant c_sim                      : boolean := false;  -- use FALSE as on HW, to verify jesd204b_rddata

  constant c_jesd204b_sampclk_period  : time := 5 ns;
  constant c_bondingclk_period        : time := 10 ns;
  constant c_sysref_period            : natural := 10000;  -- number of sample clocks between sysref pulses
  constant c_sysref_pulselength       : natural := 7;  -- number of sample clocks that sysref is high (>= 1)

  constant c_nof_jesd204b_tx          : natural := 3;  -- number of jesd204b input sources to instantiate
  constant c_nof_streams_jesd204b     : natural := 12;  -- number of jesd204b receiver channels

  constant c_jesd204b_rx_data_w            : natural := 32;
  constant c_jesd204b_rx_somf_w            : natural := c_jesd204b_rx_data_w / 8;  -- One somf bit per octet
  constant c_jesd204b_rx_framer_data_w     : natural := c_jesd204b_rx_data_w / 2;  -- IP outputs two samples in parallel
  constant c_jesd204b_rx_framer_somf_w     : natural := c_jesd204b_rx_somf_w / 2;  -- IP outputs two samples in parallel

  -- Transport delays
  type t_time_arr            is array (0 to c_nof_streams_jesd204b - 1) of time;
  constant c_delay_data_arr     : t_time_arr := (4000 ps,
                                                 5000 ps,
                                                 6000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps);  -- transport delays tx to rx data
  constant c_delay_sysreftoadc_arr : t_time_arr := (4000 ps,
                                                 5000 ps,
                                                 6000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps);  -- transport delays clock source to adc(tx)
  constant c_delay_sysreftofpga : time := 10200 ps;

  -- clocks and resets for the jesd204b tx
  signal txlink_clk          : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal dev_sync_n          : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal txphy_clk           : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal mm_rst              : std_logic;
  signal avs_rst_n           : std_logic;
  signal txlink_rst_n        : std_logic;
  signal tx_analogreset      : std_logic_vector(0 downto 0);
  signal tx_digitalreset     : std_logic_vector(0 downto 0);
  signal tx_bonding_clocks   : std_logic_vector(5 downto 0) := (others => '0');
  signal bonding_clock_0     : std_logic := '0';
  signal bonding_clock_1     : std_logic := '0';
  signal bonding_clock_2     : std_logic := '0';
  signal bonding_clock_3     : std_logic := '0';
  signal bonding_clock_4     : std_logic := '0';
  signal bonding_clock_5     : std_logic := '0';
  signal pll_locked          : std_logic_vector(0 downto 0);

  constant c_mm_clk_period   : time := 20 ns;
  signal mm_clk              : std_logic := '0';

  -- Tb
  signal tb_end                      : std_logic := '0';
  signal tb_timeout                  : std_logic := '0';
  signal sim_done                    : std_logic := '0';

  -- mm control buses
  -- JESD
  signal jesd204b_mosi               : t_mem_mosi := c_mem_mosi_rst;
  signal jesd204b_miso               : t_mem_miso := c_mem_miso_rst;
  signal jesd204b_rddata             : std_logic_vector(c_word_w - 1 downto 0);

  -- serial transceivers
  signal serial_tx                   : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal bck_rx                      : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0) := (others => '0');

  -- jesd204b syncronization signals and delayed copies
  signal jesd204b_sysref             : std_logic;
  signal jesd204b_sampclk            : std_logic := '0';
  signal rx_clk                      : std_logic := '0';
  signal rx_rst                      : std_logic := '0';
  signal rx_sysref                   : std_logic := '0';
  signal rx_sosi_arr                 : t_dp_sosi_arr(c_nof_streams_jesd204b - 1 downto 0);

  -- Debug signals for rx_sosi_arr
  signal rx_sosi                     : t_dp_sosi;
  signal rx_sosi_data                : std_logic_vector(c_jesd204b_rx_framer_data_w - 1 downto 0);  -- 16 bit samples
  signal rx_sosi_channel             : std_logic_vector(c_jesd204b_rx_framer_somf_w - 1 downto 0);  -- two samples
  signal rx_sosi_valid               : std_logic;
  signal rx_sosi_sync                : std_logic;
  signal rx_sosi_sop                 : std_logic;
  signal rx_sosi_eop                 : std_logic;
  signal rx_sosi_valid_arr           : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);

  signal jesd204b_sampclk_fpga       : std_logic := '1';
  signal jesd204b_sampclk_adc        : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_sysref_fpga        : std_logic;
  signal jesd204b_sysref_adc         : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_sysref_adc_1       : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_sysref_adc_2       : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_sync_n_adc         : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_sync_n_fpga        : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);

  -- Test bench data
  signal jesd204b_tx_link_data_arr   : t_slv_32_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_tx_link_valid      : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_tx_link_ready      : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_tx_frame_ready     : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);

  -- Diagnostic signals
  signal avs_chipselect              : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal avs_read                    : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal avs_readdata                : t_slv_32_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal avs_address                 : t_slv_8_arr(c_nof_streams_jesd204b - 1 downto 0);

  signal jesd204b_disable_arr        : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  jesd204b_sampclk <= not jesd204b_sampclk after c_jesd204b_sampclk_period / 2;  -- JESD sample clock (200MHz)
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after 800 ns;

  jesd204b_disable_arr <= (others => '0');

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_jesd204b : entity work.tech_jesd204b
  generic map(
    g_sim                => c_sim,
    g_nof_streams        => c_nof_streams_jesd204b,
    g_nof_sync_n         => c_nof_streams_jesd204b
  )
  port map(
    jesd204b_refclk      => jesd204b_sampclk_fpga,
    jesd204b_sysref      => jesd204b_sysref_fpga,
    jesd204b_sync_n_arr  => jesd204b_sync_n_fpga,

    jesd204b_disable_arr => jesd204b_disable_arr,

    rx_sosi_arr          => rx_sosi_arr,
    rx_clk               => rx_clk,
    rx_rst               => rx_rst,
    rx_sysref            => rx_sysref,

    -- MM
    mm_clk               => mm_clk,
    mm_rst               => mm_rst,

    jesd204b_mosi        => jesd204b_mosi,
    jesd204b_miso        => jesd204b_miso,

     -- Serial
    serial_tx_arr        => open,
    serial_rx_arr        => bck_rx(c_nof_streams_jesd204b - 1 downto 0)
  );

  p_monitor_jesd204b : process
  begin
    -- Wait until after MM interface reset_out5 = rx_avs_rst_arr in ip_arria10_e2sg_jesd204b has been released
    wait until sim_done = '1';
    proc_common_wait_some_cycles(mm_clk, 1);  -- align with mm_clk domain

    proc_mem_mm_bus_rd(tech_jesd204b_field_rx_err_enable_adr, mm_clk, jesd204b_miso, jesd204b_mosi);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert unsigned(jesd204b_rddata) = tech_jesd204b_field_rx_err_enable_reset
      report "Wrong rx_err_enable_reset: " & integer'image(TO_SINT(jesd204b_rddata)) & " /= " & integer'image(tech_jesd204b_field_rx_err_enable_reset)
      severity ERROR;

    proc_mem_mm_bus_rd(tech_jesd204b_field_rx_err_link_reinit_adr, mm_clk, jesd204b_miso, jesd204b_mosi);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert unsigned(jesd204b_rddata) = tech_jesd204b_field_rx_err_link_reinit_reset
      report "Wrong rx_err_link_reinit_reset: " & integer'image(TO_SINT(jesd204b_rddata)) & " /= " & integer'image(tech_jesd204b_field_rx_err_link_reinit_reset)
      severity ERROR;

    proc_mem_mm_bus_rd(tech_jesd204b_field_rx_syncn_sysref_ctrl_adr, mm_clk, jesd204b_miso, jesd204b_mosi);

    proc_mem_mm_bus_rd(tech_jesd204b_field_rx_err0_adr,              mm_clk, jesd204b_miso, jesd204b_mosi);
    proc_mem_mm_bus_rd(tech_jesd204b_field_rx_err1_adr,              mm_clk, jesd204b_miso, jesd204b_mosi);
    proc_mem_mm_bus_rd(tech_jesd204b_field_csr_rbd_count_adr,        mm_clk, jesd204b_miso, jesd204b_mosi);
    proc_mem_mm_bus_rd(tech_jesd204b_field_csr_dev_syncn_adr,        mm_clk, jesd204b_miso, jesd204b_mosi);
    wait;
  end process;

  jesd204b_rddata <= jesd204b_miso.rddata(c_word_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Transport
  -----------------------------------------------------------------------------

  gen_transport : for i in 0 to c_nof_jesd204b_tx - 1 generate
    jesd204b_sampclk_adc(i) <= transport jesd204b_sampclk after c_delay_sysreftoadc_arr(i);
    jesd204b_sysref_adc(i)  <= transport jesd204b_sysref after c_delay_sysreftoadc_arr(i);
--    txlink_clk(i) <= jesd204b_sampclk_div2 after c_delay_sysreftoadc_arr(i);
    bck_rx(i) <= transport serial_tx(i) after c_delay_data_arr(i);
    jesd204b_sync_n_adc(i) <= transport jesd204b_sync_n_fpga(i) after c_delay_data_arr(i);
  end generate;

  jesd204b_sampclk_fpga <= transport jesd204b_sampclk after c_delay_sysreftofpga;
  jesd204b_sysref_fpga <= transport jesd204b_sysref after c_delay_sysreftofpga;

  -----------------------------------------------------------------------------
  -- Use a jesd204b instance in TX-ONLY modeTransmit Only.
  -----------------------------------------------------------------------------

  gen_jesd204b_tx : for i in 0 to c_nof_jesd204b_tx - 1 generate
    -- Tb DAC
    u_tech_jesd204b_tx : entity work.tech_jesd204b_tx
    port map (
      csr_cf                     => OPEN,
      csr_cs                     => OPEN,
      csr_f                      => OPEN,
      csr_hd                     => OPEN,
      csr_k                      => OPEN,
      csr_l                      => OPEN,
      csr_lane_powerdown         => open,  -- out
      csr_m                      => OPEN,
      csr_n                      => OPEN,
      csr_np                     => OPEN,
      csr_tx_testmode            => OPEN,
      csr_tx_testpattern_a       => OPEN,
      csr_tx_testpattern_b       => OPEN,
      csr_tx_testpattern_c       => OPEN,
      csr_tx_testpattern_d       => OPEN,
      csr_s                      => OPEN,
      dev_sync_n                 => dev_sync_n(i),  -- out
      jesd204_tx_avs_chipselect  => avs_chipselect(i),  -- jesd204b_mosi_arr(i).chipselect,
      jesd204_tx_avs_address     => avs_address(i),
      jesd204_tx_avs_read        => avs_read(i),
      jesd204_tx_avs_readdata    => avs_readdata(i),
      jesd204_tx_avs_waitrequest => open,
      jesd204_tx_avs_write       => '0',
      jesd204_tx_avs_writedata   => (others => '0'),
      jesd204_tx_avs_clk         => mm_clk,
      jesd204_tx_avs_rst_n       => avs_rst_n,
      jesd204_tx_dlb_data        => open,  -- debug/loopback testing
      jesd204_tx_dlb_kchar_data  => open,  -- debug/loopback testing
      jesd204_tx_frame_ready     => jesd204b_tx_frame_ready(i),
      jesd204_tx_frame_error     => '0',
      jesd204_tx_int             => OPEN,  -- Connected to status IO in example design
      jesd204_tx_link_data       => jesd204b_tx_link_data_arr(i),  -- in
      jesd204_tx_link_valid      => jesd204b_tx_link_valid(i),  -- in
      jesd204_tx_link_ready      => jesd204b_tx_link_ready(i),  -- out
      mdev_sync_n                => dev_sync_n(i),  -- in
      pll_locked                 => pll_locked,  -- in
      sync_n                     => jesd204b_sync_n_adc(i),  -- in
      tx_analogreset             => tx_analogreset,
      tx_bonding_clocks          => tx_bonding_clocks,  -- : in  std_logic_vector(5 downto 0)  := (others => 'X'); -- clk
      tx_cal_busy                => open,
      tx_digitalreset            => tx_digitalreset,
      tx_serial_data             => serial_tx(i downto i),
      txlink_clk                 => txlink_clk(i),
      txlink_rst_n_reset_n       => txlink_rst_n,
      txphy_clk                  => txphy_clk(i downto i),
      somf                       => OPEN,
      sysref                     => jesd204b_sysref_adc(i)
    );

    -- Generate test pattern at each ADC
    proc_data : process (jesd204b_sampclk_adc(i), mm_rst)
      variable v_data  : integer := 0;
      variable v_even_sample : boolean := true;
    begin
      if mm_rst = '1' then
         jesd204b_tx_link_data_arr(i) <= (others => '0');
         jesd204b_tx_link_valid(i) <= '0';
         txlink_clk(i) <= '0';
         v_data := 0;
         v_even_sample := true;
       elsif rising_edge(jesd204b_sampclk_adc(i)) then
         txlink_clk(i) <= not txlink_clk(i);
         jesd204b_sysref_adc_1(i) <= jesd204b_sysref_adc(i);
         jesd204b_sysref_adc_2(i) <= jesd204b_sysref_adc_1(i);

         --generate a positive and negative going pulse after the rising edge of SYSREF
         if (jesd204b_sysref_adc(i) = '1' and jesd204b_sysref_adc_1(i) = '0') then
           v_data := 1000;
         elsif (jesd204b_sysref_adc_1(i) = '1' and jesd204b_sysref_adc_2(i) = '0') then
           v_data := -1000;
         else
           v_data := 0;
         end if;

         -- Frame the data to 32 bits at half the rate
         if(jesd204b_tx_link_ready(i) = '0') then
           v_even_sample := true;
         else
           v_even_sample := not v_even_sample;
         end if;
         if (v_even_sample = true) then
           jesd204b_tx_link_data_arr(i)(15 downto 0) <= TO_SVEC(v_data, 16);
           jesd204b_tx_link_valid(i) <= '0';
         else
           jesd204b_tx_link_data_arr(i)(31 downto 16) <= TO_SVEC(v_data, 16);
           jesd204b_tx_link_valid(i) <= '1';
         end if;
       end if;
    end process;

  end generate;

  -----------------------------------------------------------------------------
  -- Stimulii
  -----------------------------------------------------------------------------

  -- Clocks and resets
  avs_rst_n <= '0', '1'  after 23500 ns;
  tx_analogreset(0) <= '1', '0' after 18500 ns;
  tx_digitalreset(0) <= '1', '0' after 23000 ns;
  txlink_rst_n <= '0', '1' after 25500 ns;
  pll_locked(0) <= '0', '1' after 1000 ns;

  -- Create bonding clocks
  -- The clock periods and offsets are copied from the JESD TX sample design
  -- They are created directly to simplifiy the test bench
  -- bonding_clock_1 has an uneven duty cycle

  bonding_clock_5 <= not bonding_clock_5 after 250 ps;
  bonding_clock_4 <= not bonding_clock_4 after 250 ps;
  bonding_clock_3 <= not bonding_clock_3 after 500 ps;
  bonding_clock_2 <= not bonding_clock_2 after 500 ps;
  bonding_clock_0 <= not bonding_clock_0 after 2500 ps;

  bonding_clock_1_process : process
  begin
    bonding_clock_1 <= '0';
    wait for 4000 ps;
    bonding_clock_1 <= '1';
    wait for 1000 ps;
  end process;

  tx_bonding_clocks(5) <= transport bonding_clock_5 after 4890 ps;
  tx_bonding_clocks(4) <= transport bonding_clock_4 after 4640 ps;
  tx_bonding_clocks(3) <= transport bonding_clock_3 after 4920 ps;
  tx_bonding_clocks(2) <= transport bonding_clock_2 after 4930 ps;
  tx_bonding_clocks(1) <= transport bonding_clock_1 after 7490 ps;
  tx_bonding_clocks(0) <= transport bonding_clock_0 after 4000 ps;

  -- clock source process
  -- generate the sysref pulses
  proc_sysref : process (jesd204b_sampclk, mm_rst)
    variable v_count : natural := 0;
  begin
    if mm_rst = '1' then
       jesd204b_sysref <= '0';
       v_count := 0;
     else
       if rising_edge(jesd204b_sampclk) then
         if (v_count = c_sysref_period - 1) then
           v_count := 0;
         else
           v_count := v_count + 1;
         end if;

         if v_count > c_sysref_period - 1 - c_sysref_pulselength then
           jesd204b_sysref <= '1';
         else
           jesd204b_sysref <= '0';
         end if;
       end if;
     end if;
  end process;

  ------------------------------------------------------------------------------
  -- Diagnostics
  -- Read registers in the tx jesd core to check it comes out of reset Ok
  -- The meaning of specific register bits is defined in section 4.7.2 of the User Guide
  -- https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_jesd204b.pdf
  ------------------------------------------------------------------------------
  proc_read_avs_regs : process
  begin
    wait for 100 ns;
    avs_address(0) <= (others => '0');
    avs_chipselect(0) <= '0';
    avs_read(0) <= '0';
    wait until avs_rst_n = '1';
    while true loop
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"14";  -- dll control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"15";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);

      avs_address(0) <= X"18";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"19";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);

      avs_address(0) <= X"20";  -- tx control0
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"26";  -- tx control0
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
    end loop;
  end process;

  ------------------------------------------------------------------------------
  -- Debug signals
  ------------------------------------------------------------------------------

  -- Wire rx_sosi_arr(0), because for some reason rx_sosi_arr does not show with as in wave window
  rx_sosi         <= rx_sosi_arr(0);
  rx_sosi_data    <= rx_sosi.data(rx_sosi_data'range);
  rx_sosi_channel <= rx_sosi.channel(rx_sosi_channel'range);
  rx_sosi_valid   <= rx_sosi.valid;
  rx_sosi_sync    <= rx_sosi.sync;
  rx_sosi_sop     <= rx_sosi.sop;
  rx_sosi_eop     <= rx_sosi.eop;

  rx_sosi_valid_arr <= func_dp_stream_arr_get(rx_sosi_arr, "VALID");

  ------------------------------------------------------------------------------
  -- Simulation end
  ------------------------------------------------------------------------------

  sim_done <= '1' when rx_sosi_arr(0).valid = '1' and rx_sosi_arr(1).valid = '1' and rx_sosi_arr(2).valid = '1' else '0';

  p_tb_end : process
  begin
    wait for 2 * c_sysref_period * c_jesd204b_sampclk_period;

    assert sim_done = '1'
      report "No rx samples were received."
      severity ERROR;
    wait for 10 us;
    tb_end <= '1';
  end process;

  proc_common_timeout_failure(200 us, tb_timeout);  -- ERROR: end simulation if it fails to end in time
  proc_common_stop_simulation(tb_end);  -- OK: end simulation
end tb;
