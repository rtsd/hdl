-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_ddr_mem_model_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from Verilog module alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv in:
  -- $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_800_master_example_design/simulation/vhdl/submodules/

  component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en is
  generic (
    MEM_IF_CLK_EN_WIDTH          : integer := 1;
    MEM_IF_CK_WIDTH              : integer := 1;
    MEM_IF_BANKADDR_WIDTH        : integer := 3;
    MEM_IF_ADDR_WIDTH            : integer := 15;
    MEM_IF_ROW_ADDR_WIDTH        : integer := 15;
    MEM_IF_COL_ADDR_WIDTH        : integer := 10;
    MEM_IF_CS_WIDTH              : integer := 1;
    MEM_IF_CONTROL_WIDTH         : integer := 1;
    MEM_IF_ODT_WIDTH             : integer := 1;
    DEVICE_DEPTH                 : integer := 1;
    DEVICE_WIDTH                 : integer := 1;
    MEM_IF_CS_PER_RANK           : integer := 1;
    MEM_IF_DQS_WIDTH             : integer := 1;
    MEM_IF_DQ_WIDTH              : integer := 8;
    MEM_MIRROR_ADDRESSING_DEC    : integer := 0;
    MEM_TRTP                     : integer := 8;
    MEM_TRCD                     : integer := 8;
    MEM_DQS_TO_CLK_CAPTURE_DELAY : integer := 100;
    MEM_CLK_TO_DQS_CAPTURE_DELAY : integer := 100000;
    MEM_REGDIMM_ENABLED          : integer := 0;
    MEM_INIT_EN                  : integer := 0;
    MEM_INIT_FILE                : string := "";
    MEM_GUARANTEED_WRITE_INIT    : integer := 0;
    DAT_DATA_WIDTH               : integer := 32;
    MEM_VERBOSE                  : integer := 1
  );
  port (
    mem_a      : in    std_logic_vector(MEM_IF_ADDR_WIDTH - 1 downto 0)    := (others => 'X');
    mem_ba     : in    std_logic_vector(MEM_IF_BANKADDR_WIDTH - 1 downto 0) := (others => 'X');
    mem_ck     : in    std_logic_vector(MEM_IF_CK_WIDTH - 1 downto 0)      := (others => 'X');
    mem_ck_n   : in    std_logic_vector(MEM_IF_CK_WIDTH - 1 downto 0)      := (others => 'X');
    mem_cke    : in    std_logic_vector(MEM_IF_CLK_EN_WIDTH - 1 downto 0)  := (others => 'X');
    mem_cs_n   : in    std_logic_vector(MEM_IF_CS_WIDTH - 1 downto 0)      := (others => 'X');
    mem_ras_n  : in    std_logic_vector(MEM_IF_CONTROL_WIDTH - 1 downto 0) := (others => 'X');
    mem_cas_n  : in    std_logic_vector(MEM_IF_CONTROL_WIDTH - 1 downto 0) := (others => 'X');
    mem_we_n   : in    std_logic_vector(MEM_IF_CONTROL_WIDTH - 1 downto 0) := (others => 'X');
    mem_reset_n: in    std_logic                                         := 'X';
    mem_dm     : in    std_logic_vector(MEM_IF_DQS_WIDTH - 1 downto 0)     := (others => 'X');
    mem_dq     : inout std_logic_vector(MEM_IF_DQ_WIDTH - 1 downto 0)      := (others => 'X');
    mem_dqs    : inout std_logic_vector(MEM_IF_DQS_WIDTH - 1 downto 0)     := (others => 'X');
    mem_dqs_n  : inout std_logic_vector(MEM_IF_DQS_WIDTH - 1 downto 0)     := (others => 'X');
    mem_odt    : in    std_logic_vector(MEM_IF_ODT_WIDTH - 1 downto 0)     := (others => 'X')
  );
  end component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd in:
  -- $HDL_WORK/libraries/technology/ip_arria10/ddr4_4g_1600/emif_0_example_design/sim/altera_emif_mem_model_141/sim
  component ed_sim_altera_emif_mem_model_141_z3tvrmq is
  port (
    mem_ck      : in    std_logic_vector(0 downto 0)  := (others => '0');  -- mem_conduit_end.mem_ck
    mem_ck_n    : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_ck_n
    mem_a       : in    std_logic_vector(16 downto 0) := (others => '0');  -- .mem_a
    mem_act_n   : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_act_n
    mem_ba      : in    std_logic_vector(1 downto 0)  := (others => '0');  -- .mem_ba
    mem_bg      : in    std_logic_vector(1 downto 0)  := (others => '0');  -- .mem_bg
    mem_cke     : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_cke
    mem_cs_n    : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_cs_n
    mem_odt     : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_odt
    mem_reset_n : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_reset_n
    mem_par     : in    std_logic_vector(0 downto 0)  := (others => '0');  -- .mem_par
    mem_alert_n : out   std_logic_vector(0 downto 0);  -- .mem_alert_n
    mem_dqs     : inout std_logic_vector(8 downto 0)  := (others => '0');  -- .mem_dqs
    mem_dqs_n   : inout std_logic_vector(8 downto 0)  := (others => '0');  -- .mem_dqs_n
    mem_dq      : inout std_logic_vector(71 downto 0) := (others => '0');  -- .mem_dq
    mem_dbi_n   : inout std_logic_vector(8 downto 0)  := (others => '0')  -- .mem_dbi_n
  );
  end component ed_sim_altera_emif_mem_model_141_z3tvrmq;
end tech_ddr_mem_model_component_pkg;

package body tech_ddr_mem_model_component_pkg is
end tech_ddr_mem_model_component_pkg;
