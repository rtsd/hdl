# Pin files
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_general_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_other_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_1Gbe_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_sensor_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_mesh_tr_clk_pin.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/FRONT_NODE_mesh_nocmu_pins.tcl