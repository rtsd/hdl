-------------------------------------------------------------------------------
-- Title      : Advanced eXtinsible Interface Version 4
-- Project    :
-------------------------------------------------------------------------------
-- File       : AXI4_pkg.vhd
-- Author     : William Kamp <william.kamp@aut.ac.nz>
-- Company    :
-- Created    : 2015-09-28
-- Last update: 2018-10-19
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2015
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2015-09-28  1.0      wkamp   Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.misc_tools_pkg.all;

package AXI4_pkg is

    constant c_DATA_WIDTH : natural := 32;

    subtype burst_t is std_logic_vector(1 downto 0);
    constant BURST_FIXED : burst_t := "00";
    constant BURST_INCR  : burst_t := "01";
    constant BURST_WRAP  : burst_t := "10";
    constant BURST_RESVD : burst_t := "11";

    type AXI4_AW_t is record
        id     : std_logic_vector(7 downto 0);   -- identification tag for the
                                                 -- write address group of
                                                 -- signals.
        addr   : std_logic_vector(31 downto 0);  -- the address of the first
                                                 -- transfer  in a write burst
                                                 -- transaction.
        len    : std_logic_vector(7 downto 0);   -- the exact number of
                                                 -- transfers in a burst.
        size   : std_logic_vector(2 downto 0);   -- the size of each transfer in
                                                 -- the burst = 2^size bytes.
        burst  : burst_t;                        -- the burst type and the size
                                                 -- information.
        lock   : std_logic;                      -- '1' = exclusive access.
        cache  : std_logic_vector(3 downto 0);   -- Memory type. This signal
                                                 -- indicates how transactions
                                                 -- are required to progress
                                                 -- through a system.
        prot   : std_logic_vector(2 downto 0);   -- Protection type. This signal
                                                 -- indicates the privilege and
                                                 -- security level of the transaction,
                                                 -- and whether the transaction is a
                                                 -- data access or an instruction access.
        qos    : std_logic_vector(3 downto 0);   -- Quality of Service, QoS. The QoS
                                                 -- identifier sent for each write
                                                 -- transaction.
        region : std_logic_vector(3 downto 0);   -- Region identifier. Permits a single
                                                 -- physical interface on a slave to
                                                 -- be used for multiple logical
                                                 -- interfaces.
        user   : std_logic_vector(31 downto 0);  -- User signal. Optional User-defined
                                                 -- signal in the write address
                                                 -- channel.
        valid  : std_logic;                      -- Write address valid. This signal indicates
                                                 -- that the channel is signaling valid write
                                                 -- address and control information.
    end record AXI4_AW_t;

    subtype AXI4_AWREADY_t is std_logic;

    type AXI4_W_t is record
        id    : std_logic_vector(7 downto 0);   -- identification tag for the
                                                -- write address group of
                                                -- signals.
        data  : std_logic_vector(31 downto 0);
        strb  : std_logic_vector(3 downto 0);   -- This signal indicates which byte
                                                -- lanes hold valid data. There is one
                                                -- write strobe bit for each eight bits
                                                -- of the write data bus.
        last  : std_logic;                      -- indicates the last transfer in a write burst.
        user  : std_logic_vector(31 downto 0);  -- User signal. Optional User-defined
                                                -- signal in the write address
                                                -- channel.
        valid : std_logic;                      -- indicates that valid write data and strobes
                                                -- are available.
    end record AXI4_W_t;

    subtype AXI4_WREADY_t is std_logic;

    constant PROT_PRIVILEGED  : std_logic_vector(2 downto 0) := "001";
    constant PROT_NON_SECURE  : std_logic_vector(2 downto 0) := "010";
    constant PROT_INSTRUCTION : std_logic_vector(2 downto 0) := "100";
    constant PROT_NO_FLAGS    : std_logic_vector(2 downto 0) := "000";

    type AXI4_B_t is record
        id    : std_logic_vector(7 downto 0);   -- identification tag of the
                                                -- read/write response
        resp  : std_logic_vector(1 downto 0);   -- indicates the status of the
                                                -- read/write transaction.
        user  : std_logic_vector(31 downto 0);  -- optional user-defined signal.
        valid : std_logic;                      -- indicates that the channel is
                                                -- signaling a valid write response.
    end record AXI4_B_t;

    subtype AXI4_BREADY_t is std_logic;

    constant RESP_OKAY   : std_logic_vector(1 downto 0) := "00";
    constant RESP_EXOKAY : std_logic_vector(1 downto 0) := "01";
    constant RESP_SLVERR : std_logic_vector(1 downto 0) := "10";
    constant RESP_DECERR : std_logic_vector(1 downto 0) := "11";

    --------------------------------------------------------------------------------------------------------------------
    -- READ CHANNELS
    --------------------------------------------------------------------------------------------------------------------
    type AXI4_AR_t is record
        id     : std_logic_vector(7 downto 0);   -- identification tag for the
                                                 -- write address group of
                                                 -- signals.
        addr   : std_logic_vector(31 downto 0);  -- the address of the first
                                                 -- transfer in a write burst
                                                 -- transaction.
        len    : std_logic_vector(7 downto 0);   -- the exact number of
                                                 -- transfers in a burst.
        size   : std_logic_vector(2 downto 0);   -- the size of each transfer in
                                                 -- the burst = 2^size bytes.
        burst  : burst_t;                        -- the burst type and the size
                                                 -- information.
        lock   : std_logic;                      -- '1' = exclusive access.
        cache  : std_logic_vector(3 downto 0);   -- Memory type. This signal
                                                 -- indicates how transactions
                                                 -- are required to progress
                                                 -- through a system.
        prot   : std_logic_vector(2 downto 0);   -- Protection type. This signal
                                                 -- indicates the privilege and
                                                 -- security level of the transaction,
                                                 -- and whether the transaction is a
                                                 -- data access or an instruction access.
        qos    : std_logic_vector(3 downto 0);   -- Quality of Service, QoS. The QoS
                                                 -- identifier sent for each write
                                                 -- transaction.
        region : std_logic_vector(3 downto 0);   -- Region identifier. Permits a single
                                                 -- physical interface on a slave to
                                                 -- be used for multiple logical
                                                 -- interfaces.
        user   : std_logic_vector(31 downto 0);  -- User signal. Optional User-defined
                                                 -- signal in the write address
                                                 -- channel.
        valid  : std_logic;                      -- Read address valid. This signal indicates
                                                 -- that the channel is signalling valid read
                                                 -- address and control information.
    end record AXI4_AR_t;

    subtype AXI4_ARREADY_t is std_logic;

    type AXI4_R_t is record
        id    : std_logic_vector(7 downto 0);   -- identification tag for the
                                                -- write address group of
                                                -- signals.
        data  : std_logic_vector(31 downto 0);
        resp  : std_logic_vector(1 downto 0);   -- indicates the status of the
                                                -- read/write transaction.
        last  : std_logic;                      -- indicates the last transfer in a write burst.
        user  : std_logic_vector(31 downto 0);  -- User signal. Optional User-defined
                                                -- signal in the write address
                                                -- channel.
        valid : std_logic;                      -- indicates that valid write data and strobes
                                                -- are available.
    end record AXI4_R_t;

    subtype AXI4_RREADY_t is std_logic;

    type AXI4_MOSI_t is record
        ar     : AXI4_AR_t;
        aw     : AXI4_AW_t;
        w      : AXI4_W_t;
        rready : AXI4_RREADY_t;
        bready : AXI4_BREADY_t;
    end record AXI4_MOSI_t;

    type AXI4_SOMI_t is record
        r       : AXI4_R_t;
        b       : AXI4_B_t;
        arready : AXI4_ARREADY_t;
        awready : AXI4_AWREADY_t;
        wready  : AXI4_WREADY_t;
    end record AXI4_SOMI_t;

    type AXI4_MOSI_a_t is array (natural range <>) of AXI4_MOSI_t;
    type AXI4_SOMI_a_t is array (natural range <>) of AXI4_SOMI_t;

    type AXI4_Streaming_1024_t is record
        data  : std_logic_vector(1023 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(127 downto 0);
        error : std_logic;
    end record AXI4_Streaming_1024_t;

    type AXI4_Streaming_512_t is record
        data  : std_logic_vector(511 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(63 downto 0);
        error : std_logic;
    end record AXI4_Streaming_512_t;

    type AXI4_Streaming_256_t is record
        data  : std_logic_vector(255 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(31 downto 0);
        error : std_logic;
    end record AXI4_Streaming_256_t;

    type AXI4_Streaming_128_t is record
        data  : std_logic_vector(127 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(15 downto 0);
        error : std_logic;
    end record AXI4_Streaming_128_t;

    type AXI4_Streaming_64_t is record
        data  : std_logic_vector(63 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(7 downto 0);
        error : std_logic;
    end record AXI4_Streaming_64_t;

    type AXI4_Streaming_32_t is record
        data  : std_logic_vector(31 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(3 downto 0);
        error : std_logic;
    end record AXI4_Streaming_32_t;

    type AXI4_Streaming_16_t is record
        data  : std_logic_vector(15 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(1 downto 0);
        error : std_logic;
    end record AXI4_Streaming_16_t;

    type AXI4_Streaming_8_t is record
        data  : std_logic_vector(7 downto 0);
        valid : std_logic;
        last  : std_logic;
        keep  : std_logic_vector(0 downto 0);
        error : std_logic;
    end record AXI4_Streaming_8_t;

    -- autogen start decl
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Sun Jun 19 16:08:58 2022
    --------------------------------------------------

    type AXI4_AW_t_a is array(natural range <>) of AXI4_AW_t;
    
    constant AXI4_AW_T_ZERO : AXI4_AW_t := (
            id     => (others => '0'),
            addr   => (others => '0'),
            len    => (others => '0'),
            size   => (others => '0'),
            burst  => (others => '0'),
            lock   => '0',
            cache  => (others => '0'),
            prot   => (others => '0'),
            qos    => (others => '0'),
            region => (others => '0'),
            user   => (others => '0'),
            valid  => '0'
        );
    
    constant AXI4_AW_T_DONT_CARE : AXI4_AW_t := (
            id     => (others => '-'),
            addr   => (others => '-'),
            len    => (others => '-'),
            size   => (others => '-'),
            burst  => (others => '-'),
            lock   => '-',
            cache  => (others => '-'),
            prot   => (others => '-'),
            qos    => (others => '-'),
            region => (others => '-'),
            user   => (others => '-'),
            valid  => '-'
        );
    
    constant AXI4_AW_T_SLV_WIDTH : natural := 102;
    
    subtype AXI4_AW_t_slv is std_logic_vector(101 downto 0);
    function to_slv (rec : AXI4_AW_t) return AXI4_AW_t_slv;
    function from_slv (slv : AXI4_AW_t_slv) return AXI4_AW_t;
    
    type AXI4_W_t_a is array(natural range <>) of AXI4_W_t;
    
    constant AXI4_W_T_ZERO : AXI4_W_t := (
            id    => (others => '0'),
            data  => (others => '0'),
            strb  => (others => '0'),
            last  => '0',
            user  => (others => '0'),
            valid => '0'
        );
    
    constant AXI4_W_T_DONT_CARE : AXI4_W_t := (
            id    => (others => '-'),
            data  => (others => '-'),
            strb  => (others => '-'),
            last  => '-',
            user  => (others => '-'),
            valid => '-'
        );
    
    constant AXI4_W_T_SLV_WIDTH : natural := 78;
    
    subtype AXI4_W_t_slv is std_logic_vector(77 downto 0);
    function to_slv (rec : AXI4_W_t) return AXI4_W_t_slv;
    function from_slv (slv : AXI4_W_t_slv) return AXI4_W_t;
    
    type AXI4_B_t_a is array(natural range <>) of AXI4_B_t;
    
    constant AXI4_B_T_ZERO : AXI4_B_t := (
            id    => (others => '0'),
            resp  => (others => '0'),
            user  => (others => '0'),
            valid => '0'
        );
    
    constant AXI4_B_T_DONT_CARE : AXI4_B_t := (
            id    => (others => '-'),
            resp  => (others => '-'),
            user  => (others => '-'),
            valid => '-'
        );
    
    constant AXI4_B_T_SLV_WIDTH : natural := 43;
    
    subtype AXI4_B_t_slv is std_logic_vector(42 downto 0);
    function to_slv (rec : AXI4_B_t) return AXI4_B_t_slv;
    function from_slv (slv : AXI4_B_t_slv) return AXI4_B_t;
    
    type AXI4_AR_t_a is array(natural range <>) of AXI4_AR_t;
    
    constant AXI4_AR_T_ZERO : AXI4_AR_t := (
            id     => (others => '0'),
            addr   => (others => '0'),
            len    => (others => '0'),
            size   => (others => '0'),
            burst  => (others => '0'),
            lock   => '0',
            cache  => (others => '0'),
            prot   => (others => '0'),
            qos    => (others => '0'),
            region => (others => '0'),
            user   => (others => '0'),
            valid  => '0'
        );
    
    constant AXI4_AR_T_DONT_CARE : AXI4_AR_t := (
            id     => (others => '-'),
            addr   => (others => '-'),
            len    => (others => '-'),
            size   => (others => '-'),
            burst  => (others => '-'),
            lock   => '-',
            cache  => (others => '-'),
            prot   => (others => '-'),
            qos    => (others => '-'),
            region => (others => '-'),
            user   => (others => '-'),
            valid  => '-'
        );
    
    constant AXI4_AR_T_SLV_WIDTH : natural := 102;
    
    subtype AXI4_AR_t_slv is std_logic_vector(101 downto 0);
    function to_slv (rec : AXI4_AR_t) return AXI4_AR_t_slv;
    function from_slv (slv : AXI4_AR_t_slv) return AXI4_AR_t;
    
    type AXI4_R_t_a is array(natural range <>) of AXI4_R_t;
    
    constant AXI4_R_T_ZERO : AXI4_R_t := (
            id    => (others => '0'),
            data  => (others => '0'),
            resp  => (others => '0'),
            last  => '0',
            user  => (others => '0'),
            valid => '0'
        );
    
    constant AXI4_R_T_DONT_CARE : AXI4_R_t := (
            id    => (others => '-'),
            data  => (others => '-'),
            resp  => (others => '-'),
            last  => '-',
            user  => (others => '-'),
            valid => '-'
        );
    
    constant AXI4_R_T_SLV_WIDTH : natural := 76;
    
    subtype AXI4_R_t_slv is std_logic_vector(75 downto 0);
    function to_slv (rec : AXI4_R_t) return AXI4_R_t_slv;
    function from_slv (slv : AXI4_R_t_slv) return AXI4_R_t;
    
    type AXI4_MOSI_t_a is array(natural range <>) of AXI4_MOSI_t;
    
    constant AXI4_MOSI_T_ZERO : AXI4_MOSI_t := (
            ar     => AXI4_AR_T_ZERO,
            aw     => AXI4_AW_T_ZERO,
            w      => AXI4_W_T_ZERO,
            rready => '0',
            bready => '0'
        );
    
    constant AXI4_MOSI_T_DONT_CARE : AXI4_MOSI_t := (
            ar     => AXI4_AR_T_DONT_CARE,
            aw     => AXI4_AW_T_DONT_CARE,
            w      => AXI4_W_T_DONT_CARE,
            rready => '-',
            bready => '-'
        );
    
    constant AXI4_MOSI_T_SLV_WIDTH : natural := 284;
    
    subtype AXI4_MOSI_t_slv is std_logic_vector(283 downto 0);
    function to_slv (rec : AXI4_MOSI_t) return AXI4_MOSI_t_slv;
    function from_slv (slv : AXI4_MOSI_t_slv) return AXI4_MOSI_t;
    
    type AXI4_SOMI_t_a is array(natural range <>) of AXI4_SOMI_t;
    
    constant AXI4_SOMI_T_ZERO : AXI4_SOMI_t := (
            r       => AXI4_R_T_ZERO,
            b       => AXI4_B_T_ZERO,
            arready => '0',
            awready => '0',
            wready  => '0'
        );
    
    constant AXI4_SOMI_T_DONT_CARE : AXI4_SOMI_t := (
            r       => AXI4_R_T_DONT_CARE,
            b       => AXI4_B_T_DONT_CARE,
            arready => '-',
            awready => '-',
            wready  => '-'
        );
    
    constant AXI4_SOMI_T_SLV_WIDTH : natural := 122;
    
    subtype AXI4_SOMI_t_slv is std_logic_vector(121 downto 0);
    function to_slv (rec : AXI4_SOMI_t) return AXI4_SOMI_t_slv;
    function from_slv (slv : AXI4_SOMI_t_slv) return AXI4_SOMI_t;
    
    type AXI4_Streaming_1024_t_a is array(natural range <>) of AXI4_Streaming_1024_t;
    
    constant AXI4_STREAMING_1024_T_ZERO : AXI4_Streaming_1024_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_1024_T_DONT_CARE : AXI4_Streaming_1024_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_1024_T_SLV_WIDTH : natural := 1155;
    
    subtype AXI4_Streaming_1024_t_slv is std_logic_vector(1154 downto 0);
    function to_slv (rec : AXI4_Streaming_1024_t) return AXI4_Streaming_1024_t_slv;
    function from_slv (slv : AXI4_Streaming_1024_t_slv) return AXI4_Streaming_1024_t;
    
    type AXI4_Streaming_512_t_a is array(natural range <>) of AXI4_Streaming_512_t;
    
    constant AXI4_STREAMING_512_T_ZERO : AXI4_Streaming_512_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_512_T_DONT_CARE : AXI4_Streaming_512_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_512_T_SLV_WIDTH : natural := 579;
    
    subtype AXI4_Streaming_512_t_slv is std_logic_vector(578 downto 0);
    function to_slv (rec : AXI4_Streaming_512_t) return AXI4_Streaming_512_t_slv;
    function from_slv (slv : AXI4_Streaming_512_t_slv) return AXI4_Streaming_512_t;
    
    type AXI4_Streaming_256_t_a is array(natural range <>) of AXI4_Streaming_256_t;
    
    constant AXI4_STREAMING_256_T_ZERO : AXI4_Streaming_256_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_256_T_DONT_CARE : AXI4_Streaming_256_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_256_T_SLV_WIDTH : natural := 291;
    
    subtype AXI4_Streaming_256_t_slv is std_logic_vector(290 downto 0);
    function to_slv (rec : AXI4_Streaming_256_t) return AXI4_Streaming_256_t_slv;
    function from_slv (slv : AXI4_Streaming_256_t_slv) return AXI4_Streaming_256_t;
    
    type AXI4_Streaming_128_t_a is array(natural range <>) of AXI4_Streaming_128_t;
    
    constant AXI4_STREAMING_128_T_ZERO : AXI4_Streaming_128_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_128_T_DONT_CARE : AXI4_Streaming_128_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_128_T_SLV_WIDTH : natural := 147;
    
    subtype AXI4_Streaming_128_t_slv is std_logic_vector(146 downto 0);
    function to_slv (rec : AXI4_Streaming_128_t) return AXI4_Streaming_128_t_slv;
    function from_slv (slv : AXI4_Streaming_128_t_slv) return AXI4_Streaming_128_t;
    
    type AXI4_Streaming_64_t_a is array(natural range <>) of AXI4_Streaming_64_t;
    
    constant AXI4_STREAMING_64_T_ZERO : AXI4_Streaming_64_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_64_T_DONT_CARE : AXI4_Streaming_64_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_64_T_SLV_WIDTH : natural := 75;
    
    subtype AXI4_Streaming_64_t_slv is std_logic_vector(74 downto 0);
    function to_slv (rec : AXI4_Streaming_64_t) return AXI4_Streaming_64_t_slv;
    function from_slv (slv : AXI4_Streaming_64_t_slv) return AXI4_Streaming_64_t;
    
    type AXI4_Streaming_32_t_a is array(natural range <>) of AXI4_Streaming_32_t;
    
    constant AXI4_STREAMING_32_T_ZERO : AXI4_Streaming_32_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_32_T_DONT_CARE : AXI4_Streaming_32_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_32_T_SLV_WIDTH : natural := 39;
    
    subtype AXI4_Streaming_32_t_slv is std_logic_vector(38 downto 0);
    function to_slv (rec : AXI4_Streaming_32_t) return AXI4_Streaming_32_t_slv;
    function from_slv (slv : AXI4_Streaming_32_t_slv) return AXI4_Streaming_32_t;
    
    type AXI4_Streaming_16_t_a is array(natural range <>) of AXI4_Streaming_16_t;
    
    constant AXI4_STREAMING_16_T_ZERO : AXI4_Streaming_16_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_16_T_DONT_CARE : AXI4_Streaming_16_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_16_T_SLV_WIDTH : natural := 21;
    
    subtype AXI4_Streaming_16_t_slv is std_logic_vector(20 downto 0);
    function to_slv (rec : AXI4_Streaming_16_t) return AXI4_Streaming_16_t_slv;
    function from_slv (slv : AXI4_Streaming_16_t_slv) return AXI4_Streaming_16_t;
    
    type AXI4_Streaming_8_t_a is array(natural range <>) of AXI4_Streaming_8_t;
    
    constant AXI4_STREAMING_8_T_ZERO : AXI4_Streaming_8_t := (
            data  => (others => '0'),
            valid => '0',
            last  => '0',
            keep  => (others => '0'),
            error => '0'
        );
    
    constant AXI4_STREAMING_8_T_DONT_CARE : AXI4_Streaming_8_t := (
            data  => (others => '-'),
            valid => '-',
            last  => '-',
            keep  => (others => '-'),
            error => '-'
        );
    
    constant AXI4_STREAMING_8_T_SLV_WIDTH : natural := 12;
    
    subtype AXI4_Streaming_8_t_slv is std_logic_vector(11 downto 0);
    function to_slv (rec : AXI4_Streaming_8_t) return AXI4_Streaming_8_t_slv;
    function from_slv (slv : AXI4_Streaming_8_t_slv) return AXI4_Streaming_8_t;
    
    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end decl
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_AR_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_R_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_AW_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_W_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_B_t;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_8_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_16_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_32_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_64_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_128_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_256_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_512_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_1024_t;

    function empty_to_keep (empty : std_logic_vector; last : std_logic := '1') return std_logic_vector;
    function keep_to_empty ( keep : std_logic_vector; last : std_logic := '1') return std_logic_vector;

end package AXI4_pkg;

package body AXI4_pkg is

    -- autogen start body
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Sun Jun 19 16:08:58 2022
    --------------------------------------------------

    function to_slv (rec : AXI4_AW_t) return AXI4_AW_t_slv is
        variable v_slv : std_logic_vector(101 downto 0);
    begin
        v_slv(0)             := rec.valid;
        v_slv(32 downto 1)   := rec.user;
        v_slv(36 downto 33)  := rec.region;
        v_slv(40 downto 37)  := rec.qos;
        v_slv(43 downto 41)  := rec.prot;
        v_slv(47 downto 44)  := rec.cache;
        v_slv(48)            := rec.lock;
        v_slv(50 downto 49)  := rec.burst;
        v_slv(53 downto 51)  := rec.size;
        v_slv(61 downto 54)  := rec.len;
        v_slv(93 downto 62)  := rec.addr;
        v_slv(101 downto 94) := rec.id;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_AW_t_slv) return AXI4_AW_t is
        variable v_rec : AXI4_AW_t;
    begin
        v_rec.valid  := slv(0);
        v_rec.user   := slv(32 downto 1);
        v_rec.region := slv(36 downto 33);
        v_rec.qos    := slv(40 downto 37);
        v_rec.prot   := slv(43 downto 41);
        v_rec.cache  := slv(47 downto 44);
        v_rec.lock   := slv(48);
        v_rec.burst  := slv(50 downto 49);
        v_rec.size   := slv(53 downto 51);
        v_rec.len    := slv(61 downto 54);
        v_rec.addr   := slv(93 downto 62);
        v_rec.id     := slv(101 downto 94);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_W_t) return AXI4_W_t_slv is
        variable v_slv : std_logic_vector(77 downto 0);
    begin
        v_slv(0)            := rec.valid;
        v_slv(32 downto 1)  := rec.user;
        v_slv(33)           := rec.last;
        v_slv(37 downto 34) := rec.strb;
        v_slv(69 downto 38) := rec.data;
        v_slv(77 downto 70) := rec.id;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_W_t_slv) return AXI4_W_t is
        variable v_rec : AXI4_W_t;
    begin
        v_rec.valid := slv(0);
        v_rec.user  := slv(32 downto 1);
        v_rec.last  := slv(33);
        v_rec.strb  := slv(37 downto 34);
        v_rec.data  := slv(69 downto 38);
        v_rec.id    := slv(77 downto 70);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_B_t) return AXI4_B_t_slv is
        variable v_slv : std_logic_vector(42 downto 0);
    begin
        v_slv(0)            := rec.valid;
        v_slv(32 downto 1)  := rec.user;
        v_slv(34 downto 33) := rec.resp;
        v_slv(42 downto 35) := rec.id;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_B_t_slv) return AXI4_B_t is
        variable v_rec : AXI4_B_t;
    begin
        v_rec.valid := slv(0);
        v_rec.user  := slv(32 downto 1);
        v_rec.resp  := slv(34 downto 33);
        v_rec.id    := slv(42 downto 35);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_AR_t) return AXI4_AR_t_slv is
        variable v_slv : std_logic_vector(101 downto 0);
    begin
        v_slv(0)             := rec.valid;
        v_slv(32 downto 1)   := rec.user;
        v_slv(36 downto 33)  := rec.region;
        v_slv(40 downto 37)  := rec.qos;
        v_slv(43 downto 41)  := rec.prot;
        v_slv(47 downto 44)  := rec.cache;
        v_slv(48)            := rec.lock;
        v_slv(50 downto 49)  := rec.burst;
        v_slv(53 downto 51)  := rec.size;
        v_slv(61 downto 54)  := rec.len;
        v_slv(93 downto 62)  := rec.addr;
        v_slv(101 downto 94) := rec.id;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_AR_t_slv) return AXI4_AR_t is
        variable v_rec : AXI4_AR_t;
    begin
        v_rec.valid  := slv(0);
        v_rec.user   := slv(32 downto 1);
        v_rec.region := slv(36 downto 33);
        v_rec.qos    := slv(40 downto 37);
        v_rec.prot   := slv(43 downto 41);
        v_rec.cache  := slv(47 downto 44);
        v_rec.lock   := slv(48);
        v_rec.burst  := slv(50 downto 49);
        v_rec.size   := slv(53 downto 51);
        v_rec.len    := slv(61 downto 54);
        v_rec.addr   := slv(93 downto 62);
        v_rec.id     := slv(101 downto 94);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_R_t) return AXI4_R_t_slv is
        variable v_slv : std_logic_vector(75 downto 0);
    begin
        v_slv(0)            := rec.valid;
        v_slv(32 downto 1)  := rec.user;
        v_slv(33)           := rec.last;
        v_slv(35 downto 34) := rec.resp;
        v_slv(67 downto 36) := rec.data;
        v_slv(75 downto 68) := rec.id;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_R_t_slv) return AXI4_R_t is
        variable v_rec : AXI4_R_t;
    begin
        v_rec.valid := slv(0);
        v_rec.user  := slv(32 downto 1);
        v_rec.last  := slv(33);
        v_rec.resp  := slv(35 downto 34);
        v_rec.data  := slv(67 downto 36);
        v_rec.id    := slv(75 downto 68);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_MOSI_t) return AXI4_MOSI_t_slv is
        variable v_slv : std_logic_vector(283 downto 0);
    begin
        v_slv(0)              := rec.bready;
        v_slv(1)              := rec.rready;
        v_slv(79 downto 2)    := to_slv(rec.w);
        v_slv(181 downto 80)  := to_slv(rec.aw);
        v_slv(283 downto 182) := to_slv(rec.ar);
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_MOSI_t_slv) return AXI4_MOSI_t is
        variable v_rec : AXI4_MOSI_t;
    begin
        v_rec.bready := slv(0);
        v_rec.rready := slv(1);
        v_rec.w      := from_slv(slv(79 downto 2));
        v_rec.aw     := from_slv(slv(181 downto 80));
        v_rec.ar     := from_slv(slv(283 downto 182));
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_SOMI_t) return AXI4_SOMI_t_slv is
        variable v_slv : std_logic_vector(121 downto 0);
    begin
        v_slv(0)             := rec.wready;
        v_slv(1)             := rec.awready;
        v_slv(2)             := rec.arready;
        v_slv(45 downto 3)   := to_slv(rec.b);
        v_slv(121 downto 46) := to_slv(rec.r);
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_SOMI_t_slv) return AXI4_SOMI_t is
        variable v_rec : AXI4_SOMI_t;
    begin
        v_rec.wready  := slv(0);
        v_rec.awready := slv(1);
        v_rec.arready := slv(2);
        v_rec.b       := from_slv(slv(45 downto 3));
        v_rec.r       := from_slv(slv(121 downto 46));
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_1024_t) return AXI4_Streaming_1024_t_slv is
        variable v_slv : std_logic_vector(1154 downto 0);
    begin
        v_slv(0)               := rec.error;
        v_slv(128 downto 1)    := rec.keep;
        v_slv(129)             := rec.last;
        v_slv(130)             := rec.valid;
        v_slv(1154 downto 131) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_1024_t_slv) return AXI4_Streaming_1024_t is
        variable v_rec : AXI4_Streaming_1024_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(128 downto 1);
        v_rec.last  := slv(129);
        v_rec.valid := slv(130);
        v_rec.data  := slv(1154 downto 131);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_512_t) return AXI4_Streaming_512_t_slv is
        variable v_slv : std_logic_vector(578 downto 0);
    begin
        v_slv(0)             := rec.error;
        v_slv(64 downto 1)   := rec.keep;
        v_slv(65)            := rec.last;
        v_slv(66)            := rec.valid;
        v_slv(578 downto 67) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_512_t_slv) return AXI4_Streaming_512_t is
        variable v_rec : AXI4_Streaming_512_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(64 downto 1);
        v_rec.last  := slv(65);
        v_rec.valid := slv(66);
        v_rec.data  := slv(578 downto 67);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_256_t) return AXI4_Streaming_256_t_slv is
        variable v_slv : std_logic_vector(290 downto 0);
    begin
        v_slv(0)             := rec.error;
        v_slv(32 downto 1)   := rec.keep;
        v_slv(33)            := rec.last;
        v_slv(34)            := rec.valid;
        v_slv(290 downto 35) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_256_t_slv) return AXI4_Streaming_256_t is
        variable v_rec : AXI4_Streaming_256_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(32 downto 1);
        v_rec.last  := slv(33);
        v_rec.valid := slv(34);
        v_rec.data  := slv(290 downto 35);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_128_t) return AXI4_Streaming_128_t_slv is
        variable v_slv : std_logic_vector(146 downto 0);
    begin
        v_slv(0)             := rec.error;
        v_slv(16 downto 1)   := rec.keep;
        v_slv(17)            := rec.last;
        v_slv(18)            := rec.valid;
        v_slv(146 downto 19) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_128_t_slv) return AXI4_Streaming_128_t is
        variable v_rec : AXI4_Streaming_128_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(16 downto 1);
        v_rec.last  := slv(17);
        v_rec.valid := slv(18);
        v_rec.data  := slv(146 downto 19);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_64_t) return AXI4_Streaming_64_t_slv is
        variable v_slv : std_logic_vector(74 downto 0);
    begin
        v_slv(0)            := rec.error;
        v_slv(8 downto 1)   := rec.keep;
        v_slv(9)            := rec.last;
        v_slv(10)           := rec.valid;
        v_slv(74 downto 11) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_64_t_slv) return AXI4_Streaming_64_t is
        variable v_rec : AXI4_Streaming_64_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(8 downto 1);
        v_rec.last  := slv(9);
        v_rec.valid := slv(10);
        v_rec.data  := slv(74 downto 11);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_32_t) return AXI4_Streaming_32_t_slv is
        variable v_slv : std_logic_vector(38 downto 0);
    begin
        v_slv(0)           := rec.error;
        v_slv(4 downto 1)  := rec.keep;
        v_slv(5)           := rec.last;
        v_slv(6)           := rec.valid;
        v_slv(38 downto 7) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_32_t_slv) return AXI4_Streaming_32_t is
        variable v_rec : AXI4_Streaming_32_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(4 downto 1);
        v_rec.last  := slv(5);
        v_rec.valid := slv(6);
        v_rec.data  := slv(38 downto 7);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_16_t) return AXI4_Streaming_16_t_slv is
        variable v_slv : std_logic_vector(20 downto 0);
    begin
        v_slv(0)           := rec.error;
        v_slv(2 downto 1)  := rec.keep;
        v_slv(3)           := rec.last;
        v_slv(4)           := rec.valid;
        v_slv(20 downto 5) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_16_t_slv) return AXI4_Streaming_16_t is
        variable v_rec : AXI4_Streaming_16_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(2 downto 1);
        v_rec.last  := slv(3);
        v_rec.valid := slv(4);
        v_rec.data  := slv(20 downto 5);
    return v_rec;
    end function from_slv;
    
    function to_slv (rec : AXI4_Streaming_8_t) return AXI4_Streaming_8_t_slv is
        variable v_slv : std_logic_vector(11 downto 0);
    begin
        v_slv(0)           := rec.error;
        v_slv(1 downto 1)  := rec.keep;
        v_slv(2)           := rec.last;
        v_slv(3)           := rec.valid;
        v_slv(11 downto 4) := rec.data;
        return v_slv;
    end function to_slv;
    
    function from_slv (slv : AXI4_Streaming_8_t_slv) return AXI4_Streaming_8_t is
        variable v_rec : AXI4_Streaming_8_t;
    begin
        v_rec.error := slv(0);
        v_rec.keep  := slv(1 downto 1);
        v_rec.last  := slv(2);
        v_rec.valid := slv(3);
        v_rec.data  := slv(11 downto 4);
    return v_rec;
    end function from_slv;
    
    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end body

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_AR_t is
        variable rec : AXI4_AR_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_R_t is
        variable rec : AXI4_R_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_AW_t is
        variable rec : AXI4_AW_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_W_t is
        variable rec : AXI4_W_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_B_t is
        variable rec : AXI4_B_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_8_t is
        variable rec : AXI4_Streaming_8_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_16_t is
        variable rec : AXI4_Streaming_16_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_32_t is
        variable rec : AXI4_Streaming_32_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_64_t is
        variable rec : AXI4_Streaming_64_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_128_t is
        variable rec : AXI4_Streaming_128_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_256_t is
        variable rec : AXI4_Streaming_256_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_512_t is
        variable rec : AXI4_Streaming_512_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_1024_t is
        variable rec : AXI4_Streaming_1024_t;
    begin
        rec       := from_slv(slv);
        rec.valid := vld;
        return rec;
    end function from_slv;

    function empty_to_keep (
            empty : std_logic_vector;
            last  : std_logic := '1'
        ) return std_logic_vector is
        variable keep : std_logic_vector(2**empty'length-1 downto 0);
    begin
        if not last then
            keep := (others => '1');
        else
            for idx in keep'range loop
                if idx >= to_01(unsigned(empty)) then
                    keep(idx) := '1';
                else
                    keep(idx) := '0';
                end if;
            end loop;
        end if;
        return keep;
    end function empty_to_keep;

    function keep_to_empty (
            keep : std_logic_vector;
            last  : std_logic := '1'
        ) return std_logic_vector is
        variable empty : unsigned(ceil_log2(keep'length+1)-1 downto 0);
    begin
        if not last then
            empty := (others => '0');
        else
            empty  := count_ones(not keep);
        end if;
        return std_logic_vector(resize(empty, ceil_log2(keep'length)));
    end function keep_to_empty;

end package body AXI4_pkg;
