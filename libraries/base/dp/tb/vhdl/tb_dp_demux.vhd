-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_demux is
  generic (
    -- Try DEMUX settings
    g_dut_nof_output  : natural := 1;  -- >= 1, when 1 then use array_init() to assign the unconstrained t_natural_arr generic arrays
    g_combined        : boolean := false
  );
end tb_dp_demux;

architecture tb of tb_dp_demux is
  -- See tb_dp_pkg.vhd for explanation and run time

  -- DUT
  constant c_dut_in_latency    : natural := 1;  -- fixed for dp_demux
  constant c_dut_out_latency   : natural := 1;  -- fixed for dp_demux

  -- The mux input streams have a channel field (c_dp_channel_user_w) and the mux will add some LSBits to represent the
  -- input port numbers (c_dut_nof_port_w) in the mux output stream channel field. The total mux output stream channel
  -- must fit in c_dp_stream_channel_w. The mux is connected to the demux DUT.
  constant c_dut_out_channel_w  : natural := c_dp_channel_user_w;
  constant c_dut_nof_port_w     : natural := true_log2(g_dut_nof_output);
  constant c_mux_channel_w      : natural := c_dut_out_channel_w + c_dut_nof_port_w;

  -- Stimuli
  constant c_tx_latency     : natural := c_dut_in_latency;  -- TX ready latency of TB
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0
  constant c_tx_offset_sop  : natural := 3;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 3,  10,   17, ...
  constant c_tx_offset_eop  : natural := 9;  -- eop in data valid cycle   9,   16,   23, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_rx_latency     : natural := c_dut_out_latency;  -- RX ready latency from DUT
  constant c_verify_en_wait : natural := 20;  -- wait some cycles before asserting verify enable

  constant c_empty_offset   : natural := 1;  -- dummy number to distinghuis the empty   field from the data field value
  constant c_channel_offset : natural := 2;  -- dummy number to distinghuis the channel field from the data field value

  constant c_random_w       : natural := 19;

  signal tb_end_vec     : std_logic_vector(0 to g_dut_nof_output - 1) := (others => '0');
  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal sync_dly       : std_logic_vector(0 to g_dut_nof_output - 1);
  signal lfsr1          : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal lfsr2          : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));

  signal cnt_dat        : t_dp_data_arr(0 to g_dut_nof_output - 1);
  signal cnt_val        : std_logic_vector(0 to g_dut_nof_output - 1);
  signal cnt_en         : std_logic_vector(0 to g_dut_nof_output - 1);

  type t_dp_state_enum_arr is array (natural range <>) of t_dp_state_enum;

  type t_tx_data_arr_arr  is array (natural range <>) of t_dp_data_arr(   0 to c_tx_latency + c_tx_void);
  type t_tx_val_arr_arr   is array (natural range <>) of std_logic_vector(0 to c_tx_latency + c_tx_void);

  type t_prev_out_ready_arr_arr is array (natural range <>) of std_logic_vector(0 to c_rx_latency);

  signal tx_data        : t_tx_data_arr_arr(0 to g_dut_nof_output - 1) := (others => (others => (others => '0')));
  signal tx_val         : t_tx_val_arr_arr( 0 to g_dut_nof_output - 1) :=          (others => (others => '0'));

  signal in_ready       : std_logic_vector(0 to g_dut_nof_output - 1);
  signal in_data        : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal in_empty       : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal in_channel     : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal in_val         : std_logic_vector(0 to g_dut_nof_output - 1);
  signal in_sop         : std_logic_vector(0 to g_dut_nof_output - 1);
  signal in_eop         : std_logic_vector(0 to g_dut_nof_output - 1);

  signal in_data_vec    : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_empty_vec   : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_channel_vec : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0) := (others => '0');

  signal in_siso        : t_dp_siso_arr(0 to g_dut_nof_output - 1);
  signal in_sosi        : t_dp_sosi_arr(0 to g_dut_nof_output - 1) := (others => c_dp_sosi_rst);
  signal mux_siso       : t_dp_siso;
  signal mux_sosi       : t_dp_sosi;
  signal demux_siso     : t_dp_siso_arr(0 to g_dut_nof_output - 1);
  signal demux_sosi     : t_dp_sosi_arr(0 to g_dut_nof_output - 1);

  signal demux_data     : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0);
  signal demux_empty    : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0);
  signal demux_channel  : std_logic_vector(g_dut_nof_output * c_dp_data_w - 1 downto 0);
  signal demux_val      : std_logic_vector(g_dut_nof_output        - 1 downto 0);
  signal demux_sop      : std_logic_vector(g_dut_nof_output        - 1 downto 0);
  signal demux_eop      : std_logic_vector(g_dut_nof_output        - 1 downto 0);
  signal demux_ready    : std_logic_vector(g_dut_nof_output        - 1 downto 0);

  signal out_ready      : std_logic_vector(        0 to g_dut_nof_output - 1);
  signal prev_out_ready : t_prev_out_ready_arr_arr(0 to g_dut_nof_output - 1);

  signal out_data       : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal out_empty      : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal out_channel    : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));
  signal out_val        : std_logic_vector(0 to g_dut_nof_output - 1);
  signal out_sop        : std_logic_vector(0 to g_dut_nof_output - 1);
  signal out_eop        : std_logic_vector(0 to g_dut_nof_output - 1);
  signal hold_out_sop   : std_logic_vector(0 to g_dut_nof_output - 1);
  signal prev_out_data  : t_dp_data_arr(0 to g_dut_nof_output - 1) := (others => (others => '0'));

  signal state          : t_dp_state_enum_arr(0 to g_dut_nof_output - 1);

  signal verify_en      : std_logic;
  signal verify_done    : std_logic_vector(0 to g_dut_nof_output - 1);

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(5000, c_dp_data_w);
begin
  tb_end <= vector_and(tb_end_vec);

  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  io : for I in 0 to g_dut_nof_output - 1 generate
    sync_dly(I) <= transport sync after clk_period * I;

    -- Input data
    cnt_val <= in_ready and cnt_en;

    proc_dp_cnt_dat(rst, clk, cnt_val(I), cnt_dat(I));
    proc_dp_tx_data(c_tx_latency, rst, clk, cnt_val(I), cnt_dat(I), tx_data(I), tx_val(I), in_data(I), in_val(I));
    proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data(I), in_val(I), in_sop(I));
    proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data(I), in_val(I), in_eop(I));

    in_empty(I)   <= INCR_UVEC(in_data(I), c_empty_offset);
    in_channel(I) <= INCR_UVEC(in_data(I), c_channel_offset);

    -- Input wires
    in_data_vec(   (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_data(I);
    in_empty_vec(  (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_empty(I);
    in_channel_vec((I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_channel(I);

    -- Stimuli control
--     proc_dp_count_en(rst, clk, sync, lfsr1(I)(c_random_w-1 DOWNTO 0), state(I), verify_done(I), tb_end_vec(I), cnt_en(I));  -- all cnt_en behave the same
--     proc_dp_out_ready(rst, clk, sync, lfsr2(I)(c_random_w DOWNTO 0), out_ready(I));                                         -- all out_ready behave the same
    proc_dp_count_en(rst, clk, sync_dly(I), lfsr1(I)(c_random_w - 1 downto 0), state(I), verify_done(I), tb_end_vec(I), cnt_en(I));  -- all cnt_en are relatively delayed
    proc_dp_out_ready(rst, clk, sync_dly(I), lfsr2(I)(c_random_w downto 0), out_ready(I));  -- all out_ready are relatively delayed

    -- Output mapping
    out_data(I)    <= demux_data(   (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w);
    out_empty(I)   <= demux_empty(  (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w);
    out_channel(I) <= demux_channel((I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w);
    out_val(I)     <= demux_val(I);
    out_sop(I)     <= demux_sop(I);
    out_eop(I)     <= demux_eop(I);
    demux_ready(I) <= out_ready(I);

    -- Output verify
    proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
    proc_dp_verify_data("demux_data", c_rx_latency, clk, verify_en, out_ready(I), out_val(I), out_data(I), prev_out_data(I));
    proc_dp_verify_valid(c_rx_latency, clk, verify_en, out_ready(I), prev_out_ready(I), out_val(I));
    proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, out_data(I), out_val(I), out_sop(I));
    proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, out_data(I), out_val(I), out_eop(I));
    proc_dp_verify_sop_and_eop(clk, out_val(I), out_sop(I), out_eop(I), hold_out_sop(I));  -- Verify that sop and eop come in pairs

    -- No need to use g_dut_use_empty, because empty is taken care of by default when the FIFO is not used in dp_mux
    proc_dp_verify_other_sosi("empty", INCR_UVEC(out_data(I), c_empty_offset), clk, verify_en, out_empty(I));
    -- No need to use g_dut_use_channel, because channel is taken care of by default when the FIFO is not used in dp_mux
    proc_dp_verify_other_sosi("channel", INCR_UVEC(out_data(I), c_channel_offset), clk, verify_en, out_channel(I));

    -- Check that the test has ran at all
    proc_dp_verify_value(e_at_least, clk, verify_done(I), exp_data, out_data(I));
  end generate;

  -- map sl, slv to record
  p_in : process(in_siso, in_data_vec, in_empty_vec, in_channel_vec, in_val, in_sop, in_eop)
  begin
    for I in 0 to g_dut_nof_output - 1 loop
      in_ready(I) <= in_siso(I).ready;  -- SISO
      in_sosi(I).data(   c_dp_data_w - 1 downto 0) <= in_data_vec(                (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w);  -- SOSI
      in_sosi(I).empty                           <= in_empty_vec(  c_dp_empty_w   + I * c_dp_data_w - 1 downto I * c_dp_data_w);
      in_sosi(I).channel                         <= in_channel_vec(c_dp_channel_w + I * c_dp_data_w - 1 downto I * c_dp_data_w);
      in_sosi(I).valid                           <= in_val(I);
      in_sosi(I).sop                             <= in_sop(I);
      in_sosi(I).eop                             <= in_eop(I);
    end loop;
  end process;

  p_out : process(demux_ready, demux_sosi)
  begin
    for I in 0 to g_dut_nof_output - 1 loop
      demux_siso(I).ready <= demux_ready(I);  -- SISO
      demux_data(                (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= demux_sosi(I).data(   c_dp_data_w - 1 downto 0);  -- SOSI
      demux_empty(  c_dp_empty_w   + I * c_dp_data_w - 1 downto I * c_dp_data_w) <= demux_sosi(I).empty;
      demux_channel(c_dp_channel_w + I * c_dp_data_w - 1 downto I * c_dp_data_w) <= demux_sosi(I).channel;
      demux_val(I)                                                         <= demux_sosi(I).valid;
      demux_sop(I)                                                         <= demux_sosi(I).sop;
      demux_eop(I)                                                         <= demux_sosi(I).eop;
    end loop;
  end process;

  mux : entity work.dp_mux
  generic map (
    g_data_w          => c_dp_data_w,
    g_empty_w         => c_dp_empty_w,
    g_in_channel_w    => c_dp_data_w,
    g_error_w         => 1,
    g_use_empty       => true,
    g_use_in_channel  => true,
    g_use_error       => false,
    g_nof_input       => g_dut_nof_output,
    g_use_fifo        => false,
    g_fifo_size       => array_init(1024, g_dut_nof_output),  -- FIFO is not used, but generic must match g_nof_input
    g_fifo_fill       => array_init(   0, g_dut_nof_output)  -- FIFO is not used, but generic must match g_nof_input
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => in_siso,  -- OUT = request to upstream ST source
    snk_in_arr  => in_sosi,
    -- ST source
    src_in      => mux_siso,  -- IN  = request from downstream ST sink
    src_out     => mux_sosi
  );

  ------------------------------------------------------------------------------
  -- DUT dp_demux
  ------------------------------------------------------------------------------

  dut : entity work.dp_demux
  generic map (
    g_nof_output    => g_dut_nof_output,
    g_combined      => g_combined
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out     => mux_siso,  -- OUT = request to upstream ST source
    snk_in      => mux_sosi,
    -- ST source
    src_in_arr  => demux_siso,  -- IN  = request from downstream ST sink
    src_out_arr => demux_sosi
  );
end tb;
