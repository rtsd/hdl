onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group #vsim_capacity# -format Analog-Step -height 500 -radix decimal /#vsim_capacity#/totals
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/classes
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/qdas
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/assertions
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/covergroups
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/solver
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/memories
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/jesd204b_refclk
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/jesd204b_sysref
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned -childformat {{/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(11) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(10) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(9) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(8) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(7) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(6) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(5) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(4) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(3) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(2) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(1) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(0) -radix unsigned}} -expand -subitemconfig {/tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(11) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(10) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(9) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(8) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(7) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(6) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(5) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(4) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(3) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(2) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(1) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr(0) {-height 16 -radix unsigned}} /tb_tech_jesd204b/u_jesd204b/jesd204b_sync_n_arr
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/rx_clk
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/rx_rst
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/rx_sysref
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/mm_clk
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/mm_rst
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/jesd204b_mosi
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/jesd204b_miso
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned /tb_tech_jesd204b/u_jesd204b/serial_tx_arr
add wave -noupdate -expand -group tb_tech_jesd204b -expand -group u_jesd204b -radix unsigned -childformat {{/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(11) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(10) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(9) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(8) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(7) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(6) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(5) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(4) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(3) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(2) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(1) -radix unsigned} {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(0) -radix unsigned}} -subitemconfig {/tb_tech_jesd204b/u_jesd204b/serial_rx_arr(11) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(10) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(9) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(8) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(7) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(6) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(5) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(4) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(3) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(2) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(1) {-height 16 -radix unsigned} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr(0) {-height 16 -radix unsigned}} /tb_tech_jesd204b/u_jesd204b/serial_rx_arr
add wave -noupdate -expand -group tb_tech_jesd204b /tb_tech_jesd204b/rx_sosi_arr
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/txlink_clk
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/dev_sync_n
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/txphy_clk
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/mm_rst
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/avs_rst_n
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/txlink_rst_n
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/tx_analogreset
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/tx_digitalreset
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/tx_bonding_clocks
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_0
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_1
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_2
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_3
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_4
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bonding_clock_5
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/pll_locked
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/mm_clk
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/tb_end
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/sim_done
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_mosi
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_miso
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/serial_tx
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/bck_rx
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sysref
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sampclk
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/rx_clk
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/rx_rst
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/rx_sysref
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sampclk_fpga
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sampclk_adc
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sysref_fpga
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sysref_adc
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sysref_adc_1
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_sysref_adc_2
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_tx_link_data_arr
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_tx_link_valid
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_tx_link_ready
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/jesd204b_tx_frame_ready
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/avs_chipselect
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/avs_read
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/avs_readdata
add wave -noupdate -expand -group tb_tech_jesd204b -radix unsigned /tb_tech_jesd204b/avs_address
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1547975361585 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 300
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {1541216595200 fs} {1725392979200 fs}
