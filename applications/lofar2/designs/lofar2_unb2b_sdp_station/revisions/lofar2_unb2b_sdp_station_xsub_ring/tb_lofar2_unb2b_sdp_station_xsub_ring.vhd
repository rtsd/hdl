-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2b_sdp_station_xsub_ring using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--        freq = 19.921875MHz = subband index 102
--        ampl = 0.5 * 2**13, full scale amplitude is 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Read crosslets statistics (XST) via ram_st_xsq and verify that the values
--      are as expected. This is done by comparing the values in the outgoing square
--      correlation matrix.
--
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--   Takes about 40 m
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, tech_pll_lib, lofar2_unb2b_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity tb_lofar2_unb2b_sdp_station_xsub_ring is
end tb_lofar2_unb2b_sdp_station_xsub_ring;

architecture tb of tb_lofar2_unb2b_sdp_station_xsub_ring is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_nof_rn          : natural := 2;
  constant c_P_sq            : natural := (c_nof_rn / 2) + 1;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 24;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_ctrl_interval_size  : natural := c_nof_clk_per_sync;

  constant c_percentage          : real := 0.05;  -- percentage that actual value may differ from expected value
  constant c_lo_factor           : real := 1.0 - c_percentage;  -- lower boundary
  constant c_hi_factor           : real := 1.0 + c_percentage;  -- higher boundary
  constant c_nof_lanes           : natural := 1;

  -- WG
  constant c_bsn_start_wg         : natural := 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  constant c_ampl_sp_0            : natural := c_sdp_FS_adc / 2;  -- = 0.5 * FS, so in number of lsb
  constant c_wg_freq_offset       : real := 0.0 / 11.0;  -- in freq_unit
  constant c_subband_sp_0         : real := 102.0;  -- Select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
  constant c_exp_wg_power_sp_0    : real := real(c_ampl_sp_0**2) / 2.0 * real(c_nof_clk_per_sync);

  -- WPFB
  constant c_nof_pfb                        : natural := 1;  -- Verifying 1 of c_sdp_P_pfb = 6 pfb to speed up simulation.
  constant c_wb_leakage_bin                 : natural := c_wpfb_sim.nof_points / c_wpfb_sim.wb_factor;  -- = 256, leakage will occur in this bin if FIR wb_factor is reversed
  constant c_exp_sp_subband_power_ratio     : real := 1.0 / 8.0;  -- depends on internal WPFB quantization and FIR coefficients
  constant c_exp_sp_subband_power_sum_ratio : real := c_exp_sp_subband_power_ratio;  -- because all sinus power is expected in one subband
  constant c_exp_subband_power_sp_0         : real := c_exp_wg_power_sp_0 * c_exp_sp_subband_power_ratio;

  type t_real_arr is array (integer range <>) of real;

  -- MM
  constant c_mm_file_reg_bsn_source_v2           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg                 : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_ram_st_sst                  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_SST";
  constant c_mm_file_reg_crosslets_info          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_CROSSLETS_INFO";
  constant c_mm_file_reg_bsn_sync_scheduler_xsub : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SYNC_SCHEDULER_XSUB";
  constant c_mm_file_ram_st_xsq                  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_XSQ";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0) := (others => '0');

  signal i_QSFP_0_TX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_QSFP_0_RX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_0_TX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_0_RX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_1_TX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_1_RX         : t_unb2b_board_qsfp_bus_2arr(c_nof_rn - 1 downto 0) := (others => (others => '0'));

  -- WG
  signal current_bsn_wg          : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- WPFB
  signal xsub_stats_arr         : t_slv_64_arr(0 to c_P_sq * c_nof_complex * c_sdp_X_sq - 1);

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';
  signal SA_CLK              : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0) := (others => '0');

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  gen_dut : for RN in 0 to c_nof_rn - 1 generate
    u_lofar_unb2b_sdp_station_xsub_ring : entity lofar2_unb2b_sdp_station_lib.lofar2_unb2b_sdp_station
    generic map (
      g_design_name            => "lofar2_unb2b_sdp_station_xsub_ring",
      g_design_note            => "",
      g_sim                    => c_sim,
      g_sim_unb_nr             => c_unb_nr + (RN / c_quad),
      g_sim_node_nr            => RN mod c_quad,
      g_wpfb                   => c_wpfb_sim,
      g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync,
      g_scope_selected_subband => natural(c_subband_sp_0)
    )
    port map (
      -- GENERAL
      CLK          => ext_clk,
      PPS          => pps,
      WDI          => WDI,
      INTA         => INTA,
      INTB         => INTB,

      -- Others
      VERSION      => c_version,
      ID           => ( TO_UVEC(RN / c_quad, c_unb2b_board_nof_uniboard_w) & TO_UVEC(RN mod c_quad, c_unb2b_board_nof_chip_w) ),
      TESTIO       => open,

      -- I2C Interface to Sensors
      SENS_SC      => sens_scl,
      SENS_SD      => sens_sda,

      PMBUS_SC     => pmbus_scl,
      PMBUS_SD     => pmbus_sda,
      PMBUS_ALERT  => open,

      -- 1GbE Control Interface
      ETH_CLK      => eth_clk,
      ETH_SGIN     => eth_rxp,
      ETH_SGOUT    => eth_txp,

      -- Transceiver clocks
      SA_CLK       => SA_CLK,
      -- front transceivers
      QSFP_0_RX    => i_QSFP_0_RX(RN),
      QSFP_0_TX    => i_QSFP_0_TX(RN),

      -- ring transceivers
      RING_0_RX    => i_RING_0_RX(RN),
      RING_0_TX    => i_RING_0_TX(RN),
      RING_1_RX    => i_RING_1_RX(RN),
      RING_1_TX    => i_RING_1_TX(RN),

      -- LEDs
      QSFP_LED     => open,

      -- back transceivers
      JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
      JESD204B_REFCLK      => JESD204B_REFCLK,

      -- jesd204b syncronization signals
      JESD204B_SYSREF => jesd204b_sysref,
      JESD204B_SYNC_N => jesd204b_sync_n
    );
  end generate;

  -- Ring connections
  gen_ring : for I in 0 to c_nof_rn - 2 generate
    -- Connect consecutive nodes with RING interfaces (PCB)
    i_RING_0_RX(I + 1) <= i_RING_1_TX(I);
    i_RING_1_RX(I)   <= i_RING_0_TX(I + 1);
  end generate;

  -- Connect first and last nodes with QSFP interface.
  i_QSFP_0_RX(0)          <= i_QSFP_0_TX(c_nof_rn - 1);
  i_QSFP_0_RX(c_nof_rn - 1) <= i_QSFP_0_TX(0);

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                   : natural;
    variable v_sp_subband_power      : real;
    variable v_W, v_C, v_A, v_X, v_B, v_A_even, v_B_even, v_SQ_offset: natural;  -- array indicies
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Enable BSN
    ----------------------------------------------------------------------------
    for RN in 0 to c_nof_rn - 1 loop
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SOURCE_V2", 3,                    0, tb_clk);
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SOURCE_V2", 2,                    0, tb_clk);  -- Init BSN = 0
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SOURCE_V2", 1,   c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SOURCE_V2", 0,         16#00000003#, tb_clk);  -- Enable BSN at PPS
    end loop;

    -- Release PPS pulser, to get first PPS now and to start BSN source
    wait for 1 us;
    pps_rst <= '0';
    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    ----------------------------------------------------------------------------
    -- Ring config
    ----------------------------------------------------------------------------
    -- Write ring configuration to all nodes.
    for RN in 0 to c_nof_rn - 1 loop
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_INFO", 2, c_nof_rn, tb_clk);  -- N_rn
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_INFO", 3, 0,        tb_clk);  -- O_rn
    end loop;

    -- Start node specific settings
    mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr, 0) & "REG_RING_INFO", 0, 1, tb_clk);  -- use_ring_to_previous_rn = 1
    mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr, 0) & "REG_RING_INFO", 1, 0, tb_clk);  -- use_ring_to_next_rn = 0

    -- End node specific settings
    mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + ((c_nof_rn - 1) / c_quad), (c_nof_rn - 1) mod c_quad) & "REG_RING_INFO", 0, 0, tb_clk);  -- use_ring_to_previous_rn = 0
    mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + ((c_nof_rn - 1) / c_quad), (c_nof_rn - 1) mod c_quad) & "REG_RING_INFO", 1, 1, tb_clk);  -- use_ring_to_next_rn = 1

    -- Access scheme 3. Each RN creates packets and sends them along the ring.
    for RN in 0 to c_nof_rn - 1 loop
      for I in 0 to c_nof_lanes - 1 loop
        -- Set transport_nof_hops to N_rn-1 on all nodes.
        mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_LANE_INFO_XST", I * 2 + 1, c_nof_rn - 1, tb_clk);
      end loop;

    ----------------------------------------------------------------------------
    -- Disable unused streams in dp_bsn_align_v2
    ----------------------------------------------------------------------------
      for I in 0 to c_sdp_P_sq - 1 loop
        if I >= c_P_sq then
          mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_ALIGN_V2_XSUB", I, 0, tb_clk);
        end if;
      end loop;

    ----------------------------------------------------------------------------
    -- Crosslets Info
    ----------------------------------------------------------------------------
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_CROSSLETS_INFO", 0,  integer(c_subband_sp_0), tb_clk);  -- offset
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_CROSSLETS_INFO", 15, 0,                       tb_clk);  -- stepsize
    end loop;
    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    for RN in 0 to c_nof_rn - 1 loop
      for I in 0 to c_sdp_S_pn - 1 loop
        mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_WG", I * 4 + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
        mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_WG", I * 4 + 1, integer(  0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
        mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_WG", I * 4 + 2, integer((c_subband_sp_0 + c_wg_freq_offset) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
        mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_WG", I * 4 + 3, integer(real(c_ampl_sp_0) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
      end loop;
    end loop;

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;

    for RN in 0 to c_nof_rn - 1 loop
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SCHEDULER", 0, c_bsn_start_wg, tb_clk);  -- first write low then high part
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SCHEDULER", 1,              0, tb_clk);  -- assume v_bsn < 2**31-1

      -- bsn_scheduler_xsub
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SYNC_SCHEDULER_XSUB", 1, c_ctrl_interval_size, tb_clk);  -- Interval size
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SYNC_SCHEDULER_XSUB", 2,       c_bsn_start_wg, tb_clk);  -- first write low then high part
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SYNC_SCHEDULER_XSUB", 3,                    0, tb_clk);  -- assume v_bsn < 2**31-1
      mmf_mm_bus_wr(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_SYNC_SCHEDULER_XSUB", 0,                    1, tb_clk);  -- enable
    end loop;

    -- Wait for enough WG data and start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 2,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    ---------------------------------------------------------------------------
    -- Read crosslet statistics
    ---------------------------------------------------------------------------
    -- Only checking P_sq index = 1 (correlating singnals from node 0 and 1) as P_sq index = 0 is verified in tb_lofar2_unb2b_sdp_station_xsub_one.
    -- Also in simulation, the MM file IO is too slow to read and verify more than 1 P_sq during 1 sync interval. Also, this way the simulation duration is shorter.
    for I in 0 to c_nof_complex * c_sdp_X_sq * (c_longword_sz / c_word_sz) - 1 loop
      v_W := I mod 2;
      v_B := I / 2;
      v_SQ_offset := 2**ceil_log2(c_sdp_N_crosslets_max * c_nof_complex * c_sdp_X_sq * (c_longword_sz / c_word_sz));  -- Address offset for next P_sq.
      if v_W = 0 then
        -- low part
        mmf_mm_bus_rd(c_mm_file_ram_st_xsq, v_SQ_offset + I, rd_data, tb_clk);
        xsub_stats_arr(v_B)(31 downto 0) <= rd_data;
      else
        -- high part
        mmf_mm_bus_rd(c_mm_file_ram_st_xsq, v_SQ_offset + I, rd_data, tb_clk);
        xsub_stats_arr(v_B)(63 downto 32) <= rd_data;
      end if;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    ---------------------------------------------------------------------------
    -- Verify crosslet statistics
    ---------------------------------------------------------------------------
    -- With all WGs having the same input all crosslets should be identical. Due to quantization cross talk
    -- between the two real inputs of the filterbank the two signals in the output pairs per P_pfb differ
    -- slightly, therefore 3 slightly different correlation values are expected. 1 for each correlation
    -- between even indexed signals, 1 for odd indexed signals and 1 for correlations between even and odd
    -- indexed signals. This is verified by checking if these values are the same.
    for I in 0 to c_nof_complex * c_sdp_X_sq - 1 loop
      v_C := I mod 2;
      v_X := I / c_nof_complex;
      v_A := v_X mod c_sdp_S_pn;
      v_B := v_X / c_sdp_S_pn;
      v_A_even := v_A mod 2;
      v_B_even := v_B mod 2;

      -- Check real values of even indices
      if v_C = 0 and v_A_even = 0 and v_B_even = 0 then
        assert signed(xsub_stats_arr(I)) = signed(xsub_stats_arr(0))
          report "correlation between even indexed signals (re) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Check real values of odd indices
      if v_C = 0 and v_A_even = 1 and v_B_even = 1 then
        assert signed(xsub_stats_arr(I)) = signed(xsub_stats_arr((c_sdp_S_pn + 1) * c_nof_complex))
          report "correlation between odd indexed signals (re) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Check real values of even correlated with odd indices
      if v_C = 0 and (v_A_even = 0 xor v_B_even = 0) then
        assert signed(xsub_stats_arr(I)) = signed(xsub_stats_arr(1 * c_nof_complex))
          report "correlation between even indexed signals (re) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Using absolute value of the imaginary part (force positive) as the sign can be opposite when comparing A to B vs B to A.
      -- Check im values of even indices
      if v_C = 1 and v_A_even = 0 and v_B_even = 0 then
        assert abs(signed(xsub_stats_arr(I))) = abs(signed(xsub_stats_arr(1)))
          report "correlation between even indexed signals (im) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Check im values of odd indices
      if v_C = 1 and v_A_even = 1 and v_B_even = 1 then
        assert abs(signed(xsub_stats_arr(I))) = abs(signed(xsub_stats_arr((c_sdp_S_pn + 1) * c_nof_complex + 1)))
          report "correlation between odd indexed signals (im) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Check im values of even correlated with odd indices
      if v_C = 1 and (v_A_even = 0 xor v_B_even = 0) then
        assert abs(signed(xsub_stats_arr(I))) = abs(signed(xsub_stats_arr(1 * c_nof_complex + 1)))
          report "correlation between even/odd indexed signals (im) is wrong at I = " & int_to_str(I)
          severity ERROR;
        end if;

      -- Check if values are > 0
      if v_C = 0 then assert (signed(xsub_stats_arr(I)) > to_signed(0, c_longword_w)) report "correlation is 0 which is unexpected! at I = " & int_to_str(I) severity ERROR; end if;
    end loop;

    ----------------------------------------------------------------------------
    -- Reporting BSN monitors of the bsn_align_v2
    ----------------------------------------------------------------------------
    for RN in 0 to c_nof_rn - 1 loop
      for J in 0 to c_P_sq - 1 loop  -- bsn_monitor index
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 0, rd_data, tb_clk);  -- status bits
        report "sync_timeout = " & integer'image(TO_UINT(rd_data(2 downto 2))) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 1, rd_data, tb_clk);  -- bsn at sync
        report "bsn_at_sync = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 3, rd_data, tb_clk);  -- nof_sop
        report "nof_sop = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 4, rd_data, tb_clk);  -- nof_valid
        report "nof_valid = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 5, rd_data, tb_clk);  -- nof_err
        report "nof_err = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
        mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB", J * 8 + 6, rd_data, tb_clk);  -- latency
        report "latency = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_rx_align_xsub on RN_" & integer'image(RN) & ", CH_" & integer'image(J) & "."
          severity NOTE;
      end loop;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 0, rd_data, tb_clk);  -- status bits
      report "sync_timeout = " & integer'image(TO_UINT(rd_data(2 downto 2))) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 1, rd_data, tb_clk);  -- bsn at sync
      report "bsn_at_sync = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 3, rd_data, tb_clk);  -- nof_sop
      report "nof_sop = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 4, rd_data, tb_clk);  -- nof_valid
      report "nof_valid = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 5, rd_data, tb_clk);  -- nof_err
      report "nof_err = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
      mmf_mm_bus_rd(mmf_unb_file_prefix(c_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB", 6, rd_data, tb_clk);  -- latency
      report "latency = " & integer'image(TO_UINT(rd_data)) & " from reg_bsn_monitor_v2_aligned_xsub on RN_" & integer'image(RN) & "."
        severity NOTE;
    end loop;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
