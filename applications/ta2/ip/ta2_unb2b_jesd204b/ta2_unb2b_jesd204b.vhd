-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide 10G ethernet I/O interface (BSP) for OpenCL kernel on Arria10
-- Description:
-- . This core consists of:
--   . An Intel/Altera 10G Low Latency MAC instance
--   . SOP/EOP insertion (kernel channel only carries data and valid)
--   . Dual clock FIFO
--     . Clock domain transition between kernel_clk and clk_txmac
--     . Buffers full Ethernet packet (10G MAC requires uninterrupted packet)
--   . Clock (PLL) / reset generation
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   .
--   . The data field of the ST-avalon interface is also used to provide
--   . SOP, EOP and empty meta-data. The implementation of this is shown below.
--   +-----------+---------+--------------------------------------------------------+
--   | Bit range | Name    | Description                                            |
--   +-----------+---------+--------------------------------------------------------+
--   | [0:15]    | payload | ADC channel 0 sample                                   |
--   +-----------+---------+--------------------------------------------------------+
library IEEE, common_lib, dp_lib, tech_pll_lib, technology_lib, tech_jesd204b_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity ta2_unb2b_jesd204b is
  generic (
    g_nof_streams : natural := 12  -- can be 1-12
  );
  port (
    mm_clk       : in  std_logic;  -- 100MHz clk for reconfig block and status interface
    mm_rst       : in  std_logic;

    -- MM Control
    jesd204b_mosi : in  t_mem_mosi;
    jesd204b_miso : out t_mem_miso;

    -- JESD204B external signals
    jesd204b_refclk       : in std_logic := '0';  -- Reference clock. For AD9683 use 200MHz direct from clock reference pin
    jesd204b_sysref       : in std_logic := '0';  -- SYSREF should drive ADC and FPGA with correct phase wrt jesd204b_device_clk
    jesd204b_sync_n_arr   : out std_logic_vector(g_nof_streams - 1 downto 0);  -- output to control ADC initialization/syncronization phase

    serial_rx_arr         : in  std_logic_vector(g_nof_streams - 1 downto 0);

    kernel_clk       : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset     : in  std_logic;

    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0)

  );
end ta2_unb2b_jesd204b;

architecture str of ta2_unb2b_jesd204b is
  constant c_sim                           : boolean := false;

  constant c_nof_streams_jesd204b          : natural := 12;

  constant c_rx_fifo_size                  : natural := 32;  -- should be large enough

  signal dp_fifo_dc_rx_src_out_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_fifo_dc_rx_snk_in_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_fifo_dc_rx_src_in_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal dp_fifo_dc_rx_snk_out_arr         : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_latency_adapter_rx_src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_rx_src_in_arr  : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal jesd204b_rx_sosi_arr              : t_dp_sosi_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_rx_clk                   : std_logic;
  signal jesd204b_rx_rst                   : std_logic;

  signal i_jesd204b_sync_n_arr             : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
  signal jesd204b_serial_rx_arr            : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0) := (others => '0');
  signal jesd204b_disable_arr              : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);
begin
  jesd204b_sync_n_arr <= i_jesd204b_sync_n_arr(g_nof_streams - 1 downto 0);
  jesd204b_serial_rx_arr(g_nof_streams - 1 downto 0) <= serial_rx_arr;
  jesd204b_disable_arr <= (others => '0');

  u_jesd204b: entity tech_jesd204b_lib.tech_jesd204b
  generic map(
    g_sim                => c_sim,
    g_nof_streams        => c_nof_streams_jesd204b
  )
  port map(
    jesd204b_refclk      => jesd204b_refclk,
    jesd204b_sysref      => jesd204b_sysref,
    jesd204b_sync_n_arr  => i_jesd204b_sync_n_arr,

    jesd204b_disable_arr => jesd204b_disable_arr,

    rx_sosi_arr          => jesd204b_rx_sosi_arr,
    rx_clk               => jesd204b_rx_clk,
    rx_rst               => jesd204b_rx_rst,

    -- MM
    mm_clk               => mm_clk,
    mm_rst               => mm_rst,

    jesd204b_mosi        => jesd204b_mosi,
    jesd204b_miso        => jesd204b_miso,

     -- Serial
    serial_tx_arr        => open,
    serial_rx_arr        => jesd204b_serial_rx_arr
  );

  gen_streams: for stream in 0 to g_nof_streams - 1 generate
    ---------------------------------------------------------------------------------------
    -- RX FIFO: adc_clk -> kernel_clk
    ---------------------------------------------------------------------------------------

    dp_fifo_dc_rx_snk_in_arr(stream).data(13 downto 0) <= jesd204b_rx_sosi_arr(stream).data(15 downto 2);
    dp_fifo_dc_rx_snk_in_arr(stream).data(14) <= jesd204b_rx_sosi_arr(stream).data(15);
    dp_fifo_dc_rx_snk_in_arr(stream).data(15) <= jesd204b_rx_sosi_arr(stream).data(15);
    dp_fifo_dc_rx_snk_in_arr(stream).valid <= dp_fifo_dc_rx_snk_out_arr(stream).ready and jesd204b_rx_sosi_arr(stream).valid;

    u_dp_fifo_dc_rx : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => 16,
      g_empty_w     => 1,
      g_use_empty   => false,
      g_use_ctrl    => false,
      g_fifo_size   => c_rx_fifo_size
    )
    port map (
      wr_rst      => jesd204b_rx_rst,
      wr_clk      => jesd204b_rx_clk,
      rd_rst      => kernel_reset,
      rd_clk      => kernel_clk,

      snk_out     => dp_fifo_dc_rx_snk_out_arr(stream),
      snk_in      => dp_fifo_dc_rx_snk_in_arr(stream),

      src_in      => dp_fifo_dc_rx_src_in_arr(stream),
      src_out     => dp_fifo_dc_rx_src_out_arr(stream)
    );

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=1 (Upstream) to RL=0 (OpenCL kernel).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_rx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_fifo_dc_rx_src_out_arr(stream),
      snk_out   => dp_fifo_dc_rx_src_in_arr(stream),

      src_out   => dp_latency_adapter_rx_src_out_arr(stream),
      src_in    => dp_latency_adapter_rx_src_in_arr(stream)
    );

    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    --gen_rx_bytes: FOR I IN 0 TO c_halfword_sz-1 GENERATE
    --  kernel_src_data(c_byte_w*(c_halfword_sz-I) -1  DOWNTO c_byte_w*(c_halfword_sz-1-I)) <= dp_latency_adapter_rx_src_out.data(c_byte_w*(I+1) -1 DOWNTO c_byte_w*I);
    --END GENERATE;

    src_out_arr(stream).data(15 downto 0)  <= dp_latency_adapter_rx_src_out_arr(stream).data(15 downto 0);
    src_out_arr(stream).valid <= dp_latency_adapter_rx_src_out_arr(stream).valid;
    dp_latency_adapter_rx_src_in_arr(stream).ready <= src_in_arr(stream).ready;
    dp_latency_adapter_rx_src_in_arr(stream).xon <= '1';
  end generate;
end str;
