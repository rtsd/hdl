-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- no support for rounding in this RTL architecture
 entity  ip_arria10_mult_rtl is
  generic (
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
 end ip_arria10_mult_rtl;

architecture str of ip_arria10_mult_rtl is
  constant c_prod_w          : natural := g_in_a_w + g_in_b_w;

  -- registers
  signal reg_a         : std_logic_vector(in_a'range);
  signal reg_b         : std_logic_vector(in_b'range);
  signal reg_prod      : std_logic_vector(g_nof_mult * c_prod_w - 1 downto 0);
  signal reg_result    : std_logic_vector(out_p'range);

  -- combinatorial
  signal nxt_a         : std_logic_vector(in_a'range);
  signal nxt_b         : std_logic_vector(in_b'range);
  signal nxt_prod      : std_logic_vector(g_nof_mult * c_prod_w - 1 downto 0);
  signal nxt_result    : std_logic_vector(out_p'range);

  -- the active signals
  signal inp_a         : std_logic_vector(in_a'range);
  signal inp_b         : std_logic_vector(in_b'range);
  signal prod          : std_logic_vector(g_nof_mult * c_prod_w - 1 downto 0);  -- stage dependent on g_pipeline_product being 0 or 1
  signal result        : std_logic_vector(out_p'range);  -- stage dependent on g_pipeline_output  being 0 or 1
begin
  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------

  -- Put all potential registers in a single process for optimal DSP inferrence
  -- Use rst only if it is supported by the DSP primitive, else leave it at '0'
  p_reg : process (rst, clk)
  begin
    if rst = '1' then
      reg_a      <= (others => '0');
      reg_b      <= (others => '0');
      reg_prod   <= (others => '0');
      reg_result <= (others => '0');
    elsif rising_edge(clk) then
      if clken = '1' then
        reg_a      <= nxt_a;
        reg_b      <= nxt_b;
        reg_prod   <= nxt_prod;
        reg_result <= nxt_result;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Inputs
  ------------------------------------------------------------------------------

  nxt_a <= in_a;
  nxt_b <= in_b;

  no_input_reg : if g_pipeline_input = 0 generate  -- wired
    inp_a <= nxt_a;
    inp_b <= nxt_b;
  end generate;

  gen_input_reg : if g_pipeline_input > 0 generate  -- register input
    inp_a <= reg_a;
    inp_b <= reg_b;
  end generate;

  ------------------------------------------------------------------------------
  -- Products
  ------------------------------------------------------------------------------

  gen_mult : for I in 0 to g_nof_mult - 1 generate
    nxt_prod((I + 1) * c_prod_w - 1 downto I * c_prod_w) <=
      std_logic_vector(  signed(inp_a((I + 1) * g_in_a_w - 1 downto I * g_in_a_w)) *   signed(inp_b((I + 1) * g_in_b_w - 1 downto I * g_in_b_w))) when g_representation = "SIGNED" else
      std_logic_vector(unsigned(inp_a((I + 1) * g_in_a_w - 1 downto I * g_in_a_w)) * unsigned(inp_b((I + 1) * g_in_b_w - 1 downto I * g_in_b_w)));
  end generate;

  no_product_reg : if g_pipeline_product = 0 generate  -- wired
    prod <= nxt_prod;
  end generate;
  gen_product_reg : if g_pipeline_product > 0 generate  -- register
    prod <= reg_prod;
  end generate;

  ------------------------------------------------------------------------------
  -- Results
  ------------------------------------------------------------------------------
  nxt_result <= prod;

  no_result_reg : if g_pipeline_output = 0 generate  -- wired
    result <= nxt_result;
  end generate;
  gen_result_reg : if g_pipeline_output > 0 generate  -- register
    result <= reg_result;
  end generate;

out_p <= result;
end str;
