--------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Eric Kooistra, 14 Feb 2023
-- Purpose:
-- . Reverse the order of multiplexed data per every g_reverse_len values in
--   time.
-- Description:
-- . The snk_in blocks size must be an integer multiple of g_reverse_len.
-- . For example with g_reverse_len = 3, then snk_in input data 012_345_678
--   becomes 210_543_876 at src_out output.
-- . See common_reverse_n_data.vhd for more detailed description.
-- Remark:
-- . The dp_reverse_n_data.vhd and dp_reverse_n_data_fc.vhd are functionally
--   equivalent. Use dp_reverse_n_data_fc.vhd flow control (fc) is needed.
--   A difference is that dp_reverse_n_data_fc causes extra gaps in the data
--   valid after each g_reverse_len. Therefore use dp_reverse_n_data when no
--   flow control is needed.
-- . Typically g_reverse_len should not be too large (~< 4), because then the
--   implementation takes relatively too much logic.

entity dp_reverse_n_data is
  generic (
    -- Pipeline: 0 for combinatorial, > 0 for registers
    g_pipeline_demux_in  : natural := 1;  -- serial to parallel section
    g_pipeline_demux_out : natural := 0;
    g_pipeline_mux_in    : natural := 0;  -- parallel to serial section
    g_pipeline_mux_out   : natural := 1;
    g_reverse_len        : natural := 2;
    g_data_w             : natural := 16;  -- should be 2 times the c_complex_w if g_use_complex = TRUE
    g_use_complex        : boolean := false;
    g_signed             : boolean := true
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_in      : in  t_dp_sosi;
    src_out     : out t_dp_sosi
  );
end dp_reverse_n_data;

architecture str of dp_reverse_n_data is
  constant c_pipeline_total : natural := g_pipeline_demux_in + g_pipeline_demux_out +
                                         g_reverse_len - 1 +
                                         g_pipeline_mux_in + g_pipeline_mux_out;
  constant c_complex_w      : natural := g_data_w / 2;

  signal in_data            : std_logic_vector(g_data_w - 1 downto 0);

  signal reversed_data      : std_logic_vector(g_data_w - 1 downto 0);
  signal reversed_val       : std_logic;

  signal snk_in_delayed     : t_dp_sosi;
begin
  p_in_data : process(snk_in)
  begin
    if g_use_complex = false then
      in_data <= snk_in.data(g_data_w - 1 downto 0);
    else
      in_data <= snk_in.im(c_complex_w - 1 downto 0) & snk_in.re(c_complex_w - 1 downto 0);
    end if;
  end process;

  u_common_reverse_n : entity common_lib.common_reverse_n_data
  generic map (
    -- Pipeline: 0 for combinatorial, > 0 for registers
    g_pipeline_demux_in  => g_pipeline_demux_in,  -- serial to parallel demux
    g_pipeline_demux_out => g_pipeline_demux_out,
    g_pipeline_mux_in    => g_pipeline_mux_in,  -- parallel to serial mux
    g_pipeline_mux_out   => g_pipeline_mux_out,
    g_reverse_len        => g_reverse_len,
    g_data_w             => g_data_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_data     => in_data,
    in_val      => snk_in.valid,
    in_eop      => snk_in.eop,
    out_data    => reversed_data,
    out_val     => reversed_val  -- = snk_in_delayed.valid
  );

  -- Pipeline other sosi fields
  u_pipe_input : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline_total
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_in       => snk_in,
    -- ST source
    src_out      => snk_in_delayed
  );

  p_src_out : process(snk_in_delayed, reversed_data)
  begin
    src_out <= snk_in_delayed;
    if g_use_complex = false then
      if g_signed = true then
        src_out.data <= RESIZE_DP_SDATA(reversed_data);
      else
        src_out.data <= RESIZE_DP_DATA(reversed_data);
      end if;
    else
      src_out.im <= RESIZE_DP_DSP_DATA(reversed_data(g_data_w - 1 downto c_complex_w));
      src_out.re <= RESIZE_DP_DSP_DATA(reversed_data(c_complex_w - 1 downto 0));
    end if;
  end process;
end str;
