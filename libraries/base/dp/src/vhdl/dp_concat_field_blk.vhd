-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Concatenate a user-defined header to a DP frame e.g. to create an Ethernet frame
-- Description:
-- . The header contents can be controlled dynamically by data path or MM control (selected by g_hdr_field_sel)
-- . The header and data can be concatened at symbol level. The g_symbol_w defines the
--   resolution of the empty field. The g_data_w must be an integer multiple of the
--   g_symbol_w. If the empty field is not used or if the empty field is always 0 then
--   set g_symbol_w = g_data_w.
-- . For example to concat header and data for an Ethernet frame use:
--   - g_data_w = 32 (1GbE) or 64 (10GbE)
--   - g_symbol_w = c_byte_w = 8 if either the header or the data can have an
--                  non-zero empty field, so when they are not a multiple of 4 bytes
--                  (= 32b) or 8 bytes (= 64b).
--     g_symbol_w = g_data_w if the empty field is always 0, so the number of bits in
--                  the header and data are an integer number of g_data_w.

library IEEE, common_lib, technology_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_concat_field_blk is
  generic (
    g_nof_streams    : natural;
    g_data_w         : natural;
    g_symbol_w       : natural;
    g_hdr_field_arr  : t_common_field_arr;  -- User defined header fields
    g_hdr_field_sel  : std_logic_vector;  -- For each header field, select the source: 0=data path, 1=MM controlled
    g_pipeline_ready : boolean := false
  );
  port (
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    dp_rst               : in  std_logic;
    dp_clk               : in  std_logic;

    reg_hdr_dat_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso     : out t_mem_miso;

    snk_in_arr           : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr          : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr          : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr           : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    hdr_fields_in_arr    : in  t_slv_1024_arr(g_nof_streams - 1 downto 0)  -- hdr_fields_in_arr(i) is considered valid @ snk_in_arr(i).sop
  );
end dp_concat_field_blk;

architecture str of dp_concat_field_blk is
  constant c_dp_field_blk_snk_data_w : natural := field_slv_out_len(field_arr_set_mode(g_hdr_field_arr,  "RW"));
  constant c_dp_field_blk_src_data_w : natural := g_data_w;

  signal dbg_c_dp_field_blk_snk_data_w : natural := c_dp_field_blk_snk_data_w;
  signal dbg_c_dp_field_blk_src_data_w : natural := c_dp_field_blk_src_data_w;

  signal dp_field_blk_snk_in_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_field_blk_snk_out_arr    : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_field_blk_src_out_arr    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_field_blk_src_in_arr     : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_concat_snk_in_2arr       : t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_concat_snk_out_2arr      : t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);

  signal reg_hdr_dat_mosi_arr        : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_hdr_dat_miso_arr        : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  assert c_dp_field_blk_snk_data_w <= c_dp_stream_data_w
    report "Number of header bits must fit in t_dp_sosi data field."
    severity FAILURE;

  ---------------------------------------------------------------------------------------
  -- Create header block & concatenate header to offload stream.
  ---------------------------------------------------------------------------------------
  gen_dp_field_blk : for i in 0 to g_nof_streams - 1 generate
    p_wire_valid : process(snk_in_arr, hdr_fields_in_arr)
    begin
      for i in 0 to g_nof_streams - 1 loop
        -- default pass on the other snk_in_arr fields as well, especially the sync, bsn and channel can
        -- be useful to preserve for the packetized output, even though only the sosi.data of the
        -- packetized output will get transmitted.
        dp_field_blk_snk_in_arr(i)       <= snk_in_arr(i);
        -- Prepare packet header as a data block with only one data word, so valid = sop = eop. If
        -- c_dp_field_blk_snk_data_w > c_dp_field_blk_src_data_w then dp_repack_data in dp_field_blk will
        -- repack this data word into a multi word header block, else dp_field_blk will pass on the
        -- dp_field_blk_snk_in_arr as a single word header block.
        dp_field_blk_snk_in_arr(i).data  <= RESIZE_DP_DATA(hdr_fields_in_arr(i)(field_slv_len(g_hdr_field_arr) - 1 downto 0));
        dp_field_blk_snk_in_arr(i).valid <= snk_in_arr(i).sop;
        dp_field_blk_snk_in_arr(i).sop   <= snk_in_arr(i).sop;  -- necessary for single word header block
        dp_field_blk_snk_in_arr(i).eop   <= snk_in_arr(i).sop;  -- necessary for single word header block
      end loop;
    end process;

    -- Both dp_concat inputs must be ready. One of the inputs toggles ready via dp_field_blk.
    snk_out_arr(i).ready <= dp_field_blk_snk_out_arr(i).ready and dp_concat_snk_out_2arr(i)(0).ready;
    snk_out_arr(i).xon <= src_in_arr(i).xon;

    -- Create multi-cycle header block from single-cycle wide header SLV
    u_dp_field_blk : entity work.dp_field_blk
    generic map (
      g_field_arr      => field_arr_set_mode(g_hdr_field_arr,  "RW"),
      g_field_sel      => g_hdr_field_sel,
      g_snk_data_w     => c_dp_field_blk_snk_data_w,
      g_src_data_w     => c_dp_field_blk_src_data_w,
      g_in_symbol_w    => g_symbol_w,
      g_out_symbol_w   => g_symbol_w,
      g_pipeline_ready => g_pipeline_ready
    )
    port map (
      dp_clk       => dp_clk,
      dp_rst       => dp_rst,

      mm_clk       => mm_clk,
      mm_rst       => mm_rst,

      snk_in       => dp_field_blk_snk_in_arr(i),
      snk_out      => dp_field_blk_snk_out_arr(i),

      src_out      => dp_field_blk_src_out_arr(i),
      src_in       => dp_field_blk_src_in_arr(i),

      reg_slv_mosi => reg_hdr_dat_mosi_arr(i),
      reg_slv_miso => reg_hdr_dat_miso_arr(i)
    );

    dp_field_blk_src_in_arr(i) <= dp_concat_snk_out_2arr(i)(1);
  end generate;

  -- Prepend the header block to the input block
  gen_dp_concat : for i in 0 to g_nof_streams - 1 generate
    dp_concat_snk_in_2arr(i)(0) <= snk_in_arr(i);
    dp_concat_snk_in_2arr(i)(1) <= dp_field_blk_src_out_arr(i);

    u_dp_concat : entity work.dp_concat
    generic map (
      g_data_w    => g_data_w,
      g_symbol_w  => g_symbol_w
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out_arr => dp_concat_snk_out_2arr(i),
      snk_in_arr  => dp_concat_snk_in_2arr(i),

      src_in      => src_in_arr(i),
      src_out     => src_out_arr(i)
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- MM control & monitoring
  ---------------------------------------------------------------------------------------
  u_common_mem_mux_hdr_dat : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(field_nof_words(g_hdr_field_arr, c_word_w))
  )
  port map (
    mosi     => reg_hdr_dat_mosi,
    miso     => reg_hdr_dat_miso,
    mosi_arr => reg_hdr_dat_mosi_arr,
    miso_arr => reg_hdr_dat_miso_arr
  );
end str;
