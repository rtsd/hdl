-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 21 Dec 2018
-- Purpose: Verify dp_dp_concat_field_blk
-- Usage:
-- > as 5
-- > run -all                 --> OK

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_concat_field_blk is
end tb_tb_dp_concat_field_blk;

architecture tb of tb_tb_dp_concat_field_blk is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- -- general
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
-- -- specific
-- g_data_w                 : NATURAL := 64;
-- g_nof_repeat             : NATURAL := 13;
-- g_pkt_len                : NATURAL := 100;
-- g_pkt_gap                : NATURAL := 0

  u_large_gap       : entity work.tb_dp_concat_field_blk generic map (e_active, e_active, 64, 13, 100, 10);  -- g_pkt_gap > header length
  u_zero_gap        : entity work.tb_dp_concat_field_blk generic map (e_active, e_active, 64, 13, 100,  0);
end tb;
