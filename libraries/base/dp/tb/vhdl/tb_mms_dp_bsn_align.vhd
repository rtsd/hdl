-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_bsn_align
-- Description:
-- Usage:
-- > as 10
-- > run -all
-- . Observe tb_state, verify*, in_* (e.g. in_sop) and out_* signals
--   (e.g. out_bsn) and DUT register in record /r in Wave Window.
-- . The verify procedures automatically check the correct output during the
--   first g_nof_repeat blocks in gen_input/p_stimuli.
-- . After that the p_special_stimuli trigger some special events that require
--   BSN alignment thanks to verify_dis_arr these cases are also verified
--   automatically.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_mms_dp_bsn_align is
  generic (
    g_block_size           : natural := 11;
    g_diff_delay           : natural := 20;
    g_diff_bsn             : natural := 3;  -- g_diff_bsn = g_bsn_latency can just be aligned
    g_nof_input            : natural := 16;  -- >= 1
    g_bsn_latency          : natural := 3;
    g_bsn_request_pipeline : natural := 2;  -- = 1, 2, ..., c_bsn_nof_stages, requested total pipelining of the BSN max and BSN min operation
    g_out_ready            : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_nof_repeat           : natural := 100  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli
  );
end tb_mms_dp_bsn_align;

architecture tb of tb_mms_dp_bsn_align is
  constant c_mm_clk_period            : time    := 20 ns;  -- 40 MHz
  constant c_rl                       : natural := 1;
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_data_w                   : natural := 16;
  constant c_data_init                : integer := 0;
  constant c_bsn_w                    : natural := 16;
  constant c_bsn_init                 : std_logic_vector(c_bsn_w - 1 downto 0) := TO_UVEC(3, c_bsn_w);
  constant c_channel_init             : integer := 0;
  constant c_err_init                 : natural := 247;
  constant c_sync_period              : natural := 7;
  constant c_sync_offset              : natural := 2;

  constant c_gap_size                 : natural := 10;
  constant c_block_period             : natural := g_block_size + c_gap_size;
  constant c_xoff_timeout             : natural := c_block_period * g_bsn_latency * 2;  -- xoff timeout to recover for next alignment attempt
  constant c_sop_timeout              : natural := c_block_period * g_bsn_latency;  -- sop timeout to end current aligment attempt

  constant c_event_input              : natural := smallest(1, g_nof_input - 1);  -- select special event input at which the event will apply, use >= g_nof_input to disable the special events

  type t_data_arr    is array (g_nof_input - 1 downto 0) of std_logic_vector(c_data_w - 1 downto 0);
  type t_bsn_arr     is array (g_nof_input - 1 downto 0) of std_logic_vector(c_bsn_w - 1 downto 0);
  type t_err_arr     is array (g_nof_input - 1 downto 0) of std_logic_vector(c_dp_stream_error_w - 1 downto 0);
  type t_channel_arr is array (g_nof_input - 1 downto 0) of std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  type t_rl_vec_arr  is array (g_nof_input - 1 downto 0) of std_logic_vector(0 to c_rl);

  type t_tb_state is (s_idle, s_bsn_mis_aligned, s_bsn_aligned, s_small_bsn_diff, s_large_bsn_diff, s_restore_bsn, s_disable_one_input, s_enable_inputs);

  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';
  signal sl1               : std_logic := '1';
  signal mm_clk            : std_logic := '1';
  signal mm_rst            : std_logic := '1';

  signal random            : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse             : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_siso_arr       : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal in_sosi_arr       : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal in_ready          : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_xon            : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_bsn            : t_bsn_arr;
  signal in_data           : t_data_arr;
  signal in_val            : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_sync           : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_sop            : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_eop            : std_logic_vector(g_nof_input - 1 downto 0);
  signal in_err            : t_err_arr;
  signal in_channel        : t_channel_arr;

  signal out_siso          : t_dp_siso;
  signal out_siso_arr      : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal out_sosi_arr      : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal out_bsn           : t_bsn_arr;
  signal out_data          : t_data_arr;
  signal out_val           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_sync          : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_sop           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_eop           : std_logic_vector(g_nof_input - 1 downto 0);
  signal out_gap           : std_logic_vector(g_nof_input - 1 downto 0) := (others => '1');
  signal out_err           : t_err_arr;
  signal out_channel       : t_channel_arr;

  signal tb_state          : t_tb_state;

  signal verify_done_arr   : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal default_end_arr   : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal default_end       : std_logic;
  signal verify_dis_arr    : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal verify_en_arr     : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');

  signal hold_out_sop      : std_logic_vector(g_nof_input - 1 downto 0);
  signal prev_out_ready    : t_rl_vec_arr;
  signal prev_out_bsn      : t_bsn_arr;
  signal expected_out_bsn  : t_bsn_arr;
  signal prev_out_data     : t_data_arr;
  signal expected_out_data : t_data_arr;

  signal verify_extra_end  : std_logic := '0';
  signal bsn_diff          : integer;
  signal bsn_offset        : integer;
  signal bsn_event         : std_logic := '0';  -- pulse '1' triggers a BSN offset for an input
  signal bsn_event_ack_arr : std_logic_vector(g_nof_input - 1 downto 0) := (others => '0');
  signal bsn_event_ack     : std_logic;
  signal in_en_event       : std_logic := '0';  -- pulse '1' indicates that the input enables in in_en_arr have been updated
  signal in_en_arr         : std_logic_vector(g_nof_input - 1 downto 0) := (others => '1');  -- default all inputs are enabled

  signal mm_mosi           : t_mem_mosi;
  signal mm_miso           : t_mem_miso;
begin
  clk    <= (not clk)    or tb_end after clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst    <= '1', '0' after clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  random <= func_common_random(random) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  out_siso.xon   <= '1';
  out_siso.ready <= '1'                 when g_out_ready = e_active else
                    random(random'high) when g_out_ready = e_random else
                    pulse               when g_out_ready = e_pulse;

  out_siso_arr <= (others => out_siso);

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_input : for I in g_nof_input - 1 downto 0 generate
    p_stimuli : process
      variable v_sync      : std_logic := '0';
      variable v_bsn       : std_logic_vector(c_bsn_w - 1 downto 0) := c_bsn_init;
      variable v_data      : natural := c_data_init;
      variable v_channel   : natural := c_channel_init;
      variable v_err       : natural := c_err_init;
      variable v_diff_bsn  : natural := 0;
    begin
      -- Create BSN misalignment between the input streams
      v_diff_bsn := I mod (g_diff_bsn + 1);

      v_data := v_data + I;
      v_bsn  := INCR_UVEC(v_bsn, v_diff_bsn);
      in_sosi_arr(I) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);
      proc_common_wait_some_cycles(clk, c_xoff_timeout * 2);

      -- Create latency misalignment between the input streams
      proc_common_wait_some_cycles(clk, I * g_diff_delay / g_nof_input);

      -- Begin of stimuli
      for R in 0 to g_nof_repeat - v_diff_bsn - 1 loop
        v_sync := sel_a_b(TO_UINT(v_bsn) mod c_sync_period = c_sync_offset, '1', '0');
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data, 0, 0, g_block_size, v_channel, v_err, v_sync, v_bsn, clk, in_en_arr(I), in_siso_arr(I), in_sosi_arr(I));
        v_bsn  := INCR_UVEC(v_bsn, 1);
        v_data := v_data + g_block_size;
        proc_common_wait_some_cycles(clk, c_gap_size);  -- create gap between frames
      end loop;

      -- End of default stimuli
      expected_out_bsn(I)  <= INCR_UVEC(v_bsn, -1);
      expected_out_data(I) <= TO_UVEC(v_data - 1, c_data_w);

      proc_common_wait_some_cycles(clk, 100);  -- depends on stream control
      default_end_arr(I) <= '1';
      verify_done_arr(I) <= '1';
      proc_common_wait_some_cycles(clk, 1);
      verify_done_arr(I) <= '0';

      --------------------------------------------------------------------------
      -- Extra
      --------------------------------------------------------------------------

      proc_common_wait_some_cycles(clk, 500);
      while verify_extra_end /= '1' loop
        v_sync := sel_a_b(TO_UINT(v_bsn) mod c_sync_period = c_sync_offset, '1', '0');
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data, 0, 0, g_block_size, v_channel, v_err, v_sync, v_bsn, clk, in_en_arr(I), in_siso_arr(I), in_sosi_arr(I));
        v_bsn := INCR_UVEC(v_bsn, 1);
        bsn_event_ack_arr(I) <= '0';
        if I = c_event_input and bsn_event = '1' then
          v_bsn := INCR_UVEC(v_bsn, bsn_offset);
          bsn_event_ack_arr(I) <= '1';
        end if;
        v_data := v_data + g_block_size;
        proc_common_wait_some_cycles(clk, c_gap_size);  -- create gap between frames
      end loop;

      -- End of extra stimuli
      expected_out_bsn(I)  <= INCR_UVEC(v_bsn, -1);
      expected_out_data(I) <= TO_UVEC(v_data - 1, c_data_w);

      verify_done_arr(I) <= '1';
      proc_common_wait_some_cycles(clk, 1);
      verify_done_arr(I) <= '0';
      wait;
    end process;
  end generate;

  default_end   <= vector_and(default_end_arr);
  bsn_event_ack <= vector_or(bsn_event_ack_arr);

  p_special_stimuli : process
  begin
    verify_dis_arr <= (others => '0');

    tb_state <= s_bsn_mis_aligned;
    in_en_event    <= '0';
    in_en_arr      <= (others => '1');

    ----------------------------------------------------------------------------
    -- Wait until default verify test is done
    ----------------------------------------------------------------------------

    proc_common_wait_until_high(clk, default_end);

    verify_dis_arr <= (others => '1');
    proc_common_wait_some_cycles(clk, 100);
    verify_dis_arr <= (others => '0');

    tb_state <= s_bsn_aligned;
    proc_common_wait_some_cycles(clk, 1000);

    ----------------------------------------------------------------------------
    -- Verify change in input BSN offset
    ----------------------------------------------------------------------------

    -- . enforce small BSN misalignment
    tb_state <= s_small_bsn_diff;
    verify_dis_arr <= (others => '1');
    bsn_offset <= -1;
    bsn_event <= '1';
    proc_common_wait_until_high(clk, bsn_event_ack);
    bsn_event <= '0';
    proc_common_wait_some_cycles(clk, 100);

    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 1000);
    verify_dis_arr <= (others => '1');

    -- . restore original BSN sequence
    tb_state <= s_restore_bsn;
    bsn_offset <= +1;
    bsn_event <= '1';
    proc_common_wait_until_high(clk, bsn_event_ack);
    bsn_event <= '0';
    proc_common_wait_some_cycles(clk, 100);

    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 1000);
--     verify_dis_arr <= (OTHERS=>'1');

    -- . enforce large BSN misalignment
    tb_state <= s_large_bsn_diff;
    bsn_offset <= -g_bsn_latency - 1;
    bsn_event <= '1';
    proc_common_wait_until_high(clk, bsn_event_ack);
    bsn_event <= '0';
    -- expect no output, because difference remains too large, so do not restart verify_en here and leave it commented:
--     proc_common_wait_some_cycles(clk, 100);
--     verify_dis_arr <= (OTHERS=>'0');
    proc_common_wait_some_cycles(clk, 1000);
    verify_dis_arr <= (others => '1');

    -- . restore original BSN sequence
    tb_state <= s_restore_bsn;
    bsn_offset <= g_bsn_latency + 1;
    bsn_event <= '1';
    proc_common_wait_until_high(clk, bsn_event_ack);
    bsn_event <= '0';
    proc_common_wait_some_cycles(clk, 100);
    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 1000);

    ----------------------------------------------------------------------------
    -- Verify change in input enables
    ----------------------------------------------------------------------------

    tb_state <= s_disable_one_input;
    verify_dis_arr <= (others => '1');

--    in_en_event <= '1';
    in_en_arr(c_event_input) <= '0';  -- switch an input off
--    proc_common_wait_some_cycles(clk, 1);
--    in_en_event <= '0';
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_mem_mm_bus_wr(c_event_input, x"0", mm_clk, mm_mosi);

    proc_common_wait_some_cycles(clk, 100);
    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 2000);  -- keep this input off for a while

    tb_state <= s_enable_inputs;
    verify_dis_arr <= (others => '1');

--    in_en_event <= '1';
    in_en_arr(c_event_input) <= '1';  -- switch this input on
--    proc_common_wait_some_cycles(clk, 1);
--    in_en_event <= '0';
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_mem_mm_bus_wr(c_event_input, x"1", mm_clk, mm_mosi);

    proc_common_wait_some_cycles(clk, 100);
    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 500);

    tb_state <= s_restore_bsn;
    verify_dis_arr <= (others => '1');
    bsn_offset <= bsn_diff;  -- use input 0 to restore original BSN sequence for input c_event_input, that got lost due to input disable
    bsn_event <= '1';
    proc_common_wait_until_high(clk, bsn_event_ack);
    bsn_event <= '0';
    proc_common_wait_some_cycles(clk, 100);
    verify_dis_arr <= (others => '0');
    proc_common_wait_some_cycles(clk, 2000);

    tb_state <= s_idle;
    verify_extra_end <= '1';
    proc_common_wait_some_cycles(clk, 500);
    tb_end <= '1';
    wait;
  end process;

  bsn_diff <= TO_UINT(in_sosi_arr(0).bsn) - TO_UINT(in_sosi_arr(c_event_input).bsn) when rising_edge(clk) and in_sosi_arr(0).sop = '1';

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  gen_verify : for I in g_nof_input - 1 downto 0 generate
    -- Verification logistics
    verify_en_arr(I) <= '1' when rising_edge(clk) and verify_dis_arr(I) = '0' and in_en_arr(I) = '1' and out_sosi_arr(I).sop = '1' else
                        '0' when rising_edge(clk) and verify_dis_arr(I) = '1';  -- verify enable after first output sop

    -- Ease in_siso_arr monitoring
    in_ready(I)   <= in_siso_arr(I).ready;
    in_xon(I)     <= in_siso_arr(I).xon;

    -- Ease in_sosi_arr monitoring
    in_data(I)    <= in_sosi_arr(I).data(c_data_w - 1 downto 0);
    in_val(I)     <= in_sosi_arr(I).valid;
    in_sop(I)     <= in_sosi_arr(I).sop;
    in_eop(I)     <= in_sosi_arr(I).eop;
    in_err(I)     <= in_sosi_arr(I).err;
    in_channel(I) <= in_sosi_arr(I).channel;
    in_sync(I)    <= in_sosi_arr(I).sync;
    in_bsn(I)     <= in_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);

    -- Ease out_sosi_arr monitoring and verification
    out_data(I)    <= out_sosi_arr(I).data(c_data_w - 1 downto 0);
    out_val(I)     <= out_sosi_arr(I).valid;
    out_sop(I)     <= out_sosi_arr(I).sop;
    out_eop(I)     <= out_sosi_arr(I).eop;
    out_err(I)     <= out_sosi_arr(I).err;
    out_channel(I) <= out_sosi_arr(I).channel;
    out_sync(I)    <= out_sosi_arr(I).sync;
    out_bsn(I)     <= out_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);

    -- Actual verification of the output streams
    -- . Verify that the output valid fits with the output ready latency
    proc_dp_verify_valid(c_rl, clk, verify_en_arr(I), out_siso_arr(I).ready, prev_out_ready(I), out_val(I));
    -- . Verify that sop and eop come in pairs
    proc_dp_verify_sop_and_eop(clk, out_val(I), out_sop(I), out_eop(I), hold_out_sop(I));

    -- . Verify that the output is incrementing, like the input stimuli
    proc_dp_verify_data("out_sosi.data", c_rl, clk, verify_en_arr(I), out_siso_arr(I).ready, out_val(I), out_data(I), prev_out_data(I));
    proc_dp_verify_data("out_sosi.bsn", c_rl, clk, verify_en_arr(I), out_siso_arr(I).ready, out_sop(I), out_bsn(I), prev_out_bsn(I));

    -- . Verify that the stimuli have been applied at all
    proc_dp_verify_value(e_equal, clk, verify_done_arr(I), expected_out_data(I), prev_out_data(I));
    proc_dp_verify_value(e_equal, clk, verify_done_arr(I), expected_out_bsn(I),  prev_out_bsn(I));
  end generate;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_mms_bsn_align : entity work.mms_dp_bsn_align
  generic map (
    g_block_size           => g_block_size,
    g_nof_input            => g_nof_input,
    g_xoff_timeout         => c_xoff_timeout,
    g_sop_timeout          => c_sop_timeout,
    g_bsn_latency          => g_bsn_latency,
    g_bsn_request_pipeline => g_bsn_request_pipeline,
    g_cross_clock_domain   => true
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => mm_mosi,
    reg_miso    => mm_miso,
    -- Streaming clock domain
    dp_rst      => rst,
    dp_clk      => clk,
    -- ST sinks
    snk_out_arr => in_siso_arr,
    snk_in_arr  => in_sosi_arr,
    -- ST source
    src_in_arr  => out_siso_arr,
    src_out_arr => out_sosi_arr
  );
end tb;
