-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Test bench for dp_block_validate_channel.
-- Description:
-- Verifies the output sosi of the DUT with the expected sosi.
-- expected values.
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_validate_channel is
  generic (
    g_nof_blocks_per_sync : natural := 8;
    g_nof_data_per_blk    : natural := 8;
    g_gap_size            : natural := 5;
    g_remove_channel      : natural := 0;
    g_mode                : string  := "="
  );
end tb_dp_block_validate_channel;

architecture tb of tb_dp_block_validate_channel is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_dp_clk_period  : time := 5 ns;
  constant c_mm_clk_period  : time := 10 ns;

  constant c_dut_pipeline   : natural := 1;
  constant c_nof_sync       : natural := 5;
  constant c_nof_blk        : natural := g_nof_blocks_per_sync * c_nof_sync;

  signal dp_clk             : std_logic := '1';
  signal rst                : std_logic := '1';
  signal tb_end             : std_logic := '0';

  signal stimuli_end        : std_logic;
  signal stimuli_sosi       : t_dp_sosi;
  signal stimuli_siso       : t_dp_siso := c_dp_siso_rdy;
  signal keep_sosi          : t_dp_sosi;
  signal remove_sosi        : t_dp_sosi;
  signal reference_sosi     : t_dp_sosi;
  signal reference_siso     : t_dp_siso := c_dp_siso_rdy;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => g_nof_blocks_per_sync * c_nof_sync,
    g_pkt_len     => g_nof_data_per_blk,
    g_pkt_gap     => g_gap_size
  )
  port map (
    rst               => rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut : entity work.dp_block_validate_channel
  generic map (
    g_mode          => g_mode
  )
  port map (
    dp_rst          => rst,
    dp_clk          => dp_clk,

    -- ST sink
    in_sosi         => stimuli_sosi,
    -- ST source
    out_keep_sosi   => keep_sosi,
    out_remove_sosi => remove_sosi,

    remove_channel  => TO_UVEC(g_remove_channel, 32)
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dut_pipeline
  )
  port map (
    rst         => rst,
    clk         => dp_clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => reference_siso,
    src_out     => reference_sosi
  );

  p_verify : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if reference_sosi.valid = '1' then
        if TO_UINT(reference_sosi.channel) = g_remove_channel and g_mode = "=" then
          assert remove_sosi = reference_sosi
            report "remove_sosi does not contain removed block!"
            severity ERROR;
          assert keep_sosi.valid = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.eop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sync = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
        elsif g_mode = "=" then
          assert keep_sosi = reference_sosi
            report "No block in keep_sosi"
            severity ERROR;
          assert remove_sosi.valid = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.eop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sync = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
        elsif TO_UINT(reference_sosi.channel) < g_remove_channel and g_mode = "<" then
          assert remove_sosi = reference_sosi
            report "remove_sosi does not contain removed block!"
            severity ERROR;
          assert keep_sosi.valid = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.eop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sync = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
        elsif g_mode = "<" then
          assert keep_sosi = reference_sosi
            report "No block in keep_sosi"
            severity ERROR;
          assert remove_sosi.valid = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.eop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sync = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
        elsif TO_UINT(reference_sosi.channel) > g_remove_channel and g_mode = ">" then
          assert remove_sosi = reference_sosi
            report "remove_sosi does not contain removed block!"
            severity ERROR;
          assert keep_sosi.valid = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.eop = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
          assert keep_sosi.sync = '0'
            report "Wrong, removed block occurs in keep_sosi!"
            severity ERROR;
        elsif g_mode = ">" then
          assert keep_sosi = reference_sosi
            report "No block in keep_sosi"
            severity ERROR;
          assert remove_sosi.valid = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.eop = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
          assert remove_sosi.sync = '0'
            report "Wrong, block occurs in remove_sosi which is unexpected!"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  tb_end <= '0', stimuli_end after (1 + 10 * c_dut_pipeline) * c_dp_clk_period;
end tb;
