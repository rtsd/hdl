--------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Linear Feedback Shift Register based pseudo random sequence generation.
-- Interface:
-- . [n/a]
-- Description:
-- . Based on Xilinx application note xapp052.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

package common_lfsr_sequences_pkg is
  constant c_common_lfsr_max_nof_feedbacks : natural := 6;
  constant c_common_lfsr_first             : natural := 1;  -- also support n = 1 and 2 in addition to n >= 3

  type t_FEEDBACKS is array (c_common_lfsr_max_nof_feedbacks - 1 downto 0) of natural;
  type t_SEQUENCES is array (natural range <>) of t_FEEDBACKS;

  -- XNOR feedbacks for n = 1:
  --   (0,0,0,0,0, 0) yields repeat <1>
  --   (0,0,0,0,0, 1) yields repeat <0, 1>

  -- XNOR feedbacks for n = 2:
  --   (0,0,0,0, 0, 1) yields repeat <1, 2>
  --   (0,0,0,0, 0, 2) yields repeat <0, 1, 3, 2>
  --   (0,0,0,0, 2, 1) yields repeat <0, 1, 2>

  -- XNOR feedbacks from outputs for n = 3 .. 72 from Xilinx xapp052.pdf (that lists feedbacks for in total 168 sequences)
  constant c_common_lfsr_sequences : t_SEQUENCES := ((0, 0, 0, 0, 0,               1),  -- 1 : <0, 1>
                                                     (0, 0, 0, 0,              0, 2),  -- 2 : <0, 1, 3, 2>
                                                     (0, 0, 0, 0,              3, 2),  -- 3
                                                     (0, 0, 0, 0,              4, 3),  -- 4
                                                     (0, 0, 0, 0,              5, 3),  -- 5
                                                     (0, 0, 0, 0,              6, 5),  -- 6
                                                     (0, 0, 0, 0,              7, 6),  -- 7
                                                     (0, 0,            8, 6, 5, 4),  -- 8
                                                     (0, 0, 0, 0,              9, 5),  -- 9
                                                     (0, 0, 0, 0,             10, 7),  -- 10
                                                     (0, 0, 0, 0,             11, 9),  -- 11
                                                     (0, 0,           12, 6, 4, 1),  -- 12
                                                     (0, 0,           13, 4, 3, 1),  -- 13
                                                     (0, 0,           14, 5, 3, 1),  -- 14
                                                     (0, 0, 0, 0,       15, 14      ),  -- 15
                                                     (0, 0,           16, 15, 13, 4),  -- 16
                                                     (0, 0, 0, 0,       17, 14      ),  -- 17
                                                     (0, 0, 0, 0,       18, 11      ),  -- 18
                                                     (0, 0,           19, 6, 2, 1),  -- 19
                                                     (0, 0, 0, 0,       20, 17      ),  -- 20
                                                     (0, 0, 0, 0,       21, 19      ),  -- 21
                                                     (0, 0, 0, 0,       22, 21      ),  -- 22
                                                     (0, 0, 0, 0,       23, 18      ),  -- 23
                                                     (0, 0,           24, 23, 22, 17),  -- 24
                                                     (0, 0, 0, 0,       25, 22      ),  -- 25
                                                     (0, 0,           26, 6, 2, 1),  -- 26
                                                     (0, 0,           27, 5, 2, 1),  -- 27
                                                     (0, 0, 0, 0,       28, 25      ),  -- 28
                                                     (0, 0, 0, 0,       29, 27      ),  -- 29
                                                     (0, 0,           30, 6, 4, 1),  -- 30
                                                     (0, 0, 0, 0,       31, 28      ),  -- 31
                                                     (0, 0,           32, 22, 2, 1),  -- 32
                                                     (0, 0, 0, 0,       33, 20      ),  -- 33
                                                     (0, 0,           34, 27, 2, 1),  -- 34
                                                     (0, 0, 0, 0,       35, 33      ),  -- 35
                                                     (0, 0, 0, 0,       36, 25      ),  -- 36
                                                     (         37, 5, 4, 3, 2, 1),  -- 37
                                                     (0, 0,           38, 6, 5, 1),  -- 38
                                                     (0, 0, 0, 0,       39, 35      ),  -- 39
                                                     (0, 0,           40, 38, 21, 19),  -- 40
                                                     (0, 0, 0, 0,       41, 38      ),  -- 41
                                                     (0, 0,           42, 41, 20, 19),  -- 42
                                                     (0, 0,           43, 42, 38, 37),  -- 43
                                                     (0, 0,           44, 43, 18, 17),  -- 44
                                                     (0, 0,           45, 44, 42, 41),  -- 45
                                                     (0, 0,           46, 45, 26, 25),  -- 46
                                                     (0, 0, 0, 0,       47, 42      ),  -- 47
                                                     (0, 0,           48, 47, 21, 20),  -- 48
                                                     (0, 0, 0, 0,       49, 40      ),  -- 49
                                                     (0, 0,           50, 49, 24, 23),  -- 50
                                                     (0, 0,           51, 50, 36, 35),  -- 51
                                                     (0, 0, 0, 0,       52, 49      ),  -- 52
                                                     (0, 0,           53, 52, 38, 37),  -- 53
                                                     (0, 0,           54, 53, 18, 17),  -- 54
                                                     (0, 0, 0, 0,       55, 31      ),  -- 55
                                                     (0, 0,           56, 55, 35, 34),  -- 56
                                                     (0, 0, 0, 0,       57, 50      ),  -- 57
                                                     (0, 0, 0, 0,       58, 39      ),  -- 58
                                                     (0, 0,           59, 58, 38, 37),  -- 59
                                                     (0, 0, 0, 0,       60, 59      ),  -- 60
                                                     (0, 0,           61, 60, 46, 45),  -- 61
                                                     (0, 0,           62, 61, 6, 5),  -- 62
                                                     (0, 0, 0, 0,       63, 62      ),  -- 63
                                                     (0, 0,           64, 63, 61, 60),  -- 64
                                                     (0, 0, 0, 0,       65, 47      ),  -- 65
                                                     (0, 0,           66, 65, 57, 56),  -- 66
                                                     (0, 0,           67, 66, 58, 57),  -- 67
                                                     (0, 0, 0, 0,       68, 59      ),  -- 68
                                                     (0, 0,           69, 67, 42, 40),  -- 69
                                                     (0, 0,           70, 69, 55, 54),  -- 70
                                                     (0, 0, 0, 0,       71, 65      ),  -- 71
                                                     (0, 0,           72, 66, 25, 19));  -- 72

  -- Procedure for calculating the next PSRG and COUNTER sequence value
  procedure common_lfsr_nxt_seq(constant c_lfsr_nr : in  natural;
                                constant g_incr    : in  integer;
                                         in_en     : in  std_logic;
                                         in_req    : in  std_logic;
                                         in_dat    : in  std_logic_vector;
                                         prsg      : in  std_logic_vector;
                                         cntr      : in  std_logic_vector;
                                signal   nxt_prsg  : out std_logic_vector;
                                signal   nxt_cntr  : out std_logic_vector);

  -- Use lfsr part of common_lfsr_nxt_seq to make a random bit generator function
  -- . width of lfsr selects the LFSR sequence
  -- . initialized lfsr with (OTHERS=>'0')
  -- . use lfsr(lfsr'HIGH) as random bit
  function func_common_random(lfsr : std_logic_vector) return std_logic_vector;
end common_lfsr_sequences_pkg;

package body common_lfsr_sequences_pkg is
  procedure common_lfsr_nxt_seq(constant c_lfsr_nr : in  natural;
                                constant g_incr    : in  integer;
                                         in_en     : in  std_logic;
                                         in_req    : in  std_logic;
                                         in_dat    : in  std_logic_vector;
                                         prsg      : in  std_logic_vector;
                                         cntr      : in  std_logic_vector;
                                signal   nxt_prsg  : out std_logic_vector;
                                signal   nxt_cntr  : out std_logic_vector) is
    variable v_feedback : std_logic;
  begin
    nxt_prsg <= prsg;
    nxt_cntr <= cntr;
    if in_en = '0' then  -- init reference value
      nxt_prsg <= in_dat;
      nxt_cntr <= in_dat;
    elsif in_req = '1' then  -- next reference value
      -- PRSG shift
      nxt_prsg    <= prsg(prsg'high - 1 downto 0) & '0';
      -- PRSG feedback
      v_feedback := '0';
      for I in c_common_lfsr_max_nof_feedbacks - 1 downto 0 loop
        if c_common_lfsr_sequences(c_lfsr_nr)(I) /= 0 then
          v_feedback := v_feedback xor prsg(c_common_lfsr_sequences(c_lfsr_nr)(I) - 1);
        end if;

      end loop;
      nxt_prsg(0) <= not v_feedback;

      -- COUNTER
      nxt_cntr <= INCR_UVEC(cntr, g_incr);
    end if;
  end common_lfsr_nxt_seq;

  function func_common_random(lfsr : std_logic_vector) return std_logic_vector is
    constant c_lfsr_nr  : natural := lfsr'length - c_common_lfsr_first;
    variable v_nxt_lfsr : std_logic_vector(lfsr'range);
    variable v_feedback : std_logic;
  begin
    -- shift
    v_nxt_lfsr := lfsr(lfsr'high - 1 downto 0) & '0';
    -- feedback
    v_feedback := '0';
    for I in c_common_lfsr_max_nof_feedbacks - 1 downto 0 loop
      if c_common_lfsr_sequences(c_lfsr_nr)(I) /= 0 then
        v_feedback := v_feedback xor lfsr(c_common_lfsr_sequences(c_lfsr_nr)(I) - 1);
      end if;
    end loop;
    v_nxt_lfsr(0) := not v_feedback;
    return v_nxt_lfsr;
  end func_common_random;
end common_lfsr_sequences_pkg;
