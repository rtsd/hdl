-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provides a wideband WG by using multiple diag_wg
-- Description:
-- Remarks:
-- Remarks:
-- . For g_wideband_factor=1 this diag_wg_wideband defaults to diag_wg. Hence
--   no need to make a mms_diag_wg.vhd.

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_diag_wg_wideband_arr is
  generic (
    g_technology         : natural := c_tech_select_default;
    -- Use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_nof_streams        : positive := 1;
    g_cross_clock_domain : boolean := true;

    -- Use g_buf_dir to be able to have different path to waveform file for sim and for synth
    g_buf_dir            : string := "data/";

    -- Wideband parameters
    g_wideband_factor    : natural := 4;  -- Wideband rate factor >= 1 for unit frequency of g_wideband_factor * Fs

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          : natural := 18;
    g_buf_addr_w         : natural := 11;
    g_calc_support       : boolean := true;
    g_calc_gain_w        : natural := 1;
    g_calc_dat_w         : natural := 12
  );
  port (
    -- Memory-mapped clock domain
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    reg_mosi             : in  t_mem_mosi;
    reg_miso             : out t_mem_miso;

    buf_mosi             : in  t_mem_mosi;
    buf_miso             : out t_mem_miso;

    -- Streaming clock domain
    st_rst               : in  std_logic;
    st_clk               : in  std_logic;
    st_restart           : in  std_logic := '0';

    out_sosi_arr         : out  t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_diag_wg_wideband_arr;

architecture str of mms_diag_wg_wideband_arr is
  constant c_reg_adr_w    : natural := 2;

  signal reg_mosi_arr     : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr     : t_mem_miso_arr(g_nof_streams - 1 downto 0);
  signal buf_mosi_arr     : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal buf_miso_arr     : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  signal wg_ovr           : std_logic_vector(g_nof_streams * g_wideband_factor            - 1 downto 0);  -- big endian, so first output sample in MSBit, MSData
  signal wg_dat           : std_logic_vector(g_nof_streams * g_wideband_factor * g_buf_dat_w - 1 downto 0);
  signal wg_val           : std_logic_vector(g_nof_streams * g_wideband_factor            - 1 downto 0);
  signal wg_sync          : std_logic_vector(g_nof_streams * g_wideband_factor            - 1 downto 0);
begin
  u_common_mem_mux_reg : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  u_common_mem_mux_buf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => g_buf_addr_w
  )
  port map (
    mosi     => buf_mosi,
    miso     => buf_miso,
    mosi_arr => buf_mosi_arr,
    miso_arr => buf_miso_arr
  );

  gen_wg : for I in 0 to g_nof_streams - 1 generate
    u_mms_diag_wg_wideband : entity work.mms_diag_wg_wideband
    generic map (
      g_technology         => g_technology,
      g_cross_clock_domain => g_cross_clock_domain,
      g_buf_dir            => g_buf_dir,
      g_wideband_factor    => g_wideband_factor,
      g_buf_dat_w          => g_buf_dat_w,
      g_buf_addr_w         => g_buf_addr_w,
      g_calc_support       => g_calc_support,
      g_calc_gain_w        => g_calc_gain_w,
      g_calc_dat_w         => g_calc_dat_w
    )
    port map (
   -- Memory-mapped clock domain
      mm_rst               => mm_rst,
      mm_clk               => mm_clk,

      reg_mosi             => reg_mosi_arr(I),
      reg_miso             => reg_miso_arr(I),

      buf_mosi             => buf_mosi_arr(I),
      buf_miso             => buf_miso_arr(I),

      -- Streaming clock domain
      st_rst               => st_rst,
      st_clk               => st_clk,
      st_restart           => st_restart,

      out_ovr              => wg_ovr( (I + 1) * g_wideband_factor            - 1 downto I * g_wideband_factor            ),
      out_dat              => wg_dat( (I + 1) * g_wideband_factor * g_buf_dat_w - 1 downto I * g_wideband_factor * g_buf_dat_w),
      out_val              => wg_val( (I + 1) * g_wideband_factor            - 1 downto I * g_wideband_factor            ),
      out_sync             => wg_sync((I + 1) * g_wideband_factor            - 1 downto I * g_wideband_factor            )
  );

    -- wire the wg signals to sosi outputs
    -- This is done as per the method used in unb1_bn_capture_input (Apertif)
    -- . all wideband samples will be valid in parallel, so using vector_or() or vector_and() is fine
    -- . if one of the wideband sample has overflow, then set the overflow error, so use vector_or()
    out_sosi_arr(I).data  <= RESIZE_DP_SDATA(wg_dat( (I + 1) * g_wideband_factor * g_buf_dat_w - 1 downto I * g_wideband_factor * g_buf_dat_w));
    out_sosi_arr(I).valid <=       vector_or(wg_val( (I + 1) * g_wideband_factor            - 1 downto I * g_wideband_factor            ));
    out_sosi_arr(I).sync  <=       vector_or(wg_sync((I + 1) * g_wideband_factor            - 1 downto I * g_wideband_factor            ));
    out_sosi_arr(I).err   <= TO_DP_ERROR(0) when
                                  vector_or(wg_ovr( (I + 1) * g_wideband_factor             - 1 downto I * g_wideband_factor            )) = '0' else
                            TO_DP_ERROR(2**7);  -- pass ADC or WG overflow info on as an error signal
  end generate;
end str;
