-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Adapt the g_in_latency input ready to the g_out_latency output latency.
--   A typical application is to use this latency adapter to provide a read
--   ahead interface to a default FIFO with e.g. read latency 1 or 2.
-- Description:
--   If g_in_latency > g_out_latency then the input latency is first adapted
--   to zero latency by means of a latency FIFO. After that a delay line for
--   src_in.ready yields the g_out_latency output latency.
--   If g_in_latency < g_out_latency, then a delay line for src_in.ready yields
--   the g_out_latency output latency.
--   The sync input is also passed on, only if it occurs during valid. The
--   constant c_pass_sync_during_not_valid is defined to preserve the
--   corresponding section of code for passing the sync also during not valid.
-- Remark:
-- . The snk_out.ready is derived combinatorially from the src_in.ready. If for
--   timing performance it is needed to register snk_out.ready, then this can
--   be done by first increasing the ready latency using this adapter with
--   g_in_latency = g_out_latency + 1, followed by a second adapter to reach
--   the required output ready latency latency.

entity dp_latency_adapter is
  generic (
    g_in_latency   : natural := 3;
    g_out_latency  : natural := 1
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- Monitor internal FIFO filling
    fifo_usedw   : out std_logic_vector(ceil_log2(2 + g_in_latency) - 1 downto 0);  -- see description of c_fifo_size, c_usedw_w for explanation of why +2
    fifo_ful     : out std_logic;
    fifo_emp     : out std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_latency_adapter;

architecture rtl of dp_latency_adapter is
  -- The difference between the input ready latency and the output ready latency
  constant c_diff_latency               : integer := g_out_latency - g_in_latency;

  -- Define constant to preserve the corresponding section of code, but default keep it at FALSE
  constant c_pass_sync_during_not_valid : boolean := false;

  -- Use g_in_latency+1 words for the FIFO data array, to go to zero latency
  constant c_high           : natural := g_in_latency;
  constant c_fifo_size      : natural := g_in_latency + 1;  -- +1 because RL=0 also requires a word
  constant c_usedw_w        : natural := ceil_log2(c_fifo_size+1);  -- +1 because to store value 2**n requires n+1 bits

  signal fifo_reg           : t_dp_sosi_arr(c_high downto 0);
  signal nxt_fifo_reg       : t_dp_sosi_arr(c_high downto 0);
  signal fifo_reg_valid     : std_logic_vector(c_high downto 0);  -- debug signal for Wave window

  signal nxt_fifo_usedw     : std_logic_vector(c_usedw_w - 1 downto 0);
  signal nxt_fifo_ful       : std_logic;
  signal nxt_fifo_emp       : std_logic;

  signal ff_siso            : t_dp_siso;  -- SISO ready
  signal ff_sosi            : t_dp_sosi;  -- SOSI

  signal i_snk_out          : t_dp_siso := c_dp_siso_rdy;
begin
  -- Use i_snk_out with defaults to force unused snk_out bits and fields to '0'
  snk_out <= i_snk_out;

  gen_wires : if c_diff_latency = 0 generate  -- g_out_latency = g_in_latency
    i_snk_out <= src_in;  -- SISO
    src_out   <= snk_in;  -- SOSI
  end generate gen_wires;

  no_fifo : if c_diff_latency > 0 generate  -- g_out_latency > g_in_latency
    -- Go from g_in_latency to required larger g_out_latency
    u_latency : entity work.dp_latency_increase
    generic map (
      g_in_latency   => g_in_latency,
      g_incr_latency => c_diff_latency
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sink
      snk_out   => i_snk_out,
      snk_in    => snk_in,
      -- ST source
      src_in    => src_in,
      src_out   => src_out
    );
  end generate no_fifo;

  gen_fifo : if c_diff_latency < 0 generate  -- g_out_latency < g_in_latency
    -- Register [0] contains the FIFO output with zero ready latency
    ff_sosi <= fifo_reg(0);

    p_clk_fifo : process(rst, clk)
    begin
      if rst = '1' then
        fifo_reg   <= (others => c_dp_sosi_rst);
        fifo_usedw <= (others => '0');
        fifo_ful   <= '0';
        fifo_emp   <= '1';
      elsif rising_edge(clk) then
        fifo_reg   <= nxt_fifo_reg;
        fifo_usedw <= nxt_fifo_usedw;
        fifo_ful   <= nxt_fifo_ful;
        fifo_emp   <= nxt_fifo_emp;
      end if;
    end process;

    -- Pass on frame level flow control
    i_snk_out.xon <= src_in.xon;

    p_snk_out_ready : process(fifo_reg, ff_siso, snk_in)
    begin
      i_snk_out.ready <= '0';
      if ff_siso.ready = '1' then
        -- Default snk_out ready when the source is ready.
        i_snk_out.ready <= '1';
      else
        -- Extra snk_out ready to look ahead for src_in RL = 0.
        -- The fifo_reg[h:0] size is g_in_latency+1 number of SOSI values.
        -- . The fifo_reg[h:1] provide free space for h=g_in_latency nof data
        --   when snk_out.ready is pulled low, because then there can still
        --   arrive g_in_latency nof new data with snk_in.valid asserted.
        -- . The [0] is the registered output SOSI value with RL=0. Therefore
        --   fifo_reg[0] can still accept a new input when ff_siso.ready is
        --   low. If this assignment is omitted then the functionallity is
        --   still OK, but the throughtput sligthly reduces.
        if fifo_reg(0).valid = '0' then
          i_snk_out.ready <= '1';
        elsif fifo_reg(1).valid = '0' then
          i_snk_out.ready <= not(snk_in.valid);
        end if;
      end if;
    end process;

    p_fifo_reg : process(fifo_reg, ff_siso, snk_in)
    begin
      -- Keep or shift the fifo_reg dependent on ff_siso.ready, no need to explicitly check fifo_reg().valid
      nxt_fifo_reg <= fifo_reg;
      if ff_siso.ready = '1' then
        nxt_fifo_reg(c_high - 1 downto 0) <= fifo_reg(c_high downto 1);
        nxt_fifo_reg(c_high).valid <= '0';
        nxt_fifo_reg(c_high).sync  <= '0';
        nxt_fifo_reg(c_high).sop   <= '0';
        nxt_fifo_reg(c_high).eop   <= '0';
        -- Forcing the nxt_fifo_reg[h] control fields to '0' is robust, but not
        -- strictly necessary, because the control fields in fifo_reg[h] will
        -- have been set to '0' already earlier due to the snk_in when
        -- ff_siso.ready was '0'.
      end if;

      -- Put input data at the first available location dependent on ff_siso.ready, no need to explicitly check snk_in.valid
      if fifo_reg(0).valid = '0' then
        nxt_fifo_reg(0) <= snk_in;  -- fifo_reg is empty
      else
        -- The fifo_reg is not empty, so filled to some extend
        for I in 1 to c_high loop
          if fifo_reg(I).valid = '0' then
            if ff_siso.ready = '0' then
              nxt_fifo_reg(I)   <= snk_in;
            else
              nxt_fifo_reg(I - 1) <= snk_in;
            end if;
            exit;
          end if;
        end loop;

        -- Default the input sync during input data valid is only passed on with the valid input data.
        -- When c_pass_sync_during_not_valid is enabled then the input sync during input data not valid is passed on via the head fifo_reg(0) if the fifo_reg is empty.
        if c_pass_sync_during_not_valid = true and snk_in.sync = '1' and snk_in.valid = '0' then
          -- Otherwise for input sync during input data not valid we need to insert the input sync at the last location with valid data independent of ff_siso.ready, to avoid that it gets lost.
          -- For streams that do not use the sync this logic will be void and optimize away by synthesis, because then snk_in.sync = '0' fixed.
          if fifo_reg(c_high).valid = '1' then  -- fifo_reg is full
            nxt_fifo_reg(c_high).sync <= '1';  -- insert input sync
          else
            for I in c_high - 1 downto 0 loop  -- fifo_reg is filled to some extend, so not full and not empty
              if fifo_reg(I).valid = '1' then
                nxt_fifo_reg(I + 1).sync <= '0';  -- overrule default sync assignment
                nxt_fifo_reg(I).sync   <= '1';  -- insert input sync
                exit;
              end if;
            end loop;
          end if;
        end if;
      end if;
    end process;

    p_fifo_usedw : process(nxt_fifo_reg)
    begin
      nxt_fifo_usedw <= (others => '0');
      for I in c_high downto 0 loop
        if nxt_fifo_reg(I).valid = '1' then
          nxt_fifo_usedw <= TO_UVEC(I + 1, c_usedw_w);
          exit;
        end if;
      end loop;
    end process;

    fifo_reg_valid <= func_dp_stream_arr_get(fifo_reg, "VALID");

    nxt_fifo_ful <= '1' when TO_UINT(nxt_fifo_usedw) >= c_high + 1 else '0';  -- using >= or = is equivalent here
    nxt_fifo_emp <= '1' when TO_UINT(nxt_fifo_usedw) = 0        else '0';

    -- Go from 0 FIFO latency to required g_out_latency (only wires when g_out_latency=0)
    u_latency : entity work.dp_latency_increase
    generic map (
      g_in_latency   => 0,
      g_incr_latency => g_out_latency
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sink
      snk_out   => ff_siso,
      snk_in    => ff_sosi,
      -- ST source
      src_in    => src_in,
      src_out   => src_out
    );
  end generate gen_fifo;
end rtl;
