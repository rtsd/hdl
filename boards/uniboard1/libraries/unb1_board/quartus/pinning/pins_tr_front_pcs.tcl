###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/pins_tr_front_pcs_clk.tcl
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/pins_tr_front_pcs_0.tcl
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/pins_tr_front_pcs_1.tcl
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/pins_tr_front_pcs_2.tcl
