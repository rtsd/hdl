-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- . Reinier van der Walle
-- Purpose:
-- . Provide 40G ethernet I/O interface (BSP) for OpenCL kernel on Arria10
-- Description:
-- . This core consists of:
--   . An Intel/Altera 40G Low Latency MAC instance
--   . SOP/EOP insertion (kernel channel only carries data and valid)
--   . Dual clock FIFO
--     . Clock domain transition between kernel_clk and clk_txmac
--     . Buffers full Ethernet packet (40G MAC requires uninterrupted packet)
--   . Clock (PLL) / reset generation
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   .
--   . The data field of the ST-avalon interface is also used to provide
--   . SOP, EOP and empty meta-data. The implementation of this is shown below.
--   +-----------+---------+--------------------------------------------------------+
--   | Bit range | Name    | Description                                            |
--   +-----------+---------+--------------------------------------------------------+
--   | [0:255]   | payload | Packet payload                                         |
--   +-----------+---------+--------------------------------------------------------+
--   | 256       | sop     | Start of packet signal                                 |
--   +-----------+---------+--------------------------------------------------------+
--   | 257       | eop     | End of packet signal                                   |
--   +-----------+---------+--------------------------------------------------------+
--   | 258       | -       | reserved bit                                           |
--   +-----------+---------+--------------------------------------------------------+
--   | 259:263   | empty   | On EOP, this field indicates how many bytes are unused |
--   +-----------+---------+--------------------------------------------------------+
library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity ta2_unb2b_40GbE is
  generic (
    g_nof_mac : natural := 1
  );
  port (
    mm_clk       : in  std_logic;  -- 100MHz clk for reconfig block and status interface
    mm_rst       : in  std_logic;

    clk_ref_r    : in  std_logic;  -- 644.53125MHz 40G MAC reference clock

    tx_serial_r  : out std_logic_vector(4 * g_nof_mac - 1 downto 0);  -- Serial TX lanes towards QSFP cage
    rx_serial_r  : in  std_logic_vector(4 * g_nof_mac - 1 downto 0);  -- Serial RX lanes from QSFP cage

    kernel_clk   : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset : in  std_logic;

    src_out_arr  : out t_dp_sosi_arr(g_nof_mac - 1 downto 0);
    src_in_arr   : in  t_dp_siso_arr(g_nof_mac - 1 downto 0);
    snk_out_arr  : out t_dp_siso_arr(g_nof_mac - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_mac - 1 downto 0)

  );
end ta2_unb2b_40GbE;

architecture str of ta2_unb2b_40GbE is
  constant c_max_packet_size : natural := 128;  -- 128 * 256 bits words
  constant c_tx_fifo_fill    : natural := 282;  -- Largest frame is 9000 bytes = 1125
  constant c_tx_fifo_size    : natural := 512;
  constant c_rx_fifo_size    : natural := 64;  -- should be large enough

  constant c_data_w : natural := 256;
  ----------------------------------------------------------------------------
  -- Reset signals
  ----------------------------------------------------------------------------
  signal rst_txmac_arr  : std_logic_vector(g_nof_mac - 1 downto 0);
  signal rst_rxmac_arr  : std_logic_vector(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- dp_xonoff
  ----------------------------------------------------------------------------
  signal dp_xonoff_src_out_arr  : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_xonoff_src_in_arr   : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- Latency adapter tx a
  ----------------------------------------------------------------------------
  signal dp_latency_adapter_tx_a_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_a_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_a_snk_in_arr  : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_a_snk_out_arr : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- Latency adapter tx b
  ----------------------------------------------------------------------------
  signal dp_latency_adapter_tx_b_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_b_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- Latency adapter rx
  ----------------------------------------------------------------------------
  signal dp_latency_adapter_rx_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_rx_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- TX FIFO
  ----------------------------------------------------------------------------
  signal dp_fifo_fill_eop_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_fifo_fill_eop_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- 40G MAC IP
  ----------------------------------------------------------------------------
  signal serial_clk_2arr : t_slv_4_arr(g_nof_mac - 1 downto 0);
  signal serial_clk_arr : std_logic_vector(g_nof_mac - 1 downto 0);

  signal pll_locked_arr : std_logic_vector(g_nof_mac - 1 downto 0);

  signal rx_pcs_ready_arr : std_logic_vector(g_nof_mac - 1 downto 0);

  signal clk_txmac_arr : std_logic_vector(g_nof_mac - 1 downto 0);  -- MAC + PCS clock - at least 312.5Mhz
  signal clk_rxmac_arr : std_logic_vector(g_nof_mac - 1 downto 0);  -- MAC + PCS clock - at least 312.5Mhz

  signal l4_tx_sosi_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal l4_tx_siso_arr : t_dp_siso_arr(g_nof_mac - 1 downto 0);
  signal l4_rx_sosi_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal l4_rx_siso_arr : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- RX FIFO
  ----------------------------------------------------------------------------
  signal dp_fifo_dc_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_fifo_dc_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  ----------------------------------------------------------------------------
  -- ATX PLL Component
  ----------------------------------------------------------------------------
  component arria10_40g_atx_pll is
  port (
    pll_cal_busy  : out std_logic;  -- pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked
    pll_powerdown : in  std_logic := 'X';  -- pll_powerdown
    pll_refclk0   : in  std_logic := 'X';  -- clk
    tx_serial_clk : out std_logic  -- clk
  );
  end component arria10_40g_atx_pll;

  ----------------------------------------------------------------------------
  -- 40G ETH IP Component
  ----------------------------------------------------------------------------
  component arria10_40g_mac is
  port (
    l4_rx_error           : out std_logic_vector(5 downto 0);  -- l4_rx_error
    l4_rx_status          : out std_logic_vector(2 downto 0);  -- l4_rx_status
    l4_rx_valid           : out std_logic;  -- l4_rx_valid
    l4_rx_startofpacket   : out std_logic;  -- l4_rx_startofpacket
    l4_rx_endofpacket     : out std_logic;  -- l4_rx_endofpacket
    l4_rx_data            : out std_logic_vector(255 downto 0);  -- l4_rx_data
    l4_rx_empty           : out std_logic_vector(4 downto 0);  -- l4_rx_empty
    l4_rx_fcs_error       : out std_logic;  -- l4_rx_fcs_error
    l4_rx_fcs_valid       : out std_logic;  -- l4_rx_fcs_valid
    l4_tx_startofpacket   : in  std_logic                      := 'X';  -- l4_tx_startofpacket
    l4_tx_endofpacket     : in  std_logic                      := 'X';  -- l4_tx_endofpacket
    l4_tx_valid           : in  std_logic                      := 'X';  -- l4_tx_valid
    l4_tx_ready           : out std_logic;  -- l4_tx_ready
    l4_tx_empty           : in  std_logic_vector(4 downto 0)   := (others => 'X');  -- l4_tx_empty
    l4_tx_data            : in  std_logic_vector(255 downto 0) := (others => 'X');  -- l4_tx_data
    l4_tx_error           : in  std_logic                      := 'X';  -- l4_tx_error
    clk_ref               : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- clk_ref
    clk_rxmac             : out std_logic_vector(0 downto 0);  -- clk_rxmac
    clk_status            : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- clk_status
    clk_txmac             : out std_logic_vector(0 downto 0);  -- clk_txmac
    reconfig_address      : in  std_logic_vector(11 downto 0)  := (others => 'X');  -- reconfig_address
    reconfig_clk          : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reconfig_clk
    reconfig_read         : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reconfig_read
    reconfig_readdata     : out std_logic_vector(31 downto 0);  -- reconfig_readdata
    reconfig_reset        : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reconfig_reset
    reconfig_waitrequest  : out std_logic_vector(0 downto 0);  -- reconfig_waitrequest
    reconfig_write        : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reconfig_write
    reconfig_writedata    : in  std_logic_vector(31 downto 0)  := (others => 'X');  -- reconfig_writedata
    reset_async           : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reset_async
    reset_status          : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- reset_status
    rx_pcs_ready          : out std_logic_vector(0 downto 0);  -- rx_pcs_ready
    rx_serial             : in  std_logic_vector(3 downto 0)   := (others => 'X');  -- rx_serial
    rx_inc_octetsOK       : out std_logic_vector(15 downto 0);  -- rx_inc_octetsOK
    rx_inc_octetsOK_valid : out std_logic_vector(0 downto 0);  -- rx_inc_octetsOK_valid
    rx_inc_runt           : out std_logic_vector(0 downto 0);  -- rx_inc_runt
    rx_inc_64             : out std_logic_vector(0 downto 0);  -- rx_inc_64
    rx_inc_127            : out std_logic_vector(0 downto 0);  -- rx_inc_127
    rx_inc_255            : out std_logic_vector(0 downto 0);  -- rx_inc_255
    rx_inc_511            : out std_logic_vector(0 downto 0);  -- rx_inc_511
    rx_inc_1023           : out std_logic_vector(0 downto 0);  -- rx_inc_1023
    rx_inc_1518           : out std_logic_vector(0 downto 0);  -- rx_inc_1518
    rx_inc_max            : out std_logic_vector(0 downto 0);  -- rx_inc_max
    rx_inc_over           : out std_logic_vector(0 downto 0);  -- rx_inc_over
    rx_inc_mcast_data_err : out std_logic_vector(0 downto 0);  -- rx_inc_mcast_data_err
    rx_inc_mcast_data_ok  : out std_logic_vector(0 downto 0);  -- rx_inc_mcast_data_ok
    rx_inc_bcast_data_err : out std_logic_vector(0 downto 0);  -- rx_inc_bcast_data_err
    rx_inc_bcast_data_ok  : out std_logic_vector(0 downto 0);  -- rx_inc_bcast_data_ok
    rx_inc_ucast_data_err : out std_logic_vector(0 downto 0);  -- rx_inc_ucast_data_err
    rx_inc_ucast_data_ok  : out std_logic_vector(0 downto 0);  -- rx_inc_ucast_data_ok
    rx_inc_mcast_ctrl     : out std_logic_vector(0 downto 0);  -- rx_inc_mcast_ctrl
    rx_inc_bcast_ctrl     : out std_logic_vector(0 downto 0);  -- rx_inc_bcast_ctrl
    rx_inc_ucast_ctrl     : out std_logic_vector(0 downto 0);  -- rx_inc_ucast_ctrl
    rx_inc_pause          : out std_logic_vector(0 downto 0);  -- rx_inc_pause
    rx_inc_fcs_err        : out std_logic_vector(0 downto 0);  -- rx_inc_fcs_err
    rx_inc_fragment       : out std_logic_vector(0 downto 0);  -- rx_inc_fragment
    rx_inc_jabber         : out std_logic_vector(0 downto 0);  -- rx_inc_jabber
    rx_inc_sizeok_fcserr  : out std_logic_vector(0 downto 0);  -- rx_inc_sizeok_fcserr
    rx_inc_pause_ctrl_err : out std_logic_vector(0 downto 0);  -- rx_inc_pause_ctrl_err
    rx_inc_mcast_ctrl_err : out std_logic_vector(0 downto 0);  -- rx_inc_mcast_ctrl_err
    rx_inc_bcast_ctrl_err : out std_logic_vector(0 downto 0);  -- rx_inc_bcast_ctrl_err
    rx_inc_ucast_ctrl_err : out std_logic_vector(0 downto 0);  -- rx_inc_ucast_ctrl_err
    status_write          : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- status_write
    status_read           : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- status_read
    status_addr           : in  std_logic_vector(15 downto 0)  := (others => 'X');  -- status_addr
    status_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X');  -- status_writedata
    status_readdata       : out std_logic_vector(31 downto 0);  -- status_readdata
    status_readdata_valid : out std_logic_vector(0 downto 0);  -- status_readdata_valid
    status_waitrequest    : out std_logic_vector(0 downto 0);  -- status_waitrequest
    status_read_timeout   : out std_logic_vector(0 downto 0);  -- status_read_timeout
    tx_lanes_stable       : out std_logic_vector(0 downto 0);  -- tx_lanes_stable
    tx_pll_locked         : in  std_logic_vector(0 downto 0)   := (others => 'X');  -- tx_pll_locked
    tx_serial             : out std_logic_vector(3 downto 0);  -- tx_serial
    tx_serial_clk         : in  std_logic_vector(3 downto 0)   := (others => 'X');  -- tx_serial_clk
    tx_inc_octetsOK       : out std_logic_vector(15 downto 0);  -- tx_inc_octetsOK
    tx_inc_octetsOK_valid : out std_logic_vector(0 downto 0);  -- tx_inc_octetsOK_valid
    tx_inc_64             : out std_logic_vector(0 downto 0);  -- tx_inc_64
    tx_inc_127            : out std_logic_vector(0 downto 0);  -- tx_inc_127
    tx_inc_255            : out std_logic_vector(0 downto 0);  -- tx_inc_255
    tx_inc_511            : out std_logic_vector(0 downto 0);  -- tx_inc_511
    tx_inc_1023           : out std_logic_vector(0 downto 0);  -- tx_inc_1023
    tx_inc_1518           : out std_logic_vector(0 downto 0);  -- tx_inc_1518
    tx_inc_max            : out std_logic_vector(0 downto 0);  -- tx_inc_max
    tx_inc_over           : out std_logic_vector(0 downto 0);  -- tx_inc_over
    tx_inc_mcast_data_err : out std_logic_vector(0 downto 0);  -- tx_inc_mcast_data_err
    tx_inc_mcast_data_ok  : out std_logic_vector(0 downto 0);  -- tx_inc_mcast_data_ok
    tx_inc_bcast_data_err : out std_logic_vector(0 downto 0);  -- tx_inc_bcast_data_err
    tx_inc_bcast_data_ok  : out std_logic_vector(0 downto 0);  -- tx_inc_bcast_data_ok
    tx_inc_ucast_data_err : out std_logic_vector(0 downto 0);  -- tx_inc_ucast_data_err
    tx_inc_ucast_data_ok  : out std_logic_vector(0 downto 0);  -- tx_inc_ucast_data_ok
    tx_inc_mcast_ctrl     : out std_logic_vector(0 downto 0);  -- tx_inc_mcast_ctrl
    tx_inc_bcast_ctrl     : out std_logic_vector(0 downto 0);  -- tx_inc_bcast_ctrl
    tx_inc_ucast_ctrl     : out std_logic_vector(0 downto 0);  -- tx_inc_ucast_ctrl
    tx_inc_pause          : out std_logic_vector(0 downto 0);  -- tx_inc_pause
    tx_inc_fcs_err        : out std_logic_vector(0 downto 0);  -- tx_inc_fcs_err
    tx_inc_fragment       : out std_logic_vector(0 downto 0);  -- tx_inc_fragment
    tx_inc_jabber         : out std_logic_vector(0 downto 0);  -- tx_inc_jabber
    tx_inc_sizeok_fcserr  : out std_logic_vector(0 downto 0)  -- tx_inc_sizeok_fcserr
  );
  end component arria10_40g_mac;
begin
  gen_mac: for mac in 0 to g_nof_mac - 1 generate
    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    gen_tx_bytes: for I in 0 to 31 generate
      dp_latency_adapter_tx_a_snk_in_arr(mac).data(8 * (32 - I) - 1  downto 8 * (31 - I)) <= snk_in_arr(mac).data(8 * (I + 1) - 1 downto 8 * I);
    end generate;

    -- Assign correct data fields to control signals.
    dp_latency_adapter_tx_a_snk_in_arr(mac).sop <= snk_in_arr(mac).data(256);
    dp_latency_adapter_tx_a_snk_in_arr(mac).eop <= snk_in_arr(mac).data(257);
    dp_latency_adapter_tx_a_snk_in_arr(mac).empty(4 downto 0) <= snk_in_arr(mac).data(263 downto 259);
    dp_latency_adapter_tx_a_snk_in_arr(mac).valid <= snk_in_arr(mac).valid;
    snk_out_arr(mac).ready <= dp_latency_adapter_tx_a_snk_out_arr(mac).ready;  -- Flow control towards source (kernel)
    snk_out_arr(mac).xon   <= rx_pcs_ready_arr(mac);

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=0 (OpenCL kernel) to RL=1 (downstream).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_tx_a : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 0,
      g_out_latency => 1
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_latency_adapter_tx_a_snk_in_arr(mac),
      snk_out   => dp_latency_adapter_tx_a_snk_out_arr(mac),

      src_out   => dp_latency_adapter_tx_a_src_out_arr(mac),
      src_in    => dp_latency_adapter_tx_a_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- dp_xonoff: discard all TX frames until 40G MAC TX side is ready
    ----------------------------------------------------------------------------
    u_dp_xonoff : entity dp_lib.dp_xonoff
    port map (
      clk           => kernel_clk,
      rst           => kernel_reset,

      in_sosi       => dp_latency_adapter_tx_a_src_out_arr(mac),
      in_siso       => dp_latency_adapter_tx_a_src_in_arr(mac),

      out_sosi      => dp_xonoff_src_out_arr(mac),
      out_siso      => dp_xonoff_src_in_arr(mac)  -- flush control via out_siso.xon
    );

    ----------------------------------------------------------------------------
    -- TX FIFO
    ----------------------------------------------------------------------------
    u_dp_fifo_fill_eop : entity dp_lib.dp_fifo_fill_eop
    generic map (
      g_data_w         => c_data_w,
      g_use_dual_clock => true,
      g_empty_w        => 8,
      g_use_empty      => true,
      g_use_bsn        => false,
      g_bsn_w          => 64,
      g_use_channel    => false,
      g_use_sync       => false,
      g_fifo_size      => c_tx_fifo_size,
      g_fifo_fill      => c_tx_fifo_fill
   )
    port map (
      wr_clk  => kernel_clk,
      wr_rst  => kernel_reset,

      rd_clk  => clk_txmac_arr(mac),
      rd_rst  => rst_txmac_arr(mac),

      snk_in  => dp_xonoff_src_out_arr(mac),
      snk_out => dp_xonoff_src_in_arr(mac),

      src_out => dp_fifo_fill_eop_src_out_arr(mac),
      src_in  => dp_fifo_fill_eop_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=1 (upstream) to RL=0 (MAC TX interface).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_tx_b : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      clk       => clk_txmac_arr(mac),
      rst       => rst_txmac_arr(mac),

      snk_in    => dp_fifo_fill_eop_src_out_arr(mac),
      snk_out   => dp_fifo_fill_eop_src_in_arr(mac),

      src_out   => dp_latency_adapter_tx_b_src_out_arr(mac),
      src_in    => dp_latency_adapter_tx_b_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- 40G MAC IP
    ----------------------------------------------------------------------------
    l4_tx_sosi_arr(mac) <= dp_latency_adapter_tx_b_src_out_arr(mac);
    dp_latency_adapter_tx_b_src_in_arr(mac) <= l4_tx_siso_arr(mac);

    u_arria10_40g_mac : arria10_40g_mac
      port map (
         reset_async(0)         => mm_rst,
         clk_txmac(0)           => clk_txmac_arr(mac),  -- MAC + PCS clock - at least 312.5Mhz
         clk_rxmac(0)           => clk_rxmac_arr(mac),  -- MAC + PCS clock - at least 312.5Mhz
         clk_ref(0)             => clk_ref_r,
         rx_pcs_ready(0)        => rx_pcs_ready_arr(mac),

         tx_serial_clk          => serial_clk_2arr(mac),
         tx_pll_locked(0)       => pll_locked_arr(mac),

         clk_status(0)          => mm_clk,
         reset_status(0)        => mm_rst,
         status_addr            => (others => '0'),
         status_read            => (others => '0'),
         status_write           => (others => '0'),
         status_writedata       => (others => '0'),
--       status_readdata        => status_readdata_eth,
--       status_read_timeout    => status_read_timeout,
--       status_readdata_valid  => status_readdata_valid_eth,

         reconfig_clk(0)        => mm_clk,
         reconfig_reset(0)      => mm_rst,
         reconfig_write         => (others => '0'),
         reconfig_read          => (others => '0'),
         reconfig_address       => (others => '0'),
         reconfig_writedata     => (others => '0'),
--       reconfig_readdata      => reco_readdata[31:0],
--       reconfig_waitrequest   => reco_waitrequest,

         l4_tx_data             => l4_tx_sosi_arr(mac).data(255 downto 0),
         l4_tx_empty            => l4_tx_sosi_arr(mac).empty(4 downto 0),
         l4_tx_startofpacket    => l4_tx_sosi_arr(mac).sop,
         l4_tx_endofpacket      => l4_tx_sosi_arr(mac).eop,
         l4_tx_ready            => l4_tx_siso_arr(mac).ready,
         l4_tx_valid            => l4_tx_sosi_arr(mac).valid,
         l4_tx_error            => '0',

         l4_rx_data             => l4_rx_sosi_arr(mac).data(255 downto 0),
         l4_rx_empty            => l4_rx_sosi_arr(mac).empty(4 downto 0),
         l4_rx_startofpacket    => l4_rx_sosi_arr(mac).sop,
         l4_rx_endofpacket      => l4_rx_sosi_arr(mac).eop,
  --       l4_rx_error            => ,
         l4_rx_valid            => l4_rx_sosi_arr(mac).valid,

  --        l4_rx_status                  (),
  --        l4_rx_fcs_error               (),
  --        l4_rx_fcs_valid               (),
  --        rx_inc_octetsOK               (),
  --        rx_inc_octetsOK_valid         (),
  --        rx_inc_runt                   (),
  --        rx_inc_64                     (),
  --        rx_inc_127                    (),
  --        rx_inc_255                    (),
  --        rx_inc_511                    (),
  --        rx_inc_1023                   (),
  --        rx_inc_1518                   (),
  --        rx_inc_max                    (),
  --        rx_inc_over                   (),
  --        rx_inc_mcast_data_err         (),
  --        rx_inc_mcast_data_ok          (),
  --        rx_inc_bcast_data_err         (),
  --        rx_inc_bcast_data_ok          (),
  --        rx_inc_ucast_data_err         (),
  --        rx_inc_ucast_data_ok          (),
  --        rx_inc_mcast_ctrl             (),
  --        rx_inc_bcast_ctrl             (),
  --        rx_inc_ucast_ctrl             (),
  --        rx_inc_pause                  (),
  --        rx_inc_fcs_err                (),
  --        rx_inc_fragment               (),
  --        rx_inc_jabber                 (),
  --        rx_inc_sizeok_fcserr          (),
  --        rx_inc_pause_ctrl_err         (),
  --        rx_inc_mcast_ctrl_err         (),
  --        rx_inc_bcast_ctrl_err         (),
  --        rx_inc_ucast_ctrl_err         (),
  --        status_waitrequest            (),
         tx_lanes_stable(0)     => l4_tx_siso_arr(mac).xon,
  --        tx_inc_octetsOK               (),
  --        tx_inc_octetsOK_valid         (),
  --        tx_inc_64                     (),
  --        tx_inc_127                    (),
  --        tx_inc_255                    (),
  --        tx_inc_511                    (),
  --        tx_inc_1023                   (),
  --        tx_inc_1518                   (),
  --        tx_inc_max                    (),
  --        tx_inc_over                   (),
  --        tx_inc_mcast_data_err         (),
  --        tx_inc_mcast_data_ok          (),
  --        tx_inc_bcast_data_err         (),
  --        tx_inc_bcast_data_ok          (),
  --        tx_inc_ucast_data_err         (),
  --        tx_inc_ucast_data_ok          (),
  --        tx_inc_mcast_ctrl             (),
  --        tx_inc_bcast_ctrl             (),
  --        tx_inc_ucast_ctrl             (),
  --        tx_inc_pause                  (),
  --        tx_inc_fcs_err                (),
  --        tx_inc_fragment               (),
  --        tx_inc_jabber                 (),
  --        tx_inc_sizeok_fcserr          (),

         tx_serial              => tx_serial_r(4 * (mac + 1) - 1 downto 4 * mac),
         rx_serial              => rx_serial_r(4 * (mac + 1) - 1 downto 4 * mac)
      );

    -- No latency adapter needed as the RX MAC does not have a ready input
    ----------------------------------------------------------------------------
    -- RX FIFO
    ----------------------------------------------------------------------------
    rst_rxmac_arr(mac) <= not rx_pcs_ready_arr(mac);
    u_dp_fifo_dc : entity dp_lib.dp_fifo_dc
    generic map (
      g_data_w         => c_data_w,
      g_empty_w        => 8,
      g_use_empty      => true,
      g_use_bsn        => false,
      g_bsn_w          => 64,
      g_use_channel    => false,
      g_use_sync       => false,
      g_fifo_size      => c_rx_fifo_size
   )
    port map (
      wr_clk  => clk_rxmac_arr(mac),
      wr_rst  => rst_rxmac_arr(mac),

      rd_clk  => kernel_clk,
      rd_rst  => kernel_reset,

      snk_in  => l4_rx_sosi_arr(mac),
      snk_out => OPEN,

      src_out => dp_fifo_dc_src_out_arr(mac),
      src_in  => dp_fifo_dc_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=1 (dp_fifo_dc) to RL=0 (OpenCL kernel).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_rx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_fifo_dc_src_out_arr(mac),
      snk_out   => dp_fifo_dc_src_in_arr(mac),

      src_out   => dp_latency_adapter_rx_src_out_arr(mac),
      src_in    => dp_latency_adapter_rx_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    gen_rx_bytes: for I in 0 to 31 generate
      src_out_arr(mac).data(8 * (32 - I) - 1  downto 8 * (31 - I)) <= dp_latency_adapter_rx_src_out_arr(mac).data(8 * (I + 1) - 1 downto 8 * I);
    end generate;

    -- Assign control signals to correct data fields.
    src_out_arr(mac).data(256) <= dp_latency_adapter_rx_src_out_arr(mac).sop;
    src_out_arr(mac).data(257) <= dp_latency_adapter_rx_src_out_arr(mac).eop;
    src_out_arr(mac).data(263 downto 259) <= dp_latency_adapter_rx_src_out_arr(mac).empty(4 downto 0);
    src_out_arr(mac).valid <= dp_latency_adapter_rx_src_out_arr(mac).valid;
    dp_latency_adapter_rx_src_in_arr(mac).ready <= src_in_arr(mac).ready;
    dp_latency_adapter_rx_src_in_arr(mac).xon <= '1';

    -------------------------------------------------------------------------------
    -- Reset signal generation
    -------------------------------------------------------------------------------

    u_common_areset_txmac : entity common_lib.common_areset
    generic map (
      g_rst_level => '1',
      g_delay_len => 3
    )
    port map (
      in_rst    => kernel_reset,
      clk       => clk_txmac_arr(mac),
      out_rst   => rst_txmac_arr(mac)
    );

    -------------------------------------------------------------------------------
    -- PLL for clock generation, every mac needs its own, due to clock nework limitations
    -------------------------------------------------------------------------------
    u_arria10_40g_atx_pll : arria10_40g_atx_pll
    port map (
      pll_cal_busy  => OPEN,
      pll_locked    => pll_locked_arr(mac),
      pll_powerdown => mm_rst,
      pll_refclk0   => clk_ref_r,
      tx_serial_clk => serial_clk_arr(mac)
    );

    gen_serial_clk_arr : for i in 0 to 3 generate
      serial_clk_2arr(mac)(i) <= serial_clk_arr(mac);
    end generate;

  end generate;
end str;
