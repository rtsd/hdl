-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper
-- Description:
--   Quartus II VHDL Template
--   Simple Dual-Port RAM with different read/write addresses and
--   different read/write clock
-- Reference:
--   Copied from ip_arria10_e2sg/ip_arria10_e2sg_simple_dual_port_ram_dual_clock.vhd
--   and the inferred Altera code was obtained using template insert with
--   Quartus 14.0a10.

library ieee;
use ieee.std_logic_1164.all;

entity ip_agi027_1e1v_simple_dual_port_ram_dual_clock is
  generic
  (
    DATA_WIDTH : natural := 8;
    ADDR_WIDTH : natural := 6
  );
  port
  (
    rclk  : in std_logic;
    wclk  : in std_logic;
    raddr : in natural range 0 to 2**ADDR_WIDTH - 1;
    waddr : in natural range 0 to 2**ADDR_WIDTH - 1;
    data  : in std_logic_vector((DATA_WIDTH - 1) downto 0);
    we    : in std_logic := '1';
    q   : out std_logic_vector((DATA_WIDTH - 1) downto 0)
  );
end ip_agi027_1e1v_simple_dual_port_ram_dual_clock;

architecture rtl of ip_agi027_1e1v_simple_dual_port_ram_dual_clock is
  -- Build a 2-D array type for the RAM
  subtype word_t is std_logic_vector((DATA_WIDTH - 1) downto 0);
  type memory_t is array(2**ADDR_WIDTH - 1 downto 0) of word_t;

  -- Declare the RAM signal.
  signal ram : memory_t;
begin
  process(wclk)
  begin
  if(rising_edge(wclk)) then
    if(we = '1') then
      ram(waddr) <= data;
    end if;
  end if;
  end process;

  process(rclk)
  begin
  if(rising_edge(rclk)) then
    q <= ram(raddr);
  end if;
  end process;
end rtl;
