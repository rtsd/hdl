-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:
--   Provide input ready control and use output ready control to the mixed
--   width FIFO.
-- Description:
-- . For framed data (g_use_ctrl=TRUE) the sop and eop are passed along with
--   the narrow data through the FIFO. Unused narrow data are filled too and
--   reported via wide data empty. The wide data empty is only used at the wide
--   data side of the FIFO. At the narrow side the empty data fields are filled
--   with dummy values in case of narrow to wide or skipped in case of wide to
--   narrow FIFO usage. The narrow data empty field is ignored.
-- . For unframed data (g_use_ctrl=FALSE) the sop, eop and wide data empty are
--   not used and the all narrow data per wide data are used.
-- Remark:
-- . When g_use_ctrl=TRUE then the sop and eop are copied (in case of write) or
--   available (in case of read) to each narrow part on the wide side, e.g. for
--   c_nof_narrow = 2:
--
--     narrow side:    wide side:
--                     data[]
--                     sop
--     data[]          eop
--     sop       <-->  data[]
--     eop             sop
--                     eop
-- . The mixed width FIFO is little endian, so the lowest part at the wide side
--   will come out first at the narrow side and vice versa. The SOSI wide data
--   is big endian. Therefore the high narrow data part of the wide SOSI data
--   needs to be accessed via the low narrow data part of the FIFO.
-- . On the wide side the empty field applies to the number of empty narrow
--   data symbols in the wide data. On the narrow side the empty field is not
--   used. Without empty on the wide side each frame needs to have an integer
--   multiple of c_nof_narrow narrow data words.
-- . In case of a narrow to wide FIFO a new wide data becomes available when
--   all narrow data for a wide data have been written.
-- . Within the narrow stream the empty, channel and error fields are not
--   supported. If necessary these need to be passed on to or from the wide
--   side seperately.
-- . The g_wr_data_w and g_rd_data_w must be an integer multiple of each other.
-- . Default the RL=1, use g_fifo_rl=0 for a the show ahead FIFO.
--
-- Warning !!!
-- . It appears that the stratix4 architecture of common_fifo_mixed_widths.vhd
--   does not work OK when g_nof_narrow is not a power of 2. After that
--   g_wr_fifo_size have been written to the FIFO the rdusedw will wrap and the
--   output goes wrong.
--
-- Development steps:
-- 1 Decide to use a mixed width FIFO and to pass on the sop and eop with each
--   narrow data in the FIFO.
-- . made a mixed width FIFO in common_lib with a tb test bench.
-- . decide to increase c_dp_stream_data_w to 256 (see dp_stream_pkg for more
--   explanation)
-- . note that the FIFO is little endian whereas the SOSI is big endian
-- 2 First get the mixed width streaming FIFO working for without padding, so
--   for integer multiple of c_nof_narrow data, for both framed data with sop
--   and eop control and for unframed data with only valid control.
-- . let the test bench verify both the wide data and the narrow data by using
--   two instances of dp_fifo_dc_mixed_widths, one from narrow to wide and one
--   from wide to narrow with a latency adapter in between to be able to also
--   verify g_rd_fifo_rl=0
-- . simulate with tb test bench, first with src_in.ready='1' and snk_in.valid
--   ='1', then with random control, make corrections and fine adjustments
-- 3 Then add padding at narrow write side and wide empty field to support
--   framed data with not an integer number of narrow data (= symbols) per wide
--   data frame.
-- . draw timing diagram of snk_in.valid and draw the required wr_pad_val to
--   fill the gap dependent on snk_in.eop
-- . add narrow data word counter called wr_pad_cnt that counts on snk_in.valid
--   or wr_pad_val
-- . derive a pad request signal for wr_pad_val that has RL=1, the NOT of this
--   pad request is wr_pad_ready
-- . apply wr_pad_val with snk_in.valid to fifo_wr_req
-- . apply wr_pad_ready with src_in.ready to snk_out.ready
-- . inititialise wr_pad_val='0' and wr_pad_ready='1' so that for all other
--   generate cases they are redundant
-- . use the wide SOSI empty field to represent the padded narrow data (these
--   are the symbols in the wide data)
-- . simulate with tb test bench, make corrections and fine adjustments
-- 4 Simulate with multi tb-tb test bench for regression tests
-- . add this multi tb-tb test bench to tb_tb_tb_dp_backpressure.vhd

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_dc_mixed_widths is
  generic (
    g_technology        : natural := c_tech_select_default;
    g_wr_data_w         : natural := 18;
    g_rd_data_w         : natural := 9;
    g_use_ctrl          : boolean := true;
    g_wr_fifo_size      : natural := 512;  -- FIFO size in nof wr_data words
    g_wr_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_wr_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_rd_fifo_rl        : natural := 1
  );
  port (
    wr_rst         : in  std_logic;
    wr_clk         : in  std_logic;
    rd_rst         : in  std_logic;
    rd_clk         : in  std_logic;
    -- Monitor FIFO filling
    wr_ful         : out std_logic;  -- corresponds to the carry bit of wr_usedw when FIFO is full
    wr_usedw       : out std_logic_vector(ceil_log2(g_wr_fifo_size) - 1 downto 0);
    rd_usedw       : out std_logic_vector(ceil_log2(g_wr_fifo_size * g_wr_data_w / g_rd_data_w) - 1 downto 0);
    rd_emp         : out std_logic;
    -- ST sink
    snk_out        : out t_dp_siso := c_dp_siso_rdy;
    snk_in         : in  t_dp_sosi;
    -- ST source
    src_in         : in  t_dp_siso := c_dp_siso_rdy;
    src_out        : out t_dp_sosi
  );
end dp_fifo_dc_mixed_widths;

architecture str of dp_fifo_dc_mixed_widths is
  constant c_wr_wide             : boolean := sel_a_b(g_wr_data_w > g_rd_data_w, true, false);
  constant c_nof_narrow          : natural := largest(g_wr_data_w / g_rd_data_w, g_rd_data_w / g_wr_data_w);
  constant c_nof_narrow_wr       : natural := sel_a_b(c_wr_wide, c_nof_narrow, 1);
  constant c_nof_narrow_rd       : natural := sel_a_b(c_wr_wide, 1, c_nof_narrow);

  constant c_narrow_data_w       : natural := sel_a_b(g_wr_data_w > g_rd_data_w, g_rd_data_w, g_wr_data_w);

  constant c_use_data            : boolean := true;
  constant c_ctrl_w              : natural := sel_a_b(g_use_ctrl, 2, 0);  -- sop and eop, or no ctrl
  constant c_empty_w             : natural := ceil_log2(c_nof_narrow);

  constant c_fifo_wr_almost_full : natural := g_wr_fifo_size - g_wr_fifo_af_margin;  -- FIFO almost full level for snk_out.ready
  constant c_fifo_wr_almost_xon  : natural := g_wr_fifo_size - g_wr_fifo_af_xon;  -- FIFO almost full level for snk_out.xon

  constant c_fifo_narrow_data_w  : natural := c_narrow_data_w + c_ctrl_w;  -- if used concat control via FIFO
  constant c_fifo_wide_data_w    : natural := c_nof_narrow * c_fifo_narrow_data_w;  -- all through one FIFO

  constant c_fifo_wr_dat_w       : natural := sel_a_b(c_wr_wide, c_fifo_wide_data_w,   c_fifo_narrow_data_w);
  constant c_fifo_rd_dat_w       : natural := sel_a_b(c_wr_wide, c_fifo_narrow_data_w, c_fifo_wide_data_w);

  signal arst           : std_logic;
  signal i_wr_usedw     : std_logic_vector(wr_usedw'range);

  signal fifo_wr_dat    : std_logic_vector(c_fifo_wr_dat_w - 1 downto 0);
  signal fifo_wr_req    : std_logic;
  signal fifo_wr_ful    : std_logic;
  signal fifo_aful      : std_logic;
  signal nxt_fifo_aful  : std_logic;

  signal fifo_rd_dat    : std_logic_vector(c_fifo_rd_dat_w - 1 downto 0) := (others => '0');
  signal fifo_rd_val    : std_logic;
  signal fifo_rd_req    : std_logic;

  signal wr_pad_cnt     : natural range 0 to c_nof_narrow - 1;
  signal nxt_wr_pad_cnt : natural;
  signal wr_pad_val     : std_logic := '0';
  signal nxt_wr_pad_val : std_logic;
  signal wr_pad_ready   : std_logic := '1';
  signal rd_eop_hld     : std_logic := '0';
  signal nxt_rd_eop_hld : std_logic;
  signal rd_gap_val     : std_logic;

  signal rd_siso        : t_dp_siso;
  signal rd_sosi        : t_dp_sosi := c_dp_sosi_rst;  -- initialize default values for unused sosi fields

  signal i_snk_out      : t_dp_siso := c_dp_siso_rst;
  signal nxt_snk_out    : t_dp_siso := c_dp_siso_rst;
begin
  -- Use i_snk_out with defaults to force unused snk_out bits and fields to '0'
  snk_out <= i_snk_out;

  -- FIFO generate control:
  --   if c_nof_narrow = 1 then   generate FIFO equal width (= dp_fifo_dc)
  --   else
  --     if c_wr_wide = TRUE then generate FIFO write wide   --> read narrow (uses common_fifo_dc_mixed_widths)
  --     else                     generate FIFO write narrow --> read wide   (uses common_fifo_dc_mixed_widths)

  gen_equal : if c_nof_narrow = 1 generate  -- fall back to equal width FIFO
    u_dp_fifo_dc : entity work.dp_fifo_dc
    generic map (
      g_technology     => g_technology,
      g_data_w         => g_wr_data_w,
      g_empty_w        => 1,
      g_channel_w      => 1,
      g_error_w        => 1,
      g_use_empty      => false,
      g_use_channel    => false,
      g_use_error      => false,
      g_use_ctrl       => g_use_ctrl,
      g_fifo_size      => g_wr_fifo_size,
      g_fifo_af_margin => g_wr_fifo_af_margin,
      g_fifo_rl        => g_rd_fifo_rl
    )
    port map (
      wr_rst      => wr_rst,
      wr_clk      => wr_clk,
      rd_rst      => rd_rst,
      rd_clk      => rd_clk,
      -- ST sink
      snk_out     => i_snk_out,
      snk_in      => snk_in,
      -- Monitor FIFO filling
      wr_ful      => wr_ful,
      wr_usedw    => wr_usedw,
      rd_usedw    => rd_usedw,
      rd_emp      => rd_emp,
      -- ST source
      src_in      => src_in,
      src_out     => src_out
    );
  end generate;  -- gen_equal

  gen_mixed : if c_nof_narrow > 1 generate  -- mixed width FIFO

    arst <= wr_rst or rd_rst;

    wr_ful   <= fifo_wr_ful;
    wr_usedw <= i_wr_usedw;

    nxt_fifo_aful <= '0' when (unsigned(i_wr_usedw) < c_fifo_wr_almost_full) else '1';

    p_wr_clk: process(wr_clk, wr_rst)
    begin
      if wr_rst = '1' then
        fifo_aful     <= '0';
        i_snk_out.xon <= '0';
      elsif rising_edge(wr_clk) then
        fifo_aful     <= nxt_fifo_aful;
        i_snk_out.xon <= nxt_snk_out.xon;
      end if;
    end process;

    -- Padding is only needed for the narrow write with frame control, in all other cases the padding control defaults to the redundant initialisation values

    -- pass on frame level flow control from src_in.xon to upstream snk_out.xon, and
    -- add flow contol dependent on whether the fifo can fit another block
    nxt_snk_out.xon <= src_in.xon when unsigned(i_wr_usedw) <= c_fifo_wr_almost_xon else '0';

    -- use FIFO almost full and no padding going on to control output ready to up stream
    i_snk_out.ready <= (not fifo_aful) and wr_pad_ready;

    -- write to the FIFO when there is valid input from the sink or when there is padding going on
    fifo_wr_req <= snk_in.valid or wr_pad_val;

    -- read request from the FIFO when the source is ready
    fifo_rd_req <= rd_siso.ready;

    u_fifo_mw : entity common_lib.common_fifo_dc_mixed_widths
    generic map (
      g_technology => g_technology,
      g_nof_words => g_wr_fifo_size,  -- FIFO size in nof wr_dat words
      g_wr_dat_w  => c_fifo_wr_dat_w,
      g_rd_dat_w  => c_fifo_rd_dat_w
    )
    port map (
      rst     => arst,
      wr_clk  => wr_clk,
      wr_dat  => fifo_wr_dat,
      wr_req  => fifo_wr_req,
      wr_ful  => fifo_wr_ful,
      wrusedw => i_wr_usedw,
      rd_clk  => rd_clk,
      rd_dat  => fifo_rd_dat,
      rd_req  => fifo_rd_req,
      rd_emp  => rd_emp,
      rdusedw => rd_usedw,
      rd_val  => fifo_rd_val
    );

    -- FIFO write multiple parallel --> read one serial
    gen_wr_wide : if c_wr_wide = true generate
      gen_ctrl : if g_use_ctrl = true generate
        -- Write the wide SOSI
        p_fifo_wr_dat : process(snk_in)
          variable vB : natural;
        begin
          -- Map big endian wide SOSI (index vB) to little endian wide FIFO (index I) and the ctrl fields
          fifo_wr_dat <= (others => '0');
          for I in 0 to c_nof_narrow - 1 loop
            vB := c_nof_narrow - I - 1;
            fifo_wr_dat((I + 1) * c_fifo_narrow_data_w - 1 downto I * c_fifo_narrow_data_w + 2) <= snk_in.data((vB + 1) * c_narrow_data_w - 1 downto vB * c_narrow_data_w);  -- c_ctrl_w = 2
            if vB = c_nof_narrow - 1 then
              fifo_wr_dat(I * c_fifo_narrow_data_w + 1) <= snk_in.sop;
            end if;
            -- Padding support: eop = '1' when empty=vB
            if vB = TO_UINT(snk_in.empty(c_empty_w - 1 downto 0)) then
              fifo_wr_dat(I * c_fifo_narrow_data_w + 0) <= snk_in.eop;
            end if;
          end loop;
        end process;

        -- Read the narrow SOSI
        rd_sosi.data  <= RESIZE_DP_DATA(fifo_rd_dat(c_fifo_narrow_data_w - 1 downto 2));  -- c_ctrl_w = 2
        rd_sosi.valid <= fifo_rd_val and not rd_gap_val;
        rd_sosi.sop   <= fifo_rd_val and fifo_rd_dat(1);
        rd_sosi.eop   <= fifo_rd_val and fifo_rd_dat(0);

        -- Read padding control in case the nof narrow data is not an integer multiple of c_nof_narrow
        p_rd_pad_clk: process(rd_clk, rd_rst)
        begin
          if rd_rst = '1' then
            rd_eop_hld <= '0';
          elsif rising_edge(rd_clk) then
            rd_eop_hld <= nxt_rd_eop_hld;
          end if;
        end process;

        p_rd_pad_ctrl : process(rd_eop_hld, fifo_rd_dat)
        begin
          -- count narrow data
          nxt_rd_eop_hld <= rd_eop_hld;
          if fifo_rd_dat(1) = '1' then
            nxt_rd_eop_hld <= '0';  -- at next frame sop
          elsif fifo_rd_dat(0) = '1' then
            nxt_rd_eop_hld <= '1';  -- at this frame eop
          end if;
        end process;

        rd_gap_val <= rd_eop_hld and not fifo_rd_dat(1);  -- valid during gap between this frame eop and next frame sop
      end generate;

      no_ctrl : if g_use_ctrl = false generate
        -- Write the wide SOSI
        p_fifo_wr_dat : process(snk_in)
          variable vB : natural;
        begin
          -- Map big endian wide SOSI (index vB) to little endian wide FIFO (index I)
          for I in 0 to c_nof_narrow - 1 loop
            vB := c_nof_narrow - I - 1;
            fifo_wr_dat((I + 1) * c_narrow_data_w - 1 downto I * c_narrow_data_w) <= snk_in.data((vB + 1) * c_narrow_data_w - 1 downto vB * c_narrow_data_w);
          end loop;
        end process;

        -- Read the narrow SOSI
        rd_sosi.data  <= RESIZE_DP_DATA(fifo_rd_dat(c_narrow_data_w - 1 downto 0));
        rd_sosi.valid <= fifo_rd_val;
      end generate;

    end generate;  -- gen_wr_wide

    -- FIFO write one serial --> read multiple parallel
    gen_rd_wide : if c_wr_wide = false generate
      gen_ctrl : if g_use_ctrl = true generate
        -- Write the narrow SOSI
        fifo_wr_dat <= snk_in.data(c_narrow_data_w - 1 downto 0) & snk_in.sop & snk_in.eop;

        -- Write-padding control in case the nof narrow data is not an integer multiple of c_nof_narrow
        p_wr_pad_clk: process(wr_clk, wr_rst)
        begin
          if wr_rst = '1' then
            wr_pad_cnt <= 0;
            wr_pad_val <= '0';
          elsif rising_edge(wr_clk) then
            wr_pad_cnt <= nxt_wr_pad_cnt;
            wr_pad_val <= nxt_wr_pad_val;
          end if;
        end process;

        p_wr_pad_ctrl : process(wr_pad_cnt, snk_in, wr_pad_val)
        begin
          -- count narrow data
          nxt_wr_pad_cnt <= wr_pad_cnt;
          if snk_in.valid = '1' or wr_pad_val = '1' then
            nxt_wr_pad_cnt <= 0;
            if wr_pad_cnt < c_nof_narrow - 1 then
              nxt_wr_pad_cnt <= wr_pad_cnt + 1;
            end if;
          end if;
          -- if necessary insert padding after eop
          nxt_wr_pad_val <= '0';
          if wr_pad_cnt < c_nof_narrow - 1 then
            nxt_wr_pad_val <= wr_pad_val;
            if snk_in.eop = '1' then
              nxt_wr_pad_val <= '1';
            end if;
          end if;
        end process;

        wr_pad_ready <= '0' when (snk_in.eop = '1' or wr_pad_val = '1') and (wr_pad_cnt < c_nof_narrow - 1) else '1';

        -- Read the wide SOSI
        p_rd_sosi : process(fifo_rd_dat, fifo_rd_val)
          variable vB     : natural;
          variable v_sosi : t_dp_sosi;
        begin
          v_sosi       := c_dp_sosi_rst;  -- must init variable here, init at declaration only causes a combinatorial loop
          v_sosi.valid := fifo_rd_val;
          for I in 0 to c_nof_narrow - 1 loop
            -- Map little endian wide FIFO (index I) to big endian wide SOSI (index vB)
            vB := c_nof_narrow - I - 1;
            v_sosi.data((vB + 1) * c_narrow_data_w - 1 downto vB * c_narrow_data_w) := fifo_rd_dat((I + 1) * c_fifo_narrow_data_w - 1 downto I * c_fifo_narrow_data_w + 2);  -- c_ctrl_w = 2
            -- Or the control fields: sop and eop
            v_sosi.sop := v_sosi.sop or fifo_rd_dat(I * c_fifo_narrow_data_w + 1);
            v_sosi.eop := v_sosi.eop or fifo_rd_dat(I * c_fifo_narrow_data_w + 0);
            -- Padding support: empty = vB when eop='1'
            if fifo_rd_dat(I * c_fifo_narrow_data_w + 0) = '1' then
              v_sosi.empty := TO_DP_EMPTY(vB);
            end if;
          end loop;
          v_sosi.sop := v_sosi.valid and v_sosi.sop;
          v_sosi.eop := v_sosi.valid and v_sosi.eop;
          rd_sosi <= v_sosi;
        end process;
      end generate;

      no_ctrl : if g_use_ctrl = false generate
        -- Write the narrow SOSI
        fifo_wr_dat <= snk_in.data(c_narrow_data_w - 1 downto 0);
        -- Read the wide SOSI
        p_rd_sosi : process(fifo_rd_dat, fifo_rd_val)
          variable vB     : natural;
          variable v_sosi : t_dp_sosi;
        begin
          v_sosi       := c_dp_sosi_rst;  -- must init variable here, init at declaration only causes a combinatorial loop
          v_sosi.valid := fifo_rd_val;
          for I in 0 to c_nof_narrow - 1 loop
            -- Map little endian wide FIFO (index I) to big endian wide SOSI (index vB)
            vB := c_nof_narrow - I - 1;
            v_sosi.data((vB + 1) * c_narrow_data_w - 1 downto vB * c_narrow_data_w) := fifo_rd_dat((I + 1) * c_narrow_data_w - 1 downto I * c_narrow_data_w);
          end loop;
          rd_sosi <= v_sosi;
        end process;
      end generate;
    end generate;  -- gen_rd_wide

    -- Support show ahead FIFO with ready latency = 0 at read output
    u_rl : entity work.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => g_rd_fifo_rl
    )
    port map (
      rst       => rd_rst,
      clk       => rd_clk,
      -- ST sink
      snk_out   => rd_siso,
      snk_in    => rd_sosi,
      -- ST source
      src_in    => src_in,
      src_out   => src_out
    );
  end generate;  -- gen_mixed
end str;
