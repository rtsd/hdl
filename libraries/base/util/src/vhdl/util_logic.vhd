-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Dummy function to use logic (LUTs and FF)
-- Description:
--   There are g_nof_reg >= 0 register stages in series. Each register stage
--   does outp := outp XOR inp to use LUTs and FF in an FPGA.
-- Remark:
-- . Using common_pipeline.vhd to invoke logic can get implemented in RAM
--   blocks for larger pipeline settings. Therefor this util_logic puts a
--   feedback with XOR at every stage to enforce using logic (LUTs and FF).

library IEEE;
use IEEE.std_logic_1164.all;

entity util_logic is
  generic (
    g_nof_reg  : natural := 1  -- 0 for wires, > 0 for registers
  );
  port (
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    in_dat  : in  std_logic_vector;
    out_dat : out std_logic_vector
  );
end util_logic;

architecture rtl of util_logic is
  constant c_reset_value : std_logic_vector(out_dat'range) := (others => '0');

  type t_out_dat is array (natural range <>) of std_logic_vector(out_dat'range);

  signal out_dat_reg     : t_out_dat(0 to g_nof_reg) := (others => c_reset_value);
  signal nxt_out_dat_reg : t_out_dat(0 to g_nof_reg);
begin
  gen_reg : for I in 1 to g_nof_reg generate
    p_clk : process(clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          for J in out_dat'range loop
            out_dat_reg(I)(J) <= out_dat_reg(I)(J) xor out_dat_reg(I - 1)(J);
          end loop;
        end if;
      end if;
    end process;
  end generate;

  -- Implement out_dat_reg(0) <= in_dat in a for loop that checks for initial
  -- 'X' in in_dat, because in simulation these 'X' will propagate forever
  -- due to the XOR feedback function.
  p_in_dat : process(in_dat)
  begin
    for J in out_dat'range loop
      out_dat_reg(0)(J) <= '0';
      if in_dat(J) = '0' or in_dat(J) = '1' then
        out_dat_reg(0)(J) <= in_dat(J);
      end if;
    end loop;
  end process;

  out_dat <= out_dat_reg(g_nof_reg);
end rtl;
