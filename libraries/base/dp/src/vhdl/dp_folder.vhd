--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Status:
-- . Active (recommended for new designs)
-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Fold n input streams into n/2, n/2/2, n/2/2/2, .. output streams
-- Description:
-- . This component assumes the user has scheduled the input streams properly.
--   . Input data that is to be folded onto one stream must not be valid at the same clock cycle
-- . nof_outputs = ceil_div(g_nof_inputs, 2^(g_nof_folds)) for g_nof_folds>=0
--   . Examples:
--     . g_nof_inputs=10, g_nof_folds=0 -> nof_outputs=10
--     . g_nof_inputs=10, g_nof_folds=1 -> nof_outputs= 5
--     . g_nof_inputs=10, g_nof_folds=2 -> nof_outputs= 3
--     . g_nof_inputs=10, g_nof_folds=3 -> nof_outputs= 2
--     . g_nof_inputs=10, g_nof_folds=4 -> nof_outputs= 1
--     . g_nof_inputs=10, g_nof_folds<0 -> nof_outputs= 1
-- . This entity recursively instantiates (registered) stages of itself when folding streams multiple times.
--
-- Remark:
-- . Use dp_deinterleave_n_to_one.vhd when:
--   - for 1 to N, so any N >= 1 (so not only powers of 2)
--   - when all sosi fields need to be preserved (so not only the valid)
--   - if flow control is needed

entity dp_folder is
  generic (
    g_nof_inputs        : natural;  -- Number of inputs to fold >0
    g_nof_folds         : integer := -1;  -- >0: Number of folds; 0: Wire out to in; <0: Fold until one output remains
    g_output_block_size : natural := 0;  -- >0: Create SOP/EOP tagged output blocks of this size. 0: Forward incoming SOP,EOP.
    g_fwd_sync_bsn      : boolean := false;  -- TRUE: forwards (stored) input Sync+BSN (from snk_in_arr(0)) to all output streams
    g_use_channel       : boolean := false  -- TRUE: Also fold the channel field
  );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    src_out_arr    : out t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0)
  );
end dp_folder;

architecture str of dp_folder is
  component dp_folder is
    generic (
      g_nof_inputs        : natural;
      g_nof_folds         : integer := -1;
      g_output_block_size : natural := 0;
      g_fwd_sync_bsn      : boolean := false;
      g_use_channel       : boolean := false
    );
    port (
      rst            : in  std_logic;
      clk            : in  std_logic;

      snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

      src_out_arr    : out t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0)
    );
  end component;

  constant c_nof_muxes : natural := ceil_div(g_nof_inputs, 2);  -- Using ceil_div always yields an even number of mux inputs

  signal mux_snk_in_arr      : t_dp_sosi_arr(2 * c_nof_muxes - 1 downto 0);
  signal mux_snk_in_2arr_2   : t_dp_sosi_2arr_2(c_nof_muxes - 1 downto 0);

  signal mux_src_out_arr     : t_dp_sosi_arr(c_nof_muxes - 1 downto 0);
  signal nxt_mux_src_out_arr : t_dp_sosi_arr(c_nof_muxes - 1 downto 0);

  signal dp_folder_src_out_arr    : t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0);

  signal dp_block_gen_snk_in_arr  : t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0);
  signal dp_block_gen_src_out_arr : t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0);
begin
  gen_arch: if g_nof_folds /= 0 generate
    -----------------------------------------------------------------------------
    -- Wire input array to mux_snk_in_arr to make sure we have an even number
    -- of buses to work with in case of an odd number of inputs
    -- . We want an even number of buses because we will wire up 2-input muxes
    -----------------------------------------------------------------------------
    gen_even_nof_buses: for i in 0 to g_nof_inputs - 1 generate
      mux_snk_in_arr(i) <= snk_in_arr(i);
    end generate;

    -----------------------------------------------------------------------------
    -- Wire inputs to the 2-input muxes
    -----------------------------------------------------------------------------
    gen_mux_inputs_0: for i in 0 to c_nof_muxes - 1 generate
      mux_snk_in_2arr_2(i)(0) <= mux_snk_in_arr(2 * i);
      mux_snk_in_2arr_2(i)(1) <= mux_snk_in_arr(2 * i + 1);
    end generate;

    -----------------------------------------------------------------------------
    -- Simple 2-input mux logic
    -----------------------------------------------------------------------------
    p_mux : process(mux_snk_in_2arr_2)
    begin
      for i in 0 to c_nof_muxes - 1 loop
        nxt_mux_src_out_arr(i) <= c_dp_sosi_rst;
        if mux_snk_in_2arr_2(i)(0).valid = '1' then
          nxt_mux_src_out_arr(i).data  <= mux_snk_in_2arr_2(i)(0).data;
          nxt_mux_src_out_arr(i).re    <= mux_snk_in_2arr_2(i)(0).re;
          nxt_mux_src_out_arr(i).im    <= mux_snk_in_2arr_2(i)(0).im;
          nxt_mux_src_out_arr(i).valid <= '1';
          if g_use_channel = true then
            nxt_mux_src_out_arr(i).channel <= mux_snk_in_2arr_2(i)(0).channel;
          end if;
        elsif mux_snk_in_2arr_2(i)(1).valid = '1' then
          nxt_mux_src_out_arr(i).data  <= mux_snk_in_2arr_2(i)(1).data;
          nxt_mux_src_out_arr(i).re    <= mux_snk_in_2arr_2(i)(1).re;
          nxt_mux_src_out_arr(i).im    <= mux_snk_in_2arr_2(i)(1).im;
          nxt_mux_src_out_arr(i).valid <= '1';
          if g_use_channel = true then
            nxt_mux_src_out_arr(i).channel <= mux_snk_in_2arr_2(i)(1).channel;
          end if;
        end if;
      end loop;
    end process;

    -- Registers
    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        mux_src_out_arr <= (others => c_dp_sosi_rst);
      elsif rising_edge(clk) then
        mux_src_out_arr <= nxt_mux_src_out_arr;
      end if;
    end process;

    -----------------------------------------------------------------------------
    -- Not done folding; add a stage.
    -- . g_nof_folds <0 : user wants to fold all the way to one output
    -- . g_nof_folds >0 : user wants to fold n times
    -----------------------------------------------------------------------------
    gen_dp_folder: if (g_nof_folds < 0 and c_nof_muxes > 1) or g_nof_folds > 1 generate
      u_dp_folder : dp_folder
      generic map (
        g_nof_inputs        => c_nof_muxes,
        g_nof_folds         => g_nof_folds - 1,
        g_output_block_size => g_output_block_size,
        g_fwd_sync_bsn      => g_fwd_sync_bsn,
        g_use_channel       => g_use_channel
      )
      port map (
        rst         => rst,
        clk         => clk,

        snk_in_arr  => mux_src_out_arr,
        src_out_arr => dp_folder_src_out_arr
      );

      src_out_arr <= dp_folder_src_out_arr;
    end generate;

    -----------------------------------------------------------------------------
    -- c_nof_muxes=1 or g_nof_folds=1, so this is the last stage.
    -----------------------------------------------------------------------------
    gen_src_out_arr: if (g_nof_folds < 0 and c_nof_muxes = 1) or g_nof_folds = 1 generate
      dp_block_gen_snk_in_arr <= mux_src_out_arr;

      -----------------------------------------------------------------------------
      -- Add SOP and EOP to the outputs
      -----------------------------------------------------------------------------
      gen_ctrl : if g_output_block_size > 0 generate
        gen_dp_block_gen : for i in 0 to c_nof_muxes - 1 generate
          u_dp_block_gen : entity work.dp_block_gen
          generic map (
            g_use_src_in       => false,
            g_nof_data         => g_output_block_size,
            g_preserve_sync    => true,
            g_preserve_bsn     => true,
            g_preserve_channel => g_use_channel
          )
          port map(
            rst        => rst,
            clk        => clk,

            snk_in     => dp_block_gen_snk_in_arr(i),
            src_out    => dp_block_gen_src_out_arr(i)
          );
        end generate;
      end generate;

      no_ctrl : if g_output_block_size = 0 generate
        dp_block_gen_src_out_arr <= dp_block_gen_snk_in_arr;
      end generate;

      -----------------------------------------------------------------------------
      -- Re-add input sync + BSN to all output streams
      -----------------------------------------------------------------------------
      gen_sync_bsn : if g_fwd_sync_bsn = true generate
        gen_dp_fifo_info: for i in 0 to c_nof_muxes - 1 generate
          u_dp_fifo_info : entity work.dp_fifo_info
          generic map (
            g_use_sync => true,
            g_use_bsn  => true
          )
          port map (
            rst          => rst,
            clk          => clk,

            data_snk_in  => dp_block_gen_src_out_arr(i),  -- delayed snk_in data
            info_snk_in  => snk_in_arr(0),  -- original snk_in info

            src_in       => c_dp_siso_rdy,
            src_out      => src_out_arr(i)
          );
        end generate;
      end generate;

      no_sync_bsn : if g_fwd_sync_bsn = false generate
        src_out_arr <= dp_block_gen_src_out_arr;
      end generate;
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- Wire output to input if g_nof_folds=0
  -----------------------------------------------------------------------------
  gen_wire_out_to_in: if g_nof_folds = 0 generate
    dp_block_gen_snk_in_arr <= snk_in_arr;
  end generate;
end str;
