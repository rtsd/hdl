#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

vmap  altera_jesd204_rx_180           ./work/

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_jesd204b_rx_200MHz/sim"
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_base.v"                -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_csr.v"                 -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_ctl.v"                 -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_descrambler.v"         -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_char_val.v"        -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_cs.v"              -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_data_store.v"      -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_ecc_dec.v"         -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_ecc_enc.v"         -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_ecc_fifo.v"        -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_frame_align.v"     -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_fs_char_replace.v" -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_lane_align.v"      -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll.v"                 -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_dll_wo_ecc_fifo.v"     -work altera_jesd204_rx_180   
  vlog         "$IP_DIR/../altera_jesd204_rx_180/sim/mentor/altera_jesd204_rx_regmap.v"              -work altera_jesd204_rx_180   
 
