library IEEE, pfs_lib;
use IEEE.std_logic_1164.all;
library common_lib;
use common_lib.common_pkg.all;

architecture str of pfs is
  constant c_nof_bands_w    : natural := ceil_log2(g_nof_bands);
  constant c_nof_fir_taps   : natural := g_nof_taps / g_nof_bands;
  constant c_nof_fir_taps_w : natural := ceil_log2(c_nof_fir_taps);

  signal taps_rdaddr        : std_logic_vector(c_nof_bands_w - 1 downto 0);
  signal taps_wraddr        : std_logic_vector(c_nof_bands_w - 1 downto 0);
  signal taps_wren          : std_logic;
  signal taps_in_x          : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_in_y          : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_out_x         : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal taps_out_y         : std_logic_vector(g_in_dat_w * c_nof_fir_taps - 1 downto 0);
  signal coefs              : std_logic_vector(g_coef_dat_w * c_nof_fir_taps - 1 downto 0);
begin
  ctrl : entity pfs_lib.pfs_ctrl
  generic map (
    g_nof_bands_w     => c_nof_bands_w,
    g_nof_taps        => c_nof_fir_taps,
    g_nof_taps_w      => c_nof_fir_taps_w,
    g_taps_w          => g_in_dat_w
  )
  port map (
    clk               => clk,
    rst               => rst,
    restart           => restart,
    in_x              => in_dat_x,
    in_y              => in_dat_y,
    in_val            => in_val,
    in_sync           => in_sync,
    taps_rdaddr       => taps_rdaddr,
    taps_wraddr       => taps_wraddr,
    taps_wren         => taps_wren,
    taps_in_x         => taps_in_x,
    taps_in_y         => taps_in_y,
    taps_out_x        => taps_out_x,
    taps_out_y        => taps_out_y,
    out_val           => out_val,
    out_sync          => out_sync
  );

  firx : entity pfs_lib.pfs_filter
  generic map (
    g_coef_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => g_in_dat_w,
    g_nof_taps        => c_nof_fir_taps
  )
  port map(
    clk               => clk,
    taps              => taps_out_x,
    coefs             => coefs,
    result            => out_dat_x
  );

  firy : entity pfs_lib.pfs_filter
  generic map (
    g_coef_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => g_in_dat_w,
    g_nof_taps        => c_nof_fir_taps
  )
  port map (
    clk               => clk,
    taps              => taps_out_y,
    coefs             => coefs,
    result            => out_dat_y
  );

  tapsbufx : entity pfs_lib.pfs_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w * c_nof_fir_taps,
    g_nof_words       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    wrdata            => taps_out_x,
    wren              => taps_wren,
    wraddr            => taps_wraddr,
    rdaddr            => taps_rdaddr,
    rddata            => taps_in_x,
    clk               => clk,
    rst               => rst
  );

  tapsbufy : entity pfs_lib.pfs_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w * c_nof_fir_taps,
    g_nof_words       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    wrdata            => taps_out_y,
    wren              => taps_wren,
    wraddr            => taps_wraddr,
    rdaddr            => taps_rdaddr,
    rddata            => taps_in_y,
    clk               => clk,
    rst               => rst
  );

  coefsbuf : entity pfs_lib.pfs_coefsbuf
  generic map (
    g_data_w          => g_coef_dat_w * c_nof_fir_taps,
    g_nof_coefs       => g_nof_bands,
    g_addr_w          => c_nof_bands_w
  )
  port map (
    addr              => taps_rdaddr,
    data              => coefs,
    clk               => clk,
    rst               => rst
  );
end str;
