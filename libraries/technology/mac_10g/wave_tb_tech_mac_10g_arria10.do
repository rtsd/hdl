onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/csr_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/csr_miso
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/mm_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/tx_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/rx_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -expand -group gen_ip_arria10 -expand -group u0 -radix unsigned -childformat {{/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.sync -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.bsn -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.data -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.re -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.im -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.valid -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.sop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.eop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.empty -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.channel -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.err -radix unsigned}} -expand -subitemconfig {/tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.sync {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.bsn {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.data {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.re {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.im {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.valid {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.sop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.eop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.empty {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.channel {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out.err {-radix unsigned}} /tb_tech_mac_10g/gen_dut/dut/gen_ip_arria10/u0/avalon_rx_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/csr_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/csr_miso
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix hexadecimal /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned -childformat {{/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.sync -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.bsn -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.data -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.re -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.im -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.valid -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.sop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.eop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.empty -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.channel -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.err -radix unsigned}} -expand -subitemconfig {/tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.sync {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.bsn {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.data {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.re {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.im {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.valid {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.sop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.eop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.empty {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.channel {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in.err {-radix unsigned}} /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_remove_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_remove_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix hexadecimal /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out_data
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned -childformat {{/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.sync -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.bsn -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.data -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.re -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.im -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.valid -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.sop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.eop -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.empty -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.channel -radix unsigned} {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.err -radix unsigned}} -expand -subitemconfig {/tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.sync {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.bsn {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.data {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.re {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.im {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.valid {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.sop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.eop {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.empty {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.channel {-radix unsigned} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out.err {-radix unsigned}} /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out_rl1
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_in_rl1
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_fifo_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_fifo_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_insert_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_insert_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -expand -group gen_dut -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/total_header
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tb_end
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_ref_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_ref_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_phy_clk_2x
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_phy_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_init
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/mm_mosi_wrdata
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_miso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_miso_rdval
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/mm_miso_rddata
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_en
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_siso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned -childformat {{/tb_tech_mac_10g/tx_sosi.sync -radix unsigned} {/tb_tech_mac_10g/tx_sosi.bsn -radix unsigned} {/tb_tech_mac_10g/tx_sosi.data -radix unsigned} {/tb_tech_mac_10g/tx_sosi.re -radix unsigned} {/tb_tech_mac_10g/tx_sosi.im -radix unsigned} {/tb_tech_mac_10g/tx_sosi.valid -radix unsigned} {/tb_tech_mac_10g/tx_sosi.sop -radix unsigned} {/tb_tech_mac_10g/tx_sosi.eop -radix unsigned} {/tb_tech_mac_10g/tx_sosi.empty -radix unsigned} {/tb_tech_mac_10g/tx_sosi.channel -radix unsigned} {/tb_tech_mac_10g/tx_sosi.err -radix unsigned}} -expand -subitemconfig {/tb_tech_mac_10g/tx_sosi.sync {-radix unsigned} /tb_tech_mac_10g/tx_sosi.bsn {-radix unsigned} /tb_tech_mac_10g/tx_sosi.data {-radix unsigned} /tb_tech_mac_10g/tx_sosi.re {-radix unsigned} /tb_tech_mac_10g/tx_sosi.im {-radix unsigned} /tb_tech_mac_10g/tx_sosi.valid {-radix unsigned} /tb_tech_mac_10g/tx_sosi.sop {-radix unsigned} /tb_tech_mac_10g/tx_sosi.eop {-radix unsigned} /tb_tech_mac_10g/tx_sosi.empty {-radix unsigned} /tb_tech_mac_10g/tx_sosi.channel {-radix unsigned} /tb_tech_mac_10g/tx_sosi.err {-radix unsigned}} /tb_tech_mac_10g/tx_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/tx_sosi_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_siso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned -childformat {{/tb_tech_mac_10g/rx_sosi.sync -radix unsigned} {/tb_tech_mac_10g/rx_sosi.bsn -radix unsigned} {/tb_tech_mac_10g/rx_sosi.data -radix unsigned} {/tb_tech_mac_10g/rx_sosi.re -radix unsigned} {/tb_tech_mac_10g/rx_sosi.im -radix unsigned} {/tb_tech_mac_10g/rx_sosi.valid -radix unsigned} {/tb_tech_mac_10g/rx_sosi.sop -radix unsigned} {/tb_tech_mac_10g/rx_sosi.eop -radix unsigned} {/tb_tech_mac_10g/rx_sosi.empty -radix unsigned} {/tb_tech_mac_10g/rx_sosi.channel -radix unsigned} {/tb_tech_mac_10g/rx_sosi.err -radix unsigned}} -expand -subitemconfig {/tb_tech_mac_10g/rx_sosi.sync {-radix unsigned} /tb_tech_mac_10g/rx_sosi.bsn {-radix unsigned} /tb_tech_mac_10g/rx_sosi.data {-radix unsigned} /tb_tech_mac_10g/rx_sosi.re {-radix unsigned} /tb_tech_mac_10g/rx_sosi.im {-radix unsigned} /tb_tech_mac_10g/rx_sosi.valid {-radix unsigned} /tb_tech_mac_10g/rx_sosi.sop {-radix unsigned} /tb_tech_mac_10g/rx_sosi.eop {-radix unsigned} /tb_tech_mac_10g/rx_sosi.empty {-radix unsigned} /tb_tech_mac_10g/rx_sosi.channel {-radix unsigned} /tb_tech_mac_10g/rx_sosi.err {-radix unsigned}} /tb_tech_mac_10g/rx_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/rx_sosi_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_pkt_cnt
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_pkt_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 fs} 0}
quietly wave cursor active 0
configure wave -namecolwidth 300
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {23798880 ps}
