README.txt for $HDL_WORK/libraries/technology/ip_arria10/fifo

Contents:

1) FIFO components
2) Arria10 IP
3) Implementation options (LUTs or block RAM)
4) Synthesis trials
5) Issues


1) FIFO components:

  ip_arria10_fifo_sc              = Single clock FIFO
  ip_arria10_fifo_dc              = Dual clock FIFO
  ip_arria10_fifo_dc_mixed_widths = Dual clock FIFO with different read and write data widths (ratio power of 2)
  

2) Arria10 IP

  The IP was ported from Stratix IV by:
  
    . copy original MegaWizard <fifo_name>.vhd file
    . rename <fifo_name>.vhd into ip_arria10_<fifo_name>.vhd (also replace name inside the file)
    . commit the fifo/ip_arria10_<fifo_name>.vhd to preserve the MegaWizard original
    . open in to Quartus 14, set device family to Arria10 and finish automatically convert to Qsys
    . then generate HDL (select VHDL for both sim and synth) and finish to save it as ip_arria10_<fifo_name>.qsys
    
  this yields:
  
    ip_arria10_fifo_sc.qsys
    ip_arria10_fifo_dc.qsys
    ip_arria10_fifo_dc_mixed_widths.qsys
  
  The Arria10 FIFO IP still uses the altera_mf package (so not the altera_lnsim package as with the block RAM). The
  FIFOs map to the altera_mf components to scfifo, dcfifo and dcfifo_mixed_widths.
  
  The IP only needs to be generated with ./generate_ip.sh if it need to be modified, because the ip_arria10_fifo_*.vhd
  directly instantiates the altera_mf component.
  
  The instantiation is copied manually from the ip_arria10_fifo_*/fifo_140/sim/ip_arria10_fifo_*.vhd and 
  saved in the <fifo_name>.vhd file. So then the MegaWizard vhd file is overwritten, but that is fine because it is 
  no longer needed, it could easily be derived from the original in $UNB and it is still as a previous verion in SVN.
   

3) Implementation options (LUTs or block RAM)

  The IP FIFO can be set to use LUTs (MLAB) or block RAM (M20K) via g_use_eab.
  

4) Synthesis trials

  The quartus/fifo.qpf Quartus project was used to verify that the FIFO IP actually synthesise to the appropriate FPGA resources.
  Use the Quartus GUI to manually select a top level component for synthesis e.g. by right clicking the entity vhd file
  in the file tab of the Quartus project navigator window.
  Then check the resource usage in the synthesis and fitter reports.


5) Issues

  No issues.