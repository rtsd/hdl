-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.TEXTIO.all;

entity tst_output is
  generic (
    g_file_name   : string;
    g_nof_data    : natural := 1;
    g_data_width  : natural;
    g_data_type   : string := "SIGNED"
  );
  port (
    clk      : in std_logic;
    rst      : in std_logic;
    in_dat   : in std_logic_vector(g_nof_data * g_data_width - 1 downto 0);
    in_val   : in std_logic
  );
begin
  assert g_data_type = "SIGNED" or g_data_type = "UNSIGNED"
    report "Unknown data type."
    severity ERROR;
end tst_output;

architecture beh of tst_output is
   file out_file : TEXT;
begin
  process(rst, clk)
    variable out_line : LINE;
  begin
    if rst = '1' then
      file_close(out_file);
      file_open (out_file, g_file_name, WRITE_MODE);
    elsif rising_edge(clk) then
      if in_val = '1' then
        for i in 0 to g_nof_data - 1 loop
          if g_data_type = "UNSIGNED" then
            write(out_line, integer(to_integer(unsigned(in_dat((i + 1) * g_data_width - 1 downto i * g_data_width)))));
            write(out_line, ' ');
          else
            write(out_line, integer(to_integer(signed(in_dat((i + 1) * g_data_width - 1 downto i * g_data_width)))));
            write(out_line, ' ');
          end if;
        end loop;
        writeline(out_file, out_line);
      end if;
    end if;
  end process;
end beh;
