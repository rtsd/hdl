-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 29 mar 2017
-- Purpose: Verify mms_dp_gain_arr
-- Description:
-- Usage:
-- > as 10
-- > run -all
-- The tb is self stopping and self checking.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_mms_dp_gain_arr is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active or random stimuli valid flow control
    g_nof_streams            : natural := 1;  -- >= 1
    g_complex_data           : boolean := true;
    g_complex_gain           : boolean := false
  );
end tb_mms_dp_gain_arr;

architecture tb of tb_mms_dp_gain_arr is
  constant c_mm_clk_period              : time := 20 ns;
  constant c_dp_clk_period              : time := 10 ns;
  constant c_cross_clock_domain_latency : natural := 20;

  constant c_real_multiply              : boolean := g_complex_data = false and g_complex_gain = false;
  constant c_gain_w                     : natural := 8;  -- from MM
  constant c_gain_max                   : integer :=  2**(c_gain_w - 1) - 1;
  constant c_gain_min                   : integer := -2**(c_gain_w - 1) + 1;  -- do not use most negative value to allow skipping double sign in gain*in_dat product
  constant c_gain_re_sequence_arr       : t_integer_arr(0 to 3) := (c_gain_max, c_gain_max - 3, c_gain_min, c_gain_min + 3);  -- some complex/real test gains
  constant c_gain_im_sequence_arr       : t_integer_arr(0 to 3) := (         0,            3,          0,           -3);  -- some complex      test gains
  constant c_in_dat_w                   : natural := 9;  -- streaming data
  constant c_in_length                  : integer :=  2**c_in_dat_w;
  constant c_in_max                     : integer :=  2**(c_in_dat_w - 1) - 1;
  constant c_in_min                     : integer := -2**(c_in_dat_w - 1);
  constant c_out_dat_w                  : natural := c_gain_w + c_in_dat_w - 1;  -- skip double sign bit and assume complex gain amplitude <= 1

  signal dbg_nof_streams    : natural := g_nof_streams;
  signal dbg_complex_data   : boolean := g_complex_data;
  signal dbg_complex_gain   : boolean := g_complex_gain;

  signal tb_end             : std_logic := '0';
  signal mm_clk             : std_logic := '1';
  signal mm_rst             : std_logic := '1';
  signal dp_clk             : std_logic := '1';
  signal dp_rst             : std_logic := '1';

  signal random             : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal stimuli_en         : std_logic := '0';
  signal verify_en          : std_logic := '0';

  signal cnt_re             : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_im             : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_val            : std_logic;

  signal in_sosi            : t_dp_sosi;
  signal in_sosi_dly           : t_dp_sosi;
  signal in_sosi_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal out_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal exp_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal gain_re_arr        : t_integer_arr(g_nof_streams - 1 downto 0) := (others => 0);
  signal gain_im_arr        : t_integer_arr(g_nof_streams - 1 downto 0) := (others => 0);

  signal reg_gain_re_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_gain_re_miso   : t_mem_miso;
  signal reg_gain_im_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_gain_im_miso   : t_mem_miso;
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  random <= func_common_random(random) when rising_edge(dp_clk);

  stimuli_en <= random(random'high) when g_flow_control_stimuli = e_random else '1';

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------
  proc_dp_cnt_dat(dp_rst, dp_clk, stimuli_en, cnt_val, cnt_re);

  -- derive cnt_im linearly from cnt_re such that abs(complex(cnt_re, cnt_im)) <= amplitude
  -- the principle is: suppose re normalized to 1 then im = 1-re when re>=0 else im = -1-re when re<0
  cnt_im <= TO_SVEC(sel_a_b(TO_SINT(cnt_re) >= 0, c_in_max, c_in_min) - TO_SINT(cnt_re), c_in_dat_w);

  in_sosi.data  <= RESIZE_DP_SDATA(cnt_re);
  in_sosi.re    <= RESIZE_DP_DSP_DATA(cnt_re);
  in_sosi.im    <= RESIZE_DP_DSP_DATA(cnt_im);
  in_sosi.valid <= cnt_val;

  in_sosi_arr <= (others => in_sosi);  -- use same data on all input streams

  u_in_sosi_dly : entity work.dp_pipeline
  generic map (
    g_pipeline   => 3  -- latency of DUT
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    snk_in   => in_sosi,
    src_out  => in_sosi_dly
  );

  p_mm_stimuli : process
    variable v_gain_re : integer;
    variable v_gain_im : integer;
  begin
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    -- Vary the gain per stream, this stimuli scheme requires that g_nof_streams < c_gain_w
    for I in 0 to g_nof_streams - 1 loop
      v_gain_re := 2**I;
      gain_re_arr(I) <= v_gain_re;
      proc_mem_mm_bus_wr(I, v_gain_re, mm_clk, reg_gain_re_miso, reg_gain_re_mosi);
      if g_complex_gain = true then
        v_gain_im := 2**(g_nof_streams - 1 - I);
        gain_im_arr(I) <= v_gain_im;
        proc_mem_mm_bus_wr(I, v_gain_im, mm_clk, reg_gain_im_miso, reg_gain_im_mosi);
      end if;
    end loop;
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);
    verify_en <= '1';
    proc_common_wait_some_cycles(dp_clk, c_in_length * 3);
    verify_en <= '0';

    -- Try extra gain settings for stream 0
    for I in c_gain_re_sequence_arr'range loop
      v_gain_re := c_gain_re_sequence_arr(I);
      gain_re_arr(0) <= v_gain_re;
      proc_mem_mm_bus_wr(0, v_gain_re, mm_clk, reg_gain_re_miso, reg_gain_re_mosi);
      if g_complex_gain = true then
        v_gain_im := c_gain_im_sequence_arr(I);
        gain_im_arr(0) <= v_gain_im;
        proc_mem_mm_bus_wr(0, v_gain_im, mm_clk, reg_gain_im_miso, reg_gain_im_mosi);
      end if;
      proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
      proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);
      verify_en <= '1';
      proc_common_wait_some_cycles(dp_clk, c_in_length * 3);
      verify_en <= '0';
    end loop;

    tb_end <= '1';
    wait;
  end process;

  -- Verify out_sosi_arr assuming that the gain and c_out_dat_w are set such that the output equals the input
  p_exp_sosi_arr : process(gain_re_arr, gain_im_arr, in_sosi_dly)
  begin
    for I in 0 to g_nof_streams - 1 loop
      if c_real_multiply = true then
        exp_sosi_arr(I).data <= TO_DP_SDATA(gain_re_arr(I) * TO_SINT(in_sosi_dly.data));
      else
        if g_complex_gain = false then
          exp_sosi_arr(I).re <= TO_DP_DSP_DATA(gain_re_arr(I) * TO_SINT(in_sosi_dly.re));
          exp_sosi_arr(I).im <= TO_DP_DSP_DATA(gain_re_arr(I) * TO_SINT(in_sosi_dly.im));
        else
          exp_sosi_arr(I).re <= TO_DP_DSP_DATA(gain_re_arr(I) * TO_SINT(in_sosi_dly.re) - gain_im_arr(I) * TO_SINT(in_sosi_dly.im));
          exp_sosi_arr(I).im <= TO_DP_DSP_DATA(gain_re_arr(I) * TO_SINT(in_sosi_dly.im) + gain_im_arr(I) * TO_SINT(in_sosi_dly.re));
        end if;
      end if;
    end loop;
  end process;

  p_verify : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_en = '1' then
        for I in 0 to g_nof_streams - 1 loop
          if c_real_multiply = true then
            assert signed(out_sosi_arr(I).data) = signed(exp_sosi_arr(I).data)
              report "Unexpected real data"
              severity ERROR;
          else
            if g_complex_gain = false then
              assert signed(out_sosi_arr(I).re) = signed(exp_sosi_arr(I).re)
                report "Unexpected complex real data, with real gain"
                severity ERROR;
              assert signed(out_sosi_arr(I).im) = signed(exp_sosi_arr(I).im)
                report "Unexpected complex imag data, with real gain"
                severity ERROR;
            else
              assert signed(out_sosi_arr(I).re) = signed(exp_sosi_arr(I).re)
                report "Unexpected complex real data, with complex gain"
                severity ERROR;
              assert signed(out_sosi_arr(I).im) = signed(exp_sosi_arr(I).im)
                report "Unexpected complex imag data, with complex gain"
                severity ERROR;
            end if;
          end if;
        end loop;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  gen_one : if g_nof_streams = 1 generate
    u_dut_one : entity work.mms_dp_gain
    generic map (
      g_technology   => g_technology,
      g_complex_data => g_complex_data,
      g_complex_gain => g_complex_gain,
      g_gain_init_re => 1,
      g_gain_init_im => 0,
      g_gain_w       => c_gain_w,
      g_in_dat_w     => c_in_dat_w,
      g_out_dat_w    => c_out_dat_w
    )
    port map (
      -- Clocks and reset
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,

      -- MM access to gain
      reg_gain_re_mosi  => reg_gain_re_mosi,
      reg_gain_re_miso  => reg_gain_re_miso,
      reg_gain_im_mosi  => reg_gain_im_mosi,
      reg_gain_im_miso  => reg_gain_im_miso,

      -- ST
      in_sosi           => in_sosi_arr(0),
      out_sosi          => out_sosi_arr(0)
    );
  end generate;

  gen_arr : if g_nof_streams > 1 generate
    u_dut_arr : entity work.mms_dp_gain_arr
    generic map (
      g_technology   => g_technology,
      g_nof_streams  => g_nof_streams,
      g_complex_data => g_complex_data,
      g_complex_gain => g_complex_gain,
      g_gain_init_re => 1,
      g_gain_init_im => 0,
      g_gain_w       => c_gain_w,
      g_in_dat_w     => c_in_dat_w,
      g_out_dat_w    => c_out_dat_w
    )
    port map (
      -- Clocks and reset
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,

      -- MM access to gain
      reg_gain_re_mosi  => reg_gain_re_mosi,
      reg_gain_re_miso  => reg_gain_re_miso,
      reg_gain_im_mosi  => reg_gain_im_mosi,
      reg_gain_im_miso  => reg_gain_im_miso,

      -- ST
      in_sosi_arr       => in_sosi_arr,
      out_sosi_arr      => out_sosi_arr
    );
  end generate;
end tb;
