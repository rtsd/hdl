--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   De-multiplex frames from one input stream with one or more channels onto
--   separate output streams.
-- Description:
--   If g_combined=TRUE then all outputs must be ready for the dp_demux to be
--   ready. This is because due to RL=1 the snk_in.channel and
--   src_in_arr().ready do not apply to the same clock cycle. The adavantage
--   of using g_combined=TRUE is that it takes minimal logic. The fact that it
--   works combinatorially may cause problems with achieving timing closure.
--   If g_combined=FALSE then only the output for the current channel needs to
--   be ready. This is possible because internally the dp_demux then operates
--   at RL=0 so it has a show-ahead on the valid snk_in.channel. The output is
--   again at RL=1 which has the additional benefit that the src_in_arr().ready
--   and the snk_out.ready SISO ready signals are connected via a register
--   stage. The RL adapter to get to RL=0 also registers the SOSI data signals.
--   The output selection scheme depends on g_mode:
--   0: Use current channel to select the output port.
--      The snk_in.channel[log2(g_nof_output)-1:0] bits are used to select the
--      output port. The remaining snk_in.channel[high:log2(g_nof_output)]
--      bits are passed on to src_out_arr().channel (assuming typically
--      g_remove_channel_lo = TRUE).
--   1: Use eop to select next output.
--   2: Use external MM control input to select the output
-- Remark:
-- . For g_nof_output=1 the dp_demux collapses to wires, because ceil_log2(1)=1.
-- . The dp_mux adds true_log2(nof ports) low bits to src_out_arr.channel and
--   this dp_demux removes true_log2(nof ports) low bits from snk_in().channel.
-- . For demultiplexing time series frames or sample it can be applicable to
--   use g_remove_channel_lo=FALSE in combination with g_mode=1.
-- . Mode 0 and 2 two work for any sosi stream, mode 1 requires framed data i.e.
--   with eop.

entity dp_demux is
  generic (
    g_mode              : natural := 0;
    g_nof_output        : natural := 2;  -- >= 1
    g_remove_channel_lo : boolean := true;
    g_combined          : boolean := true;
    g_sel_ctrl_invert   : boolean := false;  -- Use default FALSE when stream array IO are indexed (0 TO g_nof_input-1), else use TRUE when indexed (g_nof_input-1 DOWNTO 0)
    g_sel_ctrl_pkt      : boolean := false  -- When TRUE and g_mode=2 and g_combined=FALSE, this selects a new output on packet boundaries only.
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- Control & Status
    sel_ctrl    : in  natural range 0 to g_nof_output - 1 := 0;  -- used by g_mode = 2
    sel_stat    : out natural range 0 to g_nof_output - 1;  -- used by g_sel_ctrl_pkt = TRUE
    -- ST sinks
    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(0 to g_nof_output - 1) := (others => c_dp_siso_rdy);
    src_out_arr : out t_dp_sosi_arr(0 to g_nof_output - 1)
  );
end dp_demux;

architecture rtl of dp_demux is
  -- The low part of snk_in.channel is used to select the output port and the high part of snk_in.channel used for src_out_arr[].channel
  constant c_sel_w  : natural := true_log2(g_nof_output);

  -- Output selection scheme
  signal sel_channel        : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);  -- used by g_mode = 0
  signal sel_eop            : std_logic;  -- used by g_mode = 1
  signal output_select      : natural range 0 to g_nof_output - 1;
  signal prev_output_select : natural range 0 to g_nof_output - 1;
  signal pkt_det            : std_logic;

  -- g_combined=FALSE
  signal adapt_siso       : t_dp_siso;
  signal adapt_sosi       : t_dp_sosi;

  signal prev_src_in_arr  : t_dp_siso_arr(0 to g_nof_output - 1);
  signal pend_src_out_arr : t_dp_sosi_arr(0 to g_nof_output - 1);
begin
  -- Collapse to wires
  gen_one : if g_nof_output = 1 generate
    snk_out        <= src_in_arr(0);
    src_out_arr(0) <= snk_in;  -- so c_sel_w = 0
  end generate;

  ------------------------------------------------------------------------------
  -- Output selection scheme
  ------------------------------------------------------------------------------

  -- Use current channel to select the output port.
  gen_mode_0 : if g_mode = 0 generate
    output_select <= TO_UINT(sel_channel(c_sel_w - 1 downto 0));
  end generate;

  -- Use eop to select next output.
  gen_mode_1 : if g_mode = 1 generate
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        output_select <= 0;
      elsif rising_edge(clk) then
        if sel_eop = '1' then
          if output_select = g_nof_output - 1 then
            output_select <= 0;
          else
            output_select <= output_select + 1;
          end if;
        end if;
      end if;
    end process;
  end generate;

  -- Use external MM control input to select the output
  gen_mode_2 : if g_mode = 2 generate
    no_pkt_ctrl: if g_sel_ctrl_pkt = false generate
      -- Select new output immediately (registered: one cycle after sel_ctrl change)
      p_clk : process(rst, clk)
      begin
        if rst = '1' then
          output_select <= 0;
        elsif rising_edge(clk) then
          if g_sel_ctrl_invert = false then
            output_select <= sel_ctrl;
          else
            output_select <= g_nof_output - 1 - sel_ctrl;
          end if;
        end if;
      end process;
    end generate;

    gen_pkt_ctrl: if g_sel_ctrl_pkt = true and g_combined = false generate
      -- Select new output while respecting packet boundaries. We need RL=0 (g_combined=FALSE) to do
      -- this in a reliable way.
      -- Changing the output is only allowed:
      --   -> When there is no packet present on the bus, or
      --   -> When there is a SOP present on the bus (this allows us to redirect back-to-back packets, thanks to RL=0)
      p_select : process(prev_output_select, sel_ctrl, pkt_det, adapt_sosi)
      begin
        output_select <= prev_output_select;
        if pkt_det = '0' or adapt_sosi.sop = '1' then
          if g_sel_ctrl_invert = false then
            output_select <= sel_ctrl;
          else
            output_select <= g_nof_output - 1 - sel_ctrl;
          end if;
        -- User might need this status port to indicate if/when the output has actually been switched
        sel_stat <= output_select;
        end if;
      end process;

      -- Keep the current selection until we're allowed to switch (on a packet bounday)
      p_clk : process(rst, clk)
      begin
        if rst = '1' then
          prev_output_select <= 0;
        elsif rising_edge(clk) then
          prev_output_select <= output_select;
        end if;
      end process;

      -- The dp_packet_detect component simply asserts its output from SOP to EOP
      u_dp_packet_detect : entity work.dp_packet_detect
      generic map (
        g_latency => 0
      )
      port map (
        rst       => rst,
        clk       => clk,
        sosi      => adapt_sosi,
        siso      => adapt_siso,  -- We're using the adapted sink_in with RL=0
        pkt_det   => pkt_det
      );
    end generate;

  end generate;

  ------------------------------------------------------------------------------
  -- g_combined=TRUE : All outputs must be ready
  ------------------------------------------------------------------------------

  gen_combined : if g_combined = true and g_nof_output > 1 generate
    snk_out.xon   <= func_dp_stream_arr_and(src_in_arr, "XON");
    snk_out.ready <= func_dp_stream_arr_and(src_in_arr, "READY");

    sel_channel <= snk_in.channel;
    sel_eop     <= snk_in.eop;

    p_output : process(snk_in, output_select)
    begin
      for I in 0 to g_nof_output - 1 loop
        src_out_arr(I)         <= snk_in;
        src_out_arr(I).sync    <= '0';
        src_out_arr(I).valid   <= '0';
        src_out_arr(I).sop     <= '0';
        src_out_arr(I).eop     <= '0';
        if g_remove_channel_lo = true then
          src_out_arr(I).channel <= SHIFT_UVEC(snk_in.channel, c_sel_w);  -- strip the low part that is used for the demux from the channel field
        end if;
        if output_select = I then
          src_out_arr(I).sync  <= snk_in.sync;
          src_out_arr(I).valid <= snk_in.valid;
          src_out_arr(I).sop   <= snk_in.sop;
          src_out_arr(I).eop   <= snk_in.eop;
        end if;
      end loop;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- g_combined=FALSE : Only the output for the current channel needs to be ready
  ------------------------------------------------------------------------------

  gen_individual : if g_combined = false and g_nof_output > 1 generate
    -- Adapt input to RL = 0 to have show-ahead
    u_rl_0 : entity work.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sink
      snk_out   => snk_out,
      snk_in    => snk_in,
      -- ST source
      src_in    => adapt_siso,
      src_out   => adapt_sosi
    );

    sel_channel <= adapt_sosi.channel;
    sel_eop     <= adapt_sosi.eop and adapt_siso.ready;  -- RL = 0, so eop is only valid when the ready acknowledges it

    -- Demux at RL = 0
    p_output : process(adapt_sosi, prev_src_in_arr, output_select)
    begin
      adapt_siso <= c_dp_siso_rst;
      for I in 0 to g_nof_output - 1 loop
        pend_src_out_arr(I)         <= adapt_sosi;
        pend_src_out_arr(I).sync    <= '0';
        pend_src_out_arr(I).valid   <= '0';
        pend_src_out_arr(I).sop     <= '0';
        pend_src_out_arr(I).eop     <= '0';
        if g_remove_channel_lo = true then
          pend_src_out_arr(I).channel <= SHIFT_UVEC(adapt_sosi.channel, c_sel_w);  -- strip the low part that is used for the demux from the channel field
        end if;
        if output_select = I then
          adapt_siso <= prev_src_in_arr(I);
          pend_src_out_arr(I).sync  <= adapt_sosi.sync;
          pend_src_out_arr(I).valid <= adapt_sosi.valid;
          pend_src_out_arr(I).sop   <= adapt_sosi.sop;
          pend_src_out_arr(I).eop   <= adapt_sosi.eop;
        end if;
      end loop;

      -- Pass on frame level flow control
      adapt_siso.xon <= func_dp_stream_arr_and(prev_src_in_arr, "XON");
    end process;

    -- Back to output RL = 1
    gen_rl_1 : for I in 0 to g_nof_output - 1 generate
      u_incr : entity work.dp_latency_increase
      generic map (
        g_in_latency   => 0,
        g_incr_latency => 1
      )
      port map (
        rst         => rst,
        clk         => clk,
        -- ST sink
        snk_out     => prev_src_in_arr(I),
        snk_in      => pend_src_out_arr(I),
        -- ST source
        src_in      => src_in_arr(I),
        src_out     => src_out_arr(I)
      );
    end generate;
  end generate;
end rtl;
