#! /usr/bin/env python3
###############################################################################
#
# Copyright 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###############################################################################

# Author: Eric Kooistra
# Date: sep 2022
# Purpose:
#   Try applying a weight after or before rounding.
# Description:
#   Model to investigate usage of subband weights on:
#   . quantized subbands --> sigma_qq
#   . unquantized subbands --> sigma_sq
#   Preliminary conclusion:
#   . For small input noise with sigma < 2 the output sigma gets disturbed
#     due to the weighting if the weighting is applied after the subband
#     quantisation
#   . Increasing -N improves the results, for LOFAR subbands N = 195312
#   . it may be preferred to apply the subband weights to the unquantized
#     WPFB output.
#   . Choosing sufficient intermediate resolution (-r) before applying
#     weights:
#     - Rounding noise changes the sigma of the noise anyway, as shown by
#       sigmas_ratio_sq_input_T. Therefore choose resolution such that
#       jumps in sigma due to rounding weighted noise, as shown by
#       sigmas_ratio_qq_sq_T, is much smaller than sigmas_ratio_sq_input_T
#       ==> -r 2.
#     - For input sigma >= 1
#       . Choose sigmas_ratio_qq_sq_T < 10%   ==> -r 3
#       . Choose sigmas_ratio_qq_sq_T <  1%   ==> -r 6
#       . Choose sigmas_ratio_qq_sq_T <  0.1% ==> -r 9
#       . Hence the disturbance (jumps) on the sigma is about proportional
#         to 1/2**r
# Note:
# . For values exactly halfway between rounded decimal values, NumPy of
#   Python3 rounds to the nearest even value. Thus 1.5 and 2.5 round to 2.0,
#   -0.5 and 0.5 round to 0.0, etc. Python2 rounds half away from zero.
# Usage:
#   > python3 try_round_weight.py -N 195312

import argparse
import textwrap

import numpy as np
import matplotlib
matplotlib.use('tkagg')  # to make X11 forwarding work
import matplotlib.pyplot as plt

# Parse arguments to derive user parameters
_parser = argparse.ArgumentParser(
    description="".join(textwrap.dedent("""\
        Model effect of applying a weight on input noise after or before rounding,
        for range of input noise sigma and range of weights:

                 sigma
                   |      2**resolution       1/2**resolution
                   |       |                   |
                   v       v                   v
                 -----   -----   ---------   -----  noise_q   -----   ---------   -------
        noise -->| X |-->| X |-->| round |-->| X |----------->| X |-->| round |-->| std |--> sigma_qq
                 ----- | -----   ---------   -----            -----   ---------   -------
                       |                                        ^
                       |                                        |
                       |                            weight ---->|
                       |                                        |
                       |                            noise_s   -----   ---------   -------
                       |------------------------------------->| X |-->| round |-->| std |--> sigma_sq
                                                              -----   ---------   -------

        sigmas_ratio_qq_sq = sigma_qq / sigma_sq --> shows jumps and variation around 1 when resolution is low
        sigmas_ratio_sq_input = sigma_sq / sigma --> shows that rounding causes change in sigma

        . weights in range(w_lo, w_hi, w_step)
        . sigma of noise in range(s_lo, s_hi, s_step)
        . use N = 195312 to model number of subband periods in 1 s of LOFAR SST
        . use different seed S to check whether result in plots depends on seed
        . use r = 0 for default integer rounding, use r > 0 to have extra
          LSbits resolution before rounding
        . use f = 0 for full double resolution before rounding, use f > 0 and
          r = 0 to have noise with a fraction of f LSbits before rounding

        # Get an overview
        > python try_round_weight.py --w_lo 0.2 --w_hi 3.0 --w_step 0.01 --s_lo 0.5 --s_hi 10 --s_step 0.2 -N 195312 -S 1
        > python try_round_weight.py --w_lo 0.2 --w_hi 2.0 --w_step 0.01 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 1

        # Zoom in at w = 0.75
        > python try_round_weight.py --w_lo 0.7 --w_hi 0.8 --w_step 0.0001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0

        # Use -r = 6 to see effect of applying weights with more (intermediate)
        # resolution before rounding
        > python try_round_weight.py --w_lo 0.7 --w_hi 0.8 --w_step 0.0001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0 -r 6
        > python try_round_weight.py --w_lo 0.3 --w_hi 1.1 --w_step 0.001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0 -r 6

        # Reproduce plots/
        > python try_round_weight.py --w_lo 0.3 --w_hi 1.1 --w_step 0.001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0 -r 0 --noplot --save
        > python try_round_weight.py --w_lo 0.3 --w_hi 1.1 --w_step 0.001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0 -r 4 --noplot --save
        > python try_round_weight.py --w_lo 0.3 --w_hi 1.1 --w_step 0.001 --s_lo 1 --s_hi 10 --s_step 1 -N 195312 -S 0 -r 6 --noplot --save
        \n""")),
    formatter_class=argparse.RawTextHelpFormatter)
_parser.add_argument('-S', default=0, type=int, help='Random number seed')
_parser.add_argument('-N', default=1000, type=int, help='Number of input samples')
_parser.add_argument('-f', default=0, type=int, help='Number of LSbits in fraction, use default 0 for double accuracy')
_parser.add_argument('-r', default=0, type=int, help='Number of LSbits extra resolution before rounding')
_parser.add_argument('--s_lo', default=0.1, type=float, help='Lowest sigma')
_parser.add_argument('--s_hi', default=25.0, type=float, help='Highest sigma')
_parser.add_argument('--s_step', default=0.1, type=float, help='Step sigma')
_parser.add_argument('--w_lo', default=0.3, type=float, help='Lowest weight')
_parser.add_argument('--w_hi', default=2.0, type=float, help='Highest weight')
_parser.add_argument('--w_step', default=0.1, type=float, help='Step weight')
_parser.add_argument('--noplot', action="store_true", help='Do not show plots')
_parser.add_argument('--save', action="store_true", help='Do save plots')
args = _parser.parse_args()

np.random.seed(args.S)

# Prepare noise signal
N_samples = args.N
noise = np.random.randn(N_samples)
noise /= np.std(noise)

# Noise levels range, 1 unit = 1 LSbit
sigma_lo = args.s_lo
sigma_hi = args.s_hi
sigma_step = args.s_step
sigmas = np.arange(sigma_lo, sigma_hi, sigma_step)
N_sigmas = len(sigmas)

# Weights range, unit weight = 1
weight_lo = args.w_lo
weight_hi = args.w_hi
weight_step = args.w_step
weights = np.arange(weight_lo, weight_hi, weight_step)
N_weights = len(weights)

# Fraction
fraction = args.f
fraction_factor = 2**fraction

# Resolution
resolution = args.r
resolution_factor = 2**resolution

# Determine weighted rounded noise sigma / weighted noise sigma for range of weights and input noise sigmas
sigmas_ratio_qq_sq = np.nan * np.zeros((N_weights, N_sigmas))  # w rows, s cols
sigmas_ratio_sq_input = np.nan * np.zeros((N_weights, N_sigmas))  # w rows, s cols
sigmas_qq = np.zeros((N_weights, N_sigmas))
sigmas_sq = np.zeros((N_weights, N_sigmas))
for s, sigma in enumerate(sigmas):
    noise_s = noise * sigma
    if resolution == 0:
        if fraction == 0:
            # Default use full double resolution of fraction of noise_s
            noise_q = np.round(noise_s)
        else:
            # First use round() to get noise_f with a fraction of fraction number of LSbits
            noise_f = np.round(noise_s * fraction_factor) / fraction_factor
            # Then use round to round the fraction of fraction number of LSbits in noise_f
            noise_q = np.round(noise_f)
    else:
        noise_q = np.round(noise_s * resolution_factor) / resolution_factor

    for w, weight in enumerate(weights):
        noise_q_weighted_q = np.round(noise_q * weight)  # apply weight to rounded noise
        noise_s_weighted_q = np.round(noise_s * weight)  # apply weight to original noise
        s_qq = np.std(noise_q_weighted_q)
        s_sq = np.std(noise_s_weighted_q)
        sigmas_qq[w][s] = s_qq
        sigmas_sq[w][s] = s_sq
        if s_sq != 0:
            sigmas_ratio_qq_sq[w][s] = s_qq / s_sq  # weighted rounded noise sigma / weighted noise sigma
            sigmas_ratio_sq_input[w][s] = s_sq / sigma  # weighted noise sigma / input noise sigma
# Transpose [w][s] to have index ranges [s][w]
sigmas_ratio_qq_sq_T = sigmas_ratio_qq_sq.transpose()
sigmas_ratio_sq_input_T = sigmas_ratio_sq_input.transpose()
sigmas_qq_T = sigmas_qq.transpose()
sigmas_sq_T = sigmas_sq.transpose()

# Plot results
figNr = 0
dpi = 254  # 10 dots per mm
s_colors = plt.cm.jet(np.linspace(0, 1, N_sigmas))
w_colors = plt.cm.jet(np.linspace(0, 1, N_weights))

figNr += 1
plt.figure(figNr)
for s, sigma in enumerate(sigmas):
    # Plot sigma_qq of twice quantized noise as function of weight for
    # different input sigmas
    plt.plot(weights, sigmas_qq_T[s], color=s_colors[s], label='s = %4.2f' % sigma)
plt.title("Sigma of weighted quantized noise")
plt.xlabel("Weight")
plt.ylabel("Sigma_qq")
plt.legend(loc='upper right')
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_w_sigma_qq.jpg' % resolution, dpi=dpi)

figNr += 1
plt.figure(figNr)
for s, sigma in enumerate(sigmas):
    # Plot sigma_qq of twice quantized noise as function of weight for
    # different input sigmas.
    # Normalize the sigma_qq by the weight, so that it can be compared with
    # the input sigma that is shown by the horizontal sigma reference lines.
    plt.plot(weights, sigmas_qq_T[s] / weights, color=s_colors[s], label='s = %4.2f' % sigma)
    plt.plot(weights, sigmas[s]*np.ones(N_weights), '--', color=s_colors[s])  # add sigma reference lines
plt.title("Sigma of weighted quantized noise, normalized for weight")
plt.xlabel("Weight")
plt.ylabel("Sigma_qq")
plt.legend(loc='upper right')
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_w_sigma_qq_normalized.jpg' % resolution, dpi=dpi)

figNr += 1
plt.figure(figNr)
for s, sigma in enumerate(sigmas):
    # Plot ratio of sigma_sq / sigma as function of weight for different
    # input sigma. The ratio deviation from 1 tells how much quantized
    # weighted noise deviates from the input noise. This shows that rounding
    # noise cause a change in sigma even when weight is 1.
    plt.plot(weights, sigmas_ratio_sq_input_T[s] / weights, color=s_colors[s], label='s = %4.2f' % sigma)
plt.title("Relative sigma difference of weighted quantized data / input data")
plt.xlabel("Weight")
plt.ylabel("Relative sigma difference")
plt.legend(loc='upper right')
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_w_sigmas_ratio_sq_input.jpg' % resolution, dpi=dpi)

figNr += 1
plt.figure(figNr)
for s, sigma in enumerate(sigmas):
    # Plot ratio of sigma_qq / sigma_sq as function of weight for different
    # input sigma. The ratio deviation from 1 tells how much the twice
    # quantized noise deviates from the noise that is only quantized after
    # the weighting.
    plt.plot(weights, sigmas_ratio_qq_sq_T[s], color=s_colors[s], label='s = %4.2f' % sigma)
plt.title("Relative sigma difference of weighting before / after quantisation")
plt.xlabel("Weight")
plt.ylabel("Relative sigma difference")
plt.legend(loc='upper right')
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_w_sigmas_ratio_qq_sq.jpg' % resolution, dpi=dpi)

figNr += 1
plt.figure(figNr)
for w, weight in enumerate(weights):
    # Plot ratio of sigma_qq / sigma_sq as function of input sigma for
    # different weights
    plt.plot(sigmas, sigmas_ratio_qq_sq[w], color=w_colors[w], label='w = %4.2f' % weight)
plt.title("Relative sigma difference of weighting before / after quantisation")
plt.xlabel("Sigma")
plt.ylabel("Relative sigma difference (s_qq / s_sq)")
plt.legend(loc='upper right')
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_s_sigmas_ratio_qq_sq.jpg' % resolution, dpi=dpi)

figNr += 1
plt.figure(figNr)
plt.imshow(sigmas_ratio_qq_sq, origin='lower', interpolation='none', aspect='auto', extent=[sigma_lo, sigma_hi, weight_lo, weight_hi])
plt.colorbar()
plt.title("Relative sigma difference of weighting after / before quantisation")
plt.xlabel("Sigma")
plt.ylabel("Weight")
plt.grid()
if args.save:
    plt.savefig('plots/try_round_weight_r%d_sw_sigmas_ratio_qq_sq.jpg' % resolution, dpi=dpi)

if not args.noplot:
    plt.show()
