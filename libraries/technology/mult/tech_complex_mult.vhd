-- -----------------------------------------------------------------------------
--
-- Copyright 2009-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : E. Kooistra
-- Changed by : D.F. Brouwer
-- Purpose : Wrapper for complex multiplier IP
-- Decription :
--
--   Calculate complex product:
--
--     (ar + j*ai) * (br + j*bi) = (ar*br - ai*bi) + j *(ar*bi + ai*br)
--                               =  pr             + j * pi
--
--   Assume IP is generated for complex product width for pr, pi of:
--
--     c_dsp_prod_w = 2*c_dsp_dat_w.
--
--   It is not necessary to support product width 2*c_dsp_dat_w + 1,
--   because this +1 bit is only needed for pi in case ar = ai = br = bi
--   = min, where
--
--     min = -2**(c_dsp_dat_w-1)
--     max =  2**(c_dsp_dat_w-1) - 1.
--
--   The largest value for pi = min**2 + min**2.
--   The largest value for pr = min**2 - min*max < largest pi.
--
--   The largest pi = 2 * min**2 = 2**(c_dsp_prod_w-1), so it just does not
--   fit in c_dsp_prod_w, but largest pi - 1 = 2**(c_dsp_dat_w-1) - 1 does
--   fit, so all other input values fit. In DSP systems the input value
--   (min + j*min) typically never occurs.
--
--   Example: g_in_a_w = 3 bit:
--            --> min = -4
--                c_dsp_prod_w = 6
--            --> largest pi = 32
--            --> largest pi - 1 = 31 = 2**(c_dsp_prod_w-1) - 1
--

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tech_mult_component_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_mult_lib;
library ip_arria10_complex_mult_altmult_complex_150;
library ip_arria10_e1sg_complex_mult_altmult_complex_180;
library ip_arria10_e2sg_complex_mult_altmult_complex_1910;
library ip_agi027_1e1v_complex_mult_altmult_complex_1910;
library ip_arria10_complex_mult_rtl_lib;
library ip_agi027_1e1v_complex_mult_rtl_lib;
library ip_arria10_complex_mult_rtl_canonical_lib;
library ip_agi027_1e1v_complex_mult_rtl_canonical_lib;

entity tech_complex_mult is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string := "IP";
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
end tech_complex_mult;

architecture str of tech_complex_mult is
  constant c_dsp_dat_w   : natural := sel_a_b(g_in_a_w <= c_dsp_mult_18_w and g_in_b_w <= c_dsp_mult_18_w, c_dsp_mult_18_w, c_dsp_mult_27_w);  -- g_in_a_w = g_in_b_w
  constant c_dsp_prod_w  : natural  := 2 * c_dsp_dat_w;

  signal ar        : std_logic_vector(c_dsp_dat_w - 1 downto 0);
  signal ai        : std_logic_vector(c_dsp_dat_w - 1 downto 0);
  signal br        : std_logic_vector(c_dsp_dat_w - 1 downto 0);
  signal bi        : std_logic_vector(c_dsp_dat_w - 1 downto 0);
  signal mult_re   : std_logic_vector(c_dsp_prod_w - 1 downto 0);
  signal mult_im   : std_logic_vector(c_dsp_prod_w - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- IP variants for <= 18 bit
  -----------------------------------------------------------------------------

  gen_ip_stratixiv_ip : if g_variant = "IP" and g_technology = c_tech_stratixiv and c_dsp_dat_w <= c_dsp_mult_18_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_18_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_18_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_18_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_18_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_18_w);

    u0 : ip_stratixiv_complex_mult
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_arria10_ip : if g_variant = "IP" and g_technology = c_tech_arria10_proto and c_dsp_dat_w <= c_dsp_mult_18_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_18_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_18_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_18_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_18_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_18_w);

    u0 : ip_arria10_complex_mult
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_arria10_e1sg_ip : if g_variant = "IP" and g_technology = c_tech_arria10_e1sg and c_dsp_dat_w <= c_dsp_mult_18_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_18_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_18_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_18_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_18_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_18_w);

    u0 : ip_arria10_e1sg_complex_mult
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_arria10_e2sg_ip : if g_variant = "IP" and g_technology = c_tech_arria10_e2sg and c_dsp_dat_w <= c_dsp_mult_18_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_18_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_18_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_18_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_18_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_18_w);

    u0 : ip_arria10_e2sg_complex_mult
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_agi027_1e1v_ip : if g_variant = "IP" and g_technology = c_tech_agi027_1e1v and c_dsp_dat_w <= c_dsp_mult_18_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_18_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_18_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_18_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_18_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_18_w);

    u0 : ip_agi027_1e1v_complex_mult
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  -----------------------------------------------------------------------------
  -- IP variants for > 18 bit and <= 27 bit
  -----------------------------------------------------------------------------

  gen_ip_arria10_e1sg_ip_27b : if g_variant = "IP" and g_technology = c_tech_arria10_e1sg and c_dsp_dat_w > c_dsp_mult_18_w and c_dsp_dat_w <= c_dsp_mult_27_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_27_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_27_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_27_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_27_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_27_w);

    u0 : ip_arria10_e1sg_complex_mult_27b
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_arria10_e2sg_ip_27b : if g_variant = "IP" and g_technology = c_tech_arria10_e2sg and c_dsp_dat_w > c_dsp_mult_18_w and c_dsp_dat_w <= c_dsp_mult_27_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_27_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_27_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_27_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_27_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_27_w);

    u0 : ip_arria10_e2sg_complex_mult_27b
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  gen_ip_agi027_1e1v_ip_27b : if g_variant = "IP" and g_technology = c_tech_agi027_1e1v and c_dsp_dat_w > c_dsp_mult_18_w and c_dsp_dat_w <= c_dsp_mult_27_w generate
    -- Adapt DSP input widths
    ar <= RESIZE_SVEC(in_ar, c_dsp_mult_27_w);
    ai <= RESIZE_SVEC(in_ai, c_dsp_mult_27_w);
    br <= RESIZE_SVEC(in_br, c_dsp_mult_27_w);
    bi <= RESIZE_SVEC(in_bi, c_dsp_mult_27_w) when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), c_dsp_mult_27_w);

    u0 : ip_agi027_1e1v_complex_mult_27b
    port map (
      aclr        => rst,
      clock       => clk,
      dataa_imag  => ai,
      dataa_real  => ar,
      datab_imag  => bi,
      datab_real  => br,
      ena         => clken,
      result_imag => mult_im,
      result_real => mult_re
    );

    -- Back to true input widths and then resize for output width
    result_re <= RESIZE_SVEC(mult_re, g_out_p_w);
    result_im <= RESIZE_SVEC(mult_im, g_out_p_w);
  end generate;

  -----------------------------------------------------------------------------
  -- RTL variants that can infer multipliers for a technology, fits all widths
  -----------------------------------------------------------------------------
  gen_ip_stratixiv_rtl : if g_variant = "RTL" and g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_complex_mult_rtl
    generic map (
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_conjugate_b      => g_conjugate_b,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map (
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_ar      => in_ar,
      in_ai      => in_ai,
      in_br      => in_br,
      in_bi      => in_bi,
      result_re  => result_re,
      result_im  => result_im
    );
  end generate;

  -- RTL variant is the same for unb2, unb2a and unb2b
  gen_ip_arria10_rtl : if g_variant = "RTL" and (g_technology = c_tech_arria10_proto or
                                               g_technology = c_tech_arria10_e3sge3 or
                                               g_technology = c_tech_arria10_e1sg or

                                               g_technology = c_tech_arria10_e2sg) generate
    u0 : ip_arria10_complex_mult_rtl
    generic map (
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_conjugate_b      => g_conjugate_b,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map (
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_ar      => in_ar,
      in_ai      => in_ai,
      in_br      => in_br,
      in_bi      => in_bi,
      result_re  => result_re,
      result_im  => result_im
    );
  end generate;

  -- RTL variant is the same for unb2, unb2a and unb2b
  gen_ip_arria10_rtl_canonical : if g_variant = "RTL_C" and (g_technology = c_tech_arria10_proto or
                                                           g_technology = c_tech_arria10_e3sge3 or
                                                           g_technology = c_tech_arria10_e1sg or
                                                           g_technology = c_tech_arria10_e2sg) generate
    -- support g_conjugate_b
    bi <= in_bi when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), g_in_b_w);

    u0 : ip_arria10_complex_mult_rtl_canonical
    generic map (
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map (
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_ar      => in_ar,
      in_ai      => in_ai,
      in_br      => in_br,
      in_bi      => bi,
      result_re  => result_re,
      result_im  => result_im
    );
  end generate;

  -- RTL variant is for iwave
  gen_ip_agi027_1e1v_rtl : if g_variant = "RTL" and (g_technology = c_tech_agi027_1e1v) generate
    u0 : ip_agi027_1e1v_complex_mult_rtl
    generic map (
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_conjugate_b      => g_conjugate_b,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map (
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_ar      => in_ar,
      in_ai      => in_ai,
      in_br      => in_br,
      in_bi      => in_bi,
      result_re  => result_re,
      result_im  => result_im
    );
  end generate;

  -- RTL variant is for iwave
  gen_ip_agi027_1e1v_rtl_canonical : if g_variant = "RTL_C" and (g_technology = c_tech_agi027_1e1v) generate
    -- support g_conjugate_b
    bi <= in_bi when g_conjugate_b = false else TO_SVEC(-TO_SINT(in_bi), g_in_b_w);

    u0 : ip_agi027_1e1v_complex_mult_rtl_canonical
    generic map (
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map (
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_ar      => in_ar,
      in_ai      => in_ai,
      in_br      => in_br,
      in_bi      => bi,
      result_re  => result_re,
      result_im  => result_im
    );
  end generate;
end str;
