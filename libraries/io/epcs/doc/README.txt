Notes on .RBF files on UniBoard2
--------------------------------

Thu Apr 21 14:44:02 CEST 2016


For now it is assumed that .RBF files for a single design are not larger than 40Mbyte.
Because the .SOF files are sized ~ 37Mbyte. But in the future it could be that 40Mbyte is not correct.

For now (40Mbyte), the following start addresses are choosen (see python scripts pi_epcs.py and pi_remu.py)

  Factory: 0
  User:    41943040 (decimal)


The EPCQL1024 flash has a capacity of 128M, so there is place for a 2nd User image.



To boot the FPGA with a factory image (address 0) an extra bootloader is required
This bootloader looks like (hexdump):


  0000000 ffff ffff ffff ffff ffff ffff 6a6a 6a6a
  0000010 0020 0000 0a03 0420 0420 0420 ffff 87fe


Then the FPGA image starts at address offset 0x20:


  0000020 a65c 0001 0004 0000 ffff ffff ffff ffff
  0000030 ffff ffff ffff ffff ffff ffff ffff ffff
  *
  00000a0 959b 3d24 0000 0001 0001 0101 0100 0100
  ...



This means that The Factory .RBF file needs this bootloader and the User .RBF not.
To generate the .RBF files (from .SOF), use the following commands:

Factory .RBF (including bootloader):

  echo "Bitstream_compression=on" > ${HOME}/.run_rbf_temp_options_file
  quartus_cpf -d EPCQL1024 -m ASx4 --option=${HOME}/.run_rbf_temp_options_file -c design_name.sof design_name.pof
  quartus_cpf -c design_name.pof design_name.hexout
  nios2-elf-objcopy -I ihex -O binary design_name.hexout design_name.rbf
  truncate -s 40M design_name.rbf

User .RBF (excluding bootloader):

  echo "Bitstream_compression=on" > ${HOME}/.run_rbf_temp_options_file
  quartus_cpf -c --option=${HOME}/.run_rbf_temp_options_file design_name.sof design_name.rbf




These commands are already included in the 'run_rbf' shell script.
So to add the bootloader it takes more steps, as shown above. .POF files do include the bootloader, thats why the steps
.SOF -> .POF -> .HEXOUT -> .RBF   are taken.

To run the 'run_rbf' script:
  run_rbf unb2a --unb2_factory design_name    # <- this adds the bootloader for the factory image
  run_rbf unb2a design_name                   # <- no bootloader added (default)

