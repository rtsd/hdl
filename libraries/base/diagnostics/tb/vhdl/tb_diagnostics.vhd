--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for diagnostics
-- Description:
-- Usage:
-- > as 5
-- > run -a

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_diagnostics is
end entity tb_diagnostics;

architecture str of tb_diagnostics is
  constant c_sim          : boolean := true;
  constant clk_period     : time    := 5 ns;  -- 200 MHz

  constant c_nof_streams  : natural := 2;  -- any NATURAL number
  constant c_dat_w        : natural := 64;  -- 8..256

  signal tb_end           : std_logic := '0';
  signal clk              : std_logic := '0';
  signal rst              : std_logic := '1';

  signal sosi_loopback_arr: t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal siso_loopback_arr: t_dp_siso_arr(c_nof_streams - 1 downto 0);

  signal snk_diag_en      : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_diag_res     : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_diag_res_val : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_val_cnt      : t_slv_32_arr(c_nof_streams - 1 downto 0);

  signal src_diag_en      : std_logic_vector(c_nof_streams - 1 downto 0);
  signal src_val_cnt      : t_slv_32_arr(c_nof_streams - 1 downto 0);
 begin
  -- Run for 1us

  rst <= '0' after 100 ns;
  clk <= not clk or tb_end after clk_period / 2;

  gen_tb_processes: for i in 0 to c_nof_streams - 1 generate
    p_stimuli : process
    begin
      for rpt in 0 to 1 loop

        snk_diag_en(i) <= '0';
        src_diag_en(i) <= '0';

        wait for 100 ns;
        wait until rising_edge(clk);

        src_diag_en(i) <= '1';

        wait for 100 ns;
        wait until rising_edge(clk);

        snk_diag_en(i) <= '1';

        wait for 100 ns;
        wait until rising_edge(clk);

        src_diag_en(i) <= '0';

        wait for 100 ns;
        wait until rising_edge(clk);

        snk_diag_en(i) <= '0';
      end loop;
      wait for 100 ns;
      tb_end <= '1';
      wait for 100 ns;
      wait;
    end process;

  end generate;

  u_dut: entity WORK.diagnostics
    generic map (
      g_dat_w       => c_dat_w,
      g_nof_streams => c_nof_streams
       )
    port map (
      rst              => rst,
      clk              => clk,

      snk_out_arr      => siso_loopback_arr,
      snk_in_arr       => sosi_loopback_arr,
      snk_diag_en      => snk_diag_en,
      snk_diag_md      => (others => '1'),
      snk_diag_res     => snk_diag_res,
      snk_diag_res_val => snk_diag_res_val,
      snk_val_cnt      => snk_val_cnt,

      src_out_arr      => sosi_loopback_arr,
      src_in_arr       => siso_loopback_arr,
      src_diag_en      => src_diag_en,
      src_diag_md      => (others => '1'),
      src_val_cnt      => src_val_cnt
    );
end architecture str;
