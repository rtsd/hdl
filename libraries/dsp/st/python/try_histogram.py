###############################################################################
#
# Copyright 2019
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

# Author: Eric Kooistra

"""Try histogram

Usage:
> python try_histogram.py -h

"""

import argparse
import numpy as np
import matplotlib.pylab as plt

figNr = 1

# Parse arguments to derive user parameters
_parser = argparse.ArgumentParser('try_histogram')
_parser.add_argument('--Nbins', default=100, type=int, help='Number of bins in historgram')
_parser.add_argument('--dc', default=0.0, type=float, help='DC offset of the data')
_parser.add_argument('--ampl', default=1.0, type=float, help='Amplitude of the sinus data')
_parser.add_argument('--sigma', default=15.0, type=float, help='Standard deviation of the random data')
args = _parser.parse_args()

Nbins = args.Nbins
dc = args.dc
ampl = args.ampl
sigma = args.sigma

# Sinus
N = Nbins * 100
t = 2 * np.pi * np.arange(0, N) / N
a = dc + ampl*np.sin(t)

# Plot histogram
plt.figure(figNr)
plt.hist(a, bins=Nbins)
plt.xlabel('Sample values')
plt.ylabel('Occurance')
plt.title('Sinus (%d values)' % N)
plt.grid(True)
figNr += 1

# Gaussian noise
x = dc + sigma * np.random.randn(N)

# Plot histogram
plt.figure(figNr)
plt.hist(x, bins=Nbins)
plt.xlabel('Sample values')
plt.ylabel('Occurance')
plt.title('Gaussian noise (%d values)' % N)
plt.grid(True)
figNr += 1

# Show plots
plt.show()



