-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . test bench for common_variable_delay.vhd
-- Description:
-- . see common_variable_delay
-- --------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_str_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_variable_delay is
end tb_common_variable_delay;

architecture tb of tb_common_variable_delay is
  constant c_clk_period       : time    := 10 ns;
  constant c_trigger_interval : natural := 40;  -- in clk's
  constant c_trigger_latency  : natural := 1;  -- in clk's

  -- Use a delay > c_trigger_interval to check wiht exp_triggers_cnt that new
  -- triggers are ignored when a delay is already busy
  constant c_delay_arr        : t_natural_arr(0 to 5) := (0, 1, 3, 12, c_trigger_interval * 3, 10);

  signal stimuli_done : std_logic := '0';

  signal tb_end : std_logic := '0';
  signal rst    : std_logic;
  signal clk    : std_logic := '0';

  signal delay       : natural   := 0;
  signal enable      : std_logic := '0';
  signal trigger     : std_logic := '0';
  signal trigger_dly : std_logic := '0';

  -- Use triggers_cnt to verify that triggers only occur when enable = '1'
  -- and when the delay is not busy
  signal triggers_cnt     : natural := 0;
  signal exp_triggers_cnt : natural := c_delay_arr'length;
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 4;

  -- generate trigger pulse signal
  proc_common_gen_pulse(1, c_trigger_interval, '1', rst, clk, trigger);

  p_in_stimuli : process
  variable clk_cnt : natural := 0;
  begin
    delay  <= 0;
    enable <= '0';

    wait until rst = '0';

    -- If enable = 0, no trigger_dly is expected
    proc_common_wait_some_cycles(clk, c_trigger_interval * 3);

    -- Enable trigger output and count clk's between trigger and trigger_dly
    enable <= '1';
    for i in c_delay_arr'range loop
      delay <= c_delay_arr(i);
      clk_cnt := 0;
      proc_common_wait_until_lo_hi(clk, trigger);
      while trigger_dly = '0' loop
        clk_cnt := clk_cnt + 1;
        proc_common_wait_some_cycles(clk, 1);
      end loop;
      -- Verify that expected delay was applied
      assert clk_cnt = c_trigger_latency + delay
        report "delay failure, got " & int_to_str(clk_cnt) & ", expect " & int_to_str(c_trigger_latency + delay)
        severity ERROR;
      proc_common_wait_some_cycles(clk, 10);
    end loop;
    enable <= '0';

    proc_common_wait_some_cycles(clk, c_trigger_interval * 3);
    assert triggers_cnt = exp_triggers_cnt
      report "wrong number of trigger_dly."
      severity ERROR;

    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  triggers_cnt <= triggers_cnt + 1 when rising_edge(clk) and trigger_dly = '1';

  -- device under test
  u_dut : entity work.common_variable_delay
  port map (
    rst       => rst,
    clk       => clk,

    delay     => delay,
    enable    => enable,
    in_pulse  => trigger,
    out_pulse => trigger_dly
  );
end tb;
