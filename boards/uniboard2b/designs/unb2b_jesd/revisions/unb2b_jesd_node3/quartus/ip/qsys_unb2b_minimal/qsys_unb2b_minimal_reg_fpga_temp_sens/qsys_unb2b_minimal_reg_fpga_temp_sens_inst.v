	qsys_unb2b_minimal_reg_fpga_temp_sens #(
		.g_adr_w (NATURAL_VALUE_FOR_g_adr_w),
		.g_dat_w (NATURAL_VALUE_FOR_g_dat_w)
	) u0 (
		.coe_address_export   (_connected_to_coe_address_export_),   //  output,  width = g_adr_w,      address.export
		.coe_clk_export       (_connected_to_coe_clk_export_),       //  output,        width = 1,          clk.export
		.avs_mem_address      (_connected_to_avs_mem_address_),      //   input,  width = g_adr_w,          mem.address
		.avs_mem_write        (_connected_to_avs_mem_write_),        //   input,        width = 1,             .write
		.avs_mem_writedata    (_connected_to_avs_mem_writedata_),    //   input,  width = g_dat_w,             .writedata
		.avs_mem_read         (_connected_to_avs_mem_read_),         //   input,        width = 1,             .read
		.avs_mem_readdata     (_connected_to_avs_mem_readdata_),     //  output,  width = g_dat_w,             .readdata
		.coe_read_export      (_connected_to_coe_read_export_),      //  output,        width = 1,         read.export
		.coe_readdata_export  (_connected_to_coe_readdata_export_),  //   input,  width = g_dat_w,     readdata.export
		.coe_reset_export     (_connected_to_coe_reset_export_),     //  output,        width = 1,        reset.export
		.csi_system_clk       (_connected_to_csi_system_clk_),       //   input,        width = 1,       system.clk
		.csi_system_reset     (_connected_to_csi_system_reset_),     //   input,        width = 1, system_reset.reset
		.coe_write_export     (_connected_to_coe_write_export_),     //  output,        width = 1,        write.export
		.coe_writedata_export (_connected_to_coe_writedata_export_)  //  output,  width = g_dat_w,    writedata.export
	);

