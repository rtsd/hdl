-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_xonoff is
  generic (
    g_nof_streams     : natural := 1;
    g_combine_streams : boolean := false;
    g_bypass          : boolean := false;
    g_default_value   : std_logic := '1';
    g_timeout_time    : natural := 0;
    g_sim             : boolean := false
  );
  port (
    -- Memory-mapped clock domain
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;

    -- Streaming clock domain
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;

    -- ST sinks
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);
    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    -- Optional override (w.r.t MM setting) to force X Off (by assigning '1')
    force_xoff_arr : in std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0')
  );
end mms_dp_xonoff;

architecture str of mms_dp_xonoff is
  constant c_nof_ctrl_streams : natural := sel_a_b(g_combine_streams, 1, g_nof_streams);
  constant c_reg_adr_w        : natural := 1;

  signal reg_mosi_arr  : t_mem_mosi_arr(c_nof_ctrl_streams - 1 downto 0);
  signal reg_miso_arr  : t_mem_miso_arr(c_nof_ctrl_streams - 1 downto 0);

  signal xonoff_reg    : std_logic_vector(c_nof_ctrl_streams - 1 downto 0);
  signal xonoff_reg_i  : std_logic_vector(g_nof_streams - 1 downto 0);
  signal src_in_arr_i  : t_dp_siso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_ctrl_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_reg : for i in 0 to c_nof_ctrl_streams - 1 generate
    gen_no_timeout : if g_timeout_time = 0 generate
      u_reg : entity work.dp_xonoff_reg
      generic map(
        g_default_value => g_default_value
      )
      port map (
        -- Clocks and reset
        mm_rst       => mm_rst,
        mm_clk       => mm_clk,
        st_rst       => dp_rst,
        st_clk       => dp_clk,

        -- Memory Mapped Slave in mm_clk domain
        sla_in       => reg_mosi_arr(i),
        sla_out      => reg_miso_arr(i),

        -- MM registers in dp_clk domain
        -- . control
        xonoff_reg   => xonoff_reg(i downto i)
      );
    end generate;

    gen_with_timeout : if g_timeout_time > 0 generate
      u_reg : entity work.dp_xonoff_reg_timeout
      generic map(
        g_default_value => g_default_value,
        g_mm_timeout    => g_timeout_time,
        g_sim           => g_sim
      )
      port map (
        -- Clocks and reset
        mm_rst       => mm_rst,
        mm_clk       => mm_clk,
        st_rst       => dp_rst,
        st_clk       => dp_clk,

        -- Memory Mapped Slave in mm_clk domain
        sla_in       => reg_mosi_arr(i),
        sla_out      => reg_miso_arr(i),

        -- MM registers in dp_clk domain
        -- . control
        xonoff_reg   => xonoff_reg(i downto i)
      );
    end generate;
  end generate;

  xonoff_reg_i <= xonoff_reg when g_combine_streams = false else (others => xonoff_reg(0));

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    -- The xon signal is AND-ed with the register value.
    src_in_arr_i(i).ready <= src_in_arr(i).ready;
    src_in_arr_i(i).xon   <= src_in_arr(i).xon and xonoff_reg_i(i);

    u_dp_xonoff : entity work.dp_xonoff
    generic map (
      g_bypass => g_bypass
    )
    port map (
      rst      => dp_rst,
      clk      => dp_clk,

      in_siso  => snk_out_arr(i),
      in_sosi  => snk_in_arr(i),
      out_siso => src_in_arr_i(i),
      out_sosi => src_out_arr(i),

      force_xoff => force_xoff_arr(i)
    );
  end generate;
end str;
