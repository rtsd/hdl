-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Generate a data stream that carries blocks to a FIFO and verify that the
--   FIFO is read out regularly by dp_throttle_sop
-- Description:
-- . Verify in the wave window that the first 10 blocks are rate-limited at a
--   regular pace of c_period, and that the second 10 blocks are pass through
--   without throttling as the minimum of c_period is met.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_throttle_sop is
end tb_dp_throttle_sop;

architecture tb of tb_dp_throttle_sop is
  constant c_period           : natural := 100;

  constant c_clk_period       : time    := 10 ns;

  constant c_gen_block_size   : natural := 20;
  constant c_gen_data_w       : natural := 32;

  signal tb_end               : std_logic := '0';
  signal rst                  : std_logic;
  signal clk                  : std_logic := '1';

  signal gen_en               : std_logic := '1';
  signal gen_src_out          : t_dp_sosi;
  signal gen_src_in           : t_dp_siso;

  signal fifo_src_out         : t_dp_sosi;
  signal fifo_src_in          : t_dp_siso;
begin
  -----------------------------------------------------------------------------
  -- Clock and reset generation
  -----------------------------------------------------------------------------
  rst  <= '1', '0' after c_clk_period * 7;
  clk  <=  not clk or tb_end after c_clk_period / 2;

  -----------------------------------------------------------------------------
  -- Test data generation:
  -- . 10 blocks as fast as possible (throttle on)
  -- . 10 blocks at lower rate (throttle off)
  -----------------------------------------------------------------------------
  p_stimuli : process
  begin
    wait until rst = '0';
    wait for 10 * c_clk_period;

    for i in 0 to 10 loop
      proc_dp_gen_block_data(1, true, c_gen_data_w, c_gen_data_w, 0, 0, 0, c_gen_block_size, 0, 0, '1', x"00000000", clk, gen_en, gen_src_in, gen_src_out);
    end loop;

    for i in 0 to 10 loop
      proc_dp_gen_block_data(1, true, c_gen_data_w, c_gen_data_w, 0, 0, 0, c_gen_block_size, 0, 0, '1', x"00000000", clk, gen_en, gen_src_in, gen_src_out);
      wait for 2 * c_period * c_clk_period;
    end loop;

    wait for 10 * c_clk_period;
    tb_end <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- FIFO acts as sink that dp_throttle_sop provides a constant flow for
  -----------------------------------------------------------------------------
  u_dp_fifo_sc : entity work.dp_fifo_sc
  generic map (
    g_data_w      => c_gen_data_w,
    g_use_bsn     => false,
    g_use_empty   => false,
    g_use_channel => false,
    g_use_error   => false,
    g_use_sync    => false,
    g_use_ctrl    => true,
    g_fifo_size   => 100
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => gen_src_in,
    snk_in      => gen_src_out,
    src_in      => fifo_src_in,
    src_out     => fifo_src_out
  );

  -----------------------------------------------------------------------------
  -- DUT
  -----------------------------------------------------------------------------
  u_dp_throttle_sop : entity work.dp_throttle_sop
  generic map (
    g_period    => c_period
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => fifo_src_in,
    snk_in      => fifo_src_out
  );
end tb;
