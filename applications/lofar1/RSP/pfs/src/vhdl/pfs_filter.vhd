-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Polyphase FIR filter
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE, common_lib, common_mult_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity pfs_filter is
  generic (
    g_coef_w        : natural;
    g_out_w         : natural;
    g_taps_w        : natural;
    g_nof_taps      : natural
  );
  port (
    clk             : in std_logic;
    taps            : in std_logic_vector(g_nof_taps * g_taps_w - 1 downto 0);
    coefs           : in std_logic_vector(g_nof_taps * g_coef_w - 1 downto 0);
    result          : out std_logic_vector(g_out_w - 1 downto 0)
  );
end pfs_filter;

architecture rtl of pfs_filter is
  type type_res is array (0 to 7) of std_logic_vector(g_coef_w + g_taps_w + 1 - 1 downto 0);

  signal res      : type_res;
  signal res_0    : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_1    : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_2    : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_3    : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal add_a    : std_logic_vector(g_coef_w + g_taps_w + 3 - 1 downto 0);
  signal add_b    : std_logic_vector(g_coef_w + g_taps_w + 3 - 1 downto 0);
  signal add_c    : std_logic_vector(g_coef_w + g_taps_w + 4 - 1 downto 0);
begin
  registers : process (clk)
  begin
    if rising_edge(clk) then
      result <= add_c(add_c'high downto add_c'length - result'length);
      add_a  <= std_logic_vector(resize(signed(res_0), add_a'length) + signed(res_1));
      add_b  <= std_logic_vector(resize(signed(res_2), add_b'length) + signed(res_3));
    end if;
  end process;

  add_c <= std_logic_vector(SHIFT_LEFT((resize(signed(add_a), add_c'length) + signed(add_b)), 4));
--  nxt_result <= STD_LOGIC_VECTOR(RESIZE(SIGNED(add_c),

  gen : for i in 0 to 7 generate
    --MULT_ADD : ENTITY common_lib.common_mult_add(rtl)
    --MULT_ADD : ENTITY common_lib.common_mult_add(virtex)
    MULT_ADD : entity common_mult_lib.common_mult_add  -- rtl
    generic map (
      g_in_a_w     => g_taps_w,
      g_in_b_w     => g_coef_w,
      g_out_dat_w  => g_coef_w + g_taps_w + 1,
      g_add_sub    => "ADD",
      g_pipeline   => 3
    )
    port map (
      clk     => clk,
      in_a0   => taps (g_taps_w * (2 * i + 1) - 1 downto g_taps_w * 2 * i),
      in_b0   => coefs(g_coef_w * (2 * i + 1) - 1 downto g_coef_w * 2 * i),
      in_a1   => taps (g_taps_w * (2 * i + 2) - 1 downto g_taps_w * (2 * i + 1)),
      in_b1   => coefs(g_coef_w * (2 * i + 2) - 1 downto g_coef_w * (2 * i + 1)),
      out_dat => res(i)
    );
  end generate;

  pipe : process (clk)
  begin
    if rising_edge(clk) then
      res_0 <= std_logic_vector(resize(signed(res(0)), res_0'length) + resize(signed(res(1)), res_0'length));
      res_1 <= std_logic_vector(resize(signed(res(2)), res_0'length) + resize(signed(res(3)), res_0'length));
      res_2 <= std_logic_vector(resize(signed(res(4)), res_0'length) + resize(signed(res(5)), res_0'length));
      res_3 <= std_logic_vector(resize(signed(res(6)), res_0'length) + resize(signed(res(7)), res_0'length));
    end if;
  end process;
end rtl;
