--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Outputs all unique pair permutations of the words at the inputs.
-- Description:
-- . c_nof_permutations = g_nof_inputs*(g_nof_inputs+1)/2
-- . Example permutations of [0,1,2]: [[0,0],[0,1],[0,2],[1,1],[1,2],[2,2]]
--   . Note the order of the permutations; low pairs to high pairs
-- . All inputs to permutate (snk_in_arr) must be valid on one and the same clock cycle.
-- . Process p_permu_wires is wires only and provides all permutations
--   instantaniously, in parallel:
--   . Low pair  [snk_in_arr[0],             snk_in_arr[0]             ] on permu_out_2arr_2[0]
--   . ..
--   . ..
--   . High pair [snk_in_arr[g_nof_inputs-1],snk_in_arr[g_nof_inputs-1]] on permu_out_2arr_2[g_nof_inputs-1]
-- . Both the input and the output of process p_permu_wires are registered by default.

entity corr_permutator is
  generic (
    g_nof_inputs      : natural := 8;
    g_register_input  : boolean := true;
    g_register_output : boolean := true
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

    src_out_2arr_2 : out t_dp_sosi_2arr_2(g_nof_inputs * (g_nof_inputs + 1) / 2 - 1 downto 0)  -- Array of pairs
  );
end corr_permutator;

architecture rtl of corr_permutator is
  constant c_nof_permutations : natural := g_nof_inputs * (g_nof_inputs + 1) / 2;

  signal reg_snk_in_arr       : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal nxt_reg_snk_in_arr   : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  type t_permu_in_2arr is array(g_nof_inputs - 1 downto 0) of t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal permu_in_2arr        : t_permu_in_2arr;
  signal permu_out_2arr_2     : t_dp_sosi_2arr_2(c_nof_permutations - 1 downto 0);
  signal nxt_src_out_2arr_2   : t_dp_sosi_2arr_2(c_nof_permutations - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Input wiring: copy the inputs g_nof_inputs times
  -- . Direct wiring if we can output all permutations in parallel
  -- . Wire via input buffer if we need to serialize the output permutations
  -----------------------------------------------------------------------------
  gen_input: for i in 0 to g_nof_inputs - 1 generate
    gen_input: for j in 0 to g_nof_inputs - 1 generate
        permu_in_2arr(i)(j) <= reg_snk_in_arr(i);
    end generate;
  end generate;

  gen_input_register: if g_register_input = true generate
    nxt_reg_snk_in_arr <= snk_in_arr;

    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        reg_snk_in_arr  <= (others => c_dp_sosi_rst);
      elsif rising_edge(clk) then
        reg_snk_in_arr  <= nxt_reg_snk_in_arr;
      end if;
    end process;
  end generate;

  no_input_register: if g_register_input = false generate
    reg_snk_in_arr <= snk_in_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- Permutation wiring, 4-input example:
  -- ====================================
  --         0    1    2    3    4    5    6    7    8    9 <-- permu_out_2arr_2[c_nof_permutations][2]
  --  _      |    |    |    |    |    |    |    |    |    |
  -- |0|0-->[0]   |    |    |    |    |    |    |    |    |
  -- | |1------->[0]   |    |    |    |    |    |    |    |
  -- | |2---------|-->[0]   |    |    |    |    |    |    |
  -- |_|3---------|----|-->[0]   |    |    |    |    |    |
  -- |1|0------->[1]   |    |    |    |    |    |    |    |
  -- | |1--------------|----|-->[1]   |    |    |    |    |
  -- | |2--------------|----|------->[1]   |    |    |    |
  -- |_|3--------------|----|---------|-->[1]   |    |    |
  -- |2|0------------>[2]   |         |    |    |    |    |
  -- | |1-------------------|------->[2]   |    |    |    |
  -- | |2-------------------|--------------|-->[2]   |    |
  -- |_|3-------------------|--------------|------->[2]   |
  -- |3|0----------------->[3]             |         |    |
  -- | |1-------------------------------->[3]        |    |
  -- | |2------------------------------------------>[3]   |
  -- |_|3----------------------------------------------->[3]
  --  ^
  --  |
  --  permu_in_2arr[g_nof_inputs][g_nof_inputs]
  --
  -- Observe the following:
  -- . Inverse indexing: E.g. permutation[5], pair index 1 gets assigned
  --   indices opposite to those of pair index 0:
  --   . permu_out_2arr_2(5)(0) <= permu_in_arr(1)(2);
  --     permu_out_2arr_2(5)(1) <= permu_in_arr(2)(1);
  -- . The number of inputs that carry duplicates increments by one
  --   for each input increment (only pair index 0 shown):
  --   . permu_out_2arr_2(0)(0) <= permu_in_arr(0)(0); --permu_in_arr(0): 0 duplicates
  --     permu_out_2arr_2(1)(0) <= permu_in_arr(0)(1);
  --     permu_out_2arr_2(2)(0) <= permu_in_arr(0)(2);
  --     permu_out_2arr_2(3)(0) <= permu_in_arr(0)(3);
  --     -- Duplicate              permu_in_arr(1)(0); --permu_in_arr(1): 1 duplicate
  --     permu_out_2arr_2(4)(0) <= permu_in_arr(1)(1);
  --     permu_out_2arr_2(5)(0) <= permu_in_arr(1)(2);
  --     permu_out_2arr_2(6)(0) <= permu_in_arr(1)(3);
  --     -- Duplicate              permu_in_arr(2)(0); --permu_in_arr(2): 2 duplicates
  --     -- Duplicate              permu_in_arr(2)(1);
  --     permu_out_2arr_2(7)(0) <= permu_in_arr(2)(2);
  --     permu_out_2arr_2(8)(0) <= permu_in_arr(2)(3);
  --     -- Duplicate              permu_in_arr(3)(0); --permu_in_arr(3): 3 duplicates
  --     -- Duplicate              permu_in_arr(3)(1);
  --     -- Duplicate              permu_in_arr(3)(2);
  --     permu_out_2arr_2(9)(0) <= permu_in_arr(3)(3);
  -----------------------------------------------------------------------------
  p_permu_wires: process(permu_in_2arr)
    variable v_out_index  : natural;
    variable v_duplicates : natural;
  begin
    v_out_index  := 0;
    v_duplicates := 0;
    for i in 0 to g_nof_inputs - 1 loop
      v_duplicates := i;
      for j in 0 to g_nof_inputs - 1 loop
        -- Wire up our permutation pair by using inverse indices (j,i) and (i,j)
        permu_out_2arr_2(v_out_index)(0) <= permu_in_2arr(i)(j);
        permu_out_2arr_2(v_out_index)(1) <= permu_in_2arr(j)(i);
        if v_duplicates = 0 then
          -- We assigned a unique pair. Keep incrementing the index.
          v_out_index := v_out_index + 1;
        else
          -- We just wired a duplicate permutation pair. Don't increment
          -- v_out_index so that permutation pair will get re-assigned in the
          -- next loop iteration.
          v_duplicates := v_duplicates - 1;
        end if;
      end loop;
    end loop;
  end process;

  -----------------------------------------------------------------------------
  -- Output wiring
  -----------------------------------------------------------------------------
  gen_output_register: if g_register_output = true generate
    nxt_src_out_2arr_2 <= permu_out_2arr_2;

    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        src_out_2arr_2  <= (others => (others => c_dp_sosi_rst));
      elsif rising_edge(clk) then
        src_out_2arr_2  <= nxt_src_out_2arr_2;
      end if;
    end process;
  end generate;

  no_output_register: if g_register_output = false generate
    src_out_2arr_2 <= permu_out_2arr_2;
  end generate;
end rtl;
