--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Technology independent component for DDR memory access.
-- Description:
--   The component also supports different types of DDR, so DDR3 and DDR4.
-- Remark:

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tech_ddr_pkg.all;

entity tech_ddr is
  generic (
    g_sim_model            : boolean := false;  -- when FALSE use IP and external DDR3 model, else when TRUE: use fast behavioural model, requires no external memory (uses memory array).
    g_technology           : natural := c_tech_select_default;
    g_tech_ddr             : t_c_tech_ddr
  );
  port (
    -- PLL reference clock
    ref_clk           : in    std_logic;
    ref_rst           : in    std_logic;

    -- Controller user interface
    ctlr_gen_clk      : out   std_logic;
    ctlr_gen_rst      : out   std_logic;
    ctlr_gen_clk_2x   : out   std_logic;
    ctlr_gen_rst_2x   : out   std_logic;

    ctlr_mosi         : in    t_mem_ctlr_mosi;
    ctlr_miso         : out   t_mem_ctlr_miso;

    term_ctrl_out     : out   t_tech_ddr3_phy_terminationcontrol;
    term_ctrl_in      : in    t_tech_ddr3_phy_terminationcontrol := c_tech_ddr3_phy_terminationcontrol_rst;

    -- DDR3 PHY interface
    phy3_in            : in    t_tech_ddr3_phy_in := c_tech_ddr3_phy_in_x;
    phy3_io            : inout t_tech_ddr3_phy_io;
    phy3_ou            : out   t_tech_ddr3_phy_ou;

    -- DDR4 PHY interface
    phy4_in            : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    phy4_io            : inout t_tech_ddr4_phy_io;
    phy4_ou            : out   t_tech_ddr4_phy_ou
  );
end tech_ddr;

architecture str of tech_ddr is
begin
  -----------------------------------------------------------------------------
  -- Technology IP cores
  -----------------------------------------------------------------------------
  gen_ip: if g_sim_model = false generate
    gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
      u0 : entity work.tech_ddr_stratixiv
      generic map (g_tech_ddr)
      port map (ref_clk, ref_rst,
                ctlr_gen_clk, ctlr_gen_rst, ctlr_gen_clk_2x, ctlr_gen_rst_2x,
                ctlr_mosi, ctlr_miso, term_ctrl_out, term_ctrl_in,
                phy3_in, phy3_io, phy3_ou);
    end generate;

    gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
      u0 : entity work.tech_ddr_arria10
      generic map (g_tech_ddr)
      port map (ref_clk, ref_rst,
                ctlr_gen_clk, ctlr_gen_rst,
                ctlr_mosi, ctlr_miso,
                phy4_in, phy4_io, phy4_ou);
    end generate;

    gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
      u0 : entity work.tech_ddr_arria10_e3sge3
      generic map (g_tech_ddr)
      port map (ref_clk, ref_rst,
                ctlr_gen_clk, ctlr_gen_rst,
                ctlr_mosi, ctlr_miso,
                phy4_in, phy4_io, phy4_ou);
    end generate;

    gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
      u0 : entity work.tech_ddr_arria10_e1sg
      generic map (g_tech_ddr)
      port map (ref_clk, ref_rst,
                ctlr_gen_clk, ctlr_gen_rst,
                ctlr_mosi, ctlr_miso,
                phy4_in, phy4_io, phy4_ou);
    end generate;

    gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
      u0 : entity work.tech_ddr_arria10_e2sg
      generic map (g_tech_ddr)
      port map (ref_clk, ref_rst,
                ctlr_gen_clk, ctlr_gen_rst,
                ctlr_mosi, ctlr_miso,
                phy4_in, phy4_io, phy4_ou);
    end generate;

  end generate;

  -----------------------------------------------------------------------------
  -- Functional simulation model of both the DDR controller and the DDR memory
  -----------------------------------------------------------------------------
  gen_sim_ddr : if g_sim_model = true generate
    u_sim_ddr : entity work.sim_ddr
    generic map (
      g_tech_ddr        => g_tech_ddr
    )
    port map (
      -- PLL reference clock
      ref_clk           => ref_clk,
      ref_rst           => ref_rst,

      -- Controller user interface
      ctlr_gen_clk      => ctlr_gen_clk,
      ctlr_gen_rst      => ctlr_gen_rst,
      ctlr_gen_clk_2x   => ctlr_gen_clk_2x,
      ctlr_gen_rst_2x   => ctlr_gen_rst_2x,

      ctlr_mosi         => ctlr_mosi,
      ctlr_miso         => ctlr_miso
    );
  end generate;
end str;
