library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_rotate is
  generic (
    g_in_dat_w      : integer;
    g_nof_fir       : integer
  );
  port (
    clk             : in std_logic;
    rst             : in std_logic;
    in_dat_x        : in std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_y        : in std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val          : in std_logic;
    in_sync         : in std_logic;
    out_dat_x       : out std_logic_vector(g_in_dat_w - 1 downto 0);
    out_dat_y       : out std_logic_vector(g_in_dat_w - 1 downto 0);
    out_val         : out std_logic_vector(g_nof_fir - 1 downto 0);
    out_sync        : out std_logic
  );
end pfs_rotate;
