-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_pipeline is
  generic (
    g_representation : string  := "SIGNED";  -- or "UNSIGNED"
    g_pipeline       : natural := 1;  -- 0 for wires, > 0 for registers,
    g_reset_value    : integer := 0;
    g_in_dat_w       : natural := 8;
    g_out_dat_w      : natural := 9
  );
  port (
    rst     : in  std_logic := '0';
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    in_clr  : in  std_logic := '0';
    in_en   : in  std_logic := '1';
    in_dat  : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    out_dat : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
end common_pipeline;

architecture rtl of common_pipeline is
  constant c_reset_value : std_logic_vector(out_dat'range) := TO_SVEC(g_reset_value, out_dat'length);

  type t_out_dat is array (natural range <>) of std_logic_vector(out_dat'range);

  signal out_dat_p  : t_out_dat(0 to g_pipeline) := (others => c_reset_value);
begin
  gen_pipe_n : if g_pipeline > 0 generate
    p_clk : process(clk, rst)
    begin
      if rst = '1' then
        out_dat_p(1 to g_pipeline) <= (others => c_reset_value);
      elsif rising_edge(clk) then
        if clken = '1' then
          if in_clr = '1' then
            out_dat_p(1 to g_pipeline) <= (others => c_reset_value);
          elsif in_en = '1' then
            out_dat_p(1 to g_pipeline) <= out_dat_p(0 to g_pipeline-1);
          end if;
        end if;
      end if;
    end process;
  end generate;

  out_dat_p(0) <= RESIZE_SVEC(in_dat, out_dat'length) when g_representation =  "SIGNED" else
                  RESIZE_UVEC(in_dat, out_dat'length) when g_representation = "UNSIGNED";

  out_dat <= out_dat_p(g_pipeline);
end rtl;
