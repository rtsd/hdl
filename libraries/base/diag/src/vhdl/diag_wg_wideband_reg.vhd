-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide single MM slave register for diag_wg_wideband
-- Description:
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |          nof_samples[15:0]        |       xxx       |   Mode[7:0]     |  0
--  |-----------------------------------------------------------------------|
--  |                xxx                |            Phase[15:0]            |  1
--  |-----------------------------------------------------------------------|
--  |x|                             Freq[30:0]                              |  2
--  |-----------------------------------------------------------------------|
--  |                xxx                |             Ampl[15:0]            |  3
--  |-----------------------------------------------------------------------|
--
--   The new mm_wg_ctrl settings are passed on to st_wg_ctrl after the write
--   event to the Mode field, i.e. to wi=0. Hence wi=0 must be written last.
--
-- Remark:
-- . This diag_wg_wideband_reg also suits diag_wg. Hence no need to make a
--   diag_wg_reg.vhd.

library IEEE, common_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;

entity diag_wg_wideband_reg is
  generic (
    g_cross_clock_domain : boolean := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst      : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk      : in  std_logic;  -- memory-mapped bus clock
    st_rst      : in  std_logic;  -- reset synchronous with st_clk
    st_clk      : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in      : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out     : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_wg_ctrl  : out t_diag_wg;  -- WG control write port
    st_mon_ctrl : in  t_diag_wg  -- WG control read port, for currently active control
  );
end diag_wg_wideband_reg;

architecture rtl of diag_wg_wideband_reg is
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 2,
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 2**2,
                                  init_sl  => '0');

  -- Registers in mm_clk domain
  signal mm_wg_ctrl           : t_diag_wg;
  signal mm_wg_ctrl_mode_wr   : std_logic;

  signal mm_mon_ctrl          : t_diag_wg;

  -- Registers in st_clk domain
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out              <= c_mem_miso_rst;
      -- Access event, register values
      mm_wg_ctrl           <= c_diag_wg_rst;
      mm_wg_ctrl_mode_wr   <= '0';
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Access event defaults
      mm_wg_ctrl_mode_wr <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            mm_wg_ctrl_mode_wr      <= '1';
            mm_wg_ctrl.mode         <= sla_in.wrdata( 7 downto  0);  -- =  8 = c_diag_wg_mode_w
            mm_wg_ctrl.nof_samples  <= sla_in.wrdata(31 downto 16);  -- = 16 = c_diag_wg_nof_samples_w
          when 1 =>
            mm_wg_ctrl.phase        <= sla_in.wrdata(15 downto  0);  -- = 16 = c_diag_wg_phase_w
          when 2 =>
            mm_wg_ctrl.freq         <= sla_in.wrdata(30 downto  0);  -- = 31 = c_diag_wg_freq_w
          when 3 =>
            mm_wg_ctrl.ampl         <= sla_in.wrdata(16 downto  0);  -- = 17 = c_diag_wg_ampl_w
          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            sla_out.rddata( 7 downto  0) <= mm_mon_ctrl.mode;  -- =  8 = c_diag_wg_mode_w
            sla_out.rddata(31 downto 16) <= mm_mon_ctrl.nof_samples;  -- = 16 = c_diag_wg_nof_samples_w
          when 1 =>
            sla_out.rddata(15 downto  0) <= mm_mon_ctrl.phase;  -- = 16 = c_diag_wg_phase_w
          when 2 =>
            sla_out.rddata(30 downto  0) <= mm_mon_ctrl.freq;  -- = 31 = c_diag_wg_freq_w
          when 3 =>
            sla_out.rddata(16 downto  0) <= mm_mon_ctrl.ampl;  -- = 17 = c_diag_wg_ampl_w
          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate  -- so mm_clk = st_clk
    -- Write: MM to ST clock domain
    p_st_clk : process(st_rst, st_clk)
    begin
      if st_rst = '1' then
        st_wg_ctrl.mode <= c_diag_wg_rst.mode;
      elsif rising_edge(st_clk) then
        if mm_wg_ctrl_mode_wr = '1' then
          st_wg_ctrl.mode <= mm_wg_ctrl.mode;
        end if;
      end if;
    end process;

    -- Read: ST to MM clock domain
    mm_mon_ctrl <= st_mon_ctrl;
  end generate;

  -- Write: MM to ST clock domain
  gen_cross_wr : if g_cross_clock_domain = true generate
    -- Assume diag WG mode gets written last, so when diag WG mode is transfered properly to the st_clk domain, then
    -- the other diag WG control fields are stable as well
    u_mode : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_new      => mm_wg_ctrl_mode_wr,  -- when '1' then new in_dat is available after g_in_new_latency
      in_dat      => mm_wg_ctrl.mode,
      in_done     => OPEN,  -- pulses when no more pending in_new
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_wg_ctrl.mode,
      out_new     => open  -- when '1' then the out_dat was updated with in_dat due to in_new
    );
  end generate;

  -- The other wg_ctrl only take effect in diag_wg after the mode has been set
  st_wg_ctrl.nof_samples <= mm_wg_ctrl.nof_samples;
  st_wg_ctrl.freq        <= mm_wg_ctrl.freq;
  st_wg_ctrl.phase       <= mm_wg_ctrl.phase;
  st_wg_ctrl.ampl        <= mm_wg_ctrl.ampl;

  -- Read: ST to MM clock domain
  gen_cross_rd : if g_cross_clock_domain = true generate
    u_mode : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_mon_ctrl.mode,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_ctrl.mode
    );

    u_nof_samples : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_mon_ctrl.nof_samples,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_ctrl.nof_samples
    );

    u_freq : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_mon_ctrl.freq,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_ctrl.freq
    );

    u_phase : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_mon_ctrl.phase,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_ctrl.phase
    );

    u_ampl : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_mon_ctrl.ampl,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_ctrl.ampl
    );
  end generate;
end rtl;
