--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, dp_lib, mdio_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mdio_lib.mdio_pkg.all;
use mdio_lib.mdio_vitesse_vsc8486_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_tr_xaui is
  generic (
    g_sim             : boolean := false;
    g_technology      : natural := c_tech_select_default;
    g_nof_xaui        : natural := 1;  -- Up to 3 (hard XAUI only) supported
    g_use_mdio        : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                  : in  std_logic;  -- 156.25 MHz
    tr_rst                  : in  std_logic;

    -- Calibration & reconfiguration clock
    cal_rec_clk             : in  std_logic;

    -- MM interface
    mm_clk                  : in  std_logic := '0';
    mm_rst                  : in  std_logic := '0';

    diagnostics_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    diagnostics_miso        : out t_mem_miso;

    xaui_mosi               : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso               : out t_mem_miso;

    mdio_mosi_arr           : in  t_mem_mosi_arr(g_nof_xaui - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr           : out t_mem_miso_arr(g_nof_xaui - 1 downto 0);

    -- Streaming TX interfaces
    tx_clk_arr              : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- 156.25 MHz, externally connect tr_clk to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    tx_rst_arr              : out std_logic_vector(g_nof_xaui - 1 downto 0);
    tx_sosi_arr             : in  t_dp_sosi_arr(g_nof_xaui - 1 downto 0) := (others => c_dp_sosi_rst);
    tx_siso_arr             : out t_dp_siso_arr(g_nof_xaui - 1 downto 0);

    -- Streaming RX interfaces
    rx_clk_arr_out          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- recovered clock per XAUI
    rx_clk_arr_in           : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr              : out std_logic_vector(g_nof_xaui - 1 downto 0);
    rx_sosi_arr             : out t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
    rx_siso_arr             : in  t_dp_siso_arr(g_nof_xaui - 1 downto 0) := (others => c_dp_siso_rst);

    --Serial I/O interface
    xaui_tx_arr             : out t_xaui_arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr             : in  t_xaui_arr(g_nof_xaui - 1 downto 0);

    -- MDIO interface
    mdio_rst                : out std_logic;
    mdio_mdc_arr            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_in_arr        : in  std_logic_vector(g_nof_xaui - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr       : out std_logic_vector(g_nof_xaui - 1 downto 0)
  );
end mms_tr_xaui;

architecture wrap of mms_tr_xaui is
  constant c_nof_select    : natural := 2;
  constant c_nof_select_w  : natural := ceil_log2(c_nof_select);
  constant c_sel_user      : natural := 0;
  constant c_sel_diag      : natural := 1;

  type t_select_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(c_nof_select - 1 downto 0);
  type t_select_siso_2arr is array (integer range <>) of t_dp_siso_arr(c_nof_select - 1 downto 0);

  type t_select_arr is array (integer range <>) of std_logic_vector(c_nof_select_w - 1 downto 0);

  signal i_tx_rst_arr             : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal i_rx_rst_arr             : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal diagnostics_rx_sosi_arr  : t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
  signal diagnostics_rx_siso_arr  : t_dp_siso_arr(g_nof_xaui - 1 downto 0);

  signal diagnostics_tx_sosi_arr  : t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
  signal diagnostics_tx_siso_arr  : t_dp_siso_arr(g_nof_xaui - 1 downto 0);

  signal diagnostics_tx_en_arr    : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal diagnostics_rx_en_arr    : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal mux_in_siso_2arr         : t_select_siso_2arr(g_nof_xaui - 1 downto 0);
  signal mux_in_sosi_2arr         : t_select_sosi_2arr(g_nof_xaui - 1 downto 0);

  signal mux_out_sosi_arr         : t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
  signal mux_out_siso_arr         : t_dp_siso_arr(g_nof_xaui - 1 downto 0);

  signal demux_in_siso_arr        : t_dp_siso_arr(g_nof_xaui - 1 downto 0);
  signal demux_in_sosi_arr        : t_dp_sosi_arr(g_nof_xaui - 1 downto 0);

  signal demux_out_sosi_2arr      : t_select_sosi_2arr(g_nof_xaui - 1 downto 0);
  signal demux_out_siso_2arr      : t_select_siso_2arr(g_nof_xaui - 1 downto 0);

  signal mux_select_arr           : t_select_arr(g_nof_xaui - 1 downto 0);
  signal nxt_mux_select_arr       : t_select_arr(g_nof_xaui - 1 downto 0);

  signal demux_select_arr         : t_select_arr(g_nof_xaui - 1 downto 0);
  signal nxt_demux_select_arr     : t_select_arr(g_nof_xaui - 1 downto 0);
begin
  tx_rst_arr <= i_tx_rst_arr;
  rx_rst_arr <= i_rx_rst_arr;

  u_tr_xaui: entity work.tr_xaui
  generic map (
    g_technology       => g_technology,
    g_sim              => g_sim,
    g_use_mdio         => g_use_mdio,
    g_nof_xaui         => g_nof_xaui
  )
  port map (
    tr_clk             => tr_clk,
    tr_rst             => tr_rst,

    cal_rec_clk        => cal_rec_clk,

    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    xaui_mosi          => xaui_mosi,
    xaui_miso          => xaui_miso,

    mdio_mosi_arr      => mdio_mosi_arr,
    mdio_miso_arr      => mdio_miso_arr,

    --Parallel data
    tx_clk_arr         => tx_clk_arr,
    tx_rst_arr         => i_tx_rst_arr,
    tx_sosi_arr        => mux_out_sosi_arr,
    tx_siso_arr        => mux_out_siso_arr,

    rx_clk_arr_out     => rx_clk_arr_out,
    rx_clk_arr_in      => rx_clk_arr_in,
    rx_rst_arr         => i_rx_rst_arr,
    rx_sosi_arr        => demux_in_sosi_arr,
    rx_siso_arr        => demux_in_siso_arr,

    --Serial data
    xaui_tx_arr        => xaui_tx_arr,
    xaui_rx_arr        => xaui_rx_arr,

    -- MDIO interface
    mdio_rst           => mdio_rst,
    mdio_mdc_arr       => mdio_mdc_arr,
    mdio_mdat_in_arr   => mdio_mdat_in_arr,
    mdio_mdat_oen_arr  => mdio_mdat_oen_arr
  );

  u_mms_diagnostics: entity diagnostics_lib.mms_diagnostics
  generic map(
    g_data_w       => c_xgmii_data_w,
    g_nof_streams  => g_nof_xaui,
    g_separate_clk => true
  )
  port map (
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,

    src_rst         => i_tx_rst_arr,
    src_clk         => tx_clk_arr,

    snk_rst         => i_rx_rst_arr,
    snk_clk         => rx_clk_arr_in,

    mm_mosi         => diagnostics_mosi,
    mm_miso         => diagnostics_miso,

    src_out_arr     => diagnostics_tx_sosi_arr,
    src_in_arr      => diagnostics_tx_siso_arr,

    snk_out_arr     => diagnostics_rx_siso_arr,
    snk_in_arr      => diagnostics_rx_sosi_arr,

    src_en_out      => diagnostics_tx_en_arr,
    snk_en_out      => diagnostics_rx_en_arr
  );

   gen_select : for i in 0 to g_nof_xaui - 1 generate
    -- 0 = user data,
    tx_siso_arr(i)                     <= mux_in_siso_2arr(i)(c_sel_user);
    mux_in_sosi_2arr(i)(c_sel_user)    <= tx_sosi_arr(i);

    demux_out_siso_2arr(i)(c_sel_user) <= rx_siso_arr(i);
    rx_sosi_arr(i)                     <= demux_out_sosi_2arr(i)(c_sel_user);

    -- 1 = internal diagnostics data
    diagnostics_tx_siso_arr(i)         <= mux_in_siso_2arr(i)(c_sel_diag);
    mux_in_sosi_2arr(i)(c_sel_diag)    <= diagnostics_tx_sosi_arr(i);

    demux_out_siso_2arr(i)(c_sel_diag) <= diagnostics_rx_siso_arr(i);
    diagnostics_rx_sosi_arr(i)         <= demux_out_sosi_2arr(i)(c_sel_diag);

    -- If user enables internal diagnostics source, the dp_mux automatically forwards that data to the corresponding transmitter
    nxt_mux_select_arr(i) <= slv(diagnostics_tx_en_arr(i));

    -- If user enables internal diagnostics sink, the dp_demux is automatically set to forward the corresponding receiver data
    nxt_demux_select_arr(i) <= slv(diagnostics_rx_en_arr(i));

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      -- Mux
      g_sel_ctrl_invert   => true,  -- We're using DOWNTO ranges in out streaming arrays, so we must invert the selection input
      g_mode              => 2,
      g_nof_input         => c_nof_select,
      g_append_channel_lo => false,
      -- Input FIFO
      g_use_fifo          => false,
      g_fifo_size         => array_init(1024, c_nof_select),
      g_fifo_fill         => array_init(   0, c_nof_select)
    )
    port map (
      rst         => i_tx_rst_arr(i),
      clk         => tx_clk_arr(i),

      sel_ctrl    => TO_UINT(mux_select_arr(i)),

      -- ST sinks
      snk_out_arr => mux_in_siso_2arr(i),
      snk_in_arr  => mux_in_sosi_2arr(i),
      -- ST source
      src_in      => mux_out_siso_arr(i),
      src_out     => mux_out_sosi_arr(i)
    );

    p_tx_clk : process(i_tx_rst_arr, tx_clk_arr)
    begin
      if i_tx_rst_arr(i) = '1' then
        mux_select_arr(i) <= (others => '0');
      elsif rising_edge(tx_clk_arr(i)) then
        mux_select_arr(i) <= nxt_mux_select_arr(i);
      end if;
    end process;

    u_dp_demux : entity dp_lib.dp_demux
    generic map (
      g_sel_ctrl_invert   => true,  -- We're using DOWNTO ranges in out streaming arrays, so we must invert the selection input
      g_mode              => 2,
      g_nof_output        => c_nof_select,
      g_remove_channel_lo => false,
      g_combined          => false
    )
    port map (
      rst         => i_rx_rst_arr(i),
      clk         => rx_clk_arr_in(i),

      sel_ctrl    => TO_UINT(demux_select_arr(i)),

      -- ST sinks
      snk_out     => demux_in_siso_arr(i),
      snk_in      => demux_in_sosi_arr(i),
      -- ST source
      src_in_arr  => demux_out_siso_2arr(i),
      src_out_arr => demux_out_sosi_2arr(i)
    );

    p_rx_clk_arr : process(i_rx_rst_arr, rx_clk_arr_in)
    begin
      if i_rx_rst_arr(i) = '1' then
        demux_select_arr(i) <= (others => '0');
      elsif rising_edge(rx_clk_arr_in(i)) then
        demux_select_arr(i) <= nxt_demux_select_arr(i);
      end if;
    end process;

  end generate;
end wrap;
