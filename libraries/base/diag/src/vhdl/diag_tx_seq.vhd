--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;

-- Purpose: Transmit continuous PRSG or COUNTER test sequence data.
-- Description:
--   The Tx test data can sequence data or constant value data dependent on
--   diag_dc.
--   The Tx test sequence data can be PRSG or COUNTER dependent on diag_sel.
--   The Tx is enabled by diag_en. When the Tx is disabled then the sequence
--   data gets initialised with diag_init.
--   The out_ready acts as a data request. When the generator is enabled then
--   output is valid for every active out_ready, to support streaming flow
--   control. With g_latency=1 the out_val is active one cycle after diag_req,
--   by using g_latency=0 outval is active in the same cycle as diag_req.
--   Use diag_mod=0 for default binary wrap at 2**g_dat_w. For diag_rx_seq
--   choose diag_step = 2**g_seq_dat_w - diag_mod + g_cnt_incr to verify ok.

entity diag_tx_seq is
  generic (
    g_latency  : natural := 1;  -- default 1 for registered out_cnt/dat/val output, use 0 for immediate combinatorial out_cnt/dat/val output
    g_sel      : std_logic := '1';  -- '0' = PRSG, '1' = COUNTER
    g_init     : natural := 0;  -- init value for out_dat when diag_en = '0'
    g_cnt_incr : integer := 1;
    g_cnt_w    : natural := c_word_w;
    g_dat_w    : natural  -- >= 1, test data width
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    -- Static control input (connect via MM or leave open to use default)
    diag_en    : in  std_logic;  -- '0' = init and disable output sequence, '1' = enable output sequence
    diag_sel   : in  std_logic := g_sel;  -- '0' = PRSG sequence data, '1' = COUNTER sequence data
    diag_dc    : in  std_logic := '0';  -- '0' = output diag_sel sequence data, '1' = output constant diag_init data
    diag_init  : in  std_logic_vector(g_dat_w - 1 downto 0) := TO_UVEC(g_init, g_dat_w);  -- init value for out_dat when diag_en = '0'
    diag_mod   : in  std_logic_vector(g_dat_w - 1 downto 0) := TO_UVEC(0, g_dat_w);  -- default 0 to wrap modulo 2*g_dat_w
    -- ST output
    diag_req   : in  std_logic := '1';  -- '1' = request output, '0' = halt output
    out_cnt    : out std_logic_vector(g_cnt_w - 1 downto 0);  -- count valid output test sequence data
    out_dat    : out std_logic_vector(g_dat_w - 1 downto 0);  -- output test sequence data
    out_val    : out std_logic  -- '1' when out_dat is valid
  );
end diag_tx_seq;

architecture rtl of diag_tx_seq is
  constant c_lfsr_nr     : natural := g_dat_w - c_common_lfsr_first;

  signal diag_dis        : std_logic;

  signal prsg            : std_logic_vector(out_dat'range);
  signal nxt_prsg        : std_logic_vector(out_dat'range);
  signal cntr            : std_logic_vector(out_dat'range) := (others => '0');  -- init to avoid Warning: "NUMERIC_STD."<": metavalue detected" with UNSIGNED()
  signal next_cntr       : std_logic_vector(out_dat'range) := (others => '0');  -- init to avoid Warning: "NUMERIC_STD."<": metavalue detected" with UNSIGNED()
  signal nxt_cntr        : std_logic_vector(out_dat'range);

  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
begin
  diag_dis <= not diag_en;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      prsg         <= (others => '0');
      cntr         <= (others => '0');
    elsif rising_edge(clk) then
      if clken = '1' then
        prsg         <= nxt_prsg;
        cntr         <= nxt_cntr;
      end if;
    end if;
  end process;

  gen_latency : if g_latency /= 0 generate
    p_clk : process (rst, clk)
    begin
      if rst = '1' then
        out_dat      <= (others => '0');
        out_val      <= '0';
      elsif rising_edge(clk) then
        if clken = '1' then
          out_dat      <= nxt_out_dat;
          out_val      <= nxt_out_val;
        end if;
      end if;
    end process;
  end generate;

  no_latency : if g_latency = 0 generate
    out_dat      <= nxt_out_dat;
    out_val      <= nxt_out_val;
  end generate;

  common_lfsr_nxt_seq(c_lfsr_nr,  -- IN
                      g_cnt_incr,  -- IN
                      diag_en,  -- IN
                      diag_req,  -- IN
                      diag_init,  -- IN
                      prsg,  -- IN
                      cntr,  -- IN
                      nxt_prsg,  -- OUT
                      next_cntr);  -- OUT

  nxt_cntr <= next_cntr when unsigned(next_cntr) < unsigned(diag_mod) else SUB_UVEC(next_cntr, diag_mod);

  nxt_out_dat <= diag_init when diag_dc = '1' else prsg when diag_sel = '0' else cntr;
  nxt_out_val <= diag_en and diag_req;  -- 'en' for entire test on/off, 'req' for dynamic invalid gaps in the stream

  -- Count number of valid output data
  u_common_counter : entity common_lib.common_counter
  generic map (
    g_latency   => g_latency,  -- default 1 for registered count output, use 0 for immediate combinatorial count output
    g_width     => g_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => diag_dis,  -- synchronous cnt_clr is only interpreted when clken is active
    cnt_en  => nxt_out_val,
    count   => out_cnt
  );
end rtl;
