-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Create 156.25 and 312.5  MHz clocks for 10GBASE-R and MAC-10G
-- Description:
--   The reference clock frequency for the PLL is 644.53125 MHz.
-- Remark:
-- . This PLL is typically instantiated at the top level of a design in the
--   board support component rather than at the ETH-10G instance. The
--   advantages are:
--   - this avoids having to propagate up the PLL output clocks through the
--     hierarchy, which can cause delta-cycle mismatch between clock
--     processes at different levels in the hierarchy.
--   - the 156 and 312 MHz clocks are also available for other purposes.
-- .

library ieee, technology_lib, common_lib;
use ieee.std_logic_1164.all;
use work.tech_pll_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_pll_xgmii_mac_clocks_altera_xcvr_fpll_a10_150;
library ip_arria10_e3sge3_pll_xgmii_mac_clocks_altera_xcvr_fpll_a10_151;
library ip_arria10_e1sg_pll_xgmii_mac_clocks_altera_xcvr_fpll_a10_180;
library ip_arria10_e2sg_pll_xgmii_mac_clocks_altera_xcvr_fpll_a10_191;

entity tech_pll_xgmii_mac_clocks is
  generic (
    g_technology  : natural := c_tech_select_default
  );
  port (
    refclk_644 : in  std_logic;  -- 644.53125 MHz reference clock for PLL
    rst_in     : in  std_logic;  -- PLL powerdown input, as reset
    clk_156    : out std_logic;  -- 156.25 MHz PLL output clock
    clk_312    : out std_logic;  -- 312.5  MHz PLL output clock
    rst_156    : out std_logic;  -- reset in clk_156 domain based on PLL locked
    rst_312    : out std_logic  -- reset in clk_312 domain based on PLL locked
  );
end tech_pll_xgmii_mac_clocks;

architecture str of tech_pll_xgmii_mac_clocks is
  signal pll_locked    : std_logic;
  signal pll_locked_n  : std_logic;

  signal i_clk_156     : std_logic;
  signal i_clk_312     : std_logic;
begin
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_pll_xgmii_mac_clocks
    port map (
      pll_refclk0   => refclk_644,
      pll_powerdown => rst_in,
      pll_locked    => pll_locked,
      outclk0       => i_clk_156,
      pll_cal_busy  => OPEN,
      outclk1       => i_clk_312
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_pll_xgmii_mac_clocks
    port map (
      pll_refclk0   => refclk_644,
      pll_powerdown => rst_in,
      pll_locked    => pll_locked,
      outclk0       => i_clk_156,
      pll_cal_busy  => OPEN,
      outclk1       => i_clk_312
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_pll_xgmii_mac_clocks
    port map (
      pll_refclk0   => refclk_644,
      pll_powerdown => rst_in,
      pll_locked    => pll_locked,
      outclk0       => i_clk_156,
      pll_cal_busy  => OPEN,
      outclk1       => i_clk_312
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_pll_xgmii_mac_clocks
    port map (
      pll_refclk0   => refclk_644,
      pll_powerdown => rst_in,
      pll_locked    => pll_locked,
      outclk0       => i_clk_156,
      pll_cal_busy  => OPEN,
      outclk1       => i_clk_312
    );
  end generate;

  pll_locked_n <= not pll_locked;

  -- The delta-cycle difference in simulation between i_clk and output clk is no issue because i_clk is only used to create rst which is not clk cycle critical
  clk_156 <= i_clk_156;
  clk_312 <= i_clk_312;

  u_common_areset_156 : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst    => pll_locked_n,
    clk       => i_clk_156,
    out_rst   => rst_156
  );

  u_common_areset_312 : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst    => pll_locked_n,
    clk       => i_clk_312,
    out_rst   => rst_312
  );
end architecture;
