%NPR_SYNTHESIS Near perfect reconstruction synthesis filter bank.
%  X = NPR_SYNTHESIS(C,Y) synthesizes a transient signal X from a number of
%  channel transients stored in Y. Each channel is represented by a row.
%  C is a two dimensional array, containing the filter coefficients.
%  The number of rows in Y must be twice the number of rows in C.
%
%  See also npr_analysis, npr_coeff.
%
% (c) 2007 - 2022 Wessel Lubberhuizen
function [x] = npr_synthesis(coeff,y)
% number of channels
N=size(coeff,1);
% number of slices
M=size(y,2);
% number of samples
L=M*N;
% split into even and odd channels
y = reshape(y,2,N,M);
y1 = squeeze(y(1,:,:));
y2 = squeeze(y(2,:,:));
% apply dft
x1 = fft(y1,[],1)*N;
x2 = fft(y2,[],1)*N;
% apply channel filters
for n=1:N
    x1(n,:) = filter(coeff(n,:),1,x1(n,:));
    x2(n,:) = filter(coeff(n,:),1,x2(n,:));
end
% reshape to time series
x1 = reshape(x1,1,L);
x2 = reshape(x2,1,L);
% apply frequency shift
phi=mod(0:L-1,2*N)*(pi/N);
x2=x2.*exp(-1i*phi);
% combine filter results
x = x1-x2;
