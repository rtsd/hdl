--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: E. Kooistra, 28 Jul 2017
-- Purpose: Multi-testbench for mms_dp_force_data_serial_arr
-- Description:
--   Verify mms_dp_force_data_serial_arr
-- Usage:
--   > as 4
--   > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_mms_dp_force_data_serial_arr is
end tb_tb_mms_dp_force_data_serial_arr;

architecture tb of tb_tb_mms_dp_force_data_serial_arr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always active or random stimuli valid flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_active;   -- always active or random verify  ready flow control
-- g_nof_streams            : NATURAL := 1;      -- >= 1
-- g_dat_w                  : NATURAL := 5;      -- must be <= 32 to fit INTEGER range
-- g_force_stream           : INTEGER := 0;      -- must be < g_nof_streams, force data on this stream
-- g_force_value            : BOOLEAN := TRUE;   -- when TRUE force value at index, else pass on snk_in at index
-- g_force_index            : NATURAL := 3;      -- sample index or block index in time dependent on g_index_sample_block_n
-- g_force_data             : INTEGER := -1;
-- g_force_re               : INTEGER := 2;
-- g_force_im               : INTEGER := -3;
-- g_index_period           : NATURAL := 17;    -- number of indices in time, must be <= 2*31 to fit in NATURAL range
-- g_index_sample_block_n   : BOOLEAN := TRUE   -- when TRUE sample index in block, else block index in sync interval

  u_act_force_data_value_sample_one_stream    : entity work.tb_mms_dp_force_data_serial_arr generic map (e_active, e_active, 1, 16, 0,  true, 7, -1, 2, -3,  17,  true);
  u_rnd_force_data_value_sample_stream0       : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 0,  true, 7, -1, 2, -3,  17,  true);
  u_rnd_force_data_value_sample_stream1       : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 1,  true, 7, -1, 2, -3,  17,  true);
  u_rnd_force_data_value_block_stream1        : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 1,  true, 7, -1, 2, -3,  17, false);
  u_rnd_force_data_value_block_stream2        : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 2,  true, 7, -1, 2, -3,  17, false);
  u_rnd_force_data_transparant_sample_stream1 : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 1, false, 7, -1, 2, -3,  17,  true);
  u_rnd_force_data_transparant_block_stream2  : entity work.tb_mms_dp_force_data_serial_arr generic map (e_random, e_random, 3, 16, 2, false, 7, -1, 2, -3,  17, false);
end tb;
