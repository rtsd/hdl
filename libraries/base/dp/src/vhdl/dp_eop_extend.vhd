-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose:
--   The extended eop output can be used to gate valid eop related stream info
--   like empty and error.
-- Description:
--   Extend the eop until the next sop.

entity dp_eop_extend is
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    in_sop     : in  std_logic;
    in_eop     : in  std_logic;
    eop_extend : out std_logic
  );
end dp_eop_extend;

architecture rtl of dp_eop_extend is
  signal extend     : std_logic;
begin
  u_extend : entity common_lib.common_switch
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => in_eop,
    switch_low  => in_sop,
    out_level   => extend
  );

  eop_extend <= (in_eop or extend) and not in_sop;
end rtl;
