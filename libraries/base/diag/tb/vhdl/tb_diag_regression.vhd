-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Regression multi TB for DIAG library
-- Description:
--   All TB are self checking. If some component TB fails then simulate that TB
--   seperately to find the cause.
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_diag_regression is
end tb_diag_regression;

architecture tb of tb_diag_regression is
begin
  -- Single tb
  u_tb_diag_block_gen             : entity work.tb_diag_block_gen;

  --u_tb_diag_tx_seq                : ENTITY work.tb_diag_tx_seq;  -- not self-checking yet
  u_tb_diag_frm_generator         : entity work.tb_diag_frm_generator;
  u_tb_diag_frm_monitor           : entity work.tb_diag_frm_monitor;

  -- Multi tb
  u_tb_tb_diag_rx_seq             : entity work.tb_tb_diag_rx_seq;
  u_tb_tb_mms_diag_seq            : entity work.tb_tb_mms_diag_seq;
  u_tb_tb_mms_diag_block_gen      : entity work.tb_tb_mms_diag_block_gen;
end tb;
