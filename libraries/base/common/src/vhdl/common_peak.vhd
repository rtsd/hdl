-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide the maximum value that is seen on the input on the output.
-- Description: The peak value of the input data is captured and maintained
--              on the output. A pulse on in_clear will reset thepeak value.
--              Only valid data will be considered.
--
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_peak is
  generic (
    g_dat_w : positive := 8
  );
  port (
    rst       : in  std_logic := '0';
    clk       : in  std_logic;
    in_dat    : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val    : in  std_logic := '1';
    in_clear  : in  std_logic;
    out_dat   : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val   : out std_logic
  );
end common_peak;

architecture rtl of common_peak is
  type reg_type is record
    peak    : std_logic_vector(g_dat_w - 1 downto 0);
    out_val : std_logic;
  end record;

  signal r, rin      : reg_type;
begin
  p_comb : process(r, rst, in_val, in_dat, in_clear)
    variable v : reg_type;
  begin
    v := r;

    if in_val = '1' then
      if TO_UINT(in_dat) > TO_UINT(r.peak) then
        v.peak := in_dat;
        v.out_val := '1';
      end if;
    end if;

    if in_clear = '1' then
      v.peak := (others => '0');
      v.out_val := '0';
    end if;

    if(rst = '1') then
      v.peak   := (others => '0');
      v.out_val := '0';
    end if;

    rin <= v;
  end process;

  p_regs : process(clk)
  begin
    if rising_edge(clk) then
      r <= rin;
    end if;
  end process;

  out_val   <= r.out_val;
  out_dat   <= r.peak;
end rtl;
