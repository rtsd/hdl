--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_transceiver_lib, ip_stratixiv_phy_xaui_lib;

library IEEE, technology_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use technology_lib.technology_pkg.all;
use work.tech_xaui_component_pkg.all;

entity tech_xaui_stratixiv is
  generic (
    g_sim             : boolean := false;
    g_nof_xaui        : natural := 1  -- Up to 3 (hard XAUI only) supported
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                    : in  std_logic;  -- 156.25 MHz
    tr_rst                    : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk               : in  std_logic;

    -- MM interface
    mm_clk                    : in  std_logic;
    mm_rst                    : in  std_logic;

    xaui_mosi                 : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso                 : out t_mem_miso;

    -- XGMII interface
    tx_clk_arr                : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- 156.25 MHz, externally connect tr_clk to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    rx_clk_arr_out            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    rx_clk_arr_in             : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay

    txc_tx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- tx_ready in tx_clk_arr domain, can be used for xon flow control
    rxc_rx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_ready in rx_clk_arr domain, typically leave not connected

    txc_rx_channelaligned_arr : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_channelaligned in tx_clk_arr domain, when asserted, indicates that all 4 RX channels are aligned.

    xgmii_tx_dc_arr           : in  t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);
    xgmii_rx_dc_arr           : out t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);

    -- Serial I/O
    xaui_tx_arr               : out t_xaui_arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr               : in  t_xaui_arr(g_nof_xaui - 1 downto 0)
  );
end tech_xaui_stratixiv;

architecture str of tech_xaui_stratixiv is
  constant c_reconf_togxb_bus_w         : natural := 4;
  constant c_reconf_fromgxb_bus_w       : natural := 17;
  constant c_reconf_nof_gx              : natural := 4;  -- 1, 2, or 3 hard XAUI with 4, 8 or 12 transceivers all require only gxb_reconfig for 4 transceivers
  constant c_reconf_soft_nof_gx         : natural := 16;  -- g_nof_xaui=4 soft XAUI, with 16 transceivers requires gxb_reconfig as for 16 transceivers
  constant c_reconf_soft_fromgxb_bus_w  : natural := 4 * c_reconf_fromgxb_bus_w;

  constant c_xaui_mosi_addr_w           : natural := tech_xaui_mosi_addr_w(c_tech_stratixiv);

  signal reconfig_togxb            : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_fromgxb          : std_logic_vector(c_reconf_fromgxb_bus_w - 1 downto 0);

  signal reconfig_soft_togxb       : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_soft_fromgxb     : std_logic_vector(c_reconf_soft_fromgxb_bus_w - 1 downto 0);

  signal reconfig_0_togxb          : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_0_fromgxb        : std_logic_vector(c_reconf_fromgxb_bus_w - 1 downto 0);
  signal reconfig_1_togxb          : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_1_fromgxb        : std_logic_vector(c_reconf_fromgxb_bus_w - 1 downto 0);
  signal reconfig_2_togxb          : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_2_fromgxb        : std_logic_vector(c_reconf_fromgxb_bus_w - 1 downto 0);

  signal a_tx_ready_arr            : std_logic_vector(g_nof_xaui - 1 downto 0);  -- tx_ready in mm_clk clock domain
  signal a_rx_ready_arr            : std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_ready in mm_clk clock domain

  signal a_rx_channelaligned_arr   : std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_channelaligned asynchronous, indicates that all 4 RX channels are aligned when asserted
  signal tx_rst_arr                : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal xaui_mosi_arr             : t_mem_mosi_arr(g_nof_xaui - 1 downto 0);
  signal xaui_miso_arr             : t_mem_miso_arr(g_nof_xaui - 1 downto 0);
begin
  gen_nof_xaui : for i in g_nof_xaui - 1 downto 0 generate
    u_async_txc_tx_ready : entity common_lib.common_async
    generic map(
      g_rst_level => '0'
    )
    port map(
      clk  => tx_clk_arr(i),
      din  => a_tx_ready_arr(i),
      dout => txc_tx_ready_arr(i)
    );

    u_async_rxc_rx_ready : entity common_lib.common_async
    generic map(
      g_rst_level => '0'
    )
    port map(
      clk  => rx_clk_arr_in(i),
      din  => a_rx_ready_arr(i),
      dout => rxc_rx_ready_arr(i)
    );

    u_areset_tx_rst : entity common_lib.common_areset
    generic map(
      g_rst_level => '1',
      g_delay_len => 4
    )
    port map(
      clk     => tx_clk_arr(i),
      in_rst  => tr_rst,
      out_rst => tx_rst_arr(i)
    );

    u_txc_rx_channelaligned_arr: entity work.tech_xaui_align_dly
    generic map (
      g_sim => g_sim  -- to use shorter delay when in simulation
    )
    port map(
      tx_rst                    => tx_rst_arr(i),
      tx_clk                    => tx_clk_arr(i),

      a_rx_channelaligned       => a_rx_channelaligned_arr(i),
      txc_rx_channelaligned_dly => txc_rx_channelaligned_arr(i)
    );

    -- IP
    gen_hard_xaui_0: if i = 0 generate
      u_ip_phy_xaui : ip_stratixiv_phy_xaui_0
      port map (
        pll_ref_clk          => tr_clk,
        xgmii_tx_clk         => tx_clk_arr(i),
        xgmii_rx_clk         => rx_clk_arr_out(i),
        xgmii_rx_dc          => xgmii_rx_dc_arr(i),
        xgmii_tx_dc          => xgmii_tx_dc_arr(i),
        xaui_rx_serial_data  => xaui_rx_arr(i),
        xaui_tx_serial_data  => xaui_tx_arr(i),
        rx_ready             => a_rx_ready_arr(i),
        tx_ready             => a_tx_ready_arr(i),
        phy_mgmt_clk         => mm_clk,
        phy_mgmt_clk_reset   => mm_rst,
        phy_mgmt_address     => xaui_mosi_arr(i).address(c_xaui_mosi_addr_w - 1 downto 0),
        phy_mgmt_read        => xaui_mosi_arr(i).rd,
        phy_mgmt_readdata    => xaui_miso_arr(i).rddata(c_word_w - 1 downto 0),
        phy_mgmt_write       => xaui_mosi_arr(i).wr,
        phy_mgmt_writedata   => xaui_mosi_arr(i).wrdata(c_word_w - 1 downto 0),
        phy_mgmt_waitrequest => xaui_miso_arr(i).waitrequest,

        rx_channelaligned    => a_rx_channelaligned_arr(i),

        -- Reconfig block connections
        reconfig_to_xcvr     => reconfig_0_togxb,
        reconfig_from_xcvr   => reconfig_0_fromgxb
      );

      u_ip_gxb_reconfig : ip_stratixiv_gxb_reconfig_v111
      generic map (
        g_nof_gx  => c_reconf_nof_gx
      )
      port map (
        reconfig_clk        => cal_rec_clk,
        reconfig_fromgxb    => reconfig_0_fromgxb,
        busy                => OPEN,
        reconfig_togxb      => reconfig_0_togxb
      );
    end generate;

    gen_hard_xaui_1: if i = 1 generate
      u_ip_phy_xaui : ip_stratixiv_phy_xaui_1
      port map (
        pll_ref_clk          => tr_clk,
        xgmii_tx_clk         => tx_clk_arr(i),
        xgmii_rx_clk         => rx_clk_arr_out(i),
        xgmii_rx_dc          => xgmii_rx_dc_arr(i),
        xgmii_tx_dc          => xgmii_tx_dc_arr(i),
        xaui_rx_serial_data  => xaui_rx_arr(i),
        xaui_tx_serial_data  => xaui_tx_arr(i),
        rx_ready             => a_rx_ready_arr(i),
        tx_ready             => a_tx_ready_arr(i),
        phy_mgmt_clk         => mm_clk,
        phy_mgmt_clk_reset   => mm_rst,
        phy_mgmt_address     => xaui_mosi_arr(i).address(c_xaui_mosi_addr_w - 1 downto 0),
        phy_mgmt_read        => xaui_mosi_arr(i).rd,
        phy_mgmt_readdata    => xaui_miso_arr(i).rddata(c_word_w - 1 downto 0),
        phy_mgmt_write       => xaui_mosi_arr(i).wr,
        phy_mgmt_writedata   => xaui_mosi_arr(i).wrdata(c_word_w - 1 downto 0),
        phy_mgmt_waitrequest => xaui_miso_arr(i).waitrequest,

        rx_channelaligned    => a_rx_channelaligned_arr(i),

        -- Reconfig block connections
        reconfig_to_xcvr     => reconfig_1_togxb,
        reconfig_from_xcvr   => reconfig_1_fromgxb
      );

      u_ip_gxb_reconfig : ip_stratixiv_gxb_reconfig_v111
      generic map (
        g_nof_gx  => c_reconf_nof_gx
      )
      port map (
        reconfig_clk        => cal_rec_clk,
        reconfig_fromgxb    => reconfig_1_fromgxb,
        busy                => OPEN,
        reconfig_togxb      => reconfig_1_togxb
      );
    end generate;

    gen_hard_xaui_2: if i = 2 generate
      u_ip_phy_xaui : ip_stratixiv_phy_xaui_2
      port map (
        pll_ref_clk          => tr_clk,
        xgmii_tx_clk         => tx_clk_arr(i),
        xgmii_rx_clk         => rx_clk_arr_out(i),
        xgmii_rx_dc          => xgmii_rx_dc_arr(i),
        xgmii_tx_dc          => xgmii_tx_dc_arr(i),
        xaui_rx_serial_data  => xaui_rx_arr(i),
        xaui_tx_serial_data  => xaui_tx_arr(i),
        rx_ready             => a_rx_ready_arr(i),
        tx_ready             => a_tx_ready_arr(i),
        phy_mgmt_clk         => mm_clk,
        phy_mgmt_clk_reset   => mm_rst,
        phy_mgmt_address     => xaui_mosi_arr(i).address(c_xaui_mosi_addr_w - 1 downto 0),
        phy_mgmt_read        => xaui_mosi_arr(i).rd,
        phy_mgmt_readdata    => xaui_miso_arr(i).rddata(c_word_w - 1 downto 0),
        phy_mgmt_write       => xaui_mosi_arr(i).wr,
        phy_mgmt_writedata   => xaui_mosi_arr(i).wrdata(c_word_w - 1 downto 0),
        phy_mgmt_waitrequest => xaui_miso_arr(i).waitrequest,

        rx_channelaligned    => a_rx_channelaligned_arr(i),

        -- Reconfig block connections
        reconfig_to_xcvr     => reconfig_2_togxb,
        reconfig_from_xcvr   => reconfig_2_fromgxb
      );

      u_ip_gxb_reconfig : ip_stratixiv_gxb_reconfig_v111
      generic map (
        g_nof_gx  => c_reconf_nof_gx
      )
      port map (
        reconfig_clk        => cal_rec_clk,
        reconfig_fromgxb    => reconfig_2_fromgxb,
        busy                => OPEN,
        reconfig_togxb      => reconfig_2_togxb
      );
    end generate;

    gen_soft_xaui: if i = 3 generate  -- NOTE: this 4th (soft) instance makes the Quartus fitter (11.1, no SP) fail, so is not supported.
      u_ip_phy_xaui : ip_stratixiv_phy_xaui_soft
      port map (
        pll_ref_clk          => tr_clk,
        xgmii_tx_clk         => tx_clk_arr(i),
        xgmii_rx_clk         => rx_clk_arr_out(i),
        xgmii_rx_dc          => xgmii_rx_dc_arr(i),
        xgmii_tx_dc          => xgmii_tx_dc_arr(i),
        xaui_rx_serial_data  => xaui_rx_arr(i),
        xaui_tx_serial_data  => xaui_tx_arr(i),
        rx_ready             => a_rx_ready_arr(i),
        tx_ready             => a_tx_ready_arr(i),
        phy_mgmt_clk         => mm_clk,
        phy_mgmt_clk_reset   => mm_rst,
        phy_mgmt_address     => xaui_mosi_arr(i).address(c_xaui_mosi_addr_w - 1 downto 0),
        phy_mgmt_read        => xaui_mosi_arr(i).rd,
        phy_mgmt_readdata    => xaui_miso_arr(i).rddata(c_word_w - 1 downto 0),
        phy_mgmt_write       => xaui_mosi_arr(i).wr,
        phy_mgmt_writedata   => xaui_mosi_arr(i).wrdata(c_word_w - 1 downto 0),
        phy_mgmt_waitrequest => xaui_miso_arr(i).waitrequest,

        rx_channelaligned    => a_rx_channelaligned_arr(i),

        -- Reconfig block connections
        reconfig_to_xcvr     => reconfig_soft_togxb,
        reconfig_from_xcvr   => reconfig_soft_fromgxb
      );

      u_ip_gxb_reconfig : ip_stratixiv_gxb_reconfig_v111
      generic map (
        g_nof_gx  => c_reconf_soft_nof_gx
      )
      port map (
        reconfig_clk        => cal_rec_clk,
        reconfig_fromgxb    => reconfig_soft_fromgxb,
        busy                => OPEN,
        reconfig_togxb      => reconfig_soft_togxb
      );
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- MM bus mux
  -----------------------------------------------------------------------------
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_xaui,
    g_mult_addr_w => c_xaui_mosi_addr_w
  )
  port map (
    mosi     => xaui_mosi,
    miso     => xaui_miso,
    mosi_arr => xaui_mosi_arr,
    miso_arr => xaui_miso_arr
  );
end str;
