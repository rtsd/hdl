-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_common_operation_tree is
end tb_tb_common_operation_tree;

architecture tb of tb_tb_common_operation_tree is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Usage:
  -- > as 2
  -- > run -all

  -- g_operation      : STRING  := "MAX";      -- supported operations "MAX", "MIN"
  -- g_representation : STRING  := "SIGNED";
  -- g_pipeline       : NATURAL := 1;  -- amount of pipelining per stage
  -- g_pipeline_mod   : NATURAL := 1;  -- only pipeline the stage output by g_pipeline when the stage number MOD g_pipeline_mod = 0
  -- g_nof_inputs     : NATURAL := 5   -- >= 1

  u_smax_0      : entity work.tb_common_operation_tree generic map ("MAX", "SIGNED", 0, 1, 5);
  u_smax_1      : entity work.tb_common_operation_tree generic map ("MAX", "SIGNED", 1, 1, 7);
  u_smax_1_4_8  : entity work.tb_common_operation_tree generic map ("MAX", "SIGNED", 1, 4, 8);
  u_smax_1_4_9  : entity work.tb_common_operation_tree generic map ("MAX", "SIGNED", 1, 4, 9);
  u_smax_1_4_16 : entity work.tb_common_operation_tree generic map ("MAX", "SIGNED", 1, 4, 16);

  u_umax_0      : entity work.tb_common_operation_tree generic map ("MAX", "UNSIGNED", 0, 1, 5);
  u_umax_1      : entity work.tb_common_operation_tree generic map ("MAX", "UNSIGNED", 1, 1, 7);
  u_umax_1_2_16 : entity work.tb_common_operation_tree generic map ("MAX", "UNSIGNED", 1, 2, 16);

  u_smin_0      : entity work.tb_common_operation_tree generic map ("MIN", "SIGNED", 0, 1, 5);
  u_smin_1      : entity work.tb_common_operation_tree generic map ("MIN", "SIGNED", 1, 1, 7);
  u_smin_1_2_8  : entity work.tb_common_operation_tree generic map ("MIN", "SIGNED", 1, 2, 8);

  u_umin_0      : entity work.tb_common_operation_tree generic map ("MIN", "UNSIGNED", 0, 1, 5);
  u_umin_1      : entity work.tb_common_operation_tree generic map ("MIN", "UNSIGNED", 1, 1, 7);
  u_umin_1_1_8  : entity work.tb_common_operation_tree generic map ("MIN", "UNSIGNED", 1, 1, 8);
end tb;
