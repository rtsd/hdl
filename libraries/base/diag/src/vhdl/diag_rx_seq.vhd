--------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Verify received continuous test sequence data.
-- Description:
--   The diag_rx_seq can operate in one of two modes that depend on g_use_steps:
--
-- . g_use_steps = FALSE
--   The test data can be PRSG or COUNTER dependent on diag_sel.
--   The Rx is enabled by diag_en. Typically the Tx should already be running,
--   but it is also allowed to first enable the Rx.
--   The Rx is always ready to accept data, therefore it has no in_ready output.
--   Inititally when diag_en is low then diag_res = -1, when diag_en is high
--   then diag_res becomes valid, indicated by diag_res_val, after two test
--   data words have been received. The diag_res verifies per input dat bit,
--   when an in_dat bit goes wrong then the corresponding bit in diag_res goes
--   high and remains high until the Rx is restarted again. This is useful if
--   the test data bits go via separate physical lines (e.g. an LVDS bus).
--   When the Rx is disabled then diag_res = -1. Typically the g_diag_res_w >
--   g_dat_w:
--   . diag_res(g_diag_res_w-1:g_dat_w) => NOT diag_res_val
--   . diag_res(     g_dat_w-1:0      ) => aggregated diff of in_dat during
--                                         diag_en
--   It is possible to use g_diag_res_w=g_dat_w, but then it is not possible to
--   distinguish between whether the test has ran at all or whether all bits
--   got errors.
--   The diag_sample keeps the last valid in_dat value. When diag_en='0' it is
--   reset to 0. Reading diag_sample via MM gives an impression of the valid
--   in_dat activity. The diag_sample_diff shows the difference of the last and
--   the previous in_dat value. The diag_sample_diff can be useful to determine
--   or debug the values that are needed for diag_steps_arr.
--
-- . g_use_steps = TRUE
--   The test data is fixed to COUNTER and diag_sel is ignored. The rx_seq can
--   verify counter data that increments in steps that are specified via
--   diag_steps_arr[3:0]. Up to g_nof_steps <= c_diag_seq_rx_reg_nof_steps = 4
--   step sizes are supported. If all steps are set to 1 then there is no
--   difference compared using the COUNTER in g_use_steps = FALSE. Constant
--   value data can be verified by setting alls step to 0. Usinf different
--   steps is useful when the data is generated in linear incrementing order,
--   but received in a different order. Eg. like after a transpose operation
--   where blocks of data are written in row and and read in colums:
--
--     tx:          0 1   2 3   4 5   6 7   8 9   10 11
--     transpose:   0 1   4 5   8 9   2 3   6 7   10 11
--     rx steps:     +1    +1    +1    +1    +1      +1
--                -11    +3    +3    -7    +3    +3
--
--   The step size value range is set by the 32 bit range of the VHDL integer.
--   Therefore typically g_dat_w should be <= 32 b. For a transpose that
--   contains more than 2**32 data words this means that the COUNTER data
--   wraps within the transpose. This is acceptable, because it use g_dat_w
--   <= 32 then still provides sufficient coverage to detect all errors.
--
--   Data errors that match a step size cannot be detected. However if such
--   an error occurs then typically the next increment will cause a mismatch.
--
-- Remarks:
-- . The feature of being able to detect errors per bit as with g_use_steps=
--   FALSE is not supported when g_use_steps=TRUE. Therefore the
--   diag_res[g_dat_w-1:0] = -1 (all '1') when a difference occurs that is no
--   in diag_steps_arr.
-- . The common_lfsr_nxt_seq() that is used when g_use_steps=FALSE uses the
--   in_dat_reg as initialization value for the reference sequence. All
--   subsequent values are derived when in_val_reg='1'. This is possible
--   because given a first value all subsequent values for PSRG or COUNTER
--   with +1 increment are known. For g_use_steps=TRUE the sequence is not
--   known in advance because different increment steps can occur at
--   arbitrary instants. Therefore then the in_dat_reg input is also used
--   during the sequence, to determine all g_nof_steps next values are correct
--   in case they occur.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use work.diag_pkg.all;

entity diag_rx_seq is
  generic (
    g_input_reg  : boolean := false;  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
    g_use_steps  : boolean := false;
    g_nof_steps  : natural := c_diag_seq_rx_reg_nof_steps;
    g_sel        : std_logic := '1';  -- '0' = PRSG, '1' = COUNTER
    g_cnt_incr   : integer := 1;
    g_cnt_w      : natural := c_word_w;
    g_dat_w      : natural := 12;
    g_diag_res_w : natural := 16
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;
    clken          : in  std_logic := '1';

    -- Static control input (connect via MM or leave open to use default)
    diag_en        : in  std_logic;  -- '0' = init and disable, '1' = enable
    diag_sel       : in  std_logic := g_sel;
    diag_steps_arr : t_integer_arr(g_nof_steps - 1 downto 0) := (others => 1);
    diag_res       : out std_logic_vector(g_diag_res_w - 1 downto 0);  -- diag_res valid indication bits & aggregate diff of in_dat during diag_en
    diag_res_val   : out std_logic;
    diag_sample      : out std_logic_vector(g_dat_w - 1 downto 0);  -- monitor last valid in_dat
    diag_sample_diff : out std_logic_vector(g_dat_w - 1 downto 0);  -- monitor difference between last valid in_dat and previous valid in_dat
    diag_sample_val  : out std_logic;

    -- ST input
    in_cnt         : out std_logic_vector(g_cnt_w - 1 downto 0);  -- count valid input test sequence data
    in_dat         : in  std_logic_vector(g_dat_w - 1 downto 0);  -- input test sequence data
    in_val         : in  std_logic  -- gaps are allowed, however diag_res requires at least 2 valid in_dat to report a valid result
  );
end diag_rx_seq;

architecture rtl of diag_rx_seq is
  constant c_lfsr_nr          : natural := g_dat_w - c_common_lfsr_first;

  constant c_diag_res_latency : natural := 3;

  -- Used special value to signal invalid diag_res, unique assuming g_diag_res_w > g_dat_w
  constant c_diag_res_invalid : std_logic_vector(diag_res'range) := (others => '1');

  signal in_val_reg      : std_logic;
  signal in_dat_reg      : std_logic_vector(in_dat'range);

  signal in_dat_dly1     : std_logic_vector(in_dat'range);  -- latency common_lfsr_nxt_seq
  signal in_dat_dly2     : std_logic_vector(in_dat'range);  -- latency ref_dat
  signal in_val_dly1     : std_logic;  -- latency common_lfsr_nxt_seq
  signal in_val_dly2     : std_logic;  -- latency ref_dat

  signal prsg            : std_logic_vector(in_dat'range);
  signal nxt_prsg        : std_logic_vector(in_dat'range);
  signal cntr            : std_logic_vector(in_dat'range);
  signal nxt_cntr        : std_logic_vector(in_dat'range);

  signal diag_dis        : std_logic;
  signal ref_en          : std_logic;
  signal diff_dis        : std_logic;
  signal diag_res_en     : std_logic;
  signal nxt_diag_res_en : std_logic;
  signal nxt_diag_res_val: std_logic;

  signal in_val_1        : std_logic;
  signal in_val_act      : std_logic;
  signal in_val_2        : std_logic;
  signal in_val_2_dly    : std_logic_vector(0 to c_diag_res_latency - 1) := (others => '0');
  signal in_val_2_act    : std_logic;

  signal ref_dat         : std_logic_vector(in_dat'range);
  signal nxt_ref_dat     : std_logic_vector(in_dat'range);
  signal diff_dat        : std_logic_vector(in_dat'range) := (others => '0');
  signal nxt_diff_dat    : std_logic_vector(in_dat'range);
  signal diff_res        : std_logic_vector(in_dat'range);
  signal nxt_diag_res    : std_logic_vector(diag_res'range);

  signal diag_res_int    : std_logic_vector(diag_res'range) := c_diag_res_invalid;

  signal i_diag_sample        : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_diag_sample      : std_logic_vector(g_dat_w - 1 downto 0);
  signal i_diag_sample_diff   : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_diag_sample_diff : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_diag_sample_val  : std_logic;

  type t_dat_arr is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);

  signal ref_dat_arr      : t_dat_arr(g_nof_steps - 1 downto 0) := (others => (others => '0'));
  signal nxt_ref_dat_arr  : t_dat_arr(g_nof_steps - 1 downto 0);
  signal diff_arr         : std_logic_vector(g_nof_steps - 1 downto 0) := (others => '0');
  signal nxt_diff_arr     : std_logic_vector(g_nof_steps - 1 downto 0);
  signal diff_detect      : std_logic := '0';
  signal nxt_diff_detect  : std_logic;
  signal diff_hold        : std_logic;
begin
  diag_dis <= not diag_en;
  diag_sample <= i_diag_sample;
  diag_sample_diff <= i_diag_sample_diff;

  gen_input_reg : if g_input_reg = true generate
    p_reg : process (clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          in_val_reg  <= in_val;
          in_dat_reg  <= in_dat;
        end if;
      end if;
    end process;
  end generate;

  no_input_reg : if g_input_reg = false generate
    in_val_reg  <= in_val;
    in_dat_reg  <= in_dat;
  end generate;

  -- Use initialisation to set initial diag_res to invalid
  diag_res <= diag_res_int;  -- use initialisation of internal signal diag_res_int rather than initialisation of entity output diag_res

--   -- Use rst to set initial diag_res to invalid
--   p_rst_clk : PROCESS (rst, clk)
--   BEGIN
--     IF rst='1' THEN
--       diag_res     <= c_diag_res_invalid;
--     ELSIF rising_edge(clk) THEN
--       IF clken='1' THEN
--         -- Internal.
--         diag_res     <= nxt_diag_res;
--         -- Outputs.
--       END IF;
--     END IF;
--   END PROCESS;

  p_clk : process (clk)
  begin
    if rising_edge(clk) then
      if clken = '1' then
        -- Inputs.
        in_dat_dly1  <= in_dat_reg;
        in_dat_dly2  <= in_dat_dly1;
        in_val_dly1  <= in_val_reg;
        in_val_dly2  <= in_val_dly1;
        -- Internal.
        in_val_2_dly <= in_val_2 & in_val_2_dly(0 to c_diag_res_latency - 2);
        diag_res_int <= nxt_diag_res;
        diag_res_en  <= nxt_diag_res_en;
        diag_res_val <= nxt_diag_res_val;
        -- . g_use_steps=FALSE
        prsg         <= nxt_prsg;
        cntr         <= nxt_cntr;
        ref_dat      <= nxt_ref_dat;
        diff_dat     <= nxt_diff_dat;
        -- . g_use_steps=TRUE
        ref_dat_arr  <= nxt_ref_dat_arr;
        diff_arr     <= nxt_diff_arr;
        diff_detect  <= nxt_diff_detect;
        -- Outputs.
        i_diag_sample      <= nxt_diag_sample;
        i_diag_sample_diff <= nxt_diag_sample_diff;
        diag_sample_val    <= nxt_diag_sample_val;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Keep last valid in_dat value for MM monitoring
  ------------------------------------------------------------------------------
  nxt_diag_sample      <= (others => '0') when diag_en = '0' else in_dat_reg                          when in_val_reg = '1' else i_diag_sample;
  nxt_diag_sample_diff <= (others => '0') when diag_en = '0' else SUB_UVEC(in_dat_reg, i_diag_sample) when in_val_reg = '1' else i_diag_sample_diff;
  nxt_diag_sample_val  <=          '0'  when diag_en = '0' else in_val_reg;

  ------------------------------------------------------------------------------
  -- Detect that there has been valid input data for at least two clock cycles
  ------------------------------------------------------------------------------

  u_in_val_1 : entity common_lib.common_switch
  port map(
    clk         => clk,
    rst         => rst,
    switch_high => in_val_reg,
    switch_low  => diag_dis,
    out_level   => in_val_1  -- first in_val has been detected, but this one was used as seed for common_lfsr_nxt_seq
  );

  in_val_act <= in_val_1 and in_val_reg;  -- Signal the second valid in_dat after diag_en='1'

  u_in_val_2 : entity common_lib.common_switch
  port map(
    clk         => clk,
    rst         => rst,
    switch_high => in_val_act,
    switch_low  => diag_dis,
    out_level   => in_val_2  -- second in_val has been detected, representing a true next sequence value
  );

  -- Use in_val_2_act instead of in_val_2 to have stable start in case diag_dis takes just a pulse and in_val is continue high
  in_val_2_act <= vector_and(in_val_2 & in_val_2_dly);

  -- Use the first valid in_dat after diag_en='1' to initialize the reference data sequence
  ref_en <= in_val_1;

  -- Use the detection of second valid in_dat after diag_en='1' to start detection of differences
  diff_dis <= not in_val_2_act;

  no_steps : if g_use_steps = false generate
    -- Determine next reference dat based on current input dat
    common_lfsr_nxt_seq(c_lfsr_nr,  -- IN
                        g_cnt_incr,  -- IN
                        ref_en,  -- IN
                        in_val_reg,  -- IN, use in_val_reg to allow gaps in the input data valid stream
                        in_dat_reg,  -- IN, used only to init nxt_prsg and nxt_cntr when ref_en='0'
                        prsg,  -- IN
                        cntr,  -- IN
                        nxt_prsg,  -- OUT
                        nxt_cntr);  -- OUT

    nxt_ref_dat <= prsg when diag_sel = '0' else cntr;

    -- Detect difference per bit. The ref_dat has latency 2 compared to the in_dat, because of the register stage in psrg/cntr and the register stage in ref_dat.
    p_diff_dat : process (diff_dat, ref_dat, in_val_dly2, in_dat_dly2)
    begin
      nxt_diff_dat <= diff_dat;
      if in_val_dly2 = '1' then
        for I in in_dat'range loop
          nxt_diff_dat(I) <= ref_dat(I) xor in_dat_dly2(I);
        end loop;
      end if;
    end process;

    gen_verify_dat : for I in in_dat'range generate
      -- Detect and report undefined diff input 'X', which in simulation leaves diff_res at OK, because switch_high only acts on '1'
      p_sim_only : process(clk)
      begin
        if rising_edge(clk) then
          if diff_dat(I) /= '0' and diff_dat(I) /= '1' then
            report "diag_rx_seq : undefined input"
              severity FAILURE;
          end if;
        end if;
      end process;

      -- Hold any difference on the in_dat bus lines
      u_dat : entity common_lib.common_switch
      port map(
        clk         => clk,
        rst         => rst,
        switch_high => diff_dat(I),
        switch_low  => diff_dis,
        out_level   => diff_res(I)
      );
    end generate;
  end generate;

  use_steps : if g_use_steps = true generate
    -- Determine next reference data for all steps increments of current input dat
    p_ref_dat_arr : process(in_dat_reg, in_val_reg, ref_dat_arr)
    begin
      nxt_ref_dat_arr <= ref_dat_arr;
      if in_val_reg = '1' then
        for I in g_nof_steps - 1 downto 0 loop
          nxt_ref_dat_arr(I) <= INCR_UVEC(in_dat_reg, diag_steps_arr(I));
        end loop;
      end if;
    end process;

    -- Detect difference for each allowed reference data.
    p_diff_arr : process(diff_arr, in_val_reg, in_dat_reg, ref_dat_arr)
    begin
      nxt_diff_arr <= diff_arr;
      if in_val_reg = '1' then
        nxt_diff_arr <= (others => '1');
        for I in g_nof_steps - 1 downto 0 loop
          if unsigned(ref_dat_arr(I)) = unsigned(in_dat_reg) then
            nxt_diff_arr(I) <= '0';
          end if;
        end loop;
      end if;
    end process;

    -- detect diff when none of the step counter value matches
    p_diff_detect : process(diff_detect, diff_arr, in_val_dly1)
    begin
      nxt_diff_detect <= diff_detect;
      if in_val_dly1 = '1' then
        nxt_diff_detect <= '0';
        if vector_and(diff_arr) = '1' then
          nxt_diff_detect <= '1';
        end if;
      end if;
    end process;

    -- hold detected diff detect
    u_dat : entity common_lib.common_switch
    port map(
      clk         => clk,
      rst         => rst,
      switch_high => diff_detect,
      switch_low  => diff_dis,
      out_level   => diff_hold
    );

    diff_res <= (others => diff_hold);  -- convert diff_hold to diff_res slv format as used for g_use_steps=FALSE
  end generate;

  ------------------------------------------------------------------------------
  -- Report valid diag_res
  ------------------------------------------------------------------------------

  nxt_diag_res_en  <= diag_en and in_val_2_act;
  nxt_diag_res_val <= diag_res_en;

  p_diag_res : process (diff_res, diag_res_en)
  begin
    nxt_diag_res <= c_diag_res_invalid;
    if diag_res_en = '1' then
      -- The test runs AND there have been valid input samples to verify
      nxt_diag_res                 <= (others => '0');  -- MSBits of valid diag_res are 0
      nxt_diag_res(diff_res'range) <= diff_res;  -- diff_res of dat[]
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Count number of valid input data
  ------------------------------------------------------------------------------
  u_common_counter : entity common_lib.common_counter
  generic map (
    g_latency   => 1,  -- default 1 for registered count output, use 0 for immediate combinatorial count output
    g_width     => g_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => diag_dis,  -- synchronous cnt_clr is only interpreted when clken is active
    cnt_en  => in_val,
    count   => in_cnt
  );
end rtl;
