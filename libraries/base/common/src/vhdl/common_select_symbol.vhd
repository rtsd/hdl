-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_components_pkg.all;

-- Purpose: Select symbol from input data stream
-- Description:
--   The in_data is a concatenation of g_nof_symbols, that are each g_symbol_w
--   bits wide. The symbol with index set by in_sel is passed on to the output
--   out_dat.
-- Remarks:
-- . If the in_select index is too large for g_nof_input range then the output
--   passes on symbol 0.

entity common_select_symbol is
  generic (
    g_pipeline_in  : natural := 0;
    g_pipeline_out : natural := 1;
    g_nof_symbols  : natural := 4;
    g_symbol_w     : natural := 16;
    g_sel_w        : natural := 2
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_data    : in  std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    in_val     : in  std_logic := '0';
    in_sop     : in  std_logic := '0';
    in_eop     : in  std_logic := '0';
    in_sync    : in  std_logic := '0';

    in_sel     : in  std_logic_vector(g_sel_w - 1 downto 0);
    out_sel    : out std_logic_vector(g_sel_w - 1 downto 0);  -- pipelined in_sel, use range to allow leaving it OPEN

    out_symbol : out std_logic_vector(g_symbol_w - 1 downto 0);
    out_val    : out std_logic;  -- pipelined in_val
    out_sop    : out std_logic;  -- pipelined in_sop
    out_eop    : out std_logic;  -- pipelined in_eop
    out_sync   : out std_logic  -- pipelined in_sync
  );
end common_select_symbol;

architecture rtl of common_select_symbol is
  constant c_pipeline   : natural := g_pipeline_in + g_pipeline_out;

  signal in_data_reg    : std_logic_vector(in_data'range);
  signal in_sel_reg     : std_logic_vector(in_sel'range);

  signal sel_symbol     : std_logic_vector(g_symbol_w - 1 downto 0);
begin
  -- pipeline input
  u_pipe_in_data : common_pipeline generic map ("SIGNED", g_pipeline_in, 0, in_data'LENGTH, in_data'length) port map (rst, clk, '1', '0', '1', in_data, in_data_reg);
  u_pipe_in_sel  : common_pipeline generic map ("SIGNED", g_pipeline_in, 0, g_sel_w,        g_sel_w)        port map (rst, clk, '1', '0', '1', in_sel,  in_sel_reg);

  no_sel : if g_nof_symbols = 1 generate
    sel_symbol <= in_data_reg;
  end generate;

  gen_sel : if g_nof_symbols > 1 generate
    -- Default pass on symbol 0 else if supported pass on the selected symbol
    p_sel : process(in_sel_reg, in_data_reg)
    begin
      sel_symbol <= in_data_reg(g_symbol_w - 1 downto 0);

      for I in g_nof_symbols - 1 downto 0 loop
        if TO_UINT(in_sel_reg) = I then
          sel_symbol <= in_data_reg((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);
        end if;
      end loop;
    end process;
  end generate;

  -- pipeline selected symbol output and control outputs
  u_pipe_out_symbol : common_pipeline generic map ("SIGNED", g_pipeline_out, 0, g_symbol_w,    g_symbol_w)    port map (rst, clk, '1', '0', '1', sel_symbol, out_symbol);
  u_pipe_out_sel    : common_pipeline generic map ("SIGNED", c_pipeline,     0, in_sel'LENGTH, in_sel'length) port map (rst, clk, '1', '0', '1', in_sel,     out_sel);

  u_pipe_out_val  : common_pipeline_sl generic map (c_pipeline, 0, false) port map (rst, clk, '1', '0', '1', in_val,  out_val);
  u_pipe_out_sop  : common_pipeline_sl generic map (c_pipeline, 0, false) port map (rst, clk, '1', '0', '1', in_sop,  out_sop);
  u_pipe_out_eop  : common_pipeline_sl generic map (c_pipeline, 0, false) port map (rst, clk, '1', '0', '1', in_eop,  out_eop);
  u_pipe_out_sync : common_pipeline_sl generic map (c_pipeline, 0, false) port map (rst, clk, '1', '0', '1', in_sync, out_sync);
end rtl;
