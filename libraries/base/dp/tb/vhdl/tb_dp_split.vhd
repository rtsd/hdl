-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

-- run 300 us

entity tb_dp_split is
  generic (
    g_data_w               : natural := 64;  -- g_data_w/g_symbol_w must be an integer
    g_symbol_w             : natural := 16;  -- use sufficient width to avoid wrap in proc_dp_gen_frame()
                                               -- use symbols width that contains whole nibbles (4-bit) to easy debugging in HEX
    g_nof_symbols_max      : natural := 50;  -- maximum supported frame size
    g_random_control       : boolean := false  -- use TRUE for random snk_in.valid and src_in.ready control
  );
end tb_dp_split;

architecture tb of tb_dp_split is
  constant c_period               : time    := 100 ns;

  constant c_rl                   : natural := 1;
  constant c_nof_symbols_per_data : natural := g_data_w / g_symbol_w;
  constant c_nof_symbols_w        : natural := ceil_log2(g_nof_symbols_max + 1);

  constant c_sync_period          : natural := 7;
  constant c_sync_offset          : natural := 2;

  constant c_verify_mode          : boolean := false;  -- use mode FALSE to verify per frame

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '0';

  signal random_0       : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1       : std_logic_vector(15 downto 0) := (others => '0');
  signal random_2       : std_logic_vector(17 downto 0) := (others => '0');

  signal nof_symbols    : std_logic_vector(c_nof_symbols_w - 1 downto 0);
  signal in_en          : std_logic := '1';
  signal in_siso        : t_dp_siso;
  signal in_sosi        : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso       : t_dp_siso_arr(0 to 1) := (others => c_dp_siso_rdy);
  signal out_sosi       : t_dp_sosi_arr(0 to 1);

  signal verify_sync    : std_logic := '0';
  signal verify_en_0    : std_logic := '0';
  signal verify_en_1    : std_logic := '0';
  signal verify_done    : std_logic;

  signal prev_out_ready_0 : std_logic_vector(0 to c_rl);
  signal prev_out_ready_1 : std_logic_vector(0 to c_rl);
  signal out_data_0       : std_logic_vector(g_data_w - 1 downto 0);
  signal out_data_1       : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_out_data_0  : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_out_data_1  : std_logic_vector(g_data_w - 1 downto 0);
  signal expected_data_0  : std_logic_vector(g_data_w - 1 downto 0);
  signal expected_data_1  : std_logic_vector(g_data_w - 1 downto 0);
  signal hold_out_sop_0   : std_logic;
  signal hold_out_sop_1   : std_logic;
begin
  rst  <= '1', '0' after c_period * 7;
  clk  <=  not clk or tb_end after c_period / 2;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);
  random_2 <= func_common_random(random_2) when rising_edge(clk);

  gen_random_control : if g_random_control = true generate
    in_en             <= random_0(random_0'high);
    out_siso(0).ready <= random_1(random_1'high);
    out_siso(1).ready <= random_2(random_2'high);
  end generate;  -- else the in_en and out_siso lines are always active

  p_stimuli : process
    variable vINIT  : natural := 0;
    variable v_sync : std_logic;
    variable v_bsn  : natural := 0;
  begin
    verify_done <= '0';
    nof_symbols <= TO_UVEC(4, c_nof_symbols_w);
    wait for 15 * c_period;
    wait until rising_edge(clk);

    -- frames
    vINIT := 0;
--     FOR vLEN IN 3 TO 3 LOOP
--       FOR vNOF IN 1 TO 1 LOOP --vLEN+c_nof_symbols_per_data LOOP
    for vLEN in 1 to g_nof_symbols_max loop
      for vNOF in 0 to vLEN + c_nof_symbols_per_data loop
        nof_symbols <= TO_UVEC(vNOF, c_nof_symbols_w);
        wait for 3 * c_period;

        v_sync := sel_a_b(v_bsn mod c_sync_period = c_sync_offset, '1', '0');
        proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w, vINIT, vLEN, v_bsn, v_sync, clk, in_en, in_siso, in_sosi);
        v_bsn := v_bsn + 1;
        vINIT := (vINIT + vLEN) mod 2**g_symbol_w;
      end loop;
    end loop;

    -- the end
    expected_data_0 <= TO_UVEC(67, g_data_w);
    expected_data_1 <= TO_UVEC(67, g_data_w);
    wait for 10 * c_period;
    verify_done <= '1';
    wait until rising_edge(clk);
    verify_done <= '0';

    wait for 100 * c_period;
    wait until rising_edge(clk);
    tb_end <= '1';
    wait;
  end process;

  dut : entity work.dp_split
  generic map (
    g_data_w      => g_data_w,
    g_symbol_w    => g_symbol_w,
    g_nof_symbols => g_nof_symbols_max
  )
  port map (
    rst         => rst,
    clk         => clk,
    nof_symbols => nof_symbols,
    snk_out     => in_siso,  -- OUT = request to upstream ST source
    snk_in      => in_sosi,
    src_in_arr  => out_siso,  -- IN  = request from downstream ST sink
    src_out_arr => out_sosi
  );

  -- Output verify
  -- Note that verify per frame can only verify frames that are longer than 1 data word
  proc_dp_verify_en(c_verify_mode, clk, out_sosi(0).valid, out_sosi(0).sop, out_sosi(0).eop, verify_en_0);
  proc_dp_verify_en(c_verify_mode, clk, out_sosi(1).valid, out_sosi(1).sop, out_sosi(1).eop, verify_en_1);

  out_data_0 <= out_sosi(0).data(g_data_w - 1 downto 0);
  out_data_1 <= out_sosi(1).data(g_data_w - 1 downto 0);

  -- Using proc_dp_verify_data() only suits c_nof_symbols_per_data = 1, therefore use proc_dp_verify_symbols() instead
  gen_verify_data : if c_nof_symbols_per_data = 1 generate
    proc_dp_verify_data("out_data_0", c_rl, clk, verify_en_0, out_siso(0).ready, out_sosi(0).valid, out_data_0, prev_out_data_0);
    proc_dp_verify_data("out_data_1", c_rl, clk, verify_en_1, out_siso(1).ready, out_sosi(1).valid, out_data_1, prev_out_data_1);
  end generate;

  -- Using proc_dp_verify_symbols() suits c_nof_symbols_per_data >= 1
  proc_dp_verify_symbols(c_rl, g_data_w, g_symbol_w, clk, verify_en_0, out_siso(0).ready, out_sosi(0).valid, out_sosi(0).eop, out_data_0, out_sosi(0).empty, prev_out_data_0);
  proc_dp_verify_symbols(c_rl, g_data_w, g_symbol_w, clk, verify_en_1, out_siso(1).ready, out_sosi(1).valid, out_sosi(1).eop, out_data_1, out_sosi(1).empty, prev_out_data_1);

  proc_dp_verify_valid(c_rl, clk, verify_en_0, out_siso(0).ready, prev_out_ready_0, out_sosi(0).valid);
  proc_dp_verify_valid(c_rl, clk, verify_en_1, out_siso(1).ready, prev_out_ready_1, out_sosi(1).valid);

  -- Verify that sop and eop come in pairs
  proc_dp_verify_sop_and_eop(clk, out_sosi(0).valid, out_sosi(0).sop, out_sosi(0).eop, hold_out_sop_0);
  proc_dp_verify_sop_and_eop(clk, out_sosi(1).valid, out_sosi(1).sop, out_sosi(1).eop, hold_out_sop_1);

  verify_sync <= '1' when verify_en_0 = '1' or verify_en_1 = '1';
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_sync, out_sosi(0).sync, out_sosi(0).sop, out_sosi(0).bsn);
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_sync, out_sosi(1).sync, out_sosi(1).sop, out_sosi(1).bsn);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_at_least, clk, verify_done, expected_data_0, out_data_0);
  proc_dp_verify_value(e_at_least, clk, verify_done, expected_data_1, out_data_1);
end tb;
