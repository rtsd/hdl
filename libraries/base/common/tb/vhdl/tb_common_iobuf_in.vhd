-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
--   > as 10
--   > run 10 us
--   in Wave window zoom in and expand out_dat to see the 50 ps delays

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_iobuf_in is
end tb_common_iobuf_in;

architecture tb of tb_common_iobuf_in is
  constant clk_period   : time := 10 ns;

  constant c_width      : natural := 8;
  constant c_delay_init : natural := 0;
  constant c_delay_incr : natural := 1;
  constant c_delay_arr  : t_natural_arr(0 to c_width - 1) := array_init(c_delay_init, c_width, c_delay_incr);  -- 0, 1, 2, 3, ...

  signal rst      : std_logic;
  signal clk      : std_logic := '0';

  signal in_dat   : std_logic_vector(c_width - 1 downto 0) := (others => '0');
  signal out_dat  : std_logic_vector(c_width - 1 downto 0);
begin
  clk <= not clk  after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  in_dat <= not(in_dat) when rising_edge(clk);

  u_dut : entity work.common_iobuf_in
  generic map (
    g_width      => c_width,
    g_delay_arr  => c_delay_arr
  )
  port map (
    config_rst => rst,
    config_clk => clk,
    in_dat     => in_dat,
    out_dat    => out_dat
  );
end tb;
