-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose: Verify multiple variations of tb_reorder_col_select_all
-- Description:
-- Usage:
-- > as 5
-- > run -all
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_reorder_col_select_all is
end tb_tb_reorder_col_select_all;

architecture tb of tb_tb_reorder_col_select_all is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- g_dsp_data_w            : natural := 16;  -- complex data width, = c_data_w / 2
-- g_nof_sync              : natural := 2;
-- g_nof_packets_per_sync  : natural := 3;
-- g_nof_blocks_per_packet : natural := 5;
-- g_nof_data_per_block    : natural := 3;
-- g_inter_valid_gap       : natural := 5;  -- nof clk gap in in_sosi.valid
-- g_inter_packet_gap      : natural := 0;  -- nof clk gap between in_sosi.eop and next in_sosi.sop
-- g_use_complex           : boolean := false;
-- g_use_identity          : boolean := false;  -- reorder identity or transpose
-- g_use_dynamic_selection : boolean := true

  u_data_5_3_no_gaps_identity : entity work.tb_reorder_col_select_all generic map(16, 3, 2, 5, 3, 0,  0, false,  true,  false);

  u_complex_5_3_no_gaps       : entity work.tb_reorder_col_select_all generic map(16, 3, 2, 5, 3, 0,  0, true,  false, false);
  u_data_5_3_no_gaps          : entity work.tb_reorder_col_select_all generic map(16, 3, 3, 5, 3, 0,  0, false, false, false);
  u_data_5_1_no_gaps          : entity work.tb_reorder_col_select_all generic map(16, 3, 4, 5, 1, 0,  0, false, false, false);
  u_data_1_3_no_gaps          : entity work.tb_reorder_col_select_all generic map(16, 3, 5, 1, 3, 0,  0, false, false, false);
  u_data_3_5_pkt_gap_1        : entity work.tb_reorder_col_select_all generic map(16, 3, 6, 3, 5, 0,  1, false, false, false);
  u_data_3_5_valid_gap_1      : entity work.tb_reorder_col_select_all generic map(16, 3, 6, 3, 5, 1,  0, false, false, false);

  u_dynamic_data_5_3_no_gaps  : entity work.tb_reorder_col_select_all generic map(16, 3, 3, 5, 3, 0,  0, false, false, true);
  u_dynamic_data_5_3_gaps     : entity work.tb_reorder_col_select_all generic map(16, 3, 3, 5, 3, 10, 100, false, false, true);
end tb;
