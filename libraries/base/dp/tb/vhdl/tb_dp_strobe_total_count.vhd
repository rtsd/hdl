-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Test bench for dp_strobe_total_count.vhd
-- Description:
-- * maximum g_count_w = 31 to fit in largest NATURAL
-- * Use g_mm_w < c_word_w = 32 to simulate large counts in feasible time.
-- * Use g_nof_sync in combination with g_mm_w and g_count_w to achieve
--   . a count > 2**g_mm_w that uses the high part of an MM word
--   . a count > 2**(g_mm_w*2) that will clip at 2**g_mm_w - 1
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_components_pkg.all;

entity tb_dp_strobe_total_count is
  generic (
    g_mm_w                 : natural := 8;
    g_count_w              : natural := 7;
    g_nof_blocks_per_sync  : natural := 10;
    g_nof_valid_per_blk    : natural := 10;
    g_nof_sync             : natural := 10;
    g_gap_size             : natural := 3
  );
end tb_dp_strobe_total_count;

architecture tb of tb_dp_strobe_total_count is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_dp_clk_period  : time := 5 ns;
  constant c_mm_clk_period  : time := 10 ns;

  constant c_skip_nof_sync         : natural := 3;
  constant c_tb_nof_sync           : natural := c_skip_nof_sync + g_nof_sync;
  constant c_clip                  : boolean := true;

  -- dut
  constant c_nof_counts_max        : natural := c_dp_strobe_total_count_reg_nof_counts_max;  -- fixed by REGMAP
  constant c_nof_counts            : natural := 3;  -- count stimuli.sync, sop, valid
  constant c_count_max             : natural := 2**g_count_w - 1;
  constant c_mm_addr_clear         : natural := c_dp_strobe_total_count_reg_clear_adr;

  -- c_tb_nof_sync - c_skip_nof_sync because first sync intervals are skipped
  -- by using clear, and -1, because stimuli_sosi.sync is used as ref_sync,
  -- and ref_sync is used to hold the counts.
  constant c_exp_nof_sync          : natural := c_tb_nof_sync - c_skip_nof_sync - 1;
  constant c_nof_blk               : natural := g_nof_blocks_per_sync * c_exp_nof_sync;
  constant c_exp_nof_sop           : natural := sel_a_b(c_nof_blk < c_count_max, c_nof_blk, c_count_max);
  constant c_nof_valid             : natural := c_nof_blk * g_nof_valid_per_blk;
  constant c_exp_nof_valid         : natural := sel_a_b(c_nof_valid < c_count_max, c_nof_valid, c_count_max);

  signal dp_clk             : std_logic := '1';
  signal mm_clk             : std_logic := '1';
  signal rst                : std_logic := '1';
  signal tb_end             : std_logic := '0';

  signal stimuli_rst        : std_logic := '1';
  signal stimuli_end        : std_logic;
  signal stimuli_sosi       : t_dp_sosi;
  signal stimuli_strobe_arr : std_logic_vector(c_nof_counts - 1 downto 0);

  signal reg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso           : t_mem_miso := c_mem_miso_rst;

  signal rd_count_arr       : t_natural_arr(c_nof_counts - 1 downto 0);
  signal exp_count_arr      : t_natural_arr(c_nof_counts - 1 downto 0) := (c_exp_nof_valid, c_exp_nof_sop, c_exp_nof_sync);
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  stimuli_rst <= '1', '0' after c_dp_clk_period * 17;

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => g_nof_blocks_per_sync * c_tb_nof_sync,
    g_pkt_len     => g_nof_valid_per_blk,
    g_pkt_gap     => g_gap_size
  )
  port map (
    rst               => stimuli_rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  stimuli_strobe_arr(0) <= stimuli_sosi.sync;
  stimuli_strobe_arr(1) <= stimuli_sosi.sop;
  stimuli_strobe_arr(2) <= stimuli_sosi.valid;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut : entity work.dp_strobe_total_count
  generic map (
    g_mm_w           => g_mm_w,
    g_nof_counts     => c_nof_counts,
    g_count_w        => g_count_w,
    g_clip           => c_clip
  )
  port map (
    dp_rst        => rst,
    dp_clk        => dp_clk,

    ref_sync      => stimuli_sosi.sync,
    in_strobe_arr => stimuli_strobe_arr,

    mm_rst        => rst,
    mm_clk        => mm_clk,

    reg_mosi      => reg_mosi,
    reg_miso      => reg_miso
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  p_mm : process
    variable v_rd_data  : std_logic_vector(g_mm_w - 1 downto 0);
    variable v_rd_count : natural;
  begin
    -- Wait until strobes have started, skip first sync intervals
    for I in 0 to c_skip_nof_sync - 1 loop
      proc_common_wait_until_lo_hi(dp_clk, stimuli_sosi.sync);
    end loop;
    proc_common_wait_some_cycles(dp_clk, 1);
    -- Clear to restart counting at next sync interval
    proc_mem_mm_bus_wr(c_mm_addr_clear, 1, mm_clk, reg_mosi);

    proc_common_wait_until_lo_hi(dp_clk, stimuli_end);
    proc_common_wait_some_cycles(dp_clk, 10);

    proc_common_wait_some_cycles(mm_clk, 1);
    for I in 0 to c_nof_counts - 1 loop
      -- read low part
      proc_mem_mm_bus_rd(I * 2, mm_clk, reg_miso, reg_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      v_rd_data := reg_miso.rddata(g_mm_w - 1 downto 0);
      v_rd_count := TO_UINT(v_rd_data);
      -- read and add high part
      proc_mem_mm_bus_rd(I * 2 + 1, mm_clk, reg_miso, reg_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      v_rd_data := reg_miso.rddata(g_mm_w - 1 downto 0);
      v_rd_count := v_rd_count + TO_UINT(v_rd_data) * 2**g_mm_w;
      rd_count_arr(I) <= v_rd_count;
      assert exp_count_arr(I) = v_rd_count
        report "Wrong total block count(" & natural'image(I) & ")"
        severity ERROR;
    end loop;

    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;
end tb;
