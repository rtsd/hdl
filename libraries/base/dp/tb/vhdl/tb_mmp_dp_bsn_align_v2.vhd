-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 6 sept 2021
-- Purpose: Verify MM part of mmp_dp_bsn_align_v2
-- Description:
--    The functional part is already verified by tb_tb_dp_bsn_align_v2.vhd.
--    Tb features:
--    . verify expected end values for output data and bsn in gen_verify_ctrl
--      to ensure that test has ran
--    . verify MM access to input stream enable in p_mm_verify_bsn_align
--    . verify MM access to input BSN monitors by verifiying expected input
--      latencies for different input delays
--    . verify MM access to output BSN monitor by verifiying expected output
--      latencies.
--
-- Usage:
-- > as 5
-- > run -all

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mmp_dp_bsn_align_v2 is
  generic (
    g_lost_input  : boolean := true  -- when TRUE use c_nof_streams-1 as lost input
  );
end tb_mmp_dp_bsn_align_v2;

architecture tb of tb_mmp_dp_bsn_align_v2 is
  constant c_mm_clk_period              : time := 40 ns;
  constant c_dp_clk_period              : time := 10 ns;
  constant c_cross_clock_domain_latency : natural := 20;

  constant c_rl                         : natural := 1;
  constant c_tb_nof_restart             : natural := 2;  -- number of times to restart the input stimuli
  constant c_tb_nof_blocks              : natural := 50;  -- number of input blocks per restart

  -- Fixed dut generics
  -- . for dp_bsn_align_v2
  constant c_nof_streams                : natural := 5;
  constant c_bsn_latency_max            : natural := 1;
  constant c_bsn_latency_first_node     : natural := c_bsn_latency_max;
  constant c_nof_aligners_max           : positive := 1;  -- fixed in this tb
  constant c_block_size                 : natural := 11;
  constant c_block_period               : natural := 11;
  constant c_block_per_sync             : natural := 7;
  constant c_bsn_w                      : natural := c_dp_stream_bsn_w;
  constant c_data_w                     : natural := 16;
  constant c_data_replacement_value     : integer := 17;
  constant c_use_mm_output              : boolean := false;
  constant c_pipeline_input             : natural := 1;
  constant c_pipeline_output            : natural := 1;
  constant c_rd_latency                 : natural := 2;
  -- . for mms_dp_bsn_monitor_v2
  constant c_nof_clk_per_sync           : natural := c_block_per_sync * c_block_period;
  constant c_nof_input_bsn_monitors     : natural := c_nof_streams;
  constant c_use_bsn_output_monitor     : boolean := true;
  constant c_mon_sync_latency           : natural := 1;  -- due to sync_reg2 in dp_bsn_monitor_v2

  constant c_reg_bsn_monitor_adr_w      : natural := ceil_log2(7);
  constant c_reg_bsn_monitor_span       : natural := 2**c_reg_bsn_monitor_adr_w;

  -- maximum nof clk delay between any inputs, <= c_align_latency_nof_clk
  -- . the -1 is due to some acceptable pipeline detail related to dp_block_from_mm
  constant c_diff_delay_max             : natural := c_bsn_latency_max * c_block_period - sel_a_b(c_rd_latency > 1, 0, 1);
  constant c_diff_delay                 : natural := c_diff_delay_max;

  -- Return input delay as function of inputs stream index I
  function func_input_delay(I : natural) return natural is
  begin
    return c_diff_delay * I / (c_nof_streams - 1);
  end;

  -- Input stream settings
  constant c_data_init                  : integer := 0;
  constant c_bsn_init                   : natural := 3;
  constant c_channel_init               : integer := 0;
  constant c_err_init                   : natural := 247;
  constant c_sync_bsn_offset            : natural := 2;
  constant c_gap_size                   : natural := c_block_period - c_block_size;

  -- DUT latency
  constant c_ref_sosi_latency           : natural := 0;
  constant c_mm_to_dp_latency           : natural := 1;
  constant c_dut_latency                : natural := c_pipeline_input + c_ref_sosi_latency +
                                                     c_rd_latency + c_mm_to_dp_latency + c_pipeline_output;

  constant c_align_latency_nof_blocks   : natural := c_bsn_latency_max * c_nof_aligners_max;  -- in number blocks
  constant c_align_latency_nof_valid    : natural := c_bsn_latency_max * c_nof_aligners_max * c_block_size;  -- in number of data samples
  constant c_align_latency_nof_clk      : natural := c_bsn_latency_max * c_nof_aligners_max * c_block_period;  -- in number clk cycles

  -- Total DUT chain latency
  constant c_total_latency              : natural := c_dut_latency + c_align_latency_nof_clk;
  constant c_verify_nof_blocks          : natural := c_tb_nof_blocks - c_align_latency_nof_blocks;  -- skip last blocks that are still in the DUT buffer

  -- Signal monitoring and verification
  type t_data_arr    is array (c_nof_streams - 1 downto 0) of std_logic_vector(c_data_w - 1 downto 0);
  type t_bsn_arr     is array (c_nof_streams - 1 downto 0) of std_logic_vector(c_bsn_w - 1 downto 0);
  type t_err_arr     is array (c_nof_streams - 1 downto 0) of std_logic_vector(c_dp_stream_error_w - 1 downto 0);
  type t_channel_arr is array (c_nof_streams - 1 downto 0) of std_logic_vector(c_dp_stream_channel_w - 1 downto 0);

  signal sl1                      : std_logic := '1';
  signal mm_end                   : std_logic := '0';
  signal dp_end_arr               : std_logic_vector(c_nof_streams - 1 downto 0) := (others => '0');
  signal dp_end                   : std_logic := '0';
  signal tb_end                   : std_logic := '0';
  signal streams_enabled          : std_logic := '0';
  signal restart_cnt_arr          : t_nat_integer_arr(c_nof_streams - 1 downto 0) := (others => -1);
  signal restart_cnt              : integer := 0;

  -- MM clock domain
  signal mm_clk                   : std_logic := '1';
  signal mm_rst                   : std_logic := '1';

  signal reg_bsn_align_copi       : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_align_cipo       : t_mem_cipo;
  signal reg_input_monitor_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_input_monitor_cipo   : t_mem_cipo;
  signal reg_output_monitor_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_output_monitor_cipo  : t_mem_cipo;

  signal mon_latency_input_arr    : t_integer_arr(c_nof_streams - 1 downto 0);
  signal mon_latency_output       : integer;

  -- DP clock domain
  signal dp_clk                   : std_logic := '1';
  signal dp_rst                   : std_logic := '1';

  signal chain_node_index         : natural := 0;
  signal ref_siso_arr             : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal ref_sosi_arr             : t_dp_sosi_arr(c_nof_streams - 1 downto 0);  -- generated stimuli
  signal in_sosi_arr              : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);  -- input stimuli

  signal in_sync_arr              : std_logic_vector(c_nof_streams - 1 downto 0);
  signal in_sop_arr               : std_logic_vector(c_nof_streams - 1 downto 0);
  signal in_eop_arr               : std_logic_vector(c_nof_streams - 1 downto 0);
  signal in_val_arr               : std_logic_vector(c_nof_streams - 1 downto 0);
  signal in_data_arr              : t_data_arr;
  signal in_bsn_arr               : t_bsn_arr;
  signal in_channel_arr           : t_channel_arr;
  signal in_err_arr               : t_err_arr;

  signal out_sosi_arr             : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);  -- output
  signal out_sosi                 : t_dp_sosi;
  signal out_sync_arr             : std_logic_vector(c_nof_streams - 1 downto 0);
  signal out_sop_arr              : std_logic_vector(c_nof_streams - 1 downto 0);
  signal out_eop_arr              : std_logic_vector(c_nof_streams - 1 downto 0);
  signal out_val_arr              : std_logic_vector(c_nof_streams - 1 downto 0);
  signal out_data_arr             : t_data_arr;
  signal hold_data_arr            : t_data_arr;
  signal out_bsn_arr              : t_bsn_arr;
  signal out_bsn                  : integer;
  signal out_channel_arr          : t_channel_arr;
  signal out_err_arr              : t_err_arr;

  signal verify_done_arr          : std_logic_vector(c_nof_streams - 1 downto 0) := (others => '0');
  signal verify_done              : std_logic := '0';

  signal hold_out_sop_arr         : std_logic_vector(c_nof_streams - 1 downto 0) := (others => '0');
  signal expected_out_bsn_arr     : t_bsn_arr;
  signal expected_out_data_arr    : t_data_arr;
  signal expected_out_channel_arr : t_channel_arr;

  -- Debug signals for view in Wave window
  signal dbg_c_align_latency_nof_blocks : natural := c_align_latency_nof_blocks;
  signal dbg_c_align_latency_nof_valid  : natural := c_align_latency_nof_valid;
  signal dbg_c_align_latency_nof_clk    : natural := c_align_latency_nof_clk;
  signal dbg_c_total_latency            : natural := c_total_latency;
  signal dbg_c_verify_nof_blocks        : natural := c_verify_nof_blocks;
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  tb_end <= mm_end and dp_end;

  ------------------------------------------------------------------------------
  -- MM stimuli
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  -- MM stimuli and verification
  ------------------------------------------------------------------------------

  p_mm_verify_bsn_align : process
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Read stream enable bits, default '1' after power up
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_rd(2 * I, mm_clk, reg_bsn_align_cipo, reg_bsn_align_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      assert reg_bsn_align_cipo.rddata(0) = '1'
        report "Wrong stream disable for output " & int_to_str(I)
        severity ERROR;
    end loop;

    -- Write stream enable bits for stream_en_arr
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_wr(2 * I, 0,  mm_clk, reg_bsn_align_cipo, reg_bsn_align_copi);
    end loop;
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    -- Read stream enable bits, should now be '0'
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_rd(2 * I, mm_clk, reg_bsn_align_cipo, reg_bsn_align_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      assert reg_bsn_align_cipo.rddata(0) = '0'
        report "Wrong BSN align stream enable for output " & int_to_str(I)
        severity ERROR;
    end loop;

    -- Write stream enable bits for stream_en_arr
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_wr(2 * I, 1,  mm_clk, reg_bsn_align_cipo, reg_bsn_align_copi);
    end loop;
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    -- Read stream enable bits, should now be '1'
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_rd(2 * I, mm_clk, reg_bsn_align_cipo, reg_bsn_align_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      assert reg_bsn_align_cipo.rddata(0) = '1'
        report "Wrong BSN align stream enable for output " & int_to_str(I)
        severity ERROR;
    end loop;

    -- End of MM test
    streams_enabled <= '1';
    wait;
  end process;

  p_mm_verify_bsn_monitors : process
    variable v_exp_latency : integer;
  begin
    proc_common_wait_until_high(mm_clk, verify_done);

    -- Read input BSN monitors
    for I in 0 to c_nof_streams - 1 loop
      proc_mem_mm_bus_rd(6 + I * c_reg_bsn_monitor_span, mm_clk, reg_input_monitor_cipo, reg_input_monitor_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      mon_latency_input_arr(I) <= TO_SINT(reg_input_monitor_cipo.rddata(31 downto 0));
      proc_common_wait_some_cycles(mm_clk, 1);
      if g_lost_input = true and I = c_nof_streams - 1 then
        v_exp_latency := -1;  -- -1 for BSN monitor timeout due to lost input
        assert mon_latency_input_arr(I) = v_exp_latency
          report "Wrong input BSN monitor latency timeout for input " & int_to_str(I) &
                 " (" & int_to_str(mon_latency_input_arr(I)) &
                 " /= " & int_to_str(v_exp_latency) &
                 ")"
          severity ERROR;
      else
        v_exp_latency := c_mon_sync_latency + func_input_delay(I);
        assert mon_latency_input_arr(I) = v_exp_latency
          report "Wrong input BSN monitor latency for input " & int_to_str(I) &
                 " (" & int_to_str(mon_latency_input_arr(I)) &
                 " /= " & int_to_str(v_exp_latency) &
                 ")"
          severity ERROR;
      end if;
    end loop;

    -- Read output BSN monitor
    proc_mem_mm_bus_rd(6, mm_clk, reg_output_monitor_cipo, reg_output_monitor_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_latency_output <= TO_SINT(reg_output_monitor_cipo.rddata(31 downto 0));

    proc_common_wait_some_cycles(mm_clk, 1);
    v_exp_latency := c_mon_sync_latency + c_total_latency;
    assert mon_latency_output = v_exp_latency
      report "Wrong output BSN monitor latency (" & int_to_str(mon_latency_output) &
             " /= " & int_to_str(v_exp_latency) &
             ")"
      severity ERROR;

    -- End of MM test
    mm_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Streaming stimuli
  ------------------------------------------------------------------------------

  -- Generate data path input data (similar as in tb_mmp_dp_bsn_align_v2.vhd)
  gen_input : for I in c_nof_streams - 1 downto 0 generate
    p_stimuli : process
      variable v_sync      : std_logic := '0';
      variable v_bsn       : natural;
      variable v_data      : natural := c_data_init;
      variable v_channel   : natural := c_channel_init;
      variable v_err       : natural := c_err_init;
    begin
      v_data := v_data + I;
      ref_sosi_arr(I) <= c_dp_sosi_rst;
      proc_common_wait_until_low(dp_clk, dp_rst);
      proc_common_wait_until_high(dp_clk, streams_enabled);
      proc_common_wait_some_cycles(dp_clk, 10);
      restart_cnt_arr(I) <= restart_cnt_arr(I) + 1;

      -- Begin of stimuli
      for S in 0 to c_tb_nof_restart - 1 loop
        v_bsn := c_bsn_init;
        for R in 0 to c_tb_nof_blocks - 1 loop
          v_sync := sel_a_b(v_bsn mod c_block_per_sync = c_sync_bsn_offset, '1', '0');
          proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data, 0, 0, c_block_size, v_channel, v_err, v_sync, TO_UVEC(v_bsn, c_bsn_w), dp_clk, sl1, ref_siso_arr(I), ref_sosi_arr(I));
          v_bsn  := v_bsn + 1;
          v_data := v_data + c_block_size;
          proc_common_wait_some_cycles(dp_clk, c_gap_size);  -- create gap between frames
        end loop;
        -- Create gap between restarts
        proc_common_wait_some_cycles(dp_clk, 10);
        restart_cnt_arr(I) <= restart_cnt_arr(I) + 1;
      end loop;

      -- End of stimuli
      -- . default c_bsn_latency_max blocks remain in DUT buffer
      expected_out_bsn_arr(I) <= TO_UVEC(v_bsn - 1 - c_align_latency_nof_blocks, c_bsn_w);
      expected_out_data_arr(I) <= TO_UVEC(v_data - 1 - c_align_latency_nof_valid, c_data_w);
      -- . default no data is lost, so all channel(bit 0) lost data flags are 0
      expected_out_channel_arr(I) <= TO_DP_CHANNEL(0);
      if g_lost_input = true then
        if I = c_nof_streams - 1 then
          expected_out_data_arr(I) <= TO_UVEC(c_data_replacement_value, c_data_w);
          expected_out_channel_arr(I) <= TO_DP_CHANNEL(1);
        end if;
      end if;
      proc_common_wait_some_cycles(dp_clk, 10);
      verify_done_arr(I) <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      verify_done_arr(I) <= '0';

      -- Simulate some more to easy recognizing verify_done in Wave window
      proc_common_wait_some_cycles(dp_clk, 100);
      dp_end_arr(I) <= '1';
      wait;
    end process;
  end generate;

  verify_done <= '1' when verify_done_arr(0) = '1';
  restart_cnt <= restart_cnt_arr(0);

  dp_end <= vector_and(dp_end_arr);

  -- Model misalignment latency between the input streams to have different
  -- input BSN monitor latencies
  no_lost_input : if g_lost_input = false generate
    gen_in_sosi_arr : for I in c_nof_streams - 1 downto 0 generate
      in_sosi_arr(I) <= transport ref_sosi_arr(I) after func_input_delay(I) * c_dp_clk_period;
    end generate;
  end generate;

  one_lost_input : if g_lost_input = true generate
    -- Model missing enabled input stream at index c_lost_input = c_nof_streams-1
    in_sosi_arr(c_nof_streams - 1) <= c_dp_sosi_rst;

    gen_in_sosi_arr : for I in c_nof_streams - 2 downto 0 generate
      in_sosi_arr(I) <= transport ref_sosi_arr(I) after func_input_delay(I) * c_dp_clk_period;
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- Data verification
  ------------------------------------------------------------------------------

  mon_sosi : for I in c_nof_streams - 1 downto 0 generate
    -- Ease in_sosi_arr monitoring
    in_sync_arr(I)    <= in_sosi_arr(I).sync;
    in_sop_arr(I)     <= in_sosi_arr(I).sop;
    in_eop_arr(I)     <= in_sosi_arr(I).eop;
    in_val_arr(I)     <= in_sosi_arr(I).valid;
    in_data_arr(I)    <= in_sosi_arr(I).data(c_data_w - 1 downto 0);
    in_bsn_arr(I)     <= in_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);
    in_channel_arr(I) <= in_sosi_arr(I).channel;
    in_err_arr(I)     <= in_sosi_arr(I).err;

    -- Ease out_sosi_arr monitoring and verification
    out_sync_arr(I)    <= out_sosi_arr(I).sync;
    out_sop_arr(I)     <= out_sosi_arr(I).sop;
    out_eop_arr(I)     <= out_sosi_arr(I).eop;
    out_val_arr(I)     <= out_sosi_arr(I).valid;
    out_data_arr(I)    <= out_sosi_arr(I).data(c_data_w - 1 downto 0);
    out_bsn_arr(I)     <= out_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);
    out_channel_arr(I) <= out_sosi_arr(I).channel;
    out_err_arr(I)     <= out_sosi_arr(I).err;
  end generate;

  out_sosi <= out_sosi_arr(0);  -- take out_sosi control and info from out_sosi_arr(0)

  out_bsn <= TO_UINT(out_sosi.bsn);  -- = out_bsn_arr().bsn = out_sosi_arr(I).bsn

  gen_verify_ctrl : for I in c_nof_streams - 1 downto 0 generate
    -- . Verify that sop and eop come in pairs
    proc_dp_verify_sop_and_eop(dp_clk, out_val_arr(I), out_sop_arr(I), out_eop_arr(I), hold_out_sop_arr(I));

    -- . Verify that the stimuli have been applied at all
    hold_data_arr(I) <= out_data_arr(I) when out_val_arr(I) = '1';  -- hold last valid data
    proc_dp_verify_value("out_data_arr", e_equal, dp_clk, verify_done_arr(I), expected_out_data_arr(I), hold_data_arr(I));
    proc_dp_verify_value("out_bsn_arr", e_equal, dp_clk, verify_done_arr(I), expected_out_bsn_arr(I), out_bsn_arr(I));
    proc_dp_verify_value("out_channel_arr", e_equal, dp_clk, verify_done_arr(I), expected_out_channel_arr(I), out_channel_arr(I));
  end generate;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  u_mmp_dp_bsn_align : entity work.mmp_dp_bsn_align_v2
  generic map (
    g_nof_streams                => c_nof_streams,
    g_bsn_latency_max            => c_bsn_latency_max,
    g_bsn_latency_first_node     => c_bsn_latency_first_node,
    g_nof_aligners_max           => c_nof_aligners_max,
    g_block_size                 => c_block_size,
    g_bsn_w                      => c_bsn_w,
    g_data_w                     => c_data_w,
    g_data_replacement_value     => c_data_replacement_value,
    g_use_mm_output              => c_use_mm_output,
    g_pipeline_input             => c_pipeline_input,
    g_pipeline_output            => c_pipeline_output,
    g_rd_latency                 => c_rd_latency,
    g_nof_clk_per_sync           => c_nof_clk_per_sync,
    g_nof_input_bsn_monitors     => c_nof_input_bsn_monitors,
    g_use_bsn_output_monitor     => c_use_bsn_output_monitor
  )
  port map (
    mm_rst                  => mm_rst,
    mm_clk                  => mm_clk,

    reg_bsn_align_copi      => reg_bsn_align_copi,
    reg_bsn_align_cipo      => reg_bsn_align_cipo,

    reg_input_monitor_copi  => reg_input_monitor_copi,
    reg_input_monitor_cipo  => reg_input_monitor_cipo,

    reg_output_monitor_copi => reg_output_monitor_copi,
    reg_output_monitor_cipo => reg_output_monitor_cipo,

    dp_rst                  => dp_rst,
    dp_clk                  => dp_clk,

    chain_node_index        => chain_node_index,
    -- Streaming input
    in_sosi_arr             => in_sosi_arr,
    -- Output via local MM in dp_clk domain
    --mm_sosi                 => mm_sosi,
    --mm_copi                 => mm_copi,
    --mm_cipo_arr             => mm_cipo_arr,
    -- Output via streaming DP interface, when g_use_mm_output = TRUE.
    out_sosi_arr            => out_sosi_arr
  );
end tb;
