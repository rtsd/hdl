-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tb_corr_accumulator is
end tb_tb_corr_accumulator;

architecture tb of tb_tb_corr_accumulator is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
                                                                      --  g_nof_inputs
                                                                      --  |   g_nof_channelss
                                                                      --  |   |   g_nof_channel_accs
                                                                      --  |   |   |   g_data_w
                                                                      --  |   |   |   |
                                                                      --  |   |   |   |   g_flow_control_stimuli
                                                                      --  |   |   |   |   |
                                                                      --  |   |   |   |   |
--  u_tb_corr_accumulator_5 : ENTITY work.tb_corr_accumulator GENERIC MAP ( 1,  5, 16, 32, e_active);
  u_tb_corr_accumulator_06_a : entity work.tb_corr_accumulator generic map( 1,  6,  11, 31, e_active);
  u_tb_corr_accumulator_07_a : entity work.tb_corr_accumulator generic map( 1,  7,  65, 32, e_active);
  u_tb_corr_accumulator_08_a : entity work.tb_corr_accumulator generic map( 1,  8, 127, 33, e_active);
  u_tb_corr_accumulator_11_a : entity work.tb_corr_accumulator generic map( 1, 11, 129,  7, e_active);

  u_tb_corr_accumulator_06_r : entity work.tb_corr_accumulator generic map( 1,  6,  11, 31, e_random);
  u_tb_corr_accumulator_07_r : entity work.tb_corr_accumulator generic map( 1,  7,  65, 32, e_random);
  u_tb_corr_accumulator_08_r : entity work.tb_corr_accumulator generic map( 1,  8, 127, 33, e_random);
  u_tb_corr_accumulator_11_r : entity work.tb_corr_accumulator generic map( 1, 11, 129,  7, e_random);

  u_tb_corr_accumulator_06_p : entity work.tb_corr_accumulator generic map( 1,  6,  11, 31, e_pulse);
  u_tb_corr_accumulator_07_p : entity work.tb_corr_accumulator generic map( 1,  7,  65, 32, e_pulse);
  u_tb_corr_accumulator_08_p : entity work.tb_corr_accumulator generic map( 1,  8, 127, 33, e_pulse);
  u_tb_corr_accumulator_11_p : entity work.tb_corr_accumulator generic map( 1, 11, 129,  7, e_pulse);
end tb;
