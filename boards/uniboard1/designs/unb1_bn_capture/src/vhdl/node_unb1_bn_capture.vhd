-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture ADC samples from ADU
-- Usage:
-- 1) This node_unb1_bn_capture.vhd together with ctrl_bn_capture.vhd and
--    sopc_bn_capture.vhd in bn_capture to be synthesized
-- 2) Standalone in tb_node_bn_capture using tb_common_mem procedures to access
--    the MM interface

library IEEE, common_lib, unb1_board_lib, dp_lib, i2c_lib, ppsh_lib, aduh_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.unb1_bn_capture_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use i2c_lib.i2c_pkg.all;
use i2c_lib.i2c_commander_aduh_pkg.all;
use aduh_lib.aduh_dd_pkg.all;

entity node_unb1_bn_capture is
  generic (
    -- General
    g_sim            : boolean := false;

    -- BN capture specific
    g_bn_capture     : t_c_bn_capture := c_bn_capture;

    -- Use PHY Interface
    -- TYPE t_c_unb_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   eth10g  : NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
    g_use_phy        : t_c_unb1_board_use_phy := (1, 0, 0, 0, 0, 0, 1, 1);
    -- Auxiliary Interface
    g_aux            : t_c_unb1_board_aux := c_unb1_board_aux;
    -- ADC Interface
    g_nof_dp_phs_clk : natural := 1;  -- nof dp_phs_clk that can be used to detect the word phase
    g_ai             : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    mm_rst                    : in  std_logic;
    mm_clk                    : in  std_logic;  -- 50 MHz from xo_clk PLL in SOPC system

    dp_rst                    : in  std_logic;
    dp_clk                    : in  std_logic;  -- 200 MHz from CLK system clock
    dp_phs_clk_vec            : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0) := (others => '1');  -- divided and phase shifted dp_clk
    dp_pps                    : in  std_logic;  -- PPS in dp_clk domain

    ext_clk                   : in  std_logic;  -- 200 MHz from CLK pin

    -- MM bsn source
    reg_bsn_source_mosi       : in  t_mem_mosi;  -- Start a BSN timestamp
    reg_bsn_source_miso       : out t_mem_miso;

    -- MM bsn scheduler for WG
    reg_bsn_scheduler_wg_mosi : in  t_mem_mosi := c_mem_mosi_rst;  -- Schedule WG restart at a BSN, read current BSN
    reg_bsn_scheduler_wg_miso : out t_mem_miso;

    -- MM aduh quad
    reg_adc_quad_mosi         : in  t_mem_mosi := c_mem_mosi_rst;  -- ADUH locked status and pattern verify for two half ADUs so 4 ADC inputs A, B, C and D
    reg_adc_quad_miso         : out t_mem_miso;

    -- MM wideband waveform generator registers [0,1,2,3] for signal paths [A,B,C,D]
    reg_wg_mosi_arr           : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1) := (others => c_mem_mosi_rst);  -- = [0:3] = Waveform Generator control ports [A,B,C,D]
    reg_wg_miso_arr           : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);
    ram_wg_mosi_arr           : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1) := (others => c_mem_mosi_rst);  -- = [0:3] = Waveform Generator waveform buffer ports [A,B,C,D]
    ram_wg_miso_arr           : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);

    -- MM dp shiftram
    reg_dp_shiftram_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_shiftram_miso      : out t_mem_miso;

    -- MM signal path monitors for [A, B, C, D]
    reg_mon_mosi_arr          : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1) := (others => c_mem_mosi_rst);  -- = [0:3] = Read only access to the data mean sums and power sums [A,B,C,D]
    reg_mon_miso_arr          : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);
    ram_mon_mosi_arr          : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1) := (others => c_mem_mosi_rst);  -- = [0:3] = Read and overwrite access to the data monitor buffers [A,B,C,D]
    ram_mon_miso_arr          : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);

    -- MM registers [0,1] for I2C access with ADUs [AB,CD]
    reg_commander_mosi_arr    : in  t_mem_mosi_arr(0 to g_ai.nof_adu - 1) := (others => c_mem_mosi_rst);
    reg_commander_miso_arr    : out t_mem_miso_arr(0 to g_ai.nof_adu - 1);
    ram_protocol_mosi_arr     : in  t_mem_mosi_arr(0 to g_ai.nof_adu - 1) := (others => c_mem_mosi_rst);
    ram_protocol_miso_arr     : out t_mem_miso_arr(0 to g_ai.nof_adu - 1);
    ram_result_mosi_arr       : in  t_mem_mosi_arr(0 to g_ai.nof_adu - 1) := (others => c_mem_mosi_rst);
    ram_result_miso_arr       : out t_mem_miso_arr(0 to g_ai.nof_adu - 1);

    reg_bsn_scheduler_sp_on_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_scheduler_sp_on_miso  : out t_mem_miso;
    reg_bsn_scheduler_sp_off_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_scheduler_sp_off_miso : out t_mem_miso;

    -- ST
    sp_sosi_arr               : out t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = Signal Paths [A,B,C,D]

    --
    -- >>> Node FPGA pins
    --
    -- ADC Interface
    ADC_BI_A                  : in    std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_B                  : in    std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_A_CLK              : in    std_logic := '0';
    ADC_BI_A_CLK_RST          : out   std_logic;
    ADC_BI_C                  : in    std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D                  : in    std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D_CLK              : in    std_logic := '0';
    ADC_BI_D_CLK_RST          : out   std_logic;
    ADC_AB_SCL                : inout std_logic;  -- = ADC_SCL[0]
    ADC_AB_SDA                : inout std_logic;  -- = ADC_SDA[0]
    ADC_CD_SCL                : inout std_logic;  -- = ADC_SCL[3]
    ADC_CD_SDA                : inout std_logic  -- = ADC_SDA[3]
  );
end node_unb1_bn_capture;

architecture str of node_unb1_bn_capture is
  -- Streaming ctrl path
  constant c_dp_factor          : natural := g_ai.rx_factor * g_ai.dd_factor;
  constant c_wideband_factor    : natural := c_dp_factor;  -- = 4 for dp_clk is 200 MHz frequency and sample frequency Fs is 800 MHz
  constant c_ai_block_size      : natural := g_bn_capture.sp.nof_samples_per_block;  -- = 1024 = nof 8b ADC samples per block
  constant c_sp_block_size      : natural := c_ai_block_size / c_wideband_factor;  -- = 256  = nof 32b samples words per block

  constant c_ai_data_w          : natural := g_ai.port_w;  -- = 8
  constant c_sp_data_w          : natural := c_wideband_factor * c_ai_data_w;  -- = 32
  constant c_sp_fifo_size       : natural := 2 * c_sp_block_size;  -- able to buffer at least one block of sp data

  constant c_use_result_ram     : boolean := true;  -- FALSE

  -- Streaming output (4 signal paths with timestamp information, can be from ADU or from internal WG)
  signal i_sp_sosi_arr          : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = Signal Paths [A,B,C,D]
  signal sp_siso_arr            : t_dp_siso_arr(0 to g_ai.nof_sp - 1);  -- and e.g. A[31:0] = [A(t0), A(t1), A(t2), A(t3)], so 4 800M samples in time per one 32b word @ 200M

  signal scheduled_sp_sosi_arr  : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);
  signal scheduled_sp_siso_arr  : t_dp_siso_arr(0 to g_ai.nof_sp - 1);

  signal dp_bsn_trigger_sp_on   : std_logic;
  signal dp_bsn_trigger_sp_off  : std_logic;
  signal sp_flush_en            : std_logic;
begin
  -----------------------------------------------------------------------------
  -- Node function
  --
  -- 1)  Capture ADU or WG input into a stream
  -- 2a) I2C control for ADU AB
  --  b) I2C control for ADU CD
  -- 3)  Feed each SP through a flusher that is controlled by a scheduler
  -- 4a) Store signal paths AB in DDR3_I
  --  b) Store signal paths CD in DDR3_II
  --
  -----------------------------------------------------------------------------

  sp_sosi_arr <= i_sp_sosi_arr;

  -----------------------------------------------------------------------------
  -- 1) Capture ADU or WG input into a stream:
  -----------------------------------------------------------------------------

  -- . input from 4 signal paths A, B, C and D of 8b @ 800 MSps each
  -- . output 4 signal streams via sp_sosi_arr with 4 samples per 32b data word
  u_input: entity work.unb1_bn_capture_input
  generic map (
    g_sim            => g_sim,
    g_bn_capture     => g_bn_capture,
    g_use_phy        => g_use_phy,
    g_nof_dp_phs_clk => g_nof_dp_phs_clk,
    g_ai             => g_ai
  )
  port map (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A                  => ADC_BI_A,
    ADC_BI_B                  => ADC_BI_B,
    ADC_BI_A_CLK              => ADC_BI_A_CLK,
    ADC_BI_A_CLK_RST          => ADC_BI_A_CLK_RST,

    -- . ADU_CD
    ADC_BI_C                  => ADC_BI_C,
    ADC_BI_D                  => ADC_BI_D,
    ADC_BI_D_CLK              => ADC_BI_D_CLK,
    ADC_BI_D_CLK_RST          => ADC_BI_D_CLK_RST,

    -- Clocks and reset
    mm_rst                    => mm_rst,
    mm_clk                    => mm_clk,
    dp_rst                    => dp_rst,
    dp_clk                    => dp_clk,
    dp_phs_clk_vec            => dp_phs_clk_vec,
    dp_pps                    => dp_pps,

    -- MM bsn source
    reg_bsn_source_mosi       => reg_bsn_source_mosi,
    reg_bsn_source_miso       => reg_bsn_source_miso,

    -- MM bsn schedule WG
    reg_bsn_scheduler_wg_mosi => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_wg_miso => reg_bsn_scheduler_wg_miso,

    -- MM aduh quad
    reg_adc_quad_mosi         => reg_adc_quad_mosi,
    reg_adc_quad_miso         => reg_adc_quad_miso,

    -- MM wideband waveform generator ports [A, B, C, B]
    reg_wg_mosi_arr           => reg_wg_mosi_arr,
    reg_wg_miso_arr           => reg_wg_miso_arr,
    ram_wg_mosi_arr           => ram_wg_mosi_arr,
    ram_wg_miso_arr           => ram_wg_miso_arr,

    -- MM DP shiftram
    reg_dp_shiftram_mosi      => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso      => reg_dp_shiftram_miso,

    -- MM signal path data monitors [A, B, C, B]
    reg_mon_mosi_arr          => reg_mon_mosi_arr,
    reg_mon_miso_arr          => reg_mon_miso_arr,
    ram_mon_mosi_arr          => ram_mon_mosi_arr,
    ram_mon_miso_arr          => ram_mon_miso_arr,

    -- Streaming output (can be from ADU or from internal WG)
    sp_sosi_arr               => i_sp_sosi_arr,
    sp_siso_arr               => sp_siso_arr
  );

  ------------------------------------------------------------------------------
  -- 2a) I2C control for ADU AB
  ------------------------------------------------------------------------------

  u_i2c_adu_ab : entity i2c_lib.i2c_commander
  generic map (
    g_sim                    => g_sim,
    g_i2c_cmdr               => c_i2c_cmdr_aduh_protocol_commander,
    g_i2c_mm                 => func_i2c_sel_a_b(g_sim, c_i2c_cmdr_aduh_i2c_mm_sim, c_i2c_cmdr_aduh_i2c_mm),  -- use full address range in sim to avoid warnings
    g_i2c_phy                => func_i2c_calculate_phy(g_bn_capture.mm_clk_freq / 10**6),
    g_protocol_ram_init_file => "data/adu_protocol_ram_init.hex",
    g_use_result_ram         => c_use_result_ram
  )
  port map (
    rst                      => mm_rst,
    clk                      => mm_clk,
    sync                     => '1',

    -- Memory Mapped slave interfaces
    commander_mosi           => reg_commander_mosi_arr(0),
    commander_miso           => reg_commander_miso_arr(0),
    protocol_mosi            => ram_protocol_mosi_arr(0),
    protocol_miso            => ram_protocol_miso_arr(0),
    result_mosi              => ram_result_mosi_arr(0),
    result_miso              => ram_result_miso_arr(0),

    -- I2C interface
    scl                      => ADC_AB_SCL,
    sda                      => ADC_AB_SDA
  );

  ------------------------------------------------------------------------------
  -- 2b) I2C control for ADU CD
  ------------------------------------------------------------------------------

  u_i2c_adu_cd : entity i2c_lib.i2c_commander
  generic map (
    g_sim                    => g_sim,
    g_i2c_cmdr               => c_i2c_cmdr_aduh_protocol_commander,
    g_i2c_mm                 => func_i2c_sel_a_b(g_sim, c_i2c_cmdr_aduh_i2c_mm_sim, c_i2c_cmdr_aduh_i2c_mm),  -- use full address range in sim to avoid warnings
    g_i2c_phy                => func_i2c_calculate_phy(g_bn_capture.mm_clk_freq / 10**6),
    g_protocol_ram_init_file => "data/adu_protocol_ram_init.hex",
    g_use_result_ram         => c_use_result_ram
  )
  port map (
    rst                      => mm_rst,
    clk                      => mm_clk,
    sync                     => '1',

    -- Memory Mapped slave interfaces
    commander_mosi           => reg_commander_mosi_arr(1),
    commander_miso           => reg_commander_miso_arr(1),
    protocol_mosi            => ram_protocol_mosi_arr(1),
    protocol_miso            => ram_protocol_miso_arr(1),
    result_mosi              => ram_result_mosi_arr(1),
    result_miso              => ram_result_miso_arr(1),

    -- I2C interface
    scl                      => ADC_CD_SCL,
    sda                      => ADC_CD_SDA
  );

  ------------------------------------------------------------------------------
  -- 3) Feed each SP through a flusher that is controlled by a scheduler
  ------------------------------------------------------------------------------

  gen_flushers: for i in 0 to g_ai.nof_sp - 1 generate
    u_dp_flush: entity dp_lib.dp_flush
    generic map (
      g_framed_xon    => true,
      g_framed_xoff   => true
    )
    port map (
      rst          => dp_rst,
      clk          => dp_clk,
      -- ST sink
      snk_in       => i_sp_sosi_arr(i),
      snk_out      => sp_siso_arr(i),
      -- ST source
      src_in       => scheduled_sp_siso_arr(i),
      src_out      => scheduled_sp_sosi_arr(i),
      -- Enable flush
      flush_en     => sp_flush_en
    );
  end generate;

  -- Convert the schedulers trigger on/off pulses to flush disable/enable level
  u_sp_flush_switch : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',
    g_priority_lo  => false,
    g_or_high      => false,
    g_and_low      => false
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    switch_high => dp_bsn_trigger_sp_off,
    switch_low  => dp_bsn_trigger_sp_on,
    out_level   => sp_flush_en
  );

  u_bsn_trigger_sp_on : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => c_bn_capture_mm_cross_clock_domain,
    g_bsn_w              => c_dp_stream_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_sp_on_mosi,
    reg_miso    => reg_bsn_scheduler_sp_on_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    snk_in      => i_sp_sosi_arr(0),  -- only uses eop (= block sync), bsn[]
    trigger_out => dp_bsn_trigger_sp_on
  );

  u_bsn_trigger_sp_off : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => c_bn_capture_mm_cross_clock_domain,
    g_bsn_w              => c_dp_stream_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_sp_off_mosi,
    reg_miso    => reg_bsn_scheduler_sp_off_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    snk_in      => i_sp_sosi_arr(0),  -- only uses eop (= block sync), bsn[]
    trigger_out => dp_bsn_trigger_sp_off
  );
end str;
