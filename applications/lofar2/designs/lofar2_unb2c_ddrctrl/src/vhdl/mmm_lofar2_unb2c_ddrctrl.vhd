-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_lofar2_unb2c_ddrctrl_pkg.all;

entity mmm_lofar2_unb2c_ddrctrl is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi             : out t_mem_mosi;
    reg_ppsh_miso             : in  t_mem_miso;

    -- eth1g
    eth1g_mm_rst              : out std_logic;
    eth1g_tse_mosi            : out t_mem_mosi;
    eth1g_tse_miso            : in  t_mem_miso;
    eth1g_reg_mosi            : out t_mem_mosi;
    eth1g_reg_miso            : in  t_mem_miso;
    eth1g_reg_interrupt       : in  std_logic;
    eth1g_ram_mosi            : out t_mem_mosi;
    eth1g_ram_miso            : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi        : out t_mem_mosi;
    reg_dpmm_data_miso        : in  t_mem_miso;
    reg_dpmm_ctrl_mosi        : out t_mem_mosi;
    reg_dpmm_ctrl_miso        : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi        : out t_mem_mosi;
    reg_mmdp_data_miso        : in  t_mem_miso;
    reg_mmdp_ctrl_mosi        : out t_mem_mosi;
    reg_mmdp_ctrl_miso        : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi             : out t_mem_mosi;
    reg_epcs_miso             : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi             : out t_mem_mosi;
    reg_remu_miso             : in  t_mem_miso;

    -- Scrap RAM
    ram_scrap_mosi            : out t_mem_mosi;
    ram_scrap_miso            : in  t_mem_miso;

    -- bsn_source_v2
    reg_bsn_source_v2_mosi    : out t_mem_mosi;
    reg_bsn_source_v2_miso    : in  t_mem_miso;

    -- bsn_sceduler
    reg_bsn_scheduler_mosi : out t_mem_mosi;
    reg_bsn_scheduler_miso : in  t_mem_miso;

    -- wg_wideband_arr
    reg_wg_wideband_arr_mosi               : out t_mem_mosi;
    reg_wg_wideband_arr_miso               : in  t_mem_miso;
    ram_wg_wideband_arr_mosi               : out t_mem_mosi;
    ram_wg_wideband_arr_miso               : in  t_mem_miso;

    -- stop_in
    reg_stop_in_mosi          : out t_mem_mosi;
    reg_stop_in_miso          : in  t_mem_miso;

    -- ddrctrl_ctrl_state
    reg_ddrctrl_ctrl_state_mosi : out t_mem_mosi;
    reg_ddrctrl_ctrl_state_miso : in  t_mem_miso;

    -- io_ddr
    reg_io_ddr_mosi           : out t_mem_mosi;
    reg_io_ddr_miso           : in  t_mem_miso;

    -- data_buffer
    reg_data_buf_mosi         : out t_mem_mosi;
    reg_data_buf_miso         : in  t_mem_miso;
    ram_data_buf_mosi         : out t_mem_mosi;
    ram_data_buf_miso         : in  t_mem_miso;
    reg_rx_seq_data_mosi      : out t_mem_mosi;
    reg_rx_seq_data_miso      : in  t_mem_miso;

    -- bsn_buffer
    reg_bsn_buf_mosi          : out t_mem_mosi;
    reg_bsn_buf_miso          : in  t_mem_miso;
    ram_bsn_buf_mosi          : out t_mem_mosi;
    ram_bsn_buf_miso          : in  t_mem_miso;
    reg_rx_seq_bsn_mosi       : out t_mem_mosi;
    reg_rx_seq_bsn_miso       : in  t_mem_miso

);
end mmm_lofar2_unb2c_ddrctrl;

architecture str of mmm_lofar2_unb2c_ddrctrl is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  signal i_reset_n         : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_fpga_temp_sens  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    u_mm_file_ram_scrap           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SCRAP")
                                               port map(mm_rst, mm_clk, ram_scrap_mosi, ram_scrap_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );

    u_mm_file_reg_bsn_source_v2   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SOURCE_V2")
                                               port map(mm_rst, mm_clk, reg_bsn_source_v2_mosi, reg_bsn_source_v2_miso );

    u_mm_file_reg_bsn_scheduler   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SCHEDULER")
                                               port map(mm_rst, mm_clk, reg_bsn_scheduler_mosi, reg_bsn_scheduler_miso );

    u_mm_file_reg_wg_wideband_arr : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WG_WIDEBAND_ARR")
                                               port map(mm_rst, mm_clk, reg_wg_wideband_arr_mosi, reg_wg_wideband_arr_miso);

    u_mm_file_ram_wg_wideband_arr : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_WG_WIDEBAND_ARR")
                                               port map(mm_rst, mm_clk, ram_wg_wideband_arr_mosi, ram_wg_wideband_arr_miso);

    u_mm_file_reg_stop_in         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STOP_IN")
                                               port map(mm_rst, mm_clk, reg_stop_in_mosi, reg_stop_in_miso);

    u_mm_file_reg_ddrctrl_state   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DDRCTRL_CTRL_STATE")
                                               port map(mm_rst, mm_clk, reg_ddrctrl_ctrl_state_mosi, reg_ddrctrl_ctrl_state_miso);

    u_mm_file_reg_io_ddr          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR")
                                               port map(mm_rst, mm_clk, reg_io_ddr_mosi, reg_io_ddr_miso);

    u_mm_file_reg_data_buf        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DATA_BUF")
                                               port map(mm_rst, mm_clk, reg_data_buf_mosi, reg_data_buf_miso);

    u_mm_file_ram_data_buf        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DATA_BUF")
                                               port map(mm_rst, mm_clk, ram_data_buf_mosi, ram_data_buf_miso);

    u_mm_file_reg_rx_seq_data     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RX_SEQ_DATA")
                                               port map(mm_rst, mm_clk, reg_rx_seq_data_mosi, reg_rx_seq_data_miso);

    u_mm_file_reg_bsn_buf         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_BUF")
                                               port map(mm_rst, mm_clk, reg_bsn_buf_mosi, reg_bsn_buf_miso);

    u_mm_file_ram_bsn_buf         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_BSN_BUF")
                                               port map(mm_rst, mm_clk, ram_bsn_buf_mosi, ram_bsn_buf_miso);

    u_mm_file_reg_rx_seq_bsn      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RX_SEQ_BSN")
                                               port map(mm_rst, mm_clk, reg_rx_seq_bsn_mosi, reg_rx_seq_bsn_miso);

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;

  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_lofar2_unb2c_ddrctrl
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2c_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2c_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      ram_scrap_reset_export                    => OPEN,
      ram_scrap_clk_export                      => OPEN,
      ram_scrap_address_export                  => ram_scrap_mosi.address(8 downto 0),
      ram_scrap_write_export                    => ram_scrap_mosi.wr,
      ram_scrap_writedata_export                => ram_scrap_mosi.wrdata(c_word_w - 1 downto 0),
      ram_scrap_read_export                     => ram_scrap_mosi.rd,
      ram_scrap_readdata_export                 => ram_scrap_miso.rddata(c_word_w - 1 downto 0),

      reg_bsn_source_v2_reset_export            => OPEN,
      reg_bsn_source_v2_clk_export              => OPEN,
      reg_bsn_source_v2_address_export          => reg_bsn_source_v2_mosi.address(2 downto 0),
      reg_bsn_source_v2_read_export             => reg_bsn_source_v2_mosi.rd,
      reg_bsn_source_v2_readdata_export         => reg_bsn_source_v2_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_source_v2_write_export            => reg_bsn_source_v2_mosi.wr,
      reg_bsn_source_v2_writedata_export        => reg_bsn_source_v2_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_scheduler_reset_export            => OPEN,
      reg_bsn_scheduler_clk_export              => OPEN,
      reg_bsn_scheduler_address_export          => reg_bsn_scheduler_mosi.address(0 downto 0),
      reg_bsn_scheduler_read_export             => reg_bsn_scheduler_mosi.rd,
      reg_bsn_scheduler_readdata_export         => reg_bsn_scheduler_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_scheduler_write_export            => reg_bsn_scheduler_mosi.wr,
      reg_bsn_scheduler_writedata_export        => reg_bsn_scheduler_mosi.wrdata(c_word_w - 1 downto 0),

      reg_wg_wideband_arr_reset_export          => OPEN,
      reg_wg_wideband_arr_clk_export            => OPEN,
      reg_wg_wideband_arr_address_export        => reg_wg_wideband_arr_mosi.address(5 downto 0),
      reg_wg_wideband_arr_read_export           => reg_wg_wideband_arr_mosi.rd,
      reg_wg_wideband_arr_readdata_export       => reg_wg_wideband_arr_miso.rddata(c_word_w - 1 downto 0),
      reg_wg_wideband_arr_write_export          => reg_wg_wideband_arr_mosi.wr,
      reg_wg_wideband_arr_writedata_export      => reg_wg_wideband_arr_mosi.wrdata(c_word_w - 1 downto 0),

      ram_wg_wideband_arr_reset_export          => OPEN,
      ram_wg_wideband_arr_clk_export            => OPEN,
      ram_wg_wideband_arr_address_export        => ram_wg_wideband_arr_mosi.address(13 downto 0),
      ram_wg_wideband_arr_read_export           => ram_wg_wideband_arr_mosi.rd,
      ram_wg_wideband_arr_readdata_export       => ram_wg_wideband_arr_miso.rddata(c_word_w - 1 downto 0),
      ram_wg_wideband_arr_write_export          => ram_wg_wideband_arr_mosi.wr,
      ram_wg_wideband_arr_writedata_export      => ram_wg_wideband_arr_mosi.wrdata(c_word_w - 1 downto 0),

      reg_stop_in_reset_export                  => OPEN,
      reg_stop_in_clk_export                    => OPEN,
      reg_stop_in_address_export                => reg_stop_in_mosi.address(0 downto 0),
      reg_stop_in_read_export                   => reg_stop_in_mosi.rd,
      reg_stop_in_readdata_export               => reg_stop_in_miso.rddata(c_word_w - 1 downto 0),
      reg_stop_in_write_export                  => reg_stop_in_mosi.wr,
      reg_stop_in_writedata_export              => reg_stop_in_mosi.wrdata(c_word_w - 1 downto 0),

      ram_data_buf_reset_export                 => OPEN,
      ram_data_buf_clk_export                   => OPEN,
      ram_data_buf_address_export               => ram_data_buf_mosi.address(13 downto 0),
      ram_data_buf_read_export                  => ram_data_buf_mosi.rd,
      ram_data_buf_readdata_export              => ram_data_buf_miso.rddata(c_word_w - 1 downto 0),
      ram_data_buf_write_export                 => ram_data_buf_mosi.wr,
      ram_data_buf_writedata_export             => ram_data_buf_mosi.wrdata(c_word_w - 1 downto 0),

      ram_bsn_buf_reset_export                  => OPEN,
      ram_bsn_buf_clk_export                    => OPEN,
      ram_bsn_buf_address_export                => ram_bsn_buf_mosi.address(14 downto 0),
      ram_bsn_buf_read_export                   => ram_bsn_buf_mosi.rd,
      ram_bsn_buf_readdata_export               => ram_bsn_buf_miso.rddata(c_word_w - 1 downto 0),
      ram_bsn_buf_write_export                  => ram_bsn_buf_mosi.wr,
      ram_bsn_buf_writedata_export              => ram_bsn_buf_mosi.wrdata(c_word_w - 1 downto 0),

      reg_io_ddr_reset_export                   => OPEN,
      reg_io_ddr_clk_export                     => OPEN,
      reg_io_ddr_address_export                 => reg_io_ddr_mosi.address(1 downto 0),
      reg_io_ddr_read_export                    => reg_io_ddr_mosi.rd,
      reg_io_ddr_readdata_export                => reg_io_ddr_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_write_export                   => reg_io_ddr_mosi.wr,
      reg_io_ddr_writedata_export               => reg_io_ddr_mosi.wrdata(c_word_w - 1 downto 0),

      reg_ddrctrl_ctrl_state_reset_export       => OPEN,
      reg_ddrctrl_ctrl_state_clk_export         => OPEN,
      reg_ddrctrl_ctrl_state_address_export     => reg_ddrctrl_ctrl_state_mosi.address(0 downto 0),
      reg_ddrctrl_ctrl_state_read_export        => reg_ddrctrl_ctrl_state_mosi.rd,
      reg_ddrctrl_ctrl_state_readdata_export    => reg_ddrctrl_ctrl_state_miso.rddata(c_word_w - 1 downto 0),
      reg_ddrctrl_ctrl_state_write_export       => reg_ddrctrl_ctrl_state_mosi.wr,
      reg_ddrctrl_ctrl_state_writedata_export   => reg_ddrctrl_ctrl_state_mosi.wrdata(c_word_w - 1 downto 0)
      );
  end generate;
end str;
