-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 11 May 2022
-- Purpose: Show pow and log functions from common_pkg.vhd
-- Description:
-- Usage:
-- > run -all
library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_str_pkg.all;

entity tb_common_log is
end tb_common_log;

architecture tb of tb_common_log is
begin
  p_log : process
    constant c_range : t_integer_arr := (99, 100, 101, 127, 128, 129);
    variable vI      : natural;
  begin
    print_str("I: pow2, ceil_pow2");
    for I in 1 to 20 loop
      print_str(int_to_str(I) & ": " &
                int_to_str(pow2(I)) & ", " &
                int_to_str(ceil_pow2(I)));
    end loop;
    print_str("");
    print_str("I: ceil_log2, true_log2, true_log_pow2, is_pow2, floor_log10");
    for I in 1 to 20 loop
      print_str(int_to_str(I) & ": " &
                int_to_str(ceil_log2(I)) & ", " &
                int_to_str(true_log2(I)) & ", " &
                int_to_str(true_log_pow2(I)) & ", " &
                bool_to_str(is_pow2(I)) & ", " &
                int_to_str(floor_log10(I)));
    end loop;
    print_str("");
    print_str("I: ceil_log2, true_log2, true_log_pow2, is_pow2, floor_log10");
    for I in c_range'range loop
      vI := c_range(I);
      print_str(int_to_str(vI) & ": " &
                int_to_str(ceil_log2(vI)) & ", " &
                int_to_str(true_log2(vI)) & ", " &
                int_to_str(true_log_pow2(vI)) & ", " &
                bool_to_str(is_pow2(vI)) & ", " &
                int_to_str(floor_log10(vI)));
    end loop;
    wait;
  end process;
end tb;
