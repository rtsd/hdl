-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose:
-- . Top-Level for OpenCL Board Support Package of TA2 applications
-- Description:
-- . This BSP makes several IO-channels available to the OpenCL kernel, these
--   include:
--   . 40 GbE
--   . 10 GbE
--   . 1 GbE
--   . ADC
--   . M&C
-- --------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, technology_lib, dp_lib, tech_ddr_lib, ta2_unb2b_40GbE_lib, ta2_unb2b_10gbe_lib, ta2_unb2b_1gbe_lib, ta2_unb2b_mm_io_lib, ta2_unb2b_jesd204b_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use tech_ddr_lib.tech_ddr_component_pkg.all;
use work.top_components_pkg.all;

entity top is
  generic (
    g_design_name       : string  := "ta2_unb2b_bsp";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e1sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id       : string  := "";  -- revision_id, commit hash (first 9 chars) or number
    g_factory_image     : boolean := false;
    g_protect_addr_range: boolean := false;
    g_tech_ddr          : t_c_tech_ddr := c_tech_ddr4_8g_1600m_64
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK       : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

    -- DDR reference clocks
    MB_I_REF_CLK  : in   std_logic := '0';  -- Reference clock for MB_I
    MB_II_REF_CLK : in   std_logic := '0';  -- Reference clock for MB_II

    -- front transceivers
    QSFP_0_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_1_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2b_board_tr_ring.bus_w - 1 downto 0) := (others => '0');
    RING_0_TX    : out   std_logic_vector(c_unb2b_board_tr_ring.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2b_board_tr_ring.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2b_board_tr_ring.bus_w - 1 downto 0);

     -- back transceivers
    BCK_RX       : in    std_logic_vector(0 downto 0);
    BCK_REF_CLK  : in    std_logic;  -- Use as JESD204B_REFCLK

    -- jesd204b syncronization signals
    JESD204B_SYSREF : in    std_logic;
    JESD204B_SYNC   : out   std_logic_vector(0 downto 0);

    -- SO-DIMM Memory Bank I
    MB_I_IN      : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    MB_I_IO      : inout t_tech_ddr4_phy_io;
    MB_I_OU      : out   t_tech_ddr4_phy_ou;

    -- SO-DIMM Memory Bank II
    --MB_II_IN     : IN    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    --MB_II_IO     : INOUT t_tech_ddr4_phy_io;
    --MB_II_OU     : OUT   t_tech_ddr4_phy_ou;

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end top;

architecture str of top is
  ---------------
  -- Constants
  ---------------
  -- QSFP
  constant c_nof_qsfp_bus           : natural := 2;
  constant c_nof_streams_qsfp       : natural := c_unb2b_board_tr_qsfp.bus_w * c_nof_qsfp_bus;

  -- RING
  constant c_nof_ring_bus           : natural := 2;
  constant c_ring_bus_w             : natural := c_unb2b_board_tr_ring.bus_w;
  constant c_nof_streams_ring       : natural := c_unb2b_board_tr_ring.bus_w * c_nof_ring_bus;

  -- 40GbE
  constant c_nof_40GbE_IP           : natural := 3;

  -- 10GbE
  constant c_nof_10GbE_IP           : natural := 1;

  -- 1 GbE
  constant c_use_1GbE_udp_offload   : boolean := true;
  constant c_nof_streams_1GbE       : natural := 1;

  -- ADC
  constant c_nof_ADC                : natural := 1;

  -- Firmware version x.y
  constant c_fw_version             : t_unb2b_board_fw_version := (1, 1);
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_100M;

  ------------
  -- Signals
  ------------
  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic;

  signal st_rst                     : std_logic;
  signal st_clk                     : std_logic;

  signal mb_I_ref_rst               : std_logic;
  signal mb_II_ref_rst              : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi    : t_mem_mosi;
  signal reg_fpga_temp_sens_miso    : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- Interface: 1GbE UDP streaming ports
  signal eth1g_udp_tx_sosi_arr      : t_dp_sosi_arr(c_nof_streams_1GbE-1 downto 0);
  signal eth1g_udp_tx_siso_arr      : t_dp_siso_arr(c_nof_streams_1GbE-1 downto 0);
  signal eth1g_udp_rx_sosi_arr      : t_dp_sosi_arr(c_nof_streams_1GbE-1 downto 0);
  signal eth1g_udp_rx_siso_arr      : t_dp_siso_arr(c_nof_streams_1GbE-1 downto 0);

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- JESD204b
  signal reg_ta2_unb2b_jesd204b_mosi : t_mem_mosi;
  signal reg_ta2_unb2b_jesd204b_miso : t_mem_miso;

  -- MM IO
  signal reg_ta2_unb2b_mm_io_mosi   : t_mem_mosi;
  signal reg_ta2_unb2b_mm_io_miso   : t_mem_miso;

  -- QSFP
  signal i_QSFP_TX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);
  signal i_QSFP_RX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);

  signal unb2b_board_front_io_serial_tx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');
  signal unb2b_board_front_io_serial_rx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0);

  -- RING
  signal i_RING_TX                  : t_unb2b_board_ring_bus_2arr(c_nof_ring_bus - 1 downto 0);
  signal i_RING_RX                  : t_unb2b_board_ring_bus_2arr(c_nof_ring_bus - 1 downto 0);

  signal unb2b_board_ring_io_serial_tx_arr : std_logic_vector(c_nof_streams_ring - 1 downto 0) := (others => '0');
  signal unb2b_board_ring_io_serial_rx_arr : std_logic_vector(c_nof_streams_ring - 1 downto 0);

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_nof_qsfp_bus + c_nof_ring_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_nof_qsfp_bus + c_nof_ring_bus - 1 downto 0);
  --SIGNAL unb2b_board_qsfp_leds_tx_src_in_arr : t_dp_siso_arr(c_nof_qsfp_bus*c_quad-1 DOWNTO 0) := (OTHERS => c_dp_siso_rst);
  signal unb2b_board_qsfp_leds_tx_src_in_arr : t_dp_siso_arr((c_nof_qsfp_bus + c_nof_qsfp_bus) * c_quad - 1 downto 0) := (others => c_dp_siso_rst);

  -- Reset
  signal i_reset_n         : std_logic;
  signal i_kernel_rst    : std_logic;

  -- OpenCL kernel
  signal board_kernel_clk_clk                         : std_logic;
  signal board_kernel_clk2x_clk                       : std_logic;
  signal board_kernel_reset_reset_n                   : std_logic;
  signal board_kernel_reset_reset_n_in                : std_logic;

  signal board_kernel_cra_waitrequest                 : std_logic;
  signal board_kernel_cra_readdata                    : std_logic_vector(63 downto 0);
  signal board_kernel_cra_readdatavalid               : std_logic;
  signal board_kernel_cra_burstcount                  : std_logic_vector(0 downto 0);
  signal board_kernel_cra_writedata                   : std_logic_vector(63 downto 0);
  signal board_kernel_cra_address                     : std_logic_vector(29 downto 0);
  signal board_kernel_cra_write                       : std_logic;
  signal board_kernel_cra_read                        : std_logic;
  signal board_kernel_cra_byteenable                  : std_logic_vector(7 downto 0);
  signal board_kernel_cra_debugaccess                 : std_logic;

  signal board_kernel_irq_irq                         : std_logic_vector(0 downto 0);

  signal board_kernel_mem0_waitrequest                :  std_logic;  -- readdata
  signal board_kernel_mem0_readdata                   :  std_logic_vector(511 downto 0);  -- readdata
  signal board_kernel_mem0_readdatavalid              :  std_logic;  -- readdata
  signal board_kernel_mem0_burstcount                 :  std_logic_vector(4 downto 0);  -- := (others => 'X'); -- address
  signal board_kernel_mem0_writedata                  :  std_logic_vector(511 downto 0);  -- := (others => 'X'); -- address
  signal board_kernel_mem0_address                    :  std_logic_vector(32 downto 0);  -- := (others => 'X'); -- address
  signal board_kernel_mem0_write                      :  std_logic;  -- := 'X';             -- write
  signal board_kernel_mem0_read                       :  std_logic;  -- := 'X';             -- write
  signal board_kernel_mem0_byteenable                 :  std_logic_vector(63 downto 0);  -- := (others => 'X'); -- byteenable
  signal board_kernel_mem0_debugaccess                :  std_logic;  -- := 'X';             -- write
--  SIGNAL amm_readdata                   :  std_logic_vector(575 downto 0);                    -- readdata
--  SIGNAL amm_burstcount                 :  std_logic_vector(6 downto 0); --  := (others => 'X'); -- address
--  SIGNAL amm_writedata                  :  std_logic_vector(575 downto 0); --  := (others => 'X'); -- address
--  SIGNAL amm_byteenable                 :  std_logic_vector(71 downto 0); --  := (others => 'X'); -- byteenable

  signal board_kernel_register_mem_address            : std_logic_vector(6 downto 0);  -- := (others => 'X'); -- address
  signal board_kernel_register_mem_clken              : std_logic;  -- := 'X';             -- clken
  signal board_kernel_register_mem_chipselect         : std_logic;  -- := 'X';             -- chipselect
  signal board_kernel_register_mem_write              : std_logic;  -- := 'X';             -- write
  signal board_kernel_register_mem_readdata           : std_logic_vector(255 downto 0);  -- readdata
  signal board_kernel_register_mem_writedata          : std_logic_vector(255 downto 0);  -- := (others => 'X'); -- writedata
  signal board_kernel_register_mem_byteenable         : std_logic_vector(31 downto 0);  -- := (others => 'X'); -- byteenable

  signal ta2_unb2b_40GbE_src_out_arr                  : t_dp_sosi_arr(c_nof_40GbE_IP - 1 downto 0);
  signal ta2_unb2b_40GbE_src_in_arr                   : t_dp_siso_arr(c_nof_40GbE_IP - 1 downto 0);
  signal ta2_unb2b_40GbE_snk_out_arr                  : t_dp_siso_arr(c_nof_40GbE_IP - 1 downto 0);
  signal ta2_unb2b_40GbE_snk_in_arr                   : t_dp_sosi_arr(c_nof_40GbE_IP - 1 downto 0);
  signal ta2_unb2b_40GbE_tx_serial_r                  : std_logic_vector(4 * c_nof_40GbE_IP - 1 downto 0);
  signal ta2_unb2b_40GbE_rx_serial_r                  : std_logic_vector(4 * c_nof_40GbE_IP - 1 downto 0);

  signal ta2_unb2b_10GbE_src_out_arr                  : t_dp_sosi_arr(c_nof_10GbE_IP - 1 downto 0);
  signal ta2_unb2b_10GbE_src_in_arr                   : t_dp_siso_arr(c_nof_10GbE_IP - 1 downto 0);
  signal ta2_unb2b_10GbE_snk_out_arr                  : t_dp_siso_arr(c_nof_10GbE_IP - 1 downto 0);
  signal ta2_unb2b_10GbE_snk_in_arr                   : t_dp_sosi_arr(c_nof_10GbE_IP - 1 downto 0);
  signal ta2_unb2b_10GbE_tx_serial_r                  : std_logic_vector(c_nof_10GbE_IP - 1 downto 0);
  signal ta2_unb2b_10GbE_rx_serial_r                  : std_logic_vector(c_nof_10GbE_IP - 1 downto 0);

  signal ta2_unb2b_1GbE_src_out                       : t_dp_sosi;
  signal ta2_unb2b_1GbE_src_in                        : t_dp_siso;
  signal ta2_unb2b_1GbE_snk_out                       : t_dp_siso;
  signal ta2_unb2b_1GbE_snk_in                        : t_dp_sosi;

  signal ta2_unb2b_ADC_src_out_arr                    : t_dp_sosi_arr(c_nof_ADC - 1 downto 0);
  signal ta2_unb2b_ADC_src_in_arr                     : t_dp_siso_arr(c_nof_ADC - 1 downto 0);

  signal ta2_unb2b_mm_io_snk_in                       : t_dp_sosi;
  signal ta2_unb2b_mm_io_snk_out                      : t_dp_siso;
  signal ta2_unb2b_mm_io_src_out                      : t_dp_sosi;
  signal ta2_unb2b_mm_io_src_in                       : t_dp_siso;

  constant c_ones : std_logic_vector(511 downto 0) := (others => '1');
begin
  ------------
  -- Front IO
  ------------

  -- put the QSFP_TX/RX ports into arrays
  i_QSFP_RX(0) <= QSFP_0_RX;
  i_QSFP_RX(1) <= QSFP_1_RX;

  QSFP_0_TX <= i_QSFP_TX(0);
  QSFP_1_TX <= i_QSFP_TX(1);

  u_unb2b_board_front_io : entity unb2b_board_lib.unb2b_board_front_io
  generic map (
    g_nof_qsfp_bus => c_nof_qsfp_bus
  )
  port map (
    serial_tx_arr => unb2b_board_front_io_serial_tx_arr,
    serial_rx_arr => unb2b_board_front_io_serial_rx_arr,

    --green_led_arr => qsfp_green_led_arr(c_nof_qsfp_bus-1 DOWNTO 0),
    --red_led_arr   => qsfp_red_led_arr(c_nof_qsfp_bus-1 DOWNTO 0),

    QSFP_RX    => i_QSFP_RX,
    QSFP_TX    => i_QSFP_TX  -- ,

    --QSFP_LED   => QSFP_LED
  );

  ------------------------
  -- qsfp LEDs controller
  ------------------------
  unb2b_board_qsfp_leds_tx_src_in_arr(0).xon <= ta2_unb2b_10GbE_snk_out_arr(0).xon;
  unb2b_board_qsfp_leds_tx_src_in_arr(4).xon <= ta2_unb2b_40GbE_snk_out_arr(0).xon;
  -- ring LED indicator
  unb2b_board_qsfp_leds_tx_src_in_arr(8).xon <= ta2_unb2b_40GbE_snk_out_arr(1).xon;
  unb2b_board_qsfp_leds_tx_src_in_arr(12).xon <= ta2_unb2b_40GbE_snk_out_arr(2).xon;

  u_unb2b_board_qsfp_leds : entity unb2b_board_lib.unb2b_board_qsfp_leds
  generic map (
    g_sim             => g_sim,
    g_factory_image   => g_factory_image,
    g_nof_qsfp        => c_nof_qsfp_bus + c_nof_ring_bus,
    g_pulse_us        => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst               => mm_rst,
    clk               => mm_clk,

    tx_siso_arr       => unb2b_board_qsfp_leds_tx_src_in_arr,

    green_led_arr     => qsfp_green_led_arr(c_nof_qsfp_bus + c_nof_ring_bus - 1 downto 0),
    red_led_arr       => qsfp_red_led_arr(c_nof_qsfp_bus + c_nof_ring_bus - 1 downto 0)
  );

  gen_leds : for i in 0 to c_nof_qsfp_bus + c_nof_ring_bus - 1 generate
    QSFP_LED(i * 2)   <=  qsfp_green_led_arr(i);
    QSFP_LED(i * 2 + 1) <=  qsfp_red_led_arr(i);
  end generate;

  ------------
  -- RING IO
  ------------
  i_RING_RX(0) <= RING_0_RX;
  i_RING_RX(1) <= RING_1_RX;
  RING_0_TX <= i_RING_TX(0);
  RING_1_TX <= i_RING_TX(1);

  u_ring_io : entity unb2b_board_lib.unb2b_board_ring_io
  generic map (
    g_nof_ring_bus => c_nof_ring_bus
  )
  port map (
    serial_tx_arr => unb2b_board_ring_io_serial_tx_arr,
    serial_rx_arr => unb2b_board_ring_io_serial_rx_arr,
    RING_RX => i_RING_RX,
    RING_TX => i_RING_TX
  );

  ---------
  -- 40GbE
  ---------
  -- Front QSFP 1 40GbE Interface, IP index = 0
  unb2b_board_front_io_serial_tx_arr(7 downto 4) <= ta2_unb2b_40GbE_tx_serial_r(3 downto 0);
  ta2_unb2b_40GbE_rx_serial_r(3 downto 0) <= unb2b_board_front_io_serial_rx_arr(7 downto 4);

  -- Ring 0 (to the left), IP index = 1
  unb2b_board_ring_io_serial_tx_arr(3 downto 0) <= ta2_unb2b_40GbE_tx_serial_r(7 downto 4);
  ta2_unb2b_40GbE_rx_serial_r(7 downto 4) <= unb2b_board_ring_io_serial_rx_arr(3 downto 0);

  -- Ring 1 (to the right), IP index = 2
  unb2b_board_ring_io_serial_tx_arr(3 + c_ring_bus_w downto c_ring_bus_w) <= ta2_unb2b_40GbE_tx_serial_r(11 downto 8);
  ta2_unb2b_40GbE_rx_serial_r(11 downto 8) <= unb2b_board_ring_io_serial_rx_arr(3 + c_ring_bus_w downto c_ring_bus_w);

  u_ta2_unb2b_40GbE : entity ta2_unb2b_40GbE_lib.ta2_unb2b_40GbE
  generic map (
    g_nof_mac => c_nof_40GbE_IP
  )
  port map (
    mm_clk       => mm_clk,
    mm_rst       => mm_rst,

    clk_ref_r    => SA_CLK,

    tx_serial_r  => ta2_unb2b_40GbE_tx_serial_r,
    rx_serial_r  => ta2_unb2b_40GbE_rx_serial_r,

    kernel_clk   => board_kernel_clk_clk,
    kernel_reset => i_kernel_rst,

    src_out_arr  => ta2_unb2b_40GbE_src_out_arr,
    src_in_arr   => ta2_unb2b_40GbE_src_in_arr,
    snk_out_arr  => ta2_unb2b_40GbE_snk_out_arr,
    snk_in_arr   => ta2_unb2b_40GbE_snk_in_arr
  );

  ----------
  -- 10GbE
  ----------
  -- Front QSFP 0 10GbE Interface, IP index = 0
  unb2b_board_front_io_serial_tx_arr(0) <= ta2_unb2b_10GbE_tx_serial_r(0);
  ta2_unb2b_10GbE_rx_serial_r(0) <= unb2b_board_front_io_serial_rx_arr(0);

  u_ta2_unb2b_10GbE : entity ta2_unb2b_10GbE_lib.ta2_unb2b_10GbE
  generic map (
    g_nof_mac => c_nof_10GbE_IP
  )
  port map (
    mm_clk           => '0',  -- mm_clk,
    mm_rst           => mm_rst,

    clk_ref_r        => SA_CLK,

    tx_serial_r  => ta2_unb2b_10GbE_tx_serial_r,
    rx_serial_r  => ta2_unb2b_10GbE_rx_serial_r,

    kernel_clk   => board_kernel_clk_clk,
    kernel_reset => i_kernel_rst,

    src_out_arr  => ta2_unb2b_10GbE_src_out_arr,
    src_in_arr   => ta2_unb2b_10GbE_src_in_arr,
    snk_out_arr  => ta2_unb2b_10GbE_snk_out_arr,
    snk_in_arr   => ta2_unb2b_10GbE_snk_in_arr
  );

  -----------------------------
  -- 1GbE Monitoring & Control
  -----------------------------
  u_ta2_unb2b_1GbE : entity ta2_unb2b_1GbE_lib.ta2_unb2b_1GbE
  port map (
    st_clk           => st_clk,
    st_rst           => st_rst,

    udp_tx_sosi      => eth1g_udp_tx_sosi_arr(0),
    udp_tx_siso      => eth1g_udp_tx_siso_arr(0),
    udp_rx_sosi      => eth1g_udp_rx_sosi_arr(0),
    udp_rx_siso      => eth1g_udp_rx_siso_arr(0),

    kernel_clk       => board_kernel_clk_clk,
    kernel_reset     => i_kernel_rst,

    src_out          => ta2_unb2b_1GbE_src_out,
    src_in           => ta2_unb2b_1GbE_src_in,
    snk_out          => ta2_unb2b_1GbE_snk_out,
    snk_in           => ta2_unb2b_1GbE_snk_in
  );

  --------------------------------------
  -- Monitoring & Control UNB protocol
  --------------------------------------
  u_ta2_unb2b_mm_io : entity ta2_unb2b_mm_io_lib.ta2_unb2b_mm_io
  generic map(
    g_use_opencl => true
  )
  port map (
    mm_clk        =>  mm_clk,
    mm_rst        =>  mm_rst,

    kernel_clk    =>  board_kernel_clk_clk,
    kernel_reset  =>  i_kernel_rst,

    mm_mosi       =>  reg_ta2_unb2b_mm_io_mosi,
    mm_miso       =>  reg_ta2_unb2b_mm_io_miso,

    snk_in        =>  ta2_unb2b_mm_io_snk_in,
    snk_out       =>  ta2_unb2b_mm_io_snk_out,
    src_out       =>  ta2_unb2b_mm_io_src_out,
    src_in        =>  ta2_unb2b_mm_io_src_in

  );

  ----------
  -- ADC
  ----------
  u_ta2_unb2b_jesd204b : entity ta2_unb2b_jesd204b_lib.ta2_unb2b_jesd204b
  generic map(
    g_nof_streams => c_nof_ADC
  )
  port map(
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    jesd204b_mosi => reg_ta2_unb2b_jesd204b_mosi,
    jesd204b_miso => reg_ta2_unb2b_jesd204b_miso,

    -- JESD204B external signals
    jesd204b_refclk       => BCK_REF_CLK,
    jesd204b_sysref       => JESD204B_SYSREF,
    jesd204b_sync_n_arr   => JESD204B_SYNC,

    serial_rx_arr         => BCK_RX,

    kernel_clk            => board_kernel_clk_clk,
    kernel_reset          => i_kernel_rst,

    src_out_arr           => ta2_unb2b_ADC_src_out_arr,
    src_in_arr            => ta2_unb2b_ADC_src_in_arr
  );

  ----------
  -- DDR4
  ----------
  --u_ta2_unb2b_ddr : ENTITY work.ta2_unb2b_ddr
  --GENERIC MAP(
  --  g_use_MB_II => FALSE
  --)
  --PORT MAP(
  --  kernel_clk            => board_kernel_clk_clk,
  --  kernel_reset          => i_kernel_rst,

  --  mem0_waitrequest      => board_kernel_mem0_waitrequest,
  --  mem0_readdata         => board_kernel_mem0_readdata,
  --  mem0_readdatavalid    => board_kernel_mem0_readdatavalid,
  --  mem0_burstcount       => board_kernel_mem0_burstcount,
  --  mem0_writedata        => board_kernel_mem0_writedata,
  --  mem0_address          => board_kernel_mem0_address,
  --  mem0_write            => board_kernel_mem0_write,
  --  mem0_read             => board_kernel_mem0_read,
  --  mem0_byteenable       => board_kernel_mem0_byteenable,
  --  mem0_debugaccess      => board_kernel_mem0_debugaccess,

  --  mb_I_ref_clk          => MB_I_REF_CLK,
  --  mb_I_ref_rst          => mb_I_ref_rst,

  --  mb_I_in               => MB_I_IN,
  --  mb_I_io               => MB_I_IO,
  --  mb_I_ou               => MB_I_OU
  --);

  -----------------------------------------------------------------------------
  -- Freeze wrapper instantiation
  -----------------------------------------------------------------------------
  freeze_wrapper_inst : freeze_wrapper
  port map(
    board_kernel_clk_clk                        => board_kernel_clk_clk,
    board_kernel_clk2x_clk                      => board_kernel_clk2x_clk,
    board_kernel_reset_reset_n                  => board_kernel_reset_reset_n_in,
    board_kernel_irq_irq                        => board_kernel_irq_irq,
    board_kernel_cra_waitrequest                => board_kernel_cra_waitrequest,
    board_kernel_cra_readdata                   => board_kernel_cra_readdata,
    board_kernel_cra_readdatavalid              => board_kernel_cra_readdatavalid,
    board_kernel_cra_burstcount                 => board_kernel_cra_burstcount,
    board_kernel_cra_writedata                  => board_kernel_cra_writedata,
    board_kernel_cra_address                    => board_kernel_cra_address,
    board_kernel_cra_write                      => board_kernel_cra_write,
    board_kernel_cra_read                       => board_kernel_cra_read,
    board_kernel_cra_byteenable                 => board_kernel_cra_byteenable,
    board_kernel_cra_debugaccess                => board_kernel_cra_debugaccess,

    --board_kernel_mem0_waitrequest               => '0',
    --board_kernel_mem0_readdata                  => c_ones,
    --board_kernel_mem0_readdatavalid             => '1',
    --board_kernel_mem0_burstcount                => OPEN,
    --board_kernel_mem0_writedata                 => OPEN,
    --board_kernel_mem0_address                   => OPEN,
    --board_kernel_mem0_write                     => OPEN,
    --board_kernel_mem0_read                      => OPEN,
    --board_kernel_mem0_byteenable                => OPEN,
    --board_kernel_mem0_debugaccess               => OPEN,

    board_kernel_mem0_waitrequest               => board_kernel_mem0_waitrequest,
    board_kernel_mem0_readdata                  => board_kernel_mem0_readdata,
    board_kernel_mem0_readdatavalid             => board_kernel_mem0_readdatavalid,
    board_kernel_mem0_burstcount                => board_kernel_mem0_burstcount,
    board_kernel_mem0_writedata                 => board_kernel_mem0_writedata,
    board_kernel_mem0_address                   => board_kernel_mem0_address,
    board_kernel_mem0_write                     => board_kernel_mem0_write,
    board_kernel_mem0_read                      => board_kernel_mem0_read,
    board_kernel_mem0_byteenable                => board_kernel_mem0_byteenable,
    board_kernel_mem0_debugaccess               => board_kernel_mem0_debugaccess,

    board_kernel_register_mem_address           => board_kernel_register_mem_address,
    board_kernel_register_mem_clken             => board_kernel_register_mem_clken,
    board_kernel_register_mem_chipselect        => board_kernel_register_mem_chipselect,
    board_kernel_register_mem_write             => board_kernel_register_mem_write,
    board_kernel_register_mem_readdata          => board_kernel_register_mem_readdata,
    board_kernel_register_mem_writedata         => board_kernel_register_mem_writedata,
    board_kernel_register_mem_byteenable        => board_kernel_register_mem_byteenable,

    board_kernel_stream_src_40GbE_data          => ta2_unb2b_40GbE_src_out_arr(0).data(263 downto 0),
    board_kernel_stream_src_40GbE_valid         => ta2_unb2b_40GbE_src_out_arr(0).valid,
    board_kernel_stream_src_40GbE_ready         => ta2_unb2b_40GbE_src_in_arr(0).ready,
    board_kernel_stream_snk_40GbE_data          => ta2_unb2b_40GbE_snk_in_arr(0).data(263 downto 0),
    board_kernel_stream_snk_40GbE_valid         => ta2_unb2b_40GbE_snk_in_arr(0).valid,
    board_kernel_stream_snk_40GbE_ready         => ta2_unb2b_40GbE_snk_out_arr(0).ready,

    board_kernel_stream_src_40GbE_ring_0_data   => ta2_unb2b_40GbE_src_out_arr(1).data(263 downto 0),
    board_kernel_stream_src_40GbE_ring_0_valid  => ta2_unb2b_40GbE_src_out_arr(1).valid,
    board_kernel_stream_src_40GbE_ring_0_ready  => ta2_unb2b_40GbE_src_in_arr(1).ready,
    board_kernel_stream_snk_40GbE_ring_0_data   => ta2_unb2b_40GbE_snk_in_arr(1).data(263 downto 0),
    board_kernel_stream_snk_40GbE_ring_0_valid  => ta2_unb2b_40GbE_snk_in_arr(1).valid,
    board_kernel_stream_snk_40GbE_ring_0_ready  => ta2_unb2b_40GbE_snk_out_arr(1).ready,

    board_kernel_stream_src_40GbE_ring_1_data   => ta2_unb2b_40GbE_src_out_arr(2).data(263 downto 0),
    board_kernel_stream_src_40GbE_ring_1_valid  => ta2_unb2b_40GbE_src_out_arr(2).valid,
    board_kernel_stream_src_40GbE_ring_1_ready  => ta2_unb2b_40GbE_src_in_arr(2).ready,
    board_kernel_stream_snk_40GbE_ring_1_data   => ta2_unb2b_40GbE_snk_in_arr(2).data(263 downto 0),
    board_kernel_stream_snk_40GbE_ring_1_valid  => ta2_unb2b_40GbE_snk_in_arr(2).valid,
    board_kernel_stream_snk_40GbE_ring_1_ready  => ta2_unb2b_40GbE_snk_out_arr(2).ready,

    board_kernel_stream_src_10GbE_data          => ta2_unb2b_10GbE_src_out_arr(0).data(71 downto 0),
    board_kernel_stream_src_10GbE_valid         => ta2_unb2b_10GbE_src_out_arr(0).valid,
    board_kernel_stream_src_10GbE_ready         => ta2_unb2b_10GbE_src_in_arr(0).ready,
    board_kernel_stream_snk_10GbE_data          => ta2_unb2b_10GbE_snk_in_arr(0).data(71 downto 0),
    board_kernel_stream_snk_10GbE_valid         => ta2_unb2b_10GbE_snk_in_arr(0).valid,
    board_kernel_stream_snk_10GbE_ready         => ta2_unb2b_10GbE_snk_out_arr(0).ready,

    board_kernel_stream_src_1GbE_data           => ta2_unb2b_1GbE_src_out.data(39 downto 0),
    board_kernel_stream_src_1GbE_valid          => ta2_unb2b_1GbE_src_out.valid,
    board_kernel_stream_src_1GbE_ready          => ta2_unb2b_1GbE_src_in.ready,
    board_kernel_stream_snk_1GbE_data           => ta2_unb2b_1GbE_snk_in.data(39 downto 0),
    board_kernel_stream_snk_1GbE_valid          => ta2_unb2b_1GbE_snk_in.valid,
    board_kernel_stream_snk_1GbE_ready          => ta2_unb2b_1GbE_snk_out.ready,

    board_kernel_stream_src_mm_io_data          => ta2_unb2b_mm_io_src_out.data(71 downto 0),
    board_kernel_stream_src_mm_io_valid         => ta2_unb2b_mm_io_src_out.valid,
    board_kernel_stream_src_mm_io_ready         => ta2_unb2b_mm_io_src_in.ready,
    board_kernel_stream_snk_mm_io_data          => ta2_unb2b_mm_io_snk_in.data(31 downto 0),
    board_kernel_stream_snk_mm_io_valid         => ta2_unb2b_mm_io_snk_in.valid,
    board_kernel_stream_snk_mm_io_ready         => ta2_unb2b_mm_io_snk_out.ready,

    board_kernel_stream_src_ADC_data            => ta2_unb2b_ADC_src_out_arr(0).data(15 downto 0),
    board_kernel_stream_src_ADC_valid           => ta2_unb2b_ADC_src_out_arr(0).valid,
    board_kernel_stream_src_ADC_ready           => ta2_unb2b_ADC_src_in_arr(0).ready

  );

  i_reset_n <= not mm_rst;
  i_kernel_rst <= not board_kernel_reset_reset_n;

  -- Kernel should start later than BSP
  u_common_areset : entity common_lib.common_areset
  generic map (
    g_rst_level => '0',
    g_delay_len => 9
  )
  port map (
    in_rst  => i_kernel_rst,
    clk     => board_kernel_clk_clk,
    out_rst => board_kernel_reset_reset_n_in
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl_unb2b_board : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                     => g_sim,
    g_technology              => g_technology,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2b_board_eth_clk_freq_125M,
    g_udp_offload             => c_use_1GbE_udp_offload,
    g_udp_offload_nof_streams => c_nof_streams_1GbE,
    g_aux                     => c_unb2b_board_aux,
    g_factory_image           => g_factory_image,
    g_protect_addr_range      => g_protect_addr_range
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => st_rst,
    dp_clk                   => st_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => st_rst,
    dp_clk_in                => st_clk,

    mb_I_ref_rst             => mb_I_ref_rst,
    mb_II_ref_rst            => mb_II_ref_rst,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- eth1g UDP streaming ports
    udp_tx_sosi_arr          =>  eth1g_udp_tx_sosi_arr,
    udp_tx_siso_arr          =>  eth1g_udp_tx_siso_arr,
    udp_rx_sosi_arr          =>  eth1g_udp_rx_sosi_arr,
    udp_rx_siso_arr          =>  eth1g_udp_rx_siso_arr,

    -- RAM scrap
    ram_scrap_mosi           => c_mem_mosi_rst,
    ram_scrap_miso           => OPEN,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . DDR reference clock domains reset creation
    MB_I_REF_CLK             => MB_I_REF_CLK,
    MB_II_REF_CLK            => MB_II_REF_CLK,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- Board qsys
  -----------------------------------------------------------------------------
  board_inst : board
  port map (
      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      kernel_clk_clk                            => board_kernel_clk_clk,
      kernel_clk2x_clk                          => board_kernel_clk2x_clk,
      kernel_reset_reset_n                      => board_kernel_reset_reset_n,

      kernel_interface_sw_reset_in_reset        => mm_rst,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2b_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2b_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 4 downto 0),  -- temp fix
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 2 downto 0),  -- temp fix
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      reg_ta2_unb2b_jesd204b_address_export     => reg_ta2_unb2b_jesd204b_mosi.address(7 downto 0),
      reg_ta2_unb2b_jesd204b_read_export        => reg_ta2_unb2b_jesd204b_mosi.rd,
      reg_ta2_unb2b_jesd204b_readdata_export    => reg_ta2_unb2b_jesd204b_miso.rddata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_jesd204b_write_export       => reg_ta2_unb2b_jesd204b_mosi.wr,
      reg_ta2_unb2b_jesd204b_writedata_export   => reg_ta2_unb2b_jesd204b_mosi.wrdata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_jesd204b_waitrequest_export => reg_ta2_unb2b_jesd204b_miso.waitrequest,

      kernel_cra_waitrequest                    => board_kernel_cra_waitrequest,
      kernel_cra_readdata                       => board_kernel_cra_readdata,
      kernel_cra_readdatavalid                  => board_kernel_cra_readdatavalid,
      kernel_cra_burstcount                     => board_kernel_cra_burstcount,
      kernel_cra_writedata                      => board_kernel_cra_writedata,
      kernel_cra_address                        => board_kernel_cra_address,
      kernel_cra_write                          => board_kernel_cra_write,
      kernel_cra_read                           => board_kernel_cra_read,
      kernel_cra_byteenable                     => board_kernel_cra_byteenable,
      kernel_cra_debugaccess                    => board_kernel_cra_debugaccess,

      kernel_irq_irq                            => board_kernel_irq_irq,

      kernel_register_mem_address               => board_kernel_register_mem_address,
      kernel_register_mem_clken                 => board_kernel_register_mem_clken,
      kernel_register_mem_chipselect            => board_kernel_register_mem_chipselect,
      kernel_register_mem_write                 => board_kernel_register_mem_write,
      kernel_register_mem_readdata              => board_kernel_register_mem_readdata,
      kernel_register_mem_writedata             => board_kernel_register_mem_writedata,
      kernel_register_mem_byteenable            => board_kernel_register_mem_byteenable,

      reg_ta2_unb2b_mm_io_address_export        => reg_ta2_unb2b_mm_io_mosi.address(7 downto 0),
      reg_ta2_unb2b_mm_io_read_export           => reg_ta2_unb2b_mm_io_mosi.rd,
      reg_ta2_unb2b_mm_io_readdata_export       => reg_ta2_unb2b_mm_io_miso.rddata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_mm_io_write_export          => reg_ta2_unb2b_mm_io_mosi.wr,
      reg_ta2_unb2b_mm_io_writedata_export      => reg_ta2_unb2b_mm_io_mosi.wrdata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_mm_io_waitrequest_export    => reg_ta2_unb2b_mm_io_miso.waitrequest,

      kernel_mem0_waitrequest                   => board_kernel_mem0_waitrequest,
      kernel_mem0_readdata                      => board_kernel_mem0_readdata,
      kernel_mem0_readdatavalid                 => board_kernel_mem0_readdatavalid,
      kernel_mem0_burstcount                    => board_kernel_mem0_burstcount,
      kernel_mem0_writedata                     => board_kernel_mem0_writedata,
      kernel_mem0_address                       => board_kernel_mem0_address,
      kernel_mem0_write                         => board_kernel_mem0_write,
      kernel_mem0_read                          => board_kernel_mem0_read,
      kernel_mem0_byteenable                    => board_kernel_mem0_byteenable,
      kernel_mem0_debugaccess                   => board_kernel_mem0_debugaccess,

      ddr4a_pll_ref_clk                         => MB_I_REF_CLK,
      ddr4a_oct_oct_rzqin                       => MB_I_IN.oct_rzqin,
      ddr4a_mem_ck                              => MB_I_OU.ck(g_tech_ddr.ck_w - 1 downto 0),
      ddr4a_mem_ck_n                            => MB_I_OU.ck_n(g_tech_ddr.ck_w - 1 downto 0),
      ddr4a_mem_a                               => MB_I_OU.a(g_tech_ddr.a_w - 1 downto 0),
   sl(ddr4a_mem_act_n)                          => MB_I_OU.act_n,
      ddr4a_mem_ba                              => MB_I_OU.ba(g_tech_ddr.ba_w - 1 downto 0),
      ddr4a_mem_bg                              => MB_I_OU.bg(g_tech_ddr.bg_w - 1 downto 0),
      ddr4a_mem_cke                             => MB_I_OU.cke(g_tech_ddr.cke_w - 1 downto 0),
      ddr4a_mem_cs_n                            => MB_I_OU.cs_n(g_tech_ddr.cs_w - 1 downto 0),
      ddr4a_mem_odt                             => MB_I_OU.odt(g_tech_ddr.odt_w - 1 downto 0),
   sl(ddr4a_mem_reset_n)                        => MB_I_OU.reset_n,
   sl(ddr4a_mem_par)                            => MB_I_OU.par,
      ddr4a_mem_alert_n                         => slv(MB_I_IN.alert_n),
      ddr4a_mem_dqs                             => MB_I_IO.dqs(g_tech_ddr.dqs_w - 1 downto 0),
      ddr4a_mem_dqs_n                           => MB_I_IO.dqs_n(g_tech_ddr.dqs_w - 1 downto 0),
      ddr4a_mem_dq                              => MB_I_IO.dq(g_tech_ddr.dq_w - 1 downto 0),
      ddr4a_mem_dbi_n                           => MB_I_IO.dbi_n(g_tech_ddr.dbi_w - 1 downto 0)

  );
end str;
