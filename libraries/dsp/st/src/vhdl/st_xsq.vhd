-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author : R. vd Walle
-- Purpose:
--   Store the statistics of the complex input streams in_a and in_b with
--   blocks of g_nof_crosslets * g_nof_signal_inputs**2 multiplexed subbands into a MM register.
-- Description:
--
--   After each sync the MM register gets updated with the statistics
--   of the previous sync interval. The length of the sync interval determines
--   the nof accumlations per statistic, hence the integration time. See st_calc
--   for more details.
--
--   View wrdata_power and a_sp, b_sp and crosslet_index in Wave window to see
--   the stat power values series.
--
-- Remarks:
-- . The in_sync is assumed to be a pulse an interpreted directly.
-- . The MM register is single page RAM to save memory resources. Therefore
--   just after the sync its contents is undefined when it gets written, but
--   after that its contents remains stable for the rest of the sync interval.
--   Therefore it is not necessary to use a dual page register that swaps at
--   the sync.
-- . The minimum c_nof_statistics = 8. Lower values lead to simulation errors. This is
--   due to the read latency of 2 of the accumulation memory in the st_calc entity.
-- . More detail can be found in:
--   https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+Correlator
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.math_real.all;  -- for sim only
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity st_xsq is
  generic (
    g_nof_signal_inputs : natural := 2;
    g_nof_crosslets     : natural := 1;
    g_in_data_w         : natural := 18;  -- width of the data to be accumulated
    g_stat_data_w       : natural := 54;  -- statistics accumulator width
    g_stat_data_sz      : natural := 2  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
  );
  port (
    mm_rst : in  std_logic;
    mm_clk : in  std_logic;
    dp_rst : in  std_logic;
    dp_clk : in  std_logic;

    -- Streaming
    in_a   : in  t_dp_sosi;  -- Complex input data
    in_b   : in  t_dp_sosi;  -- Complex input data

    -- Memory Mapped
    ram_st_xsq_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_st_xsq_miso : out t_mem_miso := c_mem_miso_rst
  );
end st_xsq;

architecture str of st_xsq is
  constant c_xsq               : natural := g_nof_signal_inputs * g_nof_signal_inputs;
  constant c_nof_statistics    : natural := g_nof_crosslets * c_xsq;
  constant c_nof_stat_w        : natural := ceil_log2(c_nof_statistics);
  constant c_nof_word          : natural := g_stat_data_sz * c_nof_statistics;
  constant c_nof_word_w        : natural := ceil_log2(c_nof_word);
  constant c_stat_word_w       : natural := g_stat_data_sz * c_word_w;
  constant c_total_ram_addr_w  : natural := ceil_log2(c_nof_complex) + c_nof_word_w;

  -- Statistics register
  constant c_mm_ram   : t_c_mem := (latency  => 1,
                                    adr_w    => c_nof_word_w,
                                    dat_w    => c_word_w,
                                    nof_dat  => c_nof_word,
                                    init_sl  => '0');  -- MM side : sla_in, sla_out
  constant c_stat_ram : t_c_mem := (latency  => 1,
                                    adr_w    => c_nof_stat_w,
                                    dat_w    => c_stat_word_w,
                                    nof_dat  => c_nof_statistics,
                                    init_sl  => '0');  -- ST side : stat_mosi
  signal pipe_in_a    : t_dp_sosi;
  signal pipe_in_b    : t_dp_sosi;

  signal stat_data_re : std_logic_vector(g_stat_data_w - 1 downto 0);
  signal stat_data_im : std_logic_vector(g_stat_data_w - 1 downto 0);

  signal wrdata_re    : std_logic_vector(c_mem_data_w - 1 downto 0);
  signal wrdata_im    : std_logic_vector(c_mem_data_w - 1 downto 0);

  -- Sim only signals for observing wrdata_power with indices in Wave window
  signal wrdata_power   : real;
  signal stat_index     : natural;
  signal a_sp           : natural;
  signal b_sp           : natural;
  signal crosslet_index : natural;

  signal stat_mosi    : t_mem_mosi := c_mem_mosi_rst;

  signal ram_st_xsq_mosi_arr      : t_mem_mosi_arr(c_nof_complex - 1 downto 0) := (others => c_mem_mosi_rst);
  signal ram_st_xsq_miso_arr      : t_mem_miso_arr(c_nof_complex - 1 downto 0) := (others => c_mem_miso_rst);
  signal remapped_ram_st_xsq_mosi : t_mem_mosi;
begin
  ---------------------------------------------------------------
  -- pipeline inputs to increase latency with 1 in comparison to sync for st_calc
  ---------------------------------------------------------------
  u_dp_pipeline_a : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline => 1
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => in_a,
    -- ST source
    src_out      => pipe_in_a
  );

  u_dp_pipeline_b : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline => 1
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => in_b,
    -- ST source
    src_out      => pipe_in_b
  );

  ---------------------------------------------------------------
  -- st_calc
  ---------------------------------------------------------------
  st_calc : entity work.st_calc
  generic map (
    g_nof_mux      => 1,
    g_nof_stat     => c_nof_statistics,
    g_in_dat_w     => g_in_data_w,
    g_out_dat_w    => g_stat_data_w,
    g_out_adr_w    => c_nof_stat_w,
    g_complex      => true
  )
  port map (
    rst            => dp_rst,
    clk            => dp_clk,
    in_ar          => pipe_in_a.re(g_in_data_w - 1 downto 0),
    in_ai          => pipe_in_a.im(g_in_data_w - 1 downto 0),
    in_br          => pipe_in_b.re(g_in_data_w - 1 downto 0),
    in_bi          => pipe_in_b.im(g_in_data_w - 1 downto 0),
    in_val         => pipe_in_a.valid,
    in_sync        => in_a.sync,
    out_adr        => stat_mosi.address(c_stat_ram.adr_w - 1 downto 0),
    out_re         => stat_data_re,
    out_im         => stat_data_im,
    out_val        => stat_mosi.wr,
    out_val_m      => open
  );

  -- Cross correlations are signed values
  wrdata_re <= RESIZE_MEM_SDATA(stat_data_re);
  wrdata_im <= RESIZE_MEM_SDATA(stat_data_im);

  -- synthesis translate_off
  wrdata_power <= COMPLEX_RADIUS(TO_SREAL(stat_data_re), TO_SREAL(stat_data_im))**2.0;

  -- Indices for statistics [crosslets][in A][in B]
  stat_index <= TO_UINT(stat_mosi.address(c_stat_ram.adr_w - 1 downto 0));
  a_sp <= (stat_index / g_nof_signal_inputs) mod g_nof_signal_inputs;
  b_sp <= stat_index mod g_nof_signal_inputs;
  crosslet_index <= stat_index / c_xsq;
  -- synthesis translate_on

  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES
  ---------------------------------------------------------------
  -- Translate incoming MM interface [crosslets][in A][in B][complex][word] to
  -- [complex][crosslets][in A][in B][word]
  p_remap : process(ram_st_xsq_mosi)
  begin
    remapped_ram_st_xsq_mosi <= ram_st_xsq_mosi;
    remapped_ram_st_xsq_mosi.address(c_total_ram_addr_w - 1 downto c_nof_word_w) <= ram_st_xsq_mosi.address(ceil_log2(c_nof_complex) + ceil_log2(g_stat_data_sz) - 1 downto ceil_log2(g_stat_data_sz));
    remapped_ram_st_xsq_mosi.address(c_nof_word_w - 1 downto 0) <= ram_st_xsq_mosi.address(c_total_ram_addr_w - 1 downto ceil_log2(c_nof_complex) + ceil_log2(g_stat_data_sz)) & ram_st_xsq_mosi.address(ceil_log2(g_stat_data_sz) - 1 downto 0);
  end process;

  -- Combine the internal array of mm interfaces for both real
  -- and imaginary part.
  u_mem_mux_select : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_complex,
    g_mult_addr_w => c_nof_word_w,
    g_rd_latency  => 1
  )
  port map (
    clk      => mm_clk,
    mosi     => remapped_ram_st_xsq_mosi,
    miso     => ram_st_xsq_miso,
    mosi_arr => ram_st_xsq_mosi_arr,
    miso_arr => ram_st_xsq_miso_arr
  );

  -- ram for real values
  stat_reg_re : entity common_lib.common_ram_crw_crw_ratio
  generic map (
    g_ram_a      => c_mm_ram,
    g_ram_b      => c_stat_ram,
    g_init_file  => "UNUSED"
  )
  port map (
    rst_a     => mm_rst,
    clk_a     => mm_clk,

    rst_b     => dp_rst,
    clk_b     => dp_clk,

    wr_en_a   => ram_st_xsq_mosi_arr(0).wr,  -- only for diagnostic purposes, typically statistics are read only
    wr_dat_a  => ram_st_xsq_mosi_arr(0).wrdata(c_mm_ram.dat_w - 1 downto 0),
    adr_a     => ram_st_xsq_mosi_arr(0).address(c_mm_ram.adr_w - 1 downto 0),
    rd_en_a   => ram_st_xsq_mosi_arr(0).rd,
    rd_dat_a  => ram_st_xsq_miso_arr(0).rddata(c_mm_ram.dat_w - 1 downto 0),
    rd_val_a  => ram_st_xsq_miso_arr(0).rdval,

    wr_en_b   => stat_mosi.wr,
    wr_dat_b  => wrdata_re(c_stat_ram.dat_w - 1 downto 0),
    adr_b     => stat_mosi.address(c_stat_ram.adr_w - 1 downto 0),
    rd_en_b   => '0',
    rd_dat_b  => OPEN,
    rd_val_b  => open
  );

  -- ram for imaginary values
  stat_reg_im : entity common_lib.common_ram_crw_crw_ratio
  generic map (
    g_ram_a      => c_mm_ram,
    g_ram_b      => c_stat_ram,
    g_init_file  => "UNUSED"
  )
  port map (
    rst_a     => mm_rst,
    clk_a     => mm_clk,

    rst_b     => dp_rst,
    clk_b     => dp_clk,

    wr_en_a   => ram_st_xsq_mosi_arr(1).wr,  -- only for diagnostic purposes, typically statistics are read only
    wr_dat_a  => ram_st_xsq_mosi_arr(1).wrdata(c_mm_ram.dat_w - 1 downto 0),
    adr_a     => ram_st_xsq_mosi_arr(1).address(c_mm_ram.adr_w - 1 downto 0),
    rd_en_a   => ram_st_xsq_mosi_arr(1).rd,
    rd_dat_a  => ram_st_xsq_miso_arr(1).rddata(c_mm_ram.dat_w - 1 downto 0),
    rd_val_a  => ram_st_xsq_miso_arr(1).rdval,

    wr_en_b   => stat_mosi.wr,
    wr_dat_b  => wrdata_im(c_stat_ram.dat_w - 1 downto 0),
    adr_b     => stat_mosi.address(c_stat_ram.adr_w - 1 downto 0),
    rd_en_b   => '0',
    rd_dat_b  => OPEN,
    rd_val_b  => open
  );
end str;
