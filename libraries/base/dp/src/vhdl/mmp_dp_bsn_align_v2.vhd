-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: Eric Kooistra, 6 Sept 2021
-- Purpose: MMP for dp_bsn_align_v2
-- Description:
--   Add MM interfaces to dp_bsn_align_v2:
--
--   * Instantiates input BSN monitors when g_nof_input_bsn_monitors > 0
--   * Instantiates output BSN monitor g_use_bsn_output_monitor = TRUE
--   * Define MM reg for input enable/disable control for input i:
--
--      wi    Bits  Access     Type   Name
--     2*i     [0]      RW  boolean   input_enable
--   2*i+1  [31:0]      RO  integer   replaced_pkt_cnt
--
--      where i = 0:g_nof_streams-1 and input_enable '1' is on, '0' is off
--
--   For more description see dp_bsn_align_v2 and
--   https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+BSN+aligner+v2

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mmp_dp_bsn_align_v2 is
  generic (
    -- for dp_bsn_align_v2
    g_nof_streams                : natural := 2;  -- number of input and output streams
    g_bsn_latency_max            : natural := 2;  -- Maximum travel latency of a remote block in number of block periods
    g_bsn_latency_first_node     : natural := 2;  -- default use same as g_bsn_latency_max
    g_nof_aligners_max           : natural := 1;  -- 1 when only align at last node,
                                                  -- > 1 when align at every intermediate node
    g_block_size                 : natural := 32;  -- > 1, g_block_size=1 is not supported
    g_bsn_w                      : natural := c_dp_stream_bsn_w;  -- number of bits in sosi BSN
    g_data_w                     : natural;  -- number of bits in sosi data
    g_data_replacement_value     : integer := 0;  -- output sosi data value for missing input blocks
    g_use_mm_output              : boolean := false;  -- output via MM or via streaming DP
    g_pipeline_input             : natural := 1;  -- >= 0, 0 for wires, 1 to ease timing closure of in_sosi_arr
    g_pipeline_output            : natural := 1;  -- >= 0, 0 for wires, 1 to ease timing closure of out_sosi_arr
    g_rd_latency                 : natural := 2;  -- 1 or 2, choose 2 to ease timing closure
    -- for mms_dp_bsn_monitor_v2
    g_nof_clk_per_sync           : natural := 200 * 10**6;
    g_nof_input_bsn_monitors     : natural := 0;
    g_use_bsn_output_monitor     : boolean := false
  );
  port (
    -- Memory-mapped clock domain
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;

    reg_bsn_align_copi      : in  t_mem_copi;
    reg_bsn_align_cipo      : out t_mem_cipo;

    reg_input_monitor_copi  : in  t_mem_copi;
    reg_input_monitor_cipo  : out t_mem_cipo;

    reg_output_monitor_copi : in  t_mem_copi;
    reg_output_monitor_cipo : out t_mem_cipo;

    -- Streaming clock domain
    dp_rst                  : in  std_logic;
    dp_clk                  : in  std_logic;

    chain_node_index       : in  natural range 0 to g_nof_aligners_max - 1 := 0;  -- only used when g_nof_aligners_max > 1

    -- Streaming input
    in_sosi_arr             : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    -- Output via local MM interface in dp_clk domain, when g_use_mm_output = true.
    mm_sosi                 : out t_dp_sosi;  -- streaming information that signals that an output block can be read
    mm_copi                 : in  t_mem_copi := c_mem_copi_rst;  -- read access to output block, all output streams share same mm_copi
    mm_cipo_arr             : out t_mem_cipo_arr(g_nof_streams - 1 downto 0);

    -- Output via streaming DP interface, when g_use_mm_output = false.
    out_sosi_arr            : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mmp_dp_bsn_align_v2;

architecture str of mmp_dp_bsn_align_v2 is
  -- Use one MM word (bit 0) per input_enable bit, similar as in dp_bsn_align_reg.vhd.

  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_reg       : t_c_mem := (1, ceil_log2(2 * g_nof_streams), c_word_w, 2 * g_nof_streams, '0');

  signal reg_wr                  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '1');
  signal reg_rd                  : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');
  signal stream_en_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal stream_replaced_cnt_arr : t_slv_32_arr(g_nof_streams - 1 downto 0);

  signal ref_sync         : std_logic;
  signal mon_out_sosi_arr : t_dp_sosi_arr(0 downto 0);
  signal i_out_sosi_arr   : t_dp_sosi_arr(g_nof_streams - 1  downto 0);
  signal i_mm_sosi        : t_dp_sosi;
begin
  -- MM control of BSN aligner
  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain   => true,
    g_readback             => false,
    g_reg                  => c_mm_reg,
    g_init_reg             => (others => '1')  -- Default all g_nof_streams are enabled.
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_bsn_align_copi,
    sla_out        => reg_bsn_align_cipo,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    out_reg        => reg_wr,  -- readback via ST clock domain
    in_reg         => reg_rd
  );

  gen_reg : for I in 0 to g_nof_streams - 1 generate
    stream_en_arr(I)                             <= sl(reg_wr(2 * I * c_word_w downto 2 * I * c_word_w));
    reg_rd(2 * I * c_word_w downto 2 * I * c_word_w) <=    reg_wr(2 * I * c_word_w downto 2 * I * c_word_w);
    reg_rd((2 * I + 2) * c_word_w - 1 downto (2 * I + 1) * c_word_w) <= stream_replaced_cnt_arr(I);
  end generate;

  -- Use local sync as reference sync input for the BSN monitors
  ref_sync <= in_sosi_arr(0).sync;

  -- Use input BSN monitors for the first g_nof_input_bsn_monitors input
  -- streams, e.g. to support:
  -- . only one input stream (g_nof_input_bsn_monitors = 1), or
  -- . all input streams (g_nof_input_bsn_monitors = g_nof_streams).
  gen_bsn_mon_input : if g_nof_input_bsn_monitors > 0 generate
    u_bsn_mon_input : entity work.mms_dp_bsn_monitor_v2
    generic map (
      g_nof_streams        => g_nof_input_bsn_monitors,
      g_cross_clock_domain => true,
      g_sync_timeout       => g_nof_clk_per_sync,
      g_bsn_w              => g_bsn_w,
      g_error_bi           => 0,
      g_cnt_sop_w          => c_word_w,
      g_cnt_valid_w        => c_word_w,
      g_cnt_latency_w      => c_word_w
    )
    port map (
      -- Memory-mapped clock domain
      mm_rst         => mm_rst,
      mm_clk         => mm_clk,
      reg_mosi       => reg_input_monitor_copi,
      reg_miso       => reg_input_monitor_cipo,

      -- Streaming clock domain
      dp_rst         => dp_rst,
      dp_clk         => dp_clk,
      ref_sync       => ref_sync,

      in_sosi_arr    => in_sosi_arr(g_nof_input_bsn_monitors - 1 downto 0)
    );
  end generate;

  gen_bsn_mon_output : if g_use_bsn_output_monitor generate
    u_bsn_mon_output : entity work.mms_dp_bsn_monitor_v2
    generic map (
      g_nof_streams        => 1,  -- all outputs have same BSN monitor information
      g_cross_clock_domain => true,
      g_sync_timeout       => g_nof_clk_per_sync,
      g_bsn_w              => g_bsn_w,
      g_error_bi           => 0,
      g_cnt_sop_w          => c_word_w,
      g_cnt_valid_w        => c_word_w,
      g_cnt_latency_w      => c_word_w
    )
    port map (
      -- Memory-mapped clock domain
      mm_rst         => mm_rst,
      mm_clk         => mm_clk,
      reg_mosi       => reg_output_monitor_copi,
      reg_miso       => reg_output_monitor_cipo,

      -- Streaming clock domain
      dp_rst         => dp_rst,
      dp_clk         => dp_clk,
      ref_sync       => ref_sync,

      in_sosi_arr    => mon_out_sosi_arr
    );
  end generate;

  -- Use mm_sosi or out_sosi_arr(0) from BSN aligner for output BSN monitor
  mon_out_sosi_arr(0) <= i_mm_sosi when g_use_mm_output = true else i_out_sosi_arr(0);

  -- wire to output
  mm_sosi <= i_mm_sosi;
  out_sosi_arr <= i_out_sosi_arr;

  u_bsn_align : entity work.dp_bsn_align_v2
  generic map (
    g_nof_streams                => g_nof_streams,
    g_bsn_latency_max            => g_bsn_latency_max,
    g_bsn_latency_first_node     => g_bsn_latency_first_node,
    g_nof_aligners_max           => g_nof_aligners_max,
    g_block_size                 => g_block_size,
    g_bsn_w                      => g_bsn_w,
    g_data_w                     => g_data_w,
    g_data_replacement_value     => g_data_replacement_value,
    g_use_mm_output              => g_use_mm_output,
    g_pipeline_input             => g_pipeline_input,
    g_pipeline_output            => g_pipeline_output,
    g_rd_latency                 => g_rd_latency
  )
  port map (
    dp_rst                  => dp_rst,
    dp_clk                  => dp_clk,
    chain_node_index        => chain_node_index,
    -- MM control
    stream_en_arr           => stream_en_arr,
    stream_replaced_cnt_arr => stream_replaced_cnt_arr,
    -- Streaming input
    in_sosi_arr             => in_sosi_arr,
    -- Output via local MM in dp_clk domain
    mm_sosi                 => i_mm_sosi,
    mm_copi                 => mm_copi,
    mm_cipo_arr             => mm_cipo_arr,
    -- Output via streaming DP interface, when g_use_mm_output = TRUE.
    out_sosi_arr            => i_out_sosi_arr
  );
end str;
