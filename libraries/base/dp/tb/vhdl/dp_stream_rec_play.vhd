-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
-- . Combine dp_stream_recorder and dp_stream_player.
-- Description:
-- . g_pass_through=TRUE : Forward SOSI/SISO without touching them. Recording
--                         is possible, playback is not.
-- . g_rec_not_play=TRUE : Record    stream
-- . g_rec_not_play=FALSE: Play back stream
-- . See dp_stream_recorder.vhd and dp_stream_player.vhd

entity dp_stream_rec_play is
  generic (
    g_sim            : boolean;
    g_pass_through   : boolean := false;
    g_rec_not_play   : boolean := true;
    g_rec_play_file  : string := "dp_stream_recorder.rec";
    g_record_invalid : boolean := true
 );
  port (
    dp_clk  : in  std_logic;
    snk_in  : in  t_dp_sosi;
    snk_out : out t_dp_siso;
    src_out : out t_dp_sosi;
    src_in  : in  t_dp_siso
  );
end dp_stream_rec_play;

architecture str of dp_stream_rec_play is
begin
  gen_pass_through: if g_pass_through = true or g_sim = false generate
    snk_out <= src_in;
    src_out <= snk_in;
  end generate;

  gen_dp_sosi_recorder : if g_sim = true and g_rec_not_play = true generate
    u_dp_sosi_recorder : entity work.dp_sosi_recorder
    generic map (
      g_record_file    => g_rec_play_file,
      g_record_invalid => g_record_invalid
    )
    port map (
      dp_clk => dp_clk,
      snk_in => snk_in
    );
  end generate;

  gen_dp_stream_player : if g_sim = true and g_pass_through = false and g_rec_not_play = false generate
    u_dp_stream_player : entity work.dp_stream_player
    generic map (
      g_playback_file => g_rec_play_file
    )
    port map (
      dp_clk => dp_clk,
      src_in => src_in,
      src_out => src_out
    );
  end generate;
end str;
