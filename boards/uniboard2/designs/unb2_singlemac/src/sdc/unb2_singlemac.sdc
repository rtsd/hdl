create_clock -period 5.000 -name {CLK} { CLK }
create_clock -period 1.552 -name {SA_CLK} { SA_CLK }
create_clock -period 1.552 -name {SB_CLK} { SB_CLK }
#create_clock -period 40.000 -name {ETH_clk} { ETH_clk }

derive_pll_clocks

set_clock_groups -asynchronous -group [get_clocks {u_system_pll|system_pll_inst|altera_pll_i|outclk_wire[0]}]
set_clock_groups -asynchronous -group [get_clocks {u_system_pll|system_pll_inst|altera_pll_i|outclk_wire[1]}]
set_false_path -from [get_clocks {*cpulse_out_bus[0]}] -to [get_clocks {*wys|clk_divtx_user}]
set_false_path -from [get_clocks {*wys|clk_divtx_user}] -to [get_clocks {*cpulse_out_bus[0]}]
