-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Core design for Lofar2 Beamformer stage
-- Description:
--   Unb2b version for lab testing
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, diag_lib, dp_lib, tech_jesd204b_lib, lofar2_unb2b_adc_lib, wpfb_lib, lofar2_sdp_lib, tech_pll_lib, nw_10gbe_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_field_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use work.lofar2_unb2b_beamformer_pkg.all;

entity lofar2_unb2b_beamformer is
  generic (
    g_design_name            : string  := "lofar2_unb2b_beamformer";
    g_design_note            : string  := "UNUSED";
    g_buf_nof_data           : natural := 1024;
    g_sim                    : boolean := false;  -- Overridden by TB
    g_sim_unb_nr             : natural := 0;
    g_sim_node_nr            : natural := 0;
    g_stamp_date             : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time             : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id            : string  := "";  -- revision ID     -- set by QSF
    g_factory_image          : boolean := false;
    g_protect_addr_range     : boolean := false;
    g_wpfb                   : t_wpfb := c_sdp_wpfb_subbands;
    g_scope_selected_subband : natural := 0
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines
    -- front transceivers
    QSFP_1_RX     : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX     : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0);

     -- back transceivers (Note: numbered from 0)
    JESD204B_SERIAL_DATA       : in    std_logic_vector(c_sdp_S_pn - 1 downto 0);
                                                  -- Connect to the BCK_RX pins in the top wrapper
    JESD204B_REFCLK            : in    std_logic;  -- Connect to BCK_REF_CLK pin in the top level wrapper

    -- jesd204b syncronization signals
    JESD204B_SYSREF            : in    std_logic;
    JESD204B_SYNC_N            : out   std_logic_vector(c_sdp_S_pn - 1 downto 0)
  );
end lofar2_unb2b_beamformer;

architecture str of lofar2_unb2b_beamformer is
  -- Revision parameters
  constant c_revision_select        : t_lofar2_unb2b_beamformer_config := func_sel_revision_rec(g_design_name);
  constant c_nof_streams            : natural := c_revision_select.nof_streams_input;  -- Streams actually passed through for processing
  constant c_dp_clk_freq            : natural := c_revision_select.dp_clk_freq;

  -- Firmware version x.y
  constant c_fw_version             : t_unb2b_board_fw_version := (1, 1);
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_100M;
  constant c_lofar2_sample_clk_freq : natural := 200 * 10**6;  -- alternate 160MHz. TODO: Use to check PPS

  -- 10 GbE Interface
  constant c_nof_streams_qsfp              : natural := 4;
  constant c_nof_qsfp_bus                  : natural := 1;
  constant c_nof_10GbE_offload_streams     : natural := 1;
  constant c_nof_blocks_per_packet         : natural := 4;
  constant c_nof_beamlets_per_block        : natural := c_sdp_N_pol * c_sdp_S_sub_bf;
  constant c_10GbE_block_size              : natural := c_nof_blocks_per_packet * c_nof_beamlets_per_block / 4;  -- 4 beamlets fit in 1 64bit longword
  constant c_fifo_tx_fill                  : natural := c_10GbE_block_size;
  constant c_fifo_tx_size                  : natural := c_fifo_tx_fill + 11;  -- Make fifo size large enough for adding header.

  -- Address widths of a single MM instance
  constant c_addr_w_ram_ss_ss_wide : natural := ceil_log2(c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_ram_bf_weights : natural := ceil_log2(c_sdp_N_pol * c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_reg_bf_scale   : natural := 1;
  constant c_addr_w_reg_hdr_dat    : natural := ceil_log2(field_nof_words(c_sdp_cep_hdr_field_arr, c_word_w));
  constant c_addr_w_reg_dp_xonoff  : natural := 1;
  constant c_addr_w_ram_st_bst     : natural := ceil_log2(c_sdp_S_sub_bf * c_sdp_N_pol * (c_longword_sz / c_word_sz));

  -- Read only sdp_info values
  constant c_f_adc     : std_logic := '1';  -- '0' => 160M, '1' => 200M
  constant c_fsub_type : std_logic := '0';  -- '0' => critical sampled PFB, '1' => oversampled PFB
  signal gn_id         : std_logic_vector(c_sdp_W_gn_id - 1 downto 0);
  signal gn_index      : natural := 0;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic := '0';

  signal dp_pps                     : std_logic;
  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi     : t_mem_mosi;
  signal reg_fpga_temp_sens_miso     : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi  : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso  : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- Scrap ram
  signal ram_scrap_mosi             : t_mem_mosi;
  signal ram_scrap_miso             : t_mem_miso;

  ----------------------------------------------
  -- AIT
  ----------------------------------------------
  -- JESD
  signal jesd204b_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal jesd204b_miso              : t_mem_miso := c_mem_miso_rst;

  -- Shiftram (applies per-antenna delay)
  signal reg_dp_shiftram_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_shiftram_miso       : t_mem_miso := c_mem_miso_rst;

  -- bsn source
  signal reg_bsn_source_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_source_miso        : t_mem_miso := c_mem_miso_rst;

  -- bsn scheduler
  signal reg_bsn_scheduler_wg_mosi  : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_scheduler_wg_miso  : t_mem_miso := c_mem_miso_rst;

  -- WG
  signal reg_wg_mosi                : t_mem_mosi := c_mem_mosi_rst;
  signal reg_wg_miso                : t_mem_miso := c_mem_miso_rst;
  signal ram_wg_mosi                : t_mem_mosi := c_mem_mosi_rst;
  signal ram_wg_miso                : t_mem_miso := c_mem_miso_rst;

  -- BSN MONITOR
  signal reg_bsn_monitor_input_mosi : t_mem_mosi;
  signal reg_bsn_monitor_input_miso : t_mem_miso;

  -- Data buffer raw
  signal ram_diag_data_buf_jesd_mosi: t_mem_mosi;
  signal ram_diag_data_buf_jesd_miso: t_mem_miso;
  signal reg_diag_data_buf_jesd_mosi: t_mem_mosi;
  signal reg_diag_data_buf_jesd_miso: t_mem_miso;

  -- Data buffer bsn
  signal ram_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal ram_diag_data_buf_bsn_miso : t_mem_miso;
  signal reg_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal reg_diag_data_buf_bsn_miso : t_mem_miso;

  -- Aduh statistics monitor
  signal ram_aduh_monitor_mosi      : t_mem_mosi;
  signal ram_aduh_monitor_miso      : t_mem_miso;
  signal reg_aduh_monitor_mosi      : t_mem_mosi;
  signal reg_aduh_monitor_miso      : t_mem_miso;

  ----------------------------------------------
  -- FUSB
  ----------------------------------------------
  -- Subband statistics
  signal ram_st_sst_mosi            : t_mem_mosi;
  signal ram_st_sst_miso            : t_mem_miso;

  -- Spectral Inversion
  signal reg_si_mosi                : t_mem_mosi;
  signal reg_si_miso                : t_mem_miso;

  -- Filter coefficients
  signal ram_fil_coefs_mosi         : t_mem_mosi;
  signal ram_fil_coefs_miso         : t_mem_miso;

  -- Equalizer gains
  signal ram_equalizer_gains_mosi   : t_mem_mosi;
  signal ram_equalizer_gains_miso   : t_mem_miso;

  -- DP Selector
  signal reg_dp_selector_mosi       : t_mem_mosi;
  signal reg_dp_selector_miso       : t_mem_miso;

  ----------------------------------------------
  -- SDP Info
  ----------------------------------------------
  signal reg_sdp_info_mosi          : t_mem_mosi;
  signal reg_sdp_info_miso          : t_mem_miso;

  ----------------------------------------------
  -- BF
  ----------------------------------------------
  -- Beamlet Subband Select
  signal ram_ss_ss_wide_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_ss_ss_wide_miso        : t_mem_miso := c_mem_miso_rst;
  signal ram_ss_ss_wide_mosi_arr    : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal ram_ss_ss_wide_miso_arr    : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  -- Local BF bf weights
  signal ram_bf_weights_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_bf_weights_miso        : t_mem_miso := c_mem_miso_rst;
  signal ram_bf_weights_mosi_arr    : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal ram_bf_weights_miso_arr    : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  -- mms_dp_scale Scale Beamlets
  signal reg_bf_scale_mosi          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bf_scale_miso          : t_mem_miso := c_mem_miso_rst;
  signal reg_bf_scale_mosi_arr      : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal reg_bf_scale_miso_arr      : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  -- Beamlet Data Output header fields
  signal reg_hdr_dat_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_hdr_dat_miso           : t_mem_miso := c_mem_miso_rst;
  signal reg_hdr_dat_mosi_arr       : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal reg_hdr_dat_miso_arr       : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  -- Beamlet Data Output xonoff
  signal reg_dp_xonoff_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_xonoff_miso         : t_mem_miso := c_mem_miso_rst;
  signal reg_dp_xonoff_mosi_arr     : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal reg_dp_xonoff_miso_arr     : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  -- Beamlet Statistics (BST)
  signal ram_st_bst_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_bst_miso            : t_mem_miso := c_mem_miso_rst;
  signal ram_st_bst_mosi_arr        : t_mem_mosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal ram_st_bst_miso_arr        : t_mem_miso_arr(c_sdp_N_beamsets - 1 downto 0);

  ----------------------------------------------
  -- 10 GbE
  ----------------------------------------------
  signal reg_nw_10GbE_mac_mosi      : t_mem_mosi;
  signal reg_nw_10GbE_mac_miso      : t_mem_miso;

  signal reg_nw_10GbE_eth10g_mosi   : t_mem_mosi;
  signal reg_nw_10GbE_eth10g_miso   : t_mem_miso;

  ----------------------------------------------

  signal ait_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal pfb_sosi_arr               : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
  signal fsub_sosi_arr              : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);

  signal bf_udp_sosi_arr            : t_dp_sosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal bf_udp_siso_arr            : t_dp_siso_arr(c_sdp_N_beamsets - 1 downto 0);
  signal bf_10GbE_hdr_fields_out_arr: t_slv_1024_arr(c_sdp_N_beamsets - 1 downto 0);

  -- 10GbE
  signal tr_ref_clk_312                    : std_logic;
  signal tr_ref_clk_156                    : std_logic;
  signal tr_ref_rst_156                    : std_logic;

  signal i_QSFP_TX                         : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);
  signal i_QSFP_RX                         : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);

  signal unb2_board_front_io_serial_tx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');
  signal unb2_board_front_io_serial_rx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');

  signal nw_10gbe_snk_in_arr               : t_dp_sosi_arr(c_nof_10GbE_offload_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nw_10gbe_snk_out_arr              : t_dp_siso_arr(c_nof_10GbE_offload_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal nw_10gbe_src_out_arr              : t_dp_sosi_arr(c_nof_10GbE_offload_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nw_10gbe_src_in_arr               : t_dp_siso_arr(c_nof_10GbE_offload_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal nw_10GbE_hdr_fields_in_arr        : t_slv_1024_arr(c_nof_10GbE_offload_streams - 1 downto 0);

  signal this_bck_id                       : std_logic_vector(c_unb2b_board_nof_uniboard_w - 1 downto 0);
  signal this_chip_id                      : std_logic_vector(c_unb2b_board_nof_chip_w - 1 downto 0);

  signal cep_eth_src_mac                   : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal cep_ip_src_addr                   : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal cep_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
  signal stat_eth_src_mac                  : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal stat_ip_src_addr                  : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal sst_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
  signal bst_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);

  signal sdp_info                          : t_sdp_info := c_sdp_info_rst;

  -- QSFP LEDS
  signal qsfp_green_led_arr                : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr                  : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);

  signal unb2_board_qsfp_leds_snk_in_arr   : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);
  signal unb2_board_qsfp_leds_snk_out_arr  : t_dp_siso_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_siso_rst);
  signal unb2_board_qsfp_leds_src_out_arr  : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                => g_sim,
    g_design_name        => g_design_name,
    g_design_note        => g_design_note,
    g_stamp_date         => g_stamp_date,
    g_stamp_time         => g_stamp_time,
    g_revision_id        => g_revision_id,
    g_fw_version         => c_fw_version,
    g_mm_clk_freq        => c_mm_clk_freq,
    g_eth_clk_freq       => c_unb2b_board_eth_clk_freq_125M,
    g_aux                => c_unb2b_board_aux,
    g_factory_image      => g_factory_image,
    g_protect_addr_range => g_protect_addr_range,
    g_dp_clk_freq        => c_dp_clk_freq,
    g_dp_clk_use_pll     => false
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,  -- Can be external 200MHz, or PLL generated
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    this_chip_id             => this_chip_id,
    this_bck_id              => this_bck_id,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_lofar2_unb2b_beamformer
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- mm interfaces for control
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,
    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- mm buses for signal flow blocks
    -- Jesd ip status/control
    jesd204b_mosi               => jesd204b_mosi,
    jesd204b_miso               => jesd204b_miso,
    reg_dp_shiftram_mosi        => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso        => reg_dp_shiftram_miso,
    reg_bsn_source_mosi         => reg_bsn_source_mosi,
    reg_bsn_source_miso         => reg_bsn_source_miso,
    reg_bsn_scheduler_mosi      => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_miso      => reg_bsn_scheduler_wg_miso,
    reg_wg_mosi                 => reg_wg_mosi,
    reg_wg_miso                 => reg_wg_miso,
    ram_wg_mosi                 => ram_wg_mosi,
    ram_wg_miso                 => ram_wg_miso,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
    ram_diag_data_buf_jesd_mosi => ram_diag_data_buf_jesd_mosi,
    ram_diag_data_buf_jesd_miso => ram_diag_data_buf_jesd_miso,
    reg_diag_data_buf_jesd_mosi => reg_diag_data_buf_jesd_mosi,
    reg_diag_data_buf_jesd_miso => reg_diag_data_buf_jesd_miso,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,
    ram_aduh_monitor_mosi       => ram_aduh_monitor_mosi,
    ram_aduh_monitor_miso       => ram_aduh_monitor_miso,
    reg_aduh_monitor_mosi       => reg_aduh_monitor_mosi,
    reg_aduh_monitor_miso       => reg_aduh_monitor_miso,
    ram_st_sst_mosi             => ram_st_sst_mosi,
    ram_st_sst_miso             => ram_st_sst_miso,
    ram_fil_coefs_mosi          => ram_fil_coefs_mosi,
    ram_fil_coefs_miso          => ram_fil_coefs_miso,
    reg_si_mosi                 => reg_si_mosi,
    reg_si_miso                 => reg_si_miso,
    ram_equalizer_gains_mosi    => ram_equalizer_gains_mosi,
    ram_equalizer_gains_miso    => ram_equalizer_gains_miso,
    reg_dp_selector_mosi        => reg_dp_selector_mosi,
    reg_dp_selector_miso        => reg_dp_selector_miso,
    reg_sdp_info_mosi           => reg_sdp_info_mosi,
    reg_sdp_info_miso           => reg_sdp_info_miso,
    ram_ss_ss_wide_mosi         => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso         => ram_ss_ss_wide_miso,
    ram_bf_weights_mosi         => ram_bf_weights_mosi,
    ram_bf_weights_miso         => ram_bf_weights_miso,
    reg_bf_scale_mosi           => reg_bf_scale_mosi,
    reg_bf_scale_miso           => reg_bf_scale_miso,
    reg_hdr_dat_mosi            => reg_hdr_dat_mosi,
    reg_hdr_dat_miso            => reg_hdr_dat_miso,
    reg_dp_xonoff_mosi          => reg_dp_xonoff_mosi,
    reg_dp_xonoff_miso          => reg_dp_xonoff_miso,
    ram_st_bst_mosi             => ram_st_bst_mosi,
    ram_st_bst_miso             => ram_st_bst_miso,
    reg_nw_10GbE_mac_mosi       => reg_nw_10GbE_mac_mosi,
    reg_nw_10GbE_mac_miso       => reg_nw_10GbE_mac_miso,
    reg_nw_10GbE_eth10g_mosi    => reg_nw_10GbE_eth10g_mosi,
    reg_nw_10GbE_eth10g_miso    => reg_nw_10GbE_eth10g_miso,
    ram_scrap_mosi              => ram_scrap_mosi,
    ram_scrap_miso              => ram_scrap_miso
  );

  -----------------------------------------------------------------------------
  -- SDP Info register
  -----------------------------------------------------------------------------
  u_sdp_info : entity lofar2_sdp_lib.sdp_info
  port map(
    -- Clocks and reset
    mm_rst    => mm_rst,  -- reset synchronous with mm_clk
    mm_clk    => mm_clk,  -- memory-mapped bus clock

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    reg_mosi  => reg_sdp_info_mosi,
    reg_miso  => reg_sdp_info_miso,

    -- inputs from other blocks
    gn_index  => gn_index,
    f_adc     => c_f_adc,
    fsub_type => c_fsub_type,

    -- sdp info
    sdp_info => sdp_info
  );

  -----------------------------------------------------------------------------
  -- nof beamsets node_sdp_beamformers (BF)
  -----------------------------------------------------------------------------
  gn_id    <= ID(c_sdp_W_gn_id - 1 downto 0);
  gn_index <= TO_UINT(gn_id);
  -- derive MAC, IP and UDP Port
  cep_eth_src_mac  <= c_sdp_cep_eth_src_mac_47_16 & RESIZE_UVEC(this_bck_id, c_byte_w) & RESIZE_UVEC(this_chip_id, c_byte_w);  -- Simply use chip_id since we only use 1 of the 6*4 = 24 10GbE port.
  cep_ip_src_addr  <= c_sdp_cep_ip_src_addr_31_16 & RESIZE_UVEC(this_bck_id, c_byte_w) & INCR_UVEC(RESIZE_UVEC(this_chip_id, c_byte_w), 1);  -- +1 to avoid IP = *.*.*.0
  cep_udp_src_port <= c_sdp_cep_udp_src_port_15_8 & ID;
  stat_eth_src_mac <= c_sdp_stat_eth_src_mac_47_16 & RESIZE_UVEC(this_bck_id, c_byte_w) & RESIZE_UVEC(this_chip_id, c_byte_w);  -- Simply use chip_id since we only use 1 of the 6*4 = 24 10GbE port.
  stat_ip_src_addr <= c_sdp_stat_ip_src_addr_31_16 & RESIZE_UVEC(this_bck_id, c_byte_w) & INCR_UVEC(RESIZE_UVEC(this_chip_id, c_byte_w), 1);  -- +1 to avoid IP = *.*.*.0
  sst_udp_src_port <= c_sdp_sst_udp_src_port_15_8 & ID;
  bst_udp_src_port <= c_sdp_bst_udp_src_port_15_8 & ID;

  -----------------------------------------------------------------------------
  -- node_adc_input_and_timing (AIT)
  --   .Contains JESD receiver, bsn source and associated data buffers, diagnostics and statistics
  -----------------------------------------------------------------------------
  u_ait: entity lofar2_unb2b_adc_lib.node_adc_input_and_timing
  generic map(
    g_nof_streams               => c_sdp_S_pn,
    g_buf_nof_data              => c_sdp_V_si_db,
    g_sim                       => g_sim
  )
  port map(
    -- clocks and resets
    mm_clk                      => mm_clk,
    mm_rst                      => mm_rst,
    dp_clk                      => dp_clk,
    dp_rst                      => dp_rst,

    -- mm control buses
    jesd_ctrl_mosi              => c_mem_mosi_rst,
    jesd204b_mosi               => jesd204b_mosi,
    jesd204b_miso               => jesd204b_miso,
    reg_dp_shiftram_mosi        => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso        => reg_dp_shiftram_miso,
    reg_bsn_source_mosi         => reg_bsn_source_mosi,
    reg_bsn_source_miso         => reg_bsn_source_miso,
    reg_bsn_scheduler_wg_mosi   => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_wg_miso   => reg_bsn_scheduler_wg_miso,
    reg_wg_mosi                 => reg_wg_mosi,
    reg_wg_miso                 => reg_wg_miso,
    ram_wg_mosi                 => ram_wg_mosi,
    ram_wg_miso                 => ram_wg_miso,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
    ram_diag_data_buf_jesd_mosi => ram_diag_data_buf_jesd_mosi,
    ram_diag_data_buf_jesd_miso => ram_diag_data_buf_jesd_miso,
    reg_diag_data_buf_jesd_mosi => reg_diag_data_buf_jesd_mosi,
    reg_diag_data_buf_jesd_miso => reg_diag_data_buf_jesd_miso,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,
    ram_aduh_monitor_mosi       => ram_aduh_monitor_mosi,
    ram_aduh_monitor_miso       => ram_aduh_monitor_miso,
    reg_aduh_monitor_mosi       => reg_aduh_monitor_mosi,
    reg_aduh_monitor_miso       => reg_aduh_monitor_miso,

     -- Jesd external IOs
    jesd204b_serial_data       => JESD204B_SERIAL_DATA,
    jesd204b_refclk            => JESD204B_REFCLK,
    jesd204b_sysref            => JESD204B_SYSREF,
    jesd204b_sync_n            => JESD204B_SYNC_N,

    -- Streaming data output
    out_sosi_arr               => ait_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- node_sdp_filterbank (FSUB)
  -----------------------------------------------------------------------------
  u_fsub : entity lofar2_sdp_lib.node_sdp_filterbank
  generic map(
    g_sim                    => g_sim,
    g_wpfb                   => g_wpfb,
    g_scope_selected_subband => g_scope_selected_subband
  )
  port map(
    dp_clk             => dp_clk,
    dp_rst             => dp_rst,

    in_sosi_arr        => ait_sosi_arr,
    pfb_sosi_arr       => pfb_sosi_arr,
    fsub_sosi_arr      => fsub_sosi_arr,

    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    reg_si_mosi        => reg_si_mosi,
    reg_si_miso        => reg_si_miso,
    ram_st_sst_mosi    => ram_st_sst_mosi,
    ram_st_sst_miso    => ram_st_sst_miso,
    ram_fil_coefs_mosi => ram_fil_coefs_mosi,
    ram_fil_coefs_miso => ram_fil_coefs_miso,
    ram_gains_mosi     => ram_equalizer_gains_mosi,
    ram_gains_miso     => ram_equalizer_gains_miso,
    reg_selector_mosi  => reg_dp_selector_mosi,
    reg_selector_miso  => reg_dp_selector_miso,

    sdp_info           => sdp_info,
    gn_id              => gn_id,
    eth_src_mac        => stat_eth_src_mac,
    ip_src_addr        => stat_ip_src_addr,
    udp_src_port       => sst_udp_src_port
  );

  -- Beamformers
  gen_bf : for beamset_id in 0 to c_sdp_N_beamsets - 1 generate
    u_bf : entity lofar2_sdp_lib.node_sdp_beamformer
    generic map(
      g_sim                    => g_sim,
      g_beamset_id             => beamset_id,
      g_scope_selected_beamlet => g_scope_selected_subband
    )
    port map(
      dp_clk       => dp_clk,
      dp_rst       => dp_rst,

      in_sosi_arr  => fsub_sosi_arr,
      bf_udp_sosi  => bf_udp_sosi_arr(beamset_id),
      bf_udp_siso  => bf_udp_siso_arr(beamset_id),
      bst_udp_sosi => OPEN,

      mm_rst       => mm_rst,
      mm_clk       => mm_clk,

      ram_ss_ss_wide_mosi => ram_ss_ss_wide_mosi_arr(beamset_id),
      ram_ss_ss_wide_miso => ram_ss_ss_wide_miso_arr(beamset_id),
      ram_bf_weights_mosi => ram_bf_weights_mosi_arr(beamset_id),
      ram_bf_weights_miso => ram_bf_weights_miso_arr(beamset_id),
      reg_bf_scale_mosi   => reg_bf_scale_mosi_arr(beamset_id),
      reg_bf_scale_miso   => reg_bf_scale_miso_arr(beamset_id),
      reg_hdr_dat_mosi    => reg_hdr_dat_mosi_arr(beamset_id),
      reg_hdr_dat_miso    => reg_hdr_dat_miso_arr(beamset_id),
      reg_dp_xonoff_mosi  => reg_dp_xonoff_mosi_arr(beamset_id),
      reg_dp_xonoff_miso  => reg_dp_xonoff_miso_arr(beamset_id),
      ram_st_sst_mosi     => ram_st_bst_mosi_arr(beamset_id),
      ram_st_sst_miso     => ram_st_bst_miso_arr(beamset_id),

      sdp_info => sdp_info,
      gn_id    => gn_id,

      eth_src_mac  => cep_eth_src_mac,
      ip_src_addr  => cep_ip_src_addr,
      udp_src_port => cep_udp_src_port,

      hdr_fields_out => bf_10GbE_hdr_fields_out_arr(beamset_id)
    );
  end generate;

  -- MM multiplexing
  u_mem_mux_ram_ss_ss_wide : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_ram_ss_ss_wide
  )
  port map (
    mosi     => ram_ss_ss_wide_mosi,
    miso     => ram_ss_ss_wide_miso,
    mosi_arr => ram_ss_ss_wide_mosi_arr,
    miso_arr => ram_ss_ss_wide_miso_arr
  );

  u_mem_mux_ram_bf_weights : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_ram_bf_weights
  )
  port map (
    mosi     => ram_bf_weights_mosi,
    miso     => ram_bf_weights_miso,
    mosi_arr => ram_bf_weights_mosi_arr,
    miso_arr => ram_bf_weights_miso_arr
  );

  u_mem_mux_reg_bf_scale : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_reg_bf_scale
  )
  port map (
    mosi     => reg_bf_scale_mosi,
    miso     => reg_bf_scale_miso,
    mosi_arr => reg_bf_scale_mosi_arr,
    miso_arr => reg_bf_scale_miso_arr
  );

  u_mem_mux_reg_hdr_dat : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_reg_hdr_dat
  )
  port map (
    mosi     => reg_hdr_dat_mosi,
    miso     => reg_hdr_dat_miso,
    mosi_arr => reg_hdr_dat_mosi_arr,
    miso_arr => reg_hdr_dat_miso_arr
  );

  u_mem_mux_reg_dp_xonoff : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_reg_dp_xonoff
  )
  port map (
    mosi     => reg_dp_xonoff_mosi,
    miso     => reg_dp_xonoff_miso,
    mosi_arr => reg_dp_xonoff_mosi_arr,
    miso_arr => reg_dp_xonoff_miso_arr
  );

  u_mem_mux_ram_st_bst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_N_beamsets,
    g_mult_addr_w => c_addr_w_ram_st_bst
  )
  port map (
    mosi     => ram_st_bst_mosi,
    miso     => ram_st_bst_miso,
    mosi_arr => ram_st_bst_mosi_arr,
    miso_arr => ram_st_bst_miso_arr
  );

  -----------------------------------------------------------------------------
  -- DP MUX
  -----------------------------------------------------------------------------
  -- Assign hdr_fields to nw_10GbE for ARP/PING functionality. Only the fields:
  -- eth_src_mac, ip_src_addr and ip_dst_addr are used. Which are identical for
  -- both beamsets.
  nw_10GbE_hdr_fields_in_arr(0) <= bf_10GbE_hdr_fields_out_arr(0);

  u_dp_mux : entity dp_lib.dp_mux
  generic map (
    g_nof_input => c_sdp_N_beamsets,
    g_sel_ctrl_invert => true,
    g_fifo_size => array_init(0, c_sdp_N_beamsets),  -- no FIFO used but must match g_nof_input
    g_fifo_fill => array_init(0, c_sdp_N_beamsets)  -- no FIFO used but must match g_nof_input
  )
  port map (
    clk => dp_clk,
    rst => dp_rst,

    snk_in_arr  => bf_udp_sosi_arr,
    snk_out_arr => bf_udp_siso_arr,

    src_out => nw_10gbe_snk_in_arr(0),
    src_in  => nw_10gbe_snk_out_arr(0)
  );

  -----------------------------------------------------------------------------
  -- Interface : 10GbE
  -----------------------------------------------------------------------------

    -- put the QSFP_TX/RX ports into arrays
    i_QSFP_RX(0) <= QSFP_1_RX;
    QSFP_1_TX <= i_QSFP_TX(0);
    ------------
    -- Front IO
    ------------
    u_front_io : entity unb2b_board_lib.unb2b_board_front_io
    generic map (
      g_nof_qsfp_bus => c_nof_qsfp_bus
    )
    port map (
      serial_tx_arr => unb2_board_front_io_serial_tx_arr,
      serial_rx_arr => unb2_board_front_io_serial_rx_arr,

      green_led_arr => qsfp_green_led_arr(c_nof_qsfp_bus - 1 downto 0),
      red_led_arr   => qsfp_red_led_arr(c_nof_qsfp_bus - 1 downto 0),

      QSFP_RX       => i_QSFP_RX,
      QSFP_TX       => i_QSFP_TX,

      QSFP_LED      => QSFP_LED
    );

    ---------
    -- PLL
    ---------
    u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    generic map (
    )
    port map (
      refclk_644 => SA_CLK,
      rst_in     => mm_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );

    ---------------
    -- nw_10GbE
    ---------------
    u_nw_10GbE: entity nw_10GbE_lib.nw_10GbE
    generic map (
      g_sim           => g_sim,
      g_sim_level     => 1,
      g_nof_macs      => c_nof_10GbE_offload_streams,
      g_direction     => "TX_RX",
      g_tx_fifo_fill  => c_fifo_tx_fill,
      g_tx_fifo_size  => c_fifo_tx_size,
      g_ip_hdr_field_arr => c_sdp_cep_hdr_field_arr

    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644        => SA_CLK,
      tr_ref_clk_312        => tr_ref_clk_312,
      tr_ref_clk_156        => tr_ref_clk_156,
      tr_ref_rst_156        => tr_ref_rst_156,

      -- MM interface
      mm_rst                => mm_rst,
      mm_clk                => mm_clk,

      reg_mac_mosi          => reg_nw_10GbE_mac_mosi,
      reg_mac_miso          => reg_nw_10GbE_mac_miso,

      reg_eth10g_mosi       => reg_nw_10GbE_eth10g_mosi,
      reg_eth10g_miso       => reg_nw_10GbE_eth10g_miso,

      -- DP interface
      dp_rst                => dp_rst,
      dp_clk                => dp_clk,
      dp_pps                => dp_pps,

      src_out_arr           => nw_10gbe_src_out_arr,
      src_in_arr            => nw_10gbe_src_in_arr,

      snk_out_arr           => nw_10gbe_snk_out_arr,
      snk_in_arr            => nw_10gbe_snk_in_arr,

      -- Serial IO
      serial_tx_arr         => unb2_board_front_io_serial_tx_arr(c_nof_10GbE_offload_streams - 1 downto 0),
      serial_rx_arr         => unb2_board_front_io_serial_rx_arr(c_nof_10GbE_offload_streams - 1 downto 0),

      hdr_fields_in_arr     => nw_10GbE_hdr_fields_in_arr
    );
end str;
