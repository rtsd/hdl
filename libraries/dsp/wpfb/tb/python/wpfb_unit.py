#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
"""
On execution, this script will start ModelSim and within ModelSim 
the LIBRARY_NAME will be compiled and the TB_NAME will be runned. 

After INIT_DELAY_NS nanoseconds (nanoseconds from the simulation environment) 
the COMMANDS wil be runned against the simulation. COMMANDS can contain a 
single testcase or multiple testcases that are runned sequentially. 

  - Example COMMANDS, single testcase: 
    COMMANDS     = ['tc_mmf_ss_reorder.py' + TARGET_NODES]
    
  - Example COMMANDS, multiple testcases
    COMMANDS     = ['$AAF/Software/python/apps/io_rsp_terminal/tc_io_rsp_terminal.py' +TARGET_NODES+ '-r 0:11',
                '    $AAF/Software/python/apps/aartfaac_bn_filterbank/tc_aartfaac_bn_filterbank_bsn_align.py' +TARGET_NODES+ '-r 0']
    
If --hold is not passed, the running ModelSim simulation window will be killed after completion.

VHDL GENERICS
In case constants are to be parsed as VHDL generics to ModelSim the first test case file 
in COMMANDS should have them listed. The constants must be placed between pragma's as 
follows: 

# START_VHDL_GENERICS
c_nof_inputs       = 8              
c_nof_outputs      = 16             # The number of outputs 
c_pipeline_in      = 1              # pipeline in_data                     
c_pipeline_out     = 1              # pipeline out_data                    
# END_VHDL_GENERICS

The auto_sim scripts will change the 'c_' to 'g_'. 

"""
import sys, os
sys.path.append(os.environ['UNB']+'/Firmware/sim/python')
from auto_sim import *
import tc_mmf_wpfb_unit


LIBRARY_NAME = 'wpfb'
TB_NAME      = 'tb_mmf_wpfb_unit'
TARGET_NODES = ' --unb 0 --bn 0 '
INIT_DELAY_NS = 500                 # Define an inital delay between start of the simulation and the start of the COMMANDS scripts

# Define generics for a narrowband variation
GENERICS     = {'g_wb_factor'      : 4,
                'g_nof_wb_streams' : 1,
                'g_nof_chan'       : 0,
                'g_nof_points'     : 1024,
                'g_nof_taps'       : 16,
                'g_in_dat_w'       : 8,
                'g_out_dat_w'      : 16,
                'g_use_separate'   : True,
                'g_nof_blocks'     : 4}
                  
COMMANDS     = ['tc_mmf_wpfb_unit.py' + TARGET_NODES + ' --gen ' +  '"' + str(GENERICS) + '"' ]  

# Generate .hex files before ModelSim starts
tc_mmf_wpfb_unit.gen_filter_hex_files(GENERICS['g_nof_taps'], GENERICS['g_wb_factor'], GENERICS['g_nof_points'])
    
# Run the sim and return its result using sys.exit([return_value])
sys.exit(auto_sim(os.environ['UNB'], LIBRARY_NAME, TB_NAME, COMMANDS, INIT_DELAY_NS, GENERICS))
