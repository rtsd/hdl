#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the settings-algorhythm. 


   > python tc_ss_parallel_algo.py --unb 0 --bn 0 --sim 

"""

###############################################################################
# System imports
import test_case
import node_io
import unb_apertif as apr
import pi_ss_parallel
import random
from tools import *
from common import *

###############################################################################

# Create a test case object
tc = test_case.Testcase('TB - ', '')
                
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

I = 2 #8   #      # nof inputs streams
J = 12 #256 #     # input frame size
N = 4 #16  #      # nof output streams    
M = 6 #96  #      # outpur frame size

# Size of the RAM depends on ratio between number of input streams(=I) and number of output streams(=N)
if N<I:
    K=I
else:
    K=N

ss_par = pi_ss_parallel.PiSsParallel(tc, io, I, N, J, M, tc.nodeBnNrs)

# Din is the input Matrix. 
Din = []    
for i in range(I):    
    row = []
    for j in range(J):
        row.append([i, j])
    Din.append(row)

## Matrix definition for Apertif. Selecting the lower 384 subbands.
#Aout = []  
#for f in range(2):          # nof FFT's
#    for g in range(2):      # nof signal paths per row
#        for h in range(3):
#            for i in range(8):    
#                row = []
#                for j in range(N):   
#                    row.append([h+4*f, j*2 + i*2*N + g])
#                Aout.append(row)
#Dout = transpose(Aout)

row_list = range(0, I, 1)
col_list = range(0, J, 1)
succes = 0

# Creating a loop that goes through every possible output configuration (Dout)
for row_permutation in itertools.product(row_list, repeat=len(row_list)):
    for col_permutation in itertools.product(col_list, repeat=len(col_list)):
        
        # Dout is composed out of the permutation lists. 
        Dout = []
        x = 0
        y = 0
        for i in range(N):    
            row = []
            for j in range(M):
                row.append([row_permutation[x], col_permutation[y]]) 
                y += 1
                y = y % J 
            x += 1
            x = x % I 
            Dout.append(row)  
        
        # Create and verify the settings
        [result, Rin, Dram, Dsel, Rout, Errout] = ss_par.create_settings(Din, Dout)

        # Plot the matrices wehn things went wrong
        if result != True:
            print "Errors!!!!"
            print "Din:"
            for i in Din:
                print i
            print ""
            
            print "Rin:"
            for i in Rin:
                print i
            print ""
            
            print "Dram:"
            for i in Dram:
                print i
            print ""
            
            print "Dsel:"
            for i in Dsel:
                print i
            print ""
            
            print "Rout:"
            for i in Rout:
                print i
            print ""
            
            print "Dout:"
            for i in Dout:
                print i
            print ""
            
            print "Refout:"
            for i in Refout:
                print i
            print ""
            
            print "Errout:"
            for i in Errout:
                print i
            print ""
            break  
        else:
            succes += 1
    if result != True:
        break

print "Number of succesful checks: " + str(succes)        

##############################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(3, '>>>')
