-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose: Per symbol pipeline of the input data stream
-- Description:
--   The in_data is a concatenation of g_nof_symbols, that are each g_symbol_w
--   bits wide. The g_nof_symbols in the in_data slv can be pipelined
--   individualy as set by g_pipeline_arr(g_nof_symbols-1:0). The output
--   control signals val, sop and eop are also pipelined per symbol.
-- Remarks:

entity common_pipeline_symbol is
  generic (
    g_pipeline_arr : t_natural_arr;  -- range g_nof_symbols-1 DOWNTO 0
    g_nof_symbols  : natural := 4;
    g_symbol_w     : natural := 16
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_data    : in  std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    in_val     : in  std_logic := '0';
    in_sop     : in  std_logic := '0';
    in_eop     : in  std_logic := '0';

    out_data    : out std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    out_val_arr : out std_logic_vector(g_nof_symbols - 1 downto 0);  -- pipelined in_val
    out_sop_arr : out std_logic_vector(g_nof_symbols - 1 downto 0);  -- pipelined in_sop
    out_eop_arr : out std_logic_vector(g_nof_symbols - 1 downto 0)  -- pipelined in_eop
  );
end common_pipeline_symbol;

architecture str of common_pipeline_symbol is
  type t_symbol_arr  is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);

  signal in_dat_arr   : t_symbol_arr(g_nof_symbols - 1 downto 0);
  signal out_dat_arr  : t_symbol_arr(g_nof_symbols - 1 downto 0);
begin
  gen_symbols : for I in g_nof_symbols - 1 downto 0 generate
    -- map input vector to arr
    in_dat_arr(I) <= in_data((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);

    -- pipeline per symbol
    u_pipe_symbol : entity work.common_pipeline
    generic map (
      g_pipeline  => g_pipeline_arr(I),
      g_in_dat_w  => g_symbol_w,
      g_out_dat_w => g_symbol_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => in_dat_arr(I),
      out_dat => out_dat_arr(I)
    );

    u_pipe_val : entity work.common_pipeline_sl
    generic map (
      g_pipeline => g_pipeline_arr(I)
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => in_val,
      out_dat => out_val_arr(I)
    );

    u_pipe_sop : entity work.common_pipeline_sl
    generic map (
      g_pipeline => g_pipeline_arr(I)
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => in_sop,
      out_dat => out_sop_arr(I)
    );

    u_pipe_eop : entity work.common_pipeline_sl
    generic map (
      g_pipeline => g_pipeline_arr(I)
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => in_eop,
      out_dat => out_eop_arr(I)
    );

    -- map arr to output vector
    out_data((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) <= out_dat_arr(I);
  end generate;
end str;
