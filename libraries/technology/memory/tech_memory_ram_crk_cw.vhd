-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Remark:
--   Due to the lack of support by the Agilex 7 (agi027_1e1v) for the crwk_crw
--   IP as used for previous FPGA technology identifiers (device types), the
--   crk_cw IP has been created and is available for use. For the previous
--   technology identifiers, it is constructed using the crwk_crw IPs. For more
--   details please refer the README.txt in the ip_agi027_1e1v/ram/ folder.
-- Reference:
--   Based on tech_memory_ram_crwk_crw.vhd and tech_memory_ram_cr_cw.vhd.
--   Changed the generics and ports to common_ram_cr_cw_ratio.vhd and
--   ip_agi027_ram_crk_cw.vhd. These changes have been incorporated into the
--   existing generate-blocks and the generate-block for agi_1e1v is added.

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_memory_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_ram_lib;
library ip_arria10_ram_lib;
library ip_arria10_e3sge3_ram_lib;
library ip_arria10_e1sg_ram_lib;
library ip_arria10_e2sg_ram_lib;
library ip_agi027_1e1v_ram_lib;

entity tech_memory_ram_crk_cw is  -- support different port data widths and corresponding address ranges
  generic (
    g_technology   : natural := c_tech_select_default;
    g_wr_adr_w     : natural := 5;
    g_wr_dat_w     : natural := 32;
    g_rd_adr_w     : natural := 7;
    g_rd_dat_w     : natural := 8;
    g_wr_nof_words : natural := 2**5;
    g_rd_nof_words : natural := 2**7;
    g_rd_latency   : natural := 2;  -- choose 1 or 2
    g_init_file    : string  := "UNUSED"
  );
  port
  (
    wraddress : in std_logic_vector(g_wr_adr_w - 1 downto 0);
    wrclock   : in std_logic  := '1';
    rdaddress : in std_logic_vector(g_rd_adr_w - 1 downto 0);
    rdclock   : in std_logic;
    data      : in std_logic_vector(g_wr_dat_w - 1 downto 0);
    wrclocken : in std_logic  := '1';
    rdclocken : in std_logic  := '1';
    rden_a    : in std_logic  := '1';
    rden_b    : in std_logic  := '1';
    wren      : in std_logic  := '0';
    q         : out std_logic_vector(g_rd_dat_w - 1 downto 0)
  );
end tech_memory_ram_crk_cw;

architecture str of tech_memory_ram_crk_cw is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_ram_crwk_crw
    generic map (g_wr_adr_w, g_wr_dat_w, g_rd_adr_w, g_rd_dat_w, g_wr_nof_words, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (wraddress, rdaddress, wrclock, rdclock, data, (others => '0'), wrclocken, rdclocken, rden_a, rden_b, wren, '0', OPEN, q);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_ram_crwk_crw
    generic map (g_wr_adr_w, g_wr_dat_w, g_rd_adr_w, g_rd_dat_w, g_wr_nof_words, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (wraddress, rdaddress, wrclock, rdclock, data, (others => '0'), wren, '0', OPEN, q);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_ram_crwk_crw
    generic map (g_wr_adr_w, g_wr_dat_w, g_rd_adr_w, g_rd_dat_w, g_wr_nof_words, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (wraddress, rdaddress, wrclock, rdclock, data, (others => '0'), wren, '0', OPEN, q);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_ram_crwk_crw
    generic map (g_wr_adr_w, g_wr_dat_w, g_rd_adr_w, g_rd_dat_w, g_wr_nof_words, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (wraddress, rdaddress, wrclock, rdclock, data, (others => '0'), wren, '0', OPEN, q);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_ram_crwk_crw
    generic map (g_wr_adr_w, g_wr_dat_w, g_rd_adr_w, g_rd_dat_w, g_wr_nof_words, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (wraddress, rdaddress, wrclock, rdclock, data, (others => '0'), wren, '0', OPEN, q);
  end generate;

  gen_ip_agi027_1e1v : if g_technology = c_tech_agi027_1e1v generate
    u0 : ip_agi027_1e1v_ram_crk_cw
    generic map (g_wr_adr_w, g_wr_dat_w, g_wr_nof_words, g_rd_adr_w, g_rd_dat_w, g_rd_nof_words, g_rd_latency, g_init_file)
    port map (data, wraddress, wrclock, wren, rdaddress, rdclock, q);
  end generate;
end architecture;
