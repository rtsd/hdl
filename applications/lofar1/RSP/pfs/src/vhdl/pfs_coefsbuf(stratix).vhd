library IEEE, altera_mf, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

architecture stratix of pfs_coefsbuf is
  component altsyncram
  generic (
    operation_mode         : string;
    width_a                : natural;
    widthad_a              : natural;
    numwords_a             : natural;
    lpm_type               : string;
    width_byteena_a        : natural;
    outdata_reg_a          : string;
    outdata_aclr_a         : string;
    read_during_write_mode_mixed_ports : string;
    ram_block_type         : string;
    init_file              : string;
    intended_device_family : string
  );
  port (
    aclr0                  : in  std_logic;
    clock0                 : in  std_logic;
    address_a              : in  std_logic_vector(g_addr_w - 1 downto 0);
    q_a                    : out std_logic_vector(g_data_w - 1 downto 0)
  );
  end component;
begin
  rom : altsyncram
  generic map (
    operation_mode         => "ROM",
    width_a                => g_data_w,
    widthad_a              => g_addr_w,
    numwords_a             => g_nof_coefs,
    lpm_type               => "altsyncram",
    width_byteena_a        => 1,
    outdata_reg_a          => "CLOCK0",
    outdata_aclr_a         => "CLEAR0",
    read_during_write_mode_mixed_ports => "DONT_CARE",
    ram_block_type         => "AUTO",
    init_file              => "../../../../pfs/src/data/pfs_coefsbuf_1024.hex",
    intended_device_family => c_rsp_device_family
  )
  port map (
    aclr0                  => rst,
    clock0                 => clk,
    address_a              => addr,
    q_a                    => data
  );
end stratix;
