--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   The dp_fifo_fill connected as a Memory Mapped Slave.
--
--
-- Description:
-- Remarks:

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_dp_fifo_fill is
  generic (
    g_technology         : natural := c_tech_select_default;
    g_nof_streams        : natural := 3;
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_data_w             : natural := 16;
    g_bsn_w              : natural := 1;
    g_empty_w            : natural := 1;
    g_channel_w          : natural := 1;
    g_error_w            : natural := 1;
    g_use_bsn            : boolean := false;
    g_use_empty          : boolean := false;
    g_use_channel        : boolean := false;
    g_use_error          : boolean := false;
    g_use_sync           : boolean := false;
    g_use_complex        : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_fill          : natural := 0;
    g_fifo_size          : natural := 256;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin     : natural := 4;  -- Nof words below max (full) at which fifo is considered almost full
    g_fifo_rl            : natural := 1  -- use RL=0 for internal show ahead FIFO, default use RL=1 for internal normal FIFO
  );
  port (
    -- Memory-mapped clock domain
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;
    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    -- Streaming clock domain
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;

    -- Monitor FIFO filling
--    wr_ful      : OUT STD_LOGIC;
--    usedw       : OUT STD_LOGIC_VECTOR(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2))-1 DOWNTO 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
--    rd_emp      : OUT STD_LOGIC;

    -- ST sink
    snk_out_arr : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);
    src_out_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_fifo_fill;

architecture str of mms_dp_fifo_fill is
  constant c_usedw_w          : natural := ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2));

  signal fifo_usedw_reg : std_logic_vector(g_nof_streams * c_word_w - 1 downto 0) := (others => '0');
  signal wr_ful_reg     : std_logic_vector(g_nof_streams - 1          downto 0);
  signal rd_emp_reg     : std_logic_vector(g_nof_streams - 1          downto 0);
begin
  gen_fifos : for I in 0 to g_nof_streams - 1 generate
    dp_fifo_sc : entity work.dp_fifo_fill
    generic map (
      g_technology     => g_technology,
      g_data_w         => g_data_w,
      g_bsn_w          => g_bsn_w,
      g_empty_w        => g_empty_w,
      g_channel_w      => g_channel_w,
      g_error_w        => g_error_w,
      g_use_bsn        => g_use_bsn,
      g_use_empty      => g_use_empty,
      g_use_channel    => g_use_channel,
      g_use_error      => g_use_error,
      g_use_sync       => g_use_sync,
      g_use_complex    => g_use_complex,
      g_fifo_fill      => g_fifo_fill,
      g_fifo_size      => g_fifo_size,
      g_fifo_af_margin => g_fifo_af_margin,
      g_fifo_rl        => g_fifo_rl
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      -- Monitor FIFO filling
      wr_ful      => wr_ful_reg(I),
      usedw       => fifo_usedw_reg(I * c_word_w + c_usedw_w - 1 downto I * c_word_w),
      rd_emp      => rd_emp_reg(I),
      -- ST sink
      snk_out     => snk_out_arr(I),
      snk_in      => snk_in_arr(I),
      -- ST source
      src_in      => src_in_arr(I),
      src_out     => src_out_arr(I)
    );
  end generate;

  u_reg : entity work.dp_fifo_fill_reg
  generic map(
    g_nof_streams        => g_nof_streams,
    g_cross_clock_domain => g_cross_clock_domain
  )
  port map(
    -- Clocks and reset
    mm_rst  => mm_rst,
    mm_clk  => mm_clk,
    st_rst  => dp_rst,
    st_clk  => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in  => reg_mosi,
    sla_out => reg_miso,

    -- MM registers in st_clk domain
    used_w  => fifo_usedw_reg,
    rd_emp  => rd_emp_reg,
    wr_ful  => wr_ful_reg
  );
end str;
