-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 10
-- > run -all
-- > observe verify_data as hexadecimal in Wave window, shows the incrementing ADC data when lvds_skew_di = c_lvds_skew_zero and lvds_skew_dq = c_lvds_skew_zero

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.aduh_dd_pkg.all;

entity tb_aduh_dd is
end tb_aduh_dd;

architecture tb of tb_aduh_dd is
  -- ADU board
  constant c_adc_w               : natural := 8;
  constant c_ana_diff            : natural := 16;  -- analogue offset value between the port A, B, C, D

  constant c_lvds_skew_zero      : t_natural_arr(c_adc_w - 1 downto 0) := (others => 0);  -- ps unit
  constant c_lvds_skew_init      : t_natural_arr(c_adc_w - 1 downto 0) := (350, 300, 250, 200, 150, 100,  50,   0);  -- ps unit
  constant c_lvds_skew_clk       : natural := 35;  -- ps unit

  constant c_dclk_init_phase_a   : natural := 0;
  constant c_dclk_init_phase_b   : natural := 0;

  -- ADU handler

  --TYPE t_c_aduh_dd_ai IS RECORD
  --  nof_sp      : NATURAL;          -- = 4
  --  nof_adu     : NATURAL;          -- = 2
  --  nof_ports   : NATURAL;          -- = 2
  --  port_w      : NATURAL;          -- = 8
  --  dd_factor   : NATURAL;          -- = 2
  --  rx_factor   : NATURAL;          -- = 2
  --  deskew      : t_c_aduh_delays;  -- = (0, 0, (OTHERS=>0), (OTHERS=>0), (OTHERS=>0), (OTHERS=>0))  -- clock: a, b, data: a, b, c, d
  --END RECORD;
  --CONSTANT c_ai                  : t_c_aduh_dd_ai := c_aduh_dd_ai;  -- use defaults
  constant c_ai                  : t_c_aduh_dd_ai := (4, 2, 2, c_adc_w, 2, 2, true, false, (7, 7, (0, 1, 2, 3, 4, 5, 6, 7),
                                                                                                  (0, 1, 2, 3, 4, 5, 6, 7),
                                                                                                  (0, 1, 2, 3, 4, 5, 6, 7),
                                                                                                  (0, 1, 2, 3, 4, 5, 6, 7)));  -- use defaults, compensate for c_lvds_skew_init

  constant c_dp_factor           : natural := c_ai.rx_factor * c_ai.dd_factor;  -- = 4 = 2 * 2

  constant c_sample_period       : time := 1250 ps;  -- 800 MTps
  constant c_dp_clk_period       : time := c_sample_period * c_dp_factor;  -- 200 MHz

  constant c_dp_clk_skew         : natural := 235;  -- ps unit, model skew between dp_clk and SCLK, both are in lock with the same reference

  constant c_dp_phs_clk_step     : time := c_dp_clk_period / 32;  -- the PLL can output clocks with phase shifts of 360/32 = 11.25 degrees
  constant c_dp_phs_clk_period   : natural := 32;  -- number of dp_clk periods per dp_phs_clk period
  constant c_nof_dp_phs_clk      : natural := 1;

  constant c_dp_dat_w            : natural := c_dp_factor * c_adc_w;  -- 32 bit

  constant c_rl                  : natural := 1;
  constant c_verify_delay        : natural := 10;
  constant c_verify_period       : time := 3 us;

  type t_dp_data_arr is array (integer range <>) of std_logic_vector(c_dp_dat_w - 1 downto 0);

  -- Analogue
  signal ANA_DAT             : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- Common ADC reference data source
  signal ANA_A               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port A
  signal ANA_B               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port B
  signal ANA_C               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port C
  signal ANA_D               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port D
  signal SCLK                : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK_AB             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_CD             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_RST_AB         : std_logic;  -- synchronise digital lvds clock
  signal DCLK_RST_CD         : std_logic;  -- synchronise digital lvds clock
  signal DIG_A               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_C               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_D               : std_logic_vector(c_ai.port_w - 1 downto 0);

  signal test_pattern_en     : std_logic := '0';
  signal lvds_skew_di        : t_natural_arr(c_ai.port_w - 1 downto 0) := c_lvds_skew_zero;  -- c_lvds_skew_init;
  signal lvds_skew_dq        : t_natural_arr(c_ai.port_w - 1 downto 0) := c_lvds_skew_zero;  -- c_lvds_skew_init;
  signal lvds_skew_dclk      : natural := c_lvds_skew_clk;

  -- Digital streaming
  signal tb_end              : std_logic := '0';
  signal dp_clk_ref          : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_clk              : std_logic;
  signal dp_rst              : std_logic;
  signal dp_phs_cnt          : natural := 0;
  signal dp_phs_clk          : std_logic := '1';
  signal dp_phs_clk_vec      : std_logic_vector(c_nof_dp_phs_clk - 1 downto 0);

  signal dp_sosi_arr         : t_dp_sosi_arr(0 to c_ai.nof_sp - 1);

  -- MM Interface
  signal ab_locked           : std_logic;
  signal ab_stable           : std_logic;
  signal ab_stable_ack       : std_logic := '0';

  signal cd_locked           : std_logic;
  signal cd_stable           : std_logic;
  signal cd_stable_ack       : std_logic := '0';

  -- Verify
  signal verify_en           : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_en_all       : std_logic := '0';
  signal verify_valid        : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_done         : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_data         : t_dp_data_arr(   0 to c_ai.nof_sp - 1);
  signal prev_verify_data    : t_dp_data_arr(   0 to c_ai.nof_sp - 1);
  signal sl0                 : std_logic := '0';
  signal sl1                 : std_logic := '1';
  signal slv0                : std_logic_vector(1 downto 0) := "00";
begin
  tb_end <= '0', '1' after 10 us;

  -----------------------------------------------------------------------------
  -- ADU0 and ADU1 for BN port A,B and C,D
  -----------------------------------------------------------------------------

  -- Same analogue reference signal for all ADC, use incrementing data to ease the verification
  ANA_DAT <= INCR_UVEC(ANA_DAT, 1) when rising_edge(SCLK);
  ANA_A   <= INCR_UVEC(ANA_DAT, 0 * c_ana_diff);
  ANA_B   <= INCR_UVEC(ANA_DAT, 1 * c_ana_diff);
  ANA_C   <= INCR_UVEC(ANA_DAT, 2 * c_ana_diff);
  ANA_D   <= INCR_UVEC(ANA_DAT, 3 * c_ana_diff);

  -- Same sample clock for all ADC
  SCLK <= not SCLK or tb_end after c_sample_period / 2;

  -- ADU model with National ADC and including backplane rewiring on AUB
  u_adu_AB : entity work.adu_half
  generic map (
    g_dclk_init_phase => c_dclk_init_phase_a
  )
  port map (
    AI              => TO_SINT(ANA_A),
    AQ              => TO_SINT(ANA_B),
    CLK             => SCLK,
    DCLK            => DCLK_AB,
    DCLK_RST        => DCLK_RST_AB,
    DI              => DIG_A,
    DQ              => DIG_B,

    test_pattern_en => test_pattern_en,
    lvds_skew_di    => lvds_skew_di,
    lvds_skew_dq    => lvds_skew_dq,
    lvds_skew_dclk  => lvds_skew_dclk
  );

  u_adu_CD : entity work.adu_half
  generic map (
    g_dclk_init_phase => c_dclk_init_phase_b
  )
  port map (
    AI              => TO_SINT(ANA_C),
    AQ              => TO_SINT(ANA_D),
    CLK             => SCLK,
    DCLK            => DCLK_CD,
    DCLK_RST        => DCLK_RST_CD,
    DI              => DIG_C,
    DQ              => DIG_D,

    test_pattern_en => test_pattern_en,
    lvds_skew_di    => lvds_skew_di,
    lvds_skew_dq    => lvds_skew_dq,
    lvds_skew_dclk  => lvds_skew_dclk
  );

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------

  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk_ref <= not dp_clk_ref or tb_end after c_dp_clk_period / 2;
  dp_clk <= transport dp_clk_ref after c_dp_clk_skew * 1 ps;

  -- Create DP phase reference clock
  p_dp_phs_clk : process(dp_clk)
  begin
    if c_dp_phs_clk_period = 1 then
      dp_phs_clk <= dp_clk;
    elsif rising_edge(dp_clk) then
      if dp_phs_cnt mod (c_dp_phs_clk_period / 2) = 0 then
        dp_phs_clk <= not dp_phs_clk;
      end if;
      if dp_phs_cnt = c_dp_phs_clk_period - 1 then
        dp_phs_cnt <= 0;
      else
        dp_phs_cnt <= dp_phs_cnt + 1;
      end if;
    end if;
  end process;

  gen_dp_phs_clk_vec : for I in c_nof_dp_phs_clk - 1 downto 0 generate
    dp_phs_clk_vec(I) <= transport dp_phs_clk after I * c_dp_phs_clk_step;
  end generate;

  u_aduh_dd : entity work.aduh_dd
  generic map (
    g_ai => c_ai
  )
  port map (
    -- LVDS Interface
    -- . g_ai.nof_sp = 4, fixed support 4 signal paths A,B,C,D
    ADC_BI_A         => DIG_A,
    ADC_BI_B         => DIG_B,
    ADC_BI_C         => DIG_C,
    ADC_BI_D         => DIG_D,

    ADC_BI_A_CLK     => DCLK_AB,
    ADC_BI_D_CLK     => DCLK_CD,

    ADC_BI_A_CLK_RST => DCLK_RST_AB,
    ADC_BI_D_CLK_RST => DCLK_RST_CD,

    -- MM Interface
    ab_locked        => ab_locked,
    ab_stable        => ab_stable,
    ab_stable_ack    => ab_stable_ack,

    cd_locked        => cd_locked,
    cd_stable        => cd_stable,
    cd_stable_ack    => cd_stable_ack,

    -- DP Interface
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    dp_phs_clk_vec   => dp_phs_clk_vec,

    -- . Streaming
    src_out_arr      => dp_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Verify dp_sosi_arr
  -- . verify that that the test has run at all
  -- . verify that the valid data per ADC port is incrementing
  -- . verify that the valid data between the ADC ports has the correct diff (alignment)
  -----------------------------------------------------------------------------

  -- Verify that that the test has run at all
  verify_en_all <= vector_and(verify_en);

  p_verify_run : process
  begin
    -- Wait some time
    wait for c_verify_period;
    if verify_en_all /= '1' then
      report "ADUH using DD: No valid data, test may not be running."
        severity ERROR;
    end if;
    wait;
  end process;

  verify_valid(0) <= dp_sosi_arr(0).valid;
  verify_valid(1) <= dp_sosi_arr(1).valid;
  verify_valid(2) <= dp_sosi_arr(2).valid;
  verify_valid(3) <= dp_sosi_arr(3).valid;

  gen_verify : for I in 0 to c_ai.nof_sp - 1 generate
    -- Enable verify after first valid data and keep it enabled
    proc_dp_verify_en(c_verify_delay, dp_rst, dp_clk, verify_valid(I), verify_en(I));

    -- Verify that the symbols in the data are incrementing per ADC (ready not used, empty not used)
    verify_data(I) <= dp_sosi_arr(I).data(c_dp_dat_w - 1 downto 0);
    proc_dp_verify_symbols(c_rl, c_dp_dat_w, c_adc_w, dp_clk, verify_en(I), sl1, dp_sosi_arr(I).valid, sl0, verify_data(I), slv0, prev_verify_data(I));

    -- Verify the data alignment between the ADCs by checking the expected c_ana_diff offset
    p_alignment : process(dp_clk)
    begin
      if I > 0 then
        if rising_edge(dp_clk) then
          if verify_en_all = '1' then
            for J in 0 to c_dp_factor - 1 loop
              if verify_data(I)((J + 1) * c_adc_w - 1 downto J * c_adc_w) /= INCR_UVEC(verify_data(I - 1)((J + 1) * c_adc_w - 1 downto J * c_adc_w), c_ana_diff) then
                report "ADUH using DD: Wrong alignment between ADCs."
                  severity ERROR;
              end if;
            end loop;
          end if;
        end if;
      end if;
    end process;

  end generate;  -- gen_verify
end tb;
