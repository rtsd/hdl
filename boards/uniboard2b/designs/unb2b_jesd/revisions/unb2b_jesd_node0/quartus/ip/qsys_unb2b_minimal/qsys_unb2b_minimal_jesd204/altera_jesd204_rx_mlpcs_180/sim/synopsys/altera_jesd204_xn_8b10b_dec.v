// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
NNVMrrG/Sve5IBcPjm3422aFjxmvBl43ZanzP82jA6XNNa7zRyOmJRlWgXq3MMfQ
7OsMcWgA7R4uBDAqs0h97N3qiCcjRQ6tOeTHW6bRgerGe7kmRDKuV3lgV+xvqGxg
nc6MnqPcSuyGvu2rcSaQ1ZeRm2izyZy9Ghtm3Fdn1F0=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1616 )
`pragma protect data_block
A0+MOwdMxq96OJSZhJCStX3O3Js+nGItp5b8BXkZbbGs/FLUYfERwXmpRmSawRmr
0sz/yoXdSkELtnoTexqmezhgLVrPYdVzfqYbn/52SfOl0U3Sb3MH7TFFEoY3OUTF
0PFN2sTJ6JP204CzzgiXEYTnPIA2Z9ZshhZ4KquMnT8PJeVqfPuyCi8ZhSTTXvCn
MAZBwUBn9UtDJJUalmIaMx7CNZdFqUqkDNRQPJhwriAz1DUQEN4imU989NdaXkXB
2R3GlNApM0zn0/7W7xVnb/OaTd0JMhWA2LB/8dU/VzE2xHUA4aNteEzXQj8C8SWF
WtTE41efdYBxcBJMjgNSyulZgve9QSYgjtiBstbYVAcgko5mYnpxkgP72F2P1Eak
aLenZYzalpw45YUEJZOrYu/b+0swexykptiu0m2RLU9URVMf42/pivQQLp5nWLGy
U4qcSoUVoGG/0+ZJs+fj9qKvvNI8bFWY9AiJNIGnD7+oNEsI2w7Goa+92awAQMia
xo67blGlk4avckWIJBGvX4rf0xrhY5v2B9mnx00Av7mwUJtRj5Lg1Llug+sjIX00
E/IC39o2QVTCnpQYsZymOb1hrIbUjxLeSiU/pG+pmKW6kuksPZCDe/3e/p5ciLt9
N8FcD1amJbvrK70tTzOIUfO3zSiFwwcGrGMB/bujngRdHzGXytBs2qDisi4YZF+m
FX+r3buvbWWMUJTqF1nVjBj6PUjX2JeohlIJz6mt0X6F73O39f9RClnjQxcKKWPb
o3cqMAiMTYuFyax6cbLUJ+RM12h9nCqbn8fQV26Om+t0RIiOo9H+fkaOKxGn9vYV
uJfR+6mHXjaRvz4cAfwD3N2U6/IxEOv6LAyaPCeBi/sURgHnsiY1w3TvNFHNJYRo
ae4iLqW3T0aZDd98ie8piaoYg7nN6xgoTy0UUXllZG3oAyxQev89pZkhp4leTRbE
+egZkgHJd52ksPDxv4HV8Tl+C5ATumGrRzdzOO5R+/LSuuHI/DVqKN8aw5ZgaKlm
di9gxOpv2YJmSdQTVBEgisgNoAkt25mhz01N/91VForzvrknVV9w2zZWvt3vS/tT
CGCnqeArXPI4oRQIK5e+A4MdwxRtQR2KANssVVgSx820kVQ1OonVxsqzvj9JTYeJ
ls5zXhHer2/uUb7HU9qgFyUg6VJ9ihXZopUImjfMQs5og4PUSUQfXEM0mrAPQTNO
QrRNpqEcw4cAOfPuhbF+Wba62hvArUrvTOJZxySftMgmg0x7cnIJVFOiU741m3iB
2qNNvfHEpXP3i06OTEqjgSPNpMgAKl5yILwKsRiLI2ZKar81i3IuhBMqTOSw0Gfg
pzCnHPSdBkG11IxiT8R3bMgAzpfWtEASzyrh9DbZAqoG2ZTzjOiRK+YHtEG704M6
DQ/xhs/UTaVMaHTngltR6aCAGIDcN46xr+nGlnI1+drYl+tDdqWLP0zn4s0WexZJ
LFuFPzkH3fK+n9R+AVnRO1zE7n5VOH1e3oawOKzkwX1/0xxpKtEKitO2kHPMv0GC
TZLSsQlNctit3VRIp5Ds4Da51CFmG3msNSFVxOAiWr0I/bJjud28xlmHLKnExJZ5
xe8/v8rmhRUCI3vB5FE/1pVySlU8gawhmkXNrJ8ZMoegtmsdmj+ET/Eg3fFRoF6/
oJSRALH3DvIJ/kf6Puckzy2MkQCZazWQI2aN4qpW2OD8m1ZQAqeWUO6L2kJ16T4J
+EBHqgnTho0IL6SW5sXvj/F4BnIxDr9pIgsq4oNAoidT2SBMFfQog5hKm4rOUK9V
jSh1jCF6ySuOQ1WfzWVLMb+bHEhIw5gdVx2v30eArIGUzPC4VIvTgou18o2a4eWO
l1thFnqHP8vnXwi96SgP5sSvb93+5ehTeZ1XE7TVi8RfFS+qW5iZHqd2FRk2EMCc
0DDjbqk9tJLjIthTOzBj1AaLw0LlT2IGYOxiGYbmnYRLWWz8FpQrbnF+Q+XkMmFX
Z6dY2t8P/tG+U3Im8WG/tNMBrPe/pV4bI2ZfKpzs6+bOc/Y5iQjRysY4cAjuQMR+
Ydys8cU67vIyUq/R8YvTr63DsGz7CStWdW+0LUSVKppLxUfe1suXGdwLtuDSteoO
nF2GkQeAoWiLm/dGkVGsEUGrrsaBeF6H8o6QtHAFTxU=

`pragma protect end_protected
