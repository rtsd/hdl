-------------------------------------------------------------------------------
-- Title      : Correlator Sample Bus Delay Line
-- Project    :
-------------------------------------------------------------------------------
-- File       : cmac_sample_bus_delay.vhd
-- Author     : William Kamp  <william.kamp@aut.ac.nz>
-- Company    : High Performance Computing Research Lab, Auckland University of Technology
-- Created    : 2016-10-12
-- Last update: 2018-07-02
-- Platform   :
-- Standard   : VHDL'2008
-------------------------------------------------------------------------------
-- Description: Delays a std_logic_vector by a constant number of cycles g_CYCLES_DELAY.
-- For g_CYCLES_DELAY < 4, this is done using using flip-flops. This should create
-- travel flops in the hyperflex architecture.
-- For g_CYCLES_DELAY >= 4, this is done using a RAM.
-------------------------------------------------------------------------------
-- Copyright (c) 2016 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-10-12  1.0      wkamp   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.misc_tools_pkg.all; -- ceil_log2

library work;
use work.lfsr_pkg.all; -- lfsr, lfsr_advance

entity pipeline_delay_ram is

    generic (
        g_CYCLES_DELAY     : natural;
        g_FORCE_USE_FLOPS  : boolean := false; -- Implement the delay using only FLOPs.
        g_FLOPS_BEFORE_RAM : natural := 0;     -- number of input flops.
        g_FLOPS_AFTER_RAM  : natural := 0      -- number of output flops.
    );

    port (
        i_clk      : in  std_logic;
        i_clk_ena  : in  std_logic := '1';
        i_bus      : in  std_logic_vector;
        i_bus_vld  : in  std_logic := '1';
        o_bus_stop : out std_logic;
        o_bus      : out std_logic_vector;
        o_bus_vld  : out std_logic;
        i_bus_stop : in  std_logic := '0'
    );

end entity pipeline_delay_ram;

architecture rtl of pipeline_delay_ram is

    constant c_RAM_CYCLES : integer := g_CYCLES_DELAY - g_FLOPS_BEFORE_RAM - g_FLOPS_AFTER_RAM;

    type t_delay is array (natural range <>) of std_logic_vector(i_bus'range);

    signal delay_line     : t_delay(g_CYCLES_DELAY downto 0);
    signal delay_line_vld : std_logic_vector(g_CYCLES_DELAY downto 0);

    function f_ASRR (constant FORCE_USE_FLOPS : boolean) return string is
    begin
        if FORCE_USE_FLOPS then
            return "off";
        end if;
        return "on";
    end function;

    -- Prevent large pipelines from going into memory-based altshift_taps, since that won't take advantage of Hyper-Registers
    attribute altera_attribute : string;
    attribute altera_attribute of delay_line     : signal is "-name AUTO_SHIFT_REGISTER_RECOGNITION " & f_ASRR(g_FORCE_USE_FLOPS);
 	attribute altera_attribute of delay_line_vld : signal is "-name AUTO_SHIFT_REGISTER_RECOGNITION " & f_ASRR(g_FORCE_USE_FLOPS);

begin -- architecture rtl

    o_bus_stop <= i_bus_stop;

    -- Here we implement a zero delay.
    delay_line(0)     <= i_bus;
    delay_line_vld(0) <= i_bus_vld;
    o_bus             <= delay_line(g_CYCLES_DELAY);
    o_bus_vld         <= delay_line_vld(g_CYCLES_DELAY) and i_clk_ena;

    -- Here delays greater than or equal to 4 using a RAM.
    G_DELAY : if c_RAM_CYCLES >= 4 and not g_FORCE_USE_FLOPS generate
        signal wr_addr : unsigned(ceil_log2(c_RAM_CYCLES)-1 downto 0) := (others => '0');
        signal rd_addr : unsigned(wr_addr'range)                      := (others => '0');

        signal wr_data : std_logic_vector(i_bus'range);
        signal wr_vld  : std_logic;
        signal rd_vld  : std_logic;
        signal rd_data : std_logic_vector(wr_data'range);
    begin
        P_ADDR_GEN : process (i_clk) is
        begin
            if rising_edge(i_clk) then
                if i_clk_ena then
                    -- Note that in using an LFSR one address (highest address) in RAM goes unused!
                    wr_addr <= lfsr(wr_addr);
                    rd_addr <= lfsr_advance(wr_addr, 2**wr_addr'length - (c_RAM_CYCLES - 2));
                    -- minus 2 for RAM latency.

                    if g_FLOPS_BEFORE_RAM > 0 then
                        delay_line(g_FLOPS_BEFORE_RAM downto 1) <= delay_line(g_FLOPS_BEFORE_RAM-1 downto 0);
                    end if;

                    -- data_vld delay line implemented in flops (may be converted to alt-shift-taps)
                    -- so that it can be used as a write_enable and read_enable for the data RAM.
                    -- This is done to reduce the dynamic power of the RAM by disabling the address
                    -- logic when it is not needed.
                    delay_line_vld(g_CYCLES_DELAY downto 1) <= delay_line_vld(g_CYCLES_DELAY-1 downto 0);
                end if;
                assert i_clk_ena report "i_clk_ena is only currently supported with g_FORCE_USE_FLOPS = true. To fix E_SDP_DATA_RAM needs a clk_enable port added." severity failure;
            end if;
        end process;

        wr_data <= delay_line(g_FLOPS_BEFORE_RAM);
        wr_vld  <= delay_line_vld(g_FLOPS_BEFORE_RAM) and i_clk_ena;
        rd_vld  <= delay_line_vld(g_FLOPS_BEFORE_RAM+c_RAM_CYCLES-2); -- minus 2 for RAM latency.

        E_SDP_DATA_RAM : entity work.sdp_ram
            generic map (
                g_REG_OUTPUT => true) -- [boolean]
            port map (
                i_clk       => i_clk,    -- [std_logic]
                i_clk_reset => '0',      -- [std_logic]
                i_wr_addr   => wr_addr,  -- [unsigned]
                i_wr_en     => wr_vld,   -- [std_logic]
                i_wr_data   => wr_data,  -- [std_logic_vector]
                i_rd_addr   => rd_addr,  -- [unsigned]
                i_rd_en     => rd_vld,   -- [std_logic]
                o_rd_data   => rd_data); -- [std_logic_vector]

        delay_line(g_FLOPS_BEFORE_RAM+c_RAM_CYCLES) <= rd_data;

        P_OUT_FLOPS : process (i_clk) is
        begin
            if rising_edge(i_clk) then
                if i_clk_ena then
                    if g_FLOPS_AFTER_RAM > 0 then
                        delay_line(g_CYCLES_DELAY downto g_FLOPS_BEFORE_RAM+c_RAM_CYCLES+1) <=
                            delay_line(g_CYCLES_DELAY-1 downto g_FLOPS_BEFORE_RAM+c_RAM_CYCLES);
                    end if;
                end if;
            end if;
        end process;

    -- Here short delays using just flops.
    elsif g_CYCLES_DELAY > 0 generate
        P_DELAY : process (i_clk) is
        begin
            if rising_edge(i_clk) then
                if i_clk_ena then
                    delay_line(g_CYCLES_DELAY downto 1)     <= delay_line(g_CYCLES_DELAY-1 downto 0);
                    delay_line_vld(g_CYCLES_DELAY downto 1) <= delay_line_vld(g_CYCLES_DELAY-1 downto 0);
                end if;
            end if;
        end process;
    end generate;

end architecture rtl;
