--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Provide a complex adder tree with streaming array I/O types
-- Description:

entity dp_complex_add is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_nof_inputs  : natural;
    g_data_w      : natural  -- Complex input data width
   );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    snk_in_arr : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

    src_out    : out t_dp_sosi
  );
end dp_complex_add;

architecture str of dp_complex_add is
  constant c_pipeline                : natural := 1;
  constant c_pipeline_adder_tree     : natural := c_pipeline * ceil_log2(g_nof_inputs);

  constant c_common_adder_tree_sum_w : natural := g_data_w + ceil_log2(g_nof_inputs);

  signal snk_in                      : t_dp_sosi;
  signal snk_in_pipe                 : t_dp_sosi;

  signal common_adder_tree_re_in_dat : std_logic_vector(g_nof_inputs * g_data_w - 1 downto 0);
  signal common_adder_tree_im_in_dat : std_logic_vector(g_nof_inputs * g_data_w - 1 downto 0);

  signal common_adder_tree_re_sum    : std_logic_vector(c_common_adder_tree_sum_w - 1 downto 0);
  signal common_adder_tree_im_sum    : std_logic_vector(c_common_adder_tree_sum_w - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Complex Adder Trees
  -----------------------------------------------------------------------------
  gen_nof_inputs : for i in 0 to g_nof_inputs - 1 generate
    -- DP SOSI array to flat STD_LOGIC_VECTORs
    common_adder_tree_re_in_dat((i + 1) * g_data_w - 1 downto i * g_data_w) <= snk_in_arr(i).re(g_data_w - 1 downto 0);
    common_adder_tree_im_in_dat((i + 1) * g_data_w - 1 downto i * g_data_w) <= snk_in_arr(i).im(g_data_w - 1 downto 0);
  end generate;

  -- One adder tree for the real part
  u_adder_tree_re : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_nof_inputs     => g_nof_inputs,
    g_dat_w          => g_data_w,
    g_sum_w          => c_common_adder_tree_sum_w
  )
  port map (
    clk    => clk,
    in_dat => common_adder_tree_re_in_dat,
    sum    => common_adder_tree_re_sum
  );

  -- One adder tree for the imaginary part
  u_adder_tree_im : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_nof_inputs     => g_nof_inputs,
    g_dat_w          => g_data_w,
    g_sum_w          => c_common_adder_tree_sum_w
  )
  port map (
    clk    => clk,
    in_dat => common_adder_tree_im_in_dat,
    sum    => common_adder_tree_im_sum
  );

  p_src_out : process(snk_in_pipe, common_adder_tree_re_sum, common_adder_tree_im_sum)
  begin
    src_out <= snk_in_pipe;
    src_out.re <= RESIZE_DP_DSP_DATA(common_adder_tree_re_sum(c_common_adder_tree_sum_w - 1 downto 0));
    src_out.im <= RESIZE_DP_DSP_DATA(common_adder_tree_im_sum(c_common_adder_tree_sum_w - 1 downto 0));
  end process;

  -----------------------------------------------------------------------------
  -- Forward the other snk_in fields with the correct latency
  -----------------------------------------------------------------------------

  -- All g_nof_inputs have same, so use other fields from input 0 for all
  snk_in <= snk_in_arr(0);

  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline_adder_tree
  )
  port map (
    rst     => rst,
    clk     => clk,
    -- ST sink
    snk_in  => snk_in,
    -- ST source
    src_out => snk_in_pipe
  );
end str;
