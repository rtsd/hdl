-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Scramble quantization noise crosstalk between two real inputs
-- Description:
-- . Ported from LOFAR1, see readme_lofar1.txt
-- . The fft_switch multiplies the samples from two real inputs A and B in a
--   block by +1 or -1. The fft_unswitch undoes this by multiplying the FFT
--   output again by +1 and -1. The fft_unswitch takes account of that the FFT
--   has time mutliplexed the two spectra of the two inputs.
-- . The input switching is pseudo random base on a LFSR (linear feedback
--   shift register) sequence. The fft_switch and fft_unswitch start at the
--   first in_val = '1' and then continue 'forever' until a next power cycle
--   by rst ='1'.
-- Remark:
-- . Copy from applications/lofar1/RSP/pft2/src/vhdl/pft_switch.vhd
-- . Removed in_sync, because the in_val are guaranteed to arrive in blocks of
--   c_nof_clk_per_block samples, forever after rst release.
--   The purpose of  the in_sync is to recover from an fractional input block,
--   but that cannot occur. The other parts of the FFT also rely on this block
--   processing, without need for in_sync to recover from fractional blocks.
--   The application that uses the FFT must guarantee to only pass on complete
--   blocks of c_nof_clk_per_block samples to the FFT.
-- . The two real inputs each use another LFSR sequence, like for LOFAR1.
--   For the crosstalk mitigation purpose scrambling only one input would be
--   enough, but scrambling both inputs is fine too.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.fft_pkg.all;

entity fft_switch is
  generic (
    g_switch_en : boolean := false;
    g_seed1     : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := c_fft_switch_seed1;
    g_seed2     : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := c_fft_switch_seed2;
    g_fft_sz_w  : natural;
    g_dat_w     : natural
  );
  port (
    in_re       : in  std_logic_vector(g_dat_w - 1 downto 0);  -- real input A
    in_im       : in  std_logic_vector(g_dat_w - 1 downto 0);  -- real input B
    in_val      : in  std_logic;
    out_re      : out std_logic_vector(g_dat_w - 1 downto 0);
    out_im      : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val     : out std_logic;
    clk         : in  std_logic;
    rst         : in  std_logic
  );
end fft_switch;

architecture rtl of fft_switch is
  constant c_nof_clk_per_block  : natural := 2**g_fft_sz_w;

  signal in_sop        : std_logic;
  signal in_eop        : std_logic;

  signal cnt           : std_logic_vector(g_fft_sz_w downto 0) := (others => '0');
  signal nxt_cnt       : std_logic_vector(cnt'range);

  signal lfsr_bit1     : std_logic;
  signal lfsr_bit2     : std_logic;
  signal lfsr_en       : std_logic;

  signal nxt_out_re    : std_logic_vector(in_re'range);
  signal nxt_out_im    : std_logic_vector(in_im'range);
begin
  -- Create input strobes to view data blocks for debugging
  u_in_strobes : entity common_lib.common_create_strobes_from_valid
  generic map (
    g_pipeline          => false,
    g_nof_clk_per_sync  => c_nof_clk_per_block * 16,  -- void value, sync is not used
    g_nof_clk_per_block => c_nof_clk_per_block
  )
  port map (
    rst       => rst,
    clk       => clk,
    in_val    => in_val,
    out_val   => OPEN,  -- out_val = in_val, because g_pipeline = FALSE
    out_sop   => in_sop,
    out_eop   => in_eop,
    out_sync  => open
  );

  no_switch : if g_switch_en = false generate
    -- wire inputs to outputs
    out_re <= in_re;
    out_im <= in_im;
    out_val <= in_val;
  end generate;

  gen_switch : if g_switch_en = true generate
    p_reg : process (rst, clk)
    begin
      if rst = '1' then
        cnt       <= (others => '0');
        out_val   <= '0';
        out_re    <= (others => '0');
        out_im    <= (others => '0');
      elsif rising_edge(clk) then
        cnt       <= nxt_cnt;
        out_val   <= in_val;
        out_re    <= nxt_out_re;
        out_im    <= nxt_out_im;
      end if;
    end process;

    p_counter: process(cnt, in_val)
    begin
      nxt_cnt <= cnt;
      if in_val = '1' then
        nxt_cnt <= INCR_UVEC(cnt, 1);
      end if;
    end process;

    p_lfsr_ctrl: process(cnt, in_val)
    begin
      if TO_SINT(cnt) = -1 and in_val = '1' then
        lfsr_en <= '1';
      else
        lfsr_en <= '0';
      end if;
    end process;

    p_out: process(in_re, in_im, cnt, lfsr_bit1, lfsr_bit2)
    begin
      nxt_out_re <= in_re;
      nxt_out_im <= in_im;

      if lfsr_bit1 = cnt(cnt'high) then
        nxt_out_re <= NEGATE_SVEC(in_re, g_dat_w);  -- negate block of input A samples
      end if;
      if lfsr_bit2 = cnt(cnt'high) then
        nxt_out_im <= NEGATE_SVEC(in_im, g_dat_w);  -- negate block of input B samples
      end if;
    end process;

    u_fft_lfsr: entity work.fft_lfsr
    generic map (
      g_seed1 => g_seed1,
      g_seed2 => g_seed2
    )
    port map (
      clk      => clk,
      rst      => rst,
      in_en    => lfsr_en,
      out_bit1 => lfsr_bit1,
      out_bit2 => lfsr_bit2
    );
  end generate;
end rtl;
