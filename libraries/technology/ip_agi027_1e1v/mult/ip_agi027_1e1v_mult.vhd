-- --------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -------------------------------------------------------------------------
--
-- Author : D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper / Instantiate MULTiplier IP with generics
-- Reference:
--   Copied from */technology/ip_arria10/mult/ip_arria10_mult.vhd and add component declaration lpm_mult from
--   generated/lpm_mult_1920/sim/ip_agi027_1e1v_lpm_mult_lpm_mult_1920_sphm57q.vhd
-- Remark:
--   Directly instantiate LPM component.
--   The Agilex 7 (agi027_1e1v) supports the lpm library, so the copied file can be reused.
--   This is checked by making the IP files on the basis of the generic and port of the entity, and also the generic and port map.
--   The IP file will also remain present in the folder, so that the settings can be reproduced later.

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

 entity ip_agi027_1e1v_mult is
  generic (
    g_in_a_w           : positive := 18;  -- Width of the data A port
    g_in_b_w           : positive := 18;  -- Width of the data B port
    g_out_p_w          : positive := 36;  -- Width of the result port
--    g_out_s_w          : POSITIVE := 1;       -- Width of the sum port (not used in current designs)
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
--    aclr       : IN  STD_LOGIC := '0'; (not used in current designs)
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
--    sum        : IN  STD_LOGIC_VECTOR(g_nof_mult*g_in_s_w-1 DOWNTO 0) := (OTHERS => '0'); (not used in current designs)
    out_p      : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
 end ip_agi027_1e1v_mult;

architecture str of ip_agi027_1e1v_mult is
  constant c_pipeline : natural := g_pipeline_input + g_pipeline_product + g_pipeline_output;

  -- When g_out_p_w < g_in_a_w+g_in_b_w then the LPM_MULT truncates the LSbits of the product. Therefore
  -- define c_prod_w to be able to let common_mult truncate the LSBits of the product.
  constant c_prod_w : natural := g_in_a_w + g_in_b_w;

  component lpm_mult
  generic (
          lpm_hint  : string;
          lpm_pipeline  : natural;
          lpm_representation  : string;
          lpm_type  : string;
          lpm_widtha  : natural;
          lpm_widthb  : natural;
        --  lpm_widths  : natural;
          lpm_widthp  : natural
  );
  port (
      dataa : in std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
      datab : in std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    --  sum : in STD_LOGIC_VECTOR((g_nof_mult*g_in_s_w-1 DOWNTO 0) := (OTHERS => '0'); (not used in current designs)
    --  aclr : in STD_LOGIC; (not used in current designs)
      clock : in std_logic;
      clken : in std_logic;
      result : out std_logic_vector(g_nof_mult * (g_in_a_w + g_in_b_w) - 1 downto 0)
  );
  end component;

  signal prod  : std_logic_vector(g_nof_mult * c_prod_w - 1 downto 0);
begin
  gen_mult : for I in 0 to g_nof_mult - 1 generate
    m : lpm_mult
    generic map (
      lpm_hint => "MAXIMIZE_SPEED=5",  -- default "UNUSED"
      lpm_pipeline => c_pipeline,
      lpm_representation => g_representation,
      lpm_type => "LPM_MULT",
      lpm_widtha => g_in_a_w,
      lpm_widthb => g_in_b_w,
--      lpm_widths => g_in_s_w, (Partial sum input with not used in current designs)
      lpm_widthp => c_prod_w
    )
    port map (
      dataa => in_a((I + 1) * g_in_a_w - 1 downto I * g_in_a_w),
      datab => in_b((I + 1) * g_in_b_w - 1 downto I * g_in_b_w),
    --  sum   => sum((I+1)*g_in_s_w-1 DOWNTO I*g_in_s_w),  -- partial sum input is not used in current designs
    --  aclr  => aclr,                                     -- async clear input is not used in current designs
      clock => clk,
      clken => clken,
      result => prod((I + 1) * c_prod_w - 1 downto I * c_prod_w)
    );

    out_p <= prod;
---- Truncate MSbits, also for signed (common_pkg.vhd for explanation of RESIZE_SVEC)
--    out_p((I+1)*g_out_p_w-1 DOWNTO I*g_out_p_w) <= RESIZE_SVEC(prod((I+1)*c_prod_w-1 DOWNTO I*c_prod_w), g_out_p_w) WHEN g_representation="SIGNED" ELSE
--                                                   RESIZE_UVEC(prod((I+1)*c_prod_w-1 DOWNTO I*c_prod_w), g_out_p_w);
  end generate;
end str;
