# (C) 1992-2018 Intel Corporation.                            
# Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
# and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
# and/or other countries. Other marks and brands may be claimed as the property  
# of others. See Trademarks on intel.com for full list of Intel trademarks or    
# the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
# Your use of Intel Corporation's design tools, logic functions and other        
# software and tools, and its AMPP partner logic functions, and any output       
# files any of the foregoing (including device programming or simulation         
# files), and any associated documentation or information are expressly subject  
# to the terms and conditions of the Altera Program License Subscription         
# Agreement, Intel MegaCore Function License Agreement, or other applicable      
# license agreement, including, without limitation, that your use is for the     
# sole purpose of programming logic devices manufactured by Intel and sold by    
# Intel or its authorized distributors.  Please refer to the applicable          
# agreement for further details.                                                 
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle
# Purpose:
# . pre-compile script for OpenCL applications
# Description:
# . " "
# ##########################################################################
post_message "Running pre-flow script"

# Make sure OpenCL SDK installation exists
post_message "Checking for OpenCL SDK installation, environment should have INTELFPGAOCLSDKROOT defined"
if {[catch {set sdk_root $::env(INTELFPGAOCLSDKROOT)} result]} {
  post_message -type error "OpenCL SDK installation not found.  Make sure INTELFPGAOCLSDKROOT is correctly set"
  post_message -type error "Terminating pre-flow script"
  exit 2
} else {
  post_message "INTELFPGAOCLSDKROOT=$::env(INTELFPGAOCLSDKROOT)"
}


# Make sure RadioHDL installation exists
post_message "Checking for RadioHDL installation, environment should have HDL_BUILD_DIR defined"
if {[catch {set radiohdl_build $::env(HDL_BUILD_DIR)} result]} {
  post_message -type error "RadioHDL installation not found.  Make sure HDL_BUILD_DIR are correctly set"
  post_message -type error "Terminating pre-flow script"
  exit 2
} else {
  post_message "HDL_BUILD_DIR=$::env(HDL_BUILD_DIR)"
}

# Load OpenCL BSP utility functions
source "$sdk_root/ip/board/bsp/opencl_bsp_util.tcl"

set project_name  top
set revision_name flat
set board_name UNKNOWN

# Get board name (from quartus(args) variable)
if { [llength $quartus(args)] > 0 } {
  set board_name [lindex $quartus(args) 0]
} else {
  set board_name ta2_unb2b_bsp
}

set fast_compile  [::aocl_fast_compile::is_fast_compile]
set device_name   [::opencl_bsp::get_device_name $project_name $revision_name]

# Make sure 
##############################################################################
##############################       MAIN        #############################
##############################################################################

post_message "Project name: $project_name"
post_message "Revision name: $revision_name"
post_message "Device part name: $device_name"

# Check if fast compile is on during base revision
if {$revision_name eq "flat"} {
  post_message "Compiling flat revision"
} else {
  post_message -type error "This BSP only supports compilation of flat revision, you are trying to compile $revision_name revision"
  post_message -type error "Terminating pre-flow script"
  exit 2
}  
 
# Copy qsf file
if {[file exists "$::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/$board_name.qsf"] == 1} {
  file copy -force $::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/$board_name.qsf radiohdl_components.qsf 
} else {
  post_message -type error "It seems that the BSP has not been initialized yet, please execute the following commands and try again:"
  post_message -type error "quartus_config unb2b; run_qsys unb2b $board_name board.qsys"
  post_message -type error "Terminating pre-flow script"
  exit 2

}
# Copy memory initialization files
if {[file exists "$::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/onchip_memory2_0.hex"] == 1} {
  file copy -force $::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/onchip_memory2_0.hex onchip_memory2_0.hex 
} else {
  post_message -type error "It seems that the BSP has not been initialized yet, please execute the following commands and try again:"
  post_message -type error "quartus_config unb2b; run_qsys unb2b $board_name board.qsys"
  post_message -type error "Terminating pre-flow script"
  exit 2
}
if {[file exists "$::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/$board_name.mif"] == 1} {
  file copy -force $::env(HDL_BUILD_DIR)/unb2b/quartus/$board_name/$board_name.mif $board_name.mif
} else {
  post_message -type error "It seems that the BSP has not been initialized yet, please execute the following commands and try again:"
  post_message -type error "quartus_config unb2b; run_qsys unb2b $board_name board.qsys"
  post_message -type error "Terminating pre-flow script"
  exit 2
}
post_message "Compiling $revision_name revision: generating and archiving board.qsys"
post_message "    qsys-generate -syn --family=\"Arria 10\" --part=$device_name board.qsys"
qexec "qsys-generate -syn --family=\"Arria 10\" --part=$device_name board.qsys"
post_message "    qsys-archive --quartus-project=$project_name --rev=opencl_bsp_ip --add-to-project board.qsys"
qexec "qsys-archive --quartus-project=$project_name --rev=opencl_bsp_ip --add-to-project board.qsys"


