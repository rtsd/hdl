-----------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify unb_terminals_mesh in node_bn_terminal_bg using looback
--          of the tx_serial_2arr to the rx_serial_2arr
-- Description:
-- . The serial Tx is loopbacked to the Rx to be able to view the transmitted
--   BG data also at the receive side of the internal unb_terminals_mesh sosi
--   output at rx_usr_sosi_2arr.
-- Usage:
-- > as 12
-- > do wave_delete.do
-- > run -all
-- Observe in Wave window that rx_usr_sosi_2arr is delayed tx_usr_sosi_2arr

library IEEE, common_lib, dp_lib, bf_lib, unb_common_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use bf_lib.bf_pkg.all;
use unb_common_lib.unb_common_pkg.all;

entity tb_tb_node_bn_terminal_bg is
  generic (
    g_diagnostics_en  : boolean := true;  -- when TRUE then alternately insert diagnostics data over the transceivers, else only BG data
    g_nof_sync        : natural := 15
  );
end tb_tb_node_bn_terminal_bg;

architecture tb of tb_tb_node_bn_terminal_bg is
  constant c_dp_pps_period           : natural := 1024;  -- sufficiently long for all nodes to have initialized their TR PHY

  constant c_mesh_nof_serial         : natural := 3;  -- need at least 3 to support the 4 input streams from the BG, use 4 to have transparant dp_distibute
  constant c_mesh_use_rx             : boolean := true or g_diagnostics_en;

  constant c_mesh_ena_reorder        : boolean := false;

  signal tb_clk           : std_logic;
  signal tb_end           : std_logic;

  signal dp_pps           : std_logic;

  signal mesh_tx_serial_2arr   : t_unb_mesh_sl_2arr;
  signal mesh_rx_serial_2arr   : t_unb_mesh_sl_2arr;
begin
  u_tb_node_bn_terminal_bg : entity work.tb_node_bn_terminal_bg
  generic map (
    -- Tb
    g_sim_level               => 1,  -- 0 = simulate GX IP, 1 = use fast serial behavioural model
    g_nof_sync                => g_nof_sync,
    g_chip_id                 => 4,
    -- Application
    g_bf                      => c_bf,
    g_bg_data_file_index_arr  => array_init(0, 16, 1),
    g_bg_data_file_name       => "../../../../../../modules/Lofar/diag/src/data/bf_in_data",
    -- Diagnostics - TRNB
    g_diagnostics_en          => g_diagnostics_en,
    -- Terminals interface
    g_mesh_nof_serial         => c_mesh_nof_serial,
    g_mesh_use_rx             => c_mesh_use_rx,
    g_mesh_ena_reorder        => c_mesh_ena_reorder
  )
  port map (
    tb_clk         => tb_clk,
    tb_end         => tb_end,

    -- Timing
    dp_pps         => dp_pps,

    -- Serial (tr_nonbonded)
    mesh_tx_serial_2arr => mesh_tx_serial_2arr,
    mesh_rx_serial_2arr => mesh_rx_serial_2arr
  );

  ------------------------------------------------------------------------------
  -- External PPS
  -- . periodic over c_dp_pps_period tb_clk cycles and high for 1 cycle
  -- . first active high pulse during node dp_rst will get ignored
  -- . second active high pulse will start enabled BG, subsequent PPS are not used
  ------------------------------------------------------------------------------

  proc_common_gen_pulse(1, c_dp_pps_period, '1', tb_clk, dp_pps);

  ------------------------------------------------------------------------------
  -- Transceivers loopback
  ------------------------------------------------------------------------------

  mesh_rx_serial_2arr <= mesh_tx_serial_2arr;

  ------------------------------------------------------------------------------
  -- Test bench end
  ------------------------------------------------------------------------------

  assert not(NOW > 0 ps and tb_end = '1')
    report "Note: TB END"
    severity FAILURE;  -- need to use FAILURE to stop the simulation, apparently resetting the tr_nonbonded is not enough
end tb;
