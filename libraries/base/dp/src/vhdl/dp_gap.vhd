-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Deassert snk_out.ready to greate gaps in source output.
--
-- Description:
-- . g_dat_len indicates the maximum number of valid data cycles.
--   During the last of these valid cycles, snk_out.ready is deasserted so
--   g_gap_len invalid cycles are produced at the source.
--
-- Remark:
-- . If a gap of sufficient (g_gap_len) length occured by itself (without
--   dp_gap de-asserting snk_out.ready, this gap is accounted for and
--   dp_gap will restart counting valid cycles as soon as the first
--   valid data work comes in.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.dp_stream_pkg.all;
use common_lib.common_pkg.all;

entity dp_gap is
  generic (
    g_dat_len    : natural := 1000000;
    g_gap_len    : natural := 5;
    g_gap_extend : boolean := false  -- if TRUE, the first valid='0' cycle is extended to g_gap_len by de-assertion of snk_out.ready.
    );  -- This results in all gaps having a minimum length of g_gap_len.
  port (
    clk       : in std_logic;
    rst       : in std_logic;
    -- ST sink
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_gap;

architecture rtl of dp_gap is
  constant c_dat_len_w : natural := ceil_log2(g_dat_len);
  constant c_gap_len_w : natural := ceil_log2(largest(g_gap_len, 1));

  type t_state_enum is (s_wait_for_val, s_counting, s_force_not_rdy);

  signal state         : t_state_enum;
  signal nxt_state     : t_state_enum;

  signal clk_cnt       : std_logic_vector(c_dat_len_w - 1 downto 0);
  signal nxt_clk_cnt   : std_logic_vector(c_dat_len_w - 1 downto 0);

  signal gap_cnt       : std_logic_vector(c_gap_len_w - 1 downto 0);
  signal nxt_gap_cnt   : std_logic_vector(c_gap_len_w - 1 downto 0);
begin
  src_out <= snk_in;

  gen_rtl : if g_gap_len > 0 generate
    p_clk : process(clk, rst)
    begin
      if rst = '1' then
        state               <= s_wait_for_val;
        clk_cnt             <= (others => '0');
        gap_cnt             <= (others => '0');
      elsif rising_edge(clk) then
        state               <= nxt_state;
        clk_cnt             <= nxt_clk_cnt;
        gap_cnt             <= nxt_gap_cnt;
      end if;
    end process;

    p_state : process(state, clk_cnt, gap_cnt, snk_in, src_in)
    begin
      nxt_state     <= state;
      nxt_clk_cnt   <= clk_cnt;
      nxt_gap_cnt   <= gap_cnt;
      snk_out       <= src_in;  -- Pass on the flow control by default

      case state is

      when s_wait_for_val =>  -- Wait for valid data to come in
        if snk_in.valid = '1' then
          nxt_clk_cnt <= TO_UVEC(1, c_dat_len_w);
          nxt_gap_cnt <= (others => '0');
          nxt_state   <= s_counting;
        end if;

      when s_counting =>  -- Start counting cycles
        nxt_clk_cnt <= INCR_UVEC(clk_cnt, 1);
        if clk_cnt = TO_UVEC(g_dat_len - 1, c_dat_len_w) then  -- time to force a gap
          nxt_state     <= s_force_not_rdy;
          snk_out.ready <= '0';
          nxt_clk_cnt   <= TO_UVEC(1, c_dat_len_w);  -- we already have 1 clk cycle with ready='0' here
        end if;
        if snk_in.valid = '0' then  -- Also start counting any invalid cycles
          if g_gap_extend = true then
            snk_out.ready <= '0';  -- Keep ready de-asserted. Gap_cnt will increment so it will be released again after g_gap_len.
          end if;
          nxt_gap_cnt <= INCR_UVEC(gap_cnt, 1);
        else
          nxt_gap_cnt <= (others => '0');
        end if;
        if gap_cnt = TO_UVEC(g_gap_len - 1, c_gap_len_w) and snk_in.valid = '0' then  -- A gap of sufficient length occured by itself (or valid='0' was extended); no need to force gap
        -- We've counted g_gap_len-1, plus the current gap cycle = g_gap_len
          nxt_gap_cnt <= (others => '0');
          nxt_clk_cnt <= (others => '0');
          nxt_state   <= s_wait_for_val;
          if g_gap_extend = true then
            snk_out.ready <= src_in.ready;  -- Release the ready signal again if it was forced down because of gap extension
          end if;
        end if;

      when s_force_not_rdy =>  -- Force snk_out.ready to '0' for g_gap_len clk cycles
        snk_out.ready <= '0';
        nxt_clk_cnt <= INCR_UVEC(clk_cnt, 1);
        if clk_cnt = TO_UVEC(g_gap_len - 1, c_dat_len_w) then
          nxt_state   <= s_wait_for_val;
          nxt_clk_cnt <= (others => '0');
        end if;
      end case;
    end process;

  end generate;

  no_rtl : if g_gap_len = 0 generate  -- bypass
    snk_out <= src_in;
  end generate;
end rtl;
