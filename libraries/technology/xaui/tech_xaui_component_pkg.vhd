-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;

package tech_xaui_component_pkg is
  function tech_xaui_mosi_addr_w(technology : in integer) return integer;

  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  component ip_stratixiv_phy_xaui_0 is
  port (
    pll_ref_clk              : in  std_logic                     := '0';  -- pll_ref_clk.clk
    xgmii_tx_clk             : in  std_logic                     := '0';  -- xgmii_tx_clk.clk
    xgmii_rx_clk             : out std_logic;  -- xgmii_rx_clk.clk
    xgmii_rx_dc              : out std_logic_vector(71 downto 0);  -- xgmii_rx_dc.data
    xgmii_tx_dc              : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_tx_dc.data
    xaui_rx_serial_data      : in  std_logic_vector(3 downto 0)  := (others => '0');  -- xaui_rx_serial_data.export
    xaui_tx_serial_data      : out std_logic_vector(3 downto 0);  -- xaui_tx_serial_data.export
    rx_ready                 : out std_logic;  -- rx_ready.export
    tx_ready                 : out std_logic;  -- tx_ready.export
    phy_mgmt_clk             : in  std_logic                     := '0';  -- phy_mgmt_clk.clk
    phy_mgmt_clk_reset       : in  std_logic                     := '0';  -- phy_mgmt_clk_reset.reset
    phy_mgmt_address         : in  std_logic_vector(8 downto 0)  := (others => '0');  -- phy_mgmt.address
    phy_mgmt_read            : in  std_logic                     := '0';  -- .read
    phy_mgmt_readdata        : out std_logic_vector(31 downto 0);  -- .readdata
    phy_mgmt_write           : in  std_logic                     := '0';  -- .write
    phy_mgmt_writedata       : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    phy_mgmt_waitrequest     : out std_logic;  -- .waitrequest
    rx_digitalreset          : in  std_logic                     := '0';  -- rx_digitalreset.data
    tx_digitalreset          : in  std_logic                     := '0';  -- tx_digitalreset.data
    rx_channelaligned        : out std_logic;  -- rx_channelaligned.data
    rx_syncstatus            : out std_logic_vector(7 downto 0);  -- rx_syncstatus.data
    rx_disperr               : out std_logic_vector(7 downto 0);  -- rx_disperr.data
    rx_errdetect             : out std_logic_vector(7 downto 0);  -- rx_errdetect.data
    rx_analogreset           : in  std_logic                     := '0';  -- rx_analogreset.data
    rx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_invpolarity.data
    rx_set_locktodata        : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktodata.data
    rx_set_locktoref         : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktoref.data
    rx_seriallpbken          : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_seriallpbken.data
    tx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- tx_invpolarity.data
    rx_is_lockedtodata       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.data
    rx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- rx_phase_comp_fifo_error.data
    rx_is_lockedtoref        : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.data
    rx_rlv                   : out std_logic_vector(3 downto 0);  -- rx_rlv.data
    rx_rmfifoempty           : out std_logic_vector(3 downto 0);  -- rx_rmfifoempty.data
    rx_rmfifofull            : out std_logic_vector(3 downto 0);  -- rx_rmfifofull.data
    tx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- tx_phase_comp_fifo_error.data
    rx_patterndetect         : out std_logic_vector(7 downto 0);  -- rx_patterndetect.data
    rx_rmfifodatadeleted     : out std_logic_vector(7 downto 0);  -- rx_rmfifodatadeleted.data
    rx_rmfifodatainserted    : out std_logic_vector(7 downto 0);  -- rx_rmfifodatainserted.data
    rx_runningdisp           : out std_logic_vector(7 downto 0);  -- rx_runningdisp.data
    cal_blk_powerdown        : in  std_logic                     := '0';  -- cal_blk_powerdown.data
    pll_powerdown            : in  std_logic                     := '0';  -- pll_powerdown.data
    gxb_powerdown            : in  std_logic                     := '0';  -- gxb_powerdown.data
    pll_locked               : out std_logic;  -- pll_locked.data
    reconfig_from_xcvr       : out std_logic_vector(16 downto 0);  -- reconfig_from_xcvr.data
    reconfig_to_xcvr         : in  std_logic_vector(3 downto 0)  := (others => '0')  -- reconfig_to_xcvr.data
  );
  end component;

  component ip_stratixiv_phy_xaui_1 is
  port (
    pll_ref_clk              : in  std_logic                     := '0';  -- pll_ref_clk.clk
    xgmii_tx_clk             : in  std_logic                     := '0';  -- xgmii_tx_clk.clk
    xgmii_rx_clk             : out std_logic;  -- xgmii_rx_clk.clk
    xgmii_rx_dc              : out std_logic_vector(71 downto 0);  -- xgmii_rx_dc.data
    xgmii_tx_dc              : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_tx_dc.data
    xaui_rx_serial_data      : in  std_logic_vector(3 downto 0)  := (others => '0');  -- xaui_rx_serial_data.export
    xaui_tx_serial_data      : out std_logic_vector(3 downto 0);  -- xaui_tx_serial_data.export
    rx_ready                 : out std_logic;  -- rx_ready.export
    tx_ready                 : out std_logic;  -- tx_ready.export
    phy_mgmt_clk             : in  std_logic                     := '0';  -- phy_mgmt_clk.clk
    phy_mgmt_clk_reset       : in  std_logic                     := '0';  -- phy_mgmt_clk_reset.reset
    phy_mgmt_address         : in  std_logic_vector(8 downto 0)  := (others => '0');  -- phy_mgmt.address
    phy_mgmt_read            : in  std_logic                     := '0';  -- .read
    phy_mgmt_readdata        : out std_logic_vector(31 downto 0);  -- .readdata
    phy_mgmt_write           : in  std_logic                     := '0';  -- .write
    phy_mgmt_writedata       : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    phy_mgmt_waitrequest     : out std_logic;  -- .waitrequest
    rx_digitalreset          : in  std_logic                     := '0';  -- rx_digitalreset.data
    tx_digitalreset          : in  std_logic                     := '0';  -- tx_digitalreset.data
    rx_channelaligned        : out std_logic;  -- rx_channelaligned.data
    rx_syncstatus            : out std_logic_vector(7 downto 0);  -- rx_syncstatus.data
    rx_disperr               : out std_logic_vector(7 downto 0);  -- rx_disperr.data
    rx_errdetect             : out std_logic_vector(7 downto 0);  -- rx_errdetect.data
    rx_analogreset           : in  std_logic                     := '0';  -- rx_analogreset.data
    rx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_invpolarity.data
    rx_set_locktodata        : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktodata.data
    rx_set_locktoref         : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktoref.data
    rx_seriallpbken          : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_seriallpbken.data
    tx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- tx_invpolarity.data
    rx_is_lockedtodata       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.data
    rx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- rx_phase_comp_fifo_error.data
    rx_is_lockedtoref        : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.data
    rx_rlv                   : out std_logic_vector(3 downto 0);  -- rx_rlv.data
    rx_rmfifoempty           : out std_logic_vector(3 downto 0);  -- rx_rmfifoempty.data
    rx_rmfifofull            : out std_logic_vector(3 downto 0);  -- rx_rmfifofull.data
    tx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- tx_phase_comp_fifo_error.data
    rx_patterndetect         : out std_logic_vector(7 downto 0);  -- rx_patterndetect.data
    rx_rmfifodatadeleted     : out std_logic_vector(7 downto 0);  -- rx_rmfifodatadeleted.data
    rx_rmfifodatainserted    : out std_logic_vector(7 downto 0);  -- rx_rmfifodatainserted.data
    rx_runningdisp           : out std_logic_vector(7 downto 0);  -- rx_runningdisp.data
    cal_blk_powerdown        : in  std_logic                     := '0';  -- cal_blk_powerdown.data
    pll_powerdown            : in  std_logic                     := '0';  -- pll_powerdown.data
    gxb_powerdown            : in  std_logic                     := '0';  -- gxb_powerdown.data
    pll_locked               : out std_logic;  -- pll_locked.data
    reconfig_from_xcvr       : out std_logic_vector(16 downto 0);  -- reconfig_from_xcvr.data
    reconfig_to_xcvr         : in  std_logic_vector(3 downto 0)  := (others => '0')  -- reconfig_to_xcvr.data
  );
  end component;

  component ip_stratixiv_phy_xaui_2 is
  port (
    pll_ref_clk              : in  std_logic                     := '0';  -- pll_ref_clk.clk
    xgmii_tx_clk             : in  std_logic                     := '0';  -- xgmii_tx_clk.clk
    xgmii_rx_clk             : out std_logic;  -- xgmii_rx_clk.clk
    xgmii_rx_dc              : out std_logic_vector(71 downto 0);  -- xgmii_rx_dc.data
    xgmii_tx_dc              : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_tx_dc.data
    xaui_rx_serial_data      : in  std_logic_vector(3 downto 0)  := (others => '0');  -- xaui_rx_serial_data.export
    xaui_tx_serial_data      : out std_logic_vector(3 downto 0);  -- xaui_tx_serial_data.export
    rx_ready                 : out std_logic;  -- rx_ready.export
    tx_ready                 : out std_logic;  -- tx_ready.export
    phy_mgmt_clk             : in  std_logic                     := '0';  -- phy_mgmt_clk.clk
    phy_mgmt_clk_reset       : in  std_logic                     := '0';  -- phy_mgmt_clk_reset.reset
    phy_mgmt_address         : in  std_logic_vector(8 downto 0)  := (others => '0');  -- phy_mgmt.address
    phy_mgmt_read            : in  std_logic                     := '0';  -- .read
    phy_mgmt_readdata        : out std_logic_vector(31 downto 0);  -- .readdata
    phy_mgmt_write           : in  std_logic                     := '0';  -- .write
    phy_mgmt_writedata       : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    phy_mgmt_waitrequest     : out std_logic;  -- .waitrequest
    rx_digitalreset          : in  std_logic                     := '0';  -- rx_digitalreset.data
    tx_digitalreset          : in  std_logic                     := '0';  -- tx_digitalreset.data
    rx_channelaligned        : out std_logic;  -- rx_channelaligned.data
    rx_syncstatus            : out std_logic_vector(7 downto 0);  -- rx_syncstatus.data
    rx_disperr               : out std_logic_vector(7 downto 0);  -- rx_disperr.data
    rx_errdetect             : out std_logic_vector(7 downto 0);  -- rx_errdetect.data
    rx_analogreset           : in  std_logic                     := '0';  -- rx_analogreset.data
    rx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_invpolarity.data
    rx_set_locktodata        : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktodata.data
    rx_set_locktoref         : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_set_locktoref.data
    rx_seriallpbken          : in  std_logic_vector(3 downto 0)  := (others => '0');  -- rx_seriallpbken.data
    tx_invpolarity           : in  std_logic_vector(3 downto 0)  := (others => '0');  -- tx_invpolarity.data
    rx_is_lockedtodata       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.data
    rx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- rx_phase_comp_fifo_error.data
    rx_is_lockedtoref        : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.data
    rx_rlv                   : out std_logic_vector(3 downto 0);  -- rx_rlv.data
    rx_rmfifoempty           : out std_logic_vector(3 downto 0);  -- rx_rmfifoempty.data
    rx_rmfifofull            : out std_logic_vector(3 downto 0);  -- rx_rmfifofull.data
    tx_phase_comp_fifo_error : out std_logic_vector(3 downto 0);  -- tx_phase_comp_fifo_error.data
    rx_patterndetect         : out std_logic_vector(7 downto 0);  -- rx_patterndetect.data
    rx_rmfifodatadeleted     : out std_logic_vector(7 downto 0);  -- rx_rmfifodatadeleted.data
    rx_rmfifodatainserted    : out std_logic_vector(7 downto 0);  -- rx_rmfifodatainserted.data
    rx_runningdisp           : out std_logic_vector(7 downto 0);  -- rx_runningdisp.data
    cal_blk_powerdown        : in  std_logic                     := '0';  -- cal_blk_powerdown.data
    pll_powerdown            : in  std_logic                     := '0';  -- pll_powerdown.data
    gxb_powerdown            : in  std_logic                     := '0';  -- gxb_powerdown.data
    pll_locked               : out std_logic;  -- pll_locked.data
    reconfig_from_xcvr       : out std_logic_vector(16 downto 0);  -- reconfig_from_xcvr.data
    reconfig_to_xcvr         : in  std_logic_vector(3 downto 0)  := (others => '0')  -- reconfig_to_xcvr.data
  );
  end component;

  component ip_stratixiv_phy_xaui_soft is
  port (
    pll_ref_clk          : in  std_logic                     := '0';  -- pll_ref_clk.clk
    xgmii_tx_clk         : in  std_logic                     := '0';  -- xgmii_tx_clk.clk
    xgmii_rx_clk         : out std_logic;  -- xgmii_rx_clk.clk
    xgmii_rx_dc          : out std_logic_vector(71 downto 0);  -- xgmii_rx_dc.data
    xgmii_tx_dc          : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_tx_dc.data
    xaui_rx_serial_data  : in  std_logic_vector(3 downto 0)  := (others => '0');  -- xaui_rx_serial_data.export
    xaui_tx_serial_data  : out std_logic_vector(3 downto 0);  -- xaui_tx_serial_data.export
    rx_ready             : out std_logic;  -- rx_ready.export
    tx_ready             : out std_logic;  -- tx_ready.export
    phy_mgmt_clk         : in  std_logic                     := '0';  -- phy_mgmt_clk.clk
    phy_mgmt_clk_reset   : in  std_logic                     := '0';  -- phy_mgmt_clk_reset.reset
    phy_mgmt_address     : in  std_logic_vector(8 downto 0)  := (others => '0');  -- phy_mgmt.address
    phy_mgmt_read        : in  std_logic                     := '0';  -- .read
    phy_mgmt_readdata    : out std_logic_vector(31 downto 0);  -- .readdata
    phy_mgmt_write       : in  std_logic                     := '0';  -- .write
    phy_mgmt_writedata   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    phy_mgmt_waitrequest : out std_logic;  -- .waitrequest
    rx_digitalreset      : in  std_logic                     := '0';  -- rx_digitalreset.data
    tx_digitalreset      : in  std_logic                     := '0';  -- tx_digitalreset.data
    rx_channelaligned    : out std_logic;  -- rx_channelaligned.data
    rx_syncstatus        : out std_logic_vector(7 downto 0);  -- rx_syncstatus.data
    rx_disperr           : out std_logic_vector(7 downto 0);  -- rx_disperr.data
    rx_errdetect         : out std_logic_vector(7 downto 0);  -- rx_errdetect.data
    cal_blk_powerdown    : in  std_logic                     := '0';  -- cal_blk_powerdown.data
    pll_powerdown        : in  std_logic                     := '0';  -- pll_powerdown.data
    gxb_powerdown        : in  std_logic                     := '0';  -- gxb_powerdown.data
    pll_locked           : out std_logic;  -- pll_locked.data
    reconfig_from_xcvr   : out std_logic_vector(67 downto 0);  -- reconfig_from_xcvr.data
    reconfig_to_xcvr     : in  std_logic_vector(3 downto 0)  := (others => '0')  -- reconfig_to_xcvr.data
  );
  end component;

  component ip_stratixiv_gxb_reconfig_v111 is
  generic (
    g_nof_gx        : natural;
    g_fromgxb_bus_w : natural := 17;
    g_togxb_bus_w   : natural := 4
  );
  port (
    reconfig_clk     : in std_logic;
    reconfig_fromgxb : in std_logic_vector(tech_ceil_div(g_nof_gx, 4) * g_fromgxb_bus_w - 1 downto 0);
    busy             : out std_logic;
    reconfig_togxb   : out std_logic_vector(g_togxb_bus_w - 1 downto 0)
  );
  end component;
end tech_xaui_component_pkg;

package body tech_xaui_component_pkg is
  function tech_xaui_mosi_addr_w(technology : in integer) return integer is
  begin
    case technology is
      when c_tech_stratixiv => return 9;
      when others           => return 9;
    end case;

  end;
end tech_xaui_component_pkg;
