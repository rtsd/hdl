This directory contains scripts and documents for modelling DSP aspects of LOFAR2.0:

Contents:

1) Original LOFAR1 scripts from FilterTaskForce.zip [1]
2) Original LOFAR1 documents
3) Copied from APERTIF [3]
4) LOFAR2.0 PFB in VHDL
5) LOFAR2.0 Python notebooks for quantization and statistics


References:

[1] https://git.astron.nl/rtsd/hdl/-/blob/master/applications/lofar1/FilterTaskForce.zip
    found by Andre Gunst from LOFAR1,for overview see readme_FilterTaskForce.txt
[2] from Andre Gunst
[3] https://git.astron.nl/rtsd/hdl/-/blob/master/apertif_matlab/README.md

[4] https://git.astron.nl/rtsd/hdl/-/blob/master/libraries/dsp/verify_pfb/tb_tb_verify_pfb_wg.vhd



1) Original LOFAR1 scripts from FilterTaskForce.zip [1]

- git/hdl/libraries/dsp/filter/src/hex/Coeffs16384Kaiser-quant.dat :
     Original PFIR filter coefficients from LOFAR 1.0 subband filterbank
     (16 tap, 1024 point FFT), the script that created these PFIR
     coefficients is not available anymore (For Apertif Erko looked in old
     repositories back to 2005 of Lofar software and Station firmware
     but could not find it.)

- pfs_coeff_final.m [1]
     creates Coefficient_16KKaiser.dat, but using e.g. meld shows that
     these do differ slightly within +-40 from
     Coeffs16384Kaiser-quant.dat that are actually used in LOFAR1.



2) Original LOFAR1 documents

- filterbanks2.ppt    [1] : With diagrams of PFB structure
- lowbitfir.pdf, 2001 [2] : Simulation Results of Low Bit FIR Filters for ALMA
- quanreq.pdf, 2002   [2] : Modeling the Effects of Re-quantization for
                            ALMA-FC: Cascading of Degradation Factors


3) Copied from APERTIF [3]

APERTIF uses the same PFB FIR coefficients as LOFAR1.

  FIR_LPF_ApertifCF.m      Script    Calculate FIR filter coefficients for Apertif channel filterbank
                                     using fircls1 (original by Ronald de Wild).
  pfir_coeff.m             Function  Compute polyphase filterbank coefficients (same purpose as
                                     FIR_LPF_ApertifCF.m but with more options)
  pfir_coeff_adjust.m      Function  Compute polyphase filterbank coefficients with optionbal half power
                                     bandwidth adjustment (calls pfir_coeff.m)
  pfir_coeff_dc_adjust.m   Function  Apply DC adjustment to polyphase filterbank coefficients
  run_pfir_coeff.m         Script    Run pfir_coeff_adjust.m and pfir_coeff.m to show filter response in
                                     time and frequency domain, and optionally calls pfir_coeff_dc_adjust.m


4) LOFAR2.0 PFB in VHDL

- tb_tb_verify_pfb_wg.vhd, 2021 [4] : VHDL simulation of LOFAR1 PFB2 (pfb2_unit.vhd) and APERTIF WPFB
                                      (wpfb_unit_dev.vhd) for LOFAR2.0


5) LOFAR2.0 Python notebooks for quantization and statistics

- lofar2_station_sdp_firmware_model.ipynb: SDP FW quantization levels
- signal_statistics.ipynb: SNR improvement for beamformer, correlator and powers

The notebook results are used in [1]. Start Jupyter notebook kernel with:
> jupyter-notebook &
and then navigate to the notebook script in the browser and open it.
The results of the ipynb runs are stored as html files with the same name.

[1] "L4 SDPFW Decision: LOFAR2.0 SDP Firmware Quantization Model", https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L4+SDPFW+Decision%3A+LOFAR2.0+SDP+Firmware+Quantization+Model
