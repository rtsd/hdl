--------------------------------------------------------------------------------
-- Author: Harm Jan Pepping : HJP at astron.nl: April 2012
--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------  --
-- Purpose:  Complex Pipelined Fast Fourier Transform
--
-- Description: The fft_r2_pipe unit performs a complex pipelined FFT on the incoming data stream.
--              The implementation is pipelined which means that at every stage only one
--              multiplier is used to perform all N/2 twiddle multiplications.
--
--              There are two optional features:
--
--              * Reordering: When enabled the output bins of the FFT are re-ordered in
--                            in such a way that the bins represent the frequencies in an
--                            incrementing way.
--
--              * Separation: When enabled the fft_r2_pipe can be used to process two real streams.
--                            The first real stream (A) presented on the real input, the second
--                            real stream (B) presented on the imaginary input.
--                            The separation unit processes outputs the spectrum of A and B in
--                            an alternating way: A(0), B(0), A(1), B(1).... etc
--
--
-- Remarks:
-- . When g_fft.use_separate = TRUE, then the two real inputs are pseudo randomly
--   multiplied by +1 or -1 every block of input samples in fft_switch. At the
--   FFT output this is undone by fft_unswitch. In this way any crosstalk due
--   to quantization noise between the two real inputs gets scrambled and thus
--   averages to zero when integrated over multiple blocks.
--
-- . When g_fft.nof_chan is used the spectrums at the output will be interleaved
--   per spectrum and NOT per sample. So in case g_fft.nof_chan = 1 there will be
--   two multiplexed channels at the input (c0t0 means channel 0, timestamp 0) :
--
--     c0t0 c1t0s c0t1 c1t1 c0t2 c1t2 ... c0t15 c1t15
--
--   At the output will find:
--
--   c0f0 c0f1 c0f2 ... c0f15 c1f0 c1f1 c1f2 ... c1f15  (c0f0 means channel 0, frequency bin 0)
--
-- . Output subband widths:
--   - out_quant_re/im : g_fft.out_dat_w bits (within c_32 word)
--   - out_raw_re/im   : c_raw_dat_w bits (within c_32 word)
--   The out_quant carries subbands of g_fft.out_dat_w bits, so the
--   c_raw_fraction_w fraction bits in c_raw_dat_w have been rounded.
--   The out_raw carries subbands of c_raw_dat_w bits, that still have the
--   c_raw_fraction_w fraction bits. The out_raw subbands are suitable for
--   further weighting or alternative rounding.
--   Note that c_raw_dat_w and c_raw_fraction_w depend on g_fft.use_separate.
--   For real input data FFT there is one fraction bit more than for complex
--   input FFT, due to the adder stage in the separate function.
--   Both out_quant and out_raw use FFT out_val, so they have the same timing.
--

library ieee, common_lib, rTwoSDF_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use work.fft_pkg.all;

entity fft_r2_pipe is
  generic (
    g_instance_index     : natural := 0;  -- used for FFT switch seed
    g_fft                : t_fft := c_fft;  -- generics for the FFT
    g_pipeline           : t_fft_pipeline := c_fft_pipeline;  -- generics for pipelining in each stage, defined in rTwoSDF_lib.rTwoSDFPkg
    g_dont_flip_channels : boolean := false  -- generic to prevent re-ordering of the channels
  );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic := '0';
    in_re          : in  std_logic_vector(g_fft.in_dat_w - 1 downto 0);
    in_im          : in  std_logic_vector(g_fft.in_dat_w - 1 downto 0);
    in_val         : in  std_logic := '1';
    out_quant_re   : out std_logic_vector(g_fft.out_dat_w - 1 downto 0);
    out_quant_im   : out std_logic_vector(g_fft.out_dat_w - 1 downto 0);
    -- use c_32 width as in t_fft_slv_arr, to allow leaving OUT port open, actual width is c_raw_dat_w
    out_raw_re     : out std_logic_vector(c_32 - 1 downto 0);
    out_raw_im     : out std_logic_vector(c_32 - 1 downto 0);
    out_val        : out std_logic
  );
end entity fft_r2_pipe;

architecture str of fft_r2_pipe is
  constant c_pipeline_remove_lsb : natural := 1;  -- to easy timing closure

  constant c_switch_en          : boolean := g_fft.use_separate;  -- default do apply switch/unswitch per real input to mitigate quantization crosstalk
  constant c_switch_sz_w        : natural := ceil_log2(g_fft.nof_points) + g_fft.nof_chan;
  constant c_switch_dat_w       : natural := g_fft.in_dat_w + 1;  -- add 1 extra bit to fit negation of most negative value per real input switch function
  constant c_switch_seed1       : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := fft_switch_new_seed(c_fft_switch_seed1, g_instance_index);
  constant c_switch_seed2       : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := fft_switch_new_seed(c_fft_switch_seed2, g_instance_index);
  constant c_nof_stages         : natural := ceil_log2(g_fft.nof_points);
  constant c_stage_offset       : natural := true_log2(g_fft.wb_factor);  -- Stage offset is required for twiddle generation in wideband fft

  constant c_in_scale_w         : natural := func_fft_in_scale_w(g_fft);
  constant c_raw_fraction_w     : natural := func_fft_raw_fraction_w(g_fft);
  constant c_raw_dat_w          : natural := func_fft_raw_dat_w(g_fft);

  constant c_unswitch_dat_w     : natural := c_raw_dat_w;  -- no need for extra bit, because most negative value cannot occur in FFT output

  -- number the stage instances from c_nof_stages:1
  -- . the data input for the first stage has index c_nof_stages
  -- . the data output of the last stage has index 0
  type t_data_arr is array(c_nof_stages downto 0) of std_logic_vector(g_fft.stage_dat_w - 1 downto 0);

  signal in_dat_re    : std_logic_vector(c_switch_dat_w - 1 downto 0);
  signal in_dat_im    : std_logic_vector(c_switch_dat_w - 1 downto 0);
  signal in_dat_val   : std_logic;

  signal switch_re    : std_logic_vector(c_switch_dat_w - 1 downto 0);
  signal switch_im    : std_logic_vector(c_switch_dat_w - 1 downto 0);
  signal switch_val   : std_logic;

  signal data_re      : t_data_arr;
  signal data_im      : t_data_arr;
  signal data_val     : std_logic_vector(c_nof_stages downto 0) := (others => '0');

  signal in_cplx      : std_logic_vector(c_nof_complex * g_fft.stage_dat_w - 1 downto 0);
  signal out_cplx     : std_logic_vector(c_nof_complex * c_raw_dat_w - 1 downto 0);

  signal fft_out_re   : std_logic_vector(c_raw_dat_w - 1 downto 0);
  signal fft_out_im   : std_logic_vector(c_raw_dat_w - 1 downto 0);
  signal fft_out_val  : std_logic;

  signal raw_re       : std_logic_vector(c_raw_dat_w - 1 downto 0);
  signal raw_im       : std_logic_vector(c_raw_dat_w - 1 downto 0);
  signal raw_val      : std_logic;

  -- debug signals to view parameters in Wave Window
  signal dbg_g_g_fft          : t_fft := g_fft;
  signal dbg_c_in_scale_w     : natural := c_in_scale_w;
  signal dbg_c_raw_fraction_w : natural := c_raw_fraction_w;
  signal dbg_c_raw_dat_w      : natural := c_raw_dat_w;
begin
  ------------------------------------------------------------------------------
  -- Mitigate quantization noise crosstalk between two real inputs by negating
  -- the inputs per lock in a random pattern, when g_fft.use_separate = TRUE.
  ------------------------------------------------------------------------------

  -- Inputs
  in_dat_re  <= RESIZE_SVEC(in_re, c_switch_dat_w);
  in_dat_im  <= RESIZE_SVEC(in_im, c_switch_dat_w);
  in_dat_val <= in_val;

  u_switch : entity work.fft_switch
  generic map (
    g_switch_en => c_switch_en,
    g_seed1     => c_switch_seed1,
    g_seed2     => c_switch_seed2,
    g_fft_sz_w  => c_switch_sz_w,
    g_dat_w     => c_switch_dat_w
  )
  port map (
    in_re      => in_dat_re,
    in_im      => in_dat_im,
    in_val     => in_dat_val,
    out_re     => switch_re,
    out_im     => switch_im,
    out_val    => switch_val,
    clk        => clk,
    rst        => rst
  );

  data_re( c_nof_stages) <= scale_and_resize_svec(switch_re, c_in_scale_w, g_fft.stage_dat_w);
  data_im( c_nof_stages) <= scale_and_resize_svec(switch_im, c_in_scale_w, g_fft.stage_dat_w);
  data_val(c_nof_stages) <= switch_val;

  ------------------------------------------------------------------------------
  -- pipelined FFT stages
  ------------------------------------------------------------------------------
  gen_fft: for stage in c_nof_stages downto 2 generate
    u_stages : entity rTwoSDF_lib.rTwoSDFStage
    generic map (
      g_nof_chan       => g_fft.nof_chan,
      g_stage          => stage,
      g_stage_offset   => c_stage_offset,
      g_twiddle_offset => g_fft.twiddle_offset,
      g_scale_enable   => sel_a_b(stage <= g_fft.guard_w, false, true),
      g_pipeline       => g_pipeline
    )
    port map (
      clk       => clk,
      rst       => rst,
      in_re     => data_re(stage),
      in_im     => data_im(stage),
      in_val    => data_val(stage),
      out_re    => data_re(stage-1),
      out_im    => data_im(stage-1),
      out_val   => data_val(stage-1)
    );
  end generate;

  -- last stage = 1
  u_last_stage : entity rTwoSDF_lib.rTwoSDFStage
  generic map (
    g_nof_chan       => g_fft.nof_chan,
    g_stage          => 1,
    g_stage_offset   => c_stage_offset,
    g_twiddle_offset => g_fft.twiddle_offset,
    g_scale_enable   => sel_a_b(1 <= g_fft.guard_w, false, true),
    g_pipeline       => g_pipeline
  )
  port map (
    clk       => clk,
    rst       => rst,
    in_re     => data_re(1),
    in_im     => data_im(1),
    in_val    => data_val(1),
    out_re    => data_re(0),
    out_im    => data_im(0),
    out_val   => data_val(0)
  );

  ------------------------------------------------------------------------------
  -- Optional output reorder and separation
  ------------------------------------------------------------------------------
  gen_reorder_and_separate : if g_fft.use_separate or g_fft.use_reorder generate
    in_cplx <= data_im(0) & data_re(0);

    u_reorder_sep : entity work.fft_reorder_sepa_pipe
    generic map (
      g_bit_flip    => g_fft.use_reorder,
      g_fft_shift   => g_fft.use_fft_shift,
      g_separate    => g_fft.use_separate,
      g_dont_flip_channels => g_dont_flip_channels,
      g_nof_points  => g_fft.nof_points,
      g_nof_chan    => g_fft.nof_chan
    )
    port map (
      clk     => clk,
      rst     => rst,
      in_dat  => in_cplx,  -- c_nof_complex * g_fft.stage_dat_w
      in_val  => data_val(0),
      out_dat => out_cplx,  -- c_nof_complex * c_raw_dat_w
      out_val => fft_out_val
    );

    -- c_raw_dat_w = g_fft.stage_dat_w     when g_fft.use_separate = false
    -- c_raw_dat_w = g_fft.stage_dat_w + 1 when g_fft.use_separate = true
    fft_out_re <= out_cplx(  c_raw_dat_w - 1 downto 0);
    fft_out_im <= out_cplx(2 * c_raw_dat_w - 1 downto c_raw_dat_w);
  end generate;

  no_reorder_no_seperate : if g_fft.use_separate = false and g_fft.use_reorder = false generate
    -- c_raw_dat_w = g_fft.stage_dat_w because g_fft.use_separate = false
    fft_out_re  <= data_re(0);
    fft_out_im  <= data_im(0);
    fft_out_val <= data_val(0);
  end generate;

  ------------------------------------------------------------------------------
  -- Undo input random negation of u_switch at FFT output when c_switch_en =
  -- g_fft.use_separate = TRUE
  ------------------------------------------------------------------------------
  u_unswitch : entity work.fft_unswitch
  generic map (
    g_switch_en => c_switch_en,
    g_seed1     => c_switch_seed1,
    g_seed2     => c_switch_seed2,
    g_fft_sz_w  => c_switch_sz_w,
    g_dat_w     => c_unswitch_dat_w
  )
  port map (
    in_re      => fft_out_re,
    in_im      => fft_out_im,
    in_val     => fft_out_val,
    out_re     => raw_re,
    out_im     => raw_im,
    out_val    => raw_val,
    clk        => clk,
    rst        => rst
  );

  ------------------------------------------------------------------------------
  -- Pipelined FFT output requantization
  ------------------------------------------------------------------------------
  u_requantize_re : entity common_lib.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_raw_fraction_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_pipeline_remove_lsb => c_pipeline_remove_lsb,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_raw_dat_w,
    g_out_dat_w           => g_fft.out_dat_w
  )
  port map (
    clk        => clk,
    in_dat     => raw_re,
    out_dat    => out_quant_re,
    out_ovr    => open
  );

  u_requantize_im : entity common_lib.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_raw_fraction_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_pipeline_remove_lsb => c_pipeline_remove_lsb,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_raw_dat_w,
    g_out_dat_w           => g_fft.out_dat_w
  )
  port map (
    clk        => clk,
    in_dat     => raw_im,
    out_dat    => out_quant_im,
    out_ovr    => open
  );

  u_quant_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => c_pipeline_remove_lsb
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => raw_val,
    out_dat => out_val
  );

  ------------------------------------------------------------------------------
  -- Pipelined FFT raw output register
  ------------------------------------------------------------------------------

  -- . Pipeline out_raw to align with out_quant, so they can share
  --   out_val.
  u_out_raw_re : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_remove_lsb,
    g_in_dat_w       => c_raw_dat_w,
    g_out_dat_w      => c_32
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => raw_re,
    out_dat => out_raw_re
  );

  u_out_raw_im : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_remove_lsb,
    g_in_dat_w       => c_raw_dat_w,
    g_out_dat_w      => c_32
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => raw_im,
    out_dat => out_raw_im
  );
end str;
