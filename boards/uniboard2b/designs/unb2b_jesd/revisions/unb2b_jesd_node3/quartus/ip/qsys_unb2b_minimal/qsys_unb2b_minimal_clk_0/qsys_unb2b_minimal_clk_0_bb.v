module qsys_unb2b_minimal_clk_0 (
		output wire  clk_out,     //          clk.clk
		input  wire  in_clk,      //       clk_in.clk
		input  wire  reset_n,     // clk_in_reset.reset_n
		output wire  reset_n_out  //    clk_reset.reset_n
	);
endmodule

