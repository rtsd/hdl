-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_throttle is
  generic (
    g_dc_period      : natural := 100;  -- provides a resolution of 1% (1/100..100/100)
    g_throttle_valid : boolean := false  -- FALSE: Stream passes through, snk_out.ready is AND'ed with pulse
                                        -- TRUE : Throttles src_out.valid instead of snk_out.ready; Onther entity I/O unused.
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    -- ST sinks
    snk_out  : out t_dp_siso;
    snk_in   : in  t_dp_sosi := c_dp_sosi_rst;
    -- ST source
    src_in   : in  t_dp_siso := c_dp_siso_rst;
    src_out  : out t_dp_sosi;

    throttle : in std_logic_vector(ceil_log2(g_dc_period + 1) - 1 downto 0)
  );
end dp_throttle;

architecture str of dp_throttle is
  signal dc_out : std_logic;
begin
  gen_throttle_ready : if g_throttle_valid = false generate
    -- Simply AND the ready signal with out duty cycle controlled signal to provide a throttle
    snk_out.ready <= src_in.ready and dc_out;
    snk_out.xon   <= src_in.xon;
    src_out <= snk_in;
  end generate;

  gen_throttle_valid : if g_throttle_valid = true generate
    -- Connect ducty cycle controlled signal directly to src_out.valid
    src_out.valid <= dc_out;
  end generate;

  u_common_duty_cycle : entity common_lib.common_duty_cycle
  generic map (
    g_rst_lvl => '0',  -- Start with '0' on the output so our connected sink is not maxed out after reset
    g_dis_lvl => '0',  -- Don't care - dc_out_en is not used.
    g_act_lvl => '1',
    g_per_cnt => g_dc_period,
    g_act_cnt => 0  -- After init, stay in idle state until we write a new DC value
  )
  port map (
    rst         => rst,
    clk         => clk,

    dc_act_cnt  => throttle,

    dc_out_en   => '1',  -- We can also disable the output by writing zero to dc_act_cnt.
    dc_out      => dc_out
   );
end str;
