-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Purpose: Verify multiple variations of tb_dp_sync_insert
-- Description:
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_dp_sync_insert is
end tb_tb_dp_sync_insert;

architecture tb of tb_tb_dp_sync_insert is
  constant c_nof_repeat            : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    g_block_size_input       : NATURAL := 16;
--    g_nof_blk_per_sync_input : NATURAL := 32;
--    g_gap_size_during_block  : NATURAL := 0;
--    g_gap_size_between_block : NATURAL := 0;
--    g_nof_data_per_block     : NATURAL := 8;
--    g_nof_blk_per_sync       : NATURAL := 4;
--    g_nof_repeat             : NATURAL := 14

  u_no_gaps    : entity work.tb_dp_sync_insert generic map(16, 32, 0, 0, 8, 4, 14);
  u_no_gap     : entity work.tb_dp_sync_insert generic map(16, 32, 1, 3, 8, 4, 14);
end tb;
