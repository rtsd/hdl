-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_unb1_board_node_ctrl is
end tb_unb1_board_node_ctrl;

architecture tb of tb_unb1_board_node_ctrl is
  constant c_scale             : natural := 100;  -- scale to speed up simulation

  constant c_xo_clk_period     : time := 1 us;  -- 1 MHz XO, slow XO to speed up simulation
  constant c_sys_clk_period    : time := c_xo_clk_period / 5;  -- 5 MHz PLL output from XO reference
  constant c_sys_locked_time   : time := 10 us;

  constant c_pulse_us          : natural := 5;  -- nof 5 MHz clk cycles to get us period
  constant c_pulse_ms          : natural := 1000 / c_scale;  -- nof pulse_us pulses to get ms period
  constant c_pulse_s           : natural := 1000;  -- nof pulse_ms pulses to get  s period

  constant c_wdi_extend_w      : natural := 14;  -- extend wdi by about 2**(14-1)= 8 s (as defined by c_pulse_ms)
  constant c_wdi_period        : time :=  1000 ms;  -- wdi toggle after c_wdi_period

  -- Use c_sw_period=40000 ms to show that the c_wdi_extend_w=5 is not enough, the WD will kick in when the sw is off during reload
  constant c_sw_period         : time := 40000 ms;  -- sw active for c_sw_period then inactive during reload for c_sw_period, etc.
  -- Use c_sw_period=10000 ms to show that the c_wdi_extend_w=5 is enough, the WD will not kick in when the sw is off during reload
  --CONSTANT c_sw_period         : TIME := 6000 ms;  -- sw active for c_sw_period then inactive during reload for c_sw_period, etc.

  signal xo_clk      : std_logic := '0';
  signal xo_rst_n    : std_logic;

  signal sys_clk     : std_logic := '0';
  signal sys_locked  : std_logic := '0';
  signal sys_rst     : std_logic;

  signal wdi         : std_logic := '0';
  signal wdi_in      : std_logic;
  signal wdi_out     : std_logic;

  signal sw          : std_logic := '0';

  signal pulse_us    : std_logic;
  signal pulse_ms    : std_logic;
  signal pulse_s     : std_logic;
begin
  -- run 2000 ms

  xo_clk <= not xo_clk after c_xo_clk_period / 2;

  sys_clk <= not sys_clk after c_sys_clk_period / 2;
  sys_locked <= '0', '1' after c_sys_locked_time;

  wdi    <= not wdi after c_wdi_period / c_scale;  -- wd interrupt
  sw     <= not sw  after c_sw_period / c_scale;  -- sw active / reload

  wdi_in <= wdi and sw;  -- sw wdi only when sw is active, during sw inactive the wdi_out will be extended

  dut : entity work.unb1_board_node_ctrl
  generic map (
    g_pulse_us     => c_pulse_us,
    g_pulse_ms     => c_pulse_ms,
    g_pulse_s      => c_pulse_s,
    g_wdi_extend_w => c_wdi_extend_w
  )
  port map (
    xo_clk      => xo_clk,
    xo_rst_n    => xo_rst_n,
    sys_clk     => sys_clk,
    sys_locked  => sys_locked,
    sys_rst     => sys_rst,
    wdi_in      => wdi_in,
    wdi_out     => wdi_out,
    pulse_us    => pulse_us,
    pulse_ms    => pulse_ms,
    pulse_s     => pulse_s
  );
end tb;
