>>> On 17/11/2015 at 02:47, in message
<40E3C1B05457DB43B21A4F4C5F191A392F8E71EA@exmbx03-cdc.nexus.csiro.au>,
<John.Bunton@csiro.au> wrote:
> Hi Marco and Wallace,
>
> I know you see the reconstruction of the polyphase filterbank data to a
> higher sample rate at somewhat unknown and risky.
> Attached are Matlab files to demonstrate a reconstruction.
>
> To use them
> First generate a prototype filter by executing Gen_filter12.m
>                 This will also plot up the impulse response of the filter,
> the response of two adjacent channel  and the response to a monochromatic
> signal.
>
> Next run reconstruct.m  with the following settings
> ImpulseResponse = 1;
> Correct = 1;
>  this will calculate an impulse response and a correction filter for the
> transfer function error
>
> Finally run reconstruct.m with
> ImpulseResponse = 0;
> Correct = 1;
>                 With the settings in the attached files
> Figure 1 shows the input (blue) and reconstructed signal (red), first 10,000
> points
> Figure 2 shows the difference between the reconstructed and input
> Figure 2 show the spectrum on the input and the difference.
>
> Input is a sinewave, line 30 if you want to play with the frequency
> Comment out lines 29 and 30 to generate a random signal.
>
> Things to change are the prototype filter line 1 of Gen_filter12 and
> Oversampling ratio line 40 of reconstruct.m , note variable k at line 62
> must be set correctly.
>
> There is a trade-off to be made.  With a sufficiently long FIR on the
> analysis and synthesis filterbank or sufficiently high oversampling ration we
> do not need the transfer function correction filter.   However there are 1024
> filterbanks compared to the required 32 correction filters.  So I expect to
> have a correction filter and for the filterbanks to be similar to what is in
> the attached files.
>
> Regards,
> John
>
> John Bunton PhD BE BSc
> Project Engineer, SKA LOW Correlator and Beamformer
> Astronomy and Space Science
> CSIRO
> E John.bunton@csiro.au T +61 1 9372 4420 M 04 2907 8320
> PO Box 76 Epping, 1710, NSW, Australia
> www.csiro.au | www.atnf.csiro.au/projects/askap
