-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
-- . SDP beamlet data output (BDO) destinations register
-- Description:
--              _________
--             |mm_fields|
--             |         |
-- reg_copi -->|  slv_out|------- _wr ----
-- reg_cipo <--|         |               |
--             |         |               v
--             |   slv_in|<--+--- _rd <--+<---- _ro
--             |_________|   |
--                           -----------------> output value
--
-- where:
-- . _ro = actual nof_destinations, actual nof_blocks_per_packet
-- . output value = tbuf_registers
--
-- References:
-- 1 https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
-- 2 https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Transient+buffer+raw+data
--

library IEEE, common_lib, mm_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_field_pkg.all;
  use work.sdp_pkg.all;
  use work.sdp_tbuf_pkg.all;

entity sdp_tbuf_registers is
  port (
    -- Clocks and reset
    mm_clk   : in  std_logic;
    mm_rst   : in  std_logic;

    dp_clk   : in  std_logic;
    dp_rst   : in  std_logic;

    reg_copi : in  t_mem_copi;
    reg_cipo : out t_mem_cipo;

    -- read only (RO) values
    tbuf_registers_ro : in t_sdp_tbuf_registers;

    -- write and RO values
    tbuf_registers    : out t_sdp_tbuf_registers
  );
end sdp_tbuf_registers;

architecture str of sdp_tbuf_registers is
  constant c_field_arr : t_common_field_arr(c_sdp_tbuf_registers_nof_hdr_fields - 1 downto 0) :=
                         c_sdp_tbuf_registers_field_arr;

  signal mm_fields_in  : std_logic_vector(field_slv_in_len(c_field_arr) - 1 downto 0);
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);

  signal tbuf_registers_rd : t_sdp_tbuf_registers := c_sdp_tbuf_registers_rst;
  signal tbuf_registers_wr : t_sdp_tbuf_registers := c_sdp_tbuf_registers_rst;
begin
  tbuf_registers <= tbuf_registers_rd;

  p_tbuf_registers_rd : process(tbuf_registers_wr, tbuf_registers_ro)
  begin
    -- default read what was written for read/write (RW) fields
    tbuf_registers_rd <= tbuf_registers_wr;

    -- overrule read only (RO) fields
    tbuf_registers_rd.nof_samples_per_block <= tbuf_registers_ro.nof_samples_per_block;
    tbuf_registers_rd.nof_pages_in_buffer   <= tbuf_registers_ro.nof_pages_in_buffer;
    tbuf_registers_rd.recorded_nof_pages    <= tbuf_registers_ro.recorded_nof_pages;
    tbuf_registers_rd.recorded_first_page   <= tbuf_registers_ro.recorded_first_page;
    tbuf_registers_rd.recorded_last_page    <= tbuf_registers_ro.recorded_last_page;
    tbuf_registers_rd.recorded_nof_samples  <= tbuf_registers_ro.recorded_nof_samples;
    tbuf_registers_rd.recorded_first_rsn    <= tbuf_registers_ro.recorded_first_rsn;
    tbuf_registers_rd.recorded_last_rsn     <= tbuf_registers_ro.recorded_last_rsn;
    tbuf_registers_rd.dump_done             <= tbuf_registers_ro.dump_done;
  end process;

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_cross_clock_domain => true,
    g_use_slv_in_val     => false,  -- use false to save logic when always slv_in_val='1'
    g_field_arr          => c_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_copi,
    mm_miso    => reg_cipo,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_in,
    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  -- add "RO" fields to mm_fields
  mm_fields_in(field_hi(c_field_arr, "dump_done") downto
               field_lo(c_field_arr, "dump_done")) <= slv(tbuf_registers_rd.dump_done);  -- 1
  mm_fields_in(field_hi(c_field_arr, "recorded_last_rsn") downto
               field_lo(c_field_arr, "recorded_last_rsn")) <= tbuf_registers_rd.recorded_last_rsn;  -- 64
  mm_fields_in(field_hi(c_field_arr, "recorded_first_rsn") downto
               field_lo(c_field_arr, "recorded_first_rsn")) <= tbuf_registers_rd.recorded_first_rsn;  -- 64
  mm_fields_in(field_hi(c_field_arr, "recorded_nof_samples") downto
               field_lo(c_field_arr, "recorded_nof_samples")) <= to_uvec(tbuf_registers_rd.recorded_nof_samples, 32);
  mm_fields_in(field_hi(c_field_arr, "recorded_last_page") downto
               field_lo(c_field_arr, "recorded_last_page")) <= to_uvec(tbuf_registers_rd.recorded_last_page, 32);
  mm_fields_in(field_hi(c_field_arr, "recorded_first_page") downto
               field_lo(c_field_arr, "recorded_first_page")) <= to_uvec(tbuf_registers_rd.recorded_first_page, 32);
  mm_fields_in(field_hi(c_field_arr, "recorded_nof_pages") downto
               field_lo(c_field_arr, "recorded_nof_pages")) <= to_uvec(tbuf_registers_rd.recorded_nof_pages, 32);
  mm_fields_in(field_hi(c_field_arr, "nof_pages_in_buffer") downto
               field_lo(c_field_arr, "nof_pages_in_buffer")) <= to_uvec(tbuf_registers_rd.nof_pages_in_buffer, 32);
  mm_fields_in(field_hi(c_field_arr, "nof_samples_per_block") downto
               field_lo(c_field_arr, "nof_samples_per_block")) <= to_uvec(tbuf_registers_rd.nof_samples_per_block, 32);

  -- get "RW" fields from mm_fields
  tbuf_registers_wr.dump_enables         <=         mm_fields_out(field_hi(c_field_arr, "dump_enables") downto
                                                                  field_lo(c_field_arr, "dump_enables"));
  tbuf_registers_wr.dump_start_rsn       <=         mm_fields_out(field_hi(c_field_arr, "dump_start_rsn") downto
                                                                  field_lo(c_field_arr, "dump_start_rsn"));
  tbuf_registers_wr.dump_nof_pages       <= to_uint(mm_fields_out(field_hi(c_field_arr, "dump_nof_pages") downto
                                                                  field_lo(c_field_arr, "dump_nof_pages")));
  tbuf_registers_wr.dump_start_page      <= to_uint(mm_fields_out(field_hi(c_field_arr, "dump_start_page") downto
                                                                  field_lo(c_field_arr, "dump_start_page")));
  tbuf_registers_wr.dump_interpacket_gap <= to_uint(mm_fields_out(field_hi(c_field_arr, "dump_interpacket_gap") downto
                                                                  field_lo(c_field_arr, "dump_interpacket_gap")));
  tbuf_registers_wr.record_enable        <=      sl(mm_fields_out(field_hi(c_field_arr, "record_enable") downto
                                                                  field_lo(c_field_arr, "record_enable")));
  tbuf_registers_wr.record_all           <=      sl(mm_fields_out(field_hi(c_field_arr, "record_all") downto
                                                                  field_lo(c_field_arr, "record_all")));
end str;
