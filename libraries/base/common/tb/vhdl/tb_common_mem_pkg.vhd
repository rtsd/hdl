-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

package tb_common_mem_pkg is
  ------------------------------------------------------------------------------
  -- MM bus access functions
  ------------------------------------------------------------------------------

  -- The mm_miso input needs to be declared as signal, because otherwise the
  -- procedure does not notice a change (also not when the mm_clk is declared
  -- as signal).

  -- Write data to the MM bus
  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;  -- [31:0]
                               constant wr_data : in  integer;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  integer;  -- [31:0]
                               signal   wr_data : in  std_logic_vector;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;  -- [31:0]
                               constant wr_data : in  integer;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi);

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;  -- [31:0]
                               constant wr_data : in  std_logic_vector;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi);

  -- Read data request to the MM bus
  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi);

  -- Wait for read data valid after read latency mm_clk cycles
  procedure proc_mem_mm_bus_rd_latency(constant c_rd_latency : in natural;
                                       signal   mm_clk       : in std_logic);

  -- Write array of data words to the memory
  procedure proc_mem_write_ram(constant offset   : in  natural;
                               constant nof_data : in  natural;
                               constant data_arr : in  t_slv_32_arr;
                               signal   mm_clk   : in  std_logic;
                               signal   mm_mosi  : out t_mem_mosi);

  procedure proc_mem_write_ram(constant data_arr : in  t_slv_32_arr;
                               signal   mm_clk   : in  std_logic;
                               signal   mm_mosi  : out t_mem_mosi);

  -- Read array of data words from the memory
  procedure proc_mem_read_ram(constant offset   : in  natural;
                              constant nof_data : in  natural;
                              signal   mm_clk   : in  std_logic;
                              signal   mm_mosi  : out t_mem_mosi;
                              signal   mm_miso  : in  t_mem_miso;
                              signal   data_arr : out t_slv_32_arr);

  procedure proc_mem_read_ram(signal   mm_clk   : in  std_logic;
                              signal   mm_mosi  : out t_mem_mosi;
                              signal   mm_miso  : in  t_mem_miso;
                              signal   data_arr : out t_slv_32_arr);
end tb_common_mem_pkg;

package body tb_common_mem_pkg is
  ------------------------------------------------------------------------------
  -- Private functions
  ------------------------------------------------------------------------------

  -- Issues a rd or a wr MM access
  procedure proc_mm_access(signal mm_clk    : in  std_logic;
                           signal mm_access : out std_logic) is
  begin
    mm_access <= '1';
    wait until rising_edge(mm_clk);
    mm_access <= '0';
  end proc_mm_access;

  -- Issues a rd or a wr MM access and wait for it to have finished
  procedure proc_mm_access(signal mm_clk     : in  std_logic;
                           signal mm_waitreq : in  std_logic;
                           signal mm_access  : out std_logic) is
  begin
    mm_access <= '1';
    wait until rising_edge(mm_clk);
    while mm_waitreq = '1' loop
      wait until rising_edge(mm_clk);
    end loop;
    mm_access <= '0';
  end proc_mm_access;

  ------------------------------------------------------------------------------
  -- Public functions
  ------------------------------------------------------------------------------

  -- Write data to the MM bus
  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;
                               constant wr_data : in  integer;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= TO_MEM_DATA(wr_data);
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.wr);
  end proc_mem_mm_bus_wr;

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  integer;
                               signal   wr_data : in  std_logic_vector;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= RESIZE_MEM_DATA(wr_data);
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.wr);
  end proc_mem_mm_bus_wr;

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;
                               constant wr_data : in  integer;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= TO_MEM_DATA(wr_data);
    proc_mm_access(mm_clk, mm_mosi.wr);
  end proc_mem_mm_bus_wr;

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;
                               constant wr_data : in  std_logic_vector;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= RESIZE_UVEC(wr_data, c_mem_data_w);
    proc_mm_access(mm_clk, mm_mosi.wr);
  end proc_mem_mm_bus_wr;

  -- Read data request to the MM bus
  -- Use proc_mem_mm_bus_rd_latency() to wait for the MM MISO rd_data signal
  -- to show the data after some read latency
  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(rd_addr);
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.rd);
  end proc_mem_mm_bus_rd;

  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(rd_addr);
    proc_mm_access(mm_clk, mm_mosi.rd);
  end proc_mem_mm_bus_rd;

  -- Wait for read data valid after read latency mm_clk cycles
  -- Directly assign mm_miso.rddata to capture the read data
  procedure proc_mem_mm_bus_rd_latency(constant c_rd_latency : in natural;
                                       signal   mm_clk       : in std_logic) is
  begin
    for I in 0 to c_rd_latency - 1 loop wait until rising_edge(mm_clk); end loop;
  end proc_mem_mm_bus_rd_latency;

  -- Write array of data words to the memory
  procedure proc_mem_write_ram(constant offset   : in  natural;
                               constant nof_data : in  natural;
                               constant data_arr : in  t_slv_32_arr;
                               signal   mm_clk   : in  std_logic;
                               signal   mm_mosi  : out t_mem_mosi) is
    constant c_data_arr : t_slv_32_arr(data_arr'length - 1 downto 0) := data_arr;  -- map to fixed range [h:0]
  begin
    for I in 0 to nof_data - 1 loop
      proc_mem_mm_bus_wr(offset + I, c_data_arr(I), mm_clk, mm_mosi);
    end loop;
  end proc_mem_write_ram;

  procedure proc_mem_write_ram(constant data_arr : in  t_slv_32_arr;
                               signal   mm_clk   : in  std_logic;
                               signal   mm_mosi  : out t_mem_mosi) is
    constant c_offset   : natural := 0;
    constant c_nof_data : natural := data_arr'length;
  begin
    proc_mem_write_ram(c_offset, c_nof_data, data_arr, mm_clk, mm_mosi);
  end proc_mem_write_ram;

  -- Read array of data words from the memory
  procedure proc_mem_read_ram(constant offset   : in  natural;
                              constant nof_data : in  natural;
                              signal   mm_clk   : in  std_logic;
                              signal   mm_mosi  : out t_mem_mosi;
                              signal   mm_miso  : in  t_mem_miso;
                              signal   data_arr : out t_slv_32_arr) is
  begin
    for I in 0 to nof_data - 1 loop
      proc_mem_mm_bus_rd(offset + I, mm_clk, mm_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);  -- assume read latency is 1
      data_arr(I) <= mm_miso.rddata(31 downto 0);
    end loop;
    -- wait one mm_clk cycle more to have last rddata captured in signal data_arr (otherwise this proc would need to use variable data_arr)
    wait until rising_edge(mm_clk);
  end proc_mem_read_ram;

  procedure proc_mem_read_ram(signal   mm_clk   : in  std_logic;
                              signal   mm_mosi  : out t_mem_mosi;
                              signal   mm_miso  : in  t_mem_miso;
                              signal   data_arr : out t_slv_32_arr) is
    constant c_offset   : natural := 0;
    constant c_nof_data : natural := data_arr'length;
  begin
    proc_mem_read_ram(c_offset, c_nof_data, mm_clk, mm_mosi, mm_miso, data_arr);
  end proc_mem_read_ram;
end tb_common_mem_pkg;
