--------------------------------------------------------------------------------
-- Author: Harm Jan Pepping : HJP at astron.nl: April 2012
--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Perform the separate function to support two real inputs
--
-- Description: It composes an output stream where the bins for input A and B are
--              interleaved in the stream: A, B, A, B etc (for both real and imaginary part)
--
--              It is assumed that the incoming data is as follows for a 1024 point FFT:
--
--              X(0), X(1024), X(1), X(1023), X(2), X(1022), etc...
--                       |
--                       |
--                    This value is X(0)!!!
--
--              The function that is performed is based on the following equation:
--
--              A.real(m) = (X.real(N-m) + X.real(m))/2
--              A.imag(m) = (X.imag(m)   - X.imag(N-m))/2
--              B.real(m) = (X.imag(m)   + X.imag(N-m))/2
--              B.imag(m) = (X.real(N-m) - X.real(m))/2
--
-- Remarks:
-- . The A, B outputs are scaled by factor 2 due to separate add and sub.
--   Therefore in_dat re, im have c_in_data_w bits and out_dat re, im have
--   c_out_data_w = c_in_data_w + 1 bits, to avoid overflow.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity fft_sepa is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    in_dat  : in  std_logic_vector;  -- c_nof_complex * c_in_data_w
    in_val  : in  std_logic;
    out_dat : out std_logic_vector;  -- c_nof_complex * c_out_data_w = c_nof_complex * (c_in_data_w + 1)
    out_val : out std_logic
  );
end entity fft_sepa;

architecture rtl of fft_sepa is
  constant c_in_data_w     : natural := in_dat'length / c_nof_complex;
  constant c_in_complex_w  : natural := c_nof_complex * c_in_data_w;
  constant c_out_data_w    : natural := c_in_data_w  + 1;
  constant c_out_complex_w : natural := c_nof_complex * c_out_data_w;
  constant c_pipeline      : natural := 3;

  type t_reg is record
    switch    : std_logic;  -- Register used to toggle between A & B definitionn
    val_dly   : std_logic_vector(c_pipeline-1 downto 0);  -- Register that delays the incoming valid signal
    xn_m_reg  : std_logic_vector(c_in_complex_w - 1 downto 0);  -- Register to hold the X(N-m) value for one cycle
    xm_reg    : std_logic_vector(c_in_complex_w - 1 downto 0);  -- Register to hold the X(m) value for one cycle
    add_reg_a : std_logic_vector(c_in_data_w - 1 downto 0);  -- Input register A for the adder
    add_reg_b : std_logic_vector(c_in_data_w - 1 downto 0);  -- Input register B for the adder
    sub_reg_a : std_logic_vector(c_in_data_w - 1 downto 0);  -- Input register A for the subtractor
    sub_reg_b : std_logic_vector(c_in_data_w - 1 downto 0);  -- Input register B for the subtractor
    out_dat   : std_logic_vector(c_out_complex_w - 1 downto 0);  -- Registered output value
    out_val   : std_logic;  -- Registered data valid signal
  end record;

  constant c_reg_init : t_reg := ('0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), '0');

  signal r          : t_reg := c_reg_init;
  signal rin        : t_reg;
  signal sub_result : std_logic_vector(c_out_data_w - 1 downto 0);  -- Result of the subtractor
  signal add_result : std_logic_vector(c_out_data_w - 1 downto 0);  -- Result of the adder
begin
  ---------------------------------------------------------------
  -- ADDER AND SUBTRACTOR
  ---------------------------------------------------------------
  adder : entity common_lib.common_add_sub
  generic map (
    g_direction       => "ADD",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,
    g_pipeline_output => 1,
    g_in_dat_w        => c_in_data_w,
    g_out_dat_w       => c_out_data_w  -- = c_in_data_w + 1
  )
  port map (
    clk     => clk,
    in_a    => r.add_reg_a,
    in_b    => r.add_reg_b,
    result  => add_result
  );

  subtractor : entity common_lib.common_add_sub
  generic map (
    g_direction       => "SUB",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,
    g_pipeline_output => 1,
    g_in_dat_w        => c_in_data_w,
    g_out_dat_w       => c_out_data_w  -- = c_in_data_w + 1
  )
  port map (
    clk     => clk,
    in_a    => r.sub_reg_a,
    in_b    => r.sub_reg_b,
    result  => sub_result
  );

  ---------------------------------------------------------------
  -- CONTROL PROCESS
  ---------------------------------------------------------------
  comb : process(r, rst, in_val, in_dat, add_result, sub_result)
    variable v : t_reg;
  begin
    v := r;

    -- Shift register for the valid signal
    v.val_dly(c_pipeline-1 downto 1) := v.val_dly(c_pipeline-2 downto 0);
    v.val_dly(0) := in_val;

    -- Composition of the output registers:
    v.out_dat := sub_result & add_result;
    v.out_val := r.val_dly(c_pipeline-1);

    -- Compose the inputs for the adder and subtractor
    -- for both A and B
    if in_val = '1' or r.val_dly(0) = '1' then
      if r.switch = '0' then
        v.xm_reg    := in_dat;
        v.add_reg_a := r.xm_reg(c_in_complex_w - 1   downto c_in_data_w);  -- Xm   imag
        v.add_reg_b := r.xn_m_reg(c_in_complex_w - 1 downto c_in_data_w);  -- Xn-m imag
        v.sub_reg_a := r.xn_m_reg(c_in_data_w - 1    downto 0);  -- Xn-m real
        v.sub_reg_b := r.xm_reg(c_in_data_w - 1      downto 0);  -- Xm   real
      else
        v.xn_m_reg  := in_dat;
        v.add_reg_a := r.xm_reg(c_in_data_w - 1    downto 0);  -- Xm   real
        v.add_reg_b := in_dat(c_in_data_w - 1      downto 0);  -- Xn-m real
        v.sub_reg_a := r.xm_reg(c_in_complex_w - 1 downto c_in_data_w);  -- Xm   imag
        v.sub_reg_b := in_dat(c_in_complex_w - 1   downto c_in_data_w);  -- Xn-m imag
      end if;
    end if;

    if in_val = '1' then
      v.switch := not r.switch;
    end if;

    if rst = '1' then
      -- Only need to reset the control signals
      v.switch    := '0';
      v.val_dly   := (others => '0');
      v.out_val   := '0';
    end if;

    rin <= v;
  end process comb;

  regs : process(clk)
  begin
    if rising_edge(clk) then
      r <= rin;
    end if;
  end process;

  ---------------------------------------------------------------
  -- OUTPUT STAGE
  ---------------------------------------------------------------
  out_dat <= r.out_dat;
  out_val <= r.out_val;
end rtl;
