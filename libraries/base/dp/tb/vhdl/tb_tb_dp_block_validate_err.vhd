-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Verify multiple variations of tb_dp_block_validate_err
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_block_validate_err is
end tb_tb_dp_block_validate_err;

architecture tb of tb_tb_dp_block_validate_err is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_blk_per_sync   : natural := 5;
  constant c_data_per_blk   : natural := 9;
  constant c_max_block_size : natural := 9;
  constant c_nof_err_counts : natural := 5;
begin
--    g_nof_blocks_per_sync  : NATURAL := 5;
--    g_nof_data_per_blk     : NATURAL := 9;
--    g_max_block_size       : NATURAL := 9;
--    g_nof_err_counts       : NATURAL := 8;
--    g_gap_size             : NATURAL := 4;
--    g_cnt_w                : NATURAL := 3

  u_normal      : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 4,   3);
  u_clip        : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 4,   3);
  u_small_cnt_w : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 4,   1);
  u_large_cnt_w : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 4,   30);
  u_small_gap   : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 1,   16);
  u_large_gap   : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, c_nof_err_counts, 100, 16);
  u_low_nof_cnt : entity work.tb_dp_block_validate_err generic map(c_blk_per_sync, c_data_per_blk, c_max_block_size, 1,                1,   16);
end tb;
