--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Reuse from LOFAR rad_repack.vhd and rad_repack(rtl).vhd

-- Purpose:
--   Repack g_in_nof_words of width g_in_dat_w into g_out_nof_words of width
--   g_out_dat_w.
-- Description:
--   The dp_repack_legacy works both as packer and as unpacker.
--   If g_in_nof_words=g_out_nof_words then in_dat is simply passed on directly
--   to out_dat via wires.
--   The nof input words indicated by in_sof and in_eof must be a multiple of
--   g_in_nof_words, i.e. padding is not supported.
--   The in_val active duty-cycle must be <= g_in_nof_words/g_out_nof_words.

entity dp_repack_legacy is
  generic (
    g_in_dat_w        : natural;
    g_in_nof_words    : natural;
    g_out_dat_w       : natural;
    g_out_nof_words   : natural;
    g_ls_to_ms        : boolean := true  -- Read input SLV from LS to MS word by default
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    in_dat           : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val           : in  std_logic := '1';
    in_sof           : in  std_logic := '0';
    in_eof           : in  std_logic := '0';
    in_sync          : in  std_logic := '0';  -- DP style sync at sof

    out_dat          : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val          : out std_logic;
    out_sof          : out std_logic;
    out_eof          : out std_logic;
    sof_sync         : out std_logic  -- DP style sync at sof, passes on in_sync
  );
end dp_repack_legacy;

architecture rtl of dp_repack_legacy is
  constant c_buf_dat_w      : natural := g_in_dat_w * g_in_nof_words;

  signal in_dat_vec         : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal nxt_in_dat_vec     : std_logic_vector(in_dat_vec'range);
  signal in_val_vec         : std_logic_vector(g_in_nof_words - 1 downto 0);
  signal nxt_in_val_vec     : std_logic_vector(in_val_vec'range);

  signal buf_dat_vec        : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal nxt_buf_dat_vec    : std_logic_vector(buf_dat_vec'range);
  signal buf_val            : std_logic;
  signal nxt_buf_val        : std_logic;

  signal out_dat_vec        : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal nxt_out_dat_vec    : std_logic_vector(out_dat_vec'range);
  signal out_val_vec        : std_logic_vector(g_out_nof_words - 1 downto 0);
  signal nxt_out_val_vec    : std_logic_vector(out_val_vec'range);
  signal out_eof_vec        : std_logic_vector(g_out_nof_words - 1 downto 0);
  signal nxt_out_eof_vec    : std_logic_vector(out_eof_vec'range);

  signal buf_load           : std_logic;  -- input vec to buf
  signal buf_flush          : std_logic;  -- buf to output vec
  signal buf_sof            : std_logic;
  signal nxt_buf_sof        : std_logic;
  signal buf_sync           : std_logic;
  signal nxt_buf_sync       : std_logic;
  signal buf_eof            : std_logic;
  signal nxt_buf_eof        : std_logic;
  signal nxt_out_sof        : std_logic;
  signal nxt_sof_sync       : std_logic;
begin
  no_pack : if g_in_nof_words = g_out_nof_words generate
    out_dat <= RESIZE_UVEC(in_dat, out_dat'length);  -- any extra bits will get stripped again by dp_repack_legacy at the other end,
                                                     -- typically g_out_dat_w=g_in_dat_w
    out_val <= in_val;
    out_sof <= in_sof;
    out_eof <= in_eof;
    sof_sync <= in_sync;
  end generate;

  gen_pack : if g_in_nof_words /= g_out_nof_words generate
    p_clk : process(clk, rst)
    begin
      if rst = '1' then
        in_dat_vec  <= (others => '0');
        in_val_vec  <= (others => '0');
        buf_dat_vec <= (others => '0');
        buf_val     <= '0';
        buf_sof     <= '0';
        buf_eof     <= '0';
        buf_sync    <= '0';
        out_dat_vec <= (others => '0');
        out_val_vec <= (others => '0');
        out_eof_vec <= (others => '0');
        out_sof     <= '0';
        sof_sync    <= '0';
      elsif rising_edge(clk) then
        in_dat_vec  <= nxt_in_dat_vec;
        in_val_vec  <= nxt_in_val_vec;
        buf_dat_vec <= nxt_buf_dat_vec;
        buf_val     <= nxt_buf_val;
        buf_sof     <= nxt_buf_sof;
        buf_eof     <= nxt_buf_eof;
        buf_sync    <= nxt_buf_sync;
        out_dat_vec <= nxt_out_dat_vec;
        out_val_vec <= nxt_out_val_vec;
        out_eof_vec <= nxt_out_eof_vec;
        out_sof     <= nxt_out_sof;
        sof_sync    <= nxt_sof_sync;
      end if;
    end process;

    gen_ls_to_ms : if g_ls_to_ms = true generate
      -- read out_dat_vec: LS word
      out_dat <= out_dat_vec(out_dat'range);
    end generate;

    gen_ms_to_ls : if g_ls_to_ms = false generate
      -- read out_dat_vec: MS word
      out_dat <= out_dat_vec(out_dat_vec'high downto out_dat_vec'high - out_dat'length + 1);
    end generate;

    out_val <= out_val_vec(0);
    out_eof <= out_eof_vec(0);

    buf_load  <= '1' when signed(in_val_vec) = -1 else '0';
                          -- in_val_vec=-1: input set complete, ready to be repacked
    buf_flush <= '1' when (unsigned(out_val_vec) = 1 or unsigned(out_val_vec) = 0) and buf_val = '1' else '0';
                          -- out_val_vec=0: ready to repack first input set
                          -- out_val_vec=1: ready to repack next input set
                          -- buf_val: new input set available

    p_in: process(in_sof, buf_load, in_val, in_val_vec, in_dat, in_dat_vec)
    begin
      nxt_in_val_vec <= in_val_vec;
      if in_sof = '1' or buf_load = '1' then
        nxt_in_val_vec <= (others => '0');
        nxt_in_val_vec(in_val_vec'high) <= in_val;
      elsif in_val = '1' then
        nxt_in_val_vec <= in_val & in_val_vec(in_val_vec'high downto 1);
      end if;

      nxt_in_dat_vec <= in_dat_vec;
      if in_val = '1' then
        if g_ls_to_ms = true then
          -- Left shift in_dat into in_dat_vec at each in_dat cycle, results in LS to MS word concatenation
          nxt_in_dat_vec <= in_dat & in_dat_vec(in_dat_vec'high downto in_dat'length);
        else
          -- Right shift in_dat into in_dat_vec at each in_dat cycle, results in MS to LS word concatenation
          nxt_in_dat_vec <= in_dat_vec(in_dat_vec'high - in_dat'length downto 0) & in_dat;
        end if;
      end if;
    end process;

    p_buf : process(buf_load, buf_flush, in_sync, in_sof, in_eof, buf_sync, buf_sof, buf_eof, in_dat_vec, in_val_vec, buf_dat_vec, buf_val)
    begin
      nxt_buf_sof <= buf_sof;  -- buf_sof
      nxt_buf_sync <= buf_sync;  -- buf_sync
      if in_sof = '1' then
        nxt_buf_sof <= '1';
        nxt_buf_sync <= in_sync;
      elsif buf_flush = '1' then
        nxt_buf_sof <= '0';
        nxt_buf_sync <= '0';
      end if;
      nxt_buf_eof <= buf_eof;  -- buf_eof
      if in_eof = '1' then
        nxt_buf_eof <= '1';
      elsif buf_flush = '1' and buf_load = '0' then
        nxt_buf_eof <= '0';
      end if;
      nxt_buf_dat_vec <= buf_dat_vec;  -- buf_dat_vec, buf_val
      nxt_buf_val     <= buf_val;
      if buf_load = '1' then
        nxt_buf_dat_vec <= in_dat_vec;
        nxt_buf_val     <= in_val_vec(in_val_vec'high);
      elsif buf_flush = '1' then
        nxt_buf_dat_vec <= (others => '0');
        nxt_buf_val     <= '0';
      end if;
    end process;

    p_out: process(buf_flush, buf_load, buf_sync, buf_sof, buf_eof, buf_dat_vec, out_val_vec, out_dat_vec, out_eof_vec)
    begin
      nxt_out_dat_vec <= out_dat_vec;  -- out_dat, out_val, out_sof
      if buf_flush = '1' then
        nxt_out_dat_vec <= buf_dat_vec;
        nxt_out_val_vec <= (others => '1');
        nxt_out_sof     <= buf_sof;
        nxt_sof_sync    <= buf_sync;
      else
        if g_ls_to_ms = true then
          -- Push SLV to the right so new word appears at LS position
          nxt_out_dat_vec <= std_logic_vector(to_unsigned(0, out_dat'length)) &
                                              out_dat_vec(out_dat_vec'high downto out_dat'length);
        else
          -- Push SLV to the left so new word appears at MS position
          nxt_out_dat_vec <= out_dat_vec(out_dat_vec'high - out_dat'length downto 0) &
                                         std_logic_vector(to_unsigned(0, out_dat'length));
        end if;

        nxt_out_val_vec <= '0' & out_val_vec(out_val_vec'high downto 1);
        nxt_out_sof <= '0';
        nxt_sof_sync <= '0';
      end if;
      nxt_out_eof_vec <= out_eof_vec;  -- out_eof
      if buf_flush = '1' and buf_load = '0' then
        nxt_out_eof_vec <= (others => '0');
        nxt_out_eof_vec(out_eof_vec'high) <= buf_eof;
      else
        nxt_out_eof_vec <= '0' & out_eof_vec(out_eof_vec'high downto 1);
      end if;
    end process;
  end generate;
end rtl;
