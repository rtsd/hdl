-------------------------------------------------------------------------------
-- Title      : Cyclic Redundancy Check Package
-- Project    :
-------------------------------------------------------------------------------
-- File       : crc_pkg.vhd
-- Author     : Will Kamp  <will@knight>
-- Company    :
-- Created    : 2016-02-17
-- Last update: 2016-04-08
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Acts as a portal to the easics.com CRC functions/packages.
-------------------------------------------------------------------------------
-- Copyright (c) 2016
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-02-17  1.0      will    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.PCK_CRC32_D8.all;
use work.PCK_CRC32_D16.all;
use work.PCK_CRC32_D32.all;
use work.PCK_CRC32_D64.all;
use work.PCK_CRC32_D128.all;
use work.PCK_CRC32_D256.all;
use work.PCK_CRC32_D512.all;
use work.PCK_CRC32_D1024.all;

package crc_pkg is

  function f_next_crc (
      data : std_logic_vector;
      crc  : std_logic_vector)
    return std_logic_vector;

end package crc_pkg;

package body crc_pkg is

  function f_next_crc (
      data : std_logic_vector;
      crc  : std_logic_vector)
    return std_logic_vector is
  begin
    case crc'length is
      when 32 =>
        case data'length is
          when 8      => return nextCRC32_D8(data, crc);
          when 16     => return nextCRC32_D16(data, crc);
          when 32     => return nextCRC32_D32(data, crc);
          when 64     => return nextCRC32_D64(data, crc);
          when 128    => return nextCRC32_D128(data, crc);
          when 256    => return nextCRC32_D256(data, crc);
          when 512    => return nextCRC32_D512(data, crc);
          when 1024   => return nextCRC32_D1024(data, crc);
          when others => report "Data width of" & integer'image(data'length) & " is not supported. Go to www.easics.com to generate one and add it to crc_pkg.vhd" severity failure;
        end case;
      when others =>
        report "CRC width of" & integer'image(crc'length) & " is not supported. Go to www.easics.com to generate one and add it to crc_pkg.vhd" severity failure;
    end case;
    return (crc'range => '-'); -- drop out to default. Keeps compiler happy.
  end function f_next_crc;

end package body crc_pkg;
