-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Behavioral model of the National adc08d1020 ADC on the ADU
-- Description:
--   The adc08d1020 model features:
--   . the sampling analogue input data via AI, AQ and overflow via AOVR
--   . offset binary ADC data format
--   . the internal ADC test pattern generation when test_pattern_en is '1'
--   . allows dynamic setting of skew for each LVDS line

entity adc08d1020 is
  generic (
    g_clk_period      : time := 1250 ps;  -- Sample clock period of CLK
    g_dclk_init_phase : natural := 0  -- When 0 then use default phase for DCLK, else use the other DDR phase
  );
  port (
    AI              : in  integer := 0;
    AQ              : in  integer := 0;
    AOVR            : in  std_logic := '0';
    CLK             : in  std_logic := '0';
    DCLK            : out std_logic;
    DCLK_RST        : in  std_logic := '0';
    DI              : out std_logic_vector(7 downto 0);
    DQ              : out std_logic_vector(7 downto 0);
    OVR             : out std_logic;

    test_pattern_en : in std_logic := '0';
    lvds_skew_di    : in t_natural_arr(7 downto 0) := (others => 0);  -- ps unit
    lvds_skew_dq    : in t_natural_arr(7 downto 0) := (others => 0);  -- ps unit
    lvds_skew_ovr   : in natural := 0;  -- ps unit
    lvds_skew_dclk  : in natural := 0  -- ps unit
  );
end adc08d1020;

architecture beh of adc08d1020 is
  constant c_nof_adc  : natural := 2;  -- for AI and AQ

  type t_adc_dat_arr is array (integer range <>) of std_logic_vector(7 downto 0);

  signal adc_dat  : t_adc_dat_arr(0 to c_nof_adc - 1);  -- [0] = AI, [1] = AQ
  signal adc_ovr  : std_logic;

  signal tp_cnt   : natural := 0;  -- test pattern sequence index counter

  signal dclk_dis : std_logic := '0';
  signal ddr_clk  : std_logic := sel_a_b(g_dclk_init_phase, '1', '0');
begin
  -- Sample the analogue I and Q input using the falling edge of the sample clock CLK
  p_sample : process(CLK)
  begin
    if falling_edge(CLK) then
      if test_pattern_en = '0' then
        adc_dat(0) <= offset_binary(TO_SVEC(AI, 8));
        adc_dat(1) <= offset_binary(TO_SVEC(AQ, 8));
        adc_ovr    <= AOVR;
      else
        --    TP_I TP_Q  TP_OV
        -- T0 02h  01h   0
        -- T1 FDh  FEh   1
        -- T2 02h  01h   0
        -- T3 02h  01h   0
        -- T4 FDh  FEh   1
        -- T5 FDh  FEh   1
        -- T6 02h  01h   0
        -- T7 02h  01h   0
        -- T8 FDh  FEh   1
        -- T9 02h  01h   0
        case tp_cnt is
          when 0 | 2 | 3 | 6 | 7 | 9 => adc_dat(0) <= TO_UVEC(16#02#, 8); adc_dat(1) <= TO_UVEC(16#01#, 8); adc_ovr <= '0';
          when others                => adc_dat(0) <= TO_UVEC(16#FD#, 8); adc_dat(1) <= TO_UVEC(16#FE#, 8); adc_ovr <= '1';
        end case;
        tp_cnt <= tp_cnt + 1;
        if tp_cnt = 9 then
          tp_cnt <= 0;
        end if;
      end if;
    end if;
  end process;

  -- Disable the DDR clock during the DCLK_RST and delay the DDR clock after release of DCLK_RST
  p_dclk_dis : process(DCLK_RST, CLK)
    variable v_cnt : natural;
  begin
    if DCLK_RST = '1' then
      dclk_dis <= '1';
      v_cnt := 0;
    elsif rising_edge(CLK) then
      dclk_dis <= '0';
      if v_cnt < 8 then  -- latency 3 or 4 CLK cycles and 4 ns propagation delay, so in total about 8 CLK cycles
        dclk_dis <= '1';
        v_cnt := v_cnt + 1;
      end if;
    end if;
  end process;

  -- Create the DDR clock that is half the rate of the sample clock CLK
  ddr_clk <= '0' when dclk_dis = '1' else not ddr_clk when falling_edge(CLK);

  -- Apply the skew to each LVDS trace
  gen_skew : for S in 7 downto 0 generate
    DI(S) <= transport adc_dat(0)(S) after lvds_skew_di(S) * 1 ps;
    DQ(S) <= transport adc_dat(1)(S) after lvds_skew_dq(S) * 1 ps;
  end generate;

  OVR  <= transport adc_ovr after lvds_skew_ovr * 1 ps;
  DCLK <= transport ddr_clk after lvds_skew_dclk * 1 ps;
end beh;
