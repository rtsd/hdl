-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;

-- Purpose:
--   Hold the in_data value when in_en is inactive. The held data can then be
--   output when the downstream sink is ready again.
-- Description:
--   Hold last in_data value when not valid else register the new in_data.
-- Remarks:
-- . The rst can be left not connected, because typically the data value at
--   reset is don't care (because the data valid will then be low)
-- . The in_en is typically in_valid or ready
-- . The in_data is typically in_data, in_empty, in_error or in_channel
-- . Typically used together with dp_hold_ctrl

entity dp_hold_data is
  port (
    rst      : in  std_logic := '0';
    clk      : in  std_logic;
    in_data  : in  std_logic_vector;
    in_en    : in  std_logic;
    hld_data : out std_logic_vector
  );
end dp_hold_data;

architecture rtl of dp_hold_data is
  signal i_hld_data   : std_logic_vector(hld_data'range);
  signal nxt_hld_data : std_logic_vector(hld_data'range);
begin
  hld_data <= i_hld_data;

  nxt_hld_data <= in_data when in_en = '1' else i_hld_data;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_hld_data <= (others => '0');
    elsif rising_edge(clk) then
      i_hld_data <= nxt_hld_data;
    end if;
  end process;
end rtl;
