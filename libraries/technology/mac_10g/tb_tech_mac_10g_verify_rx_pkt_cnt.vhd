-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench verification for the tech_mac_10g
-- Description:
-- . Verify that > 0 packets were transmitted and that all were received

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;

entity tb_tech_mac_10g_verify_rx_pkt_cnt is
  generic (
    g_nof_pkt     : natural
  );
  port (
    tx_clk      : in  std_logic;
    tx_sosi     : in  t_dp_sosi;
    rx_clk      : in  std_logic;
    rx_sosi     : in  t_dp_sosi;
    tx_pkt_cnt  : out natural;
    rx_pkt_cnt  : out natural;
    rx_end      : in  std_logic
  );
end tb_tech_mac_10g_verify_rx_pkt_cnt;

architecture tb of tb_tech_mac_10g_verify_rx_pkt_cnt is
  signal i_tx_pkt_cnt     : natural := 0;
  signal i_rx_pkt_cnt     : natural := 0;
begin
  tx_pkt_cnt <= i_tx_pkt_cnt;
  rx_pkt_cnt <= i_rx_pkt_cnt;

  -- Verification
  i_tx_pkt_cnt <= i_tx_pkt_cnt + 1 when tx_sosi.sop = '1' and rising_edge(tx_clk);
  i_rx_pkt_cnt <= i_rx_pkt_cnt + 1 when rx_sosi.eop = '1' and rising_edge(rx_clk);

  p_verify_pkt_cnt : process
  begin
    wait until rx_end = '1';

    -- Verify that all transmitted packets have been received
    if i_tx_pkt_cnt = 0 then
      report "No packets were transmitted."
        severity ERROR;
    elsif i_tx_pkt_cnt /= g_nof_pkt then
      report "Not all packets were transmitted."
        severity ERROR;
    elsif i_rx_pkt_cnt = 0 then
      report "No packets were received."
        severity ERROR;
    elsif i_tx_pkt_cnt /= i_rx_pkt_cnt then
      report "Not all transmitted packets were received."
        severity ERROR;
    end if;

    wait;
  end process;
end tb;
