-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- ----------------------------------------------------------------------------

-- Author: E. Kooistra
-- Purpose:
-- . Test bench for dp_packet_merge and dp_packet_unmerge
-- Description:
-- . The p_stimuli_st uses proc_dp_gen_block_data to generate g_nof_repeat
--   stimuli_src_out packets of length g_pkt_len_merge.
-- . The u_dp_packet_merge merges g_nof_pkt_merge packets from stimuli_src_out
--   into dp_packet_merge_src_out.
-- . The p_stimuli_unmerge provides stimuli for the .err and .empty fields of
--   dp_packet_merge_src_out to get dp_packet_merge_sosi
-- . The u_dp_packet_unmerge unmerges the dp_packet_merge_sosi into packets
--   of length g_pkt_len_unmerge into verify_snk_in. If g_pkt_len_unmerge /=
--   g_pkt_len_merge, then:
--   - the last unmerged packet may be shorter then g_pkt_len_unmerge, to
--     contain the remaining data.
--   - the number of unmerged packets c_nof_pkt_unmerge, may differ from
--     g_nof_pkt_merge.
--   - use c_bsn_increment_unmerge = 0 for u_dp_packet_unmerge and expect BSN
--     increment of 0 for unmerged packets from same merged packet or
--     c_nof_pkt_unmerge for unmerged packets from subsequent merged packets.
--     Verify this only manually in wave window, because verifying different
--     increments is not supported by proc_dp_verify_data().
-- . The p_verify section does the verification of verify_snk_in.
-- . Flow control is verified via g_flow_control_*.
-- . Block diagram:
--
--   p_stimuli_st --> u_dp_packet_merge --+--> u_dp_packet_unmerge --> p_verify
--                                        ^
--                                        |
--                    p_stimuli_unmerge --/
--
-- Usage:
-- > as 10
-- > run -all
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_packet_merge_unmerge is
  generic (
    -- general
    -- . always active, random or pulse flow control
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;
    -- specific
    g_pipeline_ready         : boolean := true;
    g_data_w                 : natural := 16;
    g_nof_repeat             : natural := 14;
    g_nof_pkt_merge          : natural := 4;
    g_pkt_len_merge          : natural := 7;
    g_pkt_len_unmerge        : natural := 7;
    g_pkt_gap                : natural := 0;
    g_bsn_increment          : natural := 0
  );
end tb_dp_packet_merge_unmerge;

architecture tb of tb_dp_packet_merge_unmerge is
  constant c_rl                       : natural := 1;
  constant c_use_ready                : boolean := g_flow_control_verify /= e_active;

  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_nof_pkt_unmerge          : natural := ceil_div(g_nof_pkt_merge * g_pkt_len_merge, g_pkt_len_unmerge);
  constant c_bsn_increment_unmerge    : natural := sel_a_b(g_nof_pkt_merge = c_nof_pkt_unmerge, g_bsn_increment, 0);

  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  constant c_data_max                 : unsigned(g_data_w - 1 downto 0) := (others => '1');
  constant c_data_init                : integer := -1;

  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";
                                          -- X"0877665544332211"
  constant c_bsn_increment_max        : natural := (g_bsn_increment + 1) * g_nof_pkt_merge;  -- +1 to ensure max > 0
  constant c_bsn_increment_w          : natural := ceil_log2(c_bsn_increment_max + 1);
  constant c_unsigned_bsn_increment   : unsigned(c_bsn_increment_w - 1 downto 0) :=
                                          to_unsigned(g_bsn_increment, c_bsn_increment_w);
  constant c_unsigned_bsn_pkt_merge : unsigned(c_bsn_increment_w - 1 downto 0) :=
                                          to_unsigned(g_nof_pkt_merge, c_bsn_increment_w);

  constant c_nof_pkt_not_zero         : natural := sel_a_b(g_nof_pkt_merge = 0, 1, g_nof_pkt_merge);
  constant c_nof_merged_sop           : natural := sel_a_b(g_nof_pkt_merge = 0, 0, ceil_div(g_nof_repeat, c_nof_pkt_not_zero));
  -- verify that at least some packets have been merged, not exact to allow variation by p_stimuli_mm
  constant c_verify_at_least          : natural := largest(1, c_nof_merged_sop / 2);

  signal tb_end                     : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal sl1                        : std_logic := '1';

  -- use different slv lengths to have different random sequences
  signal random_0                   : std_logic_vector(14 downto 0) := (others => '0');
  signal random_1                   : std_logic_vector(15 downto 0) := (others => '0');
  signal pulse_0                    : std_logic;
  signal pulse_1                    : std_logic;
  signal pulse_en                   : std_logic := '1';

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_data               : std_logic_vector(g_data_w - 1 downto 0);

  signal dp_packet_merge_snk_in     : t_dp_siso;
  signal dp_packet_merge_src_out    : t_dp_sosi;
  signal dp_packet_merge_siso       : t_dp_siso;
  signal dp_packet_merge_sosi       : t_dp_sosi;

  signal prev_verify_snk_out        : t_dp_siso;
  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_data                : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_verify_snk_in         : t_dp_sosi;

  signal input_pkt_cnt              : natural := 0;
  signal merged_pkt_cnt             : natural := 0;
  signal output_pkt_cnt             : natural := 0;
  signal exp_channel                : natural := 0;
  signal exp_error                  : natural := 0;
  signal exp_empty                  : natural := 0;

  signal verify_hold_sop            : std_logic := '0';
  signal verify_en_valid            : std_logic := '0';
  signal verify_en_sop              : std_logic := '0';
  signal verify_en_eop              : std_logic := '0';
  signal verify_done                : std_logic := '0';
  signal verify_value_en            : std_logic := sel_a_b(g_nof_pkt_merge = 0, '0', '1');

  signal expected_verify_snk_in     : t_dp_sosi;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,     '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  stimuli_en           <= '1'                     when g_flow_control_stimuli = e_active else
                          random_0(random_0'high) when g_flow_control_stimuli = e_random else
                          pulse_0                 when g_flow_control_stimuli = e_pulse;

  verify_snk_out.ready <= '1'                     when g_flow_control_verify = e_active  else
                          random_1(random_1'high) when g_flow_control_verify = e_random  else
                          pulse_1                 when g_flow_control_verify = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    constant c_channel : natural := 0;
    constant c_err     : natural := 0;
    variable v_sosi    : t_dp_sosi := c_dp_sosi_rst;
    variable v_bsn     : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn  := INCR_UVEC(c_bsn_init, -1);
    v_sosi.data := INCR_UVEC(TO_DP_DATA(c_data_init), -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn  := INCR_UVEC(v_sosi.bsn, g_bsn_increment);
      -- insert sync starting at BSN = c_sync_offset and with period c_sync_period
      v_sosi.sync := sel_a_b((unsigned(v_sosi.bsn) mod c_sync_period) = c_sync_offset, '1', '0');
      v_sosi.data := INCR_UVEC(v_sosi.data, g_pkt_len_merge);
      v_sosi.data := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w

      -- Send packet
      proc_dp_gen_block_data(g_data_w, TO_UINT(v_sosi.data), g_pkt_len_merge,
                             c_channel, c_err, v_sosi.sync, v_sosi.bsn,
                             clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    -- . e_qual (account for merge size g_nof_pkt_merge)
    -- . e_at_least
    v_sosi.bsn := std_logic_vector(unsigned(c_bsn_init) + c_verify_at_least * g_nof_pkt_merge);

    -- . account for g_pkt_len_merge
    v_sosi.data := INCR_UVEC(v_sosi.data, g_pkt_len_merge - 1);
    v_sosi.data := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w
    expected_verify_snk_in <= v_sosi;

    -- Signal end of stimuli
    -- . latency from stimuli to verify depends on the flow control, so wait
    --   sufficiently long for last packet to have passed through
    proc_common_wait_some_cycles(clk, 100);
    if verify_value_en = '1' then
      proc_common_gen_pulse(clk, verify_done);
    end if;
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  -- . p_verify
  ------------------------------------------------------------------------------

  -- Start verify after first valid, sop or eop
  verify_en_valid <= '1' when verify_snk_in.valid = '1' and rising_edge(clk);
  verify_en_sop   <= '1' when verify_snk_in.sop = '1'   and rising_edge(clk);
  verify_en_eop   <= '1' when verify_snk_in.eop = '1'   and rising_edge(clk);

  -- Verify that the stimuli have been applied at all
  proc_dp_verify_value("verify_snk_in.valid",               clk, verify_done, sl1, verify_en_valid);
  proc_dp_verify_value("verify_snk_in.sop",                 clk, verify_done, sl1, verify_en_sop);
  proc_dp_verify_value("verify_snk_in.eop",                 clk, verify_done, sl1, verify_en_eop);
  proc_dp_verify_value("verify_snk_in.data",    e_equal,    clk, verify_done, expected_verify_snk_in.data,    verify_snk_in.data);
  proc_dp_verify_value("verify_snk_in.bsn",     e_at_least, clk, verify_done, expected_verify_snk_in.bsn,     verify_snk_in.bsn);

  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_data("verify_snk_in.data", c_rl, c_data_max, c_unsigned_1,
                      clk, verify_en_valid, verify_snk_out.ready,
                      verify_snk_in.valid, verify_snk_in.data, prev_verify_snk_in.data);

  -- Verify that the output is incrementing BSN, like the input stimuli
  gen_verify_bsn_increment : if g_nof_pkt_merge = c_nof_pkt_unmerge generate
    -- If number of packets stays the same, then unmerge bsn will increment
    -- as stimuli bsn by c_unsigned_bsn_increment = g_bsn_increment
    proc_dp_verify_data("verify_snk_in.bsn increment", c_rl,
                         c_unsigned_0, c_unsigned_bsn_increment,
                         clk, verify_en_sop, verify_snk_out.ready,
                         verify_snk_in.sop, verify_snk_in.bsn, prev_verify_snk_in.bsn);
  end generate;
  gen_verify_bsn_pkt_unmerge : if g_nof_pkt_merge /= c_nof_pkt_unmerge generate
    -- If number of packets differs after unmerge, then use
    -- c_bsn_increment_unmerge = 0 to not increment bsn during ummerge of
    -- packets, this is covered by gap c_unsigned_0. The bsn of the merged
    -- packets is passed on into each of the unmerged packets, this is
    -- covered by c_unsigned_bsn_pkt_merge.
    proc_dp_verify_data("verify_snk_in.bsn pkt_unmerge", c_rl,
                         c_unsigned_0, c_unsigned_0, c_unsigned_bsn_pkt_merge,
                         clk, verify_en_sop, verify_snk_out.ready,
                         verify_snk_in.sop, verify_snk_in.bsn, prev_verify_snk_in.bsn);
  end generate;

  -- Verify output packet ctrl
  proc_dp_verify_sop_and_eop(clk, verify_snk_in.valid, verify_snk_in.sop, verify_snk_in.eop, verify_hold_sop);

  -- Verify output ready latency
  proc_dp_verify_valid(clk, verify_en_valid, verify_snk_out.ready, prev_verify_snk_out.ready, verify_snk_in.valid);

  -- Verify output verify_snk_in.sosi fields
  p_verify_fields : process(clk)
  begin
    if rising_edge(clk) then
      -- Use verify_snk_in.valid, because the assumption with dp_packet_unmerge
      -- is that the channel (at sop) and err, empty (at eop) are valid during
      -- the entire unmerged packet.
      if verify_snk_in.valid = '1' then
        -- Verify that output channel yields index of unmerged packet, in range(c_nof_pkt_unmerge)
        assert unsigned(verify_snk_in.channel) = exp_channel
          report "Wrong unmerged verify_snk_in.channel"
          severity ERROR;

        -- Verify that output error is same per c_nof_pkt_unmerge unmerged packets
        assert unsigned(verify_snk_in.err) = exp_error
          report "Wrong unmerged verify_snk_in.err"
          severity ERROR;

        -- Verify that output empty is same per c_nof_pkt_unmerge unmerged packets
        assert unsigned(verify_snk_in.empty) = exp_empty
          report "Wrong unmerged verify_snk_in.empty"
          severity ERROR;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT dp_packet_merge
  ------------------------------------------------------------------------------

  -- Merge every g_nof_pkt_merge incomming packets into output packets
  u_dp_packet_merge : entity work.dp_packet_merge
  generic map (
    g_nof_pkt       => g_nof_pkt_merge,
    g_align_at_sync => false,  -- not used in this tb
    g_bsn_increment => g_bsn_increment,
    g_bsn_err_bi    => 0  -- not used in this tb
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => stimuli_src_in,
    snk_in    => stimuli_src_out,

    src_in    => dp_packet_merge_snk_in,
    src_out   => dp_packet_merge_src_out
  );

  dp_packet_merge_snk_in <= dp_packet_merge_siso;

  ------------------------------------------------------------------------------
  -- TB stimuli for dp_packet_unmerge
  ------------------------------------------------------------------------------
  p_stimuli_unmerge : process(dp_packet_merge_src_out)
  begin
    dp_packet_merge_sosi <= dp_packet_merge_src_out;
    -- Use counter as err field stimulus, per merged packet. Use
    -- offset to distinguish err and empty values
    dp_packet_merge_sosi.err <= TO_DP_ERROR(merged_pkt_cnt);
    dp_packet_merge_sosi.empty <= TO_DP_EMPTY(merged_pkt_cnt + 1);
  end process;

  ------------------------------------------------------------------------------
  -- TB signals for verification of dp_packet_unmerge
  ------------------------------------------------------------------------------
  input_pkt_cnt <= input_pkt_cnt + 1 when rising_edge(clk) and stimuli_src_out.eop = '1';
  merged_pkt_cnt <= merged_pkt_cnt + 1 when rising_edge(clk) and dp_packet_merge_src_out.eop = '1';
  output_pkt_cnt <= output_pkt_cnt + 1 when rising_edge(clk) and verify_snk_in.eop = '1';

  -- get expected value, aligned with output packet boundaries
  p_exp_channel : process(output_pkt_cnt)
  begin
    -- Avoid divide by 0
    exp_channel <= 0;
    if c_nof_pkt_unmerge > 0 then
      exp_channel <= output_pkt_cnt mod c_nof_pkt_unmerge;
    end if;
  end process;

  exp_error <= merged_pkt_cnt when rising_edge(clk) and verify_snk_in.eop = '1';
  exp_empty <= exp_error + 1;

  ------------------------------------------------------------------------------
  -- Optional reverse DUT dp_packet_unmerge
  ------------------------------------------------------------------------------
  u_dp_packet_unmerge : entity work.dp_packet_unmerge
  generic map (
    g_use_ready       => c_use_ready,
    g_pipeline_ready  => g_pipeline_ready,
    g_nof_pkt         => c_nof_pkt_unmerge,
    g_pkt_len         => g_pkt_len_unmerge,
    g_bsn_increment   => c_bsn_increment_unmerge
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => dp_packet_merge_siso,
    snk_in    => dp_packet_merge_sosi,

    src_in    => verify_snk_out,
    src_out   => verify_snk_in
  );

  -- Map to slv to ease monitoring in wave window
  stimuli_data <= stimuli_src_out.data(g_data_w - 1 downto 0);
  verify_data  <= verify_snk_in.data(g_data_w - 1 downto 0);
end tb;
