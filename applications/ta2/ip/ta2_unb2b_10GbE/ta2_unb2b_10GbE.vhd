-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide 10G ethernet I/O interface (BSP) for OpenCL kernel on Arria10
-- Description:
-- . This core consists of:
--   . An Intel/Altera 10G Low Latency MAC instance
--   . SOP/EOP insertion (kernel channel only carries data and valid)
--   . Dual clock FIFO
--     . Clock domain transition between kernel_clk and clk_txmac
--     . Buffers full Ethernet packet (10G MAC requires uninterrupted packet)
--   . Clock (PLL) / reset generation
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   .
--   . The data field of the ST-avalon interface is also used to provide
--   . SOP, EOP and empty meta-data. The implementation of this is shown below.
--   +-----------+---------+--------------------------------------------------------+
--   | Bit range | Name    | Description                                            |
--   +-----------+---------+--------------------------------------------------------+
--   | [0:63]    | payload | Packet payload                                         |
--   +-----------+---------+--------------------------------------------------------+
--   | 64        | sop     | Start of packet signal                                 |
--   +-----------+---------+--------------------------------------------------------+
--   | 65        | eop     | End of packet signal                                   |
--   +-----------+---------+--------------------------------------------------------+
--   | 66:68     | -       | reserved bits                                          |
--   +-----------+---------+--------------------------------------------------------+
--   | 69:71     | empty   | On EOP, this field indicates how many bytes are unused |
--   +-----------+---------+--------------------------------------------------------+
--   | 72:103    | err     | On EOP, this field indicates errors (only used if      |
--   |           |         | g_use_err = TRUE)                                      |
--   +-----------+---------+--------------------------------------------------------+

library IEEE, common_lib, dp_lib, tech_pll_lib, technology_lib, tech_eth_10g_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity ta2_unb2b_10GbE is
  generic (
    g_nof_mac : natural;  -- Valid inputs are 1, 3, 4, 12, 24, 48
    g_use_err : boolean := false;
    g_err_w   : positive := 32;
    g_use_pll : boolean := true
  );
  port (
    mm_clk       : in  std_logic;  -- 100MHz clk for reconfig block and status interface
    mm_rst       : in  std_logic;

    clk_ref_r    : in  std_logic;  -- 644.53125MHz 10G MAC reference clock
    clk_156      : in  std_logic := '0';
    clk_312      : in  std_logic := '0';
    rst_156      : in  std_logic := '0';

    tx_serial_r  : out std_logic_vector(g_nof_mac - 1 downto 0);  -- Serial TX lanes towards QSFP cage
    rx_serial_r  : in  std_logic_vector(g_nof_mac - 1 downto 0);  -- Serial RX lanes from QSFP cage

    kernel_clk   : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset : in  std_logic;

    src_out_arr  : out t_dp_sosi_arr(g_nof_mac - 1 downto 0);
    src_in_arr   : in  t_dp_siso_arr(g_nof_mac - 1 downto 0);
    snk_out_arr  : out t_dp_siso_arr(g_nof_mac - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_mac - 1 downto 0)

  );
end ta2_unb2b_10GbE;

architecture str of ta2_unb2b_10GbE is
  constant c_sim                           : boolean := false;

  constant c_tx_fifo_fill                  : natural := 1125;  -- Largest frame is 9000 bytes = 1125
  constant c_tx_fifo_size                  : natural := 2048;
  constant c_rx_fifo_size                  : natural := 256;  -- should be large enough

  signal tr_ref_clk_312                    : std_logic;
  signal tr_ref_clk_156                    : std_logic;
  signal tr_ref_rst_156                    : std_logic;

  signal eth_ref_clk_644                   : std_logic;
  signal eth_ref_clk_312                   : std_logic;
  signal eth_ref_clk_156                   : std_logic;
  signal eth_ref_rst_156                   : std_logic;

  signal eth_tx_clk_arr                    : std_logic_vector(g_nof_mac - 1 downto 0);
  signal eth_tx_rst_arr                    : std_logic_vector(g_nof_mac - 1 downto 0);
  signal eth_rx_clk_arr                    : std_logic_vector(g_nof_mac - 1 downto 0);
  signal eth_rx_rst_arr                    : std_logic_vector(g_nof_mac - 1 downto 0);

  signal dp_latency_adapter_tx_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_snk_in_arr  : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_tx_snk_out_arr : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  signal dp_fifo_fill_tx_src_out_arr       : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_fifo_fill_tx_src_in_arr        : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  signal mac_10g_src_out_arr               : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal mac_10g_src_in_arr                : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  signal dp_fifo_dc_rx_src_out_arr         : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_fifo_dc_rx_src_in_arr          : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  signal dp_latency_adapter_rx_src_out_arr : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_latency_adapter_rx_src_in_arr  : t_dp_siso_arr(g_nof_mac - 1 downto 0);

  signal dp_xonoff_src_out_arr             : t_dp_sosi_arr(g_nof_mac - 1 downto 0);
  signal dp_xonoff_src_in_arr              : t_dp_siso_arr(g_nof_mac - 1 downto 0);
begin
  --------
  -- PLL
  --------
  g_pll : if g_use_pll generate
    u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    generic map (
      g_technology => c_tech_arria10_e1sg
    )
    port map (
      refclk_644 => clk_ref_r,
      rst_in     => mm_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );
  end generate;

  gen_no_pll : if not g_use_pll generate
    tr_ref_clk_156 <= clk_156;
    tr_ref_clk_312 <= clk_312;
    tr_ref_rst_156 <= rst_156;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Clocks and reset
  ---------------------------------------------------------------------------------------
  -- Apply the clocks from top level down such that they have their rising_edge() aligned without any delta-delay
  u_tech_eth_10g_clocks : entity tech_eth_10g_lib.tech_eth_10g_clocks
  generic map (
    g_technology     => c_tech_arria10_e1sg,
    g_nof_channels   => g_nof_mac
  )
  port map (
    -- Input clocks
    -- . Reference
    tr_ref_clk_644    => clk_ref_r,
    tr_ref_clk_312    => tr_ref_clk_312,
    tr_ref_clk_156    => tr_ref_clk_156,
    tr_ref_rst_156    => tr_ref_rst_156,

    -- Output clocks
    -- . Reference
    eth_ref_clk_644   => eth_ref_clk_644,
    eth_ref_clk_312   => eth_ref_clk_312,
    eth_ref_clk_156   => eth_ref_clk_156,
    eth_ref_rst_156   => eth_ref_rst_156,

    -- . Data
    eth_tx_clk_arr    => eth_tx_clk_arr,
    eth_tx_rst_arr    => eth_tx_rst_arr,

    eth_rx_clk_arr    => eth_rx_clk_arr,
    eth_rx_rst_arr    => eth_rx_rst_arr
  );

  gen_mac: for mac in 0 to g_nof_mac - 1 generate
    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    gen_tx_bytes: for I in 0 to 7 generate
      dp_latency_adapter_tx_snk_in_arr(mac).data(8 * (8 - I) - 1  downto 8 * (7 - I)) <= snk_in_arr(mac).data(8 * (I + 1) - 1 downto 8 * I);
    end generate;

    -- Assign correct data fields to control signals.
    gen_err_in: if g_use_err generate
      dp_latency_adapter_tx_snk_in_arr(mac).err(g_err_w - 1 downto 0) <= snk_in_arr(mac).data(71 + g_err_w downto 72);
    end generate;

    dp_latency_adapter_tx_snk_in_arr(mac).sop <= snk_in_arr(mac).data(64);
    dp_latency_adapter_tx_snk_in_arr(mac).eop <= snk_in_arr(mac).data(65);
    dp_latency_adapter_tx_snk_in_arr(mac).empty(2 downto 0) <= snk_in_arr(mac).data(71 downto 69);
    dp_latency_adapter_tx_snk_in_arr(mac).valid <= snk_in_arr(mac).valid;
    snk_out_arr(mac).ready <= dp_latency_adapter_tx_snk_out_arr(mac).ready;  -- Flow control towards source (kernel)
    snk_out_arr(mac).xon   <= dp_xonoff_src_in_arr(mac).xon;  -- use xonoff_src_in for status as xonoff_snk_out is always '1'

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=0 (OpenCL kernel) to RL=1 (Downstream).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_tx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 0,
      g_out_latency => 1
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_latency_adapter_tx_snk_in_arr(mac),
      snk_out   => dp_latency_adapter_tx_snk_out_arr(mac),

      src_out   => dp_latency_adapter_tx_src_out_arr(mac),
      src_in    => dp_latency_adapter_tx_src_in_arr(mac)
    );

    -----------------------------------------------------------------------------
    -- RX XON frame control
    -----------------------------------------------------------------------------
    u_dp_xonoff : entity dp_lib.dp_xonoff
    port map (
      rst      => kernel_reset,
      clk      => kernel_clk,

      in_siso  => dp_latency_adapter_tx_src_in_arr(mac),
      in_sosi  => dp_latency_adapter_tx_src_out_arr(mac),

      out_siso => dp_xonoff_src_in_arr(mac),
      out_sosi => dp_xonoff_src_out_arr(mac)
    );

    ---------------------------------------------------------------------------------------
    -- FIFO FILL with fill level/eop trigger so we can deliver packets to the MAC fast enough
    ---------------------------------------------------------------------------------------
    u_dp_fifo_fill_tx_eop : entity dp_lib.dp_fifo_fill_eop
    generic map (
      g_technology     => c_tech_arria10_e1sg,
      g_use_dual_clock => true,
      g_data_w         => c_xgmii_data_w,
      g_empty_w        => c_tech_mac_10g_empty_w,
      g_use_empty      => true,
      g_fifo_fill      => c_tx_fifo_fill,
      g_fifo_size      => c_tx_fifo_size
    )
    port map (
      wr_rst      => kernel_reset,
      wr_clk      => kernel_clk,
      rd_rst      => eth_tx_rst_arr(mac),
      rd_clk      => eth_tx_clk_arr(mac),

      snk_out     => dp_xonoff_src_in_arr(mac),
      snk_in      => dp_xonoff_src_out_arr(mac),

      src_in      => dp_fifo_fill_tx_src_in_arr(mac),
      src_out     => dp_fifo_fill_tx_src_out_arr(mac)
    );

    ---------------------------------------------------------------------------------------
    -- RX FIFO: rx_clk -> dp_clk
    ---------------------------------------------------------------------------------------
    u_dp_fifo_dc_rx : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => c_xgmii_data_w,
      g_empty_w     => c_tech_mac_10g_empty_w,
      g_use_empty   => true,
      g_fifo_size   => c_rx_fifo_size
    )
    port map (
      wr_rst      => eth_rx_rst_arr(mac),
      wr_clk      => eth_rx_clk_arr(mac),
      rd_rst      => kernel_reset,
      rd_clk      => kernel_clk,

      snk_out     => mac_10g_src_in_arr(mac),
      snk_in      => mac_10g_src_out_arr(mac),

      src_in      => dp_fifo_dc_rx_src_in_arr(mac),
      src_out     => dp_fifo_dc_rx_src_out_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=1 (Upstream) to RL=0 (OpenCL kernel).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_rx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_fifo_dc_rx_src_out_arr(mac),
      snk_out   => dp_fifo_dc_rx_src_in_arr(mac),

      src_out   => dp_latency_adapter_rx_src_out_arr(mac),
      src_in    => dp_latency_adapter_rx_src_in_arr(mac)
    );

    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    gen_rx_bytes: for I in 0 to 7 generate
      src_out_arr(mac).data(8 * (8 - I) - 1  downto 8 * (7 - I)) <= dp_latency_adapter_rx_src_out_arr(mac).data(8 * (I + 1) - 1 downto 8 * I);
    end generate;

    -- Assign control signals to correct data fields.
    gen_err_out: if g_use_err generate
      src_out_arr(mac).data(71 + g_err_w downto 72) <= dp_latency_adapter_rx_src_out_arr(mac).err(g_err_w - 1 downto 0);
    end generate;

    src_out_arr(mac).data(64) <= dp_latency_adapter_rx_src_out_arr(mac).sop;
    src_out_arr(mac).data(65) <= dp_latency_adapter_rx_src_out_arr(mac).eop;
    src_out_arr(mac).data(71 downto 69) <= dp_latency_adapter_rx_src_out_arr(mac).empty(2 downto 0);
    src_out_arr(mac).valid <= dp_latency_adapter_rx_src_out_arr(mac).valid;
    dp_latency_adapter_rx_src_in_arr(mac).ready <= src_in_arr(mac).ready;
    dp_latency_adapter_rx_src_in_arr(mac).xon <= '1';
 end generate;

  ---------------------------------------------------------------------------------------
  -- ETH MAC + PHY
  ---------------------------------------------------------------------------------------
  u_tech_eth_10g : entity tech_eth_10g_lib.tech_eth_10g
  generic map (
    g_technology          => c_tech_arria10_e1sg,
    g_sim                 => c_sim,
    g_sim_level           => 1,  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        => g_nof_mac,
    g_direction           => "TX_RX",
    g_pre_header_padding  => false
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   => eth_ref_clk_644,  -- 644.531250 MHz for 10GBASE-R
    tr_ref_clk_312   => eth_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156   => eth_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156   => eth_ref_rst_156,  -- for 10GBASE-R or for XAUI

    -- MM
    mm_clk           => '0',
    mm_rst           => '0',

    -- ST
    tx_snk_in_arr    => dp_fifo_fill_tx_src_out_arr,  -- 64 bit data @ 156 MHz
    tx_snk_out_arr   => dp_fifo_fill_tx_src_in_arr,

    rx_src_out_arr   => mac_10g_src_out_arr,  -- 64 bit data @ 156 MHz
    rx_src_in_arr    => mac_10g_src_in_arr,

    -- PHY serial IO
    -- . 10GBASE-R (single lane)
    serial_tx_arr    => tx_serial_r,
    serial_rx_arr    => rx_serial_r
  );
end str;
