-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author : P.Donker  okt. 2020, added bsn_time_offset
--          E. Kooistra aug 2022, corected s_bsn_time_offset, clarified
--            sync_size_cnt, removed redundant current_bsn_time_offset
--            and s_init, simplified implementation of dp_on_status.
--
-- Purpose :
--   Start a periodic block sync interval and maintain a block sequence
--   number according to the PPS and BSN grid defined in [1].
-- Description:
--   When dp_on is low then all outputs are low. When dp_on is high, the
--   output sync starts pulsing after bsn_time_offset (in clk cycles) with
--   a period of g_block_size number of clk cycles and the output valid,
--   sop and eop will be active.
--   The dp_on is asynchronous. Alternatively, one can assert dp_on while
--   dp_on_pps is high to start the data path on the next PPS.
--   The src_out.sync always happens at the src_out.sop.
--   If nof_clk_per_sync / g_block_size is an integer than all src_out.sync
--   intervals will have nof_clk_per_sync clk cycles, else nof_clk_per_sync
--   is the average number of clock cycles between src_out.sync and then the
--   number of blocks per sync intervals will vary between c_nof_block_hi
--   and c_nof_block_lo.
--   The dp_bsn_source_v2 takes care that src_out.valid starts with a
--   src_out.sync and src_out.sop and that src_out.valid can only go low
--   after a src_out.eop, to ensure that src_out only produces complete
--   sop-eop blocks that enter the subsequent processing.
--   The bs_restart is active at the first src_out.sop after dp_on went high.
-- Remarks:
-- . Starting the data path is only possible from the dp_off state, so one
--   has to disable (dp_on='0') the data path before restarting it.
-- . Effectively dp_on_status = src_out.valid, because when the BSN source
--   is on, then src_out.valid = '1' at every clk cycle.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Timing+in+Station

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_bsn_source_v2 is
  generic (
    g_block_size         : natural := 256;  -- >= 3, see state machine
    g_nof_clk_per_sync   : natural := 200 * 10**6;
    g_bsn_w              : natural := 48;
    g_bsn_time_offset_w  : natural := 10
  );
  port (
    rst                     : in  std_logic;
    clk                     : in  std_logic;
    pps                     : in  std_logic := '1';

    dp_on                   : in  std_logic;
    dp_on_pps               : in  std_logic;

    dp_on_status            : out std_logic;  -- = src_out.valid
    bs_restart              : out std_logic;  -- = src_out.sync for first sync after dp_on went high
    bs_new_interval         : out std_logic;  -- = active during first src_out.sync interval

    nof_clk_per_sync        : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_nof_clk_per_sync, c_word_w);
    bsn_init                : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    bsn_time_offset         : in  std_logic_vector(g_bsn_time_offset_w - 1 downto 0) := (others => '0');

    src_out                 : out t_dp_sosi  -- only uses sync, bsn[], valid, sop and eop
  );
end dp_bsn_source_v2;

architecture rtl of dp_bsn_source_v2 is
  constant c_block_size_cnt_w : natural := ceil_log2(g_block_size);
  constant c_block_cnt_zero   : std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');

  constant c_nof_block_lo     : natural := g_nof_clk_per_sync / g_block_size;
  constant c_nof_block_hi     : natural := ceil_div(g_nof_clk_per_sync, g_block_size);

  type t_state_enum is (s_dp_off, s_bsn_time_offset, s_dp_on_sop, s_dp_on, s_dp_on_eop);

  signal state       : t_state_enum;
  signal nxt_state   : t_state_enum;
  signal prev_state  : t_state_enum;

  signal block_size_cnt     : std_logic_vector(c_block_size_cnt_w - 1 downto 0);
  signal nxt_block_size_cnt : std_logic_vector(c_block_size_cnt_w - 1 downto 0);

  signal i_src_out          : t_dp_sosi := c_dp_sosi_init;
  signal nxt_src_out        : t_dp_sosi;

  signal i_bs_restart        : std_logic;
  signal nxt_bs_restart      : std_logic;
  signal i_bs_new_interval   : std_logic;
  signal reg_bs_new_interval : std_logic;

  signal nxt_bsn_time_offset_cnt : std_logic_vector(g_bsn_time_offset_w - 1 downto 0);
  signal bsn_time_offset_cnt     : std_logic_vector(g_bsn_time_offset_w - 1 downto 0);

  signal nxt_sync_size_cnt       : std_logic_vector(c_word_w - 1 downto 0);
  signal sync_size_cnt           : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_sync                : std_logic;
  signal sync                    : std_logic;
begin
  src_out <= i_src_out;
  dp_on_status <= i_src_out.valid;
  bs_restart <= i_bs_restart;
  bs_new_interval <= i_bs_new_interval;

  p_state : process(sync, sync_size_cnt, nof_clk_per_sync,
                    state, i_src_out, block_size_cnt, bsn_time_offset_cnt,
                    bsn_init, dp_on, dp_on_pps, pps, bsn_time_offset, prev_state)
  begin
    -- Maintain sync_size_cnt for nof_clk_per_sync
    -- . nof_clk_per_sync is the number of clk per pps interval and the
    --   average number of clk per src_out.sync interval, due to that the
    --   src_out.sync has to occur at the src_out.sop of a block.
    -- . The sync_size_cnt is started in s_dp_off at the pps
    -- . The pps interval is nof_clk_per_sync, so when the sync_size_cnt
    --   wraps, then the sync is used to ensure that src_out.sync will
    --   pulse at the src_out.sop of the next block.
    nxt_sync <= sync;
    nxt_sync_size_cnt <= INCR_UVEC(sync_size_cnt, 1);
    if unsigned(sync_size_cnt) = unsigned(nof_clk_per_sync) - 1 then
      nxt_sync <= '1';  -- will set src_out.sync on next src_out.sop
      nxt_sync_size_cnt <= (others => '0');
    end if;

    -- State machine for src_out
    nxt_state                   <= state;
    nxt_src_out                 <= i_src_out;  -- hold src_out.bsn
    nxt_src_out.sync            <= '0';
    nxt_src_out.valid           <= '0';
    nxt_src_out.sop             <= '0';
    nxt_src_out.eop             <= '0';
    nxt_block_size_cnt          <= block_size_cnt;
    nxt_bsn_time_offset_cnt     <= bsn_time_offset_cnt;

    case state is
      when s_dp_off =>
        nxt_src_out.bsn <= RESIZE_DP_BSN(bsn_init);
        nxt_bsn_time_offset_cnt <= (others => '0');
        nxt_sync <= '0';
        nxt_sync_size_cnt <= (others => '0');
        nxt_block_size_cnt <= (others => '0');
        if dp_on = '1' then
          nxt_sync <= '1';  -- ensure issue sync at first sync interval
          if dp_on_pps = '1' then
            -- start at pps
            if pps = '1' then
              nxt_state <= s_bsn_time_offset;
            end if;
          else
            -- start immediately
            nxt_state <= s_dp_on_sop;
          end if;
        end if;

      when s_bsn_time_offset =>
        if unsigned(bsn_time_offset_cnt) = unsigned(bsn_time_offset) then
          -- The bsn_time_offset can be 0, so IF UNSIGNED(bsn_time_offset)-1
          -- can not be used to account for on clk latency of state
          -- s_bsn_time_offset. Therefore do not count sync_size_cnt during
          -- latency of state s_bsn_time_offset.
          nxt_sync_size_cnt <= sync_size_cnt;
          nxt_state <= s_dp_on_sop;
        else
          nxt_bsn_time_offset_cnt <= INCR_UVEC(bsn_time_offset_cnt, 1);
        end if;

      -- using separate states s_dp_on_sop and s_dp_on_eop instead of only
      -- s_dp_on state and block_size_cnt, cause that g_block_size must be
      -- >= 3, but that is fine.
      when s_dp_on_sop =>
        -- Start of block
        nxt_src_out.sop <= '1';
        nxt_src_out.valid  <= '1';
        nxt_state <= s_dp_on;
        -- block_size_cnt = 0 at src_out.sop
        nxt_block_size_cnt <= (others => '0');
        -- after first block, increment bsn per block
        if prev_state = s_dp_on_eop then
          nxt_src_out.bsn <= INCR_DP_BSN(i_src_out.bsn, 1, g_bsn_w);
        end if;
        -- check for pending sync
        if sync = '1' then
          nxt_src_out.sync <= '1';
          nxt_sync <= '0';
        end if;

      when s_dp_on =>
        -- During block
        nxt_src_out.valid <= '1';
        -- block_size_cnt increments to determine end of block
        nxt_block_size_cnt <= INCR_UVEC(block_size_cnt, 1);
        if unsigned(block_size_cnt) >= g_block_size - 3 then
          nxt_state <= s_dp_on_eop;
        end if;

      when s_dp_on_eop =>
        -- End of block
        nxt_src_out.eop <= '1';
        nxt_src_out.valid <= '1';
        nxt_state <= s_dp_on_sop;
        -- block_size_cnt is dont care at at src_out.eop
        -- accept dp_off after eop, to avoid fractional blocks
        if dp_on = '0' then
          nxt_state <= s_dp_off;
        end if;

      when others =>  -- reover from undefined state
        nxt_state <= s_dp_off;
    end case;
  end process;

  -- src_out.valid transition from 0 to 1 is a bs_restart, use nxt_bs_restart
  -- to have bs_restart at first src_out.sync and src_out.sop.
  nxt_bs_restart <= nxt_src_out.valid and not i_src_out.valid;

  i_bs_new_interval <= '1' when i_bs_restart = '1' else
                       '0' when i_src_out.sync = '1' else
                       reg_bs_new_interval;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      prev_state          <= s_dp_off;
      state               <= s_dp_off;
      i_src_out           <= c_dp_sosi_rst;
      sync_size_cnt       <= (others => '0');
      sync                <= '0';
      block_size_cnt      <= (others => '0');
      i_bs_restart        <= '0';
      reg_bs_new_interval <= '0';
      bsn_time_offset_cnt <= (others => '0');
    elsif rising_edge(clk) then
      prev_state          <= state;
      state               <= nxt_state;
      i_src_out           <= nxt_src_out;
      sync_size_cnt       <= nxt_sync_size_cnt;
      sync                <= nxt_sync;
      block_size_cnt      <= nxt_block_size_cnt;
      i_bs_restart        <= nxt_bs_restart;
      reg_bs_new_interval <= i_bs_new_interval;
      bsn_time_offset_cnt <= nxt_bsn_time_offset_cnt;
    end if;
  end process;
end rtl;
