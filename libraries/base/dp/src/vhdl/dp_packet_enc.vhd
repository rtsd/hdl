--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_packet_pkg.all;

-- Purpose: Encode DP sosi to DP packet.
-- Description:
--
--   Pass on the block of input SOSI data:
--
--                            -----------------------
--                            | data ...            |
--                            -----------------------
--                              sop             eop
--                              val   val  ...  val
--                              sync  sync ...  sync
--                              bsn
--                              channel
--                                              empty
--                                              err
--
--   into an output DP packet:
--
--          ----------------------------------------------
--          |chan| bsn        | data ...             |err|
--          ----------------------------------------------
--            sop                                     eop
--            val  val   val    val ...               val
--            channel_lo
--            sync
--
--   The data length (eop) and empty do not show as fields in the packet
--   because they are implitely known per channel.
--
--   The dp_packet_dec does the reverse.
--
--   The g_channel_lo parameter allows using the LSbit channel fields to be
--   used for defining different groups of DP packet streams. This is e.g. used
--   to map these groups to corresponding 'tlen' types in case of further
--   transport using UTH packets over a transceiver link.
--
-- Remark:
-- . This dp_packet_enc has a similar structure as uth_tx(rtl_hold).
-- . The snk_in.sync is encoded into the frame BSN field but also always passed
--   on via the src_out.sync field. The g_blk_sync determines how the sync is
--   passed on. Use the default g_blk_sync is FALSE for DP style, because then
--   the src_out.sync does not need to be AND-ed with the src_out.sop when it
--   is used to trigger an event. When g_blk_sync is TRUE then the sync is kept
--   until the sop of the next input frame. This is useful to mimic the LOFAR
--   style sync whereby the sync preceeds the sop.

entity dp_packet_enc is
  generic (
    g_data_w       : natural := 32;
    g_channel_lo   : natural := 0;
    g_blk_sync     : boolean := false  -- when false force src_out.sync to '0' after the sop (= DP style), else keep src_out.sync value after this sop so until the next sop (= LOFAR style)
  );
  port (
    rst       : in  std_logic;
    clk       : in  std_logic;
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_packet_enc;

architecture rtl of dp_packet_enc is
  constant c_channel_len    : natural := ceil_div(c_dp_packet_channel_w, g_data_w);
  constant c_bsn_len        : natural := ceil_div(c_dp_packet_bsn_w,     g_data_w);
  constant c_error_len      : natural := ceil_div(c_dp_packet_error_w,   g_data_w);
  constant c_len_max        : natural := c_bsn_len;  -- the BSN is the widest field

  constant c_channel_vec_w  : natural := c_channel_len * g_data_w;
  constant c_bsn_vec_w      : natural := c_bsn_len     * g_data_w;
  constant c_error_vec_w    : natural := c_error_len   * g_data_w;

  constant c_rl             : natural := 1;
  signal tb_ready_reg       : std_logic_vector(0 to c_rl);

  type t_state is (s_channel, s_bsn, s_data, s_error);

  signal state           : t_state;
  signal nxt_state       : t_state;

  signal cnt             : natural range 0 to c_len_max - 1;
  signal nxt_cnt         : natural;

  signal in_err          : std_logic_vector(c_error_vec_w - 1 downto 0);
  signal nxt_in_err      : std_logic_vector(in_err'range);

  signal hold_src_in     : t_dp_siso;
  signal src_buf         : t_dp_sosi;
  signal nxt_src_buf     : t_dp_sosi;
  signal next_src_buf    : t_dp_sosi;
  signal pend_src_buf    : t_dp_sosi;

  signal i_src_out       : t_dp_sosi;
  signal nxt_src_out     : t_dp_sosi;

  signal i_snk_out       : t_dp_siso;
begin
  src_out <= i_src_out;
  snk_out <= i_snk_out;

  -- Monitor sink valid input and sink ready output
  proc_dp_siso_alert(clk, snk_in, i_snk_out, tb_ready_reg);

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      in_err         <= (others => '0');
      src_buf        <= c_dp_sosi_rst;
      i_src_out      <= c_dp_sosi_rst;
      state          <= s_channel;
      cnt            <= c_channel_len - 1;
    elsif rising_edge(clk) then
      in_err         <= nxt_in_err;
      src_buf        <= nxt_src_buf;
      i_src_out      <= nxt_src_out;
      state          <= nxt_state;
      cnt            <= nxt_cnt;
    end if;
  end process;

  -- Hold input register
  nxt_src_buf <= next_src_buf;

  u_hold : entity work.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => snk_in,
    -- ST source
    src_in       => hold_src_in,
    next_src_out => next_src_buf,
    pend_src_out => pend_src_buf,
    src_out_reg  => src_buf
  );

  -- State machine
  p_state : process(state, cnt, src_in, pend_src_buf, i_src_out, in_err)
    variable v_in_channel  : std_logic_vector(c_channel_vec_w - 1 downto 0);
    variable v_in_bsn      : std_logic_vector(c_bsn_vec_w - 1 downto 0);  -- sync & bsn[46:0]
    variable v_in_err      : std_logic_vector(c_error_vec_w - 1 downto 0);
    variable v_out_channel : std_logic_vector(g_channel_lo - 1 downto 0);
  begin
    v_in_channel            := RESIZE_UVEC(pend_src_buf.channel(c_dp_stream_channel_w - 1 downto g_channel_lo), v_in_channel'length);
    v_in_bsn                := RESIZE_UVEC(pend_src_buf.bsn, v_in_bsn'length);
    v_in_bsn(v_in_bsn'high) := pend_src_buf.sync;  -- Transport SOSI sync via MSbit of BSN field
    v_in_err                := RESIZE_UVEC(pend_src_buf.err, v_in_err'length);
    v_out_channel           := pend_src_buf.channel(g_channel_lo - 1 downto 0);

    nxt_state         <= state;
    nxt_cnt           <= cnt;
    nxt_in_err        <= in_err;

    nxt_src_out       <= i_src_out;
    if g_blk_sync = false then
      nxt_src_out.sync <= '0';  -- when true use dp style sync that is only active at sop,
    end if;  -- else use LOFAR style block sync that keeps the sync until the next sop
    nxt_src_out.valid <= '0';
    nxt_src_out.sop   <= '0';
    nxt_src_out.eop   <= '0';

    -- default flow control
    i_snk_out         <= src_in;
    hold_src_in       <= src_in;

    case state is
      when s_channel =>
        i_snk_out.ready   <= not pend_src_buf.sop;  -- flush until sop, then stop further input and hold this sosi
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then
          if pend_src_buf.sop = '1' then  -- output SOSI channel field
            nxt_src_out.data  <= RESIZE_DP_DATA(v_in_channel((cnt + 1) * g_data_w - 1 downto cnt * g_data_w));
            nxt_src_out.valid <= '1';
            if cnt = c_channel_len - 1 then
              nxt_src_out.sync    <= pend_src_buf.sync;
              nxt_src_out.sop     <= '1';
              nxt_src_out.channel <= RESIZE_DP_CHANNEL(v_out_channel);
            end if;
            if cnt = 0 then
              nxt_cnt <= c_bsn_len - 1;
              nxt_state <= s_bsn;
            else
              nxt_cnt <= cnt - 1;
            end if;
          end if;
        end if;
      when s_bsn =>
        i_snk_out.ready   <= '0';
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then  -- output SOSI BSN field
          nxt_src_out.data  <= RESIZE_DP_DATA(v_in_bsn((cnt + 1) * g_data_w - 1 downto cnt * g_data_w));
          nxt_src_out.valid <= '1';
          if cnt = 0 then
            nxt_cnt <= c_error_len - 1;
            nxt_state <= s_data;
          else
            nxt_cnt <= cnt - 1;
          end if;
        end if;
      when s_data =>
        if src_in.ready = '1' then
          nxt_src_out.data  <= pend_src_buf.data;
          nxt_src_out.valid <= pend_src_buf.valid;
          if pend_src_buf.eop = '1' then
            nxt_in_err <= v_in_err;
            nxt_state <= s_error;
          end if;
        end if;
      when others =>  -- s_error
        i_snk_out.ready   <= not pend_src_buf.sop;  -- flush until next sop
        hold_src_in.ready <= '0';
        if src_in.ready = '1' then  -- output SOSI error field
          nxt_src_out.data  <= RESIZE_DP_DATA(in_err((cnt + 1) * g_data_w - 1 downto cnt * g_data_w));
          nxt_src_out.valid <= '1';
          if cnt = 0 then
            nxt_src_out.eop <= '1';
            nxt_cnt <= c_channel_len - 1;
            nxt_state <= s_channel;
          else
            nxt_cnt <= cnt - 1;
          end if;
        end if;
    end case;

    -- Pass on frame level flow control
    i_snk_out.xon   <= src_in.xon;
    hold_src_in.xon <= src_in.xon;
  end process;
end rtl;
