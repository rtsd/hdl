--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tech_transceiver_tx_rst is
  generic(
    g_nof_gx             : natural
    );
  port(
    tr_clk               : in  std_logic;
    tr_rst               : in  std_logic;
    trc_tx_pll_powerdown : out std_logic;
    tx_pll_locked        : in  std_logic_vector(g_nof_gx - 1 downto 0);
    trc_tx_digital_rst   : out std_logic;
    trc_tx_rdy           : out std_logic
   );
end tech_transceiver_tx_rst;

architecture rtl of tech_transceiver_tx_rst is
  -- The minimum pulse width in to keep trc_tx_pll_powerdown asserted = 1us (stratix iv device datasheet).
  -- At 156.25Mhz (6.4ns) this takes 156.25 cycles. As a lower data rate increases the tx_clk period,
  -- we're already on the safe side using this value.
  constant c_pll_powerdown   : natural := 157;  -- 156.25 cycles
  constant c_pll_powerdown_w : natural := ceil_log2(c_pll_powerdown);

  type t_state_enum is (s_init, s_power_down_pll, s_wait_for_pll_lock, s_trc_tx_rdy);

  signal state                    : t_state_enum;
  signal nxt_state                : t_state_enum;

  signal i_trc_tx_pll_powerdown   : std_logic;
  signal nxt_trc_tx_pll_powerdown : std_logic;

  signal i_trc_tx_digital_rst     : std_logic;
  signal nxt_trc_tx_digital_rst   : std_logic;

  signal i_trc_tx_rdy             : std_logic;
  signal nxt_trc_tx_rdy           : std_logic;

  signal cycle_cnt                : std_logic_vector(c_pll_powerdown_w - 1 downto 0);
  signal nxt_cycle_cnt            : std_logic_vector(c_pll_powerdown_w - 1 downto 0);

  signal trc_tx_pll_locked        : std_logic_vector(g_nof_gx - 1 downto 0);
begin
  trc_tx_pll_powerdown <= i_trc_tx_pll_powerdown;
  trc_tx_digital_rst   <= i_trc_tx_digital_rst;
  trc_tx_rdy           <= i_trc_tx_rdy;

  p_clk : process(tr_rst, tr_clk)
  begin
    if tr_rst = '1' then
      state                  <= s_init;
      i_trc_tx_pll_powerdown <= '1';
      i_trc_tx_digital_rst   <= '1';
      i_trc_tx_rdy           <= '0';
      cycle_cnt              <= (others => '0');
    elsif rising_edge(tr_clk) then
      state                  <= nxt_state;
      i_trc_tx_pll_powerdown <= nxt_trc_tx_pll_powerdown;
      i_trc_tx_digital_rst   <= nxt_trc_tx_digital_rst;
      i_trc_tx_rdy           <= nxt_trc_tx_rdy;
      cycle_cnt              <= nxt_cycle_cnt;
    end if;
  end process;

  p_state : process(state, i_trc_tx_pll_powerdown, i_trc_tx_digital_rst, i_trc_tx_rdy, cycle_cnt, trc_tx_pll_locked)
  begin
    nxt_state                <= state;
    nxt_trc_tx_pll_powerdown <= i_trc_tx_pll_powerdown;
    nxt_trc_tx_digital_rst   <= i_trc_tx_digital_rst;
    nxt_trc_tx_rdy           <= i_trc_tx_rdy;
    nxt_cycle_cnt            <= cycle_cnt;

    case state is
      when s_init =>
        nxt_state <= s_power_down_pll;

      when s_power_down_pll =>  -- Wait until we're sure the PLL is powered down, then power it up
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_pll_powerdown, c_pll_powerdown_w) then
          nxt_trc_tx_pll_powerdown <= '0';
          nxt_state                <= s_wait_for_pll_lock;
        end if;

      when s_wait_for_pll_lock =>  -- Wait until PLL is locked
        if andv(trc_tx_pll_locked) = '1' then
          nxt_trc_tx_digital_rst <= '0';
          nxt_state              <= s_trc_tx_rdy;
        end if;

      when s_trc_tx_rdy =>  -- Transmitter ready to use.
        nxt_trc_tx_rdy <= '1';

      when others =>
        nxt_state <= s_power_down_pll;
    end case;
  end process;

  gen_asyncs: for i in 0 to g_nof_gx - 1 generate
    u_async_pll_locked : entity common_lib.common_async
    generic map(
      g_rst_level => '0'
    )
    port map(
      rst  => tr_rst,
      clk  => tr_clk,
      din  => tx_pll_locked(i),
      dout => trc_tx_pll_locked(i)
    );
  end generate;
end rtl;
