-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Tb for common_mult_add2 architectures
-- Description:
--   The tb is self verifying.
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_common_mult_add2 is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string   := "RTL";
    g_in_dat_w         : natural := 5;
    g_out_dat_w        : natural := 11;  -- 2*g_in_dat_w+1
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18 for 'stratix4'
    g_add_sub          : string := "ADD";  -- or "SUB"
    g_pipeline_input   : natural := 1;
    g_pipeline_product : natural := 0;
    g_pipeline_adder   : natural := 1;
    g_pipeline_output  : natural := 1
  );
end tb_common_mult_add2;

architecture tb of tb_common_mult_add2 is
  constant clk_period    : time := 10 ns;
  constant c_pipeline    : natural := g_pipeline_input + g_pipeline_product + g_pipeline_adder + g_pipeline_output;
  constant c_nof_mult    : natural := 2;  -- fixed

  constant c_max_p         : integer :=  2**(g_in_dat_w - 1) - 1;
  constant c_min  : integer := -c_max_p;
  constant c_max_n         : integer := -2**(g_in_dat_w - 1);

  function func_result(in_a0, in_b0, in_a1, in_b1 : std_logic_vector) return std_logic_vector is
    -- From mti_numeric_std.vhd follows:
    -- . SIGNED * --> output width = 2 * input width
    -- . SIGNED + --> output width = largest(input width)
    constant c_in_w   : natural := g_in_dat_w;
    constant c_res_w  : natural := 2 * g_in_dat_w + ceil_log2(c_nof_mult);  -- use sufficiently large result width
    variable v_a0     : signed(c_in_w - 1 downto 0);
    variable v_b0     : signed(c_in_w - 1 downto 0);
    variable v_a1     : signed(c_in_w - 1 downto 0);
    variable v_b1     : signed(c_in_w - 1 downto 0);
    variable v_result : signed(c_res_w - 1 downto 0);
  begin
    -- Calculate expected result
    v_a0 := RESIZE_NUM(signed(in_a0), c_in_w);
    v_b0 := RESIZE_NUM(signed(in_b0), c_in_w);
    v_a1 := RESIZE_NUM(signed(in_a1), c_in_w);
    v_b1 := RESIZE_NUM(signed(in_b1), c_in_w);
    if g_add_sub = "ADD" then v_result := RESIZE_NUM(v_a0 * v_b0, c_res_w) + v_a1 * v_b1; end if;
    if g_add_sub = "SUB" then v_result := RESIZE_NUM(v_a0 * v_b0, c_res_w) - v_a1 * v_b1; end if;
    -- Wrap to avoid warning: NUMERIC_STD.TO_SIGNED: vector truncated
    if v_result >  2**(g_out_dat_w - 1) - 1 then v_result := v_result - 2**g_out_dat_w; end if;
    if v_result < - 2**(g_out_dat_w - 1)   then v_result := v_result + 2**g_out_dat_w; end if;
    return std_logic_vector(v_result);
  end;

  signal tb_end              : std_logic := '0';
  signal rst                 : std_logic;
  signal clk                 : std_logic := '0';
  signal in_a0               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b0               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_a1               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b1               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_a                : std_logic_vector(c_nof_mult * g_in_dat_w - 1 downto 0);
  signal in_b                : std_logic_vector(c_nof_mult * g_in_dat_w - 1 downto 0);

  signal out_result          : std_logic_vector(g_out_dat_w - 1 downto 0);  -- combinatorial result
  signal result_expected     : std_logic_vector(g_out_dat_w - 1 downto 0);  -- pipelined results
  signal result_rtl          : std_logic_vector(g_out_dat_w - 1 downto 0);
begin
  clk  <= not clk or tb_end after clk_period / 2;

  -- run 1 us
  p_in_stimuli : process
  begin
    rst <= '1';
    in_a0 <= TO_SVEC(0, g_in_dat_w);
    in_b0 <= TO_SVEC(0, g_in_dat_w);
    in_a1 <= TO_SVEC(0, g_in_dat_w);
    in_b1 <= TO_SVEC(0, g_in_dat_w);
    wait until rising_edge(clk);
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;
    rst <= '0';
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;

    -- Some special combinations
    in_a0 <= TO_SVEC(2, g_in_dat_w);
    in_b0 <= TO_SVEC(3, g_in_dat_w);
    in_a1 <= TO_SVEC(4, g_in_dat_w);
    in_b1 <= TO_SVEC(5, g_in_dat_w);
    wait until rising_edge(clk);
    in_a0 <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*p + p*p = 2pp
    in_b0 <= TO_SVEC(c_max_p, g_in_dat_w);
    in_a1 <= TO_SVEC(c_max_p, g_in_dat_w);
    in_b1 <= TO_SVEC(c_max_p, g_in_dat_w);
    wait until rising_edge(clk);
    in_a0 <= TO_SVEC(c_max_n, g_in_dat_w);  -- -p*-p + -p*-p = -2pp
    in_b0 <= TO_SVEC(c_max_n, g_in_dat_w);
    in_a1 <= TO_SVEC(c_max_n, g_in_dat_w);
    in_b1 <= TO_SVEC(c_max_n, g_in_dat_w);
    wait until rising_edge(clk);
    in_a0 <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*-p + p*-p =  = -2pp
    in_b0 <= TO_SVEC(c_max_n, g_in_dat_w);
    in_a1 <= TO_SVEC(c_max_p, g_in_dat_w);
    in_b1 <= TO_SVEC(c_max_n, g_in_dat_w);
    wait until rising_edge(clk);
    in_a0 <= TO_SVEC(c_max_p, g_in_dat_w);  -- p*(-p-1)_ + p*(-p-1) = -(2pp + 2p)
    in_b0 <= TO_SVEC(c_min, g_in_dat_w);
    in_a1 <= TO_SVEC(c_max_p, g_in_dat_w);
    in_b1 <= TO_SVEC(c_min, g_in_dat_w);
    wait until rising_edge(clk);
    in_a0 <= TO_SVEC(c_max_n, g_in_dat_w);  -- -p*(-p-1)_ + -p*(-p-1) = 2pp + 2p
    in_b0 <= TO_SVEC(c_min, g_in_dat_w);
    in_a1 <= TO_SVEC(c_max_n, g_in_dat_w);
    in_b1 <= TO_SVEC(c_min, g_in_dat_w);
    wait until rising_edge(clk);

    for I in 0 to 49 loop
      wait until rising_edge(clk);
    end loop;

    -- All combinations
    for I in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
      for J in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
        for K in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
          for L in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
            in_a0 <= TO_SVEC(I, g_in_dat_w);
            in_b0 <= TO_SVEC(J, g_in_dat_w);
            in_a1 <= TO_SVEC(K, g_in_dat_w);
            in_b1 <= TO_SVEC(L, g_in_dat_w);
            wait until rising_edge(clk);
          end loop;
        end loop;
      end loop;
    end loop;

    tb_end <= '1';
    wait;
  end process;

  in_a <= in_a1 & in_a0;
  in_b <= in_b1 & in_b0;

  out_result <= func_result(in_a0, in_b0, in_a1, in_b1);

  u_result : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_out_dat_w,
    g_out_dat_w      => g_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => out_result,
    out_dat => result_expected
  );

  u_dut_rtl : entity work.common_mult_add2
  generic map (
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_res_w            => g_out_dat_w,  -- g_in_a_w + g_in_b_w + log2(2)
    g_force_dsp        => g_force_dsp,  -- not applicable for 'rtl'
    g_add_sub          => g_add_sub,
    g_nof_mult         => 2,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_adder   => g_pipeline_adder,
    g_pipeline_output  => g_pipeline_output
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => '1',
    in_a    => in_a,
    in_b    => in_b,
    res     => result_rtl
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        assert result_rtl = result_expected
          report "Error: wrong RTL result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
