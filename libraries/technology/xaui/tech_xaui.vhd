--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tech_xaui is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_sim             : boolean := false;
    g_sim_level       : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_xaui        : natural := 1  -- Up to 3 (hard XAUI only) supported
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                    : in  std_logic;  -- 156.25 MHz
    tr_rst                    : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk               : in  std_logic;

    -- MM interface
    mm_clk                    : in  std_logic;
    mm_rst                    : in  std_logic;

    xaui_mosi                 : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso                 : out t_mem_miso;

    -- XGMII interface
    tx_clk_arr                : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- 156.25 MHz, externally connect tr_clk to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    rx_clk_arr_out            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    rx_clk_arr_in             : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay

    txc_tx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- tx_ready in tx_clk_arr domain, can be used for xon flow control
    rxc_rx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_ready in rx_clk_arr domain, typically leave not connected

    txc_rx_channelaligned_arr : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- rx_channelaligned in tx_clk_arr domain, indicates that all 4 RX channels are aligned when asserted

    xgmii_tx_dc_arr           : in  t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);
    xgmii_rx_dc_arr           : out t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);

    -- Serial I/O
    xaui_tx_arr               : out t_xaui_arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr               : in  t_xaui_arr(g_nof_xaui - 1 downto 0)
  );
end tech_xaui;

architecture str of tech_xaui is
  constant c_use_technology : boolean := g_sim = false or g_sim_level = 0;
  constant c_use_sim_model  : boolean := not c_use_technology;
begin
  gen_ip_stratixiv : if c_use_technology = true and g_technology = c_tech_stratixiv generate
    u0 : entity work.tech_xaui_stratixiv
    generic map (g_sim, g_nof_xaui)
    port map (tr_clk, tr_rst, cal_rec_clk, mm_clk, mm_rst, xaui_mosi, xaui_miso,
              tx_clk_arr, rx_clk_arr_out, rx_clk_arr_in, txc_tx_ready_arr, rxc_rx_ready_arr, txc_rx_channelaligned_arr,
              xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              xaui_tx_arr, xaui_rx_arr);
  end generate;

  gem_sim_xaui : if c_use_sim_model = true or g_technology /= c_tech_stratixiv generate
    u0 : entity work.sim_xaui
    generic map (g_sim, g_nof_xaui)
    port map (tr_clk, tr_rst, cal_rec_clk, mm_clk, mm_rst, xaui_mosi, xaui_miso,
              tx_clk_arr, rx_clk_arr_out, rx_clk_arr_in, txc_tx_ready_arr, rxc_rx_ready_arr, txc_rx_channelaligned_arr,
              xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              xaui_tx_arr, xaui_rx_arr);
  end generate;
end str;
