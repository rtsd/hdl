-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi-testbench for regression test of all tb in unb1_board library
-- Description:
-- Usage:
--   > as 4
--   > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_tb_unb1_board_regression is
end tb_tb_tb_unb1_board_regression;

architecture tb of tb_tb_tb_unb1_board_regression is
begin
  u_tb_mms_unb1_board_sens           : entity work.tb_mms_unb1_board_sens;
  u_tb_unb1_board_mesh_reorder_bidir : entity work.tb_unb1_board_mesh_reorder_bidir;
end tb;
