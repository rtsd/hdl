--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra, 1 May 2018
-- Purpose:
-- . Reshape the block size by redefining the sop and eop for all streams.
-- Description:
--   See dp_block_reshape.vhd for functional description.
--
--   This dp_block_reshape_arr instantiate dp_block_reshape g_nof_streams times.
--   When g_shared_control = TRUE then stream 0 is used to control all streams.
--   When g_shared_control = FALSE then each stream controls itself, independent
--   of the other streams.
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_reshape_arr is
  generic (
    g_nof_streams              : natural := 1;  -- nof streams
    g_shared_control           : boolean := false;  -- when TRUE then use snk_in_arr(0) for control, else use per snk_in_arr stream
    g_input_nof_data_per_sync  : natural := 2;  -- nof data per input sync interval, used only for sop_index
    g_reshape_nof_data_per_blk : natural := 2;  -- nof data per output block
    g_pipeline_src_out         : natural := 1;  -- Pipeline source outputs (all sosi)
    g_pipeline_src_in          : natural := 0  -- Pipeline source inputs (all siso). This will also pipeline src_out.
  );
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;

    snk_in_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr      : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr       : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_index_2arr_2 : out t_natural_2arr_2(g_nof_streams - 1 downto 0)  -- [1] sop index, [0] valid index per stream
  );
end dp_block_reshape_arr;

architecture str of dp_block_reshape_arr is
  signal in_sosi_arr  : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal in_siso_arr  : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal out_sosi_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal out_siso_arr : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal in_sosi      : t_dp_sosi;
  signal in_siso      : t_dp_siso;
  signal out_sosi     : t_dp_sosi;
  signal out_siso     : t_dp_siso;

  signal out_index_2arr_2 : t_natural_2arr_2(g_nof_streams - 1 downto 0);  -- [1] sop index, [0] valid index per stream
begin
  -- Dependent on g_shared_control use stream(0) to control all streams or use separate control per stream
  in_sosi  <= snk_in_arr(0);
  in_siso  <= in_siso_arr(0);
  out_sosi <= out_sosi_arr(0);
  out_siso <= src_in_arr(0);

  p_in : process(snk_in_arr, in_sosi, in_siso, in_siso_arr)
  begin
    -- in_sosi_arr
    if g_shared_control = false then
      in_sosi_arr <= snk_in_arr;
    else
      in_sosi_arr <= (others => in_sosi);
      for I in 0 to g_nof_streams - 1 loop
        in_sosi_arr(I).data <= snk_in_arr(I).data;
        in_sosi_arr(I).re   <= snk_in_arr(I).re;
        in_sosi_arr(I).im   <= snk_in_arr(I).im;
      end loop;
    end if;

    -- snk_out_arr
    if g_shared_control = false then
      snk_out_arr <= in_siso_arr;
    else
      snk_out_arr <= (others => in_siso);
    end if;
  end process;

  p_out : process(out_sosi_arr, out_sosi, out_siso, src_in_arr)
  begin
    -- src_out_arr
    if g_shared_control = false then
      src_out_arr <= out_sosi_arr;
    else
      src_out_arr <= (others => out_sosi);
      for I in 0 to g_nof_streams - 1 loop
        src_out_arr(I).data <= out_sosi_arr(I).data;
        src_out_arr(I).re   <= out_sosi_arr(I).re;
        src_out_arr(I).im   <= out_sosi_arr(I).im;
      end loop;
    end if;

    -- out_siso_arr
    if g_shared_control = false then
      out_siso_arr <= src_in_arr;
    else
      out_siso_arr <= (others => out_siso);
    end if;
  end process;

  -- Instantiate dp_block_reshape per stream
  gen_streams : for I in 0 to g_nof_streams - 1 generate
    u_block_reshape : entity work.dp_block_reshape
    generic map (
      g_input_nof_data_per_sync  => g_input_nof_data_per_sync,
      g_reshape_nof_data_per_blk => g_reshape_nof_data_per_blk,
      g_pipeline_src_out         => g_pipeline_src_out,
      g_pipeline_src_in          => g_pipeline_src_in
    )
    port map (
      clk           => clk,
      rst           => rst,

      snk_in        => in_sosi_arr(I),
      snk_out       => in_siso_arr(I),

      src_out       => out_sosi_arr(I),
      src_in        => out_siso_arr(I),
      src_index_arr => out_index_2arr_2(I)
    );
  end generate;

  -- Wire index arr
  src_index_2arr_2 <= out_index_2arr_2 when g_shared_control = false else (others => out_index_2arr_2(0));
end str;
