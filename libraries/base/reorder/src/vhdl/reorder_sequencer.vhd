-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Purpose: Providing address and control signals for external memory realizing a
--          reordering of the data. Based on alternating read and writes.
--
--
-- Description: The values of generic g_reorder_seq determine the rhythm of the
--              sequencer. The sequencer is based on the use of a dualpage memory.
--              While data is written in page 0, page 1 is read. The state machine
--              alternated between reads and writes: read periods and write periods.
--              The number of accesses in a read and write period is determined by the
--              values of the generics.
--              The generics are interpreted as addresses in the sosi clock-domain. If the
--              datarate and/or the datawidth of the memory interface is higher than the
--              sosi domain than the g_data_w_ratio generic can be used to specify the
--              that ratio. An example:
--
--                          SOSI DOMAIN    MEMORY DOMAIN (DDR3)
--              Datawidth      64 bits       256 bits
--              Clockrate     200 MHz        200 MHz
--
--              The g_data_w_ratio should be 4 to facilitate this transition

--              The meaning of the other generics is explained her:
--
--              - wr_chunksize is the number of samples that are written during a write access.
--                A write access always consists of 1 access.
--              - rd_chunksize is the number of samples that are read during a read access.(multiple read accesses can be perfromed during a read period!!!!)
--              - rd_nof_chunks is the number of read accesses performed during a read period
--              - rd_interval defines the number of blocks in between two consecutive reads.
--              - gapsize is the address space to be skipped in between two consecutive write or
--                read periods. Gapsize can be used to create byte-alignment in the memory.
--              - nof_blocks determines the number of write and read periods before the page
--                swap takes place. Note that this number should be a multiple of rd_chunksize.
--                Generally these number of blocks define a sync period.
--
--              The example shows the following configuration:
--
--              g_data_w_ratio  = 1
--              wr_chunksize    : POSITIVE := 8
--              rd_chunksize    : POSITIVE := 4;
--              rd_nof_chunks   : POSITIVE := 2;
--              rd_interval     : POSITIVE := 2;
--              gapsize         : NATURAL  := 0;
--              nof_blocks      : POSITIVE := 8;
--
--              The two columns represents the pages of the dual paged memory. The first column
--              is written, while the second column is read. After the first chunk is written(0)
--              to the write-page, the chunks (0,0) and (0,1) are read from the read-page. Then
--              the second chunk (1) is written and after that chunks (1,0) and (1,1) are read.
--
--              The wr_chunk_index and rd_chunk_index indicate in which order the chunks are
--              written and read. Note that we read twice as much chunks as that we write. That
--              is because the read chunks are smaller than the write chunks.
--
--                                                         wr     rd
--                                                         chunk  chunk
--                    WR             RD                    index  index
--                 --------       --------
--                 |0     |       |0,0   |  rd_chunksize   0      0
--   wr_chunksize  |      |       --------
--                 |      |       |2,0   |  rd_chunksize          4
--                 --------       --------
--        gapsize  |      |       |      |  gapsize
--                 --------       --------
--                 |1     |       |4,0   |                 1      8
--                 |      |       --------
--                 |      |       |6,0   |                        12
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |2     |       |0,1   |                 2      1
--                 |      |       --------
--                 |      |       |2,1   |                        5
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |3     |       |4,1   |                 3      9
--                 |      |       --------
--                 |      |       |6,1   |                        13
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |4     |       |1,0   |                 4      2
--                 |      |       --------
--                 |      |       |3,0   |                        6
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |5     |       |5,0   |                 5      10
--                 |      |       --------
--                 |      |       |7,0   |                        14
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |6     |       |1,1   |                 6      3
--                 |      |       --------
--                 |      |       |3,1   |                        7
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                 |7     |       |5,1   |                 7      11
--                 |      |       --------
--                 |      |       |7,1   |                        15
--                 --------       --------
--                 |      |       |      |
--                 --------       --------
--                  page 0         page 1
--
--              Alternately after every single write access of wr_chunksize to one page there are rd_nof_chunks read accesses of rd_chunksize to the other page.
--              A write block has size wr_chunksize + gapsize.
--              A page contains nof_blocks, so nof_blocks write accesses of wr_chunksize write a page and nof_blocks*rd_nof_chunks read accesses of rd_chunksize read a page.
--              When the last rd_chunksize is read the write acces pages and read access page swap.
--              The index in the write page indicate the write period(0-4) given by range nof_blocks
--              The first index in the read page indicates the read period(0-4) given by range nof_blocks
--              The second index in the read page indicates the chunknumber(0-1) given by range rd_nof_chunks within a block
--              Together the first and second index in the read page indicate the read order (0-9) of the chunks given by range nof_blocks*rd_nof_chunks.
--              One can see the influence of rd_interval = 2 in the fact that there is a gap of 2 blocks in between two consecutive chunks that are read.
--              The first chunck (0,0) = 0 is read from the block 0. The second chunk (0,1) = 1 is read from block 2. The third chunk (1,0) = 2 is read from block 4 and the last
--              chunk (4,1) = 9 is read from block 3.
--
-- Remarks:     1) If the g_data_w_ratio is used, be sure that the generics wr_chunksize,
--                 rd_chunksize, rd_nof_chunks, gapsize and nof_blocks are divisible by g_data_w_ratio.
--              2) wr_chunksize = rd_chunksize*rd_nof_chunks, so write one big chunk and read small chunks.
--                 It is not supported to write small chunks and read one big chunk.
--              3) Also be sure that nof_blocks is larger than or equal to rd_chunksize

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.reorder_pkg.all;

entity reorder_sequencer is
  generic (
    g_reorder_seq   : t_reorder_seq := c_reorder_seq;
    g_data_w_ratio  : positive := 1  -- (256/256) Ratio between datawidth of the memory controller and SOSI domain
 );  -- Used to determine the width of counters.
  port (
    -- Clocks and reset
    dp_rst      : in  std_logic;  -- reset synchronous with st_clk
    dp_clk      : in  std_logic;  -- other clock domain clock

    en_evt      : out std_logic;
    wr_not_rd   : out std_logic;

    address     : out std_logic_vector;
    burstsize   : out std_logic_vector;

    done        : in  std_logic
   );
end reorder_sequencer;

architecture rtl of reorder_sequencer is
  -- Rescale to memory addressing
  constant c_wr_chunksize       : positive := g_reorder_seq.wr_chunksize / g_data_w_ratio;
  constant c_rd_chunksize       : positive := g_reorder_seq.rd_chunksize / g_data_w_ratio;
  constant c_gapsize            : natural  := g_reorder_seq.gapsize / g_data_w_ratio;

  constant c_blocksize          : positive := c_wr_chunksize + c_gapsize;
  constant c_page_size          : positive := c_blocksize * g_reorder_seq.nof_blocks;
  constant c_mem_size           : positive := 2 * c_page_size;
  constant c_rd_block_increment : positive := c_blocksize * g_reorder_seq.rd_interval;

  type   state_type is (s_idle, s_write, s_first_write, s_wait_wr, s_read, s_wait_rd);

  type reg_type is record
    rd_page_offset   : natural;
    wr_page_offset   : natural;
    ddr3_en          : std_logic;
    wr_not_rd        : std_logic;
    rd_block_offset  : natural;
    rd_chunks_offset : natural;
    wr_block_offset  : natural;
    switch_cnt       : natural range 0 to g_reorder_seq.rd_nof_chunks;  -- Counter that counts the write and read accesses to determine the switch between read and write phase.
    page_cnt         : natural range 0 to g_reorder_seq.nof_blocks;  -- Counter that counts the number of write accesses to determine the page-swap.
    first_write      : std_logic;
    start_addr       : natural range 0 to c_mem_size-1;
    burstsize        : natural range 0 to largest(c_wr_chunksize, c_rd_chunksize);
    state            : state_type;  -- The state machine.
  end record;

  signal r, rin      : reg_type;
begin
  ---------------------------------------------------------------
  -- CHECK IF PROVIDED GENERICS ARE ALLOWED.
  ---------------------------------------------------------------
  assert g_reorder_seq.wr_chunksize = g_reorder_seq.rd_nof_chunks * g_reorder_seq.rd_chunksize
    report "Total write configuration is different from total read configuration!!!"
    severity FAILURE;
  assert g_reorder_seq.nof_blocks >= g_reorder_seq.rd_chunksize
    report "The nof_blocks must be larger than or equal to the rd_chunksize!!!"
    severity FAILURE;
  assert g_reorder_seq.wr_chunksize rem g_data_w_ratio = 0
    report "The wr_chunksize must divisible by the g_data_w_ratio!!!"
    severity FAILURE;
  assert g_reorder_seq.rd_chunksize rem g_data_w_ratio = 0
    report "The rd_chunksize must divisible by the g_data_w_ratio!!!"
    severity FAILURE;
  assert g_reorder_seq.gapsize rem g_data_w_ratio = 0
    report "The gapsize must divisible by the g_data_w_ratio!!!"
    severity FAILURE;
  assert g_reorder_seq.nof_blocks rem g_data_w_ratio = 0
    report "The nof_blocks must divisible by the g_data_w_ratio!!!"
    severity FAILURE;

  p_comb : process(r, dp_rst, done)
    variable v : reg_type;
  begin
    v         := r;
    v.ddr3_en := '0';

    case r.state is
      when s_idle =>
        if(done = '1') then
          v.first_write := '1';
          v.state       := s_first_write;
        end if;

      when s_first_write =>
        v.wr_not_rd  := '1';
        v.ddr3_en    := '1';
        v.start_addr := r.wr_page_offset + r.wr_block_offset;
        v.burstsize  := c_wr_chunksize;
        v.state      := s_wait_wr;

      when s_write =>
        if(done = '1') then
          v.wr_not_rd  := '1';
          v.ddr3_en    := '1';
          v.start_addr := r.wr_page_offset + r.wr_block_offset;
          v.burstsize  := c_wr_chunksize;
          v.state      := s_wait_wr;
        end if;

      when s_wait_wr =>
        v.wr_block_offset := r.wr_block_offset + c_blocksize;
        v.page_cnt        := r.page_cnt + 1;
        v.switch_cnt      := 0;
        v.state           := s_read;

      when s_read =>
        if(done = '1') then
          v.wr_not_rd := '0';
          if( r.first_write = '0') then
            v.ddr3_en := '1';
          end if;
          v.start_addr  := r.rd_page_offset + r.rd_block_offset + r.rd_chunks_offset;
          v.burstsize   := c_rd_chunksize;
          v.switch_cnt  := r.switch_cnt + 1;
          v.state       := s_wait_rd;
        end if;

      when s_wait_rd =>

        v.rd_block_offset := r.rd_block_offset + c_rd_block_increment;

        if(r.rd_block_offset + c_rd_block_increment >= c_page_size) then
          v.rd_chunks_offset := r.rd_chunks_offset + c_rd_chunksize;
          v.rd_block_offset  := r.rd_block_offset + c_rd_block_increment - c_page_size;
        end if;

        if(r.switch_cnt = g_reorder_seq.rd_nof_chunks) then
          v.switch_cnt := 0;
          v.state      := s_write;
          if(r.page_cnt = g_reorder_seq.nof_blocks) then
            v.rd_page_offset   := r.wr_page_offset;
            v.wr_page_offset   := r.rd_page_offset;
            v.page_cnt         := 0;
            v.first_write      := '0';
            v.rd_block_offset  := 0;
            v.rd_chunks_offset := 0;
            v.wr_block_offset  := 0;
          end if;
        else
          v.state := s_read;
        end if;

      when others =>
        v.state := s_idle;
    end case;

    if(dp_rst = '1') then
      v.rd_page_offset   := c_page_size;
      v.wr_page_offset   := 0;
      v.page_cnt         := 0;
      v.switch_cnt       := 0;
      v.ddr3_en          := '0';
      v.wr_not_rd        := '0';
      v.wr_block_offset  := 0;
      v.rd_block_offset  := 0;
      v.rd_chunks_offset := 0;
      v.start_addr       := 0;
      v.burstsize        := 0;
      v.first_write      := '1';
      v.state            := s_idle;
    end if;

    rin <= v;
  end process;

  p_regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  en_evt      <= r.ddr3_en;
  wr_not_rd   <= r.wr_not_rd;
  address     <= TO_UVEC(r.start_addr, address'length);
  burstsize   <= TO_UVEC(r.burstsize,  burstsize'length);
end rtl;
