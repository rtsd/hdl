-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . This package contains ring specific constants.
-- Description:
-- Remark:
-------------------------------------------------------------------------------
library ieee, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;

package ring_pkg is
-- lane info, see https://support.astron.nl/confluence/x/jyu7Ag
  type t_ring_lane_info is record
    transport_nof_hops : std_logic_vector(c_word_w - 1 downto 0);
    lane_direction     : std_logic;
  end record;

  constant c_ring_lane_info_rst : t_ring_lane_info :=
      ( (others => '0'), '0' );

  constant c_ring_lane_info_field_arr : t_common_field_arr(1 downto 0) :=
      ( (field_name_pad("transport_nof_hops"), "RW", 32, field_default(0)),
        (field_name_pad("lane_direction"),     "RO",  1, field_default(0)) );

-- ring info, see https://support.astron.nl/confluence/x/jyu7Ag
  type t_ring_info is record
    O_rn                     : std_logic_vector(c_byte_w - 1 downto 0);
    N_rn                     : std_logic_vector(c_byte_w - 1 downto 0);
    use_cable_to_next_rn     : std_logic;
    use_cable_to_previous_rn : std_logic;
  end record;

  constant c_ring_info_rst : t_ring_info :=
      ( (others => '0'), (others => '0'), '0', '0' );

  constant c_ring_info_field_arr : t_common_field_arr(3 downto 0) :=
      ( (field_name_pad("O_rn"),                     "RW", 8, field_default( 0)),
        (field_name_pad("N_rn"),                     "RW", 8, field_default(16)),
        (field_name_pad("use_cable_to_next_rn"),     "RW", 1, field_default( 0)),
        (field_name_pad("use_cable_to_previous_rn"), "RW", 1, field_default( 0)) );

  constant c_ring_eth_dst_mac : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := x"FFFFFFFFFFFF";
  constant c_ring_eth_src_mac : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := x"002286080000";
  constant c_ring_pkt_type_bf : std_logic_vector(c_halfword_w - 1 downto 0) := x"10FB";
  constant c_ring_pkt_type_xc : std_logic_vector(c_halfword_w - 1 downto 0) := x"10FC";
  constant c_ring_pkt_type_so : std_logic_vector(c_halfword_w - 1 downto 0) := x"10FD";
  constant c_ring_pkt_type_tb : std_logic_vector(c_halfword_w - 1 downto 0) := x"10FE";

  constant c_ring_eth_nof_hdr_fields : natural := 3;
  constant c_ring_eth_hdr_field_sel  : std_logic_vector(c_ring_eth_nof_hdr_fields - 1 downto 0) := "000";
  constant c_ring_eth_hdr_field_arr : t_common_field_arr(c_ring_eth_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("eth_dst_mac"  ), "RW", 48, field_default(c_ring_eth_dst_mac) ),
      ( field_name_pad("eth_src_mac"  ), "RW", 48, field_default(c_ring_eth_src_mac) ),
      ( field_name_pad("eth_type"     ), "RW", 16, field_default(0) )
  );
  constant c_ring_eth_hdr_field_size : natural := ceil_div(field_slv_len(c_ring_eth_hdr_field_arr), c_longword_w);  -- = 14/8 = 2 longwords

  constant c_ring_dp_nof_hdr_fields : natural := 6;
  constant c_ring_dp_hdr_field_sel  : std_logic_vector(c_ring_dp_nof_hdr_fields - 1 downto 0) := "000" & "000";
  constant c_ring_dp_hdr_field_arr : t_common_field_arr(c_ring_dp_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("eth_dst_mac"  ), "RW", 48, field_default(c_ring_eth_dst_mac) ),
      ( field_name_pad("eth_src_mac"  ), "RW", 48, field_default(c_ring_eth_src_mac) ),
      ( field_name_pad("eth_type"     ), "RW", 16, field_default(0) ),
      ( field_name_pad("dp_channel"   ), "RW", 16, field_default(0) ),
      ( field_name_pad("dp_sync"      ), "RW",  1, field_default(0) ),
      ( field_name_pad("dp_bsn"       ), "RW", 63, field_default(0) )
  );
  constant c_ring_dp_hdr_field_size : natural := ceil_div(field_slv_len(c_ring_dp_hdr_field_arr), c_longword_w);  -- = 24/8 = 3 longwords

  function func_ring_nof_hops_to_source_rn(hops, this_rn, N_rn, lane_dir : natural) return natural;
  function func_ring_nof_hops_to_source_rn(hops, this_rn, N_rn : std_logic_vector; lane_dir : natural) return std_logic_vector;  -- return vector length is same as hops vector length
 end package ring_pkg;

package body ring_pkg is
  function func_ring_nof_hops_to_source_rn(hops, this_rn, N_rn, lane_dir : natural) return natural is
    variable v_source_rn     : integer;
    variable v_source_rn_nat : natural;
  begin

    if lane_dir > 0 then  -- transport in positive direction (even lanes)
      v_source_rn := this_rn - hops;
    else  -- transport in negative direction (odd lanes)
      v_source_rn := this_rn + hops;
    end if;

    if v_source_rn < 0 then  -- Cannot use MOD as N_rn is not a constant.
      v_source_rn := v_source_rn + N_rn;
    end if;

    if v_source_rn >= N_rn then
      v_source_rn := v_source_rn - N_rn;
    end if;

    v_source_rn_nat := v_source_rn;
    return v_source_rn_nat;
  end;

  function func_ring_nof_hops_to_source_rn(hops, this_rn, N_rn : std_logic_vector; lane_dir : natural) return std_logic_vector is
  begin
    return TO_UVEC(func_ring_nof_hops_to_source_rn(TO_UINT(hops), TO_UINT(this_rn), TO_UINT(N_rn), lane_dir), hops'length);
  end;
end ring_pkg;
