# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and other 
# software and tools, and its AMPP partner logic functions, and any output 
# files any of the foregoing (including device programming or simulation 
# files), and any associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License Subscription 
# Agreement, Altera MegaCore Function License Agreement, or other applicable 
# license agreement, including, without limitation, that your use is for the 
# sole purpose of programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the applicable 
# agreement for further details.



proc error_and_exit {msg} {
   post_message -type error "SCRIPT_ABORTED!!!"
   foreach line [split $msg "\n"] {
      post_message -type error $line
   }
   qexit -error
}

proc show_usage_and_exit {argv0} {
   post_message -type error  "USAGE: $argv0 \[device_name\]"
   qexit -error
}

set argv0 "quartus_sh -t [info script]"
set args $quartus(args)

if {[llength $args] == 1 } {
   set force_device [string toupper [string trim [lindex $args 0]]]
} else {
   set force_device ""
}

if {[llength $args] > 1} {
   show_usage_and_exit $argv0
}

if {[string compare -nocase $quartus(nameofexecutable) "quartus"] == 0} {
   set gui_mode 1
} else {
   set gui_mode 0
}

set script_path [file dirname [file normalize [info script]]]

source "$script_path/params.tcl"

if {[is_project_open]} {

   set curr_family [get_global_assignment -name FAMILY]
   if {[string compare -nocase $curr_family $ip_params(SYS_INFO_DEVICE_FAMILY)] == 0} {
      if {$force_device == ""} {
         set force_device [get_global_assignment -name DEVICE]
      }
   }

   post_message "Closing currently opened project..."
   project_close
}


set ex_design_path         "$script_path/qii"
set system_name            $ed_params(SYNTH_QSYS_NAME)
set qsys_file              "${system_name}.qsys"
set family                 $ip_params(SYS_INFO_DEVICE_FAMILY)

set toolkit_setting        $ip_params(DIAG_EXPORT_SEQ_AVALON_SLAVE)
set use_toolkit            [string equal $toolkit_setting "CAL_DEBUG_EXPORT_MODE_JTAG"]
set use_tg_avl_2           $ip_params(DIAG_USE_TG_AVL_2)

if {$force_device == ""} {
   set device $ed_params(DEFAULT_DEVICE)
} else {
   set device $force_device
}

post_message " "
post_message "*************************************************************************"
post_message "Altera External Memory Interface IP Example Design Builder"
post_message " "
post_message "Type  : Quartus Prime Project"
post_message "Family: $family"
post_message "Device: $device"
post_message " "
post_message "This script takes ~1 minute to execute..."
post_message "*************************************************************************"
post_message " "

if {[file isdirectory $ex_design_path]} {
   error_and_exit "Directory $ex_design_path has already been generated.\nThis script cannot overwrite generated example designs.\nIf you would like to regenerate the design by re-running the script, please remove the directory."
}

file mkdir $ex_design_path
file copy -force "${script_path}/$qsys_file" "${ex_design_path}/$qsys_file"

if {[file exists "${script_path}/quartus.ini"]} {
   file copy -force "${script_path}/quartus.ini" "${ex_design_path}/quartus.ini"
}

post_message "Generating example design files..."

set ip_generate_exe_path "$::env(QUARTUS_ROOTDIR)/sopc_builder/bin/ip-generate"

cd $ex_design_path
exec -ignorestderr $ip_generate_exe_path --remove-qsys-generate-warning --file-set=QUARTUS_SYNTH --system-info=DEVICE=$device --output-directory=. --report-file=qip $qsys_file >>& ip_generate.out

file delete -force ip_generate.out

post_message "Creating Quartus Prime project..."
project_new -family $family -part $device $system_name
set_global_assignment -name QIP_FILE ${system_name}.qip
if {$use_tg_avl_2 && $use_toolkit} {
   set_global_assignment -name VERILOG_MACRO "\"ALTERA_EMIF_ENABLE_ISSP=1\""
}
project_close

if {$gui_mode} {
   project_open $system_name
}

post_message " "
post_message "*************************************************************************"
post_message "Successfully generated example design at the following location:"
post_message " "
post_message "   $ex_design_path"
post_message " "
post_message "*************************************************************************"
post_message " "
