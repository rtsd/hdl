library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_fir is
  generic (
    g_in_dat_w          : natural;
    g_out_dat_w         : natural;
    g_coef_dat_w        : natural;
--    g_nof_fir           : NATURAL;
--    g_nof_subbands      : NATURAL;
    g_nof_prefilter     : natural;
--    g_nof_polarizations : NATURAL;
    g_nof_taps          : natural;
    g_fir_nr            : natural
  );
  port(
    clk                 : in std_logic;
    rst                 : in std_logic;
    in_hor              : in std_logic_vector(g_in_dat_w - 1 downto 0);
    in_ver              : in std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val              : in std_logic;
    in_sync             : in std_logic;
    res_hor             : out std_logic_vector(g_out_dat_w - 1 downto 0);
    res_ver             : out std_logic_vector(g_out_dat_w - 1 downto 0);
    res_val             : out std_logic;
    res_sync            : out std_logic
  );
end pfs_fir;
