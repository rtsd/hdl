-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_distribute using random stimuli
-- Description:
-- . Data flow:
--     in_sosi_arr(0:n-1) --> dp_distribute(n,m) --> distribute(0:m-1)
--                        --> dp_distribute(m,n) --> out_sosi_arr(0:n-1)
--                                               <-- out_siso_arr(0:n-1)
--
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct output
--
-- Remark:
-- . Using g_code_channel_lo=TRUE works provided that the g_data_w is
--   sufficient with respect to g_nof_repeat * frame_len so that channel_lo
--   does not influence the incrementing data sequence. Otherwise the tb
--   would need to use DP packet encoder and decoders.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_distribute is
  generic (
    -- Try flow control
    g_in_en            : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_out_ready        : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
    g_nof_repeat       : natural := 100;  -- >= 1, number of frames
    -- Try DP link decoding or encoding
    g_code_channel_lo  : boolean := true;
    -- Try input FIFO for Tx distribute
    g_tx_use_fifo      : boolean := true;
    g_tx_fifo_fill     : natural := 15;
    -- Try input FIFO for Rx distribute
    g_rx_use_fifo      : boolean := true;
    g_rx_fifo_fill     : natural := 15;
    -- Stimuli settings
    g_data_init_offset : integer := 10;
    g_frame_len_offset : integer := 0;
    -- Try distribute settings
    g_nof_input        : natural := 4;  -- = n
    g_nof_serial       : natural := 3;  -- = m
    g_transpose        : boolean := false
  );
end tb_dp_distribute;

architecture tb of tb_dp_distribute is
  constant c_rl                          : natural := 1;
  constant c_data_init                   : integer := 0;
  constant c_frame_len                   : integer := 10;
  constant c_frame_len_max               : integer := c_frame_len + g_nof_input * g_frame_len_offset;
  constant c_pulse_active                : natural := 1;
  constant c_pulse_period                : natural := 7;
  constant c_nof_repeat_extra            : natural := sel_a_b(g_tx_use_fifo, 1, 0) * (ceil_div(g_tx_fifo_fill, c_frame_len) + 1) + sel_a_b(g_rx_use_fifo, 1, 0) * (ceil_div(g_rx_fifo_fill, c_frame_len) + 1);
  constant c_in_nof_repeat               : natural := g_nof_repeat + c_nof_repeat_extra;
  constant c_nof_expected                : natural := g_nof_repeat;
  constant c_sync_period                 : natural := 7;
  constant c_sync_offset                 : natural := 2;

  constant c_data_w                      : natural := 16;
  constant c_fifo_size                   : natural := 32;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
  constant c_link_channel_lo             : natural := ceil_log2(g_nof_serial);
  constant c_rx_use_fifo_link_channel_lo : boolean := not g_code_channel_lo;  -- FALSE when the link_channel_lo is coded in the CHAN field of the DP packet data, else it needs to go in parallel through the Rx FIFO

  subtype t_data_arr   is t_slv_16_arr(0 to g_nof_input - 1);  -- width 16 must match c_data_w
  type    t_rl_vec_arr is array (0 to g_nof_input - 1) of std_logic_vector(0 to c_rl);

  signal tb_end_vec        : std_logic_vector(0 to g_nof_input - 1) := (others => '0');
  signal tb_end            : std_logic;
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso_arr       : t_dp_siso_arr(0 to g_nof_input - 1);
  signal in_sosi_arr       : t_dp_sosi_arr(0 to g_nof_input - 1);

  signal serial_siso_arr   : t_dp_siso_arr(0 to g_nof_serial - 1);
  signal serial_sosi_arr   : t_dp_sosi_arr(0 to g_nof_serial - 1);

  signal out_siso          : t_dp_siso;
  signal out_siso_arr      : t_dp_siso_arr(0 to g_nof_input - 1);
  signal out_sosi_arr      : t_dp_sosi_arr(0 to g_nof_input - 1);

  signal verify_en         : std_logic_vector(0 to g_nof_input - 1) := (others => '0');
  signal verify_done       : std_logic_vector(0 to g_nof_input - 1) := (others => '0');
  signal verify_end        : std_logic_vector(0 to g_nof_input - 1) := (others => '0');
  signal count_eop         : t_integer_arr(0 to g_nof_input - 1) := (others => 0);
  signal prev_count_eop    : t_integer_arr(0 to g_nof_input - 1) := (others => 0);

  signal prev_out_ready    : t_rl_vec_arr;
  signal prev_out_data     : t_data_arr := array_init(c_data_init - 1, g_nof_input, g_data_init_offset);
  signal out_bsn           : t_data_arr;
  signal out_data          : t_data_arr;
  signal out_sync          : std_logic_vector(0 to g_nof_input - 1);
  signal out_val           : std_logic_vector(0 to g_nof_input - 1);
  signal out_sop           : std_logic_vector(0 to g_nof_input - 1);
  signal out_eop           : std_logic_vector(0 to g_nof_input - 1);
  signal hold_out_sop      : std_logic_vector(0 to g_nof_input - 1);
  signal expected_out_data : t_data_arr;
begin
  tb_end <= vector_and(tb_end_vec);

  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;
  out_siso.xon   <= '1';

  out_siso_arr <= (others => out_siso);

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_input : for I in 0 to g_nof_input - 1 generate
    p_stimuli : process
      variable v_data_init  : natural;
      variable v_data_value : natural;
      variable v_frame_len  : natural;
      variable v_sync       : std_logic;
    begin
      v_data_init := c_data_init + I * g_data_init_offset;  -- offset the frame data   for each input, so the inputs can be recognized at the output
      v_frame_len := c_frame_len + I * g_frame_len_offset;  -- offset the frame length for each input, so the inputs can be recognized at the output
      in_sosi_arr(I) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);
      proc_common_wait_some_cycles(clk, 5);

      -- Pre-calculate the expected out_data for at the end of the stimuli
      expected_out_data(I) <= TO_UVEC(v_data_init + v_frame_len * c_nof_expected - 1, c_data_w);

      -- Begin of stimuli
      v_data_value := v_data_init;
      for R in 0 to c_in_nof_repeat - 1 loop
        v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_value, 0, 0, v_frame_len, 0, 0, v_sync, TO_DP_BSN(R), clk, in_en, in_siso_arr(I), in_sosi_arr(I));
        --proc_common_wait_some_cycles(clk, 10);
        v_data_value := v_data_value + v_frame_len;
      end loop;

      -- End of stimuli
      proc_common_wait_until_high(clk, verify_end(I));  -- in case of using FIFOs verify_done is issued somewhat before the output will stop due to c_nof_expected
      proc_common_wait_some_cycles(clk, 10 * c_frame_len_max);  -- continue some more after verify_done to see that the output has stopped
      tb_end_vec(I) <= '1';
      wait;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  gen_verify : for I in 0 to g_nof_input - 1 generate
    -- Verification logistics
    verify_en(I) <= '1'               when rising_edge(clk) and out_sosi_arr(I).sop = '1';  -- enable verify after first output sop
    count_eop(I) <= count_eop(I) + 1    when rising_edge(clk) and out_sosi_arr(I).eop = '1';  -- count number of output eop
    prev_count_eop(I) <= count_eop(I) when rising_edge(clk);
    verify_done(I) <= '1'             when rising_edge(clk) and count_eop(I) = c_nof_expected and count_eop(I) /= prev_count_eop(I) else
                      '0'             when rising_edge(clk);  -- signal verify done after c_nof_expected frames, use pulse to have only one assert from proc_dp_verify_value()
    verify_end(I) <= '1'              when rising_edge(clk) and verify_done(I) = '1';  -- signal verify_end after verify_done pulse, use level to ensure tb_end even if the stimuli are still busy

    -- Actual verification of the output streams
    proc_dp_verify_data("out_sosi_arr.data", c_rl, clk, verify_en(I), out_siso_arr(I).ready, out_sosi_arr(I).valid, out_data(I), prev_out_data(I));  -- Verify that the output is incrementing data, like the input stimuli
    proc_dp_verify_valid(c_rl, clk, verify_en(I), out_siso_arr(I).ready, prev_out_ready(I), out_sosi_arr(I).valid);  -- Verify that the output valid fits with the output ready latency
    proc_dp_verify_sop_and_eop(clk, out_sosi_arr(I).valid, out_sosi_arr(I).sop, out_sosi_arr(I).eop, hold_out_sop(I));  -- Verify that sop and eop come in pairs
    proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en(I), out_sosi_arr(I).sync, out_sosi_arr(I).sop, out_sosi_arr(I).bsn);

    gen_at_least : if g_tx_use_fifo = true or g_tx_use_fifo = true generate
      -- Use at least because there will still be some frames in the FIFO due to the required fill level
      proc_dp_verify_value(e_at_least, clk, verify_done(I), expected_out_data(I), prev_out_data(I));  -- Verify that the stimuli have been applied at all
    end generate;

    gen_equal : if g_tx_use_fifo = false and g_tx_use_fifo = false generate
      -- Use equal because without FIFOs all frames must come through
      proc_dp_verify_value(e_equal, clk, verify_done(I), expected_out_data(I), prev_out_data(I));  -- Verify that the stimuli have been applied at all
    end generate;

    -- Monitoring
    out_data(I) <= out_sosi_arr(I).data(c_data_w - 1 downto 0);
    out_bsn(I)  <= out_sosi_arr(I).bsn(c_data_w - 1 downto 0);
    out_sync(I) <= out_sosi_arr(I).sync;
    out_val(I)  <= out_sosi_arr(I).valid;
    out_sop(I)  <= out_sosi_arr(I).sop;
    out_eop(I)  <= out_sosi_arr(I).eop;
  end generate;

  ------------------------------------------------------------------------------
  -- DUT dp_distribute
  ------------------------------------------------------------------------------

  -- n --> m
  tx : entity work.dp_distribute
  generic map (
    -- Distribution IO
    g_tx              => true,
    g_nof_input       => g_nof_input,
    g_nof_output      => g_nof_serial,
    g_transpose       => g_transpose,
    g_code_channel_lo => g_code_channel_lo,
    g_data_w          => c_data_w,
    -- Input FIFO
    g_use_fifo        => g_tx_use_fifo,
    g_bsn_w           => c_data_w,
    g_empty_w         => 1,
    g_channel_w       => 1,
    g_error_w         => 1,
    g_use_bsn         => g_tx_use_fifo,
    g_use_empty       => false,
    g_use_channel     => false,
    g_use_error       => false,
    g_use_sync        => g_tx_use_fifo,
    g_fifo_fill       => g_tx_fifo_fill,
    g_fifo_size       => c_fifo_size
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => in_siso_arr,
    snk_in_arr  => in_sosi_arr,
    -- ST source
    src_in_arr  => serial_siso_arr,
    src_out_arr => serial_sosi_arr
  );

  -- m --> n
  rx : entity work.dp_distribute
  generic map (
    -- Distribution IO
    g_tx              => false,
    g_nof_input       => g_nof_serial,
    g_nof_output      => g_nof_input,
    g_transpose       => g_transpose,
    g_code_channel_lo => g_code_channel_lo,
    g_data_w          => c_data_w,
    -- Input FIFO
    g_use_fifo        => g_rx_use_fifo,
    g_bsn_w           => c_data_w,
    g_empty_w         => 1,
    g_channel_w       => c_link_channel_lo,  -- c_link_channel_lo-1 DOWNTO 0
    g_error_w         => 1,
    g_use_bsn         => g_rx_use_fifo,
    g_use_empty       => false,
    g_use_channel     => c_rx_use_fifo_link_channel_lo,
    g_use_error       => false,
    g_use_sync        => g_rx_use_fifo,
    g_fifo_fill       => g_rx_fifo_fill,
    g_fifo_size       => c_fifo_size
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => serial_siso_arr,
    snk_in_arr  => serial_sosi_arr,
    -- ST source
    src_in_arr  => out_siso_arr,
    src_out_arr => out_sosi_arr
  );
end tb;
