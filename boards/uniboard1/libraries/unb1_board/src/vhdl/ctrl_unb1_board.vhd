-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide general control infrastructure
-- Usage: In a design <design_name>.vhd that consists of:
--   . sopc_<design_name>.vhd with a Nios2 and the MM bus and the peripherals
--   . ctrl_unb1_board.vhd with e.g. 1GbE and PPS
--   . node_<design_name>.vhd with the actual functionality of <design_name>

library IEEE, common_lib, mm_lib, dp_lib, ppsh_lib, i2c_lib, technology_lib, tech_tse_lib, eth_lib, remu_lib, epcs_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;
use i2c_lib.i2c_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use eth_lib.eth_pkg.all;

entity ctrl_unb1_board is
  generic (
    ----------------------------------------------------------------------------
    -- General
    ----------------------------------------------------------------------------
    g_technology      : natural := c_tech_stratixiv;
    g_sim             : boolean := false;
    g_sim_level       : natural := 0;  -- when g_sim = TRUE, then 0 = use IP; 1 = use fast serdes model
    g_sim_flash_model : boolean := false;  -- only maximum one instance should have the flash model (see mms_epcs.vhd description)
    g_base_ip         : std_logic_vector(16 - 1 downto 0) := X"0A63";  -- Base IP address used by unb_osy: 10.99.xx.yy
    g_design_name     : string := "UNUSED  ";
    g_fw_version      : t_unb1_board_fw_version := (0, 0);  -- firmware version x.y
    g_stamp_date      : natural := 0;
    g_stamp_time      : natural := 0;
    g_stamp_svn       : integer := 0;
    g_design_note     : string  := "UNUSED";
    g_mm_clk_freq     : natural := c_unb1_board_mm_clk_freq_125M;  -- default use same MM clock as for TSE clock
    g_xo_clk_use_pll  : boolean := false;
    g_dp_clk_use_xo_pll : boolean := false;  -- Use the 200MHz XO PLL output (no external CLK)

    ----------------------------------------------------------------------------
    -- External CLK
    ----------------------------------------------------------------------------
    g_dp_clk_freq    : natural := c_unb1_board_ext_clk_freq_200M;
    g_dp_clk_use_pll : boolean := true;
    -- PLL phase clk shift with respect to CL
    --     STRING :=    "0"             = 0
    --     STRING :=  "156"             = 011.25
    --     STRING :=  "313"             = 022.5
    --     STRING :=  "469"             = 033.75
    --     STRING :=  "625"             = 045
    --     STRING :=  "781"             = 056.25
    --     STRING :=  "938"             = 067.5
    --     STRING := "1094"             = 078.75
    --     STRING := "1250"             = 090
    --     STRING := "1406" = 1250+ 156 = 101.25
    --     STRING := "1563" = 1250+ 313 = 112.5
    --     STRING := "1719" = 1250+ 469 = 123.75
    --     STRING := "1875" = 1250+ 625 = 135
    --     STRING := "2031" = 1250+ 781 = 146.25
    --     STRING := "2188" = 1250+ 938 = 157.5
    --     STRING := "2344" = 1250+1094 = 168.75
    --     STRING := "2500" = 1250+1250 = 180
    --     STRING := "2656" = 2500+ 156 = 191.25
    --     STRING := "2813" = 2500+ 313 = 202.5
    --     STRING := "2969" = 2500+ 469 = 213.75
    --     STRING := "3125" = 2500+ 625 = 225
    --     STRING := "3281" = 2500+ 781 = 236.25
    --     STRING := "3438" = 2500+ 938 = 247.5
    --     STRING := "3594" = 2500+1094 = 258.75
    --     STRING := "3750" = 2500+1250 = 270
    --     STRING := "3906" = 3750+ 156 = 281.25
    --     STRING := "4063" = 3750+ 313 = 292.5
    --     STRING := "4219" = 3750+ 469 = 303.75
    --     STRING := "4375" = 3750+ 625 = 315
    --     STRING := "4531" = 3750+ 781 = 326.25
    --     STRING := "4688" = 3750+ 938 = 337.5
    --     STRING := "4844" = 3750+1094 = 348.75
    --     STRING := "5000" = 3750+1250 = 360
    g_dp_clk_phase         : string := "0";  -- phase offset for PLL c0, typically any phase is fine, do not use 225 +-30 degrees because there the PPS edge occurs
    g_dp_phs_clk_vec_w     : natural := 0;  -- >= 0 and <= 6, nof extra PLL output clocks dp_phs_clk_vec[5:0] = [c6, c5, c4, c3, c2, c1]
    g_dp_phs_clk_divide_by : natural := 32;  -- divided by factor for dp_phs_clk_vec[5:0]

    ----------------------------------------------------------------------------
    -- PPS delay
    -- . Maximum number of dp_clk cycles that pps can be delayed. Actual number
    --   is determined dynamically by MM register reg_common_pulse_delay.
    ----------------------------------------------------------------------------
    g_pps_delay_max      : natural := 0;

    ----------------------------------------------------------------------------
    -- Use PHY Interface
    ----------------------------------------------------------------------------
    -- TYPE t_c_unb1_board_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   eth10g  : NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
    g_use_phy        : t_c_unb1_board_use_phy := (1, 0, 0, 0, 0, 0, 0, 1);

    ----------------------------------------------------------------------------
    -- 1GbE UDP offload
    ----------------------------------------------------------------------------
    g_udp_offload             : boolean := false;
    g_udp_offload_nof_streams : natural := c_eth_nof_udp_ports;

    ----------------------------------------------------------------------------
    -- Auxiliary Interface
    ----------------------------------------------------------------------------
    g_fpga_temp_high : natural := 85;
    g_app_led_red    : boolean := false;  -- when TRUE use external LED control via app_led_red
    g_app_led_green  : boolean := false;  -- when TRUE use external LED control via app_led_green

    g_aux            : t_c_unb1_board_aux := c_unb1_board_aux;
    g_epcs_protect_addr_range : boolean := false
  );
  port (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    cs_sim                 : out std_logic;
    xo_clk                 : out std_logic;  -- 25 MHz ETH_clk
    xo_rst                 : out std_logic;
    xo_rst_n               : out std_logic;

    mm_clk                 : in  std_logic := '0';  -- from QSYS
    mm_clk_out             : out std_logic;

    mm_locked              : in  std_logic := '0';  -- from QSYS
    mm_locked_out          : out std_logic;
    mm_rst                 : out std_logic;

    epcs_clk               : in  std_logic := '0';  -- from QSYS
    epcs_clk_out           : out std_logic;

    dp_rst                 : out std_logic;
    dp_clk                 : out std_logic;  -- 200 MHz from CLK system clock
    dp_phs_clk_vec         : out std_logic_vector(g_dp_phs_clk_vec_w - 1 downto 0);  -- divided and phase shifted from 200 MHz CLK system clock when a PLL is used
    dp_pps                 : out std_logic;  -- PPS in dp_clk domain
    dp_rst_in              : in  std_logic;  -- externally wire OUT dp_rst to dp_rst_in to avoid delta cycle difference on dp_clk
    dp_clk_in              : in  std_logic;  -- externally wire OUT dp_clk to dp_clk_in to avoid delta cycle difference on dp_clk

    cal_rec_clk            : out std_logic;

    this_chip_id           : out std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- [2:0], so range 0-3 for FN and range 4-7 for BN
    this_bck_id            : out std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);  -- [1:0] used out of ID[7:3] to index boards 3..0 in subrack

    app_led_red            : in std_logic := '0';
    app_led_green          : in std_logic := '1';

    -- PIOs
    pout_debug_wave        : in  std_logic_vector(c_word_w - 1 downto 0) := (others => '0');  -- can be removed !!, has no more function
    pout_wdi               : in  std_logic;  -- Toggled by unb_osy; can be overriden by reg_wdi.

    -- Manual WDI override
    reg_wdi_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_wdi_miso           : out t_mem_miso;

    -- REMU
    reg_remu_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_remu_miso          : out t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dpmm_data_miso     : out t_mem_miso;
    reg_dpmm_ctrl_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dpmm_ctrl_miso     : out t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mmdp_data_miso     : out t_mem_miso;
    reg_mmdp_ctrl_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mmdp_ctrl_miso     : out t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_epcs_miso          : out t_mem_miso;

    -- MM buses to/from mms_unb1_board_system_info
    reg_unb_system_info_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_unb_system_info_miso : out t_mem_miso;

    rom_unb_system_info_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    rom_unb_system_info_miso : out t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_unb_sens_miso      : out t_mem_miso;

    -- PPSH
    reg_ppsh_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ppsh_miso          : out t_mem_miso;

    -- Pulse (PPS) delay
    reg_common_pulse_delay_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_common_pulse_delay_miso : out t_mem_miso := c_mem_miso_rst;

    -- eth1g control&monitoring
    eth1g_tse_clk_out      : out std_logic;
    eth1g_tse_clk          : in  std_logic;
    eth1g_mm_rst           : in  std_logic;
    eth1g_tse_mosi         : in  t_mem_mosi;  -- ETH TSE MAC registers
    eth1g_tse_miso         : out t_mem_miso;
    eth1g_reg_mosi         : in  t_mem_mosi;  -- ETH control and status registers
    eth1g_reg_miso         : out t_mem_miso;
    eth1g_reg_interrupt    : out std_logic;  -- Interrupt
    eth1g_ram_mosi         : in  t_mem_mosi;  -- ETH rx frame and tx frame memory
    eth1g_ram_miso         : out t_mem_miso;

    -- eth1g UDP streaming ports
    udp_tx_sosi_arr        : in  t_dp_sosi_arr(g_udp_offload_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
    udp_tx_siso_arr        : out t_dp_siso_arr(g_udp_offload_nof_streams - 1 downto 0);
    udp_rx_sosi_arr        : out t_dp_sosi_arr(g_udp_offload_nof_streams - 1 downto 0);
    udp_rx_siso_arr        : in  t_dp_siso_arr(g_udp_offload_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    --
    -- >>> Ctrl FPGA pins
    --
    -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(g_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(g_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(g_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC                : inout std_logic := '1';
    SENS_SD                : inout std_logic := '1';

    -- 1GbE Control Interface
    ETH_CLK                : in    std_logic;
    ETH_SGIN               : in    std_logic;
    ETH_SGOUT              : out   std_logic
  );
end ctrl_unb1_board;

architecture str of ctrl_unb1_board is
  constant c_use_flash   : boolean := g_sim = false or g_sim_flash_model = true;  -- enable on HW, disable to save simulation time when not used in tb

  constant c_rom_version : natural := 1;  -- Only increment when something changes to the register map of rom_system_info.

  -- g_sel=0 for clk200_pll.vhd     : used when ADUH is not used so g_dp_phs_clk_vec_w = g_nof_dp_phs_clk = 0
  -- g_sel=1 for clk200_pll_p6.vhd  : used when ADUH is     used so g_dp_phs_clk_vec_w = g_nof_dp_phs_clk > 0
  constant c_dp_clk_pll_sel : natural := sel_a_b(g_dp_phs_clk_vec_w = 0, 0, 1);

  -- Define c_dp_clk*_phase dependend on g_dp_phs_clk_vec_w in equal steps from 0 to 90 degrees of 200 MHz dp_clk.
  -- g_dp_clk_vec_w = g_nof_dp_phs_clk =                          0    1      2      3      4      5      6
  constant c_dp_clk1_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0",   "0",   "0",   "0",   "0",   "0");  -- degrees phase shifted PLL c1 output clock = dp_phs_clk_vec(0)
  constant c_dp_clk2_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0", "625", "313", "313", "156", "156");  -- degrees phase shifted PLL c2 output clock = dp_phs_clk_vec(1)
  constant c_dp_clk3_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0",   "0", "781", "625", "313", "313");  -- degrees phase shifted PLL c3 output clock = dp_phs_clk_vec(2)
  constant c_dp_clk4_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0",   "0",   "0", "938", "469", "469");  -- degrees phase shifted PLL c4 output clock = dp_phs_clk_vec(3)
  constant c_dp_clk5_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0",   "0",   "0",   "0", "625", "625");  -- degrees phase shifted PLL c5 output clock = dp_phs_clk_vec(4)
  constant c_dp_clk6_phase : string := sel_n(g_dp_phs_clk_vec_w, "0", "0",   "0",   "0",   "0",   "0", "938");  -- degrees phase shifted PLL c6 output clock = dp_phs_clk_vec(5)

  -- Simulation
  signal sim_mm_clk             : std_logic := '1';

  -- Clock and reset
  signal i_xo_clk               : std_logic;
  signal i_xo_rst               : std_logic;
  signal i_xo_rst_n             : std_logic;
  signal i_mm_rst               : std_logic;

  signal clk200M                : std_logic := '1';
  signal clk125M                : std_logic := '1';
  signal clk40M                 : std_logic := '1';
  signal clk50M                 : std_logic := '1';
  signal clk20M                 : std_logic := '1';

  signal mm_wdi                 : std_logic;
  signal eth1g_st_clk           : std_logic;
  signal eth1g_st_rst           : std_logic;

  signal ext_clk                : std_logic;
  signal ext_pps                : std_logic;
  signal dp_dis                 : std_logic;
  signal mms_ppsh_pps_sys       : std_logic;

  signal node_ctrl_dp_clk_in    : std_logic := '0';
  signal node_ctrl_dp_rst_out   : std_logic;

  signal mm_pulse_ms            : std_logic;
  signal mm_pulse_s             : std_logic;
  signal mm_board_sens_start    : std_logic;

  signal led_toggle             : std_logic;
  signal led_toggle_red         : std_logic;
  signal led_toggle_green       : std_logic;

  -- eth1g
  signal eth1g_led              : t_tech_tse_led;

  -- Manual WDI override
  signal wdi_override           : std_logic;

  -- Temperature alarm  (temp > g_fpga_temp_high)
  signal temp_alarm             : std_logic;

  -- UDP offload I/O
  signal eth1g_udp_tx_sosi_arr  : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_sosi_rst);
  signal eth1g_udp_tx_siso_arr  : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal eth1g_udp_rx_sosi_arr  : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal eth1g_udp_rx_siso_arr  : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rdy);
begin
  xo_clk     <= i_xo_clk;
  xo_rst     <= i_xo_rst;
  xo_rst_n   <= i_xo_rst_n;
  mm_rst     <= i_mm_rst;

  epcs_clk_out      <= clk20M;
  cal_rec_clk       <= clk40M;
  eth1g_tse_clk_out <= clk125M;

  -----------------------------------------------------------------------------
  -- Node set up
  -----------------------------------------------------------------------------

  -- xo_rst output for SOPC's altpll0 instance
  i_xo_rst <= not i_xo_rst_n;

  -- Default leave unused INOUT tri-state
  INTA <= 'Z';
  INTB <= 'Z';

  TESTIO <= (others => 'Z');  -- Leave unused INOUT tri-state

  -- Clock and reset
  i_xo_clk <= ETH_CLK;  -- use the 25 MHz from the ETH_CLK pin as xo_clk
  ext_clk  <= CLK;  -- use the external 200 MHz CLK as ext_clk
  ext_pps  <= PPS;  -- use more special name for PPS pin signal to ease searching for it in editor

  dp_dis <= i_mm_rst;  -- could use software control for this instead

  gen_pll: if g_dp_clk_use_pll = true generate
    u_unb1_board_clk200_pll : entity work.unb1_board_clk200_pll
    generic map (
      g_technology          => g_technology,
      g_sel                 => c_dp_clk_pll_sel,
      g_clk200_phase_shift  => g_dp_clk_phase,
      g_clk_vec_w           => g_dp_phs_clk_vec_w,
      g_clk1_phase_shift    => c_dp_clk1_phase,  -- dp_phs_clk_vec(0)
      g_clk2_phase_shift    => c_dp_clk2_phase,  -- dp_phs_clk_vec(1)
      g_clk3_phase_shift    => c_dp_clk3_phase,  -- dp_phs_clk_vec(2)
      g_clk4_phase_shift    => c_dp_clk4_phase,  -- dp_phs_clk_vec(3)
      g_clk5_phase_shift    => c_dp_clk5_phase,  -- dp_phs_clk_vec(4)
      g_clk6_phase_shift    => c_dp_clk6_phase,  -- dp_phs_clk_vec(5)
      g_clk1_divide_by      => g_dp_phs_clk_divide_by,
      g_clk2_divide_by      => g_dp_phs_clk_divide_by,
      g_clk3_divide_by      => g_dp_phs_clk_divide_by,
      g_clk4_divide_by      => g_dp_phs_clk_divide_by,
      g_clk5_divide_by      => g_dp_phs_clk_divide_by,
      g_clk6_divide_by      => g_dp_phs_clk_divide_by
    )
    port map (
      arst       => dp_dis,
      clk200     => ext_clk,
      st_clk200  => dp_clk,  -- = c0
      st_rst200  => dp_rst,
      st_clk_vec => dp_phs_clk_vec  -- PLL c6-c1
    );
  end generate;

  no_pll: if g_dp_clk_use_pll = false and g_dp_clk_use_xo_pll = false generate
    dp_clk              <= ext_clk;

    dp_rst              <= node_ctrl_dp_rst_out;
    node_ctrl_dp_clk_in <= dp_clk_in;
  end generate;

  -- for UNB1 designs with SOPC g_xo_clk_use_pll=FALSE and the the SOPC generates and outputs the mm_clk
  -- for UNB1 designs with QSYS g_xo_clk_use_pll=TRUE and this ctrl_unb1_board generates and outputs the mm_clk
  gen_clk25_pll: if g_xo_clk_use_pll = true generate
    gen_sim : if g_sim = true generate
      sim_mm_clk <= not sim_mm_clk after c_mmf_mm_clk_period / 2;
    end generate;

    mm_clk_out <= sim_mm_clk when g_sim = true else
                  clk125M    when g_mm_clk_freq = c_unb1_board_mm_clk_freq_125M else
                  clk50M     when g_mm_clk_freq = c_unb1_board_mm_clk_freq_50M else
                  clk50M;

    gen_dp_clk : if g_dp_clk_use_xo_pll = true and g_dp_clk_use_pll = false generate
      dp_clk <= clk200M;

      dp_rst              <= node_ctrl_dp_rst_out;
      node_ctrl_dp_clk_in <= dp_clk_in;
    end generate;

    u_unb1_board_clk25_pll : entity work.unb1_board_clk25_pll
    generic map (
      g_technology => g_technology
    )
    port map (
      arst       => i_xo_rst,
      clk25      => i_xo_clk,
      c0_clk20   => clk20M,
      c1_clk40   => clk40M,
      c2_clk50   => clk50M,
      c3_clk125  => clk125M,
      c4_clk200  => clk200M,
      pll_locked => mm_locked_out
    );
  end generate;

  u_unb1_board_node_ctrl : entity work.unb1_board_node_ctrl
  generic map (
    g_pulse_us => g_mm_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
  )
  port map (
    xo_clk      => i_xo_clk,
    xo_rst_n    => i_xo_rst_n,
    sys_clk     => mm_clk,
    sys_locked  => mm_locked,
    sys_rst     => i_mm_rst,
    cal_clk     => '0',
    cal_rst     => OPEN,
    st_clk      => node_ctrl_dp_clk_in,
    st_rst      => node_ctrl_dp_rst_out,
    wdi_in      => pout_wdi,
    wdi_out     => mm_wdi,  -- actively toggle the WDI via pout_wdi from software with toggle extend to allow software reload
    pulse_us    => OPEN,
    pulse_ms    => mm_pulse_ms,
    pulse_s     => mm_pulse_s  -- could be used to toggle a LED
  );

  -- System info
  cs_sim <= is_true(g_sim);

  u_mms_unb1_board_system_info : entity work.mms_unb1_board_system_info
  generic map (
    g_sim               => g_sim,
    g_design_name       => g_design_name,
    g_use_phy           => g_use_phy,
    g_fw_version        => g_fw_version,
    g_stamp_date        => g_stamp_date,
    g_stamp_time        => g_stamp_time,
    g_stamp_svn         => g_stamp_svn,
    g_design_note       => g_design_note,
    g_rom_version       => c_rom_version,
    g_technology        => g_technology
  )
  port map (
    mm_clk      => mm_clk,
    mm_rst      => i_mm_rst,

    hw_version  => VERSION,
    id          => ID,

    reg_mosi    => reg_unb_system_info_mosi,
    reg_miso    => reg_unb_system_info_miso,

    rom_mosi    => rom_unb_system_info_mosi,
    rom_miso    => rom_unb_system_info_miso,

    chip_id     => this_chip_id,
    bck_id      => this_bck_id
  );

  -----------------------------------------------------------------------------
  -- Red LED control
  -----------------------------------------------------------------------------

  gen_app_led_red: if g_app_led_red = true generate
    -- Let external app control the LED via the app_led_red input
    TESTIO(c_unb1_board_testio_led_red)   <= app_led_red;
  end generate;

  no_app_led_red: if g_app_led_red = false generate
    TESTIO(c_unb1_board_testio_led_red)   <= led_toggle_red;
  end generate;

  -----------------------------------------------------------------------------
  -- Green LED control
  -----------------------------------------------------------------------------

  gen_app_led_green: if g_app_led_green = true generate
    -- Let external app control the LED via the app_led_green input
    TESTIO(c_unb1_board_testio_led_green)   <= app_led_green;
  end generate;

  no_app_led_green: if g_app_led_green = false generate
    TESTIO(c_unb1_board_testio_led_green)   <= led_toggle_green;
  end generate;

  ------------------------------------------------------------------------------
  -- Toggle red LED when unb1_minimal is running, green LED for other designs.
  ------------------------------------------------------------------------------
  led_toggle_red <= sel_a_b(g_design_name(1 to 8) = "unb1_min", led_toggle, '0');
  led_toggle_green <= sel_a_b(g_design_name(1 to 8) /= "unb1_min", led_toggle, '0');

  u_toggle : entity common_lib.common_toggle
  port map (
    rst         => i_mm_rst,
    clk         => mm_clk,
    in_dat      => mm_pulse_s,
    out_dat     => led_toggle
  );

  ------------------------------------------------------------------------------
  -- WDI override
  ------------------------------------------------------------------------------
  -- Actively reset watchdog from software when used, else disable watchdog by leaving the WDI at tri-state level.
  -- A high temp_alarm will keep WDI asserted, causing the watch dog to reset the FPGA.
  -- A third option is to override the WDI manually using the output of a dedicated reg_wdi.
  WDI <= sel_a_b(g_use_phy.wdi, mm_wdi or temp_alarm or wdi_override, 'Z');

  u_unb1_board_wdi_reg : entity work.unb1_board_wdi_reg
  port map (
    mm_rst              => i_mm_rst,
    mm_clk              => mm_clk,

    sla_in              => reg_wdi_mosi,
    sla_out             => reg_wdi_miso,

    wdi_override        => wdi_override
  );

  ------------------------------------------------------------------------------
  -- Remote upgrade
  ------------------------------------------------------------------------------
  -- Every design instantiates an mms_remu instance + MM status & control ports.
  -- So there is full control over the memory mapped registers to set start address of the flash
  -- and reconfigure from that address.
  gen_mms_remu : if c_use_flash = true generate  -- enable on HW, disable to save simulation time when not used in tb
    u_mms_remu : entity remu_lib.mms_remu
    port map (
      mm_rst             => i_mm_rst,
      mm_clk             => mm_clk,

      epcs_clk           => epcs_clk,

      remu_mosi          => reg_remu_mosi,
      remu_miso          => reg_remu_miso
    );
  end generate;

  no_remu_in_sim : if c_use_flash = false generate
    reg_remu_miso <= c_mem_miso_rst;
  end generate;

  -----------------------------------------------------------------------------
  -- EPCS
  -----------------------------------------------------------------------------
  gen_mms_epcs : if c_use_flash = true generate  -- enable on HW, disable to save simulation time when not used in tb
    u_mms_epcs : entity epcs_lib.mms_epcs
    generic map (
      g_sim_flash_model    => g_sim_flash_model,
      g_protect_addr_range => g_epcs_protect_addr_range
    )
    port map (
      mm_rst             => i_mm_rst,
      mm_clk             => mm_clk,

      epcs_clk           => epcs_clk,

      epcs_mosi          => reg_epcs_mosi,
      epcs_miso          => reg_epcs_miso,

      dpmm_ctrl_mosi     => reg_dpmm_ctrl_mosi,
      dpmm_ctrl_miso     => reg_dpmm_ctrl_miso,

      dpmm_data_mosi     => reg_dpmm_data_mosi,
      dpmm_data_miso     => reg_dpmm_data_miso,

      mmdp_ctrl_mosi     => reg_mmdp_ctrl_mosi,
      mmdp_ctrl_miso     => reg_mmdp_ctrl_miso,

      mmdp_data_mosi     => reg_mmdp_data_mosi,
      mmdp_data_miso     => reg_mmdp_data_miso
    );
  end generate;

  no_epcs_in_sim : if c_use_flash = false generate
    reg_dpmm_ctrl_miso <= c_mem_miso_rst;
    reg_dpmm_data_miso <= c_mem_miso_rst;
    reg_mmdp_ctrl_miso <= c_mem_miso_rst;
    reg_mmdp_data_miso <= c_mem_miso_rst;
  end generate;

  ------------------------------------------------------------------------------
  -- PPS input
  ------------------------------------------------------------------------------
  u_mms_ppsh : entity ppsh_lib.mms_ppsh
  generic map (
    g_st_clk_freq     => g_dp_clk_freq
  )
  port map (
    -- Clocks and reset
    mm_rst           => i_mm_rst,
    mm_clk           => mm_clk,
    st_rst           => dp_rst_in,
    st_clk           => dp_clk_in,
    pps_ext          => ext_pps,  -- with unknown but constant phase to st_clk

    -- Memory-mapped clock domain
    reg_mosi         => reg_ppsh_mosi,
    reg_miso         => reg_ppsh_miso,

    -- Streaming clock domain
    pps_sys          => mms_ppsh_pps_sys
  );

  ------------------------------------------------------------------------------
  -- PPS delay
  ------------------------------------------------------------------------------
  gen_mms_common_pulse_delay : if g_pps_delay_max > 0 generate
    u_mms_common_pulse_delay : entity common_lib.mms_common_pulse_delay
    generic map (
      g_pulse_delay_max => g_pps_delay_max,
      g_register_out    => true
    )
    port map (
      pulse_clk => dp_clk_in,
      pulse_rst => dp_rst_in,
      pulse_in  => mms_ppsh_pps_sys,
      pulse_out => dp_pps,

      mm_clk    => mm_clk,
      mm_rst    => i_mm_rst,
      reg_mosi  => reg_common_pulse_delay_mosi,
      reg_miso  => reg_common_pulse_delay_miso
    );
  end generate;

  no_mms_common_pulse_delay : if g_pps_delay_max = 0 generate
    dp_pps <= mms_ppsh_pps_sys;
  end generate;

  ------------------------------------------------------------------------------
  -- I2C control for UniBoard sensors
  ------------------------------------------------------------------------------

  mm_board_sens_start <= mm_pulse_s when g_sim = false else mm_pulse_ms;  -- speed up in simulation

  u_mms_unb1_board_sens : entity work.mms_unb1_board_sens
  generic map (
    g_sim       => g_sim,
    g_clk_freq  => g_mm_clk_freq,
    g_temp_high => g_fpga_temp_high
  )
  port map (
    -- Clocks and reset
    mm_rst    => i_mm_rst,
    mm_clk    => mm_clk,
    mm_start  => mm_board_sens_start,

    -- Memory-mapped clock domain
    reg_mosi  => reg_unb_sens_mosi,
    reg_miso  => reg_unb_sens_miso,

    -- i2c bus
    scl       => sens_sc,
    sda       => sens_sd,

    -- Temperature alarm
    temp_alarm => temp_alarm
  );

  ------------------------------------------------------------------------------
  -- Ethernet 1GbE
  ------------------------------------------------------------------------------

  wire_udp_offload: for i in 0 to g_udp_offload_nof_streams - 1 generate
    eth1g_udp_tx_sosi_arr(i) <= udp_tx_sosi_arr(i);
    udp_tx_siso_arr(i)       <= eth1g_udp_tx_siso_arr(i);

    udp_rx_sosi_arr(i)       <= eth1g_udp_rx_sosi_arr(i);
    eth1g_udp_rx_siso_arr(i) <= udp_rx_siso_arr(i);
  end generate;

  -- In simulation use file IO for MM control. In simulation only use 1GbE for streaming DP data offload (or on load) via 1GbE.
  no_eth1g : if g_sim = true and g_udp_offload = false generate
    eth1g_reg_interrupt <= '0';
    eth1g_tse_miso <= c_mem_miso_rst;
    eth1g_reg_miso <= c_mem_miso_rst;
    eth1g_ram_miso <= c_mem_miso_rst;
  end generate;

  --On hardware always generate 1GbE for MM control. In simulation only use 1GbE for streaming DP data offload (or on load) via 1GbE.
  gen_eth1g : if g_sim = false or g_udp_offload = true generate
    eth1g_st_clk <= dp_clk_in when g_udp_offload = true else mm_clk;
    eth1g_st_rst <= dp_rst_in when g_udp_offload = true else eth1g_mm_rst;

    u_mac : entity eth_lib.eth
    generic map (
      g_technology         => g_technology,
      g_init_ip_address    => g_base_ip & X"0000",  -- Last two bytes set by board/FPGA ID.
      g_cross_clock_domain => g_udp_offload,
      g_sim                => g_sim,
      g_sim_level          => g_sim_level
    )
    port map (
      -- Clocks and reset
      mm_rst            => eth1g_mm_rst,  -- use reset from QSYS
      mm_clk            => mm_clk,  -- use mm_clk direct
      eth_clk           => eth1g_tse_clk,  -- use the dedicated 125 MHz tse_clock, independent of the mm_clk
      st_rst            => eth1g_st_rst,
      st_clk            => eth1g_st_clk,

      -- UDP transmit interface
      udp_tx_snk_in_arr  => eth1g_udp_tx_sosi_arr,
      udp_tx_snk_out_arr => eth1g_udp_tx_siso_arr,
      -- UDP receive interface
      udp_rx_src_in_arr  => eth1g_udp_rx_siso_arr,
      udp_rx_src_out_arr => eth1g_udp_rx_sosi_arr,

      -- Memory Mapped Slaves
      tse_sla_in        => eth1g_tse_mosi,
      tse_sla_out       => eth1g_tse_miso,
      reg_sla_in        => eth1g_reg_mosi,
      reg_sla_out       => eth1g_reg_miso,
      reg_sla_interrupt => eth1g_reg_interrupt,
      ram_sla_in        => eth1g_ram_mosi,
      ram_sla_out       => eth1g_ram_miso,

      -- PHY interface
      eth_txp           => ETH_SGOUT,
      eth_rxp           => ETH_SGIN,

      -- LED interface
      tse_led           => eth1g_led
    );
  end generate;
end str;
