/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */
#include <unbos_i2c.h>

/* Compile switches. */

/* includes */
#include "system.h"

#ifdef __AVS_I2C_MASTER
    // System has I2C 

    /* private constants (#define) */
    
    /* private macros */

    /* private types */

    /* private function protos */
    #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    static void i2c_isr(void* p)
    #else
    static void i2c_isr(void* p, alt_u32 id)
    #endif
    {
      // Read the control register to clear the in-tar-upt
      AVS_I2C_MASTER_REG_RD_CONTROL(AVS_I2C_MASTER_SENS_CONTROL_BASE);
    }

    /* private constants (const) */
    
    /* private variables */
    
    /* public variables */

    /* public function implements */
    void UNBOS_i2c_init(void) {
    #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
        alt_ic_isr_register(AVS_I2C_MASTER_SENS_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID,
                            AVS_I2C_MASTER_SENS_CONTROL_IRQ,
                            i2c_isr,
                            (void *)0,
                            (void *)0);
    #else
        alt_irq_register(AVS_I2C_MASTER_SENS_CONTROL_IRQ, (void *)0, i2c_isr);
    #endif
    }

#endif  // __AVS_I2C_MASTER

