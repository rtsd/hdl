--------------------------------------------------------------------------------
--
-- Author: Daniel van der Schuur
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Calculate the cross- and auto correlations of g_nof_inputs inputs for multiple channels using a specified number of multipliers
-- Description:
-- . Number of visibilities (nof_visibilities): (g_nof_inputs*(g_nof_inputs+1))/2
--   . Example: With 24 inputs, nof_visibilities = 24*(24+1)/2=300
-- . One visibility v for one channel of one input pair i0,i1 is calculated as follows: v=accumulate(i0*conj(i1))
-- . Serialization a.k.a folding:
--   . g_nof_mults*g_nof_cycles=nof_visibilities
--     . These 2 generics determine in how many clock cycles the visibilities are produced.
--     . Note: The product (nof_mults*nof_serialization_cycles) must be equal to nof_visibilities!
--     . Examples: (300, 1) = 300 mults* 1 clock cycle  (produce all 300 visibilities in one clock cycle)
--                 (150, 2) = 150 mults* 2 clock cycles
--                 (100, 3) = 100 mults* 3 clock cycles
--                 ( 75, 4) =  75 mults* 4 clock cycles
--                 ( 60, 5) =  60 mults* 5 clock cycles
--                 ( 50, 6) =  50 mults* 6 clock cycles
--                 ( 30,10) =  30 mults*10 clock cycles (produce all 300 visibilities in 10 clock cycles)
--   . When serializing the permutation pairs (g_nof_cycles>1), introduce gaps in the input data accordingly.
-- . Channels
--   . For each input stream, g_nof_channels are allowed to be fed in series.
-- . Accumulators
--   . For each (channel per) visibility, an individual accumulator is kept.
--   . The accumulators are emptied on snk_in(0).sync.
--     . To empty the accumulators, assert snk_in(0).sync on the last valid data of your integration period.
--   . g_nof_channel_accs determines the data width of the accumulators.

entity correlator_dev is
  generic (
    g_data_w           : natural;  -- Complex data width (of snk_in(i).re and snk_in(i).im)
    g_nof_inputs       : natural;  -- Number of inputs
    g_nof_mults        : natural;  -- Number of complex multipliers
    g_nof_cycles       : natural;  -- Number of cycles to produce all visibilities
    g_nof_channels     : natural;  -- Number of (serial) channels per (parallel) input
    g_nof_channel_accs : natural  -- The maximum  number of samples per channel that can be accumulated (determines accumulator width)
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);  -- Input data to be correlated must be valid at the same single cycle
    snk_out_arr    : out t_dp_siso_arr(g_nof_inputs - 1 downto 0);  -- snk_out_arr(i).ready is low during processing when g_nof_cycles>1

    src_out_arr    : out t_dp_sosi_arr(g_nof_mults - 1 downto 0)
  );
end correlator_dev;

architecture str of correlator_dev is
  constant c_nof_accumulators         : natural := g_nof_channels * g_nof_cycles;
  constant c_mult_output_w            : natural := 2 * g_data_w;
  constant c_acc_data_w               : natural := c_mult_output_w + ceil_log2(g_nof_channel_accs);

  signal corr_permutor_src_out_2arr_2 : t_dp_sosi_2arr_2(g_nof_mults - 1 downto 0);  -- Array of (serialized) pairs
  signal corr_multiplier_src_out_arr  : t_dp_sosi_arr(g_nof_mults - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Create all unique pair permutations of the input streams
  -- . Also performs serialization
  -----------------------------------------------------------------------------
  u_corr_permutor : entity work.corr_permutor
  generic map (
    g_nof_inputs   => g_nof_inputs,
    g_nof_outputs  => g_nof_mults,
    g_nof_cycles   => g_nof_cycles,
    g_data_w       => g_data_w
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_arr     => snk_in_arr,
    snk_out_arr    => snk_out_arr,

    src_out_2arr_2 => corr_permutor_src_out_2arr_2
  );

  -----------------------------------------------------------------------------
  -- Complex multiplier stage
  -----------------------------------------------------------------------------
  u_corr_multiplier : entity work.corr_multiplier
  generic map (
    g_nof_inputs => g_nof_mults,
    g_data_w     => g_data_w
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_2arr_2  => corr_permutor_src_out_2arr_2,
    src_out_arr    => corr_multiplier_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Accumulator stage
  -----------------------------------------------------------------------------
  u_corr_accumulator : entity work.corr_accumulator
  generic map (
    g_nof_inputs       => g_nof_mults,
    g_nof_channels     => c_nof_accumulators,
    g_nof_channel_accs => g_nof_channel_accs,
    g_data_w           => 2 * g_data_w  -- = Multiplier output data width
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_arr     => corr_multiplier_src_out_arr,
    src_out_arr    => src_out_arr
  );
end str;
