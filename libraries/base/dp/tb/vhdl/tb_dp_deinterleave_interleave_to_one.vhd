-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 29 Jan 2019
-- Purpose:
-- . Test bench for dp_deinterleave using dp_interleave_n_to_one.
-- Description:
--   Block diagram:
--
--    stimuli --> dp_deinterleave --> dp_interleave_n_to_one --> verify
--
-- Remark:
-- . Same tb as for dp_deinterleave_one_to_n but now for dp_interleave with:
--   - c_flow_control_verify = e_active, because dp_interleave does not
--     support backpressure flow control
--   - fixed g_block_size_int = 1, to fit dp_interleave_n_to_one
--   - fixed c_pipeline = 1, to fit dp_interleave
--   - fixed use FIFO to break flow control between 1 to N and N to 1,
--     because dp_deinterleave does not support backpressure
--
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_deinterleave_interleave_to_one is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    -- specific
    g_use_complex            : boolean := false;
    g_nof_repeat             : natural := 5;
    g_nof_streams            : natural := 4;
    g_pkt_len                : natural := 12;  -- typcially multiple of g_nof_streams
    g_pkt_gap                : natural := 10
  );
end tb_dp_deinterleave_interleave_to_one;

architecture tb of tb_dp_deinterleave_interleave_to_one is
  constant c_flow_control_verify      : t_dp_flow_control_enum := e_active;
  constant c_pipeline                 : natural := 1;  -- 0 for combinatorial, > 0 for registers

  -- Only verify the sosi data fields when g_pkt_len is an integer multiple of g_nof_streams,
  -- the sosi info and control fields are always verified using c_out_pkt_len
  --CONSTANT c_par_pkt_len              : NATURAL := ceil_div(g_pkt_len, g_nof_streams);
  constant c_par_pkt_len              : natural := g_pkt_len / g_nof_streams;
  constant c_out_pkt_len              : natural := c_par_pkt_len * g_nof_streams;
  constant c_verify_sosi_data_en      : boolean := sel_a_b(g_pkt_len mod g_nof_streams = 0, true, false);
  constant c_verify_data              : boolean := c_verify_sosi_data_en and not g_use_complex;
  constant c_verify_complex           : boolean := c_verify_sosi_data_en and     g_use_complex;

  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;
  constant c_stimuli_pulse_period     : natural := 7;

  constant c_fifo_size                : natural := g_nof_repeat * g_pkt_len;
  constant c_data_w                   : natural := 16;
  constant c_data_init                : natural := 0;
  constant c_re_init                  : natural := 7;
  constant c_im_init                  : natural := 17;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init                 : natural := 247;
  constant c_channel_init             : natural := 5;  -- fixed

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_flow_control_latency_pls : natural := g_nof_repeat * g_pkt_len * (c_verify_pulse_period * c_stimuli_pulse_period) / (c_stimuli_pulse_active * c_verify_pulse_active);
  constant c_flow_control_latency_rnd : natural := g_nof_repeat * g_pkt_len;
  constant c_flow_control_latency     : natural := sel_a_b(g_flow_control_stimuli = e_pulse or c_flow_control_verify = e_pulse,
                                                           c_flow_control_latency_pls,
                                                           c_flow_control_latency_rnd);  -- worst case value

  constant c_data_max                 : unsigned(c_data_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(c_data_w - 1 downto 0) := (others => '1');

  --CONSTANT c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_rst;  -- default 0 is no wrap
  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  -- both
  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(c_data_w - 1 downto 0);

  signal parallel_snk_out_arr       : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal parallel_snk_in_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal parallel_src_in_arr        : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal parallel_src_out_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(c_data_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_use_complex    => g_use_complex,
    g_data_init      => c_data_init,
    g_re_init        => c_re_init,
    g_im_init        => c_im_init,
    g_bsn_init       => c_bsn_init,
    g_err_init       => c_err_init,
    g_channel_init   => c_channel_init,
    -- specific
    g_in_dat_w       => c_data_w,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => g_pkt_len,
    g_pkt_gap        => g_pkt_gap,
    g_wait_last_evt  => c_flow_control_latency  -- number of clk cycles to wait with last_snk_in_evt after finishing the stimuli
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  -- Throttle stimuli to ensure active = 1, period = 3, level '1'
  --proc_common_gen_pulse(1, 3, '1', rst, clk, stimuli_src_in.ready);

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';  -- or '0'
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1' when c_verify_data    else '0';
  verify_snk_in_enable.re      <= '1' when c_verify_complex else '0';
  verify_snk_in_enable.im      <= '1' when c_verify_complex else '0';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '0';
  verify_snk_in_enable.err     <= '0';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;
  verify_last_snk_in_evt.data    <= last_snk_in_evt when c_verify_data    else '0';
  verify_last_snk_in_evt.re      <= last_snk_in_evt when c_verify_complex else '0';
  verify_last_snk_in_evt.im      <= last_snk_in_evt when c_verify_complex else '0';
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= '0';
  verify_last_snk_in_evt.err     <= '0';

  u_dp_stream_verify : entity work.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => c_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => c_data_w,
    g_pkt_len        => c_out_pkt_len
  )
  port map (
    rst                        => rst,
    clk                        => clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- DUT: 1 to N
  ------------------------------------------------------------------------------
  u_dp_deinterleave : entity work.dp_deinterleave
  generic map (
    g_dat_w             => c_data_w,
    g_nof_out           => g_nof_streams,
    g_block_size_int    => 1,
    g_block_size_output => c_par_pkt_len,  -- Output block size: The number of samles in the blocks at the output
    g_use_ctrl          => true,  -- Requires: [input block size (sop:eop)] / [g_nof_out]/ [g_block_size_output] = integer number!
    g_use_sync_bsn      => true,  -- forwards (stored) input Sync+BSN to all output streams
    g_use_complex       => g_use_complex,
    g_align_out         => true  -- Aligns the output streams
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_in      => stimuli_src_out,
    src_out_arr => parallel_snk_in_arr
  );

  -- Use FIFO to break flow control between one to N and N to one, so that stimuli see ready = '1'
  gen_fifos : for I in 0 to g_nof_streams - 1 generate
    u_dp_fifo_sc : entity work.dp_fifo_sc
    generic map (
      g_data_w         => c_data_w,
      g_bsn_w          => c_dp_stream_dsp_data_w,
      g_empty_w        => c_dp_stream_empty_w,
      g_channel_w      => c_dp_stream_channel_w,
      g_error_w        => c_dp_stream_error_w,
      g_use_bsn        => true,
      g_use_empty      => true,
      g_use_channel    => true,
      g_use_error      => true,
      g_use_sync       => true,
      g_use_ctrl       => true,  -- sop & eop
      g_use_complex    => g_use_complex,  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
      g_fifo_size      => c_fifo_size  -- (16+2) * 512 = 1 M9K, g_data_w+2 for sop and eop
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- Monitor FIFO filling
      -- ST sink
      snk_out     => parallel_snk_out_arr(I),
      snk_in      => parallel_snk_in_arr(I),
      -- ST source
      src_in      => parallel_src_in_arr(I),
      src_out     => parallel_src_out_arr(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- DUT: N to 1
  ------------------------------------------------------------------------------
  u_n_to_one: entity work.dp_interleave_n_to_one
  generic map (
    g_pipeline      => c_pipeline,
    g_nof_inputs    => g_nof_streams
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out_arr => parallel_src_in_arr,
    snk_in_arr  => parallel_src_out_arr,
    src_in      => verify_snk_out,
    src_out     => verify_snk_in
  );

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(c_data_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(c_data_w - 1 downto 0);
end tb;
