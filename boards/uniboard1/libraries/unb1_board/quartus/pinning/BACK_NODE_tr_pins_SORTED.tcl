###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
set_location_assignment PIN_N2 -to BN_BI_0_RX[0]                              #  GXB_RX_R8p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_RX[0]      # 
set_location_assignment PIN_M4 -to BN_BI_0_TX[0]                              #  GXB_TX_R8p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_TX[0]      #
                                                                            
set_location_assignment PIN_L2 -to BN_BI_0_RX[1]                              #  GXB_RX_R9p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_RX[1]      #
set_location_assignment PIN_K4 -to BN_BI_0_TX[1]                              #  GXB_TX_R9p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_TX[1]      #
                                                                            
set_location_assignment PIN_E2 -to BN_BI_0_RX[2]                              #  GXB_RX_R10p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_RX[2]      #
set_location_assignment PIN_D4 -to BN_BI_0_TX[2]                              #  GXB_TX_R10p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_TX[2]      #
                                                                          
set_location_assignment PIN_C2 -to BN_BI_0_RX[3]                              #  GXB_RX_R11p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_RX[3]      #
set_location_assignment PIN_B4 -to BN_BI_0_TX[3]                              #  GXB_TX_R11p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_0_TX[3]      #
                                                                      
set_location_assignment PIN_AE2 -to BN_BI_1_RX[0]                             #  GXB_RX_R4p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_RX[0]      #
set_location_assignment PIN_AD4 -to BN_BI_1_TX[0]                             #  GXB_TX_R4p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_TX[0]      #
                                                                       
set_location_assignment PIN_AC2 -to BN_BI_1_RX[1]                             #  GXB_RX_R5p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_RX[1]      #
set_location_assignment PIN_AB4 -to BN_BI_1_TX[1]                             #  GXB_TX_R5p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_TX[1]      #
                                                                      
set_location_assignment PIN_U2 -to BN_BI_1_RX[2]                              #  GXB_RX_R6p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_RX[2]      #
set_location_assignment PIN_T4 -to BN_BI_1_TX[2]                              #  GXB_TX_R6p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_TX[2]      #
                                                                       
set_location_assignment PIN_R2 -to BN_BI_1_RX[3]                              #  GXB_RX_R7p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_RX[3]      #
set_location_assignment PIN_P4 -to BN_BI_1_TX[3]                              #  GXB_TX_R7p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_1_TX[3]      #
                                                                      
set_location_assignment PIN_AU2 -to BN_BI_2_RX[0]                             #  GXB_RX_R0p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_RX[0]      #
set_location_assignment PIN_AT4 -to BN_BI_2_TX[0]                             #  GXB_TX_R0p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_TX[0]      #
                                                                      
set_location_assignment PIN_AR2 -to BN_BI_2_RX[1]                             #  GXB_RX_R1p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_RX[1]      #
set_location_assignment PIN_AP4 -to BN_BI_2_TX[1]                             #  GXB_TX_R1p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_TX[1]      #
                                                                              
set_location_assignment PIN_AJ2 -to BN_BI_2_RX[2]                             #  GXB_RX_R2p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_RX[2]      #
set_location_assignment PIN_AH4 -to BN_BI_2_TX[2]                             #  GXB_TX_R2p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_TX[2]      #
                                                                     
set_location_assignment PIN_AG2 -to BN_BI_2_RX[3]                             #  GXB_RX_R3p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_RX[3]      #
set_location_assignment PIN_AF4 -to BN_BI_2_TX[3]                             #  GXB_TX_R3p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_2_TX[3]      #
                                                                             
set_location_assignment PIN_AN2 -to BN_BI_3_RX[0]                             #  GXB_CMURX_R0p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_RX[0]      #
set_location_assignment PIN_AM4 -to BN_BI_3_TX[0]                             #  GXB_CMUTX_R0p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_TX[0]      #
                                                                            
set_location_assignment PIN_AL2 -to BN_BI_3_RX[1]                             #  GXB_CMURX_R1p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_RX[1]      #
set_location_assignment PIN_AK4 -to BN_BI_3_TX[1]                             #  GXB_CMUTX_R1p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_TX[1]      #
                                                                             
set_location_assignment PIN_J2 -to BN_BI_3_RX[2]                              #  GXB_CMURX_R4p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_RX[2]      #
set_location_assignment PIN_H4 -to BN_BI_3_TX[2]                              #  GXB_CMUTX_R4p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_TX[2]      #
                                                                              
set_location_assignment PIN_G2 -to BN_BI_3_RX[3]                              #  GXB_CMURX_R5p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_RX[3]      #
set_location_assignment PIN_F4 -to BN_BI_3_TX[3]                              #  GXB_CMUTX_R5p
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to BN_BI_3_TX[3]      #









