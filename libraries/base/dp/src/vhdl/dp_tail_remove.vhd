--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
--
-- Purpose: Remove tail from an input block
-- Description:
--   A shift register is used to remove the tail from a stream and output the
--   stripped stream on src_out.
-- Remark:
-- . If g_symbol_w = g_data_w then it is better to use dp_block_resize.vhd,
--   because that provides a simpler solution. The dp_block_resize.vhd can
--   remove 0 or more head data and 0 or more tail data from the input block.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_tail_remove is
  generic (
    g_data_w      : natural;
    g_symbol_w    : natural;
    g_nof_symbols : natural  -- Nof symbols to be stripped from end of the packet,
                            -- and accounting for the nof empty symbols.
  );
  port (
    st_rst  : in  std_logic;
    st_clk  : in  std_logic;

    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;

    src_in : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_tail_remove;

architecture str of dp_tail_remove is
  constant c_output_reg     : boolean := true;  -- register dp_shiftreg output to ease timing closure
  constant c_tail_nof_words : natural := ceil_div(g_nof_symbols * g_symbol_w, g_data_w);

  -- The number of words we need to buffer equals c_tail_nof_words, plus one extra because we need to move the EOP to
  -- the last valid data word.
  constant c_nof_shiftreg_words : natural := c_tail_nof_words + 1;

  signal rd_sosi_arr        : t_dp_sosi_arr(0 to c_nof_shiftreg_words - 1);
  signal wr_sosi_arr        : t_dp_sosi_arr(0 to c_nof_shiftreg_words - 1);
begin
  snk_out <= src_in;

  u_src_shift : entity work.dp_shiftreg
  generic map (
    g_output_reg     => c_output_reg,
    g_flush_eop      => true,
    g_modify_support => true,
    g_nof_words      => c_nof_shiftreg_words
  )
  port map (
    rst                 => st_rst,
    clk                 => st_clk,
    -- ST sink
    snk_out             => OPEN,
    snk_in              => snk_in,
    -- Control shift register contents
    cur_shiftreg_inputs => rd_sosi_arr,
    new_shiftreg_inputs => wr_sosi_arr,
    -- ST source
    src_in              => src_in,  -- We correct the stream via new_shiftreg_inputs, so
    src_out             => src_out  -- the shiftreg sources everything but the tail.
  );

  p_shift: process(rd_sosi_arr)
    variable v_wr_sosi_arr : t_dp_sosi_arr(0 to c_nof_shiftreg_words - 1);
  begin
    v_wr_sosi_arr := rd_sosi_arr;

    if rd_sosi_arr(0).eop = '1' then
      for i in 0 to c_nof_shiftreg_words - 2 loop
        v_wr_sosi_arr(i).valid := '0';
      end loop;
      v_wr_sosi_arr(0).eop := '0';
      v_wr_sosi_arr(c_nof_shiftreg_words - 1).eop := '1';
      v_wr_sosi_arr(c_nof_shiftreg_words - 1).empty := rd_sosi_arr(0).empty;
      v_wr_sosi_arr(c_nof_shiftreg_words - 1).err := rd_sosi_arr(0).err;
    end if;

    wr_sosi_arr <= v_wr_sosi_arr;
  end process;
end str;
