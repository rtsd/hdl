-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:  Testbench for the st_xst unit.
--
-- Usage in non-auto-mode (c_modelsim_start = 0 in python):
--   > as 5
--   > run -all
-- Description:
-- The tb generates random data to feed into st_xst. The output is compared to
-- a pre-calculated expected array of xsq values.
-- Remark:
-- . More detail can be found in:
--   https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+Correlator

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_math_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.tb_st_pkg.all;

entity tb_st_xst is
  generic(
    g_nof_streams        : natural := 9;
    g_nof_crosslets      : natural := 2;
    g_nof_signal_inputs  : natural := 12;
    g_in_data_w          : natural := 16;
    g_nof_sync           : natural := 3;
    g_stat_data_w        : natural := 64;  -- statistics accumulator width
    g_stat_data_sz       : natural := 2;  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
    g_nof_block_per_sync : natural := 5;
    g_nof_clk_per_blk    : natural := 1024
  );
end tb_st_xst;

architecture tb of tb_st_xst is
  constant c_sim                     : boolean := true;
  constant c_rl                      : natural := 1;
  constant c_block_size              : natural := g_nof_crosslets * g_nof_signal_inputs;
  constant c_random_data_w           : natural := 8;
  constant c_xsq                     : natural := g_nof_signal_inputs * g_nof_signal_inputs;
  constant c_nof_statistics          : natural := g_nof_crosslets * c_xsq;
  constant c_nof_statistics_w        : natural := ceil_log2(c_nof_statistics);
  constant c_nof_statistics_mem_size : natural := c_nof_complex * 2**c_nof_statistics_w;
  constant c_single_stream_mem_size  : natural := c_nof_complex * c_nof_statistics * g_stat_data_sz;
  constant c_total_mem_size          : natural := g_nof_streams * c_nof_statistics_mem_size * g_stat_data_sz;
  constant c_random_seed             : natural := 100;

  constant c_mm_ram   : t_c_mem := (latency  => 1,
                                    adr_w    => ceil_log2(c_block_size),
                                    dat_w    => c_nof_complex * g_in_data_w,
                                    nof_dat  => c_block_size,
                                    init_sl  => '0');  -- MM side : sla_in, sla_out

  type t_random_in_2arr is array (integer range <>) of t_integer_arr(0 to c_block_size-1);
  type t_xsq_2arr       is array (integer range <>) of t_integer_arr(0 to c_nof_statistics * c_nof_complex - 1);
  type t_xsq_out_2arr   is array (integer range <>) of t_slv_32_arr(0 to c_single_stream_mem_size-1);

  signal random_in_re_2arr : t_random_in_2arr(0 to g_nof_streams - 1);
  signal random_in_im_2arr : t_random_in_2arr(0 to g_nof_streams - 1);
  signal expected_xsq_2arr : t_xsq_2arr(0 to g_nof_streams - 1);

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period  : time := 100 ps;
  constant c_dp_clk_period  : time := 5 ns;

  signal tb_end             : std_logic;

  signal mm_rst             : std_logic := '1';
  signal mm_clk             : std_logic := '1';

  signal dp_rst             : std_logic;
  signal dp_clk             : std_logic := '1';

  signal in_sosi            : t_dp_sosi := c_dp_sosi_rst;

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal ram_st_xsq_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_xsq_miso    : t_mem_miso := c_mem_miso_rst;
  signal st_xst_mm_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal st_xst_mm_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0) := (others => c_mem_miso_rst);

  signal in_mosi_arr        : t_mem_mosi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_mosi_rst);

  ----------------------------------------------------------------------------
  -- Output array
  ----------------------------------------------------------------------------
  signal st_xsq_out_2arr    : t_xsq_out_2arr(0 to g_nof_streams - 1) := (others => (others => (others => '0')));
begin
  -- random input and expected xsq
  gen_in_exp : for I in 0 to g_nof_streams - 1 generate
    random_in_re_2arr(I) <= common_math_create_random_arr(c_block_size, c_random_data_w, c_random_seed + 2 * I);
    random_in_im_2arr(I) <= common_math_create_random_arr(c_block_size, c_random_data_w, c_random_seed + 2 * I + 1);
    expected_xsq_2arr(I) <= func_st_calculate_expected_xsq(random_in_re_2arr(0), random_in_im_2arr(0), random_in_re_2arr(I), random_in_im_2arr(I), g_nof_crosslets, g_nof_block_per_sync);
  end generate;

  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- in mosi Stimuli
  ------------------------------------------------------------------------------
  gen_p_in_mosi : for M in 0 to g_nof_streams - 1 generate
    p_in_mosi : process
      variable v_ram_arr : t_slv_32_arr(c_block_size-1 downto 0);
    begin
      -- write random data to ram
      proc_common_wait_until_low(mm_clk, mm_rst);
      for J in 0 to c_block_size-1 loop
        v_ram_arr(J) := TO_SVEC(random_in_im_2arr(M)(J), g_in_data_w) & TO_SVEC(random_in_re_2arr(M)(J), g_in_data_w);
      end loop;
      proc_mem_write_ram(v_ram_arr, mm_clk, in_mosi_arr(M));
      wait;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- MM Stimuli
  ------------------------------------------------------------------------------
  gen_mm_stimuli : for M in 0 to g_nof_streams - 1 generate
    p_mm_stimuli : process
    begin
      -- read statistics
      for I in 0 to g_nof_sync - 1 loop
        proc_common_wait_until_lo_hi(dp_clk, in_sosi.sync);
      end loop;
      proc_common_wait_some_cycles(dp_clk, g_nof_clk_per_blk);
      proc_common_wait_some_cycles(dp_clk, 20);
      proc_common_wait_some_cycles(mm_clk, 2 * M * c_single_stream_mem_size);
      proc_mem_read_ram(M * c_nof_statistics_mem_size, c_single_stream_mem_size, mm_clk, ram_st_xsq_mosi, ram_st_xsq_miso, st_xsq_out_2arr(M));
      wait;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- Data blocks
  ------------------------------------------------------------------------------
  p_in_sosi : process
  begin
    tb_end <= '0';
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(dp_clk, dp_rst);

    -- Run some sync intervals with DSP counter data for the real and imag fields
    wait until rising_edge(dp_clk);
    proc_common_wait_some_cycles(dp_clk, 7);
    for I in 0 to g_nof_sync - 1 loop
      in_sosi.sop <= '1';
      in_sosi.eop <= '1';
      in_sosi.sync <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      in_sosi <= c_dp_sosi_rst;
      proc_common_wait_some_cycles(dp_clk, g_nof_clk_per_blk - 1);
      for J in 0 to g_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
        in_sosi.sop <= '1';
        in_sosi.eop <= '1';
        proc_common_wait_some_cycles(dp_clk, 1);
        in_sosi <= c_dp_sosi_rst;
        proc_common_wait_some_cycles(dp_clk, g_nof_clk_per_blk - 1);
      end loop;
    end loop;
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  p_verify : process
  begin
    proc_common_wait_until_high(mm_clk, ram_st_xsq_miso.rdval);
    proc_common_wait_some_cycles(mm_clk, 2 * c_total_mem_size + 10);
    for M in 0 to g_nof_streams - 1 loop
      for I in 0 to c_nof_statistics * c_nof_complex - 1 loop
        assert TO_SINT(st_xsq_out_2arr(M)(g_stat_data_sz * I)) = expected_xsq_2arr(M)(I)
          report "WRONG XSQ DATA"
          severity ERROR;  -- Only read low part of statistic
      end loop;
    end loop;
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- RAMs that contain a block of crosslets for each stream
  ----------------------------------------------------------------------------
  gen_ram : for I in 0 to g_nof_streams - 1 generate
    u_ram : entity common_lib.common_ram_cr_cw
    generic map(
      g_ram => c_mm_ram
    )
    port map(
      wr_rst => mm_rst,
      wr_clk => mm_clk,
      wr_en  => in_mosi_arr(I).wr,
      wr_adr => in_mosi_arr(I).address(c_mm_ram.adr_w - 1 downto 0),
      wr_dat => in_mosi_arr(I).wrdata(c_mm_ram.dat_w - 1 downto 0),

      rd_rst => dp_rst,
      rd_clk => dp_clk,
      rd_en  => st_xst_mm_mosi.rd,
      rd_adr  => st_xst_mm_mosi.address(c_mm_ram.adr_w - 1 downto 0),
      rd_dat  => st_xst_mm_miso_arr(I).rddata(c_mm_ram.dat_w - 1 downto 0),
      rd_val  => st_xst_mm_miso_arr(I).rdval
    );
  end generate;

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.st_xst
  generic map(
    g_nof_streams        => g_nof_streams,
    g_nof_signal_inputs  => g_nof_signal_inputs,
    g_nof_crosslets      => g_nof_crosslets,
    g_in_data_w          => g_in_data_w,
    g_stat_data_w        => g_stat_data_w,
    g_stat_data_sz       => g_stat_data_sz
  )
  port map(
    mm_rst           =>  mm_rst,
    mm_clk           =>  mm_clk,
    dp_rst           =>  dp_rst,
    dp_clk           =>  dp_clk,

    -- Streaming
    in_sosi          => in_sosi,

    -- DP Memory Mapped
    mm_mosi          => st_xst_mm_mosi,
    mm_miso_arr      => st_xst_mm_miso_arr,

    -- Memory Mapped
    ram_st_xsq_mosi  => ram_st_xsq_mosi,
    ram_st_xsq_miso  => ram_st_xsq_miso
  );
end tb;
