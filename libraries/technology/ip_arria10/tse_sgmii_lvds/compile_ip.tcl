#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - compile_ip.tcl: vmap the IP specific libraries to ./work and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - compile_ip.tcl: replace IP_DIR by IP_DIR
# - hdllib.cfg: add this compile_ip.tcl to the modelsim_compile_ip_files key in the hdllib.cfg
# - hdllib.cfg: the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl

set IP_DIR   "$env(HDL_BUILD_DIR)/unb2/qsys-generate/sim"

#vlib ./work/         ;# Assume library work already exists

vmap ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150                ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_reset_controller_150           ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_eth_tse_nf_lvds_terminator_150 ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_lvds_150                       ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150    ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_eth_tse_avalon_arbiter_150     ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150                ./work/
vmap ip_arria10_tse_sgmii_lvds_altera_eth_tse_150                    ./work/

  vlog -sv "$IP_DIR/../altera_lvds_core20_150/sim/ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150_lnpe55i.sv"        -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog -sv "$IP_DIR/../altera_lvds_core20_150/sim/altera_lvds_core20.sv"                                              -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog     "$IP_DIR/../altera_lvds_core20_150/sim/altera_lvds_core20_pll.v"                                           -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog -sv "$IP_DIR/../altera_lvds_core20_150/sim/ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150_gpbqs4y.sv"        -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog -sv "$IP_DIR/../altera_lvds_core20_150/sim/altera_lvds_core20.sv"                                              -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog     "$IP_DIR/../altera_lvds_core20_150/sim/altera_lvds_core20_pll.v"                                           -work ip_arria10_tse_sgmii_lvds_altera_lvds_core20_150               
  vlog     "$IP_DIR/../altera_reset_controller_150/sim/altera_reset_controller.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_reset_controller_150          
  vlog     "$IP_DIR/../altera_reset_controller_150/sim/altera_reset_synchronizer.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_reset_controller_150          
  vlog     "$IP_DIR/../altera_eth_tse_nf_lvds_terminator_150/sim/mentor/altera_eth_tse_nf_lvds_terminator.v"          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_nf_lvds_terminator_150
  vlog     "$IP_DIR/../altera_eth_tse_nf_lvds_terminator_150/sim/mentor/altera_tse_reset_synchronizer.v"              -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_nf_lvds_terminator_150
  vlog     "$IP_DIR/../altera_eth_tse_nf_lvds_terminator_150/sim/mentor/altera_tse_nf_lvds_channel_reset_sequencer.v" -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_nf_lvds_terminator_150
  vlog     "$IP_DIR/../altera_eth_tse_nf_lvds_terminator_150/sim/mentor/altera_tse_nf_lvds_common_reset_sequencer.v"  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_nf_lvds_terminator_150
  vlog     "$IP_DIR/../altera_lvds_150/sim/ip_arria10_tse_sgmii_lvds_altera_lvds_150_c7jevba.v"                       -work ip_arria10_tse_sgmii_lvds_altera_lvds_150                      
  vlog     "$IP_DIR/../altera_lvds_150/sim/ip_arria10_tse_sgmii_lvds_altera_lvds_150_lae6doy.v"                       -work ip_arria10_tse_sgmii_lvds_altera_lvds_150                      
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_eth_tse_pcs_pma_nf_lvds.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_align_sync.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_dec10b8b.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_dec_func.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_enc8b10b.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_autoneg.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_carrier_sense.v"                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_clk_gen.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_sgmii_clk_div.v"                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_sgmii_clk_enable.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_rx_encapsulation.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_tx_encapsulation.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_rx_encapsulation_strx_gx.v"           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_pcs_control.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_pcs_host_control.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_mdio_reg.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_mii_rx_if_pcs.v"                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_mii_tx_if_pcs.v"                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_rx_sync.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_sgmii_clk_cntl.v"                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_colision_detect.v"                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_rx_converter.v"                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_rx_fifo_rd.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_rx_converter.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_sgmii.v"                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_sgmii_strx_gx.v"                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_tx_converter.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_tx_converter.v"                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_1000_base_x.v"                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_1000_base_x_strx_gx.v"            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_pcs.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_pcs_strx_gx.v"                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_rx.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_top_tx.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_lvds_reset_sequencer.v"               -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_lvds_reverse_loopback.v"              -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_pma_lvds_rx_av.v"                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_pma_lvds_rx.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_pma_lvds_tx.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_false_path_marker.v"                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_reset_synchronizer.v"                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_clock_crosser.v"                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_13.v"                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_24.v"                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_34.v"                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_opt_1246.v"                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_opt_14_44.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_a_fifo_opt_36_10.v"                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_gray_cnt.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_sdpm_altsyncram.v"                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_altsyncram_dpm_fifo.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_bin_cnt.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog -sv "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ph_calculator.sv"                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_sdpm_gen.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x10.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x10.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x10_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x14.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x14.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x14_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x2.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x2.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x2_wrapper.v"                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x23.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x23.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x23_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x36.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x36.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x36_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x40.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x40.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x40_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_dec_x30.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x30.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_enc_x30_wrapper.v"                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_150/sim/mentor/altera_tse_ecc_status_crosser.v"                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_pcs_pma_nf_lvds_150   
  vlog     "$IP_DIR/../altera_eth_tse_avalon_arbiter_150/sim/mentor/altera_eth_tse_avalon_arbiter.v"                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_avalon_arbiter_150    
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_eth_tse_mac.v"                                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_clk_cntl.v"                                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_crc328checker.v"                                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_crc328generator.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_crc32ctl8.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_crc32galois8.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_gmii_io.v"                                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_lb_read_cntl.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_lb_wrt_cntl.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_hashing.v"                                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_host_control.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_host_control_small.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mac_control.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_register_map.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_register_map_small.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_counter_cntl.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_shared_mac_control.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_shared_register_map.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_counter_cntl.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_lfsr_10.v"                                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_loopback_ff.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_altshifttaps.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_fifoless_mac_rx.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mac_rx.v"                                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_fifoless_mac_tx.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mac_tx.v"                                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_magic_detection.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mdio.v"                                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mdio_clk_gen.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mdio_cntl.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_mdio.v"                                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mii_rx_if.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_mii_tx_if.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_pipeline_base.v"                                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog -sv "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_pipeline_stage.sv"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_dpram_16x32.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_dpram_8x32.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_dpram_ecc_16x32.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_quad_16x32.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_quad_8x32.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_fifoless_retransmit_cntl.v"                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_retransmit_cntl.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rgmii_in1.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rgmii_in4.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_nf_rgmii_module.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rgmii_module.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rgmii_out1.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rgmii_out4.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_ff.v"                                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_min_ff.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_ff_cntrl.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_ff_cntrl_32.v"                                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_ff_cntrl_32_shift16.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_ff_length.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_rx_stat_extract.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_timing_adapter32.v"                               -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_timing_adapter8.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_timing_adapter_fifo32.v"                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_timing_adapter_fifo8.v"                           -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_1geth.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_fifoless_1geth.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_w_fifo.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_w_fifo_10_100_1000.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_wo_fifo.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_wo_fifo_10_100_1000.v"                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_top_gen_host.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff.v"                                          -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_min_ff.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff_cntrl.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff_cntrl_32.v"                                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff_cntrl_32_shift16.v"                         -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff_length.v"                                   -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_ff_read_cntl.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_tx_stat_extract.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_false_path_marker.v"                              -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_reset_synchronizer.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_clock_crosser.v"                                  -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_13.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_24.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_34.v"                                      -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_opt_1246.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_opt_14_44.v"                               -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_a_fifo_opt_36_10.v"                               -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_gray_cnt.v"                                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_sdpm_altsyncram.v"                                -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_altsyncram_dpm_fifo.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_bin_cnt.v"                                        -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog -sv "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ph_calculator.sv"                                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_sdpm_gen.v"                                       -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x10.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x10.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x10_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x14.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x14.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x14_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x2.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x2.v"                                     -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x2_wrapper.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x23.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x23.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x23_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x36.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x36.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x36_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x40.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x40.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x40_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_dec_x30.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x30.v"                                    -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_enc_x30_wrapper.v"                            -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_mac_150/sim/mentor/altera_tse_ecc_status_crosser.v"                             -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_mac_150               
  vlog     "$IP_DIR/../altera_eth_tse_150/sim/ip_arria10_tse_sgmii_lvds_altera_eth_tse_150_jjd52za.v"                 -work ip_arria10_tse_sgmii_lvds_altera_eth_tse_150                   
  vcom     "$IP_DIR/ip_arria10_tse_sgmii_lvds.vhd"                                                                                                                                         
