-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Connect an MM slave to the MM bus or represent an not connected
--          slave. Force waitrequest = '0' if slave does not need mosi flow
--          control
-- Description:
-- * g_enable
--   When FALSE then the in_miso output tot the master is forced to
--   c_mem_miso_rst to represent a not connected MM slave. When TRUE then
--   the out_miso signal from the slave is passed on to the master.
-- * g_waitrequest
--   When FALSE then the in_miso.waitrequest is forced to '0' to indicate
--   that the MM slave does not need mosi flow control. When FALSE then
--   the miso.waitrequest from the connected slave is passed on to the
--   master.
-- * g_rd_latency
--   Used to derive in_miso.rdval from in_mosi.rd and out_miso.waitrequest,
--   to provide rdval for MM slaves that do not drive rdval. Typically any
--   MM slave that needs miso.waitrequest flow control, also should support
--   rdval themselves.
--
-- Todo:
-- * Add miso.response field as defined in Avalon bus, to inform master about
--   rd status (00 = okay, 01 = rsvd, 10 = slaveerror, 11 = decodeerror).
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_slave_enable is
  generic (
    g_enable       : boolean;
    g_waitrequest  : boolean;
    g_rd_latency   : natural
  );
  port (
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;
    -- MM input RL = 1
    in_mosi       : in  t_mem_mosi;
    in_miso       : out t_mem_miso;
    -- MM output RL = 0
    out_mosi      : out t_mem_mosi;
    out_miso      : in  t_mem_miso
  );
end mm_slave_enable;

architecture rtl of mm_slave_enable is
  signal rd          : std_logic;
  signal rdval       : std_logic;
  signal waitrequest : std_logic;
begin
  -- Use mosi.rd to create miso.rdval for unconnected slave or for slaves that do not support rdval
  u_rdval : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => g_rd_latency
  )
  port map (
    rst     => mm_rst,
    clk     => mm_clk,
    in_dat  => rd,
    out_dat => rdval
  );

  no_slave : if g_enable = false generate
    out_mosi <= c_mem_mosi_rst;

    rd <= in_mosi.rd;

    p_in_miso : process(rdval)
    begin
      in_miso <= c_mem_miso_rst;  -- force all miso to 0, so rddata = 0 and no waitrequest
      in_miso.rdval <= rdval;  -- support rdval to avoid hanging master that waits for rdval
    end process;
  end generate;

  gen_slave : if g_enable = true generate
    out_mosi <= in_mosi;

    -- Use waitrequest from slave, or force waitrequest = '0' if slave does not need mosi flow control
    waitrequest <= out_miso.waitrequest when g_waitrequest = true else '0';

    rd <= in_mosi.rd and not waitrequest;

    p_in_miso : process(out_miso, rdval, waitrequest)
    begin
      in_miso <= out_miso;
      in_miso.rdval <= rdval;
      in_miso.waitrequest <= waitrequest;
    end process;
  end generate;
end rtl;
