-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, mm_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_lofar2_unb2c_ring_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity mmc_lofar2_unb2c_ring is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_rst                                : in  std_logic;
    mm_clk                                : in  std_logic;

    pout_wdi                              : out std_logic;

    -- Manual WDI override
    reg_wdi_copi                          : out t_mem_copi;
    reg_wdi_cipo                          : in  t_mem_cipo;

    -- system_info
    reg_unb_system_info_copi              : out t_mem_copi;
    reg_unb_system_info_cipo              : in  t_mem_cipo;
    rom_unb_system_info_copi              : out t_mem_copi;
    rom_unb_system_info_cipo              : in  t_mem_cipo;

    -- FPGA sensors
    reg_fpga_temp_sens_copi               : out t_mem_copi;
    reg_fpga_temp_sens_cipo               : in  t_mem_cipo;
    reg_fpga_voltage_sens_copi            : out t_mem_copi;
    reg_fpga_voltage_sens_cipo            : in  t_mem_cipo;

    -- PPSH
    reg_ppsh_copi                         : out t_mem_copi;
    reg_ppsh_cipo                         : in  t_mem_cipo;

    -- eth1g
    eth1g_mm_rst                          : out std_logic;
    eth1g_tse_copi                        : out t_mem_copi;
    eth1g_tse_cipo                        : in  t_mem_cipo;
    eth1g_reg_copi                        : out t_mem_copi;
    eth1g_reg_cipo                        : in  t_mem_cipo;
    eth1g_reg_interrupt                   : in  std_logic;
    eth1g_ram_copi                        : out t_mem_copi;
    eth1g_ram_cipo                        : in  t_mem_cipo;

    -- EPCS read
    reg_dpmm_data_copi                    : out t_mem_copi;
    reg_dpmm_data_cipo                    : in  t_mem_cipo;
    reg_dpmm_ctrl_copi                    : out t_mem_copi;
    reg_dpmm_ctrl_cipo                    : in  t_mem_cipo;

    -- EPCS write
    reg_mmdp_data_copi                    : out t_mem_copi;
    reg_mmdp_data_cipo                    : in  t_mem_cipo;
    reg_mmdp_ctrl_copi                    : out t_mem_copi;
    reg_mmdp_ctrl_cipo                    : in  t_mem_cipo;

    -- EPCS status/control
    reg_epcs_copi                         : out t_mem_copi;
    reg_epcs_cipo                         : in  t_mem_cipo;

    -- Remote Update
    reg_remu_copi                         : out t_mem_copi;
    reg_remu_cipo                         : in  t_mem_cipo;

    -- BSN Monitor
    reg_bsn_monitor_v2_ring_rx_copi       : out t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_rx_cipo       : in  t_mem_cipo := c_mem_cipo_rst;

    -- BSN Monitor
    reg_bsn_monitor_v2_ring_tx_copi       : out t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_tx_cipo       : in  t_mem_cipo := c_mem_cipo_rst;

    -- BG
    reg_diag_bg_copi                      : out t_mem_copi;
    reg_diag_bg_cipo                      : in  t_mem_cipo;
    ram_diag_bg_copi                      : out t_mem_copi;
    ram_diag_bg_cipo                      : in  t_mem_cipo;

    -- ring_lane_info
    reg_ring_lane_info_copi                : out t_mem_copi;
    reg_ring_lane_info_cipo                : in  t_mem_cipo;

    -- Lane xonoff
    reg_dp_xonoff_lane_copi                : out t_mem_copi;
    reg_dp_xonoff_lane_cipo                : in  t_mem_cipo;

    -- Local xonoff
    reg_dp_xonoff_local_copi               : out t_mem_copi;
    reg_dp_xonoff_local_cipo               : in  t_mem_cipo;

    -- Local xonoff
    reg_dp_block_validate_err_copi         : out t_mem_copi;
    reg_dp_block_validate_err_cipo         : in  t_mem_cipo;

    -- Local xonoff
    reg_dp_block_validate_bsn_at_sync_copi : out t_mem_copi;
    reg_dp_block_validate_bsn_at_sync_cipo : in  t_mem_cipo;

    -- ring_info
    reg_ring_info_copi                     : out t_mem_copi;
    reg_ring_info_cipo                     : in  t_mem_cipo;

    reg_tr_10GbE_mac_copi                  : out t_mem_copi;
    reg_tr_10GbE_mac_cipo                  : in  t_mem_cipo;

    reg_tr_10GbE_eth10g_copi               : out t_mem_copi;
    reg_tr_10GbE_eth10g_cipo               : in  t_mem_cipo;

    -- Scrap ram
    ram_scrap_copi                         : out t_mem_copi;
    ram_scrap_cipo                         : in  t_mem_cipo

  );
end mmc_lofar2_unb2c_ring;

architecture str of mmc_lofar2_unb2c_ring is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  signal i_reset_n         : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $UPE/sim.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                                           port map(mm_rst, mm_clk, reg_unb_system_info_copi, reg_unb_system_info_cipo );

    u_mm_file_rom_unb_system_info                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                                           port map(mm_rst, mm_clk, rom_unb_system_info_copi, rom_unb_system_info_cipo );

    u_mm_file_reg_wdi                            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                                           port map(mm_rst, mm_clk, reg_wdi_copi, reg_wdi_cipo );

    u_mm_file_reg_fpga_temp_sens                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                                           port map(mm_rst, mm_clk, reg_fpga_temp_sens_copi, reg_fpga_temp_sens_cipo );

    u_mm_file_reg_fpga_voltage_sens              :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                                           port map(mm_rst, mm_clk, reg_fpga_voltage_sens_copi, reg_fpga_voltage_sens_cipo );

    u_mm_file_reg_ppsh                           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                                           port map(mm_rst, mm_clk, reg_ppsh_copi, reg_ppsh_cipo );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth                            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                                           port map(mm_rst, mm_clk, eth1g_reg_copi, eth1g_reg_cipo );

    u_mm_file_reg_dp_block_validate_err          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_BLOCK_VALIDATE_ERR")
                                                           port map(mm_rst, mm_clk, reg_dp_block_validate_err_copi, reg_dp_block_validate_err_cipo );

    u_mm_file_reg_dp_block_validate_bsn_at_sync  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC")
                                                           port map(mm_rst, mm_clk, reg_dp_block_validate_bsn_at_sync_copi, reg_dp_block_validate_bsn_at_sync_cipo );

    u_mm_file_reg_bsn_monitor_v2_ring_rx         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_RX")
                                                           port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_rx_copi, reg_bsn_monitor_v2_ring_rx_cipo );

    u_mm_file_reg_bsn_monitor_v2_ring_tx         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_TX")
                                                           port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_tx_copi, reg_bsn_monitor_v2_ring_tx_cipo );

    u_mm_file_reg_bg                             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG")
                                                           port map(mm_rst, mm_clk, reg_diag_bg_copi, reg_diag_bg_cipo );
    u_mm_file_ram_bg                             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG")
                                                           port map(mm_rst, mm_clk, ram_diag_bg_copi, ram_diag_bg_cipo );

    u_mm_file_reg_ring_lane_info                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RING_LANE_INFO")
                                                           port map(mm_rst, mm_clk, reg_ring_lane_info_copi, reg_ring_lane_info_cipo );

    u_mm_file_reg_dp_xonoff_lane                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_XONOFF_LANE")
                                                           port map(mm_rst, mm_clk, reg_dp_xonoff_lane_copi, reg_dp_xonoff_lane_cipo );

    u_mm_file_reg_dp_xonoff_local                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_XONOFF_LOCAL")
                                                           port map(mm_rst, mm_clk, reg_dp_xonoff_local_copi, reg_dp_xonoff_local_cipo );

    u_mm_file_reg_ring_info                      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RING_INFO")
                                                           port map(mm_rst, mm_clk, reg_ring_info_copi, reg_ring_info_cipo);

    u_mm_file_reg_tr_10GbE_mac                   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_MAC")
                                                           port map(mm_rst, mm_clk, reg_tr_10GbE_mac_copi, reg_tr_10GbE_mac_cipo );

    u_mm_file_reg_tr_10GbE_eth10g                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_ETH10G")
                                                           port map(mm_rst, mm_clk, reg_tr_10GbE_eth10g_copi, reg_tr_10GbE_eth10g_cipo );

    u_mm_file_ram_scrap                          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SCRAP")
                                                           port map(mm_rst, mm_clk, ram_scrap_copi, ram_scrap_cipo );
    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;
  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_lofar2_unb2c_ring
    port map (

      clk_clk                                            => mm_clk,
      reset_reset_n                                      => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2c_board.
      pio_wdi_external_connection_export                 => pout_wdi,

      avs_eth_0_reset_export                             => eth1g_mm_rst,
      avs_eth_0_clk_export                               => OPEN,
      avs_eth_0_tse_address_export                       => eth1g_tse_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                         => eth1g_tse_copi.wr,
      avs_eth_0_tse_read_export                          => eth1g_tse_copi.rd,
      avs_eth_0_tse_writedata_export                     => eth1g_tse_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export                      => eth1g_tse_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export                   => eth1g_tse_cipo.waitrequest,
      avs_eth_0_reg_address_export                       => eth1g_reg_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                         => eth1g_reg_copi.wr,
      avs_eth_0_reg_read_export                          => eth1g_reg_copi.rd,
      avs_eth_0_reg_writedata_export                     => eth1g_reg_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export                      => eth1g_reg_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export                       => eth1g_ram_copi.address(c_unb2c_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                         => eth1g_ram_copi.wr,
      avs_eth_0_ram_read_export                          => eth1g_ram_copi.rd,
      avs_eth_0_ram_writedata_export                     => eth1g_ram_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export                      => eth1g_ram_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                               => eth1g_reg_interrupt,

      reg_fpga_temp_sens_reset_export                    => OPEN,
      reg_fpga_temp_sens_clk_export                      => OPEN,
      reg_fpga_temp_sens_address_export                  => reg_fpga_temp_sens_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export                    => reg_fpga_temp_sens_copi.wr,
      reg_fpga_temp_sens_writedata_export                => reg_fpga_temp_sens_copi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export                     => reg_fpga_temp_sens_copi.rd,
      reg_fpga_temp_sens_readdata_export                 => reg_fpga_temp_sens_cipo.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export                 => OPEN,
      reg_fpga_voltage_sens_clk_export                   => OPEN,
      reg_fpga_voltage_sens_address_export               => reg_fpga_voltage_sens_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export                 => reg_fpga_voltage_sens_copi.wr,
      reg_fpga_voltage_sens_writedata_export             => reg_fpga_voltage_sens_copi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export                  => reg_fpga_voltage_sens_copi.rd,
      reg_fpga_voltage_sens_readdata_export              => reg_fpga_voltage_sens_cipo.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export                       => OPEN,
      rom_system_info_clk_export                         => OPEN,
      rom_system_info_address_export                     => rom_unb_system_info_copi.address(c_unb2c_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export                       => rom_unb_system_info_copi.wr,
      rom_system_info_writedata_export                   => rom_unb_system_info_copi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export                        => rom_unb_system_info_copi.rd,
      rom_system_info_readdata_export                    => rom_unb_system_info_cipo.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export                       => OPEN,
      pio_system_info_clk_export                         => OPEN,
      pio_system_info_address_export                     => reg_unb_system_info_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export                       => reg_unb_system_info_copi.wr,
      pio_system_info_writedata_export                   => reg_unb_system_info_copi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export                        => reg_unb_system_info_copi.rd,
      pio_system_info_readdata_export                    => reg_unb_system_info_cipo.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                               => OPEN,
      pio_pps_clk_export                                 => OPEN,
      pio_pps_address_export                             => reg_ppsh_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                               => reg_ppsh_copi.wr,
      pio_pps_writedata_export                           => reg_ppsh_copi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                                => reg_ppsh_copi.rd,
      pio_pps_readdata_export                            => reg_ppsh_cipo.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                               => OPEN,
      reg_wdi_clk_export                                 => OPEN,
      reg_wdi_address_export                             => reg_wdi_copi.address(0 downto 0),
      reg_wdi_write_export                               => reg_wdi_copi.wr,
      reg_wdi_writedata_export                           => reg_wdi_copi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                                => reg_wdi_copi.rd,
      reg_wdi_readdata_export                            => reg_wdi_cipo.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                              => OPEN,
      reg_remu_clk_export                                => OPEN,
      reg_remu_address_export                            => reg_remu_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                              => reg_remu_copi.wr,
      reg_remu_writedata_export                          => reg_remu_copi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                               => reg_remu_copi.rd,
      reg_remu_readdata_export                           => reg_remu_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_rx_address_export          => reg_bsn_monitor_v2_ring_rx_copi.address(c_sdp_reg_bsn_monitor_v2_ring_rx_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_clk_export              => OPEN,
      reg_bsn_monitor_v2_ring_rx_read_export             => reg_bsn_monitor_v2_ring_rx_copi.rd,
      reg_bsn_monitor_v2_ring_rx_readdata_export         => reg_bsn_monitor_v2_ring_rx_cipo.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_reset_export            => OPEN,
      reg_bsn_monitor_v2_ring_rx_write_export            => reg_bsn_monitor_v2_ring_rx_copi.wr,
      reg_bsn_monitor_v2_ring_rx_writedata_export        => reg_bsn_monitor_v2_ring_rx_copi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_tx_address_export          => reg_bsn_monitor_v2_ring_tx_copi.address(c_sdp_reg_bsn_monitor_v2_ring_tx_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_clk_export              => OPEN,
      reg_bsn_monitor_v2_ring_tx_read_export             => reg_bsn_monitor_v2_ring_tx_copi.rd,
      reg_bsn_monitor_v2_ring_tx_readdata_export         => reg_bsn_monitor_v2_ring_tx_cipo.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_reset_export            => OPEN,
      reg_bsn_monitor_v2_ring_tx_write_export            => reg_bsn_monitor_v2_ring_tx_copi.wr,
      reg_bsn_monitor_v2_ring_tx_writedata_export        => reg_bsn_monitor_v2_ring_tx_copi.wrdata(c_word_w - 1 downto 0),

      -- waveform generators (multiplexed)
      reg_diag_bg_clk_export                             => OPEN,
      reg_diag_bg_reset_export                           => OPEN,
      reg_diag_bg_address_export                         => reg_diag_bg_copi.address(c_sdp_reg_diag_bg_addr_w - 1 downto 0),
      reg_diag_bg_read_export                            => reg_diag_bg_copi.rd,
      reg_diag_bg_readdata_export                        => reg_diag_bg_cipo.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_write_export                           => reg_diag_bg_copi.wr,
      reg_diag_bg_writedata_export                       => reg_diag_bg_copi.wrdata(c_word_w - 1 downto 0),

      ram_diag_bg_clk_export                             => OPEN,
      ram_diag_bg_reset_export                           => OPEN,
      ram_diag_bg_address_export                         => ram_diag_bg_copi.address(c_sdp_ram_diag_bg_addr_w - 1 downto 0),
      ram_diag_bg_read_export                            => ram_diag_bg_copi.rd,
      ram_diag_bg_readdata_export                        => ram_diag_bg_cipo.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_write_export                           => ram_diag_bg_copi.wr,
      ram_diag_bg_writedata_export                       => ram_diag_bg_copi.wrdata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                              => OPEN,
      reg_epcs_clk_export                                => OPEN,
      reg_epcs_address_export                            => reg_epcs_copi.address(c_unb2c_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                              => reg_epcs_copi.wr,
      reg_epcs_writedata_export                          => reg_epcs_copi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                               => reg_epcs_copi.rd,
      reg_epcs_readdata_export                           => reg_epcs_cipo.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                         => OPEN,
      reg_dpmm_ctrl_clk_export                           => OPEN,
      reg_dpmm_ctrl_address_export                       => reg_dpmm_ctrl_copi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                         => reg_dpmm_ctrl_copi.wr,
      reg_dpmm_ctrl_writedata_export                     => reg_dpmm_ctrl_copi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                          => reg_dpmm_ctrl_copi.rd,
      reg_dpmm_ctrl_readdata_export                      => reg_dpmm_ctrl_cipo.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                         => OPEN,
      reg_mmdp_data_clk_export                           => OPEN,
      reg_mmdp_data_address_export                       => reg_mmdp_data_copi.address(0 downto 0),
      reg_mmdp_data_write_export                         => reg_mmdp_data_copi.wr,
      reg_mmdp_data_writedata_export                     => reg_mmdp_data_copi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                          => reg_mmdp_data_copi.rd,
      reg_mmdp_data_readdata_export                      => reg_mmdp_data_cipo.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                         => OPEN,
      reg_dpmm_data_clk_export                           => OPEN,
      reg_dpmm_data_address_export                       => reg_dpmm_data_copi.address(0 downto 0),
      reg_dpmm_data_read_export                          => reg_dpmm_data_copi.rd,
      reg_dpmm_data_readdata_export                      => reg_dpmm_data_cipo.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                         => reg_dpmm_data_copi.wr,
      reg_dpmm_data_writedata_export                     => reg_dpmm_data_copi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                         => OPEN,
      reg_mmdp_ctrl_clk_export                           => OPEN,
      reg_mmdp_ctrl_address_export                       => reg_mmdp_ctrl_copi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                          => reg_mmdp_ctrl_copi.rd,
      reg_mmdp_ctrl_readdata_export                      => reg_mmdp_ctrl_cipo.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                         => reg_mmdp_ctrl_copi.wr,
      reg_mmdp_ctrl_writedata_export                     => reg_mmdp_ctrl_copi.wrdata(c_word_w - 1 downto 0),

      reg_ring_lane_info_clk_export                      => OPEN,
      reg_ring_lane_info_reset_export                    => OPEN,
      reg_ring_lane_info_address_export                  => reg_ring_lane_info_copi.address(c_sdp_reg_ring_lane_info_addr_w - 1 downto 0),
      reg_ring_lane_info_write_export                    => reg_ring_lane_info_copi.wr,
      reg_ring_lane_info_writedata_export                => reg_ring_lane_info_copi.wrdata(c_word_w - 1 downto 0),
      reg_ring_lane_info_read_export                     => reg_ring_lane_info_copi.rd,
      reg_ring_lane_info_readdata_export                 => reg_ring_lane_info_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_xonoff_lane_clk_export                      => OPEN,
      reg_dp_xonoff_lane_reset_export                    => OPEN,
      reg_dp_xonoff_lane_address_export                  => reg_dp_xonoff_lane_copi.address(c_sdp_reg_dp_xonoff_lane_addr_w - 1 downto 0),
      reg_dp_xonoff_lane_write_export                    => reg_dp_xonoff_lane_copi.wr,
      reg_dp_xonoff_lane_writedata_export                => reg_dp_xonoff_lane_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_xonoff_lane_read_export                     => reg_dp_xonoff_lane_copi.rd,
      reg_dp_xonoff_lane_readdata_export                 => reg_dp_xonoff_lane_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_xonoff_local_clk_export                     => OPEN,
      reg_dp_xonoff_local_reset_export                   => OPEN,
      reg_dp_xonoff_local_address_export                 => reg_dp_xonoff_local_copi.address(c_sdp_reg_dp_xonoff_local_addr_w - 1 downto 0),
      reg_dp_xonoff_local_write_export                   => reg_dp_xonoff_local_copi.wr,
      reg_dp_xonoff_local_writedata_export               => reg_dp_xonoff_local_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_xonoff_local_read_export                    => reg_dp_xonoff_local_copi.rd,
      reg_dp_xonoff_local_readdata_export                => reg_dp_xonoff_local_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_block_validate_err_clk_export               => OPEN,
      reg_dp_block_validate_err_reset_export             => OPEN,
      reg_dp_block_validate_err_address_export           => reg_dp_block_validate_err_copi.address(c_sdp_reg_dp_block_validate_err_addr_w - 1 downto 0),
      reg_dp_block_validate_err_write_export             => reg_dp_block_validate_err_copi.wr,
      reg_dp_block_validate_err_writedata_export         => reg_dp_block_validate_err_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_block_validate_err_read_export              => reg_dp_block_validate_err_copi.rd,
      reg_dp_block_validate_err_readdata_export          => reg_dp_block_validate_err_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_block_validate_bsn_at_sync_clk_export       => OPEN,
      reg_dp_block_validate_bsn_at_sync_reset_export     => OPEN,
      reg_dp_block_validate_bsn_at_sync_address_export   => reg_dp_block_validate_bsn_at_sync_copi.address(c_sdp_reg_dp_block_validate_bsn_at_sync_addr_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_write_export     => reg_dp_block_validate_bsn_at_sync_copi.wr,
      reg_dp_block_validate_bsn_at_sync_writedata_export => reg_dp_block_validate_bsn_at_sync_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_read_export      => reg_dp_block_validate_bsn_at_sync_copi.rd,
      reg_dp_block_validate_bsn_at_sync_readdata_export  => reg_dp_block_validate_bsn_at_sync_cipo.rddata(c_word_w - 1 downto 0),

      reg_ring_info_clk_export                           => OPEN,
      reg_ring_info_reset_export                         => OPEN,
      reg_ring_info_address_export                       => reg_ring_info_copi.address(c_sdp_reg_ring_info_addr_w - 1 downto 0),
      reg_ring_info_write_export                         => reg_ring_info_copi.wr,
      reg_ring_info_writedata_export                     => reg_ring_info_copi.wrdata(c_word_w - 1 downto 0),
      reg_ring_info_read_export                          => reg_ring_info_copi.rd,
      reg_ring_info_readdata_export                      => reg_ring_info_cipo.rddata(c_word_w - 1 downto 0),

      reg_tr_10GbE_mac_clk_export                        => OPEN,
      reg_tr_10GbE_mac_reset_export                      => OPEN,
      reg_tr_10GbE_mac_address_export                    => reg_tr_10GbE_mac_copi.address(c_sdp_reg_tr_10GbE_mac_addr_w - 1 downto 0),
      reg_tr_10GbE_mac_write_export                      => reg_tr_10GbE_mac_copi.wr,
      reg_tr_10GbE_mac_writedata_export                  => reg_tr_10GbE_mac_copi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10GbE_mac_read_export                       => reg_tr_10GbE_mac_copi.rd,
      reg_tr_10GbE_mac_readdata_export                   => reg_tr_10GbE_mac_cipo.rddata(c_word_w - 1 downto 0),

      reg_tr_10GbE_eth10g_clk_export                     => OPEN,
      reg_tr_10GbE_eth10g_reset_export                   => OPEN,
      reg_tr_10GbE_eth10g_address_export                 => reg_tr_10GbE_eth10g_copi.address(c_sdp_reg_tr_10GbE_eth10g_addr_w - 1 downto 0),
      reg_tr_10GbE_eth10g_write_export                   => reg_tr_10GbE_eth10g_copi.wr,
      reg_tr_10GbE_eth10g_writedata_export               => reg_tr_10GbE_eth10g_copi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10GbE_eth10g_read_export                    => reg_tr_10GbE_eth10g_copi.rd,
      reg_tr_10GbE_eth10g_readdata_export                => reg_tr_10GbE_eth10g_cipo.rddata(c_word_w - 1 downto 0),

      ram_scrap_clk_export                               => OPEN,
      ram_scrap_reset_export                             => OPEN,
      ram_scrap_address_export                           => ram_scrap_copi.address(9 - 1 downto 0),
      ram_scrap_write_export                             => ram_scrap_copi.wr,
      ram_scrap_writedata_export                         => ram_scrap_copi.wrdata(c_word_w - 1 downto 0),
      ram_scrap_read_export                              => ram_scrap_copi.rd,
      ram_scrap_readdata_export                          => ram_scrap_cipo.rddata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
