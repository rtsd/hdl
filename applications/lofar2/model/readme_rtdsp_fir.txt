Author: Eric Kooistra, nov 2023

* Practise DSP [1].

* Try to reproduce LOFAR subband filter FIR coefficients using scipy instead
  of MATLAB.
  The pfs_coeff_final.m from the Filter Task Force (FTF) in 2005 use fircls1
  with r_pass and r_stop to define the ripple. In addition it post applies a
  Kaiser window with beta = 1 to make the filter attenuation a bit more deep
  near the transition. The pfir_coeff.m from Apertif also uses fircls1.
  Both use fircls1 with N = 1024 FIR coefficients and then Fourier
  interpolation to achieve Ncoefs = 1024 * 16 FIR coefficients. Both scripts
  can not exactly reproduce the actual LOFAR1 coefficients, therefore these
  are loaded from a file Coeffs16384Kaiser-quant.dat

* Try low pass filter design methods using windowed sync, firls, remez [2]
  The windowed sync method, firls leased squares method and remez method all
  yield comparable results, but firls and remez perform slightly better near
  the transition band. The firls and remez functions from scipy.signal use
  transition bandwidth and weights between pass and stop band to influence
  the transition region and ripple. For remez the ripple is constant in the
  pass band and stop band, for firls the ripple is largest near the band
  transition.

* It is possible to design a good FIR filter using Python scipy. Possibly with
  some extra help of a filter design and analysis (FDA) tool like pyfda [3].

[1] https://git.astron.nl/rtsd/rtdsp/-/blob/main/doc/dsp_study_erko.txt,
    summary of DSP books
[2] python package rtdsp and Jupyter notebooks, for radio telescope DSP
    investigations in Python at https://git.astron.nl/rtsd/rtdsp
[3] pyfda, dsp, at https://github.com/chipmuenk
