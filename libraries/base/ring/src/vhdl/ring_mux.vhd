-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose: Implement the function of ring_mux.
-- Description: See https://support.astron.nl/confluence/x/jyu7Ag
-- Please see dp_mux for usage of generics.

-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity ring_mux is
  generic (
    g_bsn_w             : natural := 16;
    g_data_w            : natural := 16;
    g_empty_w           : natural := 1;
    g_channel_w         : natural := 1;
    g_error_w           : natural := 1;
    g_use_bsn           : boolean := true;
    g_use_empty         : boolean := true;
    g_use_error         : boolean := true;
    g_use_sync          : boolean := true;
    g_fifo_af_xon       : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_size         : t_natural_arr := array_init(1024, 2)  -- must match c_nof_input
  );
  port (
    -- Clocks and reset

    dp_clk : in  std_logic;
    dp_rst : in  std_logic;

    remote_sosi : in  t_dp_sosi := c_dp_sosi_rst;
    remote_siso : out t_dp_siso;
    local_sosi  : in  t_dp_sosi := c_dp_sosi_rst;
    local_siso  : out t_dp_siso;

    mux_sosi    : out t_dp_sosi;
    mux_siso    : in  t_dp_siso := c_dp_siso_rdy

  );
end ring_mux;

architecture str of ring_mux is
  constant c_nof_input : natural := 2;

  signal dp_mux_in_sosi_arr : t_dp_sosi_arr(0 to c_nof_input - 1);
  signal dp_mux_in_siso_arr : t_dp_siso_arr(0 to c_nof_input - 1);
begin
  -- rewire dp_mux inputs
  dp_mux_in_sosi_arr(0) <= local_sosi;
  dp_mux_in_sosi_arr(1) <= remote_sosi;
  local_siso  <= dp_mux_in_siso_arr(0);
  remote_siso <= dp_mux_in_siso_arr(1);

  u_dp_mux : entity dp_lib.dp_mux
  generic map (
    g_nof_input         => c_nof_input,
    g_append_channel_lo => false,  -- Keep channels the same as the input.
    g_use_fifo          => true,
    g_bsn_w             => g_bsn_w,
    g_data_w            => g_data_w,
    g_empty_w           => g_empty_w,
    g_in_channel_w      => g_channel_w,
    g_error_w           => g_error_w,
    g_use_bsn           => g_use_bsn,
    g_use_empty         => g_use_empty,
    g_use_in_channel    => true,
    g_use_error         => g_use_error,
    g_use_sync          => g_use_sync,
    g_fifo_af_xon       => g_fifo_af_xon,
    g_fifo_size         => g_fifo_size
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    snk_out_arr => dp_mux_in_siso_arr,
    snk_in_arr  => dp_mux_in_sosi_arr,

    src_in      => mux_siso,
    src_out     => mux_sosi
  );
end str;
