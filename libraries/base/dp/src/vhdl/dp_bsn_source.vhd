-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose :
--   Start a periodic block sync interval and maintain a block sequence
--   number
-- Description:
--   When dp_on is low then all outputs are low. When dp_on is high, the
--   output sync starts pulsing with a period of g_block_size number of clk
--   cycles and the output valid, sop and eop will be active.
--   Alternatively, one can assert dp_on while dp_on_pps is high to
--   start the data path on the next PPS.
-- Remarks:
--   Starting the data path is only possible from the dp_off state, so one
--   has to disable (dp_on='0') the data path before restarting it.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_bsn_source is
  generic (
    g_block_size         : natural := 256;
    g_nof_block_per_sync : natural := 8;
    g_bsn_w              : natural := 48
  );
  port (
    rst                : in  std_logic;
    clk                : in  std_logic;
    pps                : in  std_logic := '1';

    dp_on              : in  std_logic;
    dp_on_pps          : in  std_logic;

    dp_on_status       : out std_logic;

    init_bsn           : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    nof_block_per_sync : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_nof_block_per_sync, c_word_w);

    src_out            : out t_dp_sosi  -- only uses sync, bsn[], valid, sop and eop
  );
end dp_bsn_source;

architecture rtl of dp_bsn_source is
  constant c_block_size_cnt_w : natural := ceil_log2(g_block_size);
  constant c_block_cnt_zero   : std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');

  type t_state_enum is (s_init, s_dp_off, s_dp_on_sop, s_dp_on, s_dp_on_eop);

  signal state       : t_state_enum;
  signal nxt_state   : t_state_enum;
  signal prev_state  : t_state_enum;

  signal block_size_cnt     : std_logic_vector(c_block_size_cnt_w - 1 downto 0);
  signal nxt_block_size_cnt : std_logic_vector(c_block_size_cnt_w - 1 downto 0);

  signal block_cnt          : std_logic_vector(g_bsn_w - 1 downto 0);
  signal nxt_block_cnt      : std_logic_vector(g_bsn_w - 1 downto 0);

  signal i_src_out          : t_dp_sosi := c_dp_sosi_init;
  signal nxt_src_out        : t_dp_sosi;

  signal nxt_dp_on_status   : std_logic;
  signal i_dp_on_status     : std_logic;
begin
  src_out <= i_src_out;
  dp_on_status <= i_dp_on_status;

  p_state : process(state, prev_state, dp_on, dp_on_pps, pps, block_size_cnt, block_cnt, nof_block_per_sync, init_bsn, i_src_out, i_dp_on_status)
  begin
    nxt_state           <= state;
    nxt_src_out         <= i_src_out;
    nxt_src_out.sync    <= '0';
    nxt_src_out.valid   <= '0';
    nxt_src_out.sop     <= '0';
    nxt_src_out.eop     <= '0';
    nxt_block_size_cnt  <= block_size_cnt;
    nxt_block_cnt       <= block_cnt;
    nxt_dp_on_status    <= i_dp_on_status;

    case state is

      when s_dp_on_eop =>
        nxt_src_out.eop    <= '1';
        nxt_src_out.valid  <= '1';
        nxt_state          <= s_dp_on_sop;
        nxt_block_size_cnt <= INCR_UVEC(block_size_cnt, 1);
        if unsigned(block_cnt) = unsigned(nof_block_per_sync) - 1 then
          nxt_block_cnt    <=  (others => '0');
        else
          nxt_block_cnt    <= INCR_UVEC(block_cnt, 1);
        end if;
        if dp_on = '0' then
          nxt_state <= s_dp_off;
        end if;

      when s_dp_on =>
        nxt_src_out.valid  <= '1';
        nxt_block_size_cnt <= INCR_UVEC(block_size_cnt, 1);
        if unsigned(block_size_cnt) = g_block_size - 3 then
          nxt_state        <= s_dp_on_eop;
        end if;

      when s_dp_on_sop =>
        nxt_dp_on_status   <= '1';
        nxt_src_out.sop    <= '1';
        nxt_src_out.valid  <= '1';
        nxt_state          <= s_dp_on;
        nxt_block_size_cnt <= (others => '0');
        if prev_state = s_dp_on_eop then
          nxt_src_out.bsn(g_bsn_w - 1 downto 0) <= INCR_UVEC(i_src_out.bsn(g_bsn_w - 1 downto 0), 1);
        end if;
        if unsigned(block_cnt) = unsigned(c_block_cnt_zero) then
          nxt_src_out.sync <= '1';
        end if;

      when s_dp_off =>
        nxt_dp_on_status <= '0';
        nxt_block_cnt <=  (others => '0');
        nxt_src_out.bsn <= RESIZE_DP_BSN(init_bsn);
        if dp_on = '1' then
          if dp_on_pps = '1' then
            if pps = '1' then
              nxt_state        <= s_dp_on_sop;
            end if;
          else
            nxt_state        <= s_dp_on_sop;
          end if;
        end if;

      when others =>  -- s_init
        nxt_state <= s_dp_off;
    end case;
  end process;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      prev_state      <= s_init;
      state           <= s_init;
      i_src_out       <= c_dp_sosi_rst;
      block_cnt       <= (others => '0');
      block_size_cnt  <= (others => '0');
      i_dp_on_status  <= '0';
    elsif rising_edge(clk) then
      prev_state      <= state;
      state           <= nxt_state;
      i_src_out       <= nxt_src_out;
      block_cnt       <= nxt_block_cnt;
      block_size_cnt  <= nxt_block_size_cnt;
      i_dp_on_status  <= nxt_dp_on_status;
    end if;
  end process;
end rtl;
