###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Apply timing commands and definition of clock groups

derive_pll_clocks

derive_clock_uncertainty

# Effectively set false path from this clock to all other clocks
set_clock_groups -asynchronous -group [get_clocks altera_reserved_tck]
set_clock_groups -asynchronous -group [get_clocks SB_CLK]
set_clock_groups -asynchronous -group [get_clocks SA_CLK]
set_clock_groups -asynchronous -group [get_clocks ETH_CLK]

set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[0]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[1]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[2]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[3]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[0]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[1]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[2]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[3]]

# QSYS, second PLL
set_clock_groups -asynchronous -group [get_clocks *|altpll_1|sd1|pll7|clk[0]]

set_clock_groups -asynchronous -group [get_clocks CLK]
set_clock_groups -asynchronous -group [get_clocks {*|altpll_component|auto_generated|pll1|clk[0]}]
set_clock_groups -asynchronous -group [get_clocks ADC_BI_A_CLK]
set_clock_groups -asynchronous -group [get_clocks ADC_BI_D_CLK]

# Transceivers: ALTGX generated RX clock
set_clock_groups -asynchronous -group [get_clocks {*|receive_pcs0|clkout}]
# Transceivers: ALTGX generated TX clock
set_clock_groups -asynchronous -group [get_clocks {*|transmit_pcs0|clkout}]

