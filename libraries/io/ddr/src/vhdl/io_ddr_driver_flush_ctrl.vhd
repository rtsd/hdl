--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Flush the write input FIFO in io_ddr when not doing a write access.
-- Description:
--   The write input FIFO is flushed when the io_ddr is not doing a write
--   access, so when idle or also when doing a read access. The flushing
--   ensures that the FIFO does not run full.
--   When the io_ddr is starting a new write access, then the write input FIFO
--   gets filled. The filling starts dependent on:
--
--   . g_mode = "VAL" : immediately start filling on next valid data
--   . g_mode = "SOP" : start filling on next sop
--   . g_mode = "SYN" : start filling on next sync
--
--   . g_use_channel = TRUE : start filling when channel matches g_start_channel
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity io_ddr_driver_flush_ctrl is
  generic (
    g_mode          : string := "VAL";  -- "VAL", "SOP", "SYN"
    g_use_channel   : boolean := false;
    g_start_channel : natural := 0;
    g_nof_channels  : positive := 1
   );
  port (
    clk                : in  std_logic;
    rst                : in  std_logic;

    -- Inputs
    dvr_en             : in  std_logic := '1';
    dvr_wr_flush_en    : in  std_logic := '1';
    dvr_wr_not_rd      : in  std_logic;
    dvr_done           : in  std_logic;
    ctlr_wr_sosi       : in  t_dp_sosi;

    -- Output
    ctlr_wr_flush_en   : out std_logic;
    state_vec          : out std_logic_vector(1 downto 0)
   );
end io_ddr_driver_flush_ctrl;

architecture str of io_ddr_driver_flush_ctrl is
  constant c_channel_w     : natural := ceil_log2(g_nof_channels);

  type t_state is (s_idle, s_flush, s_stop);

  signal state     : t_state;
  signal nxt_state : t_state;

  signal channel   : natural range 0 to g_nof_channels - 1;

  signal flush_dis : std_logic;

  signal nxt_ctlr_wr_flush_en : std_logic;
begin
  -- Flush disable control
  no_channel: if g_use_channel = false generate
    gen_valid : if g_mode = "VAL" generate flush_dis <= ctlr_wr_sosi.valid; end generate;
    gen_sop   : if g_mode = "SOP" generate flush_dis <= ctlr_wr_sosi.sop; end generate;
    gen_sync  : if g_mode = "SYN" generate flush_dis <= ctlr_wr_sosi.sync; end generate;
  end generate;

  use_channel: if g_use_channel = true generate
    channel <= TO_UINT(ctlr_wr_sosi.channel(c_channel_w - 1 downto 0));

    gen_valid : if g_mode = "VAL" generate flush_dis <= '1' when ctlr_wr_sosi.valid = '1' and channel = g_start_channel else '0'; end generate;
    gen_sop   : if g_mode = "SOP" generate flush_dis <= '1' when ctlr_wr_sosi.sop  = '1' and channel = g_start_channel else '0'; end generate;
    gen_sync  : if g_mode = "SYN" generate flush_dis <= '1' when ctlr_wr_sosi.sync = '1' and channel = g_start_channel else '0'; end generate;
  end generate;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      state            <= s_flush;  -- default start in flush mode after power up,
      ctlr_wr_flush_en <= '1';  -- so default write flush is enabled
    elsif rising_edge(clk) then
      state            <= nxt_state;
      ctlr_wr_flush_en <= nxt_ctlr_wr_flush_en;
    end if;
  end process;

  p_state : process(state, dvr_wr_flush_en, dvr_done, dvr_en, dvr_wr_not_rd, flush_dis)
  begin
    nxt_state <= state;
    nxt_ctlr_wr_flush_en <= '0';
    case state is
      when s_idle =>
        state_vec <= TO_UVEC(1, 2);
        if dvr_wr_flush_en = '1' and dvr_done = '1' then
          nxt_ctlr_wr_flush_en <= '1';
          nxt_state <= s_flush;
        end if;
      when s_flush =>
        state_vec <= TO_UVEC(2, 2);
        nxt_ctlr_wr_flush_en <= '1';
        if dvr_en = '1' and dvr_wr_not_rd = '1' then
          nxt_state <= s_stop;
        end if;
      when others =>  -- s_stop
        state_vec <= TO_UVEC(3, 2);
        nxt_ctlr_wr_flush_en <= '1';
        if flush_dis = '1' then  -- flush_dis comes from sosi control (valid, sop or sync) from look ahead (RL=0) write FIFO
          nxt_ctlr_wr_flush_en <= '0';
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
end str;
