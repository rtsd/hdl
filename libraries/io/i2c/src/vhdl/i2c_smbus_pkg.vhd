-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package i2c_smbus_pkg is
  -- Opcodes used in protocol definitions
  type OPCODE is (
      -- I2C opcodes
      OP_LD_ADR,  -- LOAD ADDRESS REGISTER
      OP_LD_CNT,  -- LOAD COUNTER REGISTER
      OP_WR_CNT,  -- WRITE COUNTER REGISTER
      OP_WR_ADR_WR,  -- WRITE ADDRESS FOR WRTTE
      OP_WR_ADR_RD,  -- WRITE ADDRESS FOR READ
      OP_WR_DAT,  -- WRITE BYTE OF DATA
      OP_WR_BLOCK,  -- WRITE BLOCK OF DATA
      OP_RD_ACK,  -- READ BYTE OF DATA AND ACKNOWLEDGE
      OP_RD_NACK,  -- READ BYTE DATA AND DO NOT ACKNOWLEDGE
      OP_RD_BLOCK,  -- READ BLOCK OF DATA
      OP_STOP,  -- STOP
      -- Control opcodes
      OP_IDLE,  -- IDLE
      OP_END,  -- END OF LIST OF PROTOCOLS
      OP_LD_TIMEOUT,  -- LOAD TIMEOUT VALUE
      OP_WAIT,  -- WAIT FOR TIMEOUT TIME UNITS
      OP_RD_SDA  -- SAMPLE SDA LINE
  );

  -- SMBUS protocol definitions
  -- a protocol is implemented as fixed length array of opcodes
  type SMBUS_PROTOCOL is  array (0 to 15) of OPCODE;

  -- The following protocols are as defined in the System Management Bus Specification v2.0

  -- Protocol: reserved
  constant PROTOCOL_RESERVED : SMBUS_PROTOCOL := ( others => OP_IDLE );

  -- Protocol: write only the address + write bit
  constant PROTOCOL_WRITE_QUICK  : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_STOP, others => OP_IDLE );

  -- Protocol: write only address + read bit
  constant PROTOCOL_READ_QUICK : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_RD, OP_STOP, others => OP_IDLE );

  -- Protocol: send byte to specified adress
  constant PROTOCOL_SEND_BYTE : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_STOP, others => OP_IDLE );

  -- Protocol: receive byte from specified address
  constant PROTOCOL_RECEIVE_BYTE : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_RD, OP_RD_NACK, OP_STOP, others => OP_IDLE );

  -- Protocol: write byte to specified address and register
  constant PROTOCOL_WRITE_BYTE : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_DAT, OP_STOP, others => OP_IDLE );

  -- Protocol: read byte from specified address and register
  constant PROTOCOL_READ_BYTE : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_ADR_RD, OP_RD_NACK, OP_STOP,
      others => OP_IDLE );

  -- Protocol: write word to specified address and register
  constant PROTOCOL_WRITE_WORD : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_DAT, OP_WR_DAT, OP_STOP,
      others => OP_IDLE );

  -- Protocol: read word from specified address and register
  constant PROTOCOL_READ_WORD : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_ADR_RD, OP_RD_ACK, OP_RD_NACK,
      OP_STOP, others => OP_IDLE);

  -- Protocol: write block to specified address and register
  constant PROTOCOL_WRITE_BLOCK : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_LD_CNT, OP_WR_CNT, OP_WR_BLOCK,
      OP_STOP, others => OP_IDLE);

  -- Protocol: read block to specified address and register
  constant PROTOCOL_READ_BLOCK : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_ADR_RD, OP_RD_ACK, OP_LD_CNT,
      OP_RD_BLOCK, OP_STOP, others => OP_IDLE);

  -- Protocol process call
  constant PROTOCOL_PROCESS_CALL : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_DAT, OP_WR_DAT,
      OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_ADR_RD, OP_RD_ACK, OP_RD_NACK,
      OP_STOP, others => OP_IDLE);

  -- The following protocols are additional custom protocols

  -- Protocol: write block to specified address and register, do not write count
  constant PROTOCOL_C_WRITE_BLOCK_NO_CNT : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_LD_CNT, OP_WR_BLOCK, OP_STOP,
      others => OP_IDLE);

  -- Protocol: read block to specified address and register, do not expect count
  constant PROTOCOL_C_READ_BLOCK_NO_CNT : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_WR_DAT, OP_WR_ADR_RD, OP_LD_CNT, OP_RD_BLOCK,
      OP_STOP, others => OP_IDLE);

  -- Protocol: send one or more bytes to specified address
  constant PROTOCOL_C_SEND_BLOCK : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_WR, OP_LD_CNT, OP_WR_BLOCK, OP_STOP, others => OP_IDLE );

  -- Protocol: receive one or more bytes from specified address
  constant PROTOCOL_C_RECEIVE_BLOCK : SMBUS_PROTOCOL :=
    ( OP_LD_ADR, OP_WR_ADR_RD, OP_LD_CNT, OP_RD_BLOCK, OP_STOP, others => OP_IDLE );

  -- Protocol: no operation
  constant PROTOCOL_C_NOP : SMBUS_PROTOCOL := ( others => OP_IDLE );

  -- Protocol: wait for the specified number (32 bit long) of delay units
  constant PROTOCOL_C_WAIT : SMBUS_PROTOCOL :=
    ( OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_WAIT, others => OP_IDLE );

  -- Protocol: signal end of sequence of protocols
  constant PROTOCOL_C_END : SMBUS_PROTOCOL :=
    ( OP_END, others => OP_IDLE );

  -- Protocol: sample SDA line
  -- . Use protocol sample SDA after sufficient WAIT timeout or stand alone to ensure that the slow SDA has been pulled up
  constant PROTOCOL_C_SAMPLE_SDA : SMBUS_PROTOCOL :=
    ( OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_LD_TIMEOUT, OP_WAIT, OP_RD_SDA, others => OP_IDLE );

  type PROTOCOL_ARRAY is array (natural range <>) of SMBUS_PROTOCOL;

  -- Protocol list
  -- This maps a protocol identifier to the corresponding protocol
  constant SMBUS_PROTOCOLS : PROTOCOL_ARRAY := (
    -- Official SMBus protocols
    PROTOCOL_RESERVED,  -- 00
    PROTOCOL_RESERVED,  -- 01
    PROTOCOL_WRITE_QUICK,  -- 02
    PROTOCOL_READ_QUICK,  -- 03
    PROTOCOL_SEND_BYTE,  -- 04
    PROTOCOL_RECEIVE_BYTE,  -- 05
    PROTOCOL_WRITE_BYTE,  -- 06
    PROTOCOL_READ_BYTE,  -- 07
    PROTOCOL_WRITE_WORD,  -- 08
    PROTOCOL_READ_WORD,  -- 09
    PROTOCOL_WRITE_BLOCK,  -- 0A
    PROTOCOL_READ_BLOCK,  -- 0B
    PROTOCOL_PROCESS_CALL,  -- 0C
    -- Additional custom protocols
    PROTOCOL_C_WRITE_BLOCK_NO_CNT,  -- 0D
    PROTOCOL_C_READ_BLOCK_NO_CNT,  -- 0E
    PROTOCOL_C_SEND_BLOCK,  -- 0F
    PROTOCOL_C_RECEIVE_BLOCK,  -- 10
    PROTOCOL_C_NOP,  -- 11
    PROTOCOL_C_WAIT,  -- 12
    PROTOCOL_C_END,  -- 13
    PROTOCOL_C_SAMPLE_SDA  -- 14
  );

  -- SMBUS protocol identifiers.
  -- As defined in the SMBUS Control Method Interface Specification v1.0
  constant SMBUS_RESERVED_0            : natural := 16#00#;
  constant SMBUS_RESERVED_1            : natural := 16#01#;
  constant SMBUS_WRITE_QUICK           : natural := 16#02#;
  constant SMBUS_READ_QUICK            : natural := 16#03#;
  constant SMBUS_SEND_BYTE             : natural := 16#04#;
  constant SMBUS_RECEIVE_BYTE          : natural := 16#05#;
  constant SMBUS_WRITE_BYTE            : natural := 16#06#;
  constant SMBUS_READ_BYTE             : natural := 16#07#;
  constant SMBUS_WRITE_WORD            : natural := 16#08#;
  constant SMBUS_READ_WORD             : natural := 16#09#;
  constant SMBUS_WRITE_BLOCK           : natural := 16#0A#;
  constant SMBUS_READ_BLOCK            : natural := 16#0B#;
  constant SMBUS_PROCESS_CALL          : natural := 16#0C#;
  -- Extra custom protocols identifiers
  constant SMBUS_C_WRITE_BLOCK_NO_CNT  : natural := 16#0D#;
  constant SMBUS_C_READ_BLOCK_NO_CNT   : natural := 16#0E#;
  constant SMBUS_C_SEND_BLOCK          : natural := 16#0F#;
  constant SMBUS_C_RECEIVE_BLOCK       : natural := 16#10#;
  constant SMBUS_C_NOP                 : natural := 16#11#;
  constant SMBUS_C_WAIT                : natural := 16#12#;
  constant SMBUS_C_END                 : natural := 16#13#;
  constant SMBUS_C_SAMPLE_SDA          : natural := 16#14#;

  constant c_smbus_unknown_protocol    : natural := 16#14#;  -- Equal to largest valid SMBUS protocol ID

  constant c_smbus_timeout_nof_byte    : natural := 4;  -- Four byte timeout value set via OP_LD_TIMEOUT
  constant c_smbus_timeout_word_w      : natural := c_smbus_timeout_nof_byte * 8;
  constant c_smbus_timeout_w           : natural := 28;  -- Only use 28 bits for actual timeout counter, 2^28 > 200M cycles in 1 sec
end package;
