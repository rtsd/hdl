--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for the MegaWizard-generated ip_stratixiv_phy_xaui_0.
-- Description:
-- . Block diagram:
--
--                Hard
--   XGMII  <-->  PCS  <-->  PMA  <--> XAUI serial
--
-- . With c_sel=0 and c_xgmii_d_test the tb is self checking xgmii_rx_d.
-- . The link_fault stimulates the link recovery and then also shows that the
--   hi and lo word of the 64b rx_xgmii_d can get swapped. This is not merely
--   an one word offset because with c_xgmii_d_count it shows that the words
--   get swapped.
-- . For the rx_clk it appears that at least in simulation:
--   - rx_clk = tr_clk, as can be checked by using g_tr_clk_156_period =
--     6.400010 ns.
--   - The rx_clk can be used as tx_clk because the rx_clk does not stop when
--     the link breaks. This is also done in the altera_eth_10g_mac_xaui
--     reference design and also shown in Figure 10.1 in
--     http://www.altera.com/literature/ug/10Gbps_MAC.pdf
--   - The rx_clk phase does not depend on the link delay, so it seems to be
--     from the local tr_clk and not derived from the recovered rx clock.
-- . Status signals:
--   - tx_ready: Indicates PMA TX has exited the reset state and the
--               transceiver can transmit data.
--   - rx_ready: Indicates PMA RX has exited the reset state and the
--               transceiver can receive data.
--   The rx_ready goes high after the tx_ready and both remain high also when
--   the link breaks. In the altera_eth_10g_mac_xaui reference design both are
--   used for tx flow control (ready) and to enable data valid.
--
--   - rx_channelaligned: When asserted, indicates that all 4 RX channels
--                        are aligned.
--   - rx_syncstatus[7:0]: Synchronization indication. RX synchronization is
--                         The indicated on the rx_syncstatus port of each
--                         channel. rx_syncstatus signal is 2 bits per channel
--                         for a total of 8 bits per hard XAUI link.
--   The rx_channelaligned is asserted after the rx_syncstatus, both go low
--   when the link breaks.
--
-- Usage:
--   > do wave_tb_ip_stratixiv_phy_xaui.do
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tb_ip_stratixiv_phy_xaui is
  generic (
    g_sim_time                : time := 5 us;
    g_tr_clk_156_period       : time := 6.4 ns;
    g_verify_link_recovery    : boolean := true;
    g_use_xaui_rx_serial_in   : boolean := false
  );
  port (
    xaui_tx_serial_out  : out std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
    xaui_rx_serial_in   : in  std_logic_vector(c_nof_xaui_lanes - 1 downto 0)
  );
end entity tb_ip_stratixiv_phy_xaui;

architecture str of tb_ip_stratixiv_phy_xaui is
  constant c_phy_loopback        : boolean := not g_use_xaui_rx_serial_in;
  constant c_mm_clk_period       : time := 25 ns;  -- 40 MHz

  constant c_sel                 : natural := 0;
  constant c_xgmii_d_test        : std_logic_vector(c_xgmii_data_w - 1 downto 0) := x"DEADBEEFCAFEBABE";  -- c_sel = 0
  constant c_xgmii_d_count       : std_logic_vector(c_xgmii_data_w - 1 downto 0) := x"0001020304050607";  -- c_sel = 1

  signal tr_clk                  : std_logic := '0';
  signal tx_clk                  : std_logic := '0';
  signal rx_clk                  : std_logic;

  signal mm_clk                  : std_logic := '0';
  signal mm_rst                  : std_logic := '1';

  signal link_fault              : std_logic := '0';
  signal xaui_tx_serial          : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
  signal xaui_tx_serial_dly      : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
  signal xaui_rx_serial          : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);

  --XGMII data and control combined:
  signal xgmii_tx_dc             : std_logic_vector(c_xgmii_w - 1 downto 0);
  signal xgmii_rx_dc             : std_logic_vector(c_xgmii_w - 1 downto 0);

  --XGMII control bits (one for each XGMII lane):
  signal xgmii_tx_c              : std_logic_vector(c_xgmii_ctrl_w - 1 downto 0);
  signal xgmii_rx_c              : std_logic_vector(c_xgmii_ctrl_w - 1 downto 0);

  --XGMII data
  signal xgmii_tx_d              : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal xgmii_rx_d              : std_logic_vector(c_xgmii_data_w - 1 downto 0);

  -- Important status signal: only send user data after you know the receiver's channel_aligned='1'.
  signal rx_channelaligned       : std_logic;
begin
  tr_clk  <= not tr_clk after g_tr_clk_156_period / 2;
  tx_clk  <= not tx_clk after g_tr_clk_156_period / 2;
  mm_clk  <= not mm_clk after c_mm_clk_period / 2;

  p_mm_rst : process
  begin
    mm_rst <= '1';
--    WAIT FOR 4*c_mm_clk_period; -- Releasing reset here instead of after 80ns will cause the 64bit RX boundary to be 'correct'.
    wait for 6 * c_mm_clk_period;  -- We don't want to base rest of development on lucky alignment. This 80ns delay gives us a normal, 32-bit RX boundary.
    wait until rising_edge(mm_clk);
    mm_rst <= '0';
    wait;
  end process;

  p_xgmii_data_ctrl : process
  begin
    while true loop
      xgmii_tx_c <= c_xgmii_c_idle;
      xgmii_tx_d <= c_xgmii_d_idle;

      wait until rx_channelaligned = '1';
      wait until rising_edge(tx_clk);

      xgmii_tx_c <= c_xgmii_c_data;
      if c_sel = 0 then
        xgmii_tx_d <= c_xgmii_d_test;
      else
        xgmii_tx_d <= c_xgmii_d_count;
      end if;
      wait until rising_edge(tx_clk);

      while rx_channelaligned = '1' loop
        if c_sel = 1 then
          for I in 0 to 7 loop
            xgmii_tx_d((I + 1) * 8 - 1 downto I * 8) <= INCR_UVEC(xgmii_tx_d((I + 1) * 8 - 1 downto I * 8), 8);
          end loop;
        end if;
        wait until rising_edge(tx_clk);
      end loop;
    end loop;

    wait;
  end process;

  p_xgmii_data_verify : process
  begin
    if c_sel = 0 then
      wait for 2 us;
      assert xgmii_rx_d = c_xgmii_d_test or
             xgmii_rx_d = c_xgmii_d_test(31 downto 0) & c_xgmii_d_test(63 downto 32)
        report "tb_ip_stratixiv_phy_xaui : Wrong xgmii_rx_d result value"
        severity ERROR;
      wait for g_sim_time - 2 us;
      assert xgmii_rx_d = c_xgmii_d_test or
             xgmii_rx_d = c_xgmii_d_test(31 downto 0) & c_xgmii_d_test(63 downto 32)
        report "tb_ip_stratixiv_phy_xaui : Wrong xgmii_rx_d result value"
        severity ERROR;
    end if;
    assert false
      report "Simulation finished."
      severity FAILURE;
    wait;
  end process;

  -- Combine data and control into XGMII
  xgmii_tx_dc <= func_xgmii_dc(xgmii_tx_d, xgmii_tx_c);

  -- Extract data (d) from combined data+control (dc) XGMII
  xgmii_rx_d <= func_xgmii_d(xgmii_rx_dc);
  xgmii_rx_c <= func_xgmii_c(xgmii_rx_dc);

  -- DUT:
  u_ip_phy_xaui : entity work.ip_stratixiv_phy_xaui_0
  port map (
    pll_ref_clk          => tr_clk,
    xgmii_tx_clk         => tx_clk,  -- rx_clk
    xgmii_rx_clk         => rx_clk,
    xgmii_rx_dc          => xgmii_rx_dc,
    xgmii_tx_dc          => xgmii_tx_dc,
    xaui_rx_serial_data  => xaui_rx_serial,
    xaui_tx_serial_data  => xaui_tx_serial,
    rx_ready             => OPEN,
    tx_ready             => OPEN,
    phy_mgmt_clk         => mm_clk,
    phy_mgmt_clk_reset   => mm_rst,
    phy_mgmt_address     => (others => '0'),
    phy_mgmt_read        => '0',
    phy_mgmt_readdata    => OPEN,
    phy_mgmt_write       => '0',
    phy_mgmt_writedata   => (others => '0'),
    phy_mgmt_waitrequest => OPEN,

    rx_channelaligned    => rx_channelaligned
  );

  xaui_tx_serial_dly <= transport xaui_tx_serial after 100 ps;

  gen_verify_link_recovery : if g_verify_link_recovery = true generate
    link_fault <= '1' after 2.5 us, '0' after 3 us;
  end generate;

  xaui_tx_serial_dly <= transport xaui_tx_serial after 100 ps;
  xaui_tx_serial_out <= xaui_tx_serial_dly;

  p_xaui_rx_serial : process(xaui_tx_serial_dly, xaui_rx_serial_in, link_fault)
  begin
    if c_phy_loopback = true then
      xaui_rx_serial <= xaui_tx_serial_dly;
    else
      xaui_rx_serial <= xaui_rx_serial_in;
    end if;
    if link_fault = '1' then
      xaui_rx_serial <= (others => '0');
    end if;
  end process;
end architecture str;
