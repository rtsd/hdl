###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_W34 -to CLK
set_location_assignment PIN_AF34 -to PPS
set_location_assignment PIN_AD31 -to WDI
set_location_assignment PIN_AB31 -to INTA
set_location_assignment PIN_AB30 -to INTB
set_location_assignment PIN_AU34 -to termination_blk0~_rup_pad
set_location_assignment PIN_AV34 -to termination_blk0~_rdn_pad

set_instance_assignment -name IO_STANDARD "DIFFERENTIAL LVPECL" -to CLK
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL LVPECL" -to PPS
set_instance_assignment -name IO_STANDARD "2.5 V" -to WDI
set_instance_assignment -name IO_STANDARD "2.5 V" -to INTA
set_instance_assignment -name IO_STANDARD "2.5 V" -to INTB

###############################################################################
# Directly set input delay for PPS (also see aduh_dd.sdc for explanation)
# . The delay setting should match the capture_edge selection via MM, default
#   use the rising edge of the sys_clk to capture the PPS.
# . The rising_edge of the sys_clk also depends on the PLL output phase that is 
#   used for the sys_clk.
###############################################################################

set_instance_assignment -name D2_DELAY 0 -to PPS
set_instance_assignment -name D3_DELAY 0 -to PPS
set_instance_assignment -name D1_DELAY 0 -to PPS

#set_instance_assignment -name PLL_COMPENSATE ON -to PPS
