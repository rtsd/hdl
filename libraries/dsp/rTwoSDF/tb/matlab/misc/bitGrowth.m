%% bit growth mesh in R2SDF FFT
% 
%-------------------------------------------------------------------------%
%   Author: Raj Thilak Rajan : rajan at astron.nl: Feb 2010
%   Copyright (C) 2009-2010
%   ASTRON (Netherlands Institute for Radio Astronomy)
%   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
% 
%   This file is part of the UniBoard software suite.
%   The file is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------%

clear all; close all; clc
'Rows: # of Bits, Colomns: # of points'
k=16; p=2^k; b=16;
K=[1:k]; Bin=[1:b];
B=zeros(size(Bin,2)+1,size(K,2)+1);

for ii=1:size(Bin,2)
  Bdel(ii,:)= ceil(0.5*(1+(K)));
  Bout(ii,:)= ceil(Bin(ii)+Bdel(ii,:));
end;

B(1,2:k+1)=2.^K;
B(2:size(B,1),:)=[Bin.', Bout]
