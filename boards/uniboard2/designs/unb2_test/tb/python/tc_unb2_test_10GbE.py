#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

""" Test case for unb2_test_ddr

Description:
  Tx seq --> DDR4 --> Rx seq
  
Usage:

  1) Load and run simulation of tb_unb2_test_ddr_MB_I, tb_unb2_test_ddr_MB_II or tb_unb2_test_ddr_MB_I_II
  2) > python tc_unb2_test_ddr.py --sim --unb 0 --gn 3 -v 5 -s I --rep 1
  3) After about 160 us cal_ok
  
"""

###############################################################################
# System imports
import sys
import test_case
import node_io
import pi_diag_tx_seq
import pi_diag_rx_seq
import pi_diag_data_buffer
#import pi_io_ddr

from tools import *
from common import *
from pi_common import *


##################################################################################################################
# Main
#
# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test case for the unb2_test_ddr design with MB = %s on %s' % (tc.gpString, tc.unb_nodes_string()))
tc.append_log(3, '>>>')
tc.append_log(3, '')
    
# Create access object for all nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)
    
# Create instances for the periperals
mb_list = tc.gpString.split(',');

nof_streams = 1 #24

io_ddr     = dict()
tx_seq_ddr = dict()
rx_seq_ddr = dict()
rx_db_ddr  = dict()
for mb in mb_list:
    if mb=='I' or mb=='II':
        tx_seq_ddr[mb] = pi_diag_tx_seq.PiDiagTxSeq(tc, io, inst_name="10GBE", nof_inst=nof_streams)
        rx_seq_ddr[mb] = pi_diag_rx_seq.PiDiagRxSeq(tc, io, inst_name="10GBE", nof_inst=nof_streams)
        rx_db_ddr[mb]  = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName="10GBE", nofStreams=nof_streams)
    else:
        sys.exit("Wrong type of MB argument, must be -s I or -s II or -s I,II")


##################################################################################################################
# Test

# Wait for power up (reset release)
io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))

# Control defaults
nof_mon = 5
nof_words = 1000 #tc.number

for rep in range(tc.repeat):
    tc.append_log(5, '')
    tc.append_log(3, '>>> Rep-%d' % rep)
    
    # Use separate for-loop sections to access the MB I and MB II more simultaneously instead of sequentially
    for mb in mb_list:
        # Initialization
        tx_seq_ddr[mb].write_disable(vLevel=5)
        rx_seq_ddr[mb].write_disable(vLevel=5)
        
    for mb in mb_list:
        # Tx sequence start
        tx_seq_ddr[mb].write_enable_cntr(vLevel=5)
        
        # Tx sequence monitor
        for mon in range(nof_mon):
            io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
            tx_seq_ddr[mb].read_cnt(vLevel=5)

    rx_db_ddr[mb].read_data_buffer()
        
    for mb in mb_list:
        # Rx sequence start
        rx_seq_ddr[mb].write_enable_cntr(vLevel=5)
        
        # Rx sequence monitor
        for mon in range(nof_mon):
            io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
            rx_seq_ddr[mb].read_cnt(vLevel=5)
        
    for mb in mb_list:
        rx_seq_ddr[mb].read_result(vLevel=5)
        

# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(0, '>>> Runtime=%f seconds (%f hours)' % (tc.get_run_time(),tc.get_run_time()/3600))
tc.append_log(3, '>>>')

