--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   The FIFO starts outputting data when the output is ready and it has been
--   filled with more than g_fifo_fill words. Given a fixed frame length, this
--   is useful when the in_val is throttled while the out_val should not be
--   inactive valid between out_sop to out_eop. This is necessary for frame
--   transport over a PHY link without separate data valid signal.
-- Description:
--   The FIFO is filled sufficiently for each input frame, as defined by the
--   sop and then read until the eop.
--   The rd_fill_32b control input is used for dynamic control of the fill
--   level on the read side of the FIFO. The rd_fill_32b defaults to
--   g_fifo_fill, so if rd_fill_32b is not connected then the fill level is
--   fixed to g_fifo_fill. A g_fifo_fill disables the fifo fill mechanism.
--   The rd_fill_32b signal must be stable in the rd_clk domain.
-- Remarks:
-- . Reuse from LOFAR rad_frame_scheduler.vhd and rad_frame_scheduler(rtl).vhd
-- . For g_fifo_fill=0 this dp_fifo_fill_core defaults to dp_fifo_core.
-- . The architecture offers two implementations via g_fifo_rl. Use 0 for show
--   ahead FIFO or 1 for normal FIFO. At the output of dp_fifo_fill_core the
--   RL=1 independent of g_fifo_rl, the g_fifo_rl only applies to the internal
--   FIFO. The show ahead FIFO uses the dp_latency_adapter to get to RL 0
--   internally. The normal FIFO is prefered, because it uses less logic. It
--   keeps the RL internally also at 1.
-- . Note that the structure of p_state is idendical in both architectures
--   for both g_fifo_rl=0 or 1. Hence the implementation of g_fifo_rl=1 with
--   dp_input_hold is an example of how to use dp_input_hold to get the same
--   behaviour as if the input had g_fifo_rl=0 as with the show ahead FIFO.
-- . To view the similarity of the p_state process for both g_fifo_rl e.g.
--   open the file in two editors or do repeatedly find (F3) on a text
--   section like 'WHEN s_fill  =>' that only occurs one in each p_state.
-- . The next_src_out = pend_src_out when src_in.ready='1'. However it is more
--   clear to only use pend_src_out and explicitely write the condition on
--   src_in.ready in the code, because then the structure of p_state is the
--   same for both g_fifo_rl=0 or 1. Furthermore using pend_src_out and
--   src_in.ready is often more clear to comprehend then using next_src_out
--   directly.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_fill_core is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_use_dual_clock : boolean := false;
    g_data_w         : natural := 16;  -- Should be 2 times the c_complex_w if g_use_complex = TRUE
    g_data_signed    : boolean := false;  -- TRUE extends g_data_w bits with the sign bit, FALSE pads g_data_w bits with zeros.
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_fill      : natural := 0;
    g_fifo_size      : natural := 256;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1  -- use RL=0 for internal show ahead FIFO, default use RL=1 for internal normal FIFO
  );
  port (
    wr_rst      : in  std_logic;
    wr_clk      : in  std_logic;
    rd_rst      : in  std_logic;
    rd_clk      : in  std_logic;
    -- Monitor FIFO filling
    wr_ful      : out std_logic;  -- corresponds to the carry bit of wr_usedw when FIFO is full
    wr_usedw    : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_usedw    : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_emp      : out std_logic;
    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = wr_usedw
    rd_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = rd_usedw
    rd_fill_32b  : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_fifo_fill, c_word_w);
    -- ST sink
    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_in      : in  t_dp_siso;
    src_out     : out t_dp_sosi
  );
end dp_fifo_fill_core;

architecture rtl of dp_fifo_fill_core is
  constant c_fifo_rl          : natural := sel_a_b(g_fifo_fill = 0, 1, g_fifo_rl);
  constant c_fifo_fill_margin : natural := g_fifo_af_margin + 2;  -- add +2 extra margin, with tb_dp_fifo_fill it follows that +1 is also enough to avoid almost full when fifo is operating near g_fifo_fill level
  constant c_fifo_size        : natural := largest(g_fifo_size, g_fifo_fill + c_fifo_fill_margin);
  constant c_fifo_size_w      : natural := ceil_log2(c_fifo_size);  -- = wr_usedw'LENGTH = rd_usedw'LENGTH

  -- The FIFO filling relies on framed data, so contrary to dp_fifo_sc the sop and eop need to be used.
  constant c_use_ctrl  : boolean := true;

  -- Define t_state as slv to avoid Modelsim warning "Nonresolved signal 'nxt_state' may have multiple sources". Due to that g_fifo_rl = 0 or 1 ar both supported.
  --TYPE t_state IS (s_idle, s_fill, s_output, s_xoff);
  constant s_idle    : std_logic_vector(1 downto 0) := "00";
  constant s_fill    : std_logic_vector(1 downto 0) := "01";
  constant s_output  : std_logic_vector(1 downto 0) := "10";
  constant s_xoff    : std_logic_vector(1 downto 0) := "11";

  signal state       : std_logic_vector(1 downto 0);  -- t_state
  signal nxt_state   : std_logic_vector(1 downto 0);  -- t_state

  signal xon_reg     : std_logic;
  signal nxt_xon_reg : std_logic;

  signal rd_siso     : t_dp_siso;
  signal rd_sosi     : t_dp_sosi := c_dp_sosi_rst;  -- initialize default values for unused sosi fields;

  signal wr_fifo_usedw  : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- = wr_usedw'RANGE
  signal rd_fifo_usedw  : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- = rd_usedw'RANGE
  signal rd_fill_ctrl   : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- used to resize rd_fill_32b to actual maximum width

  signal i_src_out   : t_dp_sosi;
  signal nxt_src_out : t_dp_sosi;

  -- Signals for g_fifo_rl=1
  signal hold_src_in  : t_dp_siso;
  signal pend_src_out : t_dp_sosi;
begin
  -- Output monitor FIFO filling
  wr_usedw <= wr_fifo_usedw;
  rd_usedw <= rd_fifo_usedw;

  -- Control FIFO fill level
  wr_usedw_32b <= RESIZE_UVEC(wr_fifo_usedw, c_word_w);
  rd_usedw_32b <= RESIZE_UVEC(rd_fifo_usedw, c_word_w);

  rd_fill_ctrl <= rd_fill_32b(c_fifo_size_w - 1 downto 0);

  gen_dp_fifo_sc : if g_use_dual_clock = false generate
    u_dp_fifo_sc : entity work.dp_fifo_sc
    generic map (
      g_technology     => g_technology,
      g_data_w         => g_data_w,
      g_data_signed    => g_data_signed,
      g_bsn_w          => g_bsn_w,
      g_empty_w        => g_empty_w,
      g_channel_w      => g_channel_w,
      g_error_w        => g_error_w,
      g_use_bsn        => g_use_bsn,
      g_use_empty      => g_use_empty,
      g_use_channel    => g_use_channel,
      g_use_error      => g_use_error,
      g_use_sync       => g_use_sync,
      g_use_ctrl       => c_use_ctrl,
      g_use_complex    => g_use_complex,
      g_fifo_size      => c_fifo_size,
      g_fifo_af_margin => g_fifo_af_margin,
      g_fifo_af_xon    => g_fifo_af_xon,
      g_fifo_rl        => c_fifo_rl
    )
    port map (
      rst         => rd_rst,
      clk         => rd_clk,
      -- Monitor FIFO filling
      wr_ful      => wr_ful,
      usedw       => rd_fifo_usedw,
      rd_emp      => rd_emp,
      -- ST sink
      snk_out     => snk_out,
      snk_in      => snk_in,
      -- ST source
      src_in      => rd_siso,  -- for RL = 0 rd_siso.ready acts as read acknowledge, for RL = 1 rd_siso.ready acts as read request
      src_out     => rd_sosi
    );

    wr_fifo_usedw <= rd_fifo_usedw;
  end generate;

  gen_dp_fifo_dc : if g_use_dual_clock = true generate
    u_dp_fifo_dc : entity work.dp_fifo_dc
    generic map (
      g_technology     => g_technology,
      g_data_w         => g_data_w,
      g_data_signed    => g_data_signed,
      g_bsn_w          => g_bsn_w,
      g_empty_w        => g_empty_w,
      g_channel_w      => g_channel_w,
      g_error_w        => g_error_w,
      g_use_bsn        => g_use_bsn,
      g_use_empty      => g_use_empty,
      g_use_channel    => g_use_channel,
      g_use_error      => g_use_error,
      g_use_sync       => g_use_sync,
      g_use_ctrl       => c_use_ctrl,
      --g_use_complex    => g_use_complex,
      g_fifo_size      => c_fifo_size,
      g_fifo_af_margin => g_fifo_af_margin,
      g_fifo_af_xon    => g_fifo_af_xon,
      g_fifo_rl        => c_fifo_rl
    )
    port map (
      wr_rst      => wr_rst,
      wr_clk      => wr_clk,
      rd_rst      => rd_rst,
      rd_clk      => rd_clk,
      -- Monitor FIFO filling
      wr_ful      => wr_ful,
      wr_usedw    => wr_fifo_usedw,
      rd_usedw    => rd_fifo_usedw,
      rd_emp      => rd_emp,
      -- ST sink
      snk_out     => snk_out,
      snk_in      => snk_in,
      -- ST source
      src_in      => rd_siso,  -- for RL = 0 rd_siso.ready acts as read acknowledge, -- for RL = 1 rd_siso.ready acts as read request
      src_out     => rd_sosi
    );
  end generate;

  no_fill : if g_fifo_fill = 0 generate
    rd_siso <= src_in;  -- SISO
    src_out <= rd_sosi;  -- SOSI
  end generate;  -- no_fill

  gen_fill : if g_fifo_fill > 0 generate
    src_out <= i_src_out;

    p_rd_clk: process(rd_clk, rd_rst)
    begin
      if rd_rst = '1' then
        xon_reg   <= '0';
        state     <= s_idle;
        i_src_out <= c_dp_sosi_rst;
      elsif rising_edge(rd_clk) then
        xon_reg   <= nxt_xon_reg;
        state     <= nxt_state;
        i_src_out <= nxt_src_out;
      end if;
    end process;

    nxt_xon_reg <= src_in.xon;  -- register xon to easy timing closure

    gen_rl_0 : if g_fifo_rl = 0 generate
      p_state : process(state, rd_sosi, src_in, xon_reg, rd_fifo_usedw, rd_fill_ctrl)
      begin
        nxt_state <= state;

        rd_siso <= src_in;  -- default acknowledge (RL=1) this input when output is ready

        -- The output register stage increase RL=0 to 1, so it matches RL = 1 for src_in.ready
        nxt_src_out       <= rd_sosi;
        nxt_src_out.valid <= '0';  -- default no output
        nxt_src_out.sop   <= '0';
        nxt_src_out.eop   <= '0';
        nxt_src_out.sync  <= '0';

        case state is
          when s_idle =>
            if xon_reg = '0' then
              nxt_state <= s_xoff;
            else
              -- read the FIFO until the sop is pending at the output, so discard any valid data between eop and sop
              if rd_sosi.sop = '0' then
                rd_siso <= c_dp_siso_rdy;  -- acknowledge (RL=0) this input independent of output ready
              else
                rd_siso <= c_dp_siso_hold;  -- stop the input, hold the rd_sosi.sop at FIFO output (RL=0)
                nxt_state <= s_fill;
              end if;
            end if;
          when s_fill =>
            if xon_reg = '0' then
              nxt_state <= s_xoff;
            else
              -- stop reading until the FIFO has been filled sufficiently
              if unsigned(rd_fifo_usedw) < unsigned(rd_fill_ctrl) then
                rd_siso <= c_dp_siso_hold;  -- stop the input, hold the pend_src_out.sop
              else
                -- if the output is ready, then start outputting the frame
                if src_in.ready = '1' then
                  nxt_src_out <= rd_sosi;  -- output sop that is still at FIFO output (RL=0)
                  nxt_state <= s_output;
                end if;
              end if;
            end if;
          when s_output =>
            -- if the output is ready continue outputting the frame, ignore xon_reg during this frame
            if src_in.ready = '1' then
              nxt_src_out <= rd_sosi;  -- output valid
              if rd_sosi.eop = '1' then
                nxt_state <= s_idle;  -- output eop, so stop reading the FIFO
              end if;
            end if;
          when others =>  -- s_xoff
            -- Flush the fill FIFO when xon='0'
            rd_siso <= c_dp_siso_flush;
            if xon_reg = '1' then
              nxt_state <= s_idle;
            end if;
        end case;

        -- Pass on frame level flow control
        rd_siso.xon <= src_in.xon;
      end process;
    end generate;  -- gen_rl_0

    gen_rl_1 : if g_fifo_rl = 1 generate
      -- Use dp_hold_input to get equivalent implementation with default RL=1 FIFO.

      -- Hold the sink input for source output
      u_snk : entity work.dp_hold_input
      port map (
        rst          => rd_rst,
        clk          => rd_clk,
        -- ST sink
        snk_out      => rd_siso,  -- SISO ready
        snk_in       => rd_sosi,  -- SOSI
        -- ST source
        src_in       => hold_src_in,  -- SISO ready
        next_src_out => OPEN,  -- SOSI
        pend_src_out => pend_src_out,
        src_out_reg  => i_src_out
      );

      p_state : process(state, src_in, xon_reg, pend_src_out, rd_fifo_usedw, rd_fill_ctrl)
      begin
        nxt_state <= state;

        hold_src_in <= src_in;  -- default request (RL=1) new input when output is ready

        -- The output register stage matches RL = 1 for src_in.ready
        nxt_src_out       <= pend_src_out;
        nxt_src_out.valid <= '0';  -- default no output
        nxt_src_out.sop   <= '0';
        nxt_src_out.eop   <= '0';
        nxt_src_out.sync  <= '0';

        case state is
          when s_idle =>
            if xon_reg = '0' then
              nxt_state <= s_xoff;
            else
              -- read the FIFO until the sop is pending at the output, so discard any valid data between eop and sop
              if pend_src_out.sop = '0' then
                hold_src_in <= c_dp_siso_rdy;  -- request (RL=1) new input independent of output ready
              else
                hold_src_in <= c_dp_siso_hold;  -- stop the input, hold the pend_src_out.sop in dp_hold_input
                nxt_state <= s_fill;
              end if;
            end if;
          when s_fill =>
            if xon_reg = '0' then
              nxt_state <= s_xoff;
            else
              -- stop reading until the FIFO has been filled sufficiently
              if unsigned(rd_fifo_usedw) < unsigned(rd_fill_ctrl) then
                hold_src_in <= c_dp_siso_hold;  -- stop the input, hold the pend_src_out.sop
              else
                -- if the output is ready, then start outputting the input frame
                if src_in.ready = '1' then
                  nxt_src_out <= pend_src_out;  -- output sop that is still pending in dp_hold_input
                  nxt_state <= s_output;
                end if;
              end if;
            end if;
          when s_output =>
            -- if the output is ready continue outputting the input frame, ignore xon_reg during this frame
            if src_in.ready = '1' then
              nxt_src_out <= pend_src_out;  -- output valid
              if pend_src_out.eop = '1' then
                nxt_state <= s_idle;  -- output eop, so stop reading the FIFO
              end if;
            end if;
          when others =>  -- s_xon
            -- Flush the fill FIFO when xon='0'
            hold_src_in <= c_dp_siso_flush;
            if xon_reg = '1' then
              nxt_state <= s_idle;
            end if;
        end case;

        -- Pass on frame level flow control
        hold_src_in.xon <= src_in.xon;
      end process;
    end generate;  -- gen_rl_1
  end generate;  -- gen_fill
end rtl;
