
#set_location_assignment PIN_K15 -to CLK
#set_location_assignment PIN_J15 -to "CLK(n)"
#set_location_assignment PIN_V9 -to CLK
#set_location_assignment PIN_V10 -to "CLK(n)"
set_location_assignment PIN_N12 -to ETH_CLK
set_location_assignment PIN_K14 -to PPS
set_location_assignment PIN_J14 -to "PPS(n)"

# enable 100 ohm termination:
#set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to CLK
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to PPS

#set_location_assignment PIN_AT33 -to CFG_DATA[0]
#set_location_assignment PIN_AT32 -to CFG_DATA[1]
#set_location_assignment PIN_BB33 -to CFG_DATA[2]
#set_location_assignment PIN_BA33 -to CFG_DATA[3]




# IO Standard Assignments from Gijs (excluding memory)
set_instance_assignment -name IO_STANDARD "1.8 V" -to ETH_CLK
set_instance_assignment -name GLOBAL_SIGNAL "GLOBAL CLOCK" -to ETH_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[1](n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[6]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[7]
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTA
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTB
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SC
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SD
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to WDI

# locations changed 30 sept
set_location_assignment PIN_P16 -to ID[0]
set_location_assignment PIN_P15 -to ID[1]
set_location_assignment PIN_K13 -to ID[2]
set_location_assignment PIN_L13 -to ID[3]
set_location_assignment PIN_N16 -to ID[4]
set_location_assignment PIN_N14 -to ID[5]
set_location_assignment PIN_U13 -to ID[6]

set_location_assignment PIN_T13 -to ID[7]
set_location_assignment PIN_AU31 -to INTA
set_location_assignment PIN_AR30 -to INTB

set_location_assignment PIN_BC31 -to SENS_SC
set_location_assignment PIN_BB31 -to SENS_SD

set_location_assignment PIN_BA25 -to PMBUS_SC
set_location_assignment PIN_BD25 -to PMBUS_SD
set_location_assignment PIN_BD26 -to PMBUS_ALERT
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SC
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SD
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_ALERT


set_location_assignment PIN_AN32 -to TESTIO[0]
set_location_assignment PIN_AP32 -to TESTIO[1]
set_location_assignment PIN_AT30 -to TESTIO[2]
set_location_assignment PIN_BD31 -to TESTIO[3]
set_location_assignment PIN_AU30 -to TESTIO[4]
set_location_assignment PIN_BD30 -to TESTIO[5]

set_location_assignment PIN_AB12 -to VERSION[0]
set_location_assignment PIN_AB13 -to VERSION[1]
set_location_assignment PIN_BB30 -to WDI

set_location_assignment PIN_K12 -to ETH_SGIN[0]
set_location_assignment PIN_J12 -to "ETH_SGIN[0](n)"
set_location_assignment PIN_AF33 -to ETH_SGIN[1]
set_location_assignment PIN_AE33 -to "ETH_SGIN[1](n)"
set_location_assignment PIN_H13 -to ETH_SGOUT[0]
set_location_assignment PIN_H12 -to "ETH_SGOUT[0](n)"
set_location_assignment PIN_AW31 -to ETH_SGOUT[1]
set_location_assignment PIN_AV31 -to "ETH_SGOUT[1](n)"

set_instance_assignment -name IO_STANDARD LVDS -to PPS
set_instance_assignment -name IO_STANDARD LVDS -to "PPS(n)"
#set_instance_assignment -name IO_STANDARD LVDS -to CLK
#set_instance_assignment -name IO_STANDARD LVDS -to "CLK(n)"



# Enable internal termination for LVDS inputs
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to PPS
#set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[1]

# Enable 100 ohm termination resistors
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to "ETH_SGIN[0](n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to ETH_SGIN[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to ETH_SGIN[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to "ETH_SGIN[1](n)"

set_location_assignment PIN_AG31 -to altera_reserved_tms
set_location_assignment PIN_AJ31 -to altera_reserved_tck
set_location_assignment PIN_AK18 -to altera_reserved_tdi
set_location_assignment PIN_AH31 -to altera_reserved_ntrst
set_location_assignment PIN_AM29 -to altera_reserved_tdo
#set_location_assignment PIN_AV33 -to ~ALTERA_DATA0~


set_location_assignment PIN_BA33 -to QSFP_LED[0]
set_location_assignment PIN_BA30 -to QSFP_LED[1]
set_location_assignment PIN_BB33 -to QSFP_LED[2]
set_location_assignment PIN_AU33 -to QSFP_LED[3]
set_location_assignment PIN_AV32 -to QSFP_LED[4]
set_location_assignment PIN_AW30 -to QSFP_LED[5]
set_location_assignment PIN_AP31 -to QSFP_LED[6]
set_location_assignment PIN_AP30 -to QSFP_LED[7]
set_location_assignment PIN_AT33 -to QSFP_LED[8]
set_location_assignment PIN_AG32 -to QSFP_LED[9]
set_location_assignment PIN_AF32 -to QSFP_LED[10]
set_location_assignment PIN_AE32 -to QSFP_LED[11]



#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_0_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_1_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_2_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_3_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_4_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to QSFP_5_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[12]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[13]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[14]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[15]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[16]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[17]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[18]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[19]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[20]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[21]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[22]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[23]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[24]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[25]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[26]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[27]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[28]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[29]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[30]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[31]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[32]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[33]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[34]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[35]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[36]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[37]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[38]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[39]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[40]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[41]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[42]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[43]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[44]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[45]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[46]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_RX[47]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[12]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[13]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[14]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[15]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[16]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[17]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[18]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[19]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[20]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[21]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[22]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[23]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[24]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[25]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[26]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[27]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[28]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[29]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[30]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[31]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[32]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[33]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[34]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[35]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[36]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[37]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[38]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[39]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[40]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[41]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[42]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[43]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[44]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[45]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[46]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to BCK_TX[47]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_0_TX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL OFF -to RING_1_TX[11]


set_instance_assignment -name IO_STANDARD LVDS -to jesd204_device_clk
set_instance_assignment -name IO_STANDARD LVDS -to "jesd204_device_clk(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to jesd204_device_clk
set_location_assignment PIN_V9 -to jesd204_device_clk
set_location_assignment PIN_V10 -to "jesd204_device_clk(n)"

set_location_assignment PIN_BA7 -to jesd204_rx_serial_data

set_location_assignment PIN_V12 -to jesd204_rx_sysref
set_instance_assignment -name IO_STANDARD "1.8 V" -to jesd204_rx_sysref

set_location_assignment PIN_U12 -to jesd204_sync_n_out
set_instance_assignment -name IO_STANDARD "1.8 V" -to jesd204_sync_n_out

