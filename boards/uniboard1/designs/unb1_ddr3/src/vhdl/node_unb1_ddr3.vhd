-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, diag_lib, technology_lib, tech_ddr_lib, io_ddr_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity node_unb1_ddr3 is
  generic (
    g_sim        : boolean := false;
    g_technology : natural := c_tech_select_default;
    g_tech_ddr   : t_c_tech_ddr;
    g_st_dat_w   : natural := 64  -- Any power of two 8..256
  );
  port (
    -- System
    mm_rst                   : in    std_logic;
    mm_clk                   : in    std_logic;

    dp_rst                   : in    std_logic;
    dp_clk                   : in    std_logic;

    ddr_ref_clk              : in    std_logic;
    ddr_ref_rst              : in    std_logic;

    -- Clock outputs
    ddr_out_clk              : out   std_logic;
    ddr_out_rst              : out   std_logic;

    -- MM interface
    reg_io_ddr_mosi          : in    t_mem_mosi := c_mem_mosi_rst;
    reg_io_ddr_miso          : out   t_mem_miso;

    -- Data Buffer Control
    reg_diag_data_buf_mosi   : in    t_mem_mosi;
    reg_diag_data_buf_miso   : out   t_mem_miso;

    -- Data Buffer Data
    ram_diag_data_buf_mosi   : in    t_mem_mosi;
    ram_diag_data_buf_miso   : out   t_mem_miso;

    -- Block Generator Control
    reg_diag_bg_ctrl_mosi    : in    t_mem_mosi;
    reg_diag_bg_ctrl_miso    : out   t_mem_miso;

    -- Block Generator Data
    ram_diag_bg_data_mosi    : in    t_mem_mosi;
    ram_diag_bg_data_miso    : out   t_mem_miso;

    -- TX Sequencer
    reg_diag_tx_seq_mosi     : in    t_mem_mosi;
    reg_diag_tx_seq_miso     : out   t_mem_miso;

    -- RX Sequencer
    reg_diag_rx_seq_mosi     : in    t_mem_mosi;
    reg_diag_rx_seq_miso     : out   t_mem_miso;

    -- SO-DIMM Memory Bank I = ddr3_I
    MB_I_IN                  : in    t_tech_ddr3_phy_in;
    MB_I_IO                  : inout t_tech_ddr3_phy_io;
    MB_I_OU                  : out   t_tech_ddr3_phy_ou
  );
end node_unb1_ddr3;

architecture str of node_unb1_ddr3 is
  constant c_wr_data_w              : natural  := g_st_dat_w;
  constant c_rd_data_w              : natural  := g_st_dat_w;
  constant c_data_w                 : natural  := g_st_dat_w;

  constant c_wr_fifo_depth          : natural  := 1024;  -- >=16                             , defined at DDR side of the FIFO.
  constant c_rd_fifo_depth          : natural  := 1024;  -- >=16 AND >g_tech_ddr.maxburstsize, defined at DDR side of the FIFO.

  constant c_use_bg                 : boolean  := false;
  constant c_use_tx_seq             : boolean  := true;
  constant c_use_db                 : boolean  := false;
  constant c_use_rx_seq             : boolean  := true;
  constant c_buf_nof_data           : natural  := 1024;
  constant c_nof_streams            : natural  := 1;
  constant c_seq_dat_w              : natural  := 16;

  signal en_sync                    : std_logic;

  signal out_siso_arr               : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);  -- Default xon='1'
  signal out_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);  -- Output SOSI that contains the waveform data
  signal in_siso_arr                : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);  -- Default xon='1'
  signal in_sosi_arr                : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
begin
  u_mms_io_ddr_diag : entity io_ddr_lib.mms_io_ddr_diag
  generic map(
    -- System
    g_technology       => g_technology,
    g_dp_data_w        => g_st_dat_w,
    g_dp_seq_dat_w     => c_seq_dat_w,
    g_dp_wr_fifo_depth => c_wr_fifo_depth,
    g_dp_rd_fifo_depth => c_rd_fifo_depth,
    -- IO_DDR
    g_io_tech_ddr      => g_tech_ddr,
    -- DIAG data buffer
    g_db_use_db        => c_use_db,
    g_db_buf_nof_data  => c_buf_nof_data
  )
  port map(
    ---------------------------------------------------------------------------
    -- System
    ---------------------------------------------------------------------------
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,
    dp_rst              => dp_rst,
    dp_clk              => dp_clk,

    ---------------------------------------------------------------------------
    -- IO_DDR
    ---------------------------------------------------------------------------
    -- DDR reference clock
    ctlr_ref_clk        => ddr_ref_clk,
    ctlr_ref_rst        => ddr_ref_rst,

    -- DDR controller clock domain
    ctlr_clk_out        => ddr_out_clk,
    ctlr_rst_out        => ddr_out_rst,

    ctlr_clk_in         => dp_clk,
    ctlr_rst_in         => dp_rst,

    -- MM interface
    reg_io_ddr_mosi     => reg_io_ddr_mosi,
    reg_io_ddr_miso     => reg_io_ddr_miso,

    -- Write / read FIFO status for monitoring purposes (in dp_clk domain)
    wr_fifo_usedw       => OPEN,
    rd_fifo_usedw       => OPEN,

    -- DDR3 pass on termination control from master to slave controller
    term_ctrl_out       => OPEN,
    term_ctrl_in        => OPEN,

    -- DDR3 PHY external interface
    phy3_in             => MB_I_in,
    phy3_io             => MB_I_io,
    phy3_ou             => MB_I_ou,

    ---------------------------------------------------------------------------
    -- DIAG Tx seq
    ---------------------------------------------------------------------------
    -- MM interface
    reg_tx_seq_mosi     => reg_diag_tx_seq_mosi,
    reg_tx_seq_miso     => reg_diag_tx_seq_miso,

    ---------------------------------------------------------------------------
    -- DIAG rx seq with optional data buffer
    ---------------------------------------------------------------------------
    -- MM interface
    reg_data_buf_mosi   => reg_diag_data_buf_mosi,
    reg_data_buf_miso   => reg_diag_data_buf_miso,

    ram_data_buf_mosi   => ram_diag_data_buf_mosi,
    ram_data_buf_miso   => ram_diag_data_buf_miso,

    reg_rx_seq_mosi     => reg_diag_rx_seq_mosi,
    reg_rx_seq_miso     => reg_diag_rx_seq_miso
  );
end str;
