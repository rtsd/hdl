--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

--synthesis_resources = mux21 56 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_merlin_router_0003 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 reset	:	IN  STD_LOGIC;
		 sink_data	:	IN  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 sink_endofpacket	:	IN  STD_LOGIC;
		 sink_ready	:	OUT  STD_LOGIC;
		 sink_startofpacket	:	IN  STD_LOGIC;
		 sink_valid	:	IN  STD_LOGIC;
		 src_channel	:	OUT  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 src_data	:	OUT  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 src_endofpacket	:	OUT  STD_LOGIC;
		 src_ready	:	IN  STD_LOGIC;
		 src_startofpacket	:	OUT  STD_LOGIC;
		 src_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_merlin_router_0003;

 ARCHITECTURE RTL OF altera_merlin_router_0003 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_15m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_22m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_27m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_33m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_34m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_39m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_44m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_45m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_46m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_51m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_53m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_56m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_57m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_58m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_63m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_65m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_67m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_68m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_69m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_70m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_75m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_77m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_78m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_79m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_80m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_81m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_82m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_87m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_88m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_89m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_90m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_91m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_92m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_93m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_channel_94m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_23m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_24m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_25m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_35m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_36m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_37m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_47m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_48m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_49m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_59m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_60m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_61m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_71m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_72m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_73m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_83m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_84m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_85m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_95m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_96m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_router_0003_src_data_97m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_w1w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_0_297_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_1_330_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_2_363_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_3_396_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_4_429_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_5_462_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_6_495_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_router_0003_src_channel_7_528_dataout :	STD_LOGIC;
 BEGIN

	wire_w1w(0) <= NOT s_wire_altera_merlin_router_0003_src_channel_0_297_dataout;
	s_wire_altera_merlin_router_0003_src_channel_0_297_dataout <= (((((((((((NOT sink_data(39)) AND (NOT sink_data(40))) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND (NOT sink_data(44))) AND (NOT sink_data(45))) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_1_330_dataout <= (((((((((((NOT sink_data(39)) AND (NOT sink_data(40))) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND sink_data(44)) AND (NOT sink_data(45))) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_2_363_dataout <= (((((((((((NOT sink_data(39)) AND (NOT sink_data(40))) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND (NOT sink_data(44))) AND sink_data(45)) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_3_396_dataout <= (((((((((((NOT sink_data(39)) AND (NOT sink_data(40))) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND sink_data(44)) AND sink_data(45)) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_4_429_dataout <= ((((((((((NOT sink_data(40)) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND sink_data(44)) AND (NOT sink_data(45))) AND sink_data(46)) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_5_462_dataout <= ((((((((((NOT sink_data(40)) AND (NOT sink_data(41))) AND (NOT sink_data(42))) AND (NOT sink_data(43))) AND (NOT sink_data(44))) AND (NOT sink_data(45))) AND (NOT sink_data(46))) AND sink_data(47)) AND (NOT sink_data(48))) AND (NOT sink_data(49)));
	s_wire_altera_merlin_router_0003_src_channel_6_495_dataout <= (((((((NOT sink_data(43)) AND (NOT sink_data(44))) AND (NOT sink_data(45))) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND (NOT sink_data(48))) AND sink_data(49));
	s_wire_altera_merlin_router_0003_src_channel_7_528_dataout <= ((((((NOT sink_data(44)) AND (NOT sink_data(45))) AND (NOT sink_data(46))) AND (NOT sink_data(47))) AND sink_data(48)) AND sink_data(49));
	sink_ready <= src_ready;
	src_channel <= ( wire_altera_merlin_router_0003_src_channel_87m_dataout & wire_altera_merlin_router_0003_src_channel_88m_dataout & wire_altera_merlin_router_0003_src_channel_89m_dataout & wire_altera_merlin_router_0003_src_channel_90m_dataout & wire_altera_merlin_router_0003_src_channel_91m_dataout & wire_altera_merlin_router_0003_src_channel_92m_dataout & wire_altera_merlin_router_0003_src_channel_93m_dataout & wire_altera_merlin_router_0003_src_channel_94m_dataout);
	src_data <= ( sink_data(68) & wire_altera_merlin_router_0003_src_data_95m_dataout & wire_altera_merlin_router_0003_src_data_96m_dataout & wire_altera_merlin_router_0003_src_data_97m_dataout & sink_data(64 DOWNTO 0));
	src_endofpacket <= sink_endofpacket;
	src_startofpacket <= sink_startofpacket;
	src_valid <= sink_valid;
	wire_altera_merlin_router_0003_src_channel_15m_dataout <= wire_w1w(0) AND NOT(s_wire_altera_merlin_router_0003_src_channel_1_330_dataout);
	wire_altera_merlin_router_0003_src_channel_22m_dataout <= s_wire_altera_merlin_router_0003_src_channel_0_297_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_1_330_dataout);
	wire_altera_merlin_router_0003_src_channel_27m_dataout <= wire_altera_merlin_router_0003_src_channel_15m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_2_363_dataout);
	wire_altera_merlin_router_0003_src_channel_33m_dataout <= s_wire_altera_merlin_router_0003_src_channel_1_330_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_2_363_dataout);
	wire_altera_merlin_router_0003_src_channel_34m_dataout <= wire_altera_merlin_router_0003_src_channel_22m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_2_363_dataout);
	wire_altera_merlin_router_0003_src_channel_39m_dataout <= wire_altera_merlin_router_0003_src_channel_27m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_3_396_dataout);
	wire_altera_merlin_router_0003_src_channel_44m_dataout <= s_wire_altera_merlin_router_0003_src_channel_2_363_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_3_396_dataout);
	wire_altera_merlin_router_0003_src_channel_45m_dataout <= wire_altera_merlin_router_0003_src_channel_33m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_3_396_dataout);
	wire_altera_merlin_router_0003_src_channel_46m_dataout <= wire_altera_merlin_router_0003_src_channel_34m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_3_396_dataout);
	wire_altera_merlin_router_0003_src_channel_51m_dataout <= wire_altera_merlin_router_0003_src_channel_39m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_channel_53m_dataout <= s_wire_altera_merlin_router_0003_src_channel_3_396_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_channel_56m_dataout <= wire_altera_merlin_router_0003_src_channel_44m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_channel_57m_dataout <= wire_altera_merlin_router_0003_src_channel_45m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_channel_58m_dataout <= wire_altera_merlin_router_0003_src_channel_46m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_channel_63m_dataout <= wire_altera_merlin_router_0003_src_channel_51m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_65m_dataout <= wire_altera_merlin_router_0003_src_channel_53m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_67m_dataout <= s_wire_altera_merlin_router_0003_src_channel_4_429_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_68m_dataout <= wire_altera_merlin_router_0003_src_channel_56m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_69m_dataout <= wire_altera_merlin_router_0003_src_channel_57m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_70m_dataout <= wire_altera_merlin_router_0003_src_channel_58m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_channel_75m_dataout <= wire_altera_merlin_router_0003_src_channel_63m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_77m_dataout <= wire_altera_merlin_router_0003_src_channel_65m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_78m_dataout <= s_wire_altera_merlin_router_0003_src_channel_5_462_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_79m_dataout <= wire_altera_merlin_router_0003_src_channel_67m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_80m_dataout <= wire_altera_merlin_router_0003_src_channel_68m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_81m_dataout <= wire_altera_merlin_router_0003_src_channel_69m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_82m_dataout <= wire_altera_merlin_router_0003_src_channel_70m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_channel_87m_dataout <= wire_altera_merlin_router_0003_src_channel_75m_dataout OR s_wire_altera_merlin_router_0003_src_channel_7_528_dataout;
	wire_altera_merlin_router_0003_src_channel_88m_dataout <= s_wire_altera_merlin_router_0003_src_channel_6_495_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_89m_dataout <= wire_altera_merlin_router_0003_src_channel_77m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_90m_dataout <= wire_altera_merlin_router_0003_src_channel_78m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_91m_dataout <= wire_altera_merlin_router_0003_src_channel_79m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_92m_dataout <= wire_altera_merlin_router_0003_src_channel_80m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_93m_dataout <= wire_altera_merlin_router_0003_src_channel_81m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_channel_94m_dataout <= wire_altera_merlin_router_0003_src_channel_82m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_7_528_dataout);
	wire_altera_merlin_router_0003_src_data_23m_dataout <= wire_w1w(0) AND NOT(s_wire_altera_merlin_router_0003_src_channel_1_330_dataout);
	wire_altera_merlin_router_0003_src_data_24m_dataout <= wire_w1w(0) AND NOT(s_wire_altera_merlin_router_0003_src_channel_1_330_dataout);
	wire_altera_merlin_router_0003_src_data_25m_dataout <= wire_w1w(0) OR s_wire_altera_merlin_router_0003_src_channel_1_330_dataout;
	wire_altera_merlin_router_0003_src_data_35m_dataout <= wire_altera_merlin_router_0003_src_data_23m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_2_363_dataout);
	wire_altera_merlin_router_0003_src_data_36m_dataout <= wire_altera_merlin_router_0003_src_data_24m_dataout OR s_wire_altera_merlin_router_0003_src_channel_2_363_dataout;
	wire_altera_merlin_router_0003_src_data_37m_dataout <= wire_altera_merlin_router_0003_src_data_25m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_2_363_dataout);
	wire_altera_merlin_router_0003_src_data_47m_dataout <= wire_altera_merlin_router_0003_src_data_35m_dataout OR s_wire_altera_merlin_router_0003_src_channel_3_396_dataout;
	wire_altera_merlin_router_0003_src_data_48m_dataout <= wire_altera_merlin_router_0003_src_data_36m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_3_396_dataout);
	wire_altera_merlin_router_0003_src_data_49m_dataout <= wire_altera_merlin_router_0003_src_data_37m_dataout OR s_wire_altera_merlin_router_0003_src_channel_3_396_dataout;
	wire_altera_merlin_router_0003_src_data_59m_dataout <= wire_altera_merlin_router_0003_src_data_47m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_4_429_dataout);
	wire_altera_merlin_router_0003_src_data_60m_dataout <= wire_altera_merlin_router_0003_src_data_48m_dataout OR s_wire_altera_merlin_router_0003_src_channel_4_429_dataout;
	wire_altera_merlin_router_0003_src_data_61m_dataout <= wire_altera_merlin_router_0003_src_data_49m_dataout OR s_wire_altera_merlin_router_0003_src_channel_4_429_dataout;
	wire_altera_merlin_router_0003_src_data_71m_dataout <= wire_altera_merlin_router_0003_src_data_59m_dataout OR s_wire_altera_merlin_router_0003_src_channel_5_462_dataout;
	wire_altera_merlin_router_0003_src_data_72m_dataout <= wire_altera_merlin_router_0003_src_data_60m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_data_73m_dataout <= wire_altera_merlin_router_0003_src_data_61m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_5_462_dataout);
	wire_altera_merlin_router_0003_src_data_83m_dataout <= wire_altera_merlin_router_0003_src_data_71m_dataout OR s_wire_altera_merlin_router_0003_src_channel_6_495_dataout;
	wire_altera_merlin_router_0003_src_data_84m_dataout <= wire_altera_merlin_router_0003_src_data_72m_dataout OR s_wire_altera_merlin_router_0003_src_channel_6_495_dataout;
	wire_altera_merlin_router_0003_src_data_85m_dataout <= wire_altera_merlin_router_0003_src_data_73m_dataout AND NOT(s_wire_altera_merlin_router_0003_src_channel_6_495_dataout);
	wire_altera_merlin_router_0003_src_data_95m_dataout <= wire_altera_merlin_router_0003_src_data_83m_dataout OR s_wire_altera_merlin_router_0003_src_channel_7_528_dataout;
	wire_altera_merlin_router_0003_src_data_96m_dataout <= wire_altera_merlin_router_0003_src_data_84m_dataout OR s_wire_altera_merlin_router_0003_src_channel_7_528_dataout;
	wire_altera_merlin_router_0003_src_data_97m_dataout <= wire_altera_merlin_router_0003_src_data_85m_dataout OR s_wire_altera_merlin_router_0003_src_channel_7_528_dataout;

 END RTL; --altera_merlin_router_0003
--synopsys translate_on
--VALID FILE
