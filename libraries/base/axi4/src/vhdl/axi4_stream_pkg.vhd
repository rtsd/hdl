-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle (edits only, see Original)
-- Purpose: General AXI stream record defintion
-- Original:
-- https://git.astron.nl/rtsd/gemini/-/blob/master/libraries/base/axi4/src/vhdl/axi4_stream_pkg.vhd
-- Remarks:
-- * Choose smallest maximum SOSI slv lengths that fit all use cases, because unconstrained record
--   fields slv is not allowed.
-- * The large SOSI data field width of 256b has some disadvantages:
--   . about 10% extra simulation time and PC memory usage compared to 72b
--     (measured using tb_unb_tse_board)
--   . a 256b number has 64 hex digits in the Wave window which is awkward because of the leading
--     zeros when typically
--     only 32b are used, fortunately integer representation still works OK (except 0 which is shown
--     as blank).
--   However the alternatives are not attractive, because they affect the implementation of the
--   streaming
--   components that use the SOSI record. Alternatives are e.g.:
--   . define an extra long SOSI data field ldata[255:0] in addition to the existing data[71:0]
--     field
--   . use the array of SOSI records to contain wider data, all with the same SOSI control field
--     values
--   . define another similar SOSI record with data[255:0].
--   Therefore define data width as 256b, because the disadvantages are acceptable and the benefit
--   is great, because all
--   streaming components can remain as they are.
-- * Added sync and bsn to SOSI to have timestamp information with the data
-- * Added re and im to SOSI to support complex data for DSP
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

package axi4_stream_pkg is
  constant c_axi4_stream_data_w     : natural :=  512;  -- Data width, upto 512bit for Xilinx IP
  constant c_axi4_stream_user_w     : natural :=  70;  -- User data, upto 70bit for Xilinx IP
  constant c_axi4_stream_tid_w      : natural :=  4;  -- Thread ID, upto 4bit for Xilinx IP
  constant c_axi4_stream_dest_w     : natural :=  32;  -- Routing data, upto 32bit for Xilinx IP
  constant c_axi4_stream_keep_w     : natural :=  c_axi4_stream_data_w / 8;  -- 1 bit for each byte in data.
  constant c_axi4_stream_strb_w     : natural :=  c_axi4_stream_data_w / 8;  -- 1 bit for each byte in daya.

  type t_axi4_siso is record  -- Source In and Sink Out
    tready    : std_logic;  -- Ready to accept data from destination
  end record;

  type t_axi4_sosi is record  -- Source Out and Sink In
    tvalid    : std_logic;  -- Data valid
    tdata     : std_logic_vector(c_axi4_stream_data_w - 1 downto 0);  -- Data bus
    tstrb     : std_logic_vector(c_axi4_stream_strb_w - 1 downto 0);  -- Byte valids, indicates if data is position (0) or data (1). Generally not used
    tkeep     : std_logic_vector(c_axi4_stream_keep_w - 1 downto 0);  -- Indicate valid data bytes (1) or null bytes (0).
    tlast     : std_logic;  -- Last transaction in a burst
    tid       : std_logic_vector(c_axi4_stream_tid_w - 1 downto 0);  -- Transaction ID
    tdest     : std_logic_vector(c_axi4_stream_dest_w - 1 downto 0);  -- Destination rounting information
    tuser     : std_logic_vector(c_axi4_stream_user_w - 1 downto 0);  -- Tranaction user fields
  end record;

  -- Initialise signal declarations with c_axi4_stream_rst/rdy to ease the interpretation of slv fields with unused bits
  constant c_axi4_siso_rst   : t_axi4_siso := (tready => '0');
  constant c_axi4_siso_x     : t_axi4_siso := (tready => 'X');
  constant c_axi4_siso_hold  : t_axi4_siso := (tready => '0');
  constant c_axi4_siso_rdy   : t_axi4_siso := (tready => '1');
  constant c_axi4_siso_flush : t_axi4_siso := (tready => '1');
  constant c_axi4_sosi_rst   : t_axi4_sosi := (tvalid => '0', tdata => (others => '0'), tstrb => (others => '0'), tkeep => (others => '0'), tlast => '0', tid => (others => '0'), tdest => (others => '0'), tuser => (others => '0'));
  constant c_axi4_sosi_x     : t_axi4_sosi := ('X', (others => 'X'), (others => 'X'), (others => 'X'), 'X', (others => 'X'), (others => 'X'), (others => 'X'));

  -- Multi port or multi register array for DP stream records
  type t_axi4_siso_arr is array (integer range <>) of t_axi4_siso;
  type t_axi4_sosi_arr is array (integer range <>) of t_axi4_sosi;

  -- Multi-dimemsion array types with fixed LS-dimension
  -- . 2 dimensional array with 2 fixed LS sosi/siso interfaces (axi4_split, axi4_concat)
  type t_axi4_siso_2arr_2 is array (integer range <>) of t_axi4_siso_arr(1 downto 0);
  type t_axi4_sosi_2arr_2 is array (integer range <>) of t_axi4_sosi_arr(1 downto 0);

  -- 2-dimensional streaming array type:
  -- Note:
  --   This t_*_mat is less useful then a t_*_2arr array of arrays, because assignments can only be done per element (i.e. not per row). However for t_*_2arr
  --   the arrays dimension must be fixed, so these t_*_2arr types are application dependent and need to be defined where used.
  type t_axi4_siso_mat is array (integer range <>, integer range <>) of t_axi4_siso;
  type t_axi4_sosi_mat is array (integer range <>, integer range <>) of t_axi4_sosi;

  -- Check sosi.valid against siso.ready
  procedure proc_axi4_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi            : in    t_axi4_sosi;
                               signal   siso            : in    t_axi4_siso;
                               signal   ready_reg       : inout std_logic_vector);

  -- Default RL=1
  procedure proc_axi4_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi            : in    t_axi4_sosi;
                               signal   siso            : in    t_axi4_siso;
                               signal   ready_reg       : inout std_logic_vector);

  -- SOSI/SISO array version
  procedure proc_axi4_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_axi4_sosi_arr;
                               signal   siso_arr        : in    t_axi4_siso_arr;
                               signal   ready_reg       : inout std_logic_vector);

  -- SOSI/SISO array version with RL=1
  procedure proc_axi4_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_axi4_sosi_arr;
                               signal   siso_arr        : in    t_axi4_siso_arr;
                               signal   ready_reg       : inout std_logic_vector);

  -- Keep part of head data and combine part of tail data, use the other sosi from head_sosi
  function func_axi4_data_shift_first(head_sosi, tail_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail              : natural) return t_axi4_sosi;
  -- Shift and combine part of previous data and this data, use the other sosi from prev_sosi
  function func_axi4_data_shift(      prev_sosi, this_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_this              : natural) return t_axi4_sosi;
  -- Shift part of tail data and account for input empty
  function func_axi4_data_shift_last(            tail_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail, input_empty : natural) return t_axi4_sosi;

  -- Determine resulting empty if two streams are concatenated or split
  function func_axi4_empty_concat(head_empty, tail_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector;
  function func_axi4_empty_split(input_empty, head_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector;

  -- Multiplex the t_axi4_sosi_arr based on the valid, assuming that at most one input is active valid.
  function func_axi4_sosi_arr_mux(axi4 : t_axi4_sosi_arr) return t_axi4_sosi;

  -- Determine the combined logical value of corresponding STD_LOGIC fields in t_axi4_*_arr (for all elements or only for the mask[]='1' elements)
  function func_axi4_stream_arr_and(axi4 : t_axi4_siso_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_axi4_stream_arr_and(axi4 : t_axi4_sosi_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_axi4_stream_arr_and(axi4 : t_axi4_siso_arr;                          str : string) return std_logic;
  function func_axi4_stream_arr_and(axi4 : t_axi4_sosi_arr;                          str : string) return std_logic;
  function func_axi4_stream_arr_or( axi4 : t_axi4_siso_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_axi4_stream_arr_or( axi4 : t_axi4_sosi_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_axi4_stream_arr_or( axi4 : t_axi4_siso_arr;                          str : string) return std_logic;
  function func_axi4_stream_arr_or( axi4 : t_axi4_sosi_arr;                          str : string) return std_logic;

  -- Functions to set or get a STD_LOGIC field as a STD_LOGIC_VECTOR to or from an siso or an sosi array
  function func_axi4_stream_arr_set(axi4 : t_axi4_siso_arr; slv : std_logic_vector; str : string) return t_axi4_siso_arr;
  function func_axi4_stream_arr_set(axi4 : t_axi4_sosi_arr; slv : std_logic_vector; str : string) return t_axi4_sosi_arr;
  function func_axi4_stream_arr_set(axi4 : t_axi4_siso_arr; sl  : std_logic;        str : string) return t_axi4_siso_arr;
  function func_axi4_stream_arr_set(axi4 : t_axi4_sosi_arr; sl  : std_logic;        str : string) return t_axi4_sosi_arr;
  function func_axi4_stream_arr_get(axi4 : t_axi4_siso_arr;                         str : string) return std_logic_vector;
  function func_axi4_stream_arr_get(axi4 : t_axi4_sosi_arr;                         str : string) return std_logic_vector;

  -- Functions to select elements from two siso or two sosi arrays (sel[] = '1' selects a, sel[] = '0' selects b)
  function func_axi4_stream_arr_select(sel : std_logic_vector; a,                 b : t_axi4_siso)     return t_axi4_siso_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a,                 b : t_axi4_sosi)     return t_axi4_sosi_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_siso_arr; b : t_axi4_siso)     return t_axi4_siso_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_sosi_arr; b : t_axi4_sosi)     return t_axi4_sosi_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_siso;     b : t_axi4_siso_arr) return t_axi4_siso_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_sosi;     b : t_axi4_sosi_arr) return t_axi4_sosi_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a,                 b : t_axi4_siso_arr) return t_axi4_siso_arr;
  function func_axi4_stream_arr_select(sel : std_logic_vector; a,                 b : t_axi4_sosi_arr) return t_axi4_sosi_arr;

  -- Fix reversed buses due to connecting TO to DOWNTO range arrays.
  function func_axi4_stream_arr_reverse_range(in_arr : t_axi4_sosi_arr) return t_axi4_sosi_arr;
  function func_axi4_stream_arr_reverse_range(in_arr : t_axi4_siso_arr) return t_axi4_siso_arr;

  -- Functions to combinatorially hold the data fields and to set or reset the control fields in an sosi array
  function func_axi4_stream_arr_set_control(  axi4 : t_axi4_sosi_arr; ctrl : t_axi4_sosi) return t_axi4_sosi_arr;
  function func_axi4_stream_arr_reset_control(axi4 : t_axi4_sosi_arr                  ) return t_axi4_sosi_arr;

  -- Functions to convert dp streaming to axi4 streaming
  function func_axi4_stream_from_dp_sosi(dp_sosi : t_dp_sosi) return t_axi4_sosi;
  function func_axi4_stream_from_dp_siso(dp_siso : t_dp_siso) return t_axi4_siso;

  -- Functions to convert dp streaming to axi4 streaming
  function func_axi4_stream_to_dp_sosi(axi4_sosi : t_axi4_sosi) return t_dp_sosi;
  function func_axi4_stream_to_dp_siso(axi4_siso : t_axi4_siso) return t_dp_siso;

  -- Function to derive DP empty from AXI4 tkeep by counting the 0s in TKEEP.
  function func_axi4_stream_tkeep_to_dp_empty(tkeep : std_logic_vector) return std_logic_vector;
end axi4_stream_pkg;

package body axi4_stream_pkg is
  -- Check sosi.valid against siso.ready
  procedure proc_axi4_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi            : in    t_axi4_sosi;
                               signal   siso            : in    t_axi4_siso;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    ready_reg(0) <= siso.tready;
    -- Register siso.ready in c_ready_latency registers
    if rising_edge(clk) then
      -- Check DP sink
      if sosi.tvalid = '1' and ready_reg(c_ready_latency) = '0' then
        report "RL ERROR"
          severity FAILURE;
      end if;

      ready_reg( 1 to c_ready_latency) <= ready_reg( 0 to c_ready_latency - 1);
    end if;
  end proc_axi4_siso_alert;

  -- Default RL=1
  procedure proc_axi4_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi            : in    t_axi4_sosi;
                               signal   siso            : in    t_axi4_siso;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    proc_axi4_siso_alert(1, clk, sosi, siso, ready_reg);
  end proc_axi4_siso_alert;

  -- SOSI/SISO array version
  procedure proc_axi4_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_axi4_sosi_arr;
                               signal   siso_arr        : in    t_axi4_siso_arr;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    for i in 0 to sosi_arr'length - 1 loop
      ready_reg(i * (c_ready_latency + 1)) <= siso_arr(i).tready;  -- SLV is used as an array: nof_streams*(0..c_ready_latency)
    end loop;
    -- Register siso.ready in c_ready_latency registers
    if rising_edge(clk) then
      for i in 0 to sosi_arr'length - 1 loop
        -- Check DP sink
        if sosi_arr(i).tvalid = '1' and ready_reg(i * (c_ready_latency + 1) + 1) = '0' then
          report "RL ERROR"
            severity FAILURE;
        end if;
        ready_reg(i * (c_ready_latency + 1) + 1 to i * (c_ready_latency + 1) + c_ready_latency) <=  ready_reg(i * (c_ready_latency + 1) to i * (c_ready_latency + 1) + c_ready_latency - 1);
      end loop;
    end if;
  end proc_axi4_siso_alert;

  -- SOSI/SISO array version with RL=1
  procedure proc_axi4_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_axi4_sosi_arr;
                               signal   siso_arr        : in    t_axi4_siso_arr;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    proc_axi4_siso_alert(1, clk, sosi_arr, siso_arr, ready_reg);
  end proc_axi4_siso_alert;

  -- Keep part of head data and combine part of tail data
  function func_axi4_data_shift_first(head_sosi, tail_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail : natural) return t_axi4_sosi is
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_axi4_sosi;
  begin
    assert nof_symbols_from_tail < vN
      report "func_axi4_data_shift_first : no symbols from head"
      severity FAILURE;
    -- use the other sosi from head_sosi
    v_sosi := head_sosi;  -- I = nof_symbols_from_tail = 0
    for I in 1 to vN - 1 loop  -- I > 0
      if nof_symbols_from_tail = I then
        v_sosi.tdata(I * symbol_w - 1 downto 0) := tail_sosi.tdata(vN * symbol_w - 1 downto (vN - I) * symbol_w);
      end if;
    end loop;
    return v_sosi;
  end func_axi4_data_shift_first;

  -- Shift and combine part of previous data and this data,
  function func_axi4_data_shift(prev_sosi, this_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_this : natural) return t_axi4_sosi is
    variable vK     : natural := nof_symbols_from_this;
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_axi4_sosi;
  begin
    -- use the other sosi from this_sosi if nof_symbols_from_this > 0 else use other sosi from prev_sosi
    if vK > 0 then
      v_sosi := this_sosi;
    else
      v_sosi := prev_sosi;
    end if;

    -- use sosi data from both if 0 < nof_symbols_from_this < nof_symbols_per_data (i.e. 0 < I < vN)
    if vK < nof_symbols_per_data then  -- I = vK = nof_symbols_from_this < vN
      -- Implementation using variable vK directly instead of via I in a LOOP
      -- IF vK > 0 THEN
      --   v_sosi.data(vN*symbol_w-1 DOWNTO vK*symbol_w)            := prev_sosi.data((vN-vK)*symbol_w-1 DOWNTO                0);
      --   v_sosi.data(                     vK*symbol_w-1 DOWNTO 0) := this_sosi.data( vN    *symbol_w-1 DOWNTO (vN-vK)*symbol_w);
      -- END IF;
      -- Implementaion using LOOP vK rather than VARIABLE vK directly as index to help synthesis and avoid potential multiplier
      v_sosi.tdata := prev_sosi.tdata;  -- I = vK = nof_symbols_from_this = 0
      for I in 1 to vN - 1 loop  -- I = vK = nof_symbols_from_this > 0
        if vK = I then
          v_sosi.tdata(vN * symbol_w - 1 downto I * symbol_w)            := prev_sosi.tdata((vN - I) * symbol_w - 1 downto               0);
          v_sosi.tdata(                     I * symbol_w - 1 downto 0) := this_sosi.tdata( vN   * symbol_w - 1 downto (vN - I) * symbol_w);
        end if;
      end loop;
    end if;
    return v_sosi;
  end func_axi4_data_shift;

  -- Shift part of tail data and account for input empty
  function func_axi4_data_shift_last(tail_sosi : t_axi4_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail, input_empty : natural) return t_axi4_sosi is
    variable vK     : natural := nof_symbols_from_tail;
    variable vL     : natural := input_empty;
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_axi4_sosi;
  begin
    assert vK > 0
      report "func_axi4_data_shift_last : no symbols from tail"
      severity FAILURE;
    assert vK + vL <= vN
      report "func_axi4_data_shift_last : impossible shift"
      severity FAILURE;
    v_sosi := tail_sosi;
    -- Implementation using variable vK directly instead of via I in a LOOP
    -- IF vK > 0 THEN
    --   v_sosi.data(vN*symbol_w-1 DOWNTO (vN-vK)*symbol_w) <= tail_sosi.data((vK+vL)*symbol_w-1 DOWNTO vL*symbol_w);
    -- END IF;
    -- Implementation using LOOP vK rather than VARIABLE vK directly as index to help synthesis and avoid potential multiplier
    -- Implementation using LOOP vL rather than VARIABLE vL directly as index to help synthesis and avoid potential multiplier
    for I in 1 to vN - 1 loop
      if vK = I then
        for J in 0 to vN - 1 loop
          if vL = J then
            v_sosi.tdata(vN * symbol_w - 1 downto (vN - I) * symbol_w) := tail_sosi.tdata((I + J) * symbol_w - 1 downto J * symbol_w);
          end if;
        end loop;
      end if;
    end loop;
    return v_sosi;
  end func_axi4_data_shift_last;

  -- Determine resulting empty if two streams are concatenated
  -- . both empty must use the same nof symbols per data
  function func_axi4_empty_concat(head_empty, tail_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector is
    variable v_a, v_b, v_empty : natural;
  begin
    v_a := TO_UINT(head_empty);
    v_b := TO_UINT(tail_empty);
    v_empty := v_a + v_b;
    if v_empty >= nof_symbols_per_data then
      v_empty := v_empty - nof_symbols_per_data;
    end if;
    return TO_UVEC(v_empty, head_empty'length);
  end func_axi4_empty_concat;

  function func_axi4_empty_split(input_empty, head_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector is
    variable v_a, v_b, v_empty : natural;
  begin
    v_a   := TO_UINT(input_empty);
    v_b   := TO_UINT(head_empty);
    if v_a >= v_b then
      v_empty := v_a - v_b;
    else
      v_empty := (nof_symbols_per_data + v_a) - v_b;
    end if;
    return TO_UVEC(v_empty, head_empty'length);
  end func_axi4_empty_split;

  -- Multiplex the t_axi4_sosi_arr based on the valid, assuming that at most one input is active valid.
  function func_axi4_sosi_arr_mux(axi4 : t_axi4_sosi_arr) return t_axi4_sosi is
    variable v_sosi : t_axi4_sosi := c_axi4_sosi_rst;
  begin
    for I in axi4'range loop
      if axi4(I).tvalid = '1' then
        v_sosi := axi4(I);
        exit;
      end if;
    end loop;
    return v_sosi;
  end func_axi4_sosi_arr_mux;

  -- Determine the combined logical value of corresponding STD_LOGIC fields in t_axi4_*_arr (for all elements or only for the mask[]='1' elements)
  function func_axi4_stream_arr_and(axi4 : t_axi4_siso_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(axi4'range) := (others => '1');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in axi4'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "READY" then v_vec(I) := axi4(I).tready;
        else  report "Error in func_axi4_stream_arr_and for t_axi4_siso_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_and(v_vec);  -- return AND of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_axi4_stream_arr_and;

  function func_axi4_stream_arr_and(axi4 : t_axi4_sosi_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(axi4'range) := (others => '1');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in axi4'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "VALID" then v_vec(I) := axi4(I).tvalid;
        else  report "Error in func_axi4_stream_arr_and for t_axi4_sosi_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_and(v_vec);  -- return AND of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_axi4_stream_arr_and;

  function func_axi4_stream_arr_and(axi4 : t_axi4_siso_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(axi4'range) := (others => '1');
  begin
    return func_axi4_stream_arr_and(axi4, c_mask, str);
  end func_axi4_stream_arr_and;

  function func_axi4_stream_arr_and(axi4 : t_axi4_sosi_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(axi4'range) := (others => '1');
  begin
    return func_axi4_stream_arr_and(axi4, c_mask, str);
  end func_axi4_stream_arr_and;

  function func_axi4_stream_arr_or(axi4 : t_axi4_siso_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(axi4'range) := (others => '0');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in axi4'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "READY" then v_vec(I) := axi4(I).tready;
        else  report "Error in func_axi4_stream_arr_or for t_axi4_siso_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_or(v_vec);  -- return OR of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_axi4_stream_arr_or;

  function func_axi4_stream_arr_or(axi4 : t_axi4_sosi_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(axi4'range) := (others => '0');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in axi4'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "VALID" then v_vec(I) := axi4(I).tvalid;
        else  report "Error in func_axi4_stream_arr_or for t_axi4_sosi_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_or(v_vec);  -- return OR of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_axi4_stream_arr_or;

  function func_axi4_stream_arr_or(axi4 : t_axi4_siso_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(axi4'range) := (others => '1');
  begin
    return func_axi4_stream_arr_or(axi4, c_mask, str);
  end func_axi4_stream_arr_or;

  function func_axi4_stream_arr_or(axi4 : t_axi4_sosi_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(axi4'range) := (others => '1');
  begin
    return func_axi4_stream_arr_or(axi4, c_mask, str);
  end func_axi4_stream_arr_or;

  -- Functions to set or get a STD_LOGIC field as a STD_LOGIC_VECTOR to or from an siso or an sosi array
  function func_axi4_stream_arr_set(axi4 : t_axi4_siso_arr; slv : std_logic_vector; str : string) return t_axi4_siso_arr is
    variable v_axi4  : t_axi4_siso_arr(axi4'range)    := axi4;  -- default
    variable v_slv : std_logic_vector(axi4'range) := slv;  -- map to ensure same range as for axi4
  begin
    for I in axi4'range loop
      if    str = "READY" then v_axi4(I).tready := v_slv(I);
      else  report "Error in func_axi4_stream_arr_set for t_axi4_siso_arr";
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_set;

  function func_axi4_stream_arr_set(axi4 : t_axi4_sosi_arr; slv : std_logic_vector; str : string) return t_axi4_sosi_arr is
    variable v_axi4  : t_axi4_sosi_arr(axi4'range)    := axi4;  -- default
    variable v_slv : std_logic_vector(axi4'range) := slv;  -- map to ensure same range as for axi4
  begin
    for I in axi4'range loop
      if    str = "VALID" then v_axi4(I).tvalid := v_slv(I);
      else  report "Error in func_axi4_stream_arr_set for t_axi4_sosi_arr";
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_set;

  function func_axi4_stream_arr_set(axi4 : t_axi4_siso_arr; sl : std_logic; str : string) return t_axi4_siso_arr is
    variable v_slv : std_logic_vector(axi4'range) := (others => sl);
  begin
    return func_axi4_stream_arr_set(axi4, v_slv, str);
  end func_axi4_stream_arr_set;

  function func_axi4_stream_arr_set(axi4 : t_axi4_sosi_arr; sl : std_logic; str : string) return t_axi4_sosi_arr is
    variable v_slv : std_logic_vector(axi4'range) := (others => sl);
  begin
    return func_axi4_stream_arr_set(axi4, v_slv, str);
  end func_axi4_stream_arr_set;

  function func_axi4_stream_arr_get(axi4 : t_axi4_siso_arr; str : string) return std_logic_vector is
    variable v_ctrl : std_logic_vector(axi4'range);
  begin
    for I in axi4'range loop
      if    str = "READY" then v_ctrl(I) := axi4(I).tready;
      else  report "Error in func_axi4_stream_arr_get for t_axi4_siso_arr";
      end if;
    end loop;
    return v_ctrl;
  end func_axi4_stream_arr_get;

  function func_axi4_stream_arr_get(axi4 : t_axi4_sosi_arr; str : string) return std_logic_vector is
    variable v_ctrl : std_logic_vector(axi4'range);
  begin
    for I in axi4'range loop
      if    str = "VALID" then v_ctrl(I) := axi4(I).tvalid;
      else  report "Error in func_axi4_stream_arr_get for t_axi4_sosi_arr";
      end if;
    end loop;
    return v_ctrl;
  end func_axi4_stream_arr_get;

  -- Functions to select elements from two siso or two sosi arrays (sel[] = '1' selects a, sel[] = '0' selects b)
  function func_axi4_stream_arr_select(sel : std_logic_vector; a, b : t_axi4_siso) return t_axi4_siso_arr is
    variable v_axi4 : t_axi4_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a;
      else
        v_axi4(I) := b;
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_siso_arr; b : t_axi4_siso) return t_axi4_siso_arr is
    variable v_axi4 : t_axi4_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a(I);
      else
        v_axi4(I) := b;
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_siso; b : t_axi4_siso_arr) return t_axi4_siso_arr is
    variable v_axi4 : t_axi4_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a;
      else
        v_axi4(I) := b(I);
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a, b : t_axi4_siso_arr) return t_axi4_siso_arr is
    variable v_axi4 : t_axi4_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a(I);
      else
        v_axi4(I) := b(I);
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a, b : t_axi4_sosi) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a;
      else
        v_axi4(I) := b;
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_sosi_arr; b : t_axi4_sosi) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a(I);
      else
        v_axi4(I) := b;
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a : t_axi4_sosi; b : t_axi4_sosi_arr) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a;
      else
        v_axi4(I) := b(I);
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_select(sel : std_logic_vector; a, b : t_axi4_sosi_arr) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_axi4(I) := a(I);
      else
        v_axi4(I) := b(I);
      end if;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_select;

  function func_axi4_stream_arr_reverse_range(in_arr : t_axi4_siso_arr) return t_axi4_siso_arr is
    variable v_to_range : t_axi4_siso_arr(0 to in_arr'high);
    variable v_downto_range : t_axi4_siso_arr(in_arr'high downto 0);
  begin
    for i in in_arr'range loop
      v_to_range(i)     := in_arr(in_arr'high - i);
      v_downto_range(i) := in_arr(in_arr'high - i);
    end loop;
    if in_arr'left > in_arr'right then
      return v_downto_range;
    elsif in_arr'left < in_arr'right then
      return v_to_range;
    else
      return in_arr;
    end if;
  end func_axi4_stream_arr_reverse_range;

  function func_axi4_stream_arr_reverse_range(in_arr : t_axi4_sosi_arr) return t_axi4_sosi_arr is
    variable v_to_range : t_axi4_sosi_arr(0 to in_arr'high);
    variable v_downto_range : t_axi4_sosi_arr(in_arr'high downto 0);
  begin
    for i in in_arr'range loop
      v_to_range(i)     := in_arr(in_arr'high - i);
      v_downto_range(i) := in_arr(in_arr'high - i);
    end loop;
    if in_arr'left > in_arr'right then
      return v_downto_range;
    elsif in_arr'left < in_arr'right then
      return v_to_range;
    else
      return in_arr;
    end if;
  end func_axi4_stream_arr_reverse_range;

  -- Functions to combinatorially hold the data fields and to set or reset the control fields in an sosi array
  function func_axi4_stream_arr_set_control(axi4 : t_axi4_sosi_arr; ctrl : t_axi4_sosi) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(axi4'range) := axi4;  -- hold sosi data
  begin
    for I in axi4'range loop  -- set sosi control
      v_axi4(I).tvalid := ctrl.tvalid;
      v_axi4(I).tuser := ctrl.tuser;
      v_axi4(I).tdest := ctrl.tdest;
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_set_control;

  function func_axi4_stream_arr_reset_control(axi4 : t_axi4_sosi_arr) return t_axi4_sosi_arr is
    variable v_axi4 : t_axi4_sosi_arr(axi4'range) := axi4;  -- hold sosi data
  begin
    for I in axi4'range loop  -- reset sosi control
      v_axi4(I).tvalid := '0';
    end loop;
    return v_axi4;
  end func_axi4_stream_arr_reset_control;

  -- Functions to convert dp streaming to axi4 streaming
  function func_axi4_stream_from_dp_sosi(dp_sosi : t_dp_sosi) return t_axi4_sosi is
    constant c_max_empty_w : natural := ceil_log2(c_axi4_stream_keep_w);
    variable v_axi4_sosi   : t_axi4_sosi := c_axi4_sosi_rst;
    variable v_empty_int   : natural := 0;
    variable v_keep        : std_logic_vector(c_axi4_stream_keep_w - 1 downto 0) := (others => '1');
  begin
    v_empty_int := TO_UINT(dp_sosi.empty(c_max_empty_w - 1 downto 0));
    if dp_sosi.eop = '1' then  -- empty is only valid on eop
      v_keep(v_empty_int - 1 downto 0) := (others => '0');  -- Keep is one hot encoded.
    else
      v_keep := (others => '1');
    end if;

    v_axi4_sosi.tvalid := dp_sosi.valid;
    v_axi4_sosi.tdata  := dp_sosi.data(c_axi4_stream_data_w - 1 downto 0);
    v_axi4_sosi.tstrb  := v_keep;
    v_axi4_sosi.tkeep  := v_keep;
    v_axi4_sosi.tlast  := dp_sosi.eop;
    v_axi4_sosi.tid    := dp_sosi.channel(c_axi4_stream_tid_w - 1 downto 0);
    v_axi4_sosi.tdest  := (others => '0');
    v_axi4_sosi.tuser  := (others => '0');
    return v_axi4_sosi;
  end func_axi4_stream_from_dp_sosi;

  function func_axi4_stream_from_dp_siso(dp_siso : t_dp_siso) return t_axi4_siso is
    variable v_axi4_siso : t_axi4_siso := c_axi4_siso_rst;
  -- Note that dp_siso.xon is not used.
  begin
    v_axi4_siso.tready := dp_siso.ready;
    return v_axi4_siso;
  end func_axi4_stream_from_dp_siso;

  -- Functions to convert dp streaming to axi4 streaming
  function func_axi4_stream_to_dp_sosi(axi4_sosi : t_axi4_sosi) return t_dp_sosi is
    variable v_dp_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    v_dp_sosi.data(c_axi4_stream_data_w - 1 downto 0) := axi4_sosi.tdata;
    v_dp_sosi.valid   := axi4_sosi.tvalid;
    v_dp_sosi.sop     := '0';  -- Should be generated (first valid after eop).
    v_dp_sosi.eop     := axi4_sosi.tlast;

    -- Cannot always derive from tkeep as it can have any bytes (not just the last bytes)
    -- selected to be valid, the empty field can only select the last bytes to be invalid
    -- for which you can use func_axi4_stream_tkeep_to_dp_empty.
    v_dp_sosi.empty   := (others => '0');
    v_dp_sosi.channel(c_axi4_stream_tid_w - 1 downto 0) := axi4_sosi.tid;
    return v_dp_sosi;
  end func_axi4_stream_to_dp_sosi;

  function func_axi4_stream_to_dp_siso(axi4_siso : t_axi4_siso) return t_dp_siso is
    variable v_dp_siso : t_dp_siso := c_dp_siso_rdy;
  begin
    v_dp_siso.ready := axi4_siso.tready;
    v_dp_siso.xon   := '1';
    return v_dp_siso;
  end func_axi4_stream_to_dp_siso;

  -- Function to derive DP empty from AXI4 tkeep by counting the 0s in TKEEP.
  -- This function assumes that only the the last bytes in an AXI4 data element are set to be null by TKEEP.
  -- e.g. TKEEP = 11111000 will be valid but TKEEP = 11011111 is not.
  function func_axi4_stream_tkeep_to_dp_empty(tkeep : std_logic_vector) return std_logic_vector is
    variable v_count : natural := 0;
  begin
    for I in tkeep'range loop
      if tkeep(I) = '0' then
        v_count := v_count + 1;
      end if;
    end loop;
    return(TO_UVEC(v_count, c_dp_stream_empty_w));
  end func_axi4_stream_tkeep_to_dp_empty;
end axi4_stream_pkg;
