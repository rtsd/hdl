--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Assert output during a packet on the input:
--
--                 ___     _________     ___
-- val      ______|   |___|         |___|   |____
--                 ___
-- sop      ______|   |__________________________
--                                       ___
-- eop      ____________________________|   |____
--
--                 _________________________
-- pkt_det  ______|                         |____

entity dp_packet_detect is
  generic (
    g_latency   : natural := 1
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    -- The SOSI/SISO buses of the stream we're probing
    sosi        : in  t_dp_sosi;
    siso        : in  t_dp_siso := c_dp_siso_rst;  -- Connect this when RL=0

    pkt_det     : out std_logic
  );
end dp_packet_detect;

architecture str of dp_packet_detect is
  signal i_pkt_det    : std_logic;
  signal prev_pkt_det : std_logic;
  signal prev_eop     : std_logic;

  -- For RL=0, these signals are only asserted when src_in.ready='1'.
  signal sop          : std_logic;
  signal eop          : std_logic;
begin
  gen_rl_0: if g_latency = 0 generate
    sop <= sosi.sop and siso.ready;
    eop <= sosi.eop and siso.ready;
  end generate;

  gen_wires: if g_latency > 0 generate
    sop <= sosi.sop;
    eop <= sosi.eop;
  end generate;

  pkt_det <= i_pkt_det;

  p_snk : process(sop, eop, prev_pkt_det, prev_eop)
  begin
    i_pkt_det <= prev_pkt_det;
    if prev_eop = '1' then
      i_pkt_det <= '0';
    end if;
    if sop = '1' then
      i_pkt_det <= '1';
    end if;
  end process;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      prev_eop <= '0';
      prev_pkt_det <= '0';
    elsif rising_edge(clk) then
      prev_eop <= eop;
      prev_pkt_det <= i_pkt_det;
    end if;
  end process;
end str;
