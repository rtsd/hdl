-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_clkbuf_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_clkbuf_global_altclkctrl_150;
library ip_arria10_e3sge3_clkbuf_global_altclkctrl_151;
library ip_arria10_e1sg_clkbuf_global_altclkctrl_180;
library ip_arria10_e2sg_clkbuf_global_altclkctrl_191;

entity tech_clkbuf is
  generic (
    g_technology       : natural := c_tech_select_default;
    g_clock_net        : string  := "GLOBAL"
  );
  port (
    inclk  : in  std_logic;
    outclk : out std_logic
  );
end tech_clkbuf;

architecture str of tech_clkbuf is
begin
  -----------------------------------------------------------------------------
  -- ip_arria10
  -----------------------------------------------------------------------------

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto and g_clock_net = "GLOBAL" generate
    u0 : ip_arria10_clkbuf_global
    port map (
      inclk  => inclk,  -- inclk
      outclk => outclk  -- outclk
    );
  end generate;

  -----------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  -----------------------------------------------------------------------------

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 and g_clock_net = "GLOBAL" generate
    u0 : ip_arria10_e3sge3_clkbuf_global
    port map (
      inclk  => inclk,  -- inclk
      outclk => outclk  -- outclk
    );
  end generate;

  -----------------------------------------------------------------------------
  -- ip_arria10_e1sg
  -----------------------------------------------------------------------------

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg and g_clock_net = "GLOBAL" generate
    u0 : ip_arria10_e1sg_clkbuf_global
    port map (
      inclk  => inclk,  -- inclk
      outclk => outclk  -- outclk
    );
  end generate;

  -----------------------------------------------------------------------------
  -- ip_arria10_e2sg
  -----------------------------------------------------------------------------

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg and g_clock_net = "GLOBAL" generate
    u0 : ip_arria10_e2sg_clkbuf_global
    port map (
      inclk  => inclk,  -- inclk
      outclk => outclk  -- outclk
    );
  end generate;
end architecture;
