library ieee;
use ieee.std_logic_1164.all;

entity pfs_fir_ctrl is
  generic (
    g_nof_prefilter   : natural;
    g_nof_prefilter_w : natural;
    g_nof_taps        : natural;
    g_nof_taps_w      : natural;
    g_sample_width    : natural
  );
  port (
    clk              : in std_logic;
    rst              : in std_logic;
    input_hor        : in std_logic_vector(g_sample_width - 1 downto 0);
    input_ver        : in std_logic_vector(g_sample_width - 1 downto 0);
    input_val        : in std_logic;
    input_sync       : in std_logic;
    coefs_addr       : out std_logic_vector(g_nof_prefilter_w + g_nof_taps_w - 1 downto 0);
    coefs_rden       : out std_logic;
    sample_addr      : out std_logic_vector(g_nof_prefilter_w + g_nof_taps_w - 1 downto 0);
    sample_data_hor  : out std_logic_vector(g_sample_width - 1 downto 0);
    sample_data_ver  : out std_logic_vector(g_sample_width - 1 downto 0);
    sample_wren      : out std_logic;
    taps_addr        : out std_logic_vector(g_nof_prefilter_w + g_nof_taps_w - 1 downto 0);
    taps_rden        : out std_logic;
    res_clr          : out std_logic;
    result_val       : out std_logic;
    result_sync      : out std_logic
  );
end pfs_fir_ctrl;
