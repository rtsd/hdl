-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package i2c_pkg is
  -- I2C register size settings
  --
  constant c_i2c_control_adr_w     : natural := 1;  -- fixed
  constant c_i2c_protocol_adr_w    : natural := 10;  -- 2*10 = 1 kByte is sufficient and fits in 1 M9K RAM block
  constant c_i2c_result_adr_w      : natural := 10;

  constant c_i2c_dat_w             : natural := 8;  -- bytes

  type t_c_i2c_mm is record
    control_adr_w    : natural;  -- = 1, fixed
    protocol_adr_w   : natural;
    protocol_nof_dat : natural;
    result_adr_w     : natural;
    result_nof_dat   : natural;
  end record;

  constant c_i2c_mm : t_c_i2c_mm := (c_i2c_control_adr_w, c_i2c_protocol_adr_w, 2**c_i2c_protocol_adr_w, c_i2c_result_adr_w, 2**c_i2c_result_adr_w);

  -- I2C clock rate and comma time settings
  --
  type t_c_i2c_phy is record
    clk_cnt   : natural;  -- minimal clk_cnt >= 2 when comma_w > 0, when comma_w=0 then minimum clk_cnt = 1
    comma_w   : natural;  -- 2**c_i2c_comma_w * system clock period comma time after I2C start and after each octet, 0 for no comma time
  end record;

  constant c_i2c_bit_rate    : natural := 50;  -- fixed default I2C bit rate in kbps
  constant c_i2c_comma_w_dis : natural := 0;
  constant c_i2c_clk_cnt_sim : natural := 2;  -- suits also comma_w > 0
  constant c_i2c_phy_sim     : t_c_i2c_phy := (1, c_i2c_comma_w_dis);

  -- Calculate clk_cnt from system_clock_freq_in_MHz
  function func_i2c_calculate_clk_cnt(system_clock_freq_in_MHz, bit_rate_in_kHz : natural) return natural;
  function func_i2c_calculate_clk_cnt(system_clock_freq_in_MHz : natural) return natural;

  -- Calculate (clk_cnt, comma_w) from system_clock_freq_in_MHz
  function func_i2c_calculate_phy(system_clock_freq_in_MHz, comma_w : natural) return t_c_i2c_phy;
  function func_i2c_calculate_phy(system_clock_freq_in_MHz          : natural) return t_c_i2c_phy;

  -- Select functions
  function func_i2c_sel_a_b(sel : boolean; a, b : t_c_i2c_mm)  return t_c_i2c_mm;
  function func_i2c_sel_a_b(sel : boolean; a, b : t_c_i2c_phy) return t_c_i2c_phy;
end i2c_pkg;

package body i2c_pkg is
  function func_i2c_calculate_clk_cnt(system_clock_freq_in_MHz, bit_rate_in_kHz : natural) return natural is
    -- . Adapt c_i2c_clk_freq and c_i2c_bit_rate and c_i2c_clk_cnt will be set appropriately
    -- . Default no comma time is needed, it appeared necessary for the uP based I2C slave in the LOFAR HBA client
    constant c_clk_cnt_factor : natural := 5;
  begin
    return system_clock_freq_in_MHz  * 1000 / bit_rate_in_kHz / c_clk_cnt_factor - 1;
  end;

  function func_i2c_calculate_clk_cnt(system_clock_freq_in_MHz : natural) return natural is
    -- . Adapt c_i2c_clk_freq and c_i2c_bit_rate and c_i2c_clk_cnt will be set appropriately
    -- . Default no comma time is needed, it appeared necessary for the uP based I2C slave in the LOFAR HBA client
    constant c_clk_cnt_factor : natural := 5;
  begin
    return system_clock_freq_in_MHz  * 1000 / c_i2c_bit_rate / c_clk_cnt_factor - 1;
  end;

  function func_i2c_calculate_phy(system_clock_freq_in_MHz, comma_w : natural) return t_c_i2c_phy is
  begin
    return (func_i2c_calculate_clk_cnt(system_clock_freq_in_MHz), comma_w);
  end;

  function func_i2c_calculate_phy(system_clock_freq_in_MHz : natural) return t_c_i2c_phy is
  begin
    return func_i2c_calculate_phy(system_clock_freq_in_MHz, c_i2c_comma_w_dis);
  end;

  function func_i2c_sel_a_b(sel : boolean; a, b : t_c_i2c_mm) return t_c_i2c_mm is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function func_i2c_sel_a_b(sel : boolean; a, b : t_c_i2c_phy) return t_c_i2c_phy is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;
end i2c_pkg;
