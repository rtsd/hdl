-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 6 aug 2021
-- Purpose: Verify MM part of mmp_dp_bsn_sync_scheduler
-- Description:
--    The functional part is already verified by tb_tb_dp_bsn_sync_scheduler.vhd.
-- Usage:
-- > as 5
-- > run -all
--   . View *_64 BSN values as radix hex in Wave window to recognize BSN hi word.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mmp_dp_bsn_sync_scheduler is
end tb_mmp_dp_bsn_sync_scheduler;

architecture tb of tb_mmp_dp_bsn_sync_scheduler is
  constant c_mm_clk_period                : time := 40 ns;
  constant c_dp_clk_period                : time := 10 ns;
  constant c_cross_clock_domain_latency   : natural := 20;

  constant c_report_note                  : boolean := false;  -- Use TRUE for tb debugging, else FALSE to keep Transcript window more empty

  constant c_nof_input_sync               : natural := 10;
  constant c_nof_block_per_input_sync     : natural := 17;
  constant c_nof_block_per_output_sync    : natural := 5;
  constant c_block_size                   : natural := 10;
  constant c_ctrl_interval_size_min       : natural := 19;  -- Minimum interval size to use if MM write interval size is set too small.
  constant c_input_gap_size               : natural := 3;
  constant c_sim_nof_blocks               : natural := c_nof_block_per_input_sync * c_nof_input_sync;

  -- DUT settings
  constant c_bsn_w                        : natural := 40;
  constant c_bsn_hi_value                 : natural := 23;
  constant c_ctrl_interval_size           : natural := c_nof_block_per_output_sync * c_block_size;
  constant c_ctrl_start_bsn_lo            : natural := 19;
  constant c_ctrl_start_bsn_hi            : natural := 17;

  signal tb_end                   : std_logic := '0';
  signal stimuli_end              : std_logic := '0';
  signal mm_clk                   : std_logic := '1';
  signal mm_rst                   : std_logic := '1';
  signal dp_clk                   : std_logic := '1';
  signal dp_rst                   : std_logic := '1';

  signal reg_mosi                 : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso                 : t_mem_miso;

  signal ctrl_start_bsn_64        : std_logic_vector(2 * c_word_w - 1 downto 0);

  signal mon_output_enable        : std_logic;
  signal mon_output_interval_size : natural;
  signal mon_current_input_bsn_64 : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal mon_input_bsn_at_sync_64 : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal mon_output_sync_bsn_64   : std_logic_vector(2 * c_word_w - 1 downto 0);
  signal mon_block_size           : natural;

  signal stimuli_sosi             : t_dp_sosi;
  signal in_sosi                  : t_dp_sosi;
  signal out_sosi                 : t_dp_sosi;
  signal out_start                : std_logic;
  signal out_enable               : std_logic;

  signal verify_bsn_hi            : std_logic := '0';
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  ------------------------------------------------------------------------------
  -- MM stimuli and verification
  ------------------------------------------------------------------------------

  p_stimuli_and_verify_mm : process
    variable v_bsn : natural;
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    ---------------------------------------------------------------------------
    -- Initial check
    ---------------------------------------------------------------------------
    -- . Read mon_block_size
    proc_mem_mm_bus_rd(11, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_block_size <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));

    -- . Verify mon_block_size
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mon_block_size = c_block_size
      report "Wrong block_size."
      severity ERROR;

    -- . Read mon_output_interval_size
    proc_mem_mm_bus_rd(1, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_interval_size <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mon_output_interval_size = c_ctrl_interval_size_min
      report "Wrong minimum output interval_size."
      severity ERROR;

    -- . Read mon_output_enable
    proc_mem_mm_bus_rd(8, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_enable <= reg_miso.rddata(0);

    -- . Verify output is off
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mon_output_enable = '0'
      report "DUT output is enabled."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Verify c_ctrl_start_bsn_hi
    ---------------------------------------------------------------------------

    -- . Write ctrl_start_bsn
    proc_mem_mm_bus_wr(2, c_ctrl_start_bsn_lo,  mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(3, c_ctrl_start_bsn_hi,  mm_clk, reg_miso, reg_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    -- . Read back ctrl_start_bsn
    proc_mem_mm_bus_rd(2, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    ctrl_start_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    ctrl_start_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    proc_common_wait_some_cycles(mm_clk, 1);
    assert c_ctrl_start_bsn_lo = TO_UINT(ctrl_start_bsn_64( c_word_w - 1 downto 0))
      report "Wrong ctrl_start_bsn low word."
      severity ERROR;
    assert c_ctrl_start_bsn_hi = TO_UINT(ctrl_start_bsn_64(2 * c_word_w - 1 downto c_word_w))
      report "Wrong ctrl_start_bsn high word."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Setup, enable and verify DUT output
    ---------------------------------------------------------------------------
    -- . Read mon_current_input_bsn_64
    proc_mem_mm_bus_rd(4, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);

    -- . Select start BSN in the future
    v_bsn := TO_UINT(mon_current_input_bsn_64) + 20;

    -- . Setup output sync interval
    proc_mem_mm_bus_wr(1, c_ctrl_interval_size, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(2, v_bsn,                mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(3, 0,                    mm_clk, reg_miso, reg_mosi);
    proc_common_wait_some_cycles(dp_clk, c_block_size * 10);

    -- . Enable output
    proc_mem_mm_bus_wr(0, 1, mm_clk, reg_miso, reg_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    proc_common_wait_some_cycles(dp_clk, c_block_size * 10);

    -- . Read back ctrl_start_bsn
    proc_mem_mm_bus_rd(2, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    ctrl_start_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    ctrl_start_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_current_input_bsn_64
    proc_mem_mm_bus_rd(4, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_input_bsn_at_sync_64
    proc_mem_mm_bus_rd(6, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(7, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_output_sync_bsn_64
    proc_mem_mm_bus_rd(9, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(10, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_output_enable
    proc_mem_mm_bus_rd(8, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_enable <= reg_miso.rddata(0);

    -- . Read mon_output_interval_size
    proc_mem_mm_bus_rd(1, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_interval_size <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));

    -- Verify output is on and running
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mon_output_enable = '1'
      report "mon_output_enable is not enabled."
      severity ERROR;
    assert out_enable = '1'
      report "output_enable is not enabled."
      severity ERROR;
    assert mon_output_interval_size = c_ctrl_interval_size
      report "mon_output_interval_size is not ctrl_interval_size."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Check that monitor BSN are incrementing
    ---------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, c_ctrl_interval_size * 3);

    -- . Check mon_current_input_bsn_64
    v_bsn := TO_UINT(mon_current_input_bsn_64);
    proc_mem_mm_bus_rd(4, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    if c_report_note then
      report "mon_current_input_bsn : " & int_to_str(v_bsn) & ", " & int_to_str(TO_UINT(mon_current_input_bsn_64))
        severity NOTE;
    end if;
    assert v_bsn < TO_UINT(mon_current_input_bsn_64)
      report "DUT mon_current_input_bsn is not incrementing."
      severity ERROR;

    -- . Check mon_input_bsn_at_sync_64
    v_bsn := TO_UINT(mon_input_bsn_at_sync_64);
    proc_mem_mm_bus_rd(6, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(7, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    if c_report_note then
      report "mon_input_bsn_at_sync : " & int_to_str(v_bsn) & ", " & int_to_str(TO_UINT(mon_input_bsn_at_sync_64))
        severity NOTE;
    end if;
    assert v_bsn < TO_UINT(mon_input_bsn_at_sync_64)
      report "DUT mon_input_bsn_at_sync is not incrementing."
      severity ERROR;
    assert (TO_UINT(mon_input_bsn_at_sync_64) - v_bsn) mod c_nof_block_per_input_sync = 0
      report "TB input_sync interval is not correct."
      severity ERROR;

    -- . Check mon_output_sync_bsn_64
    v_bsn := TO_UINT(mon_output_sync_bsn_64);
    proc_mem_mm_bus_rd(9, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(10, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    if c_report_note then
      report "mon_output_sync_bsn : " & int_to_str(v_bsn) & ", " & int_to_str(TO_UINT(mon_output_sync_bsn_64))
        severity NOTE;
    end if;
    assert v_bsn < TO_UINT(mon_output_sync_bsn_64)
      report "DUT mon_output_sync_bsn is not incrementing."
      severity ERROR;
    assert (TO_UINT(mon_output_sync_bsn_64) - v_bsn) mod c_nof_block_per_output_sync = 0
      report "DUT output_sync interval is not correct."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Verify BSN hi word
    ---------------------------------------------------------------------------
    -- . wait until input block boundary
    proc_common_wait_until_high(dp_clk, stimuli_sosi.sop);
    verify_bsn_hi <= '1';
    proc_common_wait_some_cycles(dp_clk, c_block_size * c_nof_block_per_input_sync * 2);

    -- . Read mon_current_input_bsn_64
    proc_mem_mm_bus_rd(4, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_input_bsn_at_sync_64
    proc_mem_mm_bus_rd(6, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(7, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_input_bsn_at_sync_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);

    -- . Read mon_output_sync_bsn_64
    proc_mem_mm_bus_rd(9, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(c_word_w - 1 downto 0) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(10, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_sync_bsn_64(2 * c_word_w - 1 downto c_word_w) <= reg_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);

    -- The out_sosi.bsn will not reach the c_bsn_hi_value, because the
    -- dp_bsn_sync_scheduler will be busy trying to catch up, see
    -- nxt_r.update_bsn = '1' in dp_bsn_sync_scheduler.vhd. Therefore
    -- expected mon_output_sync_bsn_64 hi value is still 0.
    assert c_bsn_hi_value = TO_UINT(mon_current_input_bsn_64(2 * c_word_w - 1 downto c_word_w))
      report "Wrong mon_current_input_bsn high word."
      severity ERROR;
    assert c_bsn_hi_value = TO_UINT(mon_input_bsn_at_sync_64(2 * c_word_w - 1 downto c_word_w))
      report "Wrong mon_input_bsn_at_sync high word."
      severity ERROR;
    assert 0 = TO_UINT(mon_output_sync_bsn_64( 2 * c_word_w - 1 downto c_word_w))
      report "Wrong mon_output_sync_bsn high word."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Disable and verify DUT output
    ---------------------------------------------------------------------------

    -- . Disable output
    proc_mem_mm_bus_wr(0, 0, mm_clk, reg_miso, reg_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    proc_common_wait_some_cycles(dp_clk, c_block_size * 10);

    -- . Read mon_output_enable
    proc_mem_mm_bus_rd(8, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    mon_output_enable <= reg_miso.rddata(0);

    -- Verify output is on and running
    proc_common_wait_some_cycles(mm_clk, 1);
    assert mon_output_enable = '0'
      report "DUT mon_output_enable is not diabled."
      severity ERROR;
    assert out_enable = '0'
      report "DUT output_enable is not enabled."
      severity ERROR;

    ---------------------------------------------------------------------------
    -- End of test
    ---------------------------------------------------------------------------
    proc_common_wait_until_high(dp_clk, stimuli_end);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Streaming stimuli
  ------------------------------------------------------------------------------

  p_in_sosi : process(stimuli_sosi, verify_bsn_hi)
  begin
    in_sosi <= stimuli_sosi;

    if verify_bsn_hi = '1' then
      -- Set hi word of input BSN
      in_sosi.bsn(2 * c_word_w - 1 downto c_word_w) <= TO_UVEC(c_bsn_hi_value, c_word_w);
    end if;
  end process;

  -- Generate data blocks with input sync
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period  => c_nof_block_per_input_sync,
    g_err_init     => 0,
    g_err_incr     => 0,  -- do not increment, to not distract from viewing of BSN in Wave window
    g_channel_init => 0,
    g_channel_incr => 0,  -- do not increment, to not distract from viewing of BSN in Wave window
    g_nof_repeat   => c_sim_nof_blocks,
    g_pkt_len      => c_block_size,
    g_pkt_gap      => c_input_gap_size
  )
  port map (
    rst               => dp_rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  u_mmp_dp_bsn_sync_scheduler : entity work.mmp_dp_bsn_sync_scheduler
  generic map (
    g_bsn_w                  => c_bsn_w,
    g_block_size             => c_block_size,
    g_ctrl_interval_size_min => c_ctrl_interval_size_min
  )
  port map (
    -- Clocks and reset
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,
    dp_rst          => dp_rst,
    dp_clk          => dp_clk,

    -- MM control
    reg_mosi        => reg_mosi,
    reg_miso        => reg_miso,

    -- Streaming
    in_sosi         => in_sosi,
    out_sosi        => out_sosi,
    out_start       => out_start,
    out_enable      => out_enable
  );
end tb;
