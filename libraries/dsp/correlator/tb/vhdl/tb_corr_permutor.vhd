--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_corr_permutor is
  generic (
    g_nof_inputs  : natural := 4;
    g_nof_outputs : natural := 2;
    g_nof_cycles  : natural := 5
  );
end tb_corr_permutor;

architecture tb of tb_corr_permutor is
  constant c_clk_period            : time := 10 ns;
  constant c_data_w                : natural := 16;
  constant c_input_gap_size        : natural := g_nof_cycles - 1;

  signal tb_end                    : std_logic := '0';
  signal clk                       : std_logic := '1';
  signal rst                       : std_logic;

  signal permutor_snk_in_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal permutor_src_out_2arr_2 : t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0);
begin
  -------------------------------------------------------------------------------
  -- Clk & reset
  -------------------------------------------------------------------------------
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  -------------------------------------------------------------------------------
  -- Permutor
  -------------------------------------------------------------------------------
  u_corr_permutor : entity work.corr_permutor
  generic map (
    g_nof_inputs   => g_nof_inputs,
    g_nof_outputs  => g_nof_outputs,
    g_nof_cycles   => g_nof_cycles,
    g_data_w       => c_data_w
  )
  port map (
    rst            => rst,
    clk            => clk,
    snk_in_arr     => permutor_snk_in_arr,
    src_out_2arr_2 => permutor_src_out_2arr_2
  );

  -------------------------------------------------------------------------------
  -- Generate stream indices as input data
  -------------------------------------------------------------------------------
  p_stimuli: process
  begin
    wait until rst = '0';
    wait for 100 ns;
    for i in 0 to 10 - 1 loop
      -- One valid input cycle
      for j in 0 to g_nof_inputs - 1 loop
        -- Feed the permutor. Use the stream index as data word
        permutor_snk_in_arr(j).re <= TO_DP_DSP_DATA(j);
        permutor_snk_in_arr(j).im <= TO_DP_DSP_DATA(j);
        permutor_snk_in_arr(j).valid <= '1';
      end loop;
      wait for c_clk_period;

      -- Invalid cycles when folding
      for j in 0 to g_nof_inputs - 1 loop
        -- Let the permutor do its thing
        permutor_snk_in_arr(j).re <= (others => 'U');
        permutor_snk_in_arr(j).im <= (others => 'U');
        permutor_snk_in_arr(j).valid <= '0';
      end loop;
      wait for c_input_gap_size * c_clk_period;
    end loop;
  end process;
end tb;
