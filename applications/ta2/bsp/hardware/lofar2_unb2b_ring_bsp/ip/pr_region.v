// (C) 1992-2018 Intel Corporation.                            
// Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
// and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
// and/or other countries. Other marks and brands may be claimed as the property  
// of others. See Trademarks on intel.com for full list of Intel trademarks or    
// the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
// Your use of Intel Corporation's design tools, logic functions and other        
// software and tools, and its AMPP partner logic functions, and any output       
// files any of the foregoing (including device programming or simulation         
// files), and any associated documentation or information are expressly subject  
// to the terms and conditions of the Altera Program License Subscription         
// Agreement, Intel MegaCore Function License Agreement, or other applicable      
// license agreement, including, without limitation, that your use is for the     
// sole purpose of programming logic devices manufactured by Intel and sold by    
// Intel or its authorized distributors.  Please refer to the applicable          
// agreement for further details.                                                 

module pr_region (
//  input  wire [30:0]  cc_snoop_data,
//  input  wire         cc_snoop_valid,
//  output wire         cc_snoop_ready,
//  input  wire         cc_snoop_clk_clk,
  input  wire         clock_reset_clk,
  input  wire         clock_reset2x_clk,
  input  wire         clock_reset_reset_reset_n,
  output wire         kernel_cra_waitrequest,
  output wire [63:0]  kernel_cra_readdata,
  output wire         kernel_cra_readdatavalid,
  input  wire [0:0]   kernel_cra_burstcount,
  input  wire [63:0]  kernel_cra_writedata,
  input  wire [29:0]  kernel_cra_address,
  input  wire         kernel_cra_write,
  input  wire         kernel_cra_read,
  input  wire [7:0]   kernel_cra_byteenable,
  input  wire         kernel_cra_debugaccess,
  output wire         kernel_irq_irq,


  output  wire [6:0]   kernel_register_mem_address,                
  output  wire         kernel_register_mem_clken,      
  output  wire         kernel_register_mem_chipselect, 
  output  wire         kernel_register_mem_write,      
  input   wire [255:0] kernel_register_mem_readdata,   
  output  wire [255:0] kernel_register_mem_writedata,  
  output  wire [31:0]  kernel_register_mem_byteenable,
 
  input  wire [71:0]  kernel_stream_src_mm_io_data,
  input  wire         kernel_stream_src_mm_io_valid,
  output wire         kernel_stream_src_mm_io_ready,
  output wire [31:0]  kernel_stream_snk_mm_io_data,
  output wire         kernel_stream_snk_mm_io_valid,
  input  wire         kernel_stream_snk_mm_io_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_0_data,
  input  wire         kernel_stream_src_10GbE_ring_0_valid,
  output wire         kernel_stream_src_10GbE_ring_0_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_0_data,
  output wire         kernel_stream_snk_10GbE_ring_0_valid,
  input  wire         kernel_stream_snk_10GbE_ring_0_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_1_data,
  input  wire         kernel_stream_src_10GbE_ring_1_valid,
  output wire         kernel_stream_src_10GbE_ring_1_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_1_data,
  output wire         kernel_stream_snk_10GbE_ring_1_valid,
  input  wire         kernel_stream_snk_10GbE_ring_1_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_2_data,
  input  wire         kernel_stream_src_10GbE_ring_2_valid,
  output wire         kernel_stream_src_10GbE_ring_2_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_2_data,
  output wire         kernel_stream_snk_10GbE_ring_2_valid,
  input  wire         kernel_stream_snk_10GbE_ring_2_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_3_data,
  input  wire         kernel_stream_src_10GbE_ring_3_valid,
  output wire         kernel_stream_src_10GbE_ring_3_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_3_data,
  output wire         kernel_stream_snk_10GbE_ring_3_valid,
  input  wire         kernel_stream_snk_10GbE_ring_3_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_4_data,
  input  wire         kernel_stream_src_10GbE_ring_4_valid,
  output wire         kernel_stream_src_10GbE_ring_4_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_4_data,
  output wire         kernel_stream_snk_10GbE_ring_4_valid,
  input  wire         kernel_stream_snk_10GbE_ring_4_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_5_data,
  input  wire         kernel_stream_src_10GbE_ring_5_valid,
  output wire         kernel_stream_src_10GbE_ring_5_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_5_data,
  output wire         kernel_stream_snk_10GbE_ring_5_valid,
  input  wire         kernel_stream_snk_10GbE_ring_5_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_6_data,
  input  wire         kernel_stream_src_10GbE_ring_6_valid,
  output wire         kernel_stream_src_10GbE_ring_6_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_6_data,
  output wire         kernel_stream_snk_10GbE_ring_6_valid,
  input  wire         kernel_stream_snk_10GbE_ring_6_ready,

  input  wire [103:0] kernel_stream_src_10GbE_ring_7_data,
  input  wire         kernel_stream_src_10GbE_ring_7_valid,
  output wire         kernel_stream_src_10GbE_ring_7_ready,
  output wire [103:0] kernel_stream_snk_10GbE_ring_7_data,
  output wire         kernel_stream_snk_10GbE_ring_7_valid,
  input  wire         kernel_stream_snk_10GbE_ring_7_ready,

  input  wire [103:0] kernel_stream_src_10GbE_qsfp_0_data,
  input  wire         kernel_stream_src_10GbE_qsfp_0_valid,
  output wire         kernel_stream_src_10GbE_qsfp_0_ready,
  output wire [103:0] kernel_stream_snk_10GbE_qsfp_0_data,
  output wire         kernel_stream_snk_10GbE_qsfp_0_valid,
  input  wire         kernel_stream_snk_10GbE_qsfp_0_ready,

  input  wire [103:0] kernel_stream_src_10GbE_qsfp_1_data,
  input  wire         kernel_stream_src_10GbE_qsfp_1_valid,
  output wire         kernel_stream_src_10GbE_qsfp_1_ready,
  output wire [103:0] kernel_stream_snk_10GbE_qsfp_1_data,
  output wire         kernel_stream_snk_10GbE_qsfp_1_valid,
  input  wire         kernel_stream_snk_10GbE_qsfp_1_ready,

  input  wire [103:0] kernel_stream_src_10GbE_qsfp_2_data,
  input  wire         kernel_stream_src_10GbE_qsfp_2_valid,
  output wire         kernel_stream_src_10GbE_qsfp_2_ready,
  output wire [103:0] kernel_stream_snk_10GbE_qsfp_2_data,
  output wire         kernel_stream_snk_10GbE_qsfp_2_valid,
  input  wire         kernel_stream_snk_10GbE_qsfp_2_ready,

  input  wire [103:0] kernel_stream_src_10GbE_qsfp_3_data,
  input  wire         kernel_stream_src_10GbE_qsfp_3_valid,
  output wire         kernel_stream_src_10GbE_qsfp_3_ready,
  output wire [103:0] kernel_stream_snk_10GbE_qsfp_3_data,
  output wire         kernel_stream_snk_10GbE_qsfp_3_valid,
  input  wire         kernel_stream_snk_10GbE_qsfp_3_ready,

  input  wire [167:0] kernel_stream_src_lane_0_data,
  input  wire         kernel_stream_src_lane_0_valid,
  output wire         kernel_stream_src_lane_0_ready,
  output wire [167:0] kernel_stream_snk_lane_0_data,
  output wire         kernel_stream_snk_lane_0_valid,
  input  wire         kernel_stream_snk_lane_0_ready,

  input  wire [167:0] kernel_stream_src_lane_1_data,
  input  wire         kernel_stream_src_lane_1_valid,
  output wire         kernel_stream_src_lane_1_ready,
  output wire [167:0] kernel_stream_snk_lane_1_data,
  output wire         kernel_stream_snk_lane_1_valid,
  input  wire         kernel_stream_snk_lane_1_ready,

  input  wire [167:0] kernel_stream_src_lane_2_data,
  input  wire         kernel_stream_src_lane_2_valid,
  output wire         kernel_stream_src_lane_2_ready,
  output wire [167:0] kernel_stream_snk_lane_2_data,
  output wire         kernel_stream_snk_lane_2_valid,
  input  wire         kernel_stream_snk_lane_2_ready,

  input  wire [167:0] kernel_stream_src_lane_3_data,
  input  wire         kernel_stream_src_lane_3_valid,
  output wire         kernel_stream_src_lane_3_ready,
  output wire [167:0] kernel_stream_snk_lane_3_data,
  output wire         kernel_stream_snk_lane_3_valid,
  input  wire         kernel_stream_snk_lane_3_ready,

  input  wire [167:0] kernel_stream_src_lane_4_data,
  input  wire         kernel_stream_src_lane_4_valid,
  output wire         kernel_stream_src_lane_4_ready,
  output wire [167:0] kernel_stream_snk_lane_4_data,
  output wire         kernel_stream_snk_lane_4_valid,
  input  wire         kernel_stream_snk_lane_4_ready,

  input  wire [167:0] kernel_stream_src_lane_5_data,
  input  wire         kernel_stream_src_lane_5_valid,
  output wire         kernel_stream_src_lane_5_ready,
  output wire [167:0] kernel_stream_snk_lane_5_data,
  output wire         kernel_stream_snk_lane_5_valid,
  input  wire         kernel_stream_snk_lane_5_ready,

  input  wire [167:0] kernel_stream_src_lane_6_data,
  input  wire         kernel_stream_src_lane_6_valid,
  output wire         kernel_stream_src_lane_6_ready,
  output wire [167:0] kernel_stream_snk_lane_6_data,
  output wire         kernel_stream_snk_lane_6_valid,
  input  wire         kernel_stream_snk_lane_6_ready,

  input  wire [167:0] kernel_stream_src_lane_7_data,
  input  wire         kernel_stream_src_lane_7_valid,
  output wire         kernel_stream_src_lane_7_ready,
  output wire [167:0] kernel_stream_snk_lane_7_data,
  output wire         kernel_stream_snk_lane_7_valid,
  input  wire         kernel_stream_snk_lane_7_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_0_data,
  output wire         kernel_stream_snk_rx_monitor_0_valid,
  input  wire         kernel_stream_snk_rx_monitor_0_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_0_data,
  output wire         kernel_stream_snk_tx_monitor_0_valid,
  input  wire         kernel_stream_snk_tx_monitor_0_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_1_data,
  output wire         kernel_stream_snk_rx_monitor_1_valid,
  input  wire         kernel_stream_snk_rx_monitor_1_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_1_data,
  output wire         kernel_stream_snk_tx_monitor_1_valid,
  input  wire         kernel_stream_snk_tx_monitor_1_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_2_data,
  output wire         kernel_stream_snk_rx_monitor_2_valid,
  input  wire         kernel_stream_snk_rx_monitor_2_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_2_data,
  output wire         kernel_stream_snk_tx_monitor_2_valid,
  input  wire         kernel_stream_snk_tx_monitor_2_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_3_data,
  output wire         kernel_stream_snk_rx_monitor_3_valid,
  input  wire         kernel_stream_snk_rx_monitor_3_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_3_data,
  output wire         kernel_stream_snk_tx_monitor_3_valid,
  input  wire         kernel_stream_snk_tx_monitor_3_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_4_data,
  output wire         kernel_stream_snk_rx_monitor_4_valid,
  input  wire         kernel_stream_snk_rx_monitor_4_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_4_data,
  output wire         kernel_stream_snk_tx_monitor_4_valid,
  input  wire         kernel_stream_snk_tx_monitor_4_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_5_data,
  output wire         kernel_stream_snk_rx_monitor_5_valid,
  input  wire         kernel_stream_snk_rx_monitor_5_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_5_data,
  output wire         kernel_stream_snk_tx_monitor_5_valid,
  input  wire         kernel_stream_snk_tx_monitor_5_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_6_data,
  output wire         kernel_stream_snk_rx_monitor_6_valid,
  input  wire         kernel_stream_snk_rx_monitor_6_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_6_data,
  output wire         kernel_stream_snk_tx_monitor_6_valid,
  input  wire         kernel_stream_snk_tx_monitor_6_ready,

  output wire [167:0] kernel_stream_snk_rx_monitor_7_data,
  output wire         kernel_stream_snk_rx_monitor_7_valid,
  input  wire         kernel_stream_snk_rx_monitor_7_ready,
  output wire [167:0] kernel_stream_snk_tx_monitor_7_data,
  output wire         kernel_stream_snk_tx_monitor_7_valid,
  input  wire         kernel_stream_snk_tx_monitor_7_ready,


  input  wire [103:0] kernel_stream_src_bs_data,
  input  wire         kernel_stream_src_bs_valid,
  output wire         kernel_stream_src_bs_ready


);
  wire [11:0] kernel_system_register_mem_address;
  wire        kernel_system_register_mem_write;
  wire        kernel_system_register_mem_read;
  reg         kernel_system_register_mem_readdatavalid;
  
  assign kernel_register_mem_address = kernel_system_register_mem_address[11:5];
  assign kernel_register_mem_chipselect = 1'b1;
  assign kernel_register_mem_clken = kernel_system_register_mem_write|kernel_system_register_mem_read;
  assign kernel_register_mem_write = kernel_system_register_mem_write;
  
  always @( posedge clock_reset_clk or negedge clock_reset_reset_reset_n )
  begin
    if (clock_reset_reset_reset_n  == 1'b0 ) begin
      kernel_system_register_mem_readdatavalid <= 1'b0;
    end else begin
       kernel_system_register_mem_readdatavalid <= kernel_system_register_mem_read;
    end
  end


//=======================================================
//  kernel_system instantiation
//=======================================================
kernel_system kernel_system_inst
(
  // kernel_system ports
  .clock_reset_clk(clock_reset_clk),
  .clock_reset2x_clk(clock_reset2x_clk),
  .clock_reset_reset_reset_n(clock_reset_reset_reset_n),
  .kernel_irq_irq(kernel_irq_irq),
  .cc_snoop_clk_clk(clock_reset_clk),
//  .cc_snoop_data(cc_snoop_data),
//  .cc_snoop_valid(cc_snoop_valid),
//  .cc_snoop_ready(cc_snoop_ready),
  .kernel_cra_waitrequest(kernel_cra_waitrequest),
  .kernel_cra_readdata(kernel_cra_readdata),
  .kernel_cra_readdatavalid(kernel_cra_readdatavalid),
  .kernel_cra_burstcount(kernel_cra_burstcount),
  .kernel_cra_writedata(kernel_cra_writedata),
  .kernel_cra_address(kernel_cra_address),
  .kernel_cra_write(kernel_cra_write),
  .kernel_cra_read(kernel_cra_read),
  .kernel_cra_byteenable(kernel_cra_byteenable),
  .kernel_cra_debugaccess(kernel_cra_debugaccess),

  .kernel_register_mem_read(kernel_system_register_mem_read),
  .kernel_register_mem_write(kernel_system_register_mem_write),
  .kernel_register_mem_address(kernel_system_register_mem_address),
  .kernel_register_mem_writedata(kernel_register_mem_writedata),
  .kernel_register_mem_byteenable(kernel_register_mem_byteenable),
  .kernel_register_mem_readdata(kernel_register_mem_readdata),
  .kernel_register_mem_readdatavalid(kernel_system_register_mem_readdatavalid),


  .kernel_input_10GbE_ring_0_data( kernel_stream_src_10GbE_ring_0_data),
  .kernel_input_10GbE_ring_0_ready(kernel_stream_src_10GbE_ring_0_ready),
  .kernel_input_10GbE_ring_0_valid(kernel_stream_src_10GbE_ring_0_valid),

  .kernel_input_10GbE_ring_1_data( kernel_stream_src_10GbE_ring_1_data),
  .kernel_input_10GbE_ring_1_ready(kernel_stream_src_10GbE_ring_1_ready),
  .kernel_input_10GbE_ring_1_valid(kernel_stream_src_10GbE_ring_1_valid),

  .kernel_input_10GbE_ring_2_data( kernel_stream_src_10GbE_ring_2_data),
  .kernel_input_10GbE_ring_2_ready(kernel_stream_src_10GbE_ring_2_ready),
  .kernel_input_10GbE_ring_2_valid(kernel_stream_src_10GbE_ring_2_valid),

  .kernel_input_10GbE_ring_3_data( kernel_stream_src_10GbE_ring_3_data),
  .kernel_input_10GbE_ring_3_ready(kernel_stream_src_10GbE_ring_3_ready),
  .kernel_input_10GbE_ring_3_valid(kernel_stream_src_10GbE_ring_3_valid),

  .kernel_input_10GbE_ring_4_data( kernel_stream_src_10GbE_ring_4_data),
  .kernel_input_10GbE_ring_4_ready(kernel_stream_src_10GbE_ring_4_ready),
  .kernel_input_10GbE_ring_4_valid(kernel_stream_src_10GbE_ring_4_valid),

  .kernel_input_10GbE_ring_5_data( kernel_stream_src_10GbE_ring_5_data),
  .kernel_input_10GbE_ring_5_ready(kernel_stream_src_10GbE_ring_5_ready),
  .kernel_input_10GbE_ring_5_valid(kernel_stream_src_10GbE_ring_5_valid),

  .kernel_input_10GbE_ring_6_data( kernel_stream_src_10GbE_ring_6_data),
  .kernel_input_10GbE_ring_6_ready(kernel_stream_src_10GbE_ring_6_ready),
  .kernel_input_10GbE_ring_6_valid(kernel_stream_src_10GbE_ring_6_valid),

  .kernel_input_10GbE_ring_7_data( kernel_stream_src_10GbE_ring_7_data),
  .kernel_input_10GbE_ring_7_ready(kernel_stream_src_10GbE_ring_7_ready),
  .kernel_input_10GbE_ring_7_valid(kernel_stream_src_10GbE_ring_7_valid),

  .kernel_input_10GbE_qsfp_0_data( kernel_stream_src_10GbE_qsfp_0_data),
  .kernel_input_10GbE_qsfp_0_ready(kernel_stream_src_10GbE_qsfp_0_ready),
  .kernel_input_10GbE_qsfp_0_valid(kernel_stream_src_10GbE_qsfp_0_valid),

  .kernel_input_10GbE_qsfp_1_data( kernel_stream_src_10GbE_qsfp_1_data),
  .kernel_input_10GbE_qsfp_1_ready(kernel_stream_src_10GbE_qsfp_1_ready),
  .kernel_input_10GbE_qsfp_1_valid(kernel_stream_src_10GbE_qsfp_1_valid),

  .kernel_input_10GbE_qsfp_2_data( kernel_stream_src_10GbE_qsfp_2_data),
  .kernel_input_10GbE_qsfp_2_ready(kernel_stream_src_10GbE_qsfp_2_ready),
  .kernel_input_10GbE_qsfp_2_valid(kernel_stream_src_10GbE_qsfp_2_valid),

  .kernel_input_10GbE_qsfp_3_data( kernel_stream_src_10GbE_qsfp_3_data),
  .kernel_input_10GbE_qsfp_3_ready(kernel_stream_src_10GbE_qsfp_3_ready),
  .kernel_input_10GbE_qsfp_3_valid(kernel_stream_src_10GbE_qsfp_3_valid),

  .kernel_output_10GbE_ring_0_data( kernel_stream_snk_10GbE_ring_0_data),
  .kernel_output_10GbE_ring_0_ready(kernel_stream_snk_10GbE_ring_0_ready),
  .kernel_output_10GbE_ring_0_valid(kernel_stream_snk_10GbE_ring_0_valid),

  .kernel_output_10GbE_ring_1_data( kernel_stream_snk_10GbE_ring_1_data),
  .kernel_output_10GbE_ring_1_ready(kernel_stream_snk_10GbE_ring_1_ready),
  .kernel_output_10GbE_ring_1_valid(kernel_stream_snk_10GbE_ring_1_valid),

  .kernel_output_10GbE_ring_2_data( kernel_stream_snk_10GbE_ring_2_data),
  .kernel_output_10GbE_ring_2_ready(kernel_stream_snk_10GbE_ring_2_ready),
  .kernel_output_10GbE_ring_2_valid(kernel_stream_snk_10GbE_ring_2_valid),

  .kernel_output_10GbE_ring_3_data( kernel_stream_snk_10GbE_ring_3_data),
  .kernel_output_10GbE_ring_3_ready(kernel_stream_snk_10GbE_ring_3_ready),
  .kernel_output_10GbE_ring_3_valid(kernel_stream_snk_10GbE_ring_3_valid),

  .kernel_output_10GbE_ring_4_data( kernel_stream_snk_10GbE_ring_4_data),
  .kernel_output_10GbE_ring_4_ready(kernel_stream_snk_10GbE_ring_4_ready),
  .kernel_output_10GbE_ring_4_valid(kernel_stream_snk_10GbE_ring_4_valid),

  .kernel_output_10GbE_ring_5_data( kernel_stream_snk_10GbE_ring_5_data),
  .kernel_output_10GbE_ring_5_ready(kernel_stream_snk_10GbE_ring_5_ready),
  .kernel_output_10GbE_ring_5_valid(kernel_stream_snk_10GbE_ring_5_valid),

  .kernel_output_10GbE_ring_6_data( kernel_stream_snk_10GbE_ring_6_data),
  .kernel_output_10GbE_ring_6_ready(kernel_stream_snk_10GbE_ring_6_ready),
  .kernel_output_10GbE_ring_6_valid(kernel_stream_snk_10GbE_ring_6_valid),

  .kernel_output_10GbE_ring_7_data( kernel_stream_snk_10GbE_ring_7_data),
  .kernel_output_10GbE_ring_7_ready(kernel_stream_snk_10GbE_ring_7_ready),
  .kernel_output_10GbE_ring_7_valid(kernel_stream_snk_10GbE_ring_7_valid),

  .kernel_output_10GbE_qsfp_0_data( kernel_stream_snk_10GbE_qsfp_0_data),
  .kernel_output_10GbE_qsfp_0_ready(kernel_stream_snk_10GbE_qsfp_0_ready),
  .kernel_output_10GbE_qsfp_0_valid(kernel_stream_snk_10GbE_qsfp_0_valid),

  .kernel_output_10GbE_qsfp_1_data( kernel_stream_snk_10GbE_qsfp_1_data),
  .kernel_output_10GbE_qsfp_1_ready(kernel_stream_snk_10GbE_qsfp_1_ready),
  .kernel_output_10GbE_qsfp_1_valid(kernel_stream_snk_10GbE_qsfp_1_valid),

  .kernel_output_10GbE_qsfp_2_data( kernel_stream_snk_10GbE_qsfp_2_data),
  .kernel_output_10GbE_qsfp_2_ready(kernel_stream_snk_10GbE_qsfp_2_ready),
  .kernel_output_10GbE_qsfp_2_valid(kernel_stream_snk_10GbE_qsfp_2_valid),

  .kernel_output_10GbE_qsfp_3_data( kernel_stream_snk_10GbE_qsfp_3_data),
  .kernel_output_10GbE_qsfp_3_ready(kernel_stream_snk_10GbE_qsfp_3_ready),
  .kernel_output_10GbE_qsfp_3_valid(kernel_stream_snk_10GbE_qsfp_3_valid),

  .kernel_input_lane_0_data( kernel_stream_src_lane_0_data),
  .kernel_input_lane_0_ready(kernel_stream_src_lane_0_ready),
  .kernel_input_lane_0_valid(kernel_stream_src_lane_0_valid),
  .kernel_output_lane_0_data( kernel_stream_snk_lane_0_data),
  .kernel_output_lane_0_ready(kernel_stream_snk_lane_0_ready),
  .kernel_output_lane_0_valid(kernel_stream_snk_lane_0_valid),

  .kernel_input_lane_1_data( kernel_stream_src_lane_1_data),
  .kernel_input_lane_1_ready(kernel_stream_src_lane_1_ready),
  .kernel_input_lane_1_valid(kernel_stream_src_lane_1_valid),
  .kernel_output_lane_1_data( kernel_stream_snk_lane_1_data),
  .kernel_output_lane_1_ready(kernel_stream_snk_lane_1_ready),
  .kernel_output_lane_1_valid(kernel_stream_snk_lane_1_valid),

  .kernel_input_lane_2_data( kernel_stream_src_lane_2_data),
  .kernel_input_lane_2_ready(kernel_stream_src_lane_2_ready),
  .kernel_input_lane_2_valid(kernel_stream_src_lane_2_valid),
  .kernel_output_lane_2_data( kernel_stream_snk_lane_2_data),
  .kernel_output_lane_2_ready(kernel_stream_snk_lane_2_ready),
  .kernel_output_lane_2_valid(kernel_stream_snk_lane_2_valid),

  .kernel_input_lane_3_data( kernel_stream_src_lane_3_data),
  .kernel_input_lane_3_ready(kernel_stream_src_lane_3_ready),
  .kernel_input_lane_3_valid(kernel_stream_src_lane_3_valid),
  .kernel_output_lane_3_data( kernel_stream_snk_lane_3_data),
  .kernel_output_lane_3_ready(kernel_stream_snk_lane_3_ready),
  .kernel_output_lane_3_valid(kernel_stream_snk_lane_3_valid),

  .kernel_input_lane_4_data( kernel_stream_src_lane_4_data),
  .kernel_input_lane_4_ready(kernel_stream_src_lane_4_ready),
  .kernel_input_lane_4_valid(kernel_stream_src_lane_4_valid),
  .kernel_output_lane_4_data( kernel_stream_snk_lane_4_data),
  .kernel_output_lane_4_ready(kernel_stream_snk_lane_4_ready),
  .kernel_output_lane_4_valid(kernel_stream_snk_lane_4_valid),

  .kernel_input_lane_5_data( kernel_stream_src_lane_5_data),
  .kernel_input_lane_5_ready(kernel_stream_src_lane_5_ready),
  .kernel_input_lane_5_valid(kernel_stream_src_lane_5_valid),
  .kernel_output_lane_5_data( kernel_stream_snk_lane_5_data),
  .kernel_output_lane_5_ready(kernel_stream_snk_lane_5_ready),
  .kernel_output_lane_5_valid(kernel_stream_snk_lane_5_valid),

  .kernel_input_lane_6_data( kernel_stream_src_lane_6_data),
  .kernel_input_lane_6_ready(kernel_stream_src_lane_6_ready),
  .kernel_input_lane_6_valid(kernel_stream_src_lane_6_valid),
  .kernel_output_lane_6_data( kernel_stream_snk_lane_6_data),
  .kernel_output_lane_6_ready(kernel_stream_snk_lane_6_ready),
  .kernel_output_lane_6_valid(kernel_stream_snk_lane_6_valid),

  .kernel_input_lane_7_data( kernel_stream_src_lane_7_data),
  .kernel_input_lane_7_ready(kernel_stream_src_lane_7_ready),
  .kernel_input_lane_7_valid(kernel_stream_src_lane_7_valid),
  .kernel_output_lane_7_data( kernel_stream_snk_lane_7_data),
  .kernel_output_lane_7_ready(kernel_stream_snk_lane_7_ready),
  .kernel_output_lane_7_valid(kernel_stream_snk_lane_7_valid),

  .kernel_output_rx_monitor_0_data( kernel_stream_snk_rx_monitor_0_data),
  .kernel_output_rx_monitor_0_ready(kernel_stream_snk_rx_monitor_0_ready),
  .kernel_output_rx_monitor_0_valid(kernel_stream_snk_rx_monitor_0_valid),
  .kernel_output_tx_monitor_0_data( kernel_stream_snk_tx_monitor_0_data),
  .kernel_output_tx_monitor_0_ready(kernel_stream_snk_tx_monitor_0_ready),
  .kernel_output_tx_monitor_0_valid(kernel_stream_snk_tx_monitor_0_valid),

  .kernel_output_rx_monitor_1_data( kernel_stream_snk_rx_monitor_1_data),
  .kernel_output_rx_monitor_1_ready(kernel_stream_snk_rx_monitor_1_ready),
  .kernel_output_rx_monitor_1_valid(kernel_stream_snk_rx_monitor_1_valid),
  .kernel_output_tx_monitor_1_data( kernel_stream_snk_tx_monitor_1_data),
  .kernel_output_tx_monitor_1_ready(kernel_stream_snk_tx_monitor_1_ready),
  .kernel_output_tx_monitor_1_valid(kernel_stream_snk_tx_monitor_1_valid),

  .kernel_output_rx_monitor_2_data( kernel_stream_snk_rx_monitor_2_data),
  .kernel_output_rx_monitor_2_ready(kernel_stream_snk_rx_monitor_2_ready),
  .kernel_output_rx_monitor_2_valid(kernel_stream_snk_rx_monitor_2_valid),
  .kernel_output_tx_monitor_2_data( kernel_stream_snk_tx_monitor_2_data),
  .kernel_output_tx_monitor_2_ready(kernel_stream_snk_tx_monitor_2_ready),
  .kernel_output_tx_monitor_2_valid(kernel_stream_snk_tx_monitor_2_valid),

  .kernel_output_rx_monitor_3_data( kernel_stream_snk_rx_monitor_3_data),
  .kernel_output_rx_monitor_3_ready(kernel_stream_snk_rx_monitor_3_ready),
  .kernel_output_rx_monitor_3_valid(kernel_stream_snk_rx_monitor_3_valid),
  .kernel_output_tx_monitor_3_data( kernel_stream_snk_tx_monitor_3_data),
  .kernel_output_tx_monitor_3_ready(kernel_stream_snk_tx_monitor_3_ready),
  .kernel_output_tx_monitor_3_valid(kernel_stream_snk_tx_monitor_3_valid),

  .kernel_output_rx_monitor_4_data( kernel_stream_snk_rx_monitor_4_data),
  .kernel_output_rx_monitor_4_ready(kernel_stream_snk_rx_monitor_4_ready),
  .kernel_output_rx_monitor_4_valid(kernel_stream_snk_rx_monitor_4_valid),
  .kernel_output_tx_monitor_4_data( kernel_stream_snk_tx_monitor_4_data),
  .kernel_output_tx_monitor_4_ready(kernel_stream_snk_tx_monitor_4_ready),
  .kernel_output_tx_monitor_4_valid(kernel_stream_snk_tx_monitor_4_valid),

  .kernel_output_rx_monitor_5_data( kernel_stream_snk_rx_monitor_5_data),
  .kernel_output_rx_monitor_5_ready(kernel_stream_snk_rx_monitor_5_ready),
  .kernel_output_rx_monitor_5_valid(kernel_stream_snk_rx_monitor_5_valid),
  .kernel_output_tx_monitor_5_data( kernel_stream_snk_tx_monitor_5_data),
  .kernel_output_tx_monitor_5_ready(kernel_stream_snk_tx_monitor_5_ready),
  .kernel_output_tx_monitor_5_valid(kernel_stream_snk_tx_monitor_5_valid),

  .kernel_output_rx_monitor_6_data( kernel_stream_snk_rx_monitor_6_data),
  .kernel_output_rx_monitor_6_ready(kernel_stream_snk_rx_monitor_6_ready),
  .kernel_output_rx_monitor_6_valid(kernel_stream_snk_rx_monitor_6_valid),
  .kernel_output_tx_monitor_6_data( kernel_stream_snk_tx_monitor_6_data),
  .kernel_output_tx_monitor_6_ready(kernel_stream_snk_tx_monitor_6_ready),
  .kernel_output_tx_monitor_6_valid(kernel_stream_snk_tx_monitor_6_valid),

  .kernel_output_rx_monitor_7_data( kernel_stream_snk_rx_monitor_7_data),
  .kernel_output_rx_monitor_7_ready(kernel_stream_snk_rx_monitor_7_ready),
  .kernel_output_rx_monitor_7_valid(kernel_stream_snk_rx_monitor_7_valid),
  .kernel_output_tx_monitor_7_data( kernel_stream_snk_tx_monitor_7_data),
  .kernel_output_tx_monitor_7_ready(kernel_stream_snk_tx_monitor_7_ready),
  .kernel_output_tx_monitor_7_valid(kernel_stream_snk_tx_monitor_7_valid),


  .kernel_input_bs_sosi_data( kernel_stream_src_bs_data),
  .kernel_input_bs_sosi_ready(kernel_stream_src_bs_ready),
  .kernel_input_bs_sosi_valid(kernel_stream_src_bs_valid),

  .kernel_input_mm_data(kernel_stream_src_mm_io_data),
  .kernel_input_mm_ready(kernel_stream_src_mm_io_ready),
  .kernel_input_mm_valid(kernel_stream_src_mm_io_valid),

  .kernel_output_mm_data(kernel_stream_snk_mm_io_data),
  .kernel_output_mm_ready(kernel_stream_snk_mm_io_ready),
  .kernel_output_mm_valid(kernel_stream_snk_mm_io_valid)

);

endmodule
