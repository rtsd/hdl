-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_sc is
end tb_tb_dp_fifo_sc;

architecture tb of tb_tb_dp_fifo_sc is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 5
  -- > run -a --> OK

  -- -- Try FIFO settings
  -- g_dut_use_lut        : BOOLEAN := FALSE;  -- when TRUE then force using LUTs instead of block RAM
  -- g_dut_fifo_size      : NATURAL := 64;
  -- g_dut_fifo_af_margin : NATURAL := 4;     -- >=4, Nof words below max (full) at which fifo is considered almost full
  -- g_dut_use_bsn        : BOOLEAN := TRUE;
  -- g_dut_use_empty      : BOOLEAN := TRUE;
  -- g_dut_use_channel    : BOOLEAN := TRUE;
  -- g_dut_use_sync       : BOOLEAN := TRUE;
  -- g_dut_use_ctrl       : BOOLEAN := TRUE;
  -- g_dut_out_latency    : NATURAL := 0       -- selectable for dp_fifo_sc: default 1 or 0 for look ahead FIFO

  u_ram_use_all_rl_0          : entity work.tb_dp_fifo_sc generic map (false, 64, 4, true,  true,  true,  true,  true, 0);
  u_ram_use_all               : entity work.tb_dp_fifo_sc generic map (false, 64, 4, true,  true,  true,  true,  true, 1);

  u_ram_use_ctrl_rl_0         : entity work.tb_dp_fifo_sc generic map (false, 16, 4, false, false, false, false,  true, 0);
  u_ram_use_ctrl              : entity work.tb_dp_fifo_sc generic map (false, 16, 4, false, false, false, false,  true, 1);

  u_lut_use_ctrl_rl_0         : entity work.tb_dp_fifo_sc generic map ( true, 16, 4, false, false, false, false,  true, 0);
  u_lut_use_ctrl              : entity work.tb_dp_fifo_sc generic map ( true, 16, 4, false, false, false, false,  true, 1);

  u_lut_small_use_ctrl_rl_0   : entity work.tb_dp_fifo_sc generic map ( true,  8, 4, false, false, false, false,  true, 0);
  u_lut_small_use_ctrl        : entity work.tb_dp_fifo_sc generic map ( true,  8, 4, false, false, false, false,  true, 1);

  u_ram_no_bsn                : entity work.tb_dp_fifo_sc generic map (false, 64, 4, false,  true,  true,  true,  true, 1);
  u_ram_no_empty              : entity work.tb_dp_fifo_sc generic map (false, 64, 4,  true, false,  true,  true,  true, 1);
  u_ram_no_channel            : entity work.tb_dp_fifo_sc generic map (false, 64, 4,  true,  true, false,  true,  true, 1);
  u_ram_no_sync               : entity work.tb_dp_fifo_sc generic map (false, 64, 4,  true,  true,  true, false,  true, 1);
  u_ram_no_ctrl               : entity work.tb_dp_fifo_sc generic map (false, 64, 4,  true,  true,  true,  true, false, 1);
end tb;
