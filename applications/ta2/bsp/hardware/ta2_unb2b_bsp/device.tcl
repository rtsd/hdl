# (C) 1992-2018 Intel Corporation.                            
# Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
# and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
# and/or other countries. Other marks and brands may be claimed as the property  
# of others. See Trademarks on intel.com for full list of Intel trademarks or    
# the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
# Your use of Intel Corporation's design tools, logic functions and other        
# software and tools, and its AMPP partner logic functions, and any output       
# files any of the foregoing (including device programming or simulation         
# files), and any associated documentation or information are expressly subject  
# to the terms and conditions of the Altera Program License Subscription         
# Agreement, Intel MegaCore Function License Agreement, or other applicable      
# license agreement, including, without limitation, that your use is for the     
# sole purpose of programming logic devices manufactured by Intel and sold by    
# Intel or its authorized distributors.  Please refer to the applicable          
# agreement for further details.                                                 

# This file contains .qsf settings that are unique to this particular device

#############################################################
# Device
#############################################################
set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name DEVICE 10AX115U2F45E1SG
set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "1.8 V"
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
#set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
#set_global_assignment -name DEVICE_FILTER_SPEED_GRADE FASTEST
set_global_assignment -name DEVICE_FILTER_SPEED_GRADE ANY
#set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 4
set_global_assignment -name ENABLE_OCT_DONE OFF
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
set_global_assignment -name ENABLE_NCE_PIN OFF
set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X4"
#set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X1"
set_global_assignment -name STRATIXII_CONFIGURATION_DEVICE EPCQL1024
set_global_assignment -name USE_CONFIGURATION_DEVICE ON
set_global_assignment -name STRATIXIII_UPDATE_MODE REMOTE

set_global_assignment -name CRC_ERROR_OPEN_DRAIN ON
#set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
#set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
#set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
#set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall

set_global_assignment -name CONFIGURATION_VCCIO_LEVEL 1.8V
set_global_assignment -name ON_CHIP_BITSTREAM_DECOMPRESSION ON
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_12_5MHZ
set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_25MHZ
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_100MHZ

set_global_assignment -name USER_START_UP_CLOCK OFF

set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
set_global_assignment -name DEVICE_FILTER_PIN_COUNT 1932


