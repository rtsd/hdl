--------------------------------------------------------------------------------
-- Title       : Append CRC to packet
-- Project     : Square Kilometer Array
--------------------------------------------------------------------------------
-- File        : append_crc.vhd
-- Author      : William Kamp <william.kamp@aut.ac.nz
-- Company     : High Performance Computing Research Lab, AUT
-- Created     : Fri Oct 11 14:34:07 2019
-- Last update : Thu Oct 31 08:32:00 2019
-- Platform    :
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2019 High Performance Computing Research Lab, AUT
-------------------------------------------------------------------------------
-- Description: Append a CRC32 to the end of packets in the AXI4 Stream
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.AXI4_pkg.keep_to_empty;
use work.crc_pkg.all;
use work.misc_tools_pkg.all; -- bit_swap

use work.bit_width_config_pkg.all;

entity append_crc is

    generic (
        g_AREA_SPEED_TRADEOFF_FACTOR     : natural range 1 to 32  := 1;                    -- defines the parallelisation of the implementation. Larger  =  more area,  but more speed.
        g_INIT_CRC                       : std_logic_vector       := (31 downto 0 => '1'); -- Initial value of the CRC calc.
        g_BIT_REVERSE_IN                 : boolean                := true;
        g_BIT_REVERSE_OUT                : boolean                := true;
        g_XOR_CRC_OUT                    : std_logic_vector       := (31 downto 0 => '1');
        g_NAND_MASK                      : std_logic_vector       := ""; -- Bits set to '1' will be cleared to '0' for the CRC calulation.
        g_OR_MASK                        : std_logic_vector       := ""; -- Bits set to '1' will be set to '1' for the CRC calulation.
        g_INVALID_CYCLES_BETWEEN_PACKETS : natural                := 0   -- guaranteed number of cycles between packets.
    );

    port (
        i_clk       : in std_logic;
        i_clk_reset : in std_logic;

        i_stream      : in  AXI4_STREAMING_RDMA_t;
        o_stream_stop : out std_logic;
        o_stream      : out AXI4_STREAMING_RDMA_t;
        i_stream_stop : in  std_logic
    );

end entity append_crc;

architecture rtl of append_crc is

    function f_padded_mask (
            constant mask     : std_logic_vector;
            constant mask_len : natural
        )
        return std_logic_vector is
        constant top         : integer := mask_len - 1;
        constant bot         : integer := top - mask'length;
        variable padded_mask : std_logic_vector(top downto 0);
    begin
        padded_mask(top downto bot + 1) := mask;
        padded_mask(bot downto 0)       := (others => '0');
        return padded_mask;
    end function f_padded_mask;

    constant c_MASK_LEN    : natural          := round_up(maximum(g_OR_MASK'length, g_NAND_MASK'length), i_stream.data'length);
    constant c_OR_MASK     : std_logic_vector := f_padded_mask(g_OR_MASK, c_MASK_LEN);
    constant c_NAND_MASK   : std_logic_vector := f_padded_mask(g_NAND_MASK, c_MASK_LEN);
    constant c_MASK_CYCLES : natural          := c_MASK_LEN/i_stream.data'length;

    function f_CRC_MASK_DELAY return natural is
    begin
        if c_MASK_CYCLES > 0 then
            return 4;
        else
            return 3;
        end if;
    end function f_CRC_MASK_DELAY;
    signal masked_stream : AXI4_Streaming_RDMA_t;

    signal crc_vld : std_logic;
    signal crc     : std_logic_vector(31 downto 0);

    signal stream_delayed     : AXI4_Streaming_RDMA_t;
    signal stream_delayed_slv : AXI4_Streaming_RDMA_t_slv;

    signal crc_save   : std_logic_vector(crc'range);
    signal crc_keep   : std_logic_vector(crc'length/8-1 downto 0);
    signal append_crc : std_logic;
    signal error_save : std_logic;

begin

    G_MASK : if c_MASK_CYCLES > 0 generate
        signal mask_cycle_cnt : unsigned(ceil_log2(c_MASK_CYCLES) downto 0);
    begin

        P_MASK_VARIANT_HEADER_FIELDS : process (i_clk)
            variable v_fb  : unsigned(mask_cycle_cnt'range);
            variable v_dec : unsigned(mask_cycle_cnt'range);
            variable v_cyc : natural range 0 to c_MASK_CYCLES -1;
            variable v_validated_data : std_logic_vector(i_stream.data'range);
        begin
            if rising_edge(i_clk) then
                if (not mask_cycle_cnt(mask_cycle_cnt'high)) and i_stream.valid and not i_stream.last then
                    v_dec := to_unsigned(1, v_dec'length);
                else
                    v_dec := to_unsigned(0, v_dec'length);
                end if;
                if i_stream.last and i_stream.valid then
                    v_fb := to_unsigned(c_MASK_CYCLES - 1, v_fb'length);
                else
                    v_fb := mask_cycle_cnt;
                end if;
                mask_cycle_cnt <= v_fb - v_dec;

                for byte in i_stream.keep'range loop
                    v_validated_data(byte*8+7 downto byte*8+0) := i_stream.data(byte*8+7 downto byte*8+0) and i_stream.keep(byte);
                end loop;

                masked_stream <= i_stream;
                if mask_cycle_cnt(mask_cycle_cnt'high) then
                    -- Payload
                    masked_stream.data <= v_validated_data;
                else
                    v_cyc := to_integer(mask_cycle_cnt);
                    -- Headers, some fields get masked off (replaced with '1's or '0's).
                    masked_stream.data <= (v_validated_data
                            or c_OR_MASK(v_cyc*i_stream.data'length + i_stream.data'high downto v_cyc*i_stream.data'length + 0))
                        and (not c_NAND_MASK(v_cyc*i_stream.data'length + i_stream.data'high downto v_cyc*i_stream.data'length + 0));
                end if;
                if i_clk_reset then
                    mask_cycle_cnt <= to_unsigned(c_MASK_CYCLES - 1, v_fb'length);
                end if;
            end if;
        end process;
    end generate G_MASK;

    G_NO_MASK : if c_MASK_CYCLES <= 0 generate
        masked_stream <= i_stream;
    end generate;

    E_CRC : entity work.crc_generator
        generic map (
            g_AREA_SPEED_TRADEOFF_FACTOR     => g_AREA_SPEED_TRADEOFF_FACTOR,
            g_INIT_CRC                       => g_INIT_CRC,
            g_BIT_REVERSE_IN                 => g_BIT_REVERSE_IN,
            g_BIT_REVERSE_OUT                => g_BIT_REVERSE_OUT,
            g_XOR_CRC_OUT                    => g_XOR_CRC_OUT,
            g_INVALID_CYCLES_BETWEEN_PACKETS => g_INVALID_CYCLES_BETWEEN_PACKETS
        )
        port map (
            i_clk       => i_clk,
            i_clk_reset => i_clk_reset,
            i_stream    => masked_stream,
            o_stop      => o_stream_stop,
            o_crc       => crc,
            o_crc_vld   => crc_vld,
            i_stop2     => i_stream_stop
        );

    -- delay to match that of E_CRC and masking.
    E_DELAY_CRC : entity work.pipeline_delay_ram
        generic map (
            g_CYCLES_DELAY     => f_CRC_MASK_DELAY,
            g_FORCE_USE_FLOPS  => false,
            g_FLOPS_BEFORE_RAM => 1,
            g_FLOPS_AFTER_RAM  => 1
        )
        port map (
            i_clk => i_clk,
            i_bus => to_slv(i_stream),
            o_bus => stream_delayed_slv
        );

    stream_delayed <= from_slv(stream_delayed_slv);

    P_APPEND_CRC : process (i_clk)
        variable v_crc_rotate  : std_logic_vector(ceil_log2(crc'length/8)-1 downto 0);
        variable v_rotated_crc : unsigned(crc'range);
    begin
        if rising_edge(i_clk) then
            if stream_delayed.valid then
                assert stream_delayed.last = crc_vld
                    report "CRC output and end of packet are not aligned. Try adjusting the E_DELAY_CRC.g_CYCLES_DELAY generic."
                    severity error;
            end if;
            o_stream <= stream_delayed;

            -- Pre-rotate the crc value so the first byte of the crc will end up in the byte position after the last data byte.
            v_crc_rotate  := keep_to_empty(to_01(stream_delayed.keep))(1 downto 0);
            v_rotated_crc := rotate_left(unsigned(crc), 8*to_integer(unsigned(v_crc_rotate)));

            for slice in stream_delayed.data'length/crc'length-1 downto 0 loop
                -- Insert the CRC everywhere there are available bytes. Will select the correct one with valid and keep.
                -- Should only really be at the last cycle of a packet (also inserted into invalid cycles, including the one after the packet).
                for byte in crc'length/8-1 downto 0 loop
                    if not stream_delayed.keep(slice*4+byte) then
                        o_stream.data(slice*32+byte*8+7 downto slice*32+byte*8) <=
                            std_logic_vector(v_rotated_crc(byte*8+7 downto byte*8));
                    end if;
                end loop;
            end loop;

            append_crc <= '0';
            crc_keep   <= (others => '1');
            crc_save   <= std_logic_vector(v_rotated_crc);
            error_save <= stream_delayed.error;
            if stream_delayed.last and stream_delayed.valid then
                if stream_delayed.keep(crc'length/8-1) then
                    -- Was not enough room in the current cycle.
                    -- Some or all of the CRC must be inserted on the next cycle.
                    o_stream.last <= '0';
                    append_crc    <= '1';
                    crc_keep      <= stream_delayed.keep(crc_keep'range);
                end if;
            end if;
            -- Wherever the crc ended up, rotate the keep to validate the first instance of the crc.
            o_stream.keep <= crc_keep & stream_delayed.keep(stream_delayed.keep'high downto crc_keep'length);

            if append_crc then
                assert stream_delayed.valid = '0'
                    report "Wanted to insert the CRC into this cycle but it was not invalid." &
                    " Expecting an invalid cycle between all incoming packets."
                    severity error;
                o_stream.last                                                              <= '1';
                o_stream.valid                                                             <= '1';
                o_stream.data                                                              <= (others => '-');
                o_stream.data(o_stream.data'high downto o_stream.data'length - crc'length) <= crc_save;
                o_stream.keep                                                              <= crc_keep & (stream_delayed.keep'high downto crc_keep'length => '0');
                o_stream.error                                                             <= error_save;
            end if;
        end if;
    end process;

end architecture rtl;
