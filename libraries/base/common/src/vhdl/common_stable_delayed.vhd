-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose:
--   Output active r_in if it is still active after some delay.
-- Description:
--   This function can be used to filter out temporary toggling in r_in. The
--   r_stable only becomes active after r_in has remained active for
--   2**g_delayed_w - 2**g_delayed_lo clk cycles. The r_stable always goes
--   inactive when r_in is inactive.
--   The active level can be set with g_active_level to '1' or '0'.
-- Remarks:
-- . Verified with tb_lvdsh_dd_phs4.vhd, because there this was used first.
-- . Instead of the combination of g_delayed_w and g_delayed_lo a single
--   g_delayed NATURAL could have been used to exactly specify the delay.
--   However then delays larger than 2**31 would be complicated due to the
--   limited 31 bit range of a NATURAL in VHDL.

entity common_stable_delayed is
  generic (
    g_active_level : std_logic := '1';
    g_delayed_w    : natural := 8;
    g_delayed_lo   : natural := 7  -- choose <= g_delayed_hi = g_delayed_w-1
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- MM
    r_in         : in  std_logic;
    r_stable     : out std_logic
  );
end common_stable_delayed;

architecture rtl of common_stable_delayed is
  signal p_in      : std_logic;
  signal p_stable  : std_logic;

  signal cnt_clr   : std_logic;
  signal cnt_en    : std_logic;
  signal cnt       : std_logic_vector(g_delayed_w - 1 downto 0);
begin
  -- Map r to internal p, to be able to internally operate with active level is '1'
  p_in     <= r_in     when g_active_level = '1' else not r_in;
  r_stable <= p_stable when g_active_level = '1' else not p_stable;

  cnt_clr <= not p_in;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      cnt_en   <= '0';
      p_stable <= '0';
    elsif rising_edge(clk) then
      cnt_en   <= not vector_and(cnt(g_delayed_w - 1 downto g_delayed_lo));
      p_stable <=     vector_and(cnt(g_delayed_w - 1 downto g_delayed_lo));
    end if;
  end process;

  u_common_counter : entity common_lib.common_counter
  generic map (
    g_width  => g_delayed_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );
end rtl;
