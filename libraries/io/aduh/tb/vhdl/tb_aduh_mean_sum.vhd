-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

-- Usage:
-- > as 10
-- > run 1 us
-- observe in_sosi, sum and sum_sync in wave window
-- expected sum at sum_sync are: 0, 120, 376, ...
-- . sum([ 0:15]) = 120
-- . sum([16:31]) = 376
-- . sum([32:47]) = 632

entity tb_aduh_mean_sum is
  generic (
    g_nof_symbols_per_data  : natural := 1;  -- choose 1 or 4, nof symbols (= ADC samples) per data word
    g_random_control        : boolean := true  -- use TRUE for random stream flow control, use FALSE for initial debugging
  );
end tb_aduh_mean_sum;

architecture tb of tb_aduh_mean_sum is
  constant clk_period              : time := 10 ns;

  constant c_rl                    : natural := 1;
  constant c_symbol_w              : natural := 8;

  constant c_sum_truncate          : boolean := true;
  constant c_sum_w                 : natural := c_word_w;  -- = 32

  constant c_data_w                : natural := g_nof_symbols_per_data * c_symbol_w;

  constant c_nof_sync              : natural := 10;
  constant c_nof_block_per_sync    : natural := 2;
  constant c_nof_symbols_per_block : natural := 8;  -- nof symbols (= ADC samples) per block
  constant c_nof_symbols_per_sync  : natural := c_nof_block_per_sync * c_nof_symbols_per_block;

  constant c_nof_accumulations     : natural := c_nof_symbols_per_sync;  -- integration time in symbols

  constant c_exp_sum_arr           : t_natural_arr(0 to 3) := (0, 120, 376, 632);

  signal tb_almost_end   : std_logic := '0';
  signal tb_end          : std_logic := '0';
  signal rst             : std_logic;
  signal clk             : std_logic := '1';

  signal random_0        : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences

  signal st_en           : std_logic := '1';
  signal st_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal st_siso         : t_dp_siso := c_dp_siso_rdy;

  signal bsn             : natural;

  signal in_sosi         : t_dp_sosi := c_dp_sosi_rst;

  signal sum             : std_logic_vector(c_sum_w - 1 downto 0);
  signal sum_sync        : std_logic;

  signal verify_done     : std_logic := '0';
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 5;

  -- ST domain
  gen_random_control : if g_random_control = true generate
    random_0 <= func_common_random(random_0) when rising_edge(clk);
    --st_en    <= NOT st_en WHEN rising_edge(clk);
    --st_en    <= random_0(random_0'HIGH);
  end generate;  -- else the st_en line is always active

  p_st_stimuli : process
    variable v_symbol  : natural := 0;
  begin
    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Run some sync intervals with DSP counter data for the data field
    for I in 0 to c_nof_sync - 1 loop
      -- Generate st_sosi with sync at sop
      proc_dp_gen_block_data(c_rl, true, c_data_w, c_symbol_w, v_symbol, 0, 0, c_nof_symbols_per_block, 0, 0, '1', "0", clk, st_en, st_siso, st_sosi);  -- next sync
      v_symbol := v_symbol + c_nof_symbols_per_block;
      for J in 0 to c_nof_block_per_sync - 2 loop
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_symbol_w, v_symbol, 0, 0, c_nof_symbols_per_block, 0, 0, '0', "0", clk, st_en, st_siso, st_sosi);  -- no sync
        v_symbol := v_symbol + c_nof_symbols_per_block;
      end loop;
    end loop;

    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(clk, 10);
    proc_common_gen_pulse(clk, tb_almost_end);
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- Time stimuli
  bsn <= bsn + 1 when rising_edge(clk) and st_sosi.eop = '1';

  -- Add BSN to the ST data
  p_in_sosi : process(st_sosi, bsn)
  begin
    in_sosi      <= st_sosi;  -- sync at sop, data
    in_sosi.bsn  <= TO_DP_BSN(bsn);  -- bsn at sop
  end process;

  u_dut : entity work.aduh_mean_sum
  generic map (
    g_symbol_w             => c_symbol_w,
    g_nof_symbols_per_data => g_nof_symbols_per_data,  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    => c_nof_accumulations,  -- integration time in symbols
    g_sum_truncate         => c_sum_truncate,  -- when TRUE truncate (keep MS part) else resize (keep sign and LS part)
    g_sum_w                => c_sum_w  -- typcially MM word width = 32
  )
  port map (
    clk         => clk,
    rst         => rst,

    -- Streaming inputs
    in_data     => in_sosi.data(c_data_w - 1 downto 0),
    in_val      => in_sosi.valid,
    in_sync     => in_sosi.sync,

    -- Accumulation outputs
    sum         => sum,
    sum_sync    => sum_sync
  );

  -- Only verify sum for a few sum_sync
  p_verify_sum : process
    variable vI : natural := 0;
  begin
    while vI < c_exp_sum_arr'length loop
      wait until rising_edge(clk);
      if sum_sync = '1' then
        assert sum = TO_SVEC(c_exp_sum_arr(vI), c_sum_w)
          report "Unexpected voltage sum."
          severity ERROR;
        vI := vI + 1;
      end if;
    end loop;
    verify_done <= '1';
    wait;
  end process;

  -- Verify that the tb has run with active data
  p_verify_done : process
  begin
    proc_common_wait_until_high(clk, verify_done);
    assert verify_done = '1'
      report "No sum output"
      severity ERROR;
    wait;
  end process;
end tb;
