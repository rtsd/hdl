library IEEE, common_lib, pft2_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

architecture str of pft_top is
  signal reg_in_re    : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_in_im    : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_in_val   : std_logic;
  signal reg_in_sync  : std_logic;
  signal d_out_re     : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal d_out_im     : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal d_out_val    : std_logic;
  signal d_out_sync   : std_logic;

  signal pfft_in_sync : std_logic;
  signal pfft_in_re   : std_logic_vector(17 downto 0);
  signal pfft_in_im   : std_logic_vector(17 downto 0);
  signal pfft_out_re  : std_logic_vector(17 downto 0);
  signal pfft_out_im  : std_logic_vector(17 downto 0);
begin
  registers : process(clk)
  begin
    if rising_edge(clk) then
      reg_in_re     <= in_re;
      reg_in_im     <= in_im;
      reg_in_val    <= in_val;
      reg_in_sync   <= in_sync;
      out_re        <= d_out_re;
      out_im        <= d_out_im;
      out_val       <= d_out_val;
      out_sync      <= d_out_sync;
    end if;
  end process;

  u_pft : entity pft2_lib.pft
  generic map (
    g_fft_size_w => g_fft_size_w,
    g_in_dat_w   => g_in_dat_w,
    g_out_dat_w  => g_out_dat_w,
    g_mode       => g_mode
  )
  port map (
    in_re        => reg_in_re,
    in_im        => reg_in_im,
    in_val       => reg_in_val,
    in_sync      => reg_in_sync,
    switch_en    => switch_en,
    out_re       => d_out_re,
    out_im       => d_out_im,
    out_val      => d_out_val,
    out_sync     => d_out_sync,
    clk          => clk,
    rst          => rst
  );
end str;
