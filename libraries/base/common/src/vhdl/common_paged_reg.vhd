-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi page register
-- Description:
--   The input wr_dat is written to the first data page. The output out_dat is
--   read from the last data page. The wr_en vector determines when the data
--   page is passed on to the next data page.
-- Remarks:

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_paged_reg is
  generic (
    g_data_w     : natural := 8;
    g_nof_pages  : natural := 2  -- >= 0
  );
  port (
    rst          : in  std_logic := '0';
    clk          : in  std_logic;
    wr_en        : in  std_logic_vector(g_nof_pages - 1 downto 0);
    wr_dat       : in  std_logic_vector(g_data_w - 1 downto 0);
    out_dat      : out std_logic_vector(g_data_w - 1 downto 0)
  );
end common_paged_reg;

architecture str of common_paged_reg is
  type t_data is array (natural range <>) of std_logic_vector(out_dat'range);

  signal reg_dat  : t_data(g_nof_pages downto 0) := (others => (others => '0'));
begin
  -- Wire input to first page and last page to output
  reg_dat(g_nof_pages) <= wr_dat;
  out_dat              <= reg_dat(0);

  -- Shift the intermediate data pages when enabled
  gen_pages : for I in g_nof_pages - 1 downto 0 generate
    u_page : entity work.common_pipeline
    generic map (
      g_in_dat_w  => g_data_w,
      g_out_dat_w => g_data_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_en   => wr_en(I),
      in_dat  => reg_dat(I + 1),
      out_dat => reg_dat(I)
    );
  end generate;
end str;
