-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, unb2c_board_lib, dp_lib, eth_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use work.unb2c_test_pkg.all;
use technology_lib.technology_pkg.all;

entity udp_stream is
  generic (
    g_sim                       : boolean := false;
    g_technology                : natural := c_tech_arria10_e1sg;
    g_nof_streams               : natural;
    g_data_w                    : natural;

    g_bg_block_size             : natural := 900;
    g_bg_gapsize                : natural := 100;
    g_bg_blocks_per_sync        : natural := 200000;

    g_def_block_size            : natural := 0;
    g_max_nof_blocks_per_packet : natural := 0;
    g_remove_crc                : boolean
  );
  port (
    -- System
    mm_rst                         : in  std_logic;
    mm_clk                         : in  std_logic;

    dp_rst                         : in  std_logic;
    dp_clk                         : in  std_logic;

    ID                             : in  std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);

    -- blockgen mm
    reg_diag_bg_mosi               : in  t_mem_mosi := c_mem_mosi_rst;  -- BG control register (one for all streams)
    reg_diag_bg_miso               : out t_mem_miso;
    ram_diag_bg_mosi               : in  t_mem_mosi := c_mem_mosi_rst;  -- BG buffer RAM (one per stream)
    ram_diag_bg_miso               : out t_mem_miso;
    reg_diag_tx_seq_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_tx_seq_miso           : out t_mem_miso;

    -- dp_offload_tx
    --reg_dp_offload_tx_mosi         : IN  t_mem_mosi := c_mem_mosi_rst;
    --reg_dp_offload_tx_miso         : OUT t_mem_miso;
    --reg_dp_offload_tx_hdr_dat_mosi : IN  t_mem_mosi := c_mem_mosi_rst;
    --reg_dp_offload_tx_hdr_dat_miso : OUT t_mem_miso;

    -- to MAC
    dp_offload_tx_src_out_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    dp_offload_tx_src_in_arr       : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    -- dp_offload_rx
    --reg_dp_offload_rx_hdr_dat_mosi : IN  t_mem_mosi := c_mem_mosi_rst;
    --reg_dp_offload_rx_hdr_dat_miso : OUT t_mem_miso;

    -- from MAC
    dp_offload_rx_snk_in_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    dp_offload_rx_snk_out_arr      : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    -- bsn
    reg_bsn_monitor_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_monitor_miso           : out t_mem_miso;

    -- databuffer
    reg_diag_data_buf_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_data_buf_miso         : out t_mem_miso;
    ram_diag_data_buf_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_data_buf_miso         : out t_mem_miso;
    reg_diag_rx_seq_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_rx_seq_miso           : out t_mem_miso
  );
end udp_stream;

architecture str of udp_stream is
  -- Block generator
  constant c_bg_ctrl                   : t_diag_block_gen := ('0',  -- enable (disabled by default)
                                                              '0',  -- enable_sync
                                                              TO_UVEC(     g_bg_block_size, c_diag_bg_samples_per_packet_w),
                                                              TO_UVEC(g_bg_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                              TO_UVEC(        g_bg_gapsize, c_diag_bg_gapsize_w),
                                                              TO_UVEC(                   0, c_diag_bg_mem_low_adrs_w),
                                                              TO_UVEC(   g_bg_block_size-1, c_diag_bg_mem_high_adrs_w),
                                                              TO_UVEC(                   0, c_diag_bg_bsn_init_w));

  constant c_nof_crc_words             : natural := 1;
  constant c_max_nof_words_per_block   : natural := g_bg_block_size;
  constant c_min_nof_words_per_block   : natural := 1;
  constant c_def_nof_blocks_per_packet : natural := 1;

  signal block_gen_src_out_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal block_gen_src_in_arr        : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal fifo_block_gen_src_out_arr  : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal fifo_block_gen_src_in_arr   : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal dp_offload_rx_src_out_arr   : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_offload_rx_src_in_arr    : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal hdr_fields_in_arr           : t_slv_1024_arr(g_nof_streams - 1 downto 0);
  signal hdr_fields_out_arr          : t_slv_1024_arr(g_nof_streams - 1 downto 0);

  signal diag_data_buf_snk_in_arr    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal diag_data_buf_snk_out_arr   : t_dp_siso_arr(g_nof_streams - 1 downto 0);
begin
  gen_hdr_in_fields : for i in 0 to g_nof_streams - 1 generate
    -- dst = src
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "eth_src_mac" ) downto field_lo(c_hdr_field_arr, "eth_src_mac")) <= x"00228608" & B"000" & ID(7 downto 3) & RESIZE_UVEC(ID(2 downto 0), c_byte_w);
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "eth_dst_mac" ) downto field_lo(c_hdr_field_arr, "eth_dst_mac")) <= x"00228608" & B"000" & ID(7 downto 3) & RESIZE_UVEC(ID(2 downto 0), c_byte_w);

    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "ip_src_addr" ) downto field_lo(c_hdr_field_arr, "ip_src_addr")) <= x"0A0A" & B"000" & ID(7 downto 3) & INCR_UVEC(RESIZE_UVEC(ID(2 downto 0), c_byte_w), 1);
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "ip_dst_addr" ) downto field_lo(c_hdr_field_arr, "ip_dst_addr")) <= x"0A0A" & B"000" & ID(7 downto 3) & INCR_UVEC(RESIZE_UVEC(ID(2 downto 0), c_byte_w), 1);

    -- dst port goes through 4000,4001,4002
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "udp_src_port") downto field_lo(c_hdr_field_arr, "udp_src_port" )) <= TO_UVEC(4000 + i, 16);
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "udp_dst_port") downto field_lo(c_hdr_field_arr, "udp_dst_port" )) <= TO_UVEC(4000 + i, 16);

    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "usr_sync"    ) downto field_lo(c_hdr_field_arr, "usr_sync"   )) <= slv(fifo_block_gen_src_out_arr(i).sync);
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "usr_bsn"     ) downto field_lo(c_hdr_field_arr, "usr_bsn"    )) <= fifo_block_gen_src_out_arr(i).bsn(46 downto 0);
  end generate;

  -----------------------------------------------------------------------------
  -- TX: Block generator and DP fifo
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen: entity diag_lib.mms_diag_block_gen
  generic map (
    g_technology         => g_technology,
    g_nof_streams        => g_nof_streams,
    g_buf_dat_w          => g_data_w,
    g_buf_addr_w         => ceil_log2(TO_UINT(c_bg_ctrl.samples_per_packet)),
    g_file_index_arr     => array_init(0, g_nof_streams),
    g_file_name_prefix   => "hex/counter_data_" & natural'image(g_data_w),
    g_diag_block_gen_rst => c_bg_ctrl
--    g_use_tx_seq         => TRUE
  )
  port map (
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,

    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    out_sosi_arr     => block_gen_src_out_arr,
    out_siso_arr     => block_gen_src_in_arr,

    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => ram_diag_bg_miso,
    reg_tx_seq_mosi  => reg_diag_tx_seq_mosi,
    reg_tx_seq_miso  => reg_diag_tx_seq_miso
  );

  gen_dp_fifo_sc : for i in 0 to g_nof_streams - 1 generate  -- FIXME : Daniel: we also need this fifo to pass on the BSN (47b) and sync (1b); set generics accordingly
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
    generic map (
      g_technology => g_technology,
      g_data_w    => g_data_w,
      g_bsn_w     => 47,
      g_use_bsn   => true,
      g_use_sync  => true,
      g_fifo_size => 50
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      -- ST sink (from BG)
      snk_out     => block_gen_src_in_arr(i),
      snk_in      => block_gen_src_out_arr(i),
      -- ST source (to tx_offload)
      src_in      => fifo_block_gen_src_in_arr(i),
      src_out     => fifo_block_gen_src_out_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- TX: dp_offload_tx
  -----------------------------------------------------------------------------
  u_dp_offload_tx : entity dp_lib.dp_offload_tx
  generic map (
    g_technology                => g_technology,
    g_nof_streams               => g_nof_streams,
    g_data_w                    => g_data_w,
    g_use_complex               => false,
--    g_max_nof_words_per_block   => c_max_nof_words_per_block,
    g_nof_words_per_block       => g_def_block_size,
--    g_max_nof_blocks_per_packet => g_max_nof_blocks_per_packet,
    g_nof_blocks_per_packet     => c_def_nof_blocks_per_packet,
    g_hdr_field_arr             => c_hdr_field_arr,
    g_hdr_field_sel             => c_hdr_field_ovr_init
   )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    -- MM
    --reg_mosi              => reg_dp_offload_tx_mosi,
    --reg_miso              => reg_dp_offload_tx_miso,
    --reg_hdr_dat_mosi      => reg_dp_offload_tx_hdr_dat_mosi,
    --reg_hdr_dat_miso      => reg_dp_offload_tx_hdr_dat_miso,

    -- from blockgen-fifo
    snk_in_arr            => fifo_block_gen_src_out_arr(g_nof_streams - 1 downto 0),
    snk_out_arr           => fifo_block_gen_src_in_arr(g_nof_streams - 1 downto 0),

    -- output to MAC
    src_out_arr           => dp_offload_tx_src_out_arr(g_nof_streams - 1 downto 0),
    src_in_arr            => dp_offload_tx_src_in_arr(g_nof_streams - 1 downto 0),

    hdr_fields_in_arr     => hdr_fields_in_arr(g_nof_streams - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- RX: dp_offload_rx
  -----------------------------------------------------------------------------
  u_dp_offload_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => g_nof_streams,
    g_data_w              => g_data_w,
    g_hdr_field_arr       => c_hdr_field_arr,
    g_remove_crc          => g_remove_crc,
    g_crc_nof_words       => c_nof_crc_words
   )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    --reg_hdr_dat_mosi      => reg_dp_offload_rx_hdr_dat_mosi,
    --reg_hdr_dat_miso      => reg_dp_offload_rx_hdr_dat_miso,

    -- from MAC
    snk_in_arr            => dp_offload_rx_snk_in_arr,
    snk_out_arr           => dp_offload_rx_snk_out_arr,

    -- to databuffer
    src_out_arr           => dp_offload_rx_src_out_arr,
    src_in_arr            => dp_offload_rx_src_in_arr,

    hdr_fields_out_arr    => hdr_fields_out_arr
  );

  gen_hdr_out_fields : for i in 0 to g_nof_streams - 1 generate
    diag_data_buf_snk_in_arr(i).sync <=          sl(hdr_fields_out_arr(i)(field_hi(c_hdr_field_arr, "usr_sync") downto field_lo(c_hdr_field_arr, "usr_sync" )));
    diag_data_buf_snk_in_arr(i).bsn  <= RESIZE_UVEC(hdr_fields_out_arr(i)(field_hi(c_hdr_field_arr, "usr_bsn" ) downto field_lo(c_hdr_field_arr, "usr_bsn"  )), c_dp_stream_bsn_w);
  end generate;

  -----------------------------------------------------------------------------
  -- RX: Data buffers and BSN monitors
  -----------------------------------------------------------------------------
  dp_offload_rx_src_in_arr <= diag_data_buf_snk_out_arr(g_nof_streams - 1 downto 0);

  gen_bsn_mon_in : for i in 0 to g_nof_streams - 1 generate
    diag_data_buf_snk_in_arr(i).data  <= dp_offload_rx_src_out_arr(i).data;
    diag_data_buf_snk_in_arr(i).valid <= dp_offload_rx_src_out_arr(i).valid;
    diag_data_buf_snk_in_arr(i).sop   <= dp_offload_rx_src_out_arr(i).sop;
    diag_data_buf_snk_in_arr(i).eop   <= dp_offload_rx_src_out_arr(i).eop;
    diag_data_buf_snk_in_arr(i).err   <= dp_offload_rx_src_out_arr(i).err;
  end generate;

  u_dp_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => g_nof_streams,
    g_cross_clock_domain => true,
    g_sync_timeout       => g_bg_blocks_per_sync * (g_bg_block_size + g_bg_gapsize),
    g_cnt_sop_w          => ceil_log2(g_bg_blocks_per_sync + 1),
    g_cnt_valid_w        => ceil_log2(g_bg_blocks_per_sync * g_bg_block_size+1),
    g_log_first_bsn      => true
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_mosi,
    reg_miso    => reg_bsn_monitor_miso,

    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    in_siso_arr => diag_data_buf_snk_out_arr(g_nof_streams - 1 downto 0),
    in_sosi_arr => diag_data_buf_snk_in_arr(g_nof_streams - 1 downto 0)
  );

  diag_data_buf_snk_out_arr <= (others => c_dp_siso_rdy);

  u_diag_data_buffer : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_technology   => g_technology,
    g_nof_streams  => g_nof_streams,
    g_data_w       => 32,  -- g_data_w, --FIXME
    g_buf_nof_data => 1024,
    g_buf_use_sync => false,  -- sync by reading last address of data buffer
    g_use_rx_seq   => true
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,
    reg_rx_seq_mosi   => reg_diag_rx_seq_mosi,
    reg_rx_seq_miso   => reg_diag_rx_seq_miso,

    in_sync           => diag_data_buf_snk_in_arr(0).sync,
    in_sosi_arr       => diag_data_buf_snk_in_arr
  );
end str;
