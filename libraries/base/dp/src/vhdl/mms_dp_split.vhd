-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_split is
  generic (
    g_nof_streams     : natural := 1;
    g_data_w          : natural := 16;
    g_symbol_w        : natural := 8;
    g_nof_symbols_max : natural := 8  -- Sets maximum nof_symbols that can be selected
  );
  port (
    -- Memory-mapped clock domain
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;

    -- Streaming clock domain
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;

    -- ST sinks
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_2arr  : in  t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);
    src_out_2arr : out t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);

    out_nof_symbols : out t_natural_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_split;

architecture str of mms_dp_split is
  constant c_reg_adr_w : natural := 1;

  signal reg_mosi_arr  : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr  : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  type t_nof_symbols_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(ceil_log2(g_nof_symbols_max + 1) - 1 downto 0);

  signal nof_symbols   : t_nof_symbols_arr;
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    out_nof_symbols(i) <= TO_UINT(nof_symbols(i));

    u_reg : entity work.dp_split_reg
    generic map (
      g_nof_symbols => g_nof_symbols_max
    )
    port map (
      -- Clocks and reset
      mm_rst       => mm_rst,
      mm_clk       => mm_clk,
      st_rst       => dp_rst,
      st_clk       => dp_clk,

      -- Memory Mapped Slave in mm_clk domain
      sla_in       => reg_mosi_arr(i),
      sla_out      => reg_miso_arr(i),

      -- MM registers in dp_clk domain
      -- . control
      nof_symbols  => nof_symbols(i)
    );

    u_dp_split : entity work.dp_split
    generic map (
      g_data_w      => g_data_w,
      g_symbol_w    => g_symbol_w,
      g_nof_symbols => g_nof_symbols_max
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      nof_symbols => nof_symbols(i),
      snk_out     => snk_out_arr(i),
      snk_in      => snk_in_arr(i),
      src_in_arr  => src_in_2arr(i),
      src_out_arr => src_out_2arr(i)
    );
  end generate;
end str;
