--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   Multiplex frames from one or more input streams into one output stream.
-- Description:
--   The frames are marked by sop and eop. The input selection scheme depends
--   on g_mode:
--   0: Framed round-robin with fair chance.
--      Uses eop to select next input after the frame has been passed on or
--      select the next input when there is no frame coming in on the current
--      input, so it has had its chance.
--   1: Framed round-robin in forced order from each input.
--      Uses eop to select next output. Holds input selection until sop is
--      detected on that input. Results in ordered (low to high) but blocking
--      (on absence of sop) input selection.
--   2: Unframed external MM control input to select the output.
--      Three options have been considered for the flow control:
--      a) Use src_in for all inputs, data from the not selected inputs
--         will get lost. In case FIFOs are used they are only useful used for
--         the selected input.
--      b) Use c_dp_siso_rdy for unused inputs, this flushes them like with
--         option a) but possibly even faster in case the src_in.ready may get
--         inactive to apply backpressure.
--      c) Use c_dp_siso_hold for unused inputs, to stop them until they get
--         selected again.
--      Support only option a) because assume that the sel_ctrl is rather
--      static and the data from the unused inputs can be ignored.
--   3: Framed external sel_ctrl input to select the output.
--      This scheme is identical to g_mode=0, but with xon='1' only for the
--      selected input. The other not selected inputs have xon='0', so they
--      will stop getting input frames and the round-robin scheme of g_mode=0
--      will then automatically select only remaining active input.
--      The assumption is that the upstream input sources do stop their output
--      after they finished the current frame when xon='0'. If necessary
--      dp_xonoff could be used to add such frame flow control to an input
--      stream that does not yet support xon/xoff. But better use g_mode=4
--      instead of g_mode=3, because the implementation of g_mode=4 is more
--      simple.
--   4) Framed external sel_ctrl input to select the output without ready.
--      This is preferred over g_mode=3 because it passes on the ready but
--      does not use it self. Not selected inputs have xon='0'. Only the
--      selected input has xon='1'. When sel_ctrl changes then briefly all
--      inputs get xon='0'. The new selected input only gets xon='1' when
--      the current selected input is idle or has become idle.
--
--   The low part of the src_out.channel has c_sel_w = log2(g_nof_input) nof
--   bits and equals the input port number. The snk_in_arr().channel bits are
--   copied into the high part of the src_out.channel. Hence the total
--   effective output channel width becomes g_in_channel_w+c_sel_w when
--   g_use_in_channel=TRUE else c_sel_w.
--   If g_use_fifo=TRUE then the frames are buffered at the input, else the
--   connecting inputs need to take care of that.
-- Remark:
-- . Using g_nof_input=1 is transparent.
-- . Difference with dp_frame_scheduler is that dp_frame_scheduler does not
--   support back pressure via the ready signals.
-- . This dp_mux adds true_log2(nof ports) low bits to out_channel and the
--   dp_demux removes true_log2(nof ports) low bits from in_channel.
-- . For multiplexing time series frames or sample it can be applicable to
--   use g_append_channel_lo=FALSE in combination with g_mode=2.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_mux is
  generic (
    g_technology        : natural := c_tech_select_default;
    -- MUX
    g_mode              : natural := 0;
    g_nof_input         : natural := 2;  -- >= 1
    g_append_channel_lo : boolean := true;
    g_sel_ctrl_invert   : boolean := false;  -- Use default FALSE when stream array IO are indexed (0 TO g_nof_input-1), else use TRUE when indexed (g_nof_input-1 DOWNTO 0)
    -- Input FIFO
    g_use_fifo          : boolean := false;
    g_bsn_w             : natural := 16;
    g_data_w            : natural := 16;
    g_empty_w           : natural := 1;
    g_in_channel_w      : natural := 1;
    g_error_w           : natural := 1;
    g_use_bsn           : boolean := false;
    g_use_empty         : boolean := false;
    g_use_in_channel    : boolean := false;
    g_use_error         : boolean := false;
    g_use_sync          : boolean := false;
    g_fifo_af_margin    : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon       : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_size         : t_natural_arr := array_init(1024, 2);  -- must match g_nof_input, even when g_use_fifo=FALSE
    g_fifo_fill         : t_natural_arr := array_init(   0, 2)  -- must match g_nof_input, even when g_use_fifo=FALSE
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- Control
    sel_ctrl    : in  natural range 0 to g_nof_input - 1 := 0;  -- used by g_mode = 2, 3, 4
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(0 to g_nof_input - 1);
    snk_in_arr  : in  t_dp_sosi_arr(0 to g_nof_input - 1);
    -- ST source
    src_in      : in  t_dp_siso;
    src_out     : out t_dp_sosi
  );
end dp_mux;

architecture rtl of dp_mux is
  -- Convert unconstrained range (that starts at INTEGER'LEFT) to 0 TO g_nof_input-1 range
  constant c_fifo_fill  : t_natural_arr(0 to g_nof_input - 1) := g_fifo_fill;
  constant c_fifo_size  : t_natural_arr(0 to g_nof_input - 1) := g_fifo_size;

  -- The low part of src_out.channel is used to represent the input port and the high part of src_out.channel is copied from snk_in_arr().channel
  constant c_sel_w      : natural := true_log2(g_nof_input);

  constant c_rl         : natural := 1;
  signal tb_ready_reg   : std_logic_vector(0 to g_nof_input * (1 + c_rl) - 1);

  type state_type is (s_idle, s_output);

  signal state            : state_type;
  signal nxt_state        : state_type;

  signal i_snk_out_arr    : t_dp_siso_arr(0 to g_nof_input - 1);

  signal sel_ctrl_reg     : natural range 0 to g_nof_input - 1;
  signal nxt_sel_ctrl_reg : natural;
  signal sel_ctrl_evt     : std_logic;
  signal nxt_sel_ctrl_evt : std_logic;

  signal in_sel           : natural range 0 to g_nof_input - 1;  -- input port low part of src_out.channel
  signal nxt_in_sel       : natural;
  signal next_sel         : natural;

  signal rd_siso_arr      : t_dp_siso_arr(0 to g_nof_input - 1);
  signal rd_sosi_arr      : t_dp_sosi_arr(0 to g_nof_input - 1);
  signal rd_sosi_busy_arr : std_logic_vector(0 to g_nof_input - 1);

  signal hold_src_in_arr  : t_dp_siso_arr(0 to g_nof_input - 1);
  signal next_src_out_arr : t_dp_sosi_arr(0 to g_nof_input - 1);
  signal pend_src_out_arr : t_dp_sosi_arr(0 to g_nof_input - 1);  -- SOSI control

  signal in_xon_arr       : std_logic_vector(0 to g_nof_input - 1);
  signal nxt_in_xon_arr   : std_logic_vector(0 to g_nof_input - 1);

  signal prev_src_in      : t_dp_siso;
  signal src_out_hi       : t_dp_sosi;  -- snk_in_arr().channel as high part of src_out.channel
  signal nxt_src_out_hi   : t_dp_sosi;
  signal channel_lo       : std_logic_vector(c_sel_w - 1 downto 0);
  signal nxt_channel_lo   : std_logic_vector(c_sel_w - 1 downto 0);
begin
  snk_out_arr <= i_snk_out_arr;

  -- Monitor sink valid input and sink ready output
  proc_dp_siso_alert(clk, snk_in_arr, i_snk_out_arr, tb_ready_reg);

  p_src_out_wires : process(src_out_hi, channel_lo)
  begin
    -- SOSI
    src_out <= src_out_hi;

    if g_append_channel_lo = true then
      -- The high part of src_out.channel copies the snk_in_arr().channel, the low part of src_out.channel is used to indicate the input port
      src_out.channel                     <= SHIFT_UVEC(src_out_hi.channel, -c_sel_w);
      src_out.channel(c_sel_w - 1 downto 0) <= channel_lo;
    end if;
  end process;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      sel_ctrl_reg <= 0;
      sel_ctrl_evt <= '0';
      in_xon_arr   <= (others => '0');
      in_sel       <= 0;
      prev_src_in  <= c_dp_siso_rst;
      state        <= s_idle;
      src_out_hi   <= c_dp_sosi_rst;
      channel_lo   <= (others => '0');
    elsif rising_edge(clk) then
      sel_ctrl_reg <= nxt_sel_ctrl_reg;
      sel_ctrl_evt <= nxt_sel_ctrl_evt;
      in_xon_arr   <= nxt_in_xon_arr;
      in_sel       <= nxt_in_sel;
      prev_src_in  <= src_in;
      state        <= nxt_state;
      src_out_hi   <= nxt_src_out_hi;
      channel_lo   <= nxt_channel_lo;
    end if;
  end process;

  gen_input : for I in 0 to g_nof_input - 1 generate
    gen_fifo : if g_use_fifo = true generate
      u_fill : entity work.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_bsn_w          => g_bsn_w,
        g_data_w         => g_data_w,
        g_empty_w        => g_empty_w,
        g_channel_w      => g_in_channel_w,
        g_error_w        => g_error_w,
        g_use_bsn        => g_use_bsn,
        g_use_empty      => g_use_empty,
        g_use_channel    => g_use_in_channel,
        g_use_error      => g_use_error,
        g_use_sync       => g_use_sync,
        g_fifo_fill      => c_fifo_fill(I),
        g_fifo_size      => c_fifo_size(I),
        g_fifo_af_margin => g_fifo_af_margin,
        g_fifo_af_xon    => g_fifo_af_xon,
        g_fifo_rl        => 1
      )
      port map (
        rst      => rst,
        clk      => clk,
        -- ST sink
        snk_out  => i_snk_out_arr(I),
        snk_in   => snk_in_arr(I),
        -- ST source
        src_in   => rd_siso_arr(I),
        src_out  => rd_sosi_arr(I)
      );
    end generate;

    no_fifo : if g_use_fifo = false generate
      i_snk_out_arr <= rd_siso_arr;
      rd_sosi_arr   <= snk_in_arr;
    end generate;

    -- Hold the sink input to be able to register the source output
    u_hold : entity work.dp_hold_input
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => OPEN,  -- SISO ready
      snk_in       => rd_sosi_arr(I),  -- SOSI
      -- ST source
      src_in       => hold_src_in_arr(I),  -- SISO ready
      next_src_out => next_src_out_arr(I),  -- SOSI
      pend_src_out => pend_src_out_arr(I),
      src_out_reg  => src_out_hi
    );
  end generate;

  -- Register and adjust external MM sel_ctrl for g_sel_ctrl_invert
  nxt_sel_ctrl_reg <= sel_ctrl when g_sel_ctrl_invert = false else g_nof_input - 1 - sel_ctrl;

  -- Detect change in sel_ctrl
  nxt_sel_ctrl_evt <= '1' when nxt_sel_ctrl_reg /= sel_ctrl_reg else '0';

  -- The output register stage matches RL = 1 for src_in.ready
  nxt_src_out_hi <= next_src_out_arr(in_sel);  -- default output selected next_src_out_arr
  nxt_channel_lo <= TO_UVEC(in_sel, c_sel_w);  -- pass on input index via channel low

  ------------------------------------------------------------------------------
  -- Unframed MM controlled input selection scheme
  ------------------------------------------------------------------------------

  gen_sel_ctrl_direct : if g_mode = 2 generate
    hold_src_in_arr <= (others => src_in);  -- pass src_in on to all inputs, only the selected input sosi gets used and the sosi from the other inputs will get lost
    rd_siso_arr     <= (others => src_in);

    nxt_in_sel <= sel_ctrl_reg;  -- external MM control selects the input
  end generate;

  ------------------------------------------------------------------------------
  -- Framed input selection schemes
  ------------------------------------------------------------------------------

  gen_sel_ctrl_framed : if g_mode = 4 generate
    u_dp_frame_busy_arr : entity work.dp_frame_busy_arr
    generic map (
      g_nof_inputs => g_nof_input,
      g_pipeline   => 1  -- register snk_in_busy to ease timing closure
    )
    port map (
      rst             => rst,
      clk             => clk,
      snk_in_arr      => rd_sosi_arr,
      snk_in_busy_arr => rd_sosi_busy_arr
    );

    hold_src_in_arr <= (others => c_dp_siso_rdy);  -- effectively bypass the dp_hold_input

    p_rd_siso_arr : process(src_in, in_xon_arr)
    begin
      for I in 0 to g_nof_input - 1 loop
        rd_siso_arr(I).ready <= src_in.ready;  -- default pass on src_in ready flow control to all inputs
        rd_siso_arr(I).xon   <= in_xon_arr(I);  -- use xon to enable one input and stop all other inputs
      end loop;
    end process;

    p_state : process(state, in_sel, rd_sosi_busy_arr, sel_ctrl_reg, sel_ctrl_evt)
    begin
      nxt_state      <= state;
      nxt_in_sel     <= in_sel;
      nxt_in_xon_arr <= (others => '0');  -- Default stop all inputs

      case state is
        when s_idle =>
          -- Wait until all inputs are inactive (due to xon='0') to ensure that the old input has finished its last frame and the new input has not started yet
          if unsigned(rd_sosi_busy_arr) = 0 then
            nxt_in_sel <= sel_ctrl_reg;
            nxt_state <= s_output;
          end if;

        when others =>  -- s_output
          -- Enable only the selected input via xon='1'
          nxt_in_xon_arr(sel_ctrl_reg) <= '1';

          -- Detect if the input selection changes
          if sel_ctrl_evt = '1' then
            nxt_state <= s_idle;
          end if;
      end case;
    end process;
  end generate;

  gen_framed : if g_mode = 0 or g_mode = 1 or g_mode = 3 generate
    p_hold_src_in_arr : process(rd_siso_arr, pend_src_out_arr, in_sel, src_in)
    begin
      hold_src_in_arr <= rd_siso_arr;  -- default ready for hold input when ready for sink input
      if pend_src_out_arr(in_sel).eop = '1' then
        hold_src_in_arr(in_sel) <= src_in;  -- also ready for hold input when the eop is there
      end if;
    end process;

    next_sel <= in_sel + 1 when in_sel < g_nof_input - 1 else 0;

    p_state : process(state, in_sel, next_sel, pend_src_out_arr, src_in, prev_src_in, sel_ctrl_reg)
    begin
      rd_siso_arr <= (others => c_dp_siso_hold);  -- default not ready for input, but xon='1'

      nxt_in_sel <= in_sel;

      nxt_state <= state;

      case state is
        when s_idle =>
          -- Need to check pend_src_out_arr(in_sel).sop, which can be active if prev_src_in.ready was '1',
          -- because src_in.ready may be '0' and then next_src_out_arr(in_sel).sop is '0'
          if pend_src_out_arr(in_sel).sop = '1' then
            if pend_src_out_arr(in_sel).eop = '1' then
              rd_siso_arr <= (others => c_dp_siso_hold);  -- the sop and the eop are there, it is a frame with only one data word, stop reading this input
              if src_in.ready = '1' then
                nxt_in_sel            <= next_sel;  -- the pend_src_out_arr(in_sel).eop will be output, so continue to next input.
                rd_siso_arr(next_sel) <= src_in;
              end if;
            else
              rd_siso_arr(in_sel) <= src_in;  -- the sop is there, so start outputting the frame from this input
              nxt_state <= s_output;
            end if;
          else
            case g_mode is
              when 0 | 3 =>
                -- Framed round-robin with fair chance per input
                if prev_src_in.ready = '0' then
                  rd_siso_arr(in_sel) <= src_in;  -- no sop, remain at current input to give it a chance
                else
                  nxt_in_sel            <= next_sel;  -- no sop, select next input, because the current input has had a chance
                  rd_siso_arr(next_sel) <= src_in;
                end if;
              when others =>  -- = 1
                -- Framed round-robin in forced order from each input
                rd_siso_arr(in_sel) <= src_in;  -- no sop, remain at current input to wait for a frame
            end case;
          end if;
        when others =>  -- s_output
          rd_siso_arr(in_sel) <= src_in;  -- output the rest of the selected input frame
          if pend_src_out_arr(in_sel).eop = '1' then
            rd_siso_arr <= (others => c_dp_siso_hold);  -- the eop is there, stop reading this input
            if src_in.ready = '1' then
              nxt_in_sel            <= next_sel;  -- the pend_src_out_arr(in_sel).eop will be output, so continue to next input.
              rd_siso_arr(next_sel) <= src_in;
              nxt_state <= s_idle;
            end if;
          end if;
      end case;

      -- Pass on frame level flow control
      for I in 0 to g_nof_input - 1 loop
        rd_siso_arr(I).xon <= src_in.xon;

        if g_mode = 3 then
          -- Framed MM control select input via XON
          rd_siso_arr(I).xon <= '0';  -- force xon='0' for not selected inputs
          if sel_ctrl_reg = I then
            rd_siso_arr(I).xon <= src_in.xon;  -- pass on frame level flow control for selected input
          end if;
        end if;
      end loop;
    end process;

  end generate;
end rtl;
