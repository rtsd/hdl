-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: AVS wrapper to make a MM slave ports for ETH available as conduit
-- Description:
-- Remark:
-- . The avs2_eth_coe_hw.tcl determines the read latency per port

library IEEE, common_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use work.eth_pkg.all;

entity avs2_eth_coe is
  port (
    ----------------------------------------------------------------------------
    -- MM side

    -- Clock interface
    csi_mm_reset               : in  std_logic;
    csi_mm_clk                 : in  std_logic;

    -- TSE MAC
    mms_tse_address            : in  std_logic_vector(c_tech_tse_byte_addr_w - 1 downto 0);
    mms_tse_write              : in  std_logic;
    mms_tse_read               : in  std_logic;
    mms_tse_writedata          : in  std_logic_vector(c_word_w - 1 downto 0);
    mms_tse_readdata           : out std_logic_vector(c_word_w - 1 downto 0);  -- read latency is 0
    mms_tse_waitrequest        : out std_logic;  -- necessary because read latency is 0

    -- ETH registers
    mms_reg_address            : in  std_logic_vector(c_eth_reg_addr_w - 1 downto 0);
    mms_reg_write              : in  std_logic;
    mms_reg_read               : in  std_logic;
    mms_reg_writedata          : in  std_logic_vector(c_word_w - 1 downto 0);
    mms_reg_readdata           : out std_logic_vector(c_word_w - 1 downto 0);  -- read latency is 1

    -- ETH packet RAM
    mms_ram_address            : in  std_logic_vector(c_eth_ram_addr_w - 1 downto 0);
    mms_ram_write              : in  std_logic;
    mms_ram_read               : in  std_logic;
    mms_ram_writedata          : in  std_logic_vector(c_word_w - 1 downto 0);
    mms_ram_readdata           : out std_logic_vector(c_word_w - 1 downto 0);  -- read latency is 2

    -- Interrupt Sender interface
    ins_interrupt_irq          : out std_logic;  -- relates to the ETH registers port

    ----------------------------------------------------------------------------
    -- User side

    -- Clock interface
    coe_reset_export           : out std_logic;
    coe_clk_export             : out std_logic;

    -- TSE MAC
    coe_tse_address_export     : out std_logic_vector(c_tech_tse_byte_addr_w - 1 downto 0);
    coe_tse_write_export       : out std_logic;
    coe_tse_read_export        : out std_logic;
    coe_tse_writedata_export   : out std_logic_vector(c_word_w - 1 downto 0);
    coe_tse_readdata_export    : in  std_logic_vector(c_word_w - 1 downto 0);
    coe_tse_waitrequest_export : in  std_logic;

    -- ETH registers
    coe_reg_address_export     : out std_logic_vector(c_eth_reg_addr_w - 1 downto 0);
    coe_reg_write_export       : out std_logic;
    coe_reg_read_export        : out std_logic;
    coe_reg_writedata_export   : out std_logic_vector(c_word_w - 1 downto 0);
    coe_reg_readdata_export    : in  std_logic_vector(c_word_w - 1 downto 0);

    -- ETH packet RAM
    coe_ram_address_export     : out std_logic_vector(c_eth_ram_addr_w - 1 downto 0);
    coe_ram_write_export       : out std_logic;
    coe_ram_read_export        : out std_logic;
    coe_ram_writedata_export   : out std_logic_vector(c_word_w - 1 downto 0);
    coe_ram_readdata_export    : in  std_logic_vector(c_word_w - 1 downto 0);

    -- Interrupt Sender interface
    coe_irq_export             : in  std_logic
  );
end avs2_eth_coe;

architecture wrap of avs2_eth_coe is
begin
  ------------------------------------------------------------------------------
  -- Wires

  -- Clock interface
  coe_reset_export         <= csi_mm_reset;
  coe_clk_export           <= csi_mm_clk;

  -- TSE MAC
  coe_tse_address_export   <= mms_tse_address;
  coe_tse_write_export     <= mms_tse_write;
  coe_tse_read_export      <= mms_tse_read;
  coe_tse_writedata_export <= mms_tse_writedata;
  mms_tse_readdata         <= coe_tse_readdata_export;
  mms_tse_waitrequest      <= coe_tse_waitrequest_export;

  -- ETH registers
  coe_reg_address_export   <= mms_reg_address;
  coe_reg_write_export     <= mms_reg_write;
  coe_reg_read_export      <= mms_reg_read;
  coe_reg_writedata_export <= mms_reg_writedata;
  mms_reg_readdata         <= coe_reg_readdata_export;

  -- ETH packet RAM
  coe_ram_address_export   <= mms_ram_address;
  coe_ram_write_export     <= mms_ram_write;
  coe_ram_read_export      <= mms_ram_read;
  coe_ram_writedata_export <= mms_ram_writedata;
  mms_ram_readdata         <= coe_ram_readdata_export;

  -- Interrupt Sender interface
  ins_interrupt_irq        <= coe_irq_export;
end wrap;
