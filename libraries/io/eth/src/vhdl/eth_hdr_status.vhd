-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
--   Determine the Ethernet header status.
-- Description:
--   Determines all relevant information from the Ethernet header after every
--   asserted sop.
-- Remark:
-- . g_ip_header_checksum_calculate:
--   = TRUE, then the IP header checksum is calculated for hdr_status
--   = FALSE, then the IP header checksum is read for hdr_status
-- . hdr_status_complete indicates when new hdr_status are available for the
--   complete header so including the status for the last header word. Per
--   each header word the status is already available after one clock cycle.

entity eth_hdr_status is
  generic (
    g_ip_header_checksum_calculate : boolean := true
  );
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;

    -- Total header
    hdr_words_arr     : in  t_network_total_header_32b_arr;
    hdr_words_arr_val : in  std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    hdr_fields        : in  t_network_total_header;
    hdr_fields_val    : in  std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    hdr_data          : in  std_logic_vector(c_word_w - 1 downto 0);
    hdr_data_val      : in  std_logic;

    -- Header status
    hdr_status          : out t_eth_hdr_status;
    hdr_status_complete : out std_logic
  );
end eth_hdr_status;

architecture rtl of eth_hdr_status is
  signal i_hdr_status        : t_eth_hdr_status;
  signal nxt_hdr_status      : t_eth_hdr_status;

  signal ip_header           : t_dp_sosi;
  signal calc_checksum       : std_logic_vector(c_halfword_w - 1 downto 0);
  signal calc_checksum_val   : std_logic;
  signal ip_checksum         : std_logic_vector(c_halfword_w - 1 downto 0);
  signal ip_checksum_val     : std_logic;
begin
  hdr_status <= i_hdr_status;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_hdr_status        <= c_eth_hdr_status_rst;
      hdr_status_complete <= '0';
    elsif rising_edge(clk) then
      i_hdr_status        <= nxt_hdr_status;
      hdr_status_complete <= hdr_fields_val(c_network_total_header_32b_nof_words - 1);
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Packet type
  nxt_hdr_status.is_arp  <= '1' when unsigned(hdr_fields.eth.eth_type) = c_network_eth_type_arp     and hdr_fields_val(c_network_total_header_32b_eth_type_wi) = '1' else '0';
  nxt_hdr_status.is_ip   <= '1' when unsigned(hdr_fields.eth.eth_type) = c_network_eth_type_ip      and hdr_fields_val(c_network_total_header_32b_eth_type_wi) = '1' else '0';
  nxt_hdr_status.is_icmp <= '1' when unsigned(hdr_fields.ip.protocol) = c_network_ip_protocol_icmp  and hdr_fields_val(c_network_total_header_32b_ip_protocol_wi) = '1'  and i_hdr_status.is_ip = '1'  else '0';
  nxt_hdr_status.is_udp  <= '1' when unsigned(hdr_fields.ip.protocol) = c_network_ip_protocol_udp   and hdr_fields_val(c_network_total_header_32b_ip_protocol_wi) = '1'  and i_hdr_status.is_ip = '1'  else '0';
  nxt_hdr_status.is_dhcp <= '1' when unsigned(hdr_fields.udp.dst_port) = c_network_udp_port_dhcp_in and hdr_fields_val(c_network_total_header_32b_udp_dst_port_wi) = '1' and i_hdr_status.is_udp = '1' else '0';

  ------------------------------------------------------------------------------
  -- IP Header checksum
  ip_header.data  <= RESIZE_dp_data(hdr_data);
  ip_header.valid <= '1' when unsigned(hdr_words_arr_val(c_network_total_header_32b_ip_lo_wi to c_network_total_header_32b_ip_hi_wi)) /= 0 else '0';
  ip_header.sop   <= hdr_words_arr_val(c_network_total_header_32b_ip_lo_wi);
  ip_header.eop   <= hdr_words_arr_val(c_network_total_header_32b_ip_hi_wi);
  ip_header.empty <= (others => '0');

  -- Calculate the IP header checksum for the packet header
  u_calc_checksum : entity work.eth_checksum
  port map (
    rst           => rst,
    clk           => clk,
    snk_in        => ip_header,
    checksum      => calc_checksum,
    checksum_val  => calc_checksum_val
  );

  -- Use calculated IP header checksum
  gen_calc : if g_ip_header_checksum_calculate = true generate
    ip_checksum     <= calc_checksum;
    ip_checksum_val <= calc_checksum_val;
  end generate;

  -- Use IP header checksum field read from the frame header
  gen_read : if g_ip_header_checksum_calculate = false generate
    ip_checksum     <= hdr_fields.ip.header_checksum;
    ip_checksum_val <= calc_checksum_val;  -- use u_calc_checksum only for valid control
  end generate;

  -- If it is an IP packet then update the hdr_status IP checksum fields, else leave them 0
  p_hdr_status_ip_checksum : process(i_hdr_status, ip_checksum, ip_checksum_val)
  begin
    nxt_hdr_status.ip_checksum_val   <= '0';
    nxt_hdr_status.ip_checksum       <= (others => '0');
    nxt_hdr_status.ip_checksum_is_ok <= '0';
    if i_hdr_status.is_ip = '1' and ip_checksum_val = '1' then
      nxt_hdr_status.ip_checksum_val <= '1';
      nxt_hdr_status.ip_checksum     <= ip_checksum;
      if unsigned(ip_checksum) = 0 then
        nxt_hdr_status.ip_checksum_is_ok <= '1';
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- UDP port number
  nxt_hdr_status.udp_port <= hdr_fields.udp.dst_port;
end rtl;
