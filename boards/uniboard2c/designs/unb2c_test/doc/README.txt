This is the testsuite for Uniboard2c.

Here are quick steps to compile and use test designs in RadionHDL.
------------------------------------------------------------------

Ideally the test design "unb2c_test" would contain all test suites in one but that does
not fit in the device. Therefore revisions are prepared.

The available revisions for unb2c_test are in the directory: ../revisions/
The specifications and features of each revision is listed in the VHDL file:
../src/vhdl/unb2c_test_pkg.vhd , below the comment "Revision Control".


Steps to go:
------------

-> At all times run:

     cd ${GIT}/hdl
     . init_hdl.sh

-> In case of a new installation, the IP's have to be generated for Arria10. 

     echo $HDL_BUILD_DIR         # should be something like /home/user/git/hdl/build
     rm -r $HDL_BUILD_DIR/unb2c  # optional
     compile_altera_simlibs unb2c
     generate_ip_libs unb2c


1. Start with the Oneclick Commands:
     modelsim_config unb2c
     quartus_config unb2c


2. Generate MMM for QSYS (select one of these revisions):
    run_qsys_pro unb2c revision  # where 'revision' is 1 design out of ../revisions/



3. -> From here either continue to Modelsim (simulation) or Quartus (synthesis)

Simulation
----------
    run_modelsim unb2c

Further Modelsim instructions: see the README file in the ../revisions/* directories



Synthesis
---------
Quartus instructions: (select one of these revisions):
    run_qcomp unb2c revision # where 'revision' is 1 design out of ../revisions/


Or compile using the Quartus GUI:
    run_quartus unb2c
load the project now from the build directory.







4. Load firmware
----------------
Using JTAG: Start the Quartus GUI and open: tools->programmer.
            Then click auto-detect; (click 4x ok)
            Use 'change file' to select the correct .sof file (in $HDL_WORK/build/unb2c/quartus/unb2c_test_...) for each FPGA
            Select the FPGA(s) which has to be programmed
            Click 'start'
Using EPCS: See step 6 below.


5. Testing on hardware
----------------------
Assuming the firmware is loaded and running already in the FPGA, the firmware can be tested from the connected
LCU computer. See the README file in the ../revisions/* directories for examples.



6.
(a)
Programming the EPCS flash.
when the EPCS module works an RBF file can be generated to program the flash,
then the .sof file file can be converted to .rbf with the 'run_rbf' script.
For generating a User image .RBF file:

    run_rbf unb2c unb2c_test_[revision]

For generating a Factory image .RBF file:

    run_rbf unb2c --unb2_factory unb2c_test_[revision]

The .RBF file is now in $HDL_WORK/build/unb2c/quartus/unb2c_test_[revision]
Now copy the .RBF file to the LCU host with 'scp'

(b)
Then to program the FPGA(s) via the LCU host, use the python script.

Program User image:
    python util_epcs.py --unb 1 --fn 0 -n 7 -s unb2c_test_[revision].rbf
Program Factory image:
    python util_epcs.py --unb 1 --fn 0 -n 3 -s unb2c_test_[revision].rbf

-> For extra info on RBF files on Uniboard2, see: $HDL_WORK/libraries/io/epcs/doc/README.txt

To start the User image:
    python util_remu.py --unb 1 --fn 0 -n 6  # ignore timeout error
To start the Factory image:
    python util_remu.py --unb 1 --fn 0 -n 5  # ignore timeout error



An alternative method to write the flash is via a .JIC file and JTAG:

Firstly a JIC file has to be generated from the SOF file.
In Quartus GUI; open current project; File -> Convert Programming Files.
Then setup:
- Output programming file: JIC
- Configuration device: EPCQL1024
- Mode: Active Serial x4
- Flash Loader: Add/Select Device Arria10/10AX115U4E3
- SOF Data: add file (the generated .sof file)
  - click the .sof file; Set property 'Compression' to ON
- Press 'Generate'
Then program the .JIC file (output_file.jic) to EPCS flash:
- Make sure that the JTAG (on server connected to board) runs at 16MHz:
  c:\altera\15.0\quartus\bin64\jtagconfig USB-BlasterII JtagClock 16M
- open tools->programmer
- make sure the 4 fpga icons have the device 10AX115U4F45ES
- right-click each fpga icon and attach flash device EPCQL1024
- optional see (*1)
- right-click each EPCQL1024 and change file from <none> to output_file.jic
- select click each Program/Configure radiobutton
- click start and wait for 'Successful'

(*1) When error select correct SFL (serial flash loader) from Altera service request for each FPGA:
     right-click each fpga and change file from <none> to sfl_enhanced_01_02e360dd.sof
     (in $HDL_WORK/boards/uniboard2/libraries/unb2c_board/quartus)


7.
Optionally you can readout the messages the NIOS application "unb_osy" prints over the JTAG interface.
First make sure that the JTAG cable is set to a correct speed. Use the command:

  jtagconfig --setparam "USB-BlasterII [USB-1]" JtagClock 8M

Then use this command to read from the JTAG terminal:

  cd /home/software/Altera/15.1/quartus
  ./bin/nios2-terminal --cable 1 --device=1

Where device is 1..4, selecting 1 of the 4 FPGAs

