-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Test multiple instances of tb_st_xst
-- Usage:
-- > as 10
-- > run -all
--
-- Description: See tb_st_xst

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_st_xst is
end tb_tb_st_xst;

architecture tb of tb_tb_st_xst is
  constant c_nof_sync       : natural := 3;
  constant c_dsp_data_w     : natural := 16;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--  GENERICS:
--    g_nof_streams        : NATURAL := 9;
--    g_nof_crosslets      : NATURAL := 2;
--    g_nof_signal_inputs  : NATURAL := 12;
--    g_in_data_w          : NATURAL := 16;
--    g_nof_sync           : NATURAL := 3;
--    g_stat_data_w        : NATURAL := 64;  -- statistics accumulator width
--    g_stat_data_sz       : NATURAL := 2;   -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
--    g_nof_block_per_sync : NATURAL := 5;
--    g_nof_clk_per_blk    : NATURAL := 1024

  u_sdp                : entity work.tb_st_xst generic map (9, 1, 12, c_dsp_data_w, c_nof_sync, 64, 2, 5, 1024);
  u_sdp_one            : entity work.tb_st_xst generic map (1, 1, 12, c_dsp_data_w, c_nof_sync, 64, 2, 5, 1024);
  u_sdp_mult_crosslets : entity work.tb_st_xst generic map (9, 7, 12, c_dsp_data_w, c_nof_sync, 64, 2, 5, 1024);
  -- Note: u_max shows that the dut will skip sync periods if nof_statistics is not < g_nof_clk_per_blk
  u_max                : entity work.tb_st_xst generic map (2, 16, 8, c_dsp_data_w, c_nof_sync, 64, 2, 5, 1024);  -- g_nof_crosslets * g_nof_signal_inputs**2 = 16 * 8 * 8 = 1024 = g_nof_clk_per_blk
end tb;
