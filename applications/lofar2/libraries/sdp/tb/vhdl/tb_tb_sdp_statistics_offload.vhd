-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle, E. Kooistra
-- Purpose: Verify multiple variations of tb_sdp_statistics_offload
-- Description:
-- Usage:
-- > as 3
-- > run -all
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_sdp_statistics_offload is
end tb_tb_sdp_statistics_offload;

architecture tb of tb_tb_sdp_statistics_offload is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    -- All
--    g_fast_mm_clk              : BOOLEAN := TRUE;  -- When TRUE use 1 GHz mm_clk  to speed up simulation, else use
--                                                   -- 100 MHz mm_clk for real speed of u_dp_block_from_mm_dc in
--                                                   -- sdp_statistics_offload
--    g_statistics_type          : STRING := "SST";
--    g_offload_node_time        : NATURAL := 50;
--    g_offload_packet_time      : natural := 5;
--    g_offload_scale_w          : natural := 0;  -- 0 = default
--    g_ctrl_interval_size       : natural := 1;  -- 1 = default
--    g_ctrl_interval_size_min   : natural := 1;  -- 1 = default
--    g_reverse_word_order       : BOOLEAN := TRUE  -- when TRUE then stream LSB word after MSB word.
--    g_gn_index                 : NATURAL := 1;  -- global node (GN) index, use > 0 to see effect of
--                                                -- g_offload_node_time
--    g_nof_sync                 : NATURAL := 3;
--    -- BST
--    g_beamset_id               : NATURAL := 0;
--    -- XST
--    g_O_rn                     : NATURAL := 0;  -- GN index of first ring node (RN)
--    g_N_rn                     : NATURAL := 16; -- <= c_sdp_N_rn_max = 16, number of nodes in ring
--    g_P_sq                     : NATURAL := c_sdp_P_sq
--    g_nof_crosslets            : NATURAL := 1;
--    g_crosslets_direction      : INTEGER := 1;  -- +1 or -1

  u_sst                     : entity work.tb_sdp_statistics_offload generic map(
                              true, "SST",    50, 5, 0, 1, 1,  true, 3, 3);
  u_sst_time                : entity work.tb_sdp_statistics_offload generic map(
                              true, "SST",    50, 8, 1, 6, 2,  true, 3, 3);
  u_sst_no_reverse          : entity work.tb_sdp_statistics_offload generic map(
                              true, "SST",    50, 5, 0, 1, 1, false, 3, 3);
  u_sst_os                  : entity work.tb_sdp_statistics_offload generic map(
                              true, "SST_OS", 50, 5, 0, 1, 1,  true, 3, 3);
  u_sst_os_no_reverse       : entity work.tb_sdp_statistics_offload generic map(
                              true, "SST_OS", 50, 5, 0, 1, 1, false, 3, 3);
  u_bst_0                   : entity work.tb_sdp_statistics_offload generic map(
                              true, "BST",    50, 5, 0, 1, 1,  true, 1, 3);
  u_bst_0_no_reverse        : entity work.tb_sdp_statistics_offload generic map(
                              true, "BST",    50, 5, 0, 1, 1, false, 1, 3, 0);
  u_bst_1                   : entity work.tb_sdp_statistics_offload generic map(
                              true, "BST",    50, 5, 0, 1, 1,  true, 1, 3, 1);
  u_xst_P1                  : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 0, 16,  1, 1, 1);
  u_xst_P1_N3               : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 0, 16,  1, 3, 1);
  u_xst_P9                  : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 0, 16,  9, 1, 1);
  u_xst_P9_N3               : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 0, 16,  9, 3, 1);
  u_xst_P9_N3_no_reverse    : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1, false, 1, 3, 0, 0, 16,  9, 3, 1);
  u_xst_P9_N3_neg_dir       : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 0, 16,  9, 3, 0);
  u_xst_P8_N7_RN1_15        : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 1, 3, 0, 1, 15,  8, 7, 0);
  -- P_sq = 1 < N_rn/2+1 = 5
  u_xst_P1_N7_RN0_7         : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 3, 3, 0, 0,  8,  1, 7, 1);
  -- P_sq = 9 > N_rn/2+1 = 5
  u_xst_P9_N7_RN0_7         : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 0, 1, 1,  true, 3, 3, 0, 0,  8,  9, 7, 1);
  -- P_sq = 9 > N_rn/2+1 = 5
  u_xst_P9_N7_RN0_7_time    : entity work.tb_sdp_statistics_offload generic map(
                              true, "XST",    50, 5, 1, 3, 2,  true, 3, 3, 0, 0,  8,  9, 7, 1);
  -- P_sq = 9 > N_rn/2+1 = 5
  u_xst_P9_N4_RN0_7_slow_mm : entity work.tb_sdp_statistics_offload generic map(
                              false, "XST",    50, 5, 0, 1, 1,  true, 3, 3, 0, 0,  8,  9, 4, 1);
  -- P_sq = 9 > N_rn/2+1 = 5
  u_xst_P9_N7_RN0_7_slow_mm : entity work.tb_sdp_statistics_offload generic map(
                              false, "XST",    50, 5, 0, 1, 1,  true, 3, 3, 0, 0,  8,  9, 7, 1);
end tb;
