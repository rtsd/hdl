-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . 'Shift register' that shifts on block boundary instead of single
--    word boundary: If the register is full, it shifts out an entire
--    block.
-- Description:
-- . A normal shift register cannot be used (instead of a FIFO) for
--   this purpose, because those fill from left to right and shifting
--   out the contents causes gaps to be shifted in.
-- Remarks:
-- . The output latency (in cycles) can be calculated as follows:
--   out_latency = block_size*rate_div - (in_block_size*rate_div - in_block_size) + 1
--   in which:
--   . rate_div = rate divider, e.g. 4 when 1/4 of the input is valid.
--   . in_block_size = input block size.
--   . the '+1' is caused by 'nxt_' register latency; always there (not
--     valid-dependent like the rest).

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_blockreg is
  generic (
    g_technology : natural := c_tech_select_default;
    g_block_size : natural;
    g_dat_w      : natural
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;

    in_dat       : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val       : in  std_logic;

    out_dat      : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val      : out std_logic
  );
end common_blockreg;

architecture str of common_blockreg is
  signal i_out_val    : std_logic;

  signal usedw        : std_logic_vector(ceil_log2(g_block_size+1) - 1 downto 0);
  signal rd_req       : std_logic;
  signal prev_rd_req  : std_logic;

  signal out_cnt      : std_logic_vector(ceil_log2(g_block_size) - 1 downto 0);
  signal nxt_out_cnt  : std_logic_vector(ceil_log2(g_block_size) - 1 downto 0);
begin
  gen_bypass: if g_block_size = 1 generate
    out_dat <= in_dat;
    out_val <= in_val;
  end generate;

  gen_block_out: if g_block_size > 1 generate
    out_val <= i_out_val;

    u_fifo : entity work.common_fifo_sc
    generic map (
      g_technology  => g_technology,
      g_note_is_ful => false,
      g_dat_w       => g_dat_w,
      g_nof_words   => g_block_size+1
    )
    port map (
      clk    => clk,
      rst    => rst,

      wr_dat => in_dat,
      wr_req => in_val,

      usedw  => usedw,
      rd_req => rd_req,

      rd_dat => out_dat,
      rd_val => i_out_val
    );

    -----------------------------------------------------------------------------
    -- Toggle rd_req to create output blocks of g_block_size
    -----------------------------------------------------------------------------
    p_block_out: process(in_val, prev_rd_req, out_cnt, usedw)
    begin
      rd_req <= prev_rd_req;
      if unsigned(out_cnt) = g_block_size-1 then
        -- End of current output block
        if unsigned(usedw) < g_block_size then
          -- de-asserting rd_req will not cause FIFO to overflow
          rd_req <= '0';
        end if;
      end if;
      if unsigned(usedw) = g_block_size then
        -- Start of new output block
        rd_req <= '1';
      end if;
    end process;

    -----------------------------------------------------------------------------
    -- Valid output word counter
    -----------------------------------------------------------------------------
    p_out_cnt : process(i_out_val, out_cnt)
    begin
      nxt_out_cnt <= out_cnt;
      if i_out_val = '1' then
        nxt_out_cnt <= INCR_UVEC(out_cnt, 1);
        if unsigned(out_cnt) = g_block_size-1 then
          nxt_out_cnt <= (others => '0');
        end if;
      end if;
    end process;

    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
         out_cnt     <= (others => '0');
         prev_rd_req <= '0';
       elsif rising_edge(clk) then
         out_cnt     <= nxt_out_cnt;
         prev_rd_req <= rd_req;
      end if;
    end process;

  end generate;
end str;
