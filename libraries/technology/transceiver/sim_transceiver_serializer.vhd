--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
--   Basic serializer model for fast transceiver simulation
-- Description:
--   The model serializes parallel data using 10 serial bits per byte. The two
--   extra bits are used to transfer control (valid, SOP/EOP).
--   The model can represent any real transceiver encoding scheme (10b/8b, 66b/64b)
--   because the modelled line rate does not have to be the same as the true line rate.
--   The key feature that the model provides is that the parallel data gets
--   transported via a single 1-bit lane. This allows fast simulation of the
--   link using the true port widths.
--   The most straightforward is to mimic 10/8 encoding for as far as data rates
--   and clock ratios are concerned (not the encoding itself):
--   * User data rate = (8/10)*line data rate
--   * User clock frequency = User data rate / user data width
--   * Serial data block size = 10 bits [9..0] LSb sent first
--     *  [9] = SOP/EOP; '1'=SOP;'U'=EOP.
--     *  [8] = Control bit.
--     *  [7..0] = Data
--   * Word/byte alignment is not required because reference clk and rst are
--     global in simulation: what gets transmitted first is received first.
--
--  The following diagram shows the serialization of the 32-bit word 0x2. The
--  grid of dots indicates the bit resolution. Note the 1 serial cycle of delay
--  before the first bit is put on the line.
--
--               . _______________________________________ . . . . . . . . . . . . . . . . . . . . .
-- tr_clk        _|. . . . . . . . . . . . . . . . . . . .|_________________________________________
--               _ . . _ . . . . . . _ . . . . . . . . . _ . . . . . . . . . _ . . . . . . . . . _ .
-- tx_serial_out .|___|.|___________|.|_________________|.|_________________|.|_________________|.|_
--
--               c P 0 1 2 3 4 5 6 7 c P 0 1 2 3 4 5 6 7 c P 0 1 2 3 4 5 6 7 c P 0 1 2 3 4 5 6 7 c P
--                  |<----- Byte 0 ---->|<----- Byte 1 ---->|<----- Byte 2 ---->|<----- Byte 3 ---->|
--
-- Remarks:
-- . All serializers in the simualation should be simultaneously released from
--   reset and have to share the same transceiver reference clock.
-- . The number of line clock cycles to transmit one data word fits within 1
--   tr_clk period. After every data word the data is realigned to the tr_clk.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity sim_transceiver_serializer is
  generic(
    g_data_w         : natural := 32;
    g_tr_clk_period  : time := 6.4 ns
  );
  port(
    tb_end        : in  std_logic := '0';

    tr_clk        : in  std_logic;
    tr_rst        : in  std_logic;

    tx_in_data    : in  std_logic_vector(g_data_w - 1 downto 0);
    tx_in_ctrl    : in  std_logic_vector(g_data_w / c_byte_w - 1 downto 0);  -- 1 valid bit per byte
    tx_in_sop     : in  std_logic_vector(g_data_w / c_byte_w - 1 downto 0) := (others => '0');  -- 1 SOP   bit per byte
    tx_in_eop     : in  std_logic_vector(g_data_w / c_byte_w - 1 downto 0) := (others => '0');  -- 1 EOP   bit per byte

    tx_serial_out : out std_logic
  );
end sim_transceiver_serializer;

architecture beh of sim_transceiver_serializer is
  constant c_line_clk_period   : time    := g_tr_clk_period * 8 / 10 / g_data_w;
  constant c_tr_clk_period_sim : time    := c_line_clk_period * g_data_w * 10 / 8;

  constant c_nof_bytes_per_data : natural := g_data_w / c_byte_w;
begin
  p_serialize: process
    variable v_tx_in_data : std_logic_vector(g_data_w - 1 downto 0);
    variable v_tx_in_ctrl : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    variable v_tx_in_sop  : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    variable v_tx_in_eop  : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
  begin
    tx_serial_out <= '0';
    wait until tr_rst = '0';

    -- Align to tr_clk
    wait until rising_edge(tr_clk);
    v_tx_in_data := tx_in_data;
    v_tx_in_ctrl := tx_in_ctrl;
    v_tx_in_sop  := tx_in_sop;
    v_tx_in_eop  := tx_in_eop;

    while tb_end = '0' loop
      -- Data word serialization cycle
      for byte in 0 to c_nof_bytes_per_data - 1 loop
        -- Serialize each data byte using 10 bits per byte on the line
        for bit in 0 to c_byte_w - 1 loop
          tx_serial_out <= v_tx_in_data(byte * c_byte_w + bit);  -- Put the 8 data bits of the data byte on the line
          wait for c_line_clk_period;
        end loop;
        tx_serial_out <= v_tx_in_ctrl(byte);  -- Put the valid bit on the line for each byte
        wait for c_line_clk_period;
        tx_serial_out <= '0';  -- Put the SOP/EOP indicator bit on the line. '1'=SOP; 'U'=EOP.
        if v_tx_in_sop(byte) = '1' then
          tx_serial_out <= '1';
        elsif v_tx_in_eop(byte) = '1' then
          tx_serial_out <= 'U';
        end if;
        if byte < c_nof_bytes_per_data - 1 then
          wait for c_line_clk_period;  -- exit loop in last line clock cycle
        end if;
      end loop;

      -- Realign to tr_clk rising edge if necessary
      wait until rising_edge(tr_clk);

      v_tx_in_data := tx_in_data;
      v_tx_in_ctrl := tx_in_ctrl;
      v_tx_in_sop  := tx_in_sop;
      v_tx_in_eop  := tx_in_eop;
    end loop;
  end process;
end beh;
