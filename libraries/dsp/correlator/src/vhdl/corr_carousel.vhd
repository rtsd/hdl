--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Outputs all unique pair permutations of the words at the inputs.
-- Description:
-- . Example permutations of [0,1,2]: [[0,0],[0,1],[0,2],[1,2],[1,1],[2,2]]
-- . c_nof_permutations = g_nof_inputs*(g_nof_inputs+1)/2
-- . Each permutation can be used to calculate one unique visibility.
-- . snk_in_arr(0).valid is used to indicate validity of all inputs.
-- Usage:
-- . Determine g_nof_inputs; e.g. 3
-- . Calculate c_nof_permutations: e.g. 6
--   . Note your output options, e.g.
--     . 6 cycles * 1 output
--     . 3 cycles * 2 outputs
--     . 2 cycles * 3 outputs
--     . 1 cycle  * 6 outputs
-- . Set your g_nof_outputs accordingly.

entity corr_carousel is
  generic (
    g_nof_inputs  : natural := 8;
    g_nof_outputs : natural := 9
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    snk_out_arr : out t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);

    src_out_arr : out t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0)  -- Array of pairs
  );
end corr_carousel;

architecture rtl of corr_carousel is
  constant c_nof_permutations : natural := g_nof_inputs * (g_nof_inputs + 1) / 2;

  signal nxt_reg_out_2arr_2   : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);
  signal reg_out_2arr_2       : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Carousel register shifting
  -- . 3-input example, carousel rotation is counter-clockwise:
  --                                                ___
  --                                               |   |
  --   snk_in_arr[2]--->nxt_reg_out_2arr_2[2][1]->[1]--|---> reg_out_2arr_2[2][1]
  --                \-->nxt_reg_out_2arr_2[2][0]---|->[0]--> reg_out_2arr_2[2][0]
  --   snk_in_arr[1]--->nxt_reg_out_2arr_2[1][1]--[1]  | --> reg_out_2arr_2[1][1]
  --                \-->nxt_reg_out_2arr_2[1][0]---|->[0]--> reg_out_2arr_2[1][0]
  --   snk_in_arr[0]--->nxt_reg_out_2arr_2[0][1]->[1]  | --> reg_out_2arr_2[0][1]
  --                \-->nxt_reg_out_2arr_2[0][0]---|->[0]--> reg_out_2arr_2[0][0]
  --                                               |___|
  -----------------------------------------------------------------------------
  p_carousel: process(snk_in_arr, reg_out_2arr_2)
  begin
    if snk_in_arr(0).valid = '1' then  -- Take fresh set of input data
      for i in 0 to g_nof_inputs - 1 loop
        nxt_reg_out_2arr_2(i)(0) <= snk_in_arr(i);
        nxt_reg_out_2arr_2(i)(1) <= snk_in_arr(i);
      end loop;
    else  -- Shift around the current data; keep producing new permutations
      -- Top of carousel, shift left
      nxt_reg_out_2arr_2(g_nof_inputs - 1)(1) <= reg_out_2arr_2(g_nof_inputs - 1)(0);
      -- Left column of carousel, shift down
      for i in 0 to g_nof_inputs - 2 loop
        nxt_reg_out_2arr_2(i)(1) <= reg_out_2arr_2(i + 1)(1);
      end loop;
      -- Right column of carousel, shift up
      for i in 1 to g_nof_inputs - 1 loop
        nxt_reg_out_2arr_2(i)(0) <= reg_out_2arr_2(i - 1)(0);
      end loop;
      -- Bottom of carousel, shift right
      nxt_reg_out_2arr_2(0)(0) <= reg_out_2arr_2(0)(1);
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      reg_out_2arr_2 <= (others => (others => c_dp_sosi_rst));
    elsif rising_edge(clk) then
      reg_out_2arr_2 <= nxt_reg_out_2arr_2;
    end if;
  end process;
end rtl;
