-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer
-- Purpose:
--   IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;

package tech_fifo_component_pkg is
  -----------------------------------------------------------------------------
  -- ip_stratixiv
  -----------------------------------------------------------------------------

  component ip_stratixiv_fifo_sc is
  generic (
    g_use_eab    : string := "ON";
    g_dat_w      : natural;
    g_nof_words  : natural
  );
  port (
    aclr  : in std_logic;
    clock : in std_logic;
    data  : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq : in std_logic;
    wrreq : in std_logic;
    empty : out std_logic;
    full  : out std_logic;
    q     : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_stratixiv_fifo_dc is
  generic (
    g_dat_w      : natural;
    g_nof_words  : natural
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_stratixiv_fifo_dc_mixed_widths is
  generic (
    g_nof_words  : natural;  -- FIFO size in nof wr_dat words
    g_wrdat_w    : natural;
    g_rddat_w    : natural
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10
  -----------------------------------------------------------------------------

  component ip_arria10_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  -----------------------------------------------------------------------------

  component ip_arria10_e3sge3_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e3sge3_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e3sge3_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e1sg
  -----------------------------------------------------------------------------

  component ip_arria10_e1sg_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e1sg_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e1sg_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e2sg
  -----------------------------------------------------------------------------

  component ip_arria10_e2sg_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e2sg_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_arria10_e2sg_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_ultrascale
  -----------------------------------------------------------------------------

  component ip_ultrascale_fifo_sc is
  generic (
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_ultrascale_fifo_dc is
  generic (
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_ultrascale_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_agi027_1e1v
  -----------------------------------------------------------------------------

  component ip_agi027_1e1v_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;
end tech_fifo_component_pkg;
