-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker

-- Purpose:
-- . SDP info register
-- Description:
--
-- https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
--
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.sdp_pkg.all;

entity sdp_info is
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk             : in  std_logic;  -- memory-mapped bus clock

    dp_clk             : in  std_logic;
    dp_rst             : in  std_logic;

    reg_mosi           : in  t_mem_mosi;
    reg_miso           : out t_mem_miso;

    -- inputs from other blocks
    f_adc              : in std_logic;
    fsub_type          : in std_logic;

    -- sdp info
    sdp_info           : out t_sdp_info
  );
end sdp_info;

architecture str of sdp_info is
  signal sdp_info_ro: t_sdp_info := c_sdp_info_rst;  -- ro = read only
  signal block_period: std_logic_vector(15 downto 0);
begin
  u_mm_fields: entity work.sdp_info_reg
  port map (

    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    reg_mosi  => reg_mosi,
    reg_miso  => reg_miso,

    -- sdp info
    sdp_info_ro => sdp_info_ro,
    sdp_info    => sdp_info
  );

  -- f_adc    : '0' => 160M, '1' => 200M
  -- fsub_type: '0' => critical sampled PFB, '1' => oversampled PFB
  p_block_period : process(f_adc, fsub_type)
  begin
    if f_adc = '0' then
      if fsub_type = '0' then
        block_period <= TO_UVEC(6400, block_period'length);  -- 160M, critical sampled
      else
        block_period <= TO_UVEC(5400, block_period'length);  -- 160M, oversampled
      end if;
    else
      if fsub_type = '0' then
        block_period <= TO_UVEC(5120, block_period'length);  -- 200M, critical sampled
      else
        block_period <= TO_UVEC(4320, block_period'length);  -- 200M, oversampled
      end if;
    end if;
  end process;

  sdp_info_ro.f_adc              <= f_adc;
  sdp_info_ro.fsub_type          <= fsub_type;
  sdp_info_ro.block_period       <= block_period;
end str;
