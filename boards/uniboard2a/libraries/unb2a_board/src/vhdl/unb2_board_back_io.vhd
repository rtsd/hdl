------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.unb2_board_pkg.all;

entity unb2_board_back_io is
  generic (
    g_nof_back_bus : natural := c_unb2_board_tr_back.nof_bus
  );
  port (
    serial_tx_arr  : in  std_logic_vector(g_nof_back_bus * c_unb2_board_tr_back.bus_w - 1 downto 0) := (others => '0');
    serial_rx_arr  : out std_logic_vector(g_nof_back_bus * c_unb2_board_tr_back.bus_w - 1 downto 0);

    -- back transceivers
    BCK_RX       : in    t_unb2_board_back_bus_2arr(g_nof_back_bus - 1 downto 0) := (others => (others => '0'));
    BCK_TX       : out   t_unb2_board_back_bus_2arr(g_nof_back_bus - 1 downto 0);

    BCK_SDA      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0);
    BCK_SCL      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0);
    BCK_ERR      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0)
  );
end unb2_board_back_io;

architecture str of unb2_board_back_io is
  -- help signals so we can iterate through buses
  signal si_tx_2arr : t_unb2_board_back_bus_2arr(g_nof_back_bus - 1 downto 0);
  signal si_rx_2arr : t_unb2_board_back_bus_2arr(g_nof_back_bus - 1 downto 0);
begin
  gen_buses : for i in 0 to g_nof_back_bus - 1 generate
    BCK_TX(i)     <= si_tx_2arr(i);
    si_rx_2arr(i) <= BCK_RX(i);
  end generate;

  gen_wire_bus : for i in 0 to g_nof_back_bus - 1 generate
    gen_wire_signals : for j in 0 to c_unb2_board_tr_back.bus_w - 1 generate
      si_tx_2arr(i)(j) <= serial_tx_arr(i * c_unb2_board_tr_back.bus_w + j);
      serial_rx_arr(i * c_unb2_board_tr_back.bus_w + j) <= si_rx_2arr(i)(j);
    end generate;
  end generate;
end;
