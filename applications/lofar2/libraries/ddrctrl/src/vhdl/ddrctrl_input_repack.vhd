-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Resize the input data vector so that the output data vector can be
--  stored into the ddr memory.
--
-- Description:
--  The input data gets resized and put into the output data vector.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The output vector must be larger than the input vector.

library IEEE, dp_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity ddrctrl_input_repack is
  generic (
    g_tech_ddr              : t_c_tech_ddr;  -- type of memory
    g_in_data_w             : natural       := 168;  -- the input data with
    g_bim                   : natural;
    g_of_pb                 : natural;
    g_block_size            : natural
  );
  port (
    clk                     : in  std_logic;
    rst                     : in  std_logic;
    in_sosi                 : in  t_dp_sosi;  -- input data
    in_stop                 : in  std_logic := '0';
    out_sosi                : out t_dp_sosi := c_dp_sosi_init;  -- output data
    out_bsn_wr              : out std_logic := '0';
    out_data_stopped        : out std_logic := '0'
  );
end ddrctrl_input_repack;

architecture rtl of ddrctrl_input_repack is
  -- constant for readability
  constant c_out_data_w     : natural       := func_tech_ddr_ctlr_data_w( g_tech_ddr );  -- the output data with, 576
  constant c_c_v_w          : natural       := c_out_data_w * 2;  -- the c_v data with, 2*576=1152

  -- type for statemachine
  type t_state is (OVERFLOW_OUTPUT, FILL_VECTOR, FIRST_OUTPUT, RESET, STOP, BSN);

  -- record for readability
  type t_reg is record
  state                     : t_state;  -- the state the process is currently in;
  c_v                       : std_logic_vector(c_c_v_w - 1 downto 0);  -- the vector that stores the input data until the data is put into the output data vector
  c_v_count                 : natural;  -- the amount of times the c_v vector received data from the input since the last time it was filled completely
  q_bsn                     : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  q_sop                     : std_logic;
  s_input_cnt               : natural;
  out_of                    : natural;
  out_data_count            : std_logic;  -- the amount of times the output data vector has been filled since the last time c_v was filled completely
  out_sosi                  : t_dp_sosi;  -- this is the sosi stream that contains the data
  out_data_stopped          : std_logic;  -- this signal is '1' when there is no more data comming form ddrctrl_input_pack
  end record;

  constant c_t_reg_init     : t_reg         := (RESET, (others => '0'), 0, (others => '0'), '0', 0, 0, '0', c_dp_sosi_init, '0');

  -- signals for readability
  signal d_reg              : t_reg         := c_t_reg_init;
  signal q_reg              : t_reg         := c_t_reg_init;
begin
  q_reg <= d_reg when rising_edge(clk);

  -- put the input data into c_v and fill the output vector from c_v
  p_state : process(q_reg, rst, in_sosi, in_stop)

    variable v                : t_reg;
  begin
    v := q_reg;

    case q_reg.state is
    when FILL_VECTOR =>  -- if the input data doesn't exceeds the output data vector width
      v.c_v(g_in_data_w * (q_reg.c_v_count + 1) + q_reg.out_of - 1 downto g_in_data_w * q_reg.c_v_count + q_reg.out_of) := in_sosi.data(g_in_data_w - 1 downto 0);  -- fill c_v
      v.c_v_count := q_reg.c_v_count + 1;  -- increase the counter of c_v with 1
      v.out_sosi.valid := '0';  -- out_sosi.valid 0
      v.s_input_cnt := q_reg.s_input_cnt + 1;
      v.out_sosi.sop := '0';
      v.out_sosi.eop := '0';
      v.out_data_stopped := '0';

    when FIRST_OUTPUT =>  -- if the input data exceeds output data vector width but not the c_v width
      v.c_v(g_in_data_w * (q_reg.c_v_count + 1) + q_reg.out_of - 1 downto g_in_data_w * q_reg.c_v_count + q_reg.out_of) := in_sosi.data(g_in_data_w - 1 downto 0);  -- fill c_v
      v.c_v_count := q_reg.c_v_count + 1;  -- increase the counter of c_v with 1
      v.out_sosi.data(c_out_data_w - 1 downto 0) := v.c_v(c_out_data_w - 1 downto 0);  -- fill out_sosi.data with 1st part of c_v
      v.out_sosi.valid := '1';  -- out_sosi.valid 1
      v.out_data_count := '1';  -- increase the counter of out_sosi.data with 1
      v.s_input_cnt := q_reg.s_input_cnt + 1;
      v.out_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0)  := q_reg.q_bsn(c_dp_stream_bsn_w - 1 downto 0);
      v.out_sosi.sop                                := q_reg.q_sop;
      v.out_sosi.eop                                := '0';
      v.out_data_stopped                            := '0';

    when OVERFLOW_OUTPUT =>  -- if the input data exceeds the output data vector width and the c_v width
      v.out_of := q_reg.out_of + (g_in_data_w * (q_reg.c_v_count + 1)) - (c_out_data_w * 2);  -- check how much overflow there is and safe it in out_of
      v.c_v(c_c_v_w - 1 downto c_c_v_w - (g_in_data_w - v.out_of)) := in_sosi.data(g_in_data_w - v.out_of - 1 downto 0);  -- fill the rest of c_v untill the end
      v.c_v(v.out_of - 1 downto 0) := in_sosi.data(g_in_data_w - 1 downto g_in_data_w - v.out_of);  -- fill the start of c_v untill the out_of
      v.out_sosi.data(c_out_data_w - 1 downto 0) := v.c_v(c_c_v_w - 1 downto c_out_data_w);  -- fill out_sosi.data with 2nd part of c_v
      v.out_sosi.valid := '1';  -- out_sosi.valid 1
      v.c_v_count      := 0;  -- reset counter
      v.out_data_count := '0';  -- reset counter
      v.s_input_cnt := q_reg.s_input_cnt + 1;
      v.q_sop := '0';
      v.out_sosi.sop := '0';
      v.out_sosi.eop := '0';
      v.out_data_stopped := '0';

    when BSN =>

      v.c_v(c_c_v_w - 1 downto ((g_in_data_w * q_reg.c_v_count) + q_reg.out_of)) := (others => '0');
      v.out_of            := 0;
      if ((g_in_data_w * q_reg.c_v_count) + q_reg.out_of < c_out_data_w * 1) then
        v.out_sosi.data(c_out_data_w - 1 downto 0) := v.c_v(c_out_data_w - 1 downto 0);  -- fill out_sosi.data with 1st part of c_v
        v.out_sosi.valid  := '1';  -- out_sosi.valid 1
      else
        v.out_sosi.data(c_out_data_w - 1 downto 0) := v.c_v(c_c_v_w - 1 downto c_out_data_w);  -- fill out_sosi.data with 2nd part of c_v
        v.out_sosi.valid  := '1';  -- out_sosi.valid 1
      end if;

      -- BSN_INPUT
      v.q_bsn             := in_sosi.bsn;  -- a bsn number is saved when the bsn changes
      v.q_sop             := '1';  -- a signal which indicates that a bsn is written in this word(576) so the address counter can save the corresponinding address. (there are delay in address counter so in_adr is not the same as the address of the word the data from the bsn is written to)
      v.c_v(g_in_data_w - 1 downto 0) := in_sosi.data(g_in_data_w - 1 downto 0);  -- fill c_v
      v.c_v_count         := 1;  -- increase the counter of c_v with 1
      v.out_data_count    := '0';
      v.out_sosi.eop      := '1';

    when RESET =>
      v := c_t_reg_init;
      v.q_bsn(c_dp_stream_bsn_w - 1 downto 0) := in_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0);

    when STOP =>
      v.out_sosi.valid    := '0';
      v.q_sop             := '0';
      v.out_data_stopped  := '1';
    end case;

    if rst = '1' then
      v.state := RESET;
    elsif in_stop = '1' or in_sosi.valid = '0' then
      v.state := STOP;
    elsif in_sosi.eop = '1' then
      v.s_input_cnt := 0;
      v.state := BSN;
    elsif ((g_in_data_w * (v.c_v_count + 1)) + v.out_of >= c_out_data_w * 1) and (v.out_data_count = '0') then
      v.state := FIRST_OUTPUT;
    elsif ((g_in_data_w * (v.c_v_count + 1)) + v.out_of >= c_out_data_w * 2) and (v.out_data_count = '1') then
      v.state := OVERFLOW_OUTPUT;
    else
      v.state := FILL_VECTOR;
    end if;

    d_reg <= v;
  end process;

  -- fill outputs
  out_sosi          <= q_reg.out_sosi;
  out_data_stopped  <= q_reg.out_data_stopped;
end rtl;
