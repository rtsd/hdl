<?xml version="1.0" encoding="UTF-8"?>
<system name="ip_arria10_e3sge3_tse_sgmii_gx">
 <component
   name="$${FILENAME}"
   displayName="$${FILENAME}"
   version="1.0"
   description=""
   tags="INTERNAL_COMPONENT=true"
   categories="System"
   tool="QsysStandard" />
 <parameter name="bonusData"><![CDATA[bonusData 
{
   element eth_tse_0
   {
      datum _sortIndex
      {
         value = "0";
         type = "int";
      }
   }
}
]]></parameter>
 <parameter name="clockCrossingAdapter" value="HANDSHAKE" />
 <parameter name="device" value="10AX115U4F45E3SGE3" />
 <parameter name="deviceFamily" value="Arria 10" />
 <parameter name="deviceSpeedGrade" value="3" />
 <parameter name="fabricMode" value="QSYS" />
 <parameter name="generateLegacySim" value="false" />
 <parameter name="generationId" value="0" />
 <parameter name="globalResetBus" value="false" />
 <parameter name="hdlLanguage" value="VERILOG" />
 <parameter name="hideFromIPCatalog" value="true" />
 <parameter name="lockedInterfaceDefinition" value="" />
 <parameter name="maxAdditionalLatency" value="1" />
 <parameter name="projectName" value="" />
 <parameter name="sopcBorderPoints" value="false" />
 <parameter name="systemHash" value="0" />
 <parameter name="systemInfos"><![CDATA[<systemInfosDefinition>
    <connPtSystemInfos>
        <entry>
            <key>control_port</key>
            <value>
                <connectionPointName>control_port</connectionPointName>
                <suppliedSystemInfos/>
                <consumedSystemInfos>
                    <entry>
                        <key>ADDRESS_MAP</key>
                        <value>&lt;address-map&gt;&lt;slave name='control_port' start='0x0' end='0x400' /&gt;&lt;/address-map&gt;</value>
                    </entry>
                    <entry>
                        <key>ADDRESS_WIDTH</key>
                        <value>10</value>
                    </entry>
                    <entry>
                        <key>MAX_SLAVE_DATA_WIDTH</key>
                        <value>32</value>
                    </entry>
                </consumedSystemInfos>
            </value>
        </entry>
    </connPtSystemInfos>
</systemInfosDefinition>]]></parameter>
 <parameter name="testBenchDutName" value="" />
 <parameter name="timeStamp" value="0" />
 <parameter name="useTestBenchNamingPattern" value="false" />
 <instanceScript></instanceScript>
 <interface
   name="control_port"
   internal="eth_tse_0.control_port"
   type="avalon"
   dir="end">
  <port name="reg_addr" internal="reg_addr" />
  <port name="reg_busy" internal="reg_busy" />
  <port name="reg_data_in" internal="reg_data_in" />
  <port name="reg_data_out" internal="reg_data_out" />
  <port name="reg_rd" internal="reg_rd" />
  <port name="reg_wr" internal="reg_wr" />
 </interface>
 <interface
   name="control_port_clock_connection"
   internal="eth_tse_0.control_port_clock_connection"
   type="clock"
   dir="end">
  <port name="clk" internal="clk" />
 </interface>
 <interface name="mac_gmii_connection" internal="eth_tse_0.mac_gmii_connection" />
 <interface name="mac_mii_connection" internal="eth_tse_0.mac_mii_connection" />
 <interface
   name="mac_misc_connection"
   internal="eth_tse_0.mac_misc_connection"
   type="conduit"
   dir="end">
  <port name="ff_rx_a_empty" internal="ff_rx_a_empty" />
  <port name="ff_rx_a_full" internal="ff_rx_a_full" />
  <port name="ff_rx_dsav" internal="ff_rx_dsav" />
  <port name="ff_tx_a_empty" internal="ff_tx_a_empty" />
  <port name="ff_tx_a_full" internal="ff_tx_a_full" />
  <port name="ff_tx_crc_fwd" internal="ff_tx_crc_fwd" />
  <port name="ff_tx_septy" internal="ff_tx_septy" />
  <port name="rx_err_stat" internal="rx_err_stat" />
  <port name="rx_frm_type" internal="rx_frm_type" />
  <port name="tx_ff_uflow" internal="tx_ff_uflow" />
 </interface>
 <interface
   name="mac_status_connection"
   internal="eth_tse_0.mac_status_connection" />
 <interface
   name="pcs_mac_rx_clock_connection"
   internal="eth_tse_0.pcs_mac_rx_clock_connection" />
 <interface
   name="pcs_mac_tx_clock_connection"
   internal="eth_tse_0.pcs_mac_tx_clock_connection" />
 <interface
   name="pcs_ref_clk_clock_connection"
   internal="eth_tse_0.pcs_ref_clk_clock_connection"
   type="clock"
   dir="end">
  <port name="ref_clk" internal="ref_clk" />
 </interface>
 <interface
   name="receive"
   internal="eth_tse_0.receive"
   type="avalon_streaming"
   dir="start">
  <port name="ff_rx_data" internal="ff_rx_data" />
  <port name="ff_rx_dval" internal="ff_rx_dval" />
  <port name="ff_rx_eop" internal="ff_rx_eop" />
  <port name="ff_rx_mod" internal="ff_rx_mod" />
  <port name="ff_rx_rdy" internal="ff_rx_rdy" />
  <port name="ff_rx_sop" internal="ff_rx_sop" />
  <port name="rx_err" internal="rx_err" />
 </interface>
 <interface
   name="receive_clock_connection"
   internal="eth_tse_0.receive_clock_connection"
   type="clock"
   dir="end">
  <port name="ff_rx_clk" internal="ff_rx_clk" />
 </interface>
 <interface
   name="reset_connection"
   internal="eth_tse_0.reset_connection"
   type="reset"
   dir="end">
  <port name="reset" internal="reset" />
 </interface>
 <interface
   name="rx_analogreset"
   internal="eth_tse_0.rx_analogreset"
   type="conduit"
   dir="end">
  <port name="rx_analogreset" internal="rx_analogreset" />
 </interface>
 <interface
   name="rx_cal_busy"
   internal="eth_tse_0.rx_cal_busy"
   type="conduit"
   dir="end">
  <port name="rx_cal_busy" internal="rx_cal_busy" />
 </interface>
 <interface
   name="rx_cdr_refclk"
   internal="eth_tse_0.rx_cdr_refclk"
   type="clock"
   dir="end">
  <port name="rx_cdr_refclk" internal="rx_cdr_refclk" />
 </interface>
 <interface
   name="rx_digitalreset"
   internal="eth_tse_0.rx_digitalreset"
   type="conduit"
   dir="end">
  <port name="rx_digitalreset" internal="rx_digitalreset" />
 </interface>
 <interface
   name="rx_is_lockedtodata"
   internal="eth_tse_0.rx_is_lockedtodata"
   type="conduit"
   dir="end">
  <port name="rx_is_lockedtodata" internal="rx_is_lockedtodata" />
 </interface>
 <interface
   name="rx_is_lockedtoref"
   internal="eth_tse_0.rx_is_lockedtoref"
   type="conduit"
   dir="end">
  <port name="rx_is_lockedtoref" internal="rx_is_lockedtoref" />
 </interface>
 <interface
   name="rx_set_locktodata"
   internal="eth_tse_0.rx_set_locktodata"
   type="conduit"
   dir="end">
  <port name="rx_set_locktodata" internal="rx_set_locktodata" />
 </interface>
 <interface
   name="rx_set_locktoref"
   internal="eth_tse_0.rx_set_locktoref"
   type="conduit"
   dir="end">
  <port name="rx_set_locktoref" internal="rx_set_locktoref" />
 </interface>
 <interface
   name="serdes_control_connection"
   internal="eth_tse_0.serdes_control_connection"
   type="conduit"
   dir="end">
  <port name="rx_recovclkout" internal="rx_recovclkout" />
 </interface>
 <interface
   name="serial_connection"
   internal="eth_tse_0.serial_connection"
   type="conduit"
   dir="end">
  <port name="rxp" internal="rxp" />
  <port name="txp" internal="txp" />
 </interface>
 <interface
   name="status_led_connection"
   internal="eth_tse_0.status_led_connection"
   type="conduit"
   dir="end">
  <port name="led_an" internal="led_an" />
  <port name="led_char_err" internal="led_char_err" />
  <port name="led_col" internal="led_col" />
  <port name="led_crs" internal="led_crs" />
  <port name="led_disp_err" internal="led_disp_err" />
  <port name="led_link" internal="led_link" />
  <port name="led_panel_link" internal="led_panel_link" />
 </interface>
 <interface name="tbi_connection" internal="eth_tse_0.tbi_connection" />
 <interface
   name="transmit"
   internal="eth_tse_0.transmit"
   type="avalon_streaming"
   dir="end">
  <port name="ff_tx_data" internal="ff_tx_data" />
  <port name="ff_tx_eop" internal="ff_tx_eop" />
  <port name="ff_tx_err" internal="ff_tx_err" />
  <port name="ff_tx_mod" internal="ff_tx_mod" />
  <port name="ff_tx_rdy" internal="ff_tx_rdy" />
  <port name="ff_tx_sop" internal="ff_tx_sop" />
  <port name="ff_tx_wren" internal="ff_tx_wren" />
 </interface>
 <interface
   name="transmit_clock_connection"
   internal="eth_tse_0.transmit_clock_connection"
   type="clock"
   dir="end">
  <port name="ff_tx_clk" internal="ff_tx_clk" />
 </interface>
 <interface
   name="tx_analogreset"
   internal="eth_tse_0.tx_analogreset"
   type="conduit"
   dir="end">
  <port name="tx_analogreset" internal="tx_analogreset" />
 </interface>
 <interface
   name="tx_cal_busy"
   internal="eth_tse_0.tx_cal_busy"
   type="conduit"
   dir="end">
  <port name="tx_cal_busy" internal="tx_cal_busy" />
 </interface>
 <interface
   name="tx_digitalreset"
   internal="eth_tse_0.tx_digitalreset"
   type="conduit"
   dir="end">
  <port name="tx_digitalreset" internal="tx_digitalreset" />
 </interface>
 <interface
   name="tx_serial_clk"
   internal="eth_tse_0.tx_serial_clk"
   type="hssi_serial_clock"
   dir="end">
  <port name="tx_serial_clk" internal="tx_serial_clk" />
 </interface>
 <module
   name="eth_tse_0"
   kind="altera_eth_tse"
   version="16.0"
   enabled="1"
   autoexport="1">
  <parameter name="AUTO_DEVICE" value="10AX115U4F45E3SGE3" />
  <parameter name="AUTO_DEVICE_SPEEDGRADE" value="3" />
  <parameter name="core_variation" value="MAC_PCS" />
  <parameter name="deviceFamilyName" value="Arria 10" />
  <parameter name="eg_addr" value="11" />
  <parameter name="ena_hash" value="false" />
  <parameter name="enable_alt_reconfig" value="false" />
  <parameter name="enable_ecc" value="false" />
  <parameter name="enable_ena" value="32" />
  <parameter name="enable_gmii_loopback" value="false" />
  <parameter name="enable_hd_logic" value="false" />
  <parameter name="enable_mac_flow_ctrl" value="false" />
  <parameter name="enable_mac_vlan" value="false" />
  <parameter name="enable_magic_detect" value="false" />
  <parameter name="enable_ptp_1step" value="false" />
  <parameter name="enable_sgmii" value="false" />
  <parameter name="enable_shift16" value="true" />
  <parameter name="enable_sup_addr" value="false" />
  <parameter name="enable_timestamping" value="false" />
  <parameter name="enable_use_internal_fifo" value="true" />
  <parameter name="export_pwrdn" value="false" />
  <parameter name="ext_stat_cnt_ena" value="false" />
  <parameter name="ifGMII" value="MII_GMII" />
  <parameter name="ing_addr" value="11" />
  <parameter name="max_channels" value="1" />
  <parameter name="mdio_clk_div" value="40" />
  <parameter name="nf_phyip_rcfg_enable" value="false" />
  <parameter name="phy_identifier" value="0" />
  <parameter name="phyip_en_synce_support" value="false" />
  <parameter name="phyip_pll_base_data_rate" value="1250 Mbps" />
  <parameter name="phyip_pll_type" value="CMU" />
  <parameter name="phyip_pma_bonding_mode" value="x1" />
  <parameter name="starting_channel_number" value="0" />
  <parameter name="stat_cnt_ena" value="false" />
  <parameter name="transceiver_type" value="GXB" />
  <parameter name="tstamp_fp_width" value="4" />
  <parameter name="useMDIO" value="false" />
  <parameter name="use_mac_clken" value="false" />
  <parameter name="use_misc_ports" value="true" />
 </module>
 <interconnectRequirement for="$system" name="qsys_mm.clockCrossingAdapter" value="HANDSHAKE" />
 <interconnectRequirement for="$system" name="qsys_mm.insertDefaultSlave" value="FALSE" />
 <interconnectRequirement for="$system" name="qsys_mm.maxAdditionalLatency" value="1" />
</system>
