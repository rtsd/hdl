-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Active Serial Memory Interface to flash device

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_flash_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_flash_lib;
library ip_arria10_asmi_parallel_altera_asmi_parallel_150;
library ip_arria10_e3sge3_asmi_parallel_altera_asmi_parallel_151;
--LIBRARY ip_arria10_e1sg_asmi_parallel_altera_asmi_parallel_180;
--LIBRARY ip_arria10_e2sg_asmi_parallel_altera_asmi_parallel_1910;

entity tech_flash_asmi_parallel is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_sim_flash_model : boolean := false
  );
  port (
    addr          : in std_logic_vector(tech_flash_addr_w(g_technology) - 1 downto 0);
    clkin         : in std_logic;
    datain        : in std_logic_vector(7 downto 0);
    rden          : in std_logic;
    read          : in std_logic;
    sector_erase  : in std_logic;
    shift_bytes   : in std_logic;
    wren          : in std_logic;
    write         : in std_logic;
    busy          : out std_logic;
    data_valid    : out std_logic;
    dataout       : out std_logic_vector(7 downto 0);
    illegal_erase : out std_logic;
    illegal_write : out std_logic;
    reset         : in  std_logic := '0';
    sce           : in  std_logic_vector(2 downto 0) := (others => '0');
    en4b_addr     : in  std_logic := '0'

  );
end tech_flash_asmi_parallel;

architecture str of tech_flash_asmi_parallel is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_asmi_parallel
    generic map (g_sim_flash_model)
    port map (addr, clkin, datain, rden, read, sector_erase, shift_bytes, wren, write, busy, data_valid, dataout, illegal_erase, illegal_write);
  end generate;

  -- Note 1: addr must be 32 bits
  -- Note 2: need ports for reset, en4b_addr
  -- Note 3: ug_altasmi_parallel.pdf not clear what sce(2 downto 0) is for
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_asmi_parallel
    port map (addr, clkin, datain, rden, read, sector_erase, shift_bytes, wren, write, busy, data_valid, dataout, illegal_erase, illegal_write, reset, sce, en4b_addr);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_asmi_parallel
    port map (addr, clkin, datain, rden, read, sector_erase, shift_bytes, wren, write, busy, data_valid, dataout, illegal_erase, illegal_write, reset, sce, en4b_addr);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_asmi_parallel
    port map (addr, clkin, datain, rden, read, sector_erase, shift_bytes, wren, write, busy, data_valid, dataout, illegal_erase, illegal_write, reset, sce, en4b_addr);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_asmi_parallel
    port map (addr, clkin, datain, rden, read, sector_erase, shift_bytes, wren, write, busy, data_valid, dataout, illegal_erase, illegal_write, reset, sce, en4b_addr);
  end generate;
end architecture;
