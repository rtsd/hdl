-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

package i2c_dev_max1617_pkg is
  -- Also applies to MAX1618
  --                   ADD0_ADD1
  constant MAX1617_ADR_LOW_LOW           : natural := 2#0011000#;
  constant MAX1617_ADR_LOW_MID           : natural := 2#0011001#;
  constant MAX1617_ADR_LOW_HIGH          : natural := 2#0011010#;
  constant MAX1617_ADR_MID_LOW           : natural := 2#0101001#;
  constant MAX1617_ADR_MID_MID           : natural := 2#0101010#;
  constant MAX1617_ADR_MID_HIGH          : natural := 2#0101011#;
  constant MAX1617_ADR_HIGH_LOW          : natural := 2#1001100#;
  constant MAX1617_ADR_HIGH_MID          : natural := 2#1001101#;
  constant MAX1617_ADR_HIGH_HIGH         : natural := 2#1001110#;

  constant MAX1617_CMD_READ_LOCAL_TEMP   : natural := 0;
  constant MAX1617_CMD_READ_REMOTE_TEMP  : natural := 1;
  constant MAX1617_CMD_READ_STATUS       : natural := 2;
  constant MAX1617_CMD_READ_CONFIG       : natural := 3;
  constant MAX1617_CMD_READ_RATE         : natural := 4;
  constant MAX1617_CMD_READ_LOCAL_HIGH   : natural := 5;
  constant MAX1617_CMD_READ_LOCAL_LOW    : natural := 6;
  constant MAX1617_CMD_READ_REMOTE_HIGH  : natural := 7;
  constant MAX1617_CMD_READ_REMOTE_LOW   : natural := 8;

  constant MAX1617_CMD_WRITE_CONFIG      : natural := 9;
  constant MAX1617_CMD_WRITE_RATE        : natural := 10;
  constant MAX1617_CMD_WRITE_LOCAL_HIGH  : natural := 11;
  constant MAX1617_CMD_WRITE_LOCAL_LOW   : natural := 12;
  constant MAX1617_CMD_WRITE_REMOTE_HIGH : natural := 13;
  constant MAX1617_CMD_WRITE_REMOTE_LOW  : natural := 14;

  constant MAX1617_CMD_ONE_SHOT          : natural := 15;

  constant MAX1617_RATE_0_0625           : natural := 0;
  constant MAX1617_RATE_0_125            : natural := 1;
  constant MAX1617_RATE_0_25             : natural := 2;
  constant MAX1617_RATE_0_5              : natural := 3;
  constant MAX1617_RATE_1                : natural := 4;
  constant MAX1617_RATE_2                : natural := 5;
  constant MAX1617_RATE_4                : natural := 6;
  constant MAX1617_RATE_8                : natural := 7;

  constant MAX1617_CONFIG_ID_BI          : natural := 3;
  constant MAX1617_CONFIG_THERM_BI       : natural := 4;
  constant MAX1617_CONFIG_POL_BI         : natural := 5;
  constant MAX1617_CONFIG_RUN_STOP_BI    : natural := 6;
  constant MAX1617_CONFIG_MASK_BI        : natural := 7;

  constant MAX1617_CONFIG_ID             : natural := 2**MAX1617_CONFIG_ID_BI;
  constant MAX1617_CONFIG_THERM          : natural := 2**MAX1617_CONFIG_THERM_BI;
  constant MAX1617_CONFIG_POL            : natural := 2**MAX1617_CONFIG_POL_BI;
  constant MAX1617_CONFIG_RUN_STOP       : natural := 2**MAX1617_CONFIG_RUN_STOP_BI;
  constant MAX1617_CONFIG_MASK           : natural := 2**MAX1617_CONFIG_MASK_BI;

  constant MAX1617_STATUS_BUSY_BI        : natural := 7;
  constant MAX1617_STATUS_RHIGH_BI       : natural := 4;
  constant MAX1617_STATUS_RLOW_BI        : natural := 3;
  constant MAX1617_STATUS_DIODE_BI       : natural := 2;

  constant MAX1617_STATUS_BUSY           : natural := 2**MAX1617_STATUS_BUSY_BI;
  constant MAX1617_STATUS_RHIGH          : natural := 2**MAX1617_STATUS_RHIGH_BI;
  constant MAX1617_STATUS_RLOW           : natural := 2**MAX1617_STATUS_RLOW_BI;
  constant MAX1617_STATUS_DIODE          : natural := 2**MAX1617_STATUS_DIODE_BI;
end package;
