-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Discard rx frame that is not for MM master or for data onload
-- Description:
--   Early discard rx frames that cannot be handled by this FPGA node. To avoid
--   overflow of eth_buffer and to avoid too much load on MM master
--   processing when the MM master is implemented in software on Nios softcore
--   microprocessor.
-- Remark:
--   It is not sure whether there will be lots of packets for this node that
--   will be discarded, because the MAC will already block packets that are
--   not for this nodes MAC address and that are not broadcast MAC. For example
--   unsupported Ethernet types, TCP and unsupported UDP ports will be
--   discarded, but will these appear in the eth Rx data path at all?

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.eth_pkg.all;

entity eth_frm_discard is
  generic (
    g_support_dhcp       : boolean := true;
    g_support_udp_onload : boolean := false
  );
  port (
    -- Clocks and reset
    rst           : in  std_logic;
    clk           : in  std_logic;

    -- MM control
    reg_config    : in  t_eth_mm_reg_config;
    reg_demux     : in  t_eth_mm_reg_demux;

    -- ST info
    hdr_status          : in  t_eth_hdr_status;
    hdr_status_complete : in  std_logic;

    -- Frame discard decision
    frm_discard     : out std_logic;
    frm_discard_val : out std_logic
  );
end eth_frm_discard;

architecture rtl of eth_frm_discard is
  signal i_frm_discard   : std_logic;
  signal nxt_frm_discard : std_logic;
begin
  frm_discard <= i_frm_discard;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      i_frm_discard   <= '1';
      frm_discard_val <= '0';
    elsif rising_edge(clk) then
      i_frm_discard   <= nxt_frm_discard;
      frm_discard_val <= hdr_status_complete;
    end if;
  end process;

  p_frm_discard : process(i_frm_discard, hdr_status_complete, hdr_status, reg_config, reg_demux)
  begin
    nxt_frm_discard <= i_frm_discard;  -- default keep discard status (instead of '1'), to more clearly see when a change occurs frm_discard
    if hdr_status_complete = '1' then
      nxt_frm_discard <= '1';  -- default discard

      -- ARP
      if hdr_status.is_arp = '1' then
        nxt_frm_discard <= '0';  -- support ARP
      end if;

      -- IP
      if hdr_status.is_ip = '1' then
        -- IP/ICMP
        if hdr_status.is_icmp = '1' then
          nxt_frm_discard <= '0';  -- support IP/ICMP = ping
        end if;

        -- IP/UDP
        if hdr_status.is_udp = '1' then
          if g_support_dhcp = true and hdr_status.is_dhcp = '1' then
            nxt_frm_discard <= '0';  -- support IP/UDP/DHCP
          end if;
          if unsigned(hdr_status.udp_port) = unsigned(reg_config.udp_port) then
            nxt_frm_discard <= '0';  -- support IP/UDP FPGA node Monitoring and Control
          end if;
          if g_support_udp_onload = true then
            for I in 1 to c_eth_nof_udp_ports loop
              if reg_demux.udp_ports_en(I) = '1' and unsigned(reg_demux.udp_ports(I)) = unsigned(hdr_status.udp_port) then
                nxt_frm_discard <= '0';  -- support IP/UDP FPGA node streaming data onload via UDP port I
              end if;
            end loop;
          end if;
        end if;
      end if;
    end if;
  end process;
end rtl;
