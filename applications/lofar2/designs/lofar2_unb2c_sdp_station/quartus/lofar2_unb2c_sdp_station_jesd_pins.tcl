###############################################################################
#
# Copyright (C) 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

#=====================
# JESD pins
# ====================
# Pins needed for the 12 channel JESD204B interface to the ADCs

set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[3]
#set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[4]
#set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[5]
#set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[6]
#set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[7]

#set_location_assignment PIN_AC7 -to BCK_RX[23]
#set_location_assignment PIN_AB5 -to BCK_RX[22]
#set_location_assignment PIN_AE7 -to BCK_RX[21]
#set_location_assignment PIN_AD5 -to BCK_RX[20]
#set_location_assignment PIN_AG7 -to BCK_RX[19]
#set_location_assignment PIN_AF5 -to BCK_RX[18]
#set_location_assignment PIN_AJ7 -to BCK_RX[17]
#set_location_assignment PIN_AH5 -to BCK_RX[16]
#set_location_assignment PIN_AL7 -to BCK_RX[15]
#set_location_assignment PIN_AK5 -to BCK_RX[14]
#set_location_assignment PIN_AN7 -to BCK_RX[13]
#set_location_assignment PIN_AM5 -to BCK_RX[12]
set_location_assignment PIN_AR7 -to BCK_RX[11]
set_location_assignment PIN_AP5 -to BCK_RX[10]
set_location_assignment PIN_AU7 -to BCK_RX[9]
set_location_assignment PIN_AT5 -to BCK_RX[8]
set_location_assignment PIN_AW7 -to BCK_RX[7]
set_location_assignment PIN_AV5 -to BCK_RX[6]
set_location_assignment PIN_BA7 -to BCK_RX[5]
set_location_assignment PIN_AY5 -to BCK_RX[4]
set_location_assignment PIN_BC7 -to BCK_RX[3]
set_location_assignment PIN_BB5 -to BCK_RX[2]
set_location_assignment PIN_AY9 -to BCK_RX[1]
set_location_assignment PIN_BB9 -to BCK_RX[0]

# Set link type to Long Reach (LR) for Backplane communication.
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_LINK LR -to BCK_RX[0]

# Set Equalizer to high gain mode.
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to BCK_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[11]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[12]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[12]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[13]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[13]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[14]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[14]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[15]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[15]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[16]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[16]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[17]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[17]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[18]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[18]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[19]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[19]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[20]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[20]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[21]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[21]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[22]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[22]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[23]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[23]

# Substitute new signal names from the jesd_simple design
set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to BCK_REF_CLK
set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"

set_instance_assignment -name IO_STANDARD LVDS -to JESD204B_SYSREF
set_instance_assignment -name IO_STANDARD LVDS -to "JESD204B_SYSREF(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to JESD204B_SYSREF
set_location_assignment PIN_Y13 -to JESD204B_SYSREF
set_location_assignment PIN_Y12 -to "JESD204B_SYSREF(n)"

set_location_assignment PIN_AD12 -to JESD204B_SYNC_N[0]
set_location_assignment PIN_AC13 -to JESD204B_SYNC_N[1]
set_location_assignment PIN_AA13 -to JESD204B_SYNC_N[2]
set_location_assignment PIN_AA12 -to JESD204B_SYNC_N[3]
#set_location_assignment PIN_V14  -to JESD204B_SYNC_N[4]
#set_location_assignment PIN_V12  -to JESD204B_SYNC_N[5]
#set_location_assignment PIN_U14  -to JESD204B_SYNC_N[6]
#set_location_assignment PIN_R13  -to JESD204B_SYNC_N[7]
