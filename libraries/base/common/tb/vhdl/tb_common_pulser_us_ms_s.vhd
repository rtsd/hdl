-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_pulser_us_ms_s
-- Description:
--   The tb checks that the pulse_us occurs aligned when pulse_ms is active
--   and when pulse_s is active.
--   The sync is used to restart the us, ms, s intervals.
-- Usage:
--   > as 3
--   > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_pulser_us_ms_s is
end tb_common_pulser_us_ms_s;

architecture tb of tb_common_pulser_us_ms_s is
  constant c_pulse_us   : natural := 10;
  constant c_1000       : natural := 10;  -- use eg 10 instead of 1000 to speed up simulation

  constant clk_period   : time := 1000 ns / c_pulse_us;

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '0';
  signal sync           : std_logic := '0';

  signal pulse_us       : std_logic;
  signal pulse_ms       : std_logic;
  signal pulse_s        : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  p_sync : process
  begin
    sync <= '0';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait for (c_1000 * c_1000 * 1 us / 4);
    wait until rising_edge(clk);
    sync <= '1';
    wait until rising_edge(clk);
    sync <= '0';
    wait until rising_edge(clk);
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    wait until pulse_s = '1';
    tb_end <= '1';
    wait for (c_1000 * c_1000 * 1 us);
    wait;
  end process;

  p_verify: process(clk)
  begin
    if rising_edge(clk) then
      if pulse_s = '1' then assert pulse_ms = '1' and pulse_us = '1' report "Error: pulse_us, ms, s misaligned" severity ERROR; end if;
      if pulse_ms = '1' then assert                  pulse_us = '1' report "Error: pulse_ms, s misaligned"     severity ERROR; end if;
    end if;
  end process;

  u_common_pulser_us_ms_s : entity work.common_pulser_us_ms_s
  generic map (
    g_pulse_us   => c_pulse_us,  -- nof clk cycles to get us period
    g_pulse_ms   => c_1000,  -- nof pulse_us pulses to get ms period
    g_pulse_s    => c_1000  -- nof pulse_ms pulses to get s period
  )
  port map (
    rst          => rst,
    clk          => clk,
    sync         => sync,
    pulse_us     => pulse_us,  -- pulses after every g_pulse_us                      clock cycles
    pulse_ms     => pulse_ms,  -- pulses after every g_pulse_us*g_pulse_ms           clock cycles
    pulse_s      => pulse_s  -- pulses after every g_pulse_us*g_pulse_ms*g_pulse_s clock cycles
  );
end tb;
