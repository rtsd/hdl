-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi testbench for tr_nonbonded
-- Description:
-- Usage:
--   > as 5
--   > run -all

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_tb_tr_nonbonded is
end tb_tb_tr_nonbonded;

architecture tb of tb_tb_tr_nonbonded is
  constant c_tb_end_vec : std_logic_vector(7 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(7 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
  -- g_tb_end    : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
  -- g_data_w    : NATURAL := 32;
  -- g_sim_level : NATURAL := 1

  u_tech : entity work.tb_tr_nonbonded generic map (false, 32, 0) port map (tb_end_vec(0));
  u_sim  : entity work.tb_tr_nonbonded generic map (false, 32, 1) port map (tb_end_vec(1));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec or c_tech_select_default /= c_tech_stratixiv  else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    assert (c_tech_select_default = c_tech_stratixiv)
      report "Technology is not stratixiv, skipping testbench..."
      severity WARNING;
    wait for 1 ms;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
