-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--  1) Verify unb1_board_mesh_reorder_bidir for SOSI using the unb1_board_mesh_model_sosi
--  2) Verify unb1_board_mesh_reorder_bidir for SISO using the unb1_board_mesh_model_siso
--  3) Verify the serial unb1_board_mesh_model_sl
-- Usage:
--  > do wave_unb1_board_mesh_reorder_bidir.do
--  > run -all
--  1) Self check SOSI should report no bn_rx_valid[] or fn_rx_valid[] error
--  2) Self check SISO should report no bn_tx_ready[] or fn_tx_ready[] error
--  3) Self check unb1_board_mesh_model_sl operates at PHY level using the
--     SOSI.valid as stimuli and the SOSI.valid result as reference and should
--     report no error.
--  . Manually observe that bn_rx_valid and fn_rx_valid show a series of pulses
-- Description:
--  . See unb1_board_mesh_reorder_bidir.vhd
--  . Use c_reorder=FALSE for a default FN to BN and BN to BN transpose without
--    reordering on the PCB. Default use c_reorder=TRUE to also model the
--    transceiver bus reorderings on UniBoard PCB.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;
use work.tb_unb1_board_pkg.all;

entity tb_unb1_board_mesh_reorder_bidir is
end tb_unb1_board_mesh_reorder_bidir;

architecture tb of tb_unb1_board_mesh_reorder_bidir is
  constant c_reorder      : boolean := true;

  constant c_chip_id_w    : natural := c_unb1_board_aux.chip_id_w;  -- = 3 to fit 8 fpgas in [2:0]
  constant c_nof_node     : natural := c_unb1_board_nof_node;  -- = 4
  constant c_nof_bus      : natural := c_unb1_board_tr.nof_bus;  -- = 4
  constant c_bus_w        : natural := c_unb1_board_tr.bus_w;  -- = 4

  constant c_repeat       : natural := 3;
  constant c_clk_period   : time := 10 ns;

  signal tb_end                 : std_logic := '0';
  signal clk                    : std_logic := '0';

  ------------------------------------------------------------------------------
  -- SL
  ------------------------------------------------------------------------------
  -- . PHY signals to mesh
  signal fn_tx_phy_sl_3arr      : t_unb1_board_mesh_sl_3arr;
  signal bn_tx_phy_sl_3arr      : t_unb1_board_mesh_sl_3arr;

  -- . PHY signals from mesh
  signal fn_rx_phy_sl_3arr      : t_unb1_board_mesh_sl_3arr;
  signal fn_rx_phy_sl           : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal fn_rx_phy_valid        : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_rx_phy_sl_3arr      : t_unb1_board_mesh_sl_3arr;
  signal bn_rx_phy_sl           : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_rx_phy_valid        : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- SOSI
  ------------------------------------------------------------------------------
  -- . Monitoring signals
  signal fn_tx_valid            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_tx_valid            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal fn_rx_valid            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_rx_valid            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);

  -- . User domain driving stimuli signals
  signal fn_tx_usr_sosi_3arr    : t_unb1_board_mesh_sosi_3arr := (others => (others => (others => c_dp_sosi_rst)));
  signal bn_tx_usr_sosi_3arr    : t_unb1_board_mesh_sosi_3arr := (others => (others => (others => c_dp_sosi_rst)));

  -- . PHY signals to mesh
  signal fn_tx_phy_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;
  signal bn_tx_phy_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;

  -- . PHY signals from mesh
  signal fn_rx_phy_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;
  signal bn_rx_phy_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;

  -- . User domain result signals
  signal fn_rx_usr_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;
  signal bn_rx_usr_sosi_3arr    : t_unb1_board_mesh_sosi_3arr;

  ------------------------------------------------------------------------------
  -- SISO
  ------------------------------------------------------------------------------
  -- . Monitoring signals
  signal fn_rx_ready            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_rx_ready            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal fn_tx_ready            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);
  signal bn_tx_ready            : std_logic_vector(c_nof_node * c_nof_bus * c_bus_w - 1 downto 0);

  -- . User domain result signals
  signal fn_tx_usr_siso_3arr    : t_unb1_board_mesh_siso_3arr;
  signal bn_tx_usr_siso_3arr    : t_unb1_board_mesh_siso_3arr;

  -- . PHY signals from mesh
  signal fn_tx_phy_siso_3arr    : t_unb1_board_mesh_siso_3arr;
  signal bn_tx_phy_siso_3arr    : t_unb1_board_mesh_siso_3arr;

  -- . PHY signals to mesh
  signal fn_rx_phy_siso_3arr    : t_unb1_board_mesh_siso_3arr;
  signal bn_rx_phy_siso_3arr    : t_unb1_board_mesh_siso_3arr;

  -- . User domain driving stimuli signals
  signal fn_rx_usr_siso_3arr    : t_unb1_board_mesh_siso_3arr := (others => (others => (others => c_dp_siso_rst)));
  signal bn_rx_usr_siso_3arr    : t_unb1_board_mesh_siso_3arr := (others => (others => (others => c_dp_siso_rst)));
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;

  ------------------------------------------------------------------------------
  -- Stimuli
  ------------------------------------------------------------------------------

  p_stimuli : process
  begin
    proc_common_wait_some_cycles(clk, 100);
    for R in 0 to c_repeat - 1 loop
      -- Sequentially set a '1' pulse on every sosi.valid and and every
      -- siso.ready then check that these get accross the UniBoard mesh in the
      -- correct order by comparing the output pattern with the (delayed)
      -- input pattern.

      -- FN
      for I in 0 to c_nof_node-1 loop
        for J in 0 to c_nof_bus - 1 loop
          for K in 0 to c_bus_w - 1 loop
            fn_tx_usr_sosi_3arr(I)(J)(K).valid <= '1';
            fn_rx_usr_siso_3arr(I)(J)(K).ready <= '1';
            proc_common_wait_some_cycles(clk, 1);
            fn_tx_usr_sosi_3arr(I)(J)(K).valid <= '0';
            fn_rx_usr_siso_3arr(I)(J)(K).ready <= '0';
          end loop;
        end loop;
      end loop;

      -- BN
      for I in 0 to c_nof_node-1 loop
        for J in 0 to c_nof_bus - 1 loop
          for K in 0 to c_bus_w - 1 loop
            bn_tx_usr_sosi_3arr(I)(J)(K).valid <= '1';
            bn_rx_usr_siso_3arr(I)(J)(K).ready <= '1';
            proc_common_wait_some_cycles(clk, 1);
            bn_tx_usr_sosi_3arr(I)(J)(K).valid <= '0';
            bn_rx_usr_siso_3arr(I)(J)(K).ready <= '0';
          end loop;
        end loop;
      end loop;
    end loop;

    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Monitor and verify
  ------------------------------------------------------------------------------

  gen_node : for I in 0 to c_nof_node-1 generate
    gen_bus : for J in 0 to c_nof_bus - 1 generate
      gen_lanes : for K in c_bus_w - 1 downto 0 generate
        -- SOSI
        -- . Transmit order
        fn_tx_valid((I * c_nof_bus + J) * c_bus_w + K) <= fn_tx_usr_sosi_3arr(I)(J)(K).valid;  -- FN
        bn_tx_valid((I * c_nof_bus + J) * c_bus_w + K) <= bn_tx_usr_sosi_3arr(I)(J)(K).valid;  -- BN
        -- . Receive order (compensate for the transpose)
        fn_rx_valid((J * c_nof_bus + I) * c_bus_w + K) <= fn_rx_usr_sosi_3arr(I)(J)(K).valid;  -- FN
        bn_rx_valid((J * c_nof_bus + I) * c_bus_w + K) <= bn_rx_usr_sosi_3arr(I)(J)(K).valid;  -- BN

        -- SISO
        -- . Transmit order
        fn_rx_ready((I * c_nof_bus + J) * c_bus_w + K) <= fn_rx_usr_siso_3arr(I)(J)(K).ready;  -- FN
        bn_rx_ready((I * c_nof_bus + J) * c_bus_w + K) <= bn_rx_usr_siso_3arr(I)(J)(K).ready;  -- BN
        -- . Receive order (compensate for the transpose)
        fn_tx_ready((J * c_nof_bus + I) * c_bus_w + K) <= fn_tx_usr_siso_3arr(I)(J)(K).ready;  -- FN
        bn_tx_ready((J * c_nof_bus + I) * c_bus_w + K) <= bn_tx_usr_siso_3arr(I)(J)(K).ready;  -- BN
      end generate;
    end generate;
  end generate;

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      -- SOSI
      assert fn_tx_valid = bn_rx_valid
        report "Mesh bn_rx_valid error"
        severity ERROR;
      assert bn_tx_valid = fn_rx_valid
        report "Mesh fn_rx_valid error"
        severity ERROR;
      -- SISO
      assert fn_rx_ready = bn_tx_ready
        report "Mesh bn_tx_ready error"
        severity ERROR;
      assert bn_rx_ready = fn_tx_ready
        report "Mesh fn_tx_ready error"
        severity ERROR;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- UniBoard FN0,1,2,3
  ------------------------------------------------------------------------------

  gen_fn : for I in 0 to c_nof_node-1 generate
    u_order : entity work.unb1_board_mesh_reorder_bidir
    generic map (
      g_node_type => e_fn,
      g_reorder   => c_reorder
    )
    port map (
      chip_id          => TO_UVEC(I, c_chip_id_w),  -- chip id 0, 1, 2, 3

      -- Transmit clock domain
      tx_clk           => clk,
      tx_usr_sosi_2arr => fn_tx_usr_sosi_3arr(I),  -- user sosi to phy = sosi.valid driver from FN user
      tx_usr_siso_2arr => fn_tx_usr_siso_3arr(I),  -- user siso from phy = siso.ready result to FN user
      tx_phy_sosi_2arr => fn_tx_phy_sosi_3arr(I),  -- phy sosi to mesh
      tx_phy_siso_2arr => fn_tx_phy_siso_3arr(I),  -- phy siso from mesh

      -- Receive clock domain
      rx_clk           => clk,
      rx_phy_sosi_2arr => fn_rx_phy_sosi_3arr(I),  -- phy sosi from mesh
      rx_phy_siso_2arr => fn_rx_phy_siso_3arr(I),  -- phy siso to mesh
      rx_usr_sosi_2arr => fn_rx_usr_sosi_3arr(I),  -- user sosi from phy = sosi.valid result to FN user
      rx_usr_siso_2arr => fn_rx_usr_siso_3arr(I)  -- user siso to phy = siso.ready driver from FN user
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Mesh between FN0,1,2,3 and BN0,1,2,3 as it is wired on the UniBoard PCB
  ------------------------------------------------------------------------------

  -- >>> unb1_board_mesh_model_sl

  -- Use tx_phy SOSI.valid stimuli for input to unb1_board_mesh_model_sl
  gen_tx_serial : for I in 0 to c_nof_node-1 generate
    gen_bus : for J in 0 to c_nof_bus - 1 generate
      gen_lanes : for K in c_bus_w - 1 downto 0 generate
        fn_tx_phy_sl_3arr(I)(J)(K) <= fn_tx_phy_sosi_3arr(I)(J)(K).valid;
        bn_tx_phy_sl_3arr(I)(J)(K) <= bn_tx_phy_sosi_3arr(I)(J)(K).valid;
      end generate;
    end generate;
  end generate;

  u_pcb_mesh_serial : entity work.unb1_board_mesh_model_sl
  generic map (
    g_reorder => c_reorder
  )
  port map (
    -- FN to BN
    fn_tx_sl_3arr  => fn_tx_phy_sl_3arr,
    bn_rx_sl_3arr  => bn_rx_phy_sl_3arr,

    -- BN to FN
    bn_tx_sl_3arr  => bn_tx_phy_sl_3arr,
    fn_rx_sl_3arr  => fn_rx_phy_sl_3arr
  );

  -- Use rx_phy SOSI.valid as reference output to verify output of unb1_board_mesh_model_sl
  mon_rx_serial : for I in 0 to c_nof_node-1 generate
    gen_bus : for J in 0 to c_nof_bus - 1 generate
      gen_lanes : for K in c_bus_w - 1 downto 0 generate
        -- Monitor SOSI valids in SLV
        bn_rx_phy_valid((I * c_nof_bus + J) * c_bus_w + K) <= bn_rx_phy_sosi_3arr(I)(J)(K).valid;
        fn_rx_phy_valid((I * c_nof_bus + J) * c_bus_w + K) <= fn_rx_phy_sosi_3arr(I)(J)(K).valid;
        -- Monitor SL valids in SLV
        bn_rx_phy_sl((I * c_nof_bus + J) * c_bus_w + K) <= bn_rx_phy_sl_3arr(I)(J)(K);
        fn_rx_phy_sl((I * c_nof_bus + J) * c_bus_w + K) <= fn_rx_phy_sl_3arr(I)(J)(K);
      end generate;
    end generate;
  end generate;

  p_verify_serial : process(clk)
  begin
    if rising_edge(clk) then
      assert bn_rx_phy_sl = bn_rx_phy_valid
        report "unb1_board_mesh_model_sl FN->BN error"
        severity ERROR;
      assert fn_rx_phy_sl = fn_rx_phy_valid
        report "unb1_board_mesh_model_sl BN->FN error"
        severity ERROR;
    end if;
  end process;

  -- >>> unb1_board_mesh_model_sosi

  u_pcb_mesh_sosi : entity work.unb1_board_mesh_model_sosi
  generic map (
    g_reorder => c_reorder
  )
  port map (
    -- FN to BN
    fn0_tx_sosi_2arr  => fn_tx_phy_sosi_3arr(0),
    fn1_tx_sosi_2arr  => fn_tx_phy_sosi_3arr(1),
    fn2_tx_sosi_2arr  => fn_tx_phy_sosi_3arr(2),
    fn3_tx_sosi_2arr  => fn_tx_phy_sosi_3arr(3),

    bn0_rx_sosi_2arr  => bn_rx_phy_sosi_3arr(0),
    bn1_rx_sosi_2arr  => bn_rx_phy_sosi_3arr(1),
    bn2_rx_sosi_2arr  => bn_rx_phy_sosi_3arr(2),
    bn3_rx_sosi_2arr  => bn_rx_phy_sosi_3arr(3),

    -- BN to FN
    bn0_tx_sosi_2arr  => bn_tx_phy_sosi_3arr(0),
    bn1_tx_sosi_2arr  => bn_tx_phy_sosi_3arr(1),
    bn2_tx_sosi_2arr  => bn_tx_phy_sosi_3arr(2),
    bn3_tx_sosi_2arr  => bn_tx_phy_sosi_3arr(3),

    fn0_rx_sosi_2arr  => fn_rx_phy_sosi_3arr(0),
    fn1_rx_sosi_2arr  => fn_rx_phy_sosi_3arr(1),
    fn2_rx_sosi_2arr  => fn_rx_phy_sosi_3arr(2),
    fn3_rx_sosi_2arr  => fn_rx_phy_sosi_3arr(3)
  );

  -- >>> unb1_board_mesh_model_siso

  u_pcb_mesh_siso : entity work.unb1_board_mesh_model_siso
  generic map (
    g_reorder => c_reorder
  )
  port map (
    -- FN to BN
    fn0_rx_siso_2arr  => fn_rx_phy_siso_3arr(0),
    fn1_rx_siso_2arr  => fn_rx_phy_siso_3arr(1),
    fn2_rx_siso_2arr  => fn_rx_phy_siso_3arr(2),
    fn3_rx_siso_2arr  => fn_rx_phy_siso_3arr(3),

    bn0_tx_siso_2arr  => bn_tx_phy_siso_3arr(0),
    bn1_tx_siso_2arr  => bn_tx_phy_siso_3arr(1),
    bn2_tx_siso_2arr  => bn_tx_phy_siso_3arr(2),
    bn3_tx_siso_2arr  => bn_tx_phy_siso_3arr(3),

    -- BN to FN
    bn0_rx_siso_2arr  => bn_rx_phy_siso_3arr(0),
    bn1_rx_siso_2arr  => bn_rx_phy_siso_3arr(1),
    bn2_rx_siso_2arr  => bn_rx_phy_siso_3arr(2),
    bn3_rx_siso_2arr  => bn_rx_phy_siso_3arr(3),

    fn0_tx_siso_2arr  => fn_tx_phy_siso_3arr(0),
    fn1_tx_siso_2arr  => fn_tx_phy_siso_3arr(1),
    fn2_tx_siso_2arr  => fn_tx_phy_siso_3arr(2),
    fn3_tx_siso_2arr  => fn_tx_phy_siso_3arr(3)
  );

  ------------------------------------------------------------------------------
  -- UniBoard BN0,1,2,3
  ------------------------------------------------------------------------------

  gen_bn : for I in 0 to c_nof_node-1 generate
    u_order : entity work.unb1_board_mesh_reorder_bidir
    generic map (
      g_node_type => e_bn,
      g_reorder   => c_reorder
    )
    port map (
      chip_id          => TO_UVEC(c_nof_node + I, c_chip_id_w),  -- chip id 4, 5, 6, 7

      -- Transmit clock domain
      tx_clk           => clk,
      tx_usr_sosi_2arr => bn_tx_usr_sosi_3arr(I),  -- user sosi to phy = sosi.valid driver from BN user
      tx_usr_siso_2arr => bn_tx_usr_siso_3arr(I),  -- user siso from phy = siso.ready result to BN user
      tx_phy_sosi_2arr => bn_tx_phy_sosi_3arr(I),  -- phy sosi to mesh
      tx_phy_siso_2arr => bn_tx_phy_siso_3arr(I),  -- phy siso from mesh

      -- Receive clock domain
      rx_clk           => clk,
      rx_phy_sosi_2arr => bn_rx_phy_sosi_3arr(I),  -- phy sosi from mesh
      rx_phy_siso_2arr => bn_rx_phy_siso_3arr(I),  -- phy siso to mesh
      rx_usr_sosi_2arr => bn_rx_usr_sosi_3arr(I),  -- user sosi from phy = sosi.valid result to BN user
      rx_usr_siso_2arr => bn_rx_usr_siso_3arr(I)  -- user siso to phy = siso.ready driver from BN user
    );
  end generate;
end tb;
