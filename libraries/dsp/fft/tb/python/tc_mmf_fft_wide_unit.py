#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the fft_wide_unit

   This testcase is used in conjunction with the tb_mmf_fft_wide_unit.vhd file. 
   
   The script starts modelsim and loads the tb_mmf_fft_wide_unit simulation configuration.
   Generics for the VHDL testbench are parsed to modelsim to configure the 
   current simulation. 
   
   The script reads back the spectrums from the databuffers and the statistics 
   from the subband statistics module with the fft_wide_unit.    
   
   The spectrums are integrated and plotted with the statistics data and the 
   python reference data. 
   
   Stimuli data is generated and written to the VHDL testbench. The script waits 
   until there is data available in the VHDL databuffers and then reads it.
   
   Python also calculates a reference FFT, based on the floating point FFT of 
   the scipy library. 
   
   From both the VHDL and Pyhton spectrums the SNR is estimated and printed. 
   
   The input data and all spectrums(VHDL raw, VHDL stats and python reference)
   are plotted when c_plot_enable is set to 1.
   
   REMARKS: 
   
   .c_nof chan > 0 is not (yet) supported icw wideband FFT
   

"""

###############################################################################
# System imports
import test_case
import node_io
import unb_apertif as apr
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_st_sst
import dsp_test  
import os       
import subprocess  
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *

###############################################################################

# Constants/Generics that are shared between VHDL and Python
c_nof_points       = 1024    # The size of the FFT
c_wb_factor        = 4       # The Wideband factor
c_nof_chan         = 0       # The exponent (for 2) that defines the number of time-multiplexed input channels
c_nof_integrations = 4       # The number of spectrums that are averaged
c_in_dat_w         = 8       # Input width of the FFT
c_out_dat_w        = 14      # Output width of the FFT
c_use_separate     = 'FALSE' # When FALSE: FFT input is considered Complex. When TRUE: FFT input is 2xReal

# Check what type of FFT is required. 
if(c_nof_points == c_wb_factor):
  c_fft_type = "par"
elif(c_wb_factor == 1):
  c_fft_type = "pipe"
else:  
  c_fft_type = "wide"  

# Build a dictionary of all VHDL generics
vhdl_generics ={'g_use_separate':c_use_separate, 'g_fft_type':c_fft_type, 'g_nof_points':c_nof_points, 'g_wb_factor':c_wb_factor, 'g_nof_chan':c_nof_chan, 'g_nof_integrations':c_nof_integrations, 'g_in_dat_w':c_in_dat_w, 'g_out_dat_w':c_out_dat_w}

# Python specific constants  
c_nof_input_channels = 2**c_nof_chan  
c_nof_streams        = c_wb_factor
c_frame_size         = c_nof_input_channels*c_nof_points/c_wb_factor
c_ram_size           = c_nof_integrations*c_frame_size
c_blocks_per_sync    = c_nof_integrations
c_stimuli_length     = c_nof_integrations*c_nof_input_channels*c_nof_points  
c_nof_regs_per_stat  = 2
c_file_name          = "fft_" + str(c_fft_type) + "_N" + str(c_nof_points) + "_inw" + str(c_in_dat_w) + "_outw" + str(c_out_dat_w) + "_out"
c_do_file_name       = "ms_do.do"          
c_vhdl_lib_name      = "fft"
c_vhdl_tb_name       = "tb_mmf_fft_unit"
c_plot_enable        = 1
c_write_to_file      = 1 
c_start_modelsim     = 1

# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for fft_wide_unit entity')
tc.append_log(3, '>>>')
tc.append_log(3, '')

# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_nof_streams, tc.nodeBnNrs, ramSizePerChannel=c_ram_size)

# Create databuffer instances
db_re = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'REAL', nofStreams=c_nof_streams, nodeNr=tc.nodeBnNrs, ramSizePerStream=c_stimuli_length/c_wb_factor)
db_im = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'IMAG', nofStreams=c_nof_streams, nodeNr=tc.nodeBnNrs, ramSizePerStream=c_stimuli_length/c_wb_factor)

# Create statistics instance
st = pi_st_sst.PiStSst(tc, io, c_wb_factor, c_frame_size, c_nof_regs_per_stat)

# Create dsp_test instance for helpful methods
dsp_test = dsp_test.DspTest(c_nof_points, c_in_dat_w, c_out_dat_w)

# Create shell object to start modelsim
if(c_start_modelsim == 1):
  # Create the .do file for modelsim 
  io.simIO.make_do_file(c_do_file_name, c_vhdl_lib_name, c_vhdl_tb_name, vhdl_generics)

  # Clear the files from the /python/sim directory 
  io.simIO.clean_sim_dir()

  # Create command for modelsim and start it
  vsim_cmd = "vsim -c -do " + c_do_file_name 
  ms_cmd = "xterm -geometry 160 -rightbar -sl 10000 -e " + vsim_cmd  
  ms = subprocess.Popen(ms_cmd, shell=True, close_fds=True)
  
  # Wait until modelsim is up
  io.simIO.wait_for_sim()    

# After 100 ns mm_rst is deasserted for sure, so we can perform MM accesses.
if tc.sim==True:
    do_until_gt(io.simIO.getSimTime, 100, s_timeout=3600)
    
# Write setting for the block generator:
bg.write_block_gen_settings(samplesPerPacket=c_frame_size, blocksPerSync=c_blocks_per_sync, gapSize=0, memLowAddr=0, memHighAddr=c_ram_size-1, BSNInit=30)

# Prepare data stimuli
# Create a real input signal. Imaginary input remains zero. 
xf_real_array=[]
for i in range(c_nof_input_channels):       
  xf_real_array.append(dsp_test.create_real_sinewave(ampl=1.0, phase=0.0, freq=16+12*i, noiseLevel=0, length=c_stimuli_length))

xf_real = []
for i in range(c_nof_integrations*c_nof_points):          
  for j in range(c_nof_input_channels):          
    xf_real.append(xf_real_array[j][i])
   
xf_imag = []
for i in range(c_stimuli_length): 
  xf_imag.append(0)

bg_vhdl_data = dsp_test.concatenate_two_lists(xf_real, xf_imag, c_in_dat_w)

# Write the stimuli to the block generator and enable the block generator         
for i in range(c_wb_factor):       
  send_data = []
  for j in range(c_stimuli_length/c_wb_factor):
    send_data.append(bg_vhdl_data[c_wb_factor*j+i])     #
  bg.write_waveform_ram(data=send_data, channelNr= i)
bg.write_enable()  

# Poll the databuffer to check if the response is there. 
# Retry after 3 seconds so we don't issue too many MM reads in case of simulation.
do_until_ge(db_re.read_nof_writes, ms_retry=3000, val=2, s_timeout=3600)

# Read out the statistics
stats_all = []
for i in range(c_wb_factor):       
  stats_all.append(st.read_stats(i))
stats_all = flatten(stats_all)

# Reorganize the statistics.   
stats_array = []
for h in range(c_nof_input_channels): 
  stats_chn = []
  for i in range(c_wb_factor):       
    stats_chn.append(stats_all[i*c_frame_size + h*c_nof_points/c_wb_factor:i*c_frame_size + (h+1)*c_nof_points/c_wb_factor])
  stats_array.append(flatten(stats_chn))

# Read the spectrums from the databuffer
Xqqq_data_real = []
Xqqq_data_imag = []
for i in range(c_wb_factor):       
  Xqqq_data_real.append(db_re.read_data_buffer(streamNr=i, n=c_stimuli_length/c_wb_factor))
  Xqqq_data_imag.append(db_im.read_data_buffer(streamNr=i, n=c_stimuli_length/c_wb_factor))

Xqqq_data_real = flatten(Xqqq_data_real)
Xqqq_data_imag = flatten(Xqqq_data_imag)

# Create array with spectrums that can be indexed like : array[channel_nr][integration_nr][points_nr]
Xqqq_data_real_array = []
Xqqq_data_imag_array = []
for h in range(c_nof_input_channels): 
  integrations_real = []
  integrations_imag = []
  for i in range(c_nof_integrations):
    spectrum_real = []
    spectrum_imag = []
    for j in range(c_wb_factor):                    
      for k in range(c_nof_points/c_wb_factor):     
        spectrum_real.append(Xqqq_data_real[j*c_stimuli_length/c_wb_factor + h*c_nof_points/c_wb_factor + i*c_nof_input_channels*c_nof_points/c_wb_factor + k])
        spectrum_imag.append(Xqqq_data_imag[j*c_stimuli_length/c_wb_factor + h*c_nof_points/c_wb_factor + i*c_nof_input_channels*c_nof_points/c_wb_factor + k])
    integrations_real.append(spectrum_real)
    integrations_imag.append(spectrum_imag)
  Xqqq_data_real_array.append(integrations_real)
  Xqqq_data_imag_array.append(integrations_imag)

# Mask the data and create a list with complex values
Xqqq_data_complex_array = []
for h in range(c_nof_input_channels):          
  Xqqq_data_complex = []
  for i in range(c_nof_integrations):
    Xqqq_data_complex.append(dsp_test.create_complex_list(Xqqq_data_real_array[h][i], Xqqq_data_imag_array[h][i], c_out_dat_w))
  Xqqq_data_complex_array.append(Xqqq_data_complex)
  
# Estimate the reference FFTs with python
Xfff_data_complex_array = []
for h in range(c_nof_input_channels):          
  Xfff_data_complex = []
  for i in range(c_nof_integrations):  
    fft_in_data = xf_real_array[h][i*c_nof_points:(i+1)*c_nof_points] 
    spectrum = sp.fft(fft_in_data, c_nof_points)
    Xfff_data_complex.append(spectrum.tolist())
  Xfff_data_complex_array.append(Xfff_data_complex)

# Estimate the average VHDL 
Xqqq_data_complex_avg_array = []                   
for h in range(c_nof_input_channels):          
  Xqqq_data_complex_avg = []
  for i in range(c_nof_points):   
    avrg = complex(0,0)
    for j in range(c_nof_integrations):
      avrg = avrg + Xqqq_data_complex_array[h][j][i]
    Xqqq_data_complex_avg.append(avrg/c_nof_integrations)
  Xqqq_data_complex_avg_array.append(Xqqq_data_complex_avg)

# Estimate the average VHDL Statistics 
pow_stats_array = []                   
for h in range(c_nof_input_channels):          
  stats_avg = []
  for i in range(c_nof_points):   
    stats_avg.append(10*sp.log10(stats_array[h][i]/c_blocks_per_sync+1))
  pow_stats_array.append(stats_avg)

# Estimate the average Python reference
Xfff_data_complex_avg_array = []                   
for h in range(c_nof_input_channels):          
  Xfff_data_complex_avg = []
  for i in range(c_nof_points):   
    avrg = complex(0,0)
    for j in range(c_nof_integrations):
      avrg = avrg + Xfff_data_complex_array[h][j][i]
    Xfff_data_complex_avg.append(avrg/c_nof_integrations)  
  Xfff_data_complex_avg_array.append(Xfff_data_complex_avg)

# Estimate the powers    
pow_Xqqq_data_array = []
pow_Xfff_data_array = []
for h in range(c_nof_input_channels):          
  pow_Xqqq_data_array.append(dsp_test.calc_power(Xqqq_data_complex_avg_array[h]))
  pow_Xfff_data_array.append(dsp_test.calc_power(Xfff_data_complex_avg_array[h]))
  
# Estimate SNR of VHDL and Python output                
snr_vhdl = []
snr_py = []
for h in range(c_nof_input_channels):          
  snr_vhdl.append(dsp_test.calc_snr(pow_Xqqq_data_array[h]))
  snr_py.append(dsp_test.calc_snr(pow_Xfff_data_array[h]))
  print "SNR VHDL(%s) channel %d = %f" % (c_fft_type, h, snr_vhdl[h])
  print "SNR PY channel %d = %f" % (h, snr_py[h])

# Write the VHDL results to a file for comparison with other FFT-types. 
if(c_write_to_file == 1):
  f = file(c_file_name, "w")
  for i in range(c_nof_points):
    s = " %x   %x  \n" % (int(Xqqq_data_complex_avg[i].real), int(Xqqq_data_complex_avg[i].imag))
    f.write(s)
  f.close()
  print "Spectra data is written to file " + c_file_name

if(c_plot_enable == 1):
  
  # Create variable for the axes. 
  f_as = []
  x_as = []
  for i in range(c_nof_points):                                                                                         
    f_as.append(i*400/(c_nof_points))
    x_as.append(i)

  # Plot the time domain signals. 
  for h in range(c_nof_input_channels):          
    pl.figure(h+1)
    pl.subplot(211)
    pl.plot(x_as, xf_real[0:c_nof_points])
    pl.xlabel('Real input (A)')
    pl.ylabel('Amplitude')
    pl.subplot(212)
    pl.plot(x_as, xf_imag[0:c_nof_points])
    pl.xlabel('Imaginary input (B)')
    pl.ylabel('Amplitude')
  
  ## Plot the VHDL spectrums                     
  for h in range(c_nof_input_channels):          
    pl.figure(100+h)  
    pl.subplot(311)
    title_string = 'Spectrums VHDL FFT(' + str(c_nof_points) + ' points) Type = ' + c_fft_type 
    pl.title(title_string )
    pl.plot(f_as, pow_stats_array[h])
    pl.xlabel('VHDL Statistics Spectrum')
    pl.ylabel('Power (dB)')
    pl.subplot(312)
    pl.plot(f_as, pow_Xqqq_data_array[h])
    pl.xlabel('VHDL Xqqq Spectrum')
    pl.ylabel('Power (dB)')
    pl.subplot(313)
    pl.plot(f_as, pow_Xfff_data_array[h])
    pl.xlabel('Python Xfff Spectrum')
    pl.ylabel('Power (dB)')
  
  pl.show()

###############################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(3, '>>>')

        
