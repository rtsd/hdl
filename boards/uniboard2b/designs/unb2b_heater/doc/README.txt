Quick steps to compile and use design [unb2a_heater] in RadionHDL
------------------------------------------------------------------

-> In case of a new installation, the IP's have to be generated for Arria10.
   In the: $HDL_WORK/libraries/technology/ip_arria10_e1sg
   directory; run the bash script: ./generate-all-ip.sh

-> For compilation it might be necessary to check the .vhd file:
   $HDL_WORK/libraries/technology/technology_select_pkg.vhd



1. Start with the Oneclick Commands:
    python $HDL_WORK/tools/oneclick/base/modelsim_config.py -t unb2b
    python $HDL_WORK/tools/oneclick/base/quartus_config.py -t unb2b


2. Generate MMM for QSYS:
    run_qsys unb2b unb2a_heater



3. -> From here either continue to Modelsim (simulation) or Quartus (synthesis)

Simulation
----------
Modelsim instructions:
    # in bash do:
    rm -r ${HDL_IOFILE_SIM_DIR}/* # (optional)
    run_modelsim unb2b

    # in Modelsim do:
    lp unb2b_heater
    mk all
    # now double click on testbench file
    as 10
    run 500us

    # while the simulation runs... in another bash session do:
    cd python/peripherals
    python util_heater.py --sim --unb 0 --fn 3 -n1  # (n=0..6, to select number of heaters)

    # (sensor results only show up after 1000us of simulation runtime)

    # to end simulation in Modelsim do:
    quit -sim


Synthesis
---------
Quartus instructions:
    run_qcomp unb2b unb2b_heater


In case of needing the Quartus GUI for inspection:
    run_quartus unb2b



4. Load firmware
----------------
Using JTAG: Start the Quartus GUI and open: tools->programmer.
            Then click auto-detect;
            Use 'change file' to select the correct .sof file for each FPGA
            Select the FPGA(s) which has to be programmed
            Click 'start'
Using EPCS: See step 6 below.




5. Testing on hardware
----------------------
Assuming the firmware is loaded and running already in the FPGA, the firmware can be tested from the connected
LCU computer.

# (assume that the Uniboard is --unb 1)

# To read out the design_name, info; enable heaters; do:

  cd python/peripherals
  python util_unb2.py --unb 1 --fn 0:3 --seq REGMAP,INFO -v5
  python util_heater.py --sim --unb 1 --fn 0:3 -n1  # (n=0..6, to select number of heaters)



6.
Programming the EPCS flash.
when the EPCS module works an RBF file can be generated to program the flash,
then the .sof file file can be converted to .rbf with the 'run_rbf' script.

But for now the only way to program the EPCS flash is via JTAG.
Firstly a JIC file has to be generated from the SOF file.
In Quartus GUI; open current project; File -> Convert Programming Files.
Then setup:
- Output programming file: JIC
- Configuration device: EPCQL1024
- Mode: Active Serial x4
- Flash Loader: Add/Select Device Arria10/10AX115U4ES
- SOF Data: add file (the generated .sof file)
  - click the .sof file; Set property 'Compression' to ON
- Press 'Generate'
Then program the .JIC file (output_file.jic) to EPCS flash:
- Make sure that the JTAG (on server connected to board) runs at 16MHz:
  c:\altera\15.0\quartus\bin64\jtagconfig USB-BlasterII JtagClock 16M
- open tools->programmer
- make sure the 4 fpga icons have the device 10AX115U4F45ES
- right-click each fpga icon and attach flash device EPCQL1024
- right-click each fpga and change file from <none> to sfl_enhanced_01_02e360dd.sof
  (in $HDL_WORK/boards/uniboard2/libraries/unb2a_board/quartus)
- right-click each EPCQL1024 and change file from <none> to output_file.jic
- select click each Program/Configure radiobutton
- click start and wait for 'Successful'
