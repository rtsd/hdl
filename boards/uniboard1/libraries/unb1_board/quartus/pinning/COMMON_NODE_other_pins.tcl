###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_AB28 -to VERSION[0]
set_location_assignment PIN_AC28 -to VERSION[1]
set_location_assignment PIN_AK33 -to ID[0]
set_location_assignment PIN_AH33 -to ID[1]
set_location_assignment PIN_AJ32 -to ID[2]
set_location_assignment PIN_AH32 -to ID[3]
set_location_assignment PIN_AG32 -to ID[4]
set_location_assignment PIN_AG31 -to ID[5]
set_location_assignment PIN_AE31 -to ID[6]
set_location_assignment PIN_AG30 -to ID[7]
set_location_assignment PIN_AM35 -to TESTIO[0]
set_location_assignment PIN_AM34 -to TESTIO[1]
set_location_assignment PIN_AL35 -to TESTIO[2]
set_location_assignment PIN_AL34 -to TESTIO[3]
set_location_assignment PIN_AK35 -to TESTIO[4]
set_location_assignment PIN_AK34 -to TESTIO[5]
set_location_assignment PIN_AJ35 -to TESTIO[6]
set_location_assignment PIN_AJ34 -to TESTIO[7]

set_instance_assignment -name IO_STANDARD "2.5 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[3]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[4]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[5]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[6]
set_instance_assignment -name IO_STANDARD "2.5 V" -to ID[7]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[6]
set_instance_assignment -name IO_STANDARD "2.5 V" -to TESTIO[7]
