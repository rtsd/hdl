onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(0) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(0)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(1) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(1)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(2) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(2)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(3) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(3)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(4) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(4)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(5) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(5)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(6) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(6)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(7) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(7)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(8) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(8)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(9) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(9)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(10) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(10)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xy_im_neg
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/products_valid_vec_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xy_re_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/product_xy_im_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group gen_iquv(11) -group dut -radix unsigned /tb_iquv_iab/dut/gen_iquv(11)/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_i -radix unsigned /tb_iquv_iab/dut/u_common_adder_i/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_i -radix unsigned /tb_iquv_iab/dut/u_common_adder_i/clken
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_i -radix unsigned /tb_iquv_iab/dut/u_common_adder_i/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_i -radix unsigned /tb_iquv_iab/dut/u_common_adder_i/sum
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_i -radix unsigned /tb_iquv_iab/dut/u_common_adder_i/adds
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_q -radix unsigned /tb_iquv_iab/dut/u_common_adder_q/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_q -radix unsigned /tb_iquv_iab/dut/u_common_adder_q/clken
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_q -radix unsigned /tb_iquv_iab/dut/u_common_adder_q/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_q -radix unsigned /tb_iquv_iab/dut/u_common_adder_q/sum
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_q -radix unsigned /tb_iquv_iab/dut/u_common_adder_q/adds
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_u -radix unsigned /tb_iquv_iab/dut/u_common_adder_u/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_u -radix unsigned /tb_iquv_iab/dut/u_common_adder_u/clken
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_u -radix unsigned /tb_iquv_iab/dut/u_common_adder_u/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_u -radix unsigned /tb_iquv_iab/dut/u_common_adder_u/sum
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_u -radix unsigned /tb_iquv_iab/dut/u_common_adder_u/adds
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_v -radix unsigned /tb_iquv_iab/dut/u_common_adder_v/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_v -radix unsigned /tb_iquv_iab/dut/u_common_adder_v/clken
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_v -radix unsigned /tb_iquv_iab/dut/u_common_adder_v/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_v -radix unsigned /tb_iquv_iab/dut/u_common_adder_v/sum
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_common_adder_v -radix unsigned /tb_iquv_iab/dut/u_common_adder_v/adds
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/clken
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/in_clr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/in_en
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_delay_valid -radix unsigned /tb_iquv_iab/dut/u_delay_valid/out_dat_p
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/snk_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/src_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/snk_out_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/u_dp_pipeline/snk_in_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/out_ovr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/snk_in_piped
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/quantized_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/quantized_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/quantized_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/out_ovr_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_i -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_i/out_ovr_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/snk_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/src_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/snk_out_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/u_dp_pipeline/snk_in_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/out_ovr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/snk_in_piped
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/quantized_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/quantized_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/quantized_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/out_ovr_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_q -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_q/out_ovr_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/snk_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/src_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/snk_out_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/u_dp_pipeline/snk_in_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/out_ovr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/snk_in_piped
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/quantized_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/quantized_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/quantized_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/out_ovr_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_u -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_u/out_ovr_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/snk_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/src_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/snk_out_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -group u_dp_pipeline -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/u_dp_pipeline/snk_in_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/snk_in
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/src_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/out_ovr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/snk_in_piped
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/quantized_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/quantized_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/quantized_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/out_ovr_re
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -group u_dp_requantize_v -radix unsigned /tb_iquv_iab/dut/u_dp_requantize_v/out_ovr_im
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/in_complex_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_i_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_q_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_u_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_v_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_valid_dly
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_i_linear
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_q_linear
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_u_linear
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/iquv_v_linear
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/i_sum_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/q_sum_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/u_sum_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/v_sum_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv_iab/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/diag_i_out_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/diag_q_out_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/diag_u_out_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/diag_v_out_data
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/diag_out_valid
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/tb_end
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/in_complex_arr
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/i_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/q_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/u_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/v_out
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/random0
add wave -noupdate -height 15 -expand -group tb_iquv_iab -height 15 -radix unsigned /tb_iquv_iab/valid_enable
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 fs} 0}
configure wave -namecolwidth 300
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {3118500 ps}
