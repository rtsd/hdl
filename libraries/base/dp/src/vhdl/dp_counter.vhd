--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Pipeline wrapper around dp_counter_func
-- Description:
-- . See dp_counter_func.
-- . dp_counter_func contains only functional logic (no pipelining).
-- . This wrapper adds pipelining for the source side outputs (g_pipeline_src_out)
--   and source side inputs (g_pipeline_src_in).
-- Usage:
-- . When g_nof_counters = 1 then dp_counter can reshape the block sizes in a
--   sync intervals, as is done in dp_block_reshape.vhd
-- . When g_nof_counters = 2 then dp_counter can reshape the block sizes and the
--   sync interval, as is done in dp_block_reshape_sync.vhd
-- . When g_nof_counters > 1 then the dp_counter count_src_out_arr provides the
--   block indices for an array of data. The g_nof_counters then defines the
--   dimension of the array within a sync interval. For example:
--
--     a[N][M][P] with N=3, M=5, p=10 will have N*M*P = 150 valid per sync interval.
--
--   The dp_counter can then provide for each valid in the sync interval the
--   corresponding array index (n, m, p), using:
--
--     g_nof_counters = 3
--     g_range_start  = (0, 0,  0)
--     g_range_stop   = (3, 5, 10)
--     g_range_step   = (1, 1,  1)
--
-- Remarks:
-- . Unfortunately it is not possible to use a generic in the definition of another generic. Therefore the
--   g_range_* generics are defined as unconstraint.
-- . When dp_counter is instanciated the length of the g_range_* generics must at least fit g_nof_counters.
--   In theory count_src_out_arr could be declared as unconstraint and then g_nof_counters could be derived
--   from the length of count_src_out_arr, but it is more clear to explitely to declare g_nof_counters.
--
--   31-05-2018 J Hargreaves added count_offset_in_arr input
--     This signal can be used to apply an offset to the start count
--   USAGE
--     Do not connect the signal if it is not needed: It will consume extra resources
--     It only works when g_range_step(i) is 1
--     It only works when g_range_start(i) + count_offset_in_arr(i) < g_range_stop(i)
--     Any other useage will break counters >= stage i

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_counter is
  generic (
    g_nof_counters     : natural := 1;
    g_range_start      : t_nat_natural_arr;  -- range must fit (g_nof_counters-1 DOWNTO 0)
    g_range_stop       : t_nat_natural_arr;  -- range must fit (g_nof_counters-1 DOWNTO 0)
    g_range_step       : t_nat_natural_arr;  -- range must fit (g_nof_counters-1 DOWNTO 0)
    g_pipeline_src_out : natural := 1;  -- Pipeline source outputs (data,valid,sop,eop etc)
    g_pipeline_src_in  : natural := 0  -- Pipeline source inputs (ready,xon). This will also pipeline src_out.
  );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    snk_in      : in  t_dp_sosi;
    snk_out     : out t_dp_siso;

    src_out     : out t_dp_sosi;
    src_in      : in  t_dp_siso := c_dp_siso_rdy;

    count_offset_in_arr : in  t_nat_natural_arr(g_nof_counters - 1 downto 0) := (others => 0);
    count_src_out_arr : out t_dp_sosi_arr(g_nof_counters - 1 downto 0)
  );
end dp_counter;

architecture wrap of dp_counter is
  -- force downto range for unconstraint g_range generics
  constant c_range_len             : natural := g_range_start'length;  -- g_nof_counters must be <= c_range_len
  constant c_range_start           : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_start;
  constant c_range_stop            : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_stop;
  constant c_range_step            : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_step;

  constant c_use_dp_pipeline       : boolean := (g_pipeline_src_out > 0 and g_pipeline_src_in = 0);
  constant c_use_dp_pipeline_ready : boolean := (g_pipeline_src_in > 0);

  signal dp_counter_func_src_out_arr : t_dp_sosi_arr(g_nof_counters - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- dp_counter_func
  ------------------------------------------------------------------------------
  u_dp_counter_func : entity work.dp_counter_func
  generic map (
    g_nof_counters => g_nof_counters,
    g_range_start  => c_range_start,
    g_range_stop   => c_range_stop,
    g_range_step   => c_range_step
  )
  port map (
    rst       => rst,
    clk       => clk,

    count_en  => snk_in.valid,

    count_offset_in_arr => count_offset_in_arr,
    count_src_out_arr   => dp_counter_func_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- dp_pipeline
  ------------------------------------------------------------------------------
  gen_dp_pipeline : if c_use_dp_pipeline = true generate
    u_dp_pipeline_snk_in : entity work.dp_pipeline
    generic map (
      g_pipeline => g_pipeline_src_out
    )
    port map (
      clk         => clk,
      rst         => rst,

      snk_in      => snk_in,
      snk_out     => snk_out,

      src_out     => src_out,
      src_in      => src_in
    );

    gen_dp_pipeline_count_src_out_arr : for i in 0 to g_nof_counters - 1 generate
      u_dp_pipeline_count_src_out_arr : entity work.dp_pipeline
      generic map (
        g_pipeline => g_pipeline_src_out
      )
      port map (
        clk     => clk,
        rst     => rst,

        snk_in  => dp_counter_func_src_out_arr(i),

        src_out => count_src_out_arr(i),
        src_in  => src_in
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- dp_pipeline_ready
  ------------------------------------------------------------------------------
  gen_dp_pipeline_ready : if c_use_dp_pipeline_ready = true generate
    u_dp_pipeline_ready : entity work.dp_pipeline_ready
    generic map (
      g_in_latency   => 1
    )
    port map (
      clk          => clk,
      rst          => rst,

      snk_in       => snk_in,
      snk_out      => snk_out,

      src_out      => src_out,
      src_in       => src_in
    );

    gen_dp_pipeline_ready_count_src_out_arr : for i in 0 to g_nof_counters - 1 generate
      u_dp_pipeline_ready_count_src_out_arr : entity work.dp_pipeline_ready
      generic map (
        g_in_latency => 1
      )
      port map (
        clk     => clk,
        rst     => rst,

        snk_in  => dp_counter_func_src_out_arr(i),

        src_out => count_src_out_arr(i),
        src_in  => src_in
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- No pipelining
  ------------------------------------------------------------------------------
  no_dp_pipeline : if c_use_dp_pipeline = false and c_use_dp_pipeline_ready = false generate
    src_out <= snk_in;
    snk_out <= src_in;

    count_src_out_arr <= dp_counter_func_src_out_arr;
  end generate;
end wrap;
