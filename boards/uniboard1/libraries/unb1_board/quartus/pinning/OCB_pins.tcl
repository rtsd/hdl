###############################################################################

#
# Copyright (C) 2012
# Station de Radioastronomie de Nancay <http://www.obs-nancay.fr>
# CNRS USR703
# 18330 Nancay, FRANCE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


set_location_assignment PIN_AR2 -to BN_SFP_RX[0]
set_location_assignment PIN_AC2 -to BN_SFP_RX[1]
set_location_assignment PIN_R2  -to BN_SFP_RX[2]
#set_location_assignment PIN_N2  -to BN_SFP_RX[2]
#set_location_assignment PIN_G2  -to BN_SFP_RX[3]   BN_SFP_RX[3]connected to bad IO on Uniboard (G2 instead of E2)
#set_location_assignment PIN_C2  -to BN_SFP_RX[4]

set_location_assignment PIN_AW6 -to BN_SFP_RXLOS[0]
set_location_assignment PIN_AT8 -to BN_SFP_RXLOS[1]
set_location_assignment PIN_AN6 -to BN_SFP_RXLOS[2]
set_location_assignment PIN_AH6 -to BN_SFP_RXLOS[3]
#set_location_assignment PIN_R6  -to BN_SFP_RXLOS[4]

set_location_assignment PIN_AP4 -to BN_SFP_TX[0]
set_location_assignment PIN_AB4 -to BN_SFP_TX[1]
set_location_assignment PIN_P4  -to BN_SFP_TX[2]
#set_location_assignment PIN_M4  -to BN_SFP_TX[2]
#set_location_assignment PIN_D4  -to BN_SFP_TX[3]
#set_location_assignment PIN_B4  -to BN_SFP_TX[4]

set_location_assignment PIN_AW5 -to BN_SFP_TXFAULT[0]
set_location_assignment PIN_AU8 -to BN_SFP_TXFAULT[1]
set_location_assignment PIN_AN5 -to BN_SFP_TXFAULT[2]
set_location_assignment PIN_AH5 -to BN_SFP_TXFAULT[3]
#set_location_assignment PIN_R5  -to BN_SFP_TXFAULT[4]

set_location_assignment PIN_AV10 -to BN_SFP_TXDISABL[0]
set_location_assignment PIN_AV5  -to BN_SFP_TXDISABL[1]
set_location_assignment PIN_AU10 -to BN_SFP_TXDISABL[2]
set_location_assignment PIN_AM6  -to BN_SFP_TXDISABL[3]
# PIN_AB6 dedicated clock => PB
#set_location_assignment PIN_AB6  -to BN_SFP_TXDISABL[4]

set_location_assignment PIN_AW10 -to BN_SFP_PRS[0]
set_location_assignment PIN_AW4  -to BN_SFP_PRS[1]
set_location_assignment PIN_AT10 -to BN_SFP_PRS[2]
set_location_assignment PIN_AM5  -to BN_SFP_PRS[3]
# PIN_AA5 dedicated clock => PB
#set_location_assignment PIN_AA5  -to BN_SFP_PRS[4]

set_location_assignment PIN_AV7 -to BN_SFP_SCL[0]
set_location_assignment PIN_AT7 -to BN_SFP_SCL[1]
set_location_assignment PIN_AP6 -to BN_SFP_SCL[2]
set_location_assignment PIN_AJ6 -to BN_SFP_SCL[3]
#set_location_assignment PIN_W8  -to BN_SFP_SCL[4]

set_location_assignment PIN_AW7 -to BN_SFP_SDA[0]
set_location_assignment PIN_AU7 -to BN_SFP_SDA[1]
set_location_assignment PIN_AP5 -to BN_SFP_SDA[2]
set_location_assignment PIN_AJ5 -to BN_SFP_SDA[3]
#set_location_assignment PIN_W7  -to BN_SFP_SDA[4]




set_instance_assignment -name IO_STANDARD "1.5-V PCML" -to BN_SFP_RX
set_instance_assignment -name IO_STANDARD "1.5-V PCML" -to BN_SFP_TX

set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_RXLOS
set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_TXFAULT
set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_TXDISABL
set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_PRS
set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_SCL
set_instance_assignment -name IO_STANDARD "2.5 V" -to BN_SFP_SDA

