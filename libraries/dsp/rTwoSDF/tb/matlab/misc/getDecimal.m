function out = getDecimal(in)
N_=size(in,2);


for(ii=1:size(in,1))
  for(jj=1:size(in,2))
    
    if in(ii,jj,1) == '1'
      sign=-1;
    elseif in(ii,jj,1) == '0'
      sign=1;
    end;
    in(ii,jj,1)='0';

    tmp= bin2dec(squeeze(in(ii,jj,:))');
    out(ii,jj)=sign*tmp;
    
  end;
end;



end