--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_e1sg_mac_10g_alt_em10g32_180;

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use work.tech_mac_10g_component_pkg.all;

entity tech_mac_10g_arria10_e1sg is
  port (
    -- MM
    mm_clk            : in  std_logic;
    mm_rst            : in  std_logic;
    csr_mosi          : in  t_mem_mosi;  -- CSR = control status register
    csr_miso          : out t_mem_miso;

    -- ST
    tx_clk_312        : in  std_logic;
    tx_clk_156        : in  std_logic;
    tx_rst            : in  std_logic;
    tx_snk_in         : in  t_dp_sosi;
    tx_snk_out        : out t_dp_siso;

    rx_clk_312        : in  std_logic;
    rx_clk_156        : in  std_logic;
    rx_rst            : in  std_logic;
    rx_src_out        : out t_dp_sosi;
    rx_src_in         : in  t_dp_siso;

    -- XGMII
    xgmii_link_status : out std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0);  -- 2 bit
    xgmii_tx_data     : out std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit
    xgmii_rx_data     : in  std_logic_vector(c_xgmii_w - 1 downto 0)  -- 72 bit
  );
end tech_mac_10g_arria10_e1sg;

architecture str of tech_mac_10g_arria10_e1sg is
  constant c_mac_10g_csr_addr_w   : natural := func_tech_mac_10g_csr_addr_w(c_tech_arria10_e1sg);  -- = 13

  signal mm_rst_n     : std_logic;
  signal tx_rst_n     : std_logic;
  signal rx_rst_n     : std_logic;

  signal avalon_rx_src_out : t_dp_sosi := c_dp_sosi_rst;
begin
  mm_rst_n <= not mm_rst;
  tx_rst_n <= not tx_rst;
  rx_rst_n <= not rx_rst;

  -- Default frame level flow control
  tx_snk_out.xon <= '1';

  -- Force rx_src_out.sop = 0 when rx_src_out.valid = '0'
  p_rx_src_out : process(avalon_rx_src_out)
  begin
    rx_src_out     <= avalon_rx_src_out;
    rx_src_out.sop <= avalon_rx_src_out.sop and avalon_rx_src_out.valid;
  end process;

  u_ip_arria10_e1sg_mac_10g : ip_arria10_e1sg_mac_10g
  port map (
    csr_clk                         => mm_clk,
    csr_rst_n                       => mm_rst_n,

    csr_address                     => csr_mosi.address(c_mac_10g_csr_addr_w - 1 downto 0),  -- 13 bit
    csr_read                        => csr_mosi.rd,
    csr_write                       => csr_mosi.wr,
    csr_writedata                   => csr_mosi.wrdata(c_word_w - 1 downto 0),  -- 32 bit
    csr_readdata                    => csr_miso.rddata(c_word_w - 1 downto 0),  -- 32 bit
    csr_waitrequest                 => csr_miso.waitrequest,

    tx_312_5_clk                    => tx_clk_312,
    tx_156_25_clk                   => tx_clk_156,
    tx_rst_n                        => tx_rst_n,

    avalon_st_tx_ready              => tx_snk_out.ready,
    avalon_st_tx_startofpacket      => tx_snk_in.sop,
    avalon_st_tx_endofpacket        => tx_snk_in.eop,
    avalon_st_tx_valid              => tx_snk_in.valid,
    avalon_st_tx_data               => tx_snk_in.data(c_xgmii_data_w - 1 downto 0),  -- 64 bit
    avalon_st_tx_empty              => tx_snk_in.empty(c_tech_mac_10g_empty_w - 1 downto 0),  -- 3 bit
    avalon_st_tx_error              => tx_snk_in.err(0),  -- 1 bit std_logic = c_tech_mac_10g_tx_error_w
    avalon_st_pause_data            => (others => '0'),

    xgmii_tx                        => xgmii_tx_data,  -- 72 bit

    avalon_st_txstatus_valid        => OPEN,
    avalon_st_txstatus_data         => OPEN,
    avalon_st_txstatus_error        => OPEN,

    rx_312_5_clk                    => rx_clk_312,
    rx_156_25_clk                   => rx_clk_156,
    rx_rst_n                        => rx_rst_n,

    xgmii_rx                        => xgmii_rx_data,  -- 72 bit

    avalon_st_rx_ready              => rx_src_in.ready,
    avalon_st_rx_startofpacket      => avalon_rx_src_out.sop,
    avalon_st_rx_endofpacket        => avalon_rx_src_out.eop,
    avalon_st_rx_valid              => avalon_rx_src_out.valid,
    avalon_st_rx_data               => avalon_rx_src_out.data(c_xgmii_data_w - 1 downto 0),  -- 64 bit
    avalon_st_rx_empty              => avalon_rx_src_out.empty(c_tech_mac_10g_empty_w - 1 downto 0),  -- 3 bit
    avalon_st_rx_error              => avalon_rx_src_out.err(c_tech_mac_10g_rx_error_w - 1 downto 0),  -- 6 bit

    avalon_st_rxstatus_valid        => OPEN,
    avalon_st_rxstatus_data         => OPEN,
    avalon_st_rxstatus_error        => OPEN,

    link_fault_status_xgmii_rx_data => xgmii_link_status,  -- 0=ok, 1=local fault, 2=remote fault

    unidirectional_en               => OPEN,
    unidirectional_remote_fault_dis => open
  );
end str;
