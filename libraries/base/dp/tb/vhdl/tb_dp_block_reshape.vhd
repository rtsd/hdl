-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 26 Apr 2018
-- Purpose:
-- . Test bench for dp_block_reshape.
-- Description:
--   Verify that the input sosi is the same as the output sosi when two
--   dp_block_reshape components are used in series, whereby the second
--   reshapes the sop,eop block size back to the original input block
--   size. Block diagram:
--
--    stimuli --> dp_block_reshape --> dp_block_reshape -- > verify
--
-- Remark:
-- . Generics g_reshape_bsn and g_reshape_channel are already covered by
--   tb_tb_dp_block_reshape_sync.vhd
-- . Pipelining and flow control are already covered by tb_dp_counter.vhd.
--
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_reshape is
  generic (
    g_pipeline   : natural := 1  -- use 0 or 1
  );
end tb_dp_block_reshape;

architecture tb of tb_dp_block_reshape is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period : time := 5 ns;

  constant c_nof_sync                 : natural :=  5;
  constant c_gap_size                 : natural :=  4;
  constant c_input_nof_data_per_blk   : natural :=  9;
  constant c_input_nof_blk_per_sync   : natural := 12;
  constant c_input_nof_data_per_sync  : natural := c_input_nof_blk_per_sync * c_input_nof_data_per_blk;
  constant c_reshape_nof_data_per_blk : natural := 36;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal tb_end         : std_logic := '0';

  signal stimuli_end       : std_logic;
  signal stimuli_sosi      : t_dp_sosi;
  signal stimuli_siso      : t_dp_siso;
  signal reshape_sosi      : t_dp_sosi;
  signal reshape_siso      : t_dp_siso;
  signal reshape_index_arr : t_natural_arr(1 downto 0);  -- [1] sop index, [0] valid index
  signal verify_sosi       : t_dp_sosi;
  signal verify_siso       : t_dp_siso := c_dp_siso_rdy;
  signal verify_index_arr  : t_natural_arr(1 downto 0);  -- [1] sop index, [0] valid index
  signal pipeline_sosi     : t_dp_sosi;
  signal pipeline_siso     : t_dp_siso := c_dp_siso_rdy;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => c_input_nof_blk_per_sync,
    g_nof_repeat  => c_input_nof_blk_per_sync * c_nof_sync,
    g_pkt_len     => c_input_nof_data_per_blk,
    g_pkt_gap     => c_gap_size
  )
  port map (
    rst               => rst,
    clk               => clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_reshape : entity work.dp_block_reshape
  generic map (
    g_input_nof_data_per_sync  => c_input_nof_data_per_sync,  -- nof data per input sync interval, used only for sop_index
    g_reshape_nof_data_per_blk => c_reshape_nof_data_per_blk,
    g_pipeline_src_out         => g_pipeline,
    g_pipeline_src_in          => 0
  )
  port map (
    clk           => clk,
    rst           => rst,

    snk_in        => stimuli_sosi,
    snk_out       => stimuli_siso,

    src_out       => reshape_sosi,
    src_in        => reshape_siso,
    src_index_arr => reshape_index_arr  -- [1] sop index, [0] valid index
  );

  u_reshape_back : entity work.dp_block_reshape
  generic map (
    g_input_nof_data_per_sync  => c_input_nof_data_per_sync,  -- nof data per input sync interval, used only for sop_index
    g_reshape_nof_data_per_blk => c_input_nof_data_per_blk,
    g_pipeline_src_out         => 0,
    g_pipeline_src_in          => g_pipeline
  )
  port map (
    clk           => clk,
    rst           => rst,

    snk_in        => reshape_sosi,
    snk_out       => reshape_siso,

    src_out       => verify_sosi,
    src_in        => verify_siso,
    src_index_arr => verify_index_arr  -- [1] sop index, [0] valid index
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => g_pipeline * 2
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => pipeline_siso,
    src_out     => pipeline_sosi
  );

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      assert verify_sosi = pipeline_sosi
        report "Wrong sosi"
        severity ERROR;
    end if;
  end process;

  tb_end <= '0', stimuli_end after (1 + 10 * g_pipeline) * c_clk_period;
end tb;
