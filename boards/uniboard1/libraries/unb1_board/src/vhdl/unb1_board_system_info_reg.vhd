-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--  RO  read only  (no VHDL present to access HW in write mode)
--  WO  write only (no VHDL present to access HW in read mode)
--  WE  write event (=WO)
--  WR  write control, read control
--  RW  read status, write control
--  RC  read, clear on read
--  FR  FIFO read
--  FW  FIFO write
--
--  wi  Bits    R/W Name          Default  Description      |REG_UNB1_BOARD_SYSTEM_INFO|
--  =============================================================================
--  0   [31..0] RO  info
--  1   [7..0]  RO  use_phy
--  2   [31..0] RO  design_name
--  .   ..      .   ..
--  9   [31..0] RO  design name
--  10  [31..0] RO  date stamp             (YYYYMMDD)
--  11  [31..0] RO  time stamp             (HHMMSS)
--  12  [31..0] RO  SVN  stamp
--  13  [31..0] RO  note
--  .   .       .   ..
--  20  [31..0] RO  note
--  =============================================================================

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_mem_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_system_info_reg is
  generic (
    g_design_name : string;
    g_use_phy     : t_c_unb1_board_use_phy;
    g_stamp_date  : natural := 0;
    g_stamp_time  : natural := 0;
    g_stamp_svn   : integer := 0;
    g_design_note : string
  );
  port (
    -- Clocks and reset
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;

    -- Memory Mapped Slave
    sla_in      : in  t_mem_mosi;
    sla_out     : out t_mem_miso;

    info        : in  std_logic_vector(c_word_w - 1 downto 0)
    );
end unb1_board_system_info_reg;

architecture rtl of unb1_board_system_info_reg is
  constant c_nof_fixed_regs       : natural := 2;  -- info, use_phy
  constant c_nof_design_name_regs : natural := 13;  -- design_name
  constant c_nof_stamp_regs       : natural := 3;  -- date, time, svn rev
  constant c_nof_design_note_regs : natural := 13;  -- note

  constant c_nof_regs             : natural := c_nof_fixed_regs + c_nof_design_name_regs + c_nof_stamp_regs + c_nof_design_note_regs;

  constant c_mm_reg        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(c_nof_regs),
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => c_nof_regs,
                                         init_sl  => '0');

  constant c_use_phy_w     : natural := 8;
  constant c_use_phy       : std_logic_vector(c_use_phy_w - 1 downto 0) := TO_UVEC(g_use_phy.eth1g,   1) &
                                                                         TO_UVEC(g_use_phy.tr_front, 1) &
                                                                         TO_UVEC(g_use_phy.tr_mesh, 1) &
                                                                         TO_UVEC(g_use_phy.tr_back, 1) &
                                                                         TO_UVEC(g_use_phy.ddr3_I,  1) &
                                                                         TO_UVEC(g_use_phy.ddr3_II, 1) &
                                                                         TO_UVEC(g_use_phy.adc,     1) &
                                                                         TO_UVEC(g_use_phy.wdi,     1);

  constant c_design_name    : t_slv_32_arr(0 to c_nof_design_name_regs - 1) := str_to_ascii_slv_32_arr(g_design_name, c_nof_design_name_regs);
  constant c_design_note    : t_slv_32_arr(0 to c_nof_design_note_regs - 1) := str_to_ascii_slv_32_arr(g_design_note, c_nof_design_note_regs);
begin
  p_mm_reg : process (mm_rst, mm_clk)
    variable vA : natural := 0;
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Read access: get register value
      if sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1

        vA := TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0));
        if vA = 0 then
          sla_out.rddata(c_word_w - 1 downto 0) <= info;
          -- Use bit 11 to indicate that we're using the MM bus (not the info SLV).
          -- Using the MM bus enables user to also read use_phy, design_name etc.
          sla_out.rddata(11) <= '1';
        elsif vA = 1 then
          sla_out.rddata(c_use_phy_w - 1 downto 0) <= c_use_phy;
        elsif vA < c_nof_fixed_regs + c_nof_design_name_regs then
          sla_out.rddata(c_word_w - 1 downto 0) <= c_design_name(vA - c_nof_fixed_regs);

        elsif vA = c_nof_fixed_regs + c_nof_design_name_regs then
          sla_out.rddata(c_word_w - 1 downto 0) <= TO_UVEC(g_stamp_date, c_word_w);

        elsif vA = c_nof_fixed_regs + c_nof_design_name_regs + 1 then
          sla_out.rddata(c_word_w - 1 downto 0) <= TO_UVEC(g_stamp_time, c_word_w);

        elsif vA = c_nof_fixed_regs + c_nof_design_name_regs + 2 then
          sla_out.rddata(c_word_w - 1 downto 0) <= TO_SVEC(g_stamp_svn, c_word_w);

        elsif vA < c_nof_fixed_regs + c_nof_design_name_regs + c_nof_stamp_regs + c_nof_design_note_regs then
          sla_out.rddata(c_word_w - 1 downto 0) <= c_design_note(vA - c_nof_fixed_regs - c_nof_design_name_regs - c_nof_stamp_regs);
        end if;
      end if;
    end if;
  end process;
end rtl;
