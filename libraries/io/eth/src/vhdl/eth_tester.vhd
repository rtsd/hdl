-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra
-- Purpose: Test the 1GbE interface by sending and counting received packets.
-- Description: See detailed design in [1]
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_components_pkg.all;
use diag_lib.diag_pkg.all;
use work.eth_pkg.all;
use work.eth_tester_pkg.all;

entity eth_tester is
  generic (
    g_nof_streams        : natural := 1;
    g_bg_sync_timeout    : natural := c_eth_tester_sync_timeout;
    g_nof_octet_generate : natural := 1;
    g_nof_octet_output   : natural := 4;  -- must be multiple of g_nof_octet_generate.
    g_use_eth_header     : boolean  := true;
    g_use_ip_udp_header  : boolean  := true;
    g_use_dp_header      : boolean  := true;
    g_hdr_calc_ip_crc    : boolean  := false;
    g_hdr_field_arr      : t_common_field_arr := c_eth_tester_hdr_field_arr;
    g_hdr_field_sel      : std_logic_vector   := c_eth_tester_hdr_field_sel;
    g_hdr_app_len        : natural := c_eth_tester_app_hdr_len;
    g_remove_crc         : boolean := true  -- use TRUE when using sim_tse and tech_tse link interface,
                                            -- use FALSE when streaming link interface
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;
    st_rst             : in  std_logic;
    st_clk             : in  std_logic;
    st_pps             : in  std_logic;

    -- UDP transmit interface
    eth_src_mac        : in  std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr        : in  std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port       : in  std_logic_vector(c_network_udp_port_w - 1 downto 0);

    tx_fifo_rd_emp_arr : out std_logic_vector(g_nof_streams - 1 downto 0);

    tx_udp_sosi_arr    : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    tx_udp_siso_arr    : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    -- UDP receive interface
    rx_udp_sosi_arr    : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);

    -- Memory Mapped Slaves (one per stream)
    -- . Tx
    reg_bg_ctrl_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_bg_ctrl_cipo               : out t_mem_cipo;
    reg_hdr_dat_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo               : out t_mem_cipo;
    reg_bsn_monitor_v2_tx_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_tx_cipo     : out t_mem_cipo;
    reg_strobe_total_count_tx_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_tx_cipo : out t_mem_cipo;
    reg_dp_split_copi              : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_split_cipo              : out t_mem_cipo;
    -- . Rx
    reg_bsn_monitor_v2_rx_copi             : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_rx_cipo             : out t_mem_cipo;
    reg_strobe_total_count_rx_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_rx_cipo         : out t_mem_cipo
  );
end eth_tester;

architecture str of eth_tester is
  constant c_dp_split_reg_adr_w : natural := 1;  -- 1 for 1 stream in dp_split

  signal ref_sync_arr  : std_logic_vector(g_nof_streams - 1 downto 0);

  signal dp_length_arr : t_natural_arr(g_nof_streams - 1 downto 0);  -- tx block length

  -- MM port multiplexers
  -- . Tx
  signal reg_bg_ctrl_copi_arr               : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bg_ctrl_cipo_arr               : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_hdr_dat_copi_arr               : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_hdr_dat_cipo_arr               : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_tx_copi_arr     : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_tx_cipo_arr     : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_strobe_total_count_tx_copi_arr : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_strobe_total_count_tx_cipo_arr : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_dp_split_copi_arr              : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_dp_split_cipo_arr              : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  -- . Rx
  signal reg_bsn_monitor_v2_rx_copi_arr     : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_rx_cipo_arr     : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_strobe_total_count_rx_copi_arr : t_mem_copi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_strobe_total_count_rx_cipo_arr : t_mem_cipo_arr(g_nof_streams - 1 downto 0) := (others => c_mem_cipo_rst);
begin
  gen_streams : for I in 0 to g_nof_streams - 1 generate
    u_tx : entity work.eth_tester_tx
    generic map (
      g_bg_sync_timeout    => g_bg_sync_timeout,
      g_nof_octet_generate => g_nof_octet_generate,
      g_nof_octet_output   => g_nof_octet_output,
      g_use_eth_header     => g_use_eth_header,
      g_use_ip_udp_header  => g_use_ip_udp_header,
      g_use_dp_header      => g_use_dp_header,
      g_hdr_calc_ip_crc    => g_hdr_calc_ip_crc,
      g_hdr_field_arr      => g_hdr_field_arr,
      g_hdr_field_sel      => g_hdr_field_sel,
      g_hdr_app_len        => g_hdr_app_len
    )
    port map (
      -- Clocks and reset
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      st_rst             => st_rst,
      st_clk             => st_clk,
      st_pps             => st_pps,
      ref_sync           => ref_sync_arr(I),

      -- UDP transmit interface
      eth_src_mac        => eth_src_mac,
      ip_src_addr        => ip_src_addr,
      udp_src_port       => udp_src_port,

      tx_length          => dp_length_arr(I),
      tx_fifo_rd_emp     => tx_fifo_rd_emp_arr(I),

      tx_udp_sosi        => tx_udp_sosi_arr(I),
      tx_udp_siso        => tx_udp_siso_arr(I),

      -- Memory Mapped Slaves (one per stream)
      reg_bg_ctrl_copi               => reg_bg_ctrl_copi_arr(I),
      reg_bg_ctrl_cipo               => reg_bg_ctrl_cipo_arr(I),
      reg_hdr_dat_copi               => reg_hdr_dat_copi_arr(I),
      reg_hdr_dat_cipo               => reg_hdr_dat_cipo_arr(I),
      reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_copi_arr(I),
      reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_cipo_arr(I),
      reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_copi_arr(I),
      reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_cipo_arr(I),
      reg_dp_split_copi              => reg_dp_split_copi_arr(I),
      reg_dp_split_cipo              => reg_dp_split_cipo_arr(I)
    );

    u_rx : entity work.eth_tester_rx
    generic map (
      g_bg_sync_timeout  => g_bg_sync_timeout,
      g_nof_octet_unpack => g_nof_octet_generate,
      g_nof_octet_input  => g_nof_octet_output,
      g_use_dp_header    => g_use_dp_header,
      g_hdr_field_arr    => g_hdr_field_arr,
      g_remove_crc       => g_remove_crc
    )
    port map (
      -- Clocks and reset
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      st_rst             => st_rst,
      st_clk             => st_clk,
      ref_sync           => ref_sync_arr(I),

      exp_length         => dp_length_arr(I),

      -- UDP transmit interface
      rx_udp_sosi        => rx_udp_sosi_arr(I),

      -- Memory Mapped Slaves (one per stream)
      reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_copi_arr(I),
      reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_cipo_arr(I),
      reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_copi_arr(I),
      reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_cipo_arr(I)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- MM port multiplexers for g_nof_streams
  -----------------------------------------------------------------------------

  -- Tx
  u_common_mem_mux_bg : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_diag_bg_reg_adr_w
  )
  port map (
    mosi     => reg_bg_ctrl_copi,
    miso     => reg_bg_ctrl_cipo,
    mosi_arr => reg_bg_ctrl_copi_arr,
    miso_arr => reg_bg_ctrl_cipo_arr
  );

  u_common_mem_mux_hdr_dat : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_eth_tester_reg_hdr_dat_addr_w
  )
  port map (
    mosi     => reg_hdr_dat_copi,
    miso     => reg_hdr_dat_cipo,
    mosi_arr => reg_hdr_dat_copi_arr,
    miso_arr => reg_hdr_dat_cipo_arr
  );

  u_common_mem_mux_bsn_monitor_v2_tx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_dp_bsn_monitor_v2_reg_adr_w
  )
  port map (
    mosi     => reg_bsn_monitor_v2_tx_copi,
    miso     => reg_bsn_monitor_v2_tx_cipo,
    mosi_arr => reg_bsn_monitor_v2_tx_copi_arr,
    miso_arr => reg_bsn_monitor_v2_tx_cipo_arr
  );

  u_common_mem_mux_strobe_total_count_tx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_dp_strobe_total_count_reg_adr_w
  )
  port map (
    mosi     => reg_strobe_total_count_tx_copi,
    miso     => reg_strobe_total_count_tx_cipo,
    mosi_arr => reg_strobe_total_count_tx_copi_arr,
    miso_arr => reg_strobe_total_count_tx_cipo_arr
  );

  u_common_mem_mux_dp_split : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_dp_split_reg_adr_w
  )
  port map (
    mosi     => reg_dp_split_copi,
    miso     => reg_dp_split_cipo,
    mosi_arr => reg_dp_split_copi_arr,
    miso_arr => reg_dp_split_cipo_arr
  );

  -- Rx
  u_common_mem_mux_bsn_monitor_v2_rx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_dp_bsn_monitor_v2_reg_adr_w
  )
  port map (
    mosi     => reg_bsn_monitor_v2_rx_copi,
    miso     => reg_bsn_monitor_v2_rx_cipo,
    mosi_arr => reg_bsn_monitor_v2_rx_copi_arr,
    miso_arr => reg_bsn_monitor_v2_rx_cipo_arr
  );

  u_common_mem_mux_strobe_total_count_rx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_dp_strobe_total_count_reg_adr_w
  )
  port map (
    mosi     => reg_strobe_total_count_rx_copi,
    miso     => reg_strobe_total_count_rx_cipo,
    mosi_arr => reg_strobe_total_count_rx_copi_arr,
    miso_arr => reg_strobe_total_count_rx_cipo_arr
  );
end str;
