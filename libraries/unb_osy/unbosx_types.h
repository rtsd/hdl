#ifndef UNBOSX_TYPES_H
#define UNBOSX_TYPES_H

#include <alt_types.h>

#define FALSE                 0
#define TRUE                  1
#define NULL                  ((void*)0)
#define ALT_IRQ_NOT_CONNECTED (-1)
typedef alt_32  TBool;
typedef alt_u16 TUInt16;
typedef alt_u32 TUInt32;

#endif
