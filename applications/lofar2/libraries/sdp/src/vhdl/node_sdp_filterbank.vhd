-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of the subband filterbank (Fsub) in the
--   LOFAR2 SDPFW design.
-- Description:
-- . The subband filterbank seperates the incoming timestamped ADC samples into
--   512 frequency bands called subbands.
-- . It implements a critically sampled poly-phase filterbank (PFB). The PFB
--   consists of a poly-phase finite impulse response (PFIR) filter per real
--   input and a  complex fast fourier transform (FFT) per 2 real inputs.
-- . The number of points of the FFT is 1024.
--
-- . Subband widths:
--   - raw_sosi   : c_subband_raw_dat_w bits
--   - quant_sosi : g_wpfb.fft_out_dat = c_sdp_W_subband bits
--   The quantized subbands are output and used for the SST.
--   The raw subbands are weighted by the equalizer and are output to allow
--   further weighting by the beamformer weights (in the BF) or to allow
--   different rounding (to c_sdp_W_crosslet bits in the XSub).
--    ____            __
--   |    |          |  |
--   |    |--raw---->|EQ|--raw----------------------------> fsub_raw_sosi_arr
--   |WPFB|          |  |--quant--+-----------------------> fsub_quant_sosi_arr
--   |    |          |__|         |    ___       ___
--   |    |                       \-->|   |     |   |
--   |    |--quant------------------->|sel|---->|SST|
--   |____|                           |___|     |___|
--
-- Remark:
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, rTwoSDF_lib, fft_lib, wpfb_lib, filter_lib, si_lib, st_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use filter_lib.fil_pkg.all;
use fft_lib.fft_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use work.sdp_pkg.all;

entity node_sdp_filterbank is
  generic (
    g_sim                    : boolean := false;
    g_sim_sdp                : t_sdp_sim := c_sdp_sim;
    g_wpfb                   : t_wpfb := c_sdp_wpfb_subbands;
    g_scope_selected_subband : natural := 0
  );
  port (
    dp_clk        : in  std_logic;
    dp_rst        : in  std_logic;

    dp_bsn_source_restart      : in std_logic;
    dp_bsn_source_new_interval : in std_logic;

    in_sosi_arr        : in  t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0);  -- c_sdp_W_adc bits
    fsub_quant_sosi_arr: out t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);  -- c_sdp_W_subband bits
    fsub_raw_sosi_arr  : out t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);  -- c_sdp_W_subband + c_subband_raw_fraction_w bits
    sst_udp_sosi       : out t_dp_sosi;
    sst_udp_siso       : in  t_dp_siso := c_dp_siso_rst;

    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;

    reg_si_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_si_miso        : out t_mem_miso;
    ram_st_sst_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_st_sst_miso    : out t_mem_miso;
    ram_fil_coefs_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_fil_coefs_miso : out t_mem_miso;
    ram_gains_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    ram_gains_miso     : out t_mem_miso;
    ram_gains_cross_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_gains_cross_miso : out t_mem_miso;
    reg_selector_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_selector_miso  : out t_mem_miso;
    reg_enable_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_enable_miso    : out t_mem_miso;
    reg_hdr_dat_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso   : out t_mem_miso;
    reg_bsn_monitor_v2_sst_offload_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_sst_offload_cipo : out t_mem_cipo;

    sdp_info : in t_sdp_info;
    gn_id    : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);

    eth_src_mac  : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr  : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port : in std_logic_vector(c_network_udp_port_w - 1 downto 0)
  );
end node_sdp_filterbank;

architecture str of node_sdp_filterbank is
  -- FIR coeffs file is copied to build data/ dir by design revision hdllib.cfg
  -- from: $HDL_WORK/libraries/dsp/filter/src/hex
  constant c_coefs_file_prefix : string := "data/Coeffs16384Kaiser-quant_1wb";

  -- Subband gains file is copied to build data/ dir by design revision
  -- hdllib.cfg from:
  -- . applications/lofar2/designs/lofar2_unb2c_sdp_station/src/data/
  -- . applications/lofar2/designs/lofar2_unb2c_sdp_station/src/data/
  -- Subband gains file can be generated by sdp/src/python/sdp_hex.py
  constant c_gains_file_name : string := "data/gains_1024_complex_" &
                                         natural'image(c_sdp_W_sub_weight) & "b" &
                                         natural'image(c_sdp_W_sub_weight_fraction) & "f_unit";

  constant c_nof_masters : positive := 2;  -- for M&C MM access and for statistics offload MM access

  constant c_fft                    : t_fft := func_wpfb_map_wpfb_parameters_to_fft(g_wpfb);
  constant c_subband_raw_dat_w      : natural := func_fft_raw_dat_w(c_fft);
  constant c_subband_raw_fraction_w : natural := func_fft_raw_fraction_w(c_fft);

  constant c_si_pipeline : natural := 1;

  signal ram_st_sst_mosi_arr : t_mem_mosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_mem_mosi_rst);
  signal ram_st_sst_miso_arr : t_mem_miso_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_mem_miso_rst);

  -- Subband statistics
  signal ram_st_offload_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_offload_miso : t_mem_miso := c_mem_miso_rst;

  signal master_mem_mux_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal master_mem_mux_miso : t_mem_miso := c_mem_miso_rst;
  signal master_mosi_arr     : t_mem_mosi_arr(0 to c_nof_masters - 1) := (others => c_mem_mosi_rst);
  signal master_miso_arr     : t_mem_miso_arr(0 to c_nof_masters - 1) := (others => c_mem_miso_rst);

  signal si_sosi_arr                      : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)  := (others => c_dp_sosi_rst);

  signal wpfb_unit_in_sosi_arr            : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal wpfb_unit_fil_sosi_arr           : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal wpfb_unit_out_raw_sosi_arr       : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal wpfb_unit_out_quant_sosi_arr     : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

  signal subband_equalizer_raw_sosi_arr   : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal subband_equalizer_quant_sosi_arr : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

  signal scope_wpfb_unit_out_quant_sosi_arr : t_dp_sosi_integer_arr(c_sdp_S_pn - 1 downto 0);
  signal scope_equalizer_quant_sosi_arr     : t_dp_sosi_integer_arr(c_sdp_S_pn - 1 downto 0);

  signal dp_selector_quant_sosi_arr       : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

  signal selector_en                : std_logic;
  signal weighted_subbands_flag     : std_logic;

  signal dp_bsn_source_restart_pipe : std_logic;

  -- debug signals to view parameters in Wave Window
  signal dbg_g_wpfb                   : t_wpfb := g_wpfb;
  signal dbg_c_subband_raw_dat_w      : natural := c_subband_raw_dat_w;
  signal dbg_c_subband_raw_fraction_w : natural := c_subband_raw_fraction_w;
begin
  ---------------------------------------------------------------
  -- SPECTRAL INVERSION
  ---------------------------------------------------------------
  u_si_arr : entity si_lib.si_arr
  generic map (
    g_nof_streams => c_sdp_S_pn,
    g_pipeline    => c_si_pipeline,
    g_dat_w       => c_sdp_W_adc
  )
  port map(
    in_sosi_arr  => in_sosi_arr,
    out_sosi_arr => si_sosi_arr,

    reg_si_mosi  => reg_si_mosi,
    reg_si_miso  => reg_si_miso,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,
    dp_clk       => dp_clk,
    dp_rst       => dp_rst
  );

  ---------------------------------------------------------------
  -- POLY-PHASE FILTERBANK
  ---------------------------------------------------------------
  -- Connect the 12 ADC streams to the re and im fields of the PFB input.
  p_pfb_streams : process(si_sosi_arr)
  begin
    for I in 0 to c_sdp_P_pfb - 1 loop
      wpfb_unit_in_sosi_arr(I) <= si_sosi_arr(2 * I);
      wpfb_unit_in_sosi_arr(I).re <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I).data);
      wpfb_unit_in_sosi_arr(I).im <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I + 1).data);
    end loop;
  end process;

  -- pipeline bsn restart signal to keep dp_bsn_source_restart aligned with si_sosi_arr
  u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_si_pipeline
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    in_dat     => dp_bsn_source_restart,
    out_dat    => dp_bsn_source_restart_pipe
  );

  -- PFB
  u_wpfb_unit_dev : entity wpfb_lib.wpfb_unit_dev
  generic map (
    g_wpfb                   => g_wpfb,
    g_use_prefilter          => true,
    g_stats_ena              => false,
    g_use_bg                 => false,
    g_coefs_file_prefix      => c_coefs_file_prefix,
    g_restart_on_valid       => false
  )
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    ram_fil_coefs_mosi => ram_fil_coefs_mosi,
    ram_fil_coefs_miso => ram_fil_coefs_miso,

    in_sosi_arr        => wpfb_unit_in_sosi_arr,
    fil_sosi_arr       => wpfb_unit_fil_sosi_arr,
    out_quant_sosi_arr => wpfb_unit_out_quant_sosi_arr,
    out_raw_sosi_arr   => wpfb_unit_out_raw_sosi_arr,

    dp_bsn_source_restart => dp_bsn_source_restart_pipe
  );

  ---------------------------------------------------------------
  -- SUBBAND EQUALIZER
  ---------------------------------------------------------------
  u_sdp_subband_equalizer : entity work.sdp_subband_equalizer
    generic map (
      g_gains_file_name => c_gains_file_name,
      g_raw_dat_w       => c_subband_raw_dat_w,
      g_raw_fraction_w  => c_subband_raw_fraction_w
    )
    port map(
      dp_clk         => dp_clk,
      dp_rst         => dp_rst,

      in_raw_sosi_arr    => wpfb_unit_out_raw_sosi_arr,
      out_raw_sosi_arr   => subband_equalizer_raw_sosi_arr,
      out_quant_sosi_arr => subband_equalizer_quant_sosi_arr,

      mm_rst         => mm_rst,
      mm_clk         => mm_clk,

      ram_gains_mosi => ram_gains_mosi,
      ram_gains_miso => ram_gains_miso,
      ram_gains_cross_mosi => ram_gains_cross_mosi,
      ram_gains_cross_miso => ram_gains_cross_miso
    );

  -- Output fsub streams
  fsub_quant_sosi_arr <= subband_equalizer_quant_sosi_arr;
  fsub_raw_sosi_arr <= subband_equalizer_raw_sosi_arr;

  ---------------------------------------------------------------
  -- DP SELECTOR for SST input
  ---------------------------------------------------------------
  u_dp_selector_arr : entity dp_lib.dp_selector_arr
    generic map (
      g_nof_arr => c_sdp_P_pfb,
      g_pipeline => c_sdp_subband_equalizer_latency
    )
    port map (
      mm_rst => mm_rst,
      mm_clk => mm_clk,
      dp_rst => dp_rst,
      dp_clk => dp_clk,

      reg_selector_mosi => reg_selector_mosi,
      reg_selector_miso => reg_selector_miso,

      pipe_sosi_arr  => wpfb_unit_out_quant_sosi_arr,
      ref_sosi_arr   => subband_equalizer_quant_sosi_arr,
      out_sosi_arr   => dp_selector_quant_sosi_arr,

      selector_en    => selector_en
    );

  ---------------------------------------------------------------
  -- SIGNAL SCOPE
  ---------------------------------------------------------------
  -- synthesis translate_off
  u_sdp_scope_wpfb : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_subband,
      g_nof_input      => c_sdp_P_pfb,
      g_n_deinterleave => c_sdp_Q_fft,
      g_dat_w          => c_sdp_W_subband
    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr    => wpfb_unit_out_quant_sosi_arr,
      scope_sosi_arr => scope_wpfb_unit_out_quant_sosi_arr
    );

  u_sdp_scope_equalizer : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_subband,
      g_nof_input      => c_sdp_P_pfb,
      g_n_deinterleave => c_sdp_Q_fft,
      g_dat_w          => c_sdp_W_subband
    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr    => subband_equalizer_quant_sosi_arr,
      scope_sosi_arr => scope_equalizer_quant_sosi_arr
    );
  -- synthesis translate_on

  ---------------------------------------------------------------
  -- SUBBAND STATISTICS
  ---------------------------------------------------------------
  gen_stats_streams: for I in 0 to c_sdp_P_pfb - 1 generate
      u_subband_stats : entity st_lib.st_sst
      generic map(
        g_nof_stat      => c_sdp_N_sub * c_sdp_Q_fft,
        g_in_data_w     => c_sdp_W_subband,
        g_stat_data_w   => g_wpfb.stat_data_w,
        g_stat_data_sz  => g_wpfb.stat_data_sz,
        g_stat_multiplex => c_sdp_Q_fft
      )
      port map (
        mm_rst          => mm_rst,
        mm_clk          => mm_clk,
        dp_rst          => dp_rst,
        dp_clk          => dp_clk,
        in_complex      => dp_selector_quant_sosi_arr(I),
        ram_st_sst_mosi => ram_st_sst_mosi_arr(I),
        ram_st_sst_miso => ram_st_sst_miso_arr(I)
      );
  end generate;

  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES OF SST
  ---------------------------------------------------------------
  -- Combine the internal array of mm interfaces for the subband
  -- statistics to one array.
  u_mem_mux_sst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_P_pfb,
    g_mult_addr_w => ceil_log2(c_sdp_N_sub * c_sdp_Q_fft * g_wpfb.stat_data_sz)
  )
  port map (
    mosi     => master_mem_mux_mosi,
    miso     => master_mem_mux_miso,
    mosi_arr => ram_st_sst_mosi_arr,
    miso_arr => ram_st_sst_miso_arr
  );

  -- Connect 2 mm_masters to the common_mem_mux output
  master_mosi_arr(0)  <= ram_st_sst_mosi;  -- MM access via QSYS MM bus
  ram_st_sst_miso     <= master_miso_arr(0);
  master_mosi_arr(1)  <= ram_st_offload_mosi;  -- MM access by SST offload
  ram_st_offload_miso <= master_miso_arr(1);

  u_mem_master_mux : entity mm_lib.mm_master_mux
  generic map (
    g_nof_masters    => c_nof_masters,
    g_rd_latency_min => 1  -- read latency of statistics RAM is 1
  )
  port map (
    mm_clk => mm_clk,

    master_mosi_arr => master_mosi_arr,
    master_miso_arr => master_miso_arr,
    mux_mosi        => master_mem_mux_mosi,
    mux_miso        => master_mem_mux_miso
  );

  ---------------------------------------------------------------
  -- STATISTICS OFFLOAD
  ---------------------------------------------------------------
  weighted_subbands_flag <= not selector_en when rising_edge(dp_clk);

  u_sdp_sst_udp_offload: entity work.sdp_statistics_offload
  generic map (
    g_statistics_type   => "SST",
    g_offload_node_time => sel_a_b(g_sim, g_sim_sdp.offload_node_time, c_sdp_offload_node_time)
  )
  port map (
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    master_mosi => ram_st_offload_mosi,
    master_miso => ram_st_offload_miso,

    reg_enable_mosi  => reg_enable_mosi,
    reg_enable_miso  => reg_enable_miso,

    reg_hdr_dat_mosi  => reg_hdr_dat_mosi,
    reg_hdr_dat_miso  => reg_hdr_dat_miso,

    reg_bsn_monitor_v2_offload_copi => reg_bsn_monitor_v2_sst_offload_copi,
    reg_bsn_monitor_v2_offload_cipo => reg_bsn_monitor_v2_sst_offload_cipo,

    in_sosi      => dp_selector_quant_sosi_arr(0),
    new_interval => dp_bsn_source_new_interval,

    out_sosi  => sst_udp_sosi,
    out_siso  => sst_udp_siso,

    eth_src_mac  => eth_src_mac,
    udp_src_port => udp_src_port,
    ip_src_addr  => ip_src_addr,

    gn_index                => TO_UINT(gn_id),
    sdp_info                => sdp_info,
    weighted_subbands_flag  => weighted_subbands_flag
  );
end str;
