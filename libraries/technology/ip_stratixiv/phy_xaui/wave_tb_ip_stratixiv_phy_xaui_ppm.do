onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group #vsim_capacity# -format Analog-Step -max 13.0 -radix unsigned /#vsim_capacity#/totals
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/classes
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/qdas
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/assertions
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/covergroups
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/solver
add wave -noupdate -group #vsim_capacity# -radix unsigned /#vsim_capacity#/memories
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/pll_ref_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xgmii_tx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xgmii_rx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xgmii_rx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xgmii_tx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xaui_rx_serial_data
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/xaui_tx_serial_data
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_ready
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/tx_ready
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_clk_reset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_address
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_read
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_readdata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_write
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_writedata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/phy_mgmt_waitrequest
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_digitalreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/tx_digitalreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_channelaligned
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_syncstatus
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_disperr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_errdetect
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_analogreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_invpolarity
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_set_locktodata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_set_locktoref
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_seriallpbken
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/tx_invpolarity
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_is_lockedtodata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_phase_comp_fifo_error
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_is_lockedtoref
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_rlv
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_rmfifoempty
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_rmfifofull
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/tx_phase_comp_fifo_error
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_patterndetect
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_rmfifodatadeleted
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_rmfifodatainserted
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/rx_runningdisp
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/cal_blk_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/pll_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/gxb_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/pll_locked
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/reconfig_from_xcvr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/u_ip_phy_xaui/reconfig_to_xcvr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xaui_tx_serial_out
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xaui_rx_serial_in
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/tr_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/tx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/rx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/mm_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/mm_rst
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/link_fault
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xaui_tx_serial
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xaui_tx_serial_dly
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xaui_rx_serial
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_tx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_rx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_tx_c
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_rx_c
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_tx_d
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/xgmii_rx_d
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_0 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_0/rx_channelaligned
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/pll_ref_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xgmii_tx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xgmii_rx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xgmii_rx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xgmii_tx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xaui_rx_serial_data
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/xaui_tx_serial_data
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_ready
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/tx_ready
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_clk_reset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_address
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_read
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_readdata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_write
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_writedata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/phy_mgmt_waitrequest
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_digitalreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/tx_digitalreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_channelaligned
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_syncstatus
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_disperr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_errdetect
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_analogreset
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_invpolarity
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_set_locktodata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_set_locktoref
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_seriallpbken
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/tx_invpolarity
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_is_lockedtodata
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_phase_comp_fifo_error
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_is_lockedtoref
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_rlv
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_rmfifoempty
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_rmfifofull
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/tx_phase_comp_fifo_error
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_patterndetect
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_rmfifodatadeleted
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_rmfifodatainserted
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/rx_runningdisp
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/cal_blk_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/pll_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/gxb_powerdown
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/pll_locked
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/reconfig_from_xcvr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -group u_ip_phy_xaui -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/u_ip_phy_xaui/reconfig_to_xcvr
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xaui_tx_serial_out
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xaui_rx_serial_in
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/tr_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/tx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/rx_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/mm_clk
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/mm_rst
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/link_fault
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xaui_tx_serial
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xaui_tx_serial_dly
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xaui_rx_serial
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_tx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_rx_dc
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_tx_c
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_rx_c
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_tx_d
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix hexadecimal /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/xgmii_rx_d
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -expand -group u_tb_ip_stratixiv_phy_xaui_1 -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/u_tb_ip_stratixiv_phy_xaui_1/rx_channelaligned
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/xaui_tx_serial_0
add wave -noupdate -expand -group tb_ip_stratixiv_phy_xaui_ppm -radix unsigned /tb_ip_stratixiv_phy_xaui_ppm/xaui_tx_serial_1
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4624000000 fs} 0} {{Cursor 2} {1056063523 fs} 0}
configure wave -namecolwidth 300
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {5250 ns}
