-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_reg_cross_domain is
end tb_common_reg_cross_domain;

architecture tb of tb_common_reg_cross_domain is
  --CONSTANT in_clk_period    : TIME := 10 ns;
  constant in_clk_period    : time := 17 ns;
  constant out_clk_period   : time := 17 ns;
  constant c_interval       : natural := 19;
  --CONSTANT c_interval       : NATURAL := 20;

  constant c_in_new_latency : natural := 2;  -- use <2 and =2 to verify s_pulse_latency
  constant c_dat_w          : natural := 8;

  -- The state name tells what kind of test is done
  type t_state_enum is (
    s_idle,
    s_pulse,
    s_pulse_latency,
    s_level,
    s_done
  );

  signal state     : t_state_enum;

  signal in_rst    : std_logic;
  signal out_rst   : std_logic;
  signal in_clk    : std_logic := '0';
  signal out_clk   : std_logic := '0';
  signal in_new    : std_logic := '0';
  signal in_done   : std_logic;
  signal in_dat    : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_dat   : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_new   : std_logic;
begin
  in_clk  <= not in_clk  after in_clk_period / 2;
  out_clk <= not out_clk after out_clk_period / 2;

  p_in_stimuli : process
  begin
    state  <= s_idle;
    in_rst <= '1';
    in_new <= '0';
    in_dat <= (others => '0');
    wait until rising_edge(in_clk);
    in_rst <= '0';
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;

    -- Single in_new pulse
    state <= s_pulse;
    in_dat <= INCR_UVEC(in_dat, 1);
    wait until rising_edge(in_clk);
    in_new <= '1';
    wait until rising_edge(in_clk);
    in_new <= '0';
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;

    -- Single in_new pulse with latency 2
    state <= s_pulse_latency;
    in_new <= '1';
    wait until rising_edge(in_clk);
    in_new <= '0';
    wait until rising_edge(in_clk);
    in_dat <= INCR_UVEC(in_dat, 1);
    wait until rising_edge(in_clk);
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;

    -- Continue level high in_new
    state <= s_level;
    in_dat <= INCR_UVEC(in_dat, 1);
    wait until rising_edge(in_clk);
    in_new <= '1';
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;
    in_dat <= INCR_UVEC(in_dat, 1);
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;
    in_dat <= INCR_UVEC(in_dat, 1);
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;
    in_new <= '0';
    for I in 0 to c_interval loop wait until rising_edge(in_clk); end loop;

    state <= s_done;
    wait;
  end process;

  u_out_rst : entity work.common_areset
  port map (
    in_rst   => in_rst,
    clk      => out_clk,
    out_rst  => out_rst
  );

  u_reg_cross_domain : entity work.common_reg_cross_domain
  generic map (
    g_in_new_latency => c_in_new_latency
  )
  port map (
    in_rst     => in_rst,
    in_clk     => in_clk,
    in_new     => in_new,  -- when '1' then new in_dat is available after g_in_new_latency
    in_dat     => in_dat,
    in_done    => in_done,
    out_rst    => out_rst,
    out_clk    => out_clk,
    out_dat    => out_dat,
    out_new    => out_new  -- when '1' then the out_dat was updated with in_dat due to in_new
  );
end tb;
