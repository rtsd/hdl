

Simulation
----------
-> Read ../../doc/README first until step 3
Modelsim instructions:

    # in Modelsim do:
    lp unb2a_test_1GbE
    mk all
    # now double click on testbench file tb_unb2a_test_1GbE.vhd
    as 10
    run 500us


    # while the simulation runs... in another terminal/bash session do:
    cd unb2a_test/tb/python

    # To read out the design_name, ppsh and sensors; do:
    python tc_unb2a_test.py --sim --unb 0 --fn 3 --seq INFO,PPSH,SENSORS 
    # (sensor results only show up after 1000us of simulation runtime)

    # To test the 1GbE offload; do:
    python tc_unb2a_test.py --sim --unb 0 --fn 3 --seq BGDB -s 1GBE -r 0


    # to end simulation in Modelsim do:
    quit -sim



Testing on hardware
-------------------
-> Read ../../doc/README first until step 5

# (assume that the Uniboard is --unb 1)

# To read out the design_name, ppsh and sensors; do:

python tc_unb2a_test.py --unb 1 --fn 0:3 --seq REGMAP,INFO,PPSH,SENSORS -v5

# To test the 1GbE offload:

python tc_unb2a_test.py --unb 1 --fn 0 --seq BGDB -s 1GBE -r 0

