/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Reinier vd Walle
* Purpose:
* . Demonstrate Monitor and Control interface
* Description:
* . This application implements a way to use the MM IO channels.
* *********************************************************************** */

#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

#define DIVIDE_AND_ROUND_UP(A,B) (((A)+(B)-1)/(B))

enum mm_channel {
  CH_PROCESS_A,
  CH_PROCESS_B,
  LAST_MM_CHANNEL_ENTRY
};


struct param_process_a_struct {
  uint keep;
  uint acc;
};

struct param_process_b_struct {
  uint keep;
  uint acc;
};

union param_process_a {
  struct param_process_a_struct parameters;
  uint arr[DIVIDE_AND_ROUND_UP(sizeof(struct param_process_a_struct),sizeof(uint))];
};

union param_process_b {
  struct param_process_b_struct parameters;
  uint arr[DIVIDE_AND_ROUND_UP(sizeof(struct param_process_b_struct),sizeof(uint))];
};


struct reg {
  uint offset;
  uint size;
} __attribute__((packed));


struct mm_in {
  uint wrdata;
  uint address;
  bool wr;
} __attribute__((packed));

struct mm_out {
  uint rddata;
} __attribute__((packed));

channel struct mm_in ch_in_mm  __attribute__((depth(0))) __attribute__((io("kernel_input_mm")));
channel struct mm_out ch_out_mm  __attribute__((depth(0))) __attribute__((io("kernel_output_mm")));

channel struct mm_in mm_channel_in[LAST_MM_CHANNEL_ENTRY] __attribute__((depth(0)));
channel struct mm_out mm_channel_out[LAST_MM_CHANNEL_ENTRY+1] __attribute__((depth(0))); // 1 extra channel for undefined addresses


__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void mm_in_controller()
{
  // Regmap table with offset, size
  const struct reg regmap[LAST_MM_CHANNEL_ENTRY] = {
    {0x00, DIVIDE_AND_ROUND_UP(sizeof(struct param_process_a_struct),sizeof(uint))},
    {0x02, DIVIDE_AND_ROUND_UP(sizeof(struct param_process_b_struct),sizeof(uint))}
  };
  while(1)
  {
    bool undefined = true;
    struct mm_in mm_request = read_channel_intel(ch_in_mm);
    #pragma unroll
    for (int i = 0; i < LAST_MM_CHANNEL_ENTRY; i++)
    {
      if (mm_request.address >= regmap[i].offset && mm_request.address < (regmap[i].offset + regmap[i].size))
      {
        undefined = false;
        struct mm_in local_mm_request;
        local_mm_request.wr = mm_request.wr;
        local_mm_request.wrdata = mm_request.wrdata;
        local_mm_request.address = mm_request.address - regmap[i].offset;
        write_channel_intel(mm_channel_in[i], local_mm_request);
      } 
    }

    if (undefined && mm_request.wr == 0)  { // undefined address
      struct mm_out zero_response;
      zero_response.rddata = 0;
      write_channel_intel(mm_channel_out[LAST_MM_CHANNEL_ENTRY], zero_response);
    }
  }
}

__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void mm_out_controller()
{
#ifdef EMULATOR
  for(int x = 0; x < 4; )
#else
  while(1)
#endif
  {
    struct mm_out mm_response;
    for (int i = 0; i < LAST_MM_CHANNEL_ENTRY+1; i++)
    {
      bool valid;
      mm_response = read_channel_nb_intel(mm_channel_out[i], &valid);
      if (valid)
      {
        write_channel_intel(ch_out_mm, mm_response);
#ifdef EMULATOR
        x++;
#endif
      }
    }
  }
}


__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void process_a()
{
  union param_process_a reg;
  reg.parameters.keep = 0; //address 0, value is stored when written
  reg.parameters.acc = 0;  //address 1, value is written value + 1
  while(1){
    // handle MM read/write requests
    struct mm_in mm_request = read_channel_intel(mm_channel_in[CH_PROCESS_A]); //blocking read
    struct mm_out mm_response;
    if(mm_request.wr) //write request
    {
      reg.arr[mm_request.address] = mm_request.wrdata;
    } else { //read request
      mm_response.rddata = reg.arr[mm_request.address];
      write_channel_intel(mm_channel_out[CH_PROCESS_A], mm_response);
    }

    // Do someting with parameters
    if(mm_request.wr > 0 && mm_request.address == 1)
      reg.parameters.acc += 1;
  }
}


__attribute__((max_global_work_dim(0)))
#ifndef EMULATOR
__attribute__((autorun))
#endif
__kernel void process_b()
{ 
  union param_process_b reg;
  reg.parameters.keep = 0; //address 0, value is stored when written
  reg.parameters.acc = 0;  //address 1, value is written value + 2
  while(1){
    // handle MM read/write requests
    struct mm_in mm_request = read_channel_intel(mm_channel_in[CH_PROCESS_B]); //blocking read
    struct mm_out mm_response;
    if(mm_request.wr) //write request
    {
      reg.arr[mm_request.address] = mm_request.wrdata;
    } else { //read request
      mm_response.rddata = reg.arr[mm_request.address];
      write_channel_intel(mm_channel_out[CH_PROCESS_B], mm_response);
    }

    // Do someting with parameters
    if(mm_request.wr && mm_request.address == 1)
      reg.parameters.acc += 2;

  }
}


__attribute__((max_global_work_dim(0)))
__kernel void dummy()
{
}

