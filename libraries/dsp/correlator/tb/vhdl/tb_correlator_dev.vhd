--------------------------------------------------------------------------------
--
-- Author: Daniel van der Schuur
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.tb_common_pkg.all;
use diag_lib.diag_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.tb_dp_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use work.corr_permutor_pkg.all;
use common_lib.common_str_pkg.all;

entity tb_correlator_dev is
  generic (
    g_data_w               : natural := 16;
    g_nof_inputs           : natural := 4;
    g_nof_mults            : natural := 5;
    g_nof_cycles           : natural := 2;
    g_nof_channels         : natural := 8;
    g_nof_channel_accs     : natural := 1000;
    g_flow_control_stimuli : t_dp_flow_control_enum := e_active  -- always active, random or pulse flow control
  );
end tb_correlator_dev;

architecture tb of tb_correlator_dev is
  -----------------------------------------------------------------------------
  -- Clock & reset
  -----------------------------------------------------------------------------
  constant c_clk_period         : time := 5 ns;
  signal tb_end                 : std_logic := '0';
  signal clk                    : std_logic := '1';
  signal rst                    : std_logic;

  -----------------------------------------------------------------------------
  -- Input stimuli
  -----------------------------------------------------------------------------
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;
  signal pulse_en                     : std_logic := '1';
  signal pulse                        : std_logic;
  signal random                       : std_logic_vector(14 downto 0) := (others => '0');
  signal dp_gen_data_en               : std_logic;
  signal dp_gen_data_src_in_arr       : t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);  -- We're using dp_gen_data_en to introduce gaps.
  signal dp_gen_data_src_out_arr      : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal in_valid_count               : natural;
  signal nxt_in_valid_count           : natural;
  signal int_period_cnt               : natural;
  signal nxt_int_period_cnt           : natural;

  -----------------------------------------------------------------------------
  -- Correlator
  -----------------------------------------------------------------------------
  signal correlator_snk_in_arr  : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal correlator_snk_out_arr : t_dp_siso_arr(g_nof_inputs - 1 downto 0);
  signal correlator_src_out_arr : t_dp_sosi_arr(g_nof_mults - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Output verification
  -----------------------------------------------------------------------------
  constant c_nof_permutations   : natural := g_nof_inputs * (g_nof_inputs + 1) / 2;
  constant c_permutation_table  : t_corr_permutation_table := corr_permute(g_nof_inputs, g_nof_cycles);  -- Returns 3D array of dimensions nof_folding_cycles*g_nof_outputs*2 input indices

  signal out_valid_count              : integer := 0;
--  SIGNAL nxt_out_valid_count          : NATURAL;
begin
  -----------------------------------------------------------------------------
  -- Clocks and reset
  -----------------------------------------------------------------------------
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Input data
  -- . We're using simple counter data with the counter value matching the
  --   channel index.
  -- . So when we want the XC to fold the data, we need to introduce gaps according
  --   to g_nof_cycles.
  --   . Less than g_nof_cycles-1 idle cycles are not allowed, but more are allowed.
  --     => How do we make this robust, and test this properly?
  -----------------------------------------------------------------------------
  gen_stimuli : for i in 0 to g_nof_inputs - 1 generate
    -- Counter data generation
    proc_dp_gen_data(1, g_data_w, 0, g_nof_channels - 1, rst, clk, dp_gen_data_en, dp_gen_data_src_in_arr(i), dp_gen_data_src_out_arr(i));
    -- Toggle dp_gen_data_en to introduce gaps
    random <= func_common_random(random) when rising_edge(clk);
    proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period, '1', rst, clk, pulse_en, pulse);

    dp_gen_data_en <= '1'                  and correlator_snk_out_arr(0).ready when g_flow_control_stimuli = e_active and rst = '0' else
                       random(random'high) and correlator_snk_out_arr(0).ready when g_flow_control_stimuli = e_random and rst = '0' else
                       pulse               and correlator_snk_out_arr(0).ready when g_flow_control_stimuli = e_pulse  and rst = '0' else '0';
  end generate;

  -- Count the number of valid input cycles so we know when we can stop the simulation using tb_end
  p_cnt_in_val : process(in_valid_count, int_period_cnt, dp_gen_data_src_out_arr)
  begin
    nxt_in_valid_count <= in_valid_count;
    nxt_int_period_cnt <= int_period_cnt;
    if dp_gen_data_src_out_arr(0).valid = '1' then
      nxt_in_valid_count <= in_valid_count + 1;
      if in_valid_count > 0 and (in_valid_count mod (g_nof_channels * g_nof_channel_accs)) = 0 then
        nxt_int_period_cnt <= int_period_cnt + 1;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- correlator
  -----------------------------------------------------------------------------
  gen_correlator_snk_in_arr : for i in 0 to g_nof_inputs - 1 generate
    correlator_snk_in_arr(i).valid <= dp_gen_data_src_out_arr(i).valid;
    correlator_snk_in_arr(i).re    <= TO_DP_DSP_DATA(i);  -- Real = Input index
    correlator_snk_in_arr(i).im    <= RESIZE_DP_DSP_DATA(dp_gen_data_src_out_arr(i).data);  -- Imag = Channel index
    -- Create a block sync of g_nof_channels wide
    correlator_snk_in_arr(0).sync  <= '1' when (in_valid_count >= (int_period_cnt + 1) * g_nof_channels * g_nof_channel_accs - g_nof_channels and
                                                in_valid_count < (int_period_cnt + 1) * g_nof_channels * g_nof_channel_accs and
                                                correlator_snk_in_arr(0).valid = '1') else '0';
  end generate;

  u_correlator_dev : entity work.correlator_dev
  generic map (
    g_data_w           => g_data_w,
    g_nof_inputs       => g_nof_inputs,
    g_nof_mults        => g_nof_mults,
    g_nof_cycles       => g_nof_cycles,
    g_nof_channels     => g_nof_channels,
    g_nof_channel_accs => g_nof_channel_accs
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in_arr  => correlator_snk_in_arr,
    snk_out_arr => correlator_snk_out_arr,
    src_out_arr => correlator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Output verification
  -----------------------------------------------------------------------------
  -- TODO: Use c_permutation_table to find out which input pairs provided
  --       input for given output at given clock cycle. Then use this info
  --       to derive the input data and calculate the accumulated cross product.
  -- 1) Get current output count
  -- 2) For each output stream, figure out which folding clock cycle we're in
  --    based on current output count
  -- 3) Get the input pairs that produced the input on that cycle
  -- 4) Figure out what the input data was, calc output.

--  p_verify_corr_accumulator_src_out_arr : PROCESS(out_valid_count, correlator_src_out_arr)
--  BEGIN
--    nxt_out_valid_count    <= out_valid_count;
--    IF correlator_src_out_arr(0).valid = '1' THEN
--      nxt_out_valid_count <= out_valid_count+1;
--
--      wait until rising_edge(clk);
--      -- Print some information
--      print_str("Correlator output valid count: " & int_to_str(out_valid_count));
--    END IF;
--
--  END PROCESS;
--
  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk : process (clk, rst)
  begin
    if rst = '1' then
--      output_channel  <= 0;
--      expected_data   <= (OTHERS=>'0');
--      out_valid_count <= 0;
      in_valid_count  <= 0;
      int_period_cnt  <= 0;
     elsif rising_edge(clk) then
--      output_channel  <= nxt_output_channel;
--      expected_data   <= nxt_expected_data;
--      out_valid_count <= nxt_out_valid_count;
      in_valid_count  <= nxt_in_valid_count;
      int_period_cnt  <= nxt_int_period_cnt;
    end if;
  end process;

  -- cycle 0: vis 0..9
  -- cycle 1: vis 10..19

  p_verify: process(correlator_src_out_arr, clk, out_valid_count)
  begin
    if correlator_src_out_arr(0).valid = '1' then
      if rising_edge(clk) then
        out_valid_count <= out_valid_count + 1;

        -- Print some information
        print_str("Correlator output valid count: " & int_to_str(out_valid_count));
        for i in 0 to g_nof_inputs - 1 loop
--          print_str(". Channel " & int_to_str(g_nof_channels)
          print_str(". Output " & int_to_str(i) & ", real: " & int_to_str(TO_UINT(correlator_src_out_arr(i).re)));
          print_str(". Output " & int_to_str(i) & ", imag: " & int_to_str(TO_UINT(correlator_src_out_arr(i).im)));
        end loop;
        print_str(". cycle " & int_to_str(g_nof_cycles));
      end if;
    end if;
  end process;
end tb;
