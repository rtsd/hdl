-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author
-- . Daniel van der Schuur
-- Purpose
-- . Generate input streams for dp_switch, verify by eye in wave window.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_switch is
end tb_dp_switch;

architecture tb of tb_dp_switch is
  constant c_nof_inputs                  : natural := 3;
  constant c_data_w                      : natural := 32;
  constant c_nof_packets                 : natural := 30;
  constant c_packet_len                  : natural := 20;
  constant c_packet_gap                  : natural := 1;  -- NOTE: dp_mux requires a minimum gap of 1 to select a new input!

  constant c_dp_clk_period               : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period               : time := 20 ns;  -- 50 MHz

  constant c_nof_switch_runs             : natural := 2;

  signal tb_end                          : std_logic := '0';
  signal dp_clk                          : std_logic := '1';
  signal dp_rst                          : std_logic;

  signal mm_clk                          : std_logic := '1';
  signal mm_rst                          : std_logic;

  signal dp_gen_block_data_in_en         : std_logic := '1';

  signal dp_gen_block_data_src_in_arr    : t_dp_siso_arr(c_nof_inputs - 1 downto 0);
  signal dp_gen_block_data_src_out_arr   : t_dp_sosi_arr(c_nof_inputs - 1 downto 0);

  signal dp_switch_snk_in_arr            : t_dp_sosi_arr(c_nof_inputs - 1 downto 0);
  signal dp_switch_snk_out_arr           : t_dp_siso_arr(c_nof_inputs - 1 downto 0);

  signal dp_switch_src_out               : t_dp_sosi;
  signal dp_switch_src_in                : t_dp_siso := c_dp_siso_rdy;

  signal reg_dp_switch_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_switch_miso              : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- Clock,reset generation
  -----------------------------------------------------------------------------
  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Generate continuous packet streams, with and without gaps
  -----------------------------------------------------------------------------
  dp_gen_block_data_src_in_arr <= dp_switch_snk_out_arr;

  gen_generate_packets : for i in 0 to c_nof_inputs - 1 generate
    p_generate_packets : process
    begin
      dp_gen_block_data_src_out_arr(i) <= c_dp_sosi_rst;

      proc_common_wait_until_low(dp_clk, dp_rst);
      --proc_common_wait_some_cycles(dp_clk, 4);

      -- Generate single packet
      proc_dp_gen_block_data(c_data_w, i * 1000, c_packet_len, 0, 0, '0', "0", dp_clk, dp_gen_block_data_in_en, dp_gen_block_data_src_in_arr(i), dp_gen_block_data_src_out_arr(i));

      -- Insert optional gap between the packets, (0 .. c_nof_inputs-1) gaps
      proc_common_wait_some_cycles(dp_clk, i);
      wait for 0 ns;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- MM write different input selections and check output
  -----------------------------------------------------------------------------
  p_switch_input_stimulus : process
  begin
    proc_common_wait_until_low(mm_clk, mm_rst);
    reg_dp_switch_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 1);
    for I in 0 to c_nof_switch_runs - 1 loop
      for J in 0 to c_nof_inputs - 1 loop
        -- write new input selection to mm bus and wait some time
        proc_mem_mm_bus_wr(0, J, mm_clk, reg_dp_switch_mosi);
        proc_common_wait_some_cycles(mm_clk, 50);

        -- check if channel number in src_out is the same as requested (J)
        assert J = TO_UINT(dp_switch_src_out.channel)
          report "Wrong src_out channel" &
                 ", expected=" & int_to_str(J) &
                 ", readback=" & int_to_str(integer(TO_UINT(dp_switch_src_out.channel)))
          severity ERROR;

        -- check if data value in src_out is in the expected value window (J*1000 <= data < (J+1)*1000))
        assert TO_UINT(dp_switch_src_out.data) >= J * 1000 and TO_UINT(dp_switch_src_out.data) < (J + 1) * 1000
          report "Wrong src_out data" &
                 ", expected between=" & int_to_str(J * 1000) & " and " & int_to_str((J + 1) * 1000) &
                 ", readback=" & int_to_str(integer(TO_UINT(dp_switch_src_out.data)))
          severity ERROR;
      end loop;
    end loop;

    -- check if output data is stopped if output send hold signal
    dp_switch_src_in <= c_dp_siso_hold;
    proc_common_wait_some_cycles(mm_clk, 50);
    assert dp_switch_src_out.valid = '0'
      report "Still data on output after setting hold command"
      severity ERROR;

    -- check if output data flows again if output send flush signal
    dp_switch_src_in <= c_dp_siso_rdy;
    proc_common_wait_some_cycles(mm_clk, 50);
    assert dp_switch_src_out.valid = '1'
      report "No data on output after setting flush command"
      severity ERROR;

    tb_end <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- dp_switch forwards either stream 0 or stream 1
  -- . Both inputs carry the same generated stream
  -----------------------------------------------------------------------------
  gen_dp_switch_snk_in_arr : for i in 0 to c_nof_inputs - 1 generate
    dp_switch_snk_in_arr(i) <= dp_gen_block_data_src_out_arr(i);
  end generate;

  u_dp_switch : entity work.dp_switch
  generic map (
    g_nof_inputs      => c_nof_inputs,
    g_default_enabled => 0
   )
  port map (
    dp_clk      => dp_clk,
    dp_rst      => dp_rst,

    mm_clk      => mm_clk,
    mm_rst      => mm_rst,

    snk_in_arr  => dp_switch_snk_in_arr,
    snk_out_arr => dp_switch_snk_out_arr,

    src_out     => dp_switch_src_out,
    src_in      => dp_switch_src_in,

    reg_mosi    => reg_dp_switch_mosi,
    reg_miso    => reg_dp_switch_miso
  );
end tb;
