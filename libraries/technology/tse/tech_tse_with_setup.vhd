-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra
-- Purpose: Instantiate and setup TSE via MM
-- Description:
-- . Based on tech_tse instance in eth.vhd
-- . Set up TSE in state machnine and then switch to external mm_ctlr, to
--   allow external monitoring of the TSE.

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_tse_pkg.all;

entity tech_tse_with_setup is
  generic (
    g_technology   : natural := c_tech_select_default;
    g_ETH_PHY      : string  := "LVDS";  -- "LVDS" (default): uses LVDS IOs for ctrl_unb_common, "XCVR": uses tranceiver PHY
    g_jumbo_en     : boolean := false;
    g_sim          : boolean := false;
    g_sim_level    : natural := 0;  -- 0 = use IP model (equivalent to g_sim = FALSE); 1 = use fast serdes model;
    g_sim_tx       : boolean := true;
    g_sim_rx       : boolean := true
  );
  port (
    -- Clocks and reset
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;  -- MM
    eth_clk        : in  std_logic;  -- 125 MHz
    tx_snk_clk     : in  std_logic;  -- DP
    rx_src_clk     : in  std_logic;  -- DP

    -- TSE setup
    src_mac        : in std_logic_vector(c_48 - 1 downto 0);
    setup_done     : out std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk    : in  std_logic := '0';

    -- Memory Mapped Peripheral
    mm_ctlr_copi   : in  t_mem_copi;
    mm_ctlr_cipo   : out t_mem_cipo;

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      : in  t_dp_sosi;
    tx_snk_out     : out t_dp_siso;

    -- MAC receive interface
    -- . ST Source
    rx_src_in      : in  t_dp_siso;
    rx_src_out     : out t_dp_sosi;

    -- PHY interface
    eth_txp        : out std_logic;
    eth_rxp        : in  std_logic;

    tse_led        : out t_tech_tse_led
  );
end tech_tse_with_setup;

architecture str of tech_tse_with_setup is
  -- Peripheral side
  signal mm_peri_copi    : t_mem_copi;
  signal mm_peri_cipo    : t_mem_cipo;

  -- MAC specific
  signal tx_mac_in       : t_tech_tse_tx_mac;
  signal tx_mac_out      : t_tech_tse_tx_mac;
  signal rx_mac_out      : t_tech_tse_rx_mac;

  signal tx_sosi         : t_dp_sosi;
begin
  -- Set up TSE as in unb_osy/unbos_eth.c
  u_tech_tse_setup : entity work.tech_tse_setup
  generic map (
    g_sim      => g_sim,
    g_jumbo_en => g_jumbo_en
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,

    -- TSE setup
    src_mac        => src_mac,
    setup_done     => setup_done,

    -- Memory Mapped Peripheral
    -- . Controller side
    mm_ctlr_copi    => mm_ctlr_copi,
    mm_ctlr_cipo    => mm_ctlr_cipo,
    -- . Peripheral side
    mm_peri_copi    => mm_peri_copi,
    mm_peri_cipo    => mm_peri_cipo
  );

  -- Force defaults as in eth.vhd
  tx_sosi <= func_dp_stream_error_set(tx_snk_in, 0);  -- force err field (value 0 for OK)

  tx_mac_in.crc_fwd <= '0';  -- when '0' then TSE MAC generates the TX CRC field

  u_tech_tse : entity work.tech_tse
  generic map (
    g_technology  => g_technology,
    g_ETH_PHY     => g_ETH_PHY,
    g_sim         => g_sim,
    g_sim_level   => g_sim_level,
    g_sim_tx      => g_sim_tx,
    g_sim_rx      => g_sim_rx
  )
  port map (
    -- Clocks and reset
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,
    eth_clk       => eth_clk,
    tx_snk_clk    => tx_snk_clk,
    rx_src_clk    => rx_src_clk,

    -- Calibration & reconfig clock
    cal_rec_clk   => cal_rec_clk,

    -- Memory Mapped Peripheral
    mm_sla_in     => mm_peri_copi,
    mm_sla_out    => mm_peri_cipo,

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in     => tx_sosi,
    tx_snk_out    => tx_snk_out,
    -- . MAC specific
    tx_mac_in     => tx_mac_in,
    tx_mac_out    => tx_mac_out,

    -- MAC receive interface
    -- . ST Source
    rx_src_in     => rx_src_in,
    rx_src_out    => rx_src_out,
    -- . MAC specific
    rx_mac_out    => rx_mac_out,

    -- PHY interface
    eth_txp       => eth_txp,
    eth_rxp       => eth_rxp,

    tse_led       => tse_led
  );
end architecture;
