-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for rTwoOrder
-- Features:
--
-- Usage:
-- > as 10
-- > run -all
-- Observe manually in Wave Window that out_dat is the previous page in_dat.
-- Use g_bit_flip=false to ease manualy interpretation of out_dat.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_rTwoOrder is
end tb_rTwoOrder;

architecture tb of tb_rTwoOrder is
  constant c_clk_period : time := 10 ns;

  constant c_nof_points : natural := 8;
  constant c_dat_w      : natural := 10;

  signal tb_end    : std_logic := '0';
  signal rst       : std_logic;
  signal clk       : std_logic := '1';

  signal random_0  : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences

  signal in_dat    : std_logic_vector(c_dat_w - 1 downto 0) := TO_UVEC(1, c_dat_w);
  signal in_val    : std_logic;

  signal out_dat   : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_val   : std_logic;
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 3;

  random_0 <= func_common_random(random_0) when rising_edge(clk);

  in_dat <= INCR_UVEC(in_dat, 1) when rising_edge(clk) and in_val = '1';

  -- run 1 us
  p_stimuli : process
  begin
    in_val <= '0';
    wait until rst = '0';
    proc_common_wait_some_cycles(clk, 3);

    for J in 0 to 7 loop
      -- wait some time
--       in_val <= '0';
--       FOR I IN 0 TO 1 LOOP WAIT UNTIL rising_edge(clk); END LOOP;

      -- one block
      in_val <= not in_val;  -- toggling
      for I in 0 to c_nof_points - 1 loop
        --in_val <= NOT in_val;                 -- toggling
        --in_val <= random_0(random_0'HIGH);    -- random
        wait until rising_edge(clk);
      end loop;
    end loop;

    in_val <= '0';

    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- device under test
  u_dut : entity work.rTwoOrder
  generic map (
    g_nof_points  => c_nof_points,
    g_bit_flip    => false
  )
  port map (
    clk     => clk,
    rst     => rst,
    in_dat  => in_dat,
    in_val  => in_val,
    out_dat => out_dat,
    out_val => out_val
  );
end tb;
