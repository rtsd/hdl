# These constraints contain LL_MEMBER_OF assignments that do not support wildcards.
# Copy this TCL file to your_design/src/tcl and modify it to use absolute
# hierarchy paths, then refer to it in the design QIP.
set_global_assignment -name LL_ENABLED ON -section_id Region_0
set_global_assignment -name LL_RESERVED OFF -section_id Region_0
set_global_assignment -name LL_SECURITY_ROUTING_INTERFACE OFF -section_id Region_0
set_global_assignment -name LL_IGNORE_IO_BANK_SECURITY_CONSTRAINT OFF -section_id Region_0
set_global_assignment -name LL_ENABLED ON -section_id Region_1
set_global_assignment -name LL_AUTO_SIZE OFF -section_id Region_1
set_global_assignment -name LL_STATE LOCKED -section_id Region_1
set_global_assignment -name LL_RESERVED OFF -section_id Region_1
set_global_assignment -name LL_SECURITY_ROUTING_INTERFACE OFF -section_id Region_1
set_global_assignment -name LL_IGNORE_IO_BANK_SECURITY_CONSTRAINT OFF -section_id Region_1
set_global_assignment -name LL_WIDTH 44 -section_id Region_1
set_global_assignment -name LL_HEIGHT 34 -section_id Region_1
set_global_assignment -name LL_ORIGIN X39_Y60 -section_id Region_1
set_global_assignment -name LL_ENABLED ON -section_id Region_2
set_global_assignment -name LL_AUTO_SIZE OFF -section_id Region_2
set_global_assignment -name LL_STATE LOCKED -section_id Region_2
set_global_assignment -name LL_RESERVED OFF -section_id Region_2
set_global_assignment -name LL_SECURITY_ROUTING_INTERFACE OFF -section_id Region_2
set_global_assignment -name LL_IGNORE_IO_BANK_SECURITY_CONSTRAINT OFF -section_id Region_2
set_global_assignment -name LL_WIDTH 51 -section_id Region_2
set_global_assignment -name LL_HEIGHT 30 -section_id Region_2
set_global_assignment -name LL_ORIGIN X32_Y20 -section_id Region_2
set_global_assignment -name LL_ENABLED ON -section_id Region_3
set_global_assignment -name LL_AUTO_SIZE OFF -section_id Region_3
set_global_assignment -name LL_STATE LOCKED -section_id Region_3
set_global_assignment -name LL_RESERVED OFF -section_id Region_3
set_global_assignment -name LL_SECURITY_ROUTING_INTERFACE OFF -section_id Region_3
set_global_assignment -name LL_IGNORE_IO_BANK_SECURITY_CONSTRAINT OFF -section_id Region_3
set_global_assignment -name LL_WIDTH 19 -section_id Region_3
set_global_assignment -name LL_HEIGHT 67 -section_id Region_3
set_global_assignment -name LL_ORIGIN X98_Y28 -section_id Region_3
set_instance_assignment -name LL_MEMBER_OF Region_0 -to "node_fn_bf:u_node_fn_bf|bf:u_bf|bf_unit:\\gen_bf_units:0:u_bf_unit" -section_id Region_0
set_instance_assignment -name LL_MEMBER_OF Region_1 -to "node_fn_bf:u_node_fn_bf|bf:u_bf|bf_unit:\\gen_bf_units:1:u_bf_unit" -section_id Region_1
set_instance_assignment -name LL_MEMBER_OF Region_2 -to "node_fn_bf:u_node_fn_bf|bf:u_bf|bf_unit:\\gen_bf_units:2:u_bf_unit" -section_id Region_2
set_instance_assignment -name LL_MEMBER_OF Region_3 -to "node_fn_bf:u_node_fn_bf|bf:u_bf|bf_unit:\\gen_bf_units:3:u_bf_unit" -section_id Region_3
set_global_assignment -name LL_HEIGHT 19 -section_id Region_0
set_global_assignment -name LL_WIDTH 108 -section_id Region_0
set_global_assignment -name LL_ORIGIN X6_Y1 -section_id Region_0
set_global_assignment -name LL_STATE LOCKED -section_id Region_0
set_global_assignment -name LL_AUTO_SIZE OFF -section_id Region_0

