-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_iobuf_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_ddio_lib;
library ip_arria10_ddio_lib;
library ip_arria10_e3sge3_ddio_lib;
library ip_arria10_e1sg_ddio_lib;
library ip_arria10_e2sg_ddio_lib;
library ip_agi027_1e1v_ddio_lib;

entity tech_iobuf_ddio_in is
  generic (
    g_technology    : natural := c_tech_select_default;
    g_width         : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
end tech_iobuf_ddio_in;

architecture str of tech_iobuf_ddio_in is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_ddio_in
    generic map ("Stratix IV", g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_ddio_in
    generic map (g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_ddio_in
    generic map (g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_ddio_in
    generic map (g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_ddio_in
    generic map (g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;

  gen_ip_agi027_1e1v : if g_technology = c_tech_agi027_1e1v generate
    u0 : ip_agi027_1e1v_ddio_in
    generic map (g_width)
    port map (in_dat, in_clk, in_clk_en, rst, out_dat_hi, out_dat_lo);
  end generate;
end architecture;
