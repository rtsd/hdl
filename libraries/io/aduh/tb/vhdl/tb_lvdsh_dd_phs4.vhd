-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for lvdsh_dd_phs4
-- Description:
--   The following generic combinations are typically applied:
--
--   Realigned data mode (g_nof_dp_phs_clk>0):
--     g_dclk_drift       g_nof_dp_phs_clk
--           g_dclk_offon     g_dp_phs_clk_en_mask
--                                     g_dp_phs_clk_en_vary
--     0   , TRUE         4,  16#FF#,  FALSE       -- default case used to verify ADU restarts
--     0   , FALSE        4,  16#FF#,  FALSE       -- used by tb_tb_lvdsh_dd_phs4 to verify g_in_phase=0,1,2,3
--     0   , TRUE         4,  16#FF#,  TRUE        -- used to verify dp_phs_clk select via MM g_dp_phs_clk_en_mask control
--     0   , TRUE         4,       0,  FALSE       -- used to verify phase alignment disabled for raw data via MM g_dp_phs_clk_en_mask=0 control
--    -8 ps, TRUE         4,       0,  FALSE       -- used to verify phase alignment disabled for raw data via MM g_dp_phs_clk_en_mask=0 control
--    +8 ps, TRUE         4,       0,  FALSE       -- used to verify phase alignment disabled for raw data via MM g_dp_phs_clk_en_mask=0 control
--    -2 ps, FALSE        4,  16#FF#,  FALSE       -- used to verify in_clk - dp_clk phase uncertainty
--    +2 ps, FALSE        4,  16#FF#,  FALSE       -- used to verify in_clk - dp_clk phase uncertainty
--
--   Raw data mode (g_nof_dp_phs_clk=0):
--     g_dclk_drift       g_nof_dp_phs_clk
--           g_dclk_offon     g_dp_phs_clk_en_mask
--                                     g_dp_phs_clk_en_vary
--     0   , TRUE         0,  16#FF#,  FALSE       -- used to verify ADU restarts with no phase alignment implemented so raw data only
--     0   , TRUE         0,  16#FF#,  TRUE        -- used to verify that  dp_phs_clk select via MM g_dp_phs_clk_en_mask control has no effect in raw data only mode
--    +8 ps, TRUE         0,  16#FF#,  FALSE       -- used to verify in_clk - dp_clk phase uncertainty effect on FIFO fill for raw data only
--    -8 ps, TRUE         0,  16#FF#,  FALSE       -- used to verify in_clk - dp_clk phase uncertainty effect on FIFO fill for raw data only
--
--   Changes due to the tb control will cause some ASSERT ERRORs that are expected near that change.
--
-- Usage:
-- > as 10
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_lvdsh_dd_phs4 is
  generic (
     g_dclk_drift         : time := -2 ps;  -- 0 ps, use -2 ps or 2 ps to model the range of sample phase uncertainty by letting the sclk and dclk drift with respect to the dp_clk,
                                               -- use factor 2 value because of integer divide by 2 for rising and falling edge per sclk clock period.
     g_dclk_offon         : boolean := false;  -- when TRUE switch the dclk off-on periodically, to model ADU not present or ADU restart or ADU replaced, use FALSE when g_dclk_drift/=0 ps
     g_dp_phs_clk_period  : natural := 32;  -- number of dp_clk periods per dp_phs_clk period
     g_nof_dp_phs_clk     : natural := 4;  -- nof dp_phs_clk that can be used to detect the lock, use 1 or 4 to ease interpretation of results when g_dclk_drift/=0 ps
     g_dp_phs_clk_en_mask : natural := 16#FF#;  -- bit mask to individually enable or disable a dp_phs_clk in range [g_nof_dp_phs_clk-1:0]
     g_dp_phs_clk_en_vary : boolean := false;  -- use FALSE to use g_dp_phs_clk_en_mask, else use TRUE to vary g_dp_phs_clk_en_vec (assuming g_nof_dp_phs_clk>1)
     g_in_phase           : natural := 0  -- 0:3
  );
end tb_lvdsh_dd_phs4;

architecture tb of tb_lvdsh_dd_phs4 is
  constant c_dd_factor          : natural := 2;
  constant c_rx_factor          : natural := 2;
  constant c_wb_factor          : natural := c_dd_factor * c_rx_factor;

  constant c_sim                : boolean := true;
  constant c_tb_duration        : natural := sel_a_b(g_dclk_drift /= 0 ps or g_dclk_offon = true, 3000, 10);  -- nof tb intervals for tb duration
  constant c_tb_init            : natural := sel_a_b(g_dclk_drift /= 0 ps or g_dclk_offon = true,   20, 20);  -- nof tb intervals for tb init before verify_en

  constant c_clk_factor         : natural := 100;  -- slow down dclk to improve modelling dclk phase uncertainy with g_dclk_drift, which is minimal 2 ps
  constant c_ref_s_clk_period   : time := c_clk_factor * 1.25 ns;  -- 800 MHz sample clock for dp_sample_dat monitor
  constant c_adc_s_clk_period   : time := c_ref_s_clk_period + g_dclk_drift;  -- 800 MHz single data rate ADC sample clock with optional drift
  constant c_ref_dp_clk_period  : time := c_ref_s_clk_period * c_wb_factor;  -- 200 MHz data clock for Data Path processing
  constant c_dp_phs_clk_step    : time := c_ref_dp_clk_period / 32;  -- the PLL can output clocks with phase shifts of 360/32 = 11.25 degrees
  constant c_dp_phs_clk_en_mask : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0) := TO_UVEC(g_dp_phs_clk_en_mask, g_nof_dp_phs_clk);

  constant c_interval           : natural := 180;  -- some unit tb interval
  constant c_on_interval        : time := (c_tb_duration * c_interval / 3 + 3) * c_ref_s_clk_period;
  constant c_off_interval       : time := (c_tb_duration * c_interval / 3 + 7) * c_ref_s_clk_period;
  --CONSTANT c_off_interval       : TIME := 1 * c_ref_s_clk_period;   -- if multiple of c_wb_factor=4 then the off period is only noticed by the FIFO running empty

  constant c_rl                 : natural := 1;
  constant c_init               : natural := 0;
  constant c_in_dat_w           : natural := 8;
  constant c_dp_dat_w           : natural := c_wb_factor * c_in_dat_w;
  constant c_dp_phase_even2     : boolean := g_in_phase mod 2 = 0;  -- TRUE for g_in_phase=0,2 and FALSE for g_in_phase=1,3
  constant c_dp_phase_even4     : boolean := g_in_phase <   2;  -- TRUE for g_in_phase=0,1 and FALSE for g_in_phase=2,3
  constant c_dp_phase_latency   : natural := 1;  -- account for internal pipeling to be able to compare dp_sample_phase with the in_sample_phase

  signal tb_g_dclk_drift    : time := g_dclk_drift;
  signal tb_g_dclk_offon    : boolean := g_dclk_offon;
  signal tb_g_in_phase      : natural := g_in_phase;

  signal tb_end             : std_logic := '0';
  signal rst                : std_logic := '1';
  signal adc_s_clk          : std_logic := '1';
  signal adc_d_clk          : std_logic := '1';
  signal adc_d_clk_off      : std_logic := '0';
  signal adc_d_clkg         : std_logic := '1';  -- = adc_d_clk gated with adc_d_clk_off
  signal ref_s_clk          : std_logic := '1';
  signal ref_d_clk          : std_logic := '1';
  signal ref_dp_clk         : std_logic := '1';
  signal dp_clk_vec         : std_logic_vector(c_wb_factor - 1 downto 0);
  signal dp_clk             : std_logic;
  signal dp_phs_cnt         : natural := 0;
  signal dp_phs_clk         : std_logic := '1';
  signal dp_phs_clk_vec     : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
  signal dp_phs_clk_en_vec  : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0) := c_dp_phs_clk_en_mask;
  signal ready              : std_logic := '1';
  signal verify_en              : std_logic := '0';
  signal verify_phase_en        : std_logic := '0';
  signal verify_sample_phase_en : std_logic := '0';
  signal verify_data_en         : std_logic := '0';
  signal cnt_en             : std_logic := '1';

  signal in_dat             : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal in_dat_lo          : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal in_val             : std_logic;
  signal dp_dat_lo          : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal dp_dat_diff        : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal dp_dat             : std_logic_vector(c_dp_dat_w - 1 downto 0);
  signal dp_val             : std_logic := '1';
  signal dp_sample_dat      : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal prev_dp_sample_dat : std_logic_vector(c_in_dat_w - 1 downto 0);

  signal in_sample_phase    : natural;
  signal dp_sample_phase    : natural;

  signal dp_status          : std_logic_vector(c_word_w - 1 downto 0);  -- extra status information for debug
  signal dp_phs_locked      : std_logic;
  signal dp_word_locked     : std_logic;
  signal dp_word_stable     : std_logic;
  signal dp_word_stable_ack : std_logic;
begin
  -- ADU clocking with optional on/off and drift
  adc_s_clk     <= not adc_s_clk or tb_end after c_adc_s_clk_period / 2;
  adc_d_clk     <= not adc_d_clk when rising_edge(adc_s_clk);
  adc_d_clkg    <= adc_d_clk or adc_d_clk_off when c_dp_phase_even2 = true else not adc_d_clk or adc_d_clk_off;

  p_on_off : process
  begin
    if g_dclk_offon = false then
      adc_d_clk_off <= '0'; wait;
    else
      adc_d_clk_off <= '0'; if tb_end = '1' then wait; else wait for c_on_interval;  end if;
      adc_d_clk_off <= '1'; if tb_end = '1' then wait; else wait for c_off_interval; end if;
    end if;
  end process;

  -- TB ideal clocking
  ref_s_clk  <= not ref_s_clk or tb_end after c_ref_s_clk_period / 2;
  ref_d_clk  <= not ref_d_clk  when rising_edge(ref_s_clk);
  ref_dp_clk <= not ref_dp_clk when rising_edge(ref_d_clk);

  gen_dp_clk_vec : for I in 0 to c_wb_factor - 1 generate
    dp_clk_vec(I) <= transport ref_dp_clk after (real(I) + 0.5) * c_ref_s_clk_period;
  end generate;

  dp_clk <= dp_clk_vec(0) when c_dp_phase_even4 = true else dp_clk_vec(2);  -- actually c_dp_phase_even4=TRUE can not truely be modeled from the outside in this tb
  --dp_clk <= dp_clk_vec(0);

  -- Create DP phase reference clock
  p_dp_phs_clk : process(dp_clk)
  begin
    if g_dp_phs_clk_period = 1 then
      dp_phs_clk <= dp_clk;
    elsif rising_edge(dp_clk) then
      if dp_phs_cnt mod (g_dp_phs_clk_period / 2) = 0 then
        dp_phs_clk <= not dp_phs_clk;
      end if;
      if dp_phs_cnt = g_dp_phs_clk_period - 1 then
        dp_phs_cnt <= 0;
      else
        dp_phs_cnt <= dp_phs_cnt + 1;
      end if;
    end if;
  end process;

  gen_dp_phs_clk_vec : for I in g_nof_dp_phs_clk - 1 downto 0 generate
    dp_phs_clk_vec(I) <= transport dp_phs_clk after I * c_dp_phs_clk_step;
  end generate;

  gen_tb_dp_phs_clk_en : if g_dp_phs_clk_en_vary = true generate
    dp_phs_clk_en_vec <= TO_UVEC(255, g_nof_dp_phs_clk),
                         TO_UVEC(  1, g_nof_dp_phs_clk) after 10 ms,
                         TO_UVEC( 64, g_nof_dp_phs_clk) after 20 ms,
                         TO_UVEC( 16, g_nof_dp_phs_clk) after 30 ms,
                         TO_UVEC(  8, g_nof_dp_phs_clk) after 40 ms,
                         TO_UVEC(  2, g_nof_dp_phs_clk) after 50 ms,
                         TO_UVEC( 32, g_nof_dp_phs_clk) after 60 ms,
                         TO_UVEC(  4, g_nof_dp_phs_clk) after 70 ms,
                         TO_UVEC(128, g_nof_dp_phs_clk) after 80 ms,
                         TO_UVEC( 14, g_nof_dp_phs_clk) after 90 ms;
  end generate;

  rst <= '1', '0' after 16 * c_ref_s_clk_period;

  proc_common_gen_pulse(1, c_interval, '1',  rst, dp_clk, dp_word_stable_ack);

  -- Stimuli data
  p_stimuli : process
  begin
    proc_common_wait_some_cycles(dp_clk, c_interval * c_tb_init);
    verify_en <= '1';

    proc_common_wait_some_cycles(dp_clk, c_interval * c_tb_duration);
    tb_end <= '1';
    wait;
  end process;

  proc_common_gen_data(c_rl, c_init, rst, adc_s_clk, cnt_en, ready, in_dat, in_val);

  in_dat_lo   <= in_dat                                     when rising_edge(dp_clk);
  dp_dat_lo   <= dp_dat(4 * c_in_dat_w - 1 downto 3 * c_in_dat_w) when rising_edge(dp_clk);
  dp_dat_diff <= SUB_UVEC(in_dat_lo, dp_dat_lo)             when rising_edge(dp_clk);
  in_sample_phase <= TO_UINT(          in_dat_lo                     ) mod c_wb_factor when rising_edge(dp_clk);
  dp_sample_phase <= TO_UINT(INCR_UVEC(dp_dat_lo, c_dp_phase_latency)) mod c_wb_factor when rising_edge(dp_clk);

  -- Verify data
  verify_data_en         <= verify_en or dp_word_locked;
  verify_phase_en        <= verify_en when g_dclk_offon = false else '0';
  verify_sample_phase_en <= verify_en when g_dclk_drift = 0 ps and g_dp_phs_clk_en_mask /= 0 else '0';

  proc_common_verify_data(c_rl, ref_s_clk, verify_data_en, ready, dp_val, dp_sample_dat, prev_dp_sample_dat);

  p_verify_phase : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_phase_en = '1' then
        assert dp_phs_locked = '1'
          report "Unexpected dp phase detected"
          severity ERROR;
      end if;
      if verify_sample_phase_en = '1' then
        assert dp_sample_phase = in_sample_phase
          report "Unexpected dp_sample_phase"
          severity ERROR;
      end if;
    end if;
  end process;

  -- DUT (device under test)
  u_lvdsh_dd_phs4 : entity work.lvdsh_dd_phs4
  generic map (
    g_sim               => c_sim,
    g_wb_factor         => c_wb_factor,
    g_dp_phs_clk_period => g_dp_phs_clk_period,
    g_nof_dp_phs_clk    => g_nof_dp_phs_clk,
    g_in_dat_w          => c_in_dat_w
  )
  port map (
    -- PHY input interface
    in_clk              => adc_d_clkg,
    in_dat              => in_dat,

    -- DD --> Rx domain interface at adc_d_clk rate or g_dp_factor lower rate (via FIFO)
    dp_rst              => rst,
    dp_clk              => dp_clk,
    dp_phs_clk_vec      => dp_phs_clk_vec,
    dp_phs_clk_en_vec   => dp_phs_clk_en_vec,
    dp_dat              => dp_dat,
    dp_val              => dp_val,

    -- Rx status monitor
    out_status          => dp_status,
    out_phs_locked      => dp_phs_locked,
    out_word_locked     => dp_word_locked,
    out_word_stable     => dp_word_stable,
    out_word_stable_ack => dp_word_stable_ack
  );

  u_wb_data_scope : entity common_lib.common_wideband_data_scope
  generic map (
    g_sim                 => true,
    g_wideband_factor     => c_wb_factor,
    g_wideband_big_endian => true,
    g_dat_w               => c_in_dat_w
  )
  port map (
    -- Sample clock
    SCLK      => ref_s_clk,

    -- Streaming input data
    in_data   => dp_dat,
    in_val    => dp_val,

    -- Scope output samples
    out_dat   => dp_sample_dat
  );
end tb;
