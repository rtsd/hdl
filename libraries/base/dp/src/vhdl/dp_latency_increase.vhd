-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Typically used in dp_latency_adapter.
-- Description:
--   Increase the output ready latency by g_incr_latency compared to the input
--   ready latency g_in_latency. Hence the output latency becomes g_in_latency
--   + g_incr_latency.
-- Remark:
-- . The SOSI data stream signals (i.e. data, empty, channel, err) are passed
--   on as wires.
-- . The out_sync, out_val, out_sop and out_eop are internally AND with the
--   delayed src_in.ready, this is only truely necessary if the input ready
--   latency is 0, but it does not harm to do it also when the input ready
--   latency > 0. However to easy achieving P&R timing it is better to not have
--   unnessary logic in the combinatorial path of out_sync, out_val, out_sop
--   and out_eop, therefore the AND with reg_val is only generated when
--   g_in_latency=0.

entity dp_latency_increase is
  generic (
    g_in_latency   : natural := 0;  -- >= 0
    g_incr_latency : natural := 2  -- >= 0
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_latency_increase;

architecture rtl of dp_latency_increase is
  constant c_out_latency : natural := g_in_latency + g_incr_latency;

  signal reg_ready : std_logic_vector(c_out_latency downto 0);
  signal reg_val   : std_logic;

  signal i_snk_out : t_dp_siso := c_dp_siso_rdy;
begin
  -- Use i_snk_out with defaults to force unused snk_out bits and fields to '0'
  snk_out <= i_snk_out;

  -- Support wires only for g_incr_latency=0
  no_latency : if g_incr_latency = 0 generate
    i_snk_out <= src_in;  -- SISO
    src_out   <= snk_in;  -- SOSI
  end generate no_latency;

  gen_latency : if g_incr_latency > 0 generate
    -- SISO
    reg_ready(0) <= src_in.ready;  -- use reg_ready(0) to combinatorially store src_in.ready

    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        reg_ready(c_out_latency downto 1) <= (others => '0');
      elsif rising_edge(clk) then
        reg_ready(c_out_latency downto 1) <= reg_ready(c_out_latency - 1 downto 0);
      end if;
    end process;

    i_snk_out.xon   <= src_in.xon;  -- Pass on frame level flow control
    i_snk_out.ready <= reg_ready(g_incr_latency);  -- Adjust ready latency

    -- SOSI
    gen_out : if g_in_latency /= 0 generate
      src_out <= snk_in;
    end generate;

    gen_zero_out : if g_in_latency = 0 generate
      reg_val <= reg_ready(c_out_latency);

      p_src_out : process(snk_in, reg_val)
      begin
        src_out       <= snk_in;
        src_out.sync  <= snk_in.sync  and reg_val;
        src_out.valid <= snk_in.valid and reg_val;
        src_out.sop   <= snk_in.sop   and reg_val;
        src_out.eop   <= snk_in.eop   and reg_val;
      end process;
    end generate;
  end generate gen_latency;
end rtl;
