-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Combine LOFAR1 pfb2 with subband statistics (SST), similar as wpfb_unit_dev
-- Description:
-- . support multiple complex input streams via g_nof_streams
-- . one complex pfb2 per stream
-- . one complex pfb2 can process two real inputs with PFT_MODE_REAL2
-- . pass on in_sosi.bsn
--
-- Remark:

library IEEE, common_lib, dp_lib, pfs_lib, pft2_lib, st_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use pfs_lib.pfs_pkg.all;
use pft2_lib.pft_pkg.all;

entity pfb2_unit is
  generic (
    g_nof_streams     : natural := 1;  -- number of pfb2 instances, 1 pfb2 per stream
    g_nof_points      : natural := 1024;

    -- pfs
    g_pfs_bypass      : boolean := false;
    g_pfs_nof_taps    : natural := 16;
    g_pfs_in_dat_w    : natural := 12;
    g_pfs_out_dat_w   : natural := 18;
    g_pfs_coef_dat_w  : natural := c_pfs_coef_w;  -- = 16, should match coefs in g_pfs_coefs_file
    g_pfs_coefs_file  : string  := c_pfs_coefs_file;  -- = "data/pfs_coefsbuf_1024.hex"

    -- pft2
    g_pft_mode        : PFT_MODE_TYPE := PFT_MODE_REAL2;
    g_pft_switch_en   : std_logic := '1';
    g_pft_stage_dat_w : natural := c_pft_stage_dat_w;  -- c_pft_stage_dat_w = 20 in pft_pkg.vhd
    g_pft_out_dat_w   : natural := 18;

    -- sst
    g_sst_data_w      : natural := 64;  -- nof bits for the SST power values
    g_sst_data_sz     : natural := 2  -- nof MM 32b words to fit g_sst_data_w
  );
  port (
    dp_rst           : in  std_logic;
    dp_clk           : in  std_logic;
    mm_rst           : in  std_logic;
    mm_clk           : in  std_logic;
    ram_st_sst_mosi  : in  t_mem_mosi := c_mem_mosi_rst;  -- Subband statistics registers
    ram_st_sst_miso  : out t_mem_miso;
    in_sosi_arr      : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    fil_sosi_arr     : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr     : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end pfb2_unit;

architecture str of pfb2_unit is
  constant c_nof_stats       : natural := g_nof_points;  -- SST X and SST Y are interleaved for PFT_MODE_REAL2

  signal ram_st_sst_mosi_arr : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal ram_st_sst_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0) := (others => c_mem_miso_rst);

  signal pft_sosi_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  ---------------------------------------------------------------
  -- Polyphase Filterbanks
  ---------------------------------------------------------------
  gen_pfb2: for I in 0 to g_nof_streams - 1 generate
    u_pfb2 : entity work.pfb2
    generic map (
      g_nof_points      => g_nof_points,

      -- pfs
      g_pfs_bypass      => g_pfs_bypass,
      g_pfs_nof_taps    => g_pfs_nof_taps,
      g_pfs_in_dat_w    => g_pfs_in_dat_w,
      g_pfs_out_dat_w   => g_pfs_out_dat_w,
      g_pfs_coef_dat_w  => g_pfs_coef_dat_w,
      g_pfs_coefs_file  => g_pfs_coefs_file,

      -- pft2
      g_pft_mode        => g_pft_mode,
      g_pft_switch_en   => g_pft_switch_en,
      g_pft_stage_dat_w => g_pft_stage_dat_w,
      g_pft_out_dat_w   => g_pft_out_dat_w
    )
    port map (
      dp_rst        => dp_rst,
      dp_clk        => dp_clk,
      in_sosi       => in_sosi_arr(I),
      fil_sosi      => fil_sosi_arr(I),
      out_sosi      => pft_sosi_arr(I)
    );
  end generate;

  ---------------------------------------------------------------
  -- Subband Statistics (SST)
  ---------------------------------------------------------------
  -- MM mux for SST
  u_mem_mux_sst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(g_sst_data_sz * c_nof_stats)
  )
  port map (
    mosi     => ram_st_sst_mosi,
    miso     => ram_st_sst_miso,
    mosi_arr => ram_st_sst_mosi_arr,
    miso_arr => ram_st_sst_miso_arr
  );

  gen_sst: for I in 0 to g_nof_streams - 1 generate
    u_sst : entity st_lib.st_sst
    generic map (
      g_nof_stat      => c_nof_stats,
      g_in_data_w     => g_pft_out_dat_w,
      g_stat_data_w   => g_sst_data_w,
      g_stat_data_sz  => g_sst_data_sz
    )
    port map (
      mm_rst          => mm_rst,
      mm_clk          => mm_clk,
      dp_rst          => dp_rst,
      dp_clk          => dp_clk,
      in_complex      => pft_sosi_arr(I),
      ram_st_sst_mosi => ram_st_sst_mosi_arr(I),
      ram_st_sst_miso => ram_st_sst_miso_arr(I)
    );
  end generate;

  out_sosi_arr <= pft_sosi_arr;
end str;
