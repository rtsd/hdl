-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity mms_dp_ram_from_mm is
  generic (
    g_ram_wr_nof_words : natural;
    g_ram_rd_dat_w     : natural;
    g_init_file        : string := "UNUSED";
    g_dp_on_at_init    : std_logic := '0'  -- Enable data path at default
  );
  port (
    mm_rst    : in  std_logic;
    mm_clk    : in  std_logic;

    st_rst    : in  std_logic;
    st_clk    : in  std_logic;

    reg_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    ram_mosi  : in  t_mem_mosi := c_mem_mosi_rst;

    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end mms_dp_ram_from_mm;

architecture str of mms_dp_ram_from_mm is
 signal dp_on  : std_logic;
begin
 u_dp_ram_from_mm : entity dp_lib.dp_ram_from_mm
  generic map(
    g_ram_wr_nof_words => g_ram_wr_nof_words,
    g_ram_rd_dat_w     => g_ram_rd_dat_w,
    g_init_file        => g_init_file
  )
  port map (
    mm_rst    => mm_rst,
    mm_clk    => mm_clk,

    st_rst    => st_rst,
    st_clk    => st_clk,

    mm_addr   => ram_mosi.address,
    mm_wr     => ram_mosi.wr,
    mm_wrdata => ram_mosi.wrdata,

    dp_on     => dp_on,

    src_in    => src_in,
    src_out   => src_out
  );

  u_dp_ram_from_mm_reg: entity work.dp_ram_from_mm_reg
  generic map(
    g_dp_on_at_init    => g_dp_on_at_init
  )
  port map (
    mm_rst  => mm_rst,
    mm_clk  => mm_clk,

    st_rst  => st_rst,
    st_clk  => st_clk,

    sla_in  => reg_mosi,

    dp_on   => dp_on
  );
end str;
