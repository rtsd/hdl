-------------------------------------------------------------------------------
--
-- Copyright (C) 2014-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, eth_lib, tr_10GbE_lib, diag_lib, io_ddr_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use eth_lib.eth_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.unb1_test_pkg.all;

entity unb1_test is
  generic (
    g_sim         : boolean := false;  -- Overridden by TB
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0;
    g_design_name : string  := "unb1_test";  -- set by QSF
    g_design_note : string  := "Test Design";
    g_technology  : natural := c_tech_stratixiv;
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn   : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic;
    ETH_SGOUT    : out   std_logic;

    -- Transceiver clocks
    SA_CLK       : in  std_logic := '0';  -- SerDes Clock BN-BI / SI_FN

    -- Serial I/O
    SI_FN_0_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_0_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    SI_FN_1_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_1_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    SI_FN_2_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_2_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    SI_FN_3_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_3_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');

    SI_FN_0_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);  -- (0 = LASI; 1=MDC; 2=MDIO)
    SI_FN_1_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_2_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_3_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_RSTN    : out   std_logic := '1';  -- ResetN is pulled up in the Vitesse chip, but pulled down again by external 1k resistor.

    BN_BI_0_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_0_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    BN_BI_1_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_1_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    BN_BI_2_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_2_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');
    BN_BI_3_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_3_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');

    -- SO-DIMM Memory Bank I
    MB_I_IN       : in    t_tech_ddr3_phy_in := c_tech_ddr3_phy_in_x;
    MB_I_IO       : inout t_tech_ddr3_phy_io;
    MB_I_OU       : out   t_tech_ddr3_phy_ou;

    -- SO-DIMM Memory Bank II
    MB_II_IN      : in    t_tech_ddr3_phy_in := c_tech_ddr3_phy_in_x;
    MB_II_IO      : inout t_tech_ddr3_phy_io;
    MB_II_OU      : out   t_tech_ddr3_phy_ou
  );
end unb1_test;

architecture str of unb1_test is
  -- Firmware version x.y
  constant c_fw_version                       : t_unb1_board_fw_version := (1, 2);

 -- Select the according revision record based on the design name.

  constant c_revision_select                  : t_unb1_test_config := func_unb1_test_sel_revision_rec(g_design_name);

  -- ddr
  constant c_nof_MB                           : natural := c_unb1_board_nof_ddr3;  -- Fixed control infrastructure for 2 modules per FPGA

  constant c_use_phy                          : t_c_unb1_board_use_phy  := (sel_a_b(c_revision_select.use_streaming_1GbE, 1, 0),
                                                                                    c_revision_select.use_front, 0,
                                                                                    c_revision_select.use_back,
                                                                                    c_revision_select.use_ddr_MB_I,
                                                                                    c_revision_select.use_ddr_MB_II,
                                                                                    0,
                                                                                    1);

  constant c_nof_streams_10GbE                : natural := c_revision_select.use_nof_streams_10GbE;
  constant c_nof_streams_1GbE                 : natural := c_revision_select.use_nof_streams_1GbE;
  constant c_nof_streams_ddr                  : natural := c_revision_select.use_nof_streams_ddr;
  constant c_nof_streams                      : natural := c_nof_streams_1GbE + c_nof_streams_10GbE + c_nof_streams_ddr;
  constant c_data_w_32                        : natural := c_eth_data_w;  -- 1GbE
  constant c_data_w_64                        : natural := c_xgmii_data_w;  -- 10GbE

  -- Block generator constants
  constant c_bg_block_size                    : natural := 900;
  constant c_bg_gapsize_1GbE                  : natural := 1000;
  constant c_bg_gapsize_10GbE                 : natural := 100;
  constant c_bg_blocks_per_sync               : natural := sel_a_b(g_sim, 10, 200000);  -- 200000*(900+100) = 200000000 cycles = 1 second

  constant c_use_jumbo_frames                 : boolean := false;
  constant c_def_1GbE_block_size              : natural := 20;  -- 0 first so we have time to set RX demux reg in dest. node
  constant c_def_10GbE_block_size             : natural := 700;  -- (700/1000) * 200MHz * 64b = 8.96Gbps user rate (excl. header overhead (16 words/packet) )

  constant c_max_frame_len                    : natural := sel_a_b(c_use_jumbo_frames, 9018, 1518);
  constant c_nof_header_bytes                 : natural := field_slv_len(c_hdr_field_arr) / c_byte_w;
  constant c_max_udp_payload_len              : natural := c_max_frame_len - c_nof_header_bytes - c_network_eth_crc_len;

  constant c_max_udp_payload_nof_words_1GbE   : natural := (c_max_udp_payload_len * c_byte_w) / c_data_w_32;
  constant c_max_udp_payload_nof_words_10GbE  : natural := (c_max_udp_payload_len * c_byte_w) / c_data_w_64;
  constant c_min_nof_words_per_block          : natural := 1;
  constant c_max_nof_blocks_per_packet_1GbE   : natural := c_max_udp_payload_nof_words_1GbE / c_min_nof_words_per_block;
  constant c_max_nof_blocks_per_packet_10GbE  : natural := c_max_udp_payload_nof_words_10GbE / c_min_nof_words_per_block;

  -- DDR3 constants
  constant c_wr_fifo_depth                    : natural  := 1024;  -- >=16                             , defined at DDR side of the FIFO.
  constant c_rd_fifo_depth                    : natural  := 1024;  -- >=16 AND >g_tech_ddr.maxburstsize, defined at DDR side of the FIFO.

  constant c_use_db                           : boolean  := false;
  constant c_buf_nof_data                     : natural  := 1024;
  constant c_seq_dat_w                        : natural  := 16;

  -- System
  signal cs_sim                               : std_logic;
  signal xo_clk                               : std_logic;
  signal xo_rst                               : std_logic;
  signal xo_rst_n                             : std_logic;
  signal mm_clk                               : std_logic;
  signal mm_locked                            : std_logic;
  signal mm_rst                               : std_logic;

  signal ddr_ref_rst                          : std_logic;

  signal cal_rec_clk                          : std_logic;
  signal epcs_clk                             : std_logic;
  signal sa_rst                               : std_logic;

  signal dp_clk                               : std_logic;
  signal dp_rst                               : std_logic;

  -- PIOs
  signal pout_wdi                             : std_logic;

  -- WDI override
  signal reg_wdi_mosi                         : t_mem_mosi;
  signal reg_wdi_miso                         : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi                        : t_mem_mosi;
  signal reg_ppsh_miso                        : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi             : t_mem_mosi;
  signal reg_unb_system_info_miso             : t_mem_miso;
  signal rom_unb_system_info_mosi             : t_mem_mosi;
  signal rom_unb_system_info_miso             : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi                    : t_mem_mosi;
  signal reg_unb_sens_miso                    : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk                        : std_logic;
  signal eth1g_mm_rst                         : std_logic;
  signal eth1g_tse_mosi                       : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso                       : t_mem_miso;
  signal eth1g_reg_mosi                       : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso                       : t_mem_miso;
  signal eth1g_reg_interrupt                  : std_logic;
  signal eth1g_ram_mosi                       : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso                       : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi                   : t_mem_mosi;
  signal reg_dpmm_data_miso                   : t_mem_miso;
  signal reg_dpmm_ctrl_mosi                   : t_mem_mosi;
  signal reg_dpmm_ctrl_miso                   : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi                   : t_mem_mosi;
  signal reg_mmdp_data_miso                   : t_mem_miso;
  signal reg_mmdp_ctrl_mosi                   : t_mem_mosi;
  signal reg_mmdp_ctrl_miso                   : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi                        : t_mem_mosi;
  signal reg_epcs_miso                        : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi                        : t_mem_mosi;
  signal reg_remu_miso                        : t_mem_miso;

  -- 10GbE
  signal xaui_tx_arr                          : t_unb1_board_xaui_sl_2arr(c_nof_streams_10GbE-1 downto 0);
  signal xaui_rx_arr                          : t_unb1_board_xaui_sl_2arr(c_nof_streams_10GbE-1 downto 0);
  signal i_xaui_tx_arr                        : t_xaui_arr(c_nof_streams_10GbE-1 downto 0);
  signal i_xaui_rx_arr                        : t_xaui_arr(c_nof_streams_10GbE-1 downto 0);

  signal mdio_mdc_arr                         : std_logic_vector(c_nof_streams_10GbE-1 downto 0);
  signal mdio_mdat_in_arr                     : std_logic_vector(c_nof_streams_10GbE-1 downto 0);
  signal mdio_mdat_oen_arr                    : std_logic_vector(c_nof_streams_10GbE-1 downto 0);

  signal reg_tr_10GbE_mosi                    : t_mem_mosi;
  signal reg_tr_10GbE_miso                    : t_mem_miso;

  signal reg_tr_xaui_mosi                     : t_mem_mosi;
  signal reg_tr_xaui_miso                     : t_mem_miso;

  signal reg_diag_bg_1GbE_mosi                : t_mem_mosi;
  signal reg_diag_bg_1GbE_miso                : t_mem_miso;
  signal ram_diag_bg_1GbE_mosi                : t_mem_mosi;
  signal ram_diag_bg_1GbE_miso                : t_mem_miso;
  signal reg_diag_tx_seq_1GbE_mosi            : t_mem_mosi;
  signal reg_diag_tx_seq_1GbE_miso            : t_mem_miso;

  signal reg_diag_bg_10GbE_mosi               : t_mem_mosi;
  signal reg_diag_bg_10GbE_miso               : t_mem_miso;
  signal ram_diag_bg_10GbE_mosi               : t_mem_mosi;
  signal ram_diag_bg_10GbE_miso               : t_mem_miso;
  signal reg_diag_tx_seq_10GbE_mosi           : t_mem_mosi;
  signal reg_diag_tx_seq_10GbE_miso           : t_mem_miso;

  signal reg_diag_tx_seq_ddr_MB_I_mosi      : t_mem_mosi;
  signal reg_diag_tx_seq_ddr_MB_I_miso      : t_mem_miso;

  signal reg_diag_tx_seq_ddr_MB_II_mosi       : t_mem_mosi;
  signal reg_diag_tx_seq_ddr_MB_II_miso       : t_mem_miso;

  signal reg_dp_offload_tx_1GbE_mosi          : t_mem_mosi;
  signal reg_dp_offload_tx_1GbE_miso          : t_mem_miso;
  signal reg_dp_offload_tx_1GbE_hdr_dat_mosi  : t_mem_mosi;
  signal reg_dp_offload_tx_1GbE_hdr_dat_miso  : t_mem_miso;

  signal reg_dp_offload_tx_10GbE_mosi         : t_mem_mosi;
  signal reg_dp_offload_tx_10GbE_miso         : t_mem_miso;
  signal reg_dp_offload_tx_10GbE_hdr_dat_mosi : t_mem_mosi;
  signal reg_dp_offload_tx_10GbE_hdr_dat_miso : t_mem_miso;

  signal reg_dp_offload_rx_1GbE_hdr_dat_mosi  : t_mem_mosi;
  signal reg_dp_offload_rx_1GbE_hdr_dat_miso  : t_mem_miso;
  signal reg_dp_offload_rx_10GbE_hdr_dat_mosi : t_mem_mosi;
  signal reg_dp_offload_rx_10GbE_hdr_dat_miso : t_mem_miso;

  signal reg_bsn_monitor_1GbE_mosi            : t_mem_mosi;
  signal reg_bsn_monitor_1GbE_miso            : t_mem_miso;
  signal reg_bsn_monitor_10GbE_mosi           : t_mem_mosi;
  signal reg_bsn_monitor_10GbE_miso           : t_mem_miso;

  signal ram_diag_data_buf_1GbE_mosi          : t_mem_mosi;
  signal ram_diag_data_buf_1GbE_miso          : t_mem_miso;
  signal reg_diag_data_buf_1GbE_mosi          : t_mem_mosi;
  signal reg_diag_data_buf_1GbE_miso          : t_mem_miso;
  signal reg_diag_rx_seq_1GbE_mosi            : t_mem_mosi;
  signal reg_diag_rx_seq_1GbE_miso            : t_mem_miso;

  signal ram_diag_data_buf_10GbE_mosi         : t_mem_mosi;
  signal ram_diag_data_buf_10GbE_miso         : t_mem_miso;
  signal reg_diag_data_buf_10GbE_mosi         : t_mem_mosi;
  signal reg_diag_data_buf_10GbE_miso         : t_mem_miso;
  signal reg_diag_rx_seq_10GbE_mosi           : t_mem_mosi;
  signal reg_diag_rx_seq_10GbE_miso           : t_mem_miso;

  signal ram_diag_data_buf_ddr_MB_I_mosi      : t_mem_mosi;
  signal ram_diag_data_buf_ddr_MB_I_miso      : t_mem_miso;
  signal reg_diag_data_buf_ddr_MB_I_mosi      : t_mem_mosi;
  signal reg_diag_data_buf_ddr_MB_I_miso      : t_mem_miso;
  signal reg_diag_rx_seq_ddr_MB_I_mosi        : t_mem_mosi;
  signal reg_diag_rx_seq_ddr_MB_I_miso        : t_mem_miso;

  signal ram_diag_data_buf_ddr_MB_II_mosi     : t_mem_mosi;
  signal ram_diag_data_buf_ddr_MB_II_miso     : t_mem_miso;
  signal reg_diag_data_buf_ddr_MB_II_mosi     : t_mem_mosi;
  signal reg_diag_data_buf_ddr_MB_II_miso     : t_mem_miso;
  signal reg_diag_rx_seq_ddr_MB_II_mosi       : t_mem_mosi;
  signal reg_diag_rx_seq_ddr_MB_II_miso       : t_mem_miso;

  signal dp_offload_tx_1GbE_src_out_arr       : t_dp_sosi_arr(c_nof_streams_1GbE-1 downto 0);
  signal dp_offload_tx_1GbE_src_in_arr        : t_dp_siso_arr(c_nof_streams_1GbE-1 downto 0);
  signal dp_offload_tx_10GbE_src_out_arr      : t_dp_sosi_arr(c_nof_streams_10GbE-1 downto 0);
  signal dp_offload_tx_10GbE_src_in_arr       : t_dp_siso_arr(c_nof_streams_10GbE-1 downto 0);

  signal dp_offload_rx_1GbE_snk_in_arr        : t_dp_sosi_arr(c_nof_streams_1GbE-1 downto 0);
  signal dp_offload_rx_1GbE_snk_out_arr       : t_dp_siso_arr(c_nof_streams_1GbE-1 downto 0);
  signal dp_offload_rx_10GbE_snk_in_arr       : t_dp_sosi_arr(c_nof_streams_10GbE-1 downto 0);
  signal dp_offload_rx_10GbE_snk_out_arr      : t_dp_siso_arr(c_nof_streams_10GbE-1 downto 0);

  signal reg_io_ddr_MB_I_mosi                 : t_mem_mosi;
  signal reg_io_ddr_MB_I_miso                 : t_mem_miso;
  signal reg_io_ddr_MB_II_mosi                : t_mem_mosi;
  signal reg_io_ddr_MB_II_miso                : t_mem_miso;

    -- DDR3 pass on termination control from master to slave controller
  signal term_ctrl_out                        : t_tech_ddr3_phy_terminationcontrol;
  signal term_ctrl_in                         : t_tech_ddr3_phy_terminationcontrol;

  signal MB_I_ctlr_clk                        : std_logic;
  signal MB_I_ctlr_rst                        : std_logic;
  signal MB_II_ctlr_clk                       : std_logic;
  signal MB_II_ctlr_rst                       : std_logic;
begin
  u_areset_ddr_ref_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1',
    g_delay_len => 40
  )
  port map(
    clk     => CLK,
    in_rst  => mm_rst,
    out_rst => ddr_ref_rst
  );
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim                     => g_sim,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_stamp_svn               => g_stamp_svn,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_unb1_board_mm_clk_freq_125M,
    g_use_phy                 => c_use_phy,
    g_aux                     => c_unb1_board_aux,
    g_udp_offload             => c_revision_select.use_streaming_1GbE,
    g_udp_offload_nof_streams => c_nof_streams_1GbE,
    g_dp_clk_use_pll          => true,
    g_xo_clk_use_pll          => true
  )
  port map (
    -- Clock and reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk_out               => mm_clk,
    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    mm_locked                => mm_locked,
    mm_locked_out            => mm_locked,

    epcs_clk                 => epcs_clk,
    epcs_clk_out             => epcs_clk,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    cal_rec_clk              => cal_rec_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk_out        => eth1g_tse_clk,
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- eth1g UDP streaming ports
    udp_tx_sosi_arr          =>  dp_offload_tx_1GbE_src_out_arr,
    udp_tx_siso_arr          =>  dp_offload_tx_1GbE_src_in_arr,
    udp_rx_sosi_arr          =>  dp_offload_rx_1GbE_snk_in_arr,
    udp_rx_siso_arr          =>  dp_offload_rx_1GbE_snk_out_arr,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- . 1GbE Control Interface
    ETH_CLK                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );
--  END GENERATE;

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb1_test
  generic map (
    g_sim               => g_sim,
    g_sim_unb_nr        => g_sim_unb_nr,
    g_sim_node_nr       => g_sim_node_nr,
    g_nof_streams_1GbE  => c_nof_streams_1GbE,
    g_nof_streams_10GbE => 3,  -- c_nof_streams_10GbE,
    g_nof_streams_ddr   => 1,  -- c_nof_streams_ddr,
    g_bg_block_size     => c_bg_block_size
   )
  port map(
    mm_rst                               => mm_rst,
    mm_clk                               => mm_clk,

    -- PIOs
    pout_wdi                             => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi                         => reg_wdi_mosi,
    reg_wdi_miso                         => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi             => reg_unb_system_info_mosi,
    reg_unb_system_info_miso             => reg_unb_system_info_miso,
    rom_unb_system_info_mosi             => rom_unb_system_info_mosi,
    rom_unb_system_info_miso             => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi                    => reg_unb_sens_mosi,
    reg_unb_sens_miso                    => reg_unb_sens_miso,

    -- PPSH
    reg_ppsh_mosi                        => reg_ppsh_mosi,
    reg_ppsh_miso                        => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst                         => eth1g_mm_rst,
    eth1g_tse_mosi                       => eth1g_tse_mosi,
    eth1g_tse_miso                       => eth1g_tse_miso,
    eth1g_reg_mosi                       => eth1g_reg_mosi,
    eth1g_reg_miso                       => eth1g_reg_miso,
    eth1g_reg_interrupt                  => eth1g_reg_interrupt,
    eth1g_ram_mosi                       => eth1g_ram_mosi,
    eth1g_ram_miso                       => eth1g_ram_miso,

    -- EPCS read
    reg_dpmm_data_mosi                   => reg_dpmm_data_mosi,
    reg_dpmm_data_miso                   => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi                   => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso                   => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi                   => reg_mmdp_data_mosi,
    reg_mmdp_data_miso                   => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi                   => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso                   => reg_mmdp_ctrl_miso,

    reg_epcs_mosi                        => reg_epcs_mosi,
    reg_epcs_miso                        => reg_epcs_miso,

    reg_remu_mosi                        => reg_remu_mosi,
    reg_remu_miso                        => reg_remu_miso,

    -- block gen
    ram_diag_bg_1GbE_mosi                => ram_diag_bg_1GbE_mosi,
    ram_diag_bg_1GbE_miso                => ram_diag_bg_1GbE_miso,
    reg_diag_bg_1GbE_mosi                => reg_diag_bg_1GbE_mosi,
    reg_diag_bg_1GbE_miso                => reg_diag_bg_1GbE_miso,
    reg_diag_tx_seq_1GbE_mosi            => reg_diag_tx_seq_1GbE_mosi,
    reg_diag_tx_seq_1GbE_miso            => reg_diag_tx_seq_1GbE_miso,

    ram_diag_bg_10GbE_mosi               => ram_diag_bg_10GbE_mosi,
    ram_diag_bg_10GbE_miso               => ram_diag_bg_10GbE_miso,
    reg_diag_bg_10GbE_mosi               => reg_diag_bg_10GbE_mosi,
    reg_diag_bg_10GbE_miso               => reg_diag_bg_10GbE_miso,
    reg_diag_tx_seq_10GbE_mosi           => reg_diag_tx_seq_10GbE_mosi,
    reg_diag_tx_seq_10GbE_miso           => reg_diag_tx_seq_10GbE_miso,

    -- dp_offload_tx
    reg_dp_offload_tx_1GbE_mosi          => reg_dp_offload_tx_1GbE_mosi,
    reg_dp_offload_tx_1GbE_miso          => reg_dp_offload_tx_1GbE_miso,
    reg_dp_offload_tx_1GbE_hdr_dat_mosi  => reg_dp_offload_tx_1GbE_hdr_dat_mosi,
    reg_dp_offload_tx_1GbE_hdr_dat_miso  => reg_dp_offload_tx_1GbE_hdr_dat_miso,

    reg_dp_offload_tx_10GbE_mosi         => reg_dp_offload_tx_10GbE_mosi,
    reg_dp_offload_tx_10GbE_miso         => reg_dp_offload_tx_10GbE_miso,
    reg_dp_offload_tx_10GbE_hdr_dat_mosi => reg_dp_offload_tx_10GbE_hdr_dat_mosi,
    reg_dp_offload_tx_10GbE_hdr_dat_miso => reg_dp_offload_tx_10GbE_hdr_dat_miso,

    -- dp_offload_rx
    reg_dp_offload_rx_1GbE_hdr_dat_mosi  => reg_dp_offload_rx_1GbE_hdr_dat_mosi,
    reg_dp_offload_rx_1GbE_hdr_dat_miso  => reg_dp_offload_rx_1GbE_hdr_dat_miso,

    reg_dp_offload_rx_10GbE_hdr_dat_mosi => reg_dp_offload_rx_10GbE_hdr_dat_mosi,
    reg_dp_offload_rx_10GbE_hdr_dat_miso => reg_dp_offload_rx_10GbE_hdr_dat_miso,

    -- bsn
    reg_bsn_monitor_1GbE_mosi            => reg_bsn_monitor_1GbE_mosi,
    reg_bsn_monitor_1GbE_miso            => reg_bsn_monitor_1GbE_miso,
    reg_bsn_monitor_10GbE_mosi           => reg_bsn_monitor_10GbE_mosi,
    reg_bsn_monitor_10GbE_miso           => reg_bsn_monitor_10GbE_miso,

    -- databuffer
    ram_diag_data_buf_1GbE_mosi          => ram_diag_data_buf_1GbE_mosi,
    ram_diag_data_buf_1GbE_miso          => ram_diag_data_buf_1GbE_miso,
    reg_diag_data_buf_1GbE_mosi          => reg_diag_data_buf_1GbE_mosi,
    reg_diag_data_buf_1GbE_miso          => reg_diag_data_buf_1GbE_miso,
    reg_diag_rx_seq_1GbE_mosi            => reg_diag_rx_seq_1GbE_mosi,
    reg_diag_rx_seq_1GbE_miso            => reg_diag_rx_seq_1GbE_miso,

    ram_diag_data_buf_10GbE_mosi         => ram_diag_data_buf_10GbE_mosi,
    ram_diag_data_buf_10GbE_miso         => ram_diag_data_buf_10GbE_miso,
    reg_diag_data_buf_10GbE_mosi         => reg_diag_data_buf_10GbE_mosi,
    reg_diag_data_buf_10GbE_miso         => reg_diag_data_buf_10GbE_miso,
    reg_diag_rx_seq_10GbE_mosi           => reg_diag_rx_seq_10GbE_mosi,
    reg_diag_rx_seq_10GbE_miso           => reg_diag_rx_seq_10GbE_miso,

    -- tr_10GbE
    reg_tr_10GbE_mosi                    => reg_tr_10GbE_mosi,
    reg_tr_10GbE_miso                    => reg_tr_10GbE_miso,
    reg_tr_xaui_mosi                     => reg_tr_xaui_mosi,
    reg_tr_xaui_miso                     => reg_tr_xaui_miso,

    -- DDR3 : MB I
    reg_io_ddr_MB_I_mosi                 => reg_io_ddr_MB_I_mosi,
    reg_io_ddr_MB_I_miso                 => reg_io_ddr_MB_I_miso,

    reg_diag_tx_seq_ddr_MB_I_mosi        => reg_diag_tx_seq_ddr_MB_I_mosi,
    reg_diag_tx_seq_ddr_MB_I_miso        => reg_diag_tx_seq_ddr_MB_I_miso,

    reg_diag_rx_seq_ddr_MB_I_mosi        => reg_diag_rx_seq_ddr_MB_I_mosi,
    reg_diag_rx_seq_ddr_MB_I_miso        => reg_diag_rx_seq_ddr_MB_I_miso,

    reg_diag_data_buf_ddr_MB_I_mosi      => reg_diag_data_buf_ddr_MB_I_mosi,
    reg_diag_data_buf_ddr_MB_I_miso      => reg_diag_data_buf_ddr_MB_I_miso,
    ram_diag_data_buf_ddr_MB_I_mosi      => ram_diag_data_buf_ddr_MB_I_mosi,
    ram_diag_data_buf_ddr_MB_I_miso      => ram_diag_data_buf_ddr_MB_I_miso,

    -- DDR3 : MB II
    reg_io_ddr_MB_II_mosi                => reg_io_ddr_MB_II_mosi,
    reg_io_ddr_MB_II_miso                => reg_io_ddr_MB_II_miso,

    reg_diag_tx_seq_ddr_MB_II_mosi       => reg_diag_tx_seq_ddr_MB_II_mosi,
    reg_diag_tx_seq_ddr_MB_II_miso       => reg_diag_tx_seq_ddr_MB_II_miso,

    reg_diag_rx_seq_ddr_MB_II_mosi       => reg_diag_rx_seq_ddr_MB_II_mosi,
    reg_diag_rx_seq_ddr_MB_II_miso       => reg_diag_rx_seq_ddr_MB_II_miso,

    reg_diag_data_buf_ddr_MB_II_mosi     => reg_diag_data_buf_ddr_MB_II_mosi,
    reg_diag_data_buf_ddr_MB_II_miso     => reg_diag_data_buf_ddr_MB_II_miso,
    ram_diag_data_buf_ddr_MB_II_mosi     => ram_diag_data_buf_ddr_MB_II_mosi,
    ram_diag_data_buf_ddr_MB_II_miso     => ram_diag_data_buf_ddr_MB_II_miso
  );

  gen_udp_stream_1GbE : if c_revision_select.use_streaming_1GbE = true generate
    u_udp_stream_1GbE : entity work.udp_stream
    generic map (
      g_sim                       => g_sim,
      g_nof_streams               => c_nof_streams_1GbE,
      g_data_w                    => c_data_w_32,
      g_bg_block_size             => c_def_1GbE_block_size,
      g_bg_gapsize                => c_bg_gapsize_1GbE,
      g_bg_blocks_per_sync        => c_bg_blocks_per_sync,
      g_def_block_size            => c_def_1GbE_block_size,
      g_max_nof_blocks_per_packet => c_max_nof_blocks_per_packet_1GbE,
      g_remove_crc                => true
    )
    port map (
      mm_rst                         => mm_rst,
      mm_clk                         => mm_clk,

      dp_rst                         => dp_rst,
      dp_clk                         => dp_clk,

      ID                             => ID,

      -- blockgen MM
      reg_diag_bg_mosi               => reg_diag_bg_1GbE_mosi,
      reg_diag_bg_miso               => reg_diag_bg_1GbE_miso,
      ram_diag_bg_mosi               => ram_diag_bg_1GbE_mosi,
      ram_diag_bg_miso               => ram_diag_bg_1GbE_miso,
      reg_diag_tx_seq_mosi           => reg_diag_tx_seq_1GbE_mosi,
      reg_diag_tx_seq_miso           => reg_diag_tx_seq_1GbE_miso,

      -- dp_offload_tx
      reg_dp_offload_tx_mosi         => reg_dp_offload_tx_1GbE_mosi,
      reg_dp_offload_tx_miso         => reg_dp_offload_tx_1GbE_miso,
      reg_dp_offload_tx_hdr_dat_mosi => reg_dp_offload_tx_1GbE_hdr_dat_mosi,
      reg_dp_offload_tx_hdr_dat_miso => reg_dp_offload_tx_1GbE_hdr_dat_miso,
      dp_offload_tx_src_out_arr      => dp_offload_tx_1GbE_src_out_arr,
      dp_offload_tx_src_in_arr       => dp_offload_tx_1GbE_src_in_arr,

      -- dp_offload_rx
      reg_dp_offload_rx_hdr_dat_mosi => reg_dp_offload_rx_1GbE_hdr_dat_mosi,
      reg_dp_offload_rx_hdr_dat_miso => reg_dp_offload_rx_1GbE_hdr_dat_miso,
      dp_offload_rx_snk_in_arr       => dp_offload_rx_1GbE_snk_in_arr,
      dp_offload_rx_snk_out_arr      => dp_offload_rx_1GbE_snk_out_arr,

      -- bsn
      reg_bsn_monitor_mosi           => reg_bsn_monitor_1GbE_mosi,
      reg_bsn_monitor_miso           => reg_bsn_monitor_1GbE_miso,

      -- databuffer
      reg_diag_data_buf_mosi         => reg_diag_data_buf_1GbE_mosi,
      reg_diag_data_buf_miso         => reg_diag_data_buf_1GbE_miso,
      ram_diag_data_buf_mosi         => ram_diag_data_buf_1GbE_mosi,
      ram_diag_data_buf_miso         => ram_diag_data_buf_1GbE_miso,
      reg_diag_rx_seq_mosi           => reg_diag_rx_seq_1GbE_mosi,
      reg_diag_rx_seq_miso           => reg_diag_rx_seq_1GbE_miso
    );
  end generate;

  gen_udp_stream_10GbE : if c_revision_select.use_10GbE = true generate
    u_udp_stream_10GbE : entity work.udp_stream
    generic map (
      g_sim                       => g_sim,
      g_nof_streams               => c_nof_streams_10GbE,
      g_data_w                    => c_data_w_64,
      g_bg_block_size             => c_def_10GbE_block_size,
      g_bg_gapsize                => c_bg_gapsize_10GbE,
      g_bg_blocks_per_sync        => c_bg_blocks_per_sync,
      g_def_block_size            => c_def_10GbE_block_size,
      g_max_nof_blocks_per_packet => c_max_nof_blocks_per_packet_1GbE,
      g_remove_crc                => false
    )
    port map (
      mm_rst                         => mm_rst,
      mm_clk                         => mm_clk,

      dp_rst                         => dp_rst,
      dp_clk                         => dp_clk,

      ID                             => ID,

      -- blockgen mm
      reg_diag_bg_mosi               => reg_diag_bg_10GbE_mosi,
      reg_diag_bg_miso               => reg_diag_bg_10GbE_miso,
      ram_diag_bg_mosi               => ram_diag_bg_10GbE_mosi,
      ram_diag_bg_miso               => ram_diag_bg_10GbE_miso,
      reg_diag_tx_seq_mosi           => reg_diag_tx_seq_10GbE_mosi,
      reg_diag_tx_seq_miso           => reg_diag_tx_seq_10GbE_miso,

      -- dp_offload_tx
      reg_dp_offload_tx_mosi         => reg_dp_offload_tx_10GbE_mosi,
      reg_dp_offload_tx_miso         => reg_dp_offload_tx_10GbE_miso,
      reg_dp_offload_tx_hdr_dat_mosi => reg_dp_offload_tx_10GbE_hdr_dat_mosi,
      reg_dp_offload_tx_hdr_dat_miso => reg_dp_offload_tx_10GbE_hdr_dat_miso,
      dp_offload_tx_src_out_arr      => dp_offload_tx_10GbE_src_out_arr,
      dp_offload_tx_src_in_arr       => dp_offload_tx_10GbE_src_in_arr,

      -- dp_offload_rx
      reg_dp_offload_rx_hdr_dat_mosi => reg_dp_offload_rx_10GbE_hdr_dat_mosi,
      reg_dp_offload_rx_hdr_dat_miso => reg_dp_offload_rx_10GbE_hdr_dat_miso,
      dp_offload_rx_snk_in_arr       => dp_offload_rx_10GbE_snk_in_arr,
      dp_offload_rx_snk_out_arr      => dp_offload_rx_10GbE_snk_out_arr,

      -- bsn
      reg_bsn_monitor_mosi           => reg_bsn_monitor_10GbE_mosi,
      reg_bsn_monitor_miso           => reg_bsn_monitor_10GbE_miso,

      -- databuffer
      reg_diag_data_buf_mosi         => reg_diag_data_buf_10GbE_mosi,
      reg_diag_data_buf_miso         => reg_diag_data_buf_10GbE_miso,
      ram_diag_data_buf_mosi         => ram_diag_data_buf_10GbE_mosi,
      ram_diag_data_buf_miso         => ram_diag_data_buf_10GbE_miso,
      reg_diag_rx_seq_mosi           => reg_diag_rx_seq_10GbE_mosi,
      reg_diag_rx_seq_miso           => reg_diag_rx_seq_10GbE_miso
    );
  end generate;

  -----------------------------------------------------------------------------
  -- tr_10GbE
  -----------------------------------------------------------------------------

  u_areset_sa_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1',
    g_delay_len => 4
  )
  port map(
    clk     => SA_CLK,
    in_rst  => '0',
    out_rst => sa_rst
  );

  gen_tr_10GbE : if c_revision_select.use_10GbE = true generate
    u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => g_sim,
      g_sim_level     => 1,
      g_nof_macs      => c_nof_streams_10GbE,
      g_use_mdio      => true,
      g_tx_fifo_fill  => c_def_10GbE_block_size,
      g_tx_fifo_size  => c_def_10GbE_block_size * 2
    )
    port map (
      tr_ref_clk_156      => SA_CLK,
      tr_ref_rst_156      => sa_rst,

      cal_rec_clk         => cal_rec_clk,  -- mm_clk, --cal_clk, mm_clk required by XAUI phy

      -- MM interface
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,

      reg_mac_mosi        => reg_tr_10GbE_mosi,
      reg_mac_miso        => reg_tr_10GbE_miso,

      xaui_mosi           => reg_tr_xaui_mosi,
      xaui_miso           => reg_tr_xaui_miso,

      -- DP interface
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      src_out_arr         => dp_offload_rx_10GbE_snk_in_arr,
      src_in_arr          => dp_offload_rx_10GbE_snk_out_arr,

      snk_out_arr         => dp_offload_tx_10GbE_src_in_arr,
      snk_in_arr          => dp_offload_tx_10GbE_src_out_arr,

      -- Serial XAUI IO
      xaui_tx_arr         => i_xaui_tx_arr,
      xaui_rx_arr         => i_xaui_rx_arr,

      -- MDIO External clock and serial data.
      mdio_rst            => SI_FN_RSTN,
      mdio_mdc_arr        => mdio_mdc_arr,
      mdio_mdat_in_arr    => mdio_mdat_in_arr,
      mdio_mdat_oen_arr   => mdio_mdat_oen_arr
    );

    -- Wire together different types
    gen_wires: for i in 0 to c_nof_streams_10GbE-1 generate
      xaui_tx_arr(i) <= i_xaui_tx_arr(i);
      i_xaui_rx_arr(i) <= xaui_rx_arr(i);
    end generate;

    gen_tr_front : if c_revision_select.use_front = 1 generate
    u_front_io : entity unb1_board_lib.unb1_board_front_io
    generic map (
      g_nof_xaui => c_nof_streams_10GbE
    )
    port map (
      xaui_tx_arr       => xaui_tx_arr,
      xaui_rx_arr       => xaui_rx_arr,

      mdio_mdc_arr      => mdio_mdc_arr,
      mdio_mdat_in_arr  => mdio_mdat_in_arr,
      mdio_mdat_oen_arr => mdio_mdat_oen_arr,

      -- Serial I/O
      SI_FN_0_TX        => SI_FN_0_TX,
      SI_FN_0_RX        => SI_FN_0_RX,
      SI_FN_1_TX        => SI_FN_1_TX,
      SI_FN_1_RX        => SI_FN_1_RX,
      SI_FN_2_TX        => SI_FN_2_TX,
      SI_FN_2_RX        => SI_FN_2_RX,

      SI_FN_0_CNTRL     => SI_FN_0_CNTRL,
      SI_FN_1_CNTRL     => SI_FN_1_CNTRL,
      SI_FN_2_CNTRL     => SI_FN_2_CNTRL,
      SI_FN_3_CNTRL     => SI_FN_3_CNTRL
    );
    end generate;
  end generate;

gen_mms_io_ddr_diag_MB_I : if c_revision_select.use_ddr_MB_I = 1 generate
  u_mms_io_ddr_diag_MB_I : entity io_ddr_lib.mms_io_ddr_diag
    generic map(
      -- System
      g_technology       => g_technology,
      g_dp_data_w        => c_data_w_64,
      g_dp_seq_dat_w     => c_seq_dat_w,
      g_dp_wr_fifo_depth => c_wr_fifo_depth,
      g_dp_rd_fifo_depth => c_rd_fifo_depth,
      -- IO_DDR
      g_io_tech_ddr      => c_revision_select.use_tech_ddr,
      -- DIAG data buffer
      g_db_use_db        => c_use_db,
      g_db_buf_nof_data  => c_buf_nof_data
    )
    port map(
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      -- DDR reference clock
      ctlr_ref_clk        => CLK,
      ctlr_ref_rst        => ddr_ref_rst,

      -- DDR controller clock domain
      ctlr_clk_out        => MB_I_ctlr_clk,
      ctlr_rst_out        => MB_I_ctlr_rst,

      ctlr_clk_in         => MB_I_ctlr_clk,
      ctlr_rst_in         => MB_I_ctlr_rst,

      -- MM interface
      reg_io_ddr_mosi     => reg_io_ddr_MB_I_mosi,
      reg_io_ddr_miso     => reg_io_ddr_MB_I_miso,

      -- Write / read FIFO status for monitoring purposes (in dp_clk domain)
      wr_fifo_usedw       => OPEN,
      rd_fifo_usedw       => OPEN,

      -- DDR3 pass on signals from master to slave controller
      term_ctrl_out       => OPEN,
      term_ctrl_in        => OPEN,

      -- DDR3 PHY external interface
      phy3_in             => MB_I_IN,
      phy3_io             => MB_I_IO,
      phy3_ou             => MB_I_OU,

      -- DIAG Tx seq
      reg_tx_seq_mosi     => reg_diag_tx_seq_ddr_MB_I_mosi,
      reg_tx_seq_miso     => reg_diag_tx_seq_ddr_MB_I_miso,

      -- DIAG rx seq with optional data buffer
      reg_data_buf_mosi   => reg_diag_data_buf_ddr_MB_I_mosi,
      reg_data_buf_miso   => reg_diag_data_buf_ddr_MB_I_miso,
      ram_data_buf_mosi   => ram_diag_data_buf_ddr_MB_I_mosi,
      ram_data_buf_miso   => ram_diag_data_buf_ddr_MB_I_miso,
      reg_rx_seq_mosi     => reg_diag_rx_seq_ddr_MB_I_mosi,
      reg_rx_seq_miso     => reg_diag_rx_seq_ddr_MB_I_miso
    );
  end generate;

gen_mms_io_ddr_diag_MB_II : if c_revision_select.use_ddr_MB_II = 1 generate
  u_mms_io_ddr_diag_MB_II : entity io_ddr_lib.mms_io_ddr_diag
    generic map(
      -- System
      g_technology       => g_technology,
      g_dp_data_w        => c_data_w_64,
      g_dp_seq_dat_w     => c_seq_dat_w,
      g_dp_wr_fifo_depth => c_wr_fifo_depth,
      g_dp_rd_fifo_depth => c_rd_fifo_depth,
      -- IO_DDR
      g_io_tech_ddr      => c_revision_select.use_tech_ddr,
      -- DIAG data buffer
      g_db_use_db        => c_use_db,
      g_db_buf_nof_data  => c_buf_nof_data
    )
    port map(
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      -- DDR reference clock
      ctlr_ref_clk        => CLK,
      ctlr_ref_rst        => ddr_ref_rst,

      -- DDR controller clock domain
      ctlr_clk_out        => MB_II_ctlr_clk,
      ctlr_rst_out        => MB_II_ctlr_rst,

      ctlr_clk_in         => MB_II_ctlr_clk,
      ctlr_rst_in         => MB_II_ctlr_rst,

      -- MM interface
      reg_io_ddr_mosi     => reg_io_ddr_MB_II_mosi,
      reg_io_ddr_miso     => reg_io_ddr_MB_II_miso,

      -- Write / read FIFO status for monitoring purposes (in dp_clk domain)
      wr_fifo_usedw       => OPEN,
      rd_fifo_usedw       => OPEN,

      -- DDR3 pass on signals from master to slave controller
      term_ctrl_out       => OPEN,
      term_ctrl_in        => OPEN,

      -- DDR3 PHY external interface
      phy3_in             => MB_II_IN,
      phy3_io             => MB_II_IO,
      phy3_ou             => MB_II_OU,

      -- DIAG Tx seq
      reg_tx_seq_mosi     => reg_diag_tx_seq_ddr_MB_II_mosi,
      reg_tx_seq_miso     => reg_diag_tx_seq_ddr_MB_II_miso,

      -- DIAG rx seq with optional data buffer
      reg_data_buf_mosi   => reg_diag_data_buf_ddr_MB_II_mosi,
      reg_data_buf_miso   => reg_diag_data_buf_ddr_MB_II_miso,
      ram_data_buf_mosi   => ram_diag_data_buf_ddr_MB_II_mosi,
      ram_data_buf_miso   => ram_diag_data_buf_ddr_MB_II_miso,
      reg_rx_seq_mosi     => reg_diag_rx_seq_ddr_MB_II_mosi,
      reg_rx_seq_miso     => reg_diag_rx_seq_ddr_MB_II_miso
    );
  end generate;
end str;
