-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Test bench to investigate quantization in poly phase filterbank (PFB) using WG. The main goal is to
--          verify that the PFB achieves the expected processing gain between input SNR and subband output SNR.
-- Description:
--   . Use g_sel_pfb to select the APERTIF wpfb_unit_dev.vhd or LOFAR1 pfb2_unit.vhd as device under test (DUT).
--   . This tb assumes two real input and wb_factor = 1 and nof_chan = 0 (as for LOFAR2.0).
--   . The tb measures the signal to noise ratio (SNR) at several stages:
--     - WG output
--     - FIR filter output
--     - SST
--     The SNR is use to determine the performance of the WPFB:
--     - The implementation loss due to quantization should be at least a factor 10 less than the input quantization
--       noise of the ADC. If the noise power increases by a factor 1.1, then the SNR will reduce by 10*log10(1.0/1.1)
--       = 0.4 dB.
--     - The FFT achieves the processing gain for the WG signal at the specific subband. The subband filterbank has
--       N_sub = N_fft/2 = 512 subbands, so the expected processing gain of the FFT (for a real input) is
--       20*log10(sqrt(N_sub) = 27.1 dB.
--     - The expected SNR of a subband in the SST is processing gain - implementation loss = 27.1 - 0.4 = 26.7 dB
--       higher than the SNR of the WG input signal.
--     - The polyphase FIR filter of the PFB uses the same Coeffs16384Kaiser-quant.dat coefficients as in LOFAR1.
--       From run_pfir_coeff.m it follows that the equivaluent nois bandwidth of a subband with this filter is
--       0.96 bins, so for the PFB the expected processing gain increases by about 10*log10(0.96) = 0.17 dB compared
--       to for the FFT, so about 27.3 dB.
--   . The WG does not produce a perfect sampled sine. Therefore the SNR of the WG input will be somewhat
--     less than the theoretical SNR of an ADC (SNR = W_adc * 6.02 + 1.76 dB). Hence for W_adc = 14 the SNR of
--     the generated carrier wave (CW) sine is SNR = c_wg_snr_a_dB = 86 dB.
--   . For fractional WG frequencies for which the period does not fit the WG internal buffer size of
--     g_buf_addr_w, the SNR of the carrier wave (CW) is limited to SNR = (g_buf_addr_w-1) * 6.02 + 1.76 dB
--     (see diag_wg.vhd). The g_buf_addr_w = 10. Therefore for fractional g_subband_index_a,b the effective SNR
--     of the generated CW sine is SNR ~= 56 dB, even when W_adc = 14 > 9 bit.
--   . The SNR of the WG and the FIR filter output is measured by estimating first the amplitude and
--     phase of the input sine. These estimated amplitude and phase are then used to create a local CW
--     (carrier wave) and subtract that from the input sine to get the equivalent noise. The SNR is then
--     equal to the CW power / noise power.
--   . The SNR of the SST is measured by using the power in the subband that corresponds to the WG
--     frequency as signal power, and the power in the other subbands as noise power.
--   . The tb does support using WG phase /= 0, this should yield same SNR results as phase = 0
--   . The tb does support using and using WG frequencies that fall fractionally between subbands, provided that
--     the fraction is a multiple of 1/c_N_blk, because then the WG sine will have DC = 0 over the sync interval
--     of c_N_blk blocks.
--   . For the SNR measurements it is sufficient to use WG frequencies at the subband center.
--
-- Remark:
-- . In retrospect it would have been better to use quantized REAL SIN() in testbench as carrier wave (CW)
--   waveform source, instead of using the WG, because the WG is not good enough for fractional subband
--   frequencies and because SIN() is easier to use in the test bench.
-- . Using quantized REAL also allows defining other input signals, like noise or combination of SIN() and
--   noise, although such tests are better done in MATLAB. Purpose of this tb is only to verify the
--   implementation losses.
-- . The WG frequency resolution can be improved by using a longer reference waveform in the WG buffer RAM.
--   Fractional subband frequencies of 0.1 can be supported accurately by using a reference waveform that
--   has a factor 1/0.1 = 10 more resolution.
--
-- Usage:
--   > as 3 default, or as 12 for details
--   > run -all
--   > testbench is partially selftesting.
--   > observe the *_scope signals as radix decimal, format analogue format signals in the Wave window.
--     default use analogue(automatic), if necessary zoom in using right click analogue(custom).
--

library ieee, common_lib, dp_lib, diag_lib, filter_lib, rTwoSDF_lib, fft_lib, wpfb_lib;
library pfs_lib, pft2_lib, pfb2_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;
use IEEE.math_real.all;
use std.textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use diag_lib.diag_pkg.all;
use diag_lib.tb_diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
-- APERTIF WPFB:
use filter_lib.fil_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use rTwoSDF_lib.twiddlesPkg.all;
use fft_lib.fft_pkg.all;
use fft_lib.tb_fft_pkg.all;
use wpfb_lib.wpfb_pkg.all;
-- LOFAR1 PFB2:
use pfs_lib.pfs_pkg.all;
use pft2_lib.pft_pkg.all;

entity tb_verify_pfb_wg is
  generic (
    g_tb_index        : natural := 0;  -- use g_tb_index to identify and separate print_str() loggings from multi tb
    g_sel_pfb         : string := "WPFB";  -- "WPFB" for APERTIF PFB, "PFB2" for LOFAR1 PBF
    --g_sel_pfb         : STRING := "PFB2";

    -- WG
    g_subband_index_a : real := 61.0;  -- 0:511
    g_subband_index_b : real := 61.0;  -- 0:511
    g_amplitude_a     : real := 1.0;  -- 1.0 is full scale
    g_amplitude_b     : real := 0.0;  -- 1.0 is full scale
    g_phase_a         : real := 0.0;  -- 0:360 degrees
    g_phase_b         : real := 0.0;  -- 0:360 degrees

    -- WPFB fields in c_wpfb
    -- . c_sdp_wpfb_subbands from sdp_pkg.vhd:
    --   . g_fil_backoff_w   = 1
    --   . g_fil_in_dat_w    = 14 = W_adc
    --   . g_internal_dat_w  = 16 = number of bits between fil and fft
    --   . g_fft_out_dat_w   = 18 = W_subband
    --   . g_fft_out_gain_w  = 1
    --   . g_fft_stage_dat_w = 18 = c_dsp_mult_w
    --   . g_fft_guard_w     = 2
    -- . c_wb1_two_real_1024 from tb_wpfb_unit_wide.vhd:
    --   . g_fil_backoff_w   = 1
    --   . g_fil_in_dat_w    = 8
    --   . g_internal_dat_w  = 16 = number of bits between fil and fft
    --   . g_fft_out_dat_w   = 16
    --   . g_fft_out_gain_w  = 1
    --   . g_fft_stage_dat_w = 18
    --   . g_fft_guard_w     = 2

    -- FIR filter
    g_fil_coefs_file_prefix : string := "data/Coeffs16384Kaiser-quant_1wb";  -- PFIR coefficients file access
    --g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";   -- bypass PFIR
    --g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_fircls1_16taps_1024points_16b_1wb";   -- g_fil_coef_dat_w = 16 bit
    --g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_fircls1_16taps_1024points_18b_1wb";   -- g_fil_coef_dat_w = 18 bit
    g_fil_coef_dat_w        : natural := 16;  -- = 16, data width of the FIR coefficients
    --g_fil_coef_dat_w        : NATURAL := 18;   -- = 16, data width of the FIR coefficients
    g_fil_backoff_w         : natural := 1;  -- = 0, number of bits for input backoff to avoid output overflow
    g_fil_in_dat_w          : natural := 14;  -- = W_adc, number of input bits

    g_internal_dat_w        : natural := 0;  -- = number of bits between fil and fft, use 0 to use maximum default:
                                               -- . WPFB : g_internal_dat_w <= g_fft_stage_dat_w - g_fft_guard_w in fft_r2_pipe
                                               -- . PFB2 : g_internal_dat_w <= g_fft_stage_dat_w

    -- FFT
    g_fft_out_dat_w         : natural := 18;  -- = W_subband, number of output bits
    g_fft_out_gain_w        : natural := 1;  -- = 1, output gain factor applied after the last stage output, before requantization to out_dat_w
    g_fft_stage_dat_w       : natural := 24;  -- = c_dsp_mult_w = 18, number of bits that are used inter-stage
    g_fft_guard_w           : natural := 1;  -- = 2
    g_switch_en             : std_logic := '0'  -- two real input decorrelation option in PFB2
  );
end entity tb_verify_pfb_wg;

architecture tb of tb_verify_pfb_wg is
  constant c_mm_clk_period          : time := 1 ns;
  constant c_dp_clk_period          : time := 10 ns;

  -- Define input stimuli WG sinus or impulse
  constant c_view_pfir_impulse_reponse : boolean := false;  -- Default FALSE to use WG data input, else use TRUE to view PFIR coefficients via fil_re_scope in Wave Window
  --CONSTANT c_view_pfir_impulse_reponse : BOOLEAN := TRUE;

  -- Determine bypass PFIR for PFB2, using g_fil_coefs_file_prefix setting for WPFB
  constant c_pfs_bypass        : boolean := g_fil_coefs_file_prefix = "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";
  constant c_pfs_lofar1        : boolean := g_fil_coefs_file_prefix = "data/Coeffs16384Kaiser-quant_1wb";

  -- Determine PFIR coefficient width for WPFB and PFB2
  constant c_pfir_coef_w       : natural := sel_a_b(g_sel_pfb = "WPFB", g_fil_coef_dat_w, 16);
  constant c_pfir_coefs_file   : string := c_pfs_coefs_file;  -- PFB2 "data/pfs_coefsbuf_1024.hex" default from pfs_pkg.vhd

  -- Determine internal data width between PFIR and PFT for WPFB and PFB2, use default if g_internal_dat_w=0
  constant c_internal_dat_w    : natural := sel_a_b(g_sel_pfb = "WPFB",
                                                    sel_a_b(g_internal_dat_w > 0, g_internal_dat_w, g_fft_stage_dat_w - g_fft_guard_w),
                                                    sel_a_b(g_internal_dat_w > 0, g_internal_dat_w, g_fft_stage_dat_w));

  -- Determine two real input decorrelation logic option, only supported in PFB2
  constant c_switch_en         : std_logic := sel_a_b(g_sel_pfb = "WPFB", '0', g_switch_en);

  -- Determine FFT twiddle factors info
  constant c_fft_twiddle       : wTyp := (others => '0');
  constant c_fft_twiddle_w     : natural := c_fft_twiddle'length;  -- from rTwoSDF twiddlesPkg.vhd
  constant c_twiddle_w         : natural := sel_a_b(g_sel_pfb = "WPFB", c_fft_twiddle_w, c_pft_twiddle_w);

  -- WPFB
  -- type t_wpfb is record
  --   -- General parameters for the wideband poly phase filter
  --   wb_factor         : natural;        -- = default 4, wideband factor
  --   nof_points        : natural;        -- = 1024, N point FFT (Also the number of subbands for the filter part)
  --   nof_chan          : natural;        -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
  --   nof_wb_streams    : natural;        -- = 1, the number of parallel wideband streams. The filter coefficients are shared on every wb-stream.
  --
  --   -- Parameters for the poly phase filter
  --   nof_taps          : natural;        -- = 16, the number of FIR taps per subband
  --   fil_backoff_w     : natural;        -- = 0, number of bits for input backoff to avoid output overflow
  --   fil_in_dat_w      : natural;        -- = 8, number of input bits
  --   fil_out_dat_w     : natural;        -- = 16, number of output bits
  --   coef_dat_w        : natural;        -- = 16, data width of the FIR coefficients
  --
  --   -- Parameters for the FFT
  --   use_reorder       : boolean;        -- = false for bit-reversed output, true for normal output
  --   use_fft_shift     : boolean;        -- = false for [0, pos, neg] bin frequencies order, true for [neg, 0, pos] bin frequencies order in case of complex input
  --   use_separate      : boolean;        -- = false for complex input, true for two real inputs
  --   fft_in_dat_w      : natural;        -- = 16, number of input bits
  --   fft_out_dat_w     : natural;        -- = 13, number of output bits
  --   fft_out_gain_w    : natural;        -- = 0, output gain factor applied after the last stage output, before requantization to out_dat_w
  --   stage_dat_w       : natural;        -- = 18, number of bits that are used inter-stage
  --   guard_w           : natural;        -- = 2
  --   guard_enable      : boolean;        -- = true
  --
  --   -- Parameters for the statistics
  --   stat_data_w       : positive;       -- = 56
  --   stat_data_sz      : positive;       -- = 2
  --   nof_blk_per_sync  : natural;        -- = 800000, number of FFT output blocks per sync interval
  --
  --   -- Pipeline parameters for both poly phase filter and FFT. These are heritaged from the filter and fft libraries.
  --   pft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the pipelined FFT
  --   fft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the parallel FFT
  --   fil_pipeline      : t_fil_ppf_pipeline; -- Pipeline settings for the filter units
  -- end record;
  constant c_wpfb : t_wpfb := (1, 1024, 0, 1,
                               16, g_fil_backoff_w, g_fil_in_dat_w, c_internal_dat_w, c_pfir_coef_w,
                               true, false, true, c_internal_dat_w, g_fft_out_dat_w, g_fft_out_gain_w, g_fft_stage_dat_w, g_fft_guard_w, true, 54, 2, 10,
                               c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);

  constant c_N_fft                   : natural := c_wpfb.nof_points;
  constant c_N_sub                   : natural := c_N_fft / c_nof_complex;
  constant c_N_blk                   : natural := c_wpfb.nof_blk_per_sync;  -- nof FFT blocks per sync interval
  constant c_N_samples               : natural := c_N_fft * c_N_blk;  -- nof samples per sync interval
  constant c_nof_channels            : natural := 2**c_wpfb.nof_chan;  -- = 2**0 = 1, so no time multiplexing of inputs
  constant c_nof_sync                : natural := 5;  -- nof sync intervals to simulate

  -- Subband at WG frequency
  constant c_bin_a                   : natural := natural(FLOOR(g_subband_index_a));
  constant c_bin_a_frac_en           : boolean := g_subband_index_a > real(c_bin_a);
  constant c_bin_b                   : natural := natural(FLOOR(g_subband_index_b));
  constant c_bin_b_frac_en           : boolean := g_subband_index_b > real(c_bin_b);

  -- BSN source
  constant c_bsn_w                   : natural := 64;

  -- ADC
  constant c_W_adc                   : natural := c_wpfb.fil_in_dat_w;
  constant c_adc_fs                  : real := real(2**(c_W_adc - 1) - 1);  -- ADC full scale
  constant c_adc_quant_power         : real := 1.0 / 12.0;  -- ADC theoretical quantization noise power

  -- Waveform Generator
  constant c_wg_ampl_a               : real := g_amplitude_a * c_adc_fs;
  constant c_wg_ampl_b               : real := g_amplitude_b * c_adc_fs;
  constant c_wg_sigma_a              : real := c_wg_ampl_a / SQRT(2.0);
  constant c_wg_sigma_b              : real := c_wg_ampl_b / SQRT(2.0);
  constant c_wg_power_a              : real := c_wg_sigma_a**2.0;
  constant c_wg_power_b              : real := c_wg_sigma_b**2.0;
  constant c_wg_snr_a                : real := c_wg_power_a / c_adc_quant_power;
  constant c_wg_snr_a_dB             : real := 10.0 * LOG10(c_wg_snr_a + c_eps);
  constant c_wg_snr_b                : real := c_wg_power_b * c_adc_quant_power;
  constant c_wg_snr_b_dB             : real := 10.0 * LOG10(c_wg_snr_b + c_eps);

  constant c_wg_buf_directory        : string := "data/";
  constant c_wg_buf_dat_w            : natural := 18;  -- default value of WG that fits 14 bits of ADC data
  constant c_wg_buf_addr_w           : natural := 10;  -- default value of WG for 1024 samples;
  constant c_wg_calc_gain_w          : natural := 1;  -- Normalized range [0 1>  maps to fixed point range [0:2**(c_W_adc-1)>
  constant c_wg_calc_dat_w           : natural := c_W_adc;
  constant c_wg_subband_freq_unit    : real := c_diag_wg_freq_unit / real(c_N_fft);  -- freq = Fs/16 = 200 MSps/16 = 12.5 MHz sinus,
                                                                                     -- subband index / c_N_fft = 64 / 1024 = 1/16

  -- SST
  -- Expected subband amplitude gain relative to input WG amplitude -1 for divide by 2 in two real input separate (Ampl --> Ampl/2)
  -- . default assume c_fir_filter_dc_gain ~= 1.0, like with c_fil_lofar1_fir_filter_dc_gain = 0.994817
  constant c_fir_filter_dc_gain      : real := sel_a_b(c_pfs_lofar1, c_fil_lofar1_fir_filter_dc_gain, 1.0);
  constant c_pfb_sub_scaling         : real := func_wpfb_subband_gain(c_wpfb, c_fir_filter_dc_gain);

  constant c_exp_sub_a_ampl          : real := c_wg_ampl_a * c_pfb_sub_scaling;
  constant c_exp_sub_b_ampl          : real := c_wg_ampl_b * c_pfb_sub_scaling;

  -- Use 1 as integration interval, because measured sst_wg_power_a,b is normalized for c_N_blk
  constant c_exp_sst_a               : real := func_wpfb_sst_level(c_exp_sub_a_ampl, 1);
  constant c_exp_sst_b               : real := func_wpfb_sst_level(c_exp_sub_b_ampl, 1);

  -- TB
  signal bs_end                 : std_logic := '0';
  signal tb_end                 : std_logic := '0';
  signal mm_rst                 : std_logic;
  signal mm_clk                 : std_logic := '0';
  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  -- MM
  signal reg_wg_mosi_a          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_wg_miso_a          : t_mem_miso;
  signal reg_wg_mosi_b          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_wg_miso_b          : t_mem_miso;
  signal ram_st_sst_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_sst_miso        : t_mem_miso;

  -- Input
  signal trigger_wg             : std_logic := '0';
  signal wg_val                 : std_logic;

  signal bs_start               : std_logic := '0';
  signal bs_sosi                : t_dp_sosi;
  signal wg_sosi_a_arr          : t_dp_sosi_arr(0 downto 0);
  signal wg_sosi_b_arr          : t_dp_sosi_arr(0 downto 0);

  signal impulse_data           : std_logic_vector(c_W_adc - 1 downto 0) := (others => '0');
  signal impulse_cnt            : natural := 0;

  signal in_sosi_arr            : t_dp_sosi_arr(0 downto 0);
  signal in_sosi                : t_dp_sosi;
  signal in_a_scope             : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal in_b_scope             : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal in_val                 : std_logic;
  signal in_val_cnt_per_sop     : natural := 0;  -- count valid samples per block
  signal in_val_cnt_per_sync    : natural := 0;  -- count valid samples per sync interval
  signal in_blk_cnt             : natural := 0;  -- count blocks per sync interval

  -- Filter output
  signal fil_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal fil_sosi               : t_dp_sosi;
  signal fil_re_scope           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal fil_im_scope           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal fil_val                : std_logic;
  signal fil_val_cnt_per_sop    : natural := 0;  -- count valid samples per block
  signal fil_val_cnt_per_sync   : natural := 0;  -- count valid samples per sync interval
  signal fil_blk_cnt            : natural := 0;  -- count blocks per sync interval

  signal sub_a_re               : integer := 0;
  signal sub_a_im               : integer := 0;
  signal sub_a_ampl             : real := 0.0;
  signal sub_a_re_frac          : integer := 0;
  signal sub_a_im_frac          : integer := 0;
  signal sub_a_ampl_frac        : real := 0.0;

  signal sub_b_re               : integer := 0;
  signal sub_b_im               : integer := 0;
  signal sub_b_ampl             : real := 0.0;
  signal sub_b_re_frac          : integer := 0;
  signal sub_b_im_frac          : integer := 0;
  signal sub_b_ampl_frac        : real := 0.0;

  -- Input power measurement
  -- . signal input A
  signal input_track_max_a      : real := 0.0;  -- track sample max
  signal input_track_min_a      : real := 0.0;  -- track sample min
  signal input_accum_mean_a     : real := 0.0;  -- accumulate sample mean (DC)
  signal input_accum_power_a    : real := 0.0;  -- accumulate sample power
  signal input_max_a            : real := 0.0;  -- measured sample max
  signal input_min_a            : real := 0.0;  -- measured sample min
  signal input_mean_a           : real := 0.0;  -- measured average input sample mean (DC) based on AST
  signal input_power_a          : real := 0.0;  -- measured average input sample power based on AST
  signal input_ampl_a           : real := 0.0;  -- measured input amplitude based on AST
  -- . signal input B
  signal input_track_max_b      : real := 0.0;  -- track sample max
  signal input_track_min_b      : real := 0.0;  -- track sample min
  signal input_accum_mean_b     : real := 0.0;  -- accumulate sample mean (DC)
  signal input_accum_power_b    : real := 0.0;  -- accumulate sample power
  signal input_max_b            : real := 0.0;  -- measured sample max
  signal input_min_b            : real := 0.0;  -- measured sample min
  signal input_mean_b           : real := 0.0;  -- measured average input sample mean (DC) based on AST
  signal input_power_b          : real := 0.0;  -- measured average input sample power based on AST
  signal input_ampl_b           : real := 0.0;  -- measured input amplitude based on AST

  -- Input CW amplitude and phase estimator
  -- . signal input A
  signal cw_ref_I_a             : real := 0.0;  -- local I in phase reference
  signal cw_ref_Q_a             : real := 0.0;  -- local Q quadrature reference
  signal cw_accum_I_a           : real := 0.0;  -- local I accumulator
  signal cw_accum_Q_a           : real := 0.0;  -- local Q accumulator
  signal cw_ampl_a              : real := 0.0;  -- estimated CW amplitude
  signal cw_phase_a             : real := 0.0;  -- estimated CW phase in radials
  signal cw_phase_Ts_a          : real := 0.0;  -- estimated CW phase in sample periods
  signal cw_dat_a               : integer := 0;  -- estimated CW = amplitude * SIN(omega*t + phase)
  signal cw_power_a             : real := 0.0;  -- estimated CW power
  signal cw_noise_a             : real := 0.0;  -- estimated CW quantization noise
  signal cw_accum_noise_a       : real := 0.0;  -- local noise power accumulator
  signal cw_noise_power_a       : real := 0.0;  -- estimated noise power
  signal wg_measured_snr_a      : real := 0.0;  -- estimated SNR
  signal wg_measured_snr_a_dB   : real := 0.0;  -- estimated SNR in dB

  -- . signal input B
  signal cw_ref_I_b             : real := 0.0;  -- local I in phase reference
  signal cw_ref_Q_b             : real := 0.0;  -- local Q quadrature reference
  signal cw_accum_I_b           : real := 0.0;  -- local I accumulator
  signal cw_accum_Q_b           : real := 0.0;  -- local Q accumulator
  signal cw_ampl_b              : real := 0.0;  -- estimated CW amplitude
  signal cw_phase_b             : real := 0.0;  -- estimated CW phase in radials
  signal cw_phase_Ts_b          : real := 0.0;  -- estimated CW phase in sample periods
  signal cw_dat_b               : integer := 0;  -- estimated CW = amplitude * SIN(omega*t + phase)
  signal cw_power_b             : real := 0.0;  -- estimated CW power
  signal cw_noise_b             : real := 0.0;  -- estimated CW quantization noise
  signal cw_accum_noise_b       : real := 0.0;  -- local noise power accumulator
  signal cw_noise_power_b       : real := 0.0;  -- estimated noise power
  signal wg_measured_snr_b      : real := 0.0;  -- estimated SNR
  signal wg_measured_snr_b_dB   : real := 0.0;  -- estimated SNR in dB

  -- FIR filter output power measurement
  -- . signal input A
  signal fir_track_max_a        : real := 0.0;  -- track sample max
  signal fir_track_min_a        : real := 0.0;  -- track sample min
  signal fir_accum_mean_a       : real := 0.0;  -- accumulate sample mean (DC)
  signal fir_accum_power_a      : real := 0.0;  -- accumulate sample power
  signal fir_max_a              : real := 0.0;  -- measured sample max
  signal fir_min_a              : real := 0.0;  -- measured sample min
  signal fir_mean_a             : real := 0.0;  -- measured average input sample mean (DC) based on AST
  signal fir_power_a            : real := 0.0;  -- measured average input sample power based on AST
  signal fir_ampl_a             : real := 0.0;  -- measured input amplitude based on AST
  -- . signal input B
  signal fir_track_max_b        : real := 0.0;  -- track sample max
  signal fir_track_min_b        : real := 0.0;  -- track sample min
  signal fir_accum_mean_b       : real := 0.0;  -- accumulate sample mean (DC)
  signal fir_accum_power_b      : real := 0.0;  -- accumulate sample power
  signal fir_max_b              : real := 0.0;  -- measured sample max
  signal fir_min_b              : real := 0.0;  -- measured sample min
  signal fir_mean_b             : real := 0.0;  -- measured average input sample mean (DC) based on AST
  signal fir_power_b            : real := 0.0;  -- measured average input sample power based on AST
  signal fir_ampl_b             : real := 0.0;  -- measured input amplitude based on AST

  -- FIR filter output CW amplitude and phase estimator
  -- . signal input A
  signal fil_ref_I_a             : real := 0.0;  -- local I in phase reference
  signal fil_ref_Q_a             : real := 0.0;  -- local Q quadrature reference
  signal fil_accum_I_a           : real := 0.0;  -- local I accumulator
  signal fil_accum_Q_a           : real := 0.0;  -- local Q accumulator
  signal fil_ampl_a              : real := 0.0;  -- estimated CW amplitude
  signal fil_phase_a             : real := 0.0;  -- estimated CW phase in radials
  signal fil_phase_Ts_a          : real := 0.0;  -- estimated CW phase in sample periods
  signal fil_dat_a               : integer := 0;  -- estimated CW = amplitude * SIN(omega*t + phase)
  signal fil_power_a             : real := 0.0;  -- estimated CW power
  signal fil_noise_a             : real := 0.0;  -- estimated CW quantization noise
  signal fil_accum_noise_a       : real := 0.0;  -- local noise power accumulator
  signal fil_noise_power_a       : real := 0.0;  -- estimated noise power
  signal fil_measured_snr_a      : real := 0.0;  -- estimated SNR
  signal fil_measured_snr_a_dB   : real := 0.0;  -- estimated SNR in dB

  -- . signal input B
  signal fil_ref_I_b             : real := 0.0;  -- local I in phase reference
  signal fil_ref_Q_b             : real := 0.0;  -- local Q quadrature reference
  signal fil_accum_I_b           : real := 0.0;  -- local I accumulator
  signal fil_accum_Q_b           : real := 0.0;  -- local Q accumulator
  signal fil_ampl_b              : real := 0.0;  -- estimated CW amplitude
  signal fil_phase_b             : real := 0.0;  -- estimated CW phase in radials
  signal fil_phase_Ts_b          : real := 0.0;  -- estimated CW phase in sample periods
  signal fil_dat_b               : integer := 0;  -- estimated CW = amplitude * SIN(omega*t + phase)
  signal fil_power_b             : real := 0.0;  -- estimated CW power
  signal fil_noise_b             : real := 0.0;  -- estimated CW quantization noise
  signal fil_accum_noise_b       : real := 0.0;  -- local noise power accumulator
  signal fil_noise_power_b       : real := 0.0;  -- estimated noise power
  signal fil_measured_snr_b      : real := 0.0;  -- estimated SNR
  signal fil_measured_snr_b_dB   : real := 0.0;  -- estimated SNR in dB

  -- SST
  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_N_sub - 1);

  signal sp_subband_powers_arr2 : t_slv_64_subbands_arr(c_nof_complex - 1 downto 0);  -- [sp][sub], sp = A,B, sub = 0:c_N_sub-1
  signal sp_subband_powers_a    : t_nat_real_arr(0 to c_N_sub - 1) := (others => 0.0);
  signal sp_subband_powers_b    : t_nat_real_arr(0 to c_N_sub - 1) := (others => 0.0);

  signal sst_wg_power_a         : real := 0.0;  -- measured WG sine power at WG bin in SST
  signal sst_wg_power_a_dB      : real := 0.0;  -- measured WG sine power at WG bin in SST in dB
  signal sst_noise_a            : real := 0.0;  -- measured sum of noise power in all other bins in SST
  signal sst_noise_a_dB         : real := 0.0;  -- measured sum of noise power in all other bins in SST in dB
  signal sst_wg_power_b         : real := 0.0;  -- measured WG sine power at WG bin in SST
  signal sst_wg_power_b_dB      : real := 0.0;  -- measured WG sine power at WG bin in SST in dB
  signal sst_noise_b            : real := 0.0;  -- measured sum of noise power in all other bins in SST
  signal sst_noise_b_dB         : real := 0.0;  -- measured sum of noise power in all other bins in SST in dB

  -- SNR and WPFB processing gain
  signal sst_measured_snr_a           : real := 0.0;
  signal sst_measured_snr_b           : real := 0.0;
  signal wpfb_measured_proc_gain_a    : real := 0.0;
  signal wpfb_measured_proc_gain_b    : real := 0.0;

  signal sst_measured_snr_a_dB        : real := 0.0;
  signal sst_measured_snr_b_dB        : real := 0.0;
  signal wpfb_measured_proc_gain_a_dB : real := 0.0;
  signal wpfb_measured_proc_gain_b_dB : real := 0.0;

  -- Output
  signal raw_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal out_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal out_sosi               : t_dp_sosi;
  signal out_re                 : integer;
  signal out_im                 : integer;
  signal out_val                : std_logic;
  signal out_val_cnt            : natural := 0;
  signal out_blk_cnt            : natural := 0;

  signal out_val_a              : std_logic;  -- for real A
  signal out_val_b              : std_logic;  -- for real B
  signal out_channel            : natural := 0;
  signal out_cnt                : natural := 0;
  signal out_bin_cnt            : natural := 0;
  signal out_bin                : natural := 0;

  signal reg_out_sosi           : t_dp_sosi;
  signal reg_out_re_a_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_im_a_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_re_b_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_im_b_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_val_a          : std_logic;
  signal reg_out_val_b          : std_logic;
  signal reg_out_val            : std_logic;
  signal reg_out_bin            : natural := 0;
  signal reg_out_blk_cnt        : natural := 0;

  -- . debug constant values in Wave Window
  signal dbg_c_bin_a            : natural := c_bin_a;
  signal dbg_c_bin_a_frac_en    : boolean := c_bin_a_frac_en;
  signal dbg_c_bin_b            : natural := c_bin_b;
  signal dbg_c_bin_b_frac_en    : boolean := c_bin_b_frac_en;
begin
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Run timestamp
  -----------------------------------------------------------------------------

  -- start bsn source at same time when wg_val goes high, to have input data start with initial WG phase.
  -- the wg_val goes high WG start latency = 10 cycles after trigger_wg, so issue bs_start 1 cycle earlier
  bs_start <= trigger_wg'delayed(9 * c_dp_clk_period);

  p_bs_sosi : process
  begin
    bs_sosi <= c_dp_sosi_rst;
    -- Start BSN when WG have started
    proc_common_wait_until_high(dp_clk, bs_start);
    bs_sosi.valid <= '1';
    for K in 0 to c_nof_sync loop  -- simulate one input sync interval extra to have c_nof_sync output sync intervals
      bs_sosi.sync <= '1';
      for J in 0 to c_N_blk - 1 loop
        bs_sosi.sop <= '1';
        for I in 0 to c_N_fft - 1 loop
          if I = c_N_fft - 1 then
            bs_sosi.eop <= '1';
          end if;
          wait until rising_edge(dp_clk);
          bs_sosi.sync <= '0';
          bs_sosi.sop <= '0';
          bs_sosi.eop <= '0';
          if bs_sosi.eop = '1' then
            bs_sosi.bsn <= INCR_UVEC(bs_sosi.bsn, 1);
          end if;
        end loop;
      end loop;
    end loop;
    bs_sosi.valid <= '0';
    bs_end <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Run waveform generator (WG) input A and B
  -----------------------------------------------------------------------------

  p_wg_enable_ab : process
  begin
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 10);

    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    -- WG-A
    proc_mem_mm_bus_wr(0, 1024 * 2**16 + 1,                                      mm_clk, reg_wg_mosi_a);  -- mode calc, nof_sample
    proc_mem_mm_bus_wr(1, integer(g_phase_a         * c_diag_wg_phase_unit),   mm_clk, reg_wg_mosi_a);  -- phase offset in degrees
    proc_mem_mm_bus_wr(2, integer(g_subband_index_a * c_wg_subband_freq_unit), mm_clk, reg_wg_mosi_a);  -- freq
    proc_mem_mm_bus_wr(3, integer(g_amplitude_a     * c_diag_wg_ampl_unit),    mm_clk, reg_wg_mosi_a);  -- amplitude
    -- WG-B
    proc_mem_mm_bus_wr(0, 1024 * 2**16 + 1,                                      mm_clk, reg_wg_mosi_b);  -- mode calc, nof_sample
    proc_mem_mm_bus_wr(1, integer(g_phase_b * c_diag_wg_phase_unit),           mm_clk, reg_wg_mosi_b);  -- phase offset in degrees
    proc_mem_mm_bus_wr(2, integer(g_subband_index_b * c_wg_subband_freq_unit), mm_clk, reg_wg_mosi_b);  -- freq
    proc_mem_mm_bus_wr(3, integer(g_amplitude_b * c_diag_wg_ampl_unit),        mm_clk, reg_wg_mosi_b);  -- amplitude

    -- Start WG
    proc_common_wait_some_cycles(dp_clk, 10);
    trigger_wg <= '1';
    proc_common_wait_some_cycles(dp_clk, 1);
    trigger_wg <= '0';
    wait;
  end process;

  u_wg_a : entity diag_lib.mms_diag_wg_wideband_arr
  generic map (
    g_nof_streams        => 1,
    g_cross_clock_domain => true,
    g_buf_dir            => c_wg_buf_directory,

    -- Wideband parameters
    g_wideband_factor    => 1,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          => c_wg_buf_dat_w,
    g_buf_addr_w         => c_wg_buf_addr_w,
    g_calc_support       => true,
    g_calc_gain_w        => c_wg_calc_gain_w,
    g_calc_dat_w         => c_wg_calc_dat_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_wg_mosi_a,
    reg_miso            => reg_wg_miso_a,

    buf_mosi            => c_mem_mosi_rst,

    -- Streaming clock domain
    st_rst              => dp_rst,
    st_clk              => dp_clk,
    st_restart          => trigger_wg,

    out_sosi_arr        => wg_sosi_a_arr
  );

  u_wg_b : entity diag_lib.mms_diag_wg_wideband_arr
  generic map (
    g_nof_streams        => 1,
    g_cross_clock_domain => true,
    g_buf_dir            => c_wg_buf_directory,

    -- Wideband parameters
    g_wideband_factor    => 1,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          => c_wg_buf_dat_w,
    g_buf_addr_w         => c_wg_buf_addr_w,
    g_calc_support       => true,
    g_calc_gain_w        => c_wg_calc_gain_w,
    g_calc_dat_w         => c_wg_calc_dat_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_wg_mosi_b,
    reg_miso            => reg_wg_miso_b,

    buf_mosi            => c_mem_mosi_rst,

    -- Streaming clock domain
    st_rst              => dp_rst,
    st_clk              => dp_clk,
    st_restart          => trigger_wg,

    out_sosi_arr        => wg_sosi_b_arr
  );

  wg_val <= wg_sosi_a_arr(0).valid;

  p_impulse : process(dp_clk)
  begin
    -- Create impulse during one block every 20 blocks, where 20 > c_wpfb.nof_taps
    if rising_edge(dp_clk) then
      if bs_sosi.eop = '1' then
        -- raise impulse for one block
        if impulse_cnt = 0 then
          impulse_data <= TO_SVEC(2**(c_W_adc - 2), c_W_adc);  -- 0.5 * full scale impulse that will be active at sop (= after eop)
        else
          impulse_data <= TO_SVEC(0, c_W_adc);
        end if;
        -- maintain impulse period
        if impulse_cnt = 20 then
          impulse_cnt <= 0;
        else
          impulse_cnt <= impulse_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  p_in_sosi : process(wg_sosi_a_arr, wg_sosi_b_arr, bs_sosi)
  begin
    -- DUT input
    in_sosi    <= bs_sosi;
    if c_view_pfir_impulse_reponse then
      -- Use impulse_data at real input to view PFIR coefficients in impulse response in fil_re_scope in Wave Window
      in_sosi.re <= RESIZE_DP_DSP_DATA(impulse_data);
      in_sosi.im <= TO_DP_DSP_DATA(0);
    else
      -- Use WG data
      in_sosi.re <= RESIZE_DP_DSP_DATA(wg_sosi_a_arr(0).data);  -- A via real input
      in_sosi.im <= RESIZE_DP_DSP_DATA(wg_sosi_b_arr(0).data);  -- B via imag input
    end if;
  end process;

  in_a_scope <= TO_SINT(in_sosi.re);
  in_b_scope <= TO_SINT(in_sosi.im);
  in_val <= in_sosi.valid;

  -- Counters per sop interval
  p_in_val_cnt_per_sop : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- WG input:
      if in_sosi.eop = '1' then
        in_val_cnt_per_sop <= 0;
      elsif in_sosi.valid = '1' then
        in_val_cnt_per_sop <= in_val_cnt_per_sop + 1;
      end if;
    end if;
  end process;

  p_fil_val_cnt_per_sop : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- FIR filter output:
      if fil_sosi.eop = '1' then
        fil_val_cnt_per_sop <= 0;
      elsif fil_sosi.valid = '1' then
        fil_val_cnt_per_sop <= fil_val_cnt_per_sop + 1;
      end if;
    end if;
  end process;

  -- Counters per sync interval
  p_in_val_cnt_per_sync : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- WG input:
      if in_sosi.eop = '1' and in_blk_cnt = c_N_blk - 1 then
        in_val_cnt_per_sync <= 0;
      elsif fil_sosi.valid = '1' then
        in_val_cnt_per_sync <= in_val_cnt_per_sync + 1;
      end if;
    end if;
  end process;

  p_fil_val_cnt_per_sync : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- FIR filter output:
      if fil_sosi.eop = '1' and fil_blk_cnt = c_N_blk - 1 then
        fil_val_cnt_per_sync <= 0;
      elsif fil_sosi.valid = '1' then
        fil_val_cnt_per_sync <= fil_val_cnt_per_sync + 1;
      end if;
    end if;
  end process;

  p_in_blk_cnt : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- WG input:
      if in_sosi.eop = '1' then
        if in_blk_cnt = c_N_blk - 1 then
          in_blk_cnt <= 0;
        else
          in_blk_cnt <= in_blk_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  p_fil_blk_cnt : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- FIR filter output:
      if fil_sosi.eop = '1' then
        if fil_blk_cnt = c_N_blk - 1 then
          fil_blk_cnt <= 0;
        else
          fil_blk_cnt <= fil_blk_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------------------
  -- Hold subband that represents the WG frequency
  ---------------------------------------------------------------------------
  sub_a_re      <= out_re when rising_edge(dp_clk) and out_bin = c_bin_a     and out_val_a = '1';
  sub_a_im      <= out_im when rising_edge(dp_clk) and out_bin = c_bin_a     and out_val_a = '1';
  sub_a_re_frac <= out_re when rising_edge(dp_clk) and out_bin = c_bin_a + 1 and out_val_a = '1';
  sub_a_im_frac <= out_im when rising_edge(dp_clk) and out_bin = c_bin_a + 1 and out_val_a = '1';

  sub_b_re      <= out_re when rising_edge(dp_clk) and out_bin = c_bin_b     and out_val_b = '1';
  sub_b_im      <= out_im when rising_edge(dp_clk) and out_bin = c_bin_b     and out_val_b = '1';
  sub_b_re_frac <= out_re when rising_edge(dp_clk) and out_bin = c_bin_b + 1 and out_val_b = '1';
  sub_b_im_frac <= out_im when rising_edge(dp_clk) and out_bin = c_bin_b + 1 and out_val_b = '1';

  sub_a_ampl      <= COMPLEX_RADIUS(sub_a_re, sub_a_im);
  sub_a_ampl_frac <= COMPLEX_RADIUS(sub_a_re_frac, sub_a_im_frac);

  sub_b_ampl      <= COMPLEX_RADIUS(sub_b_re, sub_b_im);
  sub_b_ampl_frac <= COMPLEX_RADIUS(sub_b_re_frac, sub_b_im_frac);

  ---------------------------------------------------------------------------
  -- Measure ADC/WG input mean (DC) and power, and determine sine amplitude
  ---------------------------------------------------------------------------
  proc_diag_measure_cw_statistics(c_N_samples, dp_clk, in_sosi.re, in_sosi.sync, in_sosi.valid, input_track_max_a, input_track_min_a, input_accum_mean_a, input_accum_power_a, input_max_a, input_min_a, input_mean_a, input_power_a, input_ampl_a);
  proc_diag_measure_cw_statistics(c_N_samples, dp_clk, in_sosi.im, in_sosi.sync, in_sosi.valid, input_track_max_b, input_track_min_b, input_accum_mean_b, input_accum_power_b, input_max_b, input_min_b, input_mean_b, input_power_b, input_ampl_b);

  ---------------------------------------------------------------------------
  -- Measure WG amplitude and phase using local I = sin and Q = cos
  ---------------------------------------------------------------------------
  proc_diag_measure_cw_ampl_and_phase(c_N_samples, c_N_fft, g_subband_index_a, dp_clk, in_sosi.re, in_sosi.sync, in_sosi.valid, in_val_cnt_per_sync, cw_ref_I_a, cw_ref_Q_a, cw_accum_I_a, cw_accum_Q_a, cw_ampl_a, cw_phase_a, cw_phase_Ts_a);
  proc_diag_measure_cw_ampl_and_phase(c_N_samples, c_N_fft, g_subband_index_b, dp_clk, in_sosi.im, in_sosi.sync, in_sosi.valid, in_val_cnt_per_sync, cw_ref_I_b, cw_ref_Q_b, cw_accum_I_b, cw_accum_Q_b, cw_ampl_b, cw_phase_b, cw_phase_Ts_b);

  cw_power_a <= (cw_ampl_a**2.0) / 2.0;
  cw_power_b <= (cw_ampl_b**2.0) / 2.0;

  proc_diag_measure_cw_noise_power(c_N_samples, c_N_fft, g_subband_index_a, dp_clk, in_sosi.re, in_sosi.sync, in_sosi.valid, in_val_cnt_per_sync, cw_ampl_a, cw_phase_a, cw_dat_a, cw_noise_a, cw_accum_noise_a, cw_noise_power_a);
  proc_diag_measure_cw_noise_power(c_N_samples, c_N_fft, g_subband_index_b, dp_clk, in_sosi.im, in_sosi.sync, in_sosi.valid, in_val_cnt_per_sync, cw_ampl_b, cw_phase_b, cw_dat_b, cw_noise_b, cw_accum_noise_b, cw_noise_power_b);

  wg_measured_snr_a    <= cw_power_a / (cw_noise_power_a + c_eps);
  wg_measured_snr_a_dB <= 10.0 * LOG10(wg_measured_snr_a + c_eps);
  wg_measured_snr_b    <= cw_power_b / (cw_noise_power_b + c_eps);
  wg_measured_snr_b_dB <= 10.0 * LOG10(wg_measured_snr_b + c_eps);

  ---------------------------------------------------------------------------
  -- Measure FIR filter output mean (DC) and power, and determine sine amplitude
  ---------------------------------------------------------------------------
  proc_diag_measure_cw_statistics(c_N_samples, dp_clk, fil_sosi.re, fil_sosi.sync, fil_sosi.valid, fir_track_max_a, fir_track_min_a, fir_accum_mean_a, fir_accum_power_a, fir_max_a, fir_min_a, fir_mean_a, fir_power_a, fir_ampl_a);  -- use fir_ to distinguish from similar fil_ signal
  proc_diag_measure_cw_statistics(c_N_samples, dp_clk, fil_sosi.im, fil_sosi.sync, fil_sosi.valid, fir_track_max_b, fir_track_min_b, fir_accum_mean_b, fir_accum_power_b, fir_max_b, fir_min_b, fir_mean_b, fir_power_b, fir_ampl_b);  -- use fir_ to distinguish from similar fil_ signal

  ---------------------------------------------------------------------------
  -- Measure FIR filter output amplitude and phase using local I = sin and Q = cos
  ---------------------------------------------------------------------------
  --gen_measure_fil_iq_a : IF NOT c_bin_a_frac_en GENERATE
    proc_diag_measure_cw_ampl_and_phase(c_N_samples, c_N_fft, g_subband_index_a, dp_clk, fil_sosi.re, fil_sosi.sync, fil_sosi.valid, fil_val_cnt_per_sync, fil_ref_I_a, fil_ref_Q_a, fil_accum_I_a, fil_accum_Q_a, fil_ampl_a, fil_phase_a, fil_phase_Ts_a);

    fil_power_a <= (fil_ampl_a**2.0) / 2.0;

    proc_diag_measure_cw_noise_power(c_N_samples, c_N_fft, g_subband_index_a, dp_clk, fil_sosi.re, fil_sosi.sync, fil_sosi.valid, fil_val_cnt_per_sync, fil_ampl_a, fil_phase_a, fil_dat_a, fil_noise_a, fil_accum_noise_a, fil_noise_power_a);

    fil_measured_snr_a    <= fil_power_a / (fil_noise_power_a + c_eps);
    fil_measured_snr_a_dB <= 10.0 * LOG10(fil_measured_snr_a + c_eps);
  --END GENERATE;

  --gen_measure_fil_iq_b : IF NOT c_bin_b_frac_en GENERATE
    proc_diag_measure_cw_ampl_and_phase(c_N_samples, c_N_fft, g_subband_index_b, dp_clk, fil_sosi.im, fil_sosi.sync, fil_sosi.valid, fil_val_cnt_per_sync, fil_ref_I_b, fil_ref_Q_b, fil_accum_I_b, fil_accum_Q_b, fil_ampl_b, fil_phase_b, fil_phase_Ts_b);

    fil_power_b <= (fil_ampl_b**2.0) / 2.0;

    proc_diag_measure_cw_noise_power(c_N_samples, c_N_fft, g_subband_index_b, dp_clk, fil_sosi.im, fil_sosi.sync, fil_sosi.valid, fil_val_cnt_per_sync, fil_ampl_b, fil_phase_b, fil_dat_b, fil_noise_b, fil_accum_noise_b, fil_noise_power_b);

    fil_measured_snr_b    <= fil_power_b / (fil_noise_power_b + c_eps);
    fil_measured_snr_b_dB <= 10.0 * LOG10(fil_measured_snr_b + c_eps);
  --END GENERATE;

  ---------------------------------------------------------------------------
  -- Read subband statistics (SST)
  -- . copied from tb_lofar2_unb2b_filterbank.vhd
  ---------------------------------------------------------------------------
  p_read_sst : process
    variable v_W, v_T, v_U, v_S, v_B : natural;  -- array indicies
    variable v_sst_noise             : real;
    variable v_gain                  : real;
  begin
    -- Wait for stimuli to finish
    proc_common_wait_until_high(dp_clk, bs_end);

    -- . the subband statistics are c_wpfb.stat_data_sz = 2 word power values.
    -- . there are c_N_sub = 512 subbands per signal path
    -- . one complex WPFB can process two real inputs A, B
    -- . the subbands are output alternately so A0 B0 A1 B1 ... A511 B511 for input A, B
    -- . the subband statistics multiple WPFB units appear in order in the ram_st_sst address map
    -- . the subband statistics are stored first lo word 0 then hi word 1

    -- Read subband statistics via MM
    for I in 0 to c_nof_complex * c_N_sub * c_wpfb.stat_data_sz - 1 loop
      v_W := I mod c_wpfb.stat_data_sz;
      v_T := (I / c_wpfb.stat_data_sz) mod c_nof_complex;
      v_U := I / (c_nof_complex * c_wpfb.stat_data_sz * c_N_sub);
      v_S := v_T + v_U * c_nof_complex;
      v_B := (I / (c_nof_complex * c_wpfb.stat_data_sz)) mod c_N_sub;
      if v_W = 0 then
        -- low part
        proc_mem_mm_bus_rd(I, mm_clk, ram_st_sst_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
        sp_subband_powers_arr2(v_S)(v_B)(31 downto 0) <= ram_st_sst_miso.rddata(31 downto 0);
      else
        -- high part
        proc_mem_mm_bus_rd(I, mm_clk, ram_st_sst_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
        sp_subband_powers_arr2(v_S)(v_B)(63 downto 32) <= ram_st_sst_miso.rddata(31 downto 0);
      end if;
    end loop;
    proc_common_wait_some_cycles(dp_clk, 10);

    -- The PFB produces narrow band subbands. Therefore the power of the input sine will appear in one subband if the
    -- frequency is at the center of the subband, or in two subbands if the frequency is between two subbands.
    -- Thanks to the PFB almost no power will leak into the other subbands. The other subbands will only contain
    -- quantization noise of the ADC and requantization noise and some cross talk between A and B due to the WPFB
    -- processing.

    -- Convert SST to unsigned REAL per signal path (SP) and normalize for integration interval of c_N_blk
    for SUB in 0 to c_N_sub - 1 loop
      sp_subband_powers_a(SUB) <= TO_UREAL(sp_subband_powers_arr2(0)(SUB)) / real(c_N_blk);
      sp_subband_powers_b(SUB) <= TO_UREAL(sp_subband_powers_arr2(1)(SUB)) / real(c_N_blk);
    end loop;
    proc_common_wait_some_cycles(dp_clk, 1);

    -- Determine WG sine SST power in WG frequency subband. Use power in two neighbour subbands if the subband frequency is not at the subband center.
    if c_bin_a_frac_en then
      sst_wg_power_a <= sp_subband_powers_a(c_bin_a) + sp_subband_powers_a(c_bin_a + 1);
    else
      sst_wg_power_a <= sp_subband_powers_a(c_bin_a);
    end if;
    if c_bin_b_frac_en then
      sst_wg_power_b <= sp_subband_powers_b(c_bin_b) + sp_subband_powers_b(c_bin_b + 1);
    else
      sst_wg_power_b <= sp_subband_powers_b(c_bin_b);
    end if;

    -- Determine noise SST power per subbands, by summing SST power in all other subbands, and skipping
    -- the subband with the WG sine SST power
    v_sst_noise := 0.0;
    for I in 0 to c_N_sub - 1 loop
      if not ( (I = c_bin_a) or (I = c_bin_a + 1 and c_bin_a_frac_en) ) then
        v_sst_noise := v_sst_noise + sp_subband_powers_a(I);
      end if;
    end loop;
    sst_noise_a <= v_sst_noise / real(c_N_sub - 1);

    v_sst_noise := 0.0;
    for I in 0 to c_N_sub - 1 loop
      if not ( (I = c_bin_b) or (I = c_bin_b + 1 and c_bin_b_frac_en) ) then
        v_sst_noise := v_sst_noise + sp_subband_powers_b(I);
      end if;
    end loop;
    sst_noise_b <= v_sst_noise / real(c_N_sub - 1);
    proc_common_wait_some_cycles(dp_clk, 1);

    -- SST power in dB
    sst_wg_power_a_dB <= 10.0 * LOG10(sst_wg_power_a + c_eps);
    sst_wg_power_b_dB <= 10.0 * LOG10(sst_wg_power_b + c_eps);
    sst_noise_a_dB    <= 10.0 * LOG10(sst_noise_a + c_eps);
    sst_noise_b_dB    <= 10.0 * LOG10(sst_noise_b + c_eps);

    -- Determine SNR in WG subband, using noise power in one subband
    sst_measured_snr_a    <= sst_wg_power_a / (sst_noise_a + c_eps);   proc_common_wait_some_cycles(dp_clk, 1);
    sst_measured_snr_a_dB <= 10.0 * LOG10(sst_measured_snr_a + c_eps);
    sst_measured_snr_b    <= sst_wg_power_b / (sst_noise_b + c_eps);   proc_common_wait_some_cycles(dp_clk, 1);
    sst_measured_snr_b_dB <= 10.0 * LOG10(sst_measured_snr_b + c_eps);
    proc_common_wait_some_cycles(dp_clk, 1);

    -- Determine WPFB processing gain in SNR
    wpfb_measured_proc_gain_a    <= sst_measured_snr_a / (wg_measured_snr_a + c_eps);         proc_common_wait_some_cycles(dp_clk, 1);
    wpfb_measured_proc_gain_a_dB <= 10.0 * LOG10(wpfb_measured_proc_gain_a + c_eps);
    wpfb_measured_proc_gain_b    <= sst_measured_snr_b / (wg_measured_snr_b + c_eps);         proc_common_wait_some_cycles(dp_clk, 1);
    wpfb_measured_proc_gain_b_dB <= 10.0 * LOG10(wpfb_measured_proc_gain_b + c_eps);
    proc_common_wait_some_cycles(dp_clk, 1);

    ---------------------------------------------------------------------------
    -- Report
    ---------------------------------------------------------------------------
    proc_common_wait_some_cycles(dp_clk, g_tb_index);  -- use g_tb_index to identify and separate logging in case of multiple tb instances finishing in parallel
    if g_sel_pfb = "WPFB" then
      print_str("  -- -----------------------------------------------------------");
      print_str("  -- WPFB settings of tb-" & int_to_str(g_tb_index) & ":");
      print_str("  -- -----------------------------------------------------------");
      print_str(". c_pfs_bypass                 = " & bool_to_str(c_pfs_bypass));
      print_str(". g_fil_coefs_file_prefix      = " & g_fil_coefs_file_prefix);
      print_str(". c_pfir_coef_w                = " & int_to_str(c_pfir_coef_w));
      print_str(". g_fil_backoff_w              = " & int_to_str(g_fil_backoff_w));
      print_str(". g_fil_in_dat_w               = " & int_to_str(g_fil_in_dat_w));
      print_str(". c_internal_dat_w             = " & int_to_str(c_internal_dat_w));
      print_str(". c_twiddle_w                  = " & int_to_str(c_twiddle_w));
      print_str(". g_fft_out_dat_w              = " & int_to_str(g_fft_out_dat_w));
      print_str(". g_fft_out_gain_w             = " & int_to_str(g_fft_out_gain_w));
      print_str(". g_fft_stage_dat_w            = " & int_to_str(g_fft_stage_dat_w));
      print_str(". g_fft_guard_w                = " & int_to_str(g_fft_guard_w));
      print_str(". c_switch_en                  = " & slv_to_str(slv(c_switch_en)));
    end if;
    if g_sel_pfb = "PFB2" then
      print_str("  -- -----------------------------------------------------------");
      print_str("  -- PFB2 settings of tb-" & int_to_str(g_tb_index) & ":");
      print_str("  -- -----------------------------------------------------------");
      print_str(". c_pfs_bypass                 = " & bool_to_str(c_pfs_bypass));
      if c_pfs_bypass = false then
        print_str(". c_pfir_coefs_file          = " & c_pfir_coefs_file & " (pfs_coefsbuf_1024 = Coeffs16384Kaiser-quant)");
      end if;
      print_str(". c_pfir_coef_w                = " & int_to_str(c_pfir_coef_w));
      print_str(". g_fil_in_dat_w               = " & int_to_str(g_fil_in_dat_w));
      print_str(". c_internal_dat_w             = " & int_to_str(c_internal_dat_w));
      print_str(". c_twiddle_w                  = " & int_to_str(c_twiddle_w));
      print_str(". g_fft_out_dat_w              = " & int_to_str(g_fft_out_dat_w));
      print_str(". g_fft_stage_dat_w            = " & int_to_str(c_pft_stage_dat_w));
      print_str(". c_switch_en                  = " & slv_to_str(slv(c_switch_en)));
    end if;
    print_str("");
    if g_amplitude_a > 0.0 then
      print_str("WG settings:");
      print_str(". g_subband_index_a            = " & real_to_str(g_subband_index_a, 6, 3));
      print_str(". g_amplitude_a                = " & real_to_str(g_amplitude_a, 5, 3));
      print_str(". g_phase_a                    = " & real_to_str(g_phase_a, 6, 1) & " degrees");
      print_str("");
      print_str("DC, min, max levels");
      print_str(". input_min_a                  = " & real_to_str(input_min_a, 10, 3));
      print_str(". input_max_a                  = " & real_to_str(input_max_a, 10, 3));
      print_str(". input_mean_a                 = " & real_to_str(input_mean_a, 10, 3));
      print_str(". fir_min_a                    = " & real_to_str(fir_min_a, 10, 3));
      print_str(". fir_max_a                    = " & real_to_str(fir_max_a, 10, 3));
      print_str(". fir_mean_a                   = " & real_to_str(fir_mean_a, 10, 3));
      print_str("");
      print_str("Amplitudes:");
      print_str(". c_wg_ampl_a                  = " & int_to_str(natural(c_wg_ampl_a)));
      print_str(". input_ampl_a                 = " & real_to_str(input_ampl_a, 10, 3));
      print_str(". cw_ampl_a                    = " & real_to_str(cw_ampl_a, 10, 3));
      print_str(". fir_ampl_a                   = " & real_to_str(fir_ampl_a, 10, 3));
      print_str(". fil_ampl_a                   = " & real_to_str(fil_ampl_a, 10, 3));
      print_str(". sub_a_re                     = " & int_to_str(sub_a_re));
      print_str(". sub_a_im                     = " & int_to_str(sub_a_im));
      print_str(". sub_a_ampl                   = " & real_to_str(sub_a_ampl, 10, 3));
      print_str(". sub_a_ampl/c_exp_sub_a_ampl  = " & real_to_str(sub_a_ampl / c_exp_sub_a_ampl, 10, 6));
      print_str(". sub_a_re_frac                = " & int_to_str(sub_a_re_frac));
      print_str(". sub_a_im_frac                = " & int_to_str(sub_a_im_frac));
      print_str(". sub_a_ampl_frac              = " & real_to_str(sub_a_ampl_frac, 10, 3));
      print_str("");
      print_str("Phases [Ts]:");
      print_str(". cw_phase_Ts_a                = " & real_to_str(cw_phase_Ts_a, 10, 3));
      print_str(". fil_phase_Ts_a               = " & real_to_str(fil_phase_Ts_a, 10, 3));
      print_str("");
      print_str("Powers:");
      print_str(". sst_wg_power_a               = " & real_to_str(sst_wg_power_a, 15, 3) & " = " & real_to_str(sst_wg_power_a_dB, 7, 2) & " [dB]");
      print_str(". sst_wg_power_a/c_exp_sst_a   = " & real_to_str(sst_wg_power_a / c_exp_sst_a, 10, 6));
      print_str(". sst_noise_a                  = " & real_to_str(sst_noise_a, 15, 3) & " = " & real_to_str(sst_noise_a_dB, 7, 2) & " [dB]");
      print_str(". sst_noise_b                  = " & real_to_str(sst_noise_b, 15, 3) & " = " & real_to_str(sst_noise_b_dB, 7, 2) & " [dB]");  -- FFT cross talk power from a to b (if g_amplitude_b = 0)
      print_str("");
      print_str("SNR and WPFB processing gain:");
      print_str(". c_wg_snr_a_dB                = " & real_to_str(c_wg_snr_a_dB, 7, 2) & " [dB]");
      print_str(". wg_measured_snr_a_dB         = " & real_to_str(wg_measured_snr_a_dB, 7, 2) & " [dB]");
      print_str(". fil_measured_snr_a_dB        = " & real_to_str(fil_measured_snr_a_dB, 7, 2) & " [dB]");
      print_str(". sst_measured_snr_a_dB        = " & real_to_str(sst_measured_snr_a_dB, 7, 2) & " [dB]");
      print_str(". wpfb_measured_proc_gain_a_dB = " & real_to_str(wpfb_measured_proc_gain_a_dB, 7, 2) & " [dB]");
      print_str("");
    end if;
    if g_amplitude_b > 0.0 then
      print_str(". g_subband_index_b            = " & real_to_str(g_subband_index_b, 6, 3));
      print_str(". g_amplitude_b                = " & real_to_str(g_amplitude_b,     5, 3));
      print_str(". g_phase_b                    = " & real_to_str(g_phase_b,         6, 1) & " degrees");
      print_str("");
      print_str("DC, min, max levels");
      print_str(". input_min_b                  = " & real_to_str(input_min_b, 10, 3));
      print_str(". input_max_b                  = " & real_to_str(input_max_b, 10, 3));
      print_str(". input_mean_b                 = " & real_to_str(input_mean_b, 10, 3));
      print_str(". fir_min_b                    = " & real_to_str(fir_min_b, 10, 3));
      print_str(". fir_max_b                    = " & real_to_str(fir_max_b, 10, 3));
      print_str(". fir_mean_b                   = " & real_to_str(fir_mean_b, 10, 3));
      print_str("");
      print_str("Amplitudes:");
      print_str(". c_wg_ampl_b                  = " & int_to_str(natural(c_wg_ampl_b)));
      print_str(". input_ampl_b                 = " & real_to_str(input_ampl_b, 10, 3));
      print_str(". cw_ampl_b                    = " & real_to_str(cw_ampl_b, 10, 3));
      print_str(". fir_ampl_b                   = " & real_to_str(fir_ampl_b, 10, 3));
      print_str(". fil_ampl_b                   = " & real_to_str(fil_ampl_b, 10, 3));
      print_str(". sub_b_re                     = " & int_to_str(sub_b_re));
      print_str(". sub_b_im                     = " & int_to_str(sub_b_im));
      print_str(". sub_b_ampl                   = " & real_to_str(sub_b_ampl, 10, 3));
      print_str(". sub_b_ampl/c_exp_sub_b_ampl  = " & real_to_str(sub_b_ampl / c_exp_sub_b_ampl, 10, 6));
      print_str(". sub_b_re_frac                = " & int_to_str(sub_b_re_frac));
      print_str(". sub_b_im_frac                = " & int_to_str(sub_b_im_frac));
      print_str(". sub_b_ampl_frac              = " & real_to_str(sub_b_ampl_frac, 10, 3));
      print_str("");
      print_str("Phases [Ts]:");
      print_str(". cw_phase_Ts_b                = " & real_to_str(cw_phase_Ts_b, 10, 3));
      print_str(". fil_phase_Ts_b               = " & real_to_str(fil_phase_Ts_b, 10, 3));
      print_str("Powers:");
      print_str(". sst_wg_power_b               = " & real_to_str(sst_wg_power_b, 15, 3) & " = " & real_to_str(sst_wg_power_b_dB, 7, 2) & " [dB]");
      print_str(". sst_wg_power_b/c_exp_sst_b   = " & real_to_str(sst_wg_power_b / c_exp_sst_b, 10, 6));
      print_str(". sst_noise_b                  = " & real_to_str(sst_noise_b, 15, 3) & " = " & real_to_str(sst_noise_b_dB, 7, 2) & " [dB]");
      print_str(". sst_noise_a                  = " & real_to_str(sst_noise_a, 15, 3) & " = " & real_to_str(sst_noise_a_dB, 7, 2) & " [dB]");  -- FFT cross talk power from b to a (if g_amplitude_a = 0)
      print_str("");
      print_str("SNR and WPFB processing gain:");
      print_str(". c_wg_snr_b_dB                = " & real_to_str(c_wg_snr_b_dB, 7, 2) & " [dB]");
      print_str(". wg_measured_snr_b_dB         = " & real_to_str(wg_measured_snr_b_dB, 7, 2) & " [dB]");
      print_str(". fil_measured_snr_b_dB        = " & real_to_str(fil_measured_snr_b_dB, 7, 2) & " [dB]");
      print_str(". sst_measured_snr_b_dB        = " & real_to_str(sst_measured_snr_b_dB, 7, 2) & " [dB]");
      print_str(". wpfb_measured_proc_gain_b_dB = " & real_to_str(wpfb_measured_proc_gain_b_dB, 7, 2) & " [dB]");
      print_str("");
    end if;

    ---------------------------------------------------------------------------
    -- Verify
    ---------------------------------------------------------------------------
    assert almost_equal(input_ampl_a, c_wg_ampl_a, 1.0)
      report "Wrong amplitude for WG input a, " & real_to_str(input_ampl_a, 7, 0) & " /~= " & real_to_str(c_wg_ampl_a, 7, 0)
      severity ERROR;
    assert almost_equal(input_ampl_a, cw_ampl_a, 0.02)
      report "Wrong estimated amplitude for WG input a, " & real_to_str(input_ampl_a, 7, 0) & " /~= " & real_to_str(cw_ampl_a, 7, 0)
      severity ERROR;
    assert almost_zero(input_mean_a, 0.01)
      report "Wrong estimated DC mean for WG input a, " & real_to_str(input_mean_a, 7, 0) & " /~= 0.0"
      severity ERROR;
    assert almost_zero(fir_mean_a, 0.01)
      report "Wrong estimated DC mean for FIR filter output a, " & real_to_str(fir_mean_a, 7, 0) & " /~= 0.0"
      severity ERROR;
    if not c_bin_a_frac_en then
      assert almost_equal(fir_ampl_a, fil_ampl_a, 10.0)
        report "Wrong estimated amplitude for FIR filter output a, " & real_to_str(fir_ampl_a, 7, 0) & " /~= " & real_to_str(fil_ampl_a, 7, 0)
        severity ERROR;
      assert almost_equal(sub_a_ampl / cw_ampl_a / c_pfb_sub_scaling, 1.0, 0.01)
        report "Wrong measured scaling for PFB subband output a, " & real_to_str(sub_a_ampl / cw_ampl_a, 7, 0) & " /~= " & real_to_str(c_pfb_sub_scaling, 7, 0)
        severity ERROR;
      assert almost_equal(sst_wg_power_a / c_exp_sst_a, 1.0, 0.01)
        report "Wrong measured scaling for PFB SST output a, " & real_to_str(sst_wg_power_a / c_exp_sst_a, 7, 0) & " /~= 1.0"
        severity ERROR;
    end if;
    tb_end <= '1';
    wait;
  end process;

  ---------------------------------------------------------------
  -- DUT = Device Under Test
  ---------------------------------------------------------------

  in_sosi_arr(0) <= in_sosi;

  -- DUT = APERTIF WFPB
  dut_wpfb_unit_dev : if g_sel_pfb = "WPFB" generate
    u_wpfb_unit_dev : entity wpfb_lib.wpfb_unit_dev
    generic map (
      g_wpfb              => c_wpfb,
      g_coefs_file_prefix => g_fil_coefs_file_prefix
    )
    port map (
      dp_rst             => dp_rst,
      dp_clk             => dp_clk,
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      ram_fil_coefs_mosi => c_mem_mosi_rst,
      ram_fil_coefs_miso => open,
      ram_st_sst_mosi    => ram_st_sst_mosi,
      ram_st_sst_miso    => ram_st_sst_miso,
      in_sosi_arr        => in_sosi_arr,
      fil_sosi_arr       => fil_sosi_arr,
      out_quant_sosi_arr => out_sosi_arr,
      out_raw_sosi_arr   => raw_sosi_arr
    );
  end generate;

  -- DUT = LOFAR1 WFPB
  dut_pfb2_unit : if g_sel_pfb = "PFB2" generate
    u_pfb2_unit : entity pfb2_lib.pfb2_unit
    generic map (
      g_nof_streams     => 1,  -- number of pfb2 instances, 1 pfb2 per stream
      g_nof_points      => c_wpfb.nof_points,

      -- pfs
      g_pfs_bypass      => c_pfs_bypass,
      g_pfs_nof_taps    => c_wpfb.nof_taps,
      g_pfs_in_dat_w    => c_wpfb.fil_in_dat_w,
      g_pfs_out_dat_w   => c_internal_dat_w,
      g_pfs_coef_dat_w  => c_pfir_coef_w,
      g_pfs_coefs_file  => c_pfir_coefs_file,

      -- pft2
      g_pft_mode        => PFT_MODE_REAL2,
      g_pft_switch_en   => c_switch_en,
      g_pft_out_dat_w   => c_wpfb.fft_out_dat_w,
      g_pft_stage_dat_w => g_fft_stage_dat_w,

      -- sst
      g_sst_data_w      => c_wpfb.stat_data_w,
      g_sst_data_sz     => c_wpfb.stat_data_sz
    )
    port map (
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      ram_st_sst_mosi   => ram_st_sst_mosi,
      ram_st_sst_miso   => ram_st_sst_miso,
      in_sosi_arr       => in_sosi_arr,
      fil_sosi_arr      => fil_sosi_arr,
      out_sosi_arr      => out_sosi_arr
    );
  end generate;

  p_fil_sosi : process(fil_sosi_arr, fil_val_cnt_per_sop)
  begin
    fil_sosi <= fil_sosi_arr(0);
    -- Add sync, sop and eop to fil_sosi for tb
    fil_sosi.sync <= '0';
    fil_sosi.sop <= '0';
    fil_sosi.eop <= '0';
    if fil_sosi_arr(0).valid = '1' then
      if fil_val_cnt_per_sop = 0 then
        if fil_blk_cnt = 0 then
          fil_sosi.sync <= '1';
        end if;
        fil_sosi.sop <= '1';
      end if;
      if fil_val_cnt_per_sop = c_N_fft - 1 then
        fil_sosi.eop <= '1';
      end if;
    end if;
  end process;

  out_sosi <= out_sosi_arr(0);

  ---------------------------------------------------------------
  -- FIR filter output
  ---------------------------------------------------------------

  fil_re_scope <= TO_SINT(fil_sosi.re);
  fil_im_scope <= TO_SINT(fil_sosi.im);
  fil_val <= fil_sosi.valid;

  ---------------------------------------------------------------
  -- FFT output
  ---------------------------------------------------------------

  out_re <= TO_SINT(out_sosi.re);
  out_im <= TO_SINT(out_sosi.im);
  out_val <= out_sosi.valid;

  out_val_cnt <= out_val_cnt + 1 when rising_edge(dp_clk) and out_val = '1' else out_val_cnt;

  out_blk_cnt <= out_blk_cnt / c_N_fft;

  proc_fft_out_control(c_wpfb.wb_factor, c_N_fft, c_nof_channels, c_wpfb.use_reorder, c_wpfb.use_fft_shift, c_wpfb.use_separate,
                       out_val_cnt, out_val, out_val_a, out_val_b, out_channel, out_bin, out_bin_cnt);

  -- clock out_sosi to hold output for A and for B
  reg_out_val_a    <= out_val_a   when rising_edge(dp_clk);
  reg_out_val_b    <= out_val_b   when rising_edge(dp_clk);
  reg_out_val      <= out_val     when rising_edge(dp_clk);
  reg_out_bin      <= out_bin     when rising_edge(dp_clk);
  reg_out_blk_cnt  <= out_blk_cnt when rising_edge(dp_clk);

  reg_out_sosi       <= out_sosi when rising_edge(dp_clk);
  reg_out_re_a_scope <= out_re   when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_im_a_scope <= out_im   when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_re_b_scope <= out_re   when rising_edge(dp_clk) and out_val_b = '1';
  reg_out_im_b_scope <= out_im   when rising_edge(dp_clk) and out_val_b = '1';
end tb;
