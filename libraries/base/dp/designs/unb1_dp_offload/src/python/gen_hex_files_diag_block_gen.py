###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
from mem_init_file import list_to_hex

# Purpose:
# . Generate the 3 counter data HEX data files that are used by the 3 
#   block generators per interface.
# Description:
# . The count ranges from 0..899
# . This range is written to 32, 64 and 128 bit HEX files matching the data
#   width of 1GbE, 10GbE and the internal loopback respectively.

PATH = "../hex"
FILENAME = "counter_data"

NOF_STREAMS = 3
MEM_WIDTHS  = [32, 64, 128]
MEM_DEPTH   = 1024

# ====================
# Write the HEX files
# ====================
for mem_width in MEM_WIDTHS:
    for stream in range(NOF_STREAMS):
        list_to_hex( range(900), PATH+"/"+FILENAME+"_"+str(mem_width)+'_'+str(stream)+".hex", mem_width, MEM_DEPTH)

