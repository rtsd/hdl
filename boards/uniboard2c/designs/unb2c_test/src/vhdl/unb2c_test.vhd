-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, unb2c_board_10gbe_lib, dp_lib, eth_lib, tr_10GbE_lib, diag_lib, technology_lib, tech_ddr_lib, io_ddr_lib, tech_jesd204b_lib, util_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use eth_lib.eth_pkg.all;
use eth_lib.eth_tester_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.unb2c_test_pkg.all;
use util_lib.util_heater_pkg.all;

entity unb2c_test is
  generic (
    g_design_name       : string  := "unb2c_test";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e2sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_sim_model_ddr     : boolean := false;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id       : string := "";  -- revision ID     -- set by QSF
    g_factory_image     : boolean := false;
    g_protect_addr_range: boolean := false
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGIN     : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK       : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines
    SB_CLK       : in    std_logic := '0';  -- Clock 10GbE back. From on-board XTAL
    BCK_REF_CLK  : in    std_logic := '0';  -- Clock 10GbE back. From external reference. To be used for JESD204B_REFCLK

    -- DDR reference clocks
    MB_I_REF_CLK  : in   std_logic := '0';  -- Reference clock for MB_I
    MB_II_REF_CLK : in   std_logic := '0';  -- Reference clock for MB_II

    -- back transceivers
    BCK_RX       : in    std_logic_vector((c_unb2c_board_tr_back.bus_w * c_unb2c_board_tr_back.nof_bus) - 1 downto 0) := (others => '0');
    BCK_TX       : out   std_logic_vector((c_unb2c_board_tr_back.bus_w * c_unb2c_board_tr_back.nof_bus) - 1 downto 0);

    -- jesd204b syncronization signals
    -- JESD204B_REFCLK : IN    STD_LOGIC;  -- Use BCK_REF_CLK instead
    JESD204B_SYSREF : in    std_logic := '0';
    JESD204B_SYNC   : out   std_logic_vector(c_unb2c_board_nof_sync_jesd204b - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0) := (others => '0');
    RING_0_TX    : out   std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0);

    -- front transceivers
    QSFP_0_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_1_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_2_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_2_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_3_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_3_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_4_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_4_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_5_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_5_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- SO-DIMM Memory Bank I
    MB_I_IN      : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    MB_I_IO      : inout t_tech_ddr4_phy_io;
    MB_I_OU      : out   t_tech_ddr4_phy_ou;

    -- SO-DIMM Memory Bank II
    MB_II_IN     : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    MB_II_IO     : inout t_tech_ddr4_phy_io;
    MB_II_OU     : out   t_tech_ddr4_phy_ou;

    -- Leds
    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2c_test;

architecture str of unb2c_test is
  -- Firmware version x.y
  constant c_fw_version                 : t_unb2c_board_fw_version := (2, 0);
  constant c_mm_clk_freq                : natural := c_unb2c_board_mm_clk_freq_125M;
  constant c_dp_clk_freq                : natural := c_unb2c_board_ext_clk_freq_200M;

  -- Revision controlled constants
  constant c_revision_select            : t_unb2c_test_config := func_sel_revision_rec(g_design_name);
  constant c_use_loopback               : boolean := c_revision_select.use_loopback;
  constant c_use_eth_0_UDP              : boolean := c_revision_select.use_1GbE_I_UDP;  -- Enable the UDP offload ports on 1GbE-I = eth_0, eth_0 is always enabled for control
  constant c_use_eth_1                  : boolean := c_revision_select.use_1GbE_II;  -- Enable the second 1GbE-II = eth_1
  constant c_use_10GbE_qsfp             : boolean := c_revision_select.use_10GbE_qsfp;
  constant c_use_10GbE_ring             : boolean := c_revision_select.use_10GbE_ring;
  constant c_use_10GbE_back0            : boolean := c_revision_select.use_10GbE_back0;
  constant c_use_10GbE                  : boolean := c_use_10GbE_qsfp or c_use_10GbE_ring or c_use_10GbE_back0;
  constant c_use_jesd204b               : boolean := c_revision_select.use_jesd204b;
  constant c_use_heater                 : boolean := c_revision_select.use_heater;
  constant c_use_MB_I                   : boolean := c_revision_select.use_MB_I;
  constant c_use_MB_II                  : boolean := c_revision_select.use_MB_II;
  constant c_ddr_MB_I                   : t_c_tech_ddr := c_revision_select.type_MB_I;
  constant c_ddr_MB_II                  : t_c_tech_ddr := c_revision_select.type_MB_II;

  -- transceivers
  constant c_nof_qsfp                   : natural := c_unb2c_board_tr_qsfp.nof_bus * c_unb2c_board_tr_qsfp.bus_w;
  constant c_nof_ring                   : natural := c_unb2c_board_tr_ring.nof_bus * c_unb2c_board_tr_ring.bus_w;
  constant c_nof_back0                  : natural := c_unb2c_board_tr_back.bus_w;
  constant c_nof_jesd204b               : natural := c_unb2c_board_tr_jesd204b.nof_bus * c_unb2c_board_tr_jesd204b.bus_w;

  -- 1GbE
  constant c_nof_udp_streams_eth_0      : natural := 4;  -- <= c_eth_nof_udp_ports = 4, shared with M&C stream
  constant c_nof_udp_streams_eth_0_w    : natural := 2;  -- = true_log2(c_nof_udp_streams_eth_0), fixed reserve 2 bit extra MM address space
  constant c_nof_udp_streams_eth_1      : natural := 1;  -- fixed 1 UDP stream, so no need for dp_mux
  constant c_nof_udp_streams_eth_1_w    : natural := 0;  -- = true_log2(c_nof_udp_streams_eth_1), fixed reserve no extra MM address space
  constant c_base_mac                   : std_logic_vector(32 - 1 downto 0) := c_eth_tester_eth_src_mac_47_16;  -- = X"00228608"
  constant c_base_ip                    : std_logic_vector(16 - 1 downto 0) := c_eth_tester_ip_src_addr_31_16;  -- = X"0A63"
  constant c_base_udp                   : std_logic_vector(8 - 1 downto 0) := c_eth_tester_udp_src_port_15_8;  -- = X"E0"

  -- 10GbE
  constant c_nof_streams_qsfp           : natural := sel_a_b(c_use_10GbE_qsfp, c_nof_qsfp, 0);
  constant c_nof_streams_ring           : natural := sel_a_b(c_use_10GbE_ring, c_nof_ring, 0);
  constant c_nof_streams_back0          : natural := sel_a_b(c_use_10GbE_back0, c_nof_back0, 0);
  constant c_nof_streams_jesd204b       : natural := sel_a_b(c_use_jesd204b, c_nof_jesd204b, 0);

  constant c_nof_streams_10GbE          : natural := c_nof_streams_qsfp + c_nof_streams_ring + c_nof_streams_back0;

  constant c_nof_qsfp_bus               : natural := ceil_div(c_nof_streams_qsfp, c_unb2c_board_tr_qsfp.bus_w);
  constant c_nof_ring_bus               : natural := ceil_div(c_nof_streams_ring, c_unb2c_board_tr_ring.bus_w);
  constant c_nof_back_bus               : natural := ceil_div(c_nof_streams_back0, c_unb2c_board_tr_back.bus_w);

  constant c_data_w_32                  : natural := c_eth_data_w;  -- 1GbE
  constant c_data_w_64                  : natural := c_xgmii_data_w;  -- 10GbE

  -- ddr
  constant c_ddr_ctlr_data_w            : natural := func_tech_ddr_ctlr_data_w(c_ddr_MB_I);  -- = 576, assume both MB_I and MB_II use the same ctlr_data_w
  constant c_ddr_dp_data_w              : natural := c_ddr_ctlr_data_w / 4;  -- DDR4 with dq_w = 72, rsl = 8 so ctrl data width = 576 and therefore the mixed width FIFO ratio is 576 /144 = 4
  constant c_ddr_dp_seq_dat_w           : natural := 16;  -- >= 1, test sequence data width. Choose c_ddr_dp_seq_dat_w <= c_ddr_dp_data_w. The seq data gets replicated to fill c_ddr_dp_data_w.
  constant c_ddr_dp_wr_fifo_depth       : natural := 256 * (c_ddr_ctlr_data_w / c_ddr_dp_data_w);  -- defined at DP side of the FIFO, choose 256 * (ctrl_data_w/g_dp_data_w) to make full use of M9K which have at least 256 words
  constant c_ddr_dp_rd_fifo_depth       : natural := 256 * (c_ddr_ctlr_data_w / c_ddr_dp_data_w);  -- defined at DP side of the FIFO, choose 256 * (ctrl_data_w/g_dp_data_w) or factors of 2 more to fit max number of read bursts
  constant c_ddr_db_buf_nof_data        : natural := 1024;
  constant c_ddr_mixed_width_ratio      : natural := ratio2(c_ddr_ctlr_data_w, c_ddr_dp_data_w);

  -- Block generator constants
  constant c_bg_block_size              : natural := 900;
  constant c_bg_gapsize_1GbE            : natural := 1000;
  constant c_bg_gapsize_10GbE           : natural := 100;
  constant c_bg_blocks_per_sync         : natural := sel_a_b(g_sim, 10, 200000);  -- 200000*(900+100) = 200000000 cycles = 1 second

  constant c_use_jumbo_frames           : boolean := false;
  constant c_def_10GbE_block_size       : natural := 700;  -- (700/1000) * 200MHz * 64b = 8.96Gbps user rate (excl. header overhead (16 words/packet) )

  constant c_max_frame_len              : natural := sel_a_b(c_use_jumbo_frames, 9018, 1518);
  constant c_nof_header_bytes           : natural := field_slv_len(c_hdr_field_arr) / c_byte_w;
  constant c_max_udp_payload_len        : natural := c_max_frame_len - c_nof_header_bytes - c_network_eth_crc_len;

  constant c_max_udp_payload_nof_words_10GbE : natural := (c_max_udp_payload_len * c_byte_w) / c_data_w_64;
  constant c_min_nof_words_per_block         : natural := 1;
  constant c_max_nof_blocks_per_packet_10GbE : natural := c_max_udp_payload_nof_words_10GbE / c_min_nof_words_per_block;

  -- System
  signal cs_sim                     : std_logic;

  signal gn_index                   : natural;

  signal ext_clk200                 : std_logic;
  signal ext_rst200                 : std_logic;

  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;

  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_clk                     : std_logic;
  signal dp_rst                     : std_logic;
  signal dp_pps                     : std_logic;

  signal mb_I_ref_rst               : std_logic;
  signal mb_II_ref_rst              : std_logic;

  signal ddr_I_clk200               : std_logic;
  signal ddr_I_rst200               : std_logic;
  signal ddr_II_clk200              : std_logic;
  signal ddr_II_rst200              : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- Scrap RAM
  signal ram_scrap_mosi             : t_mem_mosi;
  signal ram_scrap_miso             : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi     : t_mem_mosi;
  signal reg_fpga_temp_sens_miso     : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi  : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso  : t_mem_miso;

  -- eth1g ch0
  signal eth_0_mm_rst          : std_logic;
  signal eth_0_tse_mosi        : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth_0_tse_miso        : t_mem_miso;
  signal eth_0_reg_mosi        : t_mem_mosi;  -- ETH control and status registers
  signal eth_0_reg_miso        : t_mem_miso;
  signal eth_0_reg_interrupt   : std_logic;  -- Interrupt
  signal eth_0_ram_mosi        : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth_0_ram_miso        : t_mem_miso;

  -- eth1g ch1 (eth_stream only has MM for TSE MAC)
  signal eth_1_mm_rst          : std_logic;
  signal eth_1_tse_mosi        : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth_1_tse_miso        : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- JESD control
  signal jesd_ctrl_mosi             : t_mem_mosi := c_mem_mosi_rst;
  signal jesd_ctrl_miso             : t_mem_miso := c_mem_miso_rst;

  -- Jesd204b ADC interface
  signal jesd204b_mosi              : t_mem_mosi;
  signal jesd204b_miso              : t_mem_miso;

  -- bsn source
  signal reg_bsn_source_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_source_miso        : t_mem_miso := c_mem_miso_rst;

  -- BSN MONITOR
  signal reg_bsn_monitor_input_mosi : t_mem_mosi;
  signal reg_bsn_monitor_input_miso : t_mem_miso;

  -- Data buffer bsn
  signal ram_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal ram_diag_data_buf_bsn_miso : t_mem_miso;
  signal reg_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal reg_diag_data_buf_bsn_miso : t_mem_miso;

  -- Heater
  signal reg_heater_mosi            : t_mem_mosi;
  signal reg_heater_miso            : t_mem_miso;

  -- 10GbE
  signal i_serial_10G_tx_qsfp_ring_arr   : std_logic_vector(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0);
  signal i_serial_10G_rx_qsfp_ring_arr   : std_logic_vector(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0);
  signal i_serial_10G_tx_back0_arr       : std_logic_vector(c_nof_streams_back0 - 1 downto 0);
  signal i_serial_10G_rx_back0_arr       : std_logic_vector(c_nof_streams_back0 - 1 downto 0);

  signal serial_10G_tx_qsfp_arr          : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');
  signal serial_10G_rx_qsfp_arr          : std_logic_vector(c_nof_streams_qsfp - 1 downto 0);
  signal serial_10G_tx_ring_arr          : std_logic_vector(c_nof_streams_ring - 1 downto 0) := (others => '0');
  signal serial_10G_rx_ring_arr          : std_logic_vector(c_nof_streams_ring - 1 downto 0);
  signal serial_rx_jesd204b_arr          : std_logic_vector(c_nof_streams_jesd204b - 1 downto 0);

  signal i_QSFP_TX                       : t_unb2c_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);
  signal i_QSFP_RX                       : t_unb2c_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);
  signal i_RING_TX                       : t_unb2c_board_ring_bus_2arr(c_nof_ring_bus - 1 downto 0);
  signal i_RING_RX                       : t_unb2c_board_ring_bus_2arr(c_nof_ring_bus - 1 downto 0);
  signal i_BCK_TX                        : t_unb2c_board_back_bus_2arr(c_nof_back_bus - 1 downto 0);
  signal i_BCK_RX                        : t_unb2c_board_back_bus_2arr(c_nof_back_bus - 1 downto 0);

  signal serial_10G_tx_back_arr          : std_logic_vector(c_nof_streams_back0 - 1 downto 0) := (others => '0');
  signal serial_10G_rx_back_arr          : std_logic_vector(c_nof_streams_back0 - 1 downto 0);
--  SIGNAL serial_rx_jesd204b_back_arr     : STD_LOGIC_VECTOR(24-1 DOWNTO 0);

  signal reg_10gbase_r_24_mosi           : t_mem_mosi;
  signal reg_10gbase_r_24_miso           : t_mem_miso;

  signal reg_tr_10GbE_qsfp_ring_mosi     : t_mem_mosi;
  signal reg_tr_10GbE_qsfp_ring_miso     : t_mem_miso;
  signal reg_tr_10GbE_back0_mosi         : t_mem_mosi;
  signal reg_tr_10GbE_back0_miso         : t_mem_miso;

  signal reg_eth10g_qsfp_ring_mosi       : t_mem_mosi;
  signal reg_eth10g_qsfp_ring_miso       : t_mem_miso;
  signal reg_eth10g_back0_mosi           : t_mem_mosi;
  signal reg_eth10g_back0_miso           : t_mem_miso;

  -- 1GbE I eth_tester (c_nof_udp_streams_eth_0_w = 2 bit)
  -- . Tx
  signal reg_diag_bg_eth_0_copi                : t_mem_copi := c_mem_copi_rst;  -- c_diag_bg_reg_adr_w = 3 --> w = 5
  signal reg_diag_bg_eth_0_cipo                : t_mem_cipo;
  signal reg_hdr_dat_eth_0_copi                : t_mem_copi := c_mem_copi_rst;  -- c_eth_tester_reg_hdr_dat_addr_w = 5 --> w = 7
  signal reg_hdr_dat_eth_0_cipo                : t_mem_cipo;
  signal reg_bsn_monitor_v2_tx_eth_0_copi      : t_mem_copi := c_mem_copi_rst;  -- c_dp_bsn_monitor_v2_reg_adr_w = 3 --> w = 5
  signal reg_bsn_monitor_v2_tx_eth_0_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_tx_eth_0_copi  : t_mem_copi := c_mem_copi_rst;  -- c_dp_strobe_total_count_reg_adr_w = 5 --> w = 7
  signal reg_strobe_total_count_tx_eth_0_cipo  : t_mem_cipo;
  -- . Rx
  signal reg_bsn_monitor_v2_rx_eth_0_copi      : t_mem_copi := c_mem_copi_rst;  -- c_dp_bsn_monitor_v2_reg_adr_w = 3 --> w = 5
  signal reg_bsn_monitor_v2_rx_eth_0_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_rx_eth_0_copi  : t_mem_copi := c_mem_copi_rst;  -- c_dp_strobe_total_count_reg_adr_w = 5 --> w = 7
  signal reg_strobe_total_count_rx_eth_0_cipo  : t_mem_cipo;

  -- 1GbE II eth_tester (c_nof_udp_streams_eth_1_w = 0 bit)
  -- . Tx
  signal reg_diag_bg_eth_1_copi                : t_mem_copi := c_mem_copi_rst;  -- c_diag_bg_reg_adr_w = 3 --> w = 3
  signal reg_diag_bg_eth_1_cipo                : t_mem_cipo;
  signal reg_hdr_dat_eth_1_copi                : t_mem_copi := c_mem_copi_rst;  -- c_eth_tester_reg_hdr_dat_addr_w = 5 --> w = 5
  signal reg_hdr_dat_eth_1_cipo                : t_mem_cipo;
  signal reg_bsn_monitor_v2_tx_eth_1_copi      : t_mem_copi := c_mem_copi_rst;  -- c_dp_bsn_monitor_v2_reg_adr_w = 3 --> w = 3
  signal reg_bsn_monitor_v2_tx_eth_1_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_tx_eth_1_copi  : t_mem_copi := c_mem_copi_rst;  -- c_dp_strobe_total_count_reg_adr_w = 5 --> w = 5
  signal reg_strobe_total_count_tx_eth_1_cipo  : t_mem_cipo;
  -- . Rx
  signal reg_bsn_monitor_v2_rx_eth_1_copi      : t_mem_copi := c_mem_copi_rst;  -- c_dp_bsn_monitor_v2_reg_adr_w = 3 --> w = 3
  signal reg_bsn_monitor_v2_rx_eth_1_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_rx_eth_1_copi  : t_mem_copi := c_mem_copi_rst;  -- c_dp_strobe_total_count_reg_adr_w = 5 --> w = 5
  signal reg_strobe_total_count_rx_eth_1_cipo  : t_mem_cipo;

  -- 10GbE
  signal reg_diag_bg_10GbE_mosi          : t_mem_mosi;
  signal reg_diag_bg_10GbE_miso          : t_mem_miso;
  signal ram_diag_bg_10GbE_mosi          : t_mem_mosi;
  signal ram_diag_bg_10GbE_miso          : t_mem_miso;
  signal reg_diag_tx_seq_10GbE_mosi      : t_mem_mosi;
  signal reg_diag_tx_seq_10GbE_miso      : t_mem_miso;

  signal reg_bsn_monitor_10GbE_mosi      : t_mem_mosi;
  signal reg_bsn_monitor_10GbE_miso      : t_mem_miso;

  signal ram_diag_data_buf_10GbE_mosi    : t_mem_mosi;
  signal ram_diag_data_buf_10GbE_miso    : t_mem_miso;
  signal reg_diag_data_buf_10GbE_mosi    : t_mem_mosi;
  signal reg_diag_data_buf_10GbE_miso    : t_mem_miso;
  signal reg_diag_rx_seq_10GbE_mosi      : t_mem_mosi;
  signal reg_diag_rx_seq_10GbE_miso      : t_mem_miso;

  signal dp_offload_tx_10GbE_src_out_arr : t_dp_sosi_arr(c_nof_streams_10GbE-1 downto 0);
  signal dp_offload_tx_10GbE_src_in_arr  : t_dp_siso_arr(c_nof_streams_10GbE-1 downto 0);
  signal dp_offload_rx_10GbE_snk_in_arr  : t_dp_sosi_arr(c_nof_streams_10GbE-1 downto 0);
  signal dp_offload_rx_10GbE_snk_out_arr : t_dp_siso_arr(c_nof_streams_10GbE-1 downto 0);

  -- DDR4 MB_I and MB_II
  signal dbg_c_ddr_ctlr_data_w             : natural := c_ddr_ctlr_data_w;
  signal dbg_c_ddr_dp_data_w               : natural := c_ddr_dp_data_w;
  signal dbg_c_ddr_dp_seq_dat_w            : natural := c_ddr_dp_seq_dat_w;
  signal dbg_c_ddr_dp_wr_fifo_depth        : natural := c_ddr_dp_wr_fifo_depth;
  signal dbg_c_ddr_dp_rd_fifo_depth        : natural := c_ddr_dp_rd_fifo_depth;
  signal dbg_c_ddr_db_buf_nof_data         : natural := c_ddr_db_buf_nof_data;
  signal dbg_c_ddr_mixed_width_ratio       : natural := c_ddr_mixed_width_ratio;

  signal reg_io_ddr_MB_I_mosi              : t_mem_mosi;
  signal reg_io_ddr_MB_I_miso              : t_mem_miso;
  signal reg_diag_tx_seq_ddr_MB_I_mosi     : t_mem_mosi;
  signal reg_diag_tx_seq_ddr_MB_I_miso     : t_mem_miso;
  signal reg_diag_rx_seq_ddr_MB_I_mosi     : t_mem_mosi;
  signal reg_diag_rx_seq_ddr_MB_I_miso     : t_mem_miso;
  signal reg_diag_data_buf_ddr_MB_I_mosi   : t_mem_mosi;
  signal reg_diag_data_buf_ddr_MB_I_miso   : t_mem_miso;
  signal ram_diag_data_buf_ddr_MB_I_mosi   : t_mem_mosi;
  signal ram_diag_data_buf_ddr_MB_I_miso   : t_mem_miso;

  signal reg_io_ddr_MB_II_mosi             : t_mem_mosi;
  signal reg_io_ddr_MB_II_miso             : t_mem_miso;
  signal reg_diag_tx_seq_ddr_MB_II_mosi    : t_mem_mosi;
  signal reg_diag_tx_seq_ddr_MB_II_miso    : t_mem_miso;
  signal reg_diag_rx_seq_ddr_MB_II_mosi    : t_mem_mosi;
  signal reg_diag_rx_seq_ddr_MB_II_miso    : t_mem_miso;
  signal reg_diag_data_buf_ddr_MB_II_mosi  : t_mem_mosi;
  signal reg_diag_data_buf_ddr_MB_II_miso  : t_mem_miso;
  signal ram_diag_data_buf_ddr_MB_II_mosi  : t_mem_mosi;
  signal ram_diag_data_buf_ddr_MB_II_miso  : t_mem_miso;

  -- DDR calibration_ok signals are set to '0' by default such that the corresponding
  -- LED is turned OFF when no IP is instantiated. This is prefered over turning it on
  -- as that would indicate a (false) correct calibration.
  signal ddr_I_cal_ok                      : std_logic := '0';
  signal ddr_II_cal_ok                     : std_logic := '0';

  -- UDP streaming ports for 1GbE I and 1GbE II
  -- . eth_0 = 1GbE I
  signal gn_eth_src_mac_I          : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal gn_ip_src_addr_I          : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal gn_udp_src_port_I         : std_logic_vector(c_network_udp_port_w - 1 downto 0);

  signal eth_0_udp_tx_sosi_arr     : t_dp_sosi_arr(c_nof_udp_streams_eth_0 - 1 downto 0);
  signal eth_0_udp_tx_siso_arr     : t_dp_siso_arr(c_nof_udp_streams_eth_0 - 1 downto 0);
  signal eth_0_udp_rx_sosi_arr     : t_dp_sosi_arr(c_nof_udp_streams_eth_0 - 1 downto 0);
  signal eth_0_udp_rx_siso_arr     : t_dp_siso_arr(c_nof_udp_streams_eth_0 - 1 downto 0) := (others => c_dp_siso_rdy);

  -- . eth_1 = 1GbE II
  signal gn_eth_src_mac_II         : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal gn_ip_src_addr_II         : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal gn_udp_src_port_II        : std_logic_vector(c_network_udp_port_w - 1 downto 0);

  signal eth_1_udp_tx_sosi_arr     : t_dp_sosi_arr(c_nof_udp_streams_eth_1 - 1 downto 0);
  signal eth_1_udp_tx_siso_arr     : t_dp_siso_arr(c_nof_udp_streams_eth_1 - 1 downto 0);
  signal eth_1_udp_rx_sosi_arr     : t_dp_sosi_arr(c_nof_udp_streams_eth_1 - 1 downto 0);
  signal eth_1_udp_rx_siso_arr     : t_dp_siso_arr(c_nof_udp_streams_eth_1 - 1 downto 0) := (others => c_dp_siso_rdy);

  -- QSFP leds
  signal qsfp_green_led_arr              : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr                : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
begin
  assert false
    report "g_design_name = " & g_design_name
    severity NOTE;

  gn_index <= TO_UINT(ID);

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2c_board_lib.ctrl_unb2c_board
  generic map (
    g_sim                     => g_sim,
    g_technology              => g_technology,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2c_board_eth_clk_freq_125M,
    g_aux                     => c_unb2c_board_aux,
    g_base_ip                 => c_base_ip,  -- = X"0A63" is base IP address used by unb_osy: 10.99.xx.yy
    g_udp_offload             => c_use_eth_0_UDP,
    g_udp_offload_nof_streams => c_nof_udp_streams_eth_0,
    g_factory_image           => g_factory_image,
    g_protect_addr_range      => g_protect_addr_range
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,

    ext_clk200               => ext_clk200,
    ext_rst200               => ext_rst200,

    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    mb_I_ref_rst             => mb_I_ref_rst,
    mb_II_ref_rst            => mb_II_ref_rst,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g ch0
    eth1g_mm_rst             => eth_0_mm_rst,
    eth1g_tse_mosi           => eth_0_tse_mosi,
    eth1g_tse_miso           => eth_0_tse_miso,
    eth1g_reg_mosi           => eth_0_reg_mosi,
    eth1g_reg_miso           => eth_0_reg_miso,
    eth1g_reg_interrupt      => eth_0_reg_interrupt,
    eth1g_ram_mosi           => eth_0_ram_mosi,
    eth1g_ram_miso           => eth_0_ram_miso,

    -- eth1g UDP streaming ports
    udp_tx_sosi_arr          =>  eth_0_udp_tx_sosi_arr,
    udp_tx_siso_arr          =>  eth_0_udp_tx_siso_arr,
    udp_rx_sosi_arr          =>  eth_0_udp_rx_sosi_arr,
    udp_rx_siso_arr          =>  eth_0_udp_rx_siso_arr,

    -- scrap ram
    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,

    -- . DDR reference clock domains reset creation
    MB_I_REF_CLK             => MB_I_REF_CLK,
    MB_II_REF_CLK            => MB_II_REF_CLK,
    -- . 1GbE Control Interface
    ETH_CLK                  => ETH_CLK(0),
    ETH_SGIN                 => ETH_SGIN(0),
    ETH_SGOUT                => ETH_SGOUT(0)
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb2c_test
  generic map (
    g_sim               => g_sim,
    g_sim_unb_nr        => g_sim_unb_nr,
    g_sim_node_nr       => g_sim_node_nr,
    g_technology        => g_technology,
    g_bg_block_size     => c_bg_block_size,
    g_hdr_field_arr     => c_hdr_field_arr,
    g_nof_streams_qsfp  => c_unb2c_board_tr_qsfp.nof_bus * c_unb2c_board_tr_qsfp.bus_w,
    g_nof_streams_ring  => c_unb2c_board_tr_ring.nof_bus * c_unb2c_board_tr_ring.bus_w,
    g_nof_streams_back0 => c_unb2c_board_tr_back.bus_w
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    -- PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g ch0
    eth_0_mm_rst        => eth_0_mm_rst,
    eth_0_tse_mosi      => eth_0_tse_mosi,
    eth_0_tse_miso      => eth_0_tse_miso,
    eth_0_reg_mosi      => eth_0_reg_mosi,
    eth_0_reg_miso      => eth_0_reg_miso,
    eth_0_reg_interrupt => eth_0_reg_interrupt,
    eth_0_ram_mosi      => eth_0_ram_mosi,
    eth_0_ram_miso      => eth_0_ram_miso,

    reg_diag_bg_eth_0_copi                => reg_diag_bg_eth_0_copi,
    reg_diag_bg_eth_0_cipo                => reg_diag_bg_eth_0_cipo,
    reg_hdr_dat_eth_0_copi                => reg_hdr_dat_eth_0_copi,
    reg_hdr_dat_eth_0_cipo                => reg_hdr_dat_eth_0_cipo,
    reg_bsn_monitor_v2_tx_eth_0_copi      => reg_bsn_monitor_v2_tx_eth_0_copi,
    reg_bsn_monitor_v2_tx_eth_0_cipo      => reg_bsn_monitor_v2_tx_eth_0_cipo,
    reg_strobe_total_count_tx_eth_0_copi  => reg_strobe_total_count_tx_eth_0_copi,
    reg_strobe_total_count_tx_eth_0_cipo  => reg_strobe_total_count_tx_eth_0_cipo,

    reg_bsn_monitor_v2_rx_eth_0_copi      => reg_bsn_monitor_v2_rx_eth_0_copi,
    reg_bsn_monitor_v2_rx_eth_0_cipo      => reg_bsn_monitor_v2_rx_eth_0_cipo,
    reg_strobe_total_count_rx_eth_0_copi  => reg_strobe_total_count_rx_eth_0_copi,
    reg_strobe_total_count_rx_eth_0_cipo  => reg_strobe_total_count_rx_eth_0_cipo,

    -- eth1g ch1
    eth_1_mm_rst        => eth_1_mm_rst,
    eth_1_tse_mosi      => eth_1_tse_mosi,
    eth_1_tse_miso      => eth_1_tse_miso,
    eth_1_reg_mosi      => OPEN,
    eth_1_reg_miso      => c_mem_cipo_rst,
    eth_1_reg_interrupt => '0',
    eth_1_ram_mosi      => OPEN,
    eth_1_ram_miso      => c_mem_cipo_rst,

    reg_diag_bg_eth_1_copi                => reg_diag_bg_eth_1_copi,
    reg_diag_bg_eth_1_cipo                => reg_diag_bg_eth_1_cipo,
    reg_hdr_dat_eth_1_copi                => reg_hdr_dat_eth_1_copi,
    reg_hdr_dat_eth_1_cipo                => reg_hdr_dat_eth_1_cipo,
    reg_bsn_monitor_v2_tx_eth_1_copi      => reg_bsn_monitor_v2_tx_eth_1_copi,
    reg_bsn_monitor_v2_tx_eth_1_cipo      => reg_bsn_monitor_v2_tx_eth_1_cipo,
    reg_strobe_total_count_tx_eth_1_copi  => reg_strobe_total_count_tx_eth_1_copi,
    reg_strobe_total_count_tx_eth_1_cipo  => reg_strobe_total_count_tx_eth_1_cipo,

    reg_bsn_monitor_v2_rx_eth_1_copi      => reg_bsn_monitor_v2_rx_eth_1_copi,
    reg_bsn_monitor_v2_rx_eth_1_cipo      => reg_bsn_monitor_v2_rx_eth_1_cipo,
    reg_strobe_total_count_rx_eth_1_copi  => reg_strobe_total_count_rx_eth_1_copi,
    reg_strobe_total_count_rx_eth_1_cipo  => reg_strobe_total_count_rx_eth_1_cipo,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- Remote Update
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- heater:
    reg_heater_mosi          => reg_heater_mosi,
    reg_heater_miso          => reg_heater_miso,

    -- block gen
    ram_diag_bg_10GbE_mosi         => ram_diag_bg_10GbE_mosi,
    ram_diag_bg_10GbE_miso         => ram_diag_bg_10GbE_miso,
    reg_diag_bg_10GbE_mosi         => reg_diag_bg_10GbE_mosi,
    reg_diag_bg_10GbE_miso         => reg_diag_bg_10GbE_miso,
    reg_diag_tx_seq_10GbE_mosi     => reg_diag_tx_seq_10GbE_mosi,
    reg_diag_tx_seq_10GbE_miso     => reg_diag_tx_seq_10GbE_miso,

    -- bsn
    reg_bsn_monitor_10GbE_mosi     => reg_bsn_monitor_10GbE_mosi,
    reg_bsn_monitor_10GbE_miso     => reg_bsn_monitor_10GbE_miso,

    -- databuffer
    ram_diag_data_buf_10GbE_mosi   => ram_diag_data_buf_10GbE_mosi,
    ram_diag_data_buf_10GbE_miso   => ram_diag_data_buf_10GbE_miso,
    reg_diag_data_buf_10GbE_mosi   => reg_diag_data_buf_10GbE_mosi,
    reg_diag_data_buf_10GbE_miso   => reg_diag_data_buf_10GbE_miso,
    reg_diag_rx_seq_10GbE_mosi     => reg_diag_rx_seq_10GbE_mosi,
    reg_diag_rx_seq_10GbE_miso     => reg_diag_rx_seq_10GbE_miso,

    -- 10GbE

    --reg_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
    --reg_10gbase_r_24_miso => reg_10gbase_r_24_miso,

    reg_tr_10GbE_qsfp_ring_mosi    => reg_tr_10GbE_qsfp_ring_mosi,
    reg_tr_10GbE_qsfp_ring_miso    => reg_tr_10GbE_qsfp_ring_miso,

    reg_tr_10GbE_back0_mosi        => reg_tr_10GbE_back0_mosi,
    reg_tr_10GbE_back0_miso        => reg_tr_10GbE_back0_miso,

    -- eth10g status
    reg_eth10g_qsfp_ring_mosi      => reg_eth10g_qsfp_ring_mosi,
    reg_eth10g_qsfp_ring_miso      => reg_eth10g_qsfp_ring_miso,

    reg_eth10g_back0_mosi          => reg_eth10g_back0_mosi,
    reg_eth10g_back0_miso          => reg_eth10g_back0_miso,

    -- DDR4 : MB I
    reg_io_ddr_MB_I_mosi              => reg_io_ddr_MB_I_mosi,
    reg_io_ddr_MB_I_miso              => reg_io_ddr_MB_I_miso,
    reg_diag_tx_seq_ddr_MB_I_mosi     => reg_diag_tx_seq_ddr_MB_I_mosi,
    reg_diag_tx_seq_ddr_MB_I_miso     => reg_diag_tx_seq_ddr_MB_I_miso,
    reg_diag_rx_seq_ddr_MB_I_mosi     => reg_diag_rx_seq_ddr_MB_I_mosi,
    reg_diag_rx_seq_ddr_MB_I_miso     => reg_diag_rx_seq_ddr_MB_I_miso,
    reg_diag_data_buf_ddr_MB_I_mosi   => reg_diag_data_buf_ddr_MB_I_mosi,
    reg_diag_data_buf_ddr_MB_I_miso   => reg_diag_data_buf_ddr_MB_I_miso,
    ram_diag_data_buf_ddr_MB_I_mosi   => ram_diag_data_buf_ddr_MB_I_mosi,
    ram_diag_data_buf_ddr_MB_I_miso   => ram_diag_data_buf_ddr_MB_I_miso,

    -- DDR4 : MB II
    reg_io_ddr_MB_II_mosi             => reg_io_ddr_MB_II_mosi,
    reg_io_ddr_MB_II_miso             => reg_io_ddr_MB_II_miso,
    reg_diag_tx_seq_ddr_MB_II_mosi    => reg_diag_tx_seq_ddr_MB_II_mosi,
    reg_diag_tx_seq_ddr_MB_II_miso    => reg_diag_tx_seq_ddr_MB_II_miso,
    reg_diag_rx_seq_ddr_MB_II_mosi    => reg_diag_rx_seq_ddr_MB_II_mosi,
    reg_diag_rx_seq_ddr_MB_II_miso    => reg_diag_rx_seq_ddr_MB_II_miso,
    reg_diag_data_buf_ddr_MB_II_mosi  => reg_diag_data_buf_ddr_MB_II_mosi,
    reg_diag_data_buf_ddr_MB_II_miso  => reg_diag_data_buf_ddr_MB_II_miso,
    ram_diag_data_buf_ddr_MB_II_mosi  => ram_diag_data_buf_ddr_MB_II_mosi,
    ram_diag_data_buf_ddr_MB_II_miso  => ram_diag_data_buf_ddr_MB_II_miso,

    -- Jesd reset control
    jesd_ctrl_mosi            => jesd_ctrl_mosi,
    jesd_ctrl_miso            => jesd_ctrl_miso,

    -- Jesd ip status/control
    jesd204b_mosi               => jesd204b_mosi,
    jesd204b_miso               => jesd204b_miso,
    reg_bsn_source_mosi         => reg_bsn_source_mosi,
    reg_bsn_source_miso         => reg_bsn_source_miso,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,

    -- Scrap RAM
    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso
  );

  gen_eth_0_udp : if c_use_eth_0_UDP = true generate
    -- Derive MAC/IP/UDP from gn_index
    gn_eth_src_mac_I     <= c_base_mac & func_eth_tester_gn_index_to_mac_15_0(gn_index, 0);
    gn_ip_src_addr_I     <= c_base_ip & func_eth_tester_gn_index_to_ip_15_0(gn_index, 0);
    gn_udp_src_port_I    <= c_base_udp & func_eth_tester_gn_index_to_udp_7_0(gn_index, 0);

    -- Generate UDP Tx and monitor UDP Rx
    u_eth_tester_I : entity eth_lib.eth_tester
    generic map (
      g_nof_streams     => c_nof_udp_streams_eth_0,
      g_bg_sync_timeout => c_eth_tester_sync_timeout,  -- BG sync interval < 11 s
      g_remove_crc      => true  -- use TRUE when using TSE link interface
    )
    port map (
      -- Clocks and reset
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      st_rst             => dp_rst,
      st_clk             => dp_clk,
      st_pps             => dp_pps,

      -- UDP transmit interface
      eth_src_mac        => gn_eth_src_mac_I,
      ip_src_addr        => gn_ip_src_addr_I,
      udp_src_port       => gn_udp_src_port_I,

      tx_fifo_rd_emp_arr => OPEN,

      tx_udp_sosi_arr    => eth_0_udp_tx_sosi_arr,
      tx_udp_siso_arr    => eth_0_udp_tx_siso_arr,

      -- UDP receive interface
      rx_udp_sosi_arr    => eth_0_udp_rx_sosi_arr,

      -- Memory Mapped Slaves (one per stream)
      -- . Tx
      reg_bg_ctrl_copi               => reg_diag_bg_eth_0_copi,
      reg_bg_ctrl_cipo               => reg_diag_bg_eth_0_cipo,
      reg_hdr_dat_copi               => reg_hdr_dat_eth_0_copi,
      reg_hdr_dat_cipo               => reg_hdr_dat_eth_0_cipo,
      reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_eth_0_copi,
      reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_eth_0_cipo,
      reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_eth_0_copi,
      reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_eth_0_cipo,
      -- . Rx
      reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_eth_0_copi,
      reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_eth_0_cipo,
      reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_eth_0_copi,
      reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_eth_0_cipo
    );

    -- Uses eth.vhd with ETH/TSE interface with UDP streams in ctrl_unb2c_board
    -- to stream UDP data via eth_0 = 1GbE-I.
  end generate;

  -- Instantiate a second 1GbE-II to check pinning and to test UDP data via a
  -- dedicated 1GbE port, instead of multiplexed with M&C
  gen_eth_1: if c_use_eth_1 = true generate
    -- Derive eth_1 MAC/IP/UDP from eth_0
    gn_eth_src_mac_II    <= c_base_mac & func_eth_tester_gn_index_to_mac_15_0(gn_index, 1);
    gn_ip_src_addr_II    <= c_base_ip & func_eth_tester_gn_index_to_ip_15_0(gn_index, 1);
    gn_udp_src_port_II   <= c_base_udp & func_eth_tester_gn_index_to_udp_7_0(gn_index, 1);

    -- Generate UDP Tx and monitor UDP Rx
    u_eth_tester_II : entity eth_lib.eth_tester
    generic map (
      g_nof_streams     => c_nof_udp_streams_eth_1,
      g_bg_sync_timeout => c_eth_tester_sync_timeout,  -- BG sync interval < 11 s
      g_remove_crc      => true  -- use TRUE when using TSE link interface
    )
    port map (
      -- Clocks and reset
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      st_rst             => dp_rst,
      st_clk             => dp_clk,
      st_pps             => dp_pps,

      -- UDP transmit interface
      eth_src_mac        => gn_eth_src_mac_II,
      ip_src_addr        => gn_ip_src_addr_II,
      udp_src_port       => gn_udp_src_port_II,

      tx_fifo_rd_emp_arr => OPEN,

      tx_udp_sosi_arr    => eth_1_udp_tx_sosi_arr,
      tx_udp_siso_arr    => eth_1_udp_tx_siso_arr,

      -- UDP receive interface
      rx_udp_sosi_arr    => eth_1_udp_rx_sosi_arr,

      -- Memory Mapped Slaves (one per stream)
      -- . Tx
      reg_bg_ctrl_copi               => reg_diag_bg_eth_1_copi,
      reg_bg_ctrl_cipo               => reg_diag_bg_eth_1_cipo,
      reg_hdr_dat_copi               => reg_hdr_dat_eth_1_copi,
      reg_hdr_dat_cipo               => reg_hdr_dat_eth_1_cipo,
      reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_eth_1_copi,
      reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_eth_1_cipo,
      reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_eth_1_copi,
      reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_eth_1_cipo,
      -- . Rx
      reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_eth_1_copi,
      reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_eth_1_cipo,
      reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_eth_1_copi,
      reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_eth_1_cipo
    );

    -- Use eth_stream with ETH/TSE interface for UDP port g_rx_udp_port to
    -- stream UDP data via eth_1 = 1GbE-II
    u_eth_stream : entity eth_lib.eth_stream
    generic map (
      g_technology   => g_technology,
      g_rx_udp_port  => TO_UINT(c_eth_rx_udp_port),  -- = 0x1771 = 6001
      g_jumbo_en     => true,
      g_sim          => g_sim,
      g_sim_level    => 1  -- 0 = use IP model (equivalent to g_sim = FALSE); 1 = use fast serdes model;
    )
    port map (
      -- Clocks and reset
      mm_rst             => mm_rst,  -- eth_1_mm_rst
      mm_clk             => mm_clk,
      eth_clk            => ETH_CLK(1),
      st_rst             => dp_rst,
      st_clk             => dp_clk,

      -- TSE setup
      src_mac            => gn_eth_src_mac_II,
      setup_done         => OPEN,

      -- UDP transmit interface
      udp_tx_snk_in      => eth_1_udp_tx_sosi_arr(0),
      udp_tx_snk_out     => eth_1_udp_tx_siso_arr(0),

      -- UDP receive interface
      udp_rx_src_in      => c_dp_siso_rdy,
      udp_rx_src_out     => eth_1_udp_rx_sosi_arr(0),

      -- Memory Mapped Slaves
      tse_ctlr_copi      => eth_1_tse_mosi,
      tse_ctlr_cipo      => eth_1_tse_miso,

      -- PHY interface
      eth_txp            => ETH_SGOUT(1),
      eth_rxp            => ETH_SGIN(1),

      -- LED interface
      tse_led            => open
    );
  end generate;

  gen_udp_stream_10GbE : if c_use_10GbE = true and c_use_loopback = false generate
    u_udp_stream_10GbE : entity work.udp_stream
    generic map (
      g_sim                       => g_sim,
      g_technology                => g_technology,
      g_nof_streams               => c_nof_streams_qsfp + c_nof_streams_ring + c_nof_streams_back0,
      g_data_w                    => c_data_w_64,
      g_bg_block_size             => c_bg_block_size,
      g_bg_gapsize                => c_bg_gapsize_10GbE,
      g_bg_blocks_per_sync        => c_bg_blocks_per_sync,
      g_def_block_size            => c_def_10GbE_block_size,
      g_max_nof_blocks_per_packet => c_max_nof_blocks_per_packet_10GbE,
      g_remove_crc                => false
    )
    port map (
      mm_rst                         => mm_rst,
      mm_clk                         => mm_clk,
      dp_rst                         => dp_rst,
      dp_clk                         => dp_clk,
      ID                             => ID,
      -- blockgen MM
      reg_diag_bg_mosi               => reg_diag_bg_10GbE_mosi,
      reg_diag_bg_miso               => reg_diag_bg_10GbE_miso,
      ram_diag_bg_mosi               => ram_diag_bg_10GbE_mosi,
      ram_diag_bg_miso               => ram_diag_bg_10GbE_miso,
      reg_diag_tx_seq_mosi           => reg_diag_tx_seq_10GbE_mosi,
      reg_diag_tx_seq_miso           => reg_diag_tx_seq_10GbE_miso,

      -- loopback:
      --dp_offload_tx_src_out_arr      => dp_offload_tx_10GbE_src_out_arr,
      --dp_offload_tx_src_in_arr       => (OTHERS=>c_dp_siso_rdy),
      --dp_offload_rx_snk_in_arr       => dp_offload_tx_10GbE_src_out_arr,
      --dp_offload_rx_snk_out_arr      => dp_offload_tx_10GbE_src_in_arr,

      -- connect to dp_offload:
      dp_offload_tx_src_out_arr      => dp_offload_tx_10GbE_src_out_arr,
      dp_offload_tx_src_in_arr       => dp_offload_tx_10GbE_src_in_arr,
      dp_offload_rx_snk_in_arr       => dp_offload_rx_10GbE_snk_in_arr,
      dp_offload_rx_snk_out_arr      => dp_offload_rx_10GbE_snk_out_arr,

      reg_bsn_monitor_mosi           => reg_bsn_monitor_10GbE_mosi,
      reg_bsn_monitor_miso           => reg_bsn_monitor_10GbE_miso,

      reg_diag_data_buf_mosi         => reg_diag_data_buf_10GbE_mosi,
      reg_diag_data_buf_miso         => reg_diag_data_buf_10GbE_miso,
      ram_diag_data_buf_mosi         => ram_diag_data_buf_10GbE_mosi,
      ram_diag_data_buf_miso         => ram_diag_data_buf_10GbE_miso,
      reg_diag_rx_seq_mosi           => reg_diag_rx_seq_10GbE_mosi,
      reg_diag_rx_seq_miso           => reg_diag_rx_seq_10GbE_miso
    );
  end generate;

  gen_jesd204b : if c_use_jesd204b = true generate
    u_jesd204b: entity work.node_adc_input_and_timing_nowg
    generic map(
      g_technology                => g_technology,
      g_nof_streams               => c_nof_streams_jesd204b,
      g_jesd_freq                 => "200MHz",
      g_nof_sync_n                => c_unb2c_board_nof_sync_jesd204b,
      g_sim                       => g_sim
    )
    port map(
      -- clocks and resets
      mm_clk                      => mm_clk,
      mm_rst                      => mm_rst,
      dp_clk                      => dp_clk,
      dp_rst                      => dp_rst,

      -- mm control buses
      jesd204b_mosi               => jesd204b_mosi,
      jesd204b_miso               => jesd204b_miso,
      reg_bsn_source_mosi         => reg_bsn_source_mosi,
      reg_bsn_source_miso         => reg_bsn_source_miso,
      reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
      reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
      ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
      ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
      reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
      reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,
      jesd_ctrl_mosi              => jesd_ctrl_mosi,
      jesd_ctrl_miso              => jesd_ctrl_miso,

       -- Jesd external IOs
      jesd204b_serial_data       => BCK_RX(c_nof_streams_jesd204b - 1 downto 0),
      jesd204b_refclk            => BCK_REF_CLK,
      jesd204b_sysref            => JESD204B_SYSREF,
      jesd204b_sync_n            => JESD204B_SYNC
    );
  end generate;

  gen_front_10GbE : if c_use_10GbE = true generate
    u_tr_10GbE_qsfp_and_ring: entity unb2c_board_10gbe_lib.unb2c_board_10gbe  -- QSFP and Ring lines
    generic map (
      g_sim           => g_sim,
      g_sim_level     => 1,
      g_technology    => g_technology,
      g_use_loopback  => c_use_loopback,
      g_nof_macs      => c_nof_streams_qsfp + c_nof_streams_ring,
      g_tx_fifo_fill  => c_def_10GbE_block_size,
      g_tx_fifo_size  => c_def_10GbE_block_size * 2
    )
    port map (
      tr_ref_clk          => SA_CLK,
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      reg_mac_mosi        => reg_tr_10GbE_qsfp_ring_mosi,
      reg_mac_miso        => reg_tr_10GbE_qsfp_ring_miso,
      reg_eth10g_mosi     => reg_eth10g_qsfp_ring_mosi,
      reg_eth10g_miso     => reg_eth10g_qsfp_ring_miso,
      --reg_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
      --reg_10gbase_r_24_miso => reg_10gbase_r_24_miso,

      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      src_out_arr         => dp_offload_rx_10GbE_snk_in_arr(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0),
      src_in_arr          => dp_offload_rx_10GbE_snk_out_arr(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0),
      snk_out_arr         => dp_offload_tx_10GbE_src_in_arr(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0),
      snk_in_arr          => dp_offload_tx_10GbE_src_out_arr(c_nof_streams_qsfp + c_nof_streams_ring - 1 downto 0),

      serial_tx_arr       => i_serial_10G_tx_qsfp_ring_arr,
      serial_rx_arr       => i_serial_10G_rx_qsfp_ring_arr
    );

    gen_qsfp_wires: for i in 0 to c_nof_streams_qsfp - 1 generate
        serial_10G_tx_qsfp_arr(i)      <= i_serial_10G_tx_qsfp_ring_arr(i);
      i_serial_10G_rx_qsfp_ring_arr(i) <=   serial_10G_rx_qsfp_arr(i);
    end generate;

    i_QSFP_RX(0) <= QSFP_0_RX;
    i_QSFP_RX(1) <= QSFP_1_RX;
    i_QSFP_RX(2) <= QSFP_2_RX;
    i_QSFP_RX(3) <= QSFP_3_RX;
    i_QSFP_RX(4) <= QSFP_4_RX;
    i_QSFP_RX(5) <= QSFP_5_RX;

    QSFP_0_TX <= i_QSFP_TX(0);
    QSFP_1_TX <= i_QSFP_TX(1);
    QSFP_2_TX <= i_QSFP_TX(2);
    QSFP_3_TX <= i_QSFP_TX(3);
    QSFP_4_TX <= i_QSFP_TX(4);
    QSFP_5_TX <= i_QSFP_TX(5);

    u_front_io : entity unb2c_board_lib.unb2c_board_front_io
    generic map (
      g_nof_qsfp_bus => c_nof_qsfp_bus
    )
    port map (
      serial_tx_arr => serial_10G_tx_qsfp_arr,
      serial_rx_arr => serial_10G_rx_qsfp_arr,

      green_led_arr => qsfp_green_led_arr(c_nof_qsfp_bus - 1 downto 0),
      red_led_arr   => qsfp_red_led_arr(c_nof_qsfp_bus - 1 downto 0),

      QSFP_RX    => i_QSFP_RX,
      QSFP_TX    => i_QSFP_TX,

      QSFP_LED   => QSFP_LED
    );

    gen_ring_wires: for i in 0 to c_nof_streams_ring - 1 generate
        serial_10G_tx_ring_arr(i) <= i_serial_10G_tx_qsfp_ring_arr(i + c_nof_streams_qsfp);
      i_serial_10G_rx_qsfp_ring_arr(i + c_nof_streams_qsfp) <= serial_10G_rx_ring_arr(i);
    end generate;

    i_RING_RX(0) <= RING_0_RX;
    i_RING_RX(1) <= RING_1_RX;
    RING_0_TX <= i_RING_TX(0);
    RING_1_TX <= i_RING_TX(1);

    u_ring_io : entity unb2c_board_lib.unb2c_board_ring_io
    generic map (
      g_nof_ring_bus => 2  -- c_nof_ring_bus
    )
    port map (
      serial_tx_arr => serial_10G_tx_ring_arr,
      serial_rx_arr => serial_10G_rx_ring_arr,
      RING_RX => i_RING_RX,
      RING_TX => i_RING_TX
    );

    gen_10gbe_back0 : if c_use_10GbE_back0 = true generate
      u_tr_10GbE_back: entity unb2c_board_10gbe_lib.unb2c_board_10gbe  -- BACK lines (upper)
      generic map (
        g_sim           => g_sim,
        g_sim_level     => 1,
        g_technology    => g_technology,
        g_use_loopback  => c_use_loopback,
        g_nof_macs      => c_nof_streams_back0,
        g_tx_fifo_fill  => c_def_10GbE_block_size,
        g_tx_fifo_size  => c_def_10GbE_block_size * 2
      )
      port map (
        tr_ref_clk          => SB_CLK,
        mm_rst              => mm_rst,
        mm_clk              => mm_clk,
        reg_mac_mosi        => reg_tr_10GbE_back0_mosi,
        reg_mac_miso        => reg_tr_10GbE_back0_miso,
        reg_eth10g_mosi     => reg_eth10g_back0_mosi,
        reg_eth10g_miso     => reg_eth10g_back0_miso,
        dp_rst              => dp_rst,
        dp_clk              => dp_clk,

        src_out_arr         => dp_offload_rx_10GbE_snk_in_arr(c_nof_streams_back0 + c_nof_streams_qsfp + c_nof_streams_ring - 1 downto c_nof_streams_qsfp + c_nof_streams_ring),
        src_in_arr          => dp_offload_rx_10GbE_snk_out_arr(c_nof_streams_back0 + c_nof_streams_qsfp + c_nof_streams_ring - 1 downto c_nof_streams_qsfp + c_nof_streams_ring),
        snk_out_arr         => dp_offload_tx_10GbE_src_in_arr(c_nof_streams_back0 + c_nof_streams_qsfp + c_nof_streams_ring - 1 downto c_nof_streams_qsfp + c_nof_streams_ring),
        snk_in_arr          => dp_offload_tx_10GbE_src_out_arr(c_nof_streams_back0 + c_nof_streams_qsfp + c_nof_streams_ring - 1 downto c_nof_streams_qsfp + c_nof_streams_ring),
        serial_tx_arr       => i_serial_10G_tx_back0_arr,
        serial_rx_arr       => i_serial_10G_rx_back0_arr
      );
    end generate;

    gen_back_wiring : if c_use_10GbE_back0 = true generate
      gen_back0_wires: for i in 0 to c_nof_streams_back0 - 1 generate
        serial_10G_tx_back_arr(i)  <= i_serial_10G_tx_back0_arr(i);
        i_serial_10G_rx_back0_arr(i) <=   serial_10G_rx_back_arr(i);
      end generate;

      u_back_io : entity unb2c_board_lib.unb2c_board_back_io
      generic map (
        g_nof_back_bus => c_nof_back_bus
      )
      port map (
        serial_tx_arr => serial_10G_tx_back_arr,
        serial_rx_arr => serial_10G_rx_back_arr,

        -- Serial I/O
        -- back transceivers
        BCK_RX(0)  => BCK_RX(c_nof_streams_back0 - 1 downto 0),
        BCK_TX(0)  => BCK_TX(c_nof_streams_back0 - 1 downto 0)
      );
    end generate;

--    gen_jesd204b_wiring : IF c_use_jesd204b = TRUE GENERATE
--      gen_jesd204b_wires: FOR i IN 0 TO c_nof_streams_jesd204b-1 GENERATE
--        serial_rx_jesd204b_arr(i) <= serial_rx_jesd204b_back_arr(i);
--      END GENERATE;
--
--      u_back_io : ENTITY unb2c_board_lib.unb2c_board_back_io
--      GENERIC MAP (
--        g_nof_back_bus => 1
--      )
--      PORT MAP (
--        --serial_tx_arr => serial_10G_tx_back_arr,
--        serial_rx_arr => serial_rx_jesd204b_back_arr,
--
--        -- Serial I/O
--        -- back transceivers
--        BCK_RX(0)  => BCK_RX(c_nof_streams_jesd204b-1 downto 0),
--        BCK_TX(0)  => open
--      );
--    END GENERATE;

    u_front_led : entity unb2c_board_lib.unb2c_board_qsfp_leds
    generic map (
      g_sim             => g_sim,
      g_factory_image   => g_factory_image,
      g_nof_qsfp        => c_nof_qsfp_bus,
      g_pulse_us        => 1000 / (10**9 / c_unb2c_board_ext_clk_freq_200M)  -- nof clk cycles to get us period
    )
    port map (
      rst               => dp_rst,
      clk               => dp_clk,

      --tx_siso_arr       => dp_offload_tx_10GbE_src_in_arr(c_nof_streams_ring-1 DOWNTO 0),
      --tx_sosi_arr       => dp_offload_tx_10GbE_src_out_arr(c_nof_streams_ring-1 DOWNTO 0),
      --rx_sosi_arr       => dp_offload_rx_10GbE_snk_in_arr(c_nof_streams_ring-1 DOWNTO 0),

      --tx_siso_arr       => dp_offload_tx_10GbE_src_in_arr(c_nof_streams_back0+c_nof_streams_qsfp+c_nof_streams_ring-1 DOWNTO c_nof_streams_qsfp+c_nof_streams_ring),
      --tx_sosi_arr       => dp_offload_tx_10GbE_src_out_arr(c_nof_streams_back0+c_nof_streams_qsfp+c_nof_streams_ring-1 DOWNTO c_nof_streams_qsfp+c_nof_streams_ring),
      --rx_sosi_arr       => dp_offload_rx_10GbE_snk_in_arr(c_nof_streams_back0+c_nof_streams_qsfp+c_nof_streams_ring-1 DOWNTO c_nof_streams_qsfp+c_nof_streams_ring),

      tx_siso_arr       => dp_offload_tx_10GbE_src_in_arr(c_nof_streams_qsfp - 1 downto 0),
      tx_sosi_arr       => dp_offload_tx_10GbE_src_out_arr(c_nof_streams_qsfp - 1 downto 0),
      rx_sosi_arr       => dp_offload_rx_10GbE_snk_in_arr(c_nof_streams_qsfp - 1 downto 0),

      green_led_arr     => qsfp_green_led_arr(c_nof_qsfp_bus - 1 downto 0),
      red_led_arr       => qsfp_red_led_arr(c_nof_qsfp_bus - 1 downto 0)
    );
  end generate;

  gen_no_udp_stream_10GbE : if c_use_10GbE = false generate
    u_front_io : entity unb2c_board_lib.unb2c_board_front_io
    generic map (
      g_nof_qsfp_bus => c_unb2c_board_tr_qsfp.nof_bus
    )
    port map (
      green_led_arr => qsfp_green_led_arr,
      red_led_arr   => qsfp_red_led_arr,
      QSFP_LED      => QSFP_LED
    );

    u_front_led : entity unb2c_board_lib.unb2c_board_qsfp_leds_v2
    generic map (
      g_sim           => g_sim,
      g_factory_image => g_factory_image,
      g_nof_qsfp      => c_unb2c_board_tr_qsfp.nof_bus,
      g_mm_pulse_us   => 1000 / (10**9 / c_mm_clk_freq),
      g_dp_pulse_us   => 1000 / (10**9 / c_dp_clk_freq)
    )
    port map (
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      dp_rst             => dp_rst,
      dp_clk             => dp_clk,
      dp_pps             => dp_pps,
      ddr_I_cal_ok       => ddr_I_cal_ok,
      ddr_II_cal_ok      => ddr_II_cal_ok,
      green_led_arr      => qsfp_green_led_arr,
      red_led_arr        => qsfp_red_led_arr
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Interface : DDR4 MB_I and MB_II
  -----------------------------------------------------------------------------
  assert c_ddr_mixed_width_ratio > 0 and is_pow2(c_ddr_mixed_width_ratio)
    report "unb2c_test: DDR4 data widths are not an integer ratio"
    severity FAILURE;
  assert func_tech_ddr_ctlr_data_w(c_ddr_MB_I) = func_tech_ddr_ctlr_data_w(c_ddr_MB_II)
    report "unb2c_test: DDR4 MB_I and MB_II must have the same ctlr data widths"
    severity FAILURE;

  gen_stream_MB_I : if c_use_MB_I = true generate
    u_mms_io_ddr_diag : entity io_ddr_lib.mms_io_ddr_diag
    generic map (
      -- System
      g_sim_model_ddr    => g_sim_model_ddr,
      g_technology       => g_technology,

      g_dp_data_w        => c_ddr_dp_data_w,
      g_dp_seq_dat_w     => c_ddr_dp_seq_dat_w,
      g_dp_wr_fifo_depth => c_ddr_dp_wr_fifo_depth,
      g_dp_rd_fifo_depth => c_ddr_dp_rd_fifo_depth,

      -- IO_DDR
      g_io_tech_ddr      => c_ddr_MB_I,

      -- DIAG data buffer
      g_db_use_db        => false,
      g_db_buf_nof_data  => c_ddr_db_buf_nof_data  -- nof words per data buffer
    )
    port map (
      ---------------------------------------------------------------------------
      -- System
      ---------------------------------------------------------------------------
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,  -- use alternative external clock or externally connect to ctlr_clk_out

      ---------------------------------------------------------------------------
      -- IO_DDR
      ---------------------------------------------------------------------------
      -- DDR reference clock
      ctlr_ref_clk        => MB_I_REF_CLK,
      ctlr_ref_rst        => mb_I_ref_rst,

      -- DDR controller clock domain
      ctlr_clk_out        => ddr_I_clk200,
      ctlr_rst_out        => ddr_I_rst200,

      ctlr_clk_in         => ddr_I_clk200,  -- connect ctlr_clk_out to ctlr_clk_in at top level to avoid potential delta-cycle differences between the same clock
      ctlr_rst_in         => ddr_I_rst200,  -- connect ctlr_rst_out to ctlr_rst_in at top level

      -- MM interface
      reg_io_ddr_mosi     => reg_io_ddr_MB_I_mosi,  -- register for DDR controller status info
      reg_io_ddr_miso     => reg_io_ddr_MB_I_miso,

      -- Write / read FIFO status for monitoring purposes (in dp_clk domain)
      wr_fifo_usedw       => OPEN,
      rd_fifo_usedw       => OPEN,

      -- DDR4 PHY external interface
      phy4_in             => MB_I_IN,
      phy4_io             => MB_I_IO,
      phy4_ou             => MB_I_OU,

      -- DDR4 Calibration result for LED indicator
      ddr_cal_ok          => ddr_I_cal_ok,

      ---------------------------------------------------------------------------
      -- DIAG Tx seq
      ---------------------------------------------------------------------------
      -- MM interface
      reg_tx_seq_mosi   => reg_diag_tx_seq_ddr_MB_I_mosi,
      reg_tx_seq_miso   => reg_diag_tx_seq_ddr_MB_I_miso,

      ---------------------------------------------------------------------------
      -- DIAG rx seq with optional data buffer
      ---------------------------------------------------------------------------
      -- MM interface
      reg_data_buf_mosi => reg_diag_data_buf_ddr_MB_I_mosi,
      reg_data_buf_miso => reg_diag_data_buf_ddr_MB_I_miso,

      ram_data_buf_mosi => ram_diag_data_buf_ddr_MB_I_mosi,
      ram_data_buf_miso => ram_diag_data_buf_ddr_MB_I_miso,

      reg_rx_seq_mosi   => reg_diag_rx_seq_ddr_MB_I_mosi,
      reg_rx_seq_miso   => reg_diag_rx_seq_ddr_MB_I_miso
    );
  end generate;

  gen_stream_MB_II : if c_use_MB_II = true generate
    u_mms_io_ddr_diag : entity io_ddr_lib.mms_io_ddr_diag
    generic map (
      -- System
      g_sim_model_ddr   => g_sim_model_ddr,
      g_technology      => g_technology,

      g_dp_data_w        => c_ddr_dp_data_w,
      g_dp_seq_dat_w     => c_ddr_dp_seq_dat_w,
      g_dp_wr_fifo_depth => c_ddr_dp_wr_fifo_depth,
      g_dp_rd_fifo_depth => c_ddr_dp_rd_fifo_depth,

      -- IO_DDR
      g_io_tech_ddr     => c_ddr_MB_II,

      -- DIAG data buffer
      g_db_use_db       => false,
      g_db_buf_nof_data => c_ddr_db_buf_nof_data  -- nof words per data buffer
    )
    port map (
      ---------------------------------------------------------------------------
      -- System
      ---------------------------------------------------------------------------
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,  -- use alternative external clock or externally connect to ctlr_clk_out

      ---------------------------------------------------------------------------
      -- IO_DDR
      ---------------------------------------------------------------------------
      -- DDR reference clock
      ctlr_ref_clk        => MB_II_REF_CLK,
      ctlr_ref_rst        => mb_II_ref_rst,

      -- DDR controller clock domain
      ctlr_clk_out        => ddr_II_clk200,
      ctlr_rst_out        => ddr_II_rst200,

      ctlr_clk_in         => ddr_II_clk200,  -- connect ctlr_clk_out to ctlr_clk_in at top level to avoid potential delta-cycle differences between the same clock
      ctlr_rst_in         => ddr_II_rst200,  -- connect ctlr_rst_out to ctlr_rst_in at top level

      -- MM interface
      reg_io_ddr_mosi     => reg_io_ddr_MB_II_mosi,  -- register for DDR controller status info
      reg_io_ddr_miso     => reg_io_ddr_MB_II_miso,

      -- Write / read FIFO status for monitoring purposes (in dp_clk domain)
      wr_fifo_usedw       => OPEN,
      rd_fifo_usedw       => OPEN,

      -- DDR4 PHY external interface
      phy4_in             => MB_II_IN,
      phy4_io             => MB_II_IO,
      phy4_ou             => MB_II_OU,

      -- DDR4 Calibration result for LED indicator
      ddr_cal_ok          => ddr_II_cal_ok,

      ---------------------------------------------------------------------------
      -- DIAG Tx seq
      ---------------------------------------------------------------------------
      -- MM interface
      reg_tx_seq_mosi   => reg_diag_tx_seq_ddr_MB_II_mosi,
      reg_tx_seq_miso   => reg_diag_tx_seq_ddr_MB_II_miso,

      ---------------------------------------------------------------------------
      -- DIAG rx seq with optional data buffer
      ---------------------------------------------------------------------------
      -- MM interface
      reg_data_buf_mosi => reg_diag_data_buf_ddr_MB_II_mosi,
      reg_data_buf_miso => reg_diag_data_buf_ddr_MB_II_miso,

      ram_data_buf_mosi => ram_diag_data_buf_ddr_MB_II_mosi,
      ram_data_buf_miso => ram_diag_data_buf_ddr_MB_II_miso,

      reg_rx_seq_mosi   => reg_diag_rx_seq_ddr_MB_II_mosi,
      reg_rx_seq_miso   => reg_diag_rx_seq_ddr_MB_II_miso
    );
  end generate;

  gen_heater : if c_use_heater = true generate
    u_heater : entity util_lib.util_heater
    generic map (
      g_technology  => g_technology,
      --g_nof_mac4   => 315 -- on Arria10 using  630 of 1518 DSP blocks
      --g_nof_mac4   => 630 --

      --g_nof_mac4   => 736, -- max 736, 23 registers * 32 *2 = 1472 of 1518 DSP blocks (97%)
      g_nof_mac4   => 750,

      g_pipeline   => 72,  -- max 72
      g_nof_ram    => 4,  -- max 4

      g_nof_logic  => 24  -- max 24
    )
    port map (
      mm_rst  => mm_rst,
      mm_clk  => mm_clk,

      st_rst  => dp_rst,
      st_clk  => dp_clk,

      sla_in  => reg_heater_mosi,
      sla_out => reg_heater_miso
    );
  end generate;
end str;
