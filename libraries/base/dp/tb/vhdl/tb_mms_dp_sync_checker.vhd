-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Test bench for dp_sync_checker
-- Description:
--   The tb verifies the DUT using the proc_dp_gen_block_data procedure to
--   generate stimuli. The data field of the output stream is verified.
-- Remark:
--   Both sample level and frame level flow control via src_in are not tested,
--   because is is not yet implemented.
-- Usage:
-- > as 6
-- > run -all
-- . signal tb_end will stop the simulation by stopping the clk
-- . the tb is self checking
--
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mms_dp_sync_checker is
  generic (
    -- general
    g_flow_control_stimuli : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_flow_control_verify  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- specific
    g_in_dat_w             : natural  := 32;
    g_in_nof_words         : natural  := 1;
    g_nof_repeat           : natural  := 100;
    g_pkt_len              : natural  := 16;  -- must be a multiple of g_in_nof_words
    g_pkt_gap              : natural  := 0;
    g_sync_period          : natural  := 17;  -- 15 for early syncs. 17 for late syncs. The sync period generated in the stimuli.
    -- DUT
    g_nof_blk_per_sync     : positive := 16  -- The sync period as expected by the sync_checker
  );
end tb_mms_dp_sync_checker;

architecture tb of tb_mms_dp_sync_checker is
  constant c_mm_clk_period      : time    := 30 ns;  -- 40 MHz
  constant c_rl                 : natural := 1;
  constant c_no_dut             : boolean := true;

  constant c_pulse_active       : natural := g_in_nof_words;
  constant c_pulse_period       : natural := g_in_nof_words;

  constant c_sync_offset        : natural := 7;

  constant c_gap                : natural := sel_a_b(g_sync_period >= g_nof_blk_per_sync, g_sync_period - g_nof_blk_per_sync, 2 * g_sync_period - g_nof_blk_per_sync);
  constant c_gapsize            : positive := c_gap * g_pkt_len + 1;

  constant c_data_max           : unsigned(g_in_dat_w - 1 downto 0) := to_unsigned(0, g_in_dat_w);  -- no wrap detection
  constant c_data_init          : integer := -1;
  constant c_bsn_init           : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init           : natural := 247;
  constant c_channel_init       : integer := 5;  -- fixed

  signal tb_end                 : std_logic := '0';
  signal clk                    : std_logic := '1';
  signal rst                    : std_logic := '1';
  signal mm_clk                 : std_logic := '1';
  signal mm_rst                 : std_logic := '1';

  signal sl1                    : std_logic := '1';

  signal random_0               : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1               : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0                : std_logic;
  signal pulse_1                : std_logic;
  signal pulse_en               : std_logic := '1';

  signal stimuli_en             : std_logic := '1';
  signal stimuli_src_in         : t_dp_siso;
  signal stimuli_src_out        : t_dp_sosi;
  signal stimuli_data           : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal dut_snk_out            : t_dp_siso;
  signal dut_snk_in             : t_dp_sosi;

  signal dut_src_in             : t_dp_siso;
  signal dut_src_out            : t_dp_sosi;

  signal prev_verify_snk_out    : t_dp_siso;
  signal verify_snk_out         : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in          : t_dp_sosi;
  signal verify_snk_in_data     : std_logic_vector(c_dp_stream_data_w - 1 downto 0);  -- used to hold valid data for verify at verify_done
  signal verify_data            : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal prev_verify_snk_in     : t_dp_sosi;

  signal verify_hold_sop        : std_logic := '0';
  signal verify_en_valid        : std_logic := '0';
  signal verify_en_sop          : std_logic := '0';
  signal verify_en_eop          : std_logic := '0';
  signal verify_done            : std_logic := '0';

  signal expected_verify_snk_in : t_dp_sosi;
  signal mm_mosi                : t_mem_mosi;
  signal mm_miso                : t_mem_miso;

  signal mm_nof_early_syncs     : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_nof_late_syncs      : std_logic_vector(c_word_w - 1 downto 0);
begin
  clk    <= (not clk)    or tb_end after clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst    <= '1', '0' after clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  stimuli_en           <= '1'                     when g_flow_control_stimuli = e_active else
                          random_0(random_0'high) when g_flow_control_stimuli = e_random else
                          pulse_0                 when g_flow_control_stimuli = e_pulse;

  verify_snk_out.ready <= '1'                     when g_flow_control_verify = e_active  else
                          random_1(random_1'high) when g_flow_control_verify = e_random  else
                          pulse_1                 when g_flow_control_verify = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------
  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn     := INCR_UVEC(c_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(c_channel_init), -1);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(c_data_init),       -1);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(c_err_init),       -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod g_sync_period) = c_sync_offset, '1', '0');  -- insert sync starting at BSN=c_sync_offset and with period g_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len);
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      v_sosi.err     := INCR_UVEC(v_sosi.err, 1);

      -- Send packet
      proc_dp_gen_block_data(g_in_dat_w, TO_UINT(v_sosi.data), g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    -- . e_qual
    v_sosi.bsn     := std_logic_vector( unsigned(c_bsn_init) + g_nof_repeat - 1);
    v_sosi.channel := TO_DP_CHANNEL(c_channel_init           + g_nof_repeat - 1);
    v_sosi.err     := TO_DP_ERROR(c_err_init                 + g_nof_repeat - 1);
    -- . account for g_pkt_len
    v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len - 1);
    v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
    expected_verify_snk_in <= v_sosi;

    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 100);  -- latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    proc_common_gen_pulse(clk, verify_done);
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------
  -- Start verify after first valid, sop or eop
  verify_en_valid <= '1' when verify_snk_in.valid = '1' and rising_edge(clk);
  verify_en_sop   <= '1' when verify_snk_in.sop = '1'   and rising_edge(clk);
  verify_en_eop   <= '1' when verify_snk_in.eop = '1'   and rising_edge(clk);

  -- Verify that the stimuli have been applied at all
  proc_dp_verify_value("verify_snk_in.valid",            clk, verify_done, sl1, verify_en_valid);
  proc_dp_verify_value("verify_snk_in.sop",              clk, verify_done, sl1, verify_en_sop);
  proc_dp_verify_value("verify_snk_in.eop",              clk, verify_done, sl1, verify_en_eop);
  proc_dp_verify_value("verify_snk_in.data",    e_equal, clk, verify_done, expected_verify_snk_in.data,    verify_snk_in_data);

  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_data("verify_snk_in.data",    c_rl, c_data_max, to_unsigned(c_gapsize, 16), clk, verify_en_valid, verify_snk_out.ready, verify_snk_in.valid, verify_snk_in.data,    prev_verify_snk_in.data);

  -- Verify output packet ctrl
  proc_dp_verify_sop_and_eop(clk, verify_snk_in.valid, verify_snk_in.sop, verify_snk_in.eop, verify_hold_sop);

  -- Verify output ready latency
  proc_dp_verify_valid(clk, verify_en_valid, verify_snk_out.ready, prev_verify_snk_out.ready, verify_snk_in.valid);

  p_mm_stimuli : process
  begin
    ----------------------------------------------------------------------------
    -- Verify reading out registers
    ----------------------------------------------------------------------------
    mm_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 290);

    -- read to verify nof syncs
    proc_mem_mm_bus_rd(0, mm_clk, mm_mosi);  -- Read nof_early_syncs
    proc_common_wait_some_cycles(mm_clk, 1);
    mm_nof_early_syncs <= mm_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(1, mm_clk, mm_mosi);  -- Read nof_late_syncs
    proc_common_wait_some_cycles(mm_clk, 1);
    mm_nof_late_syncs <= mm_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);

    assert TO_UINT(mm_nof_early_syncs) = 0
      report "Unexpected mm_nof_early_syncs"
      severity ERROR;
    assert TO_UINT(mm_nof_late_syncs) > 0
      report "Unexpected mm_nof_late_syncs"
      severity ERROR;

    -- read again to verify clear after rd
    proc_mem_mm_bus_rd(0, mm_clk, mm_mosi);  -- Read nof_early_syncs
    proc_common_wait_some_cycles(mm_clk, 1);
    mm_nof_early_syncs <= mm_miso.rddata(c_word_w - 1 downto 0);
    proc_mem_mm_bus_rd(1, mm_clk, mm_mosi);  -- Read nof_late_syncs
    proc_common_wait_some_cycles(mm_clk, 1);
    mm_nof_late_syncs <= mm_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);

    assert TO_UINT(mm_nof_early_syncs) = 0
      report "Unexpected cleared mm_nof_early_syncs"
      severity ERROR;
    assert TO_UINT(mm_nof_late_syncs) = 0
      report "Unexpected cleared mm_nof_late_syncs"
      severity ERROR;

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  -- Connect stimuli input stream to DUT sink
  stimuli_src_in <= dut_snk_out;
  dut_snk_in     <= stimuli_src_out;

  -- DUT function
  dut : entity work.mms_dp_sync_checker
  generic map (
    g_cross_clock_domain => true,
    g_nof_blk_per_sync   => g_nof_blk_per_sync
  )
  port map (
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    dp_rst                   => rst,
    dp_clk                   => clk,
    snk_out                  => dut_snk_out,
    snk_in                   => dut_snk_in,
    src_in                   => dut_src_in,
    src_out                  => dut_src_out,
    reg_dp_sync_checker_mosi => mm_mosi,
    reg_dp_sync_checker_miso => mm_miso
  );

  -- Connect DUT source output stream to verification
  dut_src_in     <= verify_snk_out;
  verify_snk_in  <= dut_src_out;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_data <= stimuli_src_out.data(g_in_dat_w - 1 downto 0);
  verify_data  <= verify_snk_in.data(g_in_dat_w - 1 downto 0);

  verify_snk_in_data <= verify_snk_in.data when verify_snk_in.valid = '1';
end tb;
