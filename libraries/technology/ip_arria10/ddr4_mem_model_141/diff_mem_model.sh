#!/bin/bash
# -------------------------------------------------------------------------- #
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>           
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands                             
#                                                                           
# This program is free software: you can redistribute it and/or modify      
# it under the terms of the GNU General Public License as published by      
# the Free Software Foundation, either version 3 of the License, or         
# (at your option) any later version.                                       
#                                                                           
# This program is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             
# GNU General Public License for more details.                              
#                                                                           
# You should have received a copy of the GNU General Public License         
# along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
# -------------------------------------------------------------------------- #
#
# Purpose: Compare DDR4 memory model core files with Qsys example design core files
# Description:
#   The DDR4 memory model core files do not depend on the size and speed of the specific DDR4 example design.
#   Therefore there shouldbe no diffs.
# Usage:
#
#   ./diff_mem_model.sh <path to specific DDR4 example design>
#
#   eg:
#
#   ./diff_mem_model.sh $HDL_WORK/libraries/technology/ip_arria10/ddr4_4g_1600/emif_0_example_design
#

EXAMPLE_DESIGN_DIR=${1}

CORE_DIR="${EXAMPLE_DESIGN_DIR}/sim/altera_emif_mem_model_core_ddr4_141/sim"

SIM_DIR="${RADIOHDL}/libraries/technology/ip_arria10/ddr4_mem_model_141/sim"

# DDR4 memory model core files
diff ${SIM_DIR}/ddr4_core/altera_emif_ddr4_crc_tree.sv                    ${CORE_DIR}/altera_emif_ddr4_crc_tree.sv           
diff ${SIM_DIR}/ddr4_core/altera_emif_ddr4_model_rcd_chip.sv              ${CORE_DIR}/altera_emif_ddr4_model_rcd_chip.sv     
diff ${SIM_DIR}/ddr4_core/altera_emif_ddrx_model_bidir_delay.sv           ${CORE_DIR}/altera_emif_ddrx_model_bidir_delay.sv  
diff ${SIM_DIR}/ddr4_core/altera_emif_ddrx_model_per_device.sv            ${CORE_DIR}/altera_emif_ddrx_model_per_device.sv   
diff ${SIM_DIR}/ddr4_core/altera_emif_ddrx_model_per_ping_pong.sv         ${CORE_DIR}/altera_emif_ddrx_model_per_ping_pong.sv
diff ${SIM_DIR}/ddr4_core/altera_emif_ddrx_model_rank.sv                  ${CORE_DIR}/altera_emif_ddrx_model_rank.sv         
diff ${SIM_DIR}/ddr4_core/altera_emif_ddrx_model.sv                       ${CORE_DIR}/altera_emif_ddrx_model.sv              

diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddr4_crc_tree.sv             ${CORE_DIR}/mentor/altera_emif_ddr4_crc_tree.sv           
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddr4_model_rcd_chip.sv       ${CORE_DIR}/mentor/altera_emif_ddr4_model_rcd_chip.sv     
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddrx_model_bidir_delay.sv    ${CORE_DIR}/mentor/altera_emif_ddrx_model_bidir_delay.sv  
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddrx_model_per_device.sv     ${CORE_DIR}/mentor/altera_emif_ddrx_model_per_device.sv   
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddrx_model_per_ping_pong.sv  ${CORE_DIR}/mentor/altera_emif_ddrx_model_per_ping_pong.sv
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddrx_model_rank.sv           ${CORE_DIR}/mentor/altera_emif_ddrx_model_rank.sv         
diff ${SIM_DIR}/ddr4_core/mentor/altera_emif_ddrx_model.sv                ${CORE_DIR}/mentor/altera_emif_ddrx_model.sv              

# DDR4 memory model device specific files
DEVICE_DIR="${EXAMPLE_DESIGN_DIR}/sim/altera_emif_mem_model_141/sim"

# Uncomment the line below to check that a recreated device specific DDR4 model is the same asthat in SVN
#diff ${SIM_DIR}/ddr4_4g_1600/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd ${DEVICE_DIR}/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd
#diff ${SIM_DIR}/ddr4_8g_2400/ed_sim_altera_emif_mem_model_141_oes36qy.vhd ${DEVICE_DIR}/ed_sim_altera_emif_mem_model_141_oes36qy.vhd
