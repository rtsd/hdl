-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Test bench for common_debounce (all g_type's)
-- Description:
--   The tb is not self checking. Manually observe the wave window and check
--   the q_out during and after reset and compare g_high and q_low with q_both:
--
--   . q_both changes level only when d_in is stable at the other level
--   . q_high goes high with q_both and low  immediately with d_in
--   . q_low  goes low  with q_both and high immediately with d_in
--
--   Therefore:
--
--   . A one cycle level change "1110111" only affects q_high
--   . A one cycle level change "0001000" only affects q_low
--
-- Usage:
--   > as 5
--   > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity tb_common_debounce is
end tb_common_debounce;

architecture tb of tb_common_debounce is
  constant clk_period   : time := 10 ns;

  constant c_rst_level_both  : std_logic := '0';  -- choose any reset level, because both levels are equivalent
  constant c_rst_level_high  : std_logic := '0';  -- choose '0' = not '1' for q_high reset level
  constant c_rst_level_low   : std_logic := '1';  -- choose '1' = not '0' for q_low reset level

  --CONSTANT d_in_level : STD_LOGIC := '1';
  constant d_in_level : std_logic := '0';

  constant c_latency    : natural := 16;
  constant c_short      : natural :=    c_latency / 2;
  constant c_long       : natural := 10 * c_latency;

  signal tb_end    : std_logic := '0';
  signal rst       : std_logic := '1';
  signal clk       : std_logic := '0';
  signal d_in      : std_logic;
  signal q_high    : std_logic;
  signal q_both    : std_logic;
  signal q_low     : std_logic;
begin
  -- run 20 us

  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * c_long;

  p_in_stimuli : process
  begin
    d_in  <= not d_in_level;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    if d_in_level = '1' then
      d_in  <= '1';
      for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    end if;

    -- Change 1 --> 0
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    -- Stable 0
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    -- Change 0 --> 1
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    -- Stable 1
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    -- No change 1 --> 0, too short to notice
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    -- Change 1 --> 0
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '1';
    for I in 0 to c_short loop wait until rising_edge(clk);  end loop;
    d_in  <= '0';
    -- Stable 0
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    -- Change 0 --> 1
    d_in  <= '1';
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    -- One cyle 1 --> 0 --> 1
    d_in  <= '0';
    wait until rising_edge(clk);
    d_in  <= '1';
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    -- Change 1 --> 0
    d_in  <= '0';
    for I in 0 to c_long loop wait until rising_edge(clk);  end loop;
    -- One cyle 0 --> 1 --> 0
    d_in  <= '1';
    wait until rising_edge(clk);
    d_in  <= '0';
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;
    for I in 0 to c_long  loop wait until rising_edge(clk);  end loop;

    tb_end <= '1';
    wait;
  end process;

  u_debounce_both : entity work.common_debounce
  generic map (
    g_delay_len  => c_meta_delay_len,
    g_latency    => c_latency,
    g_init_level => c_rst_level_both
  )
  port map (
    rst   => rst,
    clk   => clk,
    clken => '1',
    d_in  => d_in,
    q_out => q_both
  );

  u_debounce_high : entity work.common_debounce
  generic map (
    g_type       => "HIGH",
    g_delay_len  => c_meta_delay_len,
    g_latency    => c_latency,
    g_init_level => c_rst_level_high
  )
  port map (
    rst   => rst,
    clk   => clk,
    clken => '1',
    d_in  => d_in,
    q_out => q_high
  );

  u_debounce_low : entity work.common_debounce
  generic map (
    g_type       => "LOW",
    g_delay_len  => c_meta_delay_len,
    g_latency    => c_latency,
    g_init_level => c_rst_level_low
  )
  port map (
    rst   => rst,
    clk   => clk,
    clken => '1',
    d_in  => d_in,
    q_out => q_low
  );
end tb;
