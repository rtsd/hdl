-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture input from four ADC [A,B,C,D] on two ADU and support
--          pattern verify for each ADC

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

entity aduh_quad is
  generic (
    -- ADC Interface
    g_sim             : boolean := false;
    g_nof_dp_phs_clk  : natural := 1;  -- nof dp_phs_clk that can be used to detect the word phase
    g_ai              : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A               : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_B               : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_A_CLK           : in  std_logic;
    ADC_BI_A_CLK_RST       : out std_logic;

    -- . ADU_CD
    ADC_BI_C               : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D               : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D_CLK           : in  std_logic;
    ADC_BI_D_CLK_RST       : out std_logic;

    -- Streaming clock domain
    dp_rst                 : in  std_logic;
    dp_clk                 : in  std_logic;
    dp_phs_clk_vec         : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);

    -- . data
    aduh_sosi_arr          : out t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = ADC_BI ports [A,B,C,D]

    -- . status
    aduh_ab_status         : out std_logic_vector(c_word_w - 1 downto 0);
    aduh_ab_locked         : out std_logic;
    aduh_ab_stable         : out std_logic;
    aduh_ab_stable_ack     : in  std_logic;
    aduh_ab_control        : in  std_logic_vector(c_word_w - 1 downto 0);

    aduh_cd_status         : out std_logic_vector(c_word_w - 1 downto 0);
    aduh_cd_locked         : out std_logic;
    aduh_cd_stable         : out std_logic;
    aduh_cd_stable_ack     : in  std_logic;
    aduh_cd_control        : in  std_logic_vector(c_word_w - 1 downto 0);

    aduh_verify_res        : out t_slv_32_arr(0 to g_ai.nof_sp - 1);  -- [8,7:0]
    aduh_verify_res_val    : out std_logic_vector(0 to g_ai.nof_sp - 1);
    aduh_verify_res_ack    : in  std_logic_vector(0 to g_ai.nof_sp - 1)
  );
end aduh_quad;

architecture str of aduh_quad is
  constant c_dp_factor       : natural := g_ai.rx_factor * g_ai.dd_factor;
  constant c_wideband_factor : natural := c_dp_factor;  -- Wideband rate factor = 4 for dp_clk is 200 MHz frequency and sample frequency Fs is 800 MHz

  constant c_adc_pattern_sel : t_natural_arr(0 to g_ai.nof_sp - 1) := (0, 1, 0, 1);  -- signal path [A, B, C, D] = ADC [I, Q, I, Q]

  signal aduh_ab_dp_phs_clk_en_vec  : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
  signal aduh_cd_dp_phs_clk_en_vec  : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);

  signal i_aduh_sosi_arr            : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = ADC_BI ports [A,B,C,D]
begin
  -- ADC [A,B,C,D] input samples
  aduh_sosi_arr <= i_aduh_sosi_arr;

  aduh_ab_dp_phs_clk_en_vec <= aduh_ab_control(g_nof_dp_phs_clk - 1 downto 0);
  aduh_cd_dp_phs_clk_en_vec <= aduh_cd_control(g_nof_dp_phs_clk - 1 downto 0);

  u_aduh : entity work.aduh_dd
  generic map (
    g_sim            => g_sim,
    g_nof_dp_phs_clk => g_nof_dp_phs_clk,
    g_ai             => g_ai
  )
  port map (
    -- LVDS Interface
    ADC_BI_A             => ADC_BI_A,
    ADC_BI_B             => ADC_BI_B,
    ADC_BI_C             => ADC_BI_C,
    ADC_BI_D             => ADC_BI_D,

    ADC_BI_A_CLK         => ADC_BI_A_CLK,  -- lvds clock from ADU_AB
    ADC_BI_D_CLK         => ADC_BI_D_CLK,  -- lvds clock from ADU_CD

    ADC_BI_A_CLK_RST     => ADC_BI_A_CLK_RST,  -- release synchronises ADU_AB DCLK divider
    ADC_BI_D_CLK_RST     => ADC_BI_D_CLK_RST,  -- release synchronises ADU_CD DCLK divider

    -- DP Interface
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    dp_phs_clk_vec       => dp_phs_clk_vec,

    -- . Control
    ab_status            => aduh_ab_status,
    ab_locked            => aduh_ab_locked,
    ab_stable            => aduh_ab_stable,
    ab_stable_ack        => aduh_ab_stable_ack,
    ab_dp_phs_clk_en_vec => aduh_ab_dp_phs_clk_en_vec,

    cd_status            => aduh_cd_status,
    cd_locked            => aduh_cd_locked,
    cd_stable            => aduh_cd_stable,
    cd_stable_ack        => aduh_cd_stable_ack,
    cd_dp_phs_clk_en_vec => aduh_cd_dp_phs_clk_en_vec,

    -- . Streaming
    src_out_arr      => i_aduh_sosi_arr
  );

  -- ADC pattern verification
  gen_verify : for I in 0 to g_ai.nof_sp - 1 generate
    aduh_verify_res(I)(c_word_w - 1 downto g_ai.port_w + 1) <= (others => '0');  -- unused bits [31:9]

    u_adc : entity work.aduh_verify
    generic map (
      g_symbol_w             => g_ai.port_w,  -- = 8, fixed
      g_nof_symbols_per_data => c_wideband_factor  -- = 4, fixed, big endian in_sosi.data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    )
    port map (
      rst            => dp_rst,
      clk            => dp_clk,

      -- ST input
      in_sosi        => i_aduh_sosi_arr(I),

      -- Static control input (connect via MM or leave open to use default)
      pattern_sel    => c_adc_pattern_sel(I),
      verify_res     => aduh_verify_res(I)(g_ai.port_w downto 0),  -- [8,7:0]
      verify_res_val => aduh_verify_res_val(I),
      verify_res_ack => aduh_verify_res_ack(I)
    );
  end generate;
end str;
