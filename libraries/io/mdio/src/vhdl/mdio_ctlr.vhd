-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

entity mdio_ctlr is
  generic (
    g_mdio_prtad           : std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0);
    g_mdio_cmd_arr         : t_mdio_cmd_arr;
    g_mdio_rst_level       : std_logic;  -- Active level of MDIO device's reset input
    g_mdio_rst_cycles      : natural;  -- Number of clock cycles when reset is active
    g_mdio_post_rst_cycles : natural  -- Number of post-reset clock cycles before FSM starts command execution
  );
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;

    mdio_rst          : out std_logic;
    mdio_en_evt       : out std_logic;
    mdio_done         : in  std_logic;
    mdio_done_ack_evt : out std_logic;

    hdr               : out std_logic_vector(c_halfword_w - 1 downto 0);
    tx_dat            : out std_logic_vector(c_halfword_w - 1 downto 0);

    exec_complete     : out std_logic
  );
end mdio_ctlr;

architecture rtl of mdio_ctlr is
  type t_state_enum is (s_init_rst, s_init_post_rst, s_idle, s_write_adr, s_write_dat, s_ackdone, s_exec_complete);

  type t_reg is record
    state              : t_state_enum;
    cycle_cnt          : natural;
    mdio_rst           : std_logic;
    cmd_processed      : std_logic;
    nof_cmds_processed : natural;
    mdio_en_evt        : std_logic;
    mdio_done_ack_evt  : std_logic;
    hdr                : std_logic_vector(c_halfword_w - 1 downto 0);
    tx_dat             : std_logic_vector(c_halfword_w - 1 downto 0);
    exec_complete      : std_logic;
  end record;

  signal r,  nxt_r : t_reg;
begin
  p_comb : process(rst, r, mdio_done)
    variable v : t_reg;
  begin
    v := r;
    v.mdio_en_evt := '0';
    v.mdio_done_ack_evt := '0';

    case r.state is

      when s_init_rst => if r.cycle_cnt < g_mdio_rst_cycles then
                           v.cycle_cnt := v.cycle_cnt + 1;
                         else
                           v.cycle_cnt := 0;
                           v.state := s_init_post_rst;
                         end if;

      when s_init_post_rst => v.mdio_rst := not g_mdio_rst_level;
                              if r.cycle_cnt < g_mdio_post_rst_cycles then
                                v.cycle_cnt := v.cycle_cnt + 1;
                              else
                                v.state := s_idle;
                              end if;

      when s_idle => v.cmd_processed := '0';
                     if g_mdio_cmd_arr'length > v.nof_cmds_processed then
                       -- Interpret command
                       v.state := s_write_adr;
                     else
                       v.state := s_exec_complete;
                     end if;

      when s_write_adr => v.cmd_processed := '0';
                          -- Set the register address we're going to access
                          v.tx_dat      := g_mdio_cmd_arr(v.nof_cmds_processed).devreg;
                          -- Now assign the 'address' header which indicated port address and device address of our register
                          v.hdr         := mdio_hdr_adr( g_mdio_prtad, g_mdio_cmd_arr(v.nof_cmds_processed).devadr);
                          -- Assert en_evt and wait for done to go high.
                          v.mdio_en_evt := '1';
                          v.state       := s_ackdone;

      when s_write_dat => v.cmd_processed := '1';
                          -- Assign wr_data to tx_dat; in case of a read access the wr_data is zero.
                          v.tx_dat      := g_mdio_cmd_arr(v.nof_cmds_processed).wrdata;
                          -- Now assign a write header or a read header
                          if g_mdio_cmd_arr(v.nof_cmds_processed).wr_not_rd = '1' then
                            v.hdr         := mdio_hdr_wr( g_mdio_prtad, g_mdio_cmd_arr(v.nof_cmds_processed).devadr);
                          else
                            v.hdr         := mdio_hdr_rd( g_mdio_prtad, g_mdio_cmd_arr(v.nof_cmds_processed).devadr);
                          end if;
                          -- Assert en_evt and wait for done to go high.
                          v.mdio_en_evt   := '1';
                          v.state         := s_ackdone;

      when s_ackdone => if mdio_done = '1' then
                          v.mdio_done_ack_evt := '1';
                          if r.cmd_processed = '0' then
                            -- Proceeed with the second part of the MDIO write access
                            v.state := s_write_dat;
                          else
                            -- Fetch a new command
                            v.nof_cmds_processed := r.nof_cmds_processed + 1;
                            v.state := s_idle;
                          end if;
                        end if;

      when s_exec_complete => v.exec_complete := '1';
    end case;

    if rst = '1' then
      v.state              := s_init_rst;
      v.cycle_cnt          := 0;
      v.mdio_rst           := g_mdio_rst_level;
      v.nof_cmds_processed := 0;
      v.mdio_en_evt        := '0';
      v.mdio_done_ack_evt  := '0';
      v.hdr                := (others => '0');
      v.tx_dat             := (others => '0');
      v.exec_complete      := '0';
    end if;

    nxt_r <= v;
  end process;

  p_seq : process(clk)
  begin
    if rising_edge(clk) then r <= nxt_r; end if;
  end process;

  mdio_rst          <= r.mdio_rst;
  mdio_en_evt       <= r.mdio_en_evt;
  mdio_done_ack_evt <= r.mdio_done_ack_evt;
  hdr               <= r.hdr;
  tx_dat            <= r.tx_dat;
  exec_complete     <= r.exec_complete;
end rtl;
