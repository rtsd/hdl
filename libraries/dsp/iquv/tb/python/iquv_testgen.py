#!/usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Generates test vectors and expected results for use in the IQUV module test
   bench tb_iquv_file_data.vhd                                                 
   
   Usage:
   > ./iquv_testgen.py

   Creates output file iquv_stimuli_1.dat
   File format per line is
       xreal ximag yreal yimag res_I_n0 res_Q_n0 res_U_n0 res_V_n0 res_I_n1 ...

   Where
       xreal ximag yreal yimag are the input stimuli
       res_P_nN is the accumulated Stokes product P, integrated over N samples

   The VHDL test bench must 
       1. Hold the input stimuli constant over N input samples
       2. Calculate the truncation to 8 or 18 bits as needed
   
"""



nints = (8, 16, 1)                      # number of samples to integrate  (typ. 8, 16, or 1 for no integration)
xpol = []
ypol = []
test_stimuli = (-2047, -2046, -1023, -2, -1, 0, 1, 2, 1023, 2046, 2047)
nvalues = len(test_stimuli)**4
print(nvalues)

# apply all possible combinations of the test stimuli values to the 2 pol complex input
for xreal in test_stimuli:
    for ximag in test_stimuli:
        for yreal in test_stimuli:
            for yimag in test_stimuli:
                xpol.append(xreal + 1j * ximag)
                ypol.append(yreal + 1j * yimag)
                #print(xreal, ximag, yreal, yimag) 
 

fh = open('../data/iquv_stimuli_1.dat','w')

# calculate the outputs
for index in range(nvalues):
    Ival = xpol[index] * xpol[index].conjugate() + ypol[index] * ypol[index].conjugate()
    Qval = xpol[index] * xpol[index].conjugate() - ypol[index] * ypol[index].conjugate()
    Uval = xpol[index] * ypol[index].conjugate() + ypol[index] * xpol[index].conjugate()
    Vval = ypol[index] * xpol[index].conjugate() - xpol[index] * ypol[index].conjugate()
    result = []
    result.append(int(xpol[index].real))
    result.append(int(xpol[index].imag))
    result.append(int(ypol[index].real))
    result.append(int(ypol[index].imag))
    for nint in nints:
        result.append(nint * int(Ival.real))
        result.append(nint * int(Qval.real))
        result.append(nint * int(Uval.real))
        result.append(nint * int(Vval.imag))
    print " ".join(str(e) for e in result)
    fh.write( str(" ".join(str(e) for e in result)) + '\n')




