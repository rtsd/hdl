--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Combine mac_10g and xaui for c_tech_stratixiv
-- Description:
--
--   Blockdiagram:
--
--                                 ref
--                             156 156
--                _________    | ^  |   ____________
--                |       |    | |  |   |          |
--                |       |    | |  \-->|          |
--                |       |<---/ \------|          |
--                |       |             |          |
--                |       |    XGMII    |          |
--     tx_snk --->|tech_  |------------>|tech_     |---> xaui_tx[3:0]
--     rx_src <---|mac_10g|<------------|xaui      |<--- xaui_rx[3:0]
--                |       |             |          |
--                |_______|--\       /--|__________|
--                    |      |       |
--                  mac_mm   |       |
--                           |       v
--                       (   |    xgmii_tx_ready       )
--                       (   v    txc_rx_channelaligned)
--     tx_snk_out.xon <--(xgmii_link_status[1:0]       )
--
-- . g_direction
--   "TX_RX" = Default support bidir
--   "TX_ONLY" = Uses a bidir MAC and connects the MAC TX to the XAUI TX and
--       also to the MAC RX. The MAC RX needs to see XGMII Rx data otherwise
--       the Tx does not start. Therefore the simple approach using
--       xgmii_internal_dc_arr is implemented. Three other implementations have
--       been considered:
--       . Instantiate a bidir MAC for Tx and another bidir MAC for Rx, to
--         avoid leaving xgmii_rx_dc_arr from XAUI PHY unconnected.
--       . Instantiate a Tx only MAC IP , but that is not tried.
--       . Configuring the bidir MAC to Tx unidirectional via MM still does
--         not seem to work without activity on the XGMII Rx data.
--   "RX_ONLY" = Same as "TX_RX", just leave the Tx ST and Tx XAUI pins not
--       connected. Synthesis will optimize away what is not needed.
--
-- Remarks:
-- . xgmii_link_status:
--   A link fault eg. due to rx disconnect is detected by the link fault status:
--     0 = OK
--     1 = local fault
--     2 = remote fault
--
--   From google search:
--     Link fault Operation
--     1) Device B detects loss of signal. Local fault is signaled by PHY of Device B to Device B.
--     2) Device B ceases transmission of MAC frames and transmits remote fault to Device A.
--     3) Device A receives remote fault from Device B.
--     4) Device A stops sending frames, continuously generates Idle.
--

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib, ip_stratixiv_eth_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use work.tech_eth_10g_component_pkg.all;

entity tech_eth_10g_stratixiv is
  generic (
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;  -- 0 = use XAUI IP; 1 = use fast serdes model
    g_nof_channels            : natural := 1;
    g_direction               : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding      : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_156   : in  std_logic;  -- 156.25 MHz for XAUI
    tr_ref_rst_156   : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk      : in  std_logic;

    -- Data clocks
    tx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr_in or connect rx_clk_arr to tx_clk_arr_in
    tx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    xaui_mosi        : in  t_mem_mosi := c_mem_mosi_rst;  -- XAUI control
    xaui_miso        : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz tx_clk_arr_in
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz rx_clk_arr_in
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- XAUI serial IO
    xaui_tx_arr      : out t_xaui_arr(g_nof_channels - 1 downto 0);
    xaui_rx_arr      : in  t_xaui_arr(g_nof_channels - 1 downto 0)
  );
end tech_eth_10g_stratixiv;

architecture str of tech_eth_10g_stratixiv is
begin
  u_ip_stratixiv_eth_10g : ip_stratixiv_eth_10g
  generic map(
    g_sim                => g_sim,
    g_sim_level          => g_sim_level,
    g_nof_channels       => g_nof_channels,
    g_direction          => g_direction,
    g_pre_header_padding => g_pre_header_padding
  )
  port map(
    -- Transceiver PLL reference clock
    tr_ref_clk_156   => tr_ref_clk_156,
  tr_ref_rst_156   => tr_ref_rst_156,

  -- Calibration & reconfig clock
  cal_rec_clk      => cal_rec_clk,

  -- Data clocks
  tx_clk_arr_in    => tx_clk_arr_in,
  tx_rst_arr_out   => tx_rst_arr_out,
  rx_clk_arr_out   => rx_clk_arr_out,
  rx_clk_arr_in    => rx_clk_arr_in,
  rx_rst_arr_out   => rx_rst_arr_out,

  -- MM
  mm_clk           => mm_clk,
  mm_rst           => mm_rst,

  mac_mosi         => mac_mosi,
  mac_miso         => mac_miso,

  xaui_mosi        => xaui_mosi,
  xaui_miso        => xaui_miso,

  -- ST
  tx_snk_in_arr    => tx_snk_in_arr,
  tx_snk_out_arr   => tx_snk_out_arr,

  rx_src_out_arr   => rx_src_out_arr,
  rx_src_in_arr    => rx_src_in_arr,

  -- XAUI serial IO
  xaui_tx_arr      => xaui_tx_arr,
  xaui_rx_arr      => xaui_rx_arr
  );
end str;
