-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose : MMS for dp_bsn_aligner
-- Description: See dp_bsn_aligner.vhd

entity mms_dp_bsn_align is
  generic (
    g_block_size           : natural := 32;  -- > 1, g_block_size=1 is not supported, g_block_size <= g_block_period
    g_block_period         : natural := 256;  -- number of clock cycles per block period or block interval (not used in the architecture, used in the comment only)
    g_nof_input            : natural := 2;  -- >= 1
    g_xoff_timeout         : natural := 1536;  -- e.g.  g_bsn_latency * 2  * g_block_period
    g_sop_timeout          : natural := 1024;  -- e.g. (g_bsn_latency + 1) * g_block_period
    g_bsn_latency          : natural := 3;
    g_bsn_request_pipeline : natural := 2;  -- = 1, 2, ..., c_bsn_nof_stages, requested total pipelining of the BSN max and BSN min operation
    g_cross_clock_domain   : boolean := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Memory-mapped clock domain
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;
    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    -- Streaming clock domain
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(g_nof_input - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_input - 1 downto 0);
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(g_nof_input - 1 downto 0) := (others => c_dp_siso_rdy);  -- only src_in_arr(0) is used for src_out flow control because all src_out need to stay aligned
    src_out_arr : out t_dp_sosi_arr(g_nof_input - 1 downto 0)
  );
end mms_dp_bsn_align;

architecture str of mms_dp_bsn_align is
  signal en_evt : std_logic;
  signal en_arr : std_logic_vector(g_nof_input - 1 downto 0);
begin
  u_align : entity work.dp_bsn_align
  generic map(
    g_block_size           => g_block_size,
    g_block_period         => g_block_period,
    g_nof_input            => g_nof_input,
    g_xoff_timeout         => g_xoff_timeout,
    g_sop_timeout          => g_sop_timeout,
    g_bsn_latency          => g_bsn_latency,
    g_bsn_request_pipeline => g_bsn_request_pipeline
  )
  port map(
    rst         => dp_rst,
    clk         => dp_clk,
    -- ST sinks
    snk_out_arr => snk_out_arr,
    snk_in_arr  => snk_in_arr,
    -- ST source
    src_in_arr  => src_in_arr,
    src_out_arr => src_out_arr,
    -- MM
    in_en_evt   => en_evt,
    in_en_arr   => en_arr
  );

  u_reg : entity work.dp_bsn_align_reg
  generic map (
    g_nof_input          => g_nof_input,
    g_cross_clock_domain => g_cross_clock_domain
  )
  port map (
    -- Clocks and reset
    mm_rst   => mm_rst,
    mm_clk   => mm_clk,
    st_rst   => dp_rst,
    st_clk   => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in   => reg_mosi,
    sla_out  => reg_miso,

    -- MM registers in st_clk domain
    out_en_evt => en_evt,
    out_en_arr => en_arr
  );
end str;
