-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb1_minimal_pkg is
    -----------------------------------------------------------------------------
    -- this component declaration is copy-pasted from Quartus v11.1 QSYS builder
    -----------------------------------------------------------------------------

    component qsys_unb1_minimal is
        port (
            coe_ram_write_export_from_the_avs_eth_0       : out std_logic;  -- export
            coe_reg_read_export_from_the_avs_eth_0        : out std_logic;  -- export
            coe_readdata_export_to_the_reg_mmdp_ctrl      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            mm_clk                                        : out std_logic;  -- clk
            coe_address_export_from_the_pio_system_info   : out std_logic_vector(4 downto 0);  -- export
            coe_address_export_from_the_pio_pps           : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_reset_export_from_the_pio_pps             : out std_logic;  -- export
            coe_readdata_export_to_the_reg_epcs           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_readdata_export_to_the_pio_pps            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_pio_system_info : out std_logic_vector(31 downto 0);  -- export
            coe_reset_export_from_the_reg_unb_sens        : out std_logic;  -- export
            coe_tse_write_export_from_the_avs_eth_0       : out std_logic;  -- export
            coe_reset_export_from_the_reg_wdi             : out std_logic;  -- export
            coe_readdata_export_to_the_reg_dpmm_data      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_reg_mmdp_ctrl   : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_dpmm_ctrl     : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_clk_export_from_the_rom_system_info       : out std_logic;  -- export
            coe_reset_export_from_the_reg_remu            : out std_logic;  -- export
            coe_read_export_from_the_reg_unb_sens         : out std_logic;  -- export
            coe_write_export_from_the_reg_unb_sens        : out std_logic;  -- export
            coe_clk_export_from_the_reg_dpmm_data         : out std_logic;  -- export
            coe_clk_export_from_the_reg_unb_sens          : out std_logic;  -- export
            coe_reg_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
            coe_readdata_export_to_the_reg_dpmm_ctrl      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_read_export_from_the_reg_wdi              : out std_logic;  -- export
            coe_reset_export_from_the_reg_mmdp_data       : out std_logic;  -- export
            coe_reg_write_export_from_the_avs_eth_0       : out std_logic;  -- export
            coe_writedata_export_from_the_reg_mmdp_data   : out std_logic_vector(31 downto 0);  -- export
            coe_read_export_from_the_reg_epcs             : out std_logic;  -- export
            coe_readdata_export_to_the_reg_remu           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_readdata_export_to_the_reg_unb_sens       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_ram_address_export_from_the_avs_eth_0     : out std_logic_vector(9 downto 0);  -- export
            coe_clk_export_from_the_pio_pps               : out std_logic;  -- export
            coe_readdata_export_to_the_pio_system_info    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_rom_system_info : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_dpmm_data     : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_write_export_from_the_reg_mmdp_ctrl       : out std_logic;  -- export
            coe_address_export_from_the_reg_wdi           : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_reset_export_from_the_avs_eth_0           : out std_logic;  -- export
            coe_write_export_from_the_pio_system_info     : out std_logic;  -- export
            coe_tse_address_export_from_the_avs_eth_0     : out std_logic_vector(9 downto 0);  -- export
            coe_write_export_from_the_pio_pps             : out std_logic;  -- export
            coe_write_export_from_the_rom_system_info     : out std_logic;  -- export
            coe_irq_export_to_the_avs_eth_0               : in  std_logic                     := 'X';  -- export
            coe_reset_export_from_the_reg_epcs            : out std_logic;  -- export
            coe_read_export_from_the_rom_system_info      : out std_logic;  -- export
            phasedone_from_the_altpll_0                   : out std_logic;  -- export
            reset_n                                       : in  std_logic                     := 'X';  -- reset_n
            coe_tse_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
            coe_clk_export_from_the_reg_mmdp_ctrl         : out std_logic;  -- export
            coe_tse_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_ram_read_export_from_the_avs_eth_0        : out std_logic;  -- export
            clk_0                                         : in  std_logic                     := 'X';  -- clk
            coe_read_export_from_the_reg_dpmm_ctrl        : out std_logic;  -- export
            coe_writedata_export_from_the_reg_remu        : out std_logic_vector(31 downto 0);  -- export
            coe_write_export_from_the_reg_dpmm_data       : out std_logic;  -- export
            coe_writedata_export_from_the_reg_unb_sens    : out std_logic_vector(31 downto 0);  -- export
            tse_clk                                       : out std_logic;  -- clk
            epcs_clk                                      : out std_logic;  -- clk
            coe_reg_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_reset_export_from_the_reg_dpmm_ctrl       : out std_logic;  -- export
            coe_tse_read_export_from_the_avs_eth_0        : out std_logic;  -- export
            coe_writedata_export_from_the_reg_dpmm_data   : out std_logic_vector(31 downto 0);  -- export
            coe_writedata_export_from_the_reg_wdi         : out std_logic_vector(31 downto 0);  -- export
            coe_reset_export_from_the_pio_system_info     : out std_logic;  -- export
            coe_read_export_from_the_pio_system_info      : out std_logic;  -- export
            coe_clk_export_from_the_reg_mmdp_data         : out std_logic;  -- export
            coe_clk_export_from_the_reg_wdi               : out std_logic;  -- export
            coe_ram_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_write_export_from_the_reg_remu            : out std_logic;  -- export
            coe_clk_export_from_the_reg_epcs              : out std_logic;  -- export
            coe_read_export_from_the_reg_mmdp_data        : out std_logic;  -- export
            coe_writedata_export_from_the_reg_epcs        : out std_logic_vector(31 downto 0);  -- export
            out_port_from_the_pio_wdi                     : out std_logic;  -- export
            coe_reset_export_from_the_reg_dpmm_data       : out std_logic;  -- export
            coe_clk_export_from_the_reg_remu              : out std_logic;  -- export
            coe_read_export_from_the_reg_mmdp_ctrl        : out std_logic;  -- export
            coe_clk_export_from_the_avs_eth_0             : out std_logic;  -- export
            coe_address_export_from_the_reg_mmdp_ctrl     : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_write_export_from_the_reg_epcs            : out std_logic;  -- export
            coe_readdata_export_to_the_rom_system_info    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_reset_export_from_the_reg_mmdp_ctrl       : out std_logic;  -- export
            coe_readdata_export_to_the_reg_mmdp_data      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_write_export_from_the_reg_wdi             : out std_logic;  -- export
            coe_readdata_export_to_the_reg_wdi            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_read_export_from_the_pio_pps              : out std_logic;  -- export
            coe_clk_export_from_the_pio_system_info       : out std_logic;  -- export
            coe_writedata_export_from_the_pio_pps         : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_epcs          : out std_logic_vector(2 downto 0);  -- export
            coe_read_export_from_the_reg_dpmm_data        : out std_logic;  -- export
            coe_reset_export_from_the_rom_system_info     : out std_logic;  -- export
            coe_tse_waitrequest_export_to_the_avs_eth_0   : in  std_logic                     := 'X';  -- export
            coe_writedata_export_from_the_reg_dpmm_ctrl   : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_mmdp_data     : out std_logic;  -- _vector(0 downto 0);                     -- export
            coe_address_export_from_the_reg_unb_sens      : out std_logic_vector(2 downto 0);  -- export
            coe_clk_export_from_the_reg_dpmm_ctrl         : out std_logic;  -- export
            coe_reg_address_export_from_the_avs_eth_0     : out std_logic_vector(3 downto 0);  -- export
            coe_address_export_from_the_rom_system_info   : out std_logic_vector(9 downto 0);  -- export
            coe_write_export_from_the_reg_mmdp_data       : out std_logic;  -- export
            coe_address_export_from_the_reg_remu          : out std_logic_vector(2 downto 0);  -- export
            areset_to_the_altpll_0                        : in  std_logic                     := 'X';  -- export
            locked_from_the_altpll_0                      : out std_logic;  -- export
            coe_write_export_from_the_reg_dpmm_ctrl       : out std_logic;  -- export
            coe_ram_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
            c3_from_the_altpll_0                          : out std_logic;  -- export
            coe_read_export_from_the_reg_remu             : out std_logic  -- export
        );
    end component qsys_unb1_minimal;

    component qsys_unb1_minimal_mm_arbiter is
        port (
            coe_ram_write_export_from_the_avs_eth_0     : out std_logic;  -- export
            coe_reg_read_export_from_the_avs_eth_0      : out std_logic;  -- export
            mm_clk                                      : out std_logic;  -- clk
            coe_tse_write_export_from_the_avs_eth_0     : out std_logic;  -- export
            coe_reg_writedata_export_from_the_avs_eth_0 : out std_logic_vector(31 downto 0);  -- export
            coe_reg_write_export_from_the_avs_eth_0     : out std_logic;  -- export
            coe_ram_address_export_from_the_avs_eth_0   : out std_logic_vector(9 downto 0);  -- export
            coe_reset_export_from_the_avs_eth_0         : out std_logic;  -- export
            coe_tse_address_export_from_the_avs_eth_0   : out std_logic_vector(9 downto 0);  -- export
            coe_irq_export_to_the_avs_eth_0             : in  std_logic                     := 'X';  -- export
            phasedone_from_the_altpll_0                 : out std_logic;  -- export
            reset_n                                     : in  std_logic                     := 'X';  -- reset_n
            coe_tse_writedata_export_from_the_avs_eth_0 : out std_logic_vector(31 downto 0);  -- export
            coe_tse_readdata_export_to_the_avs_eth_0    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_ram_read_export_from_the_avs_eth_0      : out std_logic;  -- export
            clk_0                                       : in  std_logic                     := 'X';  -- clk
            tse_clk                                     : out std_logic;  -- clk
            epcs_clk                                    : out std_logic;  -- clk
            coe_reg_readdata_export_to_the_avs_eth_0    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_tse_read_export_from_the_avs_eth_0      : out std_logic;  -- export
            coe_ram_readdata_export_to_the_avs_eth_0    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            out_port_from_the_pio_wdi                   : out std_logic;  -- export
            coe_clk_export_from_the_avs_eth_0           : out std_logic;  -- export
            coe_tse_waitrequest_export_to_the_avs_eth_0 : in  std_logic                     := 'X';  -- export
            coe_reg_address_export_from_the_avs_eth_0   : out std_logic_vector(3 downto 0);  -- export
            areset_to_the_altpll_0                      : in  std_logic                     := 'X';  -- export
            locked_from_the_altpll_0                    : out std_logic;  -- export
            coe_ram_writedata_export_from_the_avs_eth_0 : out std_logic_vector(31 downto 0);  -- export
            c3_from_the_altpll_0                        : out std_logic;  -- export
            to_mm_arbiter_reset_export                  : out std_logic;  -- export
            to_mm_arbiter_clk_export                    : out std_logic;  -- export
            to_mm_arbiter_address_export                : out std_logic_vector(10 downto 0);  -- export
            to_mm_arbiter_write_export                  : out std_logic;  -- export
            to_mm_arbiter_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            to_mm_arbiter_read_export                   : out std_logic;  -- export
            to_mm_arbiter_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export                : out std_logic;  -- export
            pio_system_info_clk_export                  : out std_logic;  -- export
            pio_system_info_address_export              : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_write_export                : out std_logic;  -- export
            pio_system_info_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_read_export                 : out std_logic;  -- export
            pio_system_info_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X')  -- export
        );
    end component qsys_unb1_minimal_mm_arbiter;
end qsys_unb1_minimal_pkg;
