-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Output a one cycle pulse every period
-- Description:
--   The pulse period can dynamically be set via the input pulse_period.
--   Default pulse_period = g_pulse_period, to also support static setting of
--   the pulse period. The pulse_clr can be used to synchronise the pulser.
--   The g_pulse_phase defines when the first pulse occurs after rst release:
--   . g_pulse_phase=0                : first pulse after g_pulse_period cycles
--   . g_pulse_phase=g_pulse_period   : first pulse after g_pulse_period cycles
--   . g_pulse_phase=g_pulse_period-1 : first pulse after              1 cycles
--   . g_pulse_phase=g_pulse_period-N : first pulse after              N cycles
entity common_pulser is
  generic (
    g_pulse_period : natural := 25000;  -- nof clk cycles to get pulse period
    g_pulse_phase  : natural := 0
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;
    clken          : in  std_logic := '1';  -- support running on clken freq
    pulse_period   : in  std_logic_vector(ceil_log2(g_pulse_period + 1) - 1 downto 0) := TO_UVEC(g_pulse_period, ceil_log2(g_pulse_period + 1));
    pulse_en       : in  std_logic := '1';  -- enable the pulse interval timer
    pulse_clr      : in  std_logic := '0';  -- restart the pulse interval timer
    pulse_out      : out std_logic
  );
end common_pulser;

architecture rtl of common_pulser is
  constant c_pulse_period_w  : natural := ceil_log2(g_pulse_period + 1);

  -- Map g_pulse_phase = phs natural range to equivalent integer range of c_pulse_init that is used by g_init of common_counter to avoid truncation warning for conversion to slv
  -- For example for c_pulse_period_w = w = 3:
  --  0 1 2 3  4  5  6  7
  --  0 1 2 3 -4 -3 -2 -1  --> if p < 2**(w-1) then return phs else return phs-2**w
  constant c_pulse_init      : integer := sel_a_b(g_pulse_phase < 2**(c_pulse_period_w - 1), g_pulse_phase, g_pulse_phase-2**c_pulse_period_w);

  signal cnt           : std_logic_vector(c_pulse_period_w - 1 downto 0) := (others => '0');
  signal cnt_en        : std_logic;
  signal cnt_clr       : std_logic;
  signal cnt_period    : std_logic;
begin
  p_clk : process(clk, rst)
  begin
    if rst = '1' then
      pulse_out <= '0';
    elsif rising_edge(clk) then
      if clken = '1' then
        pulse_out <= cnt_period;
      end if;
    end if;
  end process;

  -- pulse period counter
  cnt_period <= '1' when pulse_en = '1' and unsigned(cnt) = unsigned(pulse_period) - 1 else '0';

  cnt_en     <= pulse_en;
  cnt_clr    <= pulse_clr or cnt_period;

  u_cnt : entity common_lib.common_counter
  generic map (
    g_init      => c_pulse_init,
    g_width     => c_pulse_period_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );
end rtl;
