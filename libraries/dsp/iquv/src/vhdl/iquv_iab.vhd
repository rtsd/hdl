-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--     Jonathan Hargreaves, 24 Nov 2016
--
-- Purpose
--     Incoherent Array Beamformer
-- Description
--     Input is g_nof_streams of time multiplexed data [time ie BLOCK0,BLOCK1 ...][subband fbin0,fbin1, ...][Polatization X,Y]
--     Use the valid signal to synchronize. Breaks are allowed at any time.
--     Calculate Stokes products I, Q, U and V on each input stream, integrating g_nof_int successive samples
--     Sum the products over all the streams and requantize the outputs
-- Remarks
--     The output valid is taken from stream 0
--     If g_use_accum is true the integration is done after adding the g_nof_streams, otherwise it is done in the IQUV modules

library IEEE, common_lib, common_mult_lib, technology_lib, dp_lib, iquv_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity iquv_iab is
  generic (
    g_sim           : boolean := false;
    g_technology    : natural := c_tech_select_default;
    g_use_accum     : boolean := false;
    g_use_gain_ctrl : boolean := false;  -- set TRUE to ignore accumulator bit growth and enable fine gain control
    g_nof_streams   : natural := 12;  -- number of imput streams
    g_nof_int       : natural := 8;  -- number of successive subbands to integrate
    g_in_data_w     : natural := 12;  -- width of the incoming complex data
    g_out_data_w    : natural := 18  -- width of the final summed output
  );
  port (
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;
    mm_rst          : in  std_logic := '0';
    mm_clk          : in  std_logic := '0';

    -- mm interface for the gain control
    reg_dp_gain_i_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_gain_i_miso : out t_mem_miso;
    reg_dp_gain_q_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_gain_q_miso : out t_mem_miso;
    reg_dp_gain_u_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_gain_u_miso : out t_mem_miso;
    reg_dp_gain_v_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_gain_v_miso : out t_mem_miso;

    -- Streaming
    in_complex_arr  : in  t_dp_sosi_arr(0 to g_nof_streams - 1);  -- Complex input data
    i_out           : out t_dp_sosi;  -- Output data I
    q_out           : out t_dp_sosi;  -- Output data Q
    u_out           : out t_dp_sosi;  -- Output data U
    v_out           : out t_dp_sosi  -- Output data V

  );
end iquv_iab;

architecture str of iquv_iab is
  constant c_accum_in_iquv        : boolean := not g_use_accum;
  constant c_accumcount_w         : natural := ceil_log2(g_nof_int);
  constant c_iquv_out_w           : natural := 18;
  constant c_adder_input_w        : natural := g_nof_streams * c_iquv_out_w;
  constant c_adder_delay          : natural := ceil_log2(g_nof_streams);
  constant c_adder_out_w          : natural := c_iquv_out_w + ceil_log2(g_nof_streams);
  constant c_accum_out_w          : natural := sel_a_b(g_use_accum, c_adder_out_w + c_accumcount_w, c_adder_out_w);

  -- bitwidths for the optional gain section
  constant c_gain_requantize_in_w : natural := c_adder_out_w;  -- ignore the accumulator bitgrowth when using gain
  constant c_mm_gain_ctrl_w       : natural := 16;
  constant c_gain_in_w            : natural := 18;
  constant c_gain_out_w           : natural := 18;
  constant c_gain_requantize_lsb_w: natural := c_gain_requantize_in_w - c_gain_in_w;
  -- bitwidths for the output requantizer section
  constant c_requantize_in_w      : natural := sel_a_b(g_use_gain_ctrl, c_gain_out_w, c_accum_out_w);
  constant c_lsb_w                : natural := c_requantize_in_w - g_out_data_w;

  signal iquv_i_sosi              : t_dp_sosi_arr(0 to g_nof_streams - 1) := (others => c_dp_sosi_rst);
  signal iquv_q_sosi              : t_dp_sosi_arr(0 to g_nof_streams - 1) := (others => c_dp_sosi_rst);
  signal iquv_u_sosi              : t_dp_sosi_arr(0 to g_nof_streams - 1) := (others => c_dp_sosi_rst);
  signal iquv_v_sosi              : t_dp_sosi_arr(0 to g_nof_streams - 1) := (others => c_dp_sosi_rst);
  signal iquv_valid               : std_logic_vector(0 downto 0);
  signal iquv_valid_dly           : std_logic_vector(0 downto 0);
  signal iquv_i_linear            : std_logic_vector(c_adder_input_w - 1 downto 0) := (others => '0');
  signal iquv_q_linear            : std_logic_vector(c_adder_input_w - 1 downto 0) := (others => '0');
  signal iquv_u_linear            : std_logic_vector(c_adder_input_w - 1 downto 0) := (others => '0');
  signal iquv_v_linear            : std_logic_vector(c_adder_input_w - 1 downto 0) := (others => '0');
  signal i_summed                 : std_logic_vector(c_adder_out_w - 1 downto 0) := (others => '0');
  signal q_summed                 : std_logic_vector(c_adder_out_w - 1 downto 0) := (others => '0');
  signal u_summed                 : std_logic_vector(c_adder_out_w - 1 downto 0) := (others => '0');
  signal v_summed                 : std_logic_vector(c_adder_out_w - 1 downto 0) := (others => '0');
  signal i_sum_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal q_sum_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal u_sum_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal v_sum_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal dp_gain_i_snk_in         : t_dp_sosi := c_dp_sosi_rst;
  signal dp_gain_q_snk_in         : t_dp_sosi := c_dp_sosi_rst;
  signal dp_gain_u_snk_in         : t_dp_sosi := c_dp_sosi_rst;
  signal dp_gain_v_snk_in         : t_dp_sosi := c_dp_sosi_rst;
  signal dp_requantize_i_snk_in   : t_dp_sosi := c_dp_sosi_rst;
  signal dp_requantize_q_snk_in   : t_dp_sosi := c_dp_sosi_rst;
  signal dp_requantize_u_snk_in   : t_dp_sosi := c_dp_sosi_rst;
  signal dp_requantize_v_snk_in   : t_dp_sosi := c_dp_sosi_rst;

  -- Diagnostic signals to make signal flow clearer in simulation
  type t_diag_iquv_arr is array (0 to g_nof_streams - 1) of std_logic_vector(c_iquv_out_w - 1 downto 0);
  type t_diag is record
    valid_in        : std_logic;
    data_in_arr     : t_diag_iquv_arr;
    data_sum_out    : std_logic_vector(c_adder_out_w - 1 downto 0);
    data_accum_out  : std_logic_vector(c_accum_out_w - 1 downto 0);
    valid_accum_out : std_logic;
    data_gain_in    : std_logic_vector(c_gain_in_w - 1 downto 0);
    valid_gain_in   : std_logic;
    mm_gain_ctrl    : std_logic_vector(c_mm_gain_ctrl_w - 1 downto 0);
    data_gain_out   : std_logic_vector(c_gain_out_w - 1 downto 0);
    valid_gain_out  : std_logic;
  end record;
  signal diag_i : t_diag;
  signal diag_q : t_diag;
  signal diag_u : t_diag;
  signal diag_v : t_diag;
  type t_diag_in_complex_arr is array (0 to 2 * g_nof_streams - 1) of std_logic_vector(g_in_data_w - 1 downto 0);
  signal diag_in_complex_arr : t_diag_in_complex_arr;
begin
  ------------------------------------------------------------------------------
  -- Calculate the integrated I, Q, U and V for each input stream
  ------------------------------------------------------------------------------
  gen_iquv : for stream in 0 to g_nof_streams - 1 generate
    dut : entity work.iquv
    generic map (
      g_sim              => g_sim,
      g_technology       => c_tech_select_default,
      g_use_accum        => c_accum_in_iquv,
      g_nof_int          => g_nof_int,
      g_in_data_w        => g_in_data_w,
      g_out_data_w       => c_iquv_out_w
    )
    port map (
      dp_rst     => dp_rst,
      dp_clk     => dp_clk,
      in_complex => in_complex_arr(stream),
      i_out      => iquv_i_sosi(stream),
      q_out      => iquv_q_sosi(stream),
      u_out      => iquv_u_sosi(stream),
      v_out      => iquv_v_sosi(stream)
    );

    iquv_i_linear((stream + 1) * c_iquv_out_w - 1 downto stream * c_iquv_out_w) <= iquv_i_sosi(stream).data(c_iquv_out_w - 1 downto 0);
    iquv_q_linear((stream + 1) * c_iquv_out_w - 1 downto stream * c_iquv_out_w) <= iquv_q_sosi(stream).data(c_iquv_out_w - 1 downto 0);
    iquv_u_linear((stream + 1) * c_iquv_out_w - 1 downto stream * c_iquv_out_w) <= iquv_u_sosi(stream).data(c_iquv_out_w - 1 downto 0);
    iquv_v_linear((stream + 1) * c_iquv_out_w - 1 downto stream * c_iquv_out_w) <= iquv_v_sosi(stream).data(c_iquv_out_w - 1 downto 0);
  end generate;

  ------------------------------------------------------------------------------
  -- Add up all the I, Q, U and V values from all the streams
  ------------------------------------------------------------------------------
  u_common_adder_i : entity common_lib.common_adder_tree
  generic map (
    g_representation   => "UNSIGNED",
    g_nof_inputs       => g_nof_streams,
    g_dat_w            => c_iquv_out_w,
    g_sum_w            => c_adder_out_w
  )
  port map (
    clk       => dp_clk,
    in_dat    => iquv_i_linear,
    sum       => i_summed
  );

  u_common_adder_q : entity common_lib.common_adder_tree
  generic map (
    g_representation   => "SIGNED",
    g_nof_inputs       => g_nof_streams,
    g_dat_w            => c_iquv_out_w,
    g_sum_w            => c_adder_out_w
  )
  port map (
    clk       => dp_clk,
    in_dat    => iquv_q_linear,
    sum       => q_summed
  );

  u_common_adder_u : entity common_lib.common_adder_tree
  generic map (
    g_representation   => "SIGNED",
    g_nof_inputs       => g_nof_streams,
    g_dat_w            => c_iquv_out_w,
    g_sum_w            => c_adder_out_w
  )
  port map (
    clk       => dp_clk,
    in_dat    => iquv_u_linear,
    sum       => u_summed
  );

  u_common_adder_v : entity common_lib.common_adder_tree
  generic map (
    g_representation   => "SIGNED",
    g_nof_inputs       => g_nof_streams,
    g_dat_w            => c_iquv_out_w,
    g_sum_w            => c_adder_out_w
  )
  port map (
    clk       => dp_clk,
    in_dat    => iquv_v_linear,
    sum       => v_summed
  );

  ------------------------------------------------------------------------------
  -- Delay the valid signal to match the pipeline delay through the adders
  ------------------------------------------------------------------------------
  iquv_valid(0) <= iquv_i_sosi(0).valid;

  u_delay_valid : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => 1,
    g_out_dat_w   => 1,
    g_reset_value => 0,
    g_pipeline    => c_adder_delay
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,
    in_dat        => iquv_valid,
    out_dat       => iquv_valid_dly
  );

  ------------------------------------------------------------------------------
  -- Optional accumulators to integrate the combined Stokes products
  ------------------------------------------------------------------------------
  gen_iquv_accum : if g_use_accum = true generate
    u_iquv_accum : entity work.iquv_accum
    generic map (
      g_accumcount_w  => c_accumcount_w,
      g_in_data_w     => c_adder_out_w
    )
    port map (
      dp_rst          => dp_rst,
      dp_clk          => dp_clk,
      i_in            => i_summed,
      q_in            => q_summed,
      u_in            => u_summed,
      v_in            => v_summed,
      in_valid        => iquv_valid_dly(0),
      i_out_sosi      => i_sum_raw_sosi,
      q_out_sosi      => q_sum_raw_sosi,
      u_out_sosi      => u_sum_raw_sosi,
      v_out_sosi      => v_sum_raw_sosi
    );
  end generate;

  gen_noiquv_accum : if g_use_accum = false generate
    i_sum_raw_sosi.data(c_adder_out_w - 1 downto 0)   <= i_summed;
    q_sum_raw_sosi.data(c_adder_out_w - 1 downto 0)   <= q_summed;
    u_sum_raw_sosi.data(c_adder_out_w - 1 downto 0)   <= u_summed;
    v_sum_raw_sosi.data(c_adder_out_w - 1 downto 0)   <= v_summed;
    i_sum_raw_sosi.valid <= iquv_valid_dly(0);
    q_sum_raw_sosi.valid <= iquv_valid_dly(0);
    u_sum_raw_sosi.valid <= iquv_valid_dly(0);
    v_sum_raw_sosi.valid <= iquv_valid_dly(0);
  end generate;

  ------------------------------------------------------------------------------
  -- Optional fine gain control
  -- First the 25 bit data from the accumulator is truncated to 22 and requantized to 18
  -- Then the I,Q,U and V streams are multiplied by gain values from the mm control
  ------------------------------------------------------------------------------

  gen_gain_ctrl : if g_use_gain_ctrl = true generate
    -- I
    u_dp_gain_requantize_i : entity dp_lib.dp_requantize
    generic map (
      g_complex             => false,
      g_representation      => "UNSIGNED",
      g_lsb_w               => c_gain_requantize_lsb_w,
      g_lsb_round           => true,
      g_lsb_round_clip      => false,
      g_msb_clip            => true,
      g_msb_clip_symmetric  => false,
      g_gain_w              => 0,
      g_pipeline_remove_lsb => 1,
      g_pipeline_remove_msb => 0,
      g_in_dat_w            => c_gain_requantize_in_w,
      g_out_dat_w           => c_gain_in_w
    )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,
      snk_in     => i_sum_raw_sosi,
      src_out    => dp_gain_i_snk_in,
      out_ovr    => open
    );

    u_dp_gain_i : entity dp_lib.mms_dp_gain
    generic map (
      g_technology    => g_technology,
      g_complex_data  => false,
      g_complex_gain  => false,
      g_gain_init_re  => 1,
      g_gain_w        => c_mm_gain_ctrl_w,
      g_in_dat_w      => c_gain_in_w,
      g_out_dat_w     => c_gain_out_w  -- Todo: Check that the MSBs get truncated correctly
    )
    port map
    (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      reg_gain_re_mosi  => reg_dp_gain_i_mosi,
      reg_gain_re_miso  => reg_dp_gain_i_miso,
      in_sosi           => dp_gain_i_snk_in,
      out_sosi          => dp_requantize_i_snk_in
    );

    -- Q
    u_dp_gain_requantize_q : entity dp_lib.dp_requantize
    generic map (
      g_complex             => false,
      g_representation      => "SIGNED",
      g_lsb_w               => c_gain_requantize_lsb_w,
      g_lsb_round           => true,
      g_lsb_round_clip      => false,
      g_msb_clip            => false,
      g_msb_clip_symmetric  => false,
      g_gain_w              => 0,
      g_pipeline_remove_lsb => 1,
      g_pipeline_remove_msb => 0,
      g_in_dat_w            => c_gain_requantize_in_w,
      g_out_dat_w           => c_gain_in_w
    )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,
      snk_in     => q_sum_raw_sosi,
      src_out    => dp_gain_q_snk_in,
      out_ovr    => open
    );

    u_dp_gain_q : entity dp_lib.mms_dp_gain
    generic map (
      g_technology    => g_technology,
      g_complex_data  => false,
      g_complex_gain  => false,
      g_gain_init_re  => 1,
      g_gain_w        => c_mm_gain_ctrl_w,
      g_in_dat_w      => c_gain_in_w,
      g_out_dat_w     => c_gain_out_w
    )
    port map
    (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      reg_gain_re_mosi  => reg_dp_gain_q_mosi,
      reg_gain_re_miso  => reg_dp_gain_q_miso,
      in_sosi           => dp_gain_q_snk_in,
      out_sosi          => dp_requantize_q_snk_in
    );

    -- U
    u_dp_gain_requantize_u : entity dp_lib.dp_requantize
    generic map (
      g_complex             => false,
      g_representation      => "SIGNED",
      g_lsb_w               => c_gain_requantize_lsb_w,
      g_lsb_round           => true,
      g_lsb_round_clip      => false,
      g_msb_clip            => false,
      g_msb_clip_symmetric  => false,
      g_gain_w              => 0,
      g_pipeline_remove_lsb => 1,
      g_pipeline_remove_msb => 0,
      g_in_dat_w            => c_gain_requantize_in_w,
      g_out_dat_w           => c_gain_in_w
    )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,
      snk_in     => u_sum_raw_sosi,
      src_out    => dp_gain_u_snk_in,
      out_ovr    => open
    );

    u_dp_gain_u : entity dp_lib.mms_dp_gain
    generic map (
      g_technology    => g_technology,
      g_complex_data  => false,
      g_complex_gain  => false,
      g_gain_init_re  => 1,
      g_gain_w        => c_mm_gain_ctrl_w,
      g_in_dat_w      => c_gain_in_w,
      g_out_dat_w     => c_gain_out_w
    )
    port map
    (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      reg_gain_re_mosi  => reg_dp_gain_u_mosi,
      reg_gain_re_miso  => reg_dp_gain_u_miso,
      in_sosi           => dp_gain_u_snk_in,
      out_sosi          => dp_requantize_u_snk_in
    );

    -- V
    u_dp_gain_requantize_v : entity dp_lib.dp_requantize
    generic map (
      g_complex             => false,
      g_representation      => "SIGNED",
      g_lsb_w               => c_gain_requantize_lsb_w,
      g_lsb_round           => true,
      g_lsb_round_clip      => false,
      g_msb_clip            => false,
      g_msb_clip_symmetric  => false,
      g_gain_w              => 0,
      g_pipeline_remove_lsb => 1,
      g_pipeline_remove_msb => 0,
      g_in_dat_w            => c_gain_requantize_in_w,
      g_out_dat_w           => c_gain_in_w
    )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,
      snk_in     => v_sum_raw_sosi,
      src_out    => dp_gain_v_snk_in,
      out_ovr    => open
    );

    u_dp_gain_v : entity dp_lib.mms_dp_gain
    generic map (
      g_technology    => g_technology,
      g_complex_data  => false,
      g_complex_gain  => false,
      g_gain_init_re  => 1,
      g_gain_w        => c_mm_gain_ctrl_w,
      g_in_dat_w      => c_gain_in_w,
      g_out_dat_w     => c_gain_out_w
    )
    port map
    (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      reg_gain_re_mosi  => reg_dp_gain_v_mosi,
      reg_gain_re_miso  => reg_dp_gain_v_miso,
      in_sosi           => dp_gain_v_snk_in,
      out_sosi          => dp_requantize_v_snk_in
    );
  end generate;

  gen_nogain_ctrl : if g_use_gain_ctrl = false generate
    dp_requantize_i_snk_in <= i_sum_raw_sosi;
    dp_requantize_q_snk_in <= q_sum_raw_sosi;
    dp_requantize_u_snk_in <= u_sum_raw_sosi;
    dp_requantize_v_snk_in <= v_sum_raw_sosi;
  end generate;

  ------------------------------------------------------------------------------
  -- Requantize the outputs to the desired bit width
  ------------------------------------------------------------------------------
  u_dp_requantize_i : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_requantize_in_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => dp_requantize_i_snk_in,
    src_out    => i_out,
    out_ovr    => open
  );

  u_dp_requantize_q : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_requantize_in_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => dp_requantize_q_snk_in,
    src_out    => q_out,
    out_ovr    => open
  );

  u_dp_requantize_u : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_requantize_in_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => dp_requantize_u_snk_in,
    src_out    => u_out,
    out_ovr    => open
  );

  u_dp_requantize_v : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_requantize_in_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => dp_requantize_v_snk_in,
    src_out    => v_out,
    out_ovr    => open
  );

  -------------------------------------------------------------------------------
  -- Diagnostic to display the I signal flow in simulation
  -------------------------------------------------------------------------------
  gen_diag : if g_sim = true generate
    gen_diag_inputs : for stream in 0 to g_nof_streams - 1 generate
      diag_in_complex_arr(stream * 2)     <= in_complex_arr(stream).re(g_in_data_w - 1 downto 0);
      diag_in_complex_arr(stream * 2 + 1) <= in_complex_arr(stream).im(g_in_data_w - 1 downto 0);
      diag_i.data_in_arr(stream) <= iquv_i_sosi(stream).data(c_iquv_out_w - 1 downto 0);
      diag_q.data_in_arr(stream) <= iquv_q_sosi(stream).data(c_iquv_out_w - 1 downto 0);
      diag_u.data_in_arr(stream) <= iquv_u_sosi(stream).data(c_iquv_out_w - 1 downto 0);
      diag_v.data_in_arr(stream) <= iquv_v_sosi(stream).data(c_iquv_out_w - 1 downto 0);
    end generate;

    diag_i.valid_in              <= iquv_i_sosi(0).valid;
    diag_q.valid_in              <= iquv_q_sosi(0).valid;
    diag_u.valid_in              <= iquv_u_sosi(0).valid;
    diag_v.valid_in              <= iquv_v_sosi(0).valid;
    diag_i.data_sum_out          <= i_summed(c_adder_out_w - 1 downto 0);
    diag_q.data_sum_out          <= q_summed(c_adder_out_w - 1 downto 0);
    diag_u.data_sum_out          <= u_summed(c_adder_out_w - 1 downto 0);
    diag_v.data_sum_out          <= v_summed(c_adder_out_w - 1 downto 0);
    diag_i.data_accum_out        <= i_sum_raw_sosi.data(c_accum_out_w - 1 downto 0);
    diag_q.data_accum_out        <= q_sum_raw_sosi.data(c_accum_out_w - 1 downto 0);
    diag_u.data_accum_out        <= u_sum_raw_sosi.data(c_accum_out_w - 1 downto 0);
    diag_v.data_accum_out        <= v_sum_raw_sosi.data(c_accum_out_w - 1 downto 0);
    diag_i.valid_accum_out       <= i_sum_raw_sosi.valid;
    diag_q.valid_accum_out       <= q_sum_raw_sosi.valid;
    diag_u.valid_accum_out       <= u_sum_raw_sosi.valid;
    diag_v.valid_accum_out       <= v_sum_raw_sosi.valid;
    diag_i.data_gain_in          <= dp_gain_i_snk_in.data(c_gain_in_w - 1 downto 0);
    diag_q.data_gain_in          <= dp_gain_q_snk_in.data(c_gain_in_w - 1 downto 0);
    diag_u.data_gain_in          <= dp_gain_u_snk_in.data(c_gain_in_w - 1 downto 0);
    diag_v.data_gain_in          <= dp_gain_v_snk_in.data(c_gain_in_w - 1 downto 0);
    diag_i.valid_gain_in         <= dp_gain_i_snk_in.valid;
    diag_q.valid_gain_in         <= dp_gain_q_snk_in.valid;
    diag_u.valid_gain_in         <= dp_gain_u_snk_in.valid;
    diag_v.valid_gain_in         <= dp_gain_v_snk_in.valid;
    diag_i.mm_gain_ctrl          <= reg_dp_gain_i_mosi.wrdata(c_mm_gain_ctrl_w - 1 downto 0);
    diag_q.mm_gain_ctrl          <= reg_dp_gain_q_mosi.wrdata(c_mm_gain_ctrl_w - 1 downto 0);
    diag_u.mm_gain_ctrl          <= reg_dp_gain_u_mosi.wrdata(c_mm_gain_ctrl_w - 1 downto 0);
    diag_v.mm_gain_ctrl          <= reg_dp_gain_v_mosi.wrdata(c_mm_gain_ctrl_w - 1 downto 0);
    diag_i.data_gain_out         <= dp_requantize_i_snk_in.data(c_gain_out_w - 1 downto 0);
    diag_q.data_gain_out         <= dp_requantize_q_snk_in.data(c_gain_out_w - 1 downto 0);
    diag_u.data_gain_out         <= dp_requantize_u_snk_in.data(c_gain_out_w - 1 downto 0);
    diag_v.data_gain_out         <= dp_requantize_v_snk_in.data(c_gain_out_w - 1 downto 0);
    diag_i.valid_gain_out        <= dp_requantize_i_snk_in.valid;
    diag_q.valid_gain_out        <= dp_requantize_q_snk_in.valid;
    diag_u.valid_gain_out        <= dp_requantize_u_snk_in.valid;
    diag_v.valid_gain_out        <= dp_requantize_v_snk_in.valid;
  end generate;
end str;
