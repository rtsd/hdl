-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Output frame busy control signal that is active from sop to eop
-- Description:
--   The busy is active during the entire frame from sop to eop, so busy
--   remains active in case valid goes low during a frame.
--   Default use g_pipeline=0 to have snk_in_busy in phase with sop and eop.
--   Use g_pipeline > 0 to register snk_in_busy to ease timing closure.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_frame_busy is
  generic (
    g_pipeline : natural := 0
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    snk_in        : in  t_dp_sosi;
    snk_in_busy   : out std_logic
  );
end dp_frame_busy;

architecture str of dp_frame_busy is
  signal busy : std_logic;
begin
  u_common_switch : entity common_lib.common_switch
  generic map (
    g_rst_level    => '0',  -- Defines the output level at reset.
    g_priority_lo  => true,  -- When TRUE then input switch_low has priority, else switch_high. Don't care when switch_high and switch_low are pulses that do not occur simultaneously.
    g_or_high      => true,  -- When TRUE and priority hi then the registered switch_level is OR-ed with the input switch_high to get out_level, else out_level is the registered switch_level
    g_and_low      => false  -- When TRUE and priority lo then the registered switch_level is AND-ed with the input switch_low to get out_level, else out_level is the registered switch_level
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => snk_in.sop,  -- A pulse on switch_high makes the out_level go high
    switch_low  => snk_in.eop,  -- A pulse on switch_low makes the out_level go low
    out_level   => busy
  );

  u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline       => g_pipeline,  -- 0 for wires, > 0 for registers,
    g_reset_value    => 0,  -- 0 or 1, bit reset value,
    g_out_invert     => false
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => busy,
    out_dat => snk_in_busy
  );
end str;
