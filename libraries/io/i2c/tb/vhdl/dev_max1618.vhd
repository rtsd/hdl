-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.i2c_dev_max1617_pkg.all;

entity dev_max1618 is
  generic(
    g_address   : std_logic_vector(6 downto 0)
  );
  port(
    scl         : in    std_logic;
    sda         : inout std_logic;
    temp        : in    integer
  );
end dev_max1618;

architecture beh of dev_max1618 is
  signal enable    : std_logic;  -- enable
  signal stop      : std_logic;  -- stop
  signal wr_dat    : std_logic_vector(7 downto 0);  -- I2C write data
  signal wr_val    : std_logic;
  signal rd_dat    : std_logic_vector(7 downto 0);  -- I2C read data
  signal rd_req    : std_logic;

  signal cmd_en    : std_logic := '0';
  signal cmd       : natural;  -- device command

  signal config_reg   : std_logic_vector(7 downto 0) := "00001000";
  signal status_reg   : std_logic_vector(7 downto 0) := (others => '0');
  signal temp_hi_reg  : std_logic_vector(7 downto 0) := "01111111";
  signal temp_lo_reg  : std_logic_vector(7 downto 0) := "11001001";
begin
  i2c_slv_device : entity work.i2c_slv_device
  generic map (
    g_address => g_address
  )
  port map (
    scl      => scl,
    sda     => sda,
    en      => enable,
    p       => stop,
    wr_dat  => wr_dat,
    wr_val  => wr_val,
    rd_req  => rd_req,
    rd_dat  => rd_dat
  );

  -- Model only config thermostat mode
  status_reg(MAX1617_STATUS_RHIGH_BI) <= '1' when temp >= unsigned(temp_hi_reg) else '0' when temp <= unsigned(temp_lo_reg);  -- ELSE latch
  status_reg(MAX1617_STATUS_RLOW_BI)  <= '1' when temp <= unsigned(temp_lo_reg) else '0' when temp >= unsigned(temp_hi_reg);  -- ELSE latch

  p_write : process (enable, wr_val)  -- first write byte is treated as command
  begin
    if rising_edge(enable) then
      cmd_en <= '1';
    elsif falling_edge(enable) then
      cmd_en <= '0';
    end if;
    if rising_edge(wr_val) then
      cmd_en <= '0';
      if cmd_en = '1' then
        cmd <= to_integer(unsigned(wr_dat));
      else
        case cmd is  -- only model some write cmd
          when MAX1617_CMD_WRITE_CONFIG      => config_reg  <= wr_dat;
          when MAX1617_CMD_WRITE_REMOTE_HIGH => temp_hi_reg <= wr_dat;
          when MAX1617_CMD_WRITE_REMOTE_LOW  => temp_lo_reg <= wr_dat;
          when others                        => null;
        end case;
      end if;
    end if;
  end process;

  p_read : process (rd_req)
  begin
    if rising_edge(rd_req) then
      case cmd is  -- only model some read cmd
        when MAX1617_CMD_READ_REMOTE_TEMP  => rd_dat <= std_logic_vector(to_signed(temp, 8));
        when MAX1617_CMD_READ_CONFIG       => rd_dat <= config_reg;
        when MAX1617_CMD_READ_STATUS       => rd_dat <= status_reg;
        when MAX1617_CMD_READ_REMOTE_HIGH  => rd_dat <= temp_hi_reg;
        when MAX1617_CMD_READ_REMOTE_LOW   => rd_dat <= temp_lo_reg;
        when others                        => rd_dat <= (others => '1');
      end case;
    end if;
  end process;
end beh;
