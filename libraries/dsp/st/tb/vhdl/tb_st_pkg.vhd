-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. vd Walle
-- Purpose:
-- Functions used in st_lib
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package tb_st_pkg is
  function func_st_calculate_expected_xsq(a_re, a_im, b_re, b_im : t_integer_arr; N_crosslets, N_int : natural) return t_integer_arr;
end tb_st_pkg;

package body tb_st_pkg is
  function func_st_calculate_expected_xsq(a_re, a_im, b_re, b_im : t_integer_arr; N_crosslets, N_int : natural) return t_integer_arr is
    constant c_N_s : natural := a_re'length / N_crosslets;
    constant c_xsq : natural := c_N_s * c_N_s;
    constant c_N_stat : natural := N_crosslets * c_xsq;
    variable v_exp_xsq : t_integer_arr(0 to c_nof_complex * c_N_stat - 1);
  begin

    for N in 0 to N_crosslets - 1 loop
      for I in 0 to c_N_s - 1 loop
        for J in 0 to c_N_s - 1 loop
          v_exp_xsq(c_nof_complex * (N * c_xsq + I * c_N_s + J)    ) := N_int * COMPLEX_MULT_REAL(a_re(N * c_N_s + I), a_im(N * c_N_s + I), b_re(N * c_N_s + J), -1 * b_im(N * c_N_s + J));
          v_exp_xsq(c_nof_complex * (N * c_xsq + I * c_N_s + J) + 1) := N_int * COMPLEX_MULT_IMAG(a_re(N * c_N_s + I), a_im(N * c_N_s + I), b_re(N * c_N_s + J), -1 * b_im(N * c_N_s + J));
        end loop;

      end loop;
    end loop;
    return v_exp_xsq;
  end;
end tb_st_pkg;
