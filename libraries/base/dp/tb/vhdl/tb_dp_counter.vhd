-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 23 Apr 2018
-- Purpose:
-- . Test bench for dp_counter.
-- Description:
--   Functional details are verified by tb_dp_counter_func.vhd. This
--   tb_dp_counter.vhd shows the dp_counter with pipelining.
--Usage:
-- . as 10
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_counter is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- dut
    g_pipeline_src_out : natural := 1;  -- Pipeline source outputs (data,valid,sop,eop etc)
    g_pipeline_src_in  : natural := 0;  -- Pipeline source inputs (ready,xon). This will also pipeline src_out.
    g_nof_counters     : natural := 3;
    -- min range = [0,2,1] => (0,1) 'the Python way'
    g_range_start      : t_nat_natural_arr(9 downto 0) := (0, 0, 0, 0, 0, 0, 0, 1, 0, 0);
    g_range_stop       : t_nat_natural_arr(9 downto 0) := (2, 2, 2, 2, 2, 2, 7, 16, 16, 1);
    g_range_step       : t_nat_natural_arr(9 downto 0) := (1, 1, 1, 1, 1, 1, 2, 2, 2, 1)
  );
end tb_dp_counter;

architecture tb of tb_dp_counter is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period : time := 5 ns;
  constant c_max_count_w : natural := 32;

  constant c_rl          : natural := 1;
  constant c_data_w      : natural := 32;
  constant c_data_init   : natural := 0;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal tb_end         : std_logic := '0';

  ------------------------------------------------------------------------------
  -- dp_counter
  ------------------------------------------------------------------------------
  signal snk_in            : t_dp_sosi := c_dp_sosi_rst;
  signal snk_out           : t_dp_siso;
  signal src_out           : t_dp_sosi;
  signal src_in            : t_dp_siso := c_dp_siso_rdy;
  signal count_src_out_arr : t_dp_sosi_arr(g_nof_counters - 1 downto 0);
  signal period            : natural;

  function calculate_period(g_counter : natural) return natural is
    variable v_range_period : t_nat_natural_arr(g_counter downto 0);
    variable v_period       : natural := 1;
  begin
    for I in 0 to g_counter loop
      v_range_period(I) := (g_range_stop(I) - 1 - g_range_start(I)) / g_range_step(I) + 1;  -- Python style range(start, stop, step)
      v_period := v_period * v_range_period(I);
    end loop;
    return v_period;
  end;

  ------------------------------------------------------------------------------
  -- flow control
  ------------------------------------------------------------------------------
  constant c_in_random_w       : natural := 15;
  constant c_out_random_w      : natural := 16;  -- use different length per random source
  constant c_in_pulse_active   : natural := 1;
  constant c_out_pulse_active  : natural := 1;
  constant c_in_pulse_period   : natural := 2;
  constant c_out_pulse_period  : natural := 2;

  signal in_random           : std_logic_vector(c_in_random_w - 1 downto 0) := TO_UVEC(1, c_in_random_w);
  signal out_random          : std_logic_vector(c_out_random_w - 1 downto 0) := TO_UVEC(1, c_out_random_w);
  signal in_pulse            : std_logic;
  signal out_pulse           : std_logic;
  signal in_pulse_en         : std_logic := '1';
  signal out_pulse_en        : std_logic := '1';

  signal in_en          : std_logic := '0';
  signal out_ready      : std_logic := '0';
begin
  ------------------------------------------------------------------------------
  -- Flow control
  ------------------------------------------------------------------------------
  in_random <= func_common_random(in_random) when rising_edge(clk);
  out_random <= func_common_random(out_random) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_in_pulse_active, c_in_pulse_period, '1', rst, clk, in_pulse_en, in_pulse);
  proc_common_gen_duty_pulse(c_out_pulse_active, c_out_pulse_period, '1', rst, clk, out_pulse_en, out_pulse);

  in_en <= '1'                       when g_flow_control_stimuli = e_active else
           in_random(in_random'high) when g_flow_control_stimuli = e_random else
           in_pulse                  when g_flow_control_stimuli = e_pulse;

  out_ready <= '1'                         when g_flow_control_verify = e_active else
               out_random(out_random'high) when g_flow_control_verify = e_random else
               out_pulse                   when g_flow_control_verify = e_pulse;

  src_in.ready <= out_ready;
  src_in.xon <= not src_in.xon when rising_edge(clk);  -- should have no effect, only passed on from src_in to snk_out

  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in incrementing data with valid
  proc_dp_gen_data(c_rl,
                   c_data_w,
                   c_data_init,
                   rst,
                   clk,
                   in_en,
                   snk_out,
                   snk_in);

  p_stimuli : process
  begin
    -- run some more intervals for slowest counter, to more view how the slowest counter behaves
    for I in 0 to 2 loop
      proc_common_wait_until_hi_lo(clk, count_src_out_arr(g_nof_counters - 1).eop);  -- wait for carry over
    end loop;

    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dp_counter : entity work.dp_counter
  generic map (
    g_nof_counters     => g_nof_counters,
    g_range_start      => g_range_start,
    g_range_stop       => g_range_stop,
    g_range_step       => g_range_step,
    g_pipeline_src_out => g_pipeline_src_out,
    g_pipeline_src_in  => g_pipeline_src_in
  )
  port map (
    rst               => rst,
    clk               => clk,

    snk_in            => snk_in,
    snk_out           => snk_out,

    src_out           => src_out,
    src_in            => src_in,

    count_src_out_arr => count_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- Verification
  --   Verify dp_counter using the counter data from the stimuli as reference.
  --   Only verify the dp_counter for the highest counter, because that covers
  --   also the low level counters. The details of dp_counter_func are tested
  --   in tb_dp_counter_func.vhd.
  ------------------------------------------------------------------------------
  p_verify : process(clk)
    constant c_period  : natural := calculate_period(g_nof_counters - 1);

    variable v_cnt : natural := 0;
  begin
    period <= c_period;  -- to view c_period in wave window
    if rising_edge(clk) then
      if count_src_out_arr(g_nof_counters - 1).valid = '1' then
        assert v_cnt = TO_UINT(src_out.data)
          report "Wrong cnt at valid : " & int_to_str(v_cnt)
          severity ERROR;
        if count_src_out_arr(g_nof_counters - 1).sop = '1' then
          assert v_cnt mod c_period = 0
            report "Wrong cnt at sop : " & int_to_str(v_cnt)
            severity ERROR;
        end if;
        if count_src_out_arr(g_nof_counters - 1).eop = '1' then
          assert v_cnt mod c_period = c_period - 1
            report "Wrong cnt at eop : " & int_to_str(v_cnt)
            severity ERROR;
        end if;
        v_cnt := v_cnt + 1;
      end if;
    end if;
  end process;
end tb;
