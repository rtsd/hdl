###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# This QSF is sourced by other design QSF files.
# ==============================================
# Note: This file can ONLY BE SOURCED (use SOURCE_TCL_SCRIPT_FILE so it will be TCL interpreted), e.g.
# by another QSF, otherwise many TCL commands such as "$::env(HDL_WORK)" do not work.

# Device:
set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name DEVICE 10AX115U4F45I3SGES
set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "1.8 V"
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
#set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
set_global_assignment -name DEVICE_FILTER_SPEED_GRADE FASTEST
#set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 4
set_global_assignment -name ENABLE_OCT_DONE OFF
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
set_global_assignment -name ENABLE_NCE_PIN OFF
set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X4"
#set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X1"
set_global_assignment -name STRATIXII_CONFIGURATION_DEVICE EPCQL1024
set_global_assignment -name USE_CONFIGURATION_DEVICE ON
set_global_assignment -name CRC_ERROR_OPEN_DRAIN ON
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall

set_global_assignment -name CONFIGURATION_VCCIO_LEVEL 1.8V
set_global_assignment -name ON_CHIP_BITSTREAM_DECOMPRESSION ON
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_12_5MHZ
set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_25MHZ
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_100MHZ

set_global_assignment -name USER_START_UP_CLOCK OFF

set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
set_global_assignment -name DEVICE_FILTER_PIN_COUNT 1932

set_global_assignment -name HEX_FILE pm_uc_ES1_ww05p1.hex
set_global_assignment -name SOURCE_FILE quartus.ini

#set_global_assignment -name EDA_BOARD_DESIGN_SIGNAL_INTEGRITY_TOOL "IBIS (Signal Integrity)"
#set_global_assignment -name EDA_OUTPUT_DATA_FORMAT IBIS -section_id eda_board_design_signal_integrity
#set_global_assignment -name EDA_IBIS_SPECIFICATION_VERSION 5P0 -section_id eda_board_design_signal_integrity
set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top


# Optimize for performance:
set_global_assignment -name OPTIMIZATION_TECHNIQUE SPEED
set_global_assignment -name SYNTH_TIMING_DRIVEN_SYNTHESIS ON
set_global_assignment -name OPTIMIZE_HOLD_TIMING "ALL PATHS"
set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING ON
set_global_assignment -name FITTER_EFFORT "STANDARD FIT"
set_global_assignment -name ROUTER_CLOCKING_TOPOLOGY_ANALYSIS ON
set_global_assignment -name PHYSICAL_SYNTHESIS_REGISTER_RETIMING ON
set_global_assignment -name ROUTER_TIMING_OPTIMIZATION_LEVEL MAXIMUM

# To set a location assignment for a PLL, do the following:
# - after compilation, open the chip planner
# - hover over the ATX PLL block (left side or right side)
# - Right click and click "Copy tooltip"
# - Paste text in here and edit
#set_location_assignment HSSIPMALCPLL_X0_Y33_N29 -to "unb2_test:u_revision|unb2_board_10gbe:\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:\gen_phy_24:u_ip_arria10_transceiver_pll_10g_0|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0|a10_xcvr_atx_pll:a10_xcvr_atx_pll_inst|twentynm_atx_pll_inst"



#set_parameter -name dbg_user_identifier 1 -to "\\Generate_XCVR_LANE_INSTANCES:1:xcvr_lane_inst|xcvr_txrx_inst|xcvr_native_a10_0"
#set_parameter -name dbg_user_identifier 0 -to "\\Generate_XCVR_LANE_INSTANCES:0:xcvr_lane_inst|xcvr_pll_inst|xcvr_atx_pll_a10_0"
#set_parameter -name dbg_user_identifier 1 -to "\\Generate_XCVR_LANE_INSTANCES:1:xcvr_lane_inst|xcvr_pll_inst|xcvr_atx_pll_a10_0"

set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_0|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_phy_10gbase_r_12:\\gen_phy_12:u_ip_arria10_phy_10gbase_r_12"
set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_1|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_phy_10gbase_r_12:\\gen_phy_12:u_ip_arria10_phy_10gbase_r_12"

set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_0|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:u_ip_arria10_transceiver_pll_10g|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0"
set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_1|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:u_ip_arria10_transceiver_pll_10g|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0"


# Pass compile stamps as generics (passed to top-level when $UNB_COMPILE_STAMPS is set)
if { [info exists ::env(UNB_COMPILE_STAMPS) ] } {
  set_parameter -name g_stamp_date [clock format [clock seconds] -format {%Y%m%d}]
  set_parameter -name g_stamp_time [clock format [clock seconds] -format {%H%M%S}]
  post_message -type info "RADIOHDL: using SVN $::env(HDL_GIT_REVISION)"
  set_parameter -name g_stamp_svn [regsub -all {[^0-9]} [exec echo $::env(HDL_GIT_REVISION)] ""] 
}

