-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for eth1g
-- Description:
--
--   The p_lcu_transmitter transmits packets and the p_eth_control loops them
--   back to p_lcu_receiver:
--
--                     -------    -------
--                 /---|     |<---|     |<--- p_lcu_transmitter
--   p_eth_control |   | DUT |    | LCU |
--                 \-->|(ETH)|--->|(TSE)|---> p_lcu_receiver
--                     -------    -------
--
--   The tb is self checking based on:
--   . proc_tech_tse_rx_packet() for expected header and data type
--   . tx_pkt_cnt=rx_pkt_cnt > 0 must be true at the tb_end.
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib, technology_lib, eth_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use eth_lib.eth_pkg.all;

entity tb_eth1g is
  -- Test bench control parameters
  generic (
    g_technology_dut : natural := c_tech_select_default;
    g_technology_lcu : natural := c_tech_select_default;
    g_frm_discard_en : boolean := false;  -- when TRUE discard frame types that would otherwise have to be discarded by the Nios MM master
    g_flush_test_en  : boolean := false;  -- when TRUE send many large frames to enforce flush in eth_buffer
    g_tb_end         : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    --   g_data_type = c_tb_tech_tse_data_type_symbols  = 0
    --   g_data_type = c_tb_tech_tse_data_type_counter  = 1
    --   g_data_type = c_tb_tech_tse_data_type_arp      = 2
    --   g_data_type = c_tb_tech_tse_data_type_ping     = 3
    --   g_data_type = c_tb_tech_tse_data_type_udp      = 4
    g_data_type : natural := c_tb_tech_tse_data_type_udp
  );
  port (
    tb_end : out std_logic
  );
end tb_eth1g;

architecture tb of tb_eth1g is
  constant sys_clk_period       : time := 10 ns;  -- 100 MHz
  constant eth_clk_period       : time :=  8 ns;  -- 125 MHz
  constant cable_delay          : time := 12 ns;

  constant c_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain

  -- TSE constants
  constant c_promis_en          : boolean := false;

  constant c_tx_ready_latency   : natural := c_tech_tse_tx_ready_latency;  -- 0, 1 are supported, must match TSE MAC c_tech_tse_tx_ready_latency
  constant c_nof_tx_not_valid   : natural := 0;  -- when > 0 then pull tx valid low for c_nof_tx_not_valid beats during tx

  -- Payload user data
  constant c_tb_nof_data        : natural := 0;  -- nof UDP user data, nof ping padding data
  constant c_tb_ip_nof_data     : natural := c_network_udp_header_len + c_tb_nof_data;  -- nof IP data,
                                          -- also suits ICMP, because c_network_icmp_header_len = c_network_udp_header_len
  constant c_tb_reply_payload   : boolean := true;  -- TRUE copy rx payload into response payload, else header only (e.g. for ARP)

  -- Packet headers
  -- . Ethernet header
  constant c_lcu_src_mac        : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";
  constant c_dut_src_mac        : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC";  -- = 12-34-56-78-9A-BC
  constant c_dut_src_mac_hi     : natural := TO_UINT(c_dut_src_mac(c_network_eth_mac_addr_w - 1 downto c_word_w));
  constant c_dut_src_mac_lo     : natural := TO_UINT(c_dut_src_mac(                c_word_w - 1 downto        0));
  -- support only ARP and IPv4 over ETH
  --                                                             symbols   counter               ARP=0x806               IP=0x800               IP=0x800
  constant c_dut_ethertype      : natural := sel_n(g_data_type, 16#07F0#, 16#07F1#, c_network_eth_type_arp, c_network_eth_type_ip, c_network_eth_type_ip);

  constant c_tx_eth_header      : t_network_eth_header := (dst_mac    => c_dut_src_mac,
                                                           src_mac    => c_lcu_src_mac,
                                                           eth_type   => TO_UVEC(c_dut_ethertype, c_network_eth_type_w));
  constant c_discard_eth_header : t_network_eth_header := (dst_mac    => c_dut_src_mac,
                                                           src_mac    => c_lcu_src_mac,
                                                           eth_type   => TO_UVEC(16#07F0#, c_network_eth_type_w));
  constant c_exp_eth_header     : t_network_eth_header := (dst_mac    => c_tx_eth_header.src_mac,  -- \/
                                                           src_mac    => c_tx_eth_header.dst_mac,  -- /\
                                                           eth_type   => c_tx_eth_header.eth_type);  -- =

  -- . IP header
  constant c_lcu_ip_addr        : natural := 16#05060708#;  -- = 05:06:07:08
  constant c_dut_ip_addr        : natural := 16#01020304#;
  constant c_tb_ip_total_length : natural := c_network_ip_total_length + c_tb_ip_nof_data;

  -- support only ping protocol or UDP protocol over IP
  --                                                          symbols counter  ARP                      ping=1                     UDP=17
  constant c_tb_ip_protocol     : natural := sel_n(g_data_type,    13,     14,  15, c_network_ip_protocol_icmp, c_network_ip_protocol_udp);

  constant c_tx_ip_header       : t_network_ip_header := (version         => TO_UVEC(c_network_ip_version,         c_network_ip_version_w),
                                                          header_length   => TO_UVEC(c_network_ip_header_length,   c_network_ip_header_length_w),
                                                          services        => TO_UVEC(c_network_ip_services,        c_network_ip_services_w),
                                                          total_length    => TO_UVEC(c_tb_ip_total_length,         c_network_ip_total_length_w),
                                                          identification  => TO_UVEC(c_network_ip_identification,  c_network_ip_identification_w),
                                                          flags           => TO_UVEC(c_network_ip_flags,           c_network_ip_flags_w),
                                                          fragment_offset => TO_UVEC(c_network_ip_fragment_offset, c_network_ip_fragment_offset_w),
                                                          time_to_live    => TO_UVEC(c_network_ip_time_to_live,    c_network_ip_time_to_live_w),
                                                          protocol        => TO_UVEC(c_tb_ip_protocol,             c_network_ip_protocol_w),
                                                          header_checksum => TO_UVEC(c_network_ip_header_checksum, c_network_ip_header_checksum_w),  -- init value (or try 0xEBBD = 60349)
                                                          src_ip_addr     => TO_UVEC(c_lcu_ip_addr,                c_network_ip_addr_w),
                                                          dst_ip_addr     => TO_UVEC(c_dut_ip_addr,                c_network_ip_addr_w));

  constant c_exp_ip_header      : t_network_ip_header := (version         => c_tx_ip_header.version,  -- =
                                                          header_length   => c_tx_ip_header.header_length,  -- =
                                                          services        => c_tx_ip_header.services,  -- =
                                                          total_length    => c_tx_ip_header.total_length,  -- =
                                                          identification  => c_tx_ip_header.identification,  -- =
                                                          flags           => c_tx_ip_header.flags,  -- =
                                                          fragment_offset => c_tx_ip_header.fragment_offset,  -- =
                                                          time_to_live    => c_tx_ip_header.time_to_live,  -- =
                                                          protocol        => c_tx_ip_header.protocol,  -- =
                                                          header_checksum => c_tx_ip_header.header_checksum,  -- init value
                                                          src_ip_addr     => c_tx_ip_header.dst_ip_addr,  -- \/
                                                          dst_ip_addr     => c_tx_ip_header.src_ip_addr);  -- /\

  -- . ARP packet
  constant c_tx_arp_packet      : t_network_arp_packet := (htype => TO_UVEC(c_network_arp_htype,        c_network_arp_htype_w),
                                                           ptype => TO_UVEC(c_network_arp_ptype,        c_network_arp_ptype_w),
                                                           hlen  => TO_UVEC(c_network_arp_hlen,         c_network_arp_hlen_w),
                                                           plen  => TO_UVEC(c_network_arp_plen,         c_network_arp_plen_w),
                                                           oper  => TO_UVEC(c_network_arp_oper_request, c_network_arp_oper_w),
                                                           sha   => c_lcu_src_mac,
                                                           spa   => TO_UVEC(c_lcu_ip_addr,              c_network_ip_addr_w),
                                                           tha   => c_dut_src_mac,
                                                           tpa   => TO_UVEC(c_dut_ip_addr,              c_network_ip_addr_w));

  constant c_exp_arp_packet     : t_network_arp_packet := (htype => c_tx_arp_packet.htype,
                                                           ptype => c_tx_arp_packet.ptype,
                                                           hlen  => c_tx_arp_packet.hlen,
                                                           plen  => c_tx_arp_packet.plen,
                                                           oper  => TO_UVEC(c_network_arp_oper_reply, c_network_arp_oper_w),  -- reply
                                                           sha   => c_tx_arp_packet.tha,  -- \/
                                                           spa   => c_tx_arp_packet.tpa,  -- /\  \/
                                                           tha   => c_tx_arp_packet.sha,  -- /  \ /\
                                                           tpa   => c_tx_arp_packet.spa);  -- /  \

  -- . ICMP header
  constant c_tx_icmp_header      : t_network_icmp_header := (msg_type => TO_UVEC(c_network_icmp_msg_type_request, c_network_icmp_msg_type_w),  -- ping request
                                                             code     => TO_UVEC(c_network_icmp_code,             c_network_icmp_code_w),
                                                             checksum => TO_UVEC(c_network_icmp_checksum,         c_network_icmp_checksum_w),  -- init value
                                                             id       => TO_UVEC(c_network_icmp_id,               c_network_icmp_id_w),
                                                             sequence => TO_UVEC(c_network_icmp_sequence,         c_network_icmp_sequence_w));
  constant c_exp_icmp_header     : t_network_icmp_header := (msg_type => TO_UVEC(c_network_icmp_msg_type_reply,   c_network_icmp_msg_type_w),  -- ping reply
                                                             code     => c_tx_icmp_header.code,
                                                             checksum => c_tx_icmp_header.checksum,  -- init value
                                                             id       => c_tx_icmp_header.id,
                                                             sequence => c_tx_icmp_header.sequence);

  -- . UDP header
  constant c_dut_udp_port_ctrl   : natural := 11;  -- ETH demux UDP for control
  constant c_dut_udp_port_st0    : natural := 57;  -- ETH demux UDP port 0
  constant c_dut_udp_port_st1    : natural := 58;  -- ETH demux UDP port 1
  constant c_dut_udp_port_st2    : natural := 59;  -- ETH demux UDP port 2
  constant c_dut_udp_port_en     : natural := 16#10000#;  -- ETH demux UDP port enable bit 16
  constant c_lcu_udp_port        : natural := 10;  -- UDP port used for src_port
  constant c_dut_udp_port_st     : natural := c_dut_udp_port_st0;  -- UDP port used for dst_port
  constant c_tb_udp_total_length : natural := c_network_udp_total_length + c_tb_nof_data;
  constant c_tx_udp_header       : t_network_udp_header := (src_port     => TO_UVEC(c_lcu_udp_port,         c_network_udp_port_w),
                                                            dst_port     => TO_UVEC(c_dut_udp_port_ctrl,    c_network_udp_port_w),  -- or use c_dut_udp_port_st#
                                                            total_length => TO_UVEC(c_tb_udp_total_length,  c_network_udp_total_length_w),
                                                            checksum     => TO_UVEC(c_network_udp_checksum, c_network_udp_checksum_w));  -- init value

  constant c_exp_udp_header      : t_network_udp_header := (src_port     => c_tx_udp_header.dst_port,  -- \/
                                                            dst_port     => c_tx_udp_header.src_port,  -- /\
                                                            total_length => c_tx_udp_header.total_length,  -- =
                                                            checksum     => c_tx_udp_header.checksum);  -- init value

  signal tx_total_header     : t_network_total_header;  -- transmitted packet header
  signal discard_total_header: t_network_total_header;  -- transmitted packet header for to be discarded packet
  signal exp_total_header    : t_network_total_header;  -- expected received packet header

  -- ETH control
  constant c_dut_control_rx_en   : natural := 2**c_eth_mm_reg_control_bi.rx_en;
  constant c_dut_control_tx_en   : natural := 2**c_eth_mm_reg_control_bi.tx_en;

  -- Clocks and reset
  signal eth_clk             : std_logic := '0';  -- tse reference clock
  signal sys_clk             : std_logic := '0';  -- system clock
  signal st_clk              : std_logic;  -- stream clock
  signal st_rst              : std_logic := '1';  -- reset synchronous with st_clk
  signal mm_clk              : std_logic;  -- memory-mapped bus clock
  signal mm_rst              : std_logic := '1';  -- reset synchronous with mm_clk

  -- ETH TSE interface
  signal dut_tse_init        : std_logic := '1';
  signal dut_eth_init        : std_logic := '1';
  signal eth_tse_miso        : t_mem_miso;
  signal eth_tse_mosi        : t_mem_mosi;
  signal eth_psc_access      : std_logic;
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;
  signal eth_led             : t_tech_tse_led;

  -- ETH MM registers interface
  signal eth_reg_miso        : t_mem_miso;
  signal eth_reg_mosi        : t_mem_mosi;
  signal eth_reg_interrupt   : std_logic;

  signal eth_mm_reg_control  : t_eth_mm_reg_control;
  signal eth_mm_reg_status   : t_eth_mm_reg_status;

  signal eth_ram_miso        : t_mem_miso;
  signal eth_ram_mosi        : t_mem_mosi;

  -- ETH UDP data path interface
  signal udp_tx_sosi_arr     : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal udp_tx_siso_arr     : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal udp_rx_siso_arr     : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal udp_rx_sosi_arr     : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);

  -- LCU TSE interface
  signal lcu_init            : std_logic := '1';
  signal lcu_tse_miso        : t_mem_miso;
  signal lcu_tse_mosi        : t_mem_mosi;
  signal lcu_psc_access      : std_logic;
  signal lcu_tx_en           : std_logic := '1';
  signal lcu_tx_siso         : t_dp_siso;
  signal lcu_tx_sosi         : t_dp_sosi;
  signal lcu_tx_mac_in       : t_tech_tse_tx_mac;
  signal lcu_tx_mac_out      : t_tech_tse_tx_mac;
  signal lcu_rx_sosi         : t_dp_sosi;
  signal lcu_rx_siso         : t_dp_siso;
  signal lcu_rx_mac_out      : t_tech_tse_rx_mac;
  signal lcu_txp             : std_logic;
  signal lcu_rxp             : std_logic;
  signal lcu_led             : t_tech_tse_led;

  -- Verification
  signal tx_end              : std_logic := '0';
  signal rx_end              : std_logic := '0';
  signal rx_timeout          : natural := 0;
  signal tx_pkt_cnt          : natural := 0;
  signal rx_pkt_cnt          : natural := 0;
  signal rx_pkt_discarded_cnt: natural := 0;
  signal rx_pkt_flushed_cnt  : std_logic_vector(c_word_w - 1 downto 0);
begin
  -- run 50 us

  eth_clk <= not eth_clk after eth_clk_period / 2;  -- TSE reference clock
  sys_clk <= not sys_clk after sys_clk_period / 2;  -- System clock

  mm_clk  <= sys_clk;
  st_clk  <= sys_clk;

  p_reset : process
  begin
    -- reset release
    st_rst <= '1';
    mm_rst <= '1';
    for I in 0 to 9 loop wait until rising_edge(mm_clk); end loop;
    mm_rst <= '0';
    wait until rising_edge(st_clk);
    st_rst <= '0';
    for I in 0 to 9 loop wait until rising_edge(mm_clk); end loop;
    wait;
  end process;

  -- Use signal to leave unused fields 'X'
  tx_total_header.eth  <= c_tx_eth_header;
  tx_total_header.arp  <= c_tx_arp_packet;
  tx_total_header.ip   <= c_tx_ip_header;
  tx_total_header.icmp <= c_tx_icmp_header;
  tx_total_header.udp  <= c_tx_udp_header;

  discard_total_header.eth  <= c_discard_eth_header;
  discard_total_header.arp  <= c_tx_arp_packet;
  discard_total_header.ip   <= c_tx_ip_header;
  discard_total_header.icmp <= c_tx_icmp_header;
  discard_total_header.udp  <= c_tx_udp_header;

  exp_total_header.eth  <= c_exp_eth_header;
  exp_total_header.arp  <= c_exp_arp_packet;
  exp_total_header.ip   <= c_exp_ip_header;
  exp_total_header.icmp <= c_exp_icmp_header;
  exp_total_header.udp  <= c_exp_udp_header;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  p_tse_setup : process
  begin
    dut_tse_init <= '1';
    eth_tse_mosi.wr <= '0';
    eth_tse_mosi.rd <= '0';
    -- Wait for ETH init
    while dut_eth_init = '1' loop wait until rising_edge(mm_clk); end loop;
    -- Setup the TSE MAC
    proc_tech_tse_setup(g_technology_dut,
                        c_promis_en, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tx_ready_latency,
                        c_dut_src_mac, eth_psc_access,
                        mm_clk, eth_tse_miso, eth_tse_mosi);
    dut_tse_init <= '0';
    wait;
  end process;

  p_eth_control : process
    variable v_eth_control_word : std_logic_vector(c_word_w - 1 downto 0);
  begin
    -- ETH setup
    dut_eth_init <= '1';
    eth_reg_mosi.wr <= '0';
    eth_reg_mosi.rd <= '0';
    eth_ram_mosi.address <= (others => '0');
    eth_ram_mosi.wr      <= '0';
    eth_ram_mosi.rd      <= '0';

    -- Wait for reset release
    while mm_rst = '1' loop wait until rising_edge(mm_clk); end loop;

    -- Setup the DEMUX UDP
    proc_mem_mm_bus_wr(c_eth_reg_demux_wi + 0, c_dut_udp_port_en + c_dut_udp_port_st0, mm_clk, eth_reg_miso, eth_reg_mosi);  -- UDP port stream 0
    proc_mem_mm_bus_wr(c_eth_reg_demux_wi + 1, c_dut_udp_port_en + c_dut_udp_port_st1, mm_clk, eth_reg_miso, eth_reg_mosi);  -- UDP port stream 1
    proc_mem_mm_bus_wr(c_eth_reg_demux_wi + 2, c_dut_udp_port_en + c_dut_udp_port_st2, mm_clk, eth_reg_miso, eth_reg_mosi);  -- UDP port stream 2
    proc_mem_mm_bus_rd(c_eth_reg_demux_wi + 0,                                       mm_clk, eth_reg_miso, eth_reg_mosi);
    proc_mem_mm_bus_rd(c_eth_reg_demux_wi + 1,                                       mm_clk, eth_reg_miso, eth_reg_mosi);
    proc_mem_mm_bus_rd(c_eth_reg_demux_wi + 2,                                       mm_clk, eth_reg_miso, eth_reg_mosi);

    -- Setup the RX config
    proc_mem_mm_bus_wr(c_eth_reg_config_wi + 0, c_dut_src_mac_lo,                    mm_clk, eth_reg_miso, eth_reg_mosi);  -- control MAC address lo word
    proc_mem_mm_bus_wr(c_eth_reg_config_wi + 1, c_dut_src_mac_hi,                    mm_clk, eth_reg_miso, eth_reg_mosi);  -- control MAC address hi halfword
    proc_mem_mm_bus_wr(c_eth_reg_config_wi + 2, c_dut_ip_addr,                       mm_clk, eth_reg_miso, eth_reg_mosi);  -- control IP address
    proc_mem_mm_bus_wr(c_eth_reg_config_wi + 3, c_dut_udp_port_ctrl,                 mm_clk, eth_reg_miso, eth_reg_mosi);  -- control UDP port
    -- Enable RX
    proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_dut_control_rx_en,                mm_clk, eth_reg_miso, eth_reg_mosi);  -- control rx en
    dut_eth_init <= '0';

    -- Wait for TSE init
    while dut_tse_init = '1' loop wait until rising_edge(mm_clk); end loop;

    -- Response control
    while true loop
      eth_mm_reg_status  <= c_eth_mm_reg_status_rst;
      eth_mm_reg_control <= c_eth_mm_reg_control_rst;
      -- wait for rx_avail interrupt
      if eth_reg_interrupt = '1' then
        -- read status register to read the status
        proc_mem_mm_bus_rd(c_eth_reg_status_wi + 0, mm_clk, eth_reg_miso, eth_reg_mosi);  -- read result available in eth_mm_reg_status
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
        eth_mm_reg_status <= func_eth_mm_reg_status(eth_reg_miso.rddata);
        wait until rising_edge(mm_clk);
        -- write status register to acknowledge the interrupt
        proc_mem_mm_bus_wr(c_eth_reg_status_wi + 0, 0, mm_clk, eth_reg_miso, eth_reg_mosi);  -- void value
        -- prepare control register for response
        if c_tb_reply_payload = true then
          eth_mm_reg_control.tx_nof_words <= INCR_UVEC(eth_mm_reg_status.rx_nof_words, -1);  -- -1 to skip the CRC word for the response
          eth_mm_reg_control.tx_empty     <= eth_mm_reg_status.rx_empty;
        else
          eth_mm_reg_control.tx_nof_words <= TO_UVEC(c_network_total_header_32b_nof_words, c_eth_max_frame_nof_words_w);
          eth_mm_reg_control.tx_empty     <= TO_UVEC(0, c_eth_empty_w);
        end if;
        eth_mm_reg_control.tx_en <= '1';
        eth_mm_reg_control.rx_en <= '1';
        wait until rising_edge(mm_clk);
        -- wait for interrupt removal due to status register read access
        while eth_reg_interrupt = '1' loop wait until rising_edge(mm_clk); end loop;
        -- write control register to enable tx
        if c_tb_reply_payload = true then
          -- . copy the received payload to the response payload (overwrite part of the default response header in case of raw ETH)
          for I in func_tech_tse_header_size(g_data_type) to TO_UINT(eth_mm_reg_control.tx_nof_words) - 1 loop
            proc_mem_mm_bus_rd(c_eth_ram_rx_offset + I, mm_clk, eth_ram_miso, eth_ram_mosi);
            proc_mem_mm_bus_rd_latency(c_mem_ram_rd_latency, mm_clk);
            proc_mem_mm_bus_wr(c_eth_ram_tx_offset + I, TO_SINT(eth_ram_miso.rddata(c_word_w - 1 downto 0)), mm_clk, eth_ram_miso, eth_ram_mosi);
          end loop;
        --ELSE
          -- . only reply header
        end if;
        v_eth_control_word := func_eth_mm_reg_control(eth_mm_reg_control);
        proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, TO_UINT(v_eth_control_word),  mm_clk, eth_reg_miso, eth_reg_mosi);
        -- write continue register to make the ETH module continue
        proc_mem_mm_bus_wr(c_eth_reg_continue_wi, 0, mm_clk, eth_reg_miso, eth_reg_mosi);  -- void value
      end if;
      wait until rising_edge(mm_clk);
    end loop;

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- LCU
  ------------------------------------------------------------------------------
  p_lcu_setup : process
  begin
    lcu_init <= '1';
    lcu_tse_mosi.wr <= '0';
    lcu_tse_mosi.rd <= '0';
    -- Wait for reset release
    while mm_rst = '1' loop wait until rising_edge(mm_clk); end loop;
    -- Setup the LCU TSE MAC
    proc_tech_tse_setup(g_technology_lcu,
                        c_promis_en, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tx_ready_latency,
                        c_lcu_src_mac, lcu_psc_access,
                        mm_clk, lcu_tse_miso, lcu_tse_mosi);
    -- Wait for DUT init done
    while dut_tse_init /= '0' loop wait until rising_edge(mm_clk); end loop;
    lcu_init <= '0';
    wait;
  end process;

  p_lcu_transmitter : process
  begin
    -- . Avalon ST
    lcu_tx_sosi.data  <= (others => '0');
    lcu_tx_sosi.valid <= '0';
    lcu_tx_sosi.sop   <= '0';
    lcu_tx_sosi.eop   <= '0';
    lcu_tx_sosi.empty <= (others => '0');
    lcu_tx_sosi.err   <= (others => '0');
    -- . MAC specific
    lcu_tx_mac_in.crc_fwd <= '0';  -- when '0' then TSE MAC generates the TX CRC field

    while lcu_init /= '0' loop wait until rising_edge(st_clk); end loop;
    for I in 0 to 9 loop wait until rising_edge(st_clk); end loop;

    for I in 0 to 40 loop
      proc_tech_tse_tx_packet(tx_total_header, I, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      --FOR J IN 0 TO 9 LOOP WAIT UNTIL rising_edge(st_clk); END LOOP;
    end loop;

    if g_frm_discard_en = true then
      -- Insert a counter data packet that should be discarded
      proc_tech_tse_tx_packet(discard_total_header, 13, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      rx_pkt_discarded_cnt <= rx_pkt_discarded_cnt + 1;
      -- Send another packet that should be received
      proc_tech_tse_tx_packet(tx_total_header,  14, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
    end if;

    if g_flush_test_en = true then
      proc_tech_tse_tx_packet(tx_total_header, 1496, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1497, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1498, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1499, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    0, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    1, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    2, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
    end if;

--     proc_tech_tse_tx_packet(tx_total_header,  104, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  105, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1472, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  101, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1000, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  102, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1000, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  103, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  104, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  105, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);

    tx_end <= '1';
    wait;
  end process;

  p_lcu_receiver : process
  begin
    -- . Avalon ST
    lcu_rx_siso <= c_dp_siso_hold;

    while lcu_init /= '0' loop wait until rising_edge(st_clk); end loop;

    -- Verification of multiple rx packets is only supported when all packets
    -- are of the same g_data_type, because the rx process can only support
    -- one expected result. The proc_tech_tse_rx_packet does not (yet) interpret the
    -- actually received packet, it relies on the preset expected total_header.

    -- Receive forever
    while true loop
      proc_tech_tse_rx_packet(exp_total_header, g_data_type, st_clk, lcu_rx_sosi, lcu_rx_siso);
    end loop;

    wait;
  end process;

  -- Wire ethernet cable between lcu and dut
  eth_rxp <= transport lcu_txp after cable_delay;
  lcu_rxp <= transport eth_txp after cable_delay;

  gen_udp_rx_siso_rdy: for i in 0 to c_eth_nof_udp_ports - 1 generate
    udp_rx_siso_arr(i).ready <= '1';
  end generate;

  dut : entity eth_lib.eth
  generic map (
    g_technology         => g_technology_dut,
    g_cross_clock_domain => c_cross_clock_domain,
    g_frm_discard_en     => g_frm_discard_en
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    eth_clk           => eth_clk,
    st_rst            => st_rst,
    st_clk            => st_clk,
    -- UDP transmit interfaceg_frm_discard_en
    -- . ST sink
    udp_tx_snk_in_arr  => udp_tx_sosi_arr,
    udp_tx_snk_out_arr => udp_tx_siso_arr,
    -- UDP receive interface
    -- . ST source
    udp_rx_src_in_arr  => udp_rx_siso_arr,
    udp_rx_src_out_arr => udp_rx_sosi_arr,
    -- Control Memory Mapped Slaves
    tse_sla_in        => eth_tse_mosi,
    tse_sla_out       => eth_tse_miso,
    reg_sla_in        => eth_reg_mosi,
    reg_sla_out       => eth_reg_miso,
    reg_sla_interrupt => eth_reg_interrupt,
    ram_sla_in        => eth_ram_mosi,
    ram_sla_out       => eth_ram_miso,
    -- Monitoring
    rx_flushed_frm_cnt => rx_pkt_flushed_cnt,
    -- PHY interface
    eth_txp           => eth_txp,
    eth_rxp           => eth_rxp,
    -- LED interface
    tse_led           => eth_led
  );

  lcu : entity tech_tse_lib.tech_tse
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    eth_clk        => eth_clk,
    tx_snk_clk     => st_clk,
    rx_src_clk     => st_clk,

    -- Memory Mapped Slave
    mm_sla_in      => lcu_tse_mosi,
    mm_sla_out     => lcu_tse_miso,

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      => lcu_tx_sosi,
    tx_snk_out     => lcu_tx_siso,
    -- . MAC specific
    tx_mac_in      => lcu_tx_mac_in,
    tx_mac_out     => lcu_tx_mac_out,

    -- MAC receive interface
    -- . ST Source
    rx_src_in      => lcu_rx_siso,
    rx_src_out     => lcu_rx_sosi,
    -- . MAC specific
    rx_mac_out     => lcu_rx_mac_out,

    -- PHY interface
    eth_txp        => lcu_txp,
    eth_rxp        => lcu_rxp,

    tse_led        => lcu_led
  );

  -- Verification
  tx_pkt_cnt <= tx_pkt_cnt + 1 when lcu_tx_sosi.sop = '1' and rising_edge(st_clk);
  rx_pkt_cnt <= rx_pkt_cnt + 1 when lcu_rx_sosi.eop = '1' and rising_edge(st_clk);

  p_rx_end : process
  begin
    rx_end <= '0';
    wait until tx_end = '1';

    -- use timeout since tx_end or last received packet to determine rx_end
    rx_timeout <= 0;
    while rx_end = '0' loop
      rx_timeout <= rx_timeout + 1;
      if lcu_rx_sosi.valid = '1' then
        rx_timeout <= 0;
      elsif rx_timeout > 5000 then  -- sufficiently large value determined by trial
        rx_end <= '1';
      end if;
      wait until rising_edge(st_clk);
    end loop;

    --WAIT FOR 10 us;
    --rx_end <= '1';
    wait;
  end process;

  p_tb_end : process
  begin
    tb_end <= '0';
    wait until rx_end = '1';

    -- Verify that all transmitted packets have been received
    if tx_pkt_cnt = 0 then
      report "No packets were transmitted."
        severity ERROR;
    elsif rx_pkt_cnt = 0 then
      report "No packets were received."
        severity ERROR;
    elsif tx_pkt_cnt /= rx_pkt_cnt + rx_pkt_discarded_cnt + TO_UINT(rx_pkt_flushed_cnt) then
      report "Not all transmitted packets were received."
        severity ERROR;
    end if;

    wait for 1 us;
    tb_end <= '1';
    if g_tb_end = false then
      report "Tb simulation finished."
        severity NOTE;
    else
      report "Tb simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;
end tb;
