-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_bsn_align is
end tb_tb_dp_bsn_align;

architecture tb of tb_tb_dp_bsn_align is
  constant c_block_size           : natural := 11;
  constant c_diff_delay           : natural := 20;
  constant c_diff_bsn             : natural := 3;  -- g_diff_bsn = g_bsn_latency can just be aligned
  constant c_bsn_latency          : natural := 3;
  constant c_nof_repeat           : natural := 100;  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 2
  -- > run -all                 --> OK

  -- g_block_size           : NATURAL := 11;
  -- g_diff_delay           : NATURAL := 20;
  -- g_diff_bsn             : NATURAL := 3;      -- g_diff_bsn = g_bsn_latency can just be aligned
  -- g_nof_input            : NATURAL := 5;      -- >= 1
  -- g_bsn_latency          : NATURAL := 3;
  -- g_bsn_request_pipeline : NATURAL := 1;      -- = 1, 2, ..., c_bsn_nof_stages, requested total pipelining of the BSN max and BSN min operation
  -- g_out_ready            : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
  -- g_nof_repeat           : NATURAL := 100     -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli

  u_act_5       : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn,  5, c_bsn_latency,  1, e_active, c_nof_repeat);
  u_rnd_5       : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn,  5, c_bsn_latency,  1, e_random, c_nof_repeat);
  u_pls_5       : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn,  5, c_bsn_latency,  1, e_pulse,  c_nof_repeat);

  u_rnd_5_2     : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn,  5, c_bsn_latency,  2, e_random, c_nof_repeat);
  u_rnd_8_2     : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn,  8, c_bsn_latency,  2, e_random, c_nof_repeat);
  u_rnd_16_2    : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn, 16, c_bsn_latency,  2, e_random, c_nof_repeat);
  u_rnd_16_3    : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn, 16, c_bsn_latency,  3, e_random, c_nof_repeat);
  u_rnd_16_4    : entity work.tb_dp_bsn_align generic map (c_block_size, c_diff_delay, c_diff_bsn, 16, c_bsn_latency,  4, e_random, c_nof_repeat);
end tb;
