module qsys_unb2b_minimal_avs_eth_0 (
		output wire        coe_clk_export,             //             clk.export
		output wire        ins_interrupt_irq,          //       interrupt.irq
		input  wire        coe_irq_export,             //             irq.export
		input  wire        csi_mm_clk,                 //              mm.clk
		input  wire        csi_mm_reset,               //        mm_reset.reset
		input  wire [9:0]  mms_ram_address,            //         mms_ram.address
		input  wire        mms_ram_write,              //                .write
		input  wire        mms_ram_read,               //                .read
		input  wire [31:0] mms_ram_writedata,          //                .writedata
		output wire [31:0] mms_ram_readdata,           //                .readdata
		input  wire [3:0]  mms_reg_address,            //         mms_reg.address
		input  wire        mms_reg_write,              //                .write
		input  wire        mms_reg_read,               //                .read
		input  wire [31:0] mms_reg_writedata,          //                .writedata
		output wire [31:0] mms_reg_readdata,           //                .readdata
		input  wire [9:0]  mms_tse_address,            //         mms_tse.address
		input  wire        mms_tse_write,              //                .write
		input  wire        mms_tse_read,               //                .read
		input  wire [31:0] mms_tse_writedata,          //                .writedata
		output wire [31:0] mms_tse_readdata,           //                .readdata
		output wire        mms_tse_waitrequest,        //                .waitrequest
		output wire [9:0]  coe_ram_address_export,     //     ram_address.export
		output wire        coe_ram_read_export,        //        ram_read.export
		input  wire [31:0] coe_ram_readdata_export,    //    ram_readdata.export
		output wire        coe_ram_write_export,       //       ram_write.export
		output wire [31:0] coe_ram_writedata_export,   //   ram_writedata.export
		output wire [3:0]  coe_reg_address_export,     //     reg_address.export
		output wire        coe_reg_read_export,        //        reg_read.export
		input  wire [31:0] coe_reg_readdata_export,    //    reg_readdata.export
		output wire        coe_reg_write_export,       //       reg_write.export
		output wire [31:0] coe_reg_writedata_export,   //   reg_writedata.export
		output wire        coe_reset_export,           //           reset.export
		output wire [9:0]  coe_tse_address_export,     //     tse_address.export
		output wire        coe_tse_read_export,        //        tse_read.export
		input  wire [31:0] coe_tse_readdata_export,    //    tse_readdata.export
		input  wire        coe_tse_waitrequest_export, // tse_waitrequest.export
		output wire        coe_tse_write_export,       //       tse_write.export
		output wire [31:0] coe_tse_writedata_export    //   tse_writedata.export
	);
endmodule

