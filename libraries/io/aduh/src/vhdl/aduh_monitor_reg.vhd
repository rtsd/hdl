-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for the mean_sum and the power_sum
-- Description:
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                             mean_sum[31:0]                            |  0
--  |-----------------------------------------------------------------------|
--  |                             mean_sum[63:32]                           |  1
--  |-----------------------------------------------------------------------|
--  |                            power_sum[31:0]                            |  2
--  |-----------------------------------------------------------------------|
--  |                            power_sum[63:32]                           |  3
--  |-----------------------------------------------------------------------|
--
-- . The new mean_sum and the power_sum are passed on the MM side when
--   st_mon_sync pulses.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity aduh_monitor_reg is
  generic (
    g_cross_clock_domain : boolean := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    st_rst            : in  std_logic;  -- reset synchronous with st_clk
    st_clk            : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_mon_mean_sum   : in  std_logic_vector(c_longword_w - 1 downto 0);  -- use fixed 64 bit sum width
    st_mon_power_sum  : in  std_logic_vector(c_longword_w - 1 downto 0);  -- use fixed 64 bit sum width
    st_mon_sync       : in  std_logic  -- at the mon_sync there are new mean_sum and pwr_sum statistics available
  );
end aduh_monitor_reg;

architecture rtl of aduh_monitor_reg is
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 2,
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 2**2,
                                  init_sl  => '0');

  -- Registers in mm_clk domain
  signal mm_mon_mean_sum          : std_logic_vector(c_longword_w - 1 downto 0);
  signal mm_mon_mean_sum_hi       : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_mon_power_sum         : std_logic_vector(c_longword_w - 1 downto 0);
  signal mm_mon_power_sum_hi      : std_logic_vector(c_word_w - 1 downto 0);

  -- Registers in st_clk domain
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out              <= c_mem_miso_rst;
      mm_mon_mean_sum_hi   <= (others => '0');
      mm_mon_power_sum_hi  <= (others => '0');
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        -- no write registers

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            sla_out.rddata(31 downto 0) <= mm_mon_mean_sum(31 downto 0);
            mm_mon_mean_sum_hi          <= mm_mon_mean_sum(63 downto 32);  -- first read low part and preserve high part
          when 1 =>
            sla_out.rddata(31 downto 0) <= mm_mon_mean_sum_hi;
          when 2 =>
            sla_out.rddata(31 downto 0) <= mm_mon_power_sum(31 downto 0);
            mm_mon_power_sum_hi         <= mm_mon_power_sum(63 downto 32);  -- first read low part and preserve high part
          when 3 =>
            sla_out.rddata(31 downto 0) <= mm_mon_power_sum_hi;
          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate  -- so mm_clk = st_clk
    p_mm_clk : process(mm_rst, mm_clk)
    begin
      if mm_rst = '1' then
        mm_mon_mean_sum  <= (others => '0');
        mm_mon_power_sum <= (others => '0');
      elsif rising_edge(mm_clk) then
        if st_mon_sync = '1' then
          mm_mon_mean_sum  <= st_mon_mean_sum;
          mm_mon_power_sum <= st_mon_power_sum;
        end if;
      end if;
    end process;
  end generate;  -- no_cross

  gen_cross : if g_cross_clock_domain = true generate
    u_mean_sum : entity common_lib.common_reg_cross_domain
    generic map (
      g_in_new_latency => 0
    )
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_new      => st_mon_sync,  -- when '1' then new in_dat is available after g_in_new_latency
      in_dat      => st_mon_mean_sum,
      in_done     => OPEN,  -- pulses when no more pending in_new
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_mean_sum,
      out_new     => open  -- when '1' then the out_dat was updated with in_dat due to in_new
    );

    u_pwr_sum : entity common_lib.common_reg_cross_domain
    generic map (
      g_in_new_latency => 0
    )
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_new      => st_mon_sync,  -- when '1' then new in_dat is available after g_in_new_latency
      in_dat      => st_mon_power_sum,
      in_done     => OPEN,  -- pulses when no more pending in_new
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_mon_power_sum,
      out_new     => open  -- when '1' then the out_dat was updated with in_dat due to in_new
    );
  end generate;  -- gen_cross
end rtl;
