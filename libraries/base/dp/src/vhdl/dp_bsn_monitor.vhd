-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Monitor the sosi and siso control of a stream per sync interval
-- Description:
--   The mon_evt pulses when new monitor output is available regarding the
--   previous in_sosi.sync interval:
--   . mon_sync_timeout        = '1' when the in_sosi.sync did not occur
--   . mon_ready_stable        = '1' when ready was always '1' during last sync interval
--   . mon_xon_stable          = '1' when xon   was always '1' during last sync interval
--   . mon_bsn_at_sync         = BSN at sync
--   . mon_nof_sop             = number of sop during last sync interval
--   . mon_nof_err             = number of err at eop during last sync interval
--   . mon_nof_valid           = number of valid during last sync interval;
--   . mon_bsn_first           = BSN at the first SOP that came in (When g_log_first_bsn = TRUE)
--   . mon_bsn_first_cycle_cnt = number of cycles after rst at which mon_bsn_first was logged (When g_log_first_bsn = TRUE)
--
--   . mon_bsn_first           = BSN at the moment sync_in is pulsed (When g_log_first_bsn = FALSE)
--   . mon_bsn_first_cycle_cnt = number of cycles after the last sop befor sync_in is pulsed (When g_log_first_bsn = FALSE)
--
-- Remarks:
-- . Assumes RL > 0 so each active valid indicates a new data (with RL = 0 the
--   valid remains active until an acknowledge by ready)

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_bsn_monitor is
  generic (
    g_sync_timeout  : natural := 200 * 10**6;  -- choose >= nof clk cycles per sync period
    g_error_bi      : natural := 0;
    g_log_first_bsn : boolean := true  -- Logs first BSN + cycle count. Useful when using BSN aligner.
  );
  port (
    rst                     : in  std_logic := '0';
    clk                     : in  std_logic;

    -- ST interface
    in_siso                 : in  t_dp_siso;
    in_sosi                 : in  t_dp_sosi;
    sync_in                 : in  std_logic := '0';  -- Sync input that samples the current bsn number and the number of clockcycles that have expired since the last sop
    -- MM interface
    -- . control
    mon_evt                 : out std_logic;  -- pulses when new monitor output is available regarding the previous sync interval
    mon_sync                : out std_logic;  -- pulses every in_sosi.sync interval
    mon_sync_timeout        : out std_logic;
    -- . siso
    mon_ready_stable        : out std_logic;
    mon_xon_stable          : out std_logic;
    -- . sosi
    mon_bsn_at_sync         : out std_logic_vector;
    mon_nof_sop             : out std_logic_vector;
    mon_nof_err             : out std_logic_vector;
    mon_nof_valid           : out std_logic_vector;
    mon_bsn_first           : out std_logic_vector;
    mon_bsn_first_cycle_cnt : out std_logic_vector
  );
end dp_bsn_monitor;

architecture rtl of dp_bsn_monitor is
  constant c_sync_timeout_w : natural := ceil_log2(g_sync_timeout);
  constant c_bsn_w          : natural := mon_bsn_at_sync'length;
  constant c_cnt_sop_w      : natural := mon_nof_sop'length;
  constant c_cnt_valid_w    : natural := mon_nof_valid'length;

  signal ready                       : std_logic;
  signal nxt_ready                   : std_logic;
  signal ready_stable                : std_logic;
  signal xon                         : std_logic;
  signal nxt_xon                     : std_logic;
  signal xon_stable                  : std_logic;

  signal err                         : std_logic;
  signal nxt_err                     : std_logic;
  signal valid                       : std_logic;
  signal nxt_valid                   : std_logic;
  signal sop                         : std_logic;
  signal nxt_sop                     : std_logic;
  signal first_sop                   : std_logic;
  signal nxt_first_sop               : std_logic;
  signal sync                        : std_logic;
  signal nxt_sync                    : std_logic;
  signal bsn                         : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_bsn                     : std_logic_vector(c_bsn_w - 1 downto 0);
  signal cnt_sop                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nof_sop                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal cnt_err                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);  -- use c_cnt_sop_w, because at maximium all frames have an error
  signal nof_err                     : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal cnt_valid                   : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal nof_valid                   : std_logic_vector(c_cnt_valid_w - 1 downto 0);

  signal i_mon_ready_stable          : std_logic;
  signal i_mon_xon_stable            : std_logic;
  signal i_mon_bsn_at_sync           : std_logic_vector(c_bsn_w - 1 downto 0);
  signal i_mon_nof_sop               : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal i_mon_nof_err               : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal i_mon_nof_valid             : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal i_mon_bsn_first             : std_logic_vector(c_bsn_w - 1 downto 0);
  signal i_mon_bsn_first_cycle_cnt   : std_logic_vector(c_word_w - 1 downto 0);
  signal i_current_bsn               : std_logic_vector(c_bsn_w - 1 downto 0);
  signal cnt_cycle                   : std_logic_vector(c_word_w - 1 downto 0);

  signal sync_timeout_cnt            : std_logic_vector(c_sync_timeout_w - 1 downto 0);
  signal sync_timeout                : std_logic;
  signal sync_timeout_n              : std_logic;
  signal nxt_sync_timeout            : std_logic;
  signal sync_timeout_revt           : std_logic;

  signal nxt_mon_evt                 : std_logic;
  signal nxt_mon_ready_stable        : std_logic;
  signal nxt_mon_xon_stable          : std_logic;
  signal nxt_mon_bsn_at_sync         : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_mon_nof_sop             : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nxt_mon_nof_err             : std_logic_vector(c_cnt_sop_w - 1 downto 0);
  signal nxt_mon_nof_valid           : std_logic_vector(c_cnt_valid_w - 1 downto 0);
  signal nxt_mon_bsn_first           : std_logic_vector(c_bsn_w - 1 downto 0);
  signal nxt_mon_bsn_first_cycle_cnt : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_current_bsn             : std_logic_vector(c_bsn_w - 1 downto 0);
begin
  mon_sync                <= sync;
  mon_sync_timeout        <= sync_timeout;
  mon_ready_stable        <= i_mon_ready_stable;
  mon_xon_stable          <= i_mon_xon_stable;
  mon_bsn_at_sync         <= i_mon_bsn_at_sync;
  mon_nof_sop             <= i_mon_nof_sop;
  mon_nof_err             <= i_mon_nof_err;
  mon_nof_valid           <= i_mon_nof_valid;
  mon_bsn_first           <= i_mon_bsn_first;
  mon_bsn_first_cycle_cnt <= i_mon_bsn_first_cycle_cnt;

  nxt_mon_evt          <= sync or sync_timeout_revt;
  nxt_mon_ready_stable <= ready_stable when sync = '1' else i_mon_ready_stable;
  nxt_mon_xon_stable   <= xon_stable   when sync = '1' else i_mon_xon_stable;
  nxt_mon_bsn_at_sync  <= bsn          when sync = '1' else i_mon_bsn_at_sync;
  nxt_mon_nof_sop      <= nof_sop      when sync = '1' else i_mon_nof_sop;
  nxt_mon_nof_err      <= nof_err      when sync = '1' else i_mon_nof_err;
  nxt_mon_nof_valid    <= nof_valid    when sync = '1' else i_mon_nof_valid;

  nof_sop   <= INCR_UVEC(cnt_sop, 1);  -- +1 because the sop at the sync also counts
  nof_err   <= cnt_err;
  nof_valid <= INCR_UVEC(cnt_valid, 1);  -- +1 because the valid at the sync also counts

  u_sync_timeout_cnt : entity common_lib.common_counter
  generic map (
    g_width => c_sync_timeout_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => sync_timeout_n,
    count   => sync_timeout_cnt
  );

  sync_timeout_n <= not nxt_sync_timeout;

  nxt_sync_timeout <= '1' when unsigned(sync_timeout_cnt) >= g_sync_timeout else '0';

  u_sync_timeout_revt : entity common_lib.common_evt
  generic map (
    g_evt_type   => "RISING",
    g_out_invert => false,
    g_out_reg    => false
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => sync_timeout,
    out_evt  => sync_timeout_revt
  );

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      -- internal
      ready                     <= '0';
      xon                       <= '0';
      valid                     <= '0';
      sop                       <= '0';
      err                       <= '0';
      sync                      <= '0';
      bsn                       <= (others => '0');
      -- output
      mon_evt                   <= '0';
      sync_timeout              <= '0';
      i_mon_ready_stable        <= '0';
      i_mon_xon_stable          <= '0';
      i_mon_bsn_at_sync         <= (others => '0');
      i_mon_nof_sop             <= (others => '0');
      i_mon_nof_err             <= (others => '0');
      i_mon_nof_valid           <= (others => '0');
      i_mon_bsn_first           <= (others => '0');
      i_mon_bsn_first_cycle_cnt <= (others => '0');
      i_current_bsn             <= (others => '0');
    elsif rising_edge(clk) then
      -- internal
      ready                     <= nxt_ready;
      xon                       <= nxt_xon;
      valid                     <= nxt_valid;
      sop                       <= nxt_sop;
      err                       <= nxt_err;
      sync                      <= nxt_sync;
      bsn                       <= nxt_bsn;
      first_sop                 <= nxt_first_sop;
      -- output
      mon_evt                   <= nxt_mon_evt;
      sync_timeout              <= nxt_sync_timeout;
      i_mon_ready_stable        <= nxt_mon_ready_stable;
      i_mon_xon_stable          <= nxt_mon_xon_stable;
      i_mon_bsn_at_sync         <= nxt_mon_bsn_at_sync;
      i_mon_nof_sop             <= nxt_mon_nof_sop;
      i_mon_nof_err             <= nxt_mon_nof_err;
      i_mon_nof_valid           <= nxt_mon_nof_valid;
      i_mon_bsn_first           <= nxt_mon_bsn_first;
      i_mon_bsn_first_cycle_cnt <= nxt_mon_bsn_first_cycle_cnt;
      i_current_bsn             <= nxt_current_bsn;
    end if;
  end process;

  -- siso
  nxt_ready <= in_siso.ready;
  nxt_xon   <= in_siso.xon;

  u_ready_stable : entity common_lib.common_stable_monitor
  port map (
    rst          => rst,
    clk          => clk,
    -- MM
    r_in         => ready,
    r_stable     => ready_stable,
    r_stable_ack => sync
  );

  u_xon_stable : entity common_lib.common_stable_monitor
  port map (
    rst          => rst,
    clk          => clk,
    -- MM
    r_in         => xon,
    r_stable     => xon_stable,
    r_stable_ack => sync
  );

  -- Sample the BSN, because BSN is only valid during sop.
  nxt_current_bsn <= in_sosi.bsn(c_bsn_w - 1 downto 0) when in_sosi.sop = '1' else i_current_bsn;

  -- sosi
  -- . no need to AND sop, eop with valid, because can only be active when valid = '1'
  -- . no need to AND sync     with sop,   because can only be active when sop   = '1'
  nxt_valid                   <= in_sosi.valid;
  nxt_sop                     <= in_sosi.sop;
  nxt_sync                    <= in_sosi.sync;
  nxt_err                     <= in_sosi.err(g_error_bi)         when in_sosi.eop = '1' else '0';  -- assume sosi.err(g_error_bi) = '1' at eop indicates an error
  nxt_bsn                     <= in_sosi.bsn(c_bsn_w - 1 downto 0) when in_sosi.sop = '1' else bsn;  -- keep bsn as defined at sop

  gen_log_first_bsn : if g_log_first_bsn = true generate
    nxt_mon_bsn_first           <= in_sosi.bsn(c_bsn_w - 1 downto 0) when first_sop = '0' and in_sosi.sop = '1' else i_mon_bsn_first;
    nxt_mon_bsn_first_cycle_cnt <= cnt_cycle                       when first_sop = '0' and in_sosi.sop = '1' else i_mon_bsn_first_cycle_cnt;
  end generate;

  nxt_first_sop <= '1' when first_sop = '0' and in_sosi.sop = '1' else first_sop;

  gen_sync_in_enable : if g_log_first_bsn = false generate
    nxt_mon_bsn_first           <= i_current_bsn when sync_in = '1' else i_mon_bsn_first;
    nxt_mon_bsn_first_cycle_cnt <= cnt_cycle     when sync_in = '1' else i_mon_bsn_first_cycle_cnt;
  end generate;

  u_cnt_sop : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_sop_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => sop,
    count   => cnt_sop
  );

  u_nof_err : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_sop_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => err,
    count   => cnt_err
  );

  u_cnt_valid : entity common_lib.common_counter
  generic map (
    g_width => c_cnt_valid_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => sync,
    cnt_en  => valid,
    count   => cnt_valid
  );

  u_cnt_cycle : entity common_lib.common_counter
  generic map (
    g_width => c_word_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => in_sosi.sop,
    cnt_en  => '1',
    count   => cnt_cycle
  );
end rtl;
