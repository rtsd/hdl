`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Y49VX9oMqF9z4LBjzSwW7yLMLXotdWJUtOGA2Q8OH2uDGQD8XomiCdpSlKJTzLIH
0o0ZG8cZSrmPwY0Je6MvY0pFIhLmxkIR+E8LHuU7RjY8NFsMjsqEkkwOxgqC7TU9
akzJ/MRg3Xa6wRNIUpXN601zgfrIl+HeYCH3tO6J3Bs=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7568)
grEsJD/36pDyQVA4iik+Ct5aHEFqLa+mfdk22ZdaOtDig8k/nx6T6zAEIW+9X0I1
spVPRaAG95HNbQkYWvwwO36PKU1254J1zxt+Ur8N/mLMR1NVgK+bkpc1GfzcM5FY
4wKWrHHMuaGx7N8zitMDSjNAqU5qN8ltB62MaVJICKFZZM3IlGGbXQK3F4PgMbmY
meTcfgXVnfQW8DZovPBtLaDJ2x9ELSFeEHDs6dKy9Rkaiuf7+/bsXXr0ehqQnPOt
vY6jgDV4M9DIv/VMb6tE5ZxXgM50XkrzkPwO/ef2WklEU9XefOUtduUc2Cfg2Owg
McH1V/cIFmS4OCE6wirDdZY8ywdlB/2bKBXoKvLR+BPd+VI3AS5mQRMWKb/WabeF
RWD0rbIINIip2k9exkTcqgNEhsLG/MB2epPZvEQEAnJvbFfXGsfO4GeQFsRnUax8
lrjntWd2u8idsDCklqCp49KkEW0J1TuCqMinPlIhWHSw/9QwAioIpV36g+Mi4ZIy
aG0vNLWSYpIRdHQnDMk4tGR1mwPF7yFnEsAYIbVJsCUKq5JWO/dbwochssxTv5Km
h3KdghDtQ8AYERn97Ur9xco6iEWmyP8ALhZSQXiEbU07XqPwco7lhA6q5VOyvyh0
VKGMbnu1H57D9zDvgRh6FWwUhLHkA3qgnSMqr6pVClIuqnDVcb7nAogWvkaL1XTY
abN9PHFCNUINiddlv3OYG6+K6+xMNzegn0G89+/ppZdGzYiZC318ocJDK2el3Tyg
fXShXYcb+N4BK9eunHJOfUcoBf/LQqp+b5MhDT8YUMtEjNrDIf869oU4yvlvRoIY
pUgSBBCIvpJGl9pqL8aqMLW80/WLxLHwpUoKnUFmKUFW2hGf7JJnALp6bb+pzbgn
jKgP4e7fwEb+YbXjosgjMHuWfsdjVak0OGEqttd/rHiARNcYKrNI2mNLExMtlG1h
+T7htMiYF9TCyMj1TNFYWsZEFhYprVNHgs1M1VoPMxwEZqspUpR17UPYdTT4saUb
lJpTbj1ZZK3NSNZyLOamM7PQm3frnWXbd8gc2DViaGWPAxW23UlU2bH9IECgimre
LJq3NPkk5ZpQQqPN6hE3wHIj13pO82mVyJqQD83LvXidzMMu8P3g53M9nlf1GJME
VOj4V/Q9HMtUdtJ2initLC2PpRuHtsmMyAdF9+XPcKmqQSJHJyCSYp1m8g1HAgqT
TlkQvt3hoK9knnrVqxsX1N6WWHM5jhHIHo/cX2/TONAyWr+ZJn0GfP1uIFI+SlZP
+7bsP/jIbkstrjUM8H09m7U0YmBt9SIx2jS8b3Io5m1erTLC0/qhFIyJsiJQk9b0
/l9bDkpJ3m1DgPgXnfBTy1vhkJBXQvXR5kf0MRQo/AkMdc7XtT03ffmRYGABGLYb
89S/YSKjL4FzbAKLcothWCfV9Ui4mFp3rz5zybwl7kgGxmXNk/q86r7SdHPJHQkY
QjgqmDxO5J/y7gJ0WlbPb4LvMgdinZ67G5P5g2wV7wMvEY00uiODxhhWBcqlfksT
0R9/Sd892IA2AXmYGylJOv+O/XEiFXR75hqca89mRqrhER0DbLto+M/7+ZYv24df
enQyMLWp3LYAqt9zPMo9z8njYiA3X4llwSwWjDnWBgHPX9b/mEwNfiW3srYVhgtt
jNnLGp27vx8nLtycUp5XwmdV7KXM/WV9SigFLi6dgY3H3XLNzvfmfvkD4wAH0FPv
so+yEVg4XFNSXJzx1Bc1qB/IRTg8q6uaNdEfA5vIR5S2etQsQcLm7UCqD+47RI9s
vv/4RBtRQ2ObOyg27kXkD7AkU/Rpc9eFoKVkcXnIuGjg//oKCoKeHOXMUkykAnCN
s4923Rn4Gf8o8+5BNfZ9tcvcAE2gDiPzYZsd20IqSrqfdvm+cRTte03ckNjLxNVW
gxO/Fhs33rJNahE2G9nx8+MzD7qBtPaup0SeVK3tICnF0yxmFdM3CrBImQPQDVnh
yMldEYhk2Q2ADoWeX7jCMAVgFh85rqQ9KyrAj1YD9Mg4DbC7lHyogkhLQzWaalZp
8slff5gsiUQcVD+bGLnT7Wol5zfJNbD+x5GeFwOgF8XQbApwAWmI4/0rQyOoPtE/
BfJRJA7nmEk8xsEi30IfD/CGsM7t5b70Qx3bi/97EFN8/eIBGIMAW8097lumjRzG
R04qwGsEmIXOlN5uJ6opTG/ME7jpCl1bnA8AZti/qbP0lJsKdF1R/wYmS8pGU1Tq
UhioPbYLlr9F0XHRpr2DMdGaRvVQfpw3me6i5eS6JPZuHKjAGnRniqlQjbqISNjW
jjy7+PR2HdIqvoY2xCwZxYI5vCaBZSX9LluiDx8yQlLhpyZLGY2vAZBqY+FfHoTS
DIWwv+zce5XPLj8rcqBKX3qUCmFU8YjGdu0ukZ0RiDKkNAm34eNegyss1iIH4d/4
4F5cKMHHAghtVvxVDgb67QbOooLAImMoaxq6dwzeu1QHsHK0HEeyhtccBVDXObhb
fSdmkAfYHGHrnBiFSyWThxG1aO462Q4iaGe1ANXv0Un+MptulNx10XrQddA64PmU
PBECurQNj8Z+D1keoIKdPNsprcpUXezlao7+DBuHJSgEKLfFD1/EwDuuF7+gT05q
0fU26ki2HWuKZ46Pyj847ho9FC0bahU1EO8iUCN2uTlLkLmVSKtvXj7wimrEHXva
Xsj9EcdbWv7qxqber8/GB83ayVsvm91mq4v4TPMeuQTdfLRU1RkUbhHCnHZvShWu
T0FIFtXQ86+MybwmMtfkZY3VaiuzEjU/LkpCnIdtKm44cxa9K0M2A7Pny14Kqjlh
m2Cv7FvZ3x5xCJHS0hLwbywUI19t0p6auAt+TnWqSBypvwk5dGOf7oDC/W2NtM8d
8ffNM8ig+Gmh6qYgoXD6kCyO+5vkPs2oEzEqK4O83TGJcQvzH6QsgnO7fxBFwBVB
X82CTcPe8/Q0N3ITPnsj6qUErrYL/9DsHCBHs3IaHgGMoLPD8MWTChN+UWKybCby
4iN/ANZBJd4RfSYIKMEkTO+UJwGQVIKcVvo7C+QKbhv285eUunHd3g2Rq/i+hX/B
KQrgP1VAlBNpbDpI5ewi33nv4Q/a0khGq8Sqhax9xLBndPNt17W3GWQKMkUfL6X4
UmvM3FYJQbEAXVqUHU4Lmfn9fAQvuixHXbDN2+zPP8ZaaTShmIjlwXPaWQfOyxLp
ht3lON0J5timYFwSPizVuTw7ut8fmB8JiBBI6XoJ1w901Rx0aO1gSE/fSCt1HNxG
8mOUS1tTSGuT/wZtfZXHzWd/nf4mOcGAsZpJg1dLk3H4R4B+K1jpcrRXUmsxJUOn
Wl6yUKzkn0xxOdLttUl2MjrjsMVTTmkngAB3glDLe3qj9SJ4ad+l2vJJzX7snMQL
N3R7/o2YNuAmAjh4IUtQ4vu/RYngWBwp/wyAuy0RcbnUpqIV8pupln5LIb3SUmqi
4YraIwGM/wOzSN2FG83fNcNLzncKwAYhwBDtM9jQzMEDDr6flEnBGcODWNkRE/d+
sqy99stCufpZNV/WSWAhAJ6wBKAG2UvGh556BZgC/zu4Pe9gxs22Pe30U5jma7cq
5shjfsOE2Wnn031na0Ai9fXr2rMy7LD8sX8iPqfo6QMt6gyI4iJTJ8Q1oIG+Nr2j
FzZ7dJVXJtaS0WL8BFAKflOq7FtAF1w6nSurw4/rSlxhOoe6RALRaQPkysul7Q4Y
9WkJR9VEYeqiNdUfmkBA9ehJ/fJ6JTAZ796Xlcy0JmmUzrudASwgl+rTO+Jn+1yK
Gmu55XeOCm5jioWSz9oYG9nOBg+0sTTPxYVmMCeMGD/KQsN6KQ/Qj+I8FTtl7S3I
Ynkf1IV28EatejImpCe6YmuSGUOM/7EtQ5sib2b3wJk4azcYTvW9eryIZARV2P1T
I7PKsONTkyz91OdDiNqVnpqOHHQTF6PT2zrICCM6RpmaRYxhn9yYzfFYh8gKYUGl
ZcLs5s3q2nqoTkJt90oTL5GSRSRLggh4ZsUp5/mRrva888U+kU8lF1ps6Bb63HJj
6n3UwAM13nmrgXA/ACk4AsB7V5z3lES7oioe9+oCuP93p/s2dAIWV4I8Wq5OlQM0
O2l/OcBtVzEM1HkDZ65o882s0/IIP/6x/rBufE8d8JxkyAf7dLNNRSzGhXKjnj9g
XCzZCleaUw7uVkwJhCTCj3pLCiGNRwhXJ4H1r8VPOTAUWxp/ioJ2B7nWyXHZHjra
tFL3uGd1B6CRnijKauh/5hgF9Z2NX4MSPqgZCCe+KAxWpaqJRefFKbZ7d43TBjNP
TMk94VCPzintt6B3vQ6xAMRX2OnuOsMxG+EdiEynU5Dh3/OWl9+6a6+TIKwpjFve
XEySA4ZWxKSeq+XA+zYBjkQrw3fdmntcxl8Xu0Gh1PmUqOR+4x2GNjNgxJbymswY
Acgi7VQ9dulJfWpIkBJzlyWauZxrx/Q/Dq0OxVKMxELcc/XV9iogH6GhgiudrY/U
XCgg7oSSjlWcQlRNBIH7apEqGJlasaeznNhAhTyhIxAlvPb5hhiPUI9ztgz0FiFA
p2sOLHqovhK/1D987f846UIsBex27qmPvbGo63C86kYEhkfxsXF1ihYOUQ8h38/4
wDtQrMCPy3O1l/g5PU1dotBEzV4ivKWjdiuN+MgrCSxwoM5DLCrZNz5acIOcEEwk
7TkgbARYYHjSmEPPinoWz8KoMXY/rZxRbn5UUk1sFUDVV1oc8IFeSZpEkU56DxbN
VHSR7b6OnC1s53c9TYvOSZBAAy5Y7JyxQmSQ9WKt87g9qDOq5EvOhnhcsW/JmMN8
nm3eM3QgkSkfr0Hzfnq+3aCajxAu8meaQvUX7wf9aA5PWxxvkwQodGhmCIyWWxpF
9ooCA26n6P1syREOzsfMpZ56JBs9038VM6jfRBuhNUghNI7EXOHt3UKbX8SPrjrg
Ni3RPAPvj9SwGDP/sN5V1DJRshpun67cSgquA8DYYpFZbAXQX4Y6n/zh0cf0HcKU
A6sMMCLglnnSEdcg/TcDlUHXE2bxovU/jBrSeqYxFkkJm8lXTtgWallCEjOaUUd8
F57ho8/KzKlQsgkNND8ES4kykBh1mv+Z1hj7T9peLQMevLHbcIQ2a4HlTzZ+5Y2x
o2g9sJd/h5LKaFFzvBW8qg9st9EQsk5jC2tCge6Qq1aizCJfvlOy8lm2hG9SVWAh
dGjfyvWDcyjw4CdMuJsbOnr1DwivoqmWjcB28GBzFNUa3201FRbkQjF3dwqijv8n
BDta58FlXMJdIiMaQuRtUSRAv7TuQwlyosz8UUytFXeZMGlV7N/H8SZ5TTuNgsye
drEHdxIG8nKjLER6/mv44gi8X5aZPI1ANSFFIar3Kljz+2xX9zO87nHrZAu5hSEt
16BT6cgDdv/BYRkxGN3AFBv/JztD6ahTrAuuO8euAQ4wYxEt9NiMLij1QaATEdiw
CRRZs7kDEk6VGBXi/lylcIPlzUOkLfrI8WNkm/iA671D5AxOfkrOqxTMP3pn5cH0
dEM+pRTSKOxZ9mfVbttxYyX3Zcx/ZywiT51fnAw20Cj/FNomO+Z+kQN+1+Hiz9v2
YztvRl6hhbzW2AtJb35CV0dnISeEKb78THlApRtny6jsr4pC/hEtwBZIjzTBzsUn
AF2i+Wuw8POYLyUr3xosD5qvEVLgZgyQML+EbPYRs5t4e4E0+oRTRHxhF3mLtIyU
Z26LJ8tIrllqObz5ZNZlD9y7YB8lbg9wDldCzRZGj+j+ikbyslslKAOBxQsXyixv
9BIa2WkcqNmpak1DDICACfVEU1tHXwaAlccYxng14mZnCHDsn8t2XN+yvqrouU1P
IEdvxCgifyZYJ/BMaDB91yE/SUd53olkut6/oj3jeyiSFuXgykxicd2No/2pJws2
UDdPaP19Gybv3YzuWe2cGRHIKjMp9QSDRRBatk5yqCIH+b9Wk4kaLOUec55zmRYj
xI0yRhHOAEhLfPt8nrXSGo8QKeEiSbbxC0927VA3JsAXmYCGaB/MOV+IJxNPHGcA
mx6MFwRCnCLrjvZk0CQEGqXbGUcCBnBWtjpqLXRojG9AIWBeP/ZRi64+weI40Z9p
y0ezqLj/J+YYyk87fTNcbGdJfeqg65//8/lI/M0NZF3AzhEsbjHxhl8alozGRhyJ
x8E7pEzcYva2XPPjIBi781D86Xa2ABZq0Ea9lq61VQ3r4RkpY51oR0W6ECEmcutd
62z5zzdKtp3FdzvGdseLeiru9mQ/K1tD7S4Q/QTsctqirV2y0MVEA40G7J5PNgZ2
wyxz8NMMM0mrM/oe/tWlnlNX3ilpjxmyRxVIHDMTozrx3MuVkYp2PnY+V9+4GJpp
+gdNRI2iolvx5+cAaNkm9SK/qMHcysFdqpVqLKrDtucS38q4OKvGVS/HcVtADaXU
e3PlO9rkUyYHFehxD9Mjc8KqNh7tdkbhgzXe2z+59rIqmGR9BIaMZX4AplSD17sU
z8sZAfdcRUSGDQ5wHG+KbnCecnoYwFbvdagEHONYLjXKn3qjeJiNXFWm2b5voV4e
cHUxhkuw/RudxAi75Ykn6PL6eeS+VxTmUz0r7MKITxHNtKi2ip7NRCUejYO4TInK
/8jeBM3u9WnaOnXSSROggG654sh0LC4CaghZbeW0cGZ8qENhNcmGgCXiMka6PIwD
DjvOW6WRg7PXDwa9+rZgyxjPc/2C+CnUlG6xrKj9BaGueSUY+qUi6D1IMuziMj5m
jysEMZdTUAKitI/bYcHUXCpeCkYMx8+aJKU3fzK9p8Oxs2TYL6zYIk2nRS19M/yp
RizxQu+niFvNEDyp2oUTme8LTWZWK27cf5OqyLIo45FT39A2H77hdFb1TfCI3Ckl
Ne4td1XkCPM8r06vloWdyYRqtmUraim8A/mZ5itBzXLEWVjvqfMcslQp/0hfJGZy
r8i4Amr86U1iyT6HHACXCUG265NOEr72C1ShlklAnOP75IvNh2AkR2DZgCtbR9Dx
Yj3/4MpxIkwbouyZ30PNPNVasFxs1fHlRoE2WWiXVvXbp5ZWb6+AfbG33tcwI/i2
XMFrULY+RWMQAfrb01O4VvJkgh0vosdZH2bb14GFzZVzWNiG/rHPXBl2T9FcqeyP
00ZYazXl9/QFC724uQhCHX5pfacU+YyRN9yCR7mcHGkd0PASdRvq5cgIDc046AEN
6Bbke6aAyTkjT7lNDRvvsLDnfwYdYPNiv2xhBJXcTPLp0zmwsUAeIAFCuGs9+scz
OhXS4NbPq3Ab7w8YDGFdklW638i2qvbAGDKbn37mEJ0fXdG+C4iRNxkhasrKBhME
Sf9OTJFs8uWi8wznCEIAgYCHuDbZUJW17YMX4eWxoTh3rfPxYejHoa1ULWzCgO3I
m7q8OeSV3YfYk00gtXjuPN+FtpnoWzX1ZmDv42mUl5JKHxHF26+mgQ7GDojfGOqd
U42afqS78BII8f0pZpQ0APmTI0VRz4K5/Rs5Fuzg4fyPhOj9fMjHAUXVlB8FQuEa
r9N0VZnO4xpLmSCtolrp9/ob9nw4ozd9+MDjmJOJ3FykrQI0TOt0taQCs+SERvAs
e2qvJqTHgGKOMCdT0824QTT1G5qdZOyOSY8YjT1d62ozyDleuApEAyYs03hzrh+q
Y5YGAIF9OAUwgxSMYPTm0mIw1tbMlMfWG9ub1K9TZODA+J0e2zJYgMse0tFTi+jR
G1mGy9CjzkG1g79Bvve2RSJf0Dr77r7K68iyX/DculNXjh8ijxY9rXV8Y7u9VXRb
Ja0bkz5k/+Fsu2KTdXsnb4LiGVNuoXvlfD0eesw/ecYIivo3v2pbfd+nuUYra8TD
1zoF6sYbxDBgeUJwTSOM6TKVWTw1yZVDdPcDW3CVHrZeqq88/aRJz6zRM5Rig8Tq
gcZlePfXEcWO2gmzuLfI1WG6pxKaX/VvPpIV3FmNW9a/8EQX7c69/6El4iDUUyWi
MizGISIQSr2XVgKgKzJFXRaPpwZaEaUSKtVznC4TnVn9efTHUzH7LERuYooREP1q
m9gtHGVnMC7bFk1R47uxELe37YhTNtm2LYeoJON9DxfIoUTu67Nfa2JPkf4raeLn
7aOrqmQ1n93xXnsDgcvxMEP6sfe4h2EUOGKc6/A7IES7slJKB5oZM06K/T4zkeY0
O/8BodolhYsqrsvxUfhjBWf6aBvOjAvSFlme8RXUZUM0kZ4Lpv8CoykxcezVe2Ye
LDvPmkQ13ygF4xx7wZZdiWgaFaKSYiumRakApfUj8Syu2dUSJlHqOYMGlmVeOE8E
aonV9Otsn+ZBmVQolJMT2ICBi/9i63qTGzawsgjt2QxHVq8qO7QUzmw3asRKOJNh
cnszCQh8Ws6yMuqVkVnJYNlriDATsZkph1uALDns+rqTSTGt/QZbIBm5tY4u6bFB
W8ZS3L4Qi//9JIm853t6iQzq3hrQuU8bdgu4jdHPECGfaJeS/LM3/5fgDB3ejVqn
st4M1Aroq04JAPcm7C89agPwLC1b6/nXhW86sftGk9frPVzZOsLxNnKK+OCbpqV2
dsoNJBntNRyYChF8EEDUi5MaUhI76g5i9Lh8ZYuzw4L7TkdzKAmuI6FI354rZLJG
uawX7G894p0SX5pnBvAaGKcvqMYIb0K6UAGgDAvA7LgMnJnUuEDN8gl+CR4Ou5wo
VfvYVWBk2g2HxRaXZv0eWVhSDlCY76I7NXMeY5gFJftAL7T0+X/RBDuhYZsJRM4q
Wr6fd+Ilo6yUi969swqkNScEpBZbIAo3DBZGUE+ZUlsV4iJPKnEpoHdJYqqyfZ7Y
U46fruZIcJT6JqN66OGCH1V/GrFae5Qr9eFgyhpdReG93n5nUTuaqVhSTeFWBg8n
wmINQ1wyJ49S6YK68fZBy6CbOGdqFVtgaJAzqmePKogZdsLNW/PFsM1Ex+o4ttzu
whinwpVyTpTuOl2fQ5GT5pzEjpAFPx5V2U+Q1PK6rHogB1+zXNgQD1TIAAn6UpK+
0h1JiyzukevQdkUn/ncoLEnGAD0x4sPyOeasy7UNRVW+3+Rm3TldCfirG3y/lTM3
6duY6ql+BGENYWtnEOXQPGSice/WLyP04Gz08XA+WJeJXaTMtw21q3H6YxyfW09r
yqOtj6sYZKfilbz4TPFHYuYgWFnjKLVp8l2FWkpZPKMtgQyTtAyameLRAExA0lqq
zWdKL+FqRKGgEtWz+soQs+7IHvyFA24is2ymXGoZdgnVrpICTEPvR1/OtjkukC5O
lSGKLEdu8HYzupYX1ieLjcoxesMVEZXfUJ8jRPotvkqY8APNmApJaMrRELymNtlI
Jar/rVvYFED8uO2dm45FRsG+VZSqfSvOF4CCWcXDGHU5zSJkNa1/gGZLiCTdfjkA
EwDZa0y2taDPgL1kPNBOT80npyKvPmfG7x/hzKgCC4QQcKMouVoSUFqs4RsPZSZ7
29gTB61Z+NOpfbvndIvHKTtI4+GeicnWWEJt4iCfWo0BvLCs0tSgbC2l8LKB8oSi
ZMOgjnx1yeya5hpn5YV/+iU5f4Haz5OLtvQk0ZrRZc6fTcezBW8NL79V7WoklnQl
+tvmWDXLxtjF5s7xZ3rc4VsjPNZszXqJTGVEQEeU7JfEIpxF2NyDFIqNnjkNfWg9
re2JCHRYFMb7o91msLvOJVDroxR5dmvCTHWbCy1qtupNA0zoV6hAWw51I0/9q9N6
magf4ChdTUwSDNXl27zrtpJInb2XgteKatZlezPSlKGrzZ2mhKgRKhrg0qTo4ftV
Et+b1BKAzQ9P9ATpySaZBNQY0PHOSz5gN0brSNo2x9RcL25CTUxBIz4AmNVt8oy0
FTTOrVai792V53Hz46IBFyHQtcuYgPIcSHsJZWIv+C35SfDHDe3+WeS9m5tMFBtR
18w7JxeYE3SKUAiiduTHOzQyXEwd8yp5BatBKpJAheeaF+p4MWLN7gc1sU5liDvw
g4ydLPE/K1MHbUhXqx7DkaRC21FAcGsGsfGKL4iM8gDXq1RQHb+NRwXhqXg2mEoH
BiCKgVNbMd85U7wng7mrHdZampol35bbHf/4UDSEXYND2uw4uO0xCMzQ//eLZemy
r6VyuuATSUrY2OeUr+tLcXNIcBKkRrRSqhQqDW+MTeU=
`pragma protect end_protected
