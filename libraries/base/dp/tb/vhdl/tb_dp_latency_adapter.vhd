-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_latency_adapter is
end tb_dp_latency_adapter;

architecture tb of tb_dp_latency_adapter is
  -- See tb_dp_pkg.vhd for explanation and run time

  subtype t_dut_range  is integer range - 1 to integer'high;

  type t_dut_natural_arr  is array (t_dut_range range <>) of natural;
  type t_dut_data_arr     is array (t_dut_range range <>) of std_logic_vector(c_dp_data_w - 1 downto 0);
  type t_dut_logic_arr    is array (t_dut_range range <>) of std_logic;  -- can not use STD_LOGIC_VECTOR because of integer range

  -- TX ready latency to DUT chain
  constant c_tx_latency     : natural := 3;
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0

  constant c_tx_offset_sop  : natural := 3;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 3,  10,  17, ...
  constant c_tx_offset_eop  : natural := 5;  -- eop in data valid cycle   5,  12,  19, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_tx_offset_sync : natural := 3;  -- sync in data valid cycle 3, 20, 37, ...
  constant c_tx_period_sync : natural := 17;

  -- The TB supports using 1 or more dp_latency_adapter Devices Under Test in a chain. DUT 0 is the first DUT and it
  -- gets the tx_data from this test bench, which has index -1. Each next DUT gets its input from the previous DUT,
  -- hence the ready latency between DUTs should be the same.
  -- The output latency of the previous must equal the input latency of the next DUT, hence it is sufficient to define
  -- only the DUT output latencies.
  --CONSTANT c_dut_latency    : t_dut_natural_arr := (c_tx_latency, 3);  -- verify single dp_latency_adapter with only wires
  --CONSTANT c_dut_latency    : t_dut_natural_arr := (c_tx_latency, 4);  -- verify single dp_latency_adapter with latency increase
  --CONSTANT c_dut_latency    : t_dut_natural_arr := (c_tx_latency, 1);  -- verify single dp_latency_adapter with latency decrease
  constant c_dut_latency    : t_dut_natural_arr := (c_tx_latency, 1, 2, 0, 5, 5, 2, 1, 0, 7);

  -- The nof dut latencies in the c_dut_latency array automatically also defines the nof DUTs c_nof_dut.
  constant c_nof_dut        : natural := c_dut_latency'high + 1;

  -- RX ready latency from DUT chain
  constant c_rx_latency     : natural := c_dut_latency(c_nof_dut - 1);

  constant c_verify_en_wait : natural := 10 + c_nof_dut * 2;  -- wait some cycles before asserting verify enable

  constant c_empty_offset   : natural := 1;
  constant c_channel_offset : natural := 2;

  constant c_random_w       : natural := 19;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal cnt_val        : std_logic;
  signal cnt_en         : std_logic;

  signal tx_data        : t_dp_data_arr(0 to c_tx_latency + c_tx_void);
  signal tx_val         : std_logic_vector(0 to c_tx_latency + c_tx_void);

  signal in_ready       : std_logic;
  signal in_data        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal in_sync        : std_logic;
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;

  -- DUT index -1 = in_data
  signal dut_ready      : t_dut_logic_arr(-1 to c_nof_dut - 1);  -- SISO
  signal dut_data       : t_dut_data_arr(-1 to c_nof_dut - 1);  -- SOSI
  signal dut_empty      : t_dut_data_arr(-1 to c_nof_dut - 1) := (others => (others => '0'));
  signal dut_channel    : t_dut_data_arr(-1 to c_nof_dut - 1) := (others => (others => '0'));
  signal dut_sync       : t_dut_logic_arr(-1 to c_nof_dut - 1);
  signal dut_val        : t_dut_logic_arr(-1 to c_nof_dut - 1);
  signal dut_sop        : t_dut_logic_arr(-1 to c_nof_dut - 1);
  signal dut_eop        : t_dut_logic_arr(-1 to c_nof_dut - 1);
  -- DUT index c_nof_dut-1 = out_data
  signal dut_siso       : t_dp_siso_arr(-1 to c_nof_dut - 1);
  signal dut_sosi       : t_dp_sosi_arr(-1 to c_nof_dut - 1) := (others => c_dp_sosi_rst);

  signal out_ready      : std_logic;
  signal prev_out_ready : std_logic_vector(0 to c_rx_latency);
  signal out_data       : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_empty      : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_channel    : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_sync       : std_logic;
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal hold_out_sop   : std_logic;
  signal prev_out_data  : std_logic_vector(out_data'range);

  signal state          : t_dp_state_enum;

  signal verify_en      : std_logic;
  signal verify_done    : std_logic;

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(19555, c_dp_data_w);
begin
  -- Use intervals marked by sync to start a new test named by state.
  --
  -- Under all circumstances the out_data should not mis or duplicate a count
  -- while out_val is asserted as checked by p_verify.
  -- The throughput must remain 100%, with only some increase in latency. This
  -- can be checked manually by checking that cnt_val does not toggle when the
  -- out_ready is asserted continuously. E.g. check that the out_data value
  -- is sufficiently high given the number of sync intervals that have passed.
  --
  -- Stimuli to verify the dp_latency_adapter DUT:
  --
  -- * Use various ready latency combinations in c_dut_latency:
  --   .     c_in_latency > c_out_latency = 0
  --   .     c_in_latency > c_out_latency > 0
  --   .     c_in_latency = c_out_latency = 0
  --   .     c_in_latency = c_out_latency > 0
  --   . 0 = c_in_latency < c_out_latency
  --   . 0 < c_in_latency < c_out_latency
  --
  -- * Manipulate the stimuli in:
  --   . p_cnt_en    : cnt_en not always active when in_ready is asserted
  --   . p_out_ready : out_ready not always active

  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  -- Input data
  cnt_val <= in_ready and cnt_en;

  proc_dp_cnt_dat(rst, clk, cnt_val, cnt_dat);
  proc_dp_tx_data(c_tx_latency, rst, clk, cnt_val, cnt_dat, tx_data, tx_val, in_data, in_val);
  proc_dp_tx_ctrl(c_tx_offset_sync, c_tx_period_sync, in_data, in_val, in_sync);
  proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data, in_val, in_sop);
  proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data, in_val, in_eop);

  -- Stimuli control
  proc_dp_count_en(rst, clk, sync, lfsr1, state, verify_done, tb_end, cnt_en);
  proc_dp_out_ready(rst, clk, sync, lfsr2, out_ready);

  -- Output verify
  proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
  proc_dp_verify_data("out_data", c_rx_latency, clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  proc_dp_verify_valid(c_rx_latency, clk, verify_en, out_ready, prev_out_ready, out_val);
  proc_dp_verify_ctrl(c_tx_offset_sync, c_tx_period_sync, "sync", clk, verify_en, out_data, out_val, out_sync);
  proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, out_data, out_val, out_sop);
  proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, out_data, out_val, out_eop);
  proc_dp_verify_sop_and_eop(c_rx_latency, false, clk, out_val, out_val, out_sop, out_eop, hold_out_sop);  -- Verify that sop and eop come in pairs, no check on valid between eop and sop
  proc_dp_verify_other_sosi("empty", INCR_UVEC(out_data, c_empty_offset), clk, verify_en, out_empty);
  proc_dp_verify_other_sosi("channel", INCR_UVEC(out_data, c_channel_offset), clk, verify_en, out_channel);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_equal, clk, verify_done, exp_data, out_data);

  ------------------------------------------------------------------------------
  -- Chain of 1 or more dp_latency_adapter DUTs
  --
  -- . Note this also models a series of streaming modules in a data path
  --
  ------------------------------------------------------------------------------

  -- Map the test bench tx counter data to the input of the chain
  in_ready        <= dut_ready(-1);
  dut_data(-1)    <=           in_data;
  dut_empty(-1)   <= INCR_UVEC(in_data, c_empty_offset);
  dut_channel(-1) <= INCR_UVEC(in_data, c_channel_offset);
  dut_sync(-1)    <= in_sync;
  dut_val(-1)     <= in_val;
  dut_sop(-1)     <= in_sop;
  dut_eop(-1)     <= in_eop;

  -- map sl, slv to record
  dut_ready(-1) <= dut_siso(-1).ready;  -- SISO
  dut_sosi(-1).data(c_dp_data_w - 1 downto 0) <= dut_data(-1);  -- SOSI
  dut_sosi(-1).empty                        <= dut_empty(-1)(c_dp_empty_w - 1 downto 0);
  dut_sosi(-1).channel                      <= dut_channel(-1)(c_dp_channel_w - 1 downto 0);
  dut_sosi(-1).sync                         <= dut_sync(-1);
  dut_sosi(-1).valid                        <= dut_val(-1);
  dut_sosi(-1).sop                          <= dut_sop(-1);
  dut_sosi(-1).eop                          <= dut_eop(-1);

  gen_chain : for I in 0 to c_nof_dut - 1 generate
    dut : entity work.dp_latency_adapter
    generic map (
      g_in_latency  => c_dut_latency(I - 1),
      g_out_latency => c_dut_latency(I)
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sink
      snk_out   => dut_siso(I - 1),
      snk_in    => dut_sosi(I - 1),
      -- ST source
      src_in    => dut_siso(I),
      src_out   => dut_sosi(I)
    );
  end generate;

  -- map record to sl, slv
  dut_siso(c_nof_dut - 1).ready <= dut_ready(c_nof_dut - 1);  -- SISO
  dut_data(c_nof_dut - 1)                               <= dut_sosi(c_nof_dut - 1).data(c_dp_data_w - 1 downto 0);  -- SOSI
  dut_empty(c_nof_dut - 1)(c_dp_empty_w - 1 downto 0)     <= dut_sosi(c_nof_dut - 1).empty;
  dut_channel(c_nof_dut - 1)(c_dp_channel_w - 1 downto 0) <= dut_sosi(c_nof_dut - 1).channel;
  dut_sync(c_nof_dut - 1)                               <= dut_sosi(c_nof_dut - 1).sync;
  dut_val(c_nof_dut - 1)                                <= dut_sosi(c_nof_dut - 1).valid;
  dut_sop(c_nof_dut - 1)                                <= dut_sosi(c_nof_dut - 1).sop;
  dut_eop(c_nof_dut - 1)                                <= dut_sosi(c_nof_dut - 1).eop;

  -- Map the output of the DUT chain to the test bench output data
  dut_ready(c_nof_dut - 1) <= out_ready;
  out_data               <= dut_data(c_nof_dut - 1);
  out_empty              <= dut_empty(c_nof_dut - 1);
  out_channel            <= dut_channel(c_nof_dut - 1);
  out_sync               <= dut_sync(c_nof_dut - 1);
  out_val                <= dut_val(c_nof_dut - 1);
  out_sop                <= dut_sop(c_nof_dut - 1);
  out_eop                <= dut_eop(c_nof_dut - 1);
end tb;
