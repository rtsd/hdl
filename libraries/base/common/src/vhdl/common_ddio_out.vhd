-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Double data rate FPGA output or register single data rate FPGA output

library IEEE, technology_lib, tech_iobuf_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_select_pkg.all;

entity common_ddio_out is
  generic(
    g_technology : natural := c_tech_select_default;
    g_width      : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
end common_ddio_out;

architecture str of common_ddio_out is
begin
  u_ddio_out : entity tech_iobuf_lib.tech_iobuf_ddio_out
  generic map (
    g_technology => g_technology,
    g_width      => g_width
  )
  port map (
    rst       => rst,
    in_clk    => in_clk,
    in_clk_en => in_clk_en,
    in_dat_hi => in_dat_hi,
    in_dat_lo => in_dat_lo,
    out_dat   => out_dat
  );
end str;
