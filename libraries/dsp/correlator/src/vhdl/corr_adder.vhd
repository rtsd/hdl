--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Provide an array of complex adders with streaming I/O
-- Description:
-- . Adds snk_in_2arr_2[i][0] to snk_in_2arr_2[i][1] yielding
--   src_out_arr[i] for i in 0..g_nof_inputs-1.
-- . Valid is taken from snk_in_2arr_2(i)(0) only.

entity corr_adder is
  generic (
    g_nof_inputs  : natural;
    g_data_w      : natural
   );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_2arr_2  : in  t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);

    src_out_arr   : out t_dp_sosi_arr(g_nof_inputs - 1 downto 0)
  );
end corr_adder;

architecture str of corr_adder is
  constant c_input_pipeline_depth  : natural := 0;
  constant c_output_pipeline_depth : natural := 1;
  constant c_pipeline_depth        : natural := c_input_pipeline_depth + c_output_pipeline_depth;

  -- Signal that carries the full adder output width
  signal common_complex_add_sub_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  -- Same as above but minus the extra bit added by the adder.
  signal ranged_common_complex_add_sub_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal dp_pipeline_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Delay valid signal
  -----------------------------------------------------------------------------
  gen_common_pipeline_sl : for i in 0 to g_nof_inputs - 1 generate
    u_common_pipeline_sl : entity common_lib.common_pipeline_sl
    generic map (
      g_pipeline  => c_pipeline_depth
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => snk_in_2arr_2(i)(0).valid,
      out_dat => src_out_arr(i).valid
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Delay sync signal of output 0
  -----------------------------------------------------------------------------
  u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_pipeline_depth
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => snk_in_2arr_2(0)(0).sync,
    out_dat => src_out_arr(0).sync
  );

  -----------------------------------------------------------------------------
  -- Complex adder
  -----------------------------------------------------------------------------
  gen_common_complex_add_sub : for i in 0 to g_nof_inputs - 1 generate
    u_common_complex_add_sub : entity common_lib.common_complex_add_sub
    generic map (
      g_direction       => "ADD",
      g_representation  => "SIGNED",
      g_pipeline_input  => c_input_pipeline_depth,
      g_pipeline_output => c_output_pipeline_depth,
      g_in_dat_w        => g_data_w,
      g_out_dat_w       => g_data_w + 1
    )
    port map (
      clk        => clk,
      in_ar      => snk_in_2arr_2(i)(0).re(g_data_w - 1 downto 0),
      in_ai      => snk_in_2arr_2(i)(0).im(g_data_w - 1 downto 0),
      in_br      => snk_in_2arr_2(i)(1).re(g_data_w - 1 downto 0),
      in_bi      => snk_in_2arr_2(i)(1).im(g_data_w - 1 downto 0),
      out_re     => common_complex_add_sub_src_out_arr(i).re(g_data_w + 1 - 1 downto 0),
      out_im     => common_complex_add_sub_src_out_arr(i).im(g_data_w + 1 - 1 downto 0)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Extend the sign bit
  -----------------------------------------------------------------------------
  gen_sign_extend : for i in 0 to g_nof_inputs - 1 generate
    src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_complex_add_sub_src_out_arr(i).re(g_data_w - 1 downto 0));
    src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_complex_add_sub_src_out_arr(i).im(g_data_w - 1 downto 0));
  end generate;
end str;
