--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: MAC for 10 Gigabit Ethernet via XGMII
-- Description:
--   See description of tech_mac_10g.
--
--   The wrapper is made because it brings the io_mac_10g at the same hierarchy
--   level as tr_xaui, which are then used in tr_10GbE. The wrapper also allows
--   for glue logic that may be needed in future and would fit better here than
--   in tech_mac_10g.
--
-- Remark:
-- . The CSR of the MAC is renamed mac_mosi/mac_miso at this IO level, to more
--   easily recognize it as the MM port of the MAC_10G IP.

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity io_mac_10g is
  generic (
    g_technology          : natural := c_tech_select_default;
    g_nof_channels        : natural := 1;
    g_pre_header_padding  : boolean := false
  );
  port (
    -- MM
    mm_clk            : in  std_logic;
    mm_rst            : in  std_logic;
    mac_mosi          : in  t_mem_mosi;  -- CSR = control status register
    mac_miso          : out t_mem_miso;

    -- ST
    tx_clk_312        : in  std_logic := '0';  -- 312.5 MHz
    tx_clk_156        : in  std_logic;  -- 156.25 MHz local reference
    tx_rst            : in  std_logic;
    tx_snk_in         : in  t_dp_sosi;  -- 64 bit data
    tx_snk_out        : out t_dp_siso;

    rx_clk_312        : in  std_logic := '0';  -- 312.5 MHz
    rx_clk_156        : in  std_logic;  -- 156.25 MHz from rx phy
    rx_rst            : in  std_logic;
    rx_src_out        : out t_dp_sosi;  -- 64 bit data
    rx_src_in         : in  t_dp_siso;

    -- XGMII
    xgmii_link_status : out std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0);  -- 2 bit
    xgmii_tx_data     : out std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit
    xgmii_rx_data     : in  std_logic_vector(c_xgmii_w - 1 downto 0)  -- 72 bit
  );
end io_mac_10g;

architecture str of io_mac_10g is
begin
  u_tech_mac_10g : entity tech_mac_10g_lib.tech_mac_10g
  generic map (
    g_technology          => g_technology,
    g_pre_header_padding  => g_pre_header_padding
  )
  port map (
    -- MM
    mm_clk            => mm_clk,
    mm_rst            => mm_rst,
    csr_mosi          => mac_mosi,
    csr_miso          => mac_miso,

    -- ST
    tx_clk_312        => tx_clk_312,
    tx_clk_156        => tx_clk_156,
    tx_rst            => tx_rst,
    tx_snk_in         => tx_snk_in,  -- 64 bit data
    tx_snk_out        => tx_snk_out,

    rx_clk_312        => rx_clk_312,
    rx_clk_156        => rx_clk_156,
    rx_rst            => rx_rst,
    rx_src_out        => rx_src_out,  -- 64 bit data
    rx_src_in         => rx_src_in,

    -- XGMII
    xgmii_link_status => xgmii_link_status,
    xgmii_tx_data     => xgmii_tx_data,
    xgmii_rx_data     => xgmii_rx_data
  );
end str;
