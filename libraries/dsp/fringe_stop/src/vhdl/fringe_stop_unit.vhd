-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author: Hajee Pepping
-- Purpose: The fringe_stop_unit implements a fringe stopping algorithm.
-- Description:
--   Figure 13 in the detailed design section 4.3 of ASTRON-MEM-199 shows a
--   block diagram of fringe_stop_unit.
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib, technology_lib, dp_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_math_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity fringe_stop_unit is
  generic (
    g_technology     : natural  := c_tech_select_default;
    g_in_dat_w       : positive := 8;  -- Width of the incoming data
    g_fs_offset_w    : positive := 10;  -- Width of the offset of the linear coefficient
    g_fs_step_w      : positive := 17;  -- Width of the step of the linear coefficient
    g_accu_w         : positive := 31;  -- Width of the accumulation register
    g_phi_minus_sign : boolean := true;  -- Apply exp(j*phi) or exp(-j*phi) based on phasor lookup table address phi
    g_phasor_w       : positive := 9;  -- Width of the phasor values in the lookup table
    g_nof_channels   : positive := 256  -- Number of serial channels for which the fringe stopping must be applied uniquely
  );
  port (
    -- System
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;

    -- MM interface
    ram_fringe_stop_offset_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_fringe_stop_offset_miso : out t_mem_miso := c_mem_miso_rst;
    ram_fringe_stop_step_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    ram_fringe_stop_step_miso   : out t_mem_miso := c_mem_miso_rst;

    -- ST interface
    in_sosi                 : in  t_dp_sosi;
    out_sosi                : out t_dp_sosi
  );
end fringe_stop_unit;

architecture str of fringe_stop_unit is
  -- invert the c_lookup_imag waveform, because the look up address phi for exp(-j*phi) is calculated,
  -- but u_multiplier applies it as exp(j*phi)
  -- . g_phi_minus_sign = FALSE : exp(j*  phi) =                           cos(phi) + j*sin(phi)
  -- . g_phi_minus_sign = TRUE  : exp(j* -phi) = cos(-phi) + j*sin(-phi) = cos(phi) + j*sin(phi + pi)
  constant c_lookup_len     : positive := 2**g_fs_offset_w;
  constant c_lookup_real    : t_nat_integer_arr := common_math_create_look_up_table_cos(c_lookup_len, g_phasor_w, false);
  constant c_lookup_imag    : t_nat_integer_arr := common_math_create_look_up_table_sin(c_lookup_len, g_phasor_w, g_phi_minus_sign);

  constant c_fs_ram_w    : positive := ceil_log2(g_nof_channels);

  constant c_input_latency            : natural := 2;  -- It is 2 because we use the in_sosi.sync to swap the page of dual page memory.
  constant c_step_adder_stage_latency : natural := 1;
  constant c_sum_adder_stage_latency  : natural := 1;
  constant c_look_up_latency          : natural := 0;
  constant c_cmult_latency            : natural := 3;
  constant c_total_latency            : natural := c_input_latency + c_step_adder_stage_latency + c_sum_adder_stage_latency +
                                                   c_look_up_latency + c_cmult_latency;

  constant c_product_w                : positive := g_in_dat_w + g_phasor_w;  -- no need for + c_sum_of_prod_w, because phasor amplitude is 1

  type reg_type is record
    in_sosi_arr           : t_dp_sosi_arr(c_total_latency - 1 downto 0);
    page_turn_arr         : std_logic_vector(c_total_latency - 1 downto 0);
    flush_accu            : std_logic;
    fs_step_accu_valid    : std_logic;
    fs_offset_data_valid  : std_logic;
    fs_sum_valid          : std_logic;
    fs_index_valid        : std_logic;
  end record;

  signal dbg_phi_minus_sign   : boolean := g_phi_minus_sign;
  signal dbg_lookup_real      : t_nat_integer_arr(c_lookup_len - 1 downto 0) := c_lookup_real;
  signal dbg_lookup_imag      : t_nat_integer_arr(c_lookup_len - 1 downto 0) := c_lookup_imag;
  signal dbg_lookup_real_dc   : integer := common_math_sum_look_up_table(c_lookup_real);
  signal dbg_lookup_imag_dc   : integer := common_math_sum_look_up_table(c_lookup_imag);

  signal r, rin               : reg_type;
  signal r_rst                : reg_type := ((others => c_dp_sosi_rst), (others => '0'), '0', '0', '0', '0', '0');

  signal wr_offset_last       : std_logic;
  signal wr_offset_last_dp    : std_logic;
  signal wr_offset_last_hold  : std_logic;
  signal wr_step_last         : std_logic;
  signal wr_step_last_dp      : std_logic;
  signal wr_step_last_hold    : std_logic;
  signal page_turn            : std_logic;
  signal page_turn_mm         : std_logic;

  signal fs_addr              : std_logic_vector(c_fs_ram_w - 1 downto 0);
  signal fs_offset_data       : std_logic_vector(g_fs_offset_w - 1 downto 0);
  signal fs_offset_data_valid : std_logic;
  signal fs_offset_data_piped : std_logic_vector(g_fs_offset_w - 1 downto 0);
  signal fs_step_data         : std_logic_vector(g_fs_step_w - 1 downto 0);
  signal fs_step_data_valid   : std_logic;
  signal fs_step_data_resized : std_logic_vector(g_accu_w - 1 downto 0);
  signal accu_flushed         : std_logic_vector(g_accu_w - 1 downto 0);
  signal accu_out             : std_logic_vector(g_accu_w - 1 downto 0);
  signal accu_in              : std_logic_vector(g_accu_w - 1 downto 0);
  signal fs_step_accumulated  : std_logic_vector(g_fs_offset_w - 1 downto 0);
  signal fs_sum               : std_logic_vector(g_fs_offset_w downto 0);
  signal phasor_real          : std_logic_vector(g_phasor_w - 1 downto 0);
  signal phasor_imag          : std_logic_vector(g_phasor_w - 1 downto 0);
  signal result_re            : std_logic_vector(c_product_w - 1 downto 0);
  signal result_im            : std_logic_vector(c_product_w - 1 downto 0);
begin
  assert dbg_lookup_real_dc = 0
    report "Fringe stop lookup COS must have DC = 0"
    severity ERROR;
  assert dbg_lookup_imag_dc = 0
    report "Fringe stop lookup SIN must have DC = 0"
    severity ERROR;

  ------------------------------------------------------------------------------
  -- Input registers
  ------------------------------------------------------------------------------
  comb : process(dp_rst, r, in_sosi, page_turn, fs_step_data_valid, fs_offset_data_valid)
    variable v : reg_type;
  begin
    v := r;
    v.in_sosi_arr(0) := in_sosi;
    v.in_sosi_arr(c_total_latency - 1 downto 1) := r.in_sosi_arr(c_total_latency - 2 downto 0);
    v.page_turn_arr(0) := page_turn;
    v.page_turn_arr(c_total_latency - 1 downto 1) := r.page_turn_arr(c_total_latency - 2 downto 0);

    v.fs_step_accu_valid    := fs_step_data_valid;  -- Valid signals only used as reference in simulation
    v.fs_sum_valid          := r.fs_step_accu_valid;  -- Valid signals only used as reference in simulation
    v.fs_offset_data_valid  := fs_offset_data_valid;  -- Valid signals only used as reference in simulation
    v.fs_index_valid        := r.in_sosi_arr(5).valid;  -- Valid signals only used as reference in simulation

    -- Create the flush_accu register for flushing the accumulation register.
    -- Only flush accu at page_turn, to keep on stepping if there is no MM control
    -- ON when active SYNC received, where active is indicated by page_turn
    -- OFF when next EOP is received.
    -- Basically it is a packet-long extension of the sync/page_turn pulse to clear the accu for all g_nof_channels
    if r.page_turn_arr(0) = '1' and r.flush_accu = '0' then
      v.flush_accu := '1';
    end if;
    if r.in_sosi_arr(1).eop = '1' and r.flush_accu = '1' then
      v.flush_accu := '0';
    end if;

    if dp_rst = '1' then
      v := r_rst;
    end if;

    rin <= v;
  end process comb;

  regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Detect mechanism to check if the complete offset and step ram is written
  -- before the next sync pulse.
  ------------------------------------------------------------------------------
  wr_offset_last <= '1' when unsigned(ram_fringe_stop_offset_mosi.address(c_fs_ram_w - 1 downto 0)) = g_nof_channels - 1 and ram_fringe_stop_offset_mosi.wr = '1' else '0';
  wr_step_last   <= '1' when unsigned(ram_fringe_stop_step_mosi.address(  c_fs_ram_w - 1 downto 0)) = g_nof_channels - 1 and ram_fringe_stop_step_mosi.wr = '1' else '0';

  -- Making the MM write pulse available in the DP clock domain
  u_common_spulse_wr_offset_last : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst     => mm_rst,
    in_clk     => mm_clk,
    in_pulse   => wr_offset_last,
    out_rst    => dp_rst,
    out_clk    => dp_clk,
    out_pulse  => wr_offset_last_dp
  );

  u_common_spulse_wr_step_last : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst     => mm_rst,
    in_clk     => mm_clk,
    in_pulse   => wr_step_last,
    out_rst    => dp_rst,
    out_clk    => dp_clk,
    out_pulse  => wr_step_last_dp
  );

  -- Holding the write pulse until the next sync
  u_common_switch_offset : entity common_lib.common_switch
  port map(
    rst         => dp_rst,
    clk         => dp_clk,
    switch_high => wr_offset_last_dp,
    switch_low  => in_sosi.sync,
    out_level   => wr_offset_last_hold
  );

  u_common_switch_step : entity common_lib.common_switch
  port map(
    rst         => dp_rst,
    clk         => dp_clk,
    switch_high => wr_step_last_dp,
    switch_low  => in_sosi.sync,
    out_level   => wr_step_last_hold
  );

  -- Page turn only happens when both offset and step memories have been written.
  page_turn <= in_sosi.sync and wr_step_last_hold and wr_offset_last_hold;

  ------------------------------------------------------------------------------
  -- Making the page turn pulse available in the MM clock domain
  ------------------------------------------------------------------------------
  u_common_spulse_page_turn : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst     => dp_rst,
    in_clk     => dp_clk,
    in_pulse   => page_turn,
    out_rst    => mm_rst,
    out_clk    => mm_clk,
    out_pulse  => page_turn_mm
  );

  ------------------------------------------------------------------------------
  -- Fringe stop Offset memory (dual page and dual port)
  ------------------------------------------------------------------------------
  u_fringe_stop_offset_ram : entity common_lib.common_paged_ram_crw_crw
  generic map (
    g_technology     => g_technology,
    g_str            => "use_adr",
    g_data_w         => g_fs_offset_w,
    g_nof_pages      => 2,
    g_page_sz        => g_nof_channels,
    g_start_page_a   => 0,
    g_start_page_b   => 1,
    g_rd_latency     => 1,
    g_true_dual_port => true
  )
  port map(
    rst_a       => mm_rst,
    clk_a       => mm_clk,

    next_page_a => page_turn_mm,
    adr_a       => ram_fringe_stop_offset_mosi.address(c_fs_ram_w - 1 downto 0),
    wr_en_a     => ram_fringe_stop_offset_mosi.wr,
    wr_dat_a    => ram_fringe_stop_offset_mosi.wrdata(g_fs_offset_w - 1 downto 0),
    rd_en_a     => ram_fringe_stop_offset_mosi.rd,
    rd_dat_a    => ram_fringe_stop_offset_miso.rddata(g_fs_offset_w - 1 downto 0),
    rd_val_a    => ram_fringe_stop_offset_miso.rdval,

    rst_b       => dp_rst,
    clk_b       => dp_clk,
    next_page_b => page_turn,
    adr_b       => fs_addr,
    wr_en_b     => '0',
    wr_dat_b    => (others => '0'),
    rd_en_b     => r.in_sosi_arr(0).valid,
    rd_dat_b    => fs_offset_data,
    rd_val_b    => fs_offset_data_valid
  );

  ------------------------------------------------------------------------------
  -- Fringe stop Step memory (dual page and dual port)
  ------------------------------------------------------------------------------
  u_fringe_stop_step_ram : entity common_lib.common_paged_ram_crw_crw
  generic map (
    g_technology     => g_technology,
    g_str            => "use_adr",
    g_data_w         => g_fs_step_w,
    g_nof_pages      => 2,
    g_page_sz        => g_nof_channels,
    g_start_page_a   => 0,
    g_start_page_b   => 1,
    g_rd_latency     => 1,
    g_true_dual_port => true
  )
  port map(
    rst_a       => mm_rst,
    clk_a       => mm_clk,

    next_page_a => page_turn_mm,
    adr_a       => ram_fringe_stop_step_mosi.address(c_fs_ram_w - 1 downto 0),
    wr_en_a     => ram_fringe_stop_step_mosi.wr,
    wr_dat_a    => ram_fringe_stop_step_mosi.wrdata(g_fs_step_w - 1 downto 0),
    rd_en_a     => ram_fringe_stop_step_mosi.rd,
    rd_dat_a    => ram_fringe_stop_step_miso.rddata(g_fs_step_w - 1 downto 0),
    rd_val_a    => ram_fringe_stop_step_miso.rdval,

    rst_b       => dp_rst,
    clk_b       => dp_clk,
    next_page_b => page_turn,
    adr_b       => fs_addr,
    wr_en_b     => '0',
    wr_dat_b    => (others => '0'),
    rd_en_b     => r.in_sosi_arr(0).valid,
    rd_dat_b    => fs_step_data,
    rd_val_b    => fs_step_data_valid
  );

  ------------------------------------------------------------------------------
  -- Pipeline offset to align with step accumulation
  ------------------------------------------------------------------------------
  u_fs_offset_data_pipeline : entity common_lib.common_pipeline
  generic map(
    g_representation => "UNSIGNED",
    g_pipeline       => 1,
    g_in_dat_w       => g_fs_offset_w,
    g_out_dat_w      => g_fs_offset_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    in_dat  => fs_offset_data,
    out_dat => fs_offset_data_piped
  );

  ------------------------------------------------------------------------------
  -- Counter used to create addresses for both the offset ram and the step ram
  ------------------------------------------------------------------------------
  u_fs_adrs_cnt : entity common_lib.common_counter
  generic map(
    g_latency   => 1,
    g_init      => 0,
    g_width     => c_fs_ram_w,
    g_max       => g_nof_channels - 1,
    g_step_size => 1
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => r.in_sosi_arr(0).eop,
    cnt_en  => r.in_sosi_arr(0).valid,
    count   => fs_addr
  );

  ------------------------------------------------------------------------------
  -- Adder for auto incrementing the step.(phi1 portion)
  ------------------------------------------------------------------------------
  u_step_adder : entity common_lib.common_add_sub
  generic map(
    g_direction       => "ADD",
    g_representation  => "UNSIGNED",
    g_pipeline_input  => 0,
    g_pipeline_output => 0,
    g_in_dat_w        => g_accu_w,
    g_out_dat_w       => g_accu_w
  )
  port map(
    clk     => dp_clk,
    in_a    => fs_step_data_resized,
    in_b    => accu_flushed,
    result  => accu_in
  );

  -- Resize to match the input width of the adder
  fs_step_data_resized <= RESIZE_SVEC(fs_step_data, g_accu_w);

  -- Flush the accumulation register when flush_accu is '1'. This is when a sync is received.
  accu_flushed <= accu_out when r.flush_accu = '0' else (others => '0');

  ------------------------------------------------------------------------------
  -- Accumulation FIFO where the intermediate phi1's are stored.
  ------------------------------------------------------------------------------
  u_accumulate_register : entity common_lib.common_delay
  generic map(
    g_dat_w => g_accu_w,
    g_depth => g_nof_channels
  )
  port map(
    clk      => dp_clk,
    in_val   => fs_step_data_valid,
    in_dat   => accu_in,
    out_dat  => accu_out
  );

  u_step_pipe : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => 1,
    g_in_dat_w       => g_fs_offset_w,
    g_out_dat_w      => g_fs_offset_w
  )
  port map (
    clk     => dp_clk,
    in_dat  => accu_in(g_accu_w - 1 downto g_accu_w - g_fs_offset_w),
    out_dat => fs_step_accumulated
  );

  ------------------------------------------------------------------------------
  -- Adder that combines the offset and the step. phi0 + phi1
  ------------------------------------------------------------------------------
  u_phi0_phi1_adder : entity common_lib.common_add_sub
  generic map(
    g_direction       => "ADD",
    g_representation  => "UNSIGNED",
    g_pipeline_input  => 0,
    g_pipeline_output => 1,
    g_in_dat_w        => g_fs_offset_w,
    g_out_dat_w       => g_fs_offset_w + 1
  )
  port map(
    clk     => dp_clk,
    in_a    => fs_step_accumulated,
    in_b    => fs_offset_data_piped,
    result  => fs_sum
  );

  phasor_real <= TO_SVEC(c_lookup_real(TO_UINT(fs_sum(g_fs_offset_w - 1 downto 0))), g_phasor_w);
  phasor_imag <= TO_SVEC(c_lookup_imag(TO_UINT(fs_sum(g_fs_offset_w - 1 downto 0))), g_phasor_w);

  u_multiplier : entity common_mult_lib.common_complex_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => "IP",
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_phasor_w,
    g_out_p_w          => c_product_w,
    g_conjugate_b      => false,
    g_pipeline_input   => 1,
    g_pipeline_product => 0,
    g_pipeline_adder   => 1,
    g_pipeline_output  => 1
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    in_ar      => r.in_sosi_arr(c_total_latency - c_cmult_latency - 1).re(g_in_dat_w - 1 downto 0),
    in_ai      => r.in_sosi_arr(c_total_latency - c_cmult_latency - 1).im(g_in_dat_w - 1 downto 0),
    in_br      => phasor_real,
    in_bi      => phasor_imag,
    out_pr     => result_re,
    out_pi     => result_im
  );

  p_set_output : process(r, result_re, result_im)
  begin
    out_sosi    <= r.in_sosi_arr(c_total_latency - 1);
    out_sosi.re <= RESIZE_DP_DSP_DATA(result_re);
    out_sosi.im <= RESIZE_DP_DSP_DATA(result_im);
  end process;
end str;
