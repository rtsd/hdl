--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
--USE dp_lib.tb_dp_pkg.ALL;
use common_lib.tb_common_pkg.all;

entity tb_corr_carousel is
  generic (
    g_nof_inputs  : natural := 8;
    g_nof_outputs : natural := 9
  );
end tb_corr_carousel;

architecture tb of tb_corr_carousel is
  constant c_clk_period                   : time := 10 ns;
  constant c_data_w                       : natural := 32;
  constant c_nof_permutations             : natural := (g_nof_inputs * (g_nof_inputs + 1)) / 2;
  constant c_nof_clk_cycles_per_input_val : natural := c_nof_permutations / g_nof_outputs;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '1';
  signal rst            : std_logic;

  signal carousel_snk_in_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal carousel_src_out_2arr_2 : t_dp_sosi_2arr_2(g_nof_outputs - 1 downto 0);
begin
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  u_corr_carousel : entity work.corr_carousel
  generic map (
    g_nof_inputs  => g_nof_inputs,
    g_nof_outputs => g_nof_outputs
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_in_arr  => carousel_snk_in_arr,
    src_out_arr => carousel_src_out_2arr_2
  );

  p_stimuli: process
  begin
    wait until rst = '0';
    wait for 100 ns;
    for i in 0 to 10 - 1 loop
      for j in 0 to g_nof_inputs - 1 loop
        -- Feed the carousel. Use the stream index as data word
        carousel_snk_in_arr(j).data(c_data_w - 1 downto 0) <= TO_UVEC(j, c_data_w);
        carousel_snk_in_arr(j).valid <= '1';
      end loop;
      wait for c_clk_period;
      for j in 0 to g_nof_inputs - 1 loop
        -- Let the carousel do its thing
        carousel_snk_in_arr(j).data <= (others => '0');
        carousel_snk_in_arr(j).valid <= '0';
      end loop;
    wait for (c_nof_clk_cycles_per_input_val - 1) * c_clk_period;
    end loop;
  end process;
end tb;
