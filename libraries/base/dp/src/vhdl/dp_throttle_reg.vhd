-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_throttle_reg is
  generic (
    g_dc_period      : natural
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;

    st_rst            : in  std_logic;
    st_clk            : in  std_logic;

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso := c_mem_miso_rst;  -- actual ranges defined by c_mm_reg

    throttle          : out std_logic_vector(ceil_log2(g_dc_period + 1) - 1 downto 0)
  );
end dp_throttle_reg;

architecture rtl of dp_throttle_reg is
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 2,
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 1,
                                  init_sl  => '0');

  signal mm_throttle : std_logic_vector(ceil_log2(g_dc_period + 1) - 1 downto 0);
begin
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Access event, register values
      mm_throttle  <= (others => '0');
      elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            mm_throttle <= sla_in.wrdata(ceil_log2(g_dc_period + 1) - 1 downto 0);

          when others => null;
        end case;
      end if;
    end if;
  end process;

  u_common_reg_cross_domain : entity common_lib.common_reg_cross_domain
  port map (
    in_rst      => mm_rst,
    in_clk      => mm_clk,
    in_dat      => mm_throttle,
    in_done     => OPEN,
    out_rst     => st_rst,
    out_clk     => st_clk,
    out_dat     => throttle,
    out_new     => open
  );
end rtl;
