-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose: Retrieve blocks of g_nof_ch_sel complex data words from a dual
--          page data buffer
-- Description:
--   The retrieve control uses a channel select buffer to know the order
--   in which the g_nof_ch_sel complex data words have to be retrieved from the
--   dual page input data buffer. The order is arbitrary and the same channel
--   may be selected multiple times.
-- Remarks:
-- . Typcially output_siso.ready='1'. When g_nof_ch_sel < g_nof_ch_in/2, then a
--   toggling output_siso.ready can be used to multiplex this reorder retrieve output
--   with another reorder retrieve output stream.
-- . The retrieve_done signal occurs when the last data of the block is read
--   requested, so 1 cycle before the output_sosi.eop.
-- . The timing of the ch_cnt for the retrieve_mosi.address is such that the
--   reorder_col can store a frame and retrieve it immediately in any order, so worst
--   case the last stored data can be retrieved first.

entity reorder_retrieve is
  generic (
    g_dsp_data_w   : natural;  -- complex data width, = c_data_w / 2
    g_nof_ch_in    : natural;
    g_nof_ch_sel   : natural
  );
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;

    -- Timing
    store_done        : in  std_logic;

    -- Read databuf control
    retrieve_mosi     : out t_mem_mosi;
    retrieve_miso     : in  t_mem_miso;
    retrieve_done     : out std_logic;

    -- Read selectbuf control
    select_mosi       : out t_mem_mosi;
    select_miso       : in  t_mem_miso;

    -- Streaming
    output_sosi       : out t_dp_sosi;
    output_siso       : in  t_dp_siso := c_dp_siso_rdy
  );
end reorder_retrieve;

architecture rtl of reorder_retrieve is
  constant c_data_w          : natural := c_nof_complex * g_dsp_data_w;
  constant c_retrieve_lat    : natural := 2;  -- fixed 1 for select buf read + 1 for store buf read

  signal ch_cnt              : integer range 0 to g_nof_ch_sel - 1;
  signal nxt_ch_cnt          : integer;

  signal retrieve_en         : std_logic;
  signal prev_retrieve_ready : std_logic;
  signal retrieve_ready      : std_logic;
  signal nxt_retrieve_done   : std_logic;

  signal retrieve_sop_dly    : std_logic_vector(0 to c_retrieve_lat);
  signal retrieve_eop_dly    : std_logic_vector(0 to c_retrieve_lat);
begin
  p_reg : process (clk, rst)
  begin
    if rst = '1' then
      -- Internal registers.
      ch_cnt                                 <= 0;
      prev_retrieve_ready                    <= '0';
      retrieve_sop_dly(1 to c_retrieve_lat)  <= (others => '0');
      retrieve_eop_dly(1 to c_retrieve_lat)  <= (others => '0');
      -- Output registers.
      retrieve_done                          <= '0';
    elsif rising_edge(clk) then
      -- Internal registers.
      ch_cnt                                 <= nxt_ch_cnt;
      prev_retrieve_ready                    <= retrieve_ready;
      retrieve_sop_dly(1 to c_retrieve_lat)  <= retrieve_sop_dly(0 to c_retrieve_lat - 1);
      retrieve_eop_dly(1 to c_retrieve_lat)  <= retrieve_eop_dly(0 to c_retrieve_lat - 1);
      -- Output registers.
      retrieve_done                          <= nxt_retrieve_done;
    end if;
  end process;

  -- Enable retrieve when a block has been stored, disable retrieve when the block has been output
  u_retrieve_en : entity common_lib.common_switch
  generic map (
    g_rst_level    => '0',
    -- store_done has priority over nxt_retrieve_done when they occur simultaneously
    g_priority_lo  => false,
    g_or_high      => true,
    g_and_low      => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => store_done,
    -- can not use retrieve_done with g_and_low = TRUE, because if retrieve_done
    -- occurs after next store_done then that page gets missed
    switch_low  => nxt_retrieve_done,
    out_level   => retrieve_en
  );

  retrieve_ready <= retrieve_en and output_siso.ready;

  p_ch_cnt : process (retrieve_ready, ch_cnt)
  begin
    nxt_retrieve_done <= '0';
    nxt_ch_cnt <= ch_cnt;

    if retrieve_ready = '1' then
      if ch_cnt = g_nof_ch_sel - 1 then
        nxt_retrieve_done <= '1';
        nxt_ch_cnt <= 0;
      else
        nxt_ch_cnt <= ch_cnt + 1;
      end if;
    end if;
  end process;

  -- Optional SS output frame control
  retrieve_sop_dly(0) <= '1' when retrieve_ready = '1' and ch_cnt = 0              else '0';
  retrieve_eop_dly(0) <= '1' when retrieve_ready = '1' and ch_cnt = g_nof_ch_sel - 1 else '0';

  -- First read store buf address from select buf when the output is ready
  select_mosi.rd        <= '1';  -- no need to use retrieve_ready here, keep rd active to ease timing closure
  select_mosi.address   <= TO_MEM_ADDRESS(ch_cnt);

  -- Then use the read select address to read the data from the store buf
  retrieve_mosi.rd      <= prev_retrieve_ready;
  retrieve_mosi.address <= RESIZE_MEM_ADDRESS(select_miso.rddata(ceil_log2(g_nof_ch_in) - 1 downto 0));

  -- The output_sosi has RL=2, because of the read accesses to the select buf followed by the
  -- read access to the store buf, both with read latency is 1, so c_retrieve_lat=2.
  output_sosi.re    <= RESIZE_DP_DSP_DATA(retrieve_miso.rddata(                g_dsp_data_w - 1 downto 0));
  output_sosi.im    <= RESIZE_DP_DSP_DATA(retrieve_miso.rddata(c_nof_complex * g_dsp_data_w - 1 downto g_dsp_data_w));
  output_sosi.data  <= RESIZE_DP_DATA(retrieve_miso.rddata(c_data_w - 1 downto 0));
  output_sosi.valid <= retrieve_miso.rdval;
  output_sosi.sop   <= retrieve_sop_dly(c_retrieve_lat);
  output_sosi.eop   <= retrieve_eop_dly(c_retrieve_lat);
end rtl;
