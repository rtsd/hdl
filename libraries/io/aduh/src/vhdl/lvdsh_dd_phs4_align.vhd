-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Word align the 800M Hz sample data to 200 MHz data path clock.
-- Description:
--   The 800 MHz in_dat samples have already been captured at the pin by
--   a common_ddio_in component.
--   Default in_maintain_phs='0' to achieve and restore word alignment. Once
--   word alignment has been achieved in_maintain_phs can be set to '1' to
--   maintain this alignment and ignore any subsequent timing jitter
--   between in_clk and dp_clk. This avoids that a too severe timing jitter
--   would cause a restore action on the word alignment, which then
--   could lead to an unwanted sample shift in the out_dat. The assumption
--   is that the initial word alignment that is found is good enough. Any
--   remaining sample phase uncertainty that could occur after a power cycle
--   needs to be calibrated for at a higher level.

entity lvdsh_dd_phs4_align is
  generic (
    g_wb_factor         : natural := 4;  -- fixed wideband factor 4 = c_rx_factor*c_dd_factor
    g_nof_dp_phs_clk    : natural := 2;  -- nof dp_phs_clk that can be used to detect lock
    g_dp_phs_clk_period : natural := 32;  -- number of dp_clk periods per dp_phs_clk period, must match g_clk*_divide_by in unb_clk200_pll
    g_dd_phs_locked_w   : natural := 8;  -- used to ensure that dd_phs_locked is only declared if dd_phs_detected is stable for at least 2**(g_dd_phs_locked_w-1) cycles
    g_in_dat_w          : natural := 8  -- nof PHY data bits
  );
  port (
    -- DP clock reference for word alignment
    dp_phs_clk_vec     : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);  -- used as data input for in_clk domain
    dp_phs_clk_select  : in  natural range 0 to g_nof_dp_phs_clk - 1 := 0;

    -- PHY input interface
    in_rst             : in  std_logic := '0';
    in_clk             : in  std_logic := '1';
    in_dat_hi          : in  std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t1], [t3], [t5], [t7], ... --> time
    in_dat_lo          : in  std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t0], [t2], [t4], [t6], ... --> time
    in_maintain_phs    : in  std_logic := '0';

    raw_phs            : out std_logic_vector(           g_wb_factor - 1 downto 0);  -- the measured sample phase before realignment
    out_phs_locked     : out std_logic;  -- '1' when realigned sample phase is stable and correct for at least 2**(g_dd_phs_locked_w-1) cycles
    out_sync           : out std_logic_vector(           g_wb_factor - 1 downto 0);  -- the measured word   phase after realignment
    out_dat            : out std_logic_vector(g_in_dat_w * g_wb_factor - 1 downto 0);  -- output words in little endian format [t3, t2, t1, t0]
    out_val            : out std_logic
  );
end lvdsh_dd_phs4_align;

architecture str of lvdsh_dd_phs4_align is
  constant c_ref_pipeline       : natural := 1;  -- need pipeline to achieve 400 MHz when g_nof_dp_phs_clk=8
  constant c_align_pipeline     : natural := 1;  -- use pipeline to achieve 400 MHz
  constant c_offset_delay_len   : integer := c_ref_pipeline + c_align_pipeline;  -- = 1+1
  constant c_delay_len          : natural := c_meta_delay_len + c_offset_delay_len;  -- = 3 + 1+1
  constant c_dd_factor          : natural := 2;  -- fixed double data rate factor
  constant c_rx_factor          : natural := 2;  -- fixed for g_wb_factor = c_rx_factor*c_dd_factor = 4
  constant c_in_phs_clk_period  : natural := c_dd_factor * g_dp_phs_clk_period;
  constant c_dd_phs_w           : natural := g_wb_factor;  -- = 4 * 1b = 4b
  constant c_dd_dat_w           : natural := g_wb_factor * g_in_dat_w;  -- = 4 * 8b = 32b
  constant c_exp_raw_phs_arr    : t_natural_arr(g_wb_factor - 1 downto 0) := (3, 6, 12, 9);  -- the expected word phase before realignment is fixed, the other values in the range indicate incorrect phase detection
  constant c_exp_phs            : natural := 3;  -- the expected word phase after realignment is fixed, the other values in the range indicate incorrect phase detection

  signal ref_r_vec           : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
  signal ref_f_vec           : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
  signal sel_r               : std_logic;
  signal sel_f               : std_logic;
  signal ref_align_en        : std_logic;
  signal ref_align_r         : std_logic;
  signal ref_align_f         : std_logic;
  signal ref_r               : std_logic;
  signal ref_f               : std_logic;
  signal sync_r              : std_logic;
  signal sync_f              : std_logic;

  type t_in_reg is record
    -- 1 sample adjust
    ref_init        : std_logic;
    ref_r_val       : std_logic;
    ref_f_val       : std_logic;
    ref_r           : std_logic;
    ref_f           : std_logic;
    phs_r           : std_logic;
    prev_phs_r      : std_logic;
    phs_f           : std_logic;
    prev_phs_f      : std_logic;
    sync_init       : std_logic;
    sync_r          : std_logic;
    prev_sync_r     : std_logic;
    sync_f          : std_logic;
    prev_in_dat_hi  : std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_hi       : std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_lo       : std_logic_vector(g_in_dat_w - 1 downto 0);
    phs_r_evt       : std_logic;
    phs_f_evt       : std_logic;
    phs_evt         : std_logic;
    -- 2 sample adjust
    phs_even        : boolean;
    d_raw_phs       : std_logic_vector(c_dd_factor - 1 downto 0);
    d_phs           : std_logic_vector(c_dd_factor - 1 downto 0);
    prev_d_raw_phs  : std_logic_vector(c_dd_factor - 1 downto 0);
    prev_d_phs      : std_logic_vector(c_dd_factor - 1 downto 0);
    d_sync          : std_logic_vector(c_dd_factor - 1 downto 0);
    prev_d_sync     : std_logic_vector(c_dd_factor - 1 downto 0);
    d_dat           : std_logic_vector(c_dd_factor * g_in_dat_w - 1 downto 0);
    prev_d_dat      : std_logic_vector(c_dd_factor * g_in_dat_w - 1 downto 0);
    -- 4 sample output
    dd_raw_phs      : std_logic_vector(c_dd_phs_w - 1 downto 0);
    prev_dd_raw_phs : std_logic_vector(c_dd_phs_w - 1 downto 0);
    dd_raw_phs_evt  : std_logic;
    dd_raw_phs_err  : std_logic;
    dd_phs          : std_logic_vector(c_dd_phs_w - 1 downto 0);
    dd_phs_err      : std_logic;
    dd_phs_detected : std_logic;
    dd_phs_locked   : std_logic;
    dd_sync         : std_logic_vector(c_dd_phs_w - 1 downto 0);
    dd_dat          : std_logic_vector(c_dd_dat_w - 1 downto 0);
    dd_val          : std_logic;
  end record;

  signal r                  : t_in_reg;
  signal nxt_r              : t_in_reg;

  signal dd_phs_detected_ok : std_logic;
begin
  ------------------------------------------------------------------------------
  -- Detect the dp_phs_clk reference
  ------------------------------------------------------------------------------

  gen_detectors : for I in g_nof_dp_phs_clk - 1 downto 0 generate
    u_common_clock_phase_detector_r: entity common_lib.common_clock_phase_detector
    generic map (
      g_rising_edge      => true,
      g_phase_rst_level  => '1',
      g_meta_delay_len   => c_delay_len,
      g_offset_delay_len => -c_offset_delay_len,
      g_clk_factor       => c_rx_factor
    )
    port map (
      in_clk    => dp_phs_clk_vec(I),  -- used as data input for in_clk domain
      rst       => in_rst,
      clk       => in_clk,
      phase     => ref_r_vec(I),
      phase_det => open
    );

    u_common_clock_phase_detector_f: entity common_lib.common_clock_phase_detector
    generic map (
      g_rising_edge      => false,
      g_phase_rst_level  => '1',
      g_meta_delay_len   => c_delay_len,
      g_offset_delay_len => -c_offset_delay_len,
      g_clk_factor       => c_rx_factor
    )
    port map (
      in_clk    => dp_phs_clk_vec(I),  -- used as data input for in_clk domain
      rst       => in_rst,
      clk       => in_clk,
      phase     => ref_f_vec(I),
      phase_det => open
    );
  end generate;

  -- No need to transfer dp_phs_clk_select to in_clk domain because it remains stable after every in_rst
  sel_r <= ref_r_vec(dp_phs_clk_select);
  sel_f <= ref_f_vec(dp_phs_clk_select);

  u_pipeline_ref_r : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline    => c_ref_pipeline,
    g_reset_value => 1
  )
  port map (
    rst     => in_rst,
    clk     => in_clk,
    in_dat  => sel_r,
    out_dat => ref_align_r
  );

  u_pipeline_ref_f : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline    => c_ref_pipeline,
    g_reset_value => 1
  )
  port map (
    rst     => in_rst,
    clk     => in_clk,
    in_dat  => sel_f,
    out_dat => ref_align_f
  );

  ------------------------------------------------------------------------------
  -- Enable word phase alignment to sel_r and sel_f or maintain current alignment
  ------------------------------------------------------------------------------
  -- The current alignment is fixed by no longer allowing sel_r and sel_f to adjust the ref_r and ref_f toggling

  ref_align_en <= not in_maintain_phs;

  u_common_toggle_align_ref_r : entity common_lib.common_toggle_align
  generic map (
    g_pipeline           => c_align_pipeline,
    g_reset_value        => 0,
    g_nof_clk_per_period => c_in_phs_clk_period
  )
  port map (
    rst         => in_rst,
    clk         => in_clk,
    in_align    => ref_align_en,
    in_toggle   => ref_align_r,
    out_toggle  => ref_r
  );

  u_common_toggle_align_ref_f : entity common_lib.common_toggle_align
  generic map (
    g_pipeline           => c_align_pipeline,
    g_reset_value        => 0,
    g_nof_clk_per_period => c_in_phs_clk_period
  )
  port map (
    rst         => in_rst,
    clk         => in_clk,
    in_align    => ref_align_en,
    in_toggle   => ref_align_f,
    out_toggle  => ref_f
  );

  ------------------------------------------------------------------------------
  -- Detect the word phase
  ------------------------------------------------------------------------------

  nxt_r.ref_r <= ref_r;
  nxt_r.ref_f <= ref_f;

  -- derive toggling phs_r and phs_f in phase with '0' to '1' transition in ref_r and ref_f to support dp_phs_clk that runs some factors 2 slower than dp_clk
  nxt_r.ref_init <= '0' when r.ref_r = '0' and r.ref_f = '0' else r.ref_init;  -- wait for dp_phs_clk low after in_rst release

  nxt_r.ref_r_val <= '1' when r.ref_init = '0' and ref_r = '1' else r.ref_r_val;  -- wait for dp_phs_clk high
  nxt_r.ref_f_val <= '1' when r.ref_init = '0' and ref_f = '1' else r.ref_f_val;  -- wait for dp_phs_clk high

  nxt_r.phs_r <= '0' when r.ref_r_val = '0' or (ref_r = '1' and r.ref_r = '0') else not r.phs_r;
  nxt_r.phs_f <= '0' when r.ref_f_val = '0' or (ref_f = '1' and r.ref_f = '0') else not r.phs_f;

  -- use ref_r and ref_f via phs to adjust the sample phase within a word of wb_factor samples and via sync to measure the word phase
  sync_r <= r.ref_r;  -- use registered r.ref_r and r.ref_f instead of ref_r and ref_f direct to have dd_sync = 0xF instead of 0xC, 0x3
  sync_f <= r.ref_f;

  nxt_r.sync_init <= '0' when sync_r = '0' and sync_f = '0' else r.sync_init;  -- wait for dp_phs_clk low after in_rst release

  nxt_r.sync_r <= '0' when r.sync_init = '1' else sync_r;
  nxt_r.sync_f <= '0' when r.sync_init = '1' else sync_f;

  ------------------------------------------------------------------------------
  -- Adjust single sample phase offset
  ------------------------------------------------------------------------------

  -- Register input to keep previous single input sample
  nxt_r.prev_phs_r     <= r.phs_r;
  nxt_r.prev_phs_f     <= r.phs_f;
  nxt_r.prev_sync_r    <= r.sync_r;
  nxt_r.prev_in_dat_hi <= r.in_dat_hi;

  -- Register in_dat_hi and in_dat_lo to ease timing closure
  nxt_r.in_dat_hi      <= in_dat_hi;
  nxt_r.in_dat_lo      <= in_dat_lo;

  -- Detect single sample phase shift event
  nxt_r.phs_r_evt <= '1' when r.phs_r = r.prev_phs_r else '0';
  nxt_r.phs_f_evt <= '1' when r.phs_f = r.prev_phs_f else '0';

  nxt_r.phs_evt <= r.phs_r_evt or r.phs_f_evt;

  -- Show single sample phase, which it TRUE when the sample phase in a word is 0 or 2 and FALSE for sample phase is 1 or 3
  nxt_r.phs_even <= true when r.phs_r = r.phs_f else false;

  -- Realign to halfword
  --                 phs_even = TRUE:                                    phs_even = FALSE:
  nxt_r.d_phs     <= r.phs_r     & r.phs_f     when r.phs_r = r.phs_f else r.phs_f     & r.prev_phs_r;
  nxt_r.d_sync    <= r.sync_r    & r.sync_f    when r.phs_r = r.phs_f else r.sync_f    & r.prev_sync_r;
  nxt_r.d_dat     <= r.in_dat_hi & r.in_dat_lo when r.phs_r = r.phs_f else r.in_dat_lo & r.prev_in_dat_hi;
  nxt_r.d_raw_phs <= r.phs_r     & r.phs_f;  -- preserve captured d phase for monitoring purposes

  ------------------------------------------------------------------------------
  -- Adjust double sample phase offset
  ------------------------------------------------------------------------------

  -- Register input to keep previous double input sample
  nxt_r.prev_d_phs     <= r.d_phs;
  nxt_r.prev_d_sync    <= r.d_sync;
  nxt_r.prev_d_dat     <= r.d_dat;
  nxt_r.prev_d_raw_phs <= r.d_raw_phs;

  -- Realign to word
  --                                                    phs_even = TRUE:                 phs_even = FALSE:
  nxt_r.dd_phs     <= r.d_phs     & r.prev_d_phs  when (r.phs_r = '1' and r.phs_f = '1') or (r.phs_r = '0' and r.phs_f = '1') else r.dd_phs;
  nxt_r.dd_sync    <= r.d_sync    & r.prev_d_sync when (r.phs_r = '1' and r.phs_f = '1') or (r.phs_r = '0' and r.phs_f = '1') else r.dd_sync;
  nxt_r.dd_dat     <= r.d_dat     & r.prev_d_dat  when (r.phs_r = '1' and r.phs_f = '1') or (r.phs_r = '0' and r.phs_f = '1') else r.dd_dat;
  nxt_r.dd_val     <=                         '1' when (r.phs_r = '1' and r.phs_f = '1') or (r.phs_r = '0' and r.phs_f = '1') else '0';
  nxt_r.dd_raw_phs <= r.d_raw_phs & r.prev_d_raw_phs;  -- preserve captured dd phase for monitoring purposes

  nxt_r.prev_dd_raw_phs <= r.dd_raw_phs;

  nxt_r.dd_raw_phs_evt <= '1' when r.prev_dd_raw_phs /= not r.dd_raw_phs else '0';
  nxt_r.dd_raw_phs_err <= '1' when TO_UINT(r.dd_raw_phs) /= c_exp_raw_phs_arr(0) and
                                   TO_UINT(r.dd_raw_phs) /= c_exp_raw_phs_arr(1) and
                                   TO_UINT(r.dd_raw_phs) /= c_exp_raw_phs_arr(2) and
                                   TO_UINT(r.dd_raw_phs) /= c_exp_raw_phs_arr(3) else '0';

  nxt_r.dd_phs_err <= '1' when TO_UINT(r.dd_phs) /= c_exp_phs else '0';

  ------------------------------------------------------------------------------
  -- Monitor the word phase alignment
  ------------------------------------------------------------------------------

  -- Define detected, locked when the dp_phs = c_exp_phs, so not only that dp_phs does not change but also that it has the expected value.
  -- . Note: a single sample phase shift due to missing a sample (when g_clk_drift=-2 ps in tb) is not noticed in realigned dd_phs. Therefore also check phs_evt and raw_phs.
  nxt_r.dd_phs_detected <= '1' when r.phs_evt = '0' and r.dd_raw_phs_evt = '0' and r.dd_phs_err = '0' and r.dd_raw_phs_err = '0' else '0';

  -- Use g_delayed_lo<g_dd_phs_locked_w-1 to waist less within the dp_phs_timeout interval in lvdsh_dd_phs4_align.vhd, such that
  -- within dp_phs_timeout it is just possible to achieve dd_phs_detected_ok.
  -- However in simulation with tb_lvdsh_dd_phs4_align.vhd when g_dclk_drift = +2ps then best use g_delayed_lo=g_dd_phs_locked_w-1.
  -- For g_dclk_drift = 0 or -2ps it is also in simulation fine to use the similar g_delayed_lo value as in HW.
  u_common_stable_delayed : entity common_lib.common_stable_delayed
  generic map (
    g_active_level  => '1',
    g_delayed_w     => g_dd_phs_locked_w,
    g_delayed_lo    => g_dd_phs_locked_w - 3  -- must be <= g_dd_phs_locked_w-1
  )
  port map (
    rst       => in_rst,
    clk       => in_clk,
    -- MM
    r_in      => r.dd_phs_detected,
    r_stable  => dd_phs_detected_ok
  );

  -- Declare dd_phs_locked when dd_sync=0 to ensure the dd_sync will be detected when it is becoming 1 again.
  nxt_r.dd_phs_locked <= '0' when dd_phs_detected_ok = '0' else
                         '1' when dd_phs_detected_ok = '1' and r.dd_sync = "0000" else r.dd_phs_locked;

  ------------------------------------------------------------------------------
  -- Output
  ------------------------------------------------------------------------------

  raw_phs          <= r.dd_raw_phs;
  out_phs_locked   <= r.dd_phs_locked;
  out_sync         <= r.dd_sync;
  out_dat          <= r.dd_dat;
  out_val          <= r.dd_val;

  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------

  p_reg : process(in_rst, in_clk)
  begin
    if in_rst = '1' then
      r.ref_init         <= '1';
      r.ref_r_val        <= '0';
      r.ref_f_val        <= '0';
      r.ref_r            <= '1';
      r.ref_f            <= '1';
      r.phs_r_evt        <= '0';
      r.phs_f_evt        <= '0';
      r.phs_evt          <= '0';
      r.phs_r            <= '0';
      r.prev_phs_r       <= '0';
      r.phs_f            <= '0';
      r.prev_phs_f       <= '0';
      r.sync_init        <= '1';
      r.sync_r           <= '1';
      r.prev_sync_r      <= '1';
      r.sync_f           <= '1';
      r.d_raw_phs        <= "00";
      r.d_phs            <= "00";
      r.prev_d_raw_phs   <= "00";
      r.prev_d_phs       <= "00";
      r.d_sync           <= "00";
      r.prev_d_sync      <= "00";
      r.dd_raw_phs       <= "0000";
      r.prev_dd_raw_phs  <= "0000";
      r.dd_raw_phs_evt   <= '0';
      r.dd_raw_phs_err   <= '1';
      r.dd_phs           <= "0000";
      r.dd_phs_err       <= '1';
      r.dd_phs_detected  <= '0';
      r.dd_phs_locked    <= '0';
      r.dd_sync          <= "0000";
      r.dd_val           <= '0';
    elsif rising_edge(in_clk) then
      r <= nxt_r;
    end if;
  end process;
end str;
