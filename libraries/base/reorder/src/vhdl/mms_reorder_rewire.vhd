-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Rewire an array of streaming interfaces based.
--
-- Description: The order of the streaming interfaces at the input can be changed towards
--              the output. Streams can be copied or moved. There are two modes to the
--              selection: Based on a selection table or based on the settings of the
--              selection register.
--
--              SELECTION TABLE (g_use_sel_table = TRUE)
--              The inputs are connected to the outputs based on a the values in a selected
--              column of the selection table (g_select_table). The column is selected by
--              the sel_in signal. If sel_in = 3 the third column from the selection table
--              will be used to determine the output. A set of predefined connection schemes
--              can be defined in the selection table.
--
--              SEL_IN (g_use_sel_table = FALSE)
--              The outputs are connected to the inputs based on the settings of the sel_in
--              register. Each output is represented by g_sel_in_w bits in the sel_in register.
--              For instance: an 8-input system with the following sel_in register content
--              will result in a reversed order of the input streams.
--
--              Output nr:  7   6   5   4   3   2   1   0
--              Register:  000 001 010 011 100 101 110 111
--
-- Remarks:
--
--
-- Some more remarks:

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.reorder_pkg.all;

entity mms_reorder_rewire is
  generic (
    g_select_table  : t_reorder_table := c_reorder_table;
    g_nof_streams   : natural     := 8;
    g_sel_in_w      : natural     := 3;
    g_use_sel_table : boolean     := true
  );
  port (
    -- Memory-mapped clock domain
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;

    -- Streaming clock domain
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;

    -- ST sinks
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_reorder_rewire;

architecture str of mms_reorder_rewire is
  signal sel_reg : std_logic_vector(sel_a_b(g_use_sel_table, g_sel_in_w, g_sel_in_w * g_nof_streams) - 1 downto 0) := (others => '0');
begin
  u_register : entity work.reorder_rewire_reg
  generic map(
    g_nof_streams        => g_nof_streams,
    g_sel_in_w           => g_sel_in_w,
    g_cross_clock_domain => true
  )
  port map(
    -- Clocks and reset
    mm_rst    => mm_rst,
    mm_clk    => mm_clk,
    st_rst    => dp_rst,
    st_clk    => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in    => reg_mosi,
    sla_out   => reg_miso,

    -- MM registers in st_clk domain
    sel_reg   => sel_reg
  );

  u_rewire: entity work.reorder_rewire
  generic map(
    g_select_table  => g_select_table,
    g_nof_streams   => g_nof_streams,
    g_sel_in_w      => g_sel_in_w,
    g_use_sel_table => g_use_sel_table
  )
  port map(
    -- System
    sel_in          => sel_reg,

    -- Inputs from transceiver
    input_sosi_arr  => snk_in_arr,
    input_siso_arr  => snk_out_arr,

    -- Outputs to processing
    output_sosi_arr => src_out_arr,
    output_siso_arr => src_in_arr
  );
end str;
