--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- . Lets common_shiftram shift counter data with all possible shift values
-- Description:
-- . The DUT output counter stream is verified
-- . Correct shift and counter values at the output with respect to the input
--   counter values are verified
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_common_shiftram is
  generic (
    g_nof_words : natural := 16;
    g_data_w    : natural := 32
  );
end tb_common_shiftram;

architecture tb of tb_common_shiftram is
  constant clk_period     : time := 10 ns;
  constant c_shift_w      : natural := ceil_log2(g_nof_words);

  constant c_data_io_delay : natural := 3;  -- data_in takes 3 cycles to emerge as data_out

  signal rst              : std_logic;
  signal clk              : std_logic := '1';
  signal tb_end           : std_logic := '0';

  signal gen_data_en      : std_logic := '1';
  signal gen_data_rdy     : std_logic := '1';

  signal data_in        : std_logic_vector(g_data_w - 1 downto 0);
  signal data_in_val    : std_logic;
  signal data_in_shift  : std_logic_vector(c_shift_w - 1 downto 0);
  signal data_out       : std_logic_vector(g_data_w - 1 downto 0);
  signal data_out_val   : std_logic;
  signal data_out_shift : std_logic_vector(c_shift_w - 1 downto 0) := (others => '0');
  signal prv_data_out_shift : std_logic_vector(c_shift_w - 1 downto 0) := (others => '0');

  signal prev_data_out   : std_logic_vector(g_data_w - 1 downto 0);
  signal ver_data_start  : std_logic;
  signal ver_data_toggle : std_logic;
  signal ver_data_en     : std_logic;
  signal ver_data_rdy    : std_logic := '1';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 3;

  -- Generate data_in
  proc_common_gen_data(1, 0, rst, clk, gen_data_en, gen_data_rdy, data_in, data_in_val);

  -- Stimuli: shift range 0->max->0
  p_shift: process
  begin
    data_in_shift <= (others => '0');
    wait until data_out_val = '1';

    for i in 0 to g_nof_words - 2 loop
      if rising_edge(clk) then
        data_in_shift <= TO_UVEC(i, c_shift_w);
      end if;
      while TO_UINT(data_out_shift) /= i loop
        wait until clk = '1';
      end loop;
      wait for 100 ns;
    end loop;

    for i in g_nof_words - 3 downto 0 loop
      if rising_edge(clk) then
        data_in_shift <= TO_UVEC(i, c_shift_w);
      end if;
      while TO_UINT(data_out_shift) /= i loop
        wait until clk = '1';
      end loop;
      wait for 100 ns;
    end loop;

    tb_end <= '1';
    wait;
  end process;

  -- DUT
  u_common_shiftram : entity work.common_shiftram
  generic map (
    g_data_w    => g_data_w,
    g_nof_words => g_nof_words
  )
  port map (
    rst            => rst,
    clk            => clk,

    data_in        => data_in,
    data_in_val    => data_in_val,
    data_in_shift  => data_in_shift,

    data_out       => data_out,
    data_out_val   => data_out_val,
    data_out_shift => data_out_shift
  );

  -- Make sure prev_data_out has been assigned
  p_verify_start: process
  begin
    wait until data_out_val = '1';
    proc_common_wait_some_cycles(clk, 1);
    ver_data_start <= '1';
    wait;
  end process;

  -- Disable verification during shifts
  prv_data_out_shift <= data_out_shift when rising_edge(clk);

  p_verify_toggle: process(data_out, data_out_val, data_out_shift)
  begin
    ver_data_toggle <= '1';
    if not (data_out_shift = prv_data_out_shift) then
      ver_data_toggle <= '0';
    end if;
  end process;

  -- Verify data_out
  ver_data_en <= ver_data_start and ver_data_toggle;
  proc_common_verify_data(1, clk, ver_data_en, ver_data_rdy, data_out_val, data_out, prev_data_out);

  -- Verify the relationship between input data and output shift/data
  p_verify_shift: process(clk)
  begin
    if rising_edge(clk) then
      if data_out_val = '1' then
        if TO_UINT(data_in) /= TO_UINT(data_out) + c_data_io_delay + TO_UINT(data_out_shift) then
          report "Wrong output data/shift with respect to input data"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;
end tb;
