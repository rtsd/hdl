-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity tst_input is
  generic (
    g_file_name   : string;
    g_file_repeat : natural := 1;
    g_nof_data    : natural := 1;
    g_data_width  : natural;
    g_data_type   : string := "SIGNED"
  );
  port (
    clk      : in  std_logic;
    rst      : in  std_logic;
    en       : in  std_logic := '1';
    sync     : in  std_logic := '1';
    out_dat  : out std_logic_vector(g_nof_data * g_data_width - 1 downto 0);
    out_val  : out std_logic;
    out_sync : out std_logic;
    out_msg  : out string(1 to 80);
    out_lno  : out natural;
    out_rep  : out natural;
    out_eof  : out std_logic;
    out_wait : out std_logic
  );
begin
  assert g_data_type = "SIGNED" or g_data_type = "UNSIGNED"
    report "Unknown data type."
    severity ERROR;
end tst_input;

architecture beh of tst_input is
  constant c_prag         : character := '%';
  constant c_prag_sync    : string := c_prag & "SYNC";
  constant c_prag_gap     : string := c_prag & "GAP ";
  constant c_prag_msg     : string := c_prag & "MSG ";
  constant c_prag_next    : string := c_prag & "NEXT";

  type STATE_TYPE is
    ( s_init, s_idle, s_next, s_read, s_enable, s_data, s_pragma, s_msg, s_sync, s_gap, s_eof);

  signal state           : STATE_TYPE;
  signal nxt_state       : STATE_TYPE;

  signal rd_dat      : std_logic_vector(out_dat'range);
  signal nxt_rd_dat  : std_logic_vector(out_dat'range);
  signal rd_val      : std_logic;
  signal nxt_rd_val  : std_logic;

  signal eof     : std_logic;
  signal nxt_eof : std_logic;
  signal lno     : natural;
  signal nxt_lno : natural;
  signal rep     : natural;
  signal nxt_rep : natural;
  signal msg     : string(out_msg'range);
  signal nxt_msg : string(msg'range);
  signal gap_count     : natural;
  signal nxt_gap_count : natural;
  signal nxt_wait    : std_logic;
  signal nxt_sync : std_logic;
begin
  out_dat <= rd_dat;
  out_val <= rd_val;
  out_eof <= eof;
  out_lno <= lno;
  out_rep <= rep;
  out_msg <= msg;

  regs: process(rst, clk)
  begin
    if rst = '1' then
      state     <= s_init;
      rd_dat    <= (others => '0');
      rd_val    <= '0';
      eof       <= '0';
      lno       <= 0;
      rep       <= 1;
      msg       <= (others => ' ');
      gap_count <= 0;
      out_wait  <= '0';
      out_sync  <= '0';
    elsif rising_edge(clk) then
      state     <= nxt_state;
      rd_dat    <= nxt_rd_dat;
      rd_val    <= nxt_rd_val;
      eof       <= nxt_eof;
      lno       <= nxt_lno;
      rep       <= nxt_rep;
      msg       <= nxt_msg;
      gap_count <= nxt_gap_count;
      out_wait  <= nxt_wait;
      out_sync  <= nxt_sync;
    end if;
  end process;

  process (rst, state, eof, sync, en, gap_count, msg, lno, rep)

    variable  file_status  : FILE_OPEN_STATUS;
    file      in_file : TEXT;
    variable  in_line : LINE;
    variable  num     : integer;
    variable  cycle_lno : integer;  -- file line number
    variable  cycle_rep : natural;  -- read file g_file_repeat times
    variable  cycle_state     : STATE_TYPE;
    variable  nxt_cycle_state : STATE_TYPE;

    procedure show_msg(sev : in severity_level; msg : in string) is
    begin
      report msg & character'val(13)
             & "    Repeat: " & integer'image(cycle_rep)
             & "    Line: " & integer'image(cycle_lno)
             & "    File: " & g_file_name
        severity sev;
    end show_msg;
  begin
    nxt_cycle_state := state;
    cycle_lno := lno;
    cycle_rep := rep;

    nxt_rd_dat <= (others => '0');
    nxt_rd_val <= '0';
    nxt_eof <= '0';
    nxt_msg <= msg;
    nxt_lno <= lno;
    nxt_rep <= rep;
    nxt_gap_count <= 0;
    nxt_wait <= '0';
    nxt_sync <= '0';

    -- We use a loop to enable mutiple state transitions
    -- within a clock cycle.
    -- To break out of the loop,  'EXIT cycle' is used. This will
    -- bring the process to the next clock cycle.

    cycle: loop
      -- cycle_state signal is used to hold the current state
      cycle_state := nxt_cycle_state;

      case cycle_state is
        when s_init =>
          if rst = '0' then
            if file_status = OPEN_OK then
              file_close(in_file);
            end if;
            file_open (file_status, in_file, g_file_name, READ_MODE);
            if file_status = OPEN_OK then
              nxt_cycle_state := s_next;
              show_msg(NOTE, "file opened for reading");
            else
              nxt_cycle_state := s_idle;
              show_msg(WARNING, "unable to open file");
              exit;
            end if;
          else
            exit cycle;
          end if;

        when s_idle =>
          if cycle_rep < g_file_repeat then
            cycle_lno := 0;
            cycle_rep := cycle_rep + 1;
            nxt_cycle_state := s_init;
          else
            -- do nothing
            nxt_rd_dat <= (others  => '0');
            nxt_rd_val <= '0';
            nxt_eof <= eof;
            exit cycle;
          end if;

        when s_next =>
          -- check if end of file is reached
          if ENDFILE(in_file) then
            show_msg(NOTE, "end of file");
            nxt_eof   <= '1';
            nxt_cycle_state := s_idle;
          else
            nxt_cycle_state := s_read;
          end if;

        when s_read =>
          -- read a line from the file
          READLINE(in_file, in_line);
          cycle_lno := cycle_lno + 1;
          if in_line'length >= 1 and in_line(1) = c_prag then
            nxt_cycle_state := s_pragma;
          else
            nxt_cycle_state := s_enable;
          end if;

        when s_enable =>
          -- wait for enable to become high
          if en = '1' then
            nxt_cycle_state := s_data;
          else
            exit cycle;
          end if;

        when s_data =>
          -- output data
          for i in 0 to g_nof_data - 1 loop
            READ(in_line, num);
            if g_data_type = "UNSIGNED" then
              nxt_rd_dat((i + 1) * g_data_width - 1 downto i * g_data_width) <= std_logic_vector(to_unsigned(num, g_data_width));
            else
              nxt_rd_dat((i + 1) * g_data_width - 1 downto i * g_data_width) <= std_logic_vector(to_signed(num, g_data_width));
            end if;
          end loop;
          nxt_rd_val  <= '1';
          nxt_cycle_state := s_next;
          exit cycle;

        when s_pragma =>
          -- handle pragmas
          case in_line(1 to 5) is
            when c_prag_msg  => nxt_cycle_state := s_msg;
            when c_prag_gap  => nxt_cycle_state := s_gap;
            when c_prag_sync => nxt_cycle_state := s_sync;
            when c_prag_next => nxt_cycle_state := s_sync;
            when others =>
              show_msg(ERROR, "unknown pragma: " & in_line(1 to in_line'length));
              nxt_cycle_state := s_next;
          end case;

        when s_msg =>
          -- handle msg pragma.
          -- This displays a notice to the user, and
          nxt_msg <= (others => ' ');
          nxt_msg(1 to in_line'length - c_prag_gap'length)
            <= in_line(c_prag_gap'length + 1 to in_line'length);
          show_msg(NOTE, in_line(c_prag_gap'length + 1 to in_line'length));
          nxt_cycle_state := s_next;

        when s_sync =>
          -- handle sync pragma. This waits sync to become high
          if sync = '1' then
            nxt_cycle_state := s_next;
            nxt_sync <= '1';
          else
            nxt_wait <= '1';
            nxt_cycle_state := s_sync;
            exit cycle;
          end if;

        when s_gap =>
          -- handle gap pragma. This creates a gap.
          if gap_count = 0 then
            -- initialize the counter
            nxt_gap_count <=
              integer'value(in_line(c_prag_gap'length + 1 to in_line'length));
            -- create the first gap cycle.
            exit cycle;
          elsif gap_count > 1 then
            -- decrease the counter
            nxt_gap_count <= gap_count - 1;
            -- create another gap cycle.
            exit cycle;
          else
            -- end the gap
            nxt_cycle_state := s_next;
          end if;

        when others =>
          -- unkown state.
          report "unknown state"
            severity ERROR;
          nxt_cycle_state := s_idle;
      end case;
    end loop;

    nxt_state <= nxt_cycle_state;
    nxt_lno   <= cycle_lno;
    nxt_rep   <= cycle_rep;
  end process;
end beh;
