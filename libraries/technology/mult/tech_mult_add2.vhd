-- -----------------------------------------------------------------------------
--
-- Copyright 2009-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tech_mult_component_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_mult_lib;
library ip_arria10_e1sg_mult_add2_lib;
library ip_arria10_e2sg_mult_add2_lib;
library ip_agi027_1e1v_mult_add2_lib;

entity tech_mult_add2 is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string := "IP";
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
end tech_mult_add2;

architecture str of tech_mult_add2 is
begin
  gen_ip_stratixiv_rtl : if (g_technology = c_tech_stratixiv and g_variant = "RTL") generate
    u0 : ip_stratixiv_mult_add2_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_res_w            => g_res_w,
      g_force_dsp        => g_force_dsp,
      g_add_sub          => g_add_sub,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map(
      rst   => rst,
      clk   => clk,
      clken => clken,
      in_a  => in_a,
      in_b  => in_b,
      res   => res
    );
  end generate;

  gen_ip_arria10_e1sg_rtl : if (g_technology = c_tech_arria10_e1sg and g_variant = "RTL") generate
    u0 : ip_arria10_e1sg_mult_add2_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_res_w            => g_res_w,
      g_force_dsp        => g_force_dsp,
      g_add_sub          => g_add_sub,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map(
      rst   => rst,
      clk   => clk,
      clken => clken,
      in_a  => in_a,
      in_b  => in_b,
      res   => res
    );
  end generate;

  gen_ip_arria10_e2sg_rtl : if (g_technology = c_tech_arria10_e2sg and g_variant = "RTL") generate
    u0 : ip_arria10_e2sg_mult_add2_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_res_w            => g_res_w,
      g_force_dsp        => g_force_dsp,
      g_add_sub          => g_add_sub,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map(
      rst   => rst,
      clk   => clk,
      clken => clken,
      in_a  => in_a,
      in_b  => in_b,
      res   => res
    );
  end generate;

  gen_ip_agi027_1e1v_rtl : if (g_technology = c_tech_agi027_1e1v and g_variant = "RTL") generate
    u0 : ip_agi027_1e1v_mult_add2_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_res_w            => g_res_w,
      g_force_dsp        => g_force_dsp,
      g_add_sub          => g_add_sub,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_adder   => g_pipeline_adder,
      g_pipeline_output  => g_pipeline_output
    )
    port map(
      rst   => rst,
      clk   => clk,
      clken => clken,
      in_a  => in_a,
      in_b  => in_b,
      res   => res
    );
  end generate;
end str;
