###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Definition of clock frequencies 

set_time_format -unit ns -decimal_places 3

create_clock -name "dp_clk" -period 5.000ns [get_ports {apertif_unb1_fn_beamformer:u_revision|ctrl_unb1_board:u_ctrl|dp_clk}]

create_clock -name {CLK} -period 5.000 -waveform { 0.000 2.500 } [get_ports {CLK}]
create_clock -name {ETH_CLK} -period 40.000 -waveform { 0.000 20.000 } [get_ports {ETH_clk}]
create_clock -name {SB_CLK} -period 6.400 -waveform { 0.000 3.200 } [get_ports {SB_CLK}]
create_clock -name {SA_CLK} -period 6.400 -waveform { 0.000 3.200 } [get_ports {SA_CLK}]
create_clock -name {ADC_BI_A_CLK} -period 2.500 -waveform { 0.000 1.250 } [get_ports {ADC_BI_A_CLK}]
create_clock -name {ADC_BI_D_CLK} -period 2.500 -waveform { 0.000 1.250 } [get_ports {ADC_BI_D_CLK}]

