Title : Consolidation of the rTwoSDF.

Contents:

I)   Introduction
II)  Summary
III) Details
  1)   Obtain golden reference test data
  2)   Verifing against the golden reference data
  3)   Cosmetic changes
  4)   Digital changes
  5)   Test bench changes
  6)   Synthesis of rTwoSDF
  7)   MATLAB reference



I) Introduction

The original rTwoSDF VHDL and MATLAB code were developed and reported in
ASTRON-RP-755 by Raj Rajan Thilak.

This readme.txt describes the subsequent steps that were done by Eric Kooistra
to prepare this rTwoSDF for developing the wideband extension of the rTwoSDF
and the integration into UniBoard firmware design (especially for APERTIF).


II) Summary

The key improvements made to the rTwoSDF concern:

a) Create a set of golden reference input stimuli and output result files to
   be able to make the subsequent changes without introducing errors. Having
   golden reference output file avoids the need to use MATLAB for verification
   of the output result. Apart from the uniNoise stimuli for 1024 point FFT it
   is useful to have some 4, 8, 16 point impulse golden reference files for
   faster simulation.
b) Cosmetic changes to follow the ASTRON VHDL coding style. Any irregularities
   that then remain in the code point at potential design flaws or errors.
c) Clearly seperate the algorithm z^(-1) latency from the digital RTL latency.
   The algorithm latency of the pipelined FFT = g_nof_points. The digital
   latency can be 0 but is typically a few clock cycles per stage to easy
   achieving timing closure with synthesis. The rTwoSDF can now accept in_val
   not being active at every clock cycle.
d) Be aware that a shift register for the algorithm pipelining depends on
   in_val whereas pipelining for the digital latency is a clock cycle delay
   independent of other enables.
e) Use common_complex_mult(stratix4) that uses a MegaWizard generated complex
   multiplier instead of inferring a multiplier from RTL code.
  

III) Details

1) Obtain golden reference test data

Updated reference test data to status that is documented in ASTRON-RP-755 v1.0:

a) tb/data/impulse_p1024_b16_in.txt  --> tb/data/impulse_p1024_b16_out.txt
                                         tb/data/impulse_p1024_b16_out.jpg
b) tb/data/test/in/uniNoise_p1024_b16_in.txt -->
                          tb/data/test/out/uniNoise_p1024_b16_tb16_out.txt
                          tb/data/test/out/uniNoise_p1024_b16_tb16_out.jpg

The *.out created by running tb_rTwoSDF.vhd for 500 us. The *.jpg is captured
using print-screen from the analysis figure of testFFT_output.m. The reported
SNR value in MATLAB is also edited into the *.jpg file. These *.out file are 
the golden results that can be used to verify that changes in the rTwoSDF code
do not affect the output (e.g. by using fc or diff).

The uniNoise_p1024_b16_in.txt is one of the range of input noise files that is
created by running testFFT_input.m.

The impulse_p1024_b16_in.txt is one of the range of input impulse files that is
created manually in an editor. The original impuls input file contained a chirp
sequence of with increasing impulses per blok and is preserved in
tb/data/impulse_chirp_p1024_b16_in.txt.



2) Verifing against the golden reference data

For uniform noise the testFFT_input.m creates a *_in.txt file with 8 input
frames, that can be valid or not valid according to a specified pattern
(seq= [H,L,H,L,L,H,H,L], so there are 4 valid frames). The testFFT_output.m 
determines the number of valid frames in the input data and does the further
analysis for that number of output frames.

For the impulse stimuli the *_in.txt file is created manualy and it also has
8 frames that are all valid.

The tb_rTwoSDF can simulate the input stimuli from the *_in.txt file one or 
more time as specified by c_repeat in the test bench. For the evaluation by
testFFT_output.m the c_repeat needs to be set >= 2. The tb_rTwoSDF.vhd has been
modified such that it can use 'run -all' and it stops automatically when the
clk stops.

To verify the uniform noise stimuli do:

  -- uncomment the uniNoise files in tb_rTwoSDF.vhd
  vsim> vsim -novopt work.tb_rtwosdf
  vsim> run -all --> this creates the *_out.txt file
  vsim> quit -sim --> to close the file

  matlab> set NOISE = 1 in testFFT_output.m
  matlab> testFFT_output --> this creates the figure as in the *_out.jpg
  
  Then use WinMerge or other diff (viewer) program to verify the *_out.txt
  against the golden result that is kept in SVN.
  With SVN Tortoise the new *_out.txt also shows as modified even when it did
  not change, only after doing 'quit -sim' it shows again as not modified,
  because then the Modelsim simulation has closed the file.

Similar for the impulse stimuli.

Default best use the uniform noise stimuli, because these trigger more (and
probably all) internal states of the FFT code. For graphical viewing the
impulse stimuli are more appropriate.



3) Cosmetic changes

a) Removed in_sync and out_sync from rTwoSDF
   The rTwoSDF code does not support the in_sync. Anyway the SOSI sync is
   better treated outside DSP functionality like an FFT, therefor in_sync and
   out_sync are removed from rTwoSDF.
b) Updated coding style to be conform How_to_write_VHDL.txt (e.g. naming, 
   indent, obsolete parts of code)
c) Changed test bench clock to 100 ns instead of 25 ns to more easily recognize
   the FFT size as nof clock cycles between two markers.
d) Use ADD_SVEC() and SUB_SVEC() functions from common_pkg.vhd in rTwoBF.vhd.


   
4) Digital changes

a) Remove reset out_dat in core/delay.vhd
b) Do not reset out_val in core/delay.vhd --> fmax = 305 MHz else 297 MHz
c) Set RTL latency via g_lat generic
d) Support g_lat=0, 1 in rTwoBF (g_lat=1 corresponds to original)
e) Move weight ROM reading outside rTwoWMul.vhd to rTwoWeight.vhd. This allows
   for more clear handling of the digital latency (= adding registers to 
   ease timing closure) and the pipeline of the pipelined FFT algorithm (=
   g_nof_points). The uniNoise_p1024_b16_in.txt now also gives the correct
   result when both c_bf_lat=0 and c_weight_lat=0.
f) Replace rtl_dsp by stratix4 architecture for common_complex_mult. The
   c_mul_lat now becomes 3 (was 2). The uniNoise_p1024_b16_in.txt now verifies
   OK (and back to using default c_bf_lat=1 and c_weight_lat=1).
g) Briefly back to c_mul_lat=2 because for c_mul_lat=3:
   . the (Re=impulse, Im=0) is OK
   . the (Re uniNoise, Im uniNoise) goes wrong.
h) Fixed issues with common_complex_mult(stratix4):
   - The maximum widths are 18b * 18b --> 36b, because for larger input widths
     the MegaWizard will use 8 or 16 instead of 4 real multipliers (see
     common_complex_mult_a_stratix4.vhd for more explanation).
   - The common_complex_mult architectures behave differently for when the
     most negative input values are input. This depends on whether the output
     is resized as unsigned or as signed. The default approach is now to use
     unsigned resize because this maps better on the FPGA multiplier DSP
     blocks (see common_top.vhd more explanation).
i) Make rTwoBF combinatorial, to ease separation of algorithm feedback FIFO
   shift register and digital pipelining to improve timing closure.
j) Put both Re and Im into one rTwoBFStage, so that they can share the feedback
   FIFO.
k) Removed g_stage from rTwoWMul.vhd entity, because it is not used.
l) Separate functional shift register delay line latency for digital clock
   cycle pipelining latency. For that changed implementation of delay.vhd and
   bitDelay.vhd. These are now intendend for shift delays that depend on
   in_val. For digital pipeline delay use the common_pipeline and
   common_pipeline_sl.
m) Use no reset in delay.vhd to allow synthesis to infer a RAM block or FIFO
   for it.
n) Do support rst and in_clr for bitDelay.vhd, to allow dynamic restart of
   filling the shift delay line. In practise the bitDelay.vhd may well get
   implemented as some statemachine by the synthesis tool instead of a true
   delay line, in case the output relates to the input by an offset.
o) Remove out_sel from rTwoWMult entity, because out_sel is not passed on
   between stages since each stages has its own ctrl_sel counter that gets
   enabled by the in_val. The in_val does get passed on between stages, and
   thanks to that all stages can initialize their ctrl_sel counter with 0.
p) Added g_stage_lat to register the output of each rTwoSDFStage.
q) Clarified product rounding (truncation) and stage output bit growth in
   rTwoWMult.vhd.
 

   
5) Testbench changes

a) Added shorter impulse_p* files for faster simulation. Manually added
   impulse_p#_16_in.txt with golden result files impulse_p#_16_out.txt for
   # =4, 8, 16 and with same valid blocks as in the uniNoise files, so
   "H L H L L H H L" (as is the default scheme in testFFT_input.m). 
b) Leave impulse_p1024_16_in.txt as it was with 8 active blocks, so
   "H H H H H H H H".
c) Introduce g_use_uniNoise_file to ease selecting between uniNoise input file
   generated by MATLAB or another file.
d) Introduce enable to decouple rst from start of stimuli and let enable go
   high somewhat after the rst release. This requires using in_val for the
   stage counter.
e) Only output valid data to the file. The testFFT_output.m only uses the 
   first 4 FFT output blocks for analysis.
f) Explained implications of using LRM 1076-1987 style file IO.
g) Support random in_val control via g_in_en.
h) The tb asserts an error when the output does not match the expected output
   that is read from the golden reference file. The output is also written
   to a default output file to support offline analysis. Originally the golden
   reference file was used as output file, so got overwritten and a diff with
   SVN would show whether it got modified.

   


6) Synthesis of rTwoSDF

a) The original rTwoSDF can be recovered via SVN update rev-4453 for the 
   directories core/, dsp/ and common/src. The fitter and timing results for
   this original are also kept in:
   . doc/r2sdf_rev4453.fit.rpt
   . doc/r2sdf_rev4453.sta.rpt
b) Summary tabel for synthesise using the rTwoSDF.vhd as top level:
                                  rev-4453     rev-4453
                                  resize()     resize_num()
                                  *1)          *2)
   * Timing                             
     . Fmax                       = 305 MHz    342 MHz
   * Fitter
     . ALUTs used                 = 1297       1290
     . Total registers            = 1544       1492
     . ALMs                       = 1169       1157
     . M9K blocks                 = 9          10
     . Total MLAB memory bits     = 1792       1540
     . DSP block 18 bit elements  = 36         40
 
   Notes:
   *1 The rev-4453 uses common_complex_mult()
   *2 Using resize_num instead of resize increases fmax but also requires 4
      more 18-bit multiplier elements
c) Better use the 'rtl' complex multiplier for stage 1, because the 'stratix4'
   IP instance does not get optimized away when the input is constant 1 and 0.
d) Use synth/quartus_top/rtwo_top.vhd with pipelining stages to the FPGA pins
   for synthesis, this makes a big difference.
e) Summary tabel for synthesise using the rtwo_top.vhd *1), *2) as top level:

                                  rev-4580
                                  stratix4
   * Timing                             
     . Fmax                       = 378 MHz
   * Fitter
     . ALUTs used                 = 1133
     . Total registers            = 2306
     . ALMs                       = 1422
     . M9K blocks                 = 11
     . DSP block 18 bit elements  = 36
        
     Notes:
     *1 The IO pipelining uses 0 ALUTs, 56 ALM and 88 FF these numbers have
        been substracted in the table
     *2 The MLAB bits are not reported, because they do not get used so much
        as when without the IO pipelining.
     
        
        
7)   MATLAB reference

a) The original rTwoSDF rev-4453 with generics set to:
     . g_use_reorder = false
     . g_in_dat_w    = 8
     . g_out_dat_w   = 13
     . g_nof_points  = 1024
   yields SNR = 78.5234 dB for uniNoise_p1024_b16_in.txt.
   The uniNoise_p1024_b16_tb16_out.txt is kept as golden reference in SVN and
   also saved as picture in uniNoise_p1024_b16_tb16_out.jpg.

