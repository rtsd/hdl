-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture double data rate FPGA input
-- Description:
--   Using the MegaWizard simply provides a GUI to set and select the generics
--   and IO. First used MegaWizard to learn how this works and what the default
--   values are:
--     %UNB%\Firmware\modules\common\src\ip\MegaWizard\ddio_in.vhd
--   Then directly used the component to define the instance. The component
--   comes from:
--     %QUARTUS_ROOTDIR%\..\quartus\eda\sim_lib\altera_mf_components.vhd
--
--   component altddio_in
--     generic (
--         width                  : positive; -- required parameter
--         invert_input_clocks    : string := "OFF";
--         intended_device_family : string := "Stratix";
--         power_up_high          : string := "OFF";
--         lpm_hint               : string := "UNUSED";
--         lpm_type               : string := "altddio_in" );
--     port (
--         datain    : in std_logic_vector(width-1 downto 0);
--         inclock   : in std_logic;
--         inclocken : in std_logic := '1';
--         aset      : in std_logic := '0';
--         aclr      : in std_logic := '0';
--         sset      : in std_logic := '0';
--         sclr      : in std_logic := '0';
--         dataout_h : out std_logic_vector(width-1 downto 0);
--         dataout_l : out std_logic_vector(width-1 downto 0) );
--   end component;

library IEEE;
use IEEE.std_logic_1164.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

entity ip_stratixiv_ddio_in is
  generic(
    g_device_family : string := "Stratix IV";
    g_width         : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
end ip_stratixiv_ddio_in;

architecture str of ip_stratixiv_ddio_in is
begin
  ddio: altddio_in
  generic map (
    intended_device_family => g_device_family,
    invert_input_clocks => "OFF",
    lpm_hint => "UNUSED",
    lpm_type => "altddio_in",
    power_up_high => "OFF",
    width => g_width
  )
  port map (
    datain      => in_dat,
    inclock     => in_clk,
    inclocken   => in_clk_en,
    aclr        => rst,
    dataout_h   => out_dat_hi,
    dataout_l   => out_dat_lo
  );
end str;
