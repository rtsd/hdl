#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from common import *
import matplotlib
matplotlib.use('TkAgg')
from common_dsp import *
import os
import numpy

# Purpose:
# Description:
# Usage:

# 0      1     2      3    4    5       6     7     8       9         10
# [sync] [bsn] [data] [re] [im] [valid] [sop] [eop] [empty] [channel] [err]
NOF_DP_RECORD_FIELDS = 11
DP_RECORD_INDEX_RE  = 3
DP_RECORD_INDEX_IM  = 4
DP_RECORD_INDEX_EOP = 7

REC_FILE = os.environ['RADIOHDL']+'/libraries/dsp/correlator/tb/rec/correlator_src_out_arr0.rec'

################################################################################
# Read the lines from the file and turn them into a list
################################################################################
with open (REC_FILE, "r") as recfile:
    lines = recfile.read().splitlines()

################################################################################
# DP stream has nof_visibilities per channel frame (from SOP to EOP)
# . so we can detect nof_visibilities by counting the number of valid words
#   (=lines written) 
################################################################################
for line_nr,line in enumerate(lines):
    split_line = line.split(' ')
    if len(split_line)==NOF_DP_RECORD_FIELDS:
        eop = split_line[DP_RECORD_INDEX_EOP]
        if eop == '1':
            nof_visibilities=line_nr+1
            break

################################################################################
# Extract the complex fields; create new complex list
################################################################################
complex_list = []
raw_data_int = []
raw_data_str = []

for line in lines:
    split_line = line.split(' ')

    if len(split_line)==NOF_DP_RECORD_FIELDS:
        # All record elements have been written to the line in the file
        # . Get the complex record fields
        str_re = split_line[DP_RECORD_INDEX_RE]
        str_im = split_line[DP_RECORD_INDEX_IM]
        raw_data_str.append( [str_re, str_im] )
        # . Convert string to integer
        re = int(str_re, 16)
        im = int(str_im, 16)
        raw_data_int.append( [re, im] )
        # Convert integers to complex    
        complex_word = complex(re,im)

    complex_list.append( complex_word )

################################################################################
# Extract the phases and amplitudes
################################################################################
phases = []
amplitudes = []
for word in complex_list:
    phasor = complex_binomial_to_phasor(word)
    ampl  = phasor[0]
    amplitudes.append(ampl)
    phase = phasor[1]
    phases.append(phase)

################################################################################
# Split the list into chunks of nof_visibilities, plot them channel after channel
################################################################################
phases_per_channel = split_list(phases, nof_visibilities)

for channel_phases in phases_per_channel:
    phases_mat = numpy.array(channel_phases)
    mat = unique_vis_to_full_matrix(phases_mat)  
    plot_matrix_color([[mat]])
    
