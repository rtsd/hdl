-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Core design for Lofar2 Filterbank stage
-- Description:
--   Unb2b version for lab testing
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, diag_lib, dp_lib, tech_jesd204b_lib, wpfb_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use work.lofar2_unb2c_filterbank_pkg.all;

entity lofar2_unb2c_filterbank is
  generic (
    g_design_name            : string  := "lofar2_unb2c_filterbank";
    g_design_note            : string  := "UNUSED";
    g_buf_nof_data           : natural := 1024;
    g_sim                    : boolean := false;  -- Overridden by TB
    g_sim_unb_nr             : natural := 0;
    g_sim_node_nr            : natural := 0;
    g_sim_model_ddr          : boolean := false;
    g_stamp_date             : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time             : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id            : string  := "";  -- revision ID     -- set by QSF
    g_factory_image          : boolean := false;
    g_protect_addr_range     : boolean := false;
    g_wpfb                   : t_wpfb := c_sdp_wpfb_subbands;
    g_scope_selected_subband : natural := 0
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic;  -- STD_LOGIC_VECTOR(c_unb2c_board_nof_eth-1 DOWNTO 0);
    ETH_SGOUT    : out   std_logic;  -- STD_LOGIC_VECTOR(c_unb2c_board_nof_eth-1 DOWNTO 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

     -- back transceivers (Note: numbered from 0)
    JESD204B_SERIAL_DATA       : in    std_logic_vector((c_unb2c_board_tr_jesd204b.bus_w * c_unb2c_board_tr_jesd204b.nof_bus) - 1 downto 0);
                                                  -- Connect to the BCK_RX pins in the top wrapper
    JESD204B_REFCLK            : in    std_logic;  -- Connect to BCK_REF_CLK pin in the top level wrapper

    -- jesd204b syncronization signals
    JESD204B_SYSREF            : in    std_logic;
    JESD204B_SYNC_N            : out   std_logic_vector((c_unb2c_board_tr_jesd204b.bus_w * c_unb2c_board_tr_jesd204b.nof_bus) - 1 downto 0)
  );
end lofar2_unb2c_filterbank;

architecture str of lofar2_unb2c_filterbank is
  -- Revision parameters
  constant c_revision_select        : t_lofar2_unb2c_filterbank_config := func_sel_revision_rec(g_design_name);
  constant c_nof_streams            : natural := c_revision_select.nof_streams_input;  -- Streams actually passed through for processing

  -- Firmware version x.y
  constant c_fw_version             : t_unb2c_board_fw_version := (1, 1);
  constant c_mm_clk_freq            : natural := c_unb2c_board_mm_clk_freq_100M;
  constant c_lofar2_sample_clk_freq : natural := 200 * 10**6;  -- alternate 160MHz. TODO: Use to check PPS

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic := '0';

  signal dp_pps                     : std_logic;
  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi     : t_mem_mosi;
  signal reg_fpga_temp_sens_miso     : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi  : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso  : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- JESD
  signal jesd204b_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal jesd204b_miso              : t_mem_miso := c_mem_miso_rst;

  -- Shiftram (applies per-antenna delay)
  signal reg_dp_shiftram_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_shiftram_miso       : t_mem_miso := c_mem_miso_rst;

  -- bsn source
  signal reg_bsn_source_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_source_miso        : t_mem_miso := c_mem_miso_rst;

  -- bsn scheduler
  signal reg_bsn_scheduler_wg_mosi  : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_scheduler_wg_miso  : t_mem_miso := c_mem_miso_rst;

  -- WG
  signal reg_wg_mosi                : t_mem_mosi := c_mem_mosi_rst;
  signal reg_wg_miso                : t_mem_miso := c_mem_miso_rst;
  signal ram_wg_mosi                : t_mem_mosi := c_mem_mosi_rst;
  signal ram_wg_miso                : t_mem_miso := c_mem_miso_rst;

  -- BSN MONITOR
  signal reg_bsn_monitor_input_mosi : t_mem_mosi;
  signal reg_bsn_monitor_input_miso : t_mem_miso;

  -- Data buffer raw
  signal ram_diag_data_buf_jesd_mosi: t_mem_mosi;
  signal ram_diag_data_buf_jesd_miso: t_mem_miso;
  signal reg_diag_data_buf_jesd_mosi: t_mem_mosi;
  signal reg_diag_data_buf_jesd_miso: t_mem_miso;

  -- Data buffer bsn
  signal ram_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal ram_diag_data_buf_bsn_miso : t_mem_miso;
  signal reg_diag_data_buf_bsn_mosi : t_mem_mosi;
  signal reg_diag_data_buf_bsn_miso : t_mem_miso;

  -- Aduh statistics monitor
  signal ram_aduh_monitor_mosi      : t_mem_mosi;
  signal ram_aduh_monitor_miso      : t_mem_miso;
  signal reg_aduh_monitor_mosi      : t_mem_mosi;
  signal reg_aduh_monitor_miso      : t_mem_miso;

  -- Subband statistics
  signal ram_st_sst_mosi            : t_mem_mosi;
  signal ram_st_sst_miso            : t_mem_miso;

  -- Spectral Inversion
  signal reg_si_mosi                : t_mem_mosi;
  signal reg_si_miso                : t_mem_miso;

  -- Filter coefficients
  signal ram_fil_coefs_mosi         : t_mem_mosi;
  signal ram_fil_coefs_miso         : t_mem_miso;

  -- Equalizer gains
  signal ram_equalizer_gains_mosi   : t_mem_mosi;
  signal ram_equalizer_gains_miso   : t_mem_miso;

  -- DP Selector
  signal reg_dp_selector_mosi       : t_mem_mosi;
  signal reg_dp_selector_miso       : t_mem_miso;

  -- Scrap ram
  signal ram_scrap_mosi             : t_mem_mosi;
  signal ram_scrap_miso             : t_mem_miso;

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);

  signal ait_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal pfb_sosi_arr               : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
  signal fsub_sosi_arr              : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2c_board_lib.ctrl_unb2c_board
  generic map (
    g_sim                => g_sim,
    g_design_name        => g_design_name,
    g_design_note        => g_design_note,
    g_stamp_date         => g_stamp_date,
    g_stamp_time         => g_stamp_time,
    g_revision_id        => g_revision_id,
    g_fw_version         => c_fw_version,
    g_mm_clk_freq        => c_mm_clk_freq,
    g_eth_clk_freq       => c_unb2c_board_eth_clk_freq_125M,
    g_aux                => c_unb2c_board_aux,
    g_factory_image      => g_factory_image,
    g_protect_addr_range => g_protect_addr_range,
    g_dp_clk_use_pll     => false
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,  -- Can be external 200MHz, or PLL generated
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_lofar2_unb2c_filterbank
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- mm interfaces for control
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,
    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- mm buses for signal flow blocks
    -- Jesd ip status/control
    jesd204b_mosi               => jesd204b_mosi,
    jesd204b_miso               => jesd204b_miso,
    reg_dp_shiftram_mosi        => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso        => reg_dp_shiftram_miso,
    reg_bsn_source_mosi         => reg_bsn_source_mosi,
    reg_bsn_source_miso         => reg_bsn_source_miso,
    reg_bsn_scheduler_mosi      => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_miso      => reg_bsn_scheduler_wg_miso,
    reg_wg_mosi                 => reg_wg_mosi,
    reg_wg_miso                 => reg_wg_miso,
    ram_wg_mosi                 => ram_wg_mosi,
    ram_wg_miso                 => ram_wg_miso,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
    ram_diag_data_buf_jesd_mosi => ram_diag_data_buf_jesd_mosi,
    ram_diag_data_buf_jesd_miso => ram_diag_data_buf_jesd_miso,
    reg_diag_data_buf_jesd_mosi => reg_diag_data_buf_jesd_mosi,
    reg_diag_data_buf_jesd_miso => reg_diag_data_buf_jesd_miso,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,
    ram_aduh_monitor_mosi       => ram_aduh_monitor_mosi,
    ram_aduh_monitor_miso       => ram_aduh_monitor_miso,
    reg_aduh_monitor_mosi       => reg_aduh_monitor_mosi,
    reg_aduh_monitor_miso       => reg_aduh_monitor_miso,
    ram_st_sst_mosi             => ram_st_sst_mosi,
    ram_st_sst_miso             => ram_st_sst_miso,
    ram_fil_coefs_mosi          => ram_fil_coefs_mosi,
    ram_fil_coefs_miso          => ram_fil_coefs_miso,
    reg_si_mosi                 => reg_si_mosi,
    reg_si_miso                 => reg_si_miso,
    ram_equalizer_gains_mosi    => ram_equalizer_gains_mosi,
    ram_equalizer_gains_miso    => ram_equalizer_gains_miso,
    reg_dp_selector_mosi        => reg_dp_selector_mosi,
    reg_dp_selector_miso        => reg_dp_selector_miso,
    ram_scrap_mosi              => ram_scrap_mosi,
    ram_scrap_miso              => ram_scrap_miso
  );

  -----------------------------------------------------------------------------
  -- node_adc_input_and_timing (AIT)
  --   .Contains JESD receiver, bsn source and associated data buffers, diagnostics and statistics
  -----------------------------------------------------------------------------

  u_ait: entity lofar2_sdp_lib.node_adc_input_and_timing
  generic map(
    g_nof_streams               => c_sdp_S_pn,
    g_buf_nof_data              => c_sdp_ait_buf_nof_data_bsn,
    g_sim                       => g_sim
  )
  port map(
    -- clocks and resets
    mm_clk                      => mm_clk,
    mm_rst                      => mm_rst,
    dp_clk                      => dp_clk,
    dp_rst                      => dp_rst,

    -- mm control buses
    jesd204b_mosi               => jesd204b_mosi,
    jesd204b_miso               => jesd204b_miso,
    reg_dp_shiftram_mosi        => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso        => reg_dp_shiftram_miso,
    reg_bsn_source_mosi         => reg_bsn_source_mosi,
    reg_bsn_source_miso         => reg_bsn_source_miso,
    reg_bsn_scheduler_wg_mosi   => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_wg_miso   => reg_bsn_scheduler_wg_miso,
    reg_wg_mosi                 => reg_wg_mosi,
    reg_wg_miso                 => reg_wg_miso,
    ram_wg_mosi                 => ram_wg_mosi,
    ram_wg_miso                 => ram_wg_miso,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_mosi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_miso,
    ram_diag_data_buf_jesd_mosi => ram_diag_data_buf_jesd_mosi,
    ram_diag_data_buf_jesd_miso => ram_diag_data_buf_jesd_miso,
    reg_diag_data_buf_jesd_mosi => reg_diag_data_buf_jesd_mosi,
    reg_diag_data_buf_jesd_miso => reg_diag_data_buf_jesd_miso,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_mosi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_miso,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_mosi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_miso,
    ram_aduh_monitor_mosi       => ram_aduh_monitor_mosi,
    ram_aduh_monitor_miso       => ram_aduh_monitor_miso,
    reg_aduh_monitor_mosi       => reg_aduh_monitor_mosi,
    reg_aduh_monitor_miso       => reg_aduh_monitor_miso,

     -- Jesd external IOs
    jesd204b_serial_data       => JESD204B_SERIAL_DATA,
    jesd204b_refclk            => JESD204B_REFCLK,
    jesd204b_sysref            => JESD204B_SYSREF,
    jesd204b_sync_n            => JESD204B_SYNC_N,

    -- Streaming data output
    out_sosi_arr               => ait_sosi_arr
  );

  u_fsub : entity lofar2_sdp_lib.node_sdp_filterbank
  generic map(
    g_sim                    => g_sim,
    g_wpfb                   => g_wpfb,
    g_scope_selected_subband => g_scope_selected_subband
  )
  port map(
    dp_clk             => dp_clk,
    dp_rst             => dp_rst,

    in_sosi_arr        => ait_sosi_arr,
    pfb_sosi_arr       => pfb_sosi_arr,
    fsub_sosi_arr      => fsub_sosi_arr,

    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    reg_si_mosi        => reg_si_mosi,
    reg_si_miso        => reg_si_miso,
    ram_st_sst_mosi    => ram_st_sst_mosi,
    ram_st_sst_miso    => ram_st_sst_miso,
    ram_fil_coefs_mosi => ram_fil_coefs_mosi,
    ram_fil_coefs_miso => ram_fil_coefs_miso,
    ram_gains_mosi     => ram_equalizer_gains_mosi,
    ram_gains_miso     => ram_equalizer_gains_miso,
    reg_selector_mosi  => reg_dp_selector_mosi,
    reg_selector_miso  => reg_dp_selector_miso
  );
end str;
