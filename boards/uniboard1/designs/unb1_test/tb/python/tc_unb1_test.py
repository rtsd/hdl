#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for unb1_test

Usage:

   --rep = number of intervals that diagnostics results are verified
   --sim targets a running simulation.

Description:
   This test case tests:
   - system info
   - read sensors
   - read ppsh
   - write to wdi to force reload from bank 0
   - flash access: write image to bank 1
   - remote update: start image in bank 1
   - read status MAC 10GbE
   - read status PHY XAUI
   - BG - DB tests for 1 port on 1 FPGA
   - BG - DB tests for all ports on all FPGAs
   - link test between ports on the same FPGA (same XO)
   - link test between ports on different Uniboards (XO drift)
   - stable link after image restart

"""

###############################################################################
# System imports
import sys
import signal
import random
import test_case
import node_io
import pi_system_info
import pi_unb_sens
import pi_ppsh
import pi_wdi
import pi_epcs
import pi_remu
import pi_eth
import pi_bsn_monitor
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_debug_wave
import pi_io_ddr
import pi_udp_offload

from tools import *
from common import *
from pi_common import *


def show_compare_arrays(tc,a,b):
    import numpy as np
    tc.append_log(5, 'Compare array A:\n%s' % np.asarray(a))
    tc.append_log(5, 'With array B:\n%s' % np.asarray(b))
    compared = (np.asarray(a) == np.asarray(b))
    tc.append_log(5, 'Result:\n%s' % compared)
    return (not False in compared)

def test_BG_to_DB(tc,io,cmd):
    if tc.number == 1:
        use_pps=True
        tc.set_section_id('Test BG-DB (pps sync) - ')
    else:
        use_pps=False
        tc.set_section_id('Test BG-DB - ')

    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    if use_pps==True: ppsh = pi_ppsh.PiPpsh(tc, io)

    tc.set_section_id('Read - ')
    instanceName=tc.gpString

    if instanceName == '1GBE':
        eth = pi_eth.PiEth(tc, io)
        eth.write_udp_port_en(0) 

    nof_streams=3
    blocksize=0
    Bg = pi_diag_block_gen.PiDiagBlockGen(tc,io,nofChannels=nof_streams,ramSizePerChannel=blocksize,instanceName=instanceName)
    Bg.write_disable()

    settings = Bg.read_block_gen_settings()
    samples_per_packet = settings[0][1]
    gapsize            = settings[0][3]
    blocksize          = pow(2, ceil_log2(samples_per_packet+gapsize))

    Bg = pi_diag_block_gen.PiDiagBlockGen(tc,io,nofChannels=nof_streams, ramSizePerChannel=blocksize,instanceName=instanceName)
    #Bg.write_block_gen_settings(samplesPerPacket=700, blocksPerSync=781250, gapSize=300, memLowAddr=0, memHighAddr=701, BSNInit=42)
    Db = pi_diag_data_buffer.PiDiagDataBuffer(tc,io,nofStreams=nof_streams,ramSizePerStream=blocksize,instanceName=instanceName)
    resetptrn = [0xc1ea1ed1]*blocksize #samples_per_packet + [0]*(blocksize-samples_per_packet)
    for s in tc.gpNumbers:
        Db.overwrite_data_buffer(resetptrn,streamNr=s,vLevel=9)

    # Trigger by reading databuffers:
    for s in tc.gpNumbers:
        Db.read_data_buffer(streamNr=s,vLevel=9)

    if use_pps==True:
        Bg.write_enable_pps()
        ppsh.wait_until_pps(vLevel=6)
    else:
        Bg.write_enable()



    # enable tx_seq
#    from pi_diag_tx_seq import REGMAP,PiDiagTxSeq
#    REGMAP_tx=REGMAP
#    from pi_diag_rx_seq import REGMAP,PiDiagRxSeq
#    REGMAP_rx=REGMAP

#    print 'read PiDiagTxSeq, PiDiagRxSeq before write'
#    on_execute(class_definition=PiDiagTxSeq,regmap=REGMAP_tx) # this reads/shows ALL registers
#    on_execute(class_definition=PiDiagRxSeq,regmap=REGMAP_rx) # this reads/shows ALL registers

#    tx_seq = PiDiagTxSeq(tc,io,inst_name='10GBE')
#    tx_seq.write(tc.nodeNrs,inst_nrs=0, registers=[('control', 1)],regmap=REGMAP_tx)
#
#    rx_seq = PiDiagRxSeq(tc,io,inst_name='10GBE')
#    rx_seq.write(tc.nodeNrs,inst_nrs=0, registers=[('control', 1)],regmap=REGMAP_rx)
#
#    print 'read PiDiagTxSeq, PiDiagRxSeq after write'
#    on_execute(class_definition=PiDiagTxSeq,regmap=REGMAP_tx) # this reads/shows ALL registers
#    on_execute(class_definition=PiDiagRxSeq,regmap=REGMAP_rx) # this reads/shows ALL registers


#    return

    if instanceName == 'DDR':
        ddr = pi_io_ddr.PiIoDdr(tc,io,nof_inst=1)
        if tc.sim == True:
            do_until_eq(ddr.read_init_done, ms_retry=1000, val=1, s_timeout=13600)  # 110000 



    bg_ram = []
    for s in tc.gpNumbers:
        ram = Bg.read_waveform_ram(channelNr=s,vLevel=5)
        rram=[]
        for r in ram:
            ram_10G = list(r) # () -> []
            # truncate: in 10GbE the data is only sized 700
            ram_10G = ram_10G[:-(blocksize-700)] # [:-(blocksize-samples_per_packet)]
            rram.append(ram_10G)
        bg_ram.append(rram)


    # Poll the databuffer to check if the response is there.
    # Retry after 3 seconds so we don't issue too many MM reads in case of simulation.
    #do_until_ge(Db.read_nof_words, ms_retry=3000, val=blocksize, s_timeout=3600)


    db_ram = []
    for s in tc.gpNumbers:
        databuf = Db.read_data_buffer(streamNr=s,vLevel=5)
        rram=[]
        for r in databuf:
            ram_10G = r
            # truncate: in 10GbE the data is only sized 700
            ram_10G = ram_10G[:-(blocksize-700)] # [:-(blocksize-samples_per_packet)]
            rram.append(ram_10G)
        db_ram.append(rram)


    #print 'bg_ram=',bg_ram
    #print 'db_ram=',db_ram

    tc.append_log(5, 'number of BG streams=%d  number of DB streams=%d  per stream:' % (len(bg_ram),len(db_ram)))
    for st in range(len(bg_ram)):
        tc.append_log(5, 'BG=%d blocks with' % len(bg_ram[st]))
        for b in range(len(bg_ram[st])):
            tc.append_log(5, '%d words' % len(bg_ram[st][b]))

        tc.append_log(5, 'DB=%d blocks with' % len(db_ram[st]))
        for b in range(len(db_ram[st])):
            tc.append_log(5, '%d words' % len(db_ram[st][b]))



    tc.set_section_id('Compare (BG==DB) - ')
    compared = (bg_ram == db_ram)
    tc.append_log(3, '')
    tc.append_log(1, '>>> (BG==DB): %s' % compared)
    tc.append_log(3, '')
    if compared==False:
        tc.set_result('FAILED')
    
    show_compare_arrays(tc,bg_ram,db_ram)
    Bg.write_disable()


def get_BG_blocksize(tc,io):
    nof_streams=3
    blocksize=0
    Bg = pi_diag_block_gen.PiDiagBlockGen(tc,io,nofChannels=nof_streams,ramSizePerChannel=blocksize)
    settings = Bg.read_block_gen_settings()
    samples_per_packet = settings[0][1]
    gapsize            = settings[0][3]
    blocksize          = pow(2, ceil_log2(samples_per_packet+gapsize))
    return blocksize

def write_BG(tc,io,buf):
    tc.set_section_id('Write BG - ')
    nof_streams=3
    blocksize=get_BG_blocksize(tc,io)
    Bg = pi_diag_block_gen.PiDiagBlockGen(tc,io,nofChannels=nof_streams, ramSizePerChannel=blocksize)
    Bg.write_disable()
    for s in tc.gpNumbers:
        Bg.write_waveform_ram(buf,channelNr=s)

def write_BG_00100(tc,io,cmd):
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    blocksize=get_BG_blocksize(tc,io)
    ptrn = [0]*blocksize
    ptrn[blocksize/2]=1
    write_BG(tc,io,ptrn)

def write_BG_ff7ff(tc,io,cmd):
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    blocksize=get_BG_blocksize(tc,io)
    ptrn = [0xffffffff]*blocksize
    ptrn[blocksize/2]=0xffff7fff
    write_BG(tc,io,ptrn)

def write_BG_rand(tc,io,cmd):
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    blocksize=get_BG_blocksize(tc,io)
    random.seed(10)
    ptrn = [random.randint(0,0xffffffff) for r in range(blocksize)]
    write_BG(tc,io,ptrn)

def write_BG_count(tc,io,cmd):
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    blocksize=get_BG_blocksize(tc,io)
    ptrn = range(blocksize)
    write_BG(tc,io,ptrn)


def test_tr_xaui(tc,io,cmd):
    tc.set_section_id('Read tr xaui status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    tc.append_log(3, '')
    from pi_tr_xaui import REGMAP,PiTrXaui
    on_execute(class_definition=PiTrXaui,regmap=REGMAP)


def verify_10GbE_status(tc,item_name,item_value):
    no_errors = [0]*len(tc.nodeNrs)
    err=[]
    for s in range(len(item_value)): err.append(item_value[s][2][0])
    tc.append_log(3,'%s=%s' % (item_name,err))
    if err != no_errors:
        tc.set_result('FAILED')


def test_tr_10GbE(tc,io,cmd):
    tc.set_section_id('tr_10GbE status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    nof_streams=3
    from pi_tr_10GbE import REGMAP,PiTr10GbE,ADDR_W
    tr10 = PiTr10GbE(tc,io,nof_inst=nof_streams)

    if tc.verbosity > 5:
        on_execute(class_definition=PiTr10GbE,regmap=REGMAP) # this reads/shows ALL status

    for s in tc.gpNumbers:
        inst_offs = s*2**ADDR_W

        # set rx/tr _frame_maxlength to 9000 (jumbo) as it inits at 1518 and causing frame errors:
        frame_size_jumbo = 9000
        tr10.write_reg(tc.nodeNrs,'REG_TR_10GBE',inst_offs+(REGMAP['rx_frame_maxlength'][2][0]),[frame_size_jumbo])
        tr10.write_reg(tc.nodeNrs,'REG_TR_10GBE',inst_offs+(REGMAP['tx_frame_maxlength'][2][0]),[frame_size_jumbo])


        tc.set_section_id('Read/verify tr_10GbE status over Nodes %s stream=%d ' % (tc.nodeNrs,s))
        tc.append_log(3, '')

        status_names = ['rx_stats_framesErr','tx_stats_framesErr','rx_stats_framesCRCErr', \
                        'tx_stats_framesCRCErr','rx_stats_etherStatsCRCErr','tx_stats_etherStatsCRCErr']
        for stat_name in status_names:
            verify_10GbE_status(tc,stat_name,tr10.read_reg(tc.nodeNrs,'REG_TR_10GBE',inst_offs+(REGMAP[stat_name][2][0]),1))


def test_eth10g(tc,io,cmd):
    tc.set_section_id('eth10g link status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    from pi_eth10g import REGMAP,PiEth10g
    on_execute(class_definition=PiEth10g,regmap=REGMAP) # this reads/shows ALL status
    #eth10g = PiEth10g(tc,io,inst_name='QSFP_RING')
    #tc.append_log(1, '>>> %s' % eth10g.read_eth10g())


def test_tx_seq(tc,io,cmd):
    tc.set_section_id('tx seq - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    from pi_diag_tx_seq import REGMAP,PiDiagTxSeq
    tx_seq = PiDiagTxSeq(tc,io,inst_name='10GBE')
    tx_seq.write(tc.nodeNrs,inst_nrs=0, registers=[('control', 1)],regmap=REGMAP)

    # instanceName is taken from tc.gpString
    on_execute(class_definition=PiDiagTxSeq,regmap=REGMAP) # this reads/shows ALL status


def test_rx_seq(tc,io,cmd):
    tc.set_section_id('rx seq - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    from pi_diag_rx_seq import REGMAP,PiDiagRxSeq
    rx_seq = PiDiagRxSeq(tc,io,inst_name='10GBE')
    rx_seq.write(tc.nodeNrs,inst_nrs=0, registers=[('control', 1)],regmap=REGMAP)



    # instanceName is taken from tc.gpString
    on_execute(class_definition=PiDiagRxSeq,regmap=REGMAP) # this reads/shows ALL status





def test_ddr_stat(tc,io,cmd):
    tc.set_section_id('DDR3 status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    from pi_io_ddr import REGMAP,PiIoDdr
    on_execute(class_definition=PiIoDdr,regmap=REGMAP)


def test_bsn_mon(tc,io,cmd):
    tc.set_section_id('Read BSN monitor status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    Bsn = pi_bsn_monitor.PiBsnMonitor(tc,io)
    Bsn.read_bsn_monitor()
    tc.append_log(3, '')



def test_info(tc,io,cmd):
    tc.set_section_id('Read System Info - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    info = pi_system_info.PiSystemInfo(tc, io)
    #info.read_system_info()
    #tc.append_log(3, '')
    #info.read_use_phy()
    tc.append_log(3, '')
    design_name = info.read_design_name()
    tc.append_log(1, '>>> design_name=%s' % design_name)
    tc.append_log(3, '')
    info.read_stamps()
    tc.append_log(3, '')
    info.read_design_note()
    
    expected_design_name = tc.gpString
    if expected_design_name != '':
        tc.set_section_id('Verify System Info - ')
        compared=True
        for name in design_name:
            if (name != expected_design_name): 
                tc.set_result('FAILED')
                compared=False
                tc.append_log(2, '>>> design_name mismatch!! (%s != %s)' % (name,expected_design_name))
        tc.append_log(1, '>>> Verify design_name == %s: %s' % (expected_design_name,compared))



def read_regmap(tc,io,cmd):
    tc.set_section_id('Update REGMAP - ')
    info = pi_system_info.PiSystemInfo(tc, io)
    tc.append_log(1, '>>> reading REGMAPs')
    info.make_register_info()
    tc.append_log(1, '>>> reload NodeIO class')
    return node_io.NodeIO(tc.nodeImages, tc.base_ip)


    
def test_sensors(tc,io,cmd):
    tc.set_section_id('Read sensors - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    sens = pi_unb_sens.PiUnbSens(tc, io)

    sens.read_unb_sensors()
    tc.append_log(3, '')
    sens.read_fpga_temperature()
    tc.append_log(3, '')
    sens.read_eth_temperature()
    tc.append_log(3, '')
    sens.read_unb_current()
    sens.read_unb_voltage()
    sens.read_unb_power()



def test_ppsh(tc,io,cmd):
    tc.set_section_id('Read PPSH capture count - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    Ppsh = pi_ppsh.PiPpsh(tc, io)
    Ppsh.read_ppsh_capture_cnt()
    tc.append_log(3, '')



def test_wdi(tc,io,cmd):
    tc.set_section_id('Reset to image in bank 0 using WDI - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    Wdi = pi_wdi.PiWdi(tc, io)
    Wdi.write_wdi_override()
    tc.append_log(3, '')
    tc.append_log(3, '>>> Booting...')
    tc.sleep(5.0)



def test_remu(tc,io,cmd):
    tc.set_section_id('REMU start image in bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    dummy_tc = test_case.Testcase('Dummy TB - ', '',logfilename='REMU-log')
    dummy_tc.set_result('PASSED')
    
    Remu = pi_remu.PiRemu(dummy_tc, io)
    try:
        Remu.write_user_reconfigure()
    except:
        pass # ignoring FAILED

    if dummy_tc.get_result() == 'FAILED':
        tc.append_log(1, 'Result=%s but ignoring this' % dummy_tc.get_result())

    tc.append_log(3, '>>> Booting...')
    tc.sleep(5.0)
    tc.append_log(3, '')



def test_eth(tc,io,cmd):
    tc.set_section_id('ETH status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    eth = pi_eth.PiEth(tc, io)
    hdr=eth.read_hdr(0)
    eth.disassemble_hdr(hdr)
    tc.append_log(3, '')



def test_flash(tc,io,cmd):
    tc.set_section_id('Flash write to bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    Epcs = pi_epcs.PiEpcs(tc, io)
    path_to_rbf = instanceName = tc.gpString
    Epcs.write_raw_binary_file("user", path_to_rbf)
    tc.append_log(3, '')

    tc.set_section_id('Flash read/verify bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> Read from flash (pi_epcs.py)')
    tc.append_log(3, '>>>')
    
    path_to_rbf = instanceName = tc.gpString
    Epcs.read_and_verify_raw_binary_file("user", path_to_rbf)
    tc.append_log(3, '')


def set_led(tc,dw,led,text):
    tc.append_log(3, text)
    dw.set_led(led)
    tc.sleep(1.0)

def test_leds(tc,io,cmd):
    tc.set_section_id('LED test - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    dw =  pi_debug_wave.PiDebugWave(tc, io)
    set_led(tc,dw,'off',   '')
    set_led(tc,dw,'red',  'RED on')
    set_led(tc,dw,'off',  'RED off')
    set_led(tc,dw,'green','GREEN on')
    set_led(tc,dw,'off',  'GREEN off')
    set_led(tc,dw,'both', 'ORANGE (RED+GREEN) on')
    set_led(tc,dw,'off',  'ORANGE (RED+GREEN) off')
    tc.append_log(3, '')


def sleep(tc,io,cmd):
    tc.set_section_id('%s - ' % cmd)
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    if cmd == 'sleep1':
        tc.sleep(1.0)
    elif cmd == 'sleep5':
        tc.sleep(5.0)

def show_help(tc,io,cmd):
    tc.set_section_id('%s - ' % cmd)
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))


# Avaliable commands
Cmd = dict()
Cmd['REGMAP']  = (read_regmap,  'using pi_system_info to read register info (access PIO_SYSTEM_INFO) and store REGMAPs','')
Cmd['INFO']    = (test_info,    'using pi_system_info to read system info (access PIO_SYSTEM_INFO)','(-s for expected design_name)')
Cmd['FLASH']   = (test_flash,   'using pi_epcs to program/verify flash','(-s for .rbf file)')
Cmd['SENSORS'] = (test_sensors, 'using pi_unb_sens to readout sensors (access REG_UNB_SENS)','')
Cmd['LED']     = (test_leds,    'using pi_debug_wave to set LEDs (access PIO_DEBUG_WAVE)','')
Cmd['PPSH']    = (test_ppsh,    'using pi_ppsh to read PPSH capture count (access PIO_PPS)','')
Cmd['ETH']     = (test_eth,     'using pi_eth to read eth status','')
Cmd['REMU']    = (test_remu,    'using pi_remu to load user image (access REG_REMU)','')
Cmd['WDI']     = (test_wdi,     'using pi_wdi to reset to image in bank 0 (access REG_WDI)','')
Cmd['XAUI']    = (test_tr_xaui, 'using pi_tr_xaui to read xaui status (access REG_TR_XAUI)','(-r for addressing streams)')
Cmd['10GBE']   = (test_tr_10GbE,'using pi_tr_10GbE to read 10GbE status (access REG_TR_10GBE)','(-r for addressing streams)')
Cmd['ETH10G']  = (test_eth10g,  'using pi_eth10g to read eth10g link status (access REG_ETH10G_*)','(-r for addressing streams)')
Cmd['DDR']     = (test_ddr_stat,'using pi_io_ddr to read DDR3 status (access REG_IO_DDR)','')
Cmd['TXSEQ']   = (test_tx_seq,  'using pi_diag_tx_seq','')
Cmd['RXSEQ']   = (test_rx_seq,  'using pi_diag_rx_seq','')
Cmd['BSN']     = (test_bsn_mon, 'using pi_bsn_monitor to read BSN monitor (access REG_BSN_MONITOR)','')
Cmd['BGDB']    = (test_BG_to_DB,'using BG (pi_diag_block_gen.py) and DB (pi_diag_data_buffer.py)','(-r and -s for addressing streams, -n1 for use pps)')
Cmd['sleep1']  = (sleep,        'Sleep 1 second','')
Cmd['sleep5']  = (sleep,        'Sleep 5 seconds','')
Cmd['example'] = (show_help,    'show several example commands','')
Cmd['help']    = (show_help,    'show help on commands','')


def help_text(tc,io,cmd):
    str=''
    if cmd == 'help':
        tc.append_log(0, '\n')
        tc.append_log(0, '>>> Help:')
        tc.append_log(0, 'Usage: %s <nodes> <command sequence> [-v..] [--rep ...]' % sys.argv[0])
        tc.append_log(0, '')
        tc.append_log(0, '    <nodes>: use: --unb N --fn N --bn N (N is a number or vector) or:')
        tc.append_log(0, '    <nodes>: use: --gn N (N is a number or vector)')
        tc.append_log(0, '    <command sequence>: use: --seq <command(s) separated by ",">:')
        tc.append_log(0, '    <streamdevice>: use: -s 10GBE  or  -s 1GBE  to select stream device')
        tc.append_log(0, '    <streamnumber>: use: -r N  to select a stream number (N can also be 0:2)')
        tc.append_log(0, '')
        for cmd in sorted(Cmd):
            tc.append_log(0, '    .  %s\t%s  %s' % (cmd,Cmd[cmd][1],Cmd[cmd][2]))
        tc.append_log(0, '')
        tc.append_log(0, '    [-vN]: verbose level N (default=5): %s' % tc.verbose_levels())
        tc.append_log(0, '    [--rep N]: N=number of repeats, where -1 is forever, non-stop')
        help_text(tc,io,'example')
    elif cmd == 'example':
        tc.append_log(0, '')
        tc.append_log(0, '>>> Examples:')
        tc.append_log(0, '')
        tc.append_log(0, 'Getting INFO from all nodes on 1 Uniboard: %s --gn 0:7 --seq INFO' % sys.argv[0])
        tc.append_log(0, '')
        tc.append_log(0, '[reset, load user img] sequence: --seq REGMAP,WDI,REGMAP,REMU,REGMAP,INFO')
        tc.append_log(0, '[flash+start user img] sequence: --seq FLASH,WDI,REGMAP,REMU,REGMAP,INFO -s file.rbf')
        tc.append_log(0, '[re-read info,sensors] sequence: --seq INFO,PPSH,SENSORS --rep -1 -s expected_design_name')
        tc.append_log(0, '[reset to factory]     sequence: --seq WDI,REGMAP')
        tc.append_log(0, '[program user image]   sequence: --seq FLASH -s file.rbf')
        tc.append_log(0, '[load user image]      sequence: --seq REMU,REGMAP')
        tc.append_log(0, '[modelsim BG-DB test] arguments: --unb 0 --fn 0 --seq BGDB --sim -r 0:2')
        tc.append_log(0, '\n')
    else:
        str = Cmd[cmd][1]
    return str


def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    tc.repeat=0


##################################################################################################################
# Main
#
# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
dgnName = tc.gpString
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Title : Test bench (%s) on nodes %s, %s' % (sys.argv[0],tc.unb_nodes_string(''),dgnName))
tc.append_log(0, '>>> Commandline : %s' % " ".join(sys.argv))
tc.append_log(3, '>>>')


# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

signal.signal(signal.SIGINT, signal_handler)

#offl = pi_udp_offload.PiUdpOffload(tc, io, tc.nodeFnNrs[0], lcu='capture5')
#offl.offload_en(offload_streams=[0], nof_words_per_block=24)
#        offl.offload_dis(offload_streams=[0])

##################################################################################################################
# Run tests
while tc.repeat != 0: # -1 for non-stop
    tc.repeat -= 1
    tc.next_run()
    tc.append_log(3, '')
    
    try:
        for cmd in tc.sequence:
            tc.set_section_id('Next command: %s ' % cmd)
            tc.append_log(1, '>>> Testrun %d (@%.02fs) - ' % (tc.get_nof_runs(),tc.get_run_time()))

            if cmd == 'REGMAP': # reload node_io:
                io = Cmd[cmd][0](tc,io,cmd)
            else:
                Cmd[cmd][0](tc,io,cmd)


    except KeyError:
        print 'Unknown command:',cmd
        cmd='help'
        Cmd[cmd][0](tc,io,cmd)
#    except:
#        print 'Catched error:',sys.exc_info()[0]



##################################################################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(0, '>>> Number of runs=%d' % tc.get_nof_runs())
tc.append_log(0, '>>> Number of errors=%d' % tc.get_nof_errors())
tc.append_log(0, '>>> Runtime=%f seconds (%f hours)' % (tc.get_run_time(),tc.get_run_time()/3600))
tc.append_log(3, '>>>')

sys.exit(tc.get_result())

