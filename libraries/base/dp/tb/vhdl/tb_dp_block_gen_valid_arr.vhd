-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_block_gen_valid_arr
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- Observe out_sosi in wave window

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_block_gen_valid_arr is
  generic (
    g_nof_streams           : positive := 1;
    g_nof_data_per_block    : positive := 11;
    g_nof_blk_per_sync      : positive := 8;
    g_check_input_sync      : boolean := true;
    g_nof_pages_bsn         : natural := 0;
    g_restore_global_bsn    : boolean := false;
    g_enable                : t_dp_flow_control_enum := e_active;  -- always e_active or e_pulse block generator enable
    g_flow_control          : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control for input valid
    g_nof_repeat            : natural := 1000
  );
end tb_dp_block_gen_valid_arr;

architecture tb of tb_dp_block_gen_valid_arr is
  constant c_pulse_period             : natural := 5;
  constant c_nof_valid_per_sync       : natural := g_nof_blk_per_sync * g_nof_data_per_block;
  constant c_nof_valid_per_enable     : natural := 3 * c_nof_valid_per_sync + 1;

  constant c_dsp_latency              : natural := 1;  -- >= 1 block and typically << g_nof_blk_per_sync blocks

  constant c_sync_offset              : natural := 1;  -- 0, < g_nof_blk_per_sync
  constant c_bsn_init                 : natural := 37;
  constant c_channel_init             : natural := 17;
  constant c_err_init                 : natural := 13;

  constant c_input_bsn_no_wrap        : natural := 0;  -- use 0 to indicate no max, so no wrap
  constant c_input_bsn_gap            : natural := g_nof_blk_per_sync;
  constant c_local_bsn_max            : natural := g_nof_blk_per_sync - 1;
  constant c_local_bsn_gap            : natural := 2;

  signal tb_input_end        : std_logic := '0';
  signal tb_end              : std_logic := '0';
  signal clk                 : std_logic := '1';
  signal rst                 : std_logic := '1';
  signal sl1                 : std_logic := '1';
  signal exp_channel         : std_logic_vector(c_dp_stream_channel_w - 1 downto 0) := TO_DP_CHANNEL(c_channel_init);
  signal exp_err             : std_logic_vector(c_dp_stream_error_w - 1 downto 0) := TO_DP_ERROR(c_err_init);

  signal enable              : std_logic;

  signal in_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal out_sosi_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal stimuli_sosi        : t_dp_sosi;
  signal dsp_sosi            : t_dp_sosi;
  signal in_sosi             : t_dp_sosi;
  signal out_sosi            : t_dp_sosi;
  signal prev_out_sosi       : t_dp_sosi;
  signal out_sosi_local      : t_dp_sosi;
  signal prev_out_sosi_local : t_dp_sosi;
  signal out_gap             : std_logic := '0';
  signal hold_sop            : std_logic := '0';
  signal exp_size            : natural := g_nof_data_per_block;
  signal cnt_size            : natural := 0;

  signal verify_bsn_en       : std_logic := '0';
  signal verify_done         : std_logic := '0';
  signal verify_valid        : std_logic := '0';
  signal verify_sop          : std_logic := '0';
  signal verify_eop          : std_logic := '0';
  signal verify_sync         : std_logic := '0';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- STIMULI
  ------------------------------------------------------------------------------

  p_enable : process
    variable vI : natural := 0;
  begin
    enable <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);
    while tb_input_end = '0' loop
      enable <= '1';
      proc_common_wait_some_cycles(clk, 1);
      if g_enable = e_pulse then
        if vI mod c_pulse_period = 0 then
          enable <= '0';
        end if;
      end if;
      proc_common_wait_some_cycles(clk, c_nof_valid_per_enable);
      vI := vI + 1;
    end loop;
    enable <= '0';

    -- End of stimuli
    proc_common_wait_some_cycles(clk, 100);
    proc_common_gen_pulse(clk, verify_done);
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- input data
  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_flow_control   => g_flow_control,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => g_nof_blk_per_sync,
    g_sync_offset    => c_sync_offset,
    g_data_init      => 0,
    g_bsn_init       => TO_DP_BSN(c_bsn_init),
    g_err_init       => c_err_init,
    g_err_incr       => 0,
    g_channel_init   => c_channel_init,
    g_channel_incr   => 0,
    -- specific
    g_in_dat_w       => 32,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => g_nof_data_per_block,
    g_pkt_gap        => 0
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Generate stimuli
    src_out             => stimuli_sosi,

    -- End of stimuli
    last_snk_in         => OPEN,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => OPEN,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_input_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  -- Use dp_pipeline to model the latency introduced by upstream DSP components
  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dsp_latency  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_in       => stimuli_sosi,
    -- ST source
    src_out      => dsp_sosi
  );

  p_in_sosi : process(dsp_sosi, stimuli_sosi)
  begin
    in_sosi <= dsp_sosi;
    if g_nof_pages_bsn > 0 then
      in_sosi.sync <= stimuli_sosi.sync;
      in_sosi.bsn  <= stimuli_sosi.bsn;
    end if;
  end process;

  -- Use same stimuli for all streams
  in_sosi_arr <= (others => in_sosi);

  ------------------------------------------------------------------------------
  -- VERIFICATION
  ------------------------------------------------------------------------------

  p_verify_bsn : process
  begin
    verify_bsn_en <= '0';
    proc_common_wait_until_high(clk, enable);
    proc_common_wait_until_hi_lo(clk, out_sosi.sop);
    proc_common_wait_until_hi_lo(clk, out_sosi.sop);  -- first two sop will have occured, so start verification for subsequent BSN
    if g_check_input_sync = true or g_nof_pages_bsn > 0 then
      proc_common_wait_until_hi_lo(clk, out_sosi.sync);  -- if necessary also wait for first sync
    end if;
    while enable = '1' loop
      verify_bsn_en <= '1';
      wait until rising_edge(clk);
    end loop;
  end process;

  -- Verify stream 0
  out_sosi <= out_sosi_arr(0);

  -- Verify that the stimuli have been applied at all
  verify_valid <= '1' when out_sosi.valid = '1' and rising_edge(clk);
  verify_sop   <= '1' when out_sosi.sop = '1'   and rising_edge(clk);
  verify_eop   <= '1' when out_sosi.eop = '1'   and rising_edge(clk);
  verify_sync  <= '1' when out_sosi.sync = '1'  and rising_edge(clk);
  proc_dp_verify_value("out_sosi.valid",            clk, verify_done, sl1,         verify_valid);
  proc_dp_verify_value("out_sosi.sop",              clk, verify_done, sl1,         verify_sop);
  proc_dp_verify_value("out_sosi.eop",              clk, verify_done, sl1,         verify_eop);
  proc_dp_verify_value("out_sosi.sync",             clk, verify_done, sl1,         verify_sync);
  proc_dp_verify_value("out_sosi.channel", e_equal, clk, verify_done, exp_channel, out_sosi.channel);
  proc_dp_verify_value("out_sosi.err",     e_equal, clk, verify_done, exp_err,     out_sosi.err);

  -- Verify some general streaming interface properties
  proc_dp_verify_gap_invalid(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, out_gap);  -- Verify that the out_sosi valid is low between blocks

  -- define out_sosi_local with sync = '0' to be able to verify local bsn incrementing during rest of sync interval
  p_out_sosi_local : process(out_sosi)
  begin
    out_sosi_local <= out_sosi;
    out_sosi_local.sync <= '0';
    if out_sosi.sync = '1' then
      out_sosi_local.sop <= '0';
    end if;
  end process;

  verify_sync_bsn : if g_check_input_sync = true or g_nof_pages_bsn > 0 generate
    -- verify BSN at sync when out_sosi should have same bsn as in_sosi
    proc_dp_verify_sync(c_input_bsn_gap, c_sync_offset, clk, verify_bsn_en, out_sosi.sync, out_sosi.sync, out_sosi.bsn);
  end generate;

  verify_local_bsn : if g_restore_global_bsn = false generate
    -- verify BSN for sop at sync
    proc_dp_verify_data("BSN", c_input_bsn_no_wrap, c_input_bsn_gap, clk, verify_bsn_en, out_sosi.sync, out_sosi.bsn, prev_out_sosi.bsn);
    -- verify BSN for the other sop between sync using out_sosi_local
    proc_dp_verify_data("BSN", c_local_bsn_max, c_local_bsn_gap, clk, verify_bsn_en, out_sosi_local.sop, out_sosi_local.bsn, prev_out_sosi_local.bsn);
  end generate;

  verify_global_bsn : if g_restore_global_bsn = true generate
    -- verify BSN for all sop
    proc_dp_verify_data("BSN", clk, verify_bsn_en, out_sosi.sop, out_sosi.bsn, prev_out_sosi.bsn);
  end generate;

  -- Verify intervals
  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_sop);

  proc_dp_verify_block_size(exp_size, clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, cnt_size);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  u_dut: entity work.dp_block_gen_valid_arr
  generic map (
    g_nof_streams           => g_nof_streams,
    g_nof_data_per_block    => g_nof_data_per_block,
    g_nof_blk_per_sync      => g_nof_blk_per_sync,
    g_check_input_sync      => g_check_input_sync,
    g_nof_pages_bsn         => g_nof_pages_bsn,
    g_restore_global_bsn    => g_restore_global_bsn
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- Streaming sink
    snk_in      => in_sosi,
    snk_in_arr  => in_sosi_arr,
    -- Streaming source
    src_out_arr => out_sosi_arr,
    -- MM control
    enable      => enable
  );
end tb;
