-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify reorder_col_wide select and reorder_row_select.
-- Usage:
-- > as 10
-- > run -all
-- * The tb is self stopping and self checking,tb_end will stop the simulation by
--   stopping the clk and thus all toggling.
--
-- Description: The tb generates counter data for all g_nof_inputs packetized according
-- to g_nof_sync, g_nof_block_per_sync and g_nof_ch_in. Each input stream then passes through
-- reorder_col_wide_select where each stream is reordered according to g_nof_ch_sel_col,
-- g_ch_sel_offsets and g_ch_sel_step. Those streams then enter reorder_row_select. Each output
-- is created by selecting one input at the time based on in_select, this tb always selects the next input (row)
-- in ascending order after g_nof_ch_sel_col elements. Each output has the same selection in this tb.
-- The tb verifies the functionality by generating an expected sosi array and comparing it with the output.
-- The signals verified are sop, eop, valid, sync, re, and im.
-- The example below shows what the expected outputs are based on the default TB generics and the following input.
-- The 6 rows in the table represent the c_nof_inputs = 6 input streams. (A,B), (C,D), etc. represent the
-- g_nof_ch_sel_col = 2 nof sequential collums to select per row.
--
-- Input:
--   | 0| 1| 2| 3| ...| 32| 33| ...|1022|1023|
--  -+-----------------------------------------
--  0|A0|B0|A1|B1| ...|A16|B16| ...|A511|B511|
--  -|--+--+--+--+----+---+---+----+----+----+
--  1|C0|D0|C1|D1| ...|C16|D16| ...|C511|D511|
--  -|--+--+--+--+----+---+---+----+----+----+
--  2|E0|F0|E1|F1| ...|E16|F16| ...|E511|F511|
--  -|--+--+--+--+----+---+---+----+----+----+
--  3|G0|H0|G1|H1| ...|G16|H16| ...|G511|H511|
--  -|--+--+--+--+----+---+---+----+----+----+
--  4|I0|J0|I1|J1| ...|I16|J16| ...|I511|J511|
--  -|--+--+--+--+----+---+---+----+----+----+
--  5|K0|L0|K1|L1| ...|K16|L16| ...|K511|L511|
--
-- Expected output for each of the g_nof_outputs = 2 output streams in the first sync intercal. You can see that
-- all "0" and all "16" elements are selected from the inputs which is configured by g_ch_sel_offset = (0, 16).
-- The next sync intervals, the offset selection will be (0+i*k, 16+i*k) where i is the sync interval and k is
-- g_ch_sel_step = 3.
-- |A0 B0 C0 D0 E0 F0 G0 H0 I0 J0 K0 L0 A16 B16 C16 D16 E16 F16 G16 H16 I16 J16 K16 L16|

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_reorder_col_wide_row_select is
  generic(
    g_nof_inputs                   : natural := 6;  -- also nof rows
    g_nof_outputs                  : natural := 2;
    g_dsp_data_w                   : natural := 16;  -- complex data width, = c_data_w / 2
    g_nof_sync                     : natural := 5;
    g_nof_block_per_sync           : natural := 4;
    g_nof_ch_in                    : natural := 1024;  -- nof input words per block, identical for all input streams.
    g_nof_ch_sel_col               : natural := 2;  -- nof of sequential collums to select per row.
    g_nof_ch_sel_offset            : natural := 2;  -- nof offsets defined
    g_ch_sel_offsets               : t_natural_arr := (0, 16);
    g_ch_sel_step                  : natural := 3;  -- offset step size to increase per sync interval
    g_reorder_row_select_pipe_in   : natural := 1;
    g_reorder_row_select_pipe_in_m : natural := 1;
    g_reorder_row_select_pipe_out  : natural := 1
  );
end tb_reorder_col_wide_row_select;

architecture tb of tb_reorder_col_wide_row_select is
  constant c_use_complex          : boolean := true;
  constant c_data_w               : natural := c_nof_complex * g_dsp_data_w;

  constant c_clk_period           : time := 10 ns;

  constant c_rl                   : natural := 1;
  constant c_nof_ch_sel_row       : natural := g_nof_inputs;
  constant c_nof_ch_sel           : natural := g_nof_ch_sel_offset * g_nof_ch_sel_col * c_nof_ch_sel_row;
  constant c_nof_inputs_w         : natural := ceil_log2(g_nof_inputs);
  constant c_in_select_w          : natural := g_nof_outputs * c_nof_inputs_w;
  constant c_in_select_dly        : natural := 1;
  constant c_ch_sel_offsets       : t_natural_arr(0 to g_nof_ch_sel_offset - 1) := g_ch_sel_offsets;

  signal rst             : std_logic;
  signal clk             : std_logic := '1';
  signal tb_end          : std_logic;

  signal mm_mosi         : t_mem_mosi;
  signal mm_miso         : t_mem_miso;

  signal st_en           : std_logic := '1';
  signal st_siso_arr     : t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);
  signal st_sosi_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal exp_sosi_arr    : t_dp_sosi_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal exp_siso_arr    : t_dp_siso_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_siso_rdy);

  signal bsn             : natural := 10;

  signal in_sosi_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr    : t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);
  signal col_wide_select_sosi_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal out_sosi_arr        : t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
  signal dly_out_sosi_arr    : t_dp_sosi_arr(g_nof_outputs - 1 downto 0);

  signal reorder_row_in_select : std_logic_vector(c_in_select_w - 1 downto 0) := (others => '0');
  signal in_select             : std_logic_vector(c_in_select_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  p_select_stimuli : process
  variable k : natural;
  begin
    for rep in 0 to g_nof_sync * g_nof_block_per_sync - 1 loop
      k := g_nof_ch_sel_col * (rep / g_nof_block_per_sync) * g_ch_sel_step;
      mm_mosi <= c_mem_mosi_rst;
      proc_common_wait_until_low(clk, mm_miso.waitrequest);
      for I in 0 to g_nof_ch_sel_offset - 1 loop
        for row in 0 to c_nof_ch_sel_row - 1 loop
          for col in 0 to g_nof_ch_sel_col - 1 loop
            for i_out in 0 to g_nof_outputs - 1 loop
              reorder_row_in_select((i_out + 1) * c_nof_inputs_w - 1 downto i_out * c_nof_inputs_w) <= TO_UVEC(row, c_nof_inputs_w);
            end loop;
            proc_mem_mm_bus_rd(c_ch_sel_offsets(I) + col + k, clk, mm_mosi);
          end loop;
        end loop;
      end loop;
    end loop;
    wait;
  end process;

  u_pipe_in_select : entity common_lib.common_pipeline
  generic map(
    g_pipeline => c_in_select_dly,
    g_in_dat_w => c_in_select_w,
    g_out_dat_w => c_in_select_w
  )
  port map(
    rst => rst,
    clk => clk,
    in_dat => reorder_row_in_select,
    out_dat => in_select
  );
  ------------------------------------------------------------------------------
  -- Data blocks
  ------------------------------------------------------------------------------
  gen_stimuli : for K in 0 to g_nof_inputs - 1 generate
    p_st_stimuli : process
      variable v_re  : natural := 0 + k * 2**5;
      variable v_im  : natural := 1 + k * 2**5;
    begin
      tb_end <= '0';
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);

      -- Run some sync intervals with DSP counter data for the real and imag fields
      wait until rising_edge(clk);
      for I in 0 to g_nof_sync - 1 loop
        proc_dp_gen_block_data(c_rl, false, g_dsp_data_w, g_dsp_data_w, 0, v_re, v_im, g_nof_ch_in, 0, 0, '1', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- next sync
        v_re := v_re + g_nof_ch_in;
        v_im := v_im + g_nof_ch_in;
        for J in 0 to g_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
          proc_dp_gen_block_data(c_rl, false, g_dsp_data_w, g_dsp_data_w, 0, v_re, v_im, g_nof_ch_in, 0, 0, '0', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- no sync
          v_re := v_re + g_nof_ch_in;
          v_im := v_im + g_nof_ch_in;
        end loop;
      end loop;
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_some_cycles(clk, g_nof_ch_in);
      proc_common_wait_some_cycles(clk, 10);
      tb_end <= '1';
      wait;
    end process;
  end generate;

  -- Time stimuli
  bsn <= bsn + 1 when rising_edge(clk) and (st_sosi_arr(0).eop = '1');  -- OR st_sosi.sync='1');

  -- Add BSN to the ST data
  p_in_sosi : process(st_sosi_arr, bsn)
  begin
    for I in 0 to g_nof_inputs - 1 loop
      in_sosi_arr(I)     <= st_sosi_arr(I);
      in_sosi_arr(I).bsn <= TO_DP_BSN(bsn);
    end loop;
  end process;

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  u_pipeline_arr : entity dp_lib.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_outputs
  )
  port map (
     rst => rst,
     clk => clk,

     snk_in_arr => out_sosi_arr,
     src_out_arr => dly_out_sosi_arr
  );

  gen_verify : for O in 0 to g_nof_outputs - 1 generate
    p_generate_exp_data : process
      variable v_col : natural := 0;
      variable v_row : natural := 0;
      variable v_offset : natural := 0;
      variable v_sync_ix : natural := 0;
      variable v_k : natural := 0;
    begin
      for I in 0 to g_nof_sync * g_nof_block_per_sync - 1 loop
        exp_sosi_arr(O) <= c_dp_sosi_rst;
        proc_common_wait_until_high(clk, out_sosi_arr(0).sop);
        for J in 0 to c_nof_ch_sel - 1 loop
          v_sync_ix := I / g_nof_block_per_sync;
          v_offset := J / (g_nof_ch_sel_col * c_nof_ch_sel_row);
          v_col := J mod g_nof_ch_sel_col;
          v_row := (J / g_nof_ch_sel_col) mod c_nof_ch_sel_row;
          v_k := g_nof_ch_sel_col * v_sync_ix * g_ch_sel_step;

          exp_sosi_arr(O) <= c_dp_sosi_rst;
          exp_sosi_arr(O).valid <= '1';
          if J = 0 then
            exp_sosi_arr(O).sop <= '1';
            if I mod g_nof_block_per_sync = 0 then
              exp_sosi_arr(O).sync <= '1';
            end if;
          elsif j = c_nof_ch_sel - 1 then
            exp_sosi_arr(O).eop <= '1';
          end if;

          exp_sosi_arr(O).re <= TO_DP_DSP_DATA(   I * g_nof_ch_in + v_k + c_ch_sel_offsets(v_offset) + v_col + v_row * 2**5);
          exp_sosi_arr(O).im <= TO_DP_DSP_DATA(1 + I * g_nof_ch_in + v_k + c_ch_sel_offsets(v_offset) + v_col + v_row * 2**5);
          proc_common_wait_some_cycles(clk, 1);
        end loop;
        exp_sosi_arr(O) <= c_dp_sosi_rst;
      end loop;
      wait;
    end process;

    p_verify_out_sosi : process(clk)
    begin
      if rising_edge(clk) then
        assert dly_out_sosi_arr(O).valid = exp_sosi_arr(O).valid
          report "Wrong out_sosi.valid"
          severity ERROR;
        assert dly_out_sosi_arr(O).sop = exp_sosi_arr(O).sop
          report "Wrong out_sosi.sop"
          severity ERROR;
        assert dly_out_sosi_arr(O).eop = exp_sosi_arr(O).eop
          report "Wrong out_sosi.eop"
          severity ERROR;
        assert dly_out_sosi_arr(O).sync = exp_sosi_arr(O).sync
          report "Wrong out_sosi.sync"
          severity ERROR;
        if exp_sosi_arr(O).valid = '1' then
          assert dly_out_sosi_arr(O).re = exp_sosi_arr(O).re
            report "Wrong out_sosi.re"
            severity ERROR;
          assert dly_out_sosi_arr(O).im = exp_sosi_arr(O).im
            report "Wrong out_sosi.im"
            severity ERROR;
        end if;
      end if;
    end process;
  end generate;

  u_dut_col : entity work.reorder_col_wide_select
  generic map (
    g_nof_inputs         => g_nof_inputs,
    g_dsp_data_w         => g_dsp_data_w,
    g_nof_ch_in          => g_nof_ch_in,
    g_nof_ch_sel         => c_nof_ch_sel,
    g_use_complex        => c_use_complex
  )
  port map (
    dp_rst         => rst,
    dp_clk         => clk,

    -- Memory Mapped
    col_select_mosi    => mm_mosi,
    col_select_miso    => mm_miso,

    -- Streaming
    input_sosi_arr     => in_sosi_arr,
    output_sosi_arr    => col_wide_select_sosi_arr
  );

  u_dut_row : entity work.reorder_row_select
  generic map (
    g_dsp_data_w         => g_dsp_data_w,
    g_nof_inputs         => g_nof_inputs,
    g_nof_outputs        => g_nof_outputs,
    g_pipeline_in        => g_reorder_row_select_pipe_in,
    g_pipeline_in_m      => g_reorder_row_select_pipe_in_m,
    g_pipeline_out       => g_reorder_row_select_pipe_out
  )
  port map (
    dp_rst         => rst,
    dp_clk         => clk,

    in_select      => in_select,

    -- Streaming
    input_sosi_arr     => col_wide_select_sosi_arr,
    output_sosi_arr    => out_sosi_arr
  );
end tb;
