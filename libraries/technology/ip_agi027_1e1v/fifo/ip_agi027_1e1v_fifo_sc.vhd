-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author: D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper / Instantiate FIFO IP with generics
-- Description:
--   Copied component declaration and instance example from generated/fifo_1921/sim/ip_agi027_1e1v_fifo_sc_fifo_1921_7egef6q.vhd

library ieee;
use ieee.std_logic_1164.all;

library technology_lib;
use technology_lib.technology_pkg.all;

library altera_mf;
use altera_mf.all;

entity ip_agi027_1e1v_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
end ip_agi027_1e1v_fifo_sc;

architecture SYN of ip_agi027_1e1v_fifo_sc is
  component  scfifo
  generic (
      add_ram_output_register  : string;
      -- enable_ecc  : string;
      intended_device_family  : string;
      lpm_numwords  : natural;
      lpm_showahead  : string;
      lpm_type  : string;
      lpm_width  : natural;
      lpm_widthu  : natural;
      overflow_checking  : string;
      underflow_checking  : string;
      use_eab  : string
  );
  port (
      aclr   : in std_logic;
      clock   : in std_logic;
      data   : in std_logic_vector(g_dat_w - 1 downto 0);
      rdreq   : in std_logic;
      wrreq   : in std_logic;
      empty   : out std_logic;
      full   : out std_logic;
      q   : out std_logic_vector(g_dat_w - 1 downto 0);
      usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;
begin
  u_scfifo : scfifo
  generic map (
    add_ram_output_register  => "ON",
    -- enable_ecc  => "FALSE",
    intended_device_family  => "Agilex 7",
    lpm_numwords  => g_nof_words,
    lpm_showahead  => "OFF",
    lpm_type  => "scfifo",
    lpm_width  => g_dat_w,
    lpm_widthu  => tech_ceil_log2(g_nof_words),
    overflow_checking  => "ON",
    underflow_checking  => "ON",
    use_eab  => g_use_eab
  )
  port map (
    aclr => aclr,
    clock => clock,
    data => data,
    rdreq => rdreq,
    wrreq => wrreq,
    empty => empty,
    full => full,
    q => q,
    usedw => usedw
  );
end SYN;
