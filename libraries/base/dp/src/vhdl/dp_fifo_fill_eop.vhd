-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle, E. Kooistra

-- Purpose:
--   The FIFO starts outputting data when the output is ready and it has been
--   filled with more than g_fifo_fill words or an eop signal has been received.
--   This is useful when the in_val is throttled while the out_val should not be
--   inactive valid between out_sop to out_eop. This is necessary for frame
--   transport over a PHY link without separate data valid signal.
-- Description:
--   Modified version of dp_fifo_fill_core. In addition to a frame being available
--   after the fifo has been filled sufficiently, a frame is also available when
--   the in_eop has been received earlier than the specified g_fifo_fill. For
--   more details, please consult the description of dp_fill_fifo_core.
-- Usage:
-- * Typically rely only on eop to release any block from the FIFO. This can
--   be achieved by choosing:
--   . g_fifo_size >= largest block size + c_fifo_fill_margin.
--   . g_fifo_fill = largest block size, or equivalently choose:
--     g_fifo_fill = g_fifo_size - c_fifo_fill_margin
--   It is not adviced to choose g_fifo_fill = g_fifo_size, because then
--   internally the FIFO becomes c_fifo_fill_margin bigger and may then use
--   more RAM. Default c_fifo_fill_margin = 4 + 2 = 6 words.
-- * Often it is possible to make full use of a block RAM by using a FIFO size
--   that is a power of 2, so g_fifo_size = true_log_pow2(largest block size +
--   c_fifo_tx_fill_margin).
-- Remark:
-- * The dp_fifo_fill_eop can handle continues stream of blocks without a
--   gap between blocks, as shown by tb_dp_fifo_fill_eop.vhd, because
--   snk_out.ready remains active and the FIFO does not run full. This was
--   fixed by Reinier in Nov 2021 as part of L2SDP-505.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_fill_eop is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_note_is_ful    : boolean := true;
    g_use_dual_clock : boolean := false;
    g_data_w         : natural := 16;  -- Should be 2 times the c_complex_w if g_use_complex = TRUE
    g_data_signed    : boolean := false;  -- TRUE extends g_data_w bits with the sign bit, FALSE pads g_data_w bits with zeros.
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_fill      : natural := 0;
    g_fifo_size      : natural := 256;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1  -- use RL=0 for internal show ahead FIFO, default use RL=1 for internal normal FIFO
  );
  port (
    wr_rst      : in  std_logic;
    wr_clk      : in  std_logic;
    rd_rst      : in  std_logic;
    rd_clk      : in  std_logic;
    -- Monitor FIFO filling
    wr_ful      : out std_logic;  -- corresponds to the carry bit of wr_usedw when FIFO is full
    wr_usedw    : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_usedw    : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_emp      : out std_logic;
    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = wr_usedw
    rd_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = rd_usedw
    rd_fill_32b  : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_fifo_fill, c_word_w);
    -- ST sink
    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_in      : in  t_dp_siso := c_dp_siso_rdy;
    src_out     : out t_dp_sosi
  );
end dp_fifo_fill_eop;

architecture rtl of dp_fifo_fill_eop is
  constant c_fifo_rl          : natural := sel_a_b(g_fifo_fill = 0, 1, g_fifo_rl);
  constant c_fifo_fill_margin : natural := g_fifo_af_margin + 2;  -- add +2 extra margin, with tb_dp_fifo_fill it follows that +1 is also enough to avoid almost full when fifo is operating near g_fifo_fill level
  constant c_fifo_size        : natural := largest(g_fifo_size, g_fifo_fill + c_fifo_fill_margin);
  constant c_fifo_size_w      : natural := ceil_log2(c_fifo_size);  -- = wr_usedw'LENGTH = rd_usedw'LENGTH

  -- The FIFO filling relies on framed data, so contrary to dp_fifo_sc the sop and eop need to be used.
  constant c_use_ctrl  : boolean := true;

  -- Define t_state as slv to avoid Modelsim warning "Nonresolved signal 'nxt_state' may have multiple sources". Due to that g_fifo_rl = 0 or 1 ar both supported.
  type t_state is (s_fill, s_output, s_xoff);

  constant c_nof_spulse : natural := 3;

  signal state       : t_state;
  signal nxt_state   : t_state;

  signal xon_reg     : std_logic;
  signal nxt_xon_reg : std_logic;

  signal rd_siso     : t_dp_siso;
  signal rd_sosi     : t_dp_sosi := c_dp_sosi_rst;  -- initialize default values for unused sosi fields;

  signal wr_fifo_usedw  : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- = wr_usedw'RANGE
  signal rd_fifo_usedw  : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- = rd_usedw'RANGE
  signal rd_fill_ctrl   : std_logic_vector(c_fifo_size_w - 1 downto 0);  -- used to resize rd_fill_32b to actual maximum width

  signal i_src_out   : t_dp_sosi;
  signal nxt_src_out : t_dp_sosi;

  -- EOP clock domain crossing signals
  signal reg_wr_eop_cnt   : std_logic_vector(c_word_w - 1 downto 0);
  signal reg_rd_eop_cnt   : std_logic_vector(c_word_w - 1 downto 0);
  signal wr_eop_done      : std_logic;
  signal wr_eop_new       : std_logic;
  signal nxt_wr_eop_new   : std_logic;
  signal rd_eop_new       : std_logic;
  signal wr_eop_busy      : std_logic;
  signal nxt_wr_eop_busy  : std_logic;
  signal wr_eop_cnt       : natural := 0;
  signal nxt_wr_eop_cnt   : natural := 0;
  signal rd_eop_cnt       : natural := 0;

  -- EOP count can be negative when a packet is sent out without having received the eop. This can be the case when g_fifo_fill has been reached.
  signal eop_cnt          : integer := 0;
  signal nxt_eop_cnt      : integer := 0;

  -- Signals for g_fifo_rl=1
  signal hold_src_in  : t_dp_siso;
  signal pend_src_out : t_dp_sosi;
begin
  -- Output monitor FIFO filling
  wr_usedw <= wr_fifo_usedw;
  rd_usedw <= rd_fifo_usedw;

  -- Control FIFO fill level
  wr_usedw_32b <= RESIZE_UVEC(wr_fifo_usedw, c_word_w);
  rd_usedw_32b <= RESIZE_UVEC(rd_fifo_usedw, c_word_w);
  rd_fill_ctrl <= rd_fill_32b(c_fifo_size_w - 1 downto 0);

  u_dp_fifo_core : entity work.dp_fifo_core
  generic map (
    g_technology     => g_technology,
    g_note_is_ful    => g_note_is_ful,
    g_use_dual_clock => g_use_dual_clock,
    g_data_w         => g_data_w,
    g_data_signed    => g_data_signed,
    g_bsn_w          => g_bsn_w,
    g_empty_w        => g_empty_w,
    g_channel_w      => g_channel_w,
    g_error_w        => g_error_w,
    g_use_bsn        => g_use_bsn,
    g_use_empty      => g_use_empty,
    g_use_channel    => g_use_channel,
    g_use_error      => g_use_error,
    g_use_sync       => g_use_sync,
    g_use_ctrl       => c_use_ctrl,
    g_use_complex    => g_use_complex,
    g_fifo_size      => c_fifo_size,
    g_fifo_af_margin => g_fifo_af_margin,
    g_fifo_af_xon    => g_fifo_af_xon,
    g_fifo_rl        => c_fifo_rl
  )
  port map (
    wr_rst      => wr_rst,
    wr_clk      => wr_clk,
    rd_rst      => rd_rst,
    rd_clk      => rd_clk,
    -- Monitor FIFO filling
    wr_ful      => wr_ful,
    wr_usedw    => wr_fifo_usedw,
    rd_usedw    => rd_fifo_usedw,
    rd_emp      => rd_emp,
    -- ST sink
    snk_out     => snk_out,
    snk_in      => snk_in,
    -- ST source
    src_in      => rd_siso,  -- for RL = 0 rd_siso.ready acts as read acknowledge, -- for RL = 1 rd_siso.ready acts as read request
    src_out     => rd_sosi
  );

  -- Transfer eop counter across clock domains for dual clock
  gen_rd_eop_cnt_dc : if g_use_dual_clock = true generate
    reg_wr_eop_cnt <= TO_UVEC(wr_eop_cnt, c_word_w);
    u_common_reg_cross_domain : entity common_lib.common_reg_cross_domain
    port map (
      in_rst  => wr_rst,
      in_clk  => wr_clk,
      in_dat  => reg_wr_eop_cnt,
      in_new  => wr_eop_new,
      in_done => wr_eop_done,
      out_rst => rd_rst,
      out_clk => rd_clk,
      out_dat => reg_rd_eop_cnt,
      out_new => rd_eop_new
    );
  end generate;

  -- No need to transfer eop counter across clock domains for single clock
  gen_rd_eop_cnt_sc : if g_use_dual_clock = false generate
    rd_eop_new <= '1';
  end generate;

  -- Set rd_eop_cnt outside generate statements to avoid Modelsim warning "Nonresolved signal 'rd_eop_cnt' may have multiple sources".
  -- This is only the case with INTEGER (sub) types as it does not have a resolve function te decide the value in case of multiple sources
  -- through GENERATE statements. STD_LOGIC / STD_LOGIC VECTORS do have such a resolve function. Modelsim cannot resolve that the two GENERATE
  -- statements where g_use_dual_clock = FALSE / TRUE will never be active simultaneously as a GENERATE statement cannot have an ELSE statement.
  rd_eop_cnt <= TO_UINT(reg_rd_eop_cnt) when g_use_dual_clock else wr_eop_cnt;

  gen_dual_clk : if g_use_dual_clock = true generate
    p_eop_cnt_comb_dc: process(wr_eop_cnt, wr_eop_new, wr_eop_busy, wr_eop_done, snk_in)
      variable v_wr_eop_cnt: natural;
      variable v_wr_eop_new: std_logic;
      variable v_wr_eop_busy: std_logic;
    begin
      -- We need to control in_new signal for common_reg_cross_domain. We can simply pulse in_new after in_done = '1'.
      -- After we have send the wr_eop_cnt accross the clock domain by seting wr_eop_new, the wr_eop_cnt is reset to 0.
      -- It is not possible to set in_new = snk_in.eop as there can be more snk_in.eop during the clock cross time necessary by common_reg_cross_domain.
      v_wr_eop_new := wr_eop_new;
      v_wr_eop_busy := wr_eop_busy;
      v_wr_eop_cnt := wr_eop_cnt;

      -- When done = 1, busy can be set to 0.
      if wr_eop_done = '1' then
        v_wr_eop_busy := '0';
      end if;
      -- If common_reg_cross_domain is not busy transfering the register we can initiate a new transfer by setting wr_eop_new.
      if wr_eop_busy = '0' then
        v_wr_eop_busy := '1';
        v_wr_eop_new := '1';
      end if;

      -- After we transfered wr_eop_cnt, we can reset it to 0.
      if wr_eop_new = '1' then
        v_wr_eop_new := '0';
        v_wr_eop_cnt := 0;
      end if;

      -- Count incoming snk_in.eop
      if snk_in.eop = '1' then
        v_wr_eop_cnt := v_wr_eop_cnt + 1;
      end if;

      nxt_wr_eop_cnt <= v_wr_eop_cnt;
      nxt_wr_eop_new <= v_wr_eop_new;
      nxt_wr_eop_busy <= v_wr_eop_busy;
    end process;
  end generate;

  gen_single_clk : if g_use_dual_clock = false generate
    -- No need to transfer eop counter across clock domains for single clock
    p_eop_cnt_comb_sc: process(snk_in)
    begin
      if snk_in.eop = '1' then
        nxt_wr_eop_cnt <= 1;  -- wr_eop_cnt can simply be set to 1 instead of counting as it is immidiatly processed due to having a single clock.
      else
        nxt_wr_eop_cnt <= 0;
      end if;
    end process;
  end generate;

  p_eop_cnt_clk: process(wr_clk, wr_rst)
  begin
    if wr_rst = '1' then
      wr_eop_cnt  <= 0;
      wr_eop_busy <= '0';
      wr_eop_new  <= '0';
    elsif rising_edge(wr_clk) then
      wr_eop_cnt  <= nxt_wr_eop_cnt;
      wr_eop_busy <= nxt_wr_eop_busy;
      wr_eop_new  <= nxt_wr_eop_new;
    end if;
  end process;

  no_fill : if g_fifo_fill = 0 generate
    rd_siso <= src_in;  -- SISO
    src_out <= rd_sosi;  -- SOSI
  end generate;  -- no_fill

  gen_fill : if g_fifo_fill > 0 generate
    src_out <= i_src_out;

    p_rd_clk: process(rd_clk, rd_rst)
    begin
      if rd_rst = '1' then
        xon_reg   <= '0';
        state     <= s_fill;
        i_src_out <= c_dp_sosi_rst;
        eop_cnt   <= 0;
      elsif rising_edge(rd_clk) then
        xon_reg   <= nxt_xon_reg;
        state     <= nxt_state;
        i_src_out <= nxt_src_out;
        eop_cnt   <= nxt_eop_cnt;
      end if;
    end process;

    nxt_xon_reg <= src_in.xon;  -- register xon to easy timing closure

    gen_rl_1 : if g_fifo_rl = 1 generate
      -- Use dp_hold_input to get equivalent implementation with default RL=1 FIFO.

      -- Hold the sink input for source output
      u_snk : entity work.dp_hold_input
      port map (
        rst          => rd_rst,
        clk          => rd_clk,
        -- ST sink
        snk_out      => rd_siso,  -- SISO ready
        snk_in       => rd_sosi,  -- SOSI
        -- ST source
        src_in       => hold_src_in,  -- SISO ready
        next_src_out => OPEN,  -- SOSI
        pend_src_out => pend_src_out,
        src_out_reg  => i_src_out
      );
    end generate;

    gen_rl_0 : if g_fifo_rl = 0 generate
      pend_src_out <= rd_sosi;
      rd_siso <= hold_src_in;
    end generate;

    p_state : process(state, src_in, xon_reg, pend_src_out, rd_fifo_usedw, rd_fill_ctrl, rd_eop_cnt, eop_cnt, rd_eop_new)
    begin
      nxt_state <= state;

      hold_src_in <= src_in;  -- default request (RL=1) new input when output is ready

      -- The output register stage matches RL = 1 for src_in.ready
      nxt_src_out       <= pend_src_out;
      nxt_src_out.valid <= '0';  -- default no output
      nxt_src_out.sop   <= '0';
      nxt_src_out.eop   <= '0';
      nxt_src_out.sync  <= '0';
      nxt_eop_cnt       <= eop_cnt;
      if rd_eop_new = '1' then
        nxt_eop_cnt <= eop_cnt + rd_eop_cnt;
      end if;

      case state is
        when s_fill =>
          if xon_reg = '0' then
            nxt_state <= s_xoff;
          else
            -- read the FIFO until the sop is pending at the output, so discard any valid data between eop and sop
            if pend_src_out.sop = '0' then
              hold_src_in <= c_dp_siso_rdy;  -- request (RL=1) new input independent of output ready
            else
              if unsigned(rd_fifo_usedw) < unsigned(rd_fill_ctrl) and eop_cnt <= 0 then
                hold_src_in <= c_dp_siso_hold;  -- stop the input, hold the pend_src_out.sop
              else
                -- if the output is ready, then start outputting the input frame
                if src_in.ready = '1' then
                  nxt_src_out <= pend_src_out;  -- output sop that is still pending in dp_hold_input
                  nxt_state <= s_output;
                  if rd_eop_new = '1' then
                    nxt_eop_cnt <= eop_cnt + rd_eop_cnt - 1;
                  else
                    nxt_eop_cnt <= eop_cnt - 1;
                  end if;
                end if;
              end if;
            end if;
          end if;
        when s_output =>
          -- if the output is ready continue outputting the input frame, ignore xon_reg during this frame
          if src_in.ready = '1' then
            nxt_src_out <= pend_src_out;  -- output valid
            if pend_src_out.eop = '1' then
              nxt_state <= s_fill;  -- output eop, so stop reading the FIFO
            end if;
          end if;
        when others =>  -- s_xoff
          -- Flush the fill FIFO when xon='0'
          hold_src_in <= c_dp_siso_flush;
          if xon_reg = '1' then
            nxt_state <= s_fill;
          end if;
      end case;

      -- Pass on frame level flow control
      hold_src_in.xon <= src_in.xon;
    end process;
  end generate;  -- gen_fill
end rtl;
