-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_st_acc is
  generic (
    g_dat_w            : natural := 6;
    g_acc_w            : natural := 9;
    g_hold_load        : boolean := true;
    g_pipeline_input   : natural := 0;
    g_pipeline_output  : natural := 4
  );
end tb_st_acc;

architecture tb of tb_st_acc is
  constant clk_period    : time := 10 ns;

  constant c_pipeline    : natural := g_pipeline_input + g_pipeline_output;

  function func_acc(in_dat, in_acc  : std_logic_vector;
                    in_val, in_load : std_logic) return std_logic_vector is
    variable v_dat, v_acc, v_result : integer;
  begin
    -- Calculate expected result
    if in_val = '0' then  -- hold: out_acc = in_acc
      v_result := TO_SINT(in_acc);
    elsif in_load = '1' then  -- force: out_acc = 0 + in_dat
      v_result := TO_SINT(in_dat);
    else  -- accumulate: out_acc = in_acc + in_dat
      v_result := TO_SINT(in_dat) + TO_SINT(in_acc);
    end if;
    -- Wrap to avoid warning: NUMERIC_STD.TO_SIGNED: vector truncated
    if v_result >  2**(g_acc_w - 1) - 1 then v_result := v_result - 2**g_acc_w; end if;
    if v_result < - 2**(g_acc_w - 1)   then v_result := v_result + 2**g_acc_w; end if;
    return TO_SVEC(v_result, g_acc_w);
  end;

  signal tb_end          : std_logic := '0';
  signal clk             : std_logic := '0';

  signal in_dat          : std_logic_vector(g_dat_w - 1 downto 0);
  signal in_acc          : std_logic_vector(g_acc_w - 1 downto 0) := (others => '0');
  signal in_val          : std_logic;
  signal in_load         : std_logic;
  signal out_val         : std_logic;
  signal out_acc         : std_logic_vector(g_acc_w - 1 downto 0);

  signal expected_acc_p  : std_logic_vector(g_acc_w - 1 downto 0);
  signal expected_acc    : std_logic_vector(g_acc_w - 1 downto 0);
begin
  clk  <= not clk or tb_end after clk_period / 2;

  ------------------------------------------------------------------------------
  -- Input stimuli
  ------------------------------------------------------------------------------

  -- run -all
  p_stimuli : process
  begin
    in_load <= '0';
    in_dat <= TO_SVEC(0, g_dat_w);
    in_val <= '0';
    wait until rising_edge(clk);
    for I in 0 to 9 loop wait until rising_edge(clk); end loop;

    in_load <= '1';
    in_val <= '1';
    for R in 0 to 2 loop  -- Repeat some intervals marked by in_load = '1'
      in_load <= '1';
      -- All combinations
      for I in - 2**(g_dat_w - 1) to 2**(g_dat_w - 1) - 1 loop
        in_dat <= TO_SVEC(I, g_dat_w);
        wait until rising_edge(clk);
        -- keep in_load low during rest of period
        in_load <= '0';
--         -- keep in_val low during rest of st_acc latency, to ease manual interpretation of out_acc as in_acc
--         in_val <= '0';
--         FOR J IN 1 TO c_pipeline-1 LOOP WAIT UNTIL rising_edge(clk); END LOOP;
--         in_val <= '1';
      end loop;
    end loop;
    in_load <= '1';  -- keep '1' to avoid further toggling of out_acc (in a real design this would safe power)
    in_val <= '0';
    for I in 0 to 9 loop wait until rising_edge(clk); end loop;
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  dut : entity work.st_acc
  generic map (
    g_dat_w            => g_dat_w,
    g_acc_w            => g_acc_w,
    g_hold_load        => g_hold_load,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_output  => g_pipeline_output
  )
  port map (
    clk        => clk,
    clken      => '1',
    in_load    => in_load,  -- start of accumulate period
    in_dat     => in_dat,
    in_acc     => in_acc,  -- use only one accumulator
    in_val     => in_val,
    out_acc    => out_acc,
    out_val    => out_val
  );

  in_acc <= out_acc when c_pipeline > 0 else
            out_acc when rising_edge(clk);  -- if DUT has no pipeline, then register feedback to avoid combinatorial loop

  ------------------------------------------------------------------------------
  -- Verify
  ------------------------------------------------------------------------------

  expected_acc <= func_acc(in_dat, in_acc, in_val, in_load);

  u_result : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_acc_w,
    g_out_dat_w      => g_acc_w
  )
  port map (
    clk     => clk,
    clken   => '1',
    in_dat  => expected_acc,
    out_dat => expected_acc_p
  );

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if out_val = '1' then
        assert out_acc = expected_acc_p
          report "Error: wrong result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
