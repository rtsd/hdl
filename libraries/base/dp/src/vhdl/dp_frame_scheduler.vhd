--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Reuse from LOFAR rad_frame_scheduler.vhd and rad_frame_scheduler(rtl).vhd

-- Purpose:
--   Schedule one or more input frames streams into one output stream.
-- Description:
--   The input frame that arrives first is transmitted. Then if the next input
--   has a frame pending then that is transmitted.
--   The scheduler starts transmitting data from a FIFO if it has been filled
--   with more than g_fifo_fill words. Given a fixed frame length, this is
--   useful when the in_val is throttled while the out_val should not be
--   inactive valid between out_sof to out_eof.
--   The required g_fifo_size per input depends on the relative frame rates,
--   typically it must be sufficient to fit at least two frames.
-- Remark:
-- . Using g_nof_input=1 is useful to have an input FIFO that can be filled
--   sufficiently to level set by g_fifo_fill to ensure that the output frame
--   has data valid without gaps.
-- . The inputs can dynamically be disabled or enabled via in_dis. The
--   dp_xonoff ensures that in_dis is applied during entire frames and before
--   they are written into the input FIFO.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_frame_scheduler is
  generic (
    g_technology : natural := c_tech_select_default;
    g_dat_w      : natural := 16;
    g_nof_input  : natural := 2;  -- >= 1
    g_fifo_rl    : natural := 1;  -- for all input use 0 for look ahead FIFO, 1 for normal FIFO
    g_fifo_size  : t_natural_arr := (1024, 1024);  -- must match g_nof_input-1 DOWNTO 0
    g_fifo_fill  : t_natural_arr := (0, 0)  -- must match g_nof_input-1 DOWNTO 0
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    gp_out   : out std_logic_vector(g_nof_input        - 1 downto 0);
    in_dat   : in  std_logic_vector(g_nof_input * g_dat_w - 1 downto 0);
    in_val   : in  std_logic_vector(g_nof_input        - 1 downto 0);
    in_sof   : in  std_logic_vector(g_nof_input        - 1 downto 0);
    in_eof   : in  std_logic_vector(g_nof_input        - 1 downto 0);
    in_dis   : in  std_logic_vector(g_nof_input        - 1 downto 0) := (others => '0');  -- optional input disable control at frame level
    out_dat  : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val  : out std_logic;
    out_sof  : out std_logic;
    out_eof  : out std_logic
  );
end dp_frame_scheduler;

architecture rtl of dp_frame_scheduler is
  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg       : boolean := false;

  -- Convert unconstrained range (that starts at INTEGER'LEFT) to g_nof_input-1 DOWNTO 0 range
  constant c_fifo_size       : t_natural_arr(g_nof_input - 1 downto 0) := g_fifo_size;
  constant c_fifo_fill       : t_natural_arr(g_nof_input - 1 downto 0) := g_fifo_fill;

  type t_dat_arr        is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);

  signal dbg_c_fifo_size : t_natural_arr(c_fifo_size'range) := c_fifo_size;
  signal dbg_c_fifo_fill : t_natural_arr(c_fifo_fill'range) := c_fifo_fill;

  -- Registered inputs (see c_input_reg)
  signal in_dat_reg      : t_dat_arr(0 to g_nof_input - 1);
  signal in_val_reg      : t_sl_arr( 0 to g_nof_input - 1);
  signal in_sof_reg      : t_sl_arr( 0 to g_nof_input - 1);
  signal in_eof_reg      : t_sl_arr( 0 to g_nof_input - 1);
  signal snk_in          : t_dp_sosi_arr(0 to g_nof_input - 1);

  signal wr_siso         : t_dp_siso_arr(0 to g_nof_input - 1);
  signal wr_sosi         : t_dp_sosi_arr(0 to g_nof_input - 1);

  signal rd_req          : t_sl_arr( 0 to g_nof_input - 1);
  signal rd_dat          : t_dat_arr(0 to g_nof_input - 1);
  signal rd_val          : t_sl_arr( 0 to g_nof_input - 1);
  signal rd_sof          : t_sl_arr( 0 to g_nof_input - 1);
  signal rd_eof          : t_sl_arr( 0 to g_nof_input - 1);
  signal rd_siso         : t_dp_siso_arr(0 to g_nof_input - 1);
  signal rd_sosi         : t_dp_sosi_arr(0 to g_nof_input - 1);

  type state_type is (s_idle, s_output);

  signal state           : state_type;
  signal nxt_state       : state_type;

  signal in_sel          : natural range 0 to g_nof_input - 1;
  signal nxt_in_sel      : natural;

  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sof     : std_logic;
  signal nxt_out_eof     : std_logic;
begin
  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_dat_reg <= (others => (others => '0'));
        in_val_reg <= (others => '0');
        in_sof_reg <= (others => '0');
        in_eof_reg <= (others => '0');
      elsif rising_edge(clk) then
        for I in 0 to g_nof_input - 1 loop
          in_dat_reg(I) <= in_dat((I + 1) * g_dat_w - 1 downto I * g_dat_w);
          in_val_reg(I) <= in_val(                       I);
          in_sof_reg(I) <= in_sof(                       I);
          in_eof_reg(I) <= in_eof(                       I);
        end loop;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    p_in_reg : process(in_dat, in_val, in_sof, in_eof)
    begin
      for I in 0 to g_nof_input - 1 loop
        in_dat_reg(I) <= in_dat((I + 1) * g_dat_w - 1 downto I * g_dat_w);
        in_val_reg(I) <= in_val(                       I);
        in_sof_reg(I) <= in_sof(                       I);
        in_eof_reg(I) <= in_eof(                       I);
      end loop;
    end process;
  end generate;

  gen_fifo : for I in 0 to g_nof_input - 1 generate
    snk_in(I).data(g_dat_w - 1 downto 0) <= in_dat_reg(I);
    snk_in(I).valid                    <= in_val_reg(I);
    snk_in(I).sop                      <= in_sof_reg(I);
    snk_in(I).eop                      <= in_eof_reg(I);

    -- Input disable control at frame level
    wr_siso(I).ready <= '1';
    wr_siso(I).xon   <= not in_dis(I);

    u_xonoff : entity work.dp_xonoff
    port map (
      rst           => rst,
      clk           => clk,
      -- Frame in
      in_siso       => OPEN,
      in_sosi       => snk_in(I),
      -- Frame out
      out_siso      => wr_siso(I),  -- flush control via xon, ready is not used and only passed on
      out_sosi      => wr_sosi(I)
    );

    -- Input FIFO
    rd_siso(I).ready <= rd_req(I);

    rd_dat(I) <= rd_sosi(I).data(g_dat_w - 1 downto 0);
    rd_val(I) <= rd_sosi(I).valid;
    rd_sof(I) <= rd_sosi(I).sop;
    rd_eof(I) <= rd_sosi(I).eop;

    u_fill : entity work.dp_fifo_fill
    generic map (
      g_technology  => g_technology,
      g_data_w      => g_dat_w,
      g_empty_w     => 1,
      g_channel_w   => 1,
      g_error_w     => 1,
      g_use_empty   => false,
      g_use_channel => false,
      g_use_error   => false,
      g_fifo_fill   => c_fifo_fill(I),
      g_fifo_size   => c_fifo_size(I),
      g_fifo_rl     => g_fifo_rl
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sink
      snk_out     => OPEN,  -- OUT = request to upstream ST source
      snk_in      => wr_sosi(I),
      -- ST source
      src_in      => rd_siso(I),  -- IN  = request from downstream ST sink
      src_out     => rd_sosi(I),

      wr_ful      => gp_out(I)
    );
  end generate;

  -- Output select
  nxt_out_dat <= rd_dat(in_sel);
  nxt_out_val <= rd_val(in_sel);
  nxt_out_sof <= rd_sof(in_sel);
  nxt_out_eof <= rd_eof(in_sel);

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      in_sel     <= 0;
      state      <= s_idle;
      out_dat    <= (others => '0');
      out_val    <= '0';
      out_sof    <= '0';
      out_eof    <= '0';
    elsif rising_edge(clk) then
      in_sel     <= nxt_in_sel;
      state      <= nxt_state;
      out_dat    <= nxt_out_dat;
      out_val    <= nxt_out_val;
      out_sof    <= nxt_out_sof;
      out_eof    <= nxt_out_eof;
    end if;
  end process;

  p_state : process(state, in_sel, rd_sof, rd_eof)
  begin
    rd_req      <= (others => '0');
    nxt_in_sel  <= in_sel;
    nxt_state   <= state;

    case state is
      when s_idle =>
        if rd_sof(in_sel) = '1' then
          -- if the sof is there, then start outputting the frame
          rd_req(in_sel) <= '1';
          nxt_state <= s_output;
        else
          -- else select next input
          if in_sel = g_nof_input - 1 then
            nxt_in_sel       <= 0;
            rd_req(0)        <= '1';
          else
            nxt_in_sel       <= in_sel + 1;
            rd_req(in_sel + 1) <= '1';
          end if;
        end if;
      when others =>  -- s_output
        -- output the frame
        rd_req(in_sel) <= '1';
        if rd_eof(in_sel) = '1' then
          rd_req(in_sel) <= '0';  -- eof so stop reading the FIFO, this also prevents reading an immediate next rd_sof for this in_sel to allow selecting next input in s_idle
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
end rtl;
