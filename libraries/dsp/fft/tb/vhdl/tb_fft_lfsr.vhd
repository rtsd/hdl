-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Verify fft_lsfr for different seeds
-- Description:
--   Check that after c_fft_lfsr_len = 41 blocks the LFSR1 and LFSR2 bits of
--   the two instances u0 and u1 indeed start to differ.
--
-- Usage:
--   > as 4
--   > run -all
--   # Not self checking, manually compare that u0_lfsr_bit1 and u1_lfsr_bit1
--     start to differ after about c_fft_lfsr_len blocks. Similar for
--     u0_lfsr_bit1 and u1_lfsr_bit1.
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.fft_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_fft_lfsr is
end tb_fft_lfsr;

architecture tb of tb_fft_lfsr is
  constant clk_period      : time := 10 ns;

  constant c_block_period  : natural := 10;
  constant c_nof_block     : natural := 1000;

  signal tb_end            : std_logic := '0';
  signal rst               : std_logic := '1';
  signal clk               : std_logic := '0';

  signal in_en             : std_logic := '0';
  signal u0_lfsr_bit1      : std_logic;
  signal u1_lfsr_bit1      : std_logic;
  signal u0_lfsr_bit2      : std_logic;
  signal u1_lfsr_bit2      : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 10 * clk_period;
  tb_end <= '0', '1' after c_nof_block * c_block_period * clk_period;

  proc_common_gen_pulse(1, c_block_period, '1', rst, clk, in_en);

  u0 : entity work.fft_lfsr
  generic map (
    g_seed1 => fft_switch_new_seed(c_fft_switch_seed1, 0),
    g_seed2 => fft_switch_new_seed(c_fft_switch_seed2, 0)
  )
  port map (
    in_en          => in_en,
    out_bit1       => u0_lfsr_bit1,
    out_bit2       => u0_lfsr_bit2,
    clk            => clk,
    rst            => rst
  );

  u1 : entity work.fft_lfsr
  generic map (
    g_seed1 => fft_switch_new_seed(c_fft_switch_seed1, 1),
    g_seed2 => fft_switch_new_seed(c_fft_switch_seed2, 1)
  )
  port map (
    in_en          => in_en,
    out_bit1       => u1_lfsr_bit1,
    out_bit2       => u1_lfsr_bit2,
    clk            => clk,
    rst            => rst
  );
end tb;
