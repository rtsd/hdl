-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_flank_to_pulse is
end tb_common_flank_to_pulse;

architecture tb of tb_common_flank_to_pulse is
  constant clk_period   : time := 10 ns;

  signal rst    : std_logic;
  signal clk    : std_logic := '0';
  signal flank_in : std_logic;
  signal pulse_out : std_logic;
begin
  clk  <= not clk  after clk_period / 2;

  p_in_stimuli : process
  begin
    rst <= '1';
    flank_in <= '0';
    wait until rising_edge(clk);
    rst <= '0';
    wait for 200 ns;
    flank_in <= '1';
    wait for 200 ns;
    flank_in <= '0';
    wait for 200 ns;
    flank_in <= '1';
    wait for 200 ns;
    flank_in <= '0';
    wait;
  end process;

  u_dut: entity work.common_flank_to_pulse
  port map (
    clk     => clk,
    rst     => rst,
    flank_in => flank_in,
    pulse_out => pulse_out
  );
end tb;
