-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

entity tb_sens is
end tb_sens;

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

architecture tb of tb_sens is
  constant c_clk_period          : time    := 5 ns;
  constant c_rst_period          : time    := 4 * c_clk_period;

  constant c_bus_dat_w           : natural := 8;
  constant c_sens_temp_volt_sz   : natural := 9;  -- Should match nof read bytes via I2C in the sens_ctrl SEQUENCE list

  -- Model I2C sensor slaves as on the LOFAR RSP board
  constant c_bp_volt_address     : std_logic_vector(6 downto 0) := "0010100";  -- MAX6652 address GND
  constant c_bp_temp_address     : std_logic_vector(6 downto 0) := "0101010";  -- MAX1618 address MID  MID
  constant c_ap0_temp_address    : std_logic_vector(6 downto 0) := "0011000";  -- MAX1618 address LOW  LOW
  constant c_ap1_temp_address    : std_logic_vector(6 downto 0) := "0011010";  -- MAX1618 address LOW  HIGH
  constant c_ap2_temp_address    : std_logic_vector(6 downto 0) := "1001100";  -- MAX1618 address HIGH LOW
  constant c_ap3_temp_address    : std_logic_vector(6 downto 0) := "1001110";  -- MAX1618 address HIGH HIGH
  constant c_bp_temp             : integer := 60;
  constant c_ap0_temp            : integer := 70;
  constant c_ap1_temp            : integer := 71;
  constant c_ap2_temp            : integer := 72;
  constant c_ap3_temp            : integer := 73;
  constant c_volt_1v2            : natural := 92;  -- 92 *  2.5/192 = 1.2
  constant c_volt_2v5            : natural := 147;  -- 147 *  3.3/192 = 2.5
  constant c_volt_nc             : natural := 13;  -- 13 * 12  /192 = 0.1
  constant c_volt_3v3            : natural := 127;  -- 127 *  5.0/192 = 3.3
  constant c_temp_pcb            : natural := 40;

  type t_sens_data_arr is array (0 to c_sens_temp_volt_sz - 1) of std_logic_vector(c_bus_dat_w - 1 downto 0);

  signal tb_end          : std_logic := '0';
  signal clk             : std_logic := '0';
  signal rst             : std_logic := '1';

  signal scl_stretch     : std_logic := 'Z';
  signal scl             : std_logic;
  signal sda             : std_logic;

  signal sens_evt        : std_logic;
  signal sens_data       : std_logic_vector(c_sens_temp_volt_sz * c_bus_dat_w - 1 downto 0);

  signal sens_data_bytes : t_sens_data_arr;
  signal exp_data_bytes  : t_nat_natural_arr(0 to c_sens_temp_volt_sz - 1) := (92, 147, 127, 40, 60, 70, 71, 72, 73);  -- expected 9 bytes as read by SEQUENCE in sens_ctrl
begin
  -- run -all

  rst <= '0' after 4 * c_clk_period;
  clk <= not clk or tb_end after c_clk_period / 2;

  p_debug : process (sens_data)
  begin
    for i in 0 to c_sens_temp_volt_sz - 1 loop
      sens_data_bytes(c_sens_temp_volt_sz - 1 - i) <= sens_data((i + 1) * c_bus_dat_w - 1 downto i * c_bus_dat_w);
    end loop;
  end process;

  p_verify : process
  begin
    proc_common_wait_until_high(clk, sens_evt);
    proc_common_wait_until_low(clk, sens_evt);
    proc_common_wait_some_cycles(clk, 10);
    for I in sens_data_bytes'range loop
      if TO_UINT(sens_data_bytes(I)) /= exp_data_bytes(I) then
        report "Unexpected I2C read sensors result."
          severity ERROR;
        exit;
      end if;
    end loop;
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- I2C bus
  scl <= 'H';  -- model I2C pull up
  sda <= 'H';  -- model I2C pull up

  scl <= scl_stretch;

  sens_clk_stretch : process (scl)
  begin
    if falling_edge(scl) then
      scl_stretch <= '0', 'Z' after 50 ns;  -- < 10 ns to effectively disable stretching, >= 50 ns to enable it
    end if;
  end process;

  -- I2C master
  sens : entity work.sens
  generic map (
    --g_sim      => 0  -- for real time I2C link debugging purposes
    g_sim      => 1
  )
  port map(
    rst        => rst,
    clk        => clk,
    scl        => scl,
    sda        => sda,
    sens_evt   => sens_evt,
    sens_data  => sens_data
  );

  -- I2C slaves
  sens_temp_bp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_bp_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_bp_temp
  );

  sens_temp_ap0 : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_ap0_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap0_temp
  );

  sens_temp_ap1 : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_ap1_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap1_temp
  );

  sens_temp_ap2 : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_ap2_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap2_temp
  );

  sens_temp_ap3 : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_ap3_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap3_temp
  );

  sens_volt_bp : entity i2c_lib.dev_max6652
  generic map (
    g_address => c_bp_volt_address
  )
  port map (
    scl       => scl,
    sda       => sda,
    volt_2v5  => c_volt_1v2,
    volt_3v3  => c_volt_2v5,
    volt_12v  => c_volt_nc,
    volt_vcc  => c_volt_3v3,
    temp      => c_temp_pcb
  );
end tb;
