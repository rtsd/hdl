-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity common_toggle is
  generic (
    g_evt_type   : string := "RISING";  -- type can be toggle at "RISING", "FALLING", or "BOTH" edges of in_dat when in_val='1'
    g_rst_level  : std_logic := '0'  -- Defines the output level at reset.
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    in_dat      : in  std_logic;
    in_val      : in  std_logic := '1';
    out_dat     : out std_logic
  );
end;

architecture rtl of common_toggle is
  signal prev_in_dat          : std_logic;
  signal in_hld               : std_logic;
  signal in_evt               : std_logic;

  signal i_out_dat            : std_logic;
  signal nxt_out_dat          : std_logic;
begin
  out_dat <= i_out_dat;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      prev_in_dat <= g_rst_level;
      i_out_dat   <= g_rst_level;
    elsif rising_edge(clk) then
      if clken = '1' then
        prev_in_dat <= in_hld;
        i_out_dat   <= nxt_out_dat;
      end if;
    end if;
  end process;

  -- Hold in_dat combinatorially
  in_hld <= in_dat when in_val = '1' else prev_in_dat;

  -- Detect in_dat event
  u_in_evt : entity work.common_evt
  generic map (
    g_evt_type   => g_evt_type,
    g_out_invert => false,
    g_out_reg    => true
  )
  port map (
    rst      => rst,
    clk      => clk,
    clken    => clken,
    in_sig   => in_hld,
    out_evt  => in_evt
  );

  -- Toggle output at in_dat event
  nxt_out_dat <= not i_out_dat when in_evt = '1' else i_out_dat;
end rtl;
