-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Calculate the 'power' sum of input symbols during a sync interval
-- Description :
--   The in_data can have g_nof_symbols_per_data >= 1 per in_data word.
--   The sync interval starts with the in_data that is valid at the in_sync.
--   The in_data is multiplied by itself to get the power value.
--   The output 'power' sum of the previous sync interval is valid at the
--   sum_sync and held until the next sum_sync.

library IEEE, technology_lib, common_lib, common_mult_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;

entity aduh_power_sum is
  generic (
    g_technology           : natural  := c_tech_select_default;
    g_variant              : string   := "RTL";
    g_symbol_w             : natural := 12;
    g_nof_symbols_per_data : natural := 4;  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    : natural := 800 * 10**6;  -- integration time in symbols
    g_pwr_sum_truncate     : boolean := true;  -- when TRUE truncate (keep MS part) else resize (keep sign and LS part)
    g_pwr_sum_w            : natural := 32  -- typcially MM word width (= 32) or double MM word width (= 64)
  );
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;

    -- Streaming inputs
    in_data      : in  std_logic_vector(g_nof_symbols_per_data * g_symbol_w - 1 downto 0);
    in_val       : in  std_logic;
    in_sync      : in  std_logic;

    -- Accumulation outputs
    pwr_sum      : out std_logic_vector(g_pwr_sum_w - 1 downto 0);
    pwr_sum_sync : out std_logic  -- af sum_sync there is a new sum
  );
end aduh_power_sum;

architecture str of aduh_power_sum is
  constant c_prod_w        : natural := 2 * g_symbol_w;
  constant c_pipeline_prod : natural := 1;

  type t_symbol_arr is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);
  type t_prod_arr   is array (integer range <>) of std_logic_vector(c_prod_w - 1 downto 0);

  signal prod_data : std_logic_vector(g_nof_symbols_per_data * c_prod_w - 1 downto 0);
  signal prod_val  : std_logic;
  signal prod_sync : std_logic;

  -- Debug signals
  signal symbol_arr   : t_symbol_arr(0 to g_nof_symbols_per_data - 1);
  signal prod_arr     : t_prod_arr(0 to g_nof_symbols_per_data - 1);
begin
  u_prod_data : entity common_mult_lib.common_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_symbol_w,
    g_in_b_w           => g_symbol_w,
    g_out_p_w          => c_prod_w,  -- <= g_in_a_w + g_in_b_w
    g_nof_mult         => g_nof_symbols_per_data,  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   => 0,  -- 0 or 1
    g_pipeline_product => 1,  -- 0 or 1
    g_pipeline_output  => 0,  -- >= 0
    g_representation   => "SIGNED"
  )
  port map (
    rst        => rst,
    clk        => clk,
    in_a       => in_data,
    in_b       => in_data,
    out_p      => prod_data
  );

  u_prod_sync : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline    => c_pipeline_prod,  -- = 1, must match total pipelining of u_prod_data
    g_reset_value => 0
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sync,
    out_dat => prod_sync
  );

  u_prod_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline    => c_pipeline_prod,  -- = 1, must match total pipelining of u_prod_data
    g_reset_value => 0
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => prod_val
  );

  u_pwr_sum : entity work.aduh_mean_sum
  generic map (
    g_symbol_w             => c_prod_w,
    g_nof_symbols_per_data => g_nof_symbols_per_data,
    g_nof_accumulations    => g_nof_accumulations,
    g_sum_truncate         => g_pwr_sum_truncate,
    g_sum_w                => g_pwr_sum_w
  )
  port map (
    clk         => clk,
    rst         => rst,

    -- Streaming inputs
    in_data     => prod_data,
    in_val      => prod_val,
    in_sync     => prod_sync,

    -- Accumulation outputs
    sum         => pwr_sum,
    sum_sync    => pwr_sum_sync
  );

  -- Debug wire signal arrays for easier data interpretation in the Wave window
  dbg_arr : for I in 0 to g_nof_symbols_per_data - 1 generate
    symbol_arr(I) <= in_data((g_nof_symbols_per_data - I) * g_symbol_w - 1 downto (g_nof_symbols_per_data - I - 1) * g_symbol_w);
    prod_arr(I) <= prod_data((g_nof_symbols_per_data - I) * c_prod_w - 1   downto (g_nof_symbols_per_data - I - 1) * c_prod_w);
  end generate;
end str;
