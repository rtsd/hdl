###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Set pin locations
###############################################################################

set_location_assignment PIN_AV10 -to ADC_BI_A[0]
set_location_assignment PIN_AV8 -to ADC_BI_A[1]
set_location_assignment PIN_AV7 -to ADC_BI_A[2]
set_location_assignment PIN_AW6 -to ADC_BI_A[3]
set_location_assignment PIN_AV5 -to ADC_BI_A[4]
set_location_assignment PIN_AT6 -to ADC_BI_A[5]
set_location_assignment PIN_AT7 -to ADC_BI_A[6]
set_location_assignment PIN_AT8 -to ADC_BI_A[7]
set_location_assignment PIN_AF6 -to ADC_BI_A_CLK
set_location_assignment PIN_AP6 -to ADC_BI_B[0]
set_location_assignment PIN_AN6 -to ADC_BI_B[1]
set_location_assignment PIN_AM6 -to ADC_BI_B[2]
set_location_assignment PIN_AL6 -to ADC_BI_B[3]
set_location_assignment PIN_AJ6 -to ADC_BI_B[4]
set_location_assignment PIN_AH6 -to ADC_BI_B[5]
set_location_assignment PIN_AG6 -to ADC_BI_B[6]
set_location_assignment PIN_AB9 -to ADC_BI_B[7]
set_location_assignment PIN_AC6 -to ADC_BI_B_CLK
set_location_assignment PIN_W8 -to ADC_BI_C[0]
set_location_assignment PIN_R6 -to ADC_BI_C[1]
set_location_assignment PIN_N8 -to ADC_BI_C[2]
set_location_assignment PIN_R7 -to ADC_BI_C[3]
set_location_assignment PIN_N6 -to ADC_BI_C[4]
set_location_assignment PIN_M6 -to ADC_BI_C[5]
set_location_assignment PIN_K6 -to ADC_BI_C[6]
set_location_assignment PIN_J6 -to ADC_BI_C[7]
set_location_assignment PIN_AB6 -to ADC_BI_C_CLK
set_location_assignment PIN_G8 -to ADC_BI_D[0]
set_location_assignment PIN_F7 -to ADC_BI_D[1]
set_location_assignment PIN_G6 -to ADC_BI_D[2]
set_location_assignment PIN_D5 -to ADC_BI_D[3]
set_location_assignment PIN_D7 -to ADC_BI_D[4]
set_location_assignment PIN_D8 -to ADC_BI_D[5]
set_location_assignment PIN_D9 -to ADC_BI_D[6]
set_location_assignment PIN_D10 -to ADC_BI_D[7]
set_location_assignment PIN_W6 -to ADC_BI_D_CLK

# BN pin ADC_SCL[0] pin connects to ADC_AB_SCL for I2C with ADU-AB
# BN pin ADC_SDA[0] pin connects to ADC_AB_SDA for I2C with ADU-AB
# BN pin ADC_SCL[1] pin connects to ADC_BI_A_CLK_RST_N for DCLK_RST to ADU-AB, will get inferred to PIN_AG7
# BN pin ADC_SDA[1] pin connects to ADC_BI_A_CLK_RST_P for DCLK_RST to ADU-AB
# BN pin ADC_SCL[2] pin connects to ADC_BI_D_CLK_RST_N for DCLK_RST to ADU-CD, will get inferred to PIN_M7
# BN pin ADC_SDA[2] pin connects to ADC_BI_D_CLK_RST_P for DCLK_RST to ADU-CD
# BN pin ADC_SCL[3] pin connects to ADC_CD_SCL for I2C with ADU-CD
# BN pin ADC_SDA[3] pin connects to ADC_CD_SDA for I2C with ADU-CD
#
# Need to use pins [1] and [2] for CLK_RST LVDS output because:
# - [1] and [2] support DIFFIO_TX = true LVDS Tx
# - [0] and [3] support DIFFIO_RX = true LVDS Rx
# - all [0:3]   support DIFFOUT   = emulated LVDS Tx, but that requires 1 (LVDS_E_R1) or 3 (LVDS_E_R3) external resistors
# Hence use pins [0] and [3] for ADU I2C.
set_location_assignment PIN_AU10 -to ADC_AB_SDA
set_location_assignment PIN_AT10 -to ADC_AB_SCL
set_location_assignment PIN_G9 -to ADC_CD_SDA
set_location_assignment PIN_F9 -to ADC_CD_SCL
set_location_assignment PIN_AG7 -to ADC_BI_A_CLK_RST
set_location_assignment PIN_M7 -to ADC_BI_D_CLK_RST
# set_location_assignment PIN_AG8 -to ADC_BI_A_CLK_RST
# set_location_assignment PIN_M8 -to ADC_BI_D_CLK_RST
#

###############################################################################
# Set IO_STANDARD
###############################################################################

set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[0]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[1]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[2]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[3]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[4]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[5]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[6]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A[7]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_A_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[0]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[1]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[2]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[3]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[4]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[5]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[6]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B[7]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_B_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[0]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[1]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[2]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[3]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[4]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[5]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[6]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C[7]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_C_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[0]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[1]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[2]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[3]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[4]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[5]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[6]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D[7]
set_instance_assignment -name IO_STANDARD LVDS -to ADC_BI_D_CLK

set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_BI_A_CLK_RST
set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_BI_D_CLK_RST
set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_AB_SCL
set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_AB_SDA
set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_CD_SCL
set_instance_assignment -name IO_STANDARD "2.5 V" -to ADC_CD_SDA


###############################################################################
# Set INPUT_TERMINATION
###############################################################################

set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[1]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[2]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[3]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[4]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[5]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[6]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A[7]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_A_CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[1]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[2]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[3]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[4]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[5]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[6]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B[7]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_B_CLK -disable
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[1]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[2]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[3]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[4]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[5]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[6]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C[7]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_C_CLK -disable
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[1]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[2]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[3]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[4]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[5]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[6]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D[7]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ADC_BI_D_CLK


###############################################################################
# Directly set input delay for ADC_BI_A,B,C,D[*] (see aduh_dd.sdc)
###############################################################################
#
# From pad to dual FF:
#
# . D2 : 0 = 377 ps, delta =  50 ps, nof steps = 8
# . D3 : 0 = 135 ps, delta = 385 ps, nof steps = 8  -->  D3 ~= 7.5 * D2
# . D1 : 0 = 222 ps, delta =  50 ps, nof steps = 16
#
# With set_input_delay it appears that via set_input_delay only D2 and D3 are
# effectively used, therefore keep D1 at default 0.

set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[0]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[1]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[2]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[3]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[4]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[5]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[6]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_A[7]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[0]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[1]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[2]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[3]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[4]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[5]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[6]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_B[7]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[0]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[1]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[2]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[3]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[4]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[5]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[6]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_C[7]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[0]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[1]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[2]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[3]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[4]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[5]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[6]
set_instance_assignment -name D2_DELAY 0 -to ADC_BI_D[7]

set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[0]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[1]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[2]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[3]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[4]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[5]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[6]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_A[7]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[0]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[1]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[2]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[3]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[4]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[5]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[6]
set_instance_assignment -name D3_DELAY 2 -to ADC_BI_B[7]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[0]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[1]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[2]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[3]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[4]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[5]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[6]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_C[7]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[0]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[1]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[2]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[3]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[4]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[5]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[6]
set_instance_assignment -name D3_DELAY 3 -to ADC_BI_D[7]

# force D1 to 0 to avoid Quartus to set them (e.g. to 1)
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[0]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[1]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[2]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[3]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[4]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[5]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[6]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_A[7]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[0]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[1]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[2]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[3]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[4]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[5]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[6]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_B[7]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[0]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[1]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[2]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[3]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[4]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[5]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[6]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_C[7]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[0]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[1]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[2]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[3]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[4]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[5]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[6]
set_instance_assignment -name D1_DELAY 0 -to ADC_BI_D[7]


###############################################################################
# Directly set the output delay for the ADC_BI_*_CLK_RST (see aduh_dd.sdc)
###############################################################################

# From dual FF to pad:
# . D5 range 0..15 in steps of about 50 ps
# . D6 range 0..6  in steps of about 50 ps
set_instance_assignment -name D5_DELAY 0 -to ADC_BI_A_CLK_RST
set_instance_assignment -name D5_DELAY 0 -to ADC_BI_D_CLK_RST
set_instance_assignment -name D6_DELAY 0 -to ADC_BI_A_CLK_RST
set_instance_assignment -name D6_DELAY 0 -to ADC_BI_D_CLK_RST


