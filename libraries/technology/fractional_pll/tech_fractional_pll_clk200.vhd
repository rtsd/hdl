-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_fractional_pll_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_fractional_pll_clk200_altera_xcvr_fpll_a10_150;
library ip_arria10_e3sge3_fractional_pll_clk200_altera_xcvr_fpll_a10_151;
library ip_arria10_e1sg_fractional_pll_clk200_altera_xcvr_fpll_a10_180;
library ip_arria10_e2sg_fractional_pll_clk200_altera_xcvr_fpll_a10_191;

entity tech_fractional_pll_clk200 is
  generic (
    g_technology       : natural := c_tech_select_default
  );
  port (
    areset  : in std_logic  := '0';
    inclk0  : in std_logic  := '0';  -- 200 MHz
    c0      : out std_logic;  -- 200 MHz
    c1      : out std_logic;  -- 200 MHz shifted 90 degrees
    c2      : out std_logic;  -- 400 MHz
    locked  : out std_logic
  );
end tech_fractional_pll_clk200;

architecture str of tech_fractional_pll_clk200 is
begin
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_fractional_pll_clk200
    port map (
      outclk0       => c0,  -- outclk0.clk
      outclk1       => c1,  -- outclk1.clk
      outclk2       => c2,  -- outclk2.clk
      pll_cal_busy  => OPEN,  -- pll_cal_busy.pll_cal_busy
      pll_locked    => locked,  -- pll_locked.pll_locked
      pll_powerdown => areset,  -- pll_powerdown.pll_powerdown
      pll_refclk0   => inclk0  -- pll_refclk0.clk
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_fractional_pll_clk200
    port map (
      outclk0       => c0,  -- outclk0.clk
      outclk1       => c1,  -- outclk1.clk
      outclk2       => c2,  -- outclk2.clk
      pll_cal_busy  => OPEN,  -- pll_cal_busy.pll_cal_busy
      pll_locked    => locked,  -- pll_locked.pll_locked
      pll_powerdown => areset,  -- pll_powerdown.pll_powerdown
      pll_refclk0   => inclk0  -- pll_refclk0.clk
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_fractional_pll_clk200
    port map (
      outclk0       => c0,  -- outclk0.clk
      outclk1       => c1,  -- outclk1.clk
      outclk2       => c2,  -- outclk2.clk
      pll_cal_busy  => OPEN,  -- pll_cal_busy.pll_cal_busy
      pll_locked    => locked,  -- pll_locked.pll_locked
      pll_powerdown => areset,  -- pll_powerdown.pll_powerdown
      pll_refclk0   => inclk0  -- pll_refclk0.clk
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_fractional_pll_clk200
    port map (
          outclk0       => c0,  -- outclk0.clk
          outclk1       => c1,  -- outclk1.clk
          outclk2       => c2,  -- outclk2.clk
          pll_cal_busy  => OPEN,  -- pll_cal_busy.pll_cal_busy
          pll_locked    => locked,  -- pll_locked.pll_locked
          pll_powerdown => areset,  -- pll_powerdown.pll_powerdown
          pll_refclk0   => inclk0  -- pll_refclk0.clk
    );
  end generate;
end architecture;
