--Legal Notice: (C)2019 Altera Corporation. All rights reserved.  Your
--use of Altera Corporation's design tools, logic functions and other
--software and tools, and its AMPP partner logic functions, and any
--output files any of the foregoing (including device programming or
--simulation files), and any associated documentation or information are
--expressly subject to the terms and conditions of the Altera Program
--License Subscription Agreement or other applicable license agreement,
--including, without limitation, that your use is for the sole purpose
--of programming logic devices manufactured by Altera and sold by Altera
--or its authorized distributors.  Please refer to the applicable
--agreement for further details.

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity qsys_unb2b_minimal_pio_wdi_altera_avalon_pio_180_2botkdq is
        port (
              -- inputs:
                 signal address : in std_logic_vector(1 downto 0);
                 signal chipselect : in std_logic;
                 signal clk : in std_logic;
                 signal reset_n : in std_logic;
                 signal write_n : in std_logic;
                 signal writedata : in std_logic_vector(31 downto 0);

              -- outputs:
                 signal out_port : out std_logic;
                 signal readdata : out std_logic_vector(31 downto 0)
              );
end entity qsys_unb2b_minimal_pio_wdi_altera_avalon_pio_180_2botkdq;

architecture europa of qsys_unb2b_minimal_pio_wdi_altera_avalon_pio_180_2botkdq is
                signal clk_en :  std_logic;
                signal data_out :  std_logic;
                signal read_mux_out :  std_logic;
begin
  clk_en <= std_logic'('1');
  --s1, which is an e_avalon_slave
  read_mux_out <= to_std_logic((((std_logic_vector'("000000000000000000000000000000") & (address)) = std_logic_vector'("00000000000000000000000000000000")))) and data_out;
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_out <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((chipselect and not write_n) and to_std_logic((((std_logic_vector'("000000000000000000000000000000") & (address)) = std_logic_vector'("00000000000000000000000000000000")))))) = '1' then
        data_out <= writedata(0);
      end if;
    end if;
  end process;

  readdata <= std_logic_vector'("00000000000000000000000000000000") or (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(read_mux_out)));
  out_port <= data_out;
end europa;
