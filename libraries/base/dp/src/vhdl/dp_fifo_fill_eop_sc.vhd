-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle, E. Kooistra
-- Purpose: Single clk wrapper for dp_fifo_fill_eop.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_fill_eop_sc is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_note_is_ful    : boolean := true;
    g_data_w         : natural := 16;  -- Should be 2 times the c_complex_w if g_use_complex = TRUE
    g_data_signed    : boolean := false;  -- TRUE extends g_data_w bits with the sign bit, FALSE pads g_data_w bits with zeros.
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_fill      : natural := 0;
    g_fifo_size      : natural := 256;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1  -- use RL=0 for internal show ahead FIFO, default use RL=1 for internal normal FIFO
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- Monitor FIFO filling
    wr_ful       : out std_logic;  -- corresponds to the carry bit of wr_usedw when FIFO is full
    usedw        : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_emp       : out std_logic;
    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = wr_usedw
    rd_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = rd_usedw
    rd_fill_32b  : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_fifo_fill, c_word_w);
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_fifo_fill_eop_sc;

architecture wrap of dp_fifo_fill_eop_sc is
begin
  u_dp_fifo_fill_eop : entity work.dp_fifo_fill_eop
  generic map (
    g_technology     => g_technology,
    g_note_is_ful    => g_note_is_ful,
    g_use_dual_clock => false,  -- single clock
    g_data_w         => g_data_w,
    g_data_signed    => g_data_signed,
    g_bsn_w          => g_bsn_w,
    g_empty_w        => g_empty_w,
    g_channel_w      => g_channel_w,
    g_error_w        => g_error_w,
    g_use_bsn        => g_use_bsn,
    g_use_empty      => g_use_empty,
    g_use_channel    => g_use_channel,
    g_use_error      => g_use_error,
    g_use_sync       => g_use_sync,
    g_use_complex    => g_use_complex,
    g_fifo_fill      => g_fifo_fill,
    g_fifo_size      => g_fifo_size,
    g_fifo_af_margin => g_fifo_af_margin,
    g_fifo_af_xon    => g_fifo_af_xon,
    g_fifo_rl        => g_fifo_rl
  )
  port map (
    wr_rst      => rst,
    wr_clk      => clk,
    rd_rst      => rst,
    rd_clk      => clk,
    -- Monitor FIFO filling
    wr_ful      => wr_ful,
    wr_usedw    => OPEN,
    rd_usedw    => usedw,  -- use rd_usedw, similar as in dp_fifo_sc, dp_fifo_fill_sc
    rd_emp      => rd_emp,
    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b => wr_usedw_32b,
    rd_usedw_32b => rd_usedw_32b,
    rd_fill_32b  => rd_fill_32b,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => snk_in,
    -- ST source
    src_in       => src_in,
    src_out      => src_out
  );
end wrap;
