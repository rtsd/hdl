-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Single instance wrapper for dp_selector_arr.
-- Description:
-- Remark:
-- . See dp_selector_arr.vhd for more detail.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_selector is
  generic (
    g_pipeline    : natural := 1
  );
  port (
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;

    -- MM interface
    reg_selector_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    reg_selector_miso       : out t_mem_miso;

    pipe_sosi               : in  t_dp_sosi;
    ref_sosi                : in  t_dp_sosi;
    out_sosi                : out t_dp_sosi;

    selector_en             : out std_logic
  );
end dp_selector;

architecture str of dp_selector is
begin
  u_dp_selector_arr : entity work.dp_selector_arr
  generic map (
    g_nof_arr   => 1,
    g_pipeline  => g_pipeline
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,

    reg_selector_mosi  => reg_selector_mosi,
    reg_selector_miso  => reg_selector_miso,

    pipe_sosi_arr(0)   =>  pipe_sosi,
    ref_sosi_arr(0)    =>  ref_sosi,
    out_sosi_arr(0)    =>  out_sosi,

    selector_en        => selector_en
  );
end str;
