-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author : R. van der Walle
-- Purpose:
--   Wrapper for Disturb2 SDP Station full design.
-- Description:
--   Unb2b version for lab testing of SDP using the WG.
--   Contains AIT input stage with WG, oversampled FSUB, XSUB, BF and RING, so without ADC JESD.

library IEEE, common_lib, unb2b_board_lib, diag_lib, dp_lib, tech_jesd204b_lib, lofar2_sdp_lib, wpfb_lib, lofar2_unb2b_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity disturb2_unb2b_sdp_station_full_wg is
  generic (
    g_design_name      : string  := "disturb2_unb2b_sdp_station_full_wg";
    g_design_note      : string  := "Disturb2 SDP station full design WG";
    g_sim              : boolean := false;  -- Overridden by TB
    g_sim_unb_nr       : natural := 0;
    g_sim_node_nr      : natural := 0;
    g_stamp_date       : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time       : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id      : string  := "";  -- revision ID     -- set by QSF
    g_wpfb             : t_wpfb  := c_sdp_wpfb_subbands;
    g_wpfb_complex     : t_wpfb  := c_sdp_wpfb_complex_subbands
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

    -- front transceivers QSFP0 for Ring.
    QSFP_0_RX     : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX     : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- front transceivers
    QSFP_1_RX     : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX     : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');  -- Using qsfp bus width also for ring interfaces
    RING_0_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0)
  );
end disturb2_unb2b_sdp_station_full_wg;

architecture str of disturb2_unb2b_sdp_station_full_wg is
begin
  u_revision : entity lofar2_unb2b_sdp_station_lib.lofar2_unb2b_sdp_station
  generic map (
    g_design_name  => g_design_name,
    g_design_note  => g_design_note,
    g_sim          => g_sim,
    g_sim_unb_nr   => g_sim_unb_nr,
    g_sim_node_nr  => g_sim_node_nr,
    g_stamp_date   => g_stamp_date,
    g_stamp_time   => g_stamp_time,
    g_revision_id  => g_revision_id,
    g_wpfb         => g_wpfb,
    g_wpfb_complex => g_wpfb_complex
  )
  port map (
    -- GENERAL
    CLK          => CLK,
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- I2C Interface to Sensors
    SENS_SC      => SENS_SC,
    SENS_SD      => SENS_SD,

    PMBUS_SC     => PMBUS_SC,
    PMBUS_SD     => PMBUS_SD,
    PMBUS_ALERT  => PMBUS_ALERT,

    -- 1GbE Control Interface
    ETH_clk      => ETH_clk,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,

    -- front transceivers QSFP0 for Ring.
    QSFP_0_RX    => QSFP_0_RX,
    QSFP_0_TX    => QSFP_0_TX,

    -- front transceivers QSFP1 for 10GbE output to CEP.
    QSFP_1_RX    => QSFP_1_RX,
    QSFP_1_TX    => QSFP_1_TX,
    -- LEDs
    QSFP_LED     => QSFP_LED,

    -- ring transceivers
    RING_0_RX    => RING_0_RX,
    RING_0_TX    => RING_0_TX,
    RING_1_RX    => RING_1_RX,
    RING_1_TX    => RING_1_TX
  );
end str;
