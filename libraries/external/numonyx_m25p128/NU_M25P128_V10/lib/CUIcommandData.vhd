--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--

Library IEEE;
   use IEEE.std_logic_1164.all;

Package CUIcommandData is

-----------  Table of Commands  ---------------------

  type CommandList_type is (
                        None, 
                        WriteEnable_cmd,
                        WriteDisable_cmd,
                        ReadIdentification_cmd,
                        ReadStatusRegister_cmd,
                        WriteStatusRegister_cmd,
                        ReadDataBytes_cmd,
                        ReadDataBytes_HS_cmd,
                        PageProgram_cmd,
                        SectorErase_cmd,
                        BulkErase_cmd
  );

  type Command_Type is record 
       cmd : CommandList_type;
       eventTime : time;
  end record;
  Type vectorCommand_type is array (Integer range <>) of Command_type;
  
  Constant DATACMD: natural := 8;
  
  SubType seqData_type is Std_Logic_vector(DATACMD - 1 downto 0);
  --------------   Command  Code ----------------
  Constant WriteEnable_cmd_SeqData         : seqData_type := X"06";
  Constant WriteDisable_cmd_SeqData        : seqData_type := X"04";
  Constant ReadIdentification_cmd_SeqData  : seqData_type := X"9F";
  Constant ReadStatusRegister_cmd_SeqData  : seqData_type := X"05";
  Constant WriteStatusRegister_cmd_SeqData : seqData_type := X"01";
  Constant ReadDataBytes_cmd_SeqData       : seqData_type := X"03";
  Constant ReadDataBytes_HS_cmd_SeqData    : seqData_type := X"0B";
  Constant PageProgram_cmd_SeqData         : seqData_type := X"02";
  Constant SectorErase_cmd_SeqData         : seqData_type := X"D8";
  Constant BulkErase_cmd_SeqData           : seqData_type := X"C7";
  
  
end CUIcommandData;

Package body CUIcommandData is

  ---------  The body is empty  -----------

end CUIcommandData;
