-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use technology_lib.technology_pkg.all;
use dp_lib.dp_stream_pkg.all;

package tech_eth_10g_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------
  component ip_stratixiv_eth_10g is
  generic (
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;  -- 0 = use XAUI IP; 1 = use fast serdes model
    g_nof_channels            : natural := 1;
    g_direction               : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding      : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_156   : in  std_logic;  -- 156.25 MHz for XAUI
    tr_ref_rst_156   : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk      : in  std_logic;

    -- Data clocks
    tx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr_in or connect rx_clk_arr to tx_clk_arr_in
    tx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    xaui_mosi        : in  t_mem_mosi := c_mem_mosi_rst;  -- XAUI control
    xaui_miso        : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz tx_clk_arr_in
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz rx_clk_arr_in
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- XAUI serial IO
    xaui_tx_arr      : out t_xaui_arr(g_nof_channels - 1 downto 0);
    xaui_rx_arr      : in  t_xaui_arr(g_nof_channels - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  component ip_arria10_eth_10g is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  ------------------------------------------------------------------------------
  component ip_arria10_e3sge3_eth_10g is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso   : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e1sg
  ------------------------------------------------------------------------------

  component ip_arria10_e1sg_eth_10g is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_use_loopback        : boolean := false;
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    reg_ip_arria10_e1sg_phy_10gbase_r_24_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e1sg_phy_10gbase_r_24_miso     : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e2sg
  ------------------------------------------------------------------------------

  component ip_arria10_e2sg_eth_10g is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_use_loopback        : boolean := false;
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    reg_ip_arria10_e2sg_phy_10gbase_r_24_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e2sg_phy_10gbase_r_24_miso     : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
  end component;
end tech_eth_10g_component_pkg;

package body tech_eth_10g_component_pkg is
end tech_eth_10g_component_pkg;
