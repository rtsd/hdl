-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Bridge between AXI4 lite and MM interfaces.
-- Description:
-- . This core consists of:
--   . Combinatorial translation of one interface to the other.
-- . Details:
--   . g_active_low_rst should be set to TRUE when in_rst is active low. This is useful as an
--     AXI4 interface often comes with an active-low reset while DP comes with an active-high
--     reset.
-- . Remark:
--   . The read latency is not adapted. Ensure that the Controller and Peripheral use the same
--     read-latency.
--   . Both AXI4-lite and MM use ready latency = 0 for waitrequest/ready.
--   . AXI4-lite is assumed to use byte addressed registers while MM uses word addressed
--     registers.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.axi4_lite_pkg.all;

entity axi4_lite_mm_bridge is
  generic (
    g_active_low_rst : boolean := false  -- When True, in_rst is interpreted as active-low.
  );
  port (
    in_clk  : in std_logic := '0';
    in_rst  : in std_logic := is_true(g_active_low_rst);  -- Default state is "not in reset".

    aresetn : out std_logic := '1';  -- AXI4 active-low reset
    mm_rst  : out std_logic := '0';  -- MM active-high reset

    -- Translate AXI4 lite to MM
    axi4_in_copi  : in  t_axi4_lite_copi := c_axi4_lite_copi_rst;
    axi4_in_cipo  : out t_axi4_lite_cipo := c_axi4_lite_cipo_rst;

    mm_out_copi   : out t_mem_copi       := c_mem_copi_rst;
    mm_out_cipo   : in  t_mem_cipo       := c_mem_cipo_rst;

    -- Translate MM to AXI4 lite
    mm_in_copi    : in  t_mem_copi       := c_mem_copi_rst;
    mm_in_cipo    : out t_mem_cipo       := c_mem_cipo_rst;

    axi4_out_copi : out t_axi4_lite_copi := c_axi4_lite_copi_rst;
    axi4_out_cipo : in  t_axi4_lite_cipo := c_axi4_lite_cipo_rst
  );
end axi4_lite_mm_bridge;

architecture str of axi4_lite_mm_bridge is
  signal i_rst : std_logic := '0';

  signal i_mm_out_copi : t_mem_copi;

  signal d_bvalid : std_logic := '0';
  signal q_bvalid : std_logic := '0';
begin
  i_rst   <= not in_rst when g_active_low_rst else in_rst;
  aresetn <= not i_rst;
  mm_rst  <= i_rst;

  -----------------------------------------------
  -- Translate MM to AXI4 Lite
  -----------------------------------------------
  axi4_out_copi <= func_axi4_lite_from_mm_copi(mm_in_copi);
  mm_in_cipo    <= func_axi4_lite_to_mm_cipo(axi4_out_cipo);

  -------------------------------------------
  -- Translate AXI4 to MM
  -------------------------------------------
  -- AXI4 Lite to MM
  mm_out_copi  <= func_axi4_lite_to_mm_copi(axi4_in_copi);
  axi4_in_cipo <= func_axi4_lite_from_mm_cipo(mm_out_cipo, q_bvalid);

  -- Generate bvalid
  q_bvalid <= d_bvalid when rising_edge(in_clk);

  -- According to AXI4 interface definition, BVALID must be asserted after
  -- WVALID and WREADY are both asserted. Then BVALID is acknowledged by
  -- BREADY. Note that in this process we use WREADY = NOT mm_out_cipo.wairequest.
  p_bvalid : process(i_rst, q_bvalid, mm_out_cipo, axi4_in_copi)
  begin
    d_bvalid <= q_bvalid;

    -- WREADY = '1' AND WVALID = '1', so assert BVALID.
    if mm_out_cipo.waitrequest = '0' and axi4_in_copi.wvalid = '1' then
      d_bvalid <= '1';

    -- BVALID is acknowledged by BREADY, so deassert BVALID.
    elsif axi4_in_copi.bready = '1' then
      d_bvalid <= '0';
    end if;
    if i_rst = '1' then
     d_bvalid <= '0';
    end if;
  end process;
end str;
