-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 2
-- > run -all --> OK

entity tb_tb3_dp_mux is
end tb_tb3_dp_mux;

architecture tb of tb_tb3_dp_mux is
  constant c_nof_inputs : natural := 3;
  constant c_nof_repeat : natural := c_nof_inputs * 10;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Select input via sel_ctrl (g_mode=2)
  --                                                       in_en,    src_in.ready, nof repeat,   nof_inputs
  u0_act_act         : entity work.tb3_dp_mux generic map (e_active, e_active,     c_nof_repeat, c_nof_inputs);
  u0_act_rnd         : entity work.tb3_dp_mux generic map (e_active, e_random,     c_nof_repeat, c_nof_inputs);
  u0_act_pls         : entity work.tb3_dp_mux generic map (e_active, e_pulse,      c_nof_repeat, c_nof_inputs);

  u0_rnd_act         : entity work.tb3_dp_mux generic map (e_random, e_active,     c_nof_repeat, c_nof_inputs);
  u0_rnd_rnd         : entity work.tb3_dp_mux generic map (e_random, e_random,     c_nof_repeat, c_nof_inputs);
  u0_rnd_pls         : entity work.tb3_dp_mux generic map (e_random, e_pulse,      c_nof_repeat, c_nof_inputs);

  u0_pls_act         : entity work.tb3_dp_mux generic map (e_pulse,  e_active,     c_nof_repeat, c_nof_inputs);
  u0_pls_rnd         : entity work.tb3_dp_mux generic map (e_pulse,  e_random,     c_nof_repeat, c_nof_inputs);
  u0_pls_pls         : entity work.tb3_dp_mux generic map (e_pulse,  e_pulse,      c_nof_repeat, c_nof_inputs);
end tb;
