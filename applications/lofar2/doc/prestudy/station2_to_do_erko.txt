*******************************************************************************
* Netherlandse Code of conduct for research integrety:
*******************************************************************************

Vijf principes:
- Eerlijkheid
- Zorgvuldigheid
- Transparantie
- Onafhankelijkheid
- Verantwoordelijkheid



*******************************************************************************
* SKA experience:
*******************************************************************************
- Experiences -> believes ->  choices -> actions -> results
- Model noise as chorus of 100 sine waves
- Phase closure with 3 stations: angle(ab*) + angle(bc*) + angle(ca*) = 0 degrees
- Amplitude closure with 4 stations: |ab*| * |cd*| / (|ac*| * |bd*|) = 1
- Correlator efficiency
- RFI flag using most negative value (in LFAA), but this value must be replaced in subsequent processing
- signal capture, power statistics, power max hold
- signal generator sinus, sinus in noise, noise, test sequence, ...
- Jones matrix
  . orthogonalisation due orientation with rotation of earth and frequency dependence due to changes in ionosfeer?
  . beamforming due to geometrical delay with rotation of earth
  . analogue complex gain calibration (temperature dependent)
  . beam shape tapering? (fixed)


*******************************************************************************
* To do:
*******************************************************************************
- Check that the Expert users (MB, SJW, MN), Maintainers (HM) and Local users are happy with the
  design decisions
- H6 M&C loads section
- H3 Functions mapping
- H3/4 Timing (1s default, PPS, event message)
  Timing en M&C in ADD --> detailed design sectie doorlezen
  * Use PPS event message from SDP --> SC for:
    1 Hz --> Set ToD (this requires that PPS is avaiable in SDP independent of data path processing)
    1 Hz --> Update BF weigths
    1 Hz --> Monitor AST, SST, BST
  * Use dedicated event message from SDP --> SC for XST readout
    1 Hz or more --> XST
  All other M&C is not time critical, which means that required rates are average rates.
- H4/8
  . Subband correlator read out via MM or offload --> let SC do read out via M&C and provide offload to TM
  . One or more subbands per interval
  . Shorter < 1 s intervals and longer > 1 s intervals
- HBA tile elements individually controlable, trimmable (e.g. in case of new production batch)
- Discuss GIT, RadioHDL with PD (read radiohdl readme and Ruuds docu to prepare)
- System enigineering (map, SEMP doc HvdM)
- System testcases in SEMP doc --> check with AJB, BH --> test plan after PDR (see SEMP doc)
- S_hba_international = 192
- nof tx, rx pkt/s, CRC error count, nof pause frames
- monitor functions publish their data points, other functions can use these for alarms, visualisation, calibration
- data buffer on output beamlets --> histogram
   . buffer all beamlets per T_sub at sync
   . buffer one beamlet for some T_sub time series after sync
- Created common_mem_bus and common_mem_master_mux to replace Qsys
  . considered using Wishbone, but for our M&C using common_mem_bus is easier than wrapping a Wishbone bus
  . add a common_arbiter to make a common_mem_master_arbiter using miso.waitrequest, however then it may
    be necessary to reconsider wrapping a Wishbone bus.
- Add support generating MM bus by wiring common_mem_bus to named mosi/miso slaves or arrays of slaves
  from yaml with ARGS
- g_sim
  . define g_sim record
  . use g_sim or consider sim models as a technology
- lofar2_unb2c structure with design_name revisions and node_<> per device: bsp, ring_(bf, xc, tb, so),
  node_(bf, xc, tb, so, tdet, adc, fb), ddr4_(0,1) for tb, offload_10g
  

- Use GIT
- Rename master/slave, mosi/miso
- Understand AXI4 streaming (versus avalon, RL =0)
  . wrap between AXI4 - Avalon for MM and DP
- Global reset only on sosi info not on sosi data
- Use ARGS to define peripherals
  . check docs and article
- Learn how gmi_minimal HDL code works to prepare for porting to unb2b_minimal_gmi
- Update RadioHDL docs
- Write RadioHDL article
- Write HDL RL=0 article - rtsd_hdl_design_article.txt
- XST : SNR = 1 per visibility for 10000 samples, brigthtest sourcre log 19.5 --> 4.5 dB --> T_int = 1 s is ok.
- BSP registers:
  . duration of operations : counts time since last power cycle (passive heartbeat)
  . cause of reboot (power cycle, overtemperature, ...)


- RCU2S-SDP signal input allocation:
  Decided:
  . LBA and HBA on seperate RCUs and also on seperate subracks and on independent rings, so SDP is independent
    for LBA and HBA.
  . X and Y on the same ring to support dual polarization ACM
  Still open:
  . X and Y together per PN or X and Y on seperate subracks
    - polarization correction via subband weights is not needed, so X and Y can be on different PN
    - EMI between X and Y, but X and Y have only about 40 dB isolation
    - EMI between single pol inputs get suppressed dependent on the station digital beam pointing
	- No need for pseudo random PFB input decorrelator function like in LOFAR1?
  . LBA inner signal inputs and LBA outer signal inputs on different subracks or arbitrary. Inner is not used
    Instead they use sparse odd and sparse even to have two more or less random antenna allocations.
  . HBA core station sub-array inputs on different Uniboard2 to reduce EMI
  . HBA-X power, Y control --> power distribution
  . HBA RCU 3X and 3Y

- EMI:
  . RCU2S-SDP signal input allocation
  . Shielding
  . more cross talk between horizontal RCUs than between vertical RCUs
  . more cross talk between neighbour RCUs
  . AC-DC instead of using AC-DC48-DC


*******************************************************************************
* System engineering:
*******************************************************************************

- Requirements,
- Functions have their own hierarchy, independent of requirement levels, and independent of PBS.
  Requirements map to products according to the L0, L1, L2 hierarchy
  Requirements map to functions at any level
- Products
- ICDs
- Roles

Polarion:
- Maintain hierarchy of functions and link them to products
- Start with parent function and allocate that to one or more L3 products/ functions in ADD tables,
  after that number the child level functions
- Start with L3 product an look for potential requirements in L2 SRS contents
  allocate requirement number + keywords
- Check for unallocated requirements
  Check for unallocated parent functions
  Check for unallocated child functions
- Number child functions in ADD tables
- After PDR:
  . Update/add parent functions and child functions in Polarion and in ADD tables
  . Number the child functions in Polarion

* Non compliant requirements
  - 3099 LOFAR 1.0 frequency grid only with critically sampled filterbank
  - 2278 Alias free broadband reconstruction only with oversampled filterbank

* Missing requirements for functions
  - operational: all sky image (maybe 2400)
  - transient buffer: trade inputs for duration
  - transient detection: what algorithm should be used and with what parameters
  - Expert user interface requirements
  - Station Control support for EAA (LORA, NenuFAR)
  - Rack space for NenuFar

* Missing functions for requirements
  - 2404 Control of RFI mitigation processes
  - 2405 RFI process handling
  - 2400

* Unmapped requirements
  -


* Mark Ruiter: Missing calibration strategy.

  level 1: Analogue input (hba, lba)
  level 2: Beam ( analog)
  level 3: (station)
  level 4: (local ionosphere)
  level 5: (distributed ionosphere lofar 2)

  This only mentions digital station calibrating assuming a lot.
  --> Gijs Schoonderbeek: Added this list in chapter 10, where the expected performance is described.


Station:
1) SC-TM: Local mode - Station and TM - Station ICD needed to define SC functionality
   - ICD for SC-TM
2) SC: SCU for application layers with OPC-UA frame work like EPICS, WinCC
3) SC-SDP: Separate 'PC / microcontroller platform' to translate OPC-UA - Gemini Protocol of UniBoard2
   - ICD for SC-SDP
   - work starts with supporting the 'hello world' of controlling a UniBoard2
4) SC-RCU2S : 'Microprocessor / microcontroller platform' to translate OPC-UA - I2C, on/off line, SPI.
   - ICD for SC-PCC
   - PCC connects to RCU2 and UniBoards
5) SC-STCA
6) SC-STF
7) SC-STIN



*******************************************************************************
* Terminology
*******************************************************************************

* Software blocks:
  . server
  . driver
  . handler

  . device

* Synonyms for Control:
  . setup
  . configure
  . handle
  . set
  . supply
  . send
  . update
  . boot, run, initialize
  . write
  . store

* Synonyms for Monitor
  . obtain
  . get
  . read
  
* Alternatives to master/slave (mosi, miso)
  . client / server --> cosi, ciso
  . primary, main / secondary, replica, subordinate
  . initiator, requester / target, responder
  . controller, host / device, worker, proxy
  . leader / follower
  . director / performer


*******************************************************************************
* Station Control
*******************************************************************************
Remarks:
- Provide direct TB access to the buffer data is an L4 or L5 function
- Station beam or station beams? --> Station beams, otherwise call it full band station beam
- There is no automatic feedback:
  . AST and SST are only for diagnosis and alarms.
  . XST is used in separate function to determine a new set of the subband calibration weigths.
  . The scale beamlets setting is fixed per beamlet mode and nominal signal input levels

+ RCU2-LBA
    * Monitor RCU2-LBA
         . Monitor RCU2 hardware                    . Sense RCU2 temperatures, voltages and currents
         . Monitor LBA current                      . Sense LBA current
    * Setup RCU2-LBA
         . Control LBA power                        . Supply LBA with power
         . Select frequency band                    . Filter band of interest
         . Select signal level                      . Set signal level
         . Set ADC mode                             . Digitize signal

+ RCU2-HBAT
    * Monitor RCU2-HBAT
         . Monitor RCU2 hardware                    . Sense RCU2 temperatures, voltages and currents
         . Monitor HBA tile current                 . Sense HBA tile current
    * Setup RCU2-HBAT
         . Control HBA tile power                   . Send power or control
         . Communicate with HBA tile
         . Select frequency band                    . Filter band of interest
         . Select signal level                      . Set signal level
         . Set ADC mode                             . Digitize signal

    * Create HBA tile beam
         . Calculate HBA tile delays
         . Update HBA tile delays
         . Communicate with HBA tile                . Convert to HBA tile control protocol

+ SDP
    * Monitor SDP
         . Monitor SDP status                       * Monitor HW, FW and interface
                                                    * Receive and timestamp ADC input
         . Monitor timing                             . L4 PPS monitor, BSN monitor (ToD timestamp, alignment)
         . Monitor ADC signal input
                                                      . L4 ADC interface monitor (L4, because only for expert/developer)
                                                      . Take snapshot of input samples
                                                      . Calculate ADC statistics (AST) (mean, power, histogram?)

                                                    * Create calibrated subbands
         . Monitor subband statistics                 . Calculate subband statistics (SST)
                                                    * Form station beams
         . Monitor beamlet statistics                 . Calculate beamlet statistics (BST)

    * Setup SDP
         . Control firmware application             * Support and run FW application (upload, store, select, boot, version)
                                                    * Receive and timestamp ADC input
         . Control ADC signal input                   . Receive ADC samples (L4 including WG and DB)
                                                      . Insert test signal
         . Compensate coarse cable length             . Align input samples
         . Set time of day                            . Time stamp input samples
                                                    * Create calibrated subbands
         . Set subband weights                        . Equalize subbands
                                                    * Form station beams
         . Select beamlet output bit range            . Scale beamlets
         . Set output header                          . Output beamlets

    * Handle subband correlator                     * Create calibrated subbands
         . Select subband                             . Correlate subband for all signal inputs (XST)
         . Monitor subband correlations

    * Derive subband weights
         . Calculate subband weights from subband correlations
         . Store subband weights

    * Create station beams                          * Form and output station beams
      . Set subband selection                         . Select subbands
      . Calculate beamlet weights
      . Update beamlet weights                        . Form beamlets

    * Handle transient detection                    * Detect radio transients
      . Set detection parameters
      . Collect and forward triggers

    * Handle transient buffer                       * Buffer ADC or subband data
      . Control TB data input                         . Select and record input data
      . Control TB data capture
      . Control TB data read out                      . Select and read out buffered data

    * Handle subband offload                        * Offload subbands


STF
    * Monitor Station TF
      . Monitor status of TD
      . Monitor timing distribution                 . Distribute timing (10 MHz, PPS)

    * Setup Station TF
      . Select sample clock frequency               . Generate sample clock (200 MHz or 160 MHz)
      . Receive time of day from L2-TD
      . Set Time of Day timestamp in SDP            . Receive and timestamp ADC input

STCA
    * Setup Station Cabinet
      . Setup power
      . Setup cooling
      . Setup network
    * Monitor Station Cabinet
      . Monitor power
      . Monitor cooling
      . Monitor network

STIN
    * Monitor Station Infrastructure
      . Monitor station housing

*******************************************************************************
* Station ADD
*******************************************************************************

- H4
  . SDP dynamisch bereik figure versimpelen + formule + simuleer processing gain van FFT voor real
    input
  . Check timing dither with PK, because this would require detailed timing control for SDP and RCU2
  . In SDP JESD details maybe too detailed design, purpose here is to explain JESD in own words to
    understand it without having to read references.
  . Check that +48 v is mentioned (not -48 V)
  . io_ddr in FN beamformer uses 26 M9K waarvan 10 in de IP. De IP gebruikt ook 1 M144K (~= 16 M9K).
    De FN beamformer gebruikt 1 DDR3 module.
    --> voor twee DDR modules zou je ongeveer (26 + 10) * 2 = 72 block RAM verwachten, terwijl
        TBB-base in LOFAR1 maar 6 gebruikt volgens MP syntesis report.
  . Timimg fixed op interne sync of op single or periodic timestamp set by SC.
    - let SDP send event for each interne sync, to help SC align its time critical M&C
    - possibly use linear interpolation for BF weights to relieve SC from tigth 1 Hz control and to
      allow fast tracking of satellites
    - define timestamp as 32b seconds and 32b blocks within second



- H6
  PK:
  . Write design decision on dithering (meant to make ADC effectively more linear):
    1) none
    2) level using additative CW --> suppress RFI harmonics already at ADC by making the ADC
       sampling more linear by crossing more levels
    3) time using delays --> does this work anyway, because the digital BF already suppresses RFI
       from all directions in which it does not point. The dithering makes that it looks like the
       RFI comes from all directions, this does not help, does it?

PDR:
- Polarization correction at station via subband weights, via BF weights?
- Station Control SCU (Local mode capabilities, Linux, ..)
- SC and L2-TD at Station
- CEP ICD (separate beamlet streams)
- HBA2 tile control
- Oversampled filterbank (design, R_os rate)
- TB for LBAS and HBAS simultaneously
- TB size
- S_sub_bf = 488
- Subband weights update rate > 10 min is sufficient?
- There are >= 488 independent Station beams per polarization. This also implies that the
  polarizations can have independent Station beams, so different frequencies and pointings. In
  LOFAR1 a subband and a beamlet are defined as a X,Y tuple. For LOFAR2.0 the beamlets are still
  output as tuples but the X and Y are treated independently, so beamlet index i for X may use
  a different subband frequency and pointing than beamlet index i for Y. Therefore in LOFAR2.0
  define a subband and a beamlet per single polarization.
- How to continue SE after PDR:
  . Update ADD with OAR answers and internal remarks from team
  . Mapping of function tree, L2 requirements and L3 products in ADD
  . Put function tree in Polarion and update function names
  . Add station functions from ADD to function tree in Polarion?
    - what is the purpose of the function tree, does it provide a check list?
    - how deep will the function tree go, till the lowest level products?
  . Do we need more SE views then that we already have with states/modes, requirements, functions
    and products?
  . ICDs
  . L4 requirements?
  . L4 products?
    - Uniboard2 hardware is an L4 product?
    - Uniboard2 firmware is an L4 product?
  . Detailed design of the firmware product

                       .
                    RO .   SDC
   --------------------.----------
   | Operations        .         |
   | ------------------.-------  |
   | |     Telescope   .      |  |
   | |     Manager     .      |  |
   | |             ----.------|  |
   | |             |   LEI    |  |
   | |-----------------.------|  |
   | |         |      |.      |  |
   | |Station  |  CEP |. SDOS |  |
   | |         |      |.      |  |
   --------------------.----------
                       .