-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Spectral inversion.
-- Description:
--   In the even Nyquist zones the sampled spectrum gets flipped in frequency.
--   This flip can be compensated for by a spectral inversion (SI). When
--   enabled the SI multiplies the input samples by (-1)**n = +1, -1, ...,
--   where n = 0 is the first sample in the FFT block. For more information
--   see section 4.19 in LOFAR_ASTRON_SDD_018_RSP_Firmware_DD.pdf.
-- Remark:
-- . Ported from LOFAR1 rsp.
-- . Rewrote code to use t_dp_sosi. Used the combinatorial style of writing the
--   code so with <sig_name>_reg instead of nxt_<sig_name).
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity si is
  generic (
    g_pipeline  : natural := 1;  -- 0 for wires, 1 for output pipeline
    g_dat_w     : natural := 18
  );
  port (
    in_sosi     : in  t_dp_sosi;
    out_sosi    : out t_dp_sosi;
    si_en       : in  std_logic;
    clk         : in  std_logic;
    rst         : in  std_logic
  );
end si;

architecture rtl of si is
  signal plus          : std_logic;
  signal plus_reg      : std_logic;
  signal si_plus       : std_logic;
  signal si_sosi       : t_dp_sosi;

  -- For view in Wave window
  signal in_data       : std_logic_vector(g_dat_w - 1 downto 0);
  signal si_data       : std_logic_vector(g_dat_w - 1 downto 0);
begin
  in_data <= in_sosi.data(g_dat_w - 1 downto 0);
  si_data <= si_sosi.data(g_dat_w - 1 downto 0);

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      plus_reg  <= '1';
    elsif rising_edge(clk) then
      plus_reg  <= plus;
    end if;
  end process;

  -- Control -1**n to start with +1 at sop and then toggle at every valid
  p_si_control : process (plus_reg, in_sosi)
  begin
    plus <= plus_reg;
    if in_sosi.sop = '1' then
      plus <= '1';
    elsif in_sosi.valid = '1' then
      plus <= not plus_reg;
    end if;
  end process;

  -- Use SI when enabled, else pass on input
  si_plus <= plus when si_en = '1' else '1';

  p_si_data : process (si_plus, in_sosi)
  begin
    si_sosi <= in_sosi;
    if si_plus = '0' then
      si_sosi.data <= NEGATE_SVEC(in_sosi.data, g_dat_w);
      si_sosi.re   <= NEGATE_SVEC(in_sosi.re, g_dat_w);
      si_sosi.im   <= NEGATE_SVEC(in_sosi.im, g_dat_w);
    end if;
  end process;

  -- Output
  u_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline  => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    snk_in  => si_sosi,
    src_out => out_sosi
  );
end rtl;
