-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Purpose:
-- . Test bench for mms_dp_xonoff
-- Description:
-- . A data stream is offered to g_nof_streams=4 inputs of the DUT.
--   A process asserts mm accesses in time to disable and enable the streams.
--
-- Usage:
-- > as 10
-- > run -all
--
-- Remarks:
-- . When g_combine_streams = FALSE: Use visual inspection to verify that the streams (src_out_arr)
--   are enabled one by one in time after the enable bit is send via mm access. Also check that all
--   streams start and stop on a package edge( stop after eop, start with sop).
--   When g_combine_streams = TRUE: check that all streams start again atthe same time.

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mms_dp_xonoff is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;  -- always active, random or pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- specific
    g_combine_streams        : boolean := false;
    g_in_dat_w               : natural := 32;
    g_in_nof_words           : natural := 1;
    g_nof_repeat             : natural := 60;
    g_pkt_len                : natural := 16;  -- must be a multiple of g_in_nof_words
    g_pkt_gap                : natural := 4
  );
end tb_mms_dp_xonoff;

architecture tb of tb_mms_dp_xonoff is
  constant c_nof_streams   : natural := 4;

  constant c_pulse_active           : natural := g_in_nof_words;
  constant c_pulse_period           : natural := g_in_nof_words;

  constant c_sync_period            : natural := 10;
  constant c_sync_offset            : natural := 7;

  constant c_data_max               : unsigned(g_in_dat_w - 1 downto 0) := (others => '1');
  constant c_data_init              : integer := -1;
  constant c_bsn_init               : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init               : natural := 247;
  constant c_channel_init           : integer := 5;  -- fixed

  signal tb_end                     : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;

  signal in_siso_arr    : t_dp_siso_arr(c_nof_streams - 1 downto 0);
  signal in_sosi_arr    : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr   : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal out_sosi_arr   : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  signal mm_mosi : t_mem_mosi;
  signal mm_miso : t_mem_miso;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------
  stimuli_en           <= '1';

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn     := INCR_UVEC(c_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(c_channel_init), -1);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(c_data_init),       -1);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(c_err_init),       -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod c_sync_period) = c_sync_offset, '1', '0');  -- insert sync starting at BSN=c_sync_offset and with period c_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len);
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      v_sosi.err     := INCR_UVEC(v_sosi.err, 1);

      -- Send packet
      proc_dp_gen_block_data(g_in_dat_w, TO_UINT(v_sosi.data), g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    -- . e_qual
    v_sosi.bsn     := std_logic_vector( unsigned(c_bsn_init) + g_nof_repeat - 1);
    v_sosi.channel := TO_DP_CHANNEL(c_channel_init           + g_nof_repeat - 1);
    v_sosi.err     := TO_DP_ERROR(c_err_init                 + g_nof_repeat - 1);
    -- . account for g_pkt_len
    v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len - 1);
    v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w

    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 100);  -- latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  gen_connect : for I in 0 to c_nof_streams - 1 generate
    in_sosi_arr(i) <= stimuli_src_out;
  end generate;

  stimuli_src_in <= c_dp_siso_rdy;

  u_dut : entity work.mms_dp_xonoff
  generic map(
    g_nof_streams     => c_nof_streams,
    g_combine_streams => g_combine_streams,
    g_bypass          => false
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => rst,
    mm_clk       => clk,

    reg_mosi     => mm_mosi,
    reg_miso     => mm_miso,

    -- Streaming clock domain
    dp_rst       => rst,
    dp_clk       => clk,

    -- ST sinks
    snk_out_arr  => in_siso_arr,
    snk_in_arr   => in_sosi_arr,
    -- ST source
    src_in_arr   => out_siso_arr,
    src_out_arr  => out_sosi_arr
  );

  p_stim: process
  begin
     wait until rst = '0';
     proc_mem_mm_bus_wr(0, x"1", clk, mm_mosi);
     proc_mem_mm_bus_wr(2, x"1", clk, mm_mosi);
     proc_mem_mm_bus_wr(4, x"1", clk, mm_mosi);
     proc_mem_mm_bus_wr(6, x"1", clk, mm_mosi);

     wait for 500 ns;

     proc_mem_mm_bus_wr(0, x"0", clk, mm_mosi);
     proc_mem_mm_bus_wr(2, x"0", clk, mm_mosi);
     proc_mem_mm_bus_wr(4, x"0", clk, mm_mosi);
     proc_mem_mm_bus_wr(6, x"0", clk, mm_mosi);

     wait for 1200 ns;

     proc_mem_mm_bus_wr(0, x"1", clk, mm_mosi);

     wait for 1400 ns;

     proc_mem_mm_bus_wr(2, x"1", clk, mm_mosi);

     wait for 2000 ns;

     proc_mem_mm_bus_wr(4, x"1", clk, mm_mosi);

     wait for 1700 ns;

     proc_mem_mm_bus_wr(6, x"1", clk, mm_mosi);

     wait;
   end process;
end tb;
