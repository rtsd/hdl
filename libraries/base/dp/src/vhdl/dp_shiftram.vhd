--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- Description:

library IEEE, common_lib, technology_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_shiftram is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_nof_streams : natural;
    g_nof_words   : natural;
    g_data_w      : natural;
    g_use_sync_in : boolean := true
  );
  port (
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;

    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;

    sync_in     : in  std_logic := '0';

    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    src_out_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end dp_shiftram;

architecture str of dp_shiftram is
  constant c_shift_w : natural := ceil_log2(g_nof_words);

  type t_common_shiftram_shift_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(c_shift_w - 1 downto 0);
  signal common_shiftram_shift_in_arr  : t_common_shiftram_shift_arr;

  constant c_field_arr : t_common_field_arr(0 downto 0) := (0 => ( field_name_pad("shift"), "RW", c_shift_w, field_default(0) ));

  signal reg_mosi_arr      : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr      : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  type t_mm_fields_out_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);
  signal mm_fields_out_arr : t_mm_fields_out_arr;
begin
  -----------------------------------------------------------------------------
  -- common_shiftram
  -----------------------------------------------------------------------------
  gen_common_shiftram : for i in 0 to g_nof_streams - 1 generate
    u_common_shiftram : entity common_lib.common_shiftram
    generic map (
      g_technology => g_technology,
      g_data_w     => g_data_w,
      g_nof_words  => g_nof_words
    )
    port map (
      rst            => dp_rst,
      clk            => dp_clk,

      data_in        => snk_in_arr(i).data(g_data_w - 1 downto 0),
      data_in_val    => snk_in_arr(i).valid,
      data_in_shift  => common_shiftram_shift_in_arr(i),

      data_out       => src_out_arr(i).data(g_data_w - 1 downto 0),
      data_out_val   => src_out_arr(i).valid,
      data_out_shift => open
    );
  end generate;

  -----------------------------------------------------------------------------
  -- MM control
  -----------------------------------------------------------------------------
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(field_nof_words(c_field_arr, c_word_w))
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    u_mm_fields: entity mm_lib.mm_fields
    generic map(
      g_field_arr => c_field_arr
    )
    port map (
      mm_clk  => mm_clk,
      mm_rst  => mm_rst,

      mm_mosi => reg_mosi_arr(i),
      mm_miso => reg_miso_arr(i),

      slv_clk => dp_clk,
      slv_rst => dp_rst,

      slv_out => mm_fields_out_arr(i)
    );

    gen_no_sync : if g_use_sync_in = false generate
      common_shiftram_shift_in_arr(i) <= mm_fields_out_arr(i)(field_hi(c_field_arr, "shift") downto field_lo(c_field_arr, "shift"));
    end generate;

    gen_sync : if g_use_sync_in = true generate
      p_clk : process(dp_rst, dp_clk, sync_in, mm_fields_out_arr)
      begin
        if dp_rst = '1' then
          common_shiftram_shift_in_arr(i) <= (others => '0');
        elsif rising_edge(dp_clk) then
          if(sync_in = '1') then
            common_shiftram_shift_in_arr(i) <= mm_fields_out_arr(i)(field_hi(c_field_arr, "shift") downto field_lo(c_field_arr, "shift"));
          end if;
        end if;
      end process;
    end generate;
  end generate;
end str;
