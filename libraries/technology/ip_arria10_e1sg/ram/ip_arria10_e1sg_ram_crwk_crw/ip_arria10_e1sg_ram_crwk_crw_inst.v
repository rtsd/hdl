	ip_arria10_e1sg_ram_crwk_crw u0 (
		.data_a    (_connected_to_data_a_),    //  ram_input.datain_a
		.data_b    (_connected_to_data_b_),    //           .datain_b
		.address_a (_connected_to_address_a_), //           .address_a
		.address_b (_connected_to_address_b_), //           .address_b
		.wren_a    (_connected_to_wren_a_),    //           .wren_a
		.wren_b    (_connected_to_wren_b_),    //           .wren_b
		.clock_a   (_connected_to_clock_a_),   //           .clock_a
		.clock_b   (_connected_to_clock_b_),   //           .clock_b
		.rden_a    (_connected_to_rden_a_),    //           .rden_a
		.rden_b    (_connected_to_rden_b_),    //           .rden_b
		.q_a       (_connected_to_q_a_),       // ram_output.dataout_a
		.q_b       (_connected_to_q_b_)        //           .dataout_b
	);

