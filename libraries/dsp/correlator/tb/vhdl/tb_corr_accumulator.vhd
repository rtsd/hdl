--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;

entity tb_corr_accumulator is
  generic (
    g_nof_inputs           : natural := 4;
    g_nof_channels         : natural := 8;
    g_nof_channel_accs     : natural := 16;
    g_data_w               : natural := 32;
    g_flow_control_stimuli : t_dp_flow_control_enum := e_active  -- always active, random or pulse flow control
  );
end tb_corr_accumulator;

architecture tb of tb_corr_accumulator is
  -----------------------------------------------------------------------------
  -- Clock & reset
  -----------------------------------------------------------------------------
  constant c_clk_period               : time := 10 ns;
  signal tb_end                       : std_logic := '0';
  signal clk                          : std_logic := '1';
  signal rst                          : std_logic;

  -----------------------------------------------------------------------------
  -- Input stimuli
  -----------------------------------------------------------------------------
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;
  signal pulse_en                     : std_logic := '1';
  signal pulse                        : std_logic;
  signal random                       : std_logic_vector(14 downto 0) := (others => '0');
  signal dp_gen_data_en               : std_logic;
  signal dp_gen_data_src_in_arr       : t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);  -- We're using dp_gen_data_en to introduce gaps.
  signal dp_gen_data_src_out_arr      : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal in_valid_count               : natural;
  signal nxt_in_valid_count           : natural;
  signal int_period_cnt               : natural;
  signal nxt_int_period_cnt           : natural;

  -----------------------------------------------------------------------------
  -- Accumulator
  -----------------------------------------------------------------------------
  signal corr_accumulator_snk_in_arr  : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal corr_accumulator_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Output verification
  -----------------------------------------------------------------------------
  constant c_nof_input_cycles         : natural := 12;  -- Number of cycles before valid data is produces by adders
  constant c_nof_accumulation_cycles  : natural := g_nof_channels * g_nof_channel_accs;  -- Number of cycles before valid data is produces by accumulator
  constant c_nof_output_cycles        : natural := g_nof_channels;  -- Number of valid output cycles per integration period
  constant c_nof_integration_periods  : natural := 5;
  constant c_nof_req_valid_cycles     : natural := c_nof_input_cycles + c_nof_accumulation_cycles + c_nof_output_cycles;  -- Minimum valid input cycles to verify one integration period
  constant c_output_data_w            : natural := g_data_w + ceil_log2(g_nof_channel_accs);
  signal expected_data                : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0);
  signal nxt_expected_data            : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0);
  signal output_channel               : natural;
  signal nxt_output_channel           : natural;
  signal verify_done                  : std_logic := '0';
  signal out_valid_count              : natural;
  signal prv_out_valid_count          : natural;
begin
  -----------------------------------------------------------------------------
  -- Clock & reset
  -----------------------------------------------------------------------------
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Input data
  -- . We're using simple counter data with the counter value matching the
  --   channel index.
   -----------------------------------------------------------------------------
  gen_stimuli : for i in 0 to g_nof_inputs - 1 generate
    -- Counter data generation
    proc_dp_gen_data(1, g_data_w, 0, g_nof_channels - 1, rst, clk, dp_gen_data_en, dp_gen_data_src_in_arr(i), dp_gen_data_src_out_arr(i));
    -- Toggle dp_gen_data_en to introduce gaps
    random <= func_common_random(random) when rising_edge(clk);
    proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period, '1', rst, clk, pulse_en, pulse);

    dp_gen_data_en <= '1'                  when g_flow_control_stimuli = e_active and rst = '0' else
                       random(random'high) when g_flow_control_stimuli = e_random and rst = '0' else
                       pulse               when g_flow_control_stimuli = e_pulse  and rst = '0' else '0';
  end generate;

  -- Count the number of valid input cycles so we know when we can stop the simulation using tb_end
  p_cnt_in_val : process(in_valid_count, int_period_cnt, dp_gen_data_src_out_arr)
  begin
    nxt_in_valid_count <= in_valid_count;
    nxt_int_period_cnt <= int_period_cnt;
    if dp_gen_data_src_out_arr(0).valid = '1' then
      nxt_in_valid_count <= in_valid_count + 1;
      if in_valid_count > 0 and (in_valid_count mod (g_nof_channels * g_nof_channel_accs)) = 0 then
        nxt_int_period_cnt <= int_period_cnt + 1;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Accumulator
  -- . We're feeding the generated counter data to both the real and imag fields.
  -----------------------------------------------------------------------------
  gen_corr_accumulator_snk_in_arr : for i in 0 to g_nof_inputs - 1 generate
    corr_accumulator_snk_in_arr(i).valid <= dp_gen_data_src_out_arr(i).valid;
    corr_accumulator_snk_in_arr(i).re    <= RESIZE_DP_DSP_DATA(dp_gen_data_src_out_arr(i).data);
    corr_accumulator_snk_in_arr(i).im    <= RESIZE_DP_DSP_DATA(dp_gen_data_src_out_arr(i).data);
    -- Create a block sync of g_nof_channels wide
    corr_accumulator_snk_in_arr(0).sync  <= '1' when (in_valid_count >= (int_period_cnt + 1) * g_nof_channels * g_nof_channel_accs - g_nof_channels and
                                                      in_valid_count < (int_period_cnt + 1) * g_nof_channels * g_nof_channel_accs and
                                                      corr_accumulator_snk_in_arr(0).valid = '1') else '0';
  end generate;

  u_corr_accumulator : entity work.corr_accumulator
  generic map (
    g_nof_inputs       => g_nof_inputs,
    g_nof_channels     => g_nof_channels,
    g_nof_channel_accs => g_nof_channel_accs,
    g_data_w           => g_data_w
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in_arr  => corr_accumulator_snk_in_arr,
    src_out_arr => corr_accumulator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Output verification
  -----------------------------------------------------------------------------
  p_verify : process
  begin
    -- Wait until we know accumulator has produced data
    wait until in_valid_count = c_nof_integration_periods * c_nof_req_valid_cycles;
    -- p_verify_corr_accumulator_src_out_arr should have done its job now. Tell proc_dp_verify_value to do the final test.
    proc_common_gen_pulse(clk, verify_done);
    -- Signal tb_end after some slack cycles
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  -- As our input channels always carry the channel indices as data, each
  -- output channel contains channel_index*g_nof_channel_accs as data.
  p_verify_corr_accumulator_src_out_arr : process(clk)
  begin
    nxt_expected_data  <= expected_data;
    nxt_output_channel <= output_channel;
    out_valid_count    <= prv_out_valid_count;

    for i in 0 to g_nof_inputs - 1 loop
      if corr_accumulator_src_out_arr(i).valid = '1' then  -- Compare against expected data
        assert corr_accumulator_src_out_arr(i).re = expected_data
          report "Expected real: " & slv_to_str(expected_data) & "; " & lf
                 & " Actual real: " & slv_to_str(corr_accumulator_src_out_arr(i).re)
          severity ERROR;
        assert corr_accumulator_src_out_arr(i).im = expected_data
          report "Expected imag: " & slv_to_str(expected_data) & "; " & lf
                 & " Actual imag: " & slv_to_str(corr_accumulator_src_out_arr(i).im)
          severity ERROR;

        if output_channel = g_nof_channels - 1 then
          nxt_output_channel <= 0;
          nxt_expected_data <= (others => '0');
        else
          nxt_output_channel <= output_channel + 1;
          nxt_expected_data <= INCR_UVEC(expected_data, g_nof_channel_accs);
        end if;

        out_valid_count <= prv_out_valid_count + 1;
      end if;
    end loop;

    -- Check the number of valid output cycles
    if verify_done = '1' then
      assert out_valid_count = c_nof_integration_periods * g_nof_channels
        report "Incorrect number of valid output cycles"
        severity ERROR;
    end if;
  end process;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      output_channel  <= 0;
      expected_data   <= (others => '0');
      prv_out_valid_count <= 0;
      in_valid_count  <= 0;
      int_period_cnt  <= 0;
     elsif rising_edge(clk) then
      output_channel  <= nxt_output_channel;
      expected_data   <= nxt_expected_data;
      prv_out_valid_count <= out_valid_count;
      in_valid_count  <= nxt_in_valid_count;
      int_period_cnt  <= nxt_int_period_cnt;
    end if;
  end process;
end tb;
