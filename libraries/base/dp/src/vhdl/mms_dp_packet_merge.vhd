-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Add MM control and multi-stream support to dp_pkt_merge.
-- Description:
-- Remarks:

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_packet_merge is
  generic (
    g_nof_streams : natural;
    g_nof_pkt     : natural
  );
  port (
    -- Memory-mapped clock domain
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;

    -- Streaming clock domain
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;

    -- ST sinks
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);
    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_packet_merge;

architecture str of mms_dp_packet_merge is
  constant c_field_arr : t_common_field_arr(0 downto 0) := (0 => ( field_name_pad("nof_pkt"), "RW", ceil_log2(g_nof_pkt + 1), field_default(18) ));

  type t_mm_fields_out_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);
  type t_nof_pkt_arr       is array(g_nof_streams - 1 downto 0) of std_logic_vector(ceil_log2(g_nof_pkt + 1) - 1 downto 0);

  signal reg_mosi_arr      : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr      : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  signal mm_fields_out_arr : t_mm_fields_out_arr;
  signal nof_pkt           : t_nof_pkt_arr;
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(field_nof_words(c_field_arr, c_word_w))
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    u_mm_fields: entity mm_lib.mm_fields
    generic map(
      g_field_arr => c_field_arr
    )
    port map (
      mm_clk  => mm_clk,
      mm_rst  => mm_rst,

      mm_mosi => reg_mosi_arr(i),
      mm_miso => reg_miso_arr(i),

      slv_clk => dp_clk,
      slv_rst => dp_rst,

      slv_out => mm_fields_out_arr(i)
    );

    nof_pkt(i) <= mm_fields_out_arr(i)(field_hi(c_field_arr, "nof_pkt") downto field_lo(c_field_arr, "nof_pkt"));

    u_dp_merge : entity work.dp_packet_merge
    generic map (
      g_nof_pkt   => g_nof_pkt
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      nof_pkt     => nof_pkt(i),
      snk_out     => snk_out_arr(i),
      snk_in      => snk_in_arr(i),
      src_in      => src_in_arr(i),
      src_out     => src_out_arr(i)
    );
  end generate;
end str;
