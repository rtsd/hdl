-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_dc_mixed_widths is
end tb_tb_dp_fifo_dc_mixed_widths;

architecture tb of tb_tb_dp_fifo_dc_mixed_widths is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 5
  -- > run 100 us --> OK

  -- Try FIFO settings : GENERIC MAP (g_random_flow, g_narrow_period, g_wide_period, g_narrow_w, g_nof_narrow, g_use_ctrl, g_read_rl)

  -- vary clock
  u_per_10_10_frm_nof_4_rl_1     : entity work.tb_dp_fifo_dc_mixed_widths generic map (false, 10 ns, 10 ns, 16, 4, true, 1);
  u_per_10_10_frm_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 4, true, 1);
  u_per_10_13_frm_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 13 ns, 16, 4, true, 1);
  u_per_10_25_frm_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 25 ns, 16, 4, true, 1);
  u_per_13_10_frm_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 13 ns, 10 ns, 16, 4, true, 1);
  u_per_25_10_frm_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 25 ns, 10 ns, 16, 4, true, 1);

  u_per_10_10_seq_nof_4_rl_1     : entity work.tb_dp_fifo_dc_mixed_widths generic map (false, 10 ns, 10 ns, 16, 4, false, 1);
  u_per_10_10_seq_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 4, false, 1);
  u_per_10_13_seq_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 13 ns, 16, 4, false, 1);
  u_per_10_25_seq_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 25 ns, 16, 4, false, 1);
  u_per_13_10_seq_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 13 ns, 10 ns, 16, 4, false, 1);
  u_per_25_10_seq_nof_4_rl_1_rnd : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 25 ns, 10 ns, 16, 4, false, 1);

  -- vary RL
  u_frm_nof_4_rl_0               : entity work.tb_dp_fifo_dc_mixed_widths generic map (false, 10 ns, 10 ns, 16, 4, true, 0);
  u_frm_nof_4_rl_0_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 4, true, 0);
  u_seq_nof_4_rl_0               : entity work.tb_dp_fifo_dc_mixed_widths generic map (false, 10 ns, 10 ns, 16, 4, false, 0);
  u_seq_nof_4_rl_0_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 4, false, 0);

  -- vary nof_narrow per wide
  -- . commented g_nof_narrow=3, because the stratix4 architecture of common_fifo_dc_mixed_widths does not work OK then
  u_frm_nof_1_rl_1_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 1, true, 1);
  u_frm_nof_2_rl_1_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 2, true, 1);
  --u_frm_nof_3_rl_1_rnd           : ENTITY work.tb_dp_fifo_dc_mixed_widths GENERIC MAP ( TRUE, 10 ns, 10 ns, 16, 3, TRUE, 1);

  u_seq_nof_1_rl_1_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 1, false, 1);
  u_seq_nof_2_rl_1_rnd           : entity work.tb_dp_fifo_dc_mixed_widths generic map ( true, 10 ns, 10 ns, 16, 2, false, 1);
  --u_seq_nof_3_rl_1_rnd           : ENTITY work.tb_dp_fifo_dc_mixed_widths GENERIC MAP ( TRUE, 10 ns, 10 ns, 16, 3, FALSE, 1);
end tb;
