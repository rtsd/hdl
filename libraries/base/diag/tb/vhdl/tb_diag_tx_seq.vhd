--------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: test bench for diag_tx_seq control
-- Usage:
--   > as 5
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_diag_tx_seq is
end tb_diag_tx_seq;

architecture tb of tb_diag_tx_seq is
  constant c_period     : time    := 100 ns;
  constant c_nof_cycles : natural := 83;  -- use prime number to avoid periodicity with sequence data, which can confuse interpretating the data

  constant c_dat_w      : natural := 4;

  signal tb_end       : std_logic := '0';
  signal clk          : std_logic := '1';
  signal rst          : std_logic;

  signal diag_en      : std_logic;
  signal diag_sel     : std_logic;
  signal diag_req     : std_logic;
  signal seq_dat      : std_logic_vector(c_dat_w - 1 downto 0);
  signal seq_val      : std_logic;
begin
  rst <= '1', '0' after c_period / 10;
  clk <= not clk or tb_end after c_period / 2;

  stimuli: process
  begin
    diag_en  <= '0';
    diag_req <= '1';

    -- COUNTER
    diag_sel <= '1';
    wait for c_period;
    diag_en  <= '1';
    wait for c_nof_cycles * c_period;
    diag_req <= '0';
    wait for c_period;
    diag_req <= '1';
    wait for c_nof_cycles * c_period;

    -- PRSG
    diag_sel <= '0';
    wait for c_nof_cycles * c_period;

    -- COUNTER
    diag_sel <= '1';
    wait for c_nof_cycles * c_period;
    diag_en  <= '0';  -- let diag_en go low for 1 clk to restart the sequence
    wait for c_period;
    diag_en  <= '1';
    wait for c_nof_cycles * c_period;
    diag_en  <= '0';
    wait for c_period;  -- let diag_en go low for > 1 clk to restart the sequence
    wait for c_period;
    diag_en  <= '1';
    wait for c_nof_cycles * c_period;
    diag_en  <= '0';
    wait for c_period;  -- let diag_en go low for > 1 clk to restart the sequence
    wait for c_period;
    wait for c_period;
    diag_en  <= '1';
    wait for c_nof_cycles * c_period;

    -- PRSG
    diag_sel <= '0';
    wait for c_nof_cycles * c_period;
    diag_en  <= '0';
    wait for c_period;
    diag_en  <= '1';
    wait for c_nof_cycles * c_period;

    tb_end <= '1';
    wait for c_nof_cycles * c_period;
    wait;
  end process;

  u_diag_tx_seq : entity work.diag_tx_seq
  generic map (
    g_dat_w  => c_dat_w
  )
  port map (
    clk      => clk,
    rst      => rst,
    diag_en  => diag_en,
    diag_sel => diag_sel,
    diag_req => diag_req,
    out_dat  => seq_dat,
    out_val  => seq_val
  );
end tb;
