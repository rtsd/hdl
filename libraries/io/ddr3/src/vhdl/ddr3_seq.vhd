-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

library IEEE, common_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;
use work.ddr3_pkg.all;

entity ddr3_seq is
  generic (
    g_ddr3_seq : t_ddr3_seq := c_ddr3_seq;
    g_ddr      : t_c_ddr3_phy
 );
  port (
    -- Clocks and reset
    dp_rst      : in  std_logic;  -- reset synchronous with st_clk
    dp_clk      : in  std_logic;  -- other clock domain clock

    en_evt      : out std_logic;
    wr_not_rd   : out std_logic;

    start_addr  : out t_ddr3_addr;
    end_addr    : out t_ddr3_addr;

    done        : in  std_logic;
    init_done   : in  std_logic;
    ctlr_rdy    : in  std_logic;

    sync_ok_in  : in  std_logic := '0';
    sync_ok_out : out std_logic
   );
end ddr3_seq;

architecture rtl of ddr3_seq is
  constant c_blocksize     : positive := g_ddr3_seq.wr_nof_chunks * (g_ddr3_seq.wr_chunksize + g_ddr3_seq.gapsize);
  constant c_page_size     : positive := c_blocksize * g_ddr3_seq.nof_blocks;
  constant c_nof_wr_access : positive := g_ddr3_seq.wr_nof_chunks * g_ddr3_seq.nof_blocks;
  constant c_address_w     : positive := sel_a_b(ceil_log2(2 * c_page_size) > g_ddr.a_col_w, ceil_log2(2 * c_page_size), g_ddr.a_col_w);

  type   state_type is (s_idle, s_write, s_first_write, s_wait_wr, s_read, s_wait_rd);

  type reg_type is record
    rd_page_offset   : natural;
    wr_page_offset   : natural;
    ddr3_en          : std_logic;
    wr_not_rd        : std_logic;
    rd_block_offset  : natural;
    rd_chunks_offset : natural;
    rd_block_cnt     : natural;
    rd_chunks_cnt    : natural;
    wr_block_offset  : natural;
    wr_chunks_offset : natural;
    wr_block_cnt     : natural;
    wr_chunks_cnt    : natural;
    switch_cnt       : natural;  -- Counter that counts the write and read accesses to determine the switch between read and write phase.
    page_cnt         : natural;  -- Counter that counts the number of write accesses to determuine the page-swap.
    first_write      : std_logic;
    sync_ok_out      : std_logic;
    start_addr       : std_logic_vector(c_address_w - 1 downto 0);
    end_addr         : std_logic_vector(c_address_w - 1 downto 0);
    state            : state_type;  -- The state machine.
  end record;

  signal r, rin      : reg_type;
begin
  ---------------------------------------------------------------
  -- CHECK IF PROVIDED GENERICS ARE ALLOWED.
  ---------------------------------------------------------------
  assert not((g_ddr3_seq.wr_nof_chunks * g_ddr3_seq.wr_chunksize) /= (g_ddr3_seq.rd_nof_chunks * g_ddr3_seq.rd_chunksize) and rising_edge(dp_clk))
    report "Total write configuration is different from total read configuration!!!"
    severity FAILURE;

  p_comb : process(r, dp_rst, init_done, done, ctlr_rdy, sync_ok_in)
    variable v : reg_type;
  begin
    v         := r;
    v.ddr3_en := '0';

    case r.state is
      when s_idle =>
        if(init_done = '1' and sync_ok_in = '1') then
          v.first_write := '1';
          v.sync_ok_out := sync_ok_in;
          v.state       := s_first_write;
        end if;

      when s_first_write =>
        v.wr_not_rd  := '1';
        v.ddr3_en    := '1';
        v.start_addr := TO_UVEC(r.wr_page_offset + r.wr_block_offset + r.wr_chunks_offset, c_address_w);
        v.end_addr   := TO_UVEC(r.wr_page_offset + r.wr_block_offset + r.wr_chunks_offset + g_ddr3_seq.wr_chunksize-4, c_address_w);
        v.switch_cnt := r.switch_cnt + 1;
        v.state      := s_wait_wr;

      when s_write =>
        if(done = '1' and ctlr_rdy = '1') then
          v.wr_not_rd  := '1';
          if(sync_ok_in = '1') then  -- Only write when good sync pattern on the input.
            v.ddr3_en    := '1';
          end if;
          v.start_addr := TO_UVEC(r.wr_page_offset + r.wr_block_offset + r.wr_chunks_offset, c_address_w);
          v.end_addr   := TO_UVEC(r.wr_page_offset + r.wr_block_offset + r.wr_chunks_offset + g_ddr3_seq.wr_chunksize-4, c_address_w);
          v.switch_cnt := r.switch_cnt + 1;
          v.state      := s_wait_wr;
        end if;

      when s_wait_wr =>
        v.page_cnt := r.page_cnt + 1;
        if(r.wr_block_cnt = g_ddr3_seq.nof_blocks - 1 and r.wr_chunks_cnt = g_ddr3_seq.wr_nof_chunks - 1) then
          v.wr_block_offset  := 0;
          v.wr_chunks_offset := 0;
          v.wr_block_cnt     := 0;
          v.wr_chunks_cnt    := 0;
        elsif(r.wr_block_cnt = g_ddr3_seq.nof_blocks - 1) then
          v.wr_block_offset  := 0;
          v.wr_chunks_offset := r.wr_chunks_offset + g_ddr3_seq.wr_chunksize;
          v.wr_block_cnt     := 0;
          v.wr_chunks_cnt    := r.wr_chunks_cnt + 1;
        else
          v.wr_block_offset := r.wr_block_offset + c_blocksize;
          v.wr_block_cnt    := r.wr_block_cnt + 1;
        end if;

        if(r.switch_cnt = g_ddr3_seq.wr_nof_chunks) then
          v.switch_cnt := 0;
          v.state      := s_read;
        else
          v.state := s_write;
        end if;

      when s_read =>
        if(done = '1' and ctlr_rdy = '1') then
          v.wr_not_rd := '0';
          if( r.first_write = '0') then
            v.ddr3_en := '1';
          end if;
          v.start_addr  := TO_UVEC(r.rd_page_offset + r.rd_block_offset + r.rd_chunks_offset, c_address_w);
          v.end_addr    := TO_UVEC(r.rd_page_offset + r.rd_block_offset + r.rd_chunks_offset + g_ddr3_seq.rd_chunksize-4, c_address_w);
          v.switch_cnt  := r.switch_cnt + 1;
          v.state       := s_wait_rd;
          v.sync_ok_out := sync_ok_in;
        end if;

      when s_wait_rd =>
        if(r.switch_cnt = g_ddr3_seq.rd_nof_chunks) then
          v.switch_cnt := 0;
          v.state      := s_write;
        else
          v.state := s_read;
        end if;

        if(r.rd_block_cnt = g_ddr3_seq.nof_blocks - 1 and r.rd_chunks_cnt = g_ddr3_seq.rd_nof_chunks - 1) then
          v.rd_block_offset  := 0;
          v.rd_chunks_offset := 0;
          v.rd_block_cnt     := 0;
          v.rd_chunks_cnt    := 0;
          if(r.page_cnt = c_nof_wr_access) then
            v.rd_page_offset := r.wr_page_offset;
            v.wr_page_offset := r.rd_page_offset;
            v.page_cnt       := 0;
            v.first_write    := '0';
            if(sync_ok_in = '0') then
              v.state := s_idle;
            end if;
          end if;
        elsif(r.rd_block_cnt = g_ddr3_seq.nof_blocks - 1) then
          v.rd_block_offset  := 0;
          v.rd_chunks_offset := r.rd_chunks_offset + g_ddr3_seq.rd_chunksize;
          v.rd_block_cnt     := 0;
          v.rd_chunks_cnt    := r.rd_chunks_cnt + 1;
        else
          v.rd_block_offset := r.rd_block_offset + c_blocksize;
          v.rd_block_cnt    := r.rd_block_cnt + 1;
        end if;

      when others =>
        v.state := s_idle;
    end case;

    if(dp_rst = '1') then
      v.rd_page_offset   := c_page_size;
      v.wr_page_offset   := 0;
      v.page_cnt         := 0;
      v.switch_cnt       := 0;
      v.ddr3_en          := '0';
      v.wr_not_rd        := '0';
      v.wr_block_offset  := 0;
      v.wr_chunks_offset := 0;
      v.wr_block_cnt     := 0;
      v.wr_chunks_cnt    := 0;
      v.rd_block_offset  := 0;
      v.rd_chunks_offset := 0;
      v.rd_block_cnt     := 0;
      v.rd_chunks_cnt    := 0;
      v.sync_ok_out      := '0';
      v.start_addr       := (others => '0');
      v.end_addr         := (others => '0');
      v.first_write      := '1';
      v.state            := s_idle;
    end if;

    rin <= v;
  end process;

  p_regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  en_evt      <= r.ddr3_en;
  wr_not_rd   <= r.wr_not_rd;
  sync_ok_out <= r.sync_ok_out;

  address : process(r)
  begin
    start_addr        <= c_ddr3_addr_lo;
    end_addr          <= c_ddr3_addr_lo;
    start_addr.column <= r.start_addr(c_ddr3_phy.a_col_w - 1 downto 0);
    end_addr.column   <= r.end_addr(c_ddr3_phy.a_col_w - 1 downto 0);
    start_addr.row(c_address_w - c_ddr3_phy.a_col_w - 1 downto 0) <= r.start_addr(c_address_w - 1 downto c_ddr3_phy.a_col_w);
    end_addr.row(c_address_w - c_ddr3_phy.a_col_w - 1 downto 0)   <= r.end_addr(c_address_w - 1 downto c_ddr3_phy.a_col_w);
  end process;
end rtl;
