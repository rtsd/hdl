--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Split one frame into two frames.
-- Description:
--   Split an input frame into a head and a tail output frame, so the reverse
--   of dp_concat. The g_nof_symbols determines how many symbols are used for
--   the head output frame via source port 0. The rest of the symbols are
--   passed on to tail output frame via source port 1.
-- Remark:
-- . Both frames will have the same data width and symbol width.
-- . The split is done at symbol level and the output frame will have the
--   aggregated empty.
-- . The split head frame gets the current channel and error field value. The
--   error field may be undefined because the true error information is not
--   known yet as it is only available at the snk_in.eop. The split tail frame
--   gets channel and error field from the input frame.
-- . If the dynamic nof_symbols input is used, then g_nof_symbols must be set
--   to fit the largest head frame and input nof_symbols. Otherwise if
--   nof_symbols is fixed, then g_nof_symbols can be set to that value and the
--   dynamic input can be left not connected.
-- . If nof_symbols=0, then the input frame is passed on entirely to the tail
--   frame output. If nof_symbols > nof input symbols then the input frame
--   is passed on entirely to the head frame output.
-- . Allow an idle cycle between head and tail frame if the split is not at a
--   word boundary. Otherwise the statemachine would become more complicated
--   and require the use of prev_src_in to check that the src_in_arr(c_tail) was
--   ready.
-- . Allow an idle cycle between tail and next head frame to ease the state
--   machine. Hence the input data must have at least one cycle gap between
--   input frames. The state machine is complicated due to that dp_split
--   support dynamic MM control of nof_symbols over a range of 0 to max.
-- . nof_symbols_out is valid at the SOP of the head frame and gives the nof
--   symbols in this head frame. Is based on nof_symbols. Therefore
--   nof_symbols_out is only correct if the input frame has >= nof_symbols.
--
-- Design steps that were taken for dp_split (took about 3 days):
--   1 aim is a component that does the opposite of dp_concat
--   2 same steps were taken as for dp_concat
--   3 verify with tb_dp_split:
--     . always ready and input available
--     . nof_symbols = 0, 1, 2, ..., 10
--     . nof_symbols_per_data = 1, 2, 3, 4
--     . random ready
--     . random input availble
--   4 combinatorial zero-delay loops and truncation warnings wer debugged by
--     restart -f, then run x ns to just before the when the error occurs and
--     then single step until the error occurs.

entity dp_split is
  generic (
    g_data_w            : natural := 16;  -- >= 1
    g_symbol_w          : natural := 8;  -- >= 1, and g_data_w/g_symbol_w must be an integer
    g_nof_symbols       : natural := 8;  -- nof symbols for the head frame, rest will go to the tail frame
    g_use_head_tail_src : boolean := false  -- FALSE: Use src_out_arr, src_in_arr. TRUE: use separate head_src_*, tail_src_*.
  );
  port (
    rst             : in  std_logic;
    clk             : in  std_logic;
    -- Static control input (connect via MM or leave open to use default)
    nof_symbols     : in  std_logic_vector(ceil_log2(g_nof_symbols + 1) - 1 downto 0) := TO_UVEC(g_nof_symbols, ceil_log2(g_nof_symbols + 1));  -- +1 to also support power of 2 values for g_nof_symbols
    nof_symbols_out : out std_logic_vector(ceil_log2(g_nof_symbols + 1) - 1 downto 0);

    snk_out         : out t_dp_siso;
    snk_in          : in  t_dp_sosi;

    src_in_arr      : in  t_dp_siso_arr(0 to 1) := (others => c_dp_siso_rdy);
    src_out_arr     : out t_dp_sosi_arr(0 to 1);

    head_src_in     : in  t_dp_siso := c_dp_siso_rdy;  -- We don't need to create an arrayed TYPE for src_*_arr when using this output, making things easier.
    head_src_out    : out t_dp_sosi;

    tail_src_in     : in  t_dp_siso := c_dp_siso_rdy;
    tail_src_out    : out t_dp_sosi
  );
end dp_split;

architecture rtl of dp_split is
  constant c_nof_symbols_per_data  : natural := g_data_w / g_symbol_w;  -- nof symbols per data
  constant c_empty_w               : natural := ceil_log2(c_nof_symbols_per_data);

  constant c_nof_symbols_w         : natural := nof_symbols'length;
  constant c_symbol_cnt_w          : natural := ceil_log2(g_nof_symbols + 1 + c_nof_symbols_per_data);  -- total symbols count with c_nof_symbols_per_data increments, +1 to also support power of 2 value

  constant c_head                  : natural := 0;  -- head frame snk_in port number
  constant c_tail                  : natural := 1;  -- tail frame snk_in port number

  type t_state is (s_head, s_tail, s_eop);

  signal state               : t_state;
  signal nxt_state           : t_state;

  signal i_snk_out           : t_dp_siso;
  signal hold_src_in         : t_dp_siso;
  signal next_src_buf        : t_dp_sosi;
  signal src_buf             : t_dp_sosi;
  signal nxt_src_buf         : t_dp_sosi;
  signal tail                : t_dp_sosi;
  signal nxt_tail            : t_dp_sosi;
  signal i_src_out_arr       : t_dp_sosi_arr(0 to 1);
  signal nxt_src_out_arr     : t_dp_sosi_arr(0 to 1);
  signal i_src_in_arr        : t_dp_siso_arr(0 to 1);

  signal busy                : std_logic;
  signal nxt_busy            : std_logic;
  signal sync                : std_logic;
  signal nxt_sync            : std_logic;
  signal nof_symbols_reg     : std_logic_vector(c_nof_symbols_w - 1 downto 0);
  signal nxt_nof_symbols_reg : std_logic_vector(c_nof_symbols_w - 1 downto 0);
  signal symbol_cnt          : std_logic_vector(c_symbol_cnt_w - 1  downto 0);
  signal nxt_symbol_cnt      : std_logic_vector(c_symbol_cnt_w - 1  downto 0);

  signal i_nof_symbols_out   : std_logic_vector(c_nof_symbols_w - 1 downto 0);
  signal nxt_nof_symbols_out : std_logic_vector(c_nof_symbols_w - 1 downto 0);

  signal head_empty_reg      : std_logic_vector(c_empty_w - 1 downto 0);
  signal nxt_head_empty_reg  : std_logic_vector(c_empty_w - 1 downto 0);
  signal input_empty_reg     : std_logic_vector(c_empty_w - 1 downto 0);
  signal nxt_input_empty_reg : std_logic_vector(c_empty_w - 1 downto 0);
  signal nof_remain_reg      : std_logic_vector(c_empty_w - 1 downto 0);
  signal nxt_nof_remain_reg  : std_logic_vector(c_empty_w - 1 downto 0);
begin
  assert (g_data_w mod g_symbol_w) = 0
    report "dp_split : g_data_w/g_symbol_w must be an integer"
    severity FAILURE;

  snk_out <= i_snk_out;
  src_out_arr <= i_src_out_arr;

  head_src_out <= i_src_out_arr(c_head);
  tail_src_out <= i_src_out_arr(c_tail);
  i_src_in_arr <= src_in_arr when g_use_head_tail_src = false else (head_src_in, tail_src_in);

  nof_symbols_out <= i_nof_symbols_out;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      busy              <= '0';
      sync              <= '0';
      nof_symbols_reg   <= (others => '0');
      state             <= s_head;
      input_empty_reg   <= (others => '0');
      head_empty_reg    <= (others => '0');
      nof_remain_reg    <= (others => '0');
      symbol_cnt        <= (others => '0');
      tail              <= c_dp_sosi_rst;
      src_buf           <= c_dp_sosi_rst;
      i_src_out_arr     <= (others => c_dp_sosi_rst);
      i_nof_symbols_out <= (others => '0');
    elsif rising_edge(clk) then
      busy              <= nxt_busy;
      sync              <= nxt_sync;
      nof_symbols_reg   <= nxt_nof_symbols_reg;
      state             <= nxt_state;
      input_empty_reg   <= nxt_input_empty_reg;
      head_empty_reg    <= nxt_head_empty_reg;
      nof_remain_reg    <= nxt_nof_remain_reg;
      symbol_cnt        <= nxt_symbol_cnt;
      tail              <= nxt_tail;
      src_buf           <= nxt_src_buf;
      i_src_out_arr     <= nxt_src_out_arr;
      i_nof_symbols_out <= nxt_nof_symbols_out;
    end if;
  end process;

  -- Hold the sink input to be able to register the source output
  u_hold : entity work.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => snk_in,
    -- ST source
    src_in       => hold_src_in,
    next_src_out => next_src_buf,
    pend_src_out => OPEN,
    src_out_reg  => src_buf
  );

  -- Hold input register
  nxt_src_buf <= next_src_buf;

  -- Only accept new nof_symbols setting when no frame is busy being output
  nxt_nof_symbols_reg <= nof_symbols_reg when next_src_buf.valid = '1' or busy = '1' else nof_symbols;

  p_state : process(state, busy, sync, nof_symbols_reg, i_nof_symbols_out, symbol_cnt, i_src_out_arr, src_in_arr, next_src_buf, tail, input_empty_reg, head_empty_reg, nof_remain_reg)
    variable v_input_empty     : std_logic_vector(c_empty_w - 1 downto 0);
    variable v_head_empty      : std_logic_vector(c_empty_w - 1 downto 0);
    variable v_symbol_cnt      : std_logic_vector(c_symbol_cnt_w - 1 downto 0);  -- total symbols count with c_nof_symbols_per_data increments
    variable v_head_symbol_cnt : std_logic_vector(c_symbol_cnt_w - 1 downto 0);  -- head symbols count
    variable v_cnt             : std_logic_vector(c_symbol_cnt_w - 1 downto 0);
    variable v_nof_from_this   : natural range 0 to c_nof_symbols_per_data;
  begin
    -- SISO default
    i_snk_out   <= c_dp_siso_rst;  -- no input request
    hold_src_in <= c_dp_siso_rst;  -- no input request

    -- SOSI defaults
    nxt_src_out_arr(c_head)       <= i_src_out_arr(c_head);  -- default no output
    nxt_src_out_arr(c_head).sync  <= '0';
    nxt_src_out_arr(c_head).valid <= '0';
    nxt_src_out_arr(c_head).sop   <= '0';
    nxt_src_out_arr(c_head).eop   <= '0';

    nxt_src_out_arr(c_tail)       <= i_src_out_arr(c_tail);  -- default no output
    nxt_src_out_arr(c_tail).sync  <= '0';
    nxt_src_out_arr(c_tail).valid <= '0';
    nxt_src_out_arr(c_tail).sop   <= '0';
    nxt_src_out_arr(c_tail).eop   <= '0';

    nxt_nof_symbols_out <= i_nof_symbols_out;

    -- detect frame busy
    nxt_busy <= busy;
    if next_src_buf.eop = '1' then
      nxt_busy <= '0';
    elsif next_src_buf.valid = '1' then
      nxt_busy <= '1';
    end if;

    -- preserve sync at sop also for tail, so head get sync directly from snk_in. tail gets the preserved sync
    -- no need to preserve other sosi control fields channel and bsn at sop, because they keep their value during snk_in frame
    nxt_sync <= sync;
    if next_src_buf.sop = '1' then
      nxt_sync <= next_src_buf.sync;
    end if;

    -- update head symbols counter
    v_symbol_cnt := symbol_cnt;
    if next_src_buf.sop = '1' then
      v_symbol_cnt := TO_UVEC(c_nof_symbols_per_data, c_symbol_cnt_w);
      nxt_nof_symbols_out <= nof_symbols_reg;  -- Report the current nof symbols as status at sop of head frame
    elsif next_src_buf.valid = '1' then
      v_symbol_cnt := INCR_UVEC(v_symbol_cnt, c_nof_symbols_per_data);
    end if;

    v_input_empty := (others => '0');
    v_head_symbol_cnt := v_symbol_cnt;
    if next_src_buf.eop = '1' then
      v_input_empty     := next_src_buf.empty(c_empty_w - 1 downto 0);
      v_head_symbol_cnt := TO_UVEC(TO_UINT(v_symbol_cnt) - TO_UINT(v_input_empty), c_symbol_cnt_w);
    end if;

    nxt_symbol_cnt <= (others => '0');  -- default reset symbol count

    -- preserve empty information for next state
    nxt_input_empty_reg <= input_empty_reg;
    nxt_head_empty_reg  <= head_empty_reg;
    nxt_nof_remain_reg  <= nof_remain_reg;

    -- preserve tail data information for next state
    nxt_tail <= tail;  -- keep the tail data part in case the split is at symbol boundary and not at word boundary,
                       -- keep the sop of the tail output, the valid and eop of tail are not used.

    -- pass on output
    nxt_state <= state;

    case state is
      when s_head =>
        nxt_input_empty_reg <= (others => '0');  -- default assume tail will           end at word boundary
        nxt_head_empty_reg  <= (others => '0');  -- default assume tail will start         at word boundary
        nxt_nof_remain_reg  <= (others => '0');  -- default assume tail will start and end at word boundary
        nxt_tail <= c_dp_sosi_rst;
        nxt_symbol_cnt <= symbol_cnt;  -- default keep symbol count

        -- Default ready for input to head or tail dependent on nof_symbols_reg
        if TO_UINT(nof_symbols_reg) /= 0 then
          i_snk_out   <= src_in_arr(c_head);  -- default input request for head output when head src_in_arr is ready
          hold_src_in <= src_in_arr(c_head);  -- hold_src_in will flush the next_src_buf output when i_snk_out is changed to tail src_in_arr
        else
          i_snk_out   <= src_in_arr(c_tail);  -- else in special of tail only split then input request for tail output when tail src_in_arr is ready
          hold_src_in <= src_in_arr(c_tail);  -- hold_src_in will flush the next_src_buf output when i_snk_out is changed to tail src_in_arr
        end if;

        -- Act when there is valid input ready for output
        if next_src_buf.valid = '1' then
          if TO_UINT(nof_symbols_reg) = 0 then  -- special case with no output head, so tail src_in_arr ready is already being used
            -- no head output, begin tail output
            nxt_symbol_cnt <= (others => '0');  -- reset symbol count
            nxt_src_out_arr(c_tail) <= next_src_buf;  -- output tail start data immediately
            if next_src_buf.eop = '0' then
              nxt_state <= s_tail;  -- there will be more output tail data words
            end if;  -- else output tail with one data word is done, remain in state s_head
          else
            -- begin or continue output head
            if TO_UINT(nof_symbols_reg) > TO_UINT(v_symbol_cnt) then
              nxt_src_out_arr(c_head) <= next_src_buf;  -- there will be more output head data words
              nxt_symbol_cnt <= v_symbol_cnt;  -- keep symbol count for state s_head
            elsif TO_UINT(nof_symbols_reg) = TO_UINT(v_symbol_cnt) then
              -- split at word boundary
              nxt_src_out_arr(c_head)     <= next_src_buf;  -- output last head
              nxt_src_out_arr(c_head).eop <= '1';
              if next_src_buf.eop = '0' then
                i_snk_out <= src_in_arr(c_tail);  -- input request for tail output start, now depends on tail src_in_arr ready
                nxt_tail.sop <= '1';  -- keep the sop via tail, the first tail data itself will arrive with the next_src_buf
                nxt_state <= s_tail;
              end if;  -- else there will be no output tail, remain in state s_head
            elsif TO_UINT(nof_symbols_reg) >= TO_UINT(v_head_symbol_cnt) then
              nxt_src_out_arr(c_head)     <= next_src_buf;  -- output last head, there are no symbols left for the tail
              nxt_symbol_cnt <= (others => '0');  -- reset symbol count and remain in s_head
            else
              -- split at symbol boundary
              v_cnt        := TO_UVEC(TO_UINT(v_symbol_cnt) - TO_UINT(nof_symbols_reg), c_symbol_cnt_w);  -- use v_cnt to avoid truncation warning
              v_head_empty := v_cnt(c_empty_w - 1 downto 0);
              nxt_src_out_arr(c_head)       <= next_src_buf;  -- output last head
              nxt_src_out_arr(c_head).empty <= RESIZE_DP_EMPTY(v_head_empty);
              nxt_src_out_arr(c_head).eop   <= '1';
              nxt_head_empty_reg <= v_head_empty;  -- keep tail start symbol boundary
              nxt_tail           <= next_src_buf;  -- keep tail start data part
              nxt_tail.sop       <= '1';
              if next_src_buf.eop = '0' then
                i_snk_out <= src_in_arr(c_tail);  -- input request for tail output start, now depends on tail src_in_arr ready
                nxt_state <= s_tail;  -- there will be more output tail data words
              else  -- need one tail word to output the tail part
                i_snk_out <= c_dp_siso_rst;  -- no input request for at least one clock cycle, to allow change in nof_symbols_reg and for state s_eop
                nxt_tail.sync <= sync;  -- force the output tail sync for the tail start
                nxt_input_empty_reg <= v_input_empty;
                nxt_nof_remain_reg  <= TO_UVEC(TO_UINT(v_head_empty) - TO_UINT(v_input_empty), c_empty_w);
                nxt_state <= s_eop;
              end if;
            end if;
          end if;
        end if;

      when s_tail =>
        -- Default ready for input to tail
        i_snk_out <= src_in_arr(c_tail);  -- input request for tail output
        hold_src_in <= src_in_arr(c_tail);  -- input request for tail output

        -- Act when there is valid input ready for output
        if next_src_buf.valid = '1' then
          nxt_tail                <= next_src_buf;  -- keep tail data part, needed in case head had empty symbols
          v_nof_from_this := c_nof_symbols_per_data - TO_UINT(head_empty_reg);  -- default output is next_src_buf in case head had no empty symbols
          nxt_src_out_arr(c_tail) <= func_dp_data_shift(tail, next_src_buf, g_symbol_w, c_nof_symbols_per_data, v_nof_from_this);
          nxt_src_out_arr(c_tail).sop  <= tail.sop;  -- force the output tail sop for the tail start
          nxt_src_out_arr(c_tail).sync <= tail.sop and sync;  -- force the output tail sync for the tail start
          if next_src_buf.eop = '1' then
            i_snk_out <= c_dp_siso_rst;  -- no input request for at least one clock cycle, to allow change in nof_symbols_reg and/or for state s_eop
            if TO_UINT(v_input_empty) >= TO_UINT(head_empty_reg) then
                                                            -- this is the last tail output, the input eop marks the tail output eop
              nxt_src_out_arr(c_tail).empty <= RESIZE_DP_EMPTY(func_dp_empty_split(v_input_empty, head_empty_reg, c_nof_symbols_per_data));
              nxt_state <= s_head;
            else  -- need one more tail word to output the last tail part
              nxt_src_out_arr(c_tail).eop <= '0';  -- the last will be flushed from tail in state s_eop
              nxt_input_empty_reg <= v_input_empty;
              nxt_nof_remain_reg <= TO_UVEC(TO_UINT(head_empty_reg) - TO_UINT(v_input_empty), c_empty_w);
              nxt_state <= s_eop;
            end if;
          end if;
        end if;

      when others =>  -- s_eop                              -- no input request until last tail word has been output
        -- Act when tail output port is ready
        if src_in_arr(c_tail).ready = '1' then  -- finish the last tail part
          nxt_state <= s_head;  -- no input request yet, allow one more idle cycle and input request again in state s_head
          -- shift tail output because tail will have less empty symbols than the head had
          nxt_src_out_arr(c_tail) <= func_dp_data_shift_last(tail, g_symbol_w, c_nof_symbols_per_data, TO_UINT(nof_remain_reg), TO_UINT(input_empty_reg));
          nxt_src_out_arr(c_tail).empty <= RESIZE_DP_EMPTY(func_dp_empty_split(input_empty_reg, head_empty_reg, c_nof_symbols_per_data));
        end if;
    end case;  -- CASE state

    -- Pass on frame level flow control
    i_snk_out.xon   <= func_dp_stream_arr_and(src_in_arr, "XON");
    hold_src_in.xon <= func_dp_stream_arr_and(src_in_arr, "XON");
  end process;
end rtl;
