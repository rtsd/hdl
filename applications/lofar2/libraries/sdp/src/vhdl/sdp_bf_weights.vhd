-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of sdp_bf_weights in the beamformer (BF) of
--   the LOFAR2 SDPFW design.
-- Description:
-- . The sdp_bf_weights.vhd consists of mms_dp_gain_serial_arr.vhd and
--   some address counter logic to select the address of the subband weight.
-- . The sdp_bf_weights.vhd does no rounding, so output data width = input
--   data width = g_raw_dat_w. Rounding can be done after the BF adder stage.
-- Remark:
-- . sdp_bf_weights.vhd is similar to sdp_subband_equalizer.vhd. The
--   difference is that they have a different purpose.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.sdp_pkg.all;

entity sdp_bf_weights is
  generic (
    g_gains_file_name : string := "UNUSED";
    -- Use no default raw width, to force instance to set it
    g_raw_dat_w       : natural  -- default: c_sdp_W_subband;
  );
  port (
    dp_clk       : in  std_logic;
    dp_rst       : in  std_logic;

    in_sosi_arr  : in  t_dp_sosi_arr(c_sdp_N_pol_bf * c_sdp_P_pfb - 1 downto 0);
    out_sosi_arr : out t_dp_sosi_arr(c_sdp_N_pol_bf * c_sdp_P_pfb - 1 downto 0);

    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    ram_gains_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_gains_miso : out t_mem_miso
  );
end sdp_bf_weights;

architecture str of sdp_bf_weights is
  constant c_gain_addr_w : natural := ceil_log2(c_sdp_Q_fft * c_sdp_S_sub_bf);

  -- Product width, do -1 to skip double sign bit in product
  constant c_gain_out_dat_w : natural := c_sdp_W_bf_weight + g_raw_dat_w - 1;

  signal in_sosi : t_dp_sosi;
  signal cnt : natural range 0 to c_sdp_Q_fft * c_sdp_S_sub_bf - 1;
  signal gains_rd_address : std_logic_vector(c_gain_addr_w - 1 downto 0);
begin
  in_sosi <= in_sosi_arr(0);  -- use ctrl from input [0]

  ---------------------------------------------------------------
  -- Counter
  ---------------------------------------------------------------
  -- The BF weigths per PN are stored as
  -- (cint16)bf_weights[N_pol_bf][S_pn/Q_fft]_[Q_fft][S_sub_bf], but have
  -- to be applied according the subband data order
  -- [N_pol_bf][S_pn/Q_fft]_[S_sub_bf][Q_fft]. Therefore this counter
  -- has to account for this difference in order.
  p_cnt : process(dp_clk, dp_rst)
    -- Use short index variables v_Q, v_BLET names in capitals, to ease
    -- recognizing them as (loop) indices.
    variable v_Q, v_BLET : natural;
  begin
    if dp_rst = '1' then
      cnt <= 0;
      v_Q := 0;
      v_BLET := 0;
    elsif rising_edge(dp_clk) then
      if in_sosi.valid = '1' then
        if in_sosi.eop = '1' then
          v_Q := 0;
          v_BLET := 0;
        else
          if v_Q >= c_sdp_Q_fft - 1 then
            v_Q := 0;
            if v_BLET >= c_sdp_S_sub_bf - 1 then
              v_BLET := 0;
            else
              v_BLET := v_BLET + 1;
            end if;
          else
            v_Q := v_Q + 1;
          end if;
        end if;
        cnt <= v_Q * c_sdp_S_sub_bf + v_BLET;
      end if;
    end if;
  end process;
  gains_rd_address <= TO_UVEC(cnt, c_gain_addr_w);

  ---------------------------------------------------------------
  -- Gain
  ---------------------------------------------------------------
  u_mms_dp_gain_serial_arr : entity dp_lib.mms_dp_gain_serial_arr
  generic map (
    g_nof_streams     => c_sdp_N_pol_bf * c_sdp_P_pfb,
    g_nof_gains       => c_sdp_Q_fft * c_sdp_S_sub_bf,
    g_complex_data    => true,
    g_complex_gain    => true,
    g_gain_w          => c_sdp_W_bf_weight,
    g_in_dat_w        => g_raw_dat_w,
    g_out_dat_w       => c_gain_out_dat_w,
    g_gains_file_name => g_gains_file_name
  )
  port map (
    -- System
    mm_rst            =>  mm_rst,
    mm_clk            =>  mm_clk,
    dp_rst            =>  dp_rst,
    dp_clk            =>  dp_clk,

    -- MM interface
    ram_gains_mosi    =>  ram_gains_mosi,
    ram_gains_miso    =>  ram_gains_miso,

    -- ST interface
    gains_rd_address  =>  gains_rd_address,

    in_sosi_arr       =>  in_sosi_arr,
    out_sosi_arr      =>  out_sosi_arr
  );
end str;
