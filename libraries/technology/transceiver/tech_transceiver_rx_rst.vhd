--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tech_transceiver_rx_rst is
  generic(
    g_nof_gx           : natural
  );
  port(
    tr_clk             : in  std_logic;
    tr_rst             : in  std_logic;
    rec_busy           : in  std_logic;
    rx_freq_locked     : in  std_logic_vector(g_nof_gx - 1 downto 0);
    trc_rx_analog_rst  : out std_logic;
    trc_rx_digital_rst : out std_logic;
    trc_rx_rdy         : out std_logic
   );
end tech_transceiver_rx_rst;

architecture rtl of tech_transceiver_rx_rst is
  -- tLTD_auto: 4us. At 156.25MHz this equals 625 clock cycles.
  constant c_ltd_auto   : natural := 625;
  constant c_ltd_auto_w : natural := ceil_log2(c_ltd_auto);

  type t_state_enum is (s_init, s_wait_while_busy, s_wait_for_freq_lock, s_wait_for_tltd_auto, s_rx_rdy);

  signal state                    : t_state_enum;
  signal nxt_state                : t_state_enum;

  signal i_trc_rx_digital_rst     : std_logic;
  signal nxt_trc_rx_digital_rst   : std_logic;

  signal i_trc_rx_analog_rst      : std_logic;
  signal nxt_trc_rx_analog_rst    : std_logic;

  signal i_trc_rx_rdy             : std_logic;
  signal nxt_trc_rx_rdy           : std_logic;

  signal cycle_cnt                : std_logic_vector(c_ltd_auto_w - 1 downto 0);
  signal nxt_cycle_cnt            : std_logic_vector(c_ltd_auto_w - 1 downto 0);

  signal trc_rx_freq_locked       : std_logic_vector(g_nof_gx - 1 downto 0);
  signal trc_rec_busy             : std_logic;
begin
  trc_rx_digital_rst   <= i_trc_rx_digital_rst;
  trc_rx_analog_rst    <= i_trc_rx_analog_rst;
  trc_rx_rdy           <= i_trc_rx_rdy;

  p_clk : process(tr_rst, tr_clk)
  begin
    if tr_rst = '1' then
      state                <= s_init;
      i_trc_rx_analog_rst  <= '1';
      i_trc_rx_digital_rst <= '1';
      i_trc_rx_rdy         <= '0';
      cycle_cnt            <= (others => '0');
    elsif rising_edge(tr_clk) then
      state                <= nxt_state;
      i_trc_rx_digital_rst <= nxt_trc_rx_digital_rst;
      i_trc_rx_analog_rst  <= nxt_trc_rx_analog_rst;
      i_trc_rx_rdy         <= nxt_trc_rx_rdy;
      cycle_cnt            <= nxt_cycle_cnt;
    end if;
  end process;

  p_state : process(state, i_trc_rx_analog_rst, i_trc_rx_digital_rst, i_trc_rx_rdy, cycle_cnt, trc_rec_busy, trc_rx_freq_locked)
  begin
    nxt_state              <= state;
    nxt_trc_rx_analog_rst  <= i_trc_rx_analog_rst;
    nxt_trc_rx_digital_rst <= i_trc_rx_digital_rst;
    nxt_trc_rx_rdy         <= i_trc_rx_rdy;
    nxt_cycle_cnt          <= cycle_cnt;

    case state is
      when s_init =>
         nxt_state <= s_wait_for_freq_lock;

      when s_wait_while_busy =>  -- Make sure there is no ALTGX reconfiguring going on.
        if trc_rec_busy = '0' then
          nxt_state <= s_wait_for_freq_lock;
        end if;

      when s_wait_for_freq_lock =>  -- Release analog receiver portion and wait for it to lock a an incoming frequency
        nxt_trc_rx_analog_rst <= '0';
        if andv(trc_rx_freq_locked) = '1' then
          nxt_state <= s_wait_for_tltd_auto;
        end if;

      when s_wait_for_tltd_auto =>  -- Wait for the parallel clock to be stable
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_ltd_auto, c_ltd_auto_w) then
          nxt_trc_rx_digital_rst <= '0';
          nxt_state              <= s_rx_rdy;
        end if;

      when s_rx_rdy =>  -- Transmitter ready to use.
        nxt_trc_rx_rdy <= '1';

      when others =>
        nxt_state <= s_wait_while_busy;
    end case;
  end process;

  u_async_busy : entity common_lib.common_async
  generic map(
    g_rst_level => '0'
  )
  port map(
    rst  => tr_rst,
    clk  => tr_clk,
    din  => rec_busy,
    dout => trc_rec_busy
  );

  gen_asyncs: for i in 0 to g_nof_gx - 1 generate
    u_async_rx_freqlocked: entity common_lib.common_async
      generic map(
        g_rst_level => '0'
      )
      port map(
        rst  => tr_rst,
        clk  => tr_clk,
        din  => rx_freq_locked(i),
        dout => trc_rx_freq_locked(i)
      );
  end generate;
end rtl;
