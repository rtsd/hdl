%% plot_poly_spectrum.m
%
% Plots 
%
%% Input
% a(t,chans):  matrix of channelized samples, rows = time, cols = channels
% nc: Number of spectral points computed for each channel
% 
%% Output
%
% f(nc,chans): frequency points
% s(nc,chans): Power 
%
%% Code
function [f, s]=plot_poly_spect(a, nc, ovs)
  nc1 = size(a,2);
  f=zeros(nc,nc1);
  s=zeros(nc,nc1);
  j1=nc/2;
  norm = 0.5/nc1;
  for i=1:nc1
     s(:,i) = circshift(fft_auto(a(:,i), nc),j1); 
     for j = 1:nc
       f(j,i) = ((i-1)+ovs/nc*(-j+1+nc/2))*norm;
     end
  end
  plot(f,abs(s))
  xlabel 'Frequency'
  ylabel 'Amplitude'
end
