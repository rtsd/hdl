-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Single clock FIFO

library IEEE, technology_lib, tech_fifo_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_fifo_sc is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_note_is_ful : boolean := true;  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
    g_fail_rd_emp : boolean := false;  -- when TRUE report FAILURE when read from an empty FIFO
    g_use_lut     : boolean := false;  -- when TRUE then force using LUTs via Altera eab="OFF",
                                       -- else use default eab="ON" and ram_block_type="AUTO", default ram_block_type="AUTO" is sufficient, because
                                       --      there seems no need to force using RAM and there are two types of Stratix IV RAM (M9K and M144K)
    g_reset       : boolean := false;  -- when TRUE release FIFO reset some cycles after rst release, else use rst directly
    g_init        : boolean := false;  -- when TRUE force wr_req inactive for some cycles after FIFO reset release, else use wr_req as is
    g_dat_w       : natural := 36;  -- 36 * 256 = 1 M9K
    g_nof_words   : natural := c_bram_m9k_fifo_depth;
    g_af_margin   : natural := 0  -- FIFO almost full margin for wr_aful flagging
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    wr_dat   : in  std_logic_vector(g_dat_w - 1 downto 0);
    wr_req   : in  std_logic;
    wr_ful   : out std_logic;
    wr_aful  : out std_logic;  -- registered FIFO almost full flag
    rd_dat   : out std_logic_vector(g_dat_w - 1 downto 0);
    rd_req   : in  std_logic;
    rd_emp   : out std_logic;
    rd_val   : out std_logic;
    usedw    : out std_logic_vector(ceil_log2(g_nof_words) - 1 downto 0)
  );
end common_fifo_sc;

architecture str of common_fifo_sc is
  constant c_use_eab          : string := sel_a_b(g_use_lut, "OFF", "ON");  -- when g_use_lut=TRUE then force using LUTs via Altera eab="OFF", else default to ram_block_type = "AUTO"

  constant c_fifo_af_latency  : natural := 1;  -- pipeline register wr_aful
  constant c_fifo_af_margin   : natural := g_af_margin + c_fifo_af_latency;  -- FIFO almost full level

  signal fifo_rst        : std_logic;
  signal fifo_init       : std_logic;
  signal fifo_wr_en      : std_logic;
  signal nxt_fifo_wr_en  : std_logic;
  signal fifo_wr_dat     : std_logic_vector(g_dat_w - 1 downto 0);
  signal nxt_fifo_wr_dat : std_logic_vector(fifo_wr_dat'range);
  signal fifo_rd_en      : std_logic;
  signal fifo_full       : std_logic;
  signal fifo_empty      : std_logic;
  signal fifo_usedw      : std_logic_vector(usedw'range);

  signal nxt_wr_aful     : std_logic;
  signal nxt_rd_val      : std_logic;
begin
  -- Control logic copied from common_fifo_sc(virtex4).vhd

  gen_fifo_rst : if g_reset = true generate
    -- Make sure the reset lasts at least 3 cycles (see fifo_generator_ug175.pdf). This is necessary in case
    -- the FIFO reset is also used functionally to flush it, so not only after power up.
    u_fifo_rst : entity work.common_areset
    generic map (
      g_rst_level => '1',
      g_delay_len => 4,
      g_tree_len  => 0  -- assume if necessary g_tree_len > 0 is covered via input rst
    )
    port map (
      in_rst    => rst,
      clk       => clk,
      out_rst   => fifo_rst
    );
  end generate;

  no_fifo_rst : if g_reset = false generate
    fifo_rst <= rst;
  end generate;

  gen_init : if g_init = true generate
    -- Wait at least 3 cycles after reset release before allowing fifo_wr_en (see fifo_generator_ug175.pdf)
    u_fifo_init : entity work.common_areset
    generic map (
      g_rst_level => '1',
      g_delay_len => 4,
      g_tree_len  => 0
    )
    port map (
      in_rst    => fifo_rst,
      clk       => clk,
      out_rst   => fifo_init
    );

    p_init_reg : process(fifo_rst, clk)
    begin
      if fifo_rst = '1' then
        fifo_wr_en  <= '0';
      elsif rising_edge(clk) then
        fifo_wr_dat <= nxt_fifo_wr_dat;
        fifo_wr_en  <= nxt_fifo_wr_en;
      end if;
    end process;

    nxt_fifo_wr_dat <= wr_dat;
    nxt_fifo_wr_en  <= wr_req and not fifo_init;  -- check on NOT full is not necessary according to fifo_generator_ug175.pdf
  end generate;

  no_init : if g_init = false generate
    fifo_wr_dat <= wr_dat;
    fifo_wr_en  <= wr_req;  -- check on NOT full is not necessary according to fifo_generator_ug175.pdf
  end generate;

  wr_ful <= fifo_full;
  rd_emp <= fifo_empty;
  usedw  <= fifo_usedw;

  fifo_rd_en <= rd_req;  -- check on NOT empty is not necessary according to fifo_generator_ds317.pdf, so skip it to easy synthesis timing

  nxt_rd_val <= fifo_rd_en and not fifo_empty;  -- check on NOT empty is necessary for rd_val

  nxt_wr_aful <= '0' when TO_UINT(fifo_usedw) < g_nof_words - c_fifo_af_margin else '1';

  p_clk : process(fifo_rst, clk)
  begin
    if fifo_rst = '1' then
      wr_aful <= '0';
      rd_val  <= '0';
    elsif rising_edge(clk) then
      wr_aful <= nxt_wr_aful;
      rd_val  <= nxt_rd_val;
    end if;
  end process;

  -- 0 < some threshold < usedw          < g_nof_words can be used as FIFO almost_full
  -- 0 <          usedw < some threshold < g_nof_words can be used as FIFO almost_empty
  u_fifo : entity tech_fifo_lib.tech_fifo_sc
  generic map (
    g_technology => g_technology,
    g_use_eab    => c_use_eab,
    g_dat_w      => g_dat_w,
    g_nof_words  => g_nof_words
  )
  port map (
    aclr    => fifo_rst,
    clock   => clk,
    data    => fifo_wr_dat,
    rdreq   => fifo_rd_en,
    wrreq   => fifo_wr_en,
    empty   => fifo_empty,
    full    => fifo_full,
    q       => rd_dat,
    usedw   => fifo_usedw
  );

  proc_common_fifo_asserts("common_fifo_sc", g_note_is_ful, g_fail_rd_emp, fifo_rst, clk, fifo_full, fifo_wr_en, clk, fifo_empty, fifo_rd_en);
end str;
