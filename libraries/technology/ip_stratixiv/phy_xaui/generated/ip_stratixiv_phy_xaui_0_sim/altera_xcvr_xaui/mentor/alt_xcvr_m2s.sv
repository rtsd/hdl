// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
HS+I8naY9DPGVQxPrII2Frh1VJghpPubC5j5F0w2tZKpYlOfA9o38NqMkGQvPRYb
ixx79iSmdoLQVAlfKvGITv++cqXK8yAHwZm2NwuCEOeEMw5LYL/hbU8LJB6+Ckv9
Smz71GMpWXutaeBQTP3kCIKoCml/V7N7Z/k/VCdY0lg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2592)
5r/HMrfpfAS3xARr91zeCBVm/Wg9g3j7Raz2VOHJb217xDWTkdu5vZR054h/UyTO
hJR6wJ5QnUa7ADBrycZVRXbKLvima8YMM6zVBWTOvYzpEXH0dEKX4Ja58GPR/pXG
hfFpyv7WNbUV8M03dtroGqE/f7OLCfZwgaBv2V1vF/Di8etUrmSDiCCcVtQ0m46k
wQkUDdeKDOXbtCPvc7nFFd84cwFKhQDAE+bKjIDEGTM9/UPApa59+jkk6IRWN9IK
eZFbJDR8XhnQoye3/G1CipwgMVsPjEz4Yq+FEZVNflT7sreXf1LSkFhp8VjUClnW
PCkctqapAFY+8F8BCOFMMGyEOdQc8GKFL5OBJKQbE4O0R26grAu6MMEpyS0EnopV
5M6k7ikzKngRdJAe+B3ICsjf4f9QqXKOXWKm+3FqNAZGEWoga/5EgaW7nqvO6kBV
x5oaZbrQCovisxQUSuh8zD3jENG4vw6f8I0pofS8b/TAk2JUWa284HyE9YtBJK1k
OSzOwoIrbpL5xYecFEaewqy2TesWYvLNiou4Cq4SXxHdKwZTBUnHikkow73+l4Rj
HEcROG8/Ko+WUUPRauXRTtNOCL+AAJMSRWldYb1R74DY/lkmps7KJooJhpQooffF
aY/dBUj/OhQXIEa3h8RUx4+h1MVJS1p2cY/r4IzrnCox2G1yE8YVDa7BNmzUOOUE
/cs6fxceInzuez9NTRz40xpEh9mY9bkYFFYJPYy7PjAEZ55J/kYJU4VpTAI99uqC
SOwOP8XIqpPw0rEVJZ7rFg/8ksmcsJXOlw0YhlEK44stmbLshIMyGy3shbFZRH99
l72KSiW5POrjbDg3RqPkRBAg6+PlDzN0aABsP9LqsKU27ZxIZnoLly27jHc9XAtf
4h63uKbsE0V8VrW4nvMZBtHQAgYtnEn+s/Pz4uEZLQUITS+61TuAUyjhCEXD3PvY
S6XICQSllHzpUq4rBR04OEEVVqtVflpGmKmHyzFVPET2hq4v1Cubzh4QIRjx7eX0
Nrm9MgVEjeC0MHjmJY19TLgveX6CIPaTa/zx94lCkAu8jOaeztxqkiOR9bOoFQ2a
sKWnfBfMKjv+JOhbAZPBQMNMftViqCC6M202C9rKDS1wFk6pGHn1O/PbsTLV71mi
/ev/kR5OuH6GyCs+27fwl2G97+2KwCCHTSf0fFXv+B2f9JCFBpmixlbb3HIT0F6r
FEVbhpAzpUsGRwOVkMtcT0f9ApZu4DoyVQ1S5yo6Mp8e21T+DXNGH+F3ZLrW9vRh
0erUnt5dwzZlT3SS/GGlGQ7FAp1uPCHsdEu9frQ5xt/RwZ0SkPSK559xmpV42JmG
pN+HWG3VtGXzyBl3Ym97NB3XLcYeSyG+mvGvoFWxRRly0Ly2aTaDpOo8gsDzujiy
LNUxevFtf1gJD8YDpjuf6/m8MI4BKOP5YpUXYu0L3JuSUScV+bsd7o/3R6StKH+i
my09IkI7gbqCfRsu/aqUJWl3QWkGEiT97/+q4TdAmOXt5ffDd4XiBjYJg4ME7Bgv
FdeeFSH/3/TRUbBqLZPjN/SUSakeQY7WJVcyPLPZ4wWIFg15ABrqHPfpZ5TdhMC5
hkX2OOpUx3JTm95kdbgOUXmh6kByQp1Qgts2HuMAlQwHnh5iafiSS0qZR2gFtYpt
qQyYfyvUeG1nWH7+wJa2mz5evzoM/de0OTXt3EMvbpDB8ggb+Qd/VLLvp/do7LkM
tpA9ExBqX0aWCo8S+sT1xInMdBKH2IU9RVBJ2XGuIjJA+GjCE/TYsePGYFpX/Eq1
2OtjnTToxn/RICLwXkEH2uuUS7iAn6ekNuMXcPm56hwX+2wElgt6/6HkzQT8ApL6
HJjSfWQ1G15xJL1b7/m5deplMs2Gz66kbjYGLQPYbagDe9S9rRGwBksPoQvTZ7xB
OhddEK0oPgztj4vuYFZOtTJDP3Lgb+/qG6aZ1t/d+t7GLke7OMIf0bJ3lJvxghwf
cy1S/aGXr3ASxfcPJ49nSHKL0A+zg6D6Fa1ByzdbZnk5vcrz0DHCaHSj9b3Hgyy0
IXnHA3Jh4GK5X5Am76PDE8VG3RXAZEAbKibvrC/Z2glhFA8I0OEit90WpwmCB5c2
FAixbpmM7l7JcatZcrQf8GAoziqCdNxikvKlHXRLVg1MtfBKARsL0cqKCkEiYSHa
z49FdMTlhj9Pa+IwzZKzUblJfGRItdDeOXDj061MpsfKxF8xpuBhIEqIu6VP3kSY
+C+GSLZQmFZWF1AzpWxuz1jA7mPcvLKoNgjpb1MZ+/3nco6gUyyfkLuCD6H9Tipi
z3+VdIa/2wfiy8rQ5vQM6pVdRyoqZI3Lwb6zuN7mTYIHDnrvxlSbLfw9OsohvAuf
mz53uRqYJ6Sa7x/lidiVkHXpbsHeMLkj/zOigtTWSbQtAclBVvj0ou+G7wKcTRn4
J2diVGjKwfB+1TQ6WUTvZi9dHnvJl6klk7duOBXSxLcKuZZ8WXDPiGHfPLLKBePJ
8fcx9wwuEqOJBWjas2eEcXMMJKzHJ52idlNEkmZUljnS7mTZ86jAsoZYn4op+h0x
HnuxOXveRLpPdIgp+QcqGzo/RO8gyg8t7yZFi6/7+/3/dsFCjkSKzyTJLMXtAPOX
x+2I5+p+jfQ17s+N8Fq5fC172qhI+hiGzoiv3JM0xSDBPkoF42rrOqJQrqRyhP+7
qfy48WadQvNQ14kVx67Egxc8gKniRkan00rYJY4g5FVFb6zMBtqjjgihywZX0e/A
/BanTvvhCOYbuJZjE+x75XO9q1gXhTMMfHyi6ekX/7EokDWaokOoVt9O3+dXBzCj
6b9DLkK+QPfiN9mrQ5nkPhvLhiFSdL9hzTC9ZJOpdXX0bwYbR5vr7aOQ6CMKzPMX
AOrjZpPR+BqLf+KQ58OdXAoLGr9r6oFf1w5XtggPcxwhipyjS7iKeVmKyO/goLmc
ykVw30UhDGci63qwCgTQLN48RNSrQmhbLLhWUN77CEPAV418nZBbzagGH2lMDVLH
dk1mxvkuuA4P16XhEhs5Kz47C9re3E3L1Iz8DIVmUcFjAkV03YsaXwlg8RJFshiZ
oMoGztc1M1A5aSD/+j0U/5+VKu8gnC8/vFs8+6RLJ/+tuef+VJxiYWdgB8Rd1XFU
NRyp5pV76gWTwOkLPuAH8okEjyhAV8QsrLWKU5QneSkvkvMmc/J32tv63aeuA7rO
OOPEEN7f3rApbyKLDd2PZSCLjr+uiFrr3EJRLwjwW23kjUYvNs4mG5tSIJ7SUqiA
Cbn34YzwYpxkbCgN45Ap+Ly4DnDb2ZB12dV/S7RxHeI0fVTkVynOa8uoBESTvDon
kCZHFr8vkcuRKv77EiULGBuZa5A0Q7cWGtkncjwe1ROexeiAjJHQ38my3Z2Jrqyf
iDgazn1HrvrsnvccMo9FRJm7jmQycvGFtXczDfOHUdwfsjo8NfHeKoAVtCpGnJEt
`pragma protect end_protected
