-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author : E. Kooistra, 7 apr 2017
-- Purpose : Control force data per stream via MM
-- Description:
--   Stream Address   Data  Access  Description
--      0      0        [0]     R/W  force enable or default disable for data pass on
--      0      1     [31:0]     R/W  force sosi data
--      0      2     [31:0]     R/W  force sosi re
--      0      3     [31:0]     R/W  force sosi im
--
--      1     4:7    idem
--
--      2     8:11   idem
--      etc.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_force_data_parallel_arr is
  generic (
    g_nof_streams           : natural := 1;
    g_dat_w                 : natural := 32;  -- must be <= 32 to fit INTEGER range
    g_increment_data        : integer := 0;
    g_increment_re          : integer := 0;
    g_increment_im          : integer := 0;
    g_increment_data_on_sop : boolean := false;
    g_increment_re_on_sop   : boolean := false;
    g_increment_im_on_sop   : boolean := false;
    g_restart_data_on_sync  : boolean := false;
    g_restart_re_on_sync    : boolean := false;
    g_restart_im_on_sync    : boolean := false;
    g_restart_data_on_sop   : boolean := false;
    g_restart_re_on_sop     : boolean := false;
    g_restart_im_on_sop     : boolean := false
  );
  port (
    -- Clocks and reset
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;
    -- MM control
    reg_force_data_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_force_data_miso : out t_mem_miso;
    -- ST sink
    snk_out_arr         : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr          : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr          : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr         : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_force_data_parallel_arr;

architecture str of mms_dp_force_data_parallel_arr is
  constant c_mm_reg_adr_w : natural := 2;  -- = ceil_log2(c_mm_reg.adr_w), copied from mms_dp_force_data_parallel.vhd

  signal reg_force_data_mosi_arr : t_mem_mosi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_mosi_rst);
  signal reg_force_data_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_mm_reg_adr_w
  )
  port map (
    mosi     => reg_force_data_mosi,
    miso     => reg_force_data_miso,
    mosi_arr => reg_force_data_mosi_arr,
    miso_arr => reg_force_data_miso_arr
  );

  gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
    u_mms_dp_force_data_parallel : entity work.mms_dp_force_data_parallel
    generic map (
      g_dat_w                 => g_dat_w,
      g_increment_data        => g_increment_data,
      g_increment_re          => g_increment_re,
      g_increment_im          => g_increment_im,
      g_increment_data_on_sop => g_increment_data_on_sop,
      g_increment_re_on_sop   => g_increment_re_on_sop,
      g_increment_im_on_sop   => g_increment_im_on_sop,
      g_restart_data_on_sync  => g_restart_data_on_sync,
      g_restart_re_on_sync    => g_restart_re_on_sync,
      g_restart_im_on_sync    => g_restart_im_on_sync,
      g_restart_data_on_sop   => g_restart_data_on_sop,
      g_restart_re_on_sop     => g_restart_re_on_sop,
      g_restart_im_on_sop     => g_restart_im_on_sop
    )
    port map (
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,
      -- MM control
      reg_force_data_mosi => reg_force_data_mosi_arr(I),
      reg_force_data_miso => reg_force_data_miso_arr(I),
      -- ST sink
      snk_out             => snk_out_arr(I),
      snk_in              => snk_in_arr(I),
      -- ST source
      src_in              => src_in_arr(I),
      src_out             => src_out_arr(I)
    );
  end generate;
end str;
