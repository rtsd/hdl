-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
--   Can be used to calculate and insert the checksum for the IP header
--   in a network package, if the correct hdr_fields_slv_in is provided.
-- Description:
--   Determine the 16 bit 1-complement checksum according IPv4 to for the
--   hdr_fields_slv_in.
--   After calculation, the checksum is inserted in the outgoing stream at
--   corresponding position based on g_hdr_field_arr and g_data_w.
-- Remarks:
--  The hdr_fields_slv_in should be valid on the snk_in.sop

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity eth_ip_header_checksum is
  generic (
    g_data_w        : natural := 64;
    g_hdr_field_arr : t_common_field_arr
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;

    src_out       : out t_dp_sosi;
    snk_in        : in  t_dp_sosi;

    src_in        : in  t_dp_siso;
    snk_out       : out t_dp_siso;

    hdr_fields_slv_in : std_logic_vector(1023 downto 0) := (others => '0')
  );
end eth_ip_header_checksum;

architecture rtl of eth_ip_header_checksum is
  constant c_hdr_len         : natural := field_slv_len(g_hdr_field_arr);
  constant c_hdr_crc_bit_hi  : natural := field_hi(g_hdr_field_arr, "ip_header_checksum");
  constant c_hdr_crc_bit_lo  : natural := field_lo(g_hdr_field_arr, "ip_header_checksum");

  -- calculate which word(s) of the incoming snk_in stream should contain the checksum.
  constant c_hdr_crc_word_hi : natural := sel_a_b((c_hdr_crc_bit_hi / g_data_w) > 0, c_hdr_crc_bit_hi / g_data_w,
                                          sel_a_b( c_hdr_crc_bit_hi > (c_hdr_len mod g_data_w), 1, 0)); --special case as the last hdr word can be < g_data_w.
  constant c_hdr_crc_word_lo : natural := sel_a_b((c_hdr_crc_bit_lo / g_data_w) > 0, c_hdr_crc_bit_lo / g_data_w,
                                          sel_a_b( c_hdr_crc_bit_lo > (c_hdr_len mod g_data_w), 1, 0)); --special case as the last hdr word can be < g_data_w.

  -- calculate in which bit range of the selected word(s) the checksum should go.
  constant c_crc_hi_bit_in_word : natural := g_data_w - ((c_hdr_len - c_hdr_crc_bit_hi) mod g_data_w);
  constant c_crc_lo_bit_in_word : natural := (g_data_w - ((c_hdr_len - c_hdr_crc_bit_lo) mod g_data_w)) mod g_data_w;

  constant c_hdr_nof_words      : natural := ceil_div(c_hdr_len, g_data_w);
  constant c_crc_word_span      : natural := 1 + c_hdr_crc_word_hi - c_hdr_crc_word_lo;
  constant c_crc_in_one_word    : boolean := c_crc_word_span = 1;

  signal checksum            : std_logic_vector(c_network_ip_header_checksum_w - 1 downto 0) := (others => '0');
  signal count               : std_logic_vector(31 downto 0);
  signal dp_pipeline_src_out : t_dp_sosi;
  signal reg_done            : std_logic := '0';
  signal nxt_reg_done        : std_logic := '0';
begin
  -- calculate checksum
  checksum <= func_network_ip_header_checksum(g_hdr_field_arr, hdr_fields_slv_in) when rising_edge(clk);

  -- register to know when crc has been inserted.
  reg_done <= nxt_reg_done when rising_edge(clk);

  ---------------------------------------------------
  -- process to insert checksum in outgoing stream --
  ---------------------------------------------------
  gen_insert_crc_one : if c_crc_in_one_word generate -- checksum is in 1 word.
    p_insert_crc_one : process(dp_pipeline_src_out, checksum, count, reg_done)
      variable v_count : natural := 0;
    begin
      v_count := TO_UINT(count);
      src_out <= dp_pipeline_src_out;
      nxt_reg_done <= reg_done;
      if reg_done = '0' and dp_pipeline_src_out.valid = '1' and v_count = c_hdr_crc_word_hi then
        src_out.data(c_crc_hi_bit_in_word downto c_crc_lo_bit_in_word) <= checksum;
        nxt_reg_done <= '1';
      end if;

      if reg_done = '1' and dp_pipeline_src_out.eop = '1' then
        nxt_reg_done <= '0';
      end if;
    end process;
  end generate;

  gen_insert_crc_multi : if not c_crc_in_one_word generate
    p_insert_crc_multi : process(dp_pipeline_src_out, checksum, count, reg_done)
      variable v_count : natural := 0;
      variable v_hi    : natural := 0;
      variable v_lo    : natural := 0;
    begin
      v_count := TO_UINT(count);
      src_out <= dp_pipeline_src_out;
      nxt_reg_done <= reg_done;
      if reg_done = '0' and dp_pipeline_src_out.valid = '1' then
        if v_count = c_hdr_crc_word_hi then
          src_out.data(c_crc_hi_bit_in_word downto 0) <= checksum(c_network_ip_header_checksum_w - 1 downto c_network_ip_header_checksum_w - c_crc_hi_bit_in_word - 1);
        elsif v_count = c_hdr_crc_word_lo then
          src_out.data(g_data_w - 1 downto c_crc_lo_bit_in_word) <= checksum(g_data_w - c_crc_lo_bit_in_word - 1 downto 0);
          nxt_reg_done <= '1';
        elsif v_count < c_hdr_crc_word_hi and v_count > c_hdr_crc_word_lo then
          v_hi := c_network_ip_header_checksum_w - 1 - c_crc_hi_bit_in_word - 1 - g_data_w * (c_hdr_crc_word_hi - v_count - 1);
          v_lo := v_hi + 1 - g_data_w;
          src_out.data(g_data_w - 1 downto 0) <= checksum(v_hi downto v_lo);
        end if;
      end if;

      if reg_done = '1' and dp_pipeline_src_out.eop = '1' then
        nxt_reg_done <= '0';
      end if;
    end process;
  end generate;

  ------------------------------------------------------------------------------------------
  -- using common_counter to keep track of the word alignment during checksum calculation --
  ------------------------------------------------------------------------------------------
  u_calc_counter : entity common_lib.common_counter
  generic map (
    g_init      => c_hdr_nof_words - 1,
    g_step_size => -1
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_ld  => snk_in.sop,
    cnt_en  => snk_in.valid,
    count   => count
  );

  -------------------------------------------------------------------------------
  -- using dp_pipeline to make room for the checksum calculation and insertion --
  -------------------------------------------------------------------------------
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline   => 1 -- fixed to 1 as common_counter has fixed latency of 1 (cannot be higher)
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => snk_in,
    -- ST source
    src_in       => src_in,
    src_out      => dp_pipeline_src_out
  );
end rtl;
