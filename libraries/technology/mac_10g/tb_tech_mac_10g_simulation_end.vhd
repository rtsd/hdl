-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench end control.
-- Description:
--    Especially suitable for the tb that reuse the tb_tech_mac_10g_* stimuli
--    components and verification components.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_tech_mac_10g_simulation_end is
  generic (
    g_tb_end            : boolean := true;
    g_nof_clk_to_rx_end : natural := 1000
  );
  port (
    clk       : in  std_logic;
    tx_end    : in  std_logic;
    rx_end    : out std_logic;
    tb_end    : out std_logic
  );
end tb_tech_mac_10g_simulation_end;

architecture tb of tb_tech_mac_10g_simulation_end is
begin
  p_tb_end : process
  begin
    tb_end <= '0';
    rx_end <= '0';
    wait until tx_end = '1';
    proc_common_wait_some_cycles(clk, g_nof_clk_to_rx_end);
    rx_end <= '1';
    proc_common_wait_some_cycles(clk, 100);

    -- Stop the simulation
    tb_end <= '1';
    proc_common_wait_some_cycles(clk, 1);
    if g_tb_end = false then
      report "Tb simulation finished."
        severity NOTE;
    else
      report "Tb simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;
end tb;
