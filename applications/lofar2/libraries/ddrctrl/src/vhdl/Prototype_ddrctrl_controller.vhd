-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: controller for ddrctrl, decides when to write when to read or when to stop writing or reading.
--
-- Description:
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--

library IEEE, dp_lib, common_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity ddrctrl_controller is
  generic (
    g_tech_ddr                : t_c_tech_ddr;
    g_stop_percentage         : natural     := 50;
    g_nof_streams             : natural;  -- 12
    g_out_data_w              : natural;  -- 14
    g_wr_data_w               : natural;  -- 168
    g_rd_fifo_depth           : natural;  -- 256
    g_rd_data_w               : natural;  -- 256
    g_block_size              : natural;  -- 1024
    g_wr_fifo_uw_w            : natural;  -- 8
    g_rd_fifo_uw_w            : natural;  -- 8
    g_max_adr                 : natural;  -- 16128
    g_burstsize               : natural;  -- 64
    g_last_burstsize          : natural;  -- 18
    g_adr_per_b               : natural;  -- 299
    g_bim                     : natural  -- 54
  );
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;

    -- ddrctrl_input
    inp_of                    : in  natural;
    inp_sosi                  : in  t_dp_sosi;
    inp_adr                   : in  natural;
    inp_bsn_adr               : in  natural;
    inp_data_stopped          : in  std_logic;
    rst_ddrctrl_input         : out std_logic;

    -- io_ddr
    dvr_mosi                  : out t_mem_ctlr_mosi;
    dvr_miso                  : in  t_mem_ctlr_miso;
    wr_sosi                   : out t_dp_sosi;
    wr_fifo_usedw             : in  std_logic_vector(g_wr_fifo_uw_w - 1 downto 0);
    rd_fifo_usedw             : in  std_logic_vector(g_rd_fifo_uw_w - 1 downto 0);

    -- ddrctrl_output
    outp_bsn                  : out std_logic_vector(c_dp_stream_bsn_w - 1 downto 0)  := (others => '0');

    -- ddrctrl_controller
    stop_in                   : in  std_logic;
    stop_out                  : out std_logic
  );
end ddrctrl_controller;

architecture rtl of ddrctrl_controller is
  constant  c_bitshift_w      : natural                                     := ceil_log2(g_burstsize);  -- bitshift to make sure there is only a burst start at a interval of c_burstsize.
  constant  c_adr_w           : natural                                     := func_tech_ddr_ctlr_address_w( g_tech_ddr );  -- the lengt of the address vector, for simulation this is smaller, otherwise the simulation would take to long, 27
  constant  c_pof_ma          : natural                                     := (((g_max_adr * (100 - g_stop_percentage)) / 100) / g_adr_per_b) * g_adr_per_b;  -- percentage of max address.

  constant  c_zeros           : std_logic_vector(c_bitshift_w - 1 downto 0)   := (others => '0');

  -- constant for reading

  constant  c_rd_data_w       : natural                                     := g_nof_streams * g_out_data_w;  -- 168
  constant  c_rest            : natural                                     := c_rd_data_w - (g_wr_data_w mod c_rd_data_w);  -- 96
  constant  c_io_ddr_data_w   : natural                                     := func_tech_ddr_ctlr_data_w(g_tech_ddr);  -- 576

  -- type for statemachine
  type t_state is (RESET, STOP_READING, WAIT_FOR_SOP, WRITING, SET_STOP, STOP_WRITING, LAST_WRITE_BURST, START_READING, READING);

  -- record for readability
  type t_reg is record
  -- state of program
  state                       : t_state;
  started                     : std_logic;

  -- stopping signals
  filled_mem                  : std_logic;
  ready_for_set_stop          : std_logic;
  stop_adr                    : std_logic_vector(c_adr_w - 1 downto 0);
  last_adr_to_write_to        : std_logic_vector(c_adr_w - 1 downto 0);
  stop_burstsize              : natural;
  stopped                     : std_logic;
  rst_ddrctrl_input           : std_logic;

  -- writing signals
  wr_burst_en                 : std_logic;
  wr_bursts_ready             : natural;

  -- reading signals
  outp_bsn                    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  read_adr                    : natural;
  rd_burst_en                 : std_logic;

  -- output
  dvr_mosi                    : t_mem_ctlr_mosi;
  wr_sosi                     : t_dp_sosi;
  end record;

  constant c_t_reg_init       : t_reg         := (RESET, '0', '0', '0', TO_UVEC(g_max_adr, c_adr_w), (others => '0'), 0, '1', '1', '0', 0, (others => '0'), 0, '1', c_mem_ctlr_mosi_rst, c_dp_sosi_init);

  -- signals for readability
  signal d_reg                : t_reg         := c_t_reg_init;
  signal q_reg                : t_reg         := c_t_reg_init;
begin
  q_reg <= d_reg when rising_edge(clk);

  -- put the input data into c_v and fill the output vector from c_v
  p_state : process(q_reg, rst, inp_of, inp_sosi, inp_adr, inp_bsn_adr, inp_data_stopped, dvr_miso, rd_fifo_usedw, wr_fifo_usedw, stop_in)

    variable v                : t_reg         := c_t_reg_init;
  begin
    v         := q_reg;
    v.wr_sosi := inp_sosi;

    case q_reg.state is
    when RESET =>
      v                                                           := c_t_reg_init;
      v.dvr_mosi.burstbegin                                       := '1';
      v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0)  := (others => '0');
      v.dvr_mosi.wr                                               := '1';
      v.wr_sosi.valid                                             := '1';

      if rst = '0' then
        v.state := STOP_READING;
      end if;

    when STOP_READING =>
      -- this is the last read burst, this make sure every data containing word in the memory has been read.
      if TO_UINT(rd_fifo_usedw) <= g_burstsize and dvr_miso.done = '1' and q_reg.rd_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.dvr_mosi.address(c_adr_w - 1 downto 0)  := q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0);
        v.dvr_mosi.burstsize    := TO_UVEC(q_reg.stop_burstsize, dvr_mosi.burstsize'length);
        v.stopped               := '0';
        v.wr_sosi.valid         := '0';
        v.state                 := WAIT_FOR_SOP;
        v.wr_burst_en           := '1';
        v.rst_ddrctrl_input     := '1';
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '0';
      v.dvr_mosi.rd             := '1';

      if dvr_miso.done = '0' then
        v.rd_burst_en := '1';
      end if;

    when WAIT_FOR_SOP =>
      v.dvr_mosi.burstbegin := '0';
      v.rst_ddrctrl_input := '0';
      if q_reg.started = '0' and inp_sosi.eop = '1' then
        v.wr_sosi.valid := '1';
      elsif inp_sosi.sop = '1' then
        v.state := WRITING;
      else
        v.wr_sosi.valid := '0';
      end if;

    when WRITING =>
      -- this state generates the rest of the write bursts, it also checks if there is a stop signal or if it needs to stop writing.
      v.wr_bursts_ready         := TO_UINT(wr_fifo_usedw(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if q_reg.wr_bursts_ready >= 1 and dvr_miso.done = '1' and q_reg.wr_burst_en = '1' and q_reg.dvr_mosi.burstbegin = '0' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en           := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * (q_reg.wr_bursts_ready - 1)), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en           := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;

      if stop_in = '1' then
        v.ready_for_set_stop := '1';
      end if;

      if inp_adr >= c_pof_ma then
        v.filled_mem := '1';
      end if

      if q_reg.ready_for_set_stop = '1' and inp_sosi.eop = '1' and stop_in = '0' and v.filled_mem = '1' then
        v.state := SET_STOP;
      elsif q_reg.stop_adr = TO_UVEC(inp_adr, c_adr_w) then
        v.state := STOP_WRITING;
      end if;

    when SET_STOP =>
      -- this state sets a stop address dependend on the g_stop_percentage.
      if inp_adr - c_pof_ma >= 0 then
        v.stop_adr(c_adr_w - 1 downto 0) := TO_UVEC(inp_adr - c_pof_ma, c_adr_w);
      else
        v.stop_adr(c_adr_w - 1 downto 0) := TO_UVEC(inp_adr + g_max_adr - c_pof_ma, c_adr_w);
      end if;
      v.ready_for_set_stop                                        := '0';
      v.last_adr_to_write_to(c_adr_w - 1 downto c_bitshift_w)       := v.stop_adr(c_adr_w - 1 downto c_bitshift_w);
      v.last_adr_to_write_to(c_bitshift_w - 1 downto 0)             := (others => '0');
      v.stop_burstsize                                            := TO_UINT(v.stop_adr(c_adr_w - 1 downto 0)) - TO_UINT(v.last_adr_to_write_to) + 1;

      -- still a write cyle
      -- if adr mod g_burstsize = 0
      -- this makes sure that only ones every 64 writes a writeburst is started.
      v.wr_bursts_ready         := TO_UINT(wr_fifo_usedw(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en            := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;
      if dvr_miso.done = '1' and q_reg.wr_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en            := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * (q_reg.wr_bursts_ready - 1)), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if q_reg.stop_adr = TO_UVEC(inp_adr, c_adr_w) then
        v.state := STOP_WRITING;
      else
        v.state := WRITING;
      end if;

    when STOP_WRITING =>
      -- this state stops the writing by generating one last whole write burst which almost empties wr_fifo.
      v.wr_sosi.valid       := '0';
      v.dvr_mosi.burstbegin := '0';
      v.stopped             := '1';
      v.stop_adr            := TO_UVEC(g_max_adr, c_adr_w);

      -- still receiving write data.
      v.wr_bursts_ready         := TO_UINT(wr_fifo_usedw(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en            := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;
      if dvr_miso.done = '1' and q_reg.wr_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en            := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * q_reg.wr_bursts_ready - 1), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if dvr_miso.done = '1' and q_reg.dvr_mosi.burstbegin = '0' and q_reg.wr_burst_en = '0' and q_reg.wr_bursts_ready = 0 then
        v.state := LAST_WRITE_BURST;
      end if;

    when LAST_WRITE_BURST =>
     -- this state stops the writing by generatign one last write burst which empties wr_fifo.
     if dvr_miso.done = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.dvr_mosi.address(c_adr_w - 1 downto 0)  := q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0);
        v.dvr_mosi.burstsize    := TO_UVEC(q_reg.stop_burstsize, dvr_mosi.burstsize'length);
        v.state                 := START_READING;
        v.rd_burst_en           := '1';
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

    when START_READING =>
      -- this state generates the first read burst, the size of this burst is dependend on the size of the last write burst.
      v.dvr_mosi.burstbegin     := '0';
      v.outp_bsn                := TO_UVEC(TO_UINT(inp_sosi.bsn) - g_bim, c_dp_stream_bsn_w);

      if dvr_miso.done = '1' and v.rd_burst_en = '1' and q_reg.dvr_mosi.burstbegin = '0' then
        v.dvr_mosi.burstbegin   := '1';
        v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0) := TO_UVEC(g_burstsize - q_reg.stop_burstsize, dvr_mosi.burstsize'length);
        v.dvr_mosi.wr           := '0';
        v.dvr_mosi.rd           := '1';
        v.dvr_mosi.address(c_adr_w - 1 downto 0) := TO_UVEC(TO_UINT(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0)) + q_reg.stop_burstsize, c_adr_w);
        v.rd_burst_en           := '0';
        v.read_adr              := TO_UINT(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0)) + g_burstsize;
      end if;

      -- makes sure the fifo is filled before asking for another rd request. to prevent 4 rd burst to happend directly after one another.
      if dvr_miso.done = '0' and q_reg.rd_burst_en = '0' then
        v.rd_burst_en := '1';
        v.state       := READING;
      end if;

    when READING =>
      -- rd_fifo needs a refil after rd_fifo_usedw <= 10 because of delays, if you wait until rd_fifo_usedw = 0 then you get an empty fifo which results in your outputs sosi.valid not being constatly valid.
      if TO_UINT(rd_fifo_usedw) <= g_burstsize and dvr_miso.done = '1' and q_reg.rd_burst_en = '1' then
        v.dvr_mosi.wr         := '0';
        v.dvr_mosi.rd         := '1';
        v.dvr_mosi.burstbegin := '1';
        v.rd_burst_en         := '0';
        if q_reg.read_adr > g_max_adr - g_burstsize then
          v.dvr_mosi.address    := TO_UVEC(q_reg.read_adr, dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
          v.read_adr            := 0;
        else
          v.dvr_mosi.address    := TO_UVEC(q_reg.read_adr, dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
          v.read_adr            := q_reg.read_adr + g_burstsize;
        end if;
      else
        v.dvr_mosi.burstbegin := '0';
      end if;

      -- makes sure the fifo is filled before asking for another rd request. to prevent 4 rd burst to happend directly after one another.
      if dvr_miso.done = '0' then
        v.rd_burst_en := '1';
      end if;

      -- goes to STOP_reading when this read burst was from the burstblock before q_reg.stop_adr
      if q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0) = TO_UVEC(q_reg.read_adr, c_adr_w) then
        v.state := STOP_READING;
      end if;
    end case;

    if rst = '1' then
      v.state := RESET;
    end if;

    if inp_sosi.eop = '1' then
      v.started := '1';
    end if;

    d_reg     <= v;
  end process;

  -- fill outputs
  dvr_mosi          <= q_reg.dvr_mosi;
  wr_sosi           <= q_reg.wr_sosi;
  stop_out          <= q_reg.stopped;
  outp_bsn          <= q_reg.outp_bsn;
  rst_ddrctrl_input <= q_reg.rst_ddrctrl_input or rst;
end rtl;
