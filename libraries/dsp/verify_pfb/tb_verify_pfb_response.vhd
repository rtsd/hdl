-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Test bench to investigate the impulse response of the poly phase filterbank (PFB).
--          The main goal is to verify the order of the FIR coefficients.
-- Description:
--   . Use g_sel_pfb to select the APERTIF wpfb_unit_dev.vhd or LOFAR1 pfb2_unit.vhd as
--     device under test (DUT).
--   . The FIR coefficients are flipped per tap, so per set of N_fft = 1024 values. This is
--     because the FIR coefficients are applied as a filter per FFT input (so using
--     convolution). For more clarification see
--     https://git.astron.nl/rtsd/apertif_matlab/-/blob/master/matlab/one_pfb.m
--
-- Usage:
--   > as 3 default, or as 12 for details
--   manually add ref_coefs_arr, flip_coefs_arr, read_coefs_arr in case they are missing
--   > run -all
--   view PFIR coefficients via fil_re_scope in Wave Window in decimal radix and analogue format
--

library ieee, common_lib, dp_lib, filter_lib, rTwoSDF_lib, fft_lib, wpfb_lib;
library pfs_lib, pft2_lib, pfb2_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;
use IEEE.math_real.all;
use std.textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
-- APERTIF WPFB:
use filter_lib.fil_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use fft_lib.fft_pkg.all;
use fft_lib.tb_fft_pkg.all;
use wpfb_lib.wpfb_pkg.all;
-- LOFAR1 PFB2:
use pfs_lib.pfs_pkg.all;
use pft2_lib.pft_pkg.all;

entity tb_verify_pfb_response is
  generic (
    g_tb_index        : natural := 0;  -- use g_tb_index to identify and separate print_str() loggings from multi tb
    -- PFB
    g_sel_pfb         : string := "WPFB";  -- "WPFB" for APERTIF PFB, "PFB2" for LOFAR1 PBF
    --g_sel_pfb         : STRING := "PFB2";

    -- LOFAR2 WPFB
    g_fil_coefs_file_prefix : string := "data/Coeffs16384Kaiser-quant_1wb";  -- PFIR coefficients file access
    --g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";   -- bypass PFIR
    --g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_fircls1_16taps_1024points_16b_1wb";   -- g_fil_coef_dat_w = 16 bit

    -- LOFAR1 PFB2
    g_pfir_coefs_file   : string := c_pfs_coefs_file  -- PFB2 "data/pfs_coefsbuf_1024.hex" = Coeffs16384Kaiser-quant.dat default from pfs_pkg.vhd

  );
end entity tb_verify_pfb_response;

architecture tb of tb_verify_pfb_response is
  constant c_mm_clk_period      : time := 1 ns;
  constant c_dp_clk_period      : time := 10 ns;

  constant c_nof_wb_streams     : natural := 1;
  constant c_nof_blk_per_sync   : natural := 20;
  constant c_pfs_bypass         : boolean := g_fil_coefs_file_prefix = "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";

  constant c_fil_coefs_dat_file : string := g_fil_coefs_file_prefix(1 to g_fil_coefs_file_prefix'length - 4) & ".dat";  -- strip _1wb

  -- WPFB
  -- type t_wpfb is record
  --   -- General parameters for the wideband poly phase filter
  --   wb_factor         : natural;        -- = default 4, wideband factor
  --   nof_points        : natural;        -- = 1024, N point FFT (Also the number of subbands for the filter part)
  --   nof_chan          : natural;        -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
  --   nof_wb_streams    : natural;        -- = 1, the number of parallel wideband streams. The filter coefficients are shared on every wb-stream.
  --
  --   -- Parameters for the poly phase filter
  --   nof_taps          : natural;        -- = 16, the number of FIR taps per subband
  --   fil_backoff_w     : natural;        -- = 0, number of bits for input backoff to avoid output overflow
  --   fil_in_dat_w      : natural;        -- = 8, number of input bits
  --   fil_out_dat_w     : natural;        -- = 16, number of output bits
  --   coef_dat_w        : natural;        -- = 16, data width of the FIR coefficients
  --
  --   -- Parameters for the FFT
  --   use_reorder       : boolean;        -- = false for bit-reversed output, true for normal output
  --   use_fft_shift     : boolean;        -- = false for [0, pos, neg] bin frequencies order, true for [neg, 0, pos] bin frequencies order in case of complex input
  --   use_separate      : boolean;        -- = false for complex input, true for two real inputs
  --   fft_in_dat_w      : natural;        -- = 16, number of input bits
  --   fft_out_dat_w     : natural;        -- = 13, number of output bits
  --   fft_out_gain_w    : natural;        -- = 0, output gain factor applied after the last stage output, before requantization to out_dat_w
  --   stage_dat_w       : natural;        -- = 18, number of bits that are used inter-stage
  --   guard_w           : natural;        -- = 2
  --   guard_enable      : boolean;        -- = true
  --
  --   -- Parameters for the statistics
  --   stat_data_w       : positive;       -- = 56
  --   stat_data_sz      : positive;       -- = 2
  --   nof_blk_per_sync  : natural;        -- = 800000, number of FFT output blocks per sync interval
  --
  --   -- Pipeline parameters for both poly phase filter and FFT. These are heritaged from the filter and fft libraries.
  --   pft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the pipelined FFT
  --   fft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the parallel FFT
  --   fil_pipeline      : t_fil_ppf_pipeline; -- Pipeline settings for the filter units
  -- end record;

  --constant c_wpfb_lofar2_subbands_dts_18b : t_wpfb := (1, 1024, 0, 6,
  --                                                     16, 1, 14, 23, 16,
  --                                                     true, false, true, 23, 18, 1, 24, 1, true, 54, 2, 195313,
  --                                                     c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);
  constant c_wpfb : t_wpfb := (1, 1024, 0, c_nof_wb_streams,
                               16, 1, 14, 23, 16,
                               true, false, true, 23, 18, 1, 24, 1, true, 54, 2, c_nof_blk_per_sync,
                               c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);

  constant c_N_fft              : natural := c_wpfb.nof_points;
  constant c_N_blk              : natural := c_wpfb.nof_blk_per_sync;  -- nof FFT blocks per sync interval
  constant c_N_taps             : natural := c_wpfb.nof_taps;
  constant c_nof_coefs          : natural := c_N_taps * c_N_fft;
  constant c_nof_channels       : natural := 2**c_wpfb.nof_chan;  -- = 2**0 = 1, so no time multiplexing of inputs
  constant c_nof_sync           : natural := 2;  -- nof sync intervals to simulate

  -- BSN source
  constant c_bsn_w              : natural := 64;

  -- ADC
  constant c_W_adc              : natural := c_wpfb.fil_in_dat_w;

  -- TB
  signal bs_end                 : std_logic := '0';
  signal tb_end                 : std_logic := '0';
  signal mm_rst                 : std_logic;
  signal mm_clk                 : std_logic := '0';
  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  -- Input
  signal bs_sosi                : t_dp_sosi;

  signal impulse_data           : std_logic_vector(c_W_adc - 1 downto 0) := (others => '0');
  signal impulse_cnt            : natural := 0;

  signal in_sosi_arr            : t_dp_sosi_arr(0 downto 0);
  signal in_sosi                : t_dp_sosi;
  signal in_a_scope             : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal in_b_scope             : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal in_val                 : std_logic;
  signal in_val_cnt_per_sop     : natural := 0;  -- count valid samples per block
  signal in_val_cnt_per_sync    : natural := 0;  -- count valid samples per sync interval
  signal in_blk_cnt             : natural := 0;  -- count blocks per sync interval

  -- Filter coefficients
  signal ref_coefs_arr          : t_integer_arr(c_nof_coefs - 1 downto 0) := (others => 0);  -- = PFIR coef read from g_fil_coefs_file_prefix file
  signal flip_coefs_arr         : t_integer_arr(c_nof_coefs - 1 downto 0) := (others => 0);  -- = PFIR coef from g_fil_coefs_file_prefix flipped per tap
  signal read_coefs_arr         : t_integer_arr(c_nof_coefs - 1 downto 0) := (others => 0);  -- = PFIR coef read via MM from the coefs memories

  signal ram_fil_coefs_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fil_coefs_miso     : t_mem_miso;

  -- Filter output
  signal fil_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal fil_sosi               : t_dp_sosi;
  signal fil_re_scope           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal fil_im_scope           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal fil_val                : std_logic;
  signal fil_val_cnt_per_sop    : natural := 0;  -- count valid samples per block
  signal fil_val_cnt_per_sync   : natural := 0;  -- count valid samples per sync interval
  signal fil_blk_cnt            : natural := 0;  -- count blocks per sync interval

  -- Output
  signal raw_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal out_sosi_arr           : t_dp_sosi_arr(0 downto 0);
  signal out_sosi               : t_dp_sosi;
  signal out_re                 : integer;
  signal out_im                 : integer;
  signal out_power              : real;
  signal out_phase              : real;
  signal out_val                : std_logic;
  signal out_val_cnt            : natural := 0;
  signal out_blk_cnt            : natural := 0;

  signal out_val_a              : std_logic;  -- for real A
  signal out_val_b              : std_logic;  -- for real B
  signal out_channel            : natural := 0;
  signal out_cnt                : natural := 0;
  signal out_bin_cnt            : natural := 0;
  signal out_bin                : natural := 0;

  signal reg_out_sosi           : t_dp_sosi;
  signal reg_out_re_a_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_im_a_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_power_a_scope  : real := 0.0;
  signal reg_out_phase_a_scope  : real := 0.0;
  signal reg_out_re_b_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_im_b_scope     : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal reg_out_power_b_scope  : real := 0.0;
  signal reg_out_phase_b_scope  : real := 0.0;
  signal reg_out_val_a          : std_logic;
  signal reg_out_val_b          : std_logic;
  signal reg_out_val            : std_logic;
  signal reg_out_bin            : natural := 0;
  signal reg_out_blk_cnt        : natural := 0;
begin
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------

  p_run_test : process
  begin
    -- Wait for stimuli to finish
    proc_common_wait_until_high(dp_clk, bs_end);

    tb_end <= '1';
    wait;
  end process;

  p_bs_sosi : process
  begin
    bs_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(dp_clk, c_N_fft);
    -- Start BSN
    bs_sosi.valid <= '1';
    for K in 0 to c_nof_sync loop  -- simulate one input sync interval extra to have c_nof_sync output sync intervals
      bs_sosi.sync <= '1';
      for J in 0 to c_N_blk - 1 loop
        bs_sosi.sop <= '1';
        for I in 0 to c_N_fft - 1 loop
          if I = c_N_fft - 1 then
            bs_sosi.eop <= '1';
          end if;
          wait until rising_edge(dp_clk);
          bs_sosi.sync <= '0';
          bs_sosi.sop <= '0';
          bs_sosi.eop <= '0';
          if bs_sosi.eop = '1' then
            bs_sosi.bsn <= INCR_UVEC(bs_sosi.bsn, 1);
          end if;
        end loop;
      end loop;
    end loop;
    bs_sosi.valid <= '0';
    bs_end <= '1';
    wait;
  end process;

  p_impulse : process(dp_clk)
  begin
    -- Create impulse during one block every 20 blocks, where 20 > c_N_taps
    if rising_edge(dp_clk) then
      if bs_sosi.eop = '1' then
        -- raise impulse for one block
        if impulse_cnt = 0 then
          impulse_data <= TO_SVEC(2**(c_W_adc - 2), c_W_adc);  -- 0.5 * full scale impulse that will be active at sop (= after eop)
        else
          impulse_data <= TO_SVEC(0, c_W_adc);
        end if;
        -- maintain impulse period
        if impulse_cnt = 20 then
          impulse_cnt <= 0;
        else
          impulse_cnt <= impulse_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  p_in_sosi : process(bs_sosi)
  begin
    -- DUT input
    in_sosi    <= bs_sosi;
    -- Use impulse_data at real input to view PFIR coefficients in impulse response in fil_re_scope in Wave Window
    in_sosi.re <= RESIZE_DP_DSP_DATA(impulse_data);
    in_sosi.im <= TO_DP_DSP_DATA(0);
  end process;

  in_a_scope <= TO_SINT(in_sosi.re);
  in_b_scope <= TO_SINT(in_sosi.im);
  in_val <= in_sosi.valid;

  ---------------------------------------------------------------
  -- Read and verify FIR coefficients (similar as in tb_fil_ppf_single.vhd)
  ---------------------------------------------------------------

  gen_mm_wpfb : if g_sel_pfb = "WPFB" generate
    p_get_coefs_ref : process
    begin
      -- Read all coeffs from coefs file
      proc_common_read_integer_file(c_fil_coefs_dat_file, 0, c_nof_coefs, 1, ref_coefs_arr);
      wait for 1 ns;
      -- Reverse the coeffs per tap
      for J in 0 to c_N_taps - 1 loop
        for I in 0 to c_N_fft - 1 loop
          flip_coefs_arr(J * c_N_fft + c_N_fft - 1 - I) <= ref_coefs_arr(J * c_N_fft + I);
        end loop;
      end loop;
      wait;
    end process;

    p_read_coefs_from_mm : process
      constant c_mif_coef_mem_addr_w : natural := ceil_log2(c_N_fft);
      constant c_mif_coef_mem_span   : natural := 2**c_mif_coef_mem_addr_w;  -- mif coef mem span for one tap

      variable v_mif_base    : natural;
      variable v_I           : natural := 0;
    begin
      ram_fil_coefs_mosi <= c_mem_mosi_rst;
      proc_common_wait_until_low(mm_clk, mm_rst);
      proc_common_wait_some_cycles(mm_clk, 10);

      for J in 0 to c_N_taps - 1 loop
        v_mif_base := J * c_mif_coef_mem_span;
        for I in 0 to c_N_fft - 1 loop
          proc_mem_mm_bus_rd(v_mif_base + I, mm_clk, ram_fil_coefs_miso, ram_fil_coefs_mosi);
          proc_mem_mm_bus_rd_latency(1, mm_clk);
          read_coefs_arr(v_I) <= TO_SINT(ram_fil_coefs_miso.rddata(c_wpfb.coef_dat_w - 1 downto 0));
          v_I := v_I + 1;
        end loop;
      end loop;
      proc_common_wait_some_cycles(mm_clk, 1);
      assert read_coefs_arr = flip_coefs_arr
        report "Coefs file does not match coefs read via MM"
        severity error;
      wait;
    end process;
  end generate;

  ---------------------------------------------------------------
  -- DUT = Device Under Test
  ---------------------------------------------------------------

  in_sosi_arr(0) <= in_sosi;

  -- DUT = APERTIF WFPB
  dut_wpfb_unit_dev : if g_sel_pfb = "WPFB" generate
    u_wpfb_unit_dev : entity wpfb_lib.wpfb_unit_dev
    generic map (
      g_wpfb              => c_wpfb,
      g_coefs_file_prefix => g_fil_coefs_file_prefix
    )
    port map (
      dp_rst             => dp_rst,
      dp_clk             => dp_clk,
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      ram_fil_coefs_mosi => ram_fil_coefs_mosi,
      ram_fil_coefs_miso => ram_fil_coefs_miso,
      in_sosi_arr        => in_sosi_arr,
      fil_sosi_arr       => fil_sosi_arr,
      out_quant_sosi_arr => out_sosi_arr,
      out_raw_sosi_arr   => raw_sosi_arr
    );
  end generate;

  -- DUT = LOFAR1 WFPB
  dut_pfb2_unit : if g_sel_pfb = "PFB2" generate
    u_pfb2_unit : entity pfb2_lib.pfb2_unit
    generic map (
      g_nof_streams     => 1,  -- number of pfb2 instances, 1 pfb2 per stream
      g_nof_points      => c_wpfb.nof_points,

      -- pfs
      g_pfs_bypass      => c_pfs_bypass,
      g_pfs_nof_taps    => c_wpfb.nof_taps,
      g_pfs_in_dat_w    => c_wpfb.fil_in_dat_w,
      g_pfs_out_dat_w   => c_wpfb.stage_dat_w,
      g_pfs_coef_dat_w  => c_wpfb.coef_dat_w,
      g_pfs_coefs_file  => g_pfir_coefs_file,

      -- pft2
      g_pft_mode        => PFT_MODE_REAL2,
      g_pft_switch_en   => '0',
      g_pft_out_dat_w   => c_wpfb.fft_out_dat_w,
      g_pft_stage_dat_w => c_wpfb.stage_dat_w,

      -- sst
      g_sst_data_w      => c_wpfb.stat_data_w,
      g_sst_data_sz     => c_wpfb.stat_data_sz
    )
    port map (
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      in_sosi_arr       => in_sosi_arr,
      fil_sosi_arr      => fil_sosi_arr,
      out_sosi_arr      => out_sosi_arr
    );
  end generate;

  out_sosi <= out_sosi_arr(0);

  ---------------------------------------------------------------
  -- FIR filter output
  ---------------------------------------------------------------

  -- Append sync, sop, eop to fil_sosi
  p_fil_blk_cnt : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- FIR filter output:
      if fil_sosi.eop = '1' then
        if fil_blk_cnt = c_N_blk - 1 then
          fil_blk_cnt <= 0;
        else
          fil_blk_cnt <= fil_blk_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  p_fil_sosi : process(fil_sosi_arr, fil_val_cnt_per_sop)
  begin
    fil_sosi <= fil_sosi_arr(0);
    -- Add sync, sop and eop to fil_sosi for tb
    fil_sosi.sync <= '0';
    fil_sosi.sop <= '0';
    fil_sosi.eop <= '0';
    if fil_sosi_arr(0).valid = '1' then
      if fil_val_cnt_per_sop = 0 then
        if fil_blk_cnt = 0 then
          fil_sosi.sync <= '1';
        end if;
        fil_sosi.sop <= '1';
      end if;
      if fil_val_cnt_per_sop = c_N_fft - 1 then
        fil_sosi.eop <= '1';
      end if;
    end if;
  end process;

  fil_re_scope <= TO_SINT(fil_sosi.re);
  fil_im_scope <= TO_SINT(fil_sosi.im);
  fil_val <= fil_sosi.valid;

  ---------------------------------------------------------------
  -- FFT output
  ---------------------------------------------------------------

  out_re    <= TO_SINT(out_sosi.re);
  out_im    <= TO_SINT(out_sosi.im);
  out_power <= COMPLEX_RADIUS(out_re, out_im);
  out_phase <= COMPLEX_PHASE(out_re, out_im);
  out_val   <= out_sosi.valid;

  out_val_cnt <= out_val_cnt + 1 when rising_edge(dp_clk) and out_val = '1' else out_val_cnt;

  out_blk_cnt <= out_blk_cnt / c_N_fft;

  proc_fft_out_control(c_wpfb.wb_factor, c_N_fft, c_nof_channels, c_wpfb.use_reorder, c_wpfb.use_fft_shift, c_wpfb.use_separate,
                       out_val_cnt, out_val, out_val_a, out_val_b, out_channel, out_bin, out_bin_cnt);

  -- clock out_sosi to hold output for A and for B
  reg_out_val_a    <= out_val_a   when rising_edge(dp_clk);
  reg_out_val_b    <= out_val_b   when rising_edge(dp_clk);
  reg_out_val      <= out_val     when rising_edge(dp_clk);
  reg_out_bin      <= out_bin     when rising_edge(dp_clk);
  reg_out_blk_cnt  <= out_blk_cnt when rising_edge(dp_clk);

  reg_out_sosi          <= out_sosi  when rising_edge(dp_clk);
  reg_out_re_a_scope    <= out_re    when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_im_a_scope    <= out_im    when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_power_a_scope <= out_power when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_phase_a_scope <= out_phase when rising_edge(dp_clk) and out_val_a = '1';
  reg_out_re_b_scope    <= out_re    when rising_edge(dp_clk) and out_val_b = '1';
  reg_out_im_b_scope    <= out_im    when rising_edge(dp_clk) and out_val_b = '1';
  reg_out_power_b_scope <= out_power when rising_edge(dp_clk) and out_val_b = '1';
  reg_out_phase_b_scope <= out_phase when rising_edge(dp_clk) and out_val_b = '1';
end tb;
