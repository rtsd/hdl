-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Compensate for the mesh reorder between FN and BN on UniBoard
-- Description:
--
-- 0) The unb1_board_mesh_reorder_* reorders combinatorially to preserve the RL.
-- 1) The unb1_board_mesh_reorder_tx maps the usr bus index for each node
--    chip_id on the phy bus, so:
--
--      usr phy        phy             usr phy        phy
--   FN bus bus  mesh  bus BN       BN bus bus  mesh  bus FN
--    0   0 0   ----->  3  0         0   0 3   ----->  0  0
--    0   1 3   ----->  3  1         0   1 2   ----->  0  1
--    0   2 2   ----->  2  2         0   2 1   ----->  0  2
--    0   3 1   ----->  1  3         0   3 0   ----->  1  3
--
--    1   0 0   ----->  2  0         1   0 3   ----->  3  0
--    1   1 1   ----->  2  1         1   1 2   ----->  1  1
--    1   2 3   ----->  3  2         1   2 1   ----->  1  2
--    1   3 2   ----->  3  3         1   3 0   ----->  0  3
--
--    2   0 0   ----->  1  0         2   0 2   ----->  2  0
--    2   1 1   ----->  1  1         2   1 3   ----->  3  1
--    2   2 2   ----->  1  2         2   2 1   ----->  2  2
--    2   3 3   ----->  2  3         2   3 0   ----->  2  3
--
--    3   0 1   ----->  0  0         3   0 1   ----->  1  0
--    3   1 0   ----->  0  1         3   1 3   ----->  2  1
--    3   2 2   ----->  0  2         3   2 2   ----->  3  2
--    3   3 3   ----->  0  3         3   3 0   ----->  3  3
--
-- 2) The unb1_board_mesh_reorder_rx maps the phy bus index for each node
--    chip_id on the usr bus, so:
--
--      phy usr        phy             phy usr        phy
--   FN bus bus  mesh  bus BN       BN bus bus  mesh  bus FN
--    0   0 0   <-----  3  0         0   0 3   <-----  1  3
--    0   1 3   <-----  1  3         0   1 2   <-----  0  2
--    0   2 2   <-----  2  2         0   2 1   <-----  0  1
--    0   3 1   <-----  3  1         0   3 0   <-----  0  0
--
--    1   0 0   <-----  2  0         1   0 3   <-----  0  3
--    1   1 1   <-----  2  1         1   1 2   <-----  1  2
--    1   2 3   <-----  3  3         1   2 1   <-----  1  1
--    1   3 2   <-----  3  2         1   3 0   <-----  3  0
--
--    2   0 0   <-----  1  0         2   0 3   <-----  2  3
--    2   1 1   <-----  1  1         2   1 2   <-----  2  2
--    2   2 2   <-----  1  2         2   2 0   <-----  2  0
--    2   3 3   <-----  2  3         2   3 1   <-----  3  1
--
--    3   0 1   <-----  0  1         3   0 3   <-----  3  3
--    3   1 0   <-----  0  0         3   1 0   <-----  1  0
--    3   2 2   <-----  0  2         3   2 2   <-----  3  2
--    3   3 3   <-----  0  3         3   3 1   <-----  2  1
--
-- 3) In summary input bus 0,1,2,3 maps on output bus:
--
--         For TX    For RX
--     FN0 0,3,2,1   0,3,2,1
--     FN1 0,1,3,2   0,1,3,2
--     FN2 0,1,2,3   0,1,2,3
--     FN3 1,0,2,3   1,0,2,3
--     BN0 3,2,1,0   3,2,1,0
--     BN1 3,2,1,0   3,2,1,0
--     BN2 2,3,1,0   3,2,0,1  -- ! different for Tx and Rx
--     BN3 1,0,2,3   3,0,2,1  -- ! different for Tx and Rx
--
-- 4) The UniBoard mesh is modelled by unb1_board_mesh_model. In summary the
--    UniBoard mesh is wired as:
--
--      phy        phy             phy        phy
--   FN bus  mesh  bus BN       BN bus  mesh  bus FN
--    0   0 ------ 3   0         0   0 ------ 1   3
--    0   1 ------ 1   3         0   1 ------ 0   2
--    0   2 ------ 2   2         0   2 ------ 0   1
--    0   3 ------ 3   1         0   3 ------ 0   0
--
--    1   0 ------ 2   0         1   0 ------ 0   3
--    1   1 ------ 2   1         1   1 ------ 1   2
--    1   2 ------ 3   3         1   2 ------ 1   1
--    1   3 ------ 3   2         1   3 ------ 3   0
--
--    2   0 ------ 1   0         2   0 ------ 2   3
--    2   1 ------ 1   1         2   1 ------ 2   2
--    2   2 ------ 1   2         2   2 ------ 2   0
--    2   3 ------ 2   3         2   3 ------ 3   1
--
--    3   0 ------ 0   1         3   0 ------ 3   3
--    3   1 ------ 0   0         3   1 ------ 1   0
--    3   2 ------ 0   2         3   2 ------ 3   2
--    3   3 ------ 0   3         3   3 ------ 2   1
--
-- Remark:
-- . The indexing of the t_unb1_board_mesh_*_2arr is (node id 0,1,2,3)(tr lane
--   3,2,1,0)
-- . Use fixed c_unb1_board_tr.nof_bus=4, because the nodes connect always to
--   4 other nodes accross the UniBoard mesh.
-- . Use fixed c_unb1_board_tr.bus_w=4 transceivers per node-to-node bus,
--   because the UniBoard mesh does have 4 transceiver lanes per bus. It is
--   possible to use less then 4 transceivers per bus in the mesh. Typically
--   use <= 3 via index 2,1,0 to only use the 12 full-featured transceivers
--   (so without the 4 less-featured CMU ones).
-- . Both the SOSI and SISO signals are supported, because the transceiver PHY
--   (e.g. tr_nonbonded) does support SISO.
-- . For the SOSI and SISO interfaces there is also a corresponding UniBoard
--   mesh model (unb1_board_mesh_model_sosi and unb1_board_mesh_model_siso).
--   Therefor there is no need for an unb1_board_mesh_reorder_bidir at serial
--   std_logic level. The serial std_logic level can be simulated, but that
--   requires using the TR PHY module (e.g. tr_nonbonded) to convert between
--   SOSI/SISO and serial. Hence for simulating the UniBoard mesh at serial
--   TR PHY level there is the serial UniBoard mesh model
--   unb1_board_mesh_model_sl.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_mesh_reorder_bidir is
  generic (
    g_node_type : t_e_unb1_board_node := e_any;  -- or e_fn, or e_bn
    g_reorder   : boolean := true
  );
  port (
    chip_id          : in  std_logic_vector(c_unb1_board_aux.chip_id_w - 1 downto 0);  -- [2:0]

    -- Transmit clock domain --> output data to node across the mesh
    tx_clk           : in  std_logic;
    tx_usr_sosi_2arr : in  t_unb1_board_mesh_sosi_2arr;  -- user side
    tx_usr_siso_2arr : out t_unb1_board_mesh_siso_2arr;
    tx_phy_sosi_2arr : out t_unb1_board_mesh_sosi_2arr;  -- phy side
    tx_phy_siso_2arr : in  t_unb1_board_mesh_siso_2arr := c_unb1_board_mesh_siso_2arr_rst;

    -- Receive clock domain --> input data from node across the mesh
    rx_clk           : in  std_logic;
    rx_phy_sosi_2arr : in  t_unb1_board_mesh_sosi_2arr;  -- phy side
    rx_phy_siso_2arr : out t_unb1_board_mesh_siso_2arr;
    rx_usr_sosi_2arr : out t_unb1_board_mesh_sosi_2arr;  -- user side
    rx_usr_siso_2arr : in  t_unb1_board_mesh_siso_2arr := c_unb1_board_mesh_siso_2arr_rst
  );
end unb1_board_mesh_reorder_bidir;

architecture str of unb1_board_mesh_reorder_bidir is
begin
  u_tx : entity work.unb1_board_mesh_reorder_tx
  generic map (
    g_node_type => g_node_type,
    g_reorder   => g_reorder
  )
  port map (
    chip_id          => chip_id,
    clk              => tx_clk,
    tx_usr_sosi_2arr => tx_usr_sosi_2arr,
    rx_usr_siso_2arr => rx_usr_siso_2arr,
    tx_phy_sosi_2arr => tx_phy_sosi_2arr,
    rx_phy_siso_2arr => rx_phy_siso_2arr
  );

  u_rx : entity work.unb1_board_mesh_reorder_rx
  generic map (
    g_node_type => g_node_type,
    g_reorder   => g_reorder
  )
  port map (
    chip_id          => chip_id,
    clk              => rx_clk,
    rx_phy_sosi_2arr => rx_phy_sosi_2arr,
    tx_phy_siso_2arr => tx_phy_siso_2arr,
    rx_usr_sosi_2arr => rx_usr_sosi_2arr,
    tx_usr_siso_2arr => tx_usr_siso_2arr
  );
end str;
