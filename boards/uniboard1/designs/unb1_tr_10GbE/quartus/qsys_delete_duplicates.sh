#!/bin/sh

cp ../../../../../build/unb1/quartus/unb1_tr_10GbE/qsys_unb1_tr_10GbE/synthesis/qsys_unb1_tr_10GbE.qip /tmp/input_file

cat /tmp/input_file \
| grep -v 'altera_avalon_st_handshake_clock_crosser.v' \
| grep -v 'altera_reset_controller.v' \
| grep -v 'altera_merlin_traffic_limiter.sv' \
| grep -v 'altera_merlin_master_agent.sv' \
| grep -v 'altera_avalon_sc_fifo.v' \
| grep -v 'altera_merlin_slave_agent.sv' \
| grep -v 'altera_merlin_slave_translator.sv' \
| grep -v 'altera_merlin_master_translator.sv' > ../../../../../build/unb1/quartus/unb1_tr_10GbE/qsys_unb1_tr_10GbE/synthesis/qsys_unb1_tr_10GbE.qip



