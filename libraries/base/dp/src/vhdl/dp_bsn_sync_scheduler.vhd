-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: Eric Kooistra, 30 July 2021
-- Purpose :
--   Create programmable sync interval for input stream, according to the
--   PPS and BSN grid defined in [1].
-- Description:
-- * ctrl_start_bsn:
--   The output sync interval starts at an in_sosi.bsn that is programmable via
--   ctrl_start_bsn. The c_ctrl_start_bsn must be in the future to enable
--   output, otherwise the output is not enabled. The alternative to enable the
--   output immediately in case the BSN is in the passed is easy for a user,
--   because it then starts at any BSN, but that is not synchronous between
--   FPGAs.
-- * ctrl_interval_size:
--   The output sync interval is programmable via ctrl_interval_size. The
--   ctrl_interval_size is the number data samples per output sync interval,
--   so an integer multiple sample periods. The g_block_size is the number of
--   data samples per block.
--   The output sync intervals are controlled such that on average the number
--   of blocks per sync interval is nof_blk = ctrl_interval_size /
--   g_block_size, also when they are not integer dividable.
-- * ctrl_enable:
--   The output is enabled at the ctrl_start_bsn when ctrl_enable = '1' and the
--   output is disable after an in_sosi.eop when ctrl_enable = '0'. If the
--   output is diabled, then the sosi control fields are forced to '0', the
--   other sosi fields of the in_sosi are passed on to the out_sosi.
-- * mon_current_input_bsn:
--   The user can read mon_current_input_bsn to determine a suitable
--   ctrl_start_bsn in the future.
-- * mon_input_bsn_at_sync:
--   The user can read mon_current_input_bsn to determine a suitable
--   ctrl_start_bsn in the future to create a output sync interval that is
--   aligned with the in_sosi.sync.
-- * mon_output_enable:
--   The user can read mon_output_enable to check whether the output is indeed
--   enabled or not (mon_output_enable = out_enable).
-- * mon_output_sync_bsn:
--   The sync interval calculation is robust to lost in_sosi blocks. As soon
--   as it receives a new in_sosi block it will try to determine the next
--   output_sync_bsn, even if blocks were lost for multiple output sync
--   intervals. If mon_output_sync_bsn - mon_current_input_bsn < 0 then the
--   output sync interval calculation should catch up after some in_sosi
--   blocks. If mon_output_sync_bsn - mon_current_input_bsn > nof_blk then
--   something went wrong and then it may be necessary to recover using
--   ctrl_enable. If mon_output_sync_bsn - mon_current_input_bsn < nof_blk and
--   > 0 then that yields the number of blocks until the next output sync.
-- * out_start:
--   Pulse at out_sosi.sync with out_sosi.bsn = ctrl_start_bsn. The first
--   out_sosi.sync interval will have nof_blk_max blocks.
-- * out_start_interval:
--   Active from out_start until next out_sosi.sync, so active during the
--   first out_sosi.sync interval of a new out_sosi sequence.
-- * out_enable:
--   Goes high at first out_sosi.sync. In case of a restart when ctrl_enable
--   was already '1', then the out_enable will go low and high to ensure that
--   the restart out_sosi.sync interval will have nof_blk_max blocks. When
--   ctrl_enable goes low or in case of a restart then out_enable will go
--   low after an out_sosi.eop, to preserve complete out_sosi blocks. The
--   out_enable is monitored via mon_output_enable, so that the user can
--   check via MM whether an ctrl_enable access was applied. Typically the
--   out_enable OUT can be left OPEN in an VHDL application, and only used for
--   verification purposes in the test bench (tb).
-- For example:
--   . sample period Ts = 5 ns
--   . g_block_size = 1024 samples
--   . ctrl_start_bsn = 0
--   . ctrl_interval_size = 200M
--   ==>
--   One block is g_block_size * Ts = 5.12 us
--   nof_blk = ctrl_interval_size / g_block_size = 195312.5
--   nof_blk_max = ceil(nof_blk) = 195313 = 1.00000256 s
--   nof_blk_min = floor(nof_blk) = 195312 = 0.99999744 s
--   The output sync interval is exactly 1 s on average, the even output sync
--   periods (starting from inex 0 is even) will use nof_blk_max and the odd
--   output sync periods will use nof_blk_min.
--   If all FPGAs are started at the same ctrl_start_bsn, then the output sync
--   interval is synchonous in the entire array of FPGAs.
-- Remark:
-- * The implementation avoids using division and modulo on signals (e.g.
--   ctrl_interval_size / g_block_size) by using counters and fractions.
-- * The implementation uses the input BSN to calculate when to output the
--   scheduled output syncs. It has to be robust against lost input blocks,
--   therfore it cannot count input valids to determine the scheduled output
--   syncs.
-- * This dp_bsn_sync_scheduler.vhd resembles dp_bsn_source_v2. The
--   similarities are that both:
--   . create the fractional output sync intervals,
--   . start at an input trigger.
--   The differences are that:
--   . dp_bsn_sync_scheduler requires in_sosi.sync and copies the other in_sosi
--     ctrl, info and data, whereas generates bs_sosi.ctrl.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Timing+in+Station

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_bsn_sync_scheduler is
  generic (
    g_bsn_w                  : natural := c_dp_stream_bsn_w;
    g_block_size             : natural := 256;  -- = number of data valid per BSN block, must be >= 2
    g_ctrl_interval_size_min : natural := 1;  -- Minimum interval size to use if MM write interval size is set too small.
    g_pipeline               : natural := 1  -- use '1' on HW, use '0' for easier debugging in Wave window
  );
  port (
    rst                      : in  std_logic;
    clk                      : in  std_logic;

    -- M&C
    ctrl_enable              : in  std_logic;
    ctrl_enable_evt          : in  std_logic;
    ctrl_interval_size       : in  natural;
    ctrl_start_bsn           : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    mon_current_input_bsn    : out std_logic_vector(g_bsn_w - 1 downto 0);
    mon_input_bsn_at_sync    : out std_logic_vector(g_bsn_w - 1 downto 0);
    mon_output_enable        : out std_logic;
    mon_output_interval_size : out natural;
    mon_output_sync_bsn      : out std_logic_vector(g_bsn_w - 1 downto 0);

    -- Streaming
    in_sosi                  : in t_dp_sosi;
    out_sosi                 : out t_dp_sosi;
    out_start                : out std_logic;  -- pulse at out_sosi.sync at ctrl_start_bsn
    out_start_interval       : out std_logic;  -- active during first out_sosi.sync interval
    out_enable               : out std_logic  -- for tb verification purposes
  );
end dp_bsn_sync_scheduler;

architecture rtl of dp_bsn_sync_scheduler is
  type t_reg is record
    enable_init       : std_logic;
    enable            : std_logic;
    blk_cnt           : natural;
    interval_size     : natural;
    start_bsn         : std_logic_vector(g_bsn_w - 1 downto 0);
    input_bsn         : std_logic_vector(g_bsn_w - 1 downto 0);
    nof_blk_min       : natural;
    nof_blk_max       : natural;
    nof_blk           : natural;
    extra             : natural range 0 to g_block_size;
    accumulate        : natural range 0 to g_block_size * 2;
    hold_eop          : std_logic;
    update_bsn        : std_logic;
    output_enable     : std_logic;
    output_sync_bsn   : std_logic_vector(g_bsn_w - 1 downto 0);
  end record;

  constant c_reg_rst  : t_reg := ('0', '0', 0, g_ctrl_interval_size_min, (others => '0'), (others => '0'), 0, 0, 0, 0, 0, '1', '0', '0', (others => '0'));

  -- Local registers
  signal r            : t_reg;
  signal nxt_r        : t_reg;

  signal output_enable             : std_logic;
  signal output_next               : std_logic;  -- active at next output_sync's
  signal output_start              : std_logic;  -- active at first output_sync
  signal output_start_interval     : std_logic;  -- active during first output_sync interval
  signal output_start_interval_reg : std_logic := '0';
  signal output_sync               : std_logic;
  signal output_sosi               : t_dp_sosi;
begin
  assert g_block_size >= 2
    report "g_block_size must be >= 2."
    severity FAILURE;

  -- Capture monitoring info
  mon_current_input_bsn    <= in_sosi.bsn(g_bsn_w - 1 downto 0) when rising_edge(clk) and in_sosi.sop = '1';
  mon_input_bsn_at_sync    <= in_sosi.bsn(g_bsn_w - 1 downto 0) when rising_edge(clk) and in_sosi.sync = '1';
  mon_output_enable        <= r.output_enable;
  mon_output_interval_size <= g_ctrl_interval_size_min when rst = '1' else r.interval_size when rising_edge(clk) and output_start = '1';
  mon_output_sync_bsn      <= r.output_sync_bsn;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, ctrl_enable, ctrl_enable_evt, ctrl_interval_size, ctrl_start_bsn, in_sosi)
    variable v      : t_reg;
    variable v_size : natural;
  begin
    v := r;
    output_start <= '0';
    output_sync <= '0';

    ---------------------------------------------------------------------------
    -- Detect ctrl_enable rising event
    ---------------------------------------------------------------------------
    if ctrl_enable = '1' and ctrl_enable_evt = '1' then
      v.enable_init := '1';
    end if;

    ---------------------------------------------------------------------------
    -- Initialization:
    --   calculate number of blocks per output sync interval, start with
    --   nof_blk_max in first sync interval
    ---------------------------------------------------------------------------
    -- . use r.enable_init instead of v.enable_init to ease timing closure and
    --   because functionally it makes no difference.
    if r.enable_init = '1' then
      -- Assume ctrl_start_bsn is scheduled more than nof_blk block clk cycles
      -- after the ctrl_enable_evt, so there is sufficient time until
      -- v.output_enable = '1', to perform the calculation of nof_blk_min and
      -- nof_blk_max sequentially. This avoids using division in logic to
      -- calculate ctrl_interval_size / g_block_size.
      v_size := r.blk_cnt * g_block_size;
      if v_size = ctrl_interval_size then
        -- Support integer number of blocks per output sync interval
        v.interval_size := ctrl_interval_size;  -- hold ctrl_interval_size
        v.start_bsn := ctrl_start_bsn;  -- hold ctrl_start_bsn
        -- Fixed sync interval control, effectively disable fractional sync interval control:
        v.nof_blk_min := r.blk_cnt;
        v.nof_blk_max := r.blk_cnt;
        v.nof_blk := r.blk_cnt;  -- nof_blk = nof_blk_max = nof_blk_min
        v.extra := 0;
        v.accumulate := 0;
        v.enable_init := '0';  -- enable initialization is done
      elsif v_size > ctrl_interval_size then
        -- Support fractional number of blocks per output sync interval
        v.interval_size := ctrl_interval_size;  -- hold ctrl_interval_size
        v.start_bsn := ctrl_start_bsn;  -- hold ctrl_start_bsn
        -- Fractional sync interval control:
        v.nof_blk_min := r.blk_cnt - 1;
        v.nof_blk_max := r.blk_cnt;
        v.nof_blk := r.blk_cnt;  -- start with nof_blk_max sync interval
        v.extra := v_size - ctrl_interval_size;  -- number of extra samples in nof_blk_max compared to ctrl_interval_size
        v.accumulate := v.extra;  -- start with nof_blk_max sync interval
        v.enable_init := '0';  -- enable initialization is done
      else
        v.blk_cnt := r.blk_cnt + 1;
      end if;
    else
      v.blk_cnt := 0;
    end if;

    ---------------------------------------------------------------------------
    -- Enable / re-enable / disable control
    ---------------------------------------------------------------------------
    if ctrl_enable = '0' then
      -- Disable output when ctrl_enable requests disable.
      v.enable := '0';
    elsif ctrl_enable_evt = '1' then
      -- ctrl_enable is on, so this is a re-enable. First disable output.
      v.enable := '0';
    elsif r.enable_init = '0' then
      -- ctrl_enable is still on, so now enable the output. In case of an
      -- enable, then the output was already disabled. In case of a re-enable,
      -- then wait until the output got disabled.
      -- Enabling the output from disabled state ensures that the output will
      -- start or restart with nof_blk_max in the first sync interval.
      -- Otherwise with re-enabling from enabled state, it is undefined
      -- whether the first sync interval after the restart will have
      -- nof_blk_min or nof_blk_max blocks.
      if r.output_enable = '0' then
        v.enable := '1';
      end if;
    end if;

    ---------------------------------------------------------------------------
    -- Output enable / disable
    -- . enable at start_bsn and issue output_start pulse for first output
    --   sync interval
    -- . disable after input block has finished.
    ---------------------------------------------------------------------------
    -- Hold input eop to detect when input has finished a block and to detect
    -- gaps between in_sosi.eop and in_sosi.sop
    if in_sosi.sop = '1' then
      v.hold_eop := '0';
    end if;
    if in_sosi.eop = '1' then
      v.hold_eop := '1';
    end if;

    -- . use r.enable instead of v.enable to ease timing closure and because
    --   functionally it makes no difference.
    if r.enable = '1' then
      -- Output enable at in_sosi.sop start_bsn
      if in_sosi.sop = '1' then
        if unsigned(in_sosi.bsn) = unsigned(r.start_bsn) then
          v.output_enable := '1';
          output_start <= '1';  -- Pulse at start of output enable at start BSN of output sync intervals
          v.output_sync_bsn := r.start_bsn;  -- Initialize output sync at start BSN of output sync intervals
        end if;
      end if;
    else
      -- Output disable after in_sosi.eop
      if r.hold_eop = '1' then
        v.output_enable := '0';
      end if;
    end if;

    ---------------------------------------------------------------------------
    -- Issue output_sync at output_sync_bsn and request next output_sync_bsn
    ---------------------------------------------------------------------------
    -- Generate output sync interval based on input BSN and ctrl_interval_size
    if v.output_enable = '1' then
      if in_sosi.sop = '1' then
        if unsigned(in_sosi.bsn) = unsigned(v.output_sync_bsn) then
          -- Matching input block
          output_sync <= '1';  -- The output sync interval
          v.update_bsn := '1';
        elsif unsigned(in_sosi.bsn) > unsigned(v.output_sync_bsn) then
          -- Missed one or more input blocks, so cannot output sync, look for
          -- next opportunity
          v.update_bsn := '1';
        end if;
      end if;
    end if;

    ---------------------------------------------------------------------------
    -- Determine BSN for next output_sync (= output_sync_bsn)
    ---------------------------------------------------------------------------
    if r.update_bsn = '1' then
      -- Similar code as in proc_dp_verify_sync(), the difference is that:
      -- . Here r.extra is number of extra samples in nof_blk_max compared to
      --   ctrl_interval_size,
      -- . in proc_dp_verify_sync() r.extra is number of extra samples in
      --   ctrl_interval_size compared to nof_blk_min.
      -- Both schemes are valid, by using different schemes here and in tb the
      -- verification coverage improves.
      v.output_sync_bsn := ADD_UVEC(r.output_sync_bsn, TO_UVEC(r.nof_blk, c_natural_w));  -- next BSN

      v.nof_blk := r.nof_blk_max;
      v.accumulate := r.accumulate + r.extra;  -- account for nof_blk_max
      if v.accumulate >= g_block_size then
        v.nof_blk := r.nof_blk_min;
        v.accumulate := v.accumulate - g_block_size;  -- adjust for nof_blk_min
      end if;

      -- Assume output_sync_bsn is in future
      v.update_bsn := '0';

      -- If output_sync_bsn is still in past, due to lost input blocks, then
      -- last r.input_bsn will be used to keep update_bsn active for more clk
      -- cycles to catch up for lost input blocks.
    end if;

    -- Hold input bsn
    if in_sosi.sop = '1' then
      v.input_bsn := in_sosi.bsn(g_bsn_w - 1 downto 0);
    end if;

    -- Catch up with output_sync_bsn in case of lost input blocks. If many
    -- input blocks were lost (e.g. > nof_blk_max), then catching up the
    -- output_sync_bsn to the current input_bsn can cause that one or more
    -- output_sync will not be made active, but that is acceptable in case
    -- recovery from of lost input blocks.
    if v.output_enable = '1' then
      if unsigned(r.input_bsn) > unsigned(v.output_sync_bsn) then
        -- Missed one or more input blocks, fast forward to look for next
        -- output_sync_bsn in every clk cycle.
        v.update_bsn := '1';
      end if;
    end if;

    nxt_r <= v;
  end process;

  output_enable <= nxt_r.output_enable;

  -----------------------------------------------------------------------------
  -- Output in_sosi with programmed sync interval or disable the output
  -----------------------------------------------------------------------------
  -- . note this is part of p_comb, but using a separate process is fine too.
  p_output_sosi : process(in_sosi, output_enable, output_sync)
  begin
    output_sosi <= in_sosi;
    if output_enable = '1' then
      output_sosi.sync <= output_sync;
    else
      output_sosi.sync  <= '0';
      output_sosi.sop   <= '0';
      output_sosi.eop   <= '0';
      output_sosi.valid <= '0';
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Determine output_start_interval
  -----------------------------------------------------------------------------
  output_next <= output_sync and not output_start;

  p_output_start_interval : process(output_start_interval_reg, output_start, output_next, output_enable)
  begin
    -- Hold output_start until next sync interval
    output_start_interval <= output_start_interval_reg;
    if output_start = '1' then
      output_start_interval <= '1';
    elsif output_next = '1' then
      output_start_interval <= '0';
    end if;
    -- provided that output_enable is still active
    if output_enable = '0' then
      output_start_interval <= '0';
    end if;
  end process;

  output_start_interval_reg <= output_start_interval when rising_edge(clk);

  -----------------------------------------------------------------------------
  -- Pipeline output to avoid timing closure problems due to use of output_enable
  -----------------------------------------------------------------------------
  u_out_sosi : entity work.dp_pipeline
  generic map (
    g_pipeline  => g_pipeline
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_in       => output_sosi,
    -- ST source
    src_out      => out_sosi
  );

  gen_pipe_out_start : if g_pipeline = 1 generate
    out_start <= output_start when rising_edge(clk);
    out_start_interval <= output_start_interval_reg;
    out_enable <= r.output_enable;
  end generate;

  no_pipe_out_start : if g_pipeline = 0 generate
    out_start <= output_start;
    out_start_interval <= output_start_interval;
    out_enable <= output_enable;
  end generate;
end rtl;
