-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the reorder_transpose unit
--           To be used in conjunction with python testscript: ../python/tc_reorder_transpose.py
--
--
-- Usage:
--   > as 8
--   > run -all (about 300 us is enough)
--   > run python script in separate terminal: "python tc_reorder_transpose.py --unb 0 --fn 0 --sim"
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute u_dr_mem_ctrl/u_io_driver/ctlr_mosi in the WAVE window for wr and rd activity.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use work.reorder_pkg.all;

entity tb_mms_reorder_rewire is
  generic (
    g_nof_streams      : positive := 8;
    g_in_dat_w         : positive := 8;
    g_use_sel_table    : boolean  := false
 );
end tb_mms_reorder_rewire;

architecture tb of tb_mms_reorder_rewire is
  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 100 ps;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi        : t_mem_mosi;
  signal reg_diag_bg_miso        : t_mem_miso;

  signal ram_diag_bg_mosi        : t_mem_mosi;
  signal ram_diag_bg_miso        : t_mem_miso;

  signal ram_diag_data_buf_mosi  : t_mem_mosi;
  signal ram_diag_data_buf_miso  : t_mem_miso;

  signal reg_diag_data_buf_mosi  : t_mem_mosi;
  signal reg_diag_data_buf_miso  : t_mem_miso;

  -- DUT
  signal reg_reorder_rewire_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_reorder_rewire_miso : t_mem_miso := c_mem_miso_rst;

  constant c_sel_table: t_reorder_table :=
  (
   --FN0 FN1 FN2 FN3 BN0 BN1 BN2 BN3
    ( 19, 10, 13, 16, 8,  8,  8,  8,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 0
    ( 10, 13, 16, 19, 11, 11, 11, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 1
    ( 13, 16, 19, 10, 14, 14, 14, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 2
    ( 16, 19, 10, 13, 17, 17, 17, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 3
    ( 8,  8,  8,  8,  10, 19, 16, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 4
    ( 11, 11, 11, 11, 13, 10, 19, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 5
    ( 14, 14, 14, 14, 16, 13, 10, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 6
    ( 17, 17, 17, 17, 19, 16, 13, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Local processing output 7
    ( 1,  1,  1,  1,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 0
    ( 2,  0,  6,  4,  1,  3,  5,  7,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 1
    ( 9,  12, 15, 18, 12, 15, 18, 9,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 2
    ( 3,  3,  3,  3,  2,  2,  2,  2,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 3
    ( 4,  2,  0,  6,  7,  1,  3,  5,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 4
    ( 12, 15, 18, 9,  9,  12, 15, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 5
    ( 5,  5,  5,  5,  4,  4,  4,  4,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 6
    ( 6,  4,  2,  0,  5,  7,  1,  3,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 7
    ( 15, 18, 9,  12, 18, 9,  12, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 8
    ( 7,  7,  7,  7,  6,  6,  6,  6,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 9
    ( 0,  6,  4,  2,  3,  5,  7,  1,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 10
    ( 18, 9,  12, 15, 15, 18, 9,  12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),  -- Mesh Transmit output 11
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
  );

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := 16;
  constant c_db_block_len           : natural  := 16;
  constant c_sel_in_w               : natural  := 3;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := g_nof_streams;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_in_dat_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_nof_blocks_per_sync : positive := 16;
  constant c_bg_gapsize             : positive := 8;
  constant c_bg_data_file_prefix    : string   := "UNUSED";  -- "../../../src/hex/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, 128, 1);

  constant c_bg_ctrl                : t_diag_block_gen := ('0',  -- enable: On by default in simulation; MM enable required on hardware.
                                                          '0',  -- enable_sync
                                                      TO_UVEC(         c_bg_block_len, c_diag_bg_samples_per_packet_w),
                                                      TO_UVEC(c_bg_nof_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                      TO_UVEC(         c_bg_gapsize, c_diag_bg_gapsize_w),
                                                      TO_UVEC(                    0, c_diag_bg_mem_low_adrs_w),
                                                      TO_UVEC  (   c_bg_block_len - 1, c_diag_bg_mem_high_adrs_w),
                                                      TO_UVEC(                    0, c_diag_bg_bsn_init_w));

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := g_nof_streams;
  constant c_db_data_w              : positive := g_in_dat_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := true;
  constant c_db_data_type           : t_diag_data_type_enum := e_data;

  signal bg_siso_arr                : t_dp_siso_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr               : t_dp_siso_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_siso_rdy);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_mosi, ram_diag_data_buf_miso);

  u_mm_file_reg_diag_data_buf : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_mosi, reg_diag_data_buf_miso);

  u_mm_file_reg_reorder_rewire   : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_REORDER_REWIRE")
                                           port map(mm_rst, mm_clk, reg_reorder_rewire_mosi, reg_reorder_rewire_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix,
    g_diag_block_gen_rst => c_bg_ctrl
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.mms_reorder_rewire
  generic map (
    g_select_table  => c_sel_table,
    g_nof_streams   => g_nof_streams,
    g_sel_in_w      => c_sel_in_w,
    g_use_sel_table => g_use_sel_table
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst   => mm_rst,
    mm_clk   => mm_clk,
    dp_rst   => dp_rst,
    dp_clk   => dp_clk,
    reg_mosi => reg_reorder_rewire_mosi,
    reg_miso => reg_reorder_rewire_miso,

    -- ST sinks
    snk_out_arr  => bg_siso_arr,
    snk_in_arr   => bg_sosi_arr,
    -- ST source
    src_in_arr   => out_siso_arr,
    src_out_arr  => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer
  ----------------------------------------------------------------------------
  u_data_buf : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,
    -- ST interface
    in_sync           => out_sosi_arr(0).sync,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
