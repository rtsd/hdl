-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- >>> Ported from UniBoard dp_latency_increase for fixed RL 0 --> 1
--
-- Purpose:
--   Increase the ready latency from RL=0 to RL=1.
-- Description:
--   Increasing the RL makes a look ahead FIFO appear as a normal FIFO.
-- Remark:
-- . The data are passed on as wires when g_hold_dat_en=FALSE. When TRUE then
--   the data is held until the next active src_out_val to ease applications
--   that still depend on the src_out_dat value after src_out_val has gone low.
--   With RL=0 at the snk_in a new valid snk_in_dat can arrive before it is
--   acknowledged by the registered src_in_ready.
-- . The src_out_val control is internally AND with the snk_out_ready.

entity common_rl_increase is
  generic (
    g_adapt       : boolean := true;  -- default when TRUE then increase sink RL 0 to source RL 1, else then implement wires
    g_hold_dat_en : boolean := true;  -- default when TRUE hold the src_out_dat until the next active src_out_val, else just pass on snk_in_dat as wires
    g_dat_w       : natural := 18
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- Sink
    snk_out_ready : out std_logic;  -- sink RL = 0
    snk_in_dat    : in  std_logic_vector(g_dat_w - 1 downto 0) := (others => 'X');
    snk_in_val    : in  std_logic := 'X';
    -- Source
    src_in_ready  : in  std_logic;  -- source RL = 1
    src_out_dat   : out std_logic_vector(g_dat_w - 1 downto 0);
    src_out_val   : out std_logic
  );
end common_rl_increase;

architecture rtl of common_rl_increase is
  signal ready_reg    : std_logic;

  signal hold_dat     : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
  signal nxt_hold_dat : std_logic_vector(g_dat_w - 1 downto 0);
  signal hold_val     : std_logic;
begin
  gen_wires : if g_adapt = false generate
    snk_out_ready <= src_in_ready;

    src_out_dat   <= snk_in_dat;
    src_out_val   <= snk_in_val;
  end generate;

  gen_adapt : if g_adapt = true generate
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        ready_reg <= '0';
      elsif rising_edge(clk) then
        ready_reg <= src_in_ready;
        hold_dat  <= nxt_hold_dat;
      end if;
    end process;

    -- SISO
    snk_out_ready <= ready_reg;  -- Adjust ready latency

    -- SOSI
    hold_val     <= snk_in_val and ready_reg;
    src_out_val  <= hold_val;

    nxt_hold_dat <= snk_in_dat when hold_val = '1' else hold_dat;
    src_out_dat  <= snk_in_dat when g_hold_dat_en = false else nxt_hold_dat;
  end generate;
end rtl;
