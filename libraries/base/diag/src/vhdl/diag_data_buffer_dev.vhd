-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Capture a block of streaming data for analysis via MM access
-- Description :
--   The first g_nof_data valid streaming data input words are stored in the
--   data buffer. Then they can be read via the MM interface. Dependend on
--   g_use_in_sync the nxt block of valid streaming data input words gets
--   stored when a new in_sync occurs or when the last word was read from via
--   the MM interface.
--
--   There are 3 modes of operation in the _dev version:
--
--   (1) NON-SYNC MODE: g_use_in_sync = FALSE
--   In this mode the first g_nof_data valid data input words are stored in the
--   data buffer. A new set of data will be stored when the last word is read
--   from the buffer via the MM interface.
--
--   (2) SYNC-MODE: g_use_in_sync = TRUE and reg_sync_delay = 0
--   On every received sync pulse a number of g_nof_data valid words are written
--   to the databuffer. Data will be overwritten on every new sync pulse. It is
--   up to the user to read out the data in time in between two sync pulses
--
--   (3) ARM-MODE: g_use_in_sync = TRUE and reg_sync_delay > 0
--   First the reg_sync_delay should be written with a desired delay value. Then
--   the arm register must be written. After being armed the databuffer will wait
--   for the first sync pulse to arrive. When it has arrived it will wait for
--   reg_sync_delay valid cycles before g_nof_data valid words are written to the
--   databuffer. The data can then be read out through the MM interface. New data
--   will only be written if the databuffer is being armed again.
--
-- Remarks:
-- . The actual RAM usage depends on g_data_w. Unused bits are forced to '0'
--   when read.
-- . The c_mm_factor must be a power of 2 factor. Typically c_mm_factor=1 is
--   sufficient for most purposes. If the application only requires
--   eg. c_mm_factor=3 then it needs to extend the data to c_mm_factor=4.
-- . If c_mm_factor=2 then in_data[g_data_w/2-1:0] will appear at MM address
--   even and in_data[g_data_w-1:g_data_w/2] at address odd.
--   The advantage of splitting at g_data_w/2 instead of at c_word_w=32 is
--   that streaming 36b data can then map on 18b RAM still fit in a single
--   RAM block. Whereas mapping the LS 32b part at even address and the MS 4b
--   part at odd address would require using c_word_w=32b RAM that could
--   require two RAM blocks. For g_data_w=2*c_word_w=64b there is no
--   difference between these 2 schemes. Hence by rising the g_data_w to a
--   power of 2 multiple of 32b the user can enforce using splitting the data
--   a c_word_w parts.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.diag_pkg.all;
use technology_lib.technology_select_pkg.all;

entity diag_data_buffer_dev is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_data_w      : natural := 32;
    g_nof_data    : natural := 1024;
    g_use_in_sync : boolean := false  -- when TRUE start filling the buffer at the in_sync, else after the last word was read
  );
  port (
    -- Memory-mapped clock domain
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    ram_mm_mosi  : in  t_mem_mosi;  -- read and overwrite access to the data buffer
    ram_mm_miso  : out t_mem_miso;

    reg_mm_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mm_miso  : out t_mem_miso;

    -- Streaming clock domain
    st_rst       : in  std_logic;
    st_clk       : in  std_logic;

    in_data      : in  std_logic_vector(g_data_w - 1 downto 0);
    in_sync      : in  std_logic := '0';
    in_val       : in  std_logic;
    out_wr_done  : out std_logic := '1'
  );
end diag_data_buffer_dev;

architecture rtl of diag_data_buffer_dev is
  constant c_version       : natural := 1;

  constant c_mm_factor     : natural := ceil_div(g_data_w, c_word_w);  -- must be a power of 2 multiple
  constant c_nof_data_mm   : natural := g_nof_data * c_mm_factor;
  constant g_data_mm_w     : natural := g_data_w / c_mm_factor;

  constant c_buf_mm        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(c_nof_data_mm),
                                         dat_w    => g_data_mm_w,
                                         nof_dat  => c_nof_data_mm,
                                         init_sl  => '0');

  constant c_buf_st        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(g_nof_data),
                                         dat_w    => g_data_w,
                                         nof_dat  => g_nof_data,
                                         init_sl  => '0');

  constant c_reg           : t_c_mem := (latency  => 1,
                                         adr_w    => c_diag_db_dev_reg_adr_w,
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => c_diag_db_dev_reg_nof_dat,  -- 3: reg_sync_delay 2: valid_cnt 1: word_cnt; 0:sync_cnt
                                         init_sl  => '0');

  signal i_ram_mm_miso   : t_mem_miso := c_mem_miso_rst;  -- used to avoid vsim-8684 error "No drivers exist" for the unused fields

  signal rd_last         : std_logic;
  signal rd_last_st      : std_logic;
  signal wr_sync         : std_logic;

  signal wr_done         : std_logic;
  signal nxt_wr_done     : std_logic;

  signal wr_data         : std_logic_vector(c_buf_st.dat_w - 1 downto 0);
  signal nxt_wr_data     : std_logic_vector(c_buf_st.dat_w - 1 downto 0);
  signal wr_addr         : std_logic_vector(c_buf_st.adr_w - 1 downto 0);
  signal nxt_wr_addr     : std_logic_vector(c_buf_st.adr_w - 1 downto 0);
  signal wr_en           : std_logic;
  signal nxt_wr_en       : std_logic;

  signal reg_rd_arr      : std_logic_vector(c_reg.nof_dat - 1 downto 0);
  signal reg_wr_arr      : std_logic_vector(c_reg.nof_dat - 1 downto 0);
  signal reg_slv_rd      : std_logic_vector(c_reg.nof_dat * c_word_w - 1 downto 0);
  signal reg_slv_wr      : std_logic_vector(c_reg.nof_dat * c_word_w - 1 downto 0);

  signal arm_enable      : std_logic := '0';
  signal sync_cnt_clr    : std_logic := '0';
  signal sync_cnt        : std_logic_vector(c_word_w - 1 downto 0);  -- Nof times buffer has been written
  signal word_cnt        : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal valid_cnt       : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal reg_sync_delay  : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');

  type   state_type is (s_idle, s_sync_count, s_wait_for_rd_last, s_armed);

  type reg_type is record
    wr_sync    : std_logic;
    state      : state_type;  -- The state machine.
  end record;

  signal r, rin : reg_type;
begin
  assert c_mm_factor = 2**true_log2(c_mm_factor)
    report "Only support mixed width data that uses a power of 2 multiple."
    severity FAILURE;

  out_wr_done <= nxt_wr_done;

  ram_mm_miso <= i_ram_mm_miso;

  rd_last <= '1' when unsigned(ram_mm_mosi.address(c_buf_mm.adr_w - 1 downto 0)) = c_nof_data_mm - 1 and ram_mm_mosi.rd = '1' else '0';

  u_rd_last_clock_cross : entity common_lib.common_spulse
  generic map (
    g_delay_len => c_meta_delay_len
  )
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => rd_last,
    out_rst   => st_rst,
    out_clk   => st_clk,
    out_pulse => rd_last_st
  );

  -- Determine the write trigger in NON-SYNC MODE
  use_rd_last : if g_use_in_sync = false generate
    wr_sync <= rd_last_st;
  end generate;

  -- Determine the write trigger in SYNC MODE and ARM MODE
  use_in_sync : if g_use_in_sync = true generate
    comb : process(st_rst, r, in_sync, rd_last_st, reg_sync_delay, arm_enable, valid_cnt)
      variable v : reg_type;
    begin
      v := r;

      v.wr_sync := '0';
      case r.state is

        when s_idle =>
          if arm_enable = '1' then
            v.state := s_armed;
          end if;

        when s_armed =>
          if (in_sync = '1' and TO_UINT(reg_sync_delay) = 1) then
            v.wr_sync := '1';
            v.state   := s_wait_for_rd_last;
          elsif(in_sync = '1' and TO_UINT(reg_sync_delay) > 1) then
            v.state      := s_sync_count;
          end if;

        when s_sync_count =>
          if ((TO_UINT(valid_cnt) + 2) >= TO_UINT(reg_sync_delay)) then
            v.wr_sync    := '1';
            v.state      := s_wait_for_rd_last;
          end if;

        when s_wait_for_rd_last =>
          if rd_last_st = '1'  then
            v.state := s_idle;
          elsif arm_enable = '1' then
            v.state := s_armed;
          end if;

        when others =>
          v.state := s_idle;
      end case;

      if st_rst = '1' then
        v.wr_sync    := '0';
        v.state      := s_idle;
      end if;

      rin <= v;
    end process comb;

    regs : process(st_clk)
    begin
      if rising_edge(st_clk) then
        r <= rin;
      end if;
    end process;

    -- Choose between SYNC MODE and ARM MODE.
    wr_sync <= in_sync when TO_UINT(reg_sync_delay) = 0 else r.wr_sync;
  end generate;

  p_st_clk : process (st_clk, st_rst)
  begin
    if st_rst = '1' then
      wr_data <= (others => '0');
      wr_addr <= (others => '0');
      wr_en   <= '0';
      wr_done <= '0';
    elsif rising_edge(st_clk) then
      wr_data <= nxt_wr_data;
      wr_addr <= nxt_wr_addr;
      wr_en   <= nxt_wr_en;
      wr_done <= nxt_wr_done;
    end if;
  end process;

  -- Write access control
  nxt_wr_data <= in_data;
  nxt_wr_en   <= in_val and not nxt_wr_done;

  p_wr_addr : process (wr_done, wr_addr, wr_sync, wr_en)
  begin
    nxt_wr_done <= wr_done;
    nxt_wr_addr <= wr_addr;
    if wr_sync = '1' then
      nxt_wr_done <= '0';
      nxt_wr_addr <= (others => '0');
    elsif wr_en = '1' then
      if unsigned(wr_addr) = g_nof_data - 1 then
        nxt_wr_done <= '1';  -- keep wr_addr, do not allow wr_addr increment >= g_nof_data to avoid RAM address out-of-bound warning in Modelsim in case c_buf.nof_dat < 2**c_buf.adr_w
      else
        nxt_wr_addr <= INCR_UVEC(wr_addr, 1);
      end if;
    end if;
  end process;

  u_buf : entity common_lib.common_ram_crw_crw_ratio
  generic map (
    g_technology => g_technology,
    g_ram_a     => c_buf_mm,
    g_ram_b     => c_buf_st,
    g_init_file => "UNUSED"
  )
  port map (
    -- MM read/write port clock domain
    rst_a    => mm_rst,
    clk_a    => mm_clk,
    wr_en_a  => ram_mm_mosi.wr,
    wr_dat_a => ram_mm_mosi.wrdata(c_buf_mm.dat_w - 1 downto 0),
    adr_a    => ram_mm_mosi.address(c_buf_mm.adr_w - 1 downto 0),
    rd_en_a  => ram_mm_mosi.rd,
    rd_dat_a => i_ram_mm_miso.rddata(c_buf_mm.dat_w - 1 downto 0),
    rd_val_a => i_ram_mm_miso.rdval,

    -- ST write only port clock domain
    rst_b     => st_rst,
    clk_b     => st_clk,
    wr_en_b   => wr_en,
    wr_dat_b  => wr_data,
    adr_b     => wr_addr,
    rd_en_b   => '0',
    rd_dat_b  => OPEN,
    rd_val_b  => open
  );

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_reg       => c_reg
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => st_rst,
    st_clk      => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_mm_mosi,
    sla_out     => reg_mm_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => reg_wr_arr,
    reg_rd_arr  => reg_rd_arr,
    in_reg      => reg_slv_rd,
    out_reg     => reg_slv_wr
  );

  arm_enable     <= reg_wr_arr(2);
  reg_sync_delay <= reg_slv_wr(4 * c_word_w - 1 downto 3 * c_word_w);
  reg_slv_rd     <= TO_UVEC(c_version, c_word_w) & TO_UVEC(0, c_word_w) & TO_UVEC(0, c_word_w) & TO_UVEC(0, c_word_w) &
                    reg_sync_delay & valid_cnt & word_cnt & sync_cnt;

  u_word_cnt : entity common_lib.common_counter
  port map (
    rst     => st_rst,
    clk     => st_clk,
    cnt_en  => wr_en,
    cnt_clr => wr_sync,
    count   => word_cnt
  );

  u_sync_cnt : entity common_lib.common_counter
  port map (
    rst     => st_rst,
    clk     => st_clk,
    cnt_en  => wr_sync,
    cnt_clr => sync_cnt_clr,
    count   => sync_cnt
  );

  u_valid_cnt : entity common_lib.common_counter
  port map (
    rst     => st_rst,
    clk     => st_clk,
    cnt_en  => in_val,
    cnt_clr => in_sync,
    count   => valid_cnt
  );
end rtl;
