# ------------------------------------------------------------------------------
#
# Copyright 2024
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ------------------------------------------------------------------------------
#
# Author: D.F. Brouwer
# Description:
#   This file is based on generated file mentor/msim_setup.tcl.
#   - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
#   - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
#   - replace QSYS_SIMDIR by IP_DIR
#   - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.
# Remark:
#   Both uses the same library source file, so only one source file initialization (compilation) into the IP library for simulation is needed

# Create the work library
vlib ./work/         ;# Assume library work already exist

# Map the following libraries to the work library:
# . Map the altera_s10_user_rst_clkgate_1945 library to the work library
vmap altera_s10_user_rst_clkgate_1945 ./work/

# Compile SystemVerilog file for altera_s10_user_rst_clkgate_1945. Read remark.
set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ci"
  vlog -sv "$IP_DIR/altera_s10_user_rst_clkgate_1945/sim/altera_s10_user_rst_clkgate.sv" -work altera_s10_user_rst_clkgate_1945
#set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ri"
#  vlog -sv "$IP_DIR/altera_s10_user_rst_clkgate_1945/sim/altera_s10_user_rst_clkgate.sv" -work altera_s10_user_rst_clkgate_1945

# Compile VHDL file for ip_agi027_1e1v_reset_release_ci
set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ci/sim"
  vcom "$IP_DIR/ip_agi027_1e1v_reset_release_ci.vhd"

# Compile VHDL file for ip_agi027_1e1v_reset_release_ri
set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ri/sim"
  vcom "$IP_DIR/ip_agi027_1e1v_reset_release_ri.vhd"
