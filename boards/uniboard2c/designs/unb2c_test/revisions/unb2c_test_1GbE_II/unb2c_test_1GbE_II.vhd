-------------------------------------------------------------------------------
--
-- Copyright (C) 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Purpose: Test both 1GbE-I and 1GbE-II ports using eth_tester
-- Description:

library IEEE, common_lib, unb2c_board_lib, unb2c_test_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use technology_lib.technology_pkg.all;

entity unb2c_test_1GbE_II is
  generic (
    g_design_name      : string  := "unb2c_test_1GbE_II";
    g_design_note      : string  := "Use eth_0 and eth_1";
    g_sim              : boolean := false;  -- Overridden by TB
    g_sim_unb_nr       : natural := 0;
    g_sim_node_nr      : natural := 0;
    g_stamp_date       : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time       : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id      : string  := ""  -- revision ID     -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGIN     : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2c_test_1GbE_II;

architecture str of unb2c_test_1GbE_II is
begin
  u_revision : entity unb2c_test_lib.unb2c_test
  generic map (
    g_design_name => g_design_name,
    g_design_note => g_design_note,
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_revision_id => g_revision_id
  )
  port map (
    -- GENERAL
    CLK          => CLK,
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- 1GbE Control Interface
    ETH_clk      => ETH_clk,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    QSFP_LED     => QSFP_LED
  );
end str;
