/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */
#ifndef UNBOS_I2C_H
#define UNBOS_I2C_H

/* Compile switches. */

/* includes */
#include <system.h>

#ifdef __AVS_I2C_MASTER
    // System has I2C 
    #include <sys/alt_irq.h>
//    #include <osx_irq.h>
    #include <alt_types.h>
    #include <avs_i2c_master_regs.h>

    /* private constants (#define) */
    
    /* private macros */

    /* private types */

    /* private function protos */

    /* private constants (const) */
    
    /* private variables */
    
    /* public variables */

    /* public function implements */
    void UNBOS_i2c_init(void);

#else

    /* system does NOT have I2C */
    #define UNBOS_i2c_init() do { } while( 0 );

#endif

#endif // UNBOS_I2C_H
