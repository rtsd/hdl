--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Define that other side must be aligned within some delay after this side got aligned.
-- Description:
--   We assume the RX connected to our TX will be channel aligned within c_align_dly_cnt cycles
--   after 'our own' RX has asserted channelaligned. This is important because txc_tx_channelaligned_dly
--   connects directly to tx_siso.ready.
--   Assert txc_rx_channelaligned: basically indicates whether or not the RX of the receiving
--   XAUI core (NOT this one) is assumed to be channelaligned.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity tech_xaui_align_dly is
  generic(
    g_sim : boolean := false
  );
  port (
    tx_clk                    : in std_logic;
    tx_rst                    : in std_logic;

    a_rx_channelaligned       : in std_logic;  -- rx aligned to tx from other side (asynchronous signal)
    txc_rx_channelaligned_dly : out std_logic
  );
end tech_xaui_align_dly;

architecture rtl of tech_xaui_align_dly is
  constant c_align_dly_cnt   : natural := sel_a_b(g_sim, 50, 156250000);  -- about 1 second on hw
begin
  u_common_debounce : entity common_lib.common_debounce
  generic map (
    g_type       => "HIGH",  -- "HIGH" = debounce g_latency clk cycles for going high when d_in='1', go low  immediately when d_in='0'
    g_delay_len  => c_meta_delay_len,  -- = 3,  combat meta stability
    g_latency    => c_align_dly_cnt,  -- >= 1, combat debounces over nof clk cycles
    g_init_level => '0'
  )
  port map (
    rst   => tx_rst,
    clk   => tx_clk,
    d_in  => a_rx_channelaligned,
    q_out => txc_rx_channelaligned_dly
  );
end rtl;
