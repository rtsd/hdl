-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra, 7 apr 2017
-- Purpose:
--   Force the sosi data,re,im field to constant or incrementing value in a
--   block or pass on.
--
--   See also dp_force_data_serial.vhd to force data at a specific index in
--   a block.
-- Description:
--   The force_en input dynamically enables or disables the entire force
--   function.
--
--   Default the force value is constant, but using the generics it can be
--   made to automatically increment for every valid and restart at sync or
--   sop.
--   . set force_data, re, im via MM interface
--   . set increment step via generic g_increment_data, re, im. Use
--     g_increment_data, re, im = 0 for constant value in a block.
--   . Dependent on g_increment_data_on_sop, re, im the increment is applied
--     for every valid or only at the sop.
--   . restart the data increment on sync or on sop via generic
--     g_restart_data_on_sync or g_restart_data_on_sop. Similar for
--     g_restart_re_on_sync, sop and g_restart_im_on_sync, sop.
--     If g_increment_data, re, im = 0 then the g_restart_data_on_sop, sync
--     setting is void.
-- Usage:
-- . DSP tests: The amplitude of constant data is useful to verify scaling and
--   requantization.
-- . Data move test: Forcing constant (zero) data is useful for to trace data
--   when the data gets reordered. A combination of constant re and
--   incrementing im can also be useful to trace data.
-- Remarks:
-- . Ready latency = 1
-- . When force_en='0' then this dp_force_data_parallel is equivalent
--   to dp_pipeline.
-- . The sosi.data and sosi.re, sosi.im fields can be forced independently.
--   However in the stream typically only the sosi.data is used or only the
--   complex sosi.re, sosi.im. Hence, when the sosi.data is used then the
--   force_re and force_im are void and when the sosi.re, sosi.im are used
--   then the force_data is void.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_force_data_parallel is
  generic (
    g_dat_w                 : natural := 32;  -- must be <= 32 to fit INTEGER range
    g_increment_data        : integer := 0;
    g_increment_re          : integer := 0;
    g_increment_im          : integer := 0;
    g_increment_data_on_sop : boolean := false;
    g_increment_re_on_sop   : boolean := false;
    g_increment_im_on_sop   : boolean := false;
    g_restart_data_on_sync  : boolean := false;
    g_restart_re_on_sync    : boolean := false;
    g_restart_im_on_sync    : boolean := false;
    g_restart_data_on_sop   : boolean := false;
    g_restart_re_on_sop     : boolean := false;
    g_restart_im_on_sop     : boolean := false
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- MM control
    force_en      : in  std_logic := '0';
    force_data    : in  integer := 0;  -- used for sosi.data
    force_re      : in  integer := 0;  -- used for sosi.re
    force_im      : in  integer := 0;  -- used for sosi.im
    -- ST sink
    snk_out       : out t_dp_siso;
    snk_in        : in  t_dp_sosi;
    -- ST source
    src_in        : in  t_dp_siso := c_dp_siso_rdy;
    src_out       : out t_dp_sosi
  );
end dp_force_data_parallel;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

architecture str of dp_force_data_parallel is
  signal data_in          : t_dp_sosi;
  signal data_out         : t_dp_sosi;
begin
  p_comb : process(snk_in, force_en, force_data, force_re, force_im, data_out)
  begin
    data_in <= snk_in;
    if force_en = '1' then
      -- default force value
      data_in.data <= TO_DP_SDATA(force_data);
      data_in.re   <= TO_DP_DSP_DATA(force_re);
      data_in.im   <= TO_DP_DSP_DATA(force_im);
      -- optional increment on valid (no need to check snk_in.valid)
      if g_increment_data /= 0 then
        data_in.data <= RESIZE_DP_SDATA(std_logic_vector(signed(data_out.data(g_dat_w - 1 downto 0)) + g_increment_data));
      end if;
      if g_increment_re /= 0 then
        data_in.re   <= RESIZE_DP_DSP_DATA(std_logic_vector(signed(data_out.re(g_dat_w - 1 downto 0)) + g_increment_re));
      end if;
      if g_increment_im /= 0 then
        data_in.im   <= RESIZE_DP_DSP_DATA(std_logic_vector(signed(data_out.im(g_dat_w - 1 downto 0)) + g_increment_im));
      end if;
      -- optional increment only on sop
      if snk_in.sop = '0' then
        if g_increment_data_on_sop = true then data_in.data <= data_out.data; end if;
        if g_increment_re_on_sop = true   then data_in.re   <= data_out.re;   end if;
        if g_increment_im_on_sop = true   then data_in.im   <= data_out.im;   end if;
      end if;
      -- optional restart increment
      if snk_in.sync = '1' then
        if g_restart_data_on_sync = true then data_in.data <= TO_DP_SDATA(force_data);  end if;
        if g_restart_re_on_sync = true   then data_in.re   <= TO_DP_DSP_DATA(force_re); end if;
        if g_restart_im_on_sync = true   then data_in.im   <= TO_DP_DSP_DATA(force_im); end if;
      end if;
      if snk_in.sop = '1' then
        if g_restart_data_on_sop = true then data_in.data <= TO_DP_SDATA(force_data);  end if;
        if g_restart_re_on_sop = true   then data_in.re   <= TO_DP_DSP_DATA(force_re); end if;
        if g_restart_im_on_sop = true   then data_in.im   <= TO_DP_DSP_DATA(force_im); end if;
      end if;
    end if;
  end process;

  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => data_in,
    -- ST source
    src_in       => src_in,
    src_out      => data_out
  );

  src_out <= data_out;
end str;
