--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Forward input stream with added channel (and optional sync, BSN) field
-- Description:
-- . The above is implemented as follows:
--   . dp_block_gen_sop_eop_sync generates the SOP, EOP and Sync
--   . A clocked process adds:
--     . channel based on SOP.
--     . the BSN based on Sync.

entity corr_output_framer is
  generic (
    g_nof_streams                 : natural;  -- Number of input/output streams
    g_nof_folds                   : natural := 0;  -- If >0, channel indices are folded accordingly
    g_nof_channels                : natural;  -- Number of channels
    g_nof_channel_frames_per_sync : natural;  -- Number of channel frames (g_nof_channels long) per sync
    g_generate_sync_and_bsn       : boolean;  -- FALSE uses sync and BSN from snk_in_arr
    g_fft_channel_index_reorder   : boolean  -- TRUE if input data is FFT output that needs channel reordering
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    src_out_arr    : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end corr_output_framer;

architecture str of corr_output_framer is
  signal dp_pipeline_src_out_arr  : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal dp_block_gen_src_out     : t_dp_sosi;
  signal reg_dp_block_gen_src_out : t_dp_sosi;

  signal channel_bsn_src_out      : t_dp_sosi;
  signal nxt_channel_bsn_src_out  : t_dp_sosi;

  signal folded_word_cnt          : std_logic_vector(g_nof_folds - 1 downto 0);
  signal nxt_folded_word_cnt      : std_logic_vector(g_nof_folds - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- dp_block_gen to create correc SOP,EOP and Sync
  -----------------------------------------------------------------------------
  u_dp_block_gen_sop_eop_sync: entity dp_lib.dp_block_gen
  generic map (
    g_use_src_in         => false,
    g_nof_data           => g_nof_channels * pow2(g_nof_folds),
    g_nof_blk_per_sync   => g_nof_channel_frames_per_sync
  )
  port map (
    rst        => rst,
    clk        => clk,

    snk_in     => snk_in_arr(0),

    src_out    => dp_block_gen_src_out
  );

  -----------------------------------------------------------------------------
  -- Create channel (based on SOP) and BSN (based on Sync)
  -- . account for folding
  -----------------------------------------------------------------------------
  p_channel_bsn : process(dp_block_gen_src_out, channel_bsn_src_out, folded_word_cnt)
  begin
    nxt_channel_bsn_src_out.channel <= channel_bsn_src_out.channel;
    nxt_channel_bsn_src_out.bsn     <= channel_bsn_src_out.bsn;
    nxt_folded_word_cnt             <= folded_word_cnt;
    -- Channel

    if dp_block_gen_src_out.valid = '1' then
      nxt_folded_word_cnt <= INCR_UVEC(folded_word_cnt, 1);
      if unsigned(folded_word_cnt) = pow2(g_nof_folds) - 1 then
        nxt_channel_bsn_src_out.channel <= INCR_UVEC(channel_bsn_src_out.channel, 1);
        nxt_folded_word_cnt <= (others => '0');
      end if;
    end if;

    if dp_block_gen_src_out.sop = '1' then
      if (channel_bsn_src_out.channel = TO_DP_CHANNEL(g_nof_channels - 1)) and (unsigned(folded_word_cnt) = pow2(g_nof_folds) - 1) then
        nxt_channel_bsn_src_out.channel <= TO_DP_CHANNEL(0);
      end if;
    end if;

    -- BSN
    if dp_block_gen_src_out.sync = '1' then
      nxt_channel_bsn_src_out.bsn <= INCR_UVEC(channel_bsn_src_out.bsn, 1);
    end if;
  end process;

  -- Registers: also register dp_block_gen_src_out to align it with p_channel_bsn outputs
  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      channel_bsn_src_out      <= (c_dp_sosi_rst);
      reg_dp_block_gen_src_out <= (c_dp_sosi_rst);
      for i in 0 to g_nof_streams - 1 loop
        channel_bsn_src_out.channel <= X"FFFFFFFF";  -- Wrap to 0 on first increment
        channel_bsn_src_out.bsn     <= X"FFFFFFFF_FFFFFFFF";  -- Wrap to 0 on first increment
--        channel_bsn_src_out.channel <= (OTHERS=>'0');
--        channel_bsn_src_out.bsn     <= (OTHERS=>'0');
        folded_word_cnt             <= (others => '1');  -- Wrap to 0 on first increment
      end loop;
    elsif rising_edge(clk) then
      channel_bsn_src_out      <= nxt_channel_bsn_src_out;
      reg_dp_block_gen_src_out <= dp_block_gen_src_out;
      folded_word_cnt          <= nxt_folded_word_cnt;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Pipeline the inputs to align them with locally generated fields
  -----------------------------------------------------------------------------
  gen_dp_pipeline : for i in 0 to g_nof_streams - 1 generate
    u_dp_pipeline : entity dp_lib.dp_pipeline
    generic map (
      g_pipeline => 2
    )
    port map (
      rst         => rst,
      clk         => clk,

      snk_in      => snk_in_arr(i),
      src_out     => dp_pipeline_src_out_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Add channel,bsn fields to reg_dp_block_gen_src_out and forward to src_out_arr
  -----------------------------------------------------------------------------
  p_wires : process(reg_dp_block_gen_src_out, channel_bsn_src_out, dp_pipeline_src_out_arr)
  begin
    src_out_arr <= dp_pipeline_src_out_arr;
    for i in 0 to g_nof_streams - 1 loop
      src_out_arr(i).data    <= (others => '0');
      src_out_arr(i).sync    <= reg_dp_block_gen_src_out.sync;

      src_out_arr(i).channel <= channel_bsn_src_out.channel;
      src_out_arr(i).bsn     <= channel_bsn_src_out.bsn;
    end loop;
  end process;
end str;
