-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Provide MM slave register for common_pulse_delay
-- Description:
--   Set pulse_delay between incoming and outgoing pulse, in units of dp_clk cycles (5ns)

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity common_pulse_delay_reg is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and pulse_clk are the same, else use TRUE to cross the clock domain
    g_pulse_delay_max    : natural := 0  -- Maximum number of clk cycles that pulse can be delayed
   );
  port (
    pulse_clk   : in  std_logic;
    pulse_rst   : in  std_logic;
    pulse_delay : out std_logic_vector(ceil_log2(g_pulse_delay_max) - 1 downto 0);

    mm_clk      : in  std_logic;
    mm_rst      : in  std_logic;
    sla_in      : in  t_mem_mosi;
    sla_out     : out t_mem_miso
  );
end common_pulse_delay_reg;

architecture rtl of common_pulse_delay_reg is
  constant c_nof_mm_regs : natural := 1;

  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => ceil_log2(c_nof_mm_regs),
                                  dat_w    => c_word_w,
                                  nof_dat  => c_nof_mm_regs,
                                  init_sl  => '0');

  signal mm_pulse_delay : std_logic_vector(ceil_log2(g_pulse_delay_max) - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out                  <= c_mem_miso_rst;

      -- Access event, register values
      mm_pulse_delay         <= (others => '0');

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            -- Write pulse_delay
            mm_pulse_delay            <= sla_in.wrdata(ceil_log2(g_pulse_delay_max) - 1 downto 0);
          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            -- Read back pulse_delay
            sla_out.rddata(ceil_log2(g_pulse_delay_max) - 1 downto 0) <= mm_pulse_delay;
          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and pulse_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------
  no_common_reg_cross_domain : if g_cross_clock_domain = false generate  -- so mm_clk = pulse_clk
    pulse_delay <= mm_pulse_delay;
  end generate;  -- common_reg_cross_domain

  gen_common_reg_cross_domain : if g_cross_clock_domain = true generate
    u_common_reg_cross_domain : entity work.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => mm_pulse_delay,
      in_done     => OPEN,
      out_rst     => pulse_rst,
      out_clk     => pulse_clk,
      out_dat     => pulse_delay,
      out_new     => open
    );
  end generate;  -- gen_common_reg_cross_domain
end rtl;
