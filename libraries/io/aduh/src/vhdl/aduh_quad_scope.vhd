-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Scope component to show the captured quad ADC samples [A,B,C,D] at
--          the sample rate in the Wave Window
-- Remark:
-- . Only for simulation

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

entity aduh_quad_scope is
  generic (
    -- General
    g_sim  : boolean := false;
    -- ADC Interface
    g_ai   : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    DCLK         : in std_logic := '0';  -- digital processing clk

    -- Streaming samples (can be from ADU or from internal WG)
    sp_sosi_arr  : in t_dp_sosi_arr(0 to g_ai.nof_sp - 1)  -- = [0:3] = Signal Paths [A,B,C,D]
  );
end aduh_quad_scope;

architecture beh of aduh_quad_scope is
  constant c_wideband_factor : natural := g_ai.rx_factor * g_ai.dd_factor;  -- Wideband rate factor = 4 for dp_clk is 200 MHz frequency and sample frequency Fs is 800 MHz

  signal dp_sosi_arr         : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = DP [3:0] = ADU Signal Paths [D,C,B,A]
begin
  -- Reverse wire ADUH range [0:3] = A,B,C,D to DP [3:0] range
  rewire : for I in 0 to g_ai.nof_sp - 1 generate
    dp_sosi_arr(I) <= sp_sosi_arr(I);
  end generate;

  -- View sp_sosi_arr at the sample rate
  u_dp_scope : entity dp_lib.dp_wideband_sp_arr_scope
  generic map (
    g_sim                 => g_sim,
    g_use_sclk            => false,
    g_nof_streams         => g_ai.nof_sp,
    g_wideband_factor     => c_wideband_factor,
    g_wideband_big_endian => false,
    g_dat_w               => g_ai.port_w
  )
  port map (
    -- Digital processing clk
    DCLK         => DCLK,

    -- Streaming samples
    sp_sosi_arr  => dp_sosi_arr  -- = [3:0] = Signal Paths [D,C,B,A]
  );
end beh;
