-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . 1GbE wrapper for dp_statistics
-- Remark:
-- . Make sure that the Tx Eth also uses the transceiver sim model (and not
--   the transceiver IP model).
-- . The eth_clk for the Rx Eth is generated internally. The eth_clk that is
--   used for the Tx Eth should have the same timing, so initialized at '0'
--   and generated using eth_clk <= NOT eth_clk. If the sim link fails, then
--   it could be due to delta cycle difference between the Rx eth_clk and
--   the Tx eth_clk. Therefore it is better not to reassign the eth_clk like
--   in e.g. eth_clk <= tx_eth_clk, because signal assignment '<=' adds a
--   simulation delta cycle.
-- . The eth_serial_in becomes active /= '0' when the Tx Eth is actually
--   sending data, so this depends on the application or tb stimuli.
-- . The transceiver sim model use 'U' to signal eop in tx_serial_out, so
--   this is why then 'red' signal level appears in Wave window.

library IEEE, common_lib, work, technology_lib, dp_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use technology_lib.technology_select_pkg.all;

entity eth_statistics is
  generic (
    g_runtime_nof_packets      : natural;  -- Run the test bench for nof_packets before asserting tb_end
    g_runtime_timeout          : time;  -- Report Failure if g_runtime_nof_packets is not reached before this time
    g_check_nof_valid          : boolean := false;  -- True enables valid count checking at tb_end. Reports Failure in case of mismatch.
    g_check_nof_valid_ref      : natural := 0  -- Reference (= expected) valid count
  );
  port (
    eth_serial_in : in  std_logic;
    eth_src_out   : out t_dp_sosi;  -- Output received sosi
    tb_end        : out std_logic  -- To be used to stop test-bench generated clocks
  );
end eth_statistics;

architecture str of eth_statistics is
  constant c_eth_word_w       : natural := 32;
  constant c_eth_clk_period   : time := 8 ns;

  signal eth_clk              : std_logic := '0';
  signal eth_rst              : std_logic;

  signal tech_tse_rx_src_out  : t_dp_sosi;

  signal i_tb_end             : std_logic;
begin
  ------------------------------------------------------------------------------
  -- We're using the tb_end output locally
  ------------------------------------------------------------------------------
  tb_end <= i_tb_end;

  ------------------------------------------------------------------------------
  -- We're generating a clock locally; it happens to be in sync with the 125Mz
  -- transmitter clock that is generated from 25MHz by a PLL.
  ------------------------------------------------------------------------------
  eth_clk <= not eth_clk or i_tb_end after c_eth_clk_period / 2;
  eth_rst <= '1', '0' after c_eth_clk_period * 8;

  ------------------------------------------------------------------------------
  -- Use tech_tse with internal simulation model as Ethernet receiver
  ------------------------------------------------------------------------------
  u_tech_tse : entity tech_tse_lib.tech_tse
  generic map(
    g_sim       => true,
    g_sim_level => 1,
    g_sim_tx    => false,
    g_sim_rx    => true
  )
  port map (
    mm_rst         => '0',
    mm_clk         => '0',
    cal_rec_clk    => '0',

    eth_clk        => eth_clk,

    mm_sla_in      => c_mem_mosi_rst,

    tx_mac_in      => ('0', '0', '0', '0', '0'),

    tx_snk_clk     => eth_clk,
    tx_snk_in      => c_dp_sosi_rst,

    rx_src_clk     => eth_clk,
    rx_src_in      => c_dp_siso_rdy,

    rx_src_out     => tech_tse_rx_src_out,

    eth_rxp        => eth_serial_in
  );

  ------------------------------------------------------------------------------
  -- dp_statistics
  ------------------------------------------------------------------------------
  u_dp_statistics : entity dp_lib.dp_statistics
    generic map (
      g_runtime_nof_packets      => g_runtime_nof_packets,
      g_runtime_timeout          => g_runtime_timeout,
      g_check_nof_valid          => g_check_nof_valid,
      g_check_nof_valid_ref      => g_check_nof_valid_ref,
      g_dp_word_w                => c_eth_word_w
    )
  port map (
    dp_clk => eth_clk,
    dp_rst => eth_rst,

    snk_in => tech_tse_rx_src_out,

    tb_end => i_tb_end
  );

  -- Output the received decoded data, to support further external analysis
  eth_src_out <= tech_tse_rx_src_out;
end str;
