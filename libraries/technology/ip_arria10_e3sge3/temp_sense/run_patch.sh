#!/bin/bash 

patchfile='altera_temp_sense.sdc.patch'

echo -e "Applying patch: $patchfile\n"

cd generated/altera_temp_sense_151/synth/
patch  <../../../${patchfile}

echo "done."
