-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
-- . This package contains specific constants, functions for sdp test benches.
-- Description:
-------------------------------------------------------------------------------
library IEEE, common_lib, dp_lib, reorder_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.tb_common_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use reorder_lib.reorder_pkg.all;
  use work.sdp_pkg.all;

package tb_sdp_pkg is
  -----------------------------------------------------------------------------
  -- Derive low part of MAC, IP from global node (GN) index
  -----------------------------------------------------------------------------
  function func_sdp_gn_index_to_mac_15_0(gn_index : natural) return std_logic_vector;
  function func_sdp_gn_index_to_ip_15_0(gn_index : natural) return std_logic_vector;

  -----------------------------------------------------------------------------
  -- Statistics offload
  -----------------------------------------------------------------------------
  function func_sdp_compose_stat_header(ip_header_checksum     : natural;
                                        sdp_info               : t_sdp_info;  -- app header
                                        g_statistics_type      : string;
                                        weighted_subbands_flag : std_logic;
                                        gn_index               : natural;
                                        nof_block_per_sync     : natural;
                                        sst_signal_input       : natural;
                                        beamlet_index          : natural;
                                        subband_index          : natural;
                                        xst_signal_input_A     : natural;
                                        xst_signal_input_B     : natural;
                                        dp_bsn                 : natural) return t_sdp_stat_header;

  function func_sdp_verify_stat_header(g_statistics_type : string; in_hdr, exp_hdr : t_sdp_stat_header) return boolean;

  -----------------------------------------------------------------------------
  -- Beamlet output via 10GbE to CEP (= central processor)
  -----------------------------------------------------------------------------
  function func_sdp_compose_cep_header(ip_src_addr            : std_logic_vector;
                                       ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       nof_blocks_per_packet  : natural;
                                       nof_beamlets_per_block : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header;

  function func_sdp_compose_cep_header(ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       nof_blocks_per_packet  : natural;
                                       nof_beamlets_per_block : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header;

  function func_sdp_compose_cep_header(ip_src_addr            : std_logic_vector;
                                       ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header;

  function func_sdp_compose_cep_header(ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header;

  function func_sdp_verify_cep_header(in_hdr            : t_sdp_cep_header;
                                      exp_hdr           : t_sdp_cep_header;
                                      beamlet_index_mod : boolean) return boolean;
  function func_sdp_verify_cep_header(in_hdr  : t_sdp_cep_header;
                                      exp_hdr : t_sdp_cep_header) return boolean;

  -----------------------------------------------------------------------------
  -- Subband equalizer (ESub)
  -----------------------------------------------------------------------------
  -- - ESub is like a two input beamformer
  -- - Esub function calculates the expected subband level for signal input sp:
  --   sp phasor * sp_weight + cross_phasor * cross weigth, where
  --   . sp phasor = (sp_subband_ampl, sp_subband_phase)
  --   . sp_weight = (sp_esub_gain, sp_esub_phase)
  --   . cross_phasor = (cross_subband_ampl, cross_subband_phase)
  --   . cross_weight = (cross_esub_gain, cross_esub_phase)
  function func_sdp_subband_equalizer(sp_subband_ampl, sp_subband_phase, sp_esub_gain, sp_esub_phase,
                                      cross_subband_ampl, cross_subband_phase, cross_esub_gain, cross_esub_phase : real)
                                      return t_real_arr;  -- 0:3 = ampl, phase, re, im

  -----------------------------------------------------------------------------
  -- Beamformer (BF)
  -----------------------------------------------------------------------------
  -- Model the SDP beamformer for one signal input (sp) and nof_rem remnant signal inputs (rem)
  -- . for local beamformer on one node use nof_rem = S_pn - 1
  -- . for remote beamformer with nof_rn ring nodes use nof_rem = nof_rn * S_pn - 1
  function func_sdp_beamformer(sp_subband_ampl, sp_subband_phase, sp_bf_gain, sp_bf_phase,
                               rem_subband_ampl, rem_subband_phase, rem_bf_gain, rem_bf_phase : real;
                               nof_rem : natural)
                               return t_real_arr;  -- 0:3 = ampl, phase, re, im

  -----------------------------------------------------------------------------
  -- Beamlet output packet
  -----------------------------------------------------------------------------
  -- beamlet complex part index [0 : 3] of X, Y, X, Y in network longword:
  -- - use separate array for re part and for im part:
  --   . re[0 : 3] at 0, 2, 4, 6 in longword
  --   . im[0 : 3] at 1, 3, 5, 7 in longword
  subtype t_sdp_beamlet_part_arr is t_slv_8_arr(0 to c_sdp_nof_beamlets_per_longword * c_sdp_N_pol_bf - 1);

  -- beamlet part index in packet with 4 blocks [0 : 4 * 488 * 2 - 1] = [0 : 3903]
  -- . use separate list for re and for im
  subtype t_sdp_beamlet_packet_list is t_slv_8_arr(0 to c_sdp_cep_nof_beamlets_per_packet * c_sdp_N_pol_bf - 1);

  -- beamlet part index in one block [0 : 488 * 2 - 1] =  [0 : 975]
  -- . use separate list for re and for im
  subtype t_sdp_beamlet_block_list is t_slv_8_arr(0 to c_sdp_cep_nof_beamlets_per_block * c_sdp_N_pol_bf - 1);

  function func_sdp_transpose_beamlet_packet(     packet_list : t_sdp_beamlet_packet_list)
      return t_sdp_beamlet_packet_list;
  function func_sdp_undo_transpose_beamlet_packet(packet_list : t_sdp_beamlet_packet_list)
      return t_sdp_beamlet_packet_list;

  -- Read beamlet packet octets per re and im parts
  procedure proc_sdp_rx_beamlet_octets(
      constant c_nof_blocks_per_packet  : in natural;
      signal   clk                      : in std_logic;
      signal   rx_beamlet_sosi          : in t_dp_sosi;
      signal   rx_beamlet_cnt           : inout natural;
      signal   rx_beamlet_valid         : out std_logic;
      signal   rx_beamlet_arr_re        : out t_sdp_beamlet_part_arr;
      signal   rx_beamlet_arr_im        : out t_sdp_beamlet_part_arr;
      signal   rx_packet_list_re        : out t_slv_8_arr;
      signal   rx_packet_list_im        : out t_slv_8_arr);

  procedure proc_sdp_rx_beamlet_octets(
      signal clk               : in std_logic;
      signal rx_beamlet_sosi   : in t_dp_sosi;
      signal rx_beamlet_cnt    : inout natural;
      signal rx_beamlet_valid  : out std_logic;
      signal rx_beamlet_arr_re : out t_sdp_beamlet_part_arr;
      signal rx_beamlet_arr_im : out t_sdp_beamlet_part_arr;
      signal rx_packet_list_re : out t_sdp_beamlet_packet_list;
      signal rx_packet_list_im : out t_sdp_beamlet_packet_list);
end package tb_sdp_pkg;

package body tb_sdp_pkg is
  function func_sdp_gn_index_to_mac_15_0(gn_index : natural) return std_logic_vector is
    constant c_unb_nr    : natural := gn_index / 4;  -- 4 PN per Uniboard2
    constant c_node_nr   : natural := gn_index mod 4;
    constant c_mac_15_0  : std_logic_vector(15 downto 0) := TO_UVEC(c_unb_nr, 8) & TO_UVEC(c_node_nr, 8);
  begin
    return c_mac_15_0;
  end func_sdp_gn_index_to_mac_15_0;

  function func_sdp_gn_index_to_ip_15_0(gn_index : natural) return std_logic_vector is
    constant c_unb_nr    : natural := gn_index / 4;  -- 4 PN per Uniboard2
    constant c_node_nr   : natural := gn_index mod 4;
    constant c_ip_15_0   : std_logic_vector(15 downto 0) :=
                               TO_UVEC(c_unb_nr, 8) & TO_UVEC(c_node_nr + 1, 8);  -- +1 to avoid IP = *.*.*.0
  begin
    return c_ip_15_0;
  end func_sdp_gn_index_to_ip_15_0;

  function func_sdp_compose_stat_header(ip_header_checksum     : natural;
                                        sdp_info               : t_sdp_info;  -- app header
                                        g_statistics_type      : string;
                                        weighted_subbands_flag : std_logic;
                                        gn_index               : natural;
                                        nof_block_per_sync     : natural;
                                        sst_signal_input       : natural;
                                        beamlet_index          : natural;
                                        subband_index          : natural;
                                        xst_signal_input_A     : natural;
                                        xst_signal_input_B     : natural;
                                        dp_bsn                 : natural) return t_sdp_stat_header is
    -- Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on gn_index
    constant c_mac_15_0                  : std_logic_vector(15 downto 0) := func_sdp_gn_index_to_mac_15_0(gn_index);
    constant c_ip_15_0                   : std_logic_vector(15 downto 0) := func_sdp_gn_index_to_ip_15_0(gn_index);

    constant c_nof_statistics_per_packet : natural := func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type);
    constant c_udp_total_length          : natural := func_sdp_get_stat_udp_total_length(g_statistics_type);
    constant c_ip_total_length           : natural := func_sdp_get_stat_ip_total_length(g_statistics_type);
    constant c_marker                    : natural := func_sdp_get_stat_marker(g_statistics_type);
    constant c_nof_signal_inputs         : natural := func_sdp_get_stat_nof_signal_inputs(g_statistics_type);

    variable v_hdr : t_sdp_stat_header;
  begin
    -- eth header
    v_hdr.eth.dst_mac        := c_sdp_stat_eth_dst_mac;
    v_hdr.eth.src_mac        := c_sdp_stat_eth_src_mac_47_16 & c_mac_15_0;
    v_hdr.eth.eth_type       := x"0800";

    -- ip header
    v_hdr.ip.version         := TO_UVEC(                4, c_network_ip_version_w);
    v_hdr.ip.header_length   := TO_UVEC(                5, c_network_ip_header_length_w);
    v_hdr.ip.services        := TO_UVEC(                0, c_network_ip_services_w);
    v_hdr.ip.total_length    := TO_UVEC(c_ip_total_length, c_network_ip_total_length_w);
    v_hdr.ip.identification  := TO_UVEC(                0, c_network_ip_identification_w);
    v_hdr.ip.flags           := TO_UVEC(                2, c_network_ip_flags_w);
    v_hdr.ip.fragment_offset := TO_UVEC(                0, c_network_ip_fragment_offset_w);
    v_hdr.ip.time_to_live    := TO_UVEC(              127, c_network_ip_time_to_live_w);
    v_hdr.ip.protocol        := TO_UVEC(               17, c_network_ip_protocol_w);
    -- the IP header check sum is calculated in IO eth, so still 0 here
    v_hdr.ip.header_checksum := TO_UVEC(ip_header_checksum, c_network_ip_header_checksum_w);
    v_hdr.ip.src_ip_addr     := c_sdp_stat_ip_src_addr_31_16 & c_ip_15_0;  -- c_network_ip_addr_w
    v_hdr.ip.dst_ip_addr     := c_sdp_stat_ip_dst_addr;  -- c_network_ip_addr_w

    -- udp header
    v_hdr.udp.src_port       := func_sdp_get_stat_udp_src_port(g_statistics_type, gn_index);
    v_hdr.udp.dst_port       :=   c_sdp_stat_udp_dst_port;
    v_hdr.udp.total_length   := TO_UVEC(c_udp_total_length, c_network_udp_port_w);
    v_hdr.udp.checksum       := TO_UVEC(                 0, c_network_udp_checksum_w);

    -- app header
    v_hdr.app.sdp_marker                              := TO_UVEC(c_marker, 8);
    v_hdr.app.sdp_version_id                          := TO_UVEC(c_sdp_stat_version_id, 8);
    v_hdr.app.sdp_observation_id                      := sdp_info.observation_id;
    v_hdr.app.sdp_station_info                        := sdp_info.antenna_field_index & sdp_info.station_id;

    v_hdr.app.sdp_source_info_antenna_band_id         := slv(sdp_info.antenna_band_index);
    v_hdr.app.sdp_source_info_nyquist_zone_id         :=     sdp_info.nyquist_zone_index;
    v_hdr.app.sdp_source_info_f_adc                   := slv(sdp_info.f_adc);
    v_hdr.app.sdp_source_info_fsub_type               := slv(sdp_info.fsub_type);
    v_hdr.app.sdp_source_info_payload_error           := TO_UVEC(0, 1);
    v_hdr.app.sdp_source_info_beam_repositioning_flag := slv(sdp_info.beam_repositioning_flag);
    v_hdr.app.sdp_source_info_weighted_subbands_flag  := slv(weighted_subbands_flag);
    v_hdr.app.sdp_source_info_gn_id                   := TO_UVEC(gn_index, 8);

    v_hdr.app.sdp_reserved                            := TO_UVEC(0, 8);
    v_hdr.app.sdp_integration_interval                := TO_UVEC(nof_block_per_sync, 24);
    if g_statistics_type = "SST" or g_statistics_type = "SST_OS" then
      v_hdr.app.sdp_data_id                           := TO_UVEC(sst_signal_input, 32);
      v_hdr.app.sdp_data_id_sst_signal_input_index    := TO_UVEC(sst_signal_input,  8);
    elsif g_statistics_type = "BST" then
      v_hdr.app.sdp_data_id                           := TO_UVEC(beamlet_index, 32);
      v_hdr.app.sdp_data_id_bst_beamlet_index         := TO_UVEC(beamlet_index, 16);
    elsif g_statistics_type = "XST" then
      v_hdr.app.sdp_data_id                           := TO_UVEC(0, 7) &
                                                         TO_UVEC(subband_index, 9) &
                                                         TO_UVEC(xst_signal_input_A, 8) &
                                                         TO_UVEC(xst_signal_input_B, 8);
      v_hdr.app.sdp_data_id_xst_subband_index         := TO_UVEC(subband_index, 9);
      v_hdr.app.sdp_data_id_xst_signal_input_A_index  := TO_UVEC(xst_signal_input_A, 8);
      v_hdr.app.sdp_data_id_xst_signal_input_B_index  := TO_UVEC(xst_signal_input_B, 8);
    end if;
    v_hdr.app.sdp_nof_signal_inputs                   := TO_UVEC(          c_nof_signal_inputs,  8);
    v_hdr.app.sdp_nof_bytes_per_statistic             := TO_UVEC(c_sdp_nof_bytes_per_statistic,  8);
    v_hdr.app.sdp_nof_statistics_per_packet           := TO_UVEC(  c_nof_statistics_per_packet, 16);
    v_hdr.app.sdp_block_period                        := sdp_info.block_period;

    v_hdr.app.dp_bsn := TO_UVEC(dp_bsn, 64);
    return v_hdr;
  end func_sdp_compose_stat_header;

  function func_sdp_verify_stat_header(g_statistics_type : string; in_hdr, exp_hdr : t_sdp_stat_header)
      return boolean is
  begin
    -- eth header
    assert in_hdr.eth.dst_mac = exp_hdr.eth.dst_mac
      report "Wrong " & g_statistics_type & " eth.dst_mac"
      severity ERROR;
    assert in_hdr.eth.src_mac = exp_hdr.eth.src_mac
      report "Wrong " & g_statistics_type & " eth.src_mac"
      severity ERROR;
    assert in_hdr.eth.eth_type = exp_hdr.eth.eth_type
      report "Wrong " & g_statistics_type & " eth.eth_type"
      severity ERROR;

    -- ip header
    assert in_hdr.ip.version = exp_hdr.ip.version
      report "Wrong " & g_statistics_type & " ip.version"
      severity ERROR;
    assert in_hdr.ip.header_length = exp_hdr.ip.header_length
      report "Wrong " & g_statistics_type & " ip.header_length"
      severity ERROR;
    assert in_hdr.ip.services = exp_hdr.ip.services
      report "Wrong " & g_statistics_type & " ip.services"
      severity ERROR;
    assert in_hdr.ip.total_length = exp_hdr.ip.total_length
      report "Wrong " & g_statistics_type & " ip.total_length"
      severity ERROR;
    assert in_hdr.ip.identification = exp_hdr.ip.identification
      report "Wrong " & g_statistics_type & " ip.identification"
      severity ERROR;
    assert in_hdr.ip.flags = exp_hdr.ip.flags
      report "Wrong " & g_statistics_type & " ip.flags"
      severity ERROR;
    assert in_hdr.ip.fragment_offset = exp_hdr.ip.fragment_offset
      report "Wrong " & g_statistics_type & " ip.fragment_offset"
      severity ERROR;
    assert in_hdr.ip.time_to_live = exp_hdr.ip.time_to_live
      report "Wrong " & g_statistics_type & " ip.time_to_live"
      severity ERROR;
    assert in_hdr.ip.protocol = exp_hdr.ip.protocol
      report "Wrong " & g_statistics_type & " ip.protocol"
      severity ERROR;
    assert in_hdr.ip.header_checksum = exp_hdr.ip.header_checksum
      report "Wrong " & g_statistics_type & " ip.header_checksum"
      severity ERROR;
    assert in_hdr.ip.src_ip_addr = exp_hdr.ip.src_ip_addr
      report "Wrong " & g_statistics_type & " ip.src_ip_addr"
      severity ERROR;
    assert in_hdr.ip.dst_ip_addr = exp_hdr.ip.dst_ip_addr
      report "Wrong " & g_statistics_type & " ip.dst_ip_addr"
      severity ERROR;

    -- udp header
    assert in_hdr.udp.src_port = exp_hdr.udp.src_port
      report "Wrong " & g_statistics_type & " udp.src_port"
      severity ERROR;
    assert in_hdr.udp.dst_port = exp_hdr.udp.dst_port
      report "Wrong " & g_statistics_type & " udp.dst_port"
      severity ERROR;
    assert in_hdr.udp.total_length = exp_hdr.udp.total_length
      report "Wrong " & g_statistics_type & " udp.total_length"
      severity ERROR;
    assert in_hdr.udp.checksum = exp_hdr.udp.checksum
      report "Wrong " & g_statistics_type & " udp.checksum"
      severity ERROR;

    -- app header
    assert in_hdr.app.sdp_marker = exp_hdr.app.sdp_marker
      report "Wrong " & g_statistics_type & " app.sdp_marker"
      severity ERROR;
    assert in_hdr.app.sdp_version_id = exp_hdr.app.sdp_version_id
      report "Wrong " & g_statistics_type & " app.sdp_version_id"
      severity ERROR;
    assert in_hdr.app.sdp_observation_id = exp_hdr.app.sdp_observation_id
      report "Wrong " & g_statistics_type & " app.sdp_observation_id"
      severity ERROR;
    assert in_hdr.app.sdp_station_info = exp_hdr.app.sdp_station_info
      report "Wrong " & g_statistics_type & " app.sdp_station_info"
      severity ERROR;

    assert in_hdr.app.sdp_source_info_antenna_band_id = exp_hdr.app.sdp_source_info_antenna_band_id
      report "Wrong " & g_statistics_type & " app.sdp_source_info_antenna_band_id"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_nyquist_zone_id = exp_hdr.app.sdp_source_info_nyquist_zone_id
      report "Wrong " & g_statistics_type & " app.sdp_source_info_nyquist_zone_id"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_f_adc = exp_hdr.app.sdp_source_info_f_adc
      report "Wrong " & g_statistics_type & " app.sdp_source_info_f_adc"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_fsub_type = exp_hdr.app.sdp_source_info_fsub_type
      report "Wrong " & g_statistics_type & " app.sdp_source_info_fsub_type"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_payload_error = exp_hdr.app.sdp_source_info_payload_error
      report "Wrong " & g_statistics_type & " app.sdp_source_info_payload_error"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_beam_repositioning_flag = exp_hdr.app.sdp_source_info_beam_repositioning_flag
      report "Wrong " & g_statistics_type & " app.sdp_source_info_beam_repositioning_flag"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_weighted_subbands_flag = exp_hdr.app.sdp_source_info_weighted_subbands_flag
      report "Wrong " & g_statistics_type & " app.sdp_source_info_weighted_subbands_flag"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_gn_id = exp_hdr.app.sdp_source_info_gn_id
      report "Wrong " & g_statistics_type & " app.sdp_source_info_gn_id"
      severity ERROR;

    assert in_hdr.app.sdp_reserved = exp_hdr.app.sdp_reserved
      report "Wrong " & g_statistics_type & " app.sdp_reserved"
      severity ERROR;
    assert in_hdr.app.sdp_integration_interval = exp_hdr.app.sdp_integration_interval
      report "Wrong " & g_statistics_type & " app.sdp_integration_interval"
      severity ERROR;

    -- . sdp_data_id word and fields per g_statistics_type
    assert in_hdr.app.sdp_data_id = exp_hdr.app.sdp_data_id
      report "Wrong " & g_statistics_type & " app.sdp_data_id"
      severity ERROR;
    if g_statistics_type = "SST" or g_statistics_type = "SST_OS" then
      assert in_hdr.app.sdp_data_id_sst_signal_input_index = exp_hdr.app.sdp_data_id_sst_signal_input_index
        report "Wrong " & g_statistics_type & " app.sdp_data_id_sst_signal_input_index"
        severity ERROR;
    elsif g_statistics_type = "BST" then
      assert in_hdr.app.sdp_data_id_bst_beamlet_index = exp_hdr.app.sdp_data_id_bst_beamlet_index
        report "Wrong " & g_statistics_type & " app.sdp_data_id_bst_beamlet_index"
        severity ERROR;
    elsif g_statistics_type = "XST" then
      assert in_hdr.app.sdp_data_id_xst_subband_index = exp_hdr.app.sdp_data_id_xst_subband_index
        report "Wrong " & g_statistics_type & " app.sdp_data_id_xst_subband_index"
        severity ERROR;
      assert in_hdr.app.sdp_data_id_xst_signal_input_A_index = exp_hdr.app.sdp_data_id_xst_signal_input_A_index
        report "Wrong " & g_statistics_type & " app.sdp_data_id_xst_signal_input_A_index"
        severity ERROR;
      assert in_hdr.app.sdp_data_id_xst_signal_input_B_index = exp_hdr.app.sdp_data_id_xst_signal_input_B_index
        report "Wrong " & g_statistics_type & " app.sdp_data_id_xst_signal_input_B_index"
        severity ERROR;
    else
      report "Wrong " & g_statistics_type
        severity FAILURE;
    end if;

    assert in_hdr.app.sdp_nof_signal_inputs = exp_hdr.app.sdp_nof_signal_inputs
      report "Wrong " & g_statistics_type & " app.sdp_nof_signal_inputs"
      severity ERROR;
    assert in_hdr.app.sdp_nof_bytes_per_statistic = exp_hdr.app.sdp_nof_bytes_per_statistic
      report "Wrong " & g_statistics_type & " app.sdp_nof_bytes_per_statistic"
      severity ERROR;
    assert in_hdr.app.sdp_nof_statistics_per_packet = exp_hdr.app.sdp_nof_statistics_per_packet
      report "Wrong " & g_statistics_type & " app.sdp_nof_statistics_per_packet"
      severity ERROR;
    assert in_hdr.app.sdp_block_period = exp_hdr.app.sdp_block_period
      report "Wrong " & g_statistics_type & " app.sdp_block_period"
      severity ERROR;

    assert in_hdr.app.dp_bsn = exp_hdr.app.dp_bsn
      report "Wrong " & g_statistics_type & " app.dp_bsn"
      severity ERROR;
    return true;
  end func_sdp_verify_stat_header;

  function func_sdp_compose_cep_header(ip_src_addr            : std_logic_vector;
                                       ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       nof_blocks_per_packet  : natural;
                                       nof_beamlets_per_block : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header is
    -- Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on gn_index
    constant c_mac_15_0 : std_logic_vector(15 downto 0) := func_sdp_gn_index_to_mac_15_0(gn_index);
    variable v_hdr : t_sdp_cep_header;
  begin
    -- eth header
    v_hdr.eth.dst_mac        := c_sdp_cep_eth_dst_mac;
    v_hdr.eth.src_mac        := c_sdp_cep_eth_src_mac_47_16 & c_mac_15_0;
    v_hdr.eth.eth_type       := x"0800";

    -- ip header
    v_hdr.ip.version         := TO_UVEC(                        4, c_network_ip_version_w);
    v_hdr.ip.header_length   := TO_UVEC(                        5, c_network_ip_header_length_w);
    v_hdr.ip.services        := TO_UVEC(                        0, c_network_ip_services_w);
    v_hdr.ip.total_length    :=         c_sdp_cep_ip_total_length;
    v_hdr.ip.identification  := TO_UVEC(                        0, c_network_ip_identification_w);
    v_hdr.ip.flags           := TO_UVEC(                        2, c_network_ip_flags_w);
    v_hdr.ip.fragment_offset := TO_UVEC(                        0, c_network_ip_fragment_offset_w);
    v_hdr.ip.time_to_live    := TO_UVEC(                      127, c_network_ip_time_to_live_w);
    v_hdr.ip.protocol        := TO_UVEC(                       17, c_network_ip_protocol_w);
    v_hdr.ip.header_checksum := TO_UVEC(       ip_header_checksum, c_network_ip_header_checksum_w);
    v_hdr.ip.src_ip_addr     := ip_src_addr;  -- c_network_ip_addr_w
    v_hdr.ip.dst_ip_addr     := c_sdp_cep_ip_dst_addr;  -- c_network_ip_addr_w

    -- udp header
    v_hdr.udp.src_port       := c_sdp_cep_udp_src_port_15_8 & TO_UVEC(gn_index, 8);
    v_hdr.udp.dst_port       := c_sdp_cep_udp_dst_port;
    v_hdr.udp.total_length   := c_sdp_cep_udp_total_length;
    v_hdr.udp.checksum       := TO_UVEC(0, c_network_udp_checksum_w);

    -- app header
    v_hdr.app.sdp_marker                              := TO_UVEC(c_sdp_marker_beamlets, 8);
    v_hdr.app.sdp_version_id                          := TO_UVEC(c_sdp_cep_version_id, 8);
    v_hdr.app.sdp_observation_id                      := sdp_info.observation_id;
    v_hdr.app.sdp_station_info                        := sdp_info.antenna_field_index & sdp_info.station_id;

    v_hdr.app.sdp_source_info_reserved                := TO_UVEC(0, 5);
    v_hdr.app.sdp_source_info_antenna_band_id         := slv(sdp_info.antenna_band_index);
    v_hdr.app.sdp_source_info_nyquist_zone_id         :=     sdp_info.nyquist_zone_index;
    v_hdr.app.sdp_source_info_f_adc                   := slv(sdp_info.f_adc);
    v_hdr.app.sdp_source_info_fsub_type               := slv(sdp_info.fsub_type);
    v_hdr.app.sdp_source_info_payload_error           := slv(payload_error);
    v_hdr.app.sdp_source_info_beam_repositioning_flag := slv(sdp_info.beam_repositioning_flag);
    v_hdr.app.sdp_source_info_beamlet_width           := TO_UVEC(c_sdp_W_beamlet, 4);
    v_hdr.app.sdp_source_info_gn_id                   := TO_UVEC(gn_index, 8);

    v_hdr.app.sdp_reserved                            := TO_UVEC(0, 32);
    v_hdr.app.sdp_beamlet_scale                       := TO_UVEC(beamlet_scale, 16);
    v_hdr.app.sdp_beamlet_index                       := TO_UVEC(beamlet_index, 16);
    v_hdr.app.sdp_nof_blocks_per_packet               := TO_UVEC(nof_blocks_per_packet, 8);
    v_hdr.app.sdp_nof_beamlets_per_block              := TO_UVEC(nof_beamlets_per_block, 16);
    v_hdr.app.sdp_block_period                        := sdp_info.block_period;

    v_hdr.app.dp_bsn := TO_UVEC(dp_bsn, 64);
    return v_hdr;
  end func_sdp_compose_cep_header;

  function func_sdp_compose_cep_header(ip_header_checksum     : natural;
                                       sdp_info               : t_sdp_info;  -- app header
                                       gn_index               : natural;
                                       payload_error          : std_logic;
                                       beamlet_scale          : natural;
                                       beamlet_index          : natural;
                                       nof_blocks_per_packet  : natural;
                                       nof_beamlets_per_block : natural;
                                       dp_bsn                 : natural) return t_sdp_cep_header is
    -- Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on gn_index
    constant c_ip_15_0     : std_logic_vector(15 downto 0) := func_sdp_gn_index_to_ip_15_0(gn_index);
    constant c_ip_src_addr : std_logic_vector(31 downto 0) := c_sdp_cep_ip_src_addr_31_16 & c_ip_15_0;
  begin
    return func_sdp_compose_cep_header(c_ip_src_addr,
                                       ip_header_checksum,
                                       sdp_info,
                                       gn_index,
                                       payload_error,
                                       beamlet_scale,
                                       beamlet_index,
                                       nof_blocks_per_packet,
                                       nof_beamlets_per_block,
                                       dp_bsn);
  end func_sdp_compose_cep_header;

  function func_sdp_compose_cep_header(ip_src_addr        : std_logic_vector;
                                       ip_header_checksum : natural;
                                       sdp_info           : t_sdp_info;  -- app header
                                       gn_index           : natural;
                                       payload_error      : std_logic;
                                       beamlet_scale      : natural;
                                       beamlet_index      : natural;
                                       dp_bsn             : natural) return t_sdp_cep_header is
  begin
    return func_sdp_compose_cep_header(ip_src_addr,
                                       ip_header_checksum,
                                       sdp_info,
                                       gn_index,
                                       payload_error,
                                       beamlet_scale,
                                       beamlet_index,
                                       c_sdp_cep_nof_blocks_per_packet,
                                       c_sdp_cep_nof_beamlets_per_block,
                                       dp_bsn);
  end func_sdp_compose_cep_header;

  function func_sdp_compose_cep_header(ip_header_checksum : natural;
                                       sdp_info           : t_sdp_info;  -- app header
                                       gn_index           : natural;
                                       payload_error      : std_logic;
                                       beamlet_scale      : natural;
                                       beamlet_index      : natural;
                                       dp_bsn             : natural) return t_sdp_cep_header is
    -- Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on gn_index
    constant c_ip_15_0     : std_logic_vector(15 downto 0) := func_sdp_gn_index_to_ip_15_0(gn_index);
    constant c_ip_src_addr : std_logic_vector(31 downto 0) := c_sdp_cep_ip_src_addr_31_16 & c_ip_15_0;
  begin
    return func_sdp_compose_cep_header(c_ip_src_addr,
                                       ip_header_checksum,
                                       sdp_info,
                                       gn_index,
                                       payload_error,
                                       beamlet_scale,
                                       beamlet_index,
                                       c_sdp_cep_nof_blocks_per_packet,
                                       c_sdp_cep_nof_beamlets_per_block,
                                       dp_bsn);
  end func_sdp_compose_cep_header;

  function func_sdp_verify_cep_header(in_hdr            : t_sdp_cep_header;
                                      exp_hdr           : t_sdp_cep_header;
                                      beamlet_index_mod : boolean) return boolean is
    variable v_beamlet_index : natural;
  begin
    -- eth header
    assert in_hdr.eth.dst_mac = exp_hdr.eth.dst_mac
      report "Wrong beamlet eth.dst_mac"
      severity ERROR;
    assert in_hdr.eth.src_mac = exp_hdr.eth.src_mac
      report "Wrong beamlet eth.src_mac"
      severity ERROR;
    assert in_hdr.eth.eth_type = exp_hdr.eth.eth_type
      report "Wrong beamlet eth.eth_type"
      severity ERROR;

    -- ip header
    assert in_hdr.ip.version = exp_hdr.ip.version
      report "Wrong beamlet ip.version"
      severity ERROR;
    assert in_hdr.ip.header_length = exp_hdr.ip.header_length
      report "Wrong beamlet ip.header_length"
      severity ERROR;
    assert in_hdr.ip.services = exp_hdr.ip.services
      report "Wrong beamlet ip.services"
      severity ERROR;
    assert in_hdr.ip.total_length = exp_hdr.ip.total_length
      report "Wrong beamlet ip.total_length"
      severity ERROR;
    assert in_hdr.ip.identification = exp_hdr.ip.identification
      report "Wrong beamlet ip.identification"
      severity ERROR;
    assert in_hdr.ip.flags = exp_hdr.ip.flags
      report "Wrong beamlet ip.flags"
      severity ERROR;
    assert in_hdr.ip.fragment_offset = exp_hdr.ip.fragment_offset
      report "Wrong beamlet ip.fragment_offset"
      severity ERROR;
    assert in_hdr.ip.time_to_live = exp_hdr.ip.time_to_live
      report "Wrong beamlet ip.time_to_live"
      severity ERROR;
    assert in_hdr.ip.protocol = exp_hdr.ip.protocol
      report "Wrong beamlet ip.protocol"
      severity ERROR;
    assert in_hdr.ip.header_checksum = exp_hdr.ip.header_checksum
      report "Wrong beamlet ip.header_checksum"
      severity ERROR;
    assert in_hdr.ip.src_ip_addr = exp_hdr.ip.src_ip_addr
      report "Wrong beamlet ip.src_ip_addr"
      severity ERROR;
    assert in_hdr.ip.dst_ip_addr = exp_hdr.ip.dst_ip_addr
      report "Wrong beamlet ip.dst_ip_addr"
      severity ERROR;

    -- udp header
    assert in_hdr.udp.src_port = exp_hdr.udp.src_port
      report "Wrong beamlet udp.src_port"
      severity ERROR;
    assert in_hdr.udp.dst_port = exp_hdr.udp.dst_port
      report "Wrong beamlet udp.dst_port"
      severity ERROR;
    assert in_hdr.udp.total_length = exp_hdr.udp.total_length
      report "Wrong beamlet udp.total_length"
      severity ERROR;
    assert in_hdr.udp.checksum = exp_hdr.udp.checksum
      report "Wrong beamlet udp.checksum"
      severity ERROR;

    -- app header
    assert in_hdr.app.sdp_marker = exp_hdr.app.sdp_marker
      report "Wrong beamlet app.sdp_marker"
      severity ERROR;
    assert in_hdr.app.sdp_version_id = exp_hdr.app.sdp_version_id
      report "Wrong beamlet app.sdp_version_id"
      severity ERROR;
    assert in_hdr.app.sdp_observation_id = exp_hdr.app.sdp_observation_id
      report "Wrong beamlet app.sdp_observation_id"
      severity ERROR;
    assert in_hdr.app.sdp_station_info = exp_hdr.app.sdp_station_info
      report "Wrong beamlet app.sdp_station_info"
      severity ERROR;

    assert in_hdr.app.sdp_source_info_reserved = exp_hdr.app.sdp_source_info_reserved
      report "Wrong beamlet app.sdp_source_info_reserved"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_antenna_band_id = exp_hdr.app.sdp_source_info_antenna_band_id
      report "Wrong beamlet app.sdp_source_info_antenna_band_id"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_nyquist_zone_id = exp_hdr.app.sdp_source_info_nyquist_zone_id
      report "Wrong beamlet app.sdp_source_info_nyquist_zone_id"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_f_adc = exp_hdr.app.sdp_source_info_f_adc
      report "Wrong beamlet app.sdp_source_info_f_adc"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_fsub_type = exp_hdr.app.sdp_source_info_fsub_type
      report "Wrong beamlet app.sdp_source_info_fsub_type"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_payload_error = exp_hdr.app.sdp_source_info_payload_error
      report "Wrong beamlet app.sdp_source_info_payload_error"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_beam_repositioning_flag = exp_hdr.app.sdp_source_info_beam_repositioning_flag
      report "Wrong beamlet app.sdp_source_info_beam_repositioning_flag"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_beamlet_width = exp_hdr.app.sdp_source_info_beamlet_width
      report "Wrong beamlet app.sdp_source_info_beamlet_width"
      severity ERROR;
    assert in_hdr.app.sdp_source_info_gn_id = exp_hdr.app.sdp_source_info_gn_id
      report "Wrong beamlet app.sdp_source_info_gn_id"
      severity ERROR;

    assert in_hdr.app.sdp_reserved = exp_hdr.app.sdp_reserved
      report "Wrong beamlet app.sdp_reserved"
      severity ERROR;
    assert in_hdr.app.sdp_beamlet_scale = exp_hdr.app.sdp_beamlet_scale
      report "Wrong beamlet app.sdp_beamlet_scale"
      severity ERROR;
    v_beamlet_index := TO_UINT(in_hdr.app.sdp_beamlet_index);
    if beamlet_index_mod then
      -- Treat beamlet_index modulo c_sdp_S_sub_bf, because the beamlet packets from different beamsets may arrive
      -- in arbitrary order
      v_beamlet_index := v_beamlet_index mod c_sdp_S_sub_bf;
    end if;
    assert v_beamlet_index = TO_UINT(exp_hdr.app.sdp_beamlet_index)
      report "Wrong beamlet app.sdp_beamlet_index"
      severity ERROR;
    assert in_hdr.app.sdp_nof_blocks_per_packet = exp_hdr.app.sdp_nof_blocks_per_packet
      report "Wrong beamlet app.sdp_nof_blocks_per_packet"
      severity ERROR;
    assert in_hdr.app.sdp_nof_beamlets_per_block = exp_hdr.app.sdp_nof_beamlets_per_block
      report "Wrong beamlet app.sdp_nof_beamlets_per_block"
      severity ERROR;
    assert in_hdr.app.sdp_block_period = exp_hdr.app.sdp_block_period
      report "Wrong beamlet app.sdp_block_period"
      severity ERROR;

    assert in_hdr.app.dp_bsn = exp_hdr.app.dp_bsn
      report "Wrong beamlet app.dp_bsn"
      severity ERROR;
    return true;
  end func_sdp_verify_cep_header;

  function func_sdp_verify_cep_header(in_hdr  : t_sdp_cep_header;
                                      exp_hdr : t_sdp_cep_header) return boolean is
  begin
    return func_sdp_verify_cep_header(in_hdr, exp_hdr, false);
  end func_sdp_verify_cep_header;

  function func_sdp_subband_equalizer(sp_subband_ampl, sp_subband_phase, sp_esub_gain, sp_esub_phase,
                                      cross_subband_ampl, cross_subband_phase, cross_esub_gain, cross_esub_phase : real)
                                      return t_real_arr is  -- 0:3 = ampl, phase, re, im
    variable v_sp_ampl, v_sp_phase, v_sp_re, v_sp_im : real;
    variable v_cross_ampl, v_cross_phase, v_cross_re, v_cross_im : real;
    variable v_sum_ampl, v_sum_phase, v_sum_re, v_sum_im : real;
    variable v_tuple : t_real_arr(0 to 3);
  begin
    v_sp_ampl   := sp_subband_ampl * sp_esub_gain;
    v_sp_phase  := sp_subband_phase + sp_esub_phase;
    v_sp_re     := COMPLEX_RE(v_sp_ampl, v_sp_phase);
    v_sp_im     := COMPLEX_IM(v_sp_ampl, v_sp_phase);
    v_cross_ampl  := cross_subband_ampl * cross_esub_gain;
    v_cross_phase := cross_subband_phase + cross_esub_phase;
    v_cross_re    := COMPLEX_RE(v_cross_ampl, v_cross_phase);
    v_cross_im    := COMPLEX_IM(v_cross_ampl, v_cross_phase);
    v_sum_re    := v_sp_re + v_cross_re;  -- ESub sum re
    v_sum_im    := v_sp_im + v_cross_im;  -- ESub sum im
    v_sum_ampl  := COMPLEX_RADIUS(v_sum_re, v_sum_im);
    v_sum_phase := COMPLEX_PHASE(v_sum_re, v_sum_im);
    v_tuple     := (0 => v_sum_ampl, 1 => v_sum_phase, 2 => v_sum_re, 3 => v_sum_im);
    return v_tuple;
  end;

  function func_sdp_beamformer(sp_subband_ampl, sp_subband_phase, sp_bf_gain, sp_bf_phase,
                               rem_subband_ampl, rem_subband_phase, rem_bf_gain, rem_bf_phase : real;
                               nof_rem : natural)
                               return t_real_arr is  -- 0:3 = ampl, phase, re, im
    variable v_nof_rem : real := real(nof_rem);  -- BF for one sp and nof_rem remnant signal inputs
    variable v_sp_ampl, v_sp_phase, v_sp_re, v_sp_im     : real;
    variable v_rem_ampl, v_rem_phase, v_rem_re, v_rem_im : real;
    variable v_sum_ampl, v_sum_phase, v_sum_re, v_sum_im : real;
    variable v_tuple                                     : t_real_arr(0 to 3);
  begin
    v_sp_ampl   := sp_subband_ampl * sp_bf_gain;
    v_sp_phase  := sp_subband_phase + sp_bf_phase;
    v_sp_re     := COMPLEX_RE(v_sp_ampl, v_sp_phase);
    v_sp_im     := COMPLEX_IM(v_sp_ampl, v_sp_phase);
    v_rem_ampl  := rem_subband_ampl * rem_bf_gain;
    v_rem_phase := rem_subband_phase + rem_bf_phase;
    v_rem_re    := COMPLEX_RE(v_rem_ampl, v_rem_phase);
    v_rem_im    := COMPLEX_IM(v_rem_ampl, v_rem_phase);
    v_sum_re    := v_sp_re + v_nof_rem * v_rem_re;  -- BF sum re
    v_sum_im    := v_sp_im + v_nof_rem * v_rem_im;  -- BF sum im
    v_sum_ampl  := COMPLEX_RADIUS(v_sum_re, v_sum_im);
    v_sum_phase := COMPLEX_PHASE(v_sum_re, v_sum_im);
    v_tuple     := (0 => v_sum_ampl, 1 => v_sum_phase, 2 => v_sum_re, 3 => v_sum_im);
    return v_tuple;
  end;

  -- BDO transpose:
  -- . See sdp/src/python/test_func_sdp_transpose_packet.py to verify that
  --   v_out = func_sdp_transpose_beamlet_packet(v_in) yields the expected v_out.
  -- . See data repacking section in:
  --   https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L4+SDPFW+Decision%3A+Multiple+beamlet+output+destinations
  -- . Use separate t_sdp_beamlet_packet_list for re and im. The packet_list
  --   contains nof_blocks_per_packet * nof_beamlets_per_block * c_sdp_N_pol_bf
  --   = 4 * 488 * 2 = 3904 beamlet part octet values.
  -- input packet_list:
  -- . blk               0,            1,            2,            3,  for nof_blocks_per_packet = 4
  -- . blet   0,   ... 487, 0,   ... 487, 0,   ... 487, 0,   ... 487,  for nof_beamlets_per_block = 488
  -- . pol_bf X,Y, ... X,Y, X,Y, ... X,Y, X,Y, ... X,Y, X,Y, ... X,Y,  for N_pol_bf = 2, X,Y = 0,1
  -- . v_in   0,   ... 975, 976, ...1951,1952, ...2927,2928, ...3903,  input list index
  -- return v_list = transposed packet_list:
  -- . pol_bf              X,Y,X,Y,X,Y,X,Y, ..., X,Y,X,Y,X,Y,X,Y,  for N_pol_bf = 2, X,Y = 0,1
  -- . blk                   0,  1,  2,  3, ...,   0,  1,  2,  3,  for nof_blocks_per_packet = 4
  -- . blet                              0, ...,             487,  for nof_beamlets_per_block = 488
  -- . v_out 0,1,  976, 977, 1952,1953, 2928,2929,
  --         2,3,  978, 979, 1954,1955, 2930,2931,
  --         ...,       ...,       ...,       ...,
  --     972,973, 1948,1949, 2924,2925, 3900,3901,
  --     974,775, 1950,1951, 2926,2927, 3902,3903, output list index
  function func_sdp_transpose_beamlet_packet(packet_list : t_sdp_beamlet_packet_list)
      return t_sdp_beamlet_packet_list is
    variable v_list : t_sdp_beamlet_packet_list;
  begin
    v_list := func_reorder_transpose_packet(c_sdp_cep_nof_blocks_per_packet,
                                            c_sdp_cep_nof_beamlets_per_block,
                                            c_sdp_N_pol_bf,
                                            packet_list);
    return v_list;
  end func_sdp_transpose_beamlet_packet;

  -- Reverse argument order nof_beamlets_per_block and nof_blocks_per_packet to undo transpose
  function func_sdp_undo_transpose_beamlet_packet(packet_list : t_sdp_beamlet_packet_list)
      return t_sdp_beamlet_packet_list is
    variable v_list : t_sdp_beamlet_packet_list;
  begin
    v_list := func_reorder_transpose_packet(c_sdp_cep_nof_beamlets_per_block,
                                            c_sdp_cep_nof_blocks_per_packet,
                                            c_sdp_N_pol_bf,
                                            packet_list);
    return v_list;
  end func_sdp_undo_transpose_beamlet_packet;

  -----------------------------------------------------------------------------
  -- CEP Read Rx 10GbE Stream
  -----------------------------------------------------------------------------
  -- Show received beamlets from 10GbE stream in Wave Window
  -- . The packet header is 9.25 longwords wide. The dp_offload_rx has stripped
  --   the header and has realigned the payload at a longword boundary.
  -- . Expect c_sdp_cep_nof_beamlets_per_block = c_sdp_S_sub_bf = 488 dual pol
  --   and complex beamlets per packet, so 2 dual pol beamlets/64b data word.
  -- . Beamlets array is stored big endian in the data, so X.real index 0 first
  --   in MSByte of rx_beamlet_sosi.data.
  procedure proc_sdp_rx_beamlet_octets(
      constant c_nof_blocks_per_packet  : in natural;
      signal   clk                      : in std_logic;
      signal   rx_beamlet_sosi          : in t_dp_sosi;
      signal   rx_beamlet_cnt           : inout natural;
      signal   rx_beamlet_valid         : out std_logic;
      signal   rx_beamlet_arr_re        : out t_sdp_beamlet_part_arr;  -- [0:3]
      signal   rx_beamlet_arr_im        : out t_sdp_beamlet_part_arr;  -- [0:3]
      signal   rx_packet_list_re        : out t_slv_8_arr;  -- [0:c_list_len - 1]
      signal   rx_packet_list_im        : out t_slv_8_arr) is
    constant c_nof_beamlets_per_packet  : natural := c_nof_blocks_per_packet * c_sdp_cep_nof_beamlets_per_block;
    constant c_nof_longwords_per_packet : natural := c_nof_beamlets_per_packet / c_sdp_nof_beamlets_per_longword;
    constant c_list_len                 : natural := c_nof_beamlets_per_packet * c_sdp_N_pol_bf;
  begin
    rx_beamlet_cnt <= 0;
    rx_beamlet_valid <= '0';
    -- Wait until start of a beamlet packet
    proc_common_wait_until_high(clk, rx_beamlet_sosi.sop);
    -- c_sdp_nof_beamlets_per_longword = 2 dual pol beamlets (= XY, XY) per 64b data word
    for I in 0 to c_nof_longwords_per_packet - 1 loop
      proc_common_wait_until_high(clk, rx_beamlet_sosi.valid);
      rx_beamlet_valid <= '1';
      -- Capture rx beamlets per longword in rx_beamlet_arr, for time series view in Wave window
      rx_beamlet_arr_re(0) <= rx_beamlet_sosi.data(63 downto 56);  -- X
      rx_beamlet_arr_im(0) <= rx_beamlet_sosi.data(55 downto 48);
      rx_beamlet_arr_re(1) <= rx_beamlet_sosi.data(47 downto 40);  -- Y
      rx_beamlet_arr_im(1) <= rx_beamlet_sosi.data(39 downto 32);
      rx_beamlet_arr_re(2) <= rx_beamlet_sosi.data(31 downto 24);  -- X
      rx_beamlet_arr_im(2) <= rx_beamlet_sosi.data(23 downto 16);
      rx_beamlet_arr_re(3) <= rx_beamlet_sosi.data(15 downto 8);  -- Y
      rx_beamlet_arr_im(3) <= rx_beamlet_sosi.data( 7 downto 0);
      -- Capture the beamlets block of each packet in rx_packet_list
      rx_packet_list_re(I * 4 + 0) <= rx_beamlet_sosi.data(63 downto 56);  -- X
      rx_packet_list_im(I * 4 + 0) <= rx_beamlet_sosi.data(55 downto 48);
      rx_packet_list_re(I * 4 + 1) <= rx_beamlet_sosi.data(47 downto 40);  -- Y
      rx_packet_list_im(I * 4 + 1) <= rx_beamlet_sosi.data(39 downto 32);
      rx_packet_list_re(I * 4 + 2) <= rx_beamlet_sosi.data(31 downto 24);  -- X
      rx_packet_list_im(I * 4 + 2) <= rx_beamlet_sosi.data(23 downto 16);
      rx_packet_list_re(I * 4 + 3) <= rx_beamlet_sosi.data(15 downto 8);  -- Y
      rx_packet_list_im(I * 4 + 3) <= rx_beamlet_sosi.data( 7 downto 0);
      proc_common_wait_until_high(clk, rx_beamlet_sosi.valid);
      -- Use at least one WAIT instead of proc_common_wait_some_cycles() to
      -- avoid Modelsim warning: (vcom-1090) Possible infinite loop: Process
      -- contains no WAIT statement.
      wait until rising_edge(clk);
      rx_beamlet_valid <= '0';
      rx_beamlet_cnt   <= (rx_beamlet_cnt + c_sdp_nof_beamlets_per_longword) mod c_sdp_cep_nof_beamlets_per_block;
    end loop;
  end proc_sdp_rx_beamlet_octets;

  procedure proc_sdp_rx_beamlet_octets(
      signal clk               : in std_logic;
      signal rx_beamlet_sosi   : in t_dp_sosi;
      signal rx_beamlet_cnt    : inout natural;
      signal rx_beamlet_valid  : out std_logic;
      signal rx_beamlet_arr_re : out t_sdp_beamlet_part_arr;
      signal rx_beamlet_arr_im : out t_sdp_beamlet_part_arr;
      signal rx_packet_list_re : out t_sdp_beamlet_packet_list;
      signal rx_packet_list_im : out t_sdp_beamlet_packet_list) is
  begin
    proc_sdp_rx_beamlet_octets(
      c_sdp_cep_nof_blocks_per_packet,  -- 4 blocks/packet
      clk,
      rx_beamlet_sosi,
      rx_beamlet_cnt,
      rx_beamlet_valid,
      rx_beamlet_arr_re,
      rx_beamlet_arr_im,
      rx_packet_list_re,
      rx_packet_list_im);
  end proc_sdp_rx_beamlet_octets;
end tb_sdp_pkg;
