-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Test bench for mm_master_mux.vhd and also mm_bus
-- Description:
--   The test bench uses mm_master_mux to access a RAM via an array of
--   masters. The array of masters is modelled using a stimuli from a single
--   master that get demultiplexed to the array of masters using
--   mm_bus. The address space of the RAM is defined by the g_base_arr
--   and g_width_arr that define the mm_bus. Therefore this test bench
--   implicitely also verifies mm_bus.vhd.
--
--               stimuli            master              mux
--               mosi               mosi_arr            mosi
--                         common -------/----> common
--   p_stimuli ----------> mem    ------/-----> mem    --------> RAM
--                         bus    -----/------> master
--                                    /         mux
--                                g_nof_masters
-- Remark:
--   In an application it is typical to use mm_master_mux to connect
--   mulitple masters to multiple slabes via a mm_bus MM bus.
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

entity tb_mm_master_mux is
 generic (
    g_nof_masters             : positive := 2;  -- Number of master memory interfaces on the MM bus array.
    g_base_arr                : t_nat_natural_arr := (0, 256);  -- Address base per slave port of mm_bus
    g_width_arr               : t_nat_natural_arr := (4,   8);  -- Address width per slave port of mm_bus
    g_waitrequest             : boolean := true;  -- When TRUE model waitrequest by the MM RAM slave, else fixed '0'
    g_pipeline_bus_mosi       : boolean := false;
    g_pipeline_bus_miso_rdval : boolean := false;
    g_pipeline_bus_miso_wait  : boolean := false
  );
end tb_mm_master_mux;

-- Usage:
--   > as 10
--   > run -all

architecture tb of tb_mm_master_mux is
  constant mm_clk_period   : time    := 10 ns;

  constant c_repeat                  : natural := sel_a_b(g_waitrequest, 10, 2);  -- repeat 2 for deterministic, more often for random
  constant c_bus_pipeline_mosi       : natural := sel_a_b(g_pipeline_bus_mosi, 1, 0);
  constant c_bus_pipeline_miso_rdval : natural := sel_a_b(g_pipeline_bus_miso_rdval, 1, 0);
  constant c_bus_pipeline_miso_wait  : natural := sel_a_b(g_pipeline_bus_miso_wait, 1, 0);
  constant c_ram_rd_latency          : natural := 1;
  constant c_ram_rd_latency_arr      : t_nat_natural_arr := array_init(c_ram_rd_latency, g_nof_masters);
  constant c_slave_enable_arr        : t_nat_boolean_arr := array_init(true, g_nof_masters);
  constant c_waitrequest_arr         : t_nat_boolean_arr := array_init(g_waitrequest, g_nof_masters);

  constant c_read_latency    : natural := c_bus_pipeline_mosi + c_ram_rd_latency + c_bus_pipeline_miso_rdval;

  constant c_addr_w          : natural := largest(ceil_log2(largest(g_base_arr)), largest(g_width_arr)) + 1;
  constant c_data_w          : natural := 32;
  constant c_test_ram        : t_c_mem := (latency  => c_ram_rd_latency,
                                           adr_w    => c_addr_w,
                                           dat_w    => c_data_w,
                                           nof_dat  => 2**c_addr_w,
                                           init_sl  => '0');
  signal mm_rst           : std_logic;
  signal mm_clk           : std_logic := '1';
  signal tb_end           : std_logic;

  signal stimuli_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal stimuli_miso     : t_mem_miso := c_mem_miso_rst;
  signal master_mosi_arr  : t_mem_mosi_arr(0 to g_nof_masters - 1) := (others => c_mem_mosi_rst);
  signal master_miso_arr  : t_mem_miso_arr(0 to g_nof_masters - 1) := (others => c_mem_miso_rst);
  signal mux_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal mux_miso         : t_mem_miso := c_mem_miso_rst;
  signal ram_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal ram_miso         : t_mem_miso := c_mem_miso_rst;
begin
  mm_clk <= not mm_clk or tb_end after mm_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 5;

  p_stimuli : process
    variable v_base    : natural;
    variable v_span    : natural;
    variable v_wrdata  : integer;  -- write data
    variable v_rddata  : integer;  -- read data
    variable v_expdata : integer;  -- expected data
  begin
    tb_end <= '0';
    stimuli_mosi <= c_mem_mosi_rst;

    -- Wait until reset is released
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Repeat twice to have wr all, rd all, wr all, rd all
    v_wrdata := 0;
    v_expdata := 0;
    for vR in 0 to c_repeat - 1 loop
      -- Write the whole memory range
      for vI in 0 to g_nof_masters - 1 loop
        v_base := g_base_arr(vI);
        v_span := 2**g_width_arr(vI);
        for vJ in 0 to v_span - 1 loop
          proc_mem_mm_bus_wr(v_base + vJ, v_wrdata, mm_clk, stimuli_miso, stimuli_mosi);
          v_wrdata := v_wrdata + 1;
        end loop;
      end loop;

      -- Read back the whole range and check if data is as expected
      for vI in 0 to g_nof_masters - 1 loop
        v_base := g_base_arr(vI);
        v_span := 2**g_width_arr(vI);
        for vJ in 0 to v_span - 1 loop
          proc_mem_mm_bus_rd(v_base + vJ, mm_clk, stimuli_miso, stimuli_mosi);
          proc_common_wait_some_cycles(mm_clk, c_read_latency);
          v_rddata := TO_UINT(stimuli_miso.rddata(c_data_w - 1 downto 0));
          if v_rddata /= v_expdata then
            report "Error! Readvalue is not as expected"
              severity ERROR;
          end if;
          v_expdata := v_expdata + 1;
        end loop;
      end loop;
    end loop;

    proc_common_wait_some_cycles(mm_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- Model multiple masters using stimuli from a single master
  u_masters : entity work.mm_bus
  generic map (
    g_nof_slaves          => g_nof_masters,
    g_base_arr            => g_base_arr,
    g_width_arr           => g_width_arr,
    g_rd_latency_arr      => c_ram_rd_latency_arr,
    g_slave_enable_arr    => c_slave_enable_arr,
    g_waitrequest_arr     => c_waitrequest_arr,
    g_pipeline_mosi       => g_pipeline_bus_mosi,
    g_pipeline_miso_rdval => g_pipeline_bus_miso_rdval,
    g_pipeline_miso_wait  => g_pipeline_bus_miso_wait
  )
  port map (
    mm_clk         => mm_clk,
    master_mosi    => stimuli_mosi,
    master_miso    => stimuli_miso,
    slave_mosi_arr => master_mosi_arr,
    slave_miso_arr => master_miso_arr
  );

  -- DUT = device under test
  u_dut: entity work.mm_master_mux
  generic map (
    g_nof_masters     => g_nof_masters,
    g_rd_latency_min  => c_read_latency
  )
  port map (
    mm_clk          => mm_clk,
    master_mosi_arr => master_mosi_arr,
    master_miso_arr => master_miso_arr,
    mux_mosi        => mux_mosi,
    mux_miso        => mux_miso
  );

  -- Model master access to MM bus with multiple slaves using a single RAM
  u_waitrequest_model : entity work.mm_waitrequest_model
  generic map (
    g_waitrequest => g_waitrequest
  )
  port map (
    mm_clk     => mm_clk,
    bus_mosi   => mux_mosi,
    bus_miso   => mux_miso,
    slave_mosi => ram_mosi,
    slave_miso => ram_miso
  );

  u_ram : entity common_lib.common_ram_r_w
  generic map (
    g_ram       => c_test_ram,
    g_init_file => "UNUSED"
  )
  port map (
    rst       => mm_rst,
    clk       => mm_clk,
    wr_en     => ram_mosi.wr,
    wr_adr    => ram_mosi.address(c_addr_w - 1 downto 0),
    wr_dat    => ram_mosi.wrdata(c_data_w - 1 downto 0),
    rd_en     => ram_mosi.rd,
    rd_adr    => ram_mosi.address(c_addr_w - 1 downto 0),
    rd_dat    => ram_miso.rddata(c_data_w - 1 downto 0),
    rd_val    => ram_miso.rdval
  );
end tb;
