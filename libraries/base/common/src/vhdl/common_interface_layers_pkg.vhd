-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use IEEE.numeric_std.all;

package common_interface_layers_pkg is
  ------------------------------------------------------------------------------
  -- XAUI
  ------------------------------------------------------------------------------

  constant c_nof_xaui_lanes    : natural := 4;

  type t_xaui_arr is array(integer range <>) of std_logic_vector(c_nof_xaui_lanes - 1 downto 0);

  ------------------------------------------------------------------------------
  -- XGMII
  ------------------------------------------------------------------------------

  constant c_xgmii_data_w      : natural := 64;
  constant c_xgmii_nof_lanes   : natural := 8;

  constant c_xgmii_ctrl_w      : natural := c_xgmii_nof_lanes;
  constant c_xgmii_lane_data_w : natural := c_xgmii_data_w / c_xgmii_nof_lanes;
  constant c_xgmii_w           : natural := c_xgmii_data_w + c_xgmii_ctrl_w;

  constant c_xgmii_d_idle      : std_logic_vector(c_xgmii_data_w - 1 downto 0) := x"0707070707070707";
  constant c_xgmii_d_start     : std_logic_vector(c_xgmii_data_w - 1 downto 0) := x"00000000000000FB";
  constant c_xgmii_d_term      : std_logic_vector(c_xgmii_data_w - 1 downto 0) := x"07070707FD000000";

  constant c_xgmii_c_init      : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0) := x"11";  -- During (re)initialization
  constant c_xgmii_c_idle      : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0) := x"FF";
  constant c_xgmii_c_data      : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0) := x"00";
  constant c_xgmii_c_start     : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0) := x"01";  -- b'00000001' as byte 0 contains START word FB
  constant c_xgmii_c_term      : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0) := x"F8";  -- b'11111000' as byte 3 contains TERMINATE word FD, bytes 7..4 are IDLE.

  function func_xgmii_dc( data : in std_logic_vector(c_xgmii_data_w - 1 downto 0); ctrl : in std_logic_vector(c_xgmii_nof_lanes - 1 downto 0)) return std_logic_vector;
  function func_xgmii_d( data_ctrl: in std_logic_vector(c_xgmii_w - 1 downto 0)) return std_logic_vector;
  function func_xgmii_c( data_ctrl: in std_logic_vector(c_xgmii_w - 1 downto 0)) return std_logic_vector;

  type t_xgmii_dc_arr is array(integer range <>) of std_logic_vector(c_xgmii_w - 1 downto 0);
  type t_xgmii_d_arr is array(integer range <>) of std_logic_vector(c_xgmii_data_w - 1 downto 0);
  type t_xgmii_c_arr is array(integer range <>) of std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);
 end common_interface_layers_pkg;

package body common_interface_layers_pkg is
  -- Refer to the 10GBASE-R PHY IP Core section of the Altera Transceiver PHY IP Core User Guide
  -- (November 2011) page 3-11: SDR XGMII Tx Interface for the proper mapping.

  -- Combine separate data and control bits into one XGMII SLV.
  function func_xgmii_dc( data : in std_logic_vector(c_xgmii_data_w - 1 downto 0); ctrl : in std_logic_vector(c_xgmii_nof_lanes - 1 downto 0)) return std_logic_vector is
    variable data_ctrl_out : std_logic_vector(c_xgmii_w - 1 downto 0);
  begin
    -- Lane 0:
    data_ctrl_out( 7 downto  0) := data( 7 downto 0);
    data_ctrl_out( 8)           := ctrl(0);
    -- Lane 1:
    data_ctrl_out(16 downto  9) := data(15 downto 8);
    data_ctrl_out(17)           := ctrl(1);
    -- Lane 2:
    data_ctrl_out(25 downto 18) := data(23 downto 16);
    data_ctrl_out(26)           := ctrl(2);
    -- Lane 3:
    data_ctrl_out(34 downto 27) := data(31 downto 24);
    data_ctrl_out(35)           := ctrl(3);
    -- Lane 4:
    data_ctrl_out(43 downto 36) := data(39 downto 32);
    data_ctrl_out(44)           := ctrl(4);
    -- Lane 5:
    data_ctrl_out(52 downto 45) := data(47 downto 40);
    data_ctrl_out(53)           := ctrl(5);
    -- Lane 6:
    data_ctrl_out(61 downto 54) := data(55 downto 48);
    data_ctrl_out(62)           := ctrl(6);
    -- Lane 7:
    data_ctrl_out(70 downto 63) := data(63 downto 56);
    data_ctrl_out(71)           := ctrl(7);

    return data_ctrl_out;
  end;

  -- Extract the data bits from combined data+ctrl XGMII SLV.
  function func_xgmii_d( data_ctrl: in std_logic_vector(c_xgmii_w - 1 downto 0)) return std_logic_vector is
    variable data_out : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  begin
    -- Lane 0:
    data_out( 7 downto 0)  := data_ctrl( 7 downto  0);
    -- Lane 1:
    data_out(15 downto 8)  := data_ctrl(16 downto  9);
    -- Lane 2:
    data_out(23 downto 16) := data_ctrl(25 downto 18);
    -- Lane 3:
    data_out(31 downto 24) := data_ctrl(34 downto 27);
    -- Lane 4:
    data_out(39 downto 32) := data_ctrl(43 downto 36);
    -- Lane 5:
    data_out(47 downto 40) := data_ctrl(52 downto 45);
    -- Lane 6:
    data_out(55 downto 48) := data_ctrl(61 downto 54);
    -- Lane 7:
    data_out(63 downto 56) := data_ctrl(70 downto 63);

    return data_out;
  end;

  -- Extract the control bits from combined data+ctrl XGMII SLV.
  function func_xgmii_c( data_ctrl: in std_logic_vector(c_xgmii_w - 1 downto 0)) return std_logic_vector is
    variable ctrl_out : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);
  begin
    -- Lane 0:
    ctrl_out(0) := data_ctrl( 8);
    -- Lane 1:
    ctrl_out(1) := data_ctrl(17);
    -- Lane 2:
    ctrl_out(2) := data_ctrl(26);
    -- Lane 3:
    ctrl_out(3) := data_ctrl(35);
    -- Lane 4:
    ctrl_out(4) := data_ctrl(44);
    -- Lane 5:
    ctrl_out(5) := data_ctrl(53);
    -- Lane 6:
    ctrl_out(6) := data_ctrl(62);
    -- Lane 7:
    ctrl_out(7) := data_ctrl(71);

    return ctrl_out;
  end;
end common_interface_layers_pkg;
