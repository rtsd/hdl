-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose: Controller that store blocks of nof_ch_in (complex) input data
--          words in a dual page data buffer
-- Description:
--   Write databuf control for nof_ch_in (complex) input data words and pulse
--   store_done for each nof_ch_in data words that have been written.
--   Maximum size for the dual page buffer is g_nof_ch_in. Default nof_ch_in
--   = g_nof_ch_in, but it can be used with smaller reorder block sizes.
-- Remarks:
-- . The reorder_store stores the complex input_sosi.re,im as concatenated
--   data = im & re with data width c_data_w, when g_use_complex = true. When
--   g_use_complex = false it stores c_data_w width of the input_sosi.data.
-- . The reorder_col does not use input_sosi.sop and eop, because it uses a
--   ch_cnt. Hence the input_sosi only needs to carry data, im, re and valid,
--   the input_sosi.sop and eop are ignored. The ch_cnt is needed anyway to
--   set the store_mosi.address. The nof_ch_in defines the number of valid
--   per input block, so from input_sosi.sop to input_sosi.eop. The
--   reorder_store assumes that the first input_sosi.valid corresponds to a
--   input_sosi.sop. The ch_cnt restarts at the and of a block, so when
--   ch_cnt = nof_ch_in-1.
-- . The store_done signal occurs when the last data of the input_sosi block
--   is being written, so 1 cycle after the input_sosi.eop.
-- . The store_busy goes high at in_sosi.sop and goes low after store_done.

entity reorder_store is
  generic (
    g_dsp_data_w  : natural;  -- complex data width, = c_data_w / 2
    g_nof_ch_in   : natural;  -- = nof valid per input block (sop to eop)
    g_use_complex : boolean := true  -- use input_sosi.re, im when true, else input_sosi.data
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- Dynamic reorder block size control
    nof_ch_in     : in natural range 0 to g_nof_ch_in := g_nof_ch_in;
    -- Streaming
    input_sosi    : in  t_dp_sosi;
    -- Timing
    store_busy    : out std_logic;
    store_done    : out std_logic;
    -- Write databuf control
    store_mosi    : out t_mem_mosi
  );
end reorder_store;

architecture rtl of reorder_store is
  constant c_data_w       : natural := c_nof_complex * g_dsp_data_w;

  signal ch_cnt           : integer range 0 to g_nof_ch_in - 1;
  signal nxt_ch_cnt       : integer;

  signal input_im_re      : std_logic_vector(c_data_w - 1 downto 0);
  signal input_data       : std_logic_vector(c_data_w - 1 downto 0);

  signal i_store_mosi     : t_mem_mosi;
  signal nxt_store_mosi   : t_mem_mosi := c_mem_mosi_rst;

  signal store_busy_reg   : std_logic;
  signal nxt_store_busy   : std_logic;
  signal nxt_store_done   : std_logic;
begin
  store_mosi <= i_store_mosi;

  p_reg : process (clk, rst)
  begin
    if rst = '1' then
      -- Internal registers.
      ch_cnt          <= 0;
      -- Output registers.
      i_store_mosi    <= c_mem_mosi_rst;
      store_busy_reg  <= '0';
      store_done      <= '0';
    elsif rising_edge(clk) then
      -- Internal registers.
      ch_cnt          <= nxt_ch_cnt;
      -- Output registers.
      i_store_mosi    <= nxt_store_mosi;
      store_busy_reg  <= nxt_store_busy;
      store_done      <= nxt_store_done;
    end if;
  end process;

  store_busy <= nxt_store_busy or store_busy_reg;

  nxt_store_busy <= '0' when ch_cnt = 0 and input_sosi.valid = '0' else '1';

  p_ch_cnt : process (ch_cnt, input_sosi, nof_ch_in)
  begin
    nxt_store_done <= '0';
    nxt_ch_cnt <= ch_cnt;

    if input_sosi.valid = '1' then
      if ch_cnt = nof_ch_in - 1 then
        nxt_store_done <= '1';
        nxt_ch_cnt <= 0;
      else
        nxt_ch_cnt <= ch_cnt + 1;
      end if;
    end if;
  end process;

  -- store
  nxt_store_mosi.wr      <= input_sosi.valid;
  nxt_store_mosi.address <= TO_MEM_ADDRESS(ch_cnt) when input_sosi.valid = '1' else i_store_mosi.address;

  -- Use intermediate signals to avoid too long code lines
  input_im_re <= input_sosi.im(g_dsp_data_w - 1 downto 0) & input_sosi.re(g_dsp_data_w - 1 downto 0);
  input_data <= input_sosi.data(c_data_w - 1 downto 0);

  -- Use complex data fields
  use_complex : if g_use_complex generate
    nxt_store_mosi.wrdata <= RESIZE_MEM_DATA(input_im_re) when input_sosi.valid = '1' else i_store_mosi.wrdata;
  end generate;

  -- Use regular data field
  use_data : if not g_use_complex generate
    nxt_store_mosi.wrdata <= RESIZE_MEM_DATA(input_data) when input_sosi.valid = '1' else i_store_mosi.wrdata;
  end generate;
end rtl;
