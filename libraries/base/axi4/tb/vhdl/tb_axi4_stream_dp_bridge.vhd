-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . TB for axi4_stream_dp_bridge.
-- Description:
-- The testbench generates a stimuli DP stream which streams into the DUT. The
-- resulting AXI4 stream is looped back into the DUT that results in the final
-- DP stream. The resulting DP stream is verified.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.axi4_stream_pkg.all;

entity tb_axi4_stream_dp_bridge is
  generic (
    g_dp_rl   : natural := 1;
    g_axi4_rl : natural := 0
  );
end tb_axi4_stream_dp_bridge;

architecture tb of tb_axi4_stream_dp_bridge is
  -- TX ready latency to DUT chain
  constant c_tx_void        : natural := sel_a_b(g_dp_rl, 1, 0);  -- used to avoid empty range VHDL warnings when g_dp_rl=0

  constant c_tx_offset_sop  : natural := 0;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 0,  7,  14, ...
  constant c_tx_offset_eop  : natural := 6;  -- eop in data valid cycle   6,  13,  20, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_tx_offset_sync : natural := 3;  -- sync in data valid cycle 3, 20, 37, ...
  constant c_tx_period_sync : natural := 17;

  constant c_verify_en_wait : natural := 10;  -- wait some cycles before asserting verify enable

  constant c_empty_offset   : natural := 1;
  constant c_channel_offset : natural := 2;

  constant c_random_w       : natural := 19;

  constant c_max_empty_w    : natural := ceil_log2(c_axi4_stream_keep_w);
  constant c_max_channel_w  : natural := c_axi4_stream_tid_w;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal cnt_val        : std_logic;
  signal cnt_en         : std_logic;

  signal tx_data        : t_dp_data_arr(0 to g_dp_rl + c_tx_void);
  signal tx_val         : std_logic_vector(0 to g_dp_rl + c_tx_void);

  signal in_ready       : std_logic;
  signal in_data        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal in_empty       : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal in_channel     : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal in_sync        : std_logic;
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;
  signal valid_data     : std_logic := '1';

  -- DUT index c_nof_dut-1 = out_data
  signal dut_siso       : t_dp_siso;
  signal dut_sosi       : t_dp_sosi   := c_dp_sosi_rst;
  signal dut_axi4_sosi  : t_axi4_sosi := c_axi4_sosi_rst;
  signal dut_axi4_siso  : t_axi4_siso := c_axi4_siso_rst;
  signal dut_out_siso   : t_dp_siso;
  signal dut_out_sosi   : t_dp_sosi   := c_dp_sosi_rst;

  signal out_ready      : std_logic;
  signal prev_out_ready : std_logic_vector(0 to g_dp_rl);
  signal out_data       : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_empty      : std_logic_vector(c_dp_empty_w - 1 downto 0);
  signal out_channel    : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_sync       : std_logic;
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal hold_out_sop   : std_logic;
  signal prev_out_data  : std_logic_vector(out_data'range);

  signal state          : t_dp_state_enum;
  signal verify_en      : std_logic;
  signal verify_done    : std_logic;

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := sel_a_b(g_dp_rl = g_axi4_rl, TO_UVEC(18953, c_dp_data_w), TO_UVEC(19279, c_dp_data_w));
  signal exp_empty      : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal exp_channel    : std_logic_vector(c_dp_channel_w - 1 downto 0);
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  -- Input data
  cnt_val <= in_ready and cnt_en;

  proc_dp_cnt_dat(rst, clk, cnt_val, cnt_dat);
  proc_dp_tx_data(g_dp_rl, rst, clk, cnt_val, cnt_dat, tx_data, tx_val, in_data, in_val);
  proc_dp_tx_ctrl(c_tx_offset_sync, c_tx_period_sync, in_data, in_val, in_sync);
  proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data, in_val, in_sop);
  proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data, in_val, in_eop);
  in_empty   <= RESIZE_UVEC(INCR_UVEC(in_data, c_empty_offset)(c_max_empty_w - 1 downto 0), c_dp_stream_empty_w) when in_eop = '1' else (others => '0');
  in_channel <= RESIZE_UVEC(INCR_UVEC(in_data, c_channel_offset)(c_max_channel_w - 1 downto 0), c_dp_stream_channel_w);

  -- Stimuli control
  proc_dp_count_en(rst, clk, sync, lfsr1, state, verify_done, tb_end, cnt_en);
  proc_dp_out_ready(rst, clk, sync, lfsr2, out_ready);

  -- Output verify
  proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
  proc_dp_verify_data("out_data", g_dp_rl, clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  proc_dp_verify_valid(g_dp_rl, clk, verify_en, out_ready, prev_out_ready, out_val);
  proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, out_data, out_val, out_sop);
  proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, out_data, out_val, out_eop);
  proc_dp_verify_sop_and_eop(g_dp_rl, false, clk, out_ready, out_val, out_sop, out_eop, hold_out_sop);  -- Verify that sop and eop come in pairs, no check on valid between eop and sop

  exp_empty   <= RESIZE_UVEC(INCR_UVEC(out_data, c_empty_offset)(c_max_empty_w - 1 downto 0), c_dp_empty_w) when out_eop = '1' else (others => '0');
  exp_channel <= RESIZE_UVEC(INCR_UVEC(out_data, c_channel_offset)(c_max_channel_w - 1 downto 0), c_dp_channel_w);
  proc_dp_verify_other_sosi("empty", exp_empty, clk, out_val, out_empty);
  proc_dp_verify_other_sosi("channel", exp_channel, clk, verify_en, out_channel);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_equal, clk, verify_done, exp_data, out_data);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  -- map sl, slv to record
  in_ready                              <= dut_siso.ready;  -- SISO
  dut_sosi.data(c_dp_data_w - 1 downto 0) <= in_data;  -- SOSI
  dut_sosi.empty                        <= in_empty;
  dut_sosi.channel                      <= in_channel;
  dut_sosi.sync                         <= in_sync;
  dut_sosi.valid                        <= in_val;
  dut_sosi.sop                          <= in_sop;
  dut_sosi.eop                          <= in_eop;

  dut : entity work.axi4_stream_dp_bridge
  generic map (
    g_use_empty => true,
    g_axi4_rl   => g_axi4_rl,
    g_dp_rl     => g_dp_rl
  )
  port map (
    in_rst    => rst,
    in_clk    => clk,
    -- ST sink
    dp_in_siso => dut_siso,
    dp_in_sosi => dut_sosi,
    -- ST source
    dp_out_siso => dut_out_siso,
    dp_out_sosi => dut_out_sosi,
    -- AXI4 Loopback
    axi4_in_sosi => dut_axi4_sosi,
    axi4_in_siso => dut_axi4_siso,
    axi4_out_sosi => dut_axi4_sosi,
    axi4_out_siso => dut_axi4_siso
  );

  -- map record to sl, slv
  dut_out_siso.ready <= out_ready;  -- SISO
  out_data                               <= dut_out_sosi.data(c_dp_data_w - 1 downto 0);  -- SOSI
  out_empty(c_dp_empty_w - 1 downto 0)     <= dut_out_sosi.empty;
  out_channel(c_dp_channel_w - 1 downto 0) <= dut_out_sosi.channel;
  out_sync                               <= dut_out_sosi.sync;
  out_val                                <= dut_out_sosi.valid;
  out_sop                                <= dut_out_sosi.sop;
  out_eop                                <= dut_out_sosi.eop;
end tb;
