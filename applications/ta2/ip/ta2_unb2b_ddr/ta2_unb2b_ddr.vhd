-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide DDR4 memory interface for OpenCL kernel on UniBoard2B
-- Description:
-- . This core consists of:
--   . An Intel/Altera Avalon MM clock cross IP
--   . Avalon MM pipe stage IP
--   . RadioHDL ip_arria10_e1sg_ddr4_8g_1600 IP
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   . The curret implementation only works with ddr4_8g_1600m
library IEEE, common_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use tech_ddr_lib.tech_ddr_component_pkg.all;

entity ta2_unb2b_ddr is
  generic (
    -- IO_DDR
    g_ddr_MB_I         : t_c_tech_ddr := c_tech_ddr4_8g_1600m;  -- DDR4 has no master or slave, so no need to check number of MB
    g_ddr_MB_II        : t_c_tech_ddr := c_tech_ddr4_8g_1600m;
    g_use_MB_I         : boolean := true;
    g_use_MB_II        : boolean := true
  );
  port (
    kernel_clk         : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset       : in  std_logic;

    mem0_waitrequest   : out std_logic;  -- waitrequest
    mem0_readdata      : out std_logic_vector(511 downto 0);  -- readdata
    mem0_readdatavalid : out std_logic;  -- readdatavalid
    mem0_burstcount    : in  std_logic_vector(4 downto 0)   := (others => 'X');  -- burstcount
    mem0_writedata     : in  std_logic_vector(511 downto 0)  := (others => 'X');  -- writedata
    mem0_address       : in  std_logic_vector(32 downto 0)   := (others => 'X');  -- address
    mem0_write         : in  std_logic                       := 'X';  -- write
    mem0_read          : in  std_logic                       := 'X';  -- read
    mem0_byteenable    : in  std_logic_vector(63 downto 0)   := (others => 'X');  -- byteenable
    mem0_debugaccess   : in  std_logic                       := 'X';  -- debugacce

    mem1_waitrequest   : out std_logic;  -- waitrequest
    mem1_readdata      : out std_logic_vector(511 downto 0);  -- readdata
    mem1_readdatavalid : out std_logic;  -- readdatavalid
    mem1_burstcount    : in  std_logic_vector(4 downto 0)   := (others => 'X');  -- burstcount
    mem1_writedata     : in  std_logic_vector(511 downto 0)  := (others => 'X');  -- writedata
    mem1_address       : in  std_logic_vector(32 downto 0)   := (others => 'X');  -- address
    mem1_write         : in  std_logic                       := 'X';  -- write
    mem1_read          : in  std_logic                       := 'X';  -- read
    mem1_byteenable    : in  std_logic_vector(63 downto 0)   := (others => 'X');  -- byteenable
    mem1_debugaccess   : in  std_logic                       := 'X';  -- debugacce

    mb_I_ref_clk       : in std_logic := '0';
    mb_I_ref_rst       : in std_logic := '1';

    mb_II_ref_clk      : in std_logic := '0';
    mb_II_ref_rst      : in std_logic := '1';

    -- SO-DIMM Memory Bank I
    mb_I_in            : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    mb_I_io            : inout t_tech_ddr4_phy_io;
    mb_I_ou            : out   t_tech_ddr4_phy_ou;

    -- SO-DIMM Memory Bank II
    mb_II_in           : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    mb_II_io           : inout t_tech_ddr4_phy_io;
    mb_II_ou           : out   t_tech_ddr4_phy_ou
  );
end ta2_unb2b_ddr;

architecture str of ta2_unb2b_ddr is
  constant c_gigabytes_MB_I             : natural := func_tech_ddr_module_size(g_ddr_MB_I);
  constant c_mb_I_ctlr_address_w        : natural := func_tech_ddr_ctlr_address_w(g_ddr_MB_I);
  constant c_mb_I_ctlr_data_w           : natural := 576;  -- func_tech_ddr_ctlr_data_w(g_ddr_MB_I);
  constant c_mb_I_ctlr_byteenable_w     : natural := 72;

  constant c_gigabytes_MB_II            : natural := func_tech_ddr_module_size(g_ddr_MB_II);
  constant c_mb_II_ctlr_address_w       : natural := func_tech_ddr_ctlr_address_w(g_ddr_MB_II);
  constant c_mb_II_ctlr_data_w          : natural := 576;  -- func_tech_ddr_ctlr_data_w(g_ddr_MB_II);
  constant c_mb_II_ctlr_byteenable_w    : natural := 72;

  constant c_data_w                     : natural := 512;
  constant c_symbol_w                   : natural := 8;
  constant c_addr_w                     : natural := 33;
  constant c_burstcount_w               : natural := 5;
  constant c_byteenable_w               : natural := c_data_w / c_symbol_w;

  constant c_command_fifo_depth         : natural := 32;
  constant c_response_fifo_depth        : natural := 512;
  constant c_master_sync_depth          : natural := 2;
  constant c_slave_sync_depth           : natural := 2;

  constant c_pipeline_command           : natural := 1;
  constant c_pipeline_response          : natural := 1;
  constant c_sync_reset                 : natural := 0;

  -- Memory Bank I signals
  signal mb_I_ref_rst_n                     : std_logic;
  signal mb_I_emif_usr_clk                  : std_logic;
  signal mb_I_emif_usr_reset                : std_logic;
  signal mb_I_emif_usr_reset_n              : std_logic;

  signal mb_I_pipe_stage_m0_waitrequest     : std_logic;
  signal mb_I_pipe_stage_m0_readdata        : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_I_pipe_stage_m0_readdatavalid   : std_logic;
  signal mb_I_pipe_stage_m0_burstcount      : std_logic_vector(c_burstcount_w - 1 downto 0);
  signal mb_I_pipe_stage_m0_writedata       : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_I_pipe_stage_m0_address         : std_logic_vector(c_addr_w - 1 downto 0);
  signal mb_I_pipe_stage_m0_write           : std_logic;
  signal mb_I_pipe_stage_m0_read            : std_logic;
  signal mb_I_pipe_stage_m0_byteenable      : std_logic_vector(c_byteenable_w - 1 downto 0);
  signal mb_I_pipe_stage_m0_debugaccess     : std_logic;

  signal mb_I_pipe_stage_s0_waitrequest     : std_logic;
  signal mb_I_pipe_stage_s0_readdata        : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_I_pipe_stage_s0_readdatavalid   : std_logic;
  signal mb_I_pipe_stage_s0_burstcount      : std_logic_vector(c_burstcount_w - 1 downto 0);
  signal mb_I_pipe_stage_s0_writedata       : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_I_pipe_stage_s0_address         : std_logic_vector(c_addr_w - 1 downto 0);
  signal mb_I_pipe_stage_s0_write           : std_logic;
  signal mb_I_pipe_stage_s0_read            : std_logic;
  signal mb_I_pipe_stage_s0_byteenable      : std_logic_vector(c_byteenable_w - 1 downto 0);
  signal mb_I_pipe_stage_s0_debugaccess     : std_logic;

  signal mb_I_amm_ready_0                   : std_logic;
  signal mb_I_amm_read_0                    : std_logic;
  signal mb_I_amm_write_0                   : std_logic;
  signal mb_I_amm_address_0                 : std_logic_vector(c_mb_I_ctlr_address_w - 1 downto 0) := (others => '0');
  signal mb_I_amm_readdata_0                : std_logic_vector(c_mb_I_ctlr_data_w - 1 downto 0) := (others => '0');
  signal mb_I_amm_writedata_0               : std_logic_vector(c_mb_I_ctlr_data_w - 1 downto 0) := (others => '0');
  signal mb_I_amm_burstcount_0              : std_logic_vector(g_ddr_MB_I.maxburstsize_w - 1 downto 0) := (others => '0');
  signal mb_I_amm_byteenable_0              : std_logic_vector(c_mb_I_ctlr_byteenable_w - 1 downto 0) := (others => '0');
  signal mb_I_amm_readdatavalid_0           : std_logic;

  -- Memory Bank II signals
  signal mb_II_ref_rst_n                     : std_logic;
  signal mb_II_emif_usr_clk                  : std_logic;
  signal mb_II_emif_usr_reset                : std_logic;
  signal mb_II_emif_usr_reset_n              : std_logic;

  signal mb_II_pipe_stage_m0_waitrequest     : std_logic;
  signal mb_II_pipe_stage_m0_readdata        : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_II_pipe_stage_m0_readdatavalid   : std_logic;
  signal mb_II_pipe_stage_m0_burstcount      : std_logic_vector(c_burstcount_w - 1 downto 0);
  signal mb_II_pipe_stage_m0_writedata       : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_II_pipe_stage_m0_address         : std_logic_vector(c_addr_w - 1 downto 0);
  signal mb_II_pipe_stage_m0_write           : std_logic;
  signal mb_II_pipe_stage_m0_read            : std_logic;
  signal mb_II_pipe_stage_m0_byteenable      : std_logic_vector(c_byteenable_w - 1 downto 0);
  signal mb_II_pipe_stage_m0_debugaccess     : std_logic;

  signal mb_II_pipe_stage_s0_waitrequest     : std_logic;
  signal mb_II_pipe_stage_s0_readdata        : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_II_pipe_stage_s0_readdatavalid   : std_logic;
  signal mb_II_pipe_stage_s0_burstcount      : std_logic_vector(c_burstcount_w - 1 downto 0);
  signal mb_II_pipe_stage_s0_writedata       : std_logic_vector(c_data_w - 1 downto 0);
  signal mb_II_pipe_stage_s0_address         : std_logic_vector(c_addr_w - 1 downto 0);
  signal mb_II_pipe_stage_s0_write           : std_logic;
  signal mb_II_pipe_stage_s0_read            : std_logic;
  signal mb_II_pipe_stage_s0_byteenable      : std_logic_vector(c_byteenable_w - 1 downto 0);
  signal mb_II_pipe_stage_s0_debugaccess     : std_logic;

  signal mb_II_amm_ready_0                   : std_logic;
  signal mb_II_amm_read_0                    : std_logic;
  signal mb_II_amm_write_0                   : std_logic;
  signal mb_II_amm_address_0                 : std_logic_vector(c_mb_II_ctlr_address_w - 1 downto 0) := (others => '0');
  signal mb_II_amm_readdata_0                : std_logic_vector(c_mb_II_ctlr_data_w - 1 downto 0) := (others => '0');
  signal mb_II_amm_writedata_0               : std_logic_vector(c_mb_II_ctlr_data_w - 1 downto 0) := (others => '0');
  signal mb_II_amm_burstcount_0              : std_logic_vector(g_ddr_MB_II.maxburstsize_w - 1 downto 0) := (others => '0');
  signal mb_II_amm_byteenable_0              : std_logic_vector(c_mb_II_ctlr_byteenable_w - 1 downto 0) := (others => '0');
  signal mb_II_amm_readdatavalid_0           : std_logic;

  -- MM Pipe stage component
  component ta2_unb2b_ddr_pipe_stage is
       generic (
           DATA_WIDTH        : integer := 32;
           SYMBOL_WIDTH      : integer := 8;
           HDL_ADDR_WIDTH    : integer := 10;
           BURSTCOUNT_WIDTH  : integer := 1;
           PIPELINE_COMMAND  : integer := 1;
           PIPELINE_RESPONSE : integer := 1;
           SYNC_RESET        : integer := 0
       );
       port (
           clk              : in  std_logic                                     := 'X';  -- clk
           m0_waitrequest   : in  std_logic                                     := 'X';  -- waitrequest
           m0_readdata      : in  std_logic_vector(DATA_WIDTH - 1 downto 0)       := (others => 'X');  -- readdata
           m0_readdatavalid : in  std_logic                                     := 'X';  -- readdatavalid
           m0_burstcount    : out std_logic_vector(BURSTCOUNT_WIDTH - 1 downto 0);  -- burstcount
           m0_writedata     : out std_logic_vector(DATA_WIDTH - 1 downto 0);  -- writedata
           m0_address       : out std_logic_vector(HDL_ADDR_WIDTH - 1 downto 0);  -- address
           m0_write         : out std_logic;  -- write
           m0_read          : out std_logic;  -- read
           m0_byteenable    : out std_logic_vector(63 downto 0);  -- byteenable
           m0_debugaccess   : out std_logic;  -- debugaccess
           reset            : in  std_logic                                     := 'X';  -- reset
           s0_waitrequest   : out std_logic;  -- waitrequest
           s0_readdata      : out std_logic_vector(DATA_WIDTH - 1 downto 0);  -- readdata
           s0_readdatavalid : out std_logic;  -- readdatavalid
           s0_burstcount    : in  std_logic_vector(BURSTCOUNT_WIDTH - 1 downto 0) := (others => 'X');  -- burstcount
           s0_writedata     : in  std_logic_vector(DATA_WIDTH - 1 downto 0)       := (others => 'X');  -- writedata
           s0_address       : in  std_logic_vector(HDL_ADDR_WIDTH - 1 downto 0)   := (others => 'X');  -- address
           s0_write         : in  std_logic                                     := 'X';  -- write
           s0_read          : in  std_logic                                     := 'X';  -- read
           s0_byteenable    : in  std_logic_vector(63 downto 0)                 := (others => 'X');  -- byteenable
           s0_debugaccess   : in  std_logic                                     := 'X'  -- debugaccess
       );
   end component ta2_unb2b_ddr_pipe_stage;

   -- MM clock cross component
   component ta2_unb2b_ddr_clock_cross is
       generic (
           DATA_WIDTH          : integer := 32;
           SYMBOL_WIDTH        : integer := 8;
           HDL_ADDR_WIDTH      : integer := 10;
           BURSTCOUNT_WIDTH    : integer := 1;
           COMMAND_FIFO_DEPTH  : integer := 4;
           RESPONSE_FIFO_DEPTH : integer := 4;
           MASTER_SYNC_DEPTH   : integer := 2;
           SLAVE_SYNC_DEPTH    : integer := 2
       );
       port (
           m0_waitrequest   : in  std_logic                                     := 'X';  -- waitrequest
           m0_readdata      : in  std_logic_vector(DATA_WIDTH - 1 downto 0)       := (others => 'X');  -- readdata
           m0_readdatavalid : in  std_logic                                     := 'X';  -- readdatavalid
           m0_burstcount    : out std_logic_vector(BURSTCOUNT_WIDTH - 1 downto 0);  -- burstcount
           m0_writedata     : out std_logic_vector(DATA_WIDTH - 1 downto 0);  -- writedata
           m0_address       : out std_logic_vector(HDL_ADDR_WIDTH - 1 downto 0);  -- address
           m0_write         : out std_logic;  -- write
           m0_read          : out std_logic;  -- read
           m0_byteenable    : out std_logic_vector(63 downto 0);  -- byteenable
           m0_debugaccess   : out std_logic;  -- debugaccess
           m0_clk           : in  std_logic                                     := 'X';  -- clk
           m0_reset         : in  std_logic                                     := 'X';  -- reset
           s0_waitrequest   : out std_logic;  -- waitrequest
           s0_readdata      : out std_logic_vector(DATA_WIDTH - 1 downto 0);  -- readdata
           s0_readdatavalid : out std_logic;  -- readdatavalid
           s0_burstcount    : in  std_logic_vector(BURSTCOUNT_WIDTH - 1 downto 0) := (others => 'X');  -- burstcount
           s0_writedata     : in  std_logic_vector(DATA_WIDTH - 1 downto 0)       := (others => 'X');  -- writedata
           s0_address       : in  std_logic_vector(HDL_ADDR_WIDTH - 1 downto 0)   := (others => 'X');  -- address
           s0_write         : in  std_logic                                     := 'X';  -- write
           s0_read          : in  std_logic                                     := 'X';  -- read
           s0_byteenable    : in  std_logic_vector(63 downto 0)                 := (others => 'X');  -- byteenable
           s0_debugaccess   : in  std_logic                                     := 'X';  -- debugaccess
           s0_clk           : in  std_logic                                     := 'X';  -- clk
           s0_reset         : in  std_logic                                     := 'X'  -- reset
       );
   end component ta2_unb2b_ddr_clock_cross;
begin
  gen_MB_I : if g_use_MB_I generate
    u_mb_I_clock_cross : ta2_unb2b_ddr_clock_cross
        generic map (
            DATA_WIDTH          => c_data_w,
            SYMBOL_WIDTH        => c_symbol_w,
            HDL_ADDR_WIDTH      => c_addr_w,
            BURSTCOUNT_WIDTH    => c_burstcount_w,
            COMMAND_FIFO_DEPTH  => c_command_fifo_depth,
            RESPONSE_FIFO_DEPTH => c_response_fifo_depth,
            MASTER_SYNC_DEPTH   => c_master_sync_depth,
            SLAVE_SYNC_DEPTH    => c_slave_sync_depth
        )
        port map (
            m0_waitrequest   => mb_I_pipe_stage_s0_waitrequest,
            m0_readdata      => mb_I_pipe_stage_s0_readdata,
            m0_readdatavalid => mb_I_pipe_stage_s0_readdatavalid,
            m0_burstcount    => mb_I_pipe_stage_s0_burstcount,
            m0_writedata     => mb_I_pipe_stage_s0_writedata,
            m0_address       => mb_I_pipe_stage_s0_address,
            m0_write         => mb_I_pipe_stage_s0_write,
            m0_read          => mb_I_pipe_stage_s0_read,
            m0_byteenable    => mb_I_pipe_stage_s0_byteenable,
            m0_debugaccess   => mb_I_pipe_stage_s0_debugaccess,
            m0_clk           => mb_I_emif_usr_clk,
            m0_reset         => mb_I_emif_usr_reset,
            s0_waitrequest   => mem0_waitrequest,
            s0_readdata      => mem0_readdata,
            s0_readdatavalid => mem0_readdatavalid,
            s0_burstcount    => mem0_burstcount,
            s0_writedata     => mem0_writedata,
            s0_address       => mem0_address,
            s0_write         => mem0_write,
            s0_read          => mem0_read,
            s0_byteenable    => mem0_byteenable,
            s0_debugaccess   => mem0_debugaccess,
            s0_clk           => kernel_clk,
            s0_reset         => kernel_reset
        );

    u_mb_I_pipe_stage : ta2_unb2b_ddr_pipe_stage
        generic map (
            DATA_WIDTH        => c_data_w,
            SYMBOL_WIDTH      => c_symbol_w,
            HDL_ADDR_WIDTH    => c_addr_w,
            BURSTCOUNT_WIDTH  => c_burstcount_w,
            PIPELINE_COMMAND  => c_pipeline_command,
            PIPELINE_RESPONSE => c_pipeline_response,
            SYNC_RESET        => c_sync_reset
        )
        port map (
            clk              => mb_I_emif_usr_clk,  -- clk.clk
            m0_waitrequest   => mb_I_pipe_stage_m0_waitrequest,
            m0_readdata      => mb_I_pipe_stage_m0_readdata,
            m0_readdatavalid => mb_I_pipe_stage_m0_readdatavalid,
            m0_burstcount    => mb_I_pipe_stage_m0_burstcount,
            m0_writedata     => mb_I_pipe_stage_m0_writedata,
            m0_address       => mb_I_pipe_stage_m0_address,
            m0_write         => mb_I_pipe_stage_m0_write,
            m0_read          => mb_I_pipe_stage_m0_read,
            m0_byteenable    => mb_I_pipe_stage_m0_byteenable,
            m0_debugaccess   => mb_I_pipe_stage_m0_debugaccess,
            reset            => mb_I_emif_usr_reset,  -- reset.reset
            s0_waitrequest   => mb_I_pipe_stage_s0_waitrequest,  -- s0.waitrequest
            s0_readdata      => mb_I_pipe_stage_s0_readdata,  -- .readdata
            s0_readdatavalid => mb_I_pipe_stage_s0_readdatavalid,  -- .readdatavalid
            s0_burstcount    => mb_I_pipe_stage_s0_burstcount,  -- .burstcount
            s0_writedata     => mb_I_pipe_stage_s0_writedata,  -- .writedata
            s0_address       => mb_I_pipe_stage_s0_address,  -- .address
            s0_write         => mb_I_pipe_stage_s0_write,  -- .write
            s0_read          => mb_I_pipe_stage_s0_read,  -- .read
            s0_byteenable    => mb_I_pipe_stage_s0_byteenable,  -- .byteenable
            s0_debugaccess   => mb_I_pipe_stage_s0_debugaccess  -- .debugaccess
        );

    mb_I_pipe_stage_m0_waitrequest                    <= not mb_I_amm_ready_0;
    mb_I_pipe_stage_m0_readdatavalid                  <= mb_I_amm_readdatavalid_0;
    mb_I_pipe_stage_m0_readdata                       <= mb_I_amm_readdata_0(c_data_w - 1 downto 0);

    mb_I_amm_read_0                                   <= mb_I_pipe_stage_m0_read;
    mb_I_amm_write_0                                  <= mb_I_pipe_stage_m0_write;
    mb_I_amm_address_0                                <= mb_I_pipe_stage_m0_address(c_addr_w - 1 downto c_addr_w - c_mb_I_ctlr_address_w);
    mb_I_amm_writedata_0(c_data_w - 1 downto 0)         <= mb_I_pipe_stage_m0_writedata;
    mb_I_amm_burstcount_0(c_burstcount_w - 1 downto 0)  <= mb_I_pipe_stage_m0_burstcount;
    mb_I_amm_byteenable_0(c_byteenable_w - 1 downto 0)  <= mb_I_pipe_stage_m0_byteenable;

    mb_I_emif_usr_reset <= not mb_I_emif_usr_reset_n;
    mb_I_ref_rst_n <= not mb_I_ref_rst;

    gen_I_ip_arria10_e1sg_ddr4_8g_1600 : if g_ddr_MB_I.name = "DDR4" and c_gigabytes_MB_I = 8 and g_ddr_MB_I.mts = 1600 generate
      u_ip_arria10_e1sg_ddr4_8g_1600 : ip_arria10_e1sg_ddr4_8g_1600
      port map (
        amm_ready_0         => mb_I_amm_ready_0,  -- ctrl_amm_avalon_slave_0.waitrequest_n
        amm_read_0          => mb_I_amm_read_0,  -- .read
        amm_write_0         => mb_I_amm_write_0,  -- .write
        amm_address_0       => mb_I_amm_address_0,  -- .address
        amm_readdata_0      => mb_I_amm_readdata_0,  -- .readdata
        amm_writedata_0     => mb_I_amm_writedata_0,  -- .writedata
        amm_burstcount_0    => mb_I_amm_burstcount_0,  -- .burstcount
        amm_byteenable_0    => mb_I_amm_byteenable_0,  -- .byteenable
        amm_readdatavalid_0 => mb_I_amm_readdatavalid_0,  -- .readdatavalid
        emif_usr_clk        => mb_I_emif_usr_clk,  -- emif_usr_clk_clock_source.clk
        emif_usr_reset_n    => mb_I_emif_usr_reset_n,  -- emif_usr_reset_reset_source.reset_n
        global_reset_n      => mb_I_ref_rst_n,  -- global_reset_reset_sink.reset_n
        mem_ck              => mb_I_ou.ck(g_ddr_MB_I.ck_w - 1 downto 0),  -- mem_conduit_end.mem_ck
        mem_ck_n            => mb_I_ou.ck_n(g_ddr_MB_I.ck_w - 1 downto 0),  -- .mem_ck_n
        mem_a               => mb_I_ou.a(g_ddr_MB_I.a_w - 1 downto 0),  -- .mem_a
     sl(mem_act_n)          => mb_I_ou.act_n,  -- .mem_act_n
        mem_ba              => mb_I_ou.ba(g_ddr_MB_I.ba_w - 1 downto 0),  -- .mem_ba
        mem_bg              => mb_I_ou.bg(g_ddr_MB_I.bg_w - 1 downto 0),  -- .mem_bg
        mem_cke             => mb_I_ou.cke(g_ddr_MB_I.cke_w - 1 downto 0),  -- .mem_cke
        mem_cs_n            => mb_I_ou.cs_n(g_ddr_MB_I.cs_w - 1 downto 0),  -- .mem_cs_n
        mem_odt             => mb_I_ou.odt(g_ddr_MB_I.odt_w - 1 downto 0),  -- .mem_odt
     sl(mem_reset_n)        => mb_I_ou.reset_n,  -- .mem_reset_n
     sl(mem_par)            => mb_I_ou.par,  -- .mem_par
        mem_alert_n         => slv(mb_I_in.alert_n),  -- .mem_alert_n
        mem_dqs             => mb_I_io.dqs(g_ddr_MB_I.dqs_w - 1 downto 0),  -- .mem_dqs
        mem_dqs_n           => mb_I_io.dqs_n(g_ddr_MB_I.dqs_w - 1 downto 0),  -- .mem_dqs_n
        mem_dq              => mb_I_io.dq(g_ddr_MB_I.dq_w - 1 downto 0),  -- .mem_dq
        mem_dbi_n           => mb_I_io.dbi_n(g_ddr_MB_I.dbi_w - 1 downto 0),  -- .mem_dbi_n
        oct_rzqin           => mb_I_in.oct_rzqin,  -- oct_conduit_end.oct_rzqin
        pll_ref_clk         => mb_I_ref_clk,  -- pll_ref_clk_clock_sink.clk
        local_cal_success   => OPEN,  -- status_conduit_end.local_cal_success
        local_cal_fail      => open  -- .local_cal_fail
      );
    end generate;

  end generate;

  gen_MB_II : if g_use_MB_II generate
    u_mb_II_clock_cross : ta2_unb2b_ddr_clock_cross
        generic map (
            DATA_WIDTH          => c_data_w,
            SYMBOL_WIDTH        => c_symbol_w,
            HDL_ADDR_WIDTH      => c_addr_w,
            BURSTCOUNT_WIDTH    => c_burstcount_w,
            COMMAND_FIFO_DEPTH  => c_command_fifo_depth,
            RESPONSE_FIFO_DEPTH => c_response_fifo_depth,
            MASTER_SYNC_DEPTH   => c_master_sync_depth,
            SLAVE_SYNC_DEPTH    => c_slave_sync_depth
        )
        port map (
            m0_waitrequest   => mb_II_pipe_stage_s0_waitrequest,
            m0_readdata      => mb_II_pipe_stage_s0_readdata,
            m0_readdatavalid => mb_II_pipe_stage_s0_readdatavalid,
            m0_burstcount    => mb_II_pipe_stage_s0_burstcount,
            m0_writedata     => mb_II_pipe_stage_s0_writedata,
            m0_address       => mb_II_pipe_stage_s0_address,
            m0_write         => mb_II_pipe_stage_s0_write,
            m0_read          => mb_II_pipe_stage_s0_read,
            m0_byteenable    => mb_II_pipe_stage_s0_byteenable,
            m0_debugaccess   => mb_II_pipe_stage_s0_debugaccess,
            m0_clk           => mb_II_emif_usr_clk,
            m0_reset         => mb_II_emif_usr_reset,
            s0_waitrequest   => mem1_waitrequest,
            s0_readdata      => mem1_readdata,
            s0_readdatavalid => mem1_readdatavalid,
            s0_burstcount    => mem1_burstcount,
            s0_writedata     => mem1_writedata,
            s0_address       => mem1_address,
            s0_write         => mem1_write,
            s0_read          => mem1_read,
            s0_byteenable    => mem1_byteenable,
            s0_debugaccess   => mem1_debugaccess,
            s0_clk           => kernel_clk,
            s0_reset         => kernel_reset
        );

    u_mb_II_pipe_stage : ta2_unb2b_ddr_pipe_stage
        generic map (
            DATA_WIDTH        => c_data_w,
            SYMBOL_WIDTH      => c_symbol_w,
            HDL_ADDR_WIDTH    => c_addr_w,
            BURSTCOUNT_WIDTH  => c_burstcount_w,
            PIPELINE_COMMAND  => c_pipeline_command,
            PIPELINE_RESPONSE => c_pipeline_response,
            SYNC_RESET        => c_sync_reset
        )
        port map (
            clk              => mb_II_emif_usr_clk,  -- clk.clk
            m0_waitrequest   => mb_II_pipe_stage_m0_waitrequest,
            m0_readdata      => mb_II_pipe_stage_m0_readdata,
            m0_readdatavalid => mb_II_pipe_stage_m0_readdatavalid,
            m0_burstcount    => mb_II_pipe_stage_m0_burstcount,
            m0_writedata     => mb_II_pipe_stage_m0_writedata,
            m0_address       => mb_II_pipe_stage_m0_address,
            m0_write         => mb_II_pipe_stage_m0_write,
            m0_read          => mb_II_pipe_stage_m0_read,
            m0_byteenable    => mb_II_pipe_stage_m0_byteenable,
            m0_debugaccess   => mb_II_pipe_stage_m0_debugaccess,
            reset            => mb_II_emif_usr_reset,  -- reset.reset
            s0_waitrequest   => mb_II_pipe_stage_s0_waitrequest,  -- s0.waitrequest
            s0_readdata      => mb_II_pipe_stage_s0_readdata,  -- .readdata
            s0_readdatavalid => mb_II_pipe_stage_s0_readdatavalid,  -- .readdatavalid
            s0_burstcount    => mb_II_pipe_stage_s0_burstcount,  -- .burstcount
            s0_writedata     => mb_II_pipe_stage_s0_writedata,  -- .writedata
            s0_address       => mb_II_pipe_stage_s0_address,  -- .address
            s0_write         => mb_II_pipe_stage_s0_write,  -- .write
            s0_read          => mb_II_pipe_stage_s0_read,  -- .read
            s0_byteenable    => mb_II_pipe_stage_s0_byteenable,  -- .byteenable
            s0_debugaccess   => mb_II_pipe_stage_s0_debugaccess  -- .debugaccess
        );

    mb_II_pipe_stage_m0_waitrequest                    <= not mb_II_amm_ready_0;
    mb_II_pipe_stage_m0_readdatavalid                  <= mb_II_amm_readdatavalid_0;
    mb_II_pipe_stage_m0_readdata                       <= mb_II_amm_readdata_0(c_data_w - 1 downto 0);

    mb_II_amm_read_0                                   <= mb_II_pipe_stage_m0_read;
    mb_II_amm_write_0                                  <= mb_II_pipe_stage_m0_write;
    mb_II_amm_address_0                                <= mb_II_pipe_stage_m0_address(c_addr_w - 1 downto c_addr_w - c_mb_II_ctlr_address_w);
    mb_II_amm_writedata_0(c_data_w - 1 downto 0)         <= mb_II_pipe_stage_m0_writedata;
    mb_II_amm_burstcount_0(c_burstcount_w - 1 downto 0)  <= mb_II_pipe_stage_m0_burstcount;
    mb_II_amm_byteenable_0(c_byteenable_w - 1 downto 0)  <= mb_II_pipe_stage_m0_byteenable;

    mb_II_emif_usr_reset <= not mb_II_emif_usr_reset_n;
    mb_II_ref_rst_n <= not mb_II_ref_rst;

    gen_II_ip_arria10_e1sg_ddr4_8g_1600 : if g_ddr_MB_II.name = "DDR4" and c_gigabytes_MB_II = 8 and g_ddr_MB_II.mts = 1600 generate
      u_ip_arria10_e1sg_ddr4_8g_1600 : ip_arria10_e1sg_ddr4_8g_1600
      port map (
        amm_ready_0         => mb_II_amm_ready_0,  -- ctrl_amm_avalon_slave_0.waitrequest_n
        amm_read_0          => mb_II_amm_read_0,  -- .read
        amm_write_0         => mb_II_amm_write_0,  -- .write
        amm_address_0       => mb_II_amm_address_0,  -- .address
        amm_readdata_0      => mb_II_amm_readdata_0,  -- .readdata
        amm_writedata_0     => mb_II_amm_writedata_0,  -- .writedata
        amm_burstcount_0    => mb_II_amm_burstcount_0,  -- .burstcount
        amm_byteenable_0    => mb_II_amm_byteenable_0,  -- .byteenable
        amm_readdatavalid_0 => mb_II_amm_readdatavalid_0,  -- .readdatavalid
        emif_usr_clk        => mb_II_emif_usr_clk,  -- emif_usr_clk_clock_source.clk
        emif_usr_reset_n    => mb_II_emif_usr_reset_n,  -- emif_usr_reset_reset_source.reset_n
        global_reset_n      => mb_II_ref_rst_n,  -- global_reset_reset_sink.reset_n
        mem_ck              => mb_II_ou.ck(g_ddr_MB_II.ck_w - 1 downto 0),  -- mem_conduit_end.mem_ck
        mem_ck_n            => mb_II_ou.ck_n(g_ddr_MB_II.ck_w - 1 downto 0),  -- .mem_ck_n
        mem_a               => mb_II_ou.a(g_ddr_MB_II.a_w - 1 downto 0),  -- .mem_a
     sl(mem_act_n)          => mb_II_ou.act_n,  -- .mem_act_n
        mem_ba              => mb_II_ou.ba(g_ddr_MB_II.ba_w - 1 downto 0),  -- .mem_ba
        mem_bg              => mb_II_ou.bg(g_ddr_MB_II.bg_w - 1 downto 0),  -- .mem_bg
        mem_cke             => mb_II_ou.cke(g_ddr_MB_II.cke_w - 1 downto 0),  -- .mem_cke
        mem_cs_n            => mb_II_ou.cs_n(g_ddr_MB_II.cs_w - 1 downto 0),  -- .mem_cs_n
        mem_odt             => mb_II_ou.odt(g_ddr_MB_II.odt_w - 1 downto 0),  -- .mem_odt
     sl(mem_reset_n)        => mb_II_ou.reset_n,  -- .mem_reset_n
     sl(mem_par)            => mb_II_ou.par,  -- .mem_par
        mem_alert_n         => slv(mb_II_in.alert_n),  -- .mem_alert_n
        mem_dqs             => mb_II_io.dqs(g_ddr_MB_II.dqs_w - 1 downto 0),  -- .mem_dqs
        mem_dqs_n           => mb_II_io.dqs_n(g_ddr_MB_II.dqs_w - 1 downto 0),  -- .mem_dqs_n
        mem_dq              => mb_II_io.dq(g_ddr_MB_II.dq_w - 1 downto 0),  -- .mem_dq
        mem_dbi_n           => mb_II_io.dbi_n(g_ddr_MB_II.dbi_w - 1 downto 0),  -- .mem_dbi_n
        oct_rzqin           => mb_II_in.oct_rzqin,  -- oct_conduit_end.oct_rzqin
        pll_ref_clk         => mb_II_ref_clk,  -- pll_ref_clk_clock_sink.clk
        local_cal_success   => OPEN,  -- status_conduit_end.local_cal_success
        local_cal_fail      => open  -- .local_cal_fail
      );
    end generate;

  end generate;
end str;
