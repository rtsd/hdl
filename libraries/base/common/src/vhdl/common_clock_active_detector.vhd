-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Detect 400 MHz in_clk active in the 200 MHz dp_clk domain
-- Description:

entity common_clock_active_detector is
  generic (
    g_in_period_w       : natural := 8;  -- created 2**g_in_period_w period source signal in in_clk domain
    g_dp_detect_period  : natural := 128;  -- expected period for source signal in dp_clk domain given 2**g_in_period_w in in_clk domain
    g_dp_detect_margin  : natural := 1
  );
  port (
    -- PHY input interface
    in_clk               : in  std_logic;
    dp_rst               : in  std_logic := '0';
    dp_clk               : in  std_logic;
    dp_in_clk_detected   : out std_logic;
    dp_in_clk_stable     : out std_logic;
    dp_in_clk_stable_ack : in  std_logic := '1'
  );
end common_clock_active_detector;

architecture str of common_clock_active_detector is
  constant c_delay_len           : natural := c_meta_delay_len;

  constant c_dp_detect_period_w  : natural := ceil_log2(g_dp_detect_period);
  constant c_dp_detect_max       : natural := g_dp_detect_period + g_dp_detect_margin;
  constant c_dp_detect_min       : natural := g_dp_detect_period - g_dp_detect_margin;
  constant c_dp_clk_cnt_w        : natural := c_dp_detect_period_w + 1;  -- +1 to be wide enough to fit somewhat more than maximum nof clock cycles per interval
  constant c_dp_clk_cnt_max      : natural := 2**c_dp_clk_cnt_w - 1;

  signal dbg_g_in_period_w       : natural := g_in_period_w;
  signal dbg_g_dp_detect_period  : natural := g_dp_detect_period;
  signal dbg_g_dp_detect_margin  : natural := g_dp_detect_margin;
  signal dbg_c_dp_detect_max     : natural := c_dp_detect_max;
  signal dbg_c_dp_detect_min     : natural := c_dp_detect_min;
  signal dbg_c_dp_clk_cnt_max    : natural := c_dp_clk_cnt_max;

  signal in_clk_cnt              : std_logic_vector(g_in_period_w - 1 downto 0);
  signal in_toggle               : std_logic;

  signal dp_toggle               : std_logic;
  signal dp_toggle_revt          : std_logic;
  signal dp_clk_cnt_en           : std_logic;
  signal dp_clk_cnt_clr          : std_logic;
  signal dp_clk_cnt              : std_logic_vector(c_dp_clk_cnt_w - 1 downto 0);
  signal dp_clk_interval         : std_logic_vector(c_dp_clk_cnt_w - 1 downto 0);
  signal nxt_dp_clk_interval     : std_logic_vector(c_dp_clk_cnt_w - 1 downto 0);
  signal i_dp_in_clk_detected    : std_logic;
  signal nxt_dp_in_clk_detected  : std_logic;
begin
  u_common_counter_in_clk : entity work.common_counter
  generic map (
    g_width => g_in_period_w
  )
  port map (
    rst     => '0',
    clk     => in_clk,
    count   => in_clk_cnt
  );

  in_toggle <= in_clk_cnt(in_clk_cnt'high);

  u_common_async_dp_toggle : entity work.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_delay_len
  )
  port map (
    rst  => dp_rst,
    clk  => dp_clk,
    din  => in_toggle,
    dout => dp_toggle
  );

  u_common_evt : entity work.common_evt
  generic map (
    g_evt_type   => "RISING",
    g_out_reg    => true
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    in_sig   => dp_toggle,
    out_evt  => dp_toggle_revt
  );

  dp_clk_cnt_en  <= '1' when unsigned(dp_clk_cnt) < c_dp_clk_cnt_max else '0';
  dp_clk_cnt_clr <= dp_toggle_revt or not dp_clk_cnt_en;

  u_common_counter_dp_clk : entity work.common_counter
  generic map (
    g_width => c_dp_clk_cnt_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => dp_clk_cnt_clr,
    cnt_en  => dp_clk_cnt_en,
    count   => dp_clk_cnt
  );

  nxt_dp_clk_interval <= INCR_UVEC(dp_clk_cnt, 1) when dp_clk_cnt_clr = '1' else dp_clk_interval;

  nxt_dp_in_clk_detected <= '1' when unsigned(dp_clk_interval) >= c_dp_detect_min and unsigned(dp_clk_interval) <= c_dp_detect_max else '0';

  p_dp_reg : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      dp_clk_interval      <= (others => '0');
      i_dp_in_clk_detected <= '0';
    elsif rising_edge(dp_clk) then
      dp_clk_interval      <= nxt_dp_clk_interval;
      i_dp_in_clk_detected <= nxt_dp_in_clk_detected;
    end if;
  end process;

  dp_in_clk_detected <= i_dp_in_clk_detected;

  u_common_stable_monitor : entity work.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => i_dp_in_clk_detected,
    r_stable     => dp_in_clk_stable,
    r_stable_ack => dp_in_clk_stable_ack
  );
end str;
