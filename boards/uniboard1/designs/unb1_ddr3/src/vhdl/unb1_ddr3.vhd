-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, technology_lib, tech_ddr_lib, io_ddr_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity unb1_ddr3 is
  generic (
    g_sim         : boolean := false;
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 4;
    -- Stamps are passed via QIP at compile start if $UNB_COMPILE_STAMPS is set
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD)
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)
    g_stamp_svn   : natural := 0;  -- SVN revision
    g_st_dat_w    : natural := 64  -- Any power of two 8..256
  );
  port (
    -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc                : inout std_logic;
    sens_sd                : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk                : in    std_logic;
    ETH_SGIN               : in    std_logic;
    ETH_SGOUT              : out   std_logic;

    -- SO-DIMM Memory Bank I
    MB_I_IN                : in    t_tech_ddr3_phy_in;
    MB_I_IO                : inout t_tech_ddr3_phy_io;
    MB_I_OU                : out   t_tech_ddr3_phy_ou
  );
end unb1_ddr3;

architecture str of unb1_ddr3 is
  -- Constant definitions for ctrl_unb_common
  constant c_design_name    : string := "unb1_ddr3";
  constant c_design_note    : string := "DDR3 reference design";
  constant c_fw_version     : t_unb1_board_fw_version := (0, 3);  -- firmware version x.y
    -- Use PHY Interface
    -- TYPE t_c_unb_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   tr_front: NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
  constant c_use_phy        : t_c_unb1_board_use_phy := (1, 0, 0, 0, 1, 0, 0, 1);
  constant c_aux            : t_c_unb1_board_aux     := c_unb1_board_aux;
  constant c_app_led_en     : boolean                := true;
  constant c_technology     : natural                := c_tech_select_default;
  constant c_tech_ddr       : t_c_tech_ddr           := c_tech_ddr3_4g_800m_master;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal cal_clk                    : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_pps                     : std_logic;

  signal ddr_ref_rst                : std_logic;

  signal this_chip_id               : std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- [2:0], so range 0-3 for FN and range 4-7 BN

  signal app_led_red                : std_logic := '0';
  signal app_led_green              : std_logic := '1';

  -- PIOs
  signal pout_debug_wave            : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;
--  SIGNAL eth1g_led                  : t_tech_tse_led;

    -- . UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- IO DDR register map
  signal reg_io_ddr_mosi            : t_mem_mosi;
  signal reg_io_ddr_miso            : t_mem_miso;

  signal reg_diag_data_buf_mosi     : t_mem_mosi;
  signal reg_diag_data_buf_miso     : t_mem_miso;

  signal ram_diag_data_buf_mosi     : t_mem_mosi;
  signal ram_diag_data_buf_miso     : t_mem_miso;

  signal reg_diag_bg_ctrl_mosi      : t_mem_mosi;
  signal reg_diag_bg_ctrl_miso      : t_mem_miso;

  signal ram_diag_bg_data_mosi      : t_mem_mosi;
  signal ram_diag_bg_data_miso      : t_mem_miso;

  signal reg_diag_tx_seq_mosi       : t_mem_mosi;
  signal reg_diag_tx_seq_miso       : t_mem_miso;

  signal reg_diag_rx_seq_mosi       : t_mem_mosi;
  signal reg_diag_rx_seq_miso       : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    -- General
    g_sim            => g_sim,
    g_stamp_date     => g_stamp_date,
    g_stamp_time     => g_stamp_time,
    g_stamp_svn      => g_stamp_svn,
    g_design_name    => c_design_name,
    g_design_note    => c_design_note,
    g_fw_version     => c_fw_version,
    g_mm_clk_freq    => c_unb1_board_mm_clk_freq_50M,
    g_dp_clk_use_pll => false,
    g_app_led_red    => c_app_led_en,
    g_app_led_green  => c_app_led_en,
    g_use_phy        => c_use_phy,
    g_aux            => c_aux
  )
  port map (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    cs_sim                 => cs_sim,
    xo_clk                 => xo_clk,
    xo_rst_n               => xo_rst_n,
    mm_clk                 => mm_clk,
    mm_locked              => mm_locked,
    mm_rst                 => mm_rst,

    dp_rst                 => OPEN,
    dp_clk                 => OPEN,
    dp_pps                 => dp_pps,
    dp_rst_in              => dp_rst,
    dp_clk_in              => dp_clk,

    this_chip_id           => this_chip_id,
    this_bck_id            => OPEN,

    app_led_red            => app_led_red,
    app_led_green          => app_led_green,

    -- PIOs
    pout_debug_wave        => pout_debug_wave,
    pout_wdi               => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi           => reg_wdi_mosi,
    reg_wdi_miso           => reg_wdi_miso,

    -- . PPSH
    reg_ppsh_mosi          => reg_ppsh_mosi,
    reg_ppsh_miso          => reg_ppsh_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

     -- UniBoard I2C sensors
    reg_unb_sens_mosi      => reg_unb_sens_mosi,
    reg_unb_sens_miso      => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk          => eth1g_tse_clk,
    eth1g_mm_rst           => eth1g_mm_rst,
    eth1g_tse_mosi         => eth1g_tse_mosi,
    eth1g_tse_miso         => eth1g_tse_miso,
    eth1g_reg_mosi         => eth1g_reg_mosi,
    eth1g_reg_miso         => eth1g_reg_miso,
    eth1g_reg_interrupt    => eth1g_reg_interrupt,
    eth1g_ram_mosi         => eth1g_ram_mosi,
    eth1g_ram_miso         => eth1g_ram_miso,

    --
    -- >>> Ctrl FPGA pins
    --
    -- General
    CLK                    => CLK,
    PPS                    => PPS,
    WDI                    => WDI,
    INTA                   => INTA,
    INTB                   => INTB,

    -- Others
    VERSION                => VERSION,
    ID                     => ID,
    TESTIO                 => TESTIO,

    -- I2C Interface to Sensors
    sens_sc                => sens_sc,
    sens_sd                => sens_sd,

    ETH_clk                => ETH_clk,
    ETH_SGIN               => ETH_SGIN,
    ETH_SGOUT              => ETH_SGOUT
  );

  u_mmm : entity work.mmm_unb1_ddr3
  generic map(
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
  )
  port map (
    -- GENERAL
    xo_clk                   => xo_clk,
    xo_rst_n                 => xo_rst_n,
    xo_rst                   => xo_rst,

    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    cal_clk                  => cal_clk,

    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- DDR3
    reg_io_ddr_mosi          => reg_io_ddr_mosi,
    reg_io_ddr_miso          => reg_io_ddr_miso,

    -- Data Buffer Control
    reg_diag_data_buf_mosi   => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso   => reg_diag_data_buf_miso,

    -- Data Buffer Data
    ram_diag_data_buf_mosi   => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso   => ram_diag_data_buf_miso,

    -- Block Generator Control
    reg_diag_bg_ctrl_mosi    => reg_diag_bg_ctrl_mosi,
    reg_diag_bg_ctrl_miso    => reg_diag_bg_ctrl_miso,

    -- Block Generator Data
    ram_diag_bg_data_mosi    => ram_diag_bg_data_mosi,
    ram_diag_bg_data_miso    => ram_diag_bg_data_miso,

    -- TX Sequencer
    reg_diag_tx_seq_mosi     => reg_diag_tx_seq_mosi,
    reg_diag_tx_seq_miso     => reg_diag_tx_seq_miso,

    -- RX Sequencer
    reg_diag_rx_seq_mosi     => reg_diag_rx_seq_mosi,
    reg_diag_rx_seq_miso     => reg_diag_rx_seq_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso
  );

  u_areset_ddr_ref_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1',
    g_delay_len => 40
  )
  port map(
    clk     => CLK,
    in_rst  => mm_rst,
    out_rst => ddr_ref_rst
  );

  u_node : entity work.node_unb1_ddr3
  generic map (
    g_sim                    => g_sim,
    g_technology             => c_technology,
    g_tech_ddr               => c_tech_ddr,
    g_st_dat_w               => g_st_dat_w
  )
  port map (
    -- System
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,

    ddr_ref_clk              => CLK,
    ddr_ref_rst              => ddr_ref_rst,

    -- Clock outputs
    ddr_out_clk              => dp_clk,
    ddr_out_rst              => dp_rst,

    -- MM interface
    reg_io_ddr_mosi          => reg_io_ddr_mosi,
    reg_io_ddr_miso          => reg_io_ddr_miso,

    -- Data Buffer Control
    reg_diag_data_buf_mosi   => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso   => reg_diag_data_buf_miso,

    -- Data Buffer Data
    ram_diag_data_buf_mosi   => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso   => ram_diag_data_buf_miso,

    -- Block Generator Control
    reg_diag_bg_ctrl_mosi    => reg_diag_bg_ctrl_mosi,
    reg_diag_bg_ctrl_miso    => reg_diag_bg_ctrl_miso,

    -- Block Generator Data
    ram_diag_bg_data_mosi    => ram_diag_bg_data_mosi,
    ram_diag_bg_data_miso    => ram_diag_bg_data_miso,

    -- TX Sequencer
    reg_diag_tx_seq_mosi     => reg_diag_tx_seq_mosi,
    reg_diag_tx_seq_miso     => reg_diag_tx_seq_miso,

    -- RX Sequencer
    reg_diag_rx_seq_mosi     => reg_diag_rx_seq_mosi,
    reg_diag_rx_seq_miso     => reg_diag_rx_seq_miso,

    MB_I_IN                  => MB_I_IN,
    MB_I_IO                  => MB_I_IO,
    MB_I_OU                  => MB_I_OU
  );
end str;
