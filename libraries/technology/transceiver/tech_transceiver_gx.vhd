--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use work.tech_transceiver_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tech_transceiver_gx is
  generic(
    g_technology            : natural := c_tech_select_default;
    g_data_w                : natural := 32;  -- Supported: 32, 16.
    g_nof_gx                : natural;
    g_mbps                  : natural;  -- Supported: 6250, 5000, 3125, 2500 when g_data_w=32, else 2500 when g_data_w=16
    g_sim                   : boolean;
    g_tx                    : boolean;
    g_rx                    : boolean
  );
  port(
    cal_rec_clk             : in  std_logic;
    tr_clk                  : in  std_logic;

    rx_clk                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_rst                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_sosi_arr             : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    rx_siso_arr             : in  t_dp_siso_arr(g_nof_gx - 1 downto 0);

    tx_clk                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_rst                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_sosi_arr             : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    tx_siso_arr             : out t_dp_siso_arr(g_nof_gx - 1 downto 0);

    --Serial I/O
    rx_datain               : in  std_logic_vector(g_nof_gx - 1 downto 0);
    tx_dataout              : out std_logic_vector(g_nof_gx - 1 downto 0);

    -- Ctrl&stat
    tx_state                : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    tx_align_en             : in  std_logic_vector(g_nof_gx - 1 downto 0);

    rx_state                : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    rx_align_en             : in  std_logic_vector(g_nof_gx - 1 downto 0)
  );
end tech_transceiver_gx;

architecture str of tech_transceiver_gx is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : entity work.tech_transceiver_gx_stratix_iv
    generic map (g_data_w, g_nof_gx, g_mbps, g_sim, g_tx, g_rx)
    port map (cal_rec_clk, tr_clk, rx_clk, rx_rst, rx_sosi_arr, rx_siso_arr, tx_clk, tx_rst, tx_sosi_arr, tx_siso_arr, rx_datain, tx_dataout, tx_state, tx_align_en, rx_state, rx_align_en);
  end generate;
end str;
