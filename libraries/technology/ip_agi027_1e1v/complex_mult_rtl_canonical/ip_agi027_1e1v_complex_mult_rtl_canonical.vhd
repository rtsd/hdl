-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author :
-- . D.F. Brouwer
-- Purpose:
-- . RTL complex multiplier, canonical version (3 simple multipliers).
-- Description:
-- . re = ((ar+ai)*(br-bi))+(ar*bi-ai*br)
--   im = ar*bi+ai*br
-- Remark:
-- . g_conjugate_b is not supported!
-- Reference:
-- . Copied from */technology/ip_arria10/complex_mult_rtl_canonical/ip_arria10_complex_mult_rtl_canonical.vhd, authored by Daniel van der Schuur

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ip_agi027_1e1v_complex_mult_rtl_canonical is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
--    g_conjugate_b      : BOOLEAN := FALSE;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
end ip_agi027_1e1v_complex_mult_rtl_canonical;

architecture str of ip_agi027_1e1v_complex_mult_rtl_canonical is
  function RESIZE_NUM(s : signed; w : natural) return signed is
  begin
    -- extend sign bit or keep LS part
    if w > s'length then
      return resize(s, w);  -- extend sign bit
    else
      return signed(resize(unsigned(s), w));  -- keep LSbits (= vec[w-1:0])
    end if;
  end;

  function largest(n, m : integer) return integer is
  begin
    if n > m then
      return n;
    else
      return m;
    end if;
  end;

  -----------------------------------------------------------------------------
  -- Multiply / add output signals
  -- . re = ((ar+ai)*(br-bi))+(ar*bi-ai*br)
  --   im = ar*bi+ai*br
  -----------------------------------------------------------------------------
  constant c_sum_ar_ai_w : natural := g_in_a_w + 1;  -- sum_ar_ai
  constant c_sum_br_bi_w : natural := g_in_b_w + 1;  -- sum_br_bi
  constant c_prod_w      : natural := g_in_a_w + g_in_b_w;  -- prod_ar_bi, prod_ai_br
  constant c_sum_prod_w  : natural := c_prod_w + 1;  -- sum_prod_ar_bi_prod_ai_br
  constant c_prod_sum_w  : natural := c_sum_ar_ai_w + c_sum_br_bi_w;  -- prod_sum_ar_ai_sum_br_bi
  constant c_sum_im_w    : natural := c_prod_w + 1;  -- sum_im
  constant c_sum_re_w    : natural := largest(c_sum_prod_w, c_prod_sum_w) + 1;  -- sum_re

  signal sum_ar_ai                 : signed(c_sum_ar_ai_w - 1 downto 0);  -- ar+ai                           : used in re
  signal sum_br_bi                 : signed(c_sum_br_bi_w - 1 downto 0);  -- br-bi                           : used in re
  signal sum_prod_ar_bi_prod_ai_br : signed(c_sum_prod_w - 1 downto 0);  -- ar*bi-ai*br                     : used in re
  signal sum_im                    : signed(c_sum_im_w - 1 downto 0);  -- ar*bi+ai*br                     : im
  signal sum_re                    : signed(c_sum_re_w - 1 downto 0);  -- ((ar+ai)*(br-bi))+(ar*bi-ai*br) : re

  signal prod_ar_bi                : signed(c_prod_w - 1 downto 0);  -- ar*bi                           : used in re and im
  signal prod_ai_br                : signed(c_prod_w - 1 downto 0);  -- ai*br                           : used in re and im
  signal prod_sum_ar_ai_sum_br_bi  : signed(c_prod_sum_w - 1 downto 0);  -- (ar+ai)*(br-bi)                 : used in re

  -----------------------------------------------------------------------------
  -- register signals
  -----------------------------------------------------------------------------
  signal ar                            : signed(g_in_a_w - 1 downto 0);
  signal ai                            : signed(g_in_a_w - 1 downto 0);
  signal br                            : signed(g_in_b_w - 1 downto 0);
  signal bi                            : signed(g_in_b_w - 1 downto 0);

  signal nxt_ar                        : signed(g_in_a_w - 1 downto 0);
  signal nxt_ai                        : signed(g_in_a_w - 1 downto 0);
  signal nxt_br                        : signed(g_in_b_w - 1 downto 0);
  signal nxt_bi                        : signed(g_in_b_w - 1 downto 0);

  signal reg_ar                        : signed(g_in_a_w - 1 downto 0);
  signal reg_ai                        : signed(g_in_a_w - 1 downto 0);
  signal reg_br                        : signed(g_in_b_w - 1 downto 0);
  signal reg_bi                        : signed(g_in_b_w - 1 downto 0);

  signal nxt_sum_ar_ai                 : signed(c_sum_ar_ai_w - 1 downto 0);
  signal nxt_sum_br_bi                 : signed(c_sum_br_bi_w - 1 downto 0);
  signal nxt_sum_prod_ar_bi_prod_ai_br : signed(c_sum_prod_w - 1 downto 0);
  signal nxt_sum_im                    : signed(c_sum_im_w - 1 downto 0);
  signal nxt_sum_re                    : signed(c_sum_re_w - 1 downto 0);

  signal nxt_prod_ar_bi                : signed(c_prod_w - 1 downto 0);
  signal nxt_prod_ai_br                : signed(c_prod_w - 1 downto 0);
  signal nxt_prod_sum_ar_ai_sum_br_bi  : signed(c_prod_sum_w - 1 downto 0);

  signal reg_sum_ar_ai                 : signed(c_sum_ar_ai_w - 1 downto 0);
  signal reg_sum_br_bi                 : signed(c_sum_br_bi_w - 1 downto 0);
  signal reg_sum_prod_ar_bi_prod_ai_br : signed(c_sum_prod_w - 1 downto 0);
  signal reg_sum_im                    : signed(c_sum_im_w - 1 downto 0);
  signal reg_sum_re                    : signed(c_sum_re_w - 1 downto 0);

  signal reg_prod_ar_bi                : signed(c_prod_w - 1 downto 0);
  signal reg_prod_ai_br                : signed(c_prod_w - 1 downto 0);
  signal reg_prod_sum_ar_ai_sum_br_bi  : signed(c_prod_sum_w - 1 downto 0);

  signal nxt_result_re                 : signed(g_out_p_w - 1 downto 0);
  signal nxt_result_im                 : signed(g_out_p_w - 1 downto 0);

  signal reg_result_re                 : signed(g_out_p_w - 1 downto 0);
  signal reg_result_im                 : signed(g_out_p_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------

  -- Put all potential registers in a single process for optimal DSP inferrence
  -- Use rst only if it is supported by the DSP primitive, else leave it at '0'
  p_reg : process (rst, clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        reg_ar                        <= (others => '0');
        reg_ai                        <= (others => '0');
        reg_br                        <= (others => '0');
        reg_bi                        <= (others => '0');

        reg_sum_ar_ai                 <= (others => '0');
        reg_sum_br_bi                 <= (others => '0');
        reg_sum_prod_ar_bi_prod_ai_br <= (others => '0');
        reg_sum_im                    <= (others => '0');
        reg_sum_re                    <= (others => '0');

        reg_prod_ar_bi                <= (others => '0');
        reg_prod_ai_br                <= (others => '0');
        reg_prod_sum_ar_ai_sum_br_bi  <= (others => '0');
      elsif clken = '1' then
        reg_ar                        <= nxt_ar;
        reg_ai                        <= nxt_ai;
        reg_br                        <= nxt_br;
        reg_bi                        <= nxt_bi;

        reg_sum_ar_ai                 <= nxt_sum_ar_ai;
        reg_sum_br_bi                 <= nxt_sum_br_bi;
        reg_sum_prod_ar_bi_prod_ai_br <= nxt_sum_prod_ar_bi_prod_ai_br;
        reg_sum_im                    <= nxt_sum_im;
        reg_sum_re                    <= nxt_sum_re;

        reg_prod_ar_bi                <= nxt_prod_ar_bi;
        reg_prod_ai_br                <= nxt_prod_ai_br;
        reg_prod_sum_ar_ai_sum_br_bi  <= nxt_prod_sum_ar_ai_sum_br_bi;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Inputs
  ------------------------------------------------------------------------------
  nxt_ar <= signed(in_ar);
  nxt_ai <= signed(in_ai);
  nxt_br <= signed(in_br);
  nxt_bi <= signed(in_bi);

  no_input_reg : if g_pipeline_input = 0 generate  -- wired
    ar <= nxt_ar;
    ai <= nxt_ai;
    br <= nxt_br;
    bi <= nxt_bi;
  end generate;

  gen_input_reg : if g_pipeline_input > 0 generate  -- register input
    ar <= reg_ar;
    ai <= reg_ai;
    br <= reg_br;
    bi <= reg_bi;
  end generate;

  ------------------------------------------------------------------------------
  -- Sums
  ------------------------------------------------------------------------------
  nxt_sum_ar_ai                 <= RESIZE_NUM(ar, c_sum_ar_ai_w) + ai;
  nxt_sum_br_bi                 <= RESIZE_NUM(br, c_sum_br_bi_w) + bi;
  nxt_sum_prod_ar_bi_prod_ai_br <= RESIZE_NUM(prod_ar_bi, c_prod_sum_w) + prod_ai_br;
  nxt_sum_re                    <= RESIZE_NUM(prod_sum_ar_ai_sum_br_bi, c_prod_sum_w), sum_prod_ar_bi_prod_ai_br;
  nxt_sum_im                    <= RESIZE_NUM(prod_ai_br, c_sum_im_w) + prod_ar_bi;

  no_adder_reg : if g_pipeline_adder = 0 generate  -- wired
    sum_ar_ai                 <= nxt_sum_ar_ai;
    sum_br_bi                 <= nxt_sum_br_bi;
    sum_prod_ar_bi_prod_ai_br <= nxt_sum_prod_ar_bi_prod_ai_br;
    sum_re                    <= nxt_sum_re;
    sum_im                    <= nxt_sum_im;
  end generate;
  gen_adder_reg : if g_pipeline_adder > 0 generate  -- register
    sum_ar_ai                 <= reg_sum_ar_ai;
    sum_br_bi                 <= reg_sum_br_bi;
    sum_prod_ar_bi_prod_ai_br <= reg_sum_prod_ar_bi_prod_ai_br;
    sum_re                    <= reg_sum_re;
    sum_im                    <= reg_sum_im;
  end generate;

  ------------------------------------------------------------------------------
  -- Products
  ------------------------------------------------------------------------------
  nxt_prod_ar_bi               <= ar * bi;
  nxt_prod_ai_br               <= ai * br;
  nxt_prod_sum_ar_ai_sum_br_bi <= sum_ar_ai * sum_br_bi;

  no_product_reg : if g_pipeline_product = 0 generate  -- wired
    prod_ar_bi               <= nxt_prod_ar_bi;
    prod_ai_br               <= nxt_prod_ai_br;
    prod_sum_ar_ai_sum_br_bi <= nxt_prod_sum_ar_ai_sum_br_bi;
  end generate;
  gen_product_reg : if g_pipeline_product > 0 generate  -- register
    prod_ar_bi               <= reg_prod_ar_bi;
    prod_ai_br               <= reg_prod_ai_br;
    prod_sum_ar_ai_sum_br_bi <= reg_prod_sum_ar_ai_sum_br_bi;
  end generate;

  ------------------------------------------------------------------------------
  -- Result sum after optional rounding
  ------------------------------------------------------------------------------

  nxt_result_re <= RESIZE_NUM(sum_re, g_out_p_w);
  nxt_result_im <= RESIZE_NUM(sum_im, g_out_p_w);

  no_result_reg : if g_pipeline_output = 0 generate  -- wired
    result_re <= std_logic_vector(nxt_result_re);
    result_im <= std_logic_vector(nxt_result_im);
  end generate;
  gen_result_reg : if g_pipeline_output > 0 generate  -- register
    result_re <= std_logic_vector(reg_result_re);
    result_im <= std_logic_vector(reg_result_im);
  end generate;
end architecture;
