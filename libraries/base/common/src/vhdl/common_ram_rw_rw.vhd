-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   -
-- Changed by:
--   D.F. Brouwer
-- Description:
--   This a true dual port ram that only uses one clock domain
-- Remarks:
--   The crw_crw RAM covers all other variants, which were utilized by other
--   common RAM variant files. However, because the crw_crw IP is no longer
--   supported as it was previously used for previous FPGA technology identifiers
--   (device types) by the Agilex 7 (agi027_1e1v), the rw_rw IP should be used.
--   As a result, this file has been modified. [1]
-- Reference:
--   [1] Based on the architecture of common_ram_crw_crw.vhd.

library IEEE, technology_lib, tech_memory_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_ram_rw_rw is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_ram            : t_c_mem := c_mem_ram;
    g_init_file      : string  := "UNUSED";
    g_true_dual_port : boolean := true
  );
  port (
    rst       : in  std_logic := '0';
    clk       : in  std_logic;
    clken     : in  std_logic := '1';
    wr_en_a   : in  std_logic := '0';
    wr_en_b   : in  std_logic := '0';
    wr_dat_a  : in  std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    wr_dat_b  : in  std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    adr_a     : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    adr_b     : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    rd_en_a   : in  std_logic := '1';
    rd_en_b   : in  std_logic := '1';
    rd_dat_a  : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    rd_dat_b  : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    rd_val_a  : out std_logic;
    rd_val_b  : out std_logic
  );
end common_ram_rw_rw;

architecture str of common_ram_rw_rw is
  constant c_rd_latency : natural := sel_a_b(g_ram.latency < 2,            g_ram.latency,              2);  -- handle read latency 1 or 2 in RAM
  constant c_pipeline   : natural := sel_a_b(g_ram.latency > c_rd_latency, g_ram.latency - c_rd_latency, 0);  -- handle rest of read latency > 2 in pipeline

  -- Intermediate signal for extra pipelining
  signal ram_rd_dat_a   : std_logic_vector(rd_dat_a'range);
  signal ram_rd_dat_b   : std_logic_vector(rd_dat_b'range);

  -- Map sl to single bit slv for rd_val pipelining
  signal ram_rd_en_a    : std_logic_vector(0 downto 0);
  signal ram_rd_en_b    : std_logic_vector(0 downto 0);
  signal ram_rd_val_a   : std_logic_vector(0 downto 0);
  signal ram_rd_val_b   : std_logic_vector(0 downto 0);
begin
  assert g_ram.latency >= 1
    report "common_ram_rw_rw : only support read latency >= 1"
    severity FAILURE;

  -- memory access
  gen_true_dual_port : if g_true_dual_port = true generate
    u_ram : entity tech_memory_lib.tech_memory_ram_rw_rw
    generic map (
      g_technology => g_technology,
      g_adr_w      => g_ram.adr_w,
      g_dat_w      => g_ram.dat_w,
      g_nof_words  => g_ram.nof_dat,
      g_rd_latency => c_rd_latency,
      g_init_file  => g_init_file
    )
    port map (
      clock       => clk,
      enable      => clken,
      wren_a      => wr_en_a,
      wren_b      => wr_en_b,
      data_a      => wr_dat_a,
      data_b      => wr_dat_b,
      address_a   => adr_a,
      address_b   => adr_b,
      q_a         => ram_rd_dat_a,
      q_b         => ram_rd_dat_b
    );
  end generate;

  gen_simple_dual_port : if g_true_dual_port = false generate
    u_ram : entity tech_memory_lib.tech_memory_ram_r_w
    generic map (
      g_technology => g_technology,
      g_adr_w      => g_ram.adr_w,
      g_dat_w      => g_ram.dat_w,
      g_nof_words  => g_ram.nof_dat,
      g_rd_latency => c_rd_latency,
      g_init_file  => g_init_file
    )
    port map
    (
      clock     => clk,
      enable    => clken,
      wren      => wr_en_a,
      wraddress => adr_a,
      data      => wr_dat_a,
      rdaddress => adr_b,
      q         => ram_rd_dat_b
    );
  end generate;

  -- read output
  u_pipe_a : entity work.common_pipeline
  generic map (
    g_pipeline   => c_pipeline,
    g_in_dat_w   => g_ram.dat_w,
    g_out_dat_w  => g_ram.dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => ram_rd_dat_a,
    out_dat => rd_dat_a
  );

  u_pipe_b : entity work.common_pipeline
  generic map (
    g_pipeline   => c_pipeline,
    g_in_dat_w   => g_ram.dat_w,
    g_out_dat_w  => g_ram.dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => ram_rd_dat_b,
    out_dat => rd_dat_b
  );

  -- rd_val control
  ram_rd_en_a(0) <= rd_en_a;
  ram_rd_en_b(0) <= rd_en_b;

  rd_val_a <= ram_rd_val_a(0);
  rd_val_b <= ram_rd_val_b(0);

  u_rd_val_a : entity work.common_pipeline
  generic map (
    g_pipeline   => g_ram.latency,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => ram_rd_en_a,
    out_dat => ram_rd_val_a
  );

  u_rd_val_b : entity work.common_pipeline
  generic map (
    g_pipeline   => g_ram.latency,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => ram_rd_en_b,
    out_dat => ram_rd_val_b
  );
end str;
