-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Dual page register to pass on the sosi sop and eop related fields
-- Description:
--   The sync, bsn and channel sosi fields are valid at the sop.
--   The empty and err sosi fields are valid at the eop.
--   The sop related input sosi fields are shifted from page to page dependent
--   on sop_wr_en[]. Idem for the eop related input sosi fields with
--   eop_wr_en[].
-- Remarks:
-- . E.g. use g_nof_pages = 2 and
--     sop_wr_en <= snk_in.sop & snk_in.sop;
--     eop_wr_en <= snk_in.eop & snk_in.eop;
--   to capture the input at the first wr_en and hold it for output at the
--   next wr_en.
-- . These are related components that try to pass on sosi info from begin to
--   end, without having to pass it on through each step in the sosi data
--   processing.
--   - dp_paged_sop_eop_reg
--   - dp_fifo_info.vhd
--   - dp_block_gen_valid_arr

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_paged_sop_eop_reg is
  generic (
    g_nof_pages  : natural := 2  -- >= 0
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- page write enable ctrl
    sop_wr_en   : in  std_logic_vector(g_nof_pages - 1 downto 0);
    eop_wr_en   : in  std_logic_vector(g_nof_pages - 1 downto 0);
    -- ST sink
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_out     : out t_dp_sosi
  );
end dp_paged_sop_eop_reg;

architecture str of dp_paged_sop_eop_reg is
  signal snk_in_sync  : std_logic_vector(0 downto 0);
  signal src_out_sync : std_logic_vector(0 downto 0);
begin
  -- Sosi info at sop
  snk_in_sync(0) <= snk_in.sync;  -- convert sl to slv
  src_out.sync   <= src_out_sync(0);  -- convert slv to sl

  u_paged_sync : entity common_lib.common_paged_reg
  generic map (
    g_data_w    => 1,
    g_nof_pages => g_nof_pages
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_en        => sop_wr_en,
    wr_dat       => snk_in_sync,
    out_dat      => src_out_sync
  );

  u_paged_bsn : entity common_lib.common_paged_reg
  generic map (
    g_data_w    => c_dp_stream_bsn_w,
    g_nof_pages => g_nof_pages
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_en        => sop_wr_en,
    wr_dat       => snk_in.bsn,
    out_dat      => src_out.bsn
  );

  u_paged_channel : entity common_lib.common_paged_reg
  generic map (
    g_data_w    => c_dp_stream_channel_w,
    g_nof_pages => g_nof_pages
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_en        => sop_wr_en,
    wr_dat       => snk_in.channel,
    out_dat      => src_out.channel
  );

  -- Sosi info at eop
  u_paged_empty : entity common_lib.common_paged_reg
  generic map (
    g_data_w    => c_dp_stream_empty_w,
    g_nof_pages => g_nof_pages
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_en        => eop_wr_en,
    wr_dat       => snk_in.empty,
    out_dat      => src_out.empty
  );

  u_paged_err : entity common_lib.common_paged_reg
  generic map (
    g_data_w    => c_dp_stream_error_w,
    g_nof_pages => g_nof_pages
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_en        => eop_wr_en,
    wr_dat       => snk_in.err,
    out_dat      => src_out.err
  );
end str;
