-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- The dp_block_validate_bsn_at_sync.vhd checks whether the remote block
-- sequence number (BSN) at the start of an in_sosi.sync interval is equal to
-- the local BSN at start of the local bs_sosi.sync interval.
-- Description:
-- The dp_block_validate_bsn_at_sync.vhd holds the local BSN that it gets at
-- each bs_sosi.sync and uses that when it receives the remote BSN at the
-- in_sosi.sync. The dp_validate_bsn_at_sync.vhd compares the entire
-- g_bsn_w bit BSN. If the remote BSN at in_sosi.sync and the local BSN at
-- bs_sosi.sync are:
--   . Not equal, then discard all subsequent in_sosi blocks until the next
--     in_sosi .sync
--   . Equal, then pass on all subsequent in_sosi blocks until the next
--     in_sosi.sync
-- Only packets with channel = g_check_channel are checked. Other packets are
-- just passed on.
-- The dp_block_validate_bsn_at_sync.vhd maintains a total number of
-- in_sosi.sync counter and a number of discarded sync intervals counter, that
-- can be read via the MM interface.
-- Remarks:
--  . g_check_channel is needed for designs like the ring. The
--    dp_block_validate_bsn_at_sync.vhd only has to check the remote packets
--    from its neighbour ring node that is at one hop distance. The packets
--    that have traveled one hop have in_sosi.channel = g_check_channel = 1.
--    The packets at other channels from more distant PN are just passed on.
--    These packets have already been checked by the
--    dp_block_validate_bsn_at_sync.vhd on other PN. In this way it is
--    suffiicent to have one instance of dp_block_validate_bsn_at_sync.vhd per
--    PN.
--  . See, https://support.astron.nl/confluence/x/jyu7Ag
-------------------------------------------------------------------------------
-- REGMAP
-------------------------------------------------------------------------------
--  wi                  Bits    R/W Name                  Default
--  =====================================================================
--  0                   [31..0] RO  nof_sync_discarded      0x0
--  1                   [31..0] RO  nof_sync                0x0
--  2                   [31..0] RW  clear                   0x0 read or write to clear counters
--  =====================================================================
-------------------------------------------------------------------------------
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_block_validate_bsn_at_sync is
  generic (
    g_check_channel  : natural := 0;
    g_bsn_w          : natural := c_dp_stream_bsn_w
  );
  port (
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;
    -- ST sink
    in_sosi       : in  t_dp_sosi;
    bs_sosi       : in  t_dp_sosi;
    -- ST source
    out_sosi      : out t_dp_sosi;

    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;

    reg_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_miso     : out t_mem_miso := c_mem_miso_rst
  );
end dp_block_validate_bsn_at_sync;

architecture rtl of dp_block_validate_bsn_at_sync is
  constant c_nof_regs   : natural := 3;
  constant c_clear_adr  : natural := c_nof_regs - 1;
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => ceil_log2(c_nof_regs),
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => c_nof_regs,  -- total counter + discarded counter
                                  init_sl  => '0');

  -- Registers in st_clk domain
  signal count_reg        : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');

  signal mm_cnt_clr       : std_logic;
  signal cnt_clr          : std_logic;
  signal cnt_sync         : std_logic_vector(c_word_w - 1 downto 0);
  signal cnt_sync_en      : std_logic;
  signal cnt_discarded    : std_logic_vector(c_word_w - 1 downto 0);
  signal cnt_discarded_en : std_logic;

  signal out_valid        : std_logic;
  signal out_valid_reg    : std_logic;
  signal bsn_ok           : std_logic;
  signal bsn_ok_reg       : std_logic;
  signal bsn_at_sync      : std_logic_vector(g_bsn_w - 1 downto 0);
  signal bsn_at_sync_reg  : std_logic_vector(g_bsn_w - 1 downto 0);

  signal block_sosi       : t_dp_sosi;
begin
  mm_cnt_clr <= (reg_mosi.rd or reg_mosi.wr) when TO_UINT(reg_mosi.address(c_mm_reg.adr_w - 1 downto 0)) = c_clear_adr else '0';
  u_common_spulse : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_cnt_clr,
      out_rst   => dp_rst,
      out_clk   => dp_clk,
      out_pulse => cnt_clr
    );

  -- discarded counter
  u_discarded_counter : entity common_lib.common_counter
  generic map (
    g_width => c_word_w,
    g_clip  => true
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    cnt_clr => cnt_clr,
    cnt_en  => cnt_discarded_en,
    count   => cnt_discarded
  );

  -- sync counter
  u_blk_counter : entity common_lib.common_counter
  generic map (
    g_width => c_word_w,
    g_clip  => true
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    cnt_clr => cnt_clr,
    cnt_en  => in_sosi.sync,
    count   => cnt_sync
  );

  -- Register mapping
  count_reg(  c_word_w - 1 downto        0 ) <= cnt_discarded;
  count_reg(2 * c_word_w - 1 downto c_word_w ) <= cnt_sync;

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => dp_rst,
    st_clk      => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_mosi,
    sla_out     => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => OPEN,
    in_reg      => count_reg,  -- read only
    out_reg     => open  -- no write
  );

  -- Process to check the bsn at sync. It captures the bsn at the sync of bs_sosi. Then compares that bsn to
  -- the bsn at sync of in_sosi. If they are unequal all packets during that sync period with in_sosi.channel
  -- equal to g_check_channel are discarded.
  p_bsn_check : process(bs_sosi, in_sosi, bsn_at_sync_reg, bsn_ok_reg, out_valid_reg)
    -- v_bsn_ok used for the first in_sosi packet of a sync period as sync and sop are both '1'.
    variable v_bsn_ok      : std_logic;
    -- Using v_bsn_at_sync to ensure correct behavior when in_sosi has no latency compared to bs_sosi.
    variable v_bsn_at_sync : std_logic_vector(g_bsn_w - 1 downto 0);
  begin
    v_bsn_at_sync    := bsn_at_sync_reg;
    v_bsn_ok         := bsn_ok_reg;
    out_valid        <= out_valid_reg;
    cnt_discarded_en <= '0';

    if bs_sosi.sync = '1' then
      v_bsn_at_sync := bs_sosi.bsn(g_bsn_w - 1 downto 0);
    end if;

    if TO_UINT(in_sosi.channel) = g_check_channel then
      if in_sosi.sync = '1' then
        -- Compare bsn at sync of in_sosi to bsn at sync of bs_sosi.
        if unsigned(in_sosi.bsn(g_bsn_w - 1 downto 0)) = unsigned(v_bsn_at_sync) then
          v_bsn_ok := '1';
        else
          v_bsn_ok := '0';
        end if;

        -- set cnt_discarded_en to control u_discarded_counter.
        cnt_discarded_en <= not v_bsn_ok;
      end if;

      -- Setting out_valid to control the discarding at packet level.
      if in_sosi.sop = '1' then
        out_valid <= v_bsn_ok;
      end if;
    else
      if in_sosi.sop = '1' then
        out_valid <= '1';  -- Packets with channel unequal to g_check_channel are always valid
      end if;
    end if;
    bsn_ok      <= v_bsn_ok;  -- bsn_ok is used to indicate if the bsn is correct for the entire sync period of g_check_channel.
    bsn_at_sync <= v_bsn_at_sync;  -- register to store the bsn at sync of bs_sosi.
  end process;

  p_dp_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      bsn_ok_reg      <= '1';
      out_valid_reg   <= '1';
      bsn_at_sync_reg <= (others => '0');
    elsif rising_edge(dp_clk) then
      bsn_ok_reg      <= bsn_ok;
      out_valid_reg   <= out_valid;
      bsn_at_sync_reg <= bsn_at_sync;
    end if;
  end process;

  p_sosi : process(in_sosi, out_valid)
  begin
    block_sosi       <= in_sosi;
    block_sosi.valid <= in_sosi.valid and out_valid;
    block_sosi.sop   <= in_sosi.sop   and out_valid;
    block_sosi.eop   <= in_sosi.eop   and out_valid;
    block_sosi.sync  <= in_sosi.sync  and out_valid;
  end process;

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => block_sosi,
    -- ST source
    src_out      => out_sosi
  );
end rtl;
