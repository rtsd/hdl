-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Description:
-- . for IP frames replace IP header checksum with the calculated value
-- . discard frames shorter than c_network_total_header_32b_nof_words = 11 words
-- . frame discard input to optionally discard a frame based on some header criteria

entity eth_hdr_ctrl is
  port (
    -- Clocks and reset
    rst             : in  std_logic;
    clk             : in  std_logic;

    -- Stored header
    hdr_words_arr   : in  t_network_total_header_32b_arr;
    hdr_status      : in  t_eth_hdr_status;

    -- Frame control
    frm_discard     : in  std_logic := '0';
    frm_discard_val : in  std_logic := '1';

    -- ST interface
    snk_in_word_cnt : in  natural range 0 to c_network_total_header_32b_nof_words;  -- snk_in hdr word count
    snk_in          : in  t_dp_sosi;
    snk_out         : out t_dp_siso;
    src_in          : in  t_dp_siso;
    src_out         : out t_dp_sosi
  );
end eth_hdr_ctrl;

architecture rtl of eth_hdr_ctrl is
  -- Internal sink ready latency and source ready latency of this component
  constant c_this_snk_latency : natural := 1;
  constant c_this_src_latency : natural := 1;

  type t_state_enum is (
    s_idle,
    s_store_header,
    s_check_frm_discard,
    s_src_header_sop,
    s_src_header,
    s_src_data
  );

  signal state               : t_state_enum;
  signal nxt_state           : t_state_enum;

  -- Sink
  signal snk_in_eop_hold     : std_logic;
  signal nxt_snk_in_eop_hold : std_logic;

  -- Source
  signal next_src_out        : t_dp_sosi;
  signal i_src_out           : t_dp_sosi;
  signal nxt_src_out         : t_dp_sosi;
  signal src_word_cnt        : natural range 0 to c_network_total_header_32b_nof_words;
  signal nxt_src_word_cnt    : natural;
begin
  src_out <= i_src_out;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      state           <= s_idle;
      snk_in_eop_hold <= '0';
      src_word_cnt    <= 0;
      i_src_out       <= c_dp_sosi_rst;
    elsif rising_edge(clk) then
      state           <= nxt_state;
      snk_in_eop_hold <= nxt_snk_in_eop_hold;
      src_word_cnt    <= nxt_src_word_cnt;
      i_src_out       <= nxt_src_out;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Hold the sink input for source output
  ------------------------------------------------------------------------------

  u_snk_in : entity dp_lib.dp_hold_input
  port map (
    rst           => rst,
    clk           => clk,
    -- ST sink
    snk_out       => OPEN,
    snk_in        => snk_in,
    -- ST source
    src_in        => src_in,  -- must not use snk_out.ready here to avoid wrong first payload data
    next_src_out  => next_src_out,
    pend_src_out  => OPEN,
    src_out_reg   => i_src_out
  );

  ------------------------------------------------------------------------------
  -- Control state machine for the source output
  ------------------------------------------------------------------------------

  -- Hold snk_in.eop, because snk_in packet can only have a header of exactly 11 words (e.g. ARP).
  -- Discard snk_in packets that are shorter than 11 words (e.g. ???), for both Rx and Tx shorter frames are not useful.
  nxt_snk_in_eop_hold <= '0' when snk_in.sop = '1' else
                         '1' when snk_in.eop = '1' else snk_in_eop_hold;

  p_state : process(state, src_word_cnt, i_src_out, snk_in, snk_in_word_cnt, frm_discard, frm_discard_val,
                           src_in, hdr_words_arr, hdr_status, snk_in_eop_hold, next_src_out)
  begin
    snk_out           <= c_dp_siso_rdy;  -- default accept sink input
    nxt_state         <= state;
    nxt_src_word_cnt  <= src_word_cnt;
    nxt_src_out       <= i_src_out;
    nxt_src_out.valid <= '0';
    nxt_src_out.sop   <= '0';
    nxt_src_out.eop   <= '0';
    case state is
      when s_idle =>
        -- wait for new frame, then store the frame header and no output yet
        if snk_in.sop = '1' then
          nxt_src_word_cnt <= 0;
          nxt_state        <= s_store_header;
        end if;
      when s_store_header =>
        -- stop the sink when the whole header has arrived or when a eop has arrived
        if snk_in.valid = '1' and snk_in_word_cnt = c_network_total_header_32b_nof_words - 1 then
          snk_out.ready <= '0';
          nxt_state <= s_check_frm_discard;
        elsif snk_in.eop = '1' then
          -- discard not supported packets shorter than 11 words
          snk_out.ready <= '0';
          nxt_state <= s_idle;
        end if;
      when s_check_frm_discard =>
        -- stop the input and check whether the frame needs to be discarded
        snk_out.ready <= '0';
        if frm_discard_val = '1' then
          if frm_discard = '1' then
            -- discard this frame because of optional external reasons
            nxt_state <= s_idle;
          else
            nxt_state <= s_src_header_sop;
          end if;
        end if;
      when s_src_header_sop =>
        -- stop the input and output the (modified) header, first the sop
        snk_out.ready <= '0';
        if src_in.ready = '1' then
          nxt_src_out.data  <= RESIZE_DP_DATA(hdr_words_arr(src_word_cnt));
          nxt_src_out.valid <= '1';
          nxt_src_out.sop   <= '1';
          nxt_src_out.eop   <= '0';
          nxt_src_word_cnt  <= src_word_cnt + 1;
          nxt_state <= s_src_header;
        end if;
      when s_src_header =>
        -- stop the input and output the rest of the (modified) header
        snk_out.ready <= '0';
        if src_in.ready = '1' then
          nxt_src_out.data  <= RESIZE_DP_DATA(hdr_words_arr(src_word_cnt));
          nxt_src_out.valid <= '1';
          nxt_src_out.sop   <= '0';
          nxt_src_out.eop   <= '0';
          nxt_src_word_cnt  <= src_word_cnt + 1;
          -- Replace IP header checksum with the calculated checksum from hdr_status.ip_checksum:
          -- . for Rx: the verified checksum, so will be 0 when OK or != when the received frame has an IP header error.
          -- . for Tx: the calculated checksum for the Tx IP header.
          if hdr_status.is_ip = '1' and src_word_cnt = c_network_total_header_32b_ip_header_checksum_wi then
            nxt_src_out.data(c_network_ip_header_checksum_w - 1 downto 0) <= hdr_status.ip_checksum;
          end if;
          if src_word_cnt = c_network_total_header_32b_nof_words - 1 then
            if snk_in_eop_hold = '1' then
              -- header only packet
              nxt_src_out.eop <= '1';
              nxt_state <= s_idle;
            else
              -- packet with payload
              nxt_state <= s_src_data;
            end if;
          end if;
        end if;
      when others =>  -- s_src_data
        -- continue the input payload and output it
        snk_out.ready <= src_in.ready;
        nxt_src_out   <= next_src_out;
        if next_src_out.eop = '1' then
          nxt_state <= s_idle;
        end if;
    end case;

    -- Pass on frame level flow control
    snk_out.xon <= src_in.xon;
  end process;
end rtl;
