-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_components_pkg.all;

-- Purpose: Select M symbols from an input data stream with N symbols
-- Description:
--   The in_data is a concatenation of N=g_nof_input, that are each g_symbol_w
--   bits wide. The out_data is a concatenation of M=g_nof_output, that are
--   each g_symbol_w bits wide. The input symbol with index set by in_select m
--   is passed on to the output symbol m in out_data.
-- Remarks:
-- . If the in_select index is too large for g_nof_input range then the output
--   passes on symbol 0.
-- . This common_select_m_symbols is functionally equivalent to
--   common_reorder_symbol. The advantage of common_select_m_symbols is that
--   the definition of in_select is more intinutive. The advantage of
--   common_reorder_symbol may be that the structure of reorder2 cells with
--   minimal line crossings allow achieving a higher f_max.

entity common_select_m_symbols is
  generic (
    g_nof_input     : natural := 4;
    g_nof_output    : natural := 4;
    g_symbol_w      : natural := 16;
    g_pipeline_in   : natural := 0;  -- pipeline in_data
    g_pipeline_in_m : natural := 0;  -- pipeline in_data for M-fold fan out
    g_pipeline_out  : natural := 1  -- pipeline out_data
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_data    : in  std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
    in_val     : in  std_logic := '0';
    in_sop     : in  std_logic := '0';
    in_eop     : in  std_logic := '0';
    in_sync    : in  std_logic := '0';

    in_select  : in  std_logic_vector(g_nof_output * ceil_log2(g_nof_input) - 1 downto 0);

    out_data   : out std_logic_vector(g_nof_output * g_symbol_w - 1 downto 0);
    out_val    : out std_logic;  -- pipelined in_val
    out_sop    : out std_logic;  -- pipelined in_sop
    out_eop    : out std_logic;  -- pipelined in_eop
    out_sync   : out std_logic  -- pipelined in_sync
  );
end common_select_m_symbols;

architecture str of common_select_m_symbols is
  constant c_sel_w      : natural := ceil_log2(g_nof_input);

  type t_symbol_arr  is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);
  type t_select_arr  is array (integer range <>) of std_logic_vector(c_sel_w - 1 downto 0);

  signal in_data_arr    : t_symbol_arr(g_nof_input - 1 downto 0);
  signal in_data_reg    : std_logic_vector(in_data'range);
  signal in_val_reg     : std_logic;
  signal in_sop_reg     : std_logic;
  signal in_eop_reg     : std_logic;
  signal in_sync_reg    : std_logic;

  signal in_select_arr  : t_select_arr(g_nof_output - 1 downto 0);
  signal in_select_reg  : std_logic_vector(in_select'range);

  signal out_data_arr   : t_symbol_arr(g_nof_output - 1 downto 0);
  signal out_val_arr    : std_logic_vector(g_nof_output - 1 downto 0);
  signal out_sop_arr    : std_logic_vector(g_nof_output - 1 downto 0);
  signal out_eop_arr    : std_logic_vector(g_nof_output - 1 downto 0);
  signal out_sync_arr   : std_logic_vector(g_nof_output - 1 downto 0);
begin
  -- pipeline input
  u_pipe_in_data   : common_pipeline generic map ("SIGNED", g_pipeline_in, 0, in_data'LENGTH,   in_data'length)   port map (rst, clk, '1', '0', '1', in_data,   in_data_reg);
  u_pipe_in_select : common_pipeline generic map ("SIGNED", g_pipeline_in, 0, in_select'LENGTH, in_select'length) port map (rst, clk, '1', '0', '1', in_select, in_select_reg);

  u_pipe_in_val    : common_pipeline_sl generic map (g_pipeline_in, 0, false) port map (rst, clk, '1', '0', '1', in_val,  in_val_reg);
  u_pipe_in_sop    : common_pipeline_sl generic map (g_pipeline_in, 0, false) port map (rst, clk, '1', '0', '1', in_sop,  in_sop_reg);
  u_pipe_in_eop    : common_pipeline_sl generic map (g_pipeline_in, 0, false) port map (rst, clk, '1', '0', '1', in_eop,  in_eop_reg);
  u_pipe_in_sync   : common_pipeline_sl generic map (g_pipeline_in, 0, false) port map (rst, clk, '1', '0', '1', in_sync, in_sync_reg);

  -- Ease viewing the in_data in the Wave window by mapping it on in_data_arr
  gen_n : for I in 0 to g_nof_input - 1 generate
    in_data_arr(I) <= in_data_reg((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);
  end generate;

  gen_m : for I in 0 to g_nof_output - 1 generate
    in_select_arr(I) <= in_select_reg((I + 1) * c_sel_w - 1 downto I * c_sel_w);

    u_sel : entity work.common_select_symbol
    generic map (
      g_pipeline_in  => g_pipeline_in_m,
      g_pipeline_out => g_pipeline_out,
      g_nof_symbols  => g_nof_input,
      g_symbol_w     => g_symbol_w,
      g_sel_w        => c_sel_w
    )
    port map (
      rst        => rst,
      clk        => clk,

      in_data    => in_data_reg,
      in_val     => in_val_reg,
      in_sop     => in_sop_reg,
      in_eop     => in_eop_reg,
      in_sync    => in_sync_reg,

      in_sel     => in_select_arr(I),
      out_sel    => OPEN,

      out_symbol => out_data_arr(I),
      out_val    => out_val_arr(I),  -- pipelined in_val
      out_sop    => out_sop_arr(I),  -- pipelined in_sop
      out_eop    => out_eop_arr(I),  -- pipelined in_eop
      out_sync   => out_sync_arr(I)  -- pipelined in_sync
    );

    out_data((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) <= out_data_arr(I);
  end generate;

  -- Use instance I=0 to pipeline the control
  out_val  <= out_val_arr(0);
  out_sop  <= out_sop_arr(0);
  out_eop  <= out_eop_arr(0);
  out_sync <= out_sync_arr(0);
end str;
