-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Pipeline the source output by one cycle or by g_pipeline cycles.
-- Description:
--   The dp_pipeline instantiates 0:g_pipeline stages of dp_pipeline_one.
--   The dp_pipeline_one provides a single clock cycle delay of the source
--   output (i.e. sosi). The dp_pipeline_one holds valid sink input in case
--   src_in.ready goes low and makes src_out.valid high again when
--   src_in.ready goes high again, without the need for a valid sink input to
--   push this held data out.
--   The dp_pipeline delays the data, sop, eop by one cycle relative to the
--   valid. However the src_out.valid still has the same phase as the
--   snk_in.valid, because both valids depends on the same src_in.ready.
--   Therefore dp_pipeline cannot be used to delay the valid phase by one
--   cycle. Hence the may purpose of dp_pipeline is to register the sosi.
-- Remarks:
-- . Ready latency = 1
-- . Without flow control so when src_in.ready = '1' fixed, then the hold
--   logic in dp_pipeline becomes void and dp_pipeline then just pipelines the
--   snk_in sosi.

entity dp_pipeline is
  generic (
    g_pipeline   : natural := 1  -- 0 for wires, > 0 for registers,
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_pipeline;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

entity dp_pipeline_one is
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_pipeline_one;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

architecture str of dp_pipeline is
  signal snk_out_arr      : t_dp_siso_arr(0 to g_pipeline);
  signal snk_in_arr       : t_dp_sosi_arr(0 to g_pipeline);
begin
  -- Input at index 0
  snk_out       <= snk_out_arr(0);
  snk_in_arr(0) <= snk_in;

  -- Output at index g_pipeline
  snk_out_arr(g_pipeline) <= src_in;
  src_out                 <= snk_in_arr(g_pipeline);

  gen_p : for I in 1 to g_pipeline generate
    u_p : entity work.dp_pipeline_one
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => snk_out_arr(I - 1),
      snk_in       => snk_in_arr(I - 1),
      -- ST source
      src_in       => snk_out_arr(I),
      src_out      => snk_in_arr(I)
    );
  end generate;
end str;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

architecture str of dp_pipeline_one is
  signal nxt_src_out      : t_dp_sosi;
  signal i_src_out        : t_dp_sosi;
begin
  src_out <= i_src_out;

  -- Pipeline register
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_src_out <= c_dp_sosi_rst;
    elsif rising_edge(clk) then
      i_src_out <= nxt_src_out;
    end if;
  end process;

  -- Input control
  u_hold_input : entity work.dp_hold_input
  port map (
    rst              => rst,
    clk              => clk,
    -- ST sink
    snk_out          => snk_out,
    snk_in           => snk_in,
    -- ST source
    src_in           => src_in,
    next_src_out     => nxt_src_out,
    pend_src_out     => OPEN,
    src_out_reg      => i_src_out
  );
end str;
