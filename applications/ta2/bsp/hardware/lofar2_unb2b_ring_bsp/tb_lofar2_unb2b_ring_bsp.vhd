-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2b_ring_bsp using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--        freq = 19.921875MHz
--        ampl = 0.5 * 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Read subband statistics (SST)
--
--   4) Read beamlet statistics (BST) via ram_st_bst and verify with
--      c_exp_beamlet_power_sp_0 at c_sdp_N_sub-1 - c_subband_sp_0.
--      View sp_beamlet_power_0  in Wave window
--   5) Compare SST with BST.
--   6) Verify 10GbE output.
--
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, tech_pll_lib, tr_10GbE_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_lofar2_unb2b_ring_bsp is
end tb_lofar2_unb2b_ring_bsp;

architecture tb of tb_lofar2_unb2b_ring_bsp is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz
  constant c_pps_period          : natural := 1000;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C
  constant c_cable_delay         : time := 12 ns;

  constant c_nof_block_per_sync  : natural := 16;

  -- MM
  constant c_mm_file_reg_sdp_info             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_SDP_INFO";
  constant c_mm_file_reg_dp_xonoff_bg         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_XONOFF_BG";
  constant c_mm_file_reg_dp_xonoff_from_lane  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_XONOFF_FROM_LANE";
  constant c_mm_file_reg_bsn_monitor_rx       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_MONITOR_V2_RX";
  constant c_mm_file_reg_bsn_monitor_tx       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_MONITOR_V2_TX";
  constant c_mm_file_reg_bg_ctrl              : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DIAG_BG_RING";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  -- 10GbE
  signal tr_10GbE_src_out       : t_dp_sosi;
  signal tr_ref_clk_312         : std_logic := '0';
  signal tr_ref_clk_156         : std_logic := '0';
  signal tr_ref_rst_156         : std_logic := '0';

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  signal SA_CLK              : std_logic := '1';
  signal si_lpbk_0           : std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_1           : std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_2           : std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)
  pps_rst <= '0' after c_ext_clk_period * 2;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, ext_clk, pps);
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2b_ring_bsp : entity work.top
  generic map (
    g_design_name            => "lofar2_unb2b_ring_bsp",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,
    -- front transceivers
    QSFP_0_RX    => si_lpbk_0,
    QSFP_0_TX    => si_lpbk_0,
    -- ring transceivers
    RING_0_RX    => si_lpbk_2,
    RING_0_TX    => si_lpbk_1,
    RING_1_RX    => si_lpbk_1,
    RING_1_TX    => si_lpbk_2,

    -- LEDs
    QSFP_LED     => open

  );

    u_unb2_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    port map (
      refclk_644 => SA_CLK,
      rst_in     => pps_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );

    u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => true,
      g_sim_level     => 1,
      g_nof_macs      => 1,
      g_use_mdio      => false
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644      => SA_CLK,
      tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
      tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
      tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

      -- MM interface
      mm_rst              => pps_rst,
      mm_clk              => tb_clk,

      -- DP interface
      dp_rst              => pps_rst,
      dp_clk              => ext_clk,

      serial_rx_arr(0)    => si_lpbk_0(0),

      src_out_arr(0)      => tr_10GbE_src_out

    );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                   : natural;
    variable v_sp_power_sum_0        : real;
    variable v_sp_beamlet_power      : real;
    variable v_sp_subband_power      : real;
    variable v_W, v_T, v_U, v_S, v_B : natural;  -- array indicies
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    ----------------------------------------------------------------------------
    -- Enable UDP offload (dp_xonoff) of beamset 0
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff_bg, 0,  1, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff_from_lane, 0,  0, tb_clk);

    ----------------------------------------------------------------------------
    -- Enable BG
    ----------------------------------------------------------------------------
    --  0: enable[1:0]           --> off=0, enable=1, enable_pps=3
    --  1: samples_per_packet[15:0]
    --  2: Blocks_per_sync[15:0]
    --  3: Gapsize[15:0]
    --  4: Mem_low_adrs[7:0]
    --  5: Mem_high_adrs[7:0]
    --  6: BSN_init[31:0]
    --  7: BSN_init[63:32]

    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 0, 0,                    tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 1, 750,                  tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 2, c_nof_block_per_sync, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 3, 250,                  tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 4, 0,                    tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 5, 127,                  tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 6, 0,                    tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 7, 0,                    tb_clk);

    mmf_mm_bus_wr(c_mm_file_reg_bg_ctrl, 0, 3,                    tb_clk);
    proc_common_wait_some_cycles(ext_clk, 2 * c_nof_block_per_sync * 1000);

    ---------------------------------------------------------------------------
    -- Read TX monitor
    ---------------------------------------------------------------------------
    for I in 0 to 8 * 128 loop
      mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_tx, I, rd_data, tb_clk);
    end loop;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
