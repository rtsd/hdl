-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib, eth_lib, tech_tse_lib, bf_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use bf_lib.all;
use bf_lib.bf_pkg.all;

entity node_unb1_fn_bf is
  generic(
    g_sim                   : boolean  := false;
    g_use_bf                : boolean  := true;  -- FALSE skips instantiation of the BF
    g_bf                    : t_c_bf   := c_bf;
    g_bf_weights_file_name  : string   := "../../../../../dsp/bf/build/data/weights";  -- default file location for synthesis
    g_ss_wide_file_prefix   : string   := "UNUSED";  -- path_to_file
    g_bf_offload            : boolean  := false;  -- Use DP TX offload to stream BF output towards LCU
    g_use_block_gen         : boolean  := true;  -- FALSE uses external input.
    g_block_gen_file_prefix : string   := "../../../../../modules/Lofar/diag/src/data/bf_in_data";  -- default file location for synthesis
    g_bg_diag_wave_period   : positive := 4;  -- This generic defines the period of the waveform that is generated with the block generator.
    g_weights_write_only    : boolean  := false  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode. When FALSE it is True Dual Port.
  );
  port(
    -- System
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;  -- 125 MHz from xo_clk PLL in SOPC system
    dp_rst                  : in  std_logic;
    dp_clk                  : in  std_logic;  -- 200 MHz from CLK system clock
    dp_pps                  : in  std_logic := '1';  -- Pulse per second
    -- MM interface
    -- . block generator
    reg_diag_bg_mosi        : in  t_mem_mosi;
    reg_diag_bg_miso        : out t_mem_miso;
    ram_diag_bg_mosi        : in  t_mem_mosi;
    ram_diag_bg_miso        : out t_mem_miso;
    -- . beam former
    ram_ss_ss_wide_mosi     : in  t_mem_mosi;
    ram_ss_ss_wide_miso     : out t_mem_miso := c_mem_miso_rst;
    ram_bf_weights_mosi     : in  t_mem_mosi;
    ram_bf_weights_miso     : out t_mem_miso;
    ram_st_sst_bf_mosi      : in  t_mem_mosi;
    ram_st_sst_bf_miso      : out t_mem_miso;
    reg_st_sst_bf_mosi      : in  t_mem_mosi;
    reg_st_sst_bf_miso      : out t_mem_miso;
    -- . hdr_insert and hdr_remove for bf_out_offload
    reg_hdr_insert_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    ram_hdr_insert_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    -- . Nof words to offload selection
    reg_dp_split_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_split_miso       : out t_mem_miso;
    reg_dp_pkt_merge_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_pkt_merge_miso   : out t_mem_miso;

    -- ST interface, BF subbands in
    ext_in_sosi_arr         : in  t_dp_sosi_arr( g_bf.nof_input_streams - 1 downto 0) := (others => c_dp_sosi_rst);
    ext_in_siso_arr         : out t_dp_siso_arr( g_bf.nof_input_streams - 1 downto 0) := (others => c_dp_siso_rst);

    -- ST interface, BF beamlets out
    out_raw_sosi_arr        : out t_dp_sosi_arr( g_bf.nof_bf_units - 1 downto 0);  -- raw beamlets
    out_bst_sosi_arr        : out t_dp_sosi_arr( g_bf.nof_bf_units - 1 downto 0);  -- 16b beamlets; a selection can be offloaded via bf_out_offload_tx_sosi_arr.
    out_qua_sosi_arr        : out t_dp_sosi_arr( g_bf.nof_bf_units - 1 downto 0);  -- 8b beamlets

    -- DP offload for 1GbE
    bf_out_offload_tx_sosi_arr : out t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);  -- 16b beamlets
    bf_out_offload_tx_siso_arr : in  t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rst)
  );
end node_unb1_fn_bf;

architecture str of node_unb1_fn_bf is
  -----------------------------------------------------------------------------
  -- Block generator
  -----------------------------------------------------------------------------
  constant c_bg_block_size          : natural := 96;  -- 24 subbands * 4 signals

  constant c_bg_mem_high_addr       : natural := c_bg_block_size-1;
  constant c_bg_gapsize             : natural := 256 - c_bg_block_size;  -- 256 = block period used in terminals
  constant c_bg_blocks_per_sync     : natural := 32;  -- Used for sim
  constant c_bg_ctrl                : t_diag_block_gen := (sel_a_b(g_sim, '1', '0'),  -- enable
                                                           sel_a_b(g_sim, '0', '0'),  -- enable_sync
                                                          TO_UVEC(     c_bg_block_size, c_diag_bg_samples_per_packet_w),
                                                          TO_UVEC(c_bg_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                          TO_UVEC(        c_bg_gapsize, c_diag_bg_gapsize_w),
                                                          TO_UVEC(                   0, c_diag_bg_mem_low_adrs_w),
                                                          TO_UVEC(  c_bg_mem_high_addr, c_diag_bg_mem_high_adrs_w),
                                                          TO_UVEC(                   0, c_diag_bg_bsn_init_w));

  signal bf_in_sosi_arr     : t_dp_sosi_arr( g_bf.nof_input_streams - 1 downto 0);
  signal bg_out_sosi_arr    : t_dp_sosi_arr( g_bf.nof_input_streams - 1 downto 0);

  signal i_out_bst_sosi_arr : t_dp_sosi_arr(g_bf.nof_bf_units - 1 downto 0);
begin
  out_bst_sosi_arr <= i_out_bst_sosi_arr;

  ---------------------------------------------------------------------------------------
  -- Use Block Generator input by default
  ---------------------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_use_usr_input      => true,
    g_use_bg             => g_use_block_gen,
    g_nof_streams        => g_bf.nof_input_streams,  -- 16
    g_use_bg_buffer_ram  => true,
    g_buf_dat_w          => c_nof_complex * g_bf.in_dat_w,  -- 2*16b=32b
    g_buf_addr_w         => ceil_log2(c_bg_block_size),
    g_file_name_prefix   => g_block_gen_file_prefix,
    g_diag_block_gen_rst => c_bg_ctrl,
    g_usr_bypass_xonoff  => true
  )
  port map(
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    en_sync          => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => ram_diag_bg_miso,
    -- ST interface
    usr_siso_arr     => OPEN,
    usr_sosi_arr     => ext_in_sosi_arr,
    out_siso_arr     => (others => c_dp_siso_rdy),
    out_sosi_arr     => bg_out_sosi_arr
  );

  bf_in_sosi_arr <= bg_out_sosi_arr;

  gen_no_bf : if g_use_bf = false generate
    out_qua_sosi_arr <= bf_in_sosi_arr(g_bf.nof_bf_units - 1 downto 0);
  end generate;

  ---------------------------------------------------------------------------------------
  -- Beam Former
  ---------------------------------------------------------------------------------------
  gen_bf : if g_use_bf = true generate
    u_bf : entity bf_lib.bf
    generic map (
      g_bf                    => g_bf,
      g_bf_weights_file_name  => g_bf_weights_file_name,
      g_ss_wide_file_prefix   => g_ss_wide_file_prefix,
      g_weights_write_only    => g_weights_write_only
    )
    port map (
      -- System
      dp_rst                  =>  dp_rst,
      dp_clk                  =>  dp_clk,
      mm_rst                  =>  mm_rst,
      mm_clk                  =>  mm_clk,

      -- MM interface
      ram_ss_ss_wide_mosi     => ram_ss_ss_wide_mosi,
      ram_ss_ss_wide_miso     => ram_ss_ss_wide_miso,
      ram_bf_weights_mosi     => ram_bf_weights_mosi,
      ram_bf_weights_miso     => ram_bf_weights_miso,
      ram_st_sst_mosi         => ram_st_sst_bf_mosi,
      ram_st_sst_miso         => ram_st_sst_bf_miso,
      reg_st_sst_mosi         => reg_st_sst_bf_mosi,
      reg_st_sst_miso         => reg_st_sst_bf_miso,

      -- ST interface
      in_sosi_arr             => bf_in_sosi_arr,
      in_siso_arr             => ext_in_siso_arr,

      out_raw_sosi_arr        => out_raw_sosi_arr,  -- raw beamlets
      out_bst_sosi_arr        => i_out_bst_sosi_arr,  -- 16b beamlets
      out_qua_sosi_arr        => out_qua_sosi_arr  -- 8b beamlets
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- Offload 16b beamlets from out_bst_sosi_arr to udp_offload TX port in ctrl_unb_common
  ---------------------------------------------------------------------------------------
  gen_bf_offload : if g_bf_offload = true and g_sim = false generate
    u_dp_offload : entity dp_lib.dp_offload_tx_legacy
    generic map (
      g_nof_streams         => g_bf.nof_bf_units,
      g_data_w              => c_eth_data_w,
      g_block_size          => g_bf.nof_weights,  -- = 256
      g_block_nof_sel_words => 20,
      g_nof_words_per_pkt   => 360,
      g_hdr_nof_words       => c_network_total_header_32b_nof_words,
      g_use_complex         => true,
      g_use_input_fifo      => true,
      g_use_output_fifo     => true
    )
    port map (
      mm_rst                => mm_rst,
      mm_clk                => mm_clk,

      st_rst                => dp_rst,
      st_clk                => dp_clk,

      reg_hdr_insert_mosi   => reg_hdr_insert_mosi,
      ram_hdr_insert_mosi   => ram_hdr_insert_mosi,
      reg_dp_split_mosi     => reg_dp_split_mosi,
      reg_dp_split_miso     => reg_dp_split_miso,
      reg_dp_pkt_merge_mosi => reg_dp_pkt_merge_mosi,
      reg_dp_pkt_merge_miso => reg_dp_pkt_merge_miso,

      dp_sosi_arr           => i_out_bst_sosi_arr,
      dp_siso_arr           => OPEN,  -- No flow control, so we're instantiating an input FIFO.

      tx_sosi_arr           => bf_out_offload_tx_sosi_arr(g_bf.nof_bf_units - 1 downto 0),
      tx_siso_arr           => bf_out_offload_tx_siso_arr(g_bf.nof_bf_units - 1 downto 0)
    );
  end generate;
end str;
