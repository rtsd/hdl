-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity i2c_list_ctrl is
  generic (
    g_protocol_adr_w : natural := 10;
    g_result_adr_w   : natural := 10
  );
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    activate         : in  std_logic;
    busy             : out std_logic;
    protocol_rd_en   : out std_logic;
    protocol_rd_adr  : out std_logic_vector(g_protocol_adr_w - 1 downto 0);
    protocol_rd_ofs  : in  natural := 0;
    protocol_rd_dat  : in  std_logic_vector(c_byte_w - 1 downto 0);
    protocol_rd_val  : in  std_logic;
    result_wr_en     : out std_logic;
    result_wr_adr    : out std_logic_vector(g_result_adr_w - 1 downto 0);
    result_wr_dat    : out std_logic_vector(c_byte_w - 1 downto 0);
    smbus_out_dat    : out std_logic_vector(c_byte_w - 1 downto 0);
    smbus_out_req    : out std_logic;
    smbus_in_dat     : in  std_logic_vector(c_byte_w - 1 downto 0);
    smbus_in_val     : in  std_logic;
    smbus_in_err     : in  std_logic;
    smbus_in_ack     : in  std_logic;
    smbus_st_idle    : in  std_logic;
    smbus_st_end     : in  std_logic
  );
end entity;

architecture rtl of i2c_list_ctrl is
  signal i_busy               : std_logic;
  signal nxt_busy             : std_logic;
  signal nxt_protocol_rd_en   : std_logic;
  signal i_protocol_rd_adr    : std_logic_vector(g_protocol_adr_w - 1 downto 0);
  signal nxt_protocol_rd_adr  : std_logic_vector(g_protocol_adr_w - 1 downto 0);
  signal i_result_wr_en       : std_logic;
  signal nxt_result_wr_en     : std_logic;
  signal i_result_wr_adr      : std_logic_vector(g_result_adr_w - 1 downto 0);
  signal nxt_result_wr_adr    : std_logic_vector(g_result_adr_w - 1 downto 0);
  signal nxt_result_wr_dat    : std_logic_vector(c_byte_w - 1 downto 0);
  signal i_smbus_in_err       : std_logic_vector(0 downto 0);
  signal prev_smbus_st_idle   : std_logic;
  signal prot_done            : std_logic;
  signal pend_st_end          : std_logic;
  signal nxt_pend_st_end      : std_logic;
  signal list_end             : std_logic;
  signal prev_list_end        : std_logic;
  signal i_smbus_out_req      : std_logic;
  signal nxt_smbus_out_req    : std_logic;
begin
  protocol_rd_adr   <= i_protocol_rd_adr;

  result_wr_en      <= i_result_wr_en;
  result_wr_adr     <= i_result_wr_adr;

  i_smbus_in_err(0) <= smbus_in_err;

  smbus_out_req     <= i_smbus_out_req;
  smbus_out_dat     <= protocol_rd_dat;

  busy              <= i_busy;
  prot_done         <= not prev_smbus_st_idle and smbus_st_idle;
  list_end          <= smbus_st_end or pend_st_end;

  regs: process(rst, clk)
  begin
    if rst = '1' then
      i_busy             <= '0';
      pend_st_end        <= '0';
      protocol_rd_en     <= '0';
      i_protocol_rd_adr  <= (others => '0');
      i_result_wr_en     <= '0';
      i_result_wr_adr    <= (others => '0');
      result_wr_dat      <= (others => '0');
      prev_smbus_st_idle <= '1';
      prev_list_end      <= '0';
      i_smbus_out_req    <= '0';
    elsif rising_edge(clk) then
      i_busy             <= nxt_busy;
      pend_st_end        <= nxt_pend_st_end;
      protocol_rd_en     <= nxt_protocol_rd_en;
      i_protocol_rd_adr  <= nxt_protocol_rd_adr;
      i_result_wr_en     <= nxt_result_wr_en;
      i_result_wr_adr    <= nxt_result_wr_adr;
      result_wr_dat      <= nxt_result_wr_dat;
      prev_smbus_st_idle <= smbus_st_idle;
      prev_list_end      <= list_end;
      i_smbus_out_req    <= nxt_smbus_out_req;
    end if;
  end process;

  do_protocol_list : process(activate, i_busy, prev_list_end, list_end, smbus_in_ack, i_protocol_rd_adr, protocol_rd_ofs)
  begin
    nxt_busy <= i_busy;
    if activate = '1' then
      nxt_busy <= '1';
    elsif prev_list_end = '1' and list_end = '0' then
      nxt_busy <= '0';
    end if;

    nxt_protocol_rd_en  <= '0';
    nxt_protocol_rd_adr <= i_protocol_rd_adr;
    if activate = '1' then
      nxt_protocol_rd_en  <= '1';
      nxt_protocol_rd_adr <= TO_UVEC(protocol_rd_ofs, g_protocol_adr_w);
    elsif smbus_in_ack = '1' and list_end = '0' then
      nxt_protocol_rd_en  <= '1';
      nxt_protocol_rd_adr <= std_logic_vector(unsigned(i_protocol_rd_adr) + 1);
    end if;
  end process;

  smbus_request : process(i_smbus_out_req, protocol_rd_val, list_end, smbus_in_ack)
  begin
    nxt_smbus_out_req <= i_smbus_out_req;
    if protocol_rd_val = '1' and list_end = '0' then
      nxt_smbus_out_req <= '1';
    elsif smbus_in_ack = '1' then
      nxt_smbus_out_req <= '0';
    end if;
  end process;
  end_of_protocol_list: process(pend_st_end, prot_done, smbus_st_end)
  begin
    nxt_pend_st_end <= pend_st_end;
    if prot_done = '1' then
      nxt_pend_st_end <= '0';
    elsif smbus_st_end = '1' then
      nxt_pend_st_end <= '1';
    end if;
  end process;

  save_results : process(activate, i_result_wr_adr, i_result_wr_en, prot_done, smbus_in_val, i_smbus_in_err, smbus_in_dat)
  begin
    nxt_result_wr_adr <= i_result_wr_adr;
    if activate = '1' then
      nxt_result_wr_adr  <= (others => '0');
    elsif i_result_wr_en = '1' then
      nxt_result_wr_adr <= std_logic_vector(unsigned(i_result_wr_adr) + 1);
    end if;

    nxt_result_wr_en  <= '0';
    nxt_result_wr_dat <= (others => '0');
    if prot_done = '1' then
      nxt_result_wr_en   <= '1';
      nxt_result_wr_dat  <= std_logic_vector(resize(unsigned(i_smbus_in_err), c_byte_w));
    elsif smbus_in_val = '1' then
      nxt_result_wr_en   <= '1';
      nxt_result_wr_dat  <= smbus_in_dat;
    end if;
  end process;
end rtl;
