-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Test bench for mm_bus.vhd
-- Remark:
-- . This test bench covers:
--   . g_nof_slaves >= 1
--   . g_waitrequest for g_pipeline_miso_wait = FALSE
--   . g_pipeline_mosi
--   . g_pipeline_miso_rdval
--   . g_pipeline_miso_wait = FALSE
--   . g_rd_latency >= 1 (using 0 is supported by mm_bus, but not by
--     the common_ram_r_w in u_slaves)
--   . same g_rd_latency for all slaves
--   . same g_width for all slaves
--   . regular base address spacing of slaves in c_base_arr
-- . The mm_bus.vhd can support a list of arbitrary width slaves, but
--   this tb_mm_bus test bench uses an array of fixed width slaves.
--   It is considered sufficient coverage for this tb and the corresponding
--   multi tb_tb to also only support regular c_base_arr, same g_rd_latency,
--   and same g_width for all slaves. The tb_mm_master_mux also uses a
--   mm_bus.vhd and the tb_mm_master_mux does uses an array of
--   arbitrary width slaves.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

entity tb_mm_bus is
 generic (
    g_nof_slaves          : positive := 1;  -- Number of slave memory interfaces on the MM bus array.
    g_base_offset         : natural := 0;  -- Address of first slave on the MM bus
    g_width_w             : positive := 4;  -- Address width of each slave memory in the MM bus array.
    g_rd_latency          : natural := 1;  -- Read latency of the slaves
    g_waitrequest         : boolean := false;  -- When TRUE model waitrequest by MM slaves, else fixed '0'
    g_pipeline_mosi       : boolean := false;
    g_pipeline_miso_rdval : boolean := false;
    g_pipeline_miso_wait  : boolean := false
  );
end tb_mm_bus;

-- Usage:
--   > as 10
--   > run -all

architecture tb of tb_mm_bus is
  constant mm_clk_period   : time    := 10 ns;

  constant c_repeat          : natural := 10;  -- sel_a_b(g_waitrequest, 10, 2);  -- repeat 2 for deterministic, more often for random
  constant c_slave_span      : natural := 2**g_width_w;
  constant c_base_arr        : t_nat_natural_arr := array_init(g_base_offset, g_nof_slaves, c_slave_span);  -- Address base per slave
  constant c_width_arr       : t_nat_natural_arr := array_init(    g_width_w, g_nof_slaves);  -- Address width per slave
  constant c_rd_latency_arr  : t_nat_natural_arr := array_init( g_rd_latency, g_nof_slaves);  -- Read latency per slave
  constant c_slave_enable_arr: t_nat_boolean_arr := array_init(         true, g_nof_slaves);  -- TRUE for connected slaves
  constant c_waitrequest_arr : t_nat_boolean_arr := array_init(g_waitrequest, g_nof_slaves);  -- Flow control per slave

  constant c_bus_pipelining      : boolean := g_pipeline_mosi or g_pipeline_miso_rdval or g_pipeline_miso_wait;
  constant c_pipeline_mosi       : natural := sel_a_b(g_pipeline_mosi, 1, 0);
  constant c_pipeline_miso_rdval : natural := sel_a_b(g_pipeline_miso_rdval, 1, 0);
  constant c_pipeline_miso_wait  : natural := sel_a_b(g_pipeline_miso_wait, 1, 0);
  constant c_read_latency        : natural := c_pipeline_mosi + g_rd_latency + c_pipeline_miso_rdval;

  constant c_data_w     : natural := 32;
  constant c_test_ram   : t_c_mem := (latency  => g_rd_latency,
                                      adr_w    => g_width_w,
                                      dat_w    => c_data_w,
                                      nof_dat  => c_slave_span,
                                      init_sl  => '0');
  signal mm_rst   : std_logic;
  signal mm_clk   : std_logic := '1';
  signal tb_end   : std_logic;

  signal cnt_rd    : natural := 0;
  signal cnt_rdval : natural := 0;

  -- MM bus
  signal master_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal master_miso      : t_mem_miso := c_mem_miso_rst;
  signal slave_mosi_arr   : t_mem_mosi_arr(0 to g_nof_slaves - 1) := (others => c_mem_mosi_rst);
  signal slave_miso_arr   : t_mem_miso_arr(0 to g_nof_slaves - 1) := (others => c_mem_miso_rst);
  signal ram_mosi_arr     : t_mem_mosi_arr(0 to g_nof_slaves - 1) := (others => c_mem_mosi_rst);
  signal ram_miso_arr     : t_mem_miso_arr(0 to g_nof_slaves - 1) := (others => c_mem_miso_rst);

  -- Debug signals for monitoring in simulation Wave window
  signal dbg_c_base_arr        : t_nat_natural_arr(0 to g_nof_slaves - 1) := c_base_arr;
  signal dbg_c_width_arr       : t_nat_natural_arr(0 to g_nof_slaves - 1) := c_width_arr;
  signal dbg_c_rd_latency_arr  : t_nat_natural_arr(0 to g_nof_slaves - 1) := c_rd_latency_arr;
begin
  mm_clk <= not mm_clk or tb_end after mm_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 5;

  -----------------------------------------------------------------------------
  -- Write stimuli and readback to verify
  -----------------------------------------------------------------------------
  p_stimuli : process
    variable v_wrdata  : integer;  -- write data
  begin
    tb_end <= '0';
    master_mosi <= c_mem_mosi_rst;

    -- Wait until reset is released
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Repeat twice to have wr all, rd all, wr all, rd all
    v_wrdata := 0;
    for vR in 0 to c_repeat - 1 loop
      -- Write the whole memory range
      for vI in 0 to g_nof_slaves - 1 loop
        for vJ in 0 to c_slave_span - 1 loop
          proc_mem_mm_bus_wr(g_base_offset + vI * c_slave_span + vJ, v_wrdata, mm_clk, master_miso, master_mosi);
          v_wrdata := v_wrdata + 1;
        end loop;
        proc_common_wait_some_cycles(mm_clk, 10);
      end loop;

      -- Read back the whole range and check if data is as expected
      for vI in 0 to g_nof_slaves - 1 loop
        for vJ in 0 to c_slave_span - 1 loop
          proc_mem_mm_bus_rd(g_base_offset + vI * c_slave_span + vJ, mm_clk, master_miso, master_mosi);
          --proc_common_wait_some_cycles(mm_clk, c_read_latency);  -- not needed, see p_verify
          cnt_rd <= cnt_rd + 1;
        end loop;
        proc_common_wait_some_cycles(mm_clk, 10);
      end loop;
    end loop;

    proc_common_wait_some_cycles(mm_clk, 10);

    -- Verify that test has indeed ran
    wait for 1 ns;  -- wait 1 ns to ensure that assert report appears at end of transcript log
    assert cnt_rdval = cnt_rd and cnt_rdval > 0
      report "Wrong number of rdval"
      severity ERROR;

    tb_end <= '1';
    wait;
  end process;

  -- Use miso.rdval to know when to verify the rddata, rather than to wait for a fixed c_read_latency after
  -- the mosi.rd. The advantage is that then rd accesses can be done on every mm_clk, without having to
  -- wait for the c_read_latency. In case of g_pipeline_mosi = TRUE or g_pipeline_miso_wait = TRUE it is
  -- even essential to use rdval, because then the latency between rd and rdval can become larger than
  -- c_read_latency and even variable (in case of g_waitrequest = TRUE). The disadvantage is that the MM
  -- slave must support rdval, but that is ensured by mm_slave_enable.
  p_verify : process
    variable v_expdata : integer := 0;  -- expected data
    variable v_rddata  : integer;  -- read data
  begin
    wait until rising_edge(mm_clk);
    if master_miso.rdval = '1' then
      cnt_rdval <= cnt_rdval + 1;
      v_rddata := TO_UINT(master_miso.rddata(c_data_w - 1 downto 0));
      if v_rddata /= v_expdata then
        report "Error! Readvalue is not as expected"
          severity ERROR;
      end if;
      v_expdata := v_expdata + 1;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- The MM bus
  -----------------------------------------------------------------------------
  u_mm_bus: entity work.mm_bus
  generic map (
    g_nof_slaves          => g_nof_slaves,
    g_base_arr            => c_base_arr,
    g_width_arr           => c_width_arr,
    g_rd_latency_arr      => c_rd_latency_arr,
    g_slave_enable_arr    => c_slave_enable_arr,
    g_waitrequest_arr     => c_waitrequest_arr,
    g_pipeline_mosi       => g_pipeline_mosi,
    g_pipeline_miso_rdval => g_pipeline_miso_rdval,
    g_pipeline_miso_wait  => g_pipeline_miso_wait
  )
  port map (
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    master_mosi    => master_mosi,
    master_miso    => master_miso,
    slave_mosi_arr => slave_mosi_arr,
    slave_miso_arr => slave_miso_arr
  );

  -----------------------------------------------------------------------------
  -- Model the MM slaves
  -----------------------------------------------------------------------------
  gen_slaves : for I in 0 to g_nof_slaves - 1 generate
    u_waitrequest_model : entity work.mm_waitrequest_model
    generic map (
      g_waitrequest => g_waitrequest,
      g_seed        => I
    )
    port map (
      mm_clk     => mm_clk,
      bus_mosi   => slave_mosi_arr(I),
      bus_miso   => slave_miso_arr(I),
      slave_mosi => ram_mosi_arr(I),
      slave_miso => ram_miso_arr(I)
    );

    u_ram : entity common_lib.common_ram_r_w
    generic map (
      g_ram       => c_test_ram,
      g_init_file => "UNUSED"
    )
    port map (
      rst       => mm_rst,
      clk       => mm_clk,
      clken     => '1',
      wr_en     => ram_mosi_arr(I).wr,
      wr_adr    => ram_mosi_arr(I).address(g_width_w - 1 downto 0),
      wr_dat    => ram_mosi_arr(I).wrdata(c_data_w - 1 downto 0),
      rd_en     => ram_mosi_arr(I).rd,
      rd_adr    => ram_mosi_arr(I).address(g_width_w - 1 downto 0),
      rd_dat    => ram_miso_arr(I).rddata(c_data_w - 1 downto 0),
      rd_val    => ram_miso_arr(I).rdval
    );
  end generate;
end tb;
