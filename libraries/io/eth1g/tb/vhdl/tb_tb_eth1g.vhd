-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi-testbench for eth1g
-- Description:
--   Verify eth1g for different data types
-- Usage:
--   > as 3
--   > run -all

library IEEE, technology_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity tb_tb_eth1g is
  generic (
    g_technology_dut : natural := c_tech_select_default
  );
end tb_tb_eth1g;

architecture tb of tb_tb_eth1g is
  constant c_technology_lcu : natural := c_tech_select_default;

  constant c_tb_end_vec : std_logic_vector(15 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(15 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
-- g_technology_dut : NATURAL := c_tech_select_default;
-- g_technology_lcu : NATURAL := c_tech_select_default;
-- g_frm_discard_en : BOOLEAN := TRUE;   -- when TRUE discard frame types that would otherwise have to be discarded by the Nios MM master
-- g_flush_test_en  : BOOLEAN := FALSE;  -- when TRUE send many large frames to enforce flush in eth_buffer
-- g_tb_end         : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
-- --   g_data_type = c_tb_tech_tse_data_type_symbols  = 0
-- --   g_data_type = c_tb_tech_tse_data_type_counter  = 1
-- --   g_data_type = c_tb_tech_tse_data_type_arp      = 2
-- --   g_data_type = c_tb_tech_tse_data_type_ping     = 3
-- --   g_data_type = c_tb_tech_tse_data_type_udp      = 4
-- g_data_type : NATURAL := c_tb_tech_tse_data_type_udp

  u_use_symbols     : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu, false, false, false, c_tb_tech_tse_data_type_symbols) port map (tb_end_vec(0));
  u_use_counter     : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu, false, false, false, c_tb_tech_tse_data_type_counter) port map (tb_end_vec(1));
  u_use_arp         : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu,  true, false, false, c_tb_tech_tse_data_type_arp    ) port map (tb_end_vec(2));
  u_use_ping        : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu,  true, false, false, c_tb_tech_tse_data_type_ping   ) port map (tb_end_vec(3));
  u_use_udp         : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu,  true, false, false, c_tb_tech_tse_data_type_udp    ) port map (tb_end_vec(4));
  u_use_udp_flush   : entity work.tb_eth1g generic map (g_technology_dut, c_technology_lcu,  true,  true, false, c_tb_tech_tse_data_type_udp    ) port map (tb_end_vec(5));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
