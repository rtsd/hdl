-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for bn_capture using Nios main.c program for stimuli
--
-- Features:
-- . Depend on main.c that runs on the Nios
--
-- Usage:
-- . NIOS SW : /apps/bn_capture/main.c
-- .    Set g_use_phy = (0, 0, 0, 0, 0, 0, 1, 0)
-- .    > do wave_bn_capture.do
-- .    > run 20 ms  (to see "WG ON" when TASK_WG is enabled in main.c)
-- .    Observe printf results in transcript window
-- .    The output depends on what tasks are set to true via TASK_* in main.c
-- . NIOS SW : /apps/bn_capture_ddr3/main.c
-- .    Set g_use_phy = (0, 0, 0, 0, 1, 0, 1, 0)

library IEEE, common_lib, dp_lib, i2c_lib, unb1_board_lib, diag_lib, aduh_lib, ddr3_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.unb1_bn_capture_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use aduh_lib.aduh_dd_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use ddr3_lib.ddr3_pkg.all;
use i2c_lib.i2c_dev_unb_pkg.all;

entity tb_bn_capture is
  generic (
    -- TYPE t_c_unb_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   eth10g  : NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
    g_use_phy     : t_c_unb_use_phy := (0, 0, 0, 0, 0, 0, 1, 0)
  );
end tb_bn_capture;

architecture tb of tb_bn_capture is
  -- UniBoard
  constant c_sim                : boolean := true;
  constant c_version            : std_logic_vector(1 downto 0) := "00";
  constant c_id_bn0             : std_logic_vector(7 downto 0) := "00000100";
  constant c_fw_version         : t_unb_fw_version := (1, 0);

  constant c_bn_capture_sp_sim  : t_c_bn_capture_sp := (800, 1024, 1024 * 1024, 1024, true);  -- 800 MSps, block size 1024 samples, nof blocks per sync interval, monitor buffer nof samples using sync
  constant c_bn_capture         : t_c_bn_capture := (c_unb_mm_clk_freq_50M,
                                                     c_unb_ext_clk_freq_200M,
                                                     c_bn_capture_sp_sim);

  constant c_ddr                : t_c_ddr3_phy := c_ddr3_phy_4g;  -- use c_ddr3_phy_4g or c_ddr3_phy_1g dependent on what was generated with the MegaWizard

  constant c_eth_clk_period     : time := 40 ns;  -- 25 MHz XO on UniBoard

  constant c_cable_delay        : time := 12 ns;

  constant c_max1618_address    : std_logic_vector := TO_UVEC(I2C_UNB_MAX1617_ADR, 7);  -- MAX1618 address MID MID
  constant c_max1618_temp       : integer := 60;

  -- ADU
  constant c_nof_dp_phs_clk     : natural := 6;  -- nof dp_phs_clk that can be used to detect the word phase
  constant c_ai                 : t_c_aduh_dd_ai := c_aduh_dd_ai;

  constant c_dp_factor          : natural := c_ai.rx_factor * c_ai.dd_factor;
  constant c_wideband_factor    : natural := c_dp_factor;  -- = 4

  constant c_sample_freq        : natural := c_wideband_factor * c_unb_ext_clk_freq_200M / 10**6;  -- 800 MSps
  constant c_sample_period      : time := (10**6 / c_sample_freq) * 1 ps;  -- 1250 ns
  constant c_ext_clk_freq       : natural := c_unb_ext_clk_freq_200M;  -- 200 MHz external reference clock for data path processing
  constant c_ext_clk_period     : time := c_wideband_factor * c_sample_period;  -- 200 MHz

  constant c_ana_diff           : natural := 16;  -- analogue offset value between the port A, B, C, D, use 0 to have same data on all ports

  -- ADC
  signal ANA_DAT               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- Common ADC reference data source
  signal ANA_A                 : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port A
  signal ANA_B                 : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port B
  signal ANA_C                 : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port C
  signal ANA_D                 : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port D
  signal ANA_OVR               : std_logic := '0';
  signal SCLK                  : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK_AB               : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_CD               : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_RST_AB           : std_logic;  -- synchronise digital lvds clock
  signal DCLK_RST_CD           : std_logic;  -- synchronise digital lvds clock
  signal DIG_A                 : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B                 : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_C                 : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_D                 : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_OVR_AB            : std_logic := '0';
  signal DIG_OVR_CD            : std_logic := '0';

  signal test_pattern_en       : std_logic := '0';

  signal ADC_AB_SCL            : std_logic;
  signal ADC_AB_SDA            : std_logic;
  signal ADC_CD_SCL            : std_logic;
  signal ADC_CD_SDA            : std_logic;

  -- SO-DIMM Memory Bank I
  signal MB_I_in               : t_ddr3_phy_in_arr(0 downto 0);
  signal MB_I_io               : t_ddr3_phy_io_arr(0 downto 0);
  signal MB_I_ou               : t_ddr3_phy_ou_arr(0 downto 0);

  -- SO-DIMM Memory Bank II
  signal MB_II_in              : t_ddr3_phy_in_arr(0 downto 0);
  signal MB_II_io              : t_ddr3_phy_io_arr(0 downto 0);
  signal MB_II_ou              : t_ddr3_phy_ou_arr(0 downto 0);

  -- DUT
  signal ext_clk               : std_logic := '0';
  signal ext_pps               : std_logic := '0';
  signal WDI                   : std_logic;
  signal INTA                  : std_logic;
  signal INTB                  : std_logic;
  signal sens_scl              : std_logic;
  signal sens_sda              : std_logic;

  signal eth_clk               : std_logic := '0';
  signal eth_txp               : std_logic;
  signal eth_rxp               : std_logic;

  signal VERSION               : std_logic_vector(c_unb_aux.version_w - 1 downto 0);
  signal ID                    : std_logic_vector(c_unb_aux.id_w - 1 downto 0);
  signal TESTIO                : std_logic_vector(c_unb_aux.testio_w - 1 downto 0);
begin
  -- Run 1 ms

  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------

  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- 1GbE XO clock (25 MHz)

  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  u_sens_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_max1618_address
  )
  port map (
    scl  => sens_scl,
    sda  => sens_sda,
    temp => c_max1618_temp
  );

  VERSION <= c_version;
  ID <= c_id_bn0;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  dut : entity work.bn_capture
  generic map (
    -- General
    g_sim         => c_sim,
    g_fw_version  => c_fw_version,
    -- BN capture specific
    g_bn_capture  => c_bn_capture,
    -- Use PHY Interface
    g_use_phy     => g_use_phy,
    -- Auxiliary Interface
    g_aux         => c_unb_aux,
    -- DDR3 Interface
    g_ddr         => c_ddr,
    -- ADC Interface
    g_nof_dp_phs_clk => c_nof_dp_phs_clk,
    g_ai             => c_ai
  )
  port map (
    -- GENERAL
    CLK                    => ext_clk,
    PPS                    => ext_pps,
    WDI                    => WDI,
    INTA                   => INTA,
    INTB                   => INTB,

    -- Others
    VERSION                => VERSION,
    ID                     => ID,
    TESTIO                 => TESTIO,

    -- I2C Interface to Sensors
    sens_sc                => sens_scl,
    sens_sd                => sens_sda,

    -- 1GbE Control Interface
    ETH_clk                => eth_clk,
    ETH_SGIN               => eth_rxp,
    ETH_SGOUT              => eth_txp,

    -- SO-DIMM Memory Bank I = ddr3_I
    MB_I_IN                => MB_I_in,
    MB_I_IO                => MB_I_io,
    MB_I_OU                => MB_I_ou,

    -- SO-DIMM Memory Bank II = ddr3_II
    MB_II_IN               => MB_II_in,
    MB_II_IO               => MB_II_io,
    MB_II_OU               => MB_II_ou,

    -- ADC Interface
    ADC_BI_A               => DIG_A,
    ADC_BI_B               => DIG_B,
    ADC_BI_A_CLK           => DCLK_AB,
    ADC_BI_A_CLK_RST       => DCLK_RST_AB,
    ADC_BI_C               => DIG_C,
    ADC_BI_D               => DIG_D,
    ADC_BI_D_CLK           => DCLK_CD,
    ADC_BI_D_CLK_RST       => DCLK_RST_CD,

    ADC_AB_SCL             => ADC_AB_SCL,
    ADC_AB_SDA             => ADC_AB_SDA,
    ADC_CD_SCL             => ADC_CD_SCL,
    ADC_CD_SDA             => ADC_CD_SDA
  );

  -----------------------------------------------------------------------------
  -- ADU0 model and ADU1 model for BN port A,B and C,D
  -----------------------------------------------------------------------------

  -- Same sample clock for all ADC
  SCLK <= not SCLK after c_sample_period / 2;

  -- Same analogue reference signal for all ADC, use incrementing data to ease the verification
  ANA_DAT <= INCR_UVEC(ANA_DAT, 1) when rising_edge(SCLK);
  ANA_A   <= INCR_UVEC(ANA_DAT, 0 * c_ana_diff);
  ANA_B   <= INCR_UVEC(ANA_DAT, 1 * c_ana_diff);
  ANA_C   <= INCR_UVEC(ANA_DAT, 2 * c_ana_diff);
  ANA_D   <= INCR_UVEC(ANA_DAT, 3 * c_ana_diff);
  ANA_OVR <= not ANA_OVR when rising_edge(SCLK);  -- simple overflow model used for both ADC

  -- Same sample clock for all ADC
  SCLK <= not SCLK after c_sample_period / 2;

  -- National ADC
  u_adc_AB : entity aduh_lib.adu_half
  port map (
    AI              => TO_SINT(ANA_A),
    AQ              => TO_SINT(ANA_B),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_AB,
    DCLK_RST        => DCLK_RST_AB,
    DI              => DIG_A,
    DQ              => DIG_B,
    OVR             => DIG_OVR_AB,
    SCL             => ADC_AB_SCL,
    SDA             => ADC_AB_SDA,
    test_pattern_en => test_pattern_en
  );

  u_adc_CD : entity aduh_lib.adu_half
  port map (
    AI              => TO_SINT(ANA_C),
    AQ              => TO_SINT(ANA_D),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_CD,
    DCLK_RST        => DCLK_RST_CD,
    DI              => DIG_C,
    DQ              => DIG_D,
    OVR             => DIG_OVR_CD,
    SCL             => ADC_CD_SCL,
    SDA             => ADC_CD_SDA,
    test_pattern_en => test_pattern_en
  );

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp <= transport eth_txp after c_cable_delay;
end tb;
