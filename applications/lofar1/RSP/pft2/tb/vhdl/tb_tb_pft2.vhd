-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multi testbench for pft2.vhd
-- Description:

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_pft2 is
end tb_tb_pft2;

use work.pft_pkg.all;

architecture tb of tb_tb_pft2 is
  constant c_sw    : std_logic := '1';  -- default for g_switch_en
  constant c_dat_w : natural := c_pft_stage_dat_w;  -- default for g_stage_dat_w

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- >>> PFT settings
  --g_switch_en    : STD_LOGIC := '0';
  --g_stage_dat_w  : NATURAL := c_pft_stage_dat_w;  -- c_pft_stage_dat_w = 20 in pft_pkg.vhd
  --
  -- The PFT has 3 modes:
  -- . PFT_MODE_BITREV   --> DIT output
  -- . PFT_MODE_COMPLEX  --> DIF output
  -- . PFT_MODE_REAL2    --> DIT output of two real inputs (default for LOFAR)
  --g_pft_mode : pft_mode_type := PFT_MODE_BITREV;
  --g_pft_mode : pft_mode_type := PFT_MODE_COMPLEX;
  --g_pft_mode : pft_mode_type := PFT_MODE_REAL2;
  --
  -- . For PFT_MODE_REAL2 select any signal pair for X and Y input.
  -- . For PFT_MODE_BITREV and PFT_MODE_COMPLEX select zeros for Y input.
  --
  -- >>> Testbench settings
  -- Select one input signal for X (used as real input to the PFT)
  --g_name_x   : STRING  := "cosin_N2";
  --g_name_x   : STRING  := "cosin_1";
  --g_name_x   : STRING  := "cosin_39";
  --g_name_x   : STRING  := "sinus_1";
  --g_name_x   : STRING  := "sinus_13";
  --g_name_x   : STRING  := "sinus_13s";
  --g_name_x   : STRING  := "impulse_0";
  --g_name_x   : STRING  := "impulse_1";
  --g_name_x   : STRING  := "zeros";
  --g_name_x   : STRING  := "dc";
  --g_name_x   : STRING  := "block_1";
  --g_name_x   : STRING  := "block_117";
  --g_name_x   : STRING  := "u_noise";
  --
  -- Select one input signal for Y (used as imag input to the PFT)
  --g_name_y   : STRING  := "cosin_N2"
  --g_name_y   : STRING  := "cosin_1"
  --g_name_y   : STRING  := "cosin_39"
  --g_name_y   : STRING  := "sinus_1"
  --g_name_y   : STRING  := "sinus_13"
  --g_name_y   : STRING  := "sinus_13s"
  --g_name_y   : STRING  := "impulse_0"
  --g_name_y   : STRING  := "impulse_1"
  --g_name_y   : STRING  := "zeros"  -- For PFT_MODE_BITREV and PFT_MODE_COMPLEX select zeros for Y input.
  --g_name_y   : STRING  := "dc"
  --g_name_y   : STRING  := "block_1"
  --g_name_y   : STRING  := "block_117"
  --g_name_y   : STRING  := "u_noise"
  --
  --g_repeat   : NATURAL := 2  -- minimal 2 due to PFT latency
  --g_repeat   : NATURAL := 10  -- > c_nof_block_per_sync to view multiple in_sync and out_sync intervals

  --                                              g_switch_en
  --                                               .       g_stage_dat_w
  --                                               .        .      g_pft_mode
  --                                               .        .       .             g_name_x
  --                                               .        .       .              .       g_name_y
  --                                               .        .       .              .         .      g_repeat
  --                                               .        .       .              .         .       .
  -- One active input, other input zeros           .        .       .              .         .       .
  u_cosin_N2  : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,  "cosin_N2", "zeros", 2);
  u_cosin_1   : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,   "cosin_1", "zeros", 2);
  u_cosin_39  : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,  "cosin_39", "zeros", 2);
  u_sinus_1   : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,   "sinus_1", "zeros", 2);
  u_sinus_13  : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,  "sinus_13", "zeros", 2);
  u_sinus_13s : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "sinus_13s", "zeros", 2);
  u_impulse_0 : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "impulse_0", "zeros", 2);
  u_impulse_1 : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "impulse_1", "zeros", 2);
  u_zeros     : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,     "zeros", "zeros", 2);
  u_dc        : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,        "dc", "zeros", 2);
  u_block_1   : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,   "block_1", "zeros", 2);
  u_block_117 : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "block_117", "zeros", 2);
  u_u_noise   : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,   "u_noise", "zeros", 2);

  -- Two active inputs
  u_impulse_0_impulse_1 : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "impulse_0", "impulse_1", 2);
  u_dc_cosin_N2         : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,        "dc",  "cosin_N2", 2);
  u_dc_sinus_13         : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,        "dc",  "sinus_13", 2);
  u_block_117_u_noise   : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2, "block_117",   "u_noise", 2);
  u_u_noise_u_noise     : entity work.tb_pft2 generic map(c_sw, c_dat_w, PFT_MODE_REAL2,   "u_noise",   "u_noise", 2);

  -- Vary g_stage_dat_w (pft2 g_in_dat_w = g_out_dat_w = 18)
  -- . for g_stage_dat_w = 16 tb result is still OK, but <= 15 it fails
  u_16_cosin_39 : entity work.tb_pft2 generic map(c_sw, 16, PFT_MODE_REAL2,  "cosin_39", "zeros", 2);
  u_17_cosin_39 : entity work.tb_pft2 generic map(c_sw, 17, PFT_MODE_REAL2,  "cosin_39", "zeros", 2);
  u_18_cosin_39 : entity work.tb_pft2 generic map(c_sw, 18, PFT_MODE_REAL2,  "cosin_39", "zeros", 2);
  u_19_cosin_39 : entity work.tb_pft2 generic map(c_sw, 19, PFT_MODE_REAL2,  "cosin_39", "zeros", 2);

  u_16_block_117_u_noise : entity work.tb_pft2 generic map(c_sw, 16, PFT_MODE_REAL2, "block_117", "u_noise", 2);
  u_17_block_117_u_noise : entity work.tb_pft2 generic map(c_sw, 17, PFT_MODE_REAL2, "block_117", "u_noise", 2);
  u_18_block_117_u_noise : entity work.tb_pft2 generic map(c_sw, 18, PFT_MODE_REAL2, "block_117", "u_noise", 2);
  u_19_block_117_u_noise : entity work.tb_pft2 generic map(c_sw, 19, PFT_MODE_REAL2, "block_117", "u_noise", 2);
  -- . for g_stage_dat_w > 20 tb result is still OK, but diff_max_* does not reduce further
end tb;
