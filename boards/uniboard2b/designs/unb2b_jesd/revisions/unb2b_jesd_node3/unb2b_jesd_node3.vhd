-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, technology_lib, diag_lib, dp_lib, unb2b_jesd_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity unb2b_jesd_node3 is
  generic (
    g_design_name       : string  := "unb2b_jesd_node3";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e1sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn         : natural := 0;  -- SVN revision    -- set by QSF
    g_protect_addr_range: boolean := false
  );
  port (
    -- GENERAL
--    CLK          : IN    STD_LOGIC; -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0);

    -- JESD signals
    jesd204_rx_serial_data  : in std_logic;
    jesd204_sync_n_out      : out std_logic;
    jesd204_rx_sysref       : in std_logic;
    jesd204_device_clk      : in std_logic
  );
end unb2b_jesd_node3;

architecture str of unb2b_jesd_node3 is
begin
  u_revision : entity unb2b_jesd_lib.unb2b_jesd
  generic map (
    g_design_name       => g_design_name,
    g_design_note       => g_design_note,
    g_technology        => g_technology,
    g_sim               => g_sim,
    g_sim_unb_nr        => g_sim_unb_nr,
    g_sim_node_nr       => g_sim_node_nr,
    g_stamp_date        => g_stamp_date,
    g_stamp_time        => g_stamp_time,
    g_stamp_svn         => g_stamp_svn,
    g_protect_addr_range => g_protect_addr_range
  )
  port map (
    -- GENERAL
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- I2C Interface to Sensors
    SENS_SC      => SENS_SC,
    SENS_SD      => SENS_SD,

    PMBUS_SC     => PMBUS_SC,
    PMBUS_SD     => PMBUS_SD,
    PMBUS_ALERT  => PMBUS_ALERT,

    -- 1GbE Control Interface
    ETH_CLK      => ETH_CLK,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    QSFP_LED     => QSFP_LED,

    -- JESD signals
    jesd204_rx_serial_data  => jesd204_rx_serial_data,
    jesd204_sync_n_out      => jesd204_sync_n_out,
    jesd204_rx_sysref       => jesd204_rx_sysref,
    jesd204_device_clk      => jesd204_device_clk
  );
end str;
