-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_pulser is
end tb_common_pulser;

architecture tb of tb_common_pulser is
  constant clk_period   : time := 40 ns;

  constant c_reset_len  : natural := 3;

  constant c_pulse_us   : natural := 25;
  constant c_pulse_ms   : natural := 1000;

  signal rst                   : std_logic;
  signal clk                   : std_logic := '0';

  signal pulse_us              : std_logic;
  signal pulse_ms_via_clk_en   : std_logic;
  signal pulse_ms_via_pulse_en : std_logic;

  signal pulse_ms_clr          : std_logic;
begin
  -- as 3
  -- run 7 ms

  clk <= not clk  after clk_period / 2;

  p_pulse_clr : process
  begin
    pulse_ms_clr <= '0';
    for I in 0 to 1500 loop
      wait until pulse_us = '1';
      wait until rising_edge(clk);
    end loop;

    wait until pulse_us = '1';
    pulse_ms_clr <= '1';
    wait until rising_edge(clk);
    pulse_ms_clr <= '0';
    wait;
  end process;

  u_reset : entity work.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => clk,
    out_rst   => rst
  );

  u_pulse_us : entity work.common_pulser
  generic map (
    g_pulse_period => c_pulse_us,
    g_pulse_phase  => c_pulse_us - 1
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    pulse_en       => '1',
    pulse_clr      => '0',
    pulse_out      => pulse_us
  );

  u_pulse_ms_via_clk_en : entity work.common_pulser
  generic map (
    g_pulse_period => c_pulse_ms
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => pulse_us,
    pulse_en       => '1',
    pulse_clr      => pulse_ms_clr,
    pulse_out      => pulse_ms_via_clk_en
  );

  u_pulse_ms_via_pulse_en : entity work.common_pulser
  generic map (
    g_pulse_period => c_pulse_ms
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    pulse_en       => pulse_us,
    pulse_clr      => pulse_ms_clr,
    pulse_out      => pulse_ms_via_pulse_en
  );
end tb;
