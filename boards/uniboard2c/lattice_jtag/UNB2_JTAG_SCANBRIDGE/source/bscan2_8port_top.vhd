-- --------------------------------------------------------------------
-- >>>>>>>>>>>>>>>>>>>>>>>>> COPYRIGHT NOTICE <<<<<<<<<<<<<<<<<<<<<<<<<
-- --------------------------------------------------------------------
-- Copyright (c) 2001 - 2008 by Lattice Semiconductor Corporation
-- --------------------------------------------------------------------
--
-- Permission:
--
--   Lattice Semiconductor grants permission to use this code for use
--   in synthesis for any Lattice programmable logic product.  Other
--   use of this code, including the selling or duplication of any
--   portion is strictly prohibited.
--
-- Disclaimer:
--
--   This VHDL or Verilog source code is intended as a design reference
--   which illustrates how these types of functions can be implemented.
--   It is the user's responsibility to verify their design for
--   consistency and functionality through the use of formal
--   verification methods.  Lattice Semiconductor provides no warranty
--   regarding the use or functionality of this code.
--
-- --------------------------------------------------------------------
--
--                     Lattice Semiconductor Corporation
--                     5555 NE Moore Court
--                     Hillsboro, OR 97214
--                     U.S.A
--
--                     TEL: 1-800-Lattice (USA and Canada)
--                          503-268-8001 (other locations)
--
--                     web: http://www.latticesemi.com/
--                     email: techsupport@latticesemi.com
--
-- --------------------------------------------------------------------
-- Code Revision History :
-- --------------------------------------------------------------------
-- Ver: | Author      |Mod. Date  |Changes Made:
-- V1.0 | J.O.        |11/10/08   |Initial Version
-- --------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bscan2 is
   -- enter the number of BSCAN2 blocks to create.  This is the only place that
   -- needs to be modified to control the number of local scan ports created.
   generic ( bscan_ports :     positive := 2 );
   port( TDI, TCK, TMS   : in  std_logic;
         TRST            : in  std_logic;
         -- Turn on slow slew in fitter for output signals
         TDO             : out std_logic;
         -- OE control for MSP ports (Active high)
         ENABLE_MSP      : in  std_logic;
         MSPTCK          : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTDI          : in  std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTDO          : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTMS          : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTRST         : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         -- one set of addresses to check for device
         IDN             : in  std_logic_vector(3 downto 0)
         );
end;

architecture behave of bscan2 is
   component top_linker is
      -- do not use the generic map to prevent the synthesis tool from
      -- appending the number of ports to the components name.
      port(TDI, TCK, TMS : in  std_logic;
           TRST          : in  std_logic;
           -- enable logic for TDO pins.
           TDO_enable    : out std_logic;
           TDO           : out std_logic;
           MSPCLK        : out std_logic_vector(4 * bscan_ports downto 1);
           MSPTDI        : in  std_logic_vector(4 * bscan_ports downto 1);
           MSPTDO        : out std_logic_vector(4 * bscan_ports downto 1);
           MSPTMS        : out std_logic_vector(4 * bscan_ports downto 1);
           MSPTRST       : out std_logic_vector(4 * bscan_ports downto 1);
           -- one set of addresses to check for device
           IDN           : in  std_logic_vector(4 downto 1)
           );
end component top_linker;
-- synthesis FILE="top_linker.ngo"

-- logic to enable TDO pins
signal ENABLE_TDO    : std_logic;
-- signal from tap controler that enables all TDOs.
signal tdoENABLE     : std_logic;
-- logic to generate tdo_sp and tdo_hdr
signal LSPTMS        : std_logic_vector(4 * bscan_ports - 1 downto 0);
signal LSPTCK        : std_logic_vector(4 * bscan_ports - 1 downto 0);
signal LSPTDO        : std_logic_vector(4 * bscan_ports - 1 downto 0);
signal LSPTRST       : std_logic_vector(4 * bscan_ports - 1 downto 0);
-- output of Port Mux
signal TDO_int       : std_logic;
begin
   -- Wire up all of the tri-state controlled lines automatically
   tri_state_lines : for lvar1 in 0 to (4 * bscan_ports - 1) generate
      MSPTCK(lvar1)  <= LSPTCK(lvar1)  when ENABLE_MSP = '1' else 'Z';
      MSPTMS(lvar1)  <= LSPTMS(lvar1)  when ENABLE_MSP = '1' else 'Z';
      MSPTRST(lvar1) <= LSPTRST(lvar1) when ENABLE_MSP = '1' else 'Z';
      -- enable MSPTDOs for 1149.1
      MSPTDO(lvar1)  <= LSPTDO(lvar1)  when ENABLE_TDO = '1' else 'Z';
   end generate tri_state_lines;

   -- MSP Port enable controls
   -- enable logic for all TDO pins
   ENABLE_TDO <= ENABLE_MSP and tdoENABLE;

   TDO  <= TDO_int when tdoENABLE  = '1' else 'Z';

   TopLinkerModule : component top_linker
      port map(
         TDO         => TDO_int,
         TMS         => TMS,
         TCK         => TCK,
         TRST        => TRST,
         TDI         => TDI,
         TDO_enable  => tdoENABLE,
         MSPTDI      => MSPTDI,
         MSPTDO      => LSPTDO,
         MSPTMS      => LSPTMS,
         MSPCLK      => LSPTCK,
         MSPTRST     => LSPTRST,
         IDN         => IDN
         );
end behave;

--------------------------------- E O F --------------------------------------
