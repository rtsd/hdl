library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library common_lib;
use common_lib.all;

architecture rtl of pft_bf_fw is
  constant c_add_pipeline  : natural := 2;
  constant c_dist          : natural := 2**g_index;
  constant c_pipeline      : natural := c_add_pipeline + c_dist + 2;
  constant c_cnt_w         : natural := g_index + 2;

  type reg_arr is array (c_dist downto - c_dist) of std_logic_vector(g_in_dat_w - 1 downto 0);
  type val_arr is array (c_dist downto - c_dist) of std_logic;

  signal xr                : reg_arr;
  signal nxt_xr            : reg_arr;

  signal xi                : reg_arr;
  signal nxt_xi            : reg_arr;

  signal pipe_val          : std_logic_vector(c_pipeline-1 downto 0);
  signal nxt_pipe_val      : std_logic_vector(pipe_val'range);
  signal pipe_sync         : std_logic_vector(c_pipeline-1 downto 0);
  signal nxt_pipe_sync     : std_logic_vector(pipe_sync'range);

  signal cnt               : std_logic_vector(c_cnt_w - 1 downto 0);
  signal nxt_cnt           : std_logic_vector(cnt'range);
  signal s0                : std_logic;
  signal s1                : std_logic;

  signal yr_a              : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal yr_b              : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal yi_a              : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal yi_b              : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal yr_add            : std_logic;
  signal yi_add            : std_logic;
  signal yr_cry            : std_logic;
  signal yi_cry            : std_logic;

  signal nxt_yr_a          : std_logic_vector(yr_a'range);
  signal nxt_yr_b          : std_logic_vector(yr_b'range);
  signal nxt_yi_a          : std_logic_vector(yi_a'range);
  signal nxt_yi_b          : std_logic_vector(yi_b'range);
  signal nxt_yr_add        : std_logic;
  signal nxt_yi_add        : std_logic;
begin
  nxt_xr  <= in_re  & xr (xr 'high downto xr 'low + 1);
  nxt_xi  <= in_im  & xi (xi 'high downto xi 'low + 1);

  nxt_pipe_val  <= in_val  & pipe_val (pipe_val 'high downto 1);
  nxt_pipe_sync <= in_sync & pipe_sync(pipe_sync'high downto 1);

  s0  <= cnt(cnt'high - 1);
  s1  <= cnt(cnt'high  ) when g_bf_name = "bf2" else '0';

  out_val  <= pipe_val(0);
  out_sync <= pipe_sync(0);

  registers : process (clk, rst)
  begin
    if rst = '1' then
      xr          <= (others => (others => '0'));
      xi          <= (others => (others => '0'));
      pipe_val    <= (others => '0');
      pipe_sync   <= (others => '0');
      cnt         <= (others => '0');
      yr_a        <= (others => '0');
      yr_b        <= (others => '0');
      yi_a        <= (others => '0');
      yi_b        <= (others => '0');
      yr_add      <= '0';
      yi_add      <= '0';
    elsif rising_edge(clk) then
      xr          <= nxt_xr;
      xi          <= nxt_xi;
      pipe_val    <= nxt_pipe_val;
      pipe_sync   <= nxt_pipe_sync;
      cnt         <= nxt_cnt;
      yr_a        <= nxt_yr_a;
      yr_b        <= nxt_yr_b;
      yi_a        <= nxt_yi_a;
      yi_b        <= nxt_yi_b;
      yr_add      <= nxt_yr_add;
      yi_add      <= nxt_yi_add;
    end if;
  end process;

  counter : process (cnt, pipe_val, pipe_sync)
  begin
    nxt_cnt <= cnt;
    if pipe_sync(pipe_sync'high - c_dist) = '1' then
      nxt_cnt <= (others => '0');
    elsif pipe_val(pipe_val'high - c_dist) = '1' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;

  process(s0, s1, xr, xi)
    variable state : std_logic_vector(1 downto 0);
  begin
    state := s1 & s0;
    case state is

      when "00" =>
        -- y  <= x(n+k) + x(n)

        -- yr <= xr(n+k) + xr(n)
        nxt_yr_add <= '1';
        nxt_yr_a   <= xr(0);
        nxt_yr_b   <= xr(c_dist);
        -- yi <= xi(n+k) + xi(n);
        nxt_yi_add <= '1';
        nxt_yi_a   <= xi(0);
        nxt_yi_b   <= xi(c_dist);

      when "01" =>
        -- y  <= x(n-k) - x(n)

        -- yr <= xr(n-k) - xr(n)
        nxt_yr_add <= '0';
        nxt_yr_a   <= xr(-c_dist);
        nxt_yr_b   <= xr(0);
        -- yi <= xi(n-k) - xi(n)
        nxt_yi_add <= '0';
        nxt_yi_a   <= xi(-c_dist);
        nxt_yi_b   <= xi(0);

      when "10" =>
        -- y <= x(n) - i*x(n+k)

        -- yr <= xr(n)   + xi(n+k)
        nxt_yr_add <= '1';
        nxt_yr_a   <= xr(0);
        nxt_yr_b   <= xi(c_dist);
        -- yi <= xi(n)   - xr(n+k)
        nxt_yi_add <= '0';
        nxt_yi_a   <= xi(0);
        nxt_yi_b   <= xr(c_dist);

      when others =>
        -- y <= x(n-k) + i*x(n)
        -- yr(n) <= xr(n-k) - xi(n);
        nxt_yr_add <= '0';
        nxt_yr_a   <= xr(-c_dist);
        nxt_yr_b   <= xi(0);
        -- yi(n) <= xi(n-k) + xr(n);
        nxt_yi_add <= '1';
        nxt_yi_a   <= xi(-c_dist);
        nxt_yi_b   <= xr(0);
    end case;
  end process;

  -- Adds/ Subs ----------------------------------------------------------------

--  Intel Altera lmp_add_sub carry in:
--  ADD: out = a + b + cin      => cin = '0' to have out = a + b
--  SUB: out = a - b + cin - 1  => cin = '1' to have out = a - b

--  yr_cry <= NOT yr_add;

--  yr : ENTITY common_lib.common_addsub
--  GENERIC MAP (
--    g_in_a_w    => g_in_dat_w,
--    g_in_b_w    => g_in_dat_w,
--    g_out_c_w   => g_out_dat_w,
--    g_pipeline  => c_add_pipeline,
--    g_add_sub   => "BOTH"
--  )
--  PORT MAP (
--    in_a        => yr_a,
--    in_b        => yr_b,
--    in_cry      => yr_cry,
--    add_sub     => yr_add,
--    clk         => clk,
--    out_c       => out_re
--  );

  yr : entity common_add_sub
  generic map (
    g_direction       => "BOTH",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_pipeline,  -- >= 0
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    sel_add  => yr_add,
    in_a     => yr_a,
    in_b     => yr_b,
    result   => out_re
  );

--  yi_cry <= NOT yi_add;
--
--  yi : ENTITY common_lib.common_addsub
--  GENERIC MAP (
--    g_in_a_w    => g_in_dat_w,
--    g_in_b_w    => g_in_dat_w,
--    g_out_c_w   => g_out_dat_w,
--    g_pipeline  => c_add_pipeline,
--    g_add_sub   => "BOTH"
--  )
--  PORT MAP (
--    in_a        => yi_a,
--    in_b        => yi_b,
--    in_cry      => yi_cry,
--    add_sub     => yi_add,
--    clk         => clk,
--    out_c       => out_im
--  );

  yi : entity common_add_sub
  generic map (
    g_direction       => "BOTH",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_pipeline,  -- >= 0
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    sel_add  => yi_add,
    in_a     => yi_a,
    in_b     => yi_b,
    result   => out_im
  );
end rtl;
