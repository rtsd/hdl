-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Adds basic networking capabilities to tr_10GbE by sending ARP request
-- packets on a predefined interval as well as respond to PING requests.
--   To avoid that the packet transmission will get a gap that will abort it.
--   The average DP data rate depends on dp_clk and on the DP data valid.
-- NOTE: When connected to a PC make sure you can handle backpressure for
--       a large amount of clock cycles (can be > 16k). This backpressure
--       is due to the possibility of receiving pause frames from the PC.
--       If your design cannot handle the backpressure from PC, you should
--       disable it on the PC side. This can be done with ethtool:
--       # ethtool -A ethX autoneg off rx off tx off
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib, tr_10GbE_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_select_pkg.all;
use technology_lib.technology_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity nw_10GbE is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_sim                    : boolean;
    g_sim_level              : natural := 1;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_macs               : natural;
    g_direction              : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_use_mdio               : boolean := false;
    g_mdio_epcs_dis          : boolean := false;  -- TRUE disables EPCS on init; e.g. to target a 10GbE card in PC that does not support it
    g_tx_fifo_fill           : natural := 10;  -- Release tx packet only when sufficiently data is available such that no data gaps are introduced.
    g_tx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_rx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_word_alignment_padding : boolean := false;
    g_xon_backpressure       : boolean := false;
    g_arp_period_s           : natural := 30;
    g_ip_hdr_field_arr       : t_common_field_arr
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R
    tr_ref_clk_312   : in  std_logic := '0';  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156   : in  std_logic := '0';  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156   : in  std_logic := '0';  -- for 10GBASE-R or for XAUI

    -- Calibration & reconfig clock
    cal_rec_clk         : in  std_logic := '0';  -- for XAUI;

    -- MM interface
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;

    reg_mac_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mac_miso        : out t_mem_miso;

    xaui_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso           : out t_mem_miso;

    reg_eth10g_mosi     : in  t_mem_mosi := c_mem_mosi_rst;  -- ETH10G (link status register)
    reg_eth10g_miso     : out t_mem_miso;

    mdio_mosi_arr       : in  t_mem_mosi_arr(g_nof_macs - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr       : out t_mem_miso_arr(g_nof_macs - 1 downto 0);

    reg_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_10gbase_r_24_miso   : out t_mem_miso;

    -- DP interface
    dp_rst              : in  std_logic := '0';
    dp_clk              : in  std_logic := '0';
    dp_pps              : in  std_logic := '0';

    snk_out_arr         : out t_dp_siso_arr(g_nof_macs - 1 downto 0);
    snk_in_arr          : in  t_dp_sosi_arr(g_nof_macs - 1 downto 0) := (others => c_dp_sosi_rst);

    src_in_arr          : in  t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr         : out t_dp_sosi_arr(g_nof_macs - 1 downto 0);

    -- Serial XAUI IO
    xaui_tx_arr         : out t_xaui_arr(g_nof_macs - 1 downto 0);
    xaui_rx_arr         : in  t_xaui_arr(g_nof_macs - 1 downto 0) := (others => (others => '0'));

    -- Serial IO
    serial_tx_arr       : out std_logic_vector(g_nof_macs - 1 downto 0);
    serial_rx_arr       : in  std_logic_vector(g_nof_macs - 1 downto 0) := (others => '0');

    -- MDIO interface
    mdio_rst            : out std_logic;
    mdio_mdc_arr        : out std_logic_vector(g_nof_macs - 1 downto 0);
    mdio_mdat_in_arr    : in  std_logic_vector(g_nof_macs - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr   : out std_logic_vector(g_nof_macs - 1 downto 0);

    hdr_fields_in_arr   : in  t_slv_1024_arr(g_nof_macs - 1 downto 0)
  );
end nw_10GbE;

architecture str of nw_10GbE is
  constant c_nof_mux_streams : natural := 3;

  signal nw_arp_request_src_out_arr : t_dp_sosi_arr(g_nof_macs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nw_arp_request_src_in_arr  : t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);

  signal nw_ping_response_src_out_arr : t_dp_sosi_arr(g_nof_macs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nw_ping_response_src_in_arr  : t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);

  signal dp_mux_snk_in_2arr  : t_dp_sosi_2arr_3(g_nof_macs - 1 downto 0) := (others => (others => c_dp_sosi_rst));
  signal dp_mux_snk_out_2arr : t_dp_siso_2arr_3(g_nof_macs - 1 downto 0);

  signal tr_10GbE_snk_in_arr  : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal tr_10GbE_snk_out_arr : t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);
  signal tr_10GbE_src_out_arr : t_dp_sosi_arr(g_nof_macs - 1 downto 0);
  signal tr_10GbE_src_in_arr  : t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);

  signal eth_src_mac_arr : t_slv_48_arr(g_nof_macs - 1 downto 0);
  signal ip_src_addr_arr : t_slv_32_arr(g_nof_macs - 1 downto 0);
  signal ip_dst_addr_arr : t_slv_32_arr(g_nof_macs - 1 downto 0);
begin
  src_out_arr <= tr_10GbE_src_out_arr;
  tr_10GbE_src_in_arr <= src_in_arr;

  gen_nof_macs : for I in 0 to g_nof_macs - 1 generate
    -- Wire hdr_fields_in_arr
    eth_src_mac_arr(I) <= hdr_fields_in_arr(I)(field_hi(g_ip_hdr_field_arr, "eth_src_mac") downto field_lo(g_ip_hdr_field_arr, "eth_src_mac"));
    ip_src_addr_arr(I) <= hdr_fields_in_arr(I)(field_hi(g_ip_hdr_field_arr, "ip_src_addr") downto field_lo(g_ip_hdr_field_arr, "ip_src_addr"));
    ip_dst_addr_arr(I) <= hdr_fields_in_arr(I)(field_hi(g_ip_hdr_field_arr, "ip_dst_addr") downto field_lo(g_ip_hdr_field_arr, "ip_dst_addr"));

    ---------------------------------------------------------------------------------------
    -- ARP request
    ---------------------------------------------------------------------------------------
    u_nw_arp_request : entity work.nw_arp_request
    generic map (
      g_period_s => g_arp_period_s
    )
    port map (
      dp_pps => dp_pps,
      dp_clk => dp_clk,
      dp_rst => dp_rst,

      src_out => nw_arp_request_src_out_arr(I),
      src_in  => nw_arp_request_src_in_arr(I),

      arp_sha => eth_src_mac_arr(I),
      arp_spa => ip_src_addr_arr(I),
      arp_tpa => ip_dst_addr_arr(I)
    );

    ---------------------------------------------------------------------------------------
    -- PING response
    ---------------------------------------------------------------------------------------
    u_nw_ping_response : entity work.nw_ping_response
    generic map (
      g_technology => g_technology
    )
    port map (
      clk => dp_clk,
      rst => dp_rst,

      snk_in  => tr_10GbE_src_out_arr(I),

      src_in  => nw_ping_response_src_in_arr(I),
      src_out => nw_ping_response_src_out_arr(I),

      eth_src_mac => eth_src_mac_arr(I)
    );

    ---------------------------------------------------------------------------------------
    -- dp_mux to multiplex the three possible data streams
    ---------------------------------------------------------------------------------------
    dp_mux_snk_in_2arr(I)(0) <= snk_in_arr(I);
    dp_mux_snk_in_2arr(I)(1) <= nw_ping_response_src_out_arr(I);
    dp_mux_snk_in_2arr(I)(2) <= nw_arp_request_src_out_arr(I);

    snk_out_arr(I)                 <= dp_mux_snk_out_2arr(I)(0);
    nw_ping_response_src_in_arr(I) <= dp_mux_snk_out_2arr(I)(1);
    nw_arp_request_src_in_arr(I)   <= dp_mux_snk_out_2arr(I)(2);

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      g_nof_input => c_nof_mux_streams,
      g_sel_ctrl_invert => true,
      g_fifo_size => array_init(0, c_nof_mux_streams),  -- no FIFO used but must match g_nof_input
      g_fifo_fill => array_init(0, c_nof_mux_streams)  -- no FIFO used but must match g_nof_input
    )
    port map (
      clk => dp_clk,
      rst => dp_rst,

      snk_in_arr  => dp_mux_snk_in_2arr(I),
      snk_out_arr => dp_mux_snk_out_2arr(I),

      src_out => tr_10GbE_snk_in_arr(I),
      src_in  => tr_10GbE_snk_out_arr(I)
    );
  end generate;

  -------------
  -- tr_10GbE
  -------------
  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
  generic map (
    g_technology             => g_technology,
    g_sim                    => g_sim,
    g_sim_level              => g_sim_level,
    g_nof_macs               => g_nof_macs,
    g_direction              => g_direction,
    g_use_mdio               => g_use_mdio,
    g_mdio_epcs_dis          => g_mdio_epcs_dis,
    g_tx_fifo_fill           => g_tx_fifo_fill,
    g_tx_fifo_size           => g_tx_fifo_size,
    g_rx_fifo_size           => g_rx_fifo_size,
    g_word_alignment_padding => g_word_alignment_padding,
    g_xon_backpressure       => g_xon_backpressure
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644      => tr_ref_clk_644,
    tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

    -- Calibration & reconfig clock
    cal_rec_clk         => cal_rec_clk,  -- for XAUI;

    -- MM interface
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mac_mosi        => reg_mac_mosi,
    reg_mac_miso        => reg_mac_miso,

    xaui_mosi           => xaui_mosi,
    xaui_miso           => xaui_miso,

    reg_eth10g_mosi     => reg_eth10g_mosi,
    reg_eth10g_miso     => reg_eth10g_miso,

    mdio_mosi_arr       => mdio_mosi_arr,
    mdio_miso_arr       => mdio_miso_arr,

    reg_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
    reg_10gbase_r_24_miso => reg_10gbase_r_24_miso,

    -- DP interface
    dp_rst              => dp_rst,
    dp_clk              => dp_clk,

    src_out_arr         => tr_10GbE_src_out_arr,
    src_in_arr          => tr_10GbE_src_in_arr,

    snk_out_arr         => tr_10GbE_snk_out_arr,
    snk_in_arr          => tr_10GbE_snk_in_arr,

    -- Serial XAUI IO
    xaui_tx_arr         => xaui_tx_arr,
    xaui_rx_arr         => xaui_rx_arr,

    -- Serial IO
    serial_tx_arr       => serial_tx_arr,
    serial_rx_arr       => serial_rx_arr,

    -- MDIO interface
    mdio_rst            => mdio_rst,
    mdio_mdc_arr        => mdio_mdc_arr,
    mdio_mdat_in_arr    => mdio_mdat_in_arr,
    mdio_mdat_oen_arr   => mdio_mdat_oen_arr
  );
end str;
