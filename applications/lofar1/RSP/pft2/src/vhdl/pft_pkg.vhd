-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Copy of pft(pkg).vhd to avoid () in file name.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package pft_pkg is
  constant c_pft_twiddle_w   : natural := 16;
  constant c_pft_stage_dat_w : natural := 20;

  type pft_mode_type is (
    PFT_MODE_BITREV,
    PFT_MODE_COMPLEX,
    PFT_MODE_REAL2
  );

  type pft_bf_type is (
    PFT_BF1,
    PFT_BF2
  );
end pft_pkg;

package body pft_pkg is
end pft_pkg;
