--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Provide streaming interface to DDR memory
-- Description:
--   Write or read a block of data to or from DDR memory. The data width is set
--   by the DDR controller data width given by RESIZE_MEM_CTLR_DATA() and eg.
--   256 bits for DDR3 with 64 bit DQ data. The block of data is located from
--   dvr_start_address to dvr_nof_data.
--   The io_ddr_driver takes care that the access is done in a number of bursts.
--   The burst size for both write and read depends on the maximum burst size
--   and the remaining block size.
-- Remarks:
-- . Both this driver and the DDR IP controller use the t_mem_ctlr_miso/mosi
--   interface. The maximum burst size of the controller is defined by
--   g_tech_ddr.maxburstsize and eg. 64 ctlr data words. The maximum burst size
--   of this driver is as large as the entire ctlr address span. The burst size
--   of driver depends on the block size of the application.
-- . The DDR IP has a command queue dept of eg 4 for DDR3 and 8 for DDR4.
--   - In the simulation for DDR3 it seems that the queue is only 1 deep for
--     both wr and rd. The p_state state machine does not support multiple
--     write bursts, because it uses burst_wr_cnt. For read the p_state
--     statemachine could generate multiple rd bursts, but then the DDR3 IP
--     the waitrequest_n remains low until the end of each rd burst.
--   - For DDR4 IP the waitrequest_n remains active during most of the write
--     access so apparently it could accept mutliple wr bursts, but these are
--     not issued due to burst_wr_cnt mechanism in wr_burst state.
--     For DDR4 IP the waitrequest_n does allow 3 rd bursts, so then care must
--     be taken that the external read FIFO can indeed handle these bursts, as
--     set by the almost full margin that drives rd_src_in.ready. Alternatively
--     it could become necessary to also add a rd_burst state that with
--     burst_rd_cnt, to ensure that a new rd burst is only issued when the
--     previous has finished so that the read FIFO can not run full. In that
--     case the burst_wr_cnt and burst_rd_cnt could be combined in a single
--     counter, because they are not used at the same time. The read FIFO can
--     run full if the rd data width is smaller than the ctlr data width and/or
--     if the rd side can notread on every rd_clk cycle.

library IEEE, tech_ddr_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity io_ddr_driver is
  generic (
    g_tech_ddr         : t_c_tech_ddr
  );
  port (
    rst                : in  std_logic;
    clk                : in  std_logic;

    dvr_miso           : out t_mem_ctlr_miso;
    dvr_mosi           : in  t_mem_ctlr_mosi;

    wr_snk_in          : in  t_dp_sosi;
    wr_snk_out         : out t_dp_siso;

    rd_src_out         : out t_dp_sosi;
    rd_src_in          : in  t_dp_siso;

    ctlr_miso          : in  t_mem_ctlr_miso;
    ctlr_mosi          : out t_mem_ctlr_mosi
   );
end io_ddr_driver;

architecture str of io_ddr_driver is
  constant c_ctlr_address_w     : natural := func_tech_ddr_ctlr_address_w(g_tech_ddr);

  type t_state_enum is (s_init, s_idle, s_wait, s_rd_request, s_wr_request, s_wr_burst);

  signal state                  : t_state_enum;
  signal nxt_state              : t_state_enum;
  signal prev_state             : t_state_enum;

  signal dvr_en                 : std_logic;
  signal dvr_wr_not_rd          : std_logic;
  signal dvr_start_address      : std_logic_vector(c_ctlr_address_w - 1 downto 0);
  signal dvr_nof_data           : std_logic_vector(c_ctlr_address_w - 1 downto 0);
  signal dvr_done               : std_logic := '0';
  signal nxt_dvr_done           : std_logic;

  signal burstbegin             : std_logic;
  signal burstbegin_evt         : std_logic;
  signal burst_size             : positive range 1 to 2**g_tech_ddr.maxburstsize_w - 1 := 1;  -- burst size >= 1
  signal nxt_burst_size         : positive;
  signal burst_wr_cnt           : natural  range 0 to 2**g_tech_ddr.maxburstsize_w - 1 := 0;  -- count down from burst_size to 0
  signal nxt_burst_wr_cnt       : natural  range 0 to 2**g_tech_ddr.maxburstsize_w - 1;

  signal cur_address            : std_logic_vector(c_ctlr_address_w - 1 downto 0) := (others => '0');
  signal nxt_cur_address        : std_logic_vector(c_ctlr_address_w - 1 downto 0);
  signal address_cnt            : std_logic_vector(c_ctlr_address_w - 1 downto 0) := (others => '0');  -- count down nof addresses = nof ctlr data words
  signal nxt_address_cnt        : std_logic_vector(c_ctlr_address_w - 1 downto 0);
  signal address_cnt_is_0       : std_logic;
  signal nxt_address_cnt_is_0   : std_logic;
begin
  -- Map original dvr interface signals to t_mem_ctlr_mosi/miso
  dvr_miso.done     <= dvr_done;  -- Requested wr or rd sequence is done
  dvr_en            <= dvr_mosi.burstbegin;
  dvr_wr_not_rd     <= dvr_mosi.wr;  -- No need to use dvr_mosi.rd
  dvr_start_address <= dvr_mosi.address(c_ctlr_address_w - 1 downto 0);
  dvr_nof_data      <= dvr_mosi.burstsize(c_ctlr_address_w - 1 downto 0);

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      state          <= s_init;
      prev_state     <= s_init;
    elsif rising_edge(clk) then
      state            <= nxt_state;
      prev_state       <= state;
      burst_wr_cnt     <= nxt_burst_wr_cnt;
      dvr_done         <= nxt_dvr_done;
      cur_address      <= nxt_cur_address;
      address_cnt      <= nxt_address_cnt;
      address_cnt_is_0 <= nxt_address_cnt_is_0;
      burst_size       <= nxt_burst_size;
    end if;
  end process;

  -- Using ctrl_mosi.burstbegin <= burstbegin simulates ok with tb_tb_io_ddr.vhd.
  -- However according to the uniphy external memory interface user guide the ctrl_mosi.burstbegin should only be assered for one clock cycle, independent of ctrl_miso.wait_request_n.
  -- Therefore use burstbegin_evt to assert ctrl_mosi.burstbegin.
  u_common_evt : entity common_lib.common_evt
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => burstbegin,
    out_evt  => burstbegin_evt
  );

  p_burst_size : process (address_cnt)
  begin
    -- Access burst size is at least 1 and if more then set to the smallest of g_tech_ddr.maxburstsize and address_cnt
    nxt_burst_size <= 1;
    if unsigned(address_cnt) > 0 then
      nxt_burst_size <= g_tech_ddr.maxburstsize;
      if unsigned(address_cnt) < g_tech_ddr.maxburstsize then
        nxt_burst_size <= TO_UINT(address_cnt);
      end if;
    end if;
  end process;

  nxt_address_cnt_is_0 <= '1' when unsigned(nxt_address_cnt) = 0 else '0';

  rd_src_out.valid <= ctlr_miso.rdval;
  rd_src_out.data <= RESIZE_DP_DATA(ctlr_miso.rddata);

  wr_snk_out.xon <= ctlr_miso.done;  -- xon when controller init is done so ready for access

  p_state : process(prev_state, state,
                    dvr_en, dvr_wr_not_rd, dvr_start_address, dvr_nof_data,
                    ctlr_miso, wr_snk_in, rd_src_in,
                    burstbegin_evt, burst_size, burst_wr_cnt, cur_address, address_cnt, address_cnt_is_0)
  begin
    nxt_state              <= state;
    ctlr_mosi.address      <= RESIZE_MEM_CTLR_ADDRESS(cur_address);  -- no need to hold during burst, because the Avalon constantBurstBehaviour=FALSE (default) of the DDR IP slave
    ctlr_mosi.wrdata       <= RESIZE_MEM_CTLR_DATA(wr_snk_in.data);
    ctlr_mosi.wr           <= '0';
    ctlr_mosi.rd           <= '0';
    burstbegin             <= '0';  -- use burstbegin and burstbegin_evt to assert ctrl_mosi.burstbegin for one clock cylce only, independent of ctrl_miso.wait_request_n
    ctlr_mosi.burstbegin   <= burstbegin_evt;  -- only used for legacy DDR controllers, because the controller can derive it internally by counting wr and rd accesses
    ctlr_mosi.burstsize    <= TO_MEM_CTLR_BURSTSIZE(burst_size);  -- burstsize >= 1,
                                                                     -- no need to hold during burst, because the Avalon constantBurstBehaviour=FALSE (default) of the DDR IP slave
    wr_snk_out.ready       <= '0';
    nxt_dvr_done           <= '0';
    nxt_cur_address        <= cur_address;
    nxt_address_cnt        <= address_cnt;
    nxt_burst_wr_cnt       <= burst_wr_cnt;

    case state is

      when s_wr_burst =>  -- Performs the rest of burst when burst_size > 1
        if wr_snk_in.valid = '1' then  -- it is allowed that valid is not always active during a burst
          ctlr_mosi.wr <= '1';
          if ctlr_miso.waitrequest_n = '1' then
            wr_snk_out.ready <= '1';  -- wr side uses latency of 0, so wr_snk_out.ready<='1' acknowledges a successful write request.
            nxt_burst_wr_cnt <= burst_wr_cnt - 1;
            if burst_wr_cnt = 1 then  -- check for the last cycle of this burst sequence
              nxt_state <= s_wr_request;  -- initiate a new wr burst or goto idle via the wr_request state, simulation shows going directly idle by checking address_cnt_is_0 here does not save a cycle
            end if;
          end if;
        end if;

      when s_wr_request =>  -- Performs 1 write access and goes into s_wr_burst when requested burst_size >1
        if address_cnt_is_0 = '1' then  -- end address reached
          nxt_dvr_done  <= '1';
          nxt_state     <= s_idle;
        elsif wr_snk_in.valid = '1' then
          ctlr_mosi.wr  <= '1';
          burstbegin    <= '1';  -- assert ctlr_mosi.burstbegin for one clock cycle only
          if ctlr_miso.waitrequest_n = '1' then
            -- Always perform 1st write here
            wr_snk_out.ready     <= '1';
            nxt_cur_address      <= INCR_UVEC(cur_address, burst_size);
            nxt_address_cnt      <= INCR_UVEC(address_cnt, -burst_size);
            nxt_burst_wr_cnt     <= burst_size-1;
            -- Return for next wr request or perform any remaining writes in this burst
            nxt_state <= s_wait;
            if burst_size > 1 then
              nxt_state <= s_wr_burst;  -- first burst wr cycle is done here, the rest are done in s_wr_burst
            end if;
          end if;
        end if;

      when s_rd_request =>  -- Posts a read request for a burst (1...g_tech_ddr.maxburstsize)
        if address_cnt_is_0 = '1' then  -- end address reached
          nxt_dvr_done  <= '1';
          nxt_state     <= s_idle;
        elsif rd_src_in.ready = '1' then  -- the external FIFO uses almost full level assert its snk_out.ready and can then still accept the maximum rd burst of words
          ctlr_mosi.rd  <= '1';
          burstbegin    <= '1';  -- assert ctlr_mosi.burstbegin for one clock cycle only
          if ctlr_miso.waitrequest_n = '1' then
            nxt_cur_address      <= INCR_UVEC(cur_address, burst_size);
            nxt_address_cnt      <= INCR_UVEC(address_cnt, -burst_size);
            -- Return for next rd request
            nxt_state <= s_wait;
          end if;
        end if;

      -- In this state address_cnt is valid and in the next state burst_size (that depends on address_cnt) will be valid.
      -- Therefore this wait state is inserted between any requests.
      when s_wait =>
        if prev_state = s_wr_request then nxt_state <= s_wr_request; end if;  -- between wr-wr burst requests
        if prev_state = s_rd_request then nxt_state <= s_rd_request; end if;  -- between rd-rd burst requests
        if prev_state = s_idle then  -- between wr and rd accesses
          if dvr_wr_not_rd = '1' then
            nxt_state <= s_wr_request;
          else
            nxt_state <= s_rd_request;
          end if;
        end if;

      when s_idle =>
        nxt_cur_address  <= dvr_start_address;
        nxt_address_cnt  <= dvr_nof_data;
        nxt_burst_wr_cnt <= 0;
        nxt_dvr_done <= '1';  -- assert dvr_done after s_init or keep it asserted after a finished access
        if dvr_en = '1' then
          nxt_dvr_done <= '0';
          nxt_state    <= s_wait;
        end if;

      when others =>  -- s_init
        nxt_dvr_done <= '0';
        if ctlr_miso.done = '1' then
          nxt_state <= s_idle;  -- and assert dvr_done when in s_idle to indicate ctlr_miso.done
        end if;
    end case;
  end process;
end str;
