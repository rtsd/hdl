-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_request is
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    sync        : in  std_logic := '1';
    in_req      : in  std_logic;
    out_req_evt : out std_logic
  );
end common_request;

-- Request control:
-- . All inputs and outputs are registered
-- . A new request is indicated by in_req going from 0 to 1
-- . The pending request gets issued immediately or when the optional sync
--   pulse occurs
-- . The output request is a pulse as indicated by the postfix '_evt'

architecture rtl of common_request is
  signal sync_reg        : std_logic;
  signal in_req_reg      : std_logic;
  signal in_req_prev     : std_logic;
  signal in_req_evt      : std_logic;
  signal req_pending     : std_logic;
  signal out_req         : std_logic;
  signal out_req_prev    : std_logic;
  signal nxt_out_req_evt : std_logic;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      -- input
      sync_reg <= '0';
      in_req_reg <= '0';
      -- internal
      in_req_prev <= '0';
      out_req_prev <= '0';
      -- output
      out_req_evt <= '0';
    elsif rising_edge(clk) then
      if clken = '1' then
        -- input
        sync_reg <= sync;
        in_req_reg <= in_req;
        -- internal
        in_req_prev <= in_req_reg;
        out_req_prev <= out_req;
        -- output
        out_req_evt <= nxt_out_req_evt;
      end if;
    end if;
  end process;

  in_req_evt <= in_req_reg and not in_req_prev;

  u_protocol_act : entity work.common_switch
  port map (
    rst         => rst,
    clk         => clk,
    clken       => clken,
    switch_high => in_req_evt,
    switch_low  => out_req,
    out_level   => req_pending
  );

  out_req <= req_pending and sync_reg;
  nxt_out_req_evt <= out_req and not out_req_prev;
end rtl;
