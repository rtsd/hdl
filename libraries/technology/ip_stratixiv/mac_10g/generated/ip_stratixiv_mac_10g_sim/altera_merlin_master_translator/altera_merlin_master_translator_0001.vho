--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

--synthesis_resources = lut 1 mux21 4 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_merlin_master_translator_0001 IS 
	 PORT 
	 ( 
		 av_address	:	IN  STD_LOGIC_VECTOR (12 DOWNTO 0);
		 av_read	:	IN  STD_LOGIC;
		 av_readdata	:	OUT  STD_LOGIC_VECTOR (31 DOWNTO 0);
		 av_waitrequest	:	OUT  STD_LOGIC;
		 av_write	:	IN  STD_LOGIC;
		 av_writedata	:	IN  STD_LOGIC_VECTOR (31 DOWNTO 0);
		 clk	:	IN  STD_LOGIC;
		 reset	:	IN  STD_LOGIC;
		 uav_address	:	OUT  STD_LOGIC_VECTOR (14 DOWNTO 0);
		 uav_burstcount	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 uav_byteenable	:	OUT  STD_LOGIC_VECTOR (3 DOWNTO 0);
		 uav_debugaccess	:	OUT  STD_LOGIC;
		 uav_lock	:	OUT  STD_LOGIC;
		 uav_read	:	OUT  STD_LOGIC;
		 uav_readdata	:	IN  STD_LOGIC_VECTOR (31 DOWNTO 0);
		 uav_readdatavalid	:	IN  STD_LOGIC;
		 uav_waitrequest	:	IN  STD_LOGIC;
		 uav_write	:	OUT  STD_LOGIC;
		 uav_writedata	:	OUT  STD_LOGIC_VECTOR (31 DOWNTO 0)
	 ); 
 END altera_merlin_master_translator_0001;

 ARCHITECTURE RTL OF altera_merlin_master_translator_0001 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nO_w51w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_av_waitrequest_183m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_177m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_170m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_181m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_w_lg_reset258w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_uav_readdatavalid257w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_uav_waitrequest255w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
 BEGIN

	wire_w_lg_reset258w(0) <= NOT reset;
	wire_w_lg_uav_readdatavalid257w(0) <= NOT uav_readdatavalid;
	wire_w_lg_uav_waitrequest255w(0) <= NOT uav_waitrequest;
	av_readdata <= ( uav_readdata(31 DOWNTO 0));
	av_waitrequest <= wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_av_waitrequest_183m_dataout;
	s_wire_vcc <= '1';
	uav_address <= ( av_address(12 DOWNTO 0) & "0" & "0");
	uav_burstcount <= ( "1" & "0" & "0");
	uav_byteenable <= ( "1" & "1" & "1" & "1");
	uav_debugaccess <= '0';
	uav_lock <= '0';
	uav_read <= (av_read AND wire_nO_w51w(0));
	uav_write <= av_write;
	uav_writedata <= ( av_writedata(31 DOWNTO 0));
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
				altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q <= wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_181m_dataout;
		END IF;
	END PROCESS;
	wire_nO_w51w(0) <= NOT altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q;
	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_av_waitrequest_183m_dataout <= wire_w_lg_uav_readdatavalid257w(0) WHEN av_read = '1'  ELSE uav_waitrequest;
	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_177m_dataout <= altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q AND NOT((uav_readdatavalid AND altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q));
	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_170m_dataout <= (wire_w_lg_uav_waitrequest255w(0) AND av_read) AND wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_av_waitrequest_183m_dataout;
	wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_181m_dataout <= wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_177m_dataout WHEN altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_186q = '1'  ELSE wire_altera_merlin_master_translator_0001_altera_merlin_master_translator_merlin_master_translator_read_accepted_170m_dataout;

 END RTL; --altera_merlin_master_translator_0001
--synopsys translate_on
--VALID FILE
