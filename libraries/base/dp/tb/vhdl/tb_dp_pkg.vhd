-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;

package tb_dp_pkg is
  ------------------------------------------------------------------------------
  -- Purpose:
  --
  -- Test bench package for applying stimuli to a streaming data path. The
  -- input is counter data, the output is verified and an error is reported
  -- if a counter value is missing or duplicate.
  --
  -- Description:
  --
  -- The test is divided into intervals marked by sync to start a new subtest
  -- named by state. New subtests can be added by adding an extra sync interval
  -- and state name to this package. In each subtest the streaming interface
  -- DUT can be verified for different situations by manipulating:
  -- . cnt_en    : cnt_en not always active when in_ready is asserted
  -- . out_ready : out_ready not always active
  --
  -- Remarks:
  -- . See e.g. tb_dp_pipeline.vhd for how to use the procedures.
  -- . To run all stimuli in Modelsim do:
  --   > as 10
  --   > run 400 us
  ------------------------------------------------------------------------------

  constant clk_period         : time := 10 ns;  -- 100 MHz
  constant c_dp_sync_interval : natural := 3000;
  constant c_dp_test_interval : natural := 100;
  constant c_dp_nof_toggle    : natural := 40;
  constant c_dp_nof_both      : natural := 50;

  -- The test bench uses other field widths than the standard t_dp_sosi record field widths, the assumptions are:
  -- . c_dp_data_w < c_dp_stream_data_w
  -- . c_dp_data_w > c_dp_stream_empty_w
  -- . c_dp_data_w > c_dp_stream_channel_w
  -- . c_dp_data_w > c_dp_stream_error_w
  constant c_dp_data_w            : natural := c_word_w;  -- =32, choose wide enough to avoid out_data wrap around issue for p_verify
  constant c_dp_bsn_w             : natural := c_dp_data_w;  -- c_dp_stream_bsn_w;
  constant c_dp_empty_w           : natural := c_dp_stream_empty_w;
  constant c_dp_channel_w         : natural := c_dp_stream_channel_w;
  constant c_dp_channel_user_w    : natural := c_dp_stream_channel_w / 2;  -- support some bits for mux input user streams channel widths
  constant c_dp_channel_mux_w     : natural := (c_dp_stream_channel_w + 1) / 2;  -- support rest bits for the nof input ports of a mux
  constant c_dp_error_w           : natural := c_dp_stream_error_w;

  type t_dp_data_arr is array (natural range <>) of std_logic_vector(c_dp_data_w - 1 downto 0);

  -- The state name tells what kind of test is done in the sync interval
  type t_dp_state_enum is (
    s_idle,
    s_both_active,
    s_pull_down_out_ready,
    s_pull_down_cnt_en,
    s_toggle_out_ready,
    s_toggle_cnt_en,
    s_toggle_both,
    s_pulse_cnt_en,
    s_chirp_out_ready,
    s_random,
    s_done
  );

  type t_dp_value_enum is (
    e_equal,
    e_at_least
  );

  -- always active, random or pulse flow control
  type t_dp_flow_control_enum is (
    e_active,
    e_random,
    e_pulse
  );

  type t_dp_flow_control_enum_arr is array (natural range <>) of t_dp_flow_control_enum;

  constant c_dp_flow_control_enum_arr : t_dp_flow_control_enum_arr := (e_active, e_random, e_pulse);  -- array all possible values that can be iterated over

  ------------------------------------------------------------------------------
  -- Stream source functions
  ------------------------------------------------------------------------------

  -- Block data generator with feedforward throttle control
  -- !!! old style: sync before sop
  -- !!! used by tb_dp_packetizing, do not use for new DP components
  procedure proc_dp_gen_block_data(constant c_nof_block_per_sync : in    natural;
                                   constant c_block_size         : in    natural;
                                   constant c_gap_size           : in    natural;
                                   constant c_throttle_num       : in    natural;
                                   constant c_throttle_den       : in    natural;
                                   signal   rst                  : in    std_logic;
                                   signal   clk                  : in    std_logic;
                                   signal   sync_nr              : inout natural;
                                   signal   block_nr             : inout natural;
                                   signal   cnt_sync             : out   std_logic;
                                   signal   cnt_val              : out   std_logic;
                                   signal   cnt_dat              : inout std_logic_vector);

  -- Block data generator with ready flow control and symbols counter
  procedure proc_dp_gen_block_data(constant c_ready_latency  : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                   constant c_use_data       : in  boolean;  -- when TRUE use data field, else use re, im fields, and keep unused fields at 'X'
                                   constant c_data_w         : in  natural;  -- data width for the data, re and im fields
                                   constant c_symbol_w       : in  natural;  -- c_data_w/c_symbol_w must be an integer
                                   constant c_symbol_init    : in  natural;  -- init counter for symbols in data field
                                   constant c_symbol_re_init : in  natural;  -- init counter for symbols in re field
                                   constant c_symbol_im_init : in  natural;  -- init counter for symbols in im field
                                   constant c_nof_symbols    : in  natural;  -- nof symbols per frame for the data, re and im fields
                                   constant c_channel        : in  natural;  -- channel field
                                   constant c_error          : in  natural;  -- error field
                                   constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                   constant c_bsn            : in  std_logic_vector;  -- bsn field
                                   signal   clk              : in  std_logic;
                                   signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                   signal   src_in           : in  t_dp_siso;
                                   signal   src_out          : out t_dp_sosi);

  -- Increment only sosi.data, keep complex re,im = 0
  procedure proc_dp_gen_block_data(constant c_data_w         : in  natural;  -- data width for the data field
                                   constant c_symbol_init    : in  natural;  -- init counter for the data in the data field
                                   constant c_nof_symbols    : in  natural;  -- nof symbols = nof data per frame for the data fields
                                   constant c_channel        : in  natural;  -- channel field
                                   constant c_error          : in  natural;  -- error field
                                   constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                   constant c_bsn            : in  std_logic_vector;  -- bsn field
                                   signal   clk              : in  std_logic;
                                   signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                   signal   src_in           : in  t_dp_siso;
                                   signal   src_out          : out t_dp_sosi);

  -- Increment only sosi.re, im, keep data = 0
  procedure proc_dp_gen_block_complex(constant c_data_w         : in  natural;  -- data width for the re, im field
                                      constant c_symbol_re_init : in  natural;  -- init counter for symbols in re field
                                      constant c_symbol_im_init : in  natural;  -- init counter for symbols in im field
                                      constant c_nof_symbols    : in  natural;  -- nof symbols = nof data per frame for the data fields
                                      constant c_channel        : in  natural;  -- channel field
                                      constant c_error          : in  natural;  -- error field
                                      constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                      constant c_bsn            : in  std_logic_vector;  -- bsn field
                                      signal   clk              : in  std_logic;
                                      signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                      signal   src_in           : in  t_dp_siso;
                                      signal   src_out          : out t_dp_sosi);

  -- Handle stream ready signal, only support RL=0 or 1.
  procedure proc_dp_stream_ready_latency(constant c_latency : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   ready     : in  std_logic;
                                         signal   in_en     : in  std_logic;  -- when '1' then active output when ready
                                         constant c_sync    : in  std_logic;
                                         constant c_valid   : in  std_logic;
                                         constant c_sop     : in  std_logic;
                                         constant c_eop     : in  std_logic;
                                         signal   out_sync  : out std_logic;
                                         signal   out_valid : out std_logic;
                                         signal   out_sop   : out std_logic;
                                         signal   out_eop   : out std_logic);

  -- Initialize the data per symbol
  function func_dp_data_init(c_data_w, c_symbol_w, init : natural) return std_logic_vector;

  -- Increment the data per symbol
  function func_dp_data_incr(c_data_w, c_symbol_w : natural; data : std_logic_vector) return std_logic_vector;

  -- Generate a counter data with valid
  procedure proc_dp_gen_data(constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                             constant c_data_w        : in  natural;
                             constant c_data_init     : in  natural;
                             signal   rst             : in  std_logic;
                             signal   clk             : in  std_logic;
                             signal   in_en           : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                             signal   src_in          : in  t_dp_siso;
                             signal   src_out         : out t_dp_sosi);

  -- As above but with counter max
  procedure proc_dp_gen_data(constant c_ready_latency   : in  natural;
                             constant c_data_w          : in  natural;
                             constant c_data_init       : in  natural;
                             constant c_data_max        : in  natural;
                             signal   rst             : in  std_logic;
                             signal   clk             : in  std_logic;
                             signal   in_en           : in  std_logic;
                             signal   src_in          : in  t_dp_siso;
                             signal   src_out         : out t_dp_sosi);

  -- Generate a frame with symbols counter
  procedure proc_dp_gen_frame(constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                              constant c_data_w        : in  natural;
                              constant c_symbol_w      : in  natural;  -- c_data_w/c_symbol_w must be an integer
                              constant c_symbol_init   : in  natural;
                              constant c_nof_symbols   : in  natural;
                              constant c_bsn           : in  natural;
                              constant c_sync          : in  std_logic;
                              signal   clk             : in  std_logic;
                              signal   in_en           : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                              signal   src_in          : in  t_dp_siso;
                              signal   src_out         : out t_dp_sosi);

  -- Input data counter
  procedure proc_dp_cnt_dat(signal rst     : in    std_logic;
                            signal clk     : in    std_logic;
                            signal in_en   : in    std_logic;
                            signal cnt_dat : inout std_logic_vector);

  procedure proc_dp_cnt_dat(signal rst     : in    std_logic;
                            signal clk     : in    std_logic;
                            signal in_en   : in    std_logic;
                            signal cnt_val : inout std_logic;
                            signal cnt_dat : inout std_logic_vector);

  -- Transmit data
  procedure proc_dp_tx_data(constant c_ready_latency : in    natural;
                            signal   rst             : in    std_logic;
                            signal   clk             : in    std_logic;
                            signal   cnt_val         : in    std_logic;
                            signal   cnt_dat         : in    std_logic_vector;
                            signal   tx_data         : inout t_dp_data_arr;
                            signal   tx_val          : inout std_logic_vector;
                            signal   out_data        : out   std_logic_vector;
                            signal   out_val         : out   std_logic);

  -- Transmit data control (use for sop, eop)
  procedure proc_dp_tx_ctrl(constant c_offset : in  natural;
                            constant c_period : in  natural;
                            signal   data     : in  std_logic_vector;
                            signal   valid    : in  std_logic;
                            signal   ctrl     : out std_logic);

  -- Define sync interval
  procedure proc_dp_sync_interval(signal clk  : in  std_logic;
                                  signal sync : out std_logic);

  -- Stimuli for cnt_en
  procedure proc_dp_count_en(signal rst    : in    std_logic;
                             signal clk    : in    std_logic;
                             signal sync   : in    std_logic;
                             signal lfsr   : inout std_logic_vector;
                             signal state  : out   t_dp_state_enum;
                             signal done   : out   std_logic;
                             signal tb_end : out   std_logic;
                             signal cnt_en : out   std_logic);

  ------------------------------------------------------------------------------
  -- Stream sink functions
  ------------------------------------------------------------------------------

  -- Stimuli for out_ready
  procedure proc_dp_out_ready(signal rst       : in    std_logic;
                              signal clk       : in    std_logic;
                              signal sync      : in    std_logic;
                              signal lfsr      : inout std_logic_vector;
                              signal out_ready : out   std_logic);

  -- DUT output verify enable
  procedure proc_dp_verify_en(constant c_delay   : in  natural;
                              signal   rst       : in  std_logic;
                              signal   clk       : in  std_logic;
                              signal   sync      : in  std_logic;
                              signal   verify_en : out std_logic);

  procedure proc_dp_verify_en(constant c_continuous : in  boolean;
                              signal   clk          : in  std_logic;
                              signal   valid        : in  std_logic;
                              signal   sop          : in  std_logic;
                              signal   eop          : in  std_logic;
                              signal   verify_en    : out std_logic);

  -- Run and verify for some cycles
  procedure proc_dp_verify_run_some_cycles(constant nof_pre_clk    : in   natural;
                                           constant nof_verify_clk : in   natural;
                                           constant nof_post_clk   : in   natural;
                                           signal   clk            : in   std_logic;
                                           signal   verify_en      : out  std_logic);

  -- Verify the expected value
  procedure proc_dp_verify_value(constant c_str : in string;
                                 constant mode  : in t_dp_value_enum;
                                 signal   clk   : in std_logic;
                                 signal   en    : in std_logic;
                                 signal   exp   : in std_logic_vector;
                                 signal   res   : in std_logic_vector);

  procedure proc_dp_verify_value(constant mode : in t_dp_value_enum;
                                 signal   clk  : in std_logic;
                                 signal   en   : in std_logic;
                                 signal   exp  : in std_logic_vector;
                                 signal   res  : in std_logic_vector);

  procedure proc_dp_verify_value(constant c_str : in string;
                                 signal   clk   : in std_logic;
                                 signal   en    : in std_logic;
                                 signal   exp   : in std_logic;
                                 signal   res   : in std_logic);

  -- Verify output global and local BSN
  -- . incrementing or replicated global BSN
  -- . incrementing local BSN that starts at 1
  procedure proc_dp_verify_bsn(constant c_use_local_bsn             : in    boolean;  -- use local BSN or only use global BSN
                               constant c_global_bsn_increment      : in    positive;  -- increment per global BSN
                               constant c_nof_replicated_global_bsn : in    positive;  -- number of replicated global BSN
                               constant c_block_per_sync            : in    positive;  -- of sop/eop blocks per sync interval
                               signal   clk                         : in    std_logic;
                               signal   out_sync                    : in    std_logic;
                               signal   out_sop                     : in    std_logic;
                               signal   out_bsn                     : in    std_logic_vector;
                               signal   verify_en                   : inout std_logic;  -- initialize '0', becomes '1' when bsn verification starts
                               signal   cnt_replicated_global_bsn   : inout natural;
                               signal   prev_out_bsn_global         : inout std_logic_vector;
                               signal   prev_out_bsn_local          : inout std_logic_vector);

  -- Verify incrementing data
  -- . wrap at c_out_data_max when >0, else no wrap when c_out_data_max=0
  -- . default increment by +1, but also allow an increment by +c_out_data_gap
  --   or +c_out_data_gap2.
  -- . by using sop or eop for out_val input, the proc_dp_verify_data() can
  --   also be used to verify other SOSI fields like bsn, error, channel, empty
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                constant c_out_data_gap2 : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  -- Verify the DUT incrementing output data that wraps in range 0 ... c_out_data_max
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  -- Verify the DUT incrementing output data, fixed increment +1
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  -- Verify incrementing data with RL > 0 or no flow control, support wrap at maximum and increment gap
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                constant c_out_data_gap2 : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                constant c_out_data_gap  : in    natural;
                                constant c_out_data_gap2 : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                constant c_out_data_gap  : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  -- Verify incrementing data with RL > 0 or no flow control, fixed increment +1
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector);

  -- Verify the DUT output symbols
  procedure proc_dp_verify_symbols(constant c_ready_latency : in    natural;
                                   constant c_data_w        : in    natural;
                                   constant c_symbol_w      : in    natural;
                                   signal   clk             : in    std_logic;
                                   signal   verify_en       : in    std_logic;
                                   signal   out_ready       : in    std_logic;
                                   signal   out_val         : in    std_logic;
                                   signal   out_eop         : in    std_logic;
                                   signal   out_data        : in    std_logic_vector;
                                   signal   out_empty       : in    std_logic_vector;
                                   signal   prev_out_data   : inout std_logic_vector);

  -- Verify the DUT output data with empty
  procedure proc_dp_verify_data_empty(constant c_ready_latency : in    natural;
                                      constant c_last_word     : in    natural;
                                      signal   clk             : in    std_logic;
                                      signal   verify_en       : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   out_eop_1       : inout std_logic;
                                      signal   out_eop_2       : inout std_logic;
                                      signal   out_data        : in    std_logic_vector;
                                      signal   out_data_1      : inout std_logic_vector;
                                      signal   out_data_2      : inout std_logic_vector;
                                      signal   out_data_3      : inout std_logic_vector;
                                      signal   out_empty       : in    std_logic_vector;
                                      signal   out_empty_1     : inout std_logic_vector);

  procedure proc_dp_verify_other_sosi(constant c_str       : in string;
                                      constant c_exp_data  : in std_logic_vector;
                                      signal   clk         : in std_logic;
                                      signal   verify_en   : in std_logic;
                                      signal   res_data    : in std_logic_vector);

  procedure proc_dp_verify_sosi_equal(constant c_str       : in string;
                                      signal   clk         : in std_logic;
                                      signal   verify_en   : in std_logic;
                                      signal   dut_sosi    : in t_dp_sosi_integer;  -- use func_dp_stream_slv_to_integer for conversion
                                      signal   exp_sosi    : in t_dp_sosi_integer);  -- use func_dp_stream_slv_to_integer for conversion

  procedure proc_dp_verify_sosi_equal(constant c_use_complex : in boolean;
                                      signal   dut_sosi      : in t_dp_sosi;
                                      signal   exp_sosi      : in t_dp_sosi);

  procedure proc_dp_verify_valid(constant c_ready_latency : in    natural;
                                 signal   clk             : in    std_logic;
                                 signal   verify_en       : in    std_logic;
                                 signal   out_ready       : in    std_logic;
                                 signal   prev_out_ready  : inout std_logic_vector;
                                 signal   out_val         : in    std_logic);

  procedure proc_dp_verify_valid(signal   clk             : in    std_logic;
                                 signal   verify_en       : in    std_logic;
                                 signal   out_ready       : in    std_logic;
                                 signal   prev_out_ready  : inout std_logic;
                                 signal   out_val         : in    std_logic);

  -- Verify the DUT output sync
  procedure proc_dp_verify_sync(signal   clk           : in    std_logic;
                                signal   verify_en     : in    std_logic;
                                signal   sync          : in    std_logic;
                                signal   sop           : in    std_logic;
                                         expected_sync : in    std_logic);

  procedure proc_dp_verify_sync(signal   clk           : in    std_logic;
                                signal   verify_en     : in    std_logic;
                                signal   sync          : in    std_logic;
                                signal   sop           : in    std_logic;
                                         bsn           : in    natural;  -- for reporting
                                         expected_bsn  : in    natural;  -- for reporting
                                         expected_sync : in    std_logic);
    -- Note: A SIGNAL IN can only connect a SIGNAL. Therefore define IN as
    --       default (= CONSTANT) instead of SIGNAL to be able to connect
    --       VARIABLE or SIGNAL.

  procedure proc_dp_verify_sync(constant c_sync_period : in    natural;
                                constant c_sync_offset : in    natural;
                                signal   clk           : in    std_logic;
                                signal   verify_en     : in    std_logic;
                                signal   sync          : in    std_logic;
                                signal   sop           : in    std_logic;
                                signal   bsn           : in    std_logic_vector);

  procedure proc_dp_verify_sync(constant c_start_bsn      : in    natural;
                                constant c_sync_period    : in    natural;
                                constant c_block_size     : in    natural;
                                signal   clk              : in    std_logic;
                                signal   verify_en        : in    std_logic;
                                signal   sync             : in    std_logic;
                                signal   sop              : in    std_logic;
                                signal   bsn              : in    std_logic_vector;
                                -- for debug purposes
                                signal   dbg_nof_blk      : out   natural;
                                signal   dbg_accumulate   : out   natural;
                                signal   dbg_expected_bsn : out   natural);

  procedure proc_dp_verify_sync(constant c_start_bsn      : in    natural;
                                constant c_sync_period    : in    natural;
                                constant c_block_size     : in    natural;
                                constant c_bsn_is_rsn     : in    boolean;  -- increment BSN by 1 or by c_block_size for RSN
                                signal   clk              : in    std_logic;
                                signal   verify_en        : in    std_logic;
                                signal   sync             : in    std_logic;
                                signal   sop              : in    std_logic;
                                signal   bsn              : in    std_logic_vector;
                                -- for debug purposes
                                signal   dbg_nof_blk      : out   natural;
                                signal   dbg_accumulate   : out   natural;
                                signal   dbg_expected_bsn : out   natural);

  -- Verify the DUT output sop and eop
  procedure proc_dp_verify_sop_and_eop(constant c_ready_latency : in    natural;
                                       constant c_verify_valid  : in    boolean;
                                       signal   clk             : in    std_logic;
                                       signal   out_ready       : in    std_logic;
                                       signal   out_val         : in    std_logic;
                                       signal   out_sop         : in    std_logic;
                                       signal   out_eop         : in    std_logic;
                                       signal   hold_sop        : inout std_logic);

  procedure proc_dp_verify_sop_and_eop(constant c_ready_latency : in    natural;
                                       signal   clk             : in    std_logic;
                                       signal   out_ready       : in    std_logic;
                                       signal   out_val         : in    std_logic;
                                       signal   out_sop         : in    std_logic;
                                       signal   out_eop         : in    std_logic;
                                       signal   hold_sop        : inout std_logic);

  procedure proc_dp_verify_sop_and_eop(signal clk      : in    std_logic;
                                       signal out_val  : in    std_logic;
                                       signal out_sop  : in    std_logic;
                                       signal out_eop  : in    std_logic;
                                       signal hold_sop : inout std_logic);

  procedure proc_dp_verify_block_size(constant c_ready_latency : in    natural;
                                      signal   alt_size        : in    natural;  -- alternative size (eg. use exp_size'last_value)
                                      signal   exp_size        : in    natural;  -- expected size
                                      signal   clk             : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural);

  procedure proc_dp_verify_block_size(constant c_ready_latency : in    natural;
                                      signal   exp_size        : in    natural;  -- expected size
                                      signal   clk             : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural);

  procedure proc_dp_verify_block_size(signal alt_size : in    natural;  -- alternative size (eg. use exp_size'last_value)
                                      signal exp_size : in    natural;  -- expected size
                                      signal clk      : in    std_logic;
                                      signal out_val  : in    std_logic;
                                      signal out_sop  : in    std_logic;
                                      signal out_eop  : in    std_logic;
                                      signal cnt_size : inout natural);

  procedure proc_dp_verify_block_size(signal exp_size : in    natural;  -- expected size
                                      signal clk      : in    std_logic;
                                      signal out_val  : in    std_logic;
                                      signal out_sop  : in    std_logic;
                                      signal out_eop  : in    std_logic;
                                      signal cnt_size : inout natural);

  -- Verify the DUT output invalid between frames
  procedure proc_dp_verify_gap_invalid(signal clk     : in    std_logic;
                                       signal in_val  : in    std_logic;
                                       signal in_sop  : in    std_logic;
                                       signal in_eop  : in    std_logic;
                                       signal out_gap : inout std_logic);  -- declare initial gap signal = '1'

  -- Verify the DUT output control (use for sop, eop)
  procedure proc_dp_verify_ctrl(constant c_offset  : in natural;
                                constant c_period  : in natural;
                                constant c_str     : in string;
                                signal   clk       : in std_logic;
                                signal   verify_en : in std_logic;
                                signal   data      : in std_logic_vector;
                                signal   valid     : in std_logic;
                                signal   ctrl      : in std_logic);

  -- Wait for stream valid
  procedure proc_dp_stream_valid(signal clk      : in  std_logic;
                                 signal in_valid : in  std_logic);

  -- Wait for stream valid AND sop
  procedure proc_dp_stream_valid_sop(signal clk      : in  std_logic;
                                     signal in_valid : in  std_logic;
                                     signal in_sop   : in  std_logic);

  -- Wait for stream valid AND eop
  procedure proc_dp_stream_valid_eop(signal clk      : in  std_logic;
                                     signal in_valid : in  std_logic;
                                     signal in_eop   : in  std_logic);
end tb_dp_pkg;

package body tb_dp_pkg is
  ------------------------------------------------------------------------------
  -- PROCEDURE: Block data generator with feedforward throttle control
  ------------------------------------------------------------------------------
  procedure proc_dp_gen_block_data(constant c_nof_block_per_sync : in    natural;
                                   constant c_block_size         : in    natural;
                                   constant c_gap_size           : in    natural;
                                   constant c_throttle_num       : in    natural;
                                   constant c_throttle_den       : in    natural;
                                   signal   rst                  : in    std_logic;
                                   signal   clk                  : in    std_logic;
                                   signal   sync_nr              : inout natural;
                                   signal   block_nr             : inout natural;
                                   signal   cnt_sync             : out   std_logic;
                                   signal   cnt_val              : out   std_logic;
                                   signal   cnt_dat              : inout std_logic_vector) is
    constant c_start_delay : natural := 10;
    variable v_throttle    : natural;
  begin
    sync_nr  <= 0;
    block_nr <= 0;

    cnt_sync <= '0';
    cnt_val  <= '0';
    cnt_dat  <= (cnt_dat'range => '1');  -- -1, so first valid cnt_dat starts at 0

    -- allow some clock cycles before start after rst release
    wait until rst = '0';
    for I in 0 to c_start_delay - 1 loop wait until rising_edge(clk); end loop;

    -- output first sync
    cnt_sync <= '1';
    wait until rising_edge(clk);
    cnt_sync <= '0';
    for I in 1 to c_gap_size-1 loop wait until rising_edge(clk); end loop;

    while true loop
      -- output block
      if c_throttle_num >= c_throttle_den then
        -- no need to throttle, so cnt_val active during whole data block
        for I in 0 to c_block_size-1 loop
          cnt_val <= '1';
          cnt_dat <= INCR_UVEC(cnt_dat, 1);
          wait until rising_edge(clk);
        end loop;

      else
        -- throttle cnt_val, so c_throttle_num active cnt_val cycles per c_throttle_den cycles
        for I in 0 to c_block_size / c_throttle_num - 1 loop
          for J in 0 to c_throttle_num - 1 loop
            cnt_val <= '1';
            cnt_dat <= INCR_UVEC(cnt_dat, 1);
            wait until rising_edge(clk);
          end loop;
          for J in 0 to c_throttle_den - c_throttle_num - 1 loop
            cnt_val <= '0';
            wait until rising_edge(clk);
          end loop;
        end loop;
      end if;
      cnt_val <= '0';
      -- output sync for next block at first sample of gap
      if block_nr > 0 and ((block_nr + 1) mod c_nof_block_per_sync) = 0 then
        cnt_sync <= '1';
        sync_nr  <= sync_nr + 1;
      end if;
      wait until rising_edge(clk);
      -- output rest of the gap
      cnt_sync <= '0';
      for I in 1 to c_gap_size-1 loop wait until rising_edge(clk); end loop;
      -- next block
      block_nr <= block_nr + 1;
    end loop;
  end proc_dp_gen_block_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Block data generator with ready flow control and symbols counter
  -- . dependent on in_en and src_in.ready
  -- . optional sync pulse at end of frame
  ------------------------------------------------------------------------------
  procedure proc_dp_gen_block_data(constant c_ready_latency  : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                   constant c_use_data       : in  boolean;  -- when TRUE use data field, else use re, im fields, and keep unused fields at 'X'
                                   constant c_data_w         : in  natural;  -- data width for the data, re and im fields
                                   constant c_symbol_w       : in  natural;  -- c_data_w/c_symbol_w must be an integer
                                   constant c_symbol_init    : in  natural;  -- init counter for symbols in data field
                                   constant c_symbol_re_init : in  natural;  -- init counter for symbols in re field
                                   constant c_symbol_im_init : in  natural;  -- init counter for symbols in im field
                                   constant c_nof_symbols    : in  natural;  -- nof symbols per frame for the data, re and im fields
                                   constant c_channel        : in  natural;  -- channel field
                                   constant c_error          : in  natural;  -- error field
                                   constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                   constant c_bsn            : in  std_logic_vector;  -- bsn field
                                   signal   clk              : in  std_logic;
                                   signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                   signal   src_in           : in  t_dp_siso;
                                   signal   src_out          : out t_dp_sosi) is
    constant c_nof_symbols_per_data : natural := c_data_w / c_symbol_w;
    constant c_div                  : natural := c_nof_symbols   / c_nof_symbols_per_data;
    constant c_mod                  : natural := c_nof_symbols mod c_nof_symbols_per_data;
    constant c_empty                : natural := sel_a_b(c_mod, c_nof_symbols_per_data - c_mod, 0);
    constant c_nof_data             : natural := sel_a_b(c_mod, 1, 0) + c_div;
    variable v_data                 : std_logic_vector(c_data_w - 1 downto 0) := func_dp_data_init(c_data_w, c_symbol_w, c_symbol_init);
    variable v_re                   : std_logic_vector(c_data_w - 1 downto 0) := func_dp_data_init(c_data_w, c_symbol_w, c_symbol_re_init);
    variable v_im                   : std_logic_vector(c_data_w - 1 downto 0) := func_dp_data_init(c_data_w, c_symbol_w, c_symbol_im_init);
  begin
    src_out <= c_dp_sosi_rst;
    if src_in.xon = '1' then
      -- Generate this block
      src_out.bsn     <= RESIZE_DP_BSN(c_bsn);
      src_out.empty   <= TO_DP_EMPTY(c_empty);
      src_out.channel <= TO_DP_CHANNEL(c_channel);
      src_out.err     <= TO_DP_ERROR(c_error);
      if c_use_data = true  then src_out.data  <= RESIZE_DP_DATA(v_data);   end if;
      if c_use_data = false then src_out.re    <= RESIZE_DP_DSP_UDATA(v_re); end if;
      if c_use_data = false then src_out.im    <= RESIZE_DP_DSP_UDATA(v_im); end if;
      if c_nof_data > 1 then
        -- . sop
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, c_sync, '1', '1', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
        -- . valid
        for I in 1 to c_nof_data - 2 loop
          v_data := func_dp_data_incr(c_data_w, c_symbol_w, v_data);
          v_re   := func_dp_data_incr(c_data_w, c_symbol_w, v_re);
          v_im   := func_dp_data_incr(c_data_w, c_symbol_w, v_im);
          if c_use_data = true  then src_out.data <= RESIZE_DP_DATA(v_data);   end if;
          if c_use_data = false then src_out.re   <= RESIZE_DP_DSP_UDATA(v_re); end if;
          if c_use_data = false then src_out.im   <= RESIZE_DP_DSP_UDATA(v_im); end if;
          proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
        end loop;

        -- . eop
        v_data := func_dp_data_incr(c_data_w, c_symbol_w, v_data);
        v_re   := func_dp_data_incr(c_data_w, c_symbol_w, v_re);
        v_im   := func_dp_data_incr(c_data_w, c_symbol_w, v_im);
        if c_use_data = true  then src_out.data <= RESIZE_DP_DATA(v_data);   end if;
        if c_use_data = false then src_out.re   <= RESIZE_DP_DSP_UDATA(v_re); end if;
        if c_use_data = false then src_out.im   <= RESIZE_DP_DSP_UDATA(v_im); end if;
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '1', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
      else
        -- . sop and eop, frame has only one word
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, c_sync, '1', '1', '1', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
      end if;
    else
      -- Skip this block
      proc_common_wait_some_cycles(clk, c_nof_data);
    end if;
  end proc_dp_gen_block_data;

  -- Increment only sosi.data, keep complex re,im = 0
  procedure proc_dp_gen_block_data(constant c_data_w         : in  natural;  -- data width for the data field
                                   constant c_symbol_init    : in  natural;  -- init counter for the data in the data field
                                   constant c_nof_symbols    : in  natural;  -- nof symbols per frame for the data fields
                                   constant c_channel        : in  natural;  -- channel field
                                   constant c_error          : in  natural;  -- error field
                                   constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                   constant c_bsn            : in  std_logic_vector;  -- bsn field
                                   signal   clk              : in  std_logic;
                                   signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                   signal   src_in           : in  t_dp_siso;
                                   signal   src_out          : out t_dp_sosi) is
  begin
    proc_dp_gen_block_data(1, true, c_data_w, c_data_w, c_symbol_init, 0, 0, c_nof_symbols, c_channel, c_error, c_sync, c_bsn, clk, in_en, src_in, src_out);
  end proc_dp_gen_block_data;

  -- Increment only sosi.re, im, keep data = 0
  procedure proc_dp_gen_block_complex(constant c_data_w         : in  natural;  -- data width for the re, im field
                                      constant c_symbol_re_init : in  natural;  -- init counter for symbols in re field
                                      constant c_symbol_im_init : in  natural;  -- init counter for symbols in im field
                                      constant c_nof_symbols    : in  natural;  -- nof symbols per frame for the data fields
                                      constant c_channel        : in  natural;  -- channel field
                                      constant c_error          : in  natural;  -- error field
                                      constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
                                      constant c_bsn            : in  std_logic_vector;  -- bsn field
                                      signal   clk              : in  std_logic;
                                      signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                                      signal   src_in           : in  t_dp_siso;
                                      signal   src_out          : out t_dp_sosi) is
  begin
    proc_dp_gen_block_data(1, false, c_data_w, c_data_w, 0, c_symbol_re_init, c_symbol_im_init, c_nof_symbols, c_channel, c_error, c_sync, c_bsn, clk, in_en, src_in, src_out);
  end proc_dp_gen_block_complex;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Handle stream ready signal
  -- . output active when src_in is ready and in_en='1'
  -- . only support RL=0 or 1, support for RL>1 requires keeping previous ready information in a STD_LOGIC_VECTOR(RL-1 DOWNTO 0).
  ------------------------------------------------------------------------------
  procedure proc_dp_stream_ready_latency(constant c_latency : in  natural;
                                         signal   clk       : in  std_logic;
                                         signal   ready     : in  std_logic;
                                         signal   in_en     : in  std_logic;
                                         constant c_sync    : in  std_logic;
                                         constant c_valid   : in  std_logic;
                                         constant c_sop     : in  std_logic;
                                         constant c_eop     : in  std_logic;
                                         signal   out_sync  : out std_logic;
                                         signal   out_valid : out std_logic;
                                         signal   out_sop   : out std_logic;
                                         signal   out_eop   : out std_logic) is
  begin
    -- Default no output
    out_sync  <= '0';
    out_valid <= '0';
    out_sop   <= '0';
    out_eop   <= '0';

    -- Skip cycles until in_en='1'
    while in_en = '0' loop
      wait until rising_edge(clk);
    end loop;

    -- Active output when ready
    -- . RL = 0
    if c_latency = 0 then
      -- show the available output until acknowledge
      out_sync  <= c_sync;
      out_valid <= c_valid;
      out_sop   <= c_sop;
      out_eop   <= c_eop;
      wait until rising_edge(clk);
      while ready /= '1' loop
        wait until rising_edge(clk);
      end loop;
      -- ready has acknowledged the valid output
    end if;

    -- . RL = 1
    if c_latency = 1 then
      -- no valid output until request
      while ready /= '1' loop
        wait until rising_edge(clk);
      end loop;
      -- ready has requested this valid output
      out_sync  <= c_sync;
      out_valid <= c_valid;
      out_sop   <= c_sop;
      out_eop   <= c_eop;
      wait until rising_edge(clk);
    end if;

    -- Return with no active output
    out_sync  <= '0';
    out_valid <= '0';
    out_sop   <= '0';
    out_eop   <= '0';
  end proc_dp_stream_ready_latency;

  ------------------------------------------------------------------------------
  -- FUNCTION: Initialize the data per symbol
  -- . use big endian
  -- . if c_data_w=32, c_symbol_w=8, init=3 then return 0x03040506
  ------------------------------------------------------------------------------
  function func_dp_data_init(c_data_w, c_symbol_w, init : natural) return std_logic_vector is
    constant c_nof_symbols_per_data : natural := c_data_w / c_symbol_w;
    variable v_data                 : std_logic_vector(c_data_w - 1 downto 0);
    variable v_sym                  : std_logic_vector(c_symbol_w - 1 downto 0);
  begin
    v_data := (others => '0');
    v_sym  := TO_UVEC(init, c_symbol_w);
    for I in c_nof_symbols_per_data - 1 downto 0 loop
      v_data((I + 1) * c_symbol_w - 1 downto I * c_symbol_w) := v_sym;
      v_sym := INCR_UVEC(v_sym, 1);
    end loop;
    return v_data;
  end func_dp_data_init;

  ------------------------------------------------------------------------------
  -- FUNCTION: Increment the data per symbol
  -- . use big endian
  -- . if c_data_w=32, c_symbol_w=8 then 0x00010203 returns 0x04050607
  -- . the actual data'LENGTH must be >= c_data_w, unused bits become 0
  -- . c_data_w/c_symbol_w must be an integer
  ------------------------------------------------------------------------------
  function func_dp_data_incr(c_data_w, c_symbol_w : natural; data : std_logic_vector) return std_logic_vector is
    constant c_nof_symbols_per_data : natural := c_data_w / c_symbol_w;
    variable v_data                 : std_logic_vector(data'length - 1 downto 0);
    variable v_sym                  : std_logic_vector(c_symbol_w - 1 downto 0);
  begin
    v_data := (others => '0');
    v_sym  := data(c_symbol_w - 1 downto 0);
    for I in c_nof_symbols_per_data - 1 downto 0 loop
      v_sym := INCR_UVEC(v_sym, 1);
      v_data((I + 1) * c_symbol_w - 1 downto I * c_symbol_w) := v_sym;
    end loop;
    return v_data;
  end func_dp_data_incr;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate counter data with valid
  -- . Output counter data dependent on in_en and src_in.ready
  ------------------------------------------------------------------------------
  procedure proc_dp_gen_data(constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                             constant c_data_w        : in  natural;
                             constant c_data_init     : in  natural;
                             signal   rst             : in  std_logic;
                             signal   clk             : in  std_logic;
                             signal   in_en           : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                             signal   src_in          : in  t_dp_siso;
                             signal   src_out         : out t_dp_sosi) is
    variable v_data : std_logic_vector(c_data_w - 1 downto 0) := TO_UVEC(c_data_init, c_data_w);
  begin
    src_out      <= c_dp_sosi_rst;
    src_out.data <= RESIZE_DP_DATA(v_data);
    if rst = '0' then
      wait until rising_edge(clk);
      while true loop
        src_out.data <= RESIZE_DP_DATA(v_data);
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
        v_data := INCR_UVEC(v_data, 1);
      end loop;
    end if;
  end proc_dp_gen_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate counter data with valid
  -- . Output counter data dependent on in_en and src_in.ready
  -- . with maximum count value
  ------------------------------------------------------------------------------
  procedure proc_dp_gen_data(constant c_ready_latency   : in  natural;
                             constant c_data_w          : in  natural;
                             constant c_data_init       : in  natural;
                             constant c_data_max        : in  natural;
                             signal   rst               : in  std_logic;
                             signal   clk               : in  std_logic;
                             signal   in_en             : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                             signal   src_in            : in  t_dp_siso;
                             signal   src_out           : out t_dp_sosi) is
    variable v_cnt     : std_logic_vector(c_data_w - 1 downto 0) := TO_UVEC(c_data_init, c_data_w);
  begin
    src_out         <= c_dp_sosi_rst;
    src_out.data    <= RESIZE_DP_DATA(v_cnt);
    if rst = '0' then
      wait until rising_edge(clk);
      while true loop
        src_out.data    <= RESIZE_DP_DATA(v_cnt);
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
        if TO_UINT(v_cnt) = c_data_max then
          v_cnt := TO_UVEC(c_data_init, c_data_w);
        else
          v_cnt := INCR_UVEC(v_cnt, 1);
        end if;
      end loop;
    end if;
  end proc_dp_gen_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate a frame with symbols counter
  -- . dependent on in_en and src_in.ready
  ------------------------------------------------------------------------------
  procedure proc_dp_gen_frame(constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                              constant c_data_w        : in  natural;
                              constant c_symbol_w      : in  natural;  -- c_data_w/c_symbol_w must be an integer
                              constant c_symbol_init   : in  natural;
                              constant c_nof_symbols   : in  natural;
                              constant c_bsn           : in  natural;
                              constant c_sync          : in  std_logic;
                              signal   clk             : in  std_logic;
                              signal   in_en           : in  std_logic;  -- when '0' then no valid output even when src_in is ready
                              signal   src_in          : in  t_dp_siso;
                              signal   src_out         : out t_dp_sosi) is
    constant c_nof_symbols_per_data : natural := c_data_w / c_symbol_w;
    constant c_div                  : natural := c_nof_symbols   / c_nof_symbols_per_data;
    constant c_mod                  : natural := c_nof_symbols mod c_nof_symbols_per_data;
    constant c_empty                : natural := sel_a_b(c_mod, c_nof_symbols_per_data - c_mod, 0);
    constant c_nof_data             : natural := sel_a_b(c_mod, 1, 0) + c_div;
    variable v_data                 : std_logic_vector(c_data_w - 1 downto 0) := func_dp_data_init(c_data_w, c_symbol_w, c_symbol_init);
  begin
    src_out       <= c_dp_sosi_rst;
    src_out.bsn   <= TO_DP_BSN(c_bsn);
    src_out.empty <= TO_DP_EMPTY(c_empty);
    src_out.data  <= RESIZE_DP_DATA(v_data);
    if c_nof_data > 1 then
      -- . sop
      proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, c_sync, '1', '1', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
      -- . valid
      for I in 1 to c_nof_data - 2 loop
        v_data := func_dp_data_incr(c_data_w, c_symbol_w, v_data);
        src_out.data <= RESIZE_DP_DATA(v_data);
        proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '0', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
      end loop;
      -- . eop
      v_data := func_dp_data_incr(c_data_w, c_symbol_w, v_data);
      src_out.data <= RESIZE_DP_DATA(v_data);
      proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, '0', '1', '0', '1', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
    else
      -- . sop and eop, frame has only one word
      proc_dp_stream_ready_latency(c_ready_latency, clk, src_in.ready, in_en, c_sync, '1', '1', '1', src_out.sync, src_out.valid, src_out.sop, src_out.eop);
    end if;
    src_out.sync  <= '0';
    src_out.valid <= '0';
    src_out.sop   <= '0';
    src_out.eop   <= '0';
  end proc_dp_gen_frame;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Input data counter
  ------------------------------------------------------------------------------
  procedure proc_dp_cnt_dat(signal rst     : in    std_logic;
                            signal clk     : in    std_logic;
                            signal in_en   : in    std_logic;
                            signal cnt_dat : inout std_logic_vector) is
  begin
    if rst = '1' then
      cnt_dat <= (cnt_dat'range => '0');
    elsif rising_edge(clk) then
      if in_en = '1' then
        cnt_dat <= std_logic_vector(unsigned(cnt_dat) + 1);
      end if;
    end if;
  end proc_dp_cnt_dat;

  procedure proc_dp_cnt_dat(signal rst     : in    std_logic;
                            signal clk     : in    std_logic;
                            signal in_en   : in    std_logic;
                            signal cnt_val : inout std_logic;
                            signal cnt_dat : inout std_logic_vector) is
  begin
    if rst = '1' then
      cnt_val <= '0';
      cnt_dat <= (cnt_dat'range => '0');
    elsif rising_edge(clk) then
      cnt_val <= '0';
      if in_en = '1' then
        cnt_val <= '1';
        cnt_dat <= std_logic_vector(unsigned(cnt_dat) + 1);
      end if;
    end if;
  end proc_dp_cnt_dat;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Transmit data
  ------------------------------------------------------------------------------
  procedure proc_dp_tx_data(constant c_ready_latency : in    natural;
                            signal   rst             : in    std_logic;
                            signal   clk             : in    std_logic;
                            signal   cnt_val         : in    std_logic;
                            signal   cnt_dat         : in    std_logic_vector;
                            signal   tx_data         : inout t_dp_data_arr;
                            signal   tx_val          : inout std_logic_vector;
                            signal   out_data        : out   std_logic_vector;
                            signal   out_val         : out   std_logic) is
    constant c_void : natural := sel_a_b(c_ready_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_ready_latency=0
  begin
    -- TX data array for output ready latency [c_ready_latency], index [0] for zero latency combinatorial
    tx_data(0) <= cnt_dat;
    tx_val( 0) <= cnt_val;

    if rst = '1' then
      tx_data(1 to c_ready_latency + c_void) <= (1 to c_ready_latency + c_void => (others => '0'));
      tx_val( 1 to c_ready_latency + c_void) <= (1 to c_ready_latency + c_void => '0');
    elsif rising_edge(clk) then
      tx_data(1 to c_ready_latency + c_void) <= tx_data(0 to c_ready_latency + c_void - 1);
      tx_val( 1 to c_ready_latency + c_void) <= tx_val( 0 to c_ready_latency + c_void - 1);
    end if;

    out_data <= tx_data(c_ready_latency);
    out_val  <= tx_val(c_ready_latency);
  end proc_dp_tx_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Transmit data control (use for sop, eop)
  ------------------------------------------------------------------------------
  procedure proc_dp_tx_ctrl(constant c_offset : in  natural;
                            constant c_period : in  natural;
                            signal   data     : in  std_logic_vector;
                            signal   valid    : in  std_logic;
                            signal   ctrl     : out std_logic) is
    variable v_data : integer;
  begin
    v_data := TO_UINT(data);
    ctrl <= '0';
    if valid = '1' and ((v_data - c_offset) mod c_period) = 0 then
      ctrl <= '1';
    end if;
  end proc_dp_tx_ctrl;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Define test sync interval
  ------------------------------------------------------------------------------
  procedure proc_dp_sync_interval(signal clk  : in  std_logic;
                                  signal sync : out std_logic) is
  begin
    sync <= '0';
    for I in 1 to c_dp_sync_interval - 1 loop wait until rising_edge(clk); end loop;
    sync <= '1';
    wait until rising_edge(clk);
  end proc_dp_sync_interval;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Stimuli for cnt_en
  ------------------------------------------------------------------------------
  procedure proc_dp_count_en(signal rst    : in    std_logic;
                             signal clk    : in    std_logic;
                             signal sync   : in    std_logic;
                             signal lfsr   : inout std_logic_vector;
                             signal state  : out   t_dp_state_enum;
                             signal done   : out   std_logic;
                             signal tb_end : out   std_logic;
                             signal cnt_en : out   std_logic) is
  begin
    -- The counter operates at zero latency
    state <= s_idle;
    done <= '0';
    tb_end <= '0';
    cnt_en <= '0';
    wait until rst = '0';
    wait until rising_edge(clk);
    -- The cnt_val may be asserted for every active in_ready, but als support
    -- cnt_val not asserted for every asserted in_ready.

    ----------------------------------------------------------------------------
    -- Interval 1
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_both_active;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 2
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_pull_down_out_ready;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 3
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_pull_down_cnt_en;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 4 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 5 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 6 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 7 cycle
    cnt_en <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 4
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_toggle_out_ready;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 5
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_toggle_cnt_en;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-1 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-2 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-1 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-2 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-3 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3-1 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-3 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3-2 toggle
    cnt_en <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      cnt_en <= '1';
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 6
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_toggle_both;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    for I in 1 to c_dp_nof_both loop
      cnt_en <= '0';
      for J in 1 to I loop wait until rising_edge(clk); end loop;
      cnt_en <= '1';
      for J in I to c_dp_nof_both loop wait until rising_edge(clk); end loop;
    end loop;
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 7
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_pulse_cnt_en;
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    for I in 1 to 15 loop
      for J in 1 to 15 loop
        cnt_en <= '0';
        for K in 1 to I loop wait until rising_edge(clk); end loop;
        cnt_en <= '1';
        wait until rising_edge(clk);
      end loop;
      for J in 1 to 20 loop wait until rising_edge(clk); end loop;
    end loop;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 8
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_chirp_out_ready;
    cnt_en <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 9
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_random;
    cnt_en <= '1';

    for I in 0 to c_dp_sync_interval - c_dp_test_interval loop
      lfsr <= func_common_random(lfsr);
      cnt_en <= lfsr(lfsr'high);
      wait until rising_edge(clk);
    end loop;

    ----------------------------------------------------------------------------
    -- Done
    ----------------------------------------------------------------------------
    wait until sync = '1';
    state <= s_done;
    wait until rising_edge(clk);
    cnt_en <= '0';

    -- pulse done
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;
    done <= '1';
    wait until rising_edge(clk);
    done <= '0';

    ----------------------------------------------------------------------------
    -- Testbench end
    ----------------------------------------------------------------------------
    -- set tb_end
    wait until sync = '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;
    tb_end <= '1';
    wait;
  end proc_dp_count_en;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Stimuli for out_ready
  ------------------------------------------------------------------------------
  procedure proc_dp_out_ready(signal rst       : in    std_logic;
                              signal clk       : in    std_logic;
                              signal sync      : in    std_logic;
                              signal lfsr      : inout std_logic_vector;
                              signal out_ready : out   std_logic) is
  begin
    out_ready <= '0';
    wait until rst = '0';
    wait until rising_edge(clk);

    ----------------------------------------------------------------------------
    -- Interval 1 : Assert both cnt_en and out_ready
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 2 : Make out_ready low for 1 or more cycles
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 4 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 5 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 6 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 7 cycle
    out_ready <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 3
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 4 : Toggle out_ready for 1 or more cycles
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-1 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-2 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-1 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-2 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 1-3 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3-1 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 2-3 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . 3-2 toggle
    out_ready <= '0';
    for I in 1 to c_dp_nof_toggle loop
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      out_ready <= '1';
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 5
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 6
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    for I in 1 to c_dp_nof_both loop
      out_ready <= '0';
      for J in I to c_dp_nof_both loop wait until rising_edge(clk); end loop;
      out_ready <= '1';
      for J in 1 to I loop wait until rising_edge(clk); end loop;
    end loop;
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 7
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 8 : Chirp out_ready
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    -- . slow toggle
    out_ready <= '0';
    for I in 0 to c_dp_nof_toggle loop
      out_ready <= '0';
      for J in 0 to I loop wait until rising_edge(clk); end loop;
      out_ready <= '1';
      for J in 0 to I loop wait until rising_edge(clk); end loop;
    end loop;
    out_ready <= '1';
    for I in 0 to c_dp_test_interval loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Interval 9 : Random
    ----------------------------------------------------------------------------
    wait until sync = '1';
    out_ready <= '1';

    for I in 0 to c_dp_sync_interval - c_dp_test_interval loop
      lfsr <= func_common_random(lfsr);
      out_ready <= lfsr(lfsr'high);
      wait until rising_edge(clk);
    end loop;

    ----------------------------------------------------------------------------
    -- Done
    ----------------------------------------------------------------------------
    wait;
  end proc_dp_out_ready;

  ------------------------------------------------------------------------------
  -- PROCEDURE: DUT output verify enable
  ------------------------------------------------------------------------------

  -- Fixed delay until verify_en active
  procedure proc_dp_verify_en(constant c_delay   : in  natural;
                              signal   rst       : in  std_logic;
                              signal   clk       : in  std_logic;
                              signal   sync      : in  std_logic;
                              signal   verify_en : out std_logic) is
  begin
    verify_en <= '0';
    wait until rst = '0';
    wait until rising_edge(clk);

    wait until sync = '1';
    -- Use c_delay delay before enabling the p_verify.
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    verify_en <= '1';
    wait;
  end proc_dp_verify_en;

  -- Dynamicly depend on first valid data to make verify_en active
  procedure proc_dp_verify_en(constant c_continuous : in  boolean;
                              signal   clk          : in  std_logic;
                              signal   valid        : in  std_logic;
                              signal   sop          : in  std_logic;
                              signal   eop          : in  std_logic;
                              signal   verify_en    : out std_logic) is
  begin
    if rising_edge(clk) then
      if c_continuous = true then
        -- Verify across frames (so enable data verify after the first data has been output)
        if valid = '1' then
          verify_en <= '1';
        end if;
      else
        -- Verify only per frame (so re-enable data verify after the every sop)
        if eop = '1' then
          verify_en <= '0';
        elsif sop = '1' then
          verify_en <= '1';
        end if;
      end if;
    end if;
  end proc_dp_verify_en;

  -- Run and verify for some cycles
  procedure proc_dp_verify_run_some_cycles(constant nof_pre_clk    : in   natural;
                                           constant nof_verify_clk : in   natural;
                                           constant nof_post_clk   : in   natural;
                                           signal   clk            : in   std_logic;
                                           signal   verify_en      : out  std_logic) is
  begin
    proc_common_wait_some_cycles(clk, nof_pre_clk);
    verify_en <= '1';
    proc_common_wait_some_cycles(clk, nof_verify_clk);
    verify_en <= '0';
    proc_common_wait_some_cycles(clk, nof_post_clk);
  end proc_dp_verify_run_some_cycles;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the expected value
  ------------------------------------------------------------------------------
  --  e.g. to check that a test has ran at all
  procedure proc_dp_verify_value(constant c_str : in string;
                                 constant mode  : in t_dp_value_enum;
                                 signal   clk   : in std_logic;
                                 signal   en    : in std_logic;
                                 signal   exp   : in std_logic_vector;
                                 signal   res   : in std_logic_vector) is
  begin
    if rising_edge(clk) then
      if en = '1' then
        if mode = e_equal and unsigned(res) /= unsigned(exp) then
          report "DP : Wrong " & c_str & " result value"
            severity ERROR;
        end if;
        if mode = e_at_least and unsigned(res) < unsigned(exp) then
          report "DP : Wrong " & c_str & " result value too small"
            severity ERROR;
        end if;
      end if;
    end if;
  end proc_dp_verify_value;

  procedure proc_dp_verify_value(constant mode : in t_dp_value_enum;
                                 signal   clk  : in std_logic;
                                 signal   en   : in std_logic;
                                 signal   exp  : in std_logic_vector;
                                 signal   res  : in std_logic_vector) is
  begin
    proc_dp_verify_value("", mode, clk, en, exp, res);
  end proc_dp_verify_value;

  procedure proc_dp_verify_value(constant c_str : in string;
                                 signal   clk   : in std_logic;
                                 signal   en    : in std_logic;
                                 signal   exp   : in std_logic;
                                 signal   res   : in std_logic) is
  begin
    if rising_edge(clk) then
      if en = '1' then
        if res /= exp then
          report "DP : Wrong " & c_str & " result value"
            severity ERROR;
        end if;
      end if;
    end if;
  end proc_dp_verify_value;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify output global and local BSN
  ------------------------------------------------------------------------------
  -- Verify BSN:
  -- . incrementing or replicated global BSN
  -- . incrementing local BSN that starts at 1
  --
  --               _              _              _              _
  --  sync      __| |____________| |____________| |____________| |____________
  --               _    _    _    _    _    _    _    _    _    _    _    _
  --   sop      __| |__| |__| |__| |__| |__| |__| |__| |__| |__| |__| |__| |__  c_block_per_sync = 3
  --
  -- c_use_local_bsn = FALSE:
  --                                                                            c_nof_replicated_global_bsn = 1
  --        bsn    3    4    5    6    7    8    9    10   11   12   13   14    c_global_bsn_increment = 1
  --        bsn    3    5    7    9   11   13   15    17   19   21   22   23    c_global_bsn_increment = 2
  --
  -- c_use_local_bsn = TRUE:
  --
  -- global bsn    3              4              5               6              c_global_bsn_increment = 1, c_nof_replicated_global_bsn = 1
  -- global bsn    3              6              9              12              c_global_bsn_increment = 3, c_nof_replicated_global_bsn = 1
  -- global bsn    3              3              9               9              c_global_bsn_increment = 6, c_nof_replicated_global_bsn = 2
  --  local bsn    -    1    2    -    1    2    -    1    2     -    1    2    range 1:c_block_per_sync-1
  --
  -- The verify_en should initially be set to '0' and gets enabled when
  -- sufficient BSN history is available to do the verification.
  --
  procedure proc_dp_verify_bsn(constant c_use_local_bsn             : in    boolean;  -- use local BSN or only use global BSN
                               constant c_global_bsn_increment      : in    positive;  -- increment per global BSN
                               constant c_nof_replicated_global_bsn : in    positive;  -- number of replicated global BSN
                               constant c_block_per_sync            : in    positive;  -- of sop/eop blocks per sync interval
                               signal   clk                         : in    std_logic;
                               signal   out_sync                    : in    std_logic;
                               signal   out_sop                     : in    std_logic;
                               signal   out_bsn                     : in    std_logic_vector;
                               signal   verify_en                   : inout std_logic;  -- initialize '0', becomes '1' when bsn verification starts
                               signal   cnt_replicated_global_bsn   : inout natural;
                               signal   prev_out_bsn_global         : inout std_logic_vector;
                               signal   prev_out_bsn_local          : inout std_logic_vector) is
  begin
    if rising_edge(clk) then
      -- out_sop must be active, because only then out_bsn will differ from the previous out_bsn
      if out_sop = '1' then
        if c_use_local_bsn = false then
          ------------------------------------------------------------------
          -- Only use global BSN
          ------------------------------------------------------------------
          prev_out_bsn_global <= out_bsn;
          -- verify
          if  out_sync = '1' then
            verify_en <= '1';
          end if;
          if verify_en = '1' then
            assert unsigned(out_bsn) = unsigned(prev_out_bsn_global) + c_global_bsn_increment
              report "DP : Wrong BSN increment"
              severity ERROR;
          end if;
        else
          ------------------------------------------------------------------
          -- Use global and local BSN
          ------------------------------------------------------------------
          if out_sync = '1' then
            prev_out_bsn_global <= out_bsn;
            if unsigned(out_bsn) /= unsigned(prev_out_bsn_global) then
              verify_en <= '1';  -- wait until after last replicated global bsn
              cnt_replicated_global_bsn <= 0;
            else
              cnt_replicated_global_bsn <= cnt_replicated_global_bsn + 1;
            end if;
            prev_out_bsn_local <= TO_UVEC(0, prev_out_bsn_global'length);
          else
            prev_out_bsn_local <= out_bsn;
          end if;
          -- verify
          if verify_en = '1' then
            if out_sync = '1' then
              if unsigned(out_bsn) /= unsigned(prev_out_bsn_global) then
                assert cnt_replicated_global_bsn = c_nof_replicated_global_bsn - 1
                  report "DP : Wrong number of replicated global BSN"
                  severity ERROR;
                assert unsigned(out_bsn) = unsigned(prev_out_bsn_global) + c_global_bsn_increment
                  report "DP : Wrong global BSN increment"
                  severity ERROR;
              else
                assert unsigned(out_bsn) = unsigned(prev_out_bsn_global)
                  report "DP : Wrong replicated global BSN"
                  severity ERROR;
              end if;
              assert unsigned(prev_out_bsn_local) = c_block_per_sync - 1
                report "DP : Wrong last local BSN in sync interval"
                severity ERROR;
            else
              assert unsigned(out_bsn) = unsigned(prev_out_bsn_local) + 1
                report "DP : Wrong local BSN increment"
                severity ERROR;
            end if;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_bsn;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output data
  ------------------------------------------------------------------------------

  -- Verify incrementing data
  -- . wrap at c_out_data_max when >0, else no wrap when c_out_data_max=0
  -- . default increment by 1, but also allow an increment by c_out_data_gap
  -- . or c_out_data_gap2.
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                constant c_out_data_gap2 : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;  -- only needed when c_ready_latency = 0
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    if rising_edge(clk) then
      -- out_val must be active, because only the out_data will it differ from the previous out_data
      if out_val = '1' then
        -- for ready_latency > 0 out_val indicates new data
        -- for ready_latency = 0 out_val only indicates new data when it is confirmed by out_ready
        if c_ready_latency /= 0 or (c_ready_latency = 0 and out_ready = '1') then
          if c_out_data_max = 0 then
            prev_out_data <= out_data;  -- no wrap detection
          elsif unsigned(out_data) < c_out_data_max then
            prev_out_data <= out_data;  -- no wrap
          else
            prev_out_data <= TO_SVEC(-1, prev_out_data'length);  -- do wrap
          end if;
          if verify_en = '1' then
            -- Default check increment +1.
            -- also allow increment +c_out_data_gap or +c_out_data_gap2.
            -- also allow increment +c_out_data_gap wrapped by c_out_data_max.
            if unsigned(out_data) /= unsigned(prev_out_data) + 1 and
               unsigned(out_data) /= unsigned(prev_out_data) + c_out_data_gap and
               unsigned(out_data) /= unsigned(prev_out_data) + c_out_data_gap2 and
               unsigned(out_data) /= unsigned(prev_out_data) + c_out_data_gap - c_out_data_max then
              report "DP : Wrong out_data " & c_str & " count (" &
                     natural'image(to_uint(out_data)) & ", " &
                     natural'image(to_uint(prev_out_data)) & ")"
                severity ERROR;
            end if;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;  -- only needed when c_ready_latency = 0
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    proc_dp_verify_data(c_str, c_ready_latency,
                        c_out_data_max, c_out_data_gap, c_unsigned_1,
                        clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  -- Verify incrementing data that wraps in range 0 ... c_out_data_max
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                constant c_out_data_max  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    proc_dp_verify_data(c_str, c_ready_latency,
                        c_out_data_max, c_unsigned_1, c_unsigned_1,
                        clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  -- Verify incrementing data
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_ready_latency : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_ready       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    proc_dp_verify_data(c_str, c_ready_latency,
                        c_unsigned_0, c_unsigned_1, c_unsigned_1,
                        clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  -- Verify incrementing data with RL > 0 or no flow control
  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                constant c_out_data_gap2 : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    -- Use out_val as void signal to pass on to unused out_ready, because a
    -- signal input can not connect a constant or variable.
    proc_dp_verify_data(c_str, 1,
                        c_out_data_max, c_out_data_gap, c_out_data_gap2,
                        clk, verify_en, out_val, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    unsigned;
                                constant c_out_data_gap  : in    unsigned;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    proc_dp_verify_data(c_str,
                        c_out_data_max, c_out_data_gap, c_unsigned_1,
                        clk, verify_en, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                constant c_out_data_gap  : in    natural;
                                constant c_out_data_gap2 : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
    constant c_data_w : natural := out_data'length;
  begin
    proc_dp_verify_data(c_str,
                        to_unsigned(c_out_data_max, c_data_w),
                        to_unsigned(c_out_data_gap, c_data_w),
                        to_unsigned(c_out_data_gap2, c_data_w),
                        clk, verify_en, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                constant c_out_data_gap  : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
    constant c_data_w : natural := out_data'length;
  begin
    proc_dp_verify_data(c_str,
                        c_out_data_max, c_out_data_gap, 1,
                        clk, verify_en, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                constant c_out_data_max  : in    natural;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
    constant c_data_w : natural := out_data'length;
  begin
    proc_dp_verify_data(c_str,
                        c_out_data_max, 1,
                        clk, verify_en, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  procedure proc_dp_verify_data(constant c_str           : in    string;
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   out_val         : in    std_logic;
                                signal   out_data        : in    std_logic_vector;
                                signal   prev_out_data   : inout std_logic_vector) is
  begin
    proc_dp_verify_data(c_str, 0,
                        clk, verify_en, out_val, out_data, prev_out_data);
  end proc_dp_verify_data;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify incrementing symbols in data
  -- . for c_data_w = c_symbol_w proc_dp_verify_symbols() = proc_dp_verify_data()
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_symbols(constant c_ready_latency : in    natural;
                                   constant c_data_w        : in    natural;
                                   constant c_symbol_w      : in    natural;
                                   signal   clk             : in    std_logic;
                                   signal   verify_en       : in    std_logic;
                                   signal   out_ready       : in    std_logic;
                                   signal   out_val         : in    std_logic;
                                   signal   out_eop         : in    std_logic;
                                   signal   out_data        : in    std_logic_vector;
                                   signal   out_empty       : in    std_logic_vector;
                                   signal   prev_out_data   : inout std_logic_vector) is
    constant c_nof_symbols_per_data : natural := c_data_w / c_symbol_w;  -- must be an integer
    constant c_empty_w              : natural := ceil_log2(c_nof_symbols_per_data);
    variable v_data                 : std_logic_vector(c_data_w - 1 downto 0);
    variable v_symbol               : std_logic_vector(c_symbol_w - 1 downto 0);
    variable v_empty                : natural;
  begin
    if rising_edge(clk) then
      -- out_val must be active, because only the out_data will it differ from the previous out_data
      if out_val = '1' then
        -- for ready_latency > 0 out_val indicates new data
        -- for ready_latency = 0 out_val only indicates new data when it is confirmed by out_ready
        if c_ready_latency /= 0 or (c_ready_latency = 0 and out_ready = '1') then
          prev_out_data <= out_data;
          if verify_en = '1' then
            v_data  := prev_out_data(c_data_w - 1 downto 0);
            for I in 0 to c_nof_symbols_per_data - 1 loop
              v_data((I + 1) * c_symbol_w - 1 downto I * c_symbol_w) := INCR_UVEC(v_data((I + 1) * c_symbol_w - 1 downto I * c_symbol_w), c_nof_symbols_per_data);  -- increment each symbol
            end loop;
            if out_eop = '0' then
              if unsigned(out_data) /= unsigned(v_data) then
                report "DP : Wrong out_data symbols count"
                  severity ERROR;
              end if;
            else
              v_empty := TO_UINT(out_empty(c_empty_w - 1 downto 0));
              if unsigned(out_data(c_data_w - 1 downto v_empty * c_symbol_w)) /= unsigned(v_data(c_data_w - 1 downto v_empty * c_symbol_w)) then
                report "DP : Wrong out_data symbols count at eop"
                  severity ERROR;
              end if;
              if v_empty > 0 then
                -- adjust prev_out_data for potentially undefined empty symbols in out_data
                v_symbol := v_data((v_empty + 1) * c_symbol_w - 1 downto v_empty * c_symbol_w);  -- last valid symbol
                for I in 0 to c_nof_symbols_per_data - 1 loop
                  v_data((I + 1) * c_symbol_w - 1 downto I * c_symbol_w) := v_symbol;  -- put the last valid symbol at the end of the v_data
                  v_symbol := INCR_UVEC(v_symbol, -1);  -- decrement each symbol towards the beginning of v_data
                end loop;
                prev_out_data <= v_data;
              end if;
            end if;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_symbols;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output data with empty
  -- . account for stream empty
  -- . support last word replace (e.g. by a CRC instead of the count, or use
  --   c_last_word=out_data for no replace)
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_data_empty(constant c_ready_latency : in    natural;
                                      constant c_last_word     : in    natural;
                                      signal   clk             : in    std_logic;
                                      signal   verify_en       : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   out_eop_1       : inout std_logic;
                                      signal   out_eop_2       : inout std_logic;
                                      signal   out_data        : in    std_logic_vector;
                                      signal   out_data_1      : inout std_logic_vector;
                                      signal   out_data_2      : inout std_logic_vector;
                                      signal   out_data_3      : inout std_logic_vector;
                                      signal   out_empty       : in    std_logic_vector;
                                      signal   out_empty_1     : inout std_logic_vector) is
    variable v_last_word    : std_logic_vector(out_data'high downto 0);
    variable v_ref_data     : std_logic_vector(out_data'high downto 0);
    variable v_empty_data   : std_logic_vector(out_data'high downto 0);
  begin
    if rising_edge(clk) then
      -- out_val must be active, because only then out_data will differ from the previous out_data
      if out_val = '1' then
        -- for ready_latency > 0 out_val indicates new data
        -- for ready_latency = 0 out_val only indicates new data when it is confirmed by out_ready
        if c_ready_latency /= 0 or (c_ready_latency = 0 and out_ready = '1') then
          -- default expected data
          out_data_1  <= out_data;
          out_data_2  <= out_data_1;
          out_data_3  <= out_data_2;
          out_empty_1 <= out_empty;
          out_eop_1   <= out_eop;
          out_eop_2   <= out_eop_1;
          if verify_en = '1' then
            -- assume sufficient valid cycles between eop and sop, so no need to check for out_sop with regard to eop empty
            if out_eop = '0' and out_eop_1 = '0' and out_eop_2 = '0'THEN
              -- verify out_data from eop-n to eop-2 and from eop+1 to eop+n, n>2
              v_ref_data := INCR_UVEC(out_data_2, 1);
              if unsigned(out_data_1) /= unsigned(v_ref_data) then
                report "DP : Wrong out_data count"
                  severity ERROR;
              end if;
            else
              -- the empty and crc replace affect data at eop_1 and eop, so need to check data from eop-2 to eop-1 to eop to eop+1
              v_last_word := TO_UVEC(c_last_word, out_data'length);
              if out_eop = '1' then
                -- verify out_data at eop
                case to_integer(unsigned(out_empty)) is
                  when 0 => v_empty_data := v_last_word;
                  when 1 => v_empty_data := v_last_word(3 * c_byte_w - 1 downto 0) & c_slv0(1 * c_byte_w - 1 downto 0);
                  when 2 => v_empty_data := v_last_word(2 * c_byte_w - 1 downto 0) & c_slv0(2 * c_byte_w - 1 downto 0);
                  when 3 => v_empty_data := v_last_word(1 * c_byte_w - 1 downto 0) & c_slv0(3 * c_byte_w - 1 downto 0);
                  when others => null;
                end case;
                if unsigned(out_data) /= unsigned(v_empty_data) then
                  report "DP : Wrong out_data count at eop"
                    severity ERROR;
                end if;
              elsif out_eop_1 = '1' then
                -- verify out_data from eop-2 to eop-1
                v_ref_data := INCR_UVEC(out_data_3, 1);
                case to_integer(unsigned(out_empty_1)) is
                  when 0 => v_empty_data := v_ref_data;
                  when 1 => v_empty_data := v_ref_data(4 * c_byte_w - 1 downto 1 * c_byte_w) & v_last_word(4 * c_byte_w - 1 downto 3 * c_byte_w);
                  when 2 => v_empty_data := v_ref_data(4 * c_byte_w - 1 downto 2 * c_byte_w) & v_last_word(4 * c_byte_w - 1 downto 2 * c_byte_w);
                  when 3 => v_empty_data := v_ref_data(4 * c_byte_w - 1 downto 3 * c_byte_w) & v_last_word(4 * c_byte_w - 1 downto 1 * c_byte_w);
                  when others => null;
                end case;
                if unsigned(out_data_2) /= unsigned(v_empty_data) then
                  report "DP : Wrong out_data count at eop-1"
                    severity ERROR;
                end if;
                -- verify out_data from eop-2 to eop+1
                v_ref_data := INCR_UVEC(out_data_3, 3);
                if unsigned(out_data) /= unsigned(v_ref_data) then
                  report "DP : Wrong out_data count at eop+1"
                    severity ERROR;
                end if;
              end if;
            end if;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_data_empty;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output other SOSI data
  -- . Suited to verify the empty, error, channel fields assuming that these
  --   are treated in the same way in parallel to the SOSI data.
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_other_sosi(constant c_str       : in string;
                                      constant c_exp_data  : in std_logic_vector;  -- use constant to support assignment via FUNCTION return value
                                      signal   clk         : in std_logic;
                                      signal   verify_en   : in std_logic;
                                      signal   res_data    : in std_logic_vector) is
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        if    c_str = "bsn" then
          if unsigned(c_exp_data(c_dp_bsn_w - 1 downto 0)) /= unsigned(res_data(c_dp_bsn_w - 1 downto 0)) then
            report "DP : Wrong sosi.bsn value"
              severity ERROR;
          end if;
        elsif c_str = "empty" then
          if unsigned(c_exp_data(c_dp_empty_w - 1 downto 0)) /= unsigned(res_data(c_dp_empty_w - 1 downto 0)) then
            report "DP : Wrong sosi.empty value"
              severity ERROR;
          end if;
        elsif c_str = "channel" then
          if unsigned(c_exp_data(c_dp_channel_user_w - 1 downto 0)) /= unsigned(res_data(c_dp_channel_user_w - 1 downto 0)) then
            report "DP : Wrong sosi.channel value"
              severity ERROR;
          end if;
        elsif c_str = "error" then
          if unsigned(c_exp_data(c_dp_error_w - 1 downto 0)) /= unsigned(res_data(c_dp_error_w - 1 downto 0)) then
            report "DP : Wrong sosi.error value"
              severity ERROR;
          end if;
        else
          report "proc_dp_verify_other_sosi : Unknown sosi." & c_str & "field"
            severity FAILURE;
        end if;
      end if;
    end if;
  end proc_dp_verify_other_sosi;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify per field whether DUT output SOSI is equal to expected SOSI
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_sosi_equal(constant c_str       : in string;
                                      signal   clk         : in std_logic;
                                      signal   verify_en   : in std_logic;
                                      signal   dut_sosi    : in t_dp_sosi_integer;  -- use func_dp_stream_slv_to_integer for conversion
                                      signal   exp_sosi    : in t_dp_sosi_integer) is  -- use func_dp_stream_slv_to_integer for conversion
  begin
    -- Use sosi integers, instead of sosi slv, for easier comparision. This
    -- implies that only integer low part of sosi slv fields is checked, so
    -- NATURAL'width = 31 bit for control and info and c_dat_w <= 32 bits for
    -- data, re, im (c_dat_w is a parameter of func_dp_stream_slv_to_integer).
    if rising_edge(clk) then
      if verify_en = '1' then
        -- sosi ctrl fields
        if    c_str = "sync" then
          if dut_sosi.sync /= exp_sosi.sync then
            report "DP : Wrong dut_sosi.sync (" & sl_to_str(dut_sosi.sync) & " /= " & sl_to_str(exp_sosi.sync) & ")"
              severity ERROR;
          end if;
        elsif c_str = "sop" then
          if dut_sosi.sop /= exp_sosi.sop then
            report "DP : Wrong dut_sosi.sop (" & sl_to_str(dut_sosi.sop) & " /= " & sl_to_str(exp_sosi.sop) & ")"
              severity ERROR;
          end if;
        elsif c_str = "eop" then
          if dut_sosi.eop /= exp_sosi.eop then
            report "DP : Wrong dut_sosi.eop (" & sl_to_str(dut_sosi.eop) & " /= " & sl_to_str(exp_sosi.eop) & ")"
              severity ERROR;
          end if;
        elsif c_str = "valid" then
          if dut_sosi.valid /= exp_sosi.valid then
            report "DP : Wrong dut_sosi.valid (" & sl_to_str(dut_sosi.valid) & " /= " & sl_to_str(exp_sosi.valid) & ")"
              severity ERROR;
          end if;

        -- sosi info fields
        elsif c_str = "bsn" then
          if dut_sosi.bsn /= exp_sosi.bsn then
            report "DP : Wrong dut_sosi.bsn (" & int_to_str(dut_sosi.bsn) & " /= " & int_to_str(exp_sosi.bsn) & ")"
              severity ERROR;
          end if;
        elsif c_str = "empty" then
          if dut_sosi.empty /= exp_sosi.empty then
            report "DP : Wrong dut_sosi.empty (" & int_to_str(dut_sosi.empty) & " /= " & int_to_str(exp_sosi.empty) & ")"
              severity ERROR;
          end if;
        elsif c_str = "channel" then
          if dut_sosi.channel /= exp_sosi.channel then
            report "DP : Wrong dut_sosi.channel (" & int_to_str(dut_sosi.channel) & " /= " & int_to_str(exp_sosi.channel) & ")"
              severity ERROR;
          end if;
        elsif c_str = "err" then
          if dut_sosi.err /= exp_sosi.err then
            report "DP : Wrong dut_sosi.err (" & int_to_str(dut_sosi.err) & " /= " & int_to_str(exp_sosi.err) & ")"
              severity ERROR;
          end if;

        -- sosi data fields
        elsif c_str = "data" then
          if dut_sosi.data /= exp_sosi.data then
            report "DP : Wrong dut_sosi.data (" & int_to_str(dut_sosi.data) & " /= " & int_to_str(exp_sosi.data) & ")"
              severity ERROR;
          end if;
        elsif c_str = "re" then
          if dut_sosi.re /= exp_sosi.re then
            report "DP : Wrong dut_sosi.re (" & int_to_str(dut_sosi.re) & " /= " & int_to_str(exp_sosi.re) & ")"
              severity ERROR;
          end if;
        elsif c_str = "im" then
          if dut_sosi.im /= exp_sosi.im then
            report "DP : Wrong dut_sosi.im (" & int_to_str(dut_sosi.im) & " /= " & int_to_str(exp_sosi.im) & ")" & ")"
              severity ERROR;
          end if;

        -- unknown sosi field
        else
          report "proc_dp_verify_sosi_equal : Unknown sosi." & c_str & "field"
            severity FAILURE;
        end if;
      end if;
    end if;
  end proc_dp_verify_sosi_equal;

  procedure proc_dp_verify_sosi_equal(constant c_use_complex : in boolean;
                                      signal   dut_sosi      : in t_dp_sosi;
                                      signal   exp_sosi      : in t_dp_sosi) is
  begin
    assert dut_sosi.sync = exp_sosi.sync
      report "Wrong dut_sosi.sync"
      severity ERROR;
    assert dut_sosi.sop = exp_sosi.sop
      report "Wrong dut_sosi.sop"
      severity ERROR;
    assert dut_sosi.eop = exp_sosi.eop
      report "Wrong dut_sosi.eop"
      severity ERROR;
    assert dut_sosi.valid = exp_sosi.valid
      report "Wrong dut_sosi.valid"
      severity ERROR;
    if exp_sosi.sop = '1' then
      assert dut_sosi.bsn = exp_sosi.bsn
        report "Wrong dut_sosi.bsn"
        severity ERROR;
      assert dut_sosi.channel = exp_sosi.channel
        report "Wrong dut_sosi.channel"
        severity ERROR;
    end if;
    if exp_sosi.eop = '1' then
      assert dut_sosi.empty = exp_sosi.empty
        report "Wrong dut_sosi.empty"
        severity ERROR;
      assert dut_sosi.err = exp_sosi.err
        report "Wrong dut_sosi.err"
        severity ERROR;
    end if;
    if exp_sosi.valid = '1' then
      if c_use_complex then
        assert dut_sosi.re = exp_sosi.re
          report "Wrong dut_sosi.re"
          severity ERROR;
        assert dut_sosi.im = exp_sosi.im
          report "Wrong dut_sosi.im"
          severity ERROR;
      else
        assert dut_sosi.data = exp_sosi.data
          report "Wrong dut_sosi.data"
          severity ERROR;
      end if;
    end if;
  end proc_dp_verify_sosi_equal;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output valid
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_valid(constant c_ready_latency : in    natural;
                                 signal   clk             : in    std_logic;
                                 signal   verify_en       : in    std_logic;
                                 signal   out_ready       : in    std_logic;
                                 signal   prev_out_ready  : inout std_logic_vector;
                                 signal   out_val         : in    std_logic) is
  begin
    if rising_edge(clk) then
      -- for ready_latency > 0 out_val may only be asserted after out_ready
      -- for ready_latency = 0 out_val may always be asserted
      prev_out_ready <= (prev_out_ready'range => '0');
      if c_ready_latency /= 0 then
        if c_ready_latency = 1 then
          prev_out_ready(0) <= out_ready;
        else
          prev_out_ready    <= out_ready & prev_out_ready(0 to c_ready_latency - 1);
        end if;
        if verify_en = '1' and out_val = '1' then
          if prev_out_ready(c_ready_latency - 1) /= '1' then
            report "DP : Wrong ready latency between out_ready and out_val"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_valid;

  procedure proc_dp_verify_valid(signal   clk             : in    std_logic;
                                 signal   verify_en       : in    std_logic;
                                 signal   out_ready       : in    std_logic;
                                 signal   prev_out_ready  : inout std_logic;
                                 signal   out_val         : in    std_logic) is
  begin
    -- Can not reuse:
    --   proc_dp_verify_valid(1, clk, verify_en, out_ready, prev_out_ready, out_val);
    -- because prev_out_ready needs to map from STD_LOGIC to STD_LOGIC_VECTOR. Therefore copy paste code for RL=1:
    if rising_edge(clk) then
      -- for ready_latency = 1 out_val may only be asserted after out_ready
      prev_out_ready <= out_ready;
      if verify_en = '1' and out_val = '1' then
        if prev_out_ready /= '1' then
          report "DP : Wrong ready latency between out_ready and out_val"
            severity ERROR;
        end if;
      end if;
    end if;
  end proc_dp_verify_valid;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output sync
  -- . sync is defined such that it can only be active at sop
  -- . report expected_sync from input
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_sync(signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   sync            : in    std_logic;
                                signal   sop             : in    std_logic;
                                         expected_sync   : in    std_logic) is
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        -- Check for unexpected sync
        if sync = '1' then
          assert expected_sync = '1'
            report "Error: Unexpected sync at BSN"
            severity ERROR;
          assert sop = '1'
            report "Error: Unexpected sync at inactive sop"
            severity ERROR;
        end if;
        -- Check for missing sync
        if sop = '1' and expected_sync = '1' then
          assert sync = '1'
            report "Error: Missing sync"
            severity ERROR;
        end if;
      end if;
    end if;
  end proc_dp_verify_sync;

  procedure proc_dp_verify_sync(signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   sync            : in    std_logic;
                                signal   sop             : in    std_logic;
                                         bsn             : in    natural;  -- for reporting
                                         expected_bsn    : in    natural;  -- for reporting
                                         expected_sync   : in    std_logic) is
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        -- Check for unexpected sync
        if sync = '1' then
          assert expected_sync = '1'
            report "Error: Unexpected sync at BSN (" & int_to_str(bsn) & " /= " & int_to_str(expected_bsn) & ")"
            severity ERROR;
          assert sop = '1'
            report "Error: Unexpected sync at inactive sop"
            severity ERROR;
        end if;
        -- Check for missing sync
        if sop = '1' and expected_sync = '1' then
          assert sync = '1'
            report "Error: Missing sync"
            severity ERROR;
        end if;
      end if;
    end if;
  end proc_dp_verify_sync;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output sync
  -- . sync is defined such that it can only be active at sop
  -- . assume that the sync occures priodically at bsn MOD c_sync_period = c_sync_offset
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_sync(constant c_sync_period   : in    natural;  -- BSN sync period
                                constant c_sync_offset   : in    natural;  -- BSN sync offset
                                signal   clk             : in    std_logic;
                                signal   verify_en       : in    std_logic;
                                signal   sync            : in    std_logic;
                                signal   sop             : in    std_logic;
                                signal   bsn             : in    std_logic_vector) is
    constant c_bsn_w         : natural := sel_a_b(bsn'length > 31, 31, bsn'length);  -- use maximally c_natural_w = 31 bit of BSN slv to allow calculations with integers
    variable v_bsn           : natural := TO_UINT(bsn(c_bsn_w - 1 downto 0));
    variable v_expected_sync : boolean;
  begin
    -- Determine v_expected_sync
    v_expected_sync := (v_bsn - c_sync_offset) mod c_sync_period = 0;
    -- Report sync and v_expected_sync
    proc_dp_verify_sync(clk, verify_en, sync, sop, to_sl(v_expected_sync));
  end proc_dp_verify_sync;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output sync
  -- . sync is defined such that it can only be active at sop
  -- . assume that the fractional sync period varies between N and N-1 blocks
  -- . the fractional sync period starts with N blocks and fits e.g.
  --   dp_bsn_source_v2, dp_bsn_sync_scheduler.
  -- . Use block sequence number (BSN) in dbg_expected_bsn.
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_sync(constant c_start_bsn      : in    natural;  -- BSN of first sync, start of fractional periods
                                constant c_sync_period    : in    natural;  -- number of sample per sync period
                                constant c_block_size     : in    natural;  -- number of sample per block
                                signal   clk              : in    std_logic;
                                signal   verify_en        : in    std_logic;
                                signal   sync             : in    std_logic;
                                signal   sop              : in    std_logic;
                                signal   bsn              : in    std_logic_vector;
                                -- for debug purposes
                                signal   dbg_nof_blk      : out   natural;
                                signal   dbg_accumulate   : out   natural;
                                signal   dbg_expected_bsn : out   natural) is
  begin
    proc_dp_verify_sync(c_start_bsn,
                        c_sync_period,
                        c_block_size,
                        false,
                        clk,
                        verify_en,
                        sync,
                        sop,
                        bsn,
                        dbg_nof_blk,
                        dbg_accumulate,
                        dbg_expected_bsn);
  end proc_dp_verify_sync;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output sync
  -- . sync is defined such that it can only be active at sop
  -- . assume that the fractional sync period varies between N and N-1 blocks
  -- . the fractional sync period starts with N blocks and fits e.g.
  --   dp_bsn_source_v2, dp_bsn_sync_scheduler.
  -- . support using block sequence number (BSN) in dbg_expected_bsn or using
  --   raw samples sequence number (RSN) in dbg_expected_bsn
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_sync(constant c_start_bsn      : in    natural;  -- BSN of first sync, start of fractional periods
                                constant c_sync_period    : in    natural;  -- number of sample per sync period
                                constant c_block_size     : in    natural;  -- number of sample per block
                                constant c_bsn_is_rsn     : in    boolean;  -- increment BSN by 1 or by c_block_size for RSN
                                signal   clk              : in    std_logic;
                                signal   verify_en        : in    std_logic;
                                signal   sync             : in    std_logic;
                                signal   sop              : in    std_logic;
                                signal   bsn              : in    std_logic_vector;
                                -- for debug purposes
                                signal   dbg_nof_blk      : out   natural;
                                signal   dbg_accumulate   : out   natural;
                                signal   dbg_expected_bsn : out   natural) is
    constant c_bsn_w         : natural := sel_a_b(bsn'length > 31, 31, bsn'length);  -- use maximally c_natural_w = 31 bit of BSN slv to allow calculations with integers
    constant c_nof_blk_min   : natural := c_sync_period / c_block_size;  -- minimum number of blocks in sync period
    constant c_extra         : natural := c_sync_period mod c_block_size;  -- number of extra samples in sync period
    variable v_bsn           : natural := TO_UINT(bsn(c_bsn_w - 1 downto 0));
    variable v_expected_sync : boolean := false;  -- default FALSE, e.g. when bsn < c_start_bsn is in the past
    variable v_expected_bsn  : natural := c_start_bsn;  -- BSN that is expected to have a sync, intialize with start BSN
    variable v_nof_blk       : natural := c_nof_blk_min + 1;  -- number of blocks in period, first sync period will be 1 block longer to achieve the fraction part
    variable v_accumulate    : integer := c_block_size - c_extra;  -- number of extra samples in period, first sync period will have v_accumulate more
  begin
    -- Determine v_expected_sync
    if c_extra = 0 then
      -- The sync period contains an integer number of blocks (c_extra = 0)
      -- Determine directly whether the input bsn is expected to have a sync
      if c_bsn_is_rsn then
        v_expected_sync := ((v_bsn - c_start_bsn) mod (c_nof_blk_min * c_block_size) = 0);
      else
        v_expected_sync := ((v_bsn - c_start_bsn) mod c_nof_blk_min = 0);
      end if;
    else
      -- The sync period contains a fractional number of blocks
      -- Determine next expected BSN with sync until the input bsn is reached using a loop
      while v_expected_bsn < v_bsn loop
        -- next expected BSN to have a sync
        if c_bsn_is_rsn then
          v_expected_bsn := v_expected_bsn + v_nof_blk * c_block_size;
        else
          v_expected_bsn := v_expected_bsn + v_nof_blk;
        end if;

        v_nof_blk := c_nof_blk_min;
        v_accumulate   := v_accumulate - c_extra;
        if v_accumulate < 0 then
          v_nof_blk := v_nof_blk + 1;
          v_accumulate   := v_accumulate + c_block_size;
        end if;
      end loop;
      v_expected_sync := (v_bsn = v_expected_bsn);
    end if;

    if verify_en = '1' then
      -- Debug signals, for view in Wave window
      dbg_nof_blk      <= v_nof_blk;
      dbg_accumulate   <= v_accumulate;
      dbg_expected_bsn <= v_expected_bsn;
    end if;

    -- Report sync and v_expected_sync
    proc_dp_verify_sync(clk, verify_en, sync, sop, v_bsn, v_expected_bsn, to_sl(v_expected_sync));
  end proc_dp_verify_sync;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output sop and eop
  ------------------------------------------------------------------------------
  -- sop and eop in pairs, valid during packet and invalid between packets
  procedure proc_dp_verify_sop_and_eop(constant c_ready_latency : in    natural;
                                       constant c_verify_valid  : in    boolean;
                                       signal   clk             : in    std_logic;
                                       signal   out_ready       : in    std_logic;
                                       signal   out_val         : in    std_logic;
                                       signal   out_sop         : in    std_logic;
                                       signal   out_eop         : in    std_logic;
                                       signal   hold_sop        : inout std_logic) is
  begin
    if rising_edge(clk) then
      if out_val = '0' then
        if out_sop = '1' then report "DP : Wrong active sop during invalid" severity ERROR; end if;
        if out_eop = '1' then report "DP : Wrong active eop during invalid" severity ERROR; end if;
      else
        -- for ready_latency > 0 out_val indicates new data
        -- for ready_latency = 0 out_val only indicates new data when it is confirmed by out_ready
        if c_ready_latency /= 0 or (c_ready_latency = 0 and out_ready = '1') then
          if out_sop = '1' then
            hold_sop <= '1';
            if hold_sop = '1' then
              report "DP : Unexpected sop without eop"
                severity ERROR;
            end if;
          end if;
          if out_eop = '1' then
            hold_sop <= '0';
            if hold_sop = '0' and out_sop = '0' then
              report "DP : Unexpected eop without sop"
                severity ERROR;
            end if;
          end if;
          -- out_val='1'
          if c_verify_valid = true and out_sop = '0' and hold_sop = '0' then
            report "DP : Unexpected valid in gap between eop and sop"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_sop_and_eop;

  procedure proc_dp_verify_sop_and_eop(constant c_ready_latency : in    natural;
                                       signal   clk             : in    std_logic;
                                       signal   out_ready       : in    std_logic;
                                       signal   out_val         : in    std_logic;
                                       signal   out_sop         : in    std_logic;
                                       signal   out_eop         : in    std_logic;
                                       signal   hold_sop        : inout std_logic) is
  begin
    proc_dp_verify_sop_and_eop(c_ready_latency, true, clk, out_ready, out_val, out_sop, out_eop, hold_sop);
  end proc_dp_verify_sop_and_eop;

  procedure proc_dp_verify_sop_and_eop(signal   clk      : in    std_logic;
                                       signal   out_val  : in    std_logic;
                                       signal   out_sop  : in    std_logic;
                                       signal   out_eop  : in    std_logic;
                                       signal   hold_sop : inout std_logic) is
  begin
    -- Use out_val as void signal to pass on to unused out_ready, because a signal input can not connect a constant or variable
    proc_dp_verify_sop_and_eop(1, true, clk, out_val, out_val, out_sop, out_eop, hold_sop);
  end proc_dp_verify_sop_and_eop;

  procedure proc_dp_verify_block_size(constant c_ready_latency : in    natural;
                                      signal   alt_size        : in    natural;  -- alternative size
                                      signal   exp_size        : in    natural;  -- expected size
                                      signal   clk             : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural) is
  begin
    if rising_edge(clk) then
      if out_val = '1' then
        -- for ready_latency > 0 out_val indicates new data
        -- for ready_latency = 0 out_val only indicates new data when it is confirmed by out_ready
        if c_ready_latency /= 0 or (c_ready_latency = 0 and out_ready = '1') then
          if out_sop = '1' then
            cnt_size <= 1;
          elsif out_eop = '1' then
            cnt_size <= 0;
            if cnt_size /= alt_size-1 and cnt_size /= exp_size-1 then
              report "DP : Unexpected block size"
                severity ERROR;
            end if;
          else
            cnt_size <= cnt_size+1;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_block_size;

  procedure proc_dp_verify_block_size(constant c_ready_latency : in    natural;
                                      signal   exp_size        : in    natural;
                                      signal   clk             : in    std_logic;
                                      signal   out_ready       : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural) is
  begin
    proc_dp_verify_block_size(c_ready_latency, exp_size, exp_size, clk, out_ready, out_val, out_sop, out_eop, cnt_size);
  end proc_dp_verify_block_size;

  procedure proc_dp_verify_block_size(signal   alt_size        : in    natural;  -- alternative size
                                      signal   exp_size        : in    natural;  -- expected size
                                      signal   clk             : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural) is
  begin
    -- Use out_val as void signal to pass on to unused out_ready, because a signal input can not connect a constant or variable
    proc_dp_verify_block_size(1, alt_size, exp_size, clk, out_val, out_val, out_sop, out_eop, cnt_size);
  end proc_dp_verify_block_size;

  procedure proc_dp_verify_block_size(signal   exp_size        : in    natural;
                                      signal   clk             : in    std_logic;
                                      signal   out_val         : in    std_logic;
                                      signal   out_sop         : in    std_logic;
                                      signal   out_eop         : in    std_logic;
                                      signal   cnt_size        : inout natural) is
  begin
    -- Use out_val as void signal to pass on to unused out_ready, because a signal input can not connect a constant or variable
    proc_dp_verify_block_size(1, exp_size, exp_size, clk, out_val, out_val, out_sop, out_eop, cnt_size);
  end proc_dp_verify_block_size;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output invalid between frames
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_gap_invalid(signal clk     : in    std_logic;
                                       signal in_val  : in    std_logic;
                                       signal in_sop  : in    std_logic;
                                       signal in_eop  : in    std_logic;
                                       signal out_gap : inout std_logic) is
  begin
    if rising_edge(clk) then
      if in_eop = '1' then
        out_gap <= '1';
      elsif in_sop = '1' then
        out_gap <= '0';
      elsif in_val = '1' and out_gap = '1' then
        report "DP : Wrong valid in gap between eop and sop"
          severity ERROR;
      end if;
    end if;
  end proc_dp_verify_gap_invalid;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Verify the DUT output control (use for sop, eop)
  ------------------------------------------------------------------------------
  procedure proc_dp_verify_ctrl(constant c_offset  : in natural;
                                constant c_period  : in natural;
                                constant c_str     : in string;
                                signal   clk       : in std_logic;
                                signal   verify_en : in std_logic;
                                signal   data      : in std_logic_vector;
                                signal   valid     : in std_logic;
                                signal   ctrl      : in std_logic) is
    variable v_data : integer;
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        v_data := TO_UINT(data);
        if ((v_data - c_offset) mod c_period) = 0 then
          if valid = '1' and ctrl /= '1' then
            report "DP : Wrong data control, missing " & c_str
              severity ERROR;
          end if;
        else
          if ctrl = '1' then
            report "DP : Wrong data control, unexpected " & c_str
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end proc_dp_verify_ctrl;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait for stream valid
  ------------------------------------------------------------------------------
  procedure proc_dp_stream_valid(signal clk      : in  std_logic;
                                 signal in_valid : in  std_logic) is
  begin
    wait until rising_edge(clk);
    while in_valid /= '1' loop
      wait until rising_edge(clk);
    end loop;
  end proc_dp_stream_valid;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait for stream valid AND sop
  ------------------------------------------------------------------------------
  procedure proc_dp_stream_valid_sop(signal clk      : in  std_logic;
                                     signal in_valid : in  std_logic;
                                     signal in_sop   : in  std_logic) is
  begin
    wait until rising_edge(clk);
    while in_valid /= '1' and in_sop /= '1' loop
      wait until rising_edge(clk);
    end loop;
  end proc_dp_stream_valid_sop;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Wait for stream valid AND eop
  ------------------------------------------------------------------------------
  procedure proc_dp_stream_valid_eop(signal clk      : in  std_logic;
                                     signal in_valid : in  std_logic;
                                     signal in_eop   : in  std_logic) is
  begin
    wait until rising_edge(clk);
    while in_valid /= '1' and in_eop /= '1' loop
      wait until rising_edge(clk);
    end loop;
  end proc_dp_stream_valid_eop;

end tb_dp_pkg;
