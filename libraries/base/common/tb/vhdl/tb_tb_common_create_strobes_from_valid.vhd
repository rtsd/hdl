-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose: Multi tb for common_create_strobes_from_valid.vhd
-- Usage:
-- > as 3
-- > run -a

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_common_create_strobes_from_valid is
end tb_tb_common_create_strobes_from_valid;

architecture tb of tb_tb_common_create_strobes_from_valid is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_pipeline           : BOOLEAN := FALSE;
  -- g_in_val_gaps        : BOOLEAN := FALSE;
  -- g_nof_clk_per_sync   : NATURAL := 17;
  -- g_nof_clk_per_block  : NATURAL := 7

  u_integer_interval                   : entity work.tb_common_create_strobes_from_valid generic map (false, false, 10, 5);
  u_integer_interval_with_gaps         : entity work.tb_common_create_strobes_from_valid generic map (false,  true, 10, 5);
  u_integer_interval_with_gaps_pipe    : entity work.tb_common_create_strobes_from_valid generic map ( true,  true, 10, 5);
  u_fractional_interval                : entity work.tb_common_create_strobes_from_valid generic map (false, false, 17, 7);
  u_fractional_interval_with_gaps      : entity work.tb_common_create_strobes_from_valid generic map (false,  true, 17, 7);
  u_fractional_interval_with_gaps_pipe : entity work.tb_common_create_strobes_from_valid generic map ( true,  true, 17, 7);
end tb;
