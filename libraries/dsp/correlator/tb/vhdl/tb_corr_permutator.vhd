--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_corr_permutator is
  generic (
    g_nof_inputs  : natural := 4
  );
end tb_corr_permutator;

architecture tb of tb_corr_permutator is
  constant c_clk_period            : time := 10 ns;
  constant c_data_w                : natural := 32;
  constant c_nof_permutations      : natural := (g_nof_inputs * (g_nof_inputs + 1)) / 2;

  signal tb_end                    : std_logic := '0';
  signal clk                       : std_logic := '1';
  signal rst                       : std_logic;

  signal permutator_snk_in_arr     : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal permutator_src_out_2arr_2 : t_dp_sosi_2arr_2(c_nof_permutations - 1 downto 0);
begin
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  u_corr_permutator : entity work.corr_permutator
  generic map (
    g_nof_inputs  => g_nof_inputs
  )
  port map (
    rst            => rst,
    clk            => clk,
    snk_in_arr     => permutator_snk_in_arr,
    src_out_2arr_2 => permutator_src_out_2arr_2
  );

  p_stimuli: process
  begin
    wait until rst = '0';
    wait for 100 ns;
    for i in 0 to 10 - 1 loop
      for j in 0 to g_nof_inputs - 1 loop
        -- Feed the permutator. Use the stream index as data word
        permutator_snk_in_arr(j).data(c_data_w - 1 downto 0) <= TO_UVEC(j, c_data_w);
        permutator_snk_in_arr(j).valid <= '1';
      end loop;
      wait for c_clk_period;
      for j in 0 to g_nof_inputs - 1 loop
        -- Let the permutator do its thing
        permutator_snk_in_arr(j).data <= (others => '0');
        permutator_snk_in_arr(j).valid <= '0';
      end loop;
    wait for c_clk_period;
    end loop;
  end process;
end tb;
