library IEEE, altera_mf, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

architecture stratix of pfs_fir_tapsbuf is
  component altsyncram
  generic (
    intended_device_family : string;
    operation_mode         : string;
    width_a                : natural;
    widthad_a              : natural;
    numwords_a             : natural;
    width_b                : natural;
    widthad_b              : natural;
    numwords_b             : natural;
    lpm_type               : string;
    width_byteena_a        : natural;
    outdata_reg_b          : string;
    address_reg_b          : string;
    outdata_aclr_b         : string;
    read_during_write_mode_mixed_ports : string;
    init_file              : string;
    ram_block_type         : string
  );
  port (
    wren_a                 : in  std_logic;
    aclr0                  : in  std_logic;
    clock0                 : in  std_logic;
    address_a              : in  std_logic_vector(g_addr_w - 1 downto 0);
    address_b              : in  std_logic_vector(g_addr_w - 1 downto 0);
    q_b                    : out std_logic_vector(g_data_w - 1 downto 0);
    data_a                 : in  std_logic_vector(g_data_w - 1 downto 0)
  );
  end component;
begin
  altsyncram_component : altsyncram
  generic map (
    intended_device_family => c_rsp_device_family,
    operation_mode         => "DUAL_PORT",
    width_a                => g_data_w,
    widthad_a              => g_addr_w,
    numwords_a             => g_nof_words,
    width_b                => g_data_w,
    widthad_b              => g_addr_w,
    numwords_b             => g_nof_words,
    lpm_type               => "altsyncram",
    width_byteena_a        => 1,
    outdata_reg_b          => "CLOCK0",
    address_reg_b          => "CLOCK0",
    outdata_aclr_b         => "CLEAR0",
    read_during_write_mode_mixed_ports => "DONT_CARE",
    init_file              => "../../../../../EPA/pfs/src/data/pfs_fir_taps_"
        & natural'image(g_nof_words) & "pts.hex",
    ram_block_type         => "AUTO"
  )
  port map (
    wren_a                 => wren_a,
    aclr0                  => rst,
    clock0                 => clk,
    address_a              => addr_a,
    address_b              => addr_b,
    data_a                 => data_a,
    q_b                    => data_b
  );
end stratix;
