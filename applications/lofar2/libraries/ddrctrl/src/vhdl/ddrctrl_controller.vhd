-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: controller for ddrctrl, decides when to write when to read or when to stop writing or reading.
--
-- Description:
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--

library IEEE, dp_lib, common_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity ddrctrl_controller is
  generic (
    g_tech_ddr                : t_c_tech_ddr;
    g_stop_percentage         : natural     := 50;
    g_nof_streams             : natural;  -- 12
    g_out_data_w              : natural;  -- 14
    g_wr_data_w               : natural;  -- 168
    g_rd_fifo_depth           : natural;  -- 256
    g_rd_data_w               : natural;  -- 256
    g_block_size              : natural;  -- 1024
    g_wr_fifo_uw_w            : natural;  -- 8
    g_rd_fifo_uw_w            : natural;  -- 8
    g_max_adr                 : natural;  -- 16128
    g_burstsize               : natural;  -- 64
    g_last_burstsize          : natural;  -- 18
    g_adr_per_b               : natural;  -- 299
    g_bim                     : natural  -- 54
  );
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;

    -- ddrctrl_input
    inp_of                    : in  natural;
    inp_sosi                  : in  t_dp_sosi;
    inp_adr                   : in  natural;
    inp_bsn_adr               : in  natural;
    inp_data_stopped          : in  std_logic;
    rst_ddrctrl_input_ac      : out std_logic;

    -- io_ddr
    dvr_mosi                  : out t_mem_ctlr_mosi;
    dvr_miso                  : in  t_mem_ctlr_miso;
    wr_sosi                   : out t_dp_sosi;
    wr_siso                   : in  t_dp_siso;
    wr_fifo_usedw             : in  std_logic_vector(g_wr_fifo_uw_w - 1 downto 0);
    rd_fifo_usedw             : in  std_logic_vector(g_rd_fifo_uw_w - 1 downto 0);
    ctlr_wr_flush_en          : in  std_logic;
    flush_state               : in  std_logic_vector(1 downto 0);

    -- ddrctrl_output
    outp_bsn                  : out std_logic_vector(c_dp_stream_bsn_w - 1 downto 0)  := (others => '0');

    -- ddrctrl_controller
    stop_in                   : in  std_logic;
    stop_out                  : out std_logic;
    ddrctrl_ctrl_state        : out std_logic_vector(32 - 1 downto 0)                 := (others => '0')
  );
end ddrctrl_controller;

architecture rtl of ddrctrl_controller is
  constant  c_bitshift_w      : natural                                     := ceil_log2(g_burstsize);  -- bitshift to make sure there is only a burst start at a interval of c_burstsize.
  constant  c_adr_w           : natural                                     := func_tech_ddr_ctlr_address_w( g_tech_ddr );  -- the lengt of the address vector, for simulation this is smaller, otherwise the simulation would take to long, 27
  constant  c_pof_ma          : natural                                     := ((natural((real(g_max_adr) * (100.0 - real(g_stop_percentage))) / 100.0) / g_adr_per_b) * g_adr_per_b);  -- percentage of max address.

  constant  c_zeros           : std_logic_vector(c_bitshift_w - 1 downto 0)   := (others => '0');
  constant  c_stop_adr_zeros  : std_logic_vector(c_adr_w - 1      downto 0)   := (others => '0');

  -- constant for reading

  constant  c_rd_data_w       : natural                                     := g_nof_streams * g_out_data_w;  -- 168
  constant  c_io_ddr_data_w   : natural                                     := func_tech_ddr_ctlr_data_w(g_tech_ddr);  -- 576

  -- constant for debugging
  constant  c_always_one_ndx  : natural := 0;
  constant  c_rst_ndx         : natural := 1;
  constant  c_low_state_ndx   : natural := 2;
  constant  c_high_state_ndx  : natural := 5;
  constant  c_state_ndx_w     : natural := c_high_state_ndx - c_low_state_ndx + 1;
  constant  c_low_bsn_ndx     : natural := 16;
  constant  c_high_bsn_ndx    : natural := 25;
  constant  c_bsn_ndx_w       : natural := c_high_bsn_ndx - c_low_bsn_ndx + 1;
  constant  c_start_bsn       : natural := 14;
  constant  c_low_adr_ndx     : natural := c_high_bsn_ndx + 1;
  constant  c_high_adr_ndx    : natural := 32 - 1;
  constant  c_adr_ndx_w       : natural := c_high_adr_ndx - c_low_adr_ndx + 1;
  constant  c_done_ndx        : natural := 8;
  constant  c_bb_ndx          : natural := 9;
  constant  c_ben_ndx         : natural := 10;
  constant  c_das_ndx         : natural := 11;
  constant  c_low_bre_ndx     : natural := 12;
  constant  c_high_bre_ndx    : natural := 15;
  constant  c_bre_ndx_w       : natural := c_high_bre_ndx - c_low_bre_ndx + 1;

  -- type for statemachine
  type t_state is (RESET, STOP_FLUSH, STOP_READING, WAIT_FOR_SOP, WRITING, SET_STOP, STOP_WRITING, LAST_WRITE_BURST, START_READING, READING);

  -- record for readability
  type t_reg is record
  -- state of program
  state                       : t_state;
  started                     : std_logic;

  -- stopping flush
  timer                       : natural;

  -- stopping signals
  ready_for_set_stop          : std_logic;
  stop_adr                    : std_logic_vector(c_adr_w - 1 downto 0);
  last_adr_to_write_to        : std_logic_vector(c_adr_w - 1 downto 0);
  stop_burstsize              : natural;
  stopped                     : std_logic;
  rst_ddrctrl_input_ac        : std_logic;

  -- writing signals
  wr_burst_en                 : std_logic;
  wr_bursts_ready             : natural;

  -- reading signals
  outp_bsn                    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  read_adr                    : natural;
  rd_burst_en                 : std_logic;

  -- output
  dvr_mosi                    : t_mem_ctlr_mosi;
  wr_sosi                     : t_dp_sosi;
  ddrctrl_ctrl_state          : std_logic_vector(32 - 1 downto 0);
  end record;

  constant c_t_reg_init       : t_reg         := (RESET, '0', 4, '0', TO_UVEC(g_max_adr, c_adr_w), (others => '0'), 0, '1', '1', '0', 0, (others => '0'), 0, '1', c_mem_ctlr_mosi_rst, c_dp_sosi_init, (others => '0'));

  -- signals for readability
  signal d_reg                : t_reg         := c_t_reg_init;
  signal q_reg                : t_reg         := c_t_reg_init;
begin
  q_reg <= d_reg when rising_edge(clk);

  -- put the input data into c_v and fill the output vector from c_v
  p_state : process(q_reg, rst, inp_of, inp_sosi, inp_adr, inp_bsn_adr, inp_data_stopped, dvr_miso, rd_fifo_usedw, wr_fifo_usedw, stop_in)

    variable v                : t_reg         := c_t_reg_init;
  begin
    v         := q_reg;
    v.wr_sosi := inp_sosi;
    --v.ddrctrl_ctrl_state(c_high_bsn_ndx DOWNTO c_low_bsn_ndx)       := inp_sosi.bsn(c_start_bsn+c_bsn_ndx_w-1 DOWNTO c_start_bsn);
    --v.ddrctrl_ctrl_state(c_high_adr_ndx DOWNTO c_low_adr_ndx)       := TO_UVEC(inp_adr, 32)(c_adr_ndx_w-1 DOWNTO 0);

    case q_reg.state is
    when RESET =>
      v                                                             := c_t_reg_init;
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(0, c_state_ndx_w);

      if rst = '0' and wr_siso.ready = '1' then
        v.state := STOP_FLUSH;
        v.timer := 0;
      end if;

    when STOP_FLUSH =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx)   := TO_UVEC(1, c_state_ndx_w);
      v.wr_sosi.valid                                                 := '0';
      if flush_state = "10" then
        v.dvr_mosi.burstbegin                                         := '1';
        v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0)    := (others => '0');
        v.dvr_mosi.wr                                                 := '1';
      elsif flush_state = "11" and q_reg.timer = 0 then
        v.wr_sosi.valid                                               := '1';
        v.timer := 127;
      end if;

      if q_reg.timer > 0 and rst = '0' then
        v.timer := q_reg.timer - 1;
      end if;

      if flush_state = "01" then
        v.state   := WAIT_FOR_SOP;
        v.stopped := '0';
      end if;

    when STOP_READING =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(2, c_state_ndx_w);
      -- this is the last read burst, this make sure every data containing word in the memory has been read.
      if TO_UINT(rd_fifo_usedw) <= g_burstsize and dvr_miso.done = '1' and q_reg.rd_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.dvr_mosi.address(c_adr_w - 1 downto 0)  := q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0);
        v.dvr_mosi.burstsize    := TO_UVEC(q_reg.stop_burstsize, dvr_mosi.burstsize'length);
        v.stopped               := '0';
        v.wr_sosi.valid         := '1';
        v.state                 := WAIT_FOR_SOP;
        v.wr_burst_en           := '1';
        v.rst_ddrctrl_input_ac  := '1';
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '0';
      v.dvr_mosi.rd             := '1';

      if dvr_miso.done = '0' then
        v.rd_burst_en := '1';
      end if;

    when WAIT_FOR_SOP =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(3, c_state_ndx_w);
      v.dvr_mosi.burstbegin                                         := '0';
      v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0)    := (others => '0');
      v.dvr_mosi.wr                                                 := '1';
      v.dvr_mosi.rd                                                 := '0';
      v.rst_ddrctrl_input_ac  := '0';
      if q_reg.started = '0' and inp_sosi.eop = '1' then
        v.wr_sosi.valid       := '0';
      elsif inp_sosi.sop = '1' then
        v.state               := WRITING;
      else
        v.wr_sosi.valid       := '0';
      end if;

    when WRITING =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(4, c_state_ndx_w);
      -- this state generates the rest of the write bursts, it also checks if there is a stop signal or if it needs to stop writing.
      v.wr_bursts_ready         := TO_UINT(wr_fifo_usedw(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if q_reg.wr_bursts_ready >= 1 and dvr_miso.done = '1' and q_reg.wr_burst_en = '1' and q_reg.dvr_mosi.burstbegin = '0' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en           := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * (q_reg.wr_bursts_ready - 1)), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en           := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;

      if stop_in = '1' then
        v.ready_for_set_stop := '1';
      end if;

      if q_reg.ready_for_set_stop = '1' and inp_sosi.eop = '1' and stop_in = '0' then
        v.state := SET_STOP;
      elsif q_reg.stop_adr = TO_UVEC(inp_adr, c_adr_w) then
        v.state := STOP_WRITING;
      end if;

    when SET_STOP =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(5, c_state_ndx_w);
      -- this state sets a stop address dependend on the g_stop_percentage.
      if inp_adr - c_pof_ma >= 0 then
        v.stop_adr(c_adr_w - 1 downto 0) := TO_UVEC(inp_adr - c_pof_ma, c_adr_w);
      else
        v.stop_adr(c_adr_w - 1 downto 0) := TO_UVEC(inp_adr + g_max_adr - c_pof_ma, c_adr_w);
      end if;
      v.ready_for_set_stop                                        := '0';
      if v.stop_adr(c_adr_w - 1 downto 0) = c_stop_adr_zeros(c_adr_w - 1 downto 0) then
        v.last_adr_to_write_to(c_adr_w - 1 downto 0)                := TO_UVEC(g_max_adr - g_last_burstsize, c_adr_w);
      else
        v.last_adr_to_write_to(c_adr_w - 1 downto c_bitshift_w)     := v.stop_adr(c_adr_w - 1 downto c_bitshift_w);
      end if;
      v.last_adr_to_write_to(c_bitshift_w - 1 downto 0)             := (others => '0');
      v.stop_burstsize                                            := TO_UINT(INCR_UVEC(INCR_UVEC(v.stop_adr(c_adr_w - 1 downto 0), -1 * TO_UINT(v.last_adr_to_write_to)), 1));

      -- still a write cyle
      -- if adr mod g_burstsize = 0
      -- this makes sure that only ones every 64 writes a writeburst is started.
      v.wr_bursts_ready         := TO_UINT(wr_fifo_usedw(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en            := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;
      if dvr_miso.done = '1' and q_reg.wr_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en            := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * (q_reg.wr_bursts_ready - 1)), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if q_reg.stop_adr = TO_UVEC(inp_adr, c_adr_w) then
        v.state := STOP_WRITING;
      else
        v.state := WRITING;
      end if;

    when STOP_WRITING =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(6, c_state_ndx_w);
      -- this state stops the writing by generating one last whole write burst which almost empties wr_fifo.
      v.wr_sosi.valid       := '0';
      v.dvr_mosi.burstbegin := '0';
      v.stopped             := '1';
      v.stop_adr            := TO_UVEC(g_max_adr, c_adr_w);

      -- still receiving write data.
      v.wr_bursts_ready         := TO_UINT(INCR_UVEC(wr_fifo_usedw, 2)(g_wr_fifo_uw_w - 1 downto c_bitshift_w));
      if not (q_reg.wr_bursts_ready = 0) and q_reg.dvr_mosi.burstbegin = '0'THEN
        v.wr_burst_en            := '1';
      elsif q_reg.wr_bursts_ready = 0 then
        v.wr_burst_en           := '0';
      end if;
      if dvr_miso.done = '1' and q_reg.wr_burst_en = '1' then
        v.dvr_mosi.burstbegin   := '1';
        v.wr_burst_en           := '0';
        if inp_adr < (g_burstsize * q_reg.wr_bursts_ready) - 1 then
          v.dvr_mosi.address    := TO_UVEC(g_max_adr - g_last_burstsize - (g_burstsize * q_reg.wr_bursts_ready - 1), dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address    := TO_UVEC(inp_adr - (g_burstsize * q_reg.wr_bursts_ready), dvr_mosi.address'length);
          v.dvr_mosi.address(c_bitshift_w - 1 downto 0) := c_zeros(c_bitshift_w - 1 downto 0);  -- makes sure that a burst is only started on a multiple of g_burstsize
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        end if;
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

      if dvr_miso.done = '1' and q_reg.dvr_mosi.burstbegin = '0' and q_reg.wr_burst_en = '0' and q_reg.wr_bursts_ready = 0 and inp_data_stopped = '1' and TO_UINT(wr_fifo_usedw) <= q_reg.stop_burstsize then
        v.state := LAST_WRITE_BURST;
      end if;

    when LAST_WRITE_BURST =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(7, c_state_ndx_w);
      -- this state stops the writing by generatign one last write burst which empties wr_fifo.
      v.wr_sosi.valid           := '0';
      if dvr_miso.done = '1' then
        if TO_UINT(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0)) >= g_max_adr then
          v.dvr_mosi.address(c_adr_w - 1 downto 0)  := TO_UVEC(0, c_adr_w);
          v.dvr_mosi.burstsize    :=  TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
        else
          v.dvr_mosi.address(c_adr_w - 1 downto 0)  := q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0);
          v.dvr_mosi.burstsize    := TO_UVEC(q_reg.stop_burstsize, dvr_mosi.burstsize'length);
        end if;
        v.dvr_mosi.burstbegin   := '1';
        v.state                 := START_READING;
        v.rd_burst_en           := '1';
      else
        v.dvr_mosi.burstbegin   := '0';
      end if;
      v.dvr_mosi.wr             := '1';
      v.dvr_mosi.rd             := '0';

    when START_READING =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(8, c_state_ndx_w);
      -- this state generates the first read burst, the size of this burst is dependend on the size of the last write burst.
      v.dvr_mosi.burstbegin     := '0';
      v.outp_bsn                := INCR_UVEC(inp_sosi.bsn, -1 * g_bim);
      v.wr_sosi.valid           := '0';

      if dvr_miso.done = '1' and v.rd_burst_en = '1' and q_reg.dvr_mosi.burstbegin = '0' then
        if TO_UINT(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0)) + q_reg.stop_burstsize >= g_max_adr then
          v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0) := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
          v.dvr_mosi.address(c_adr_w - 1 downto 0)  := TO_UVEC(0, c_adr_w);
          v.read_adr            := g_burstsize;
        else
          v.dvr_mosi.burstsize(dvr_mosi.burstsize'length - 1 downto 0) := TO_UVEC(g_burstsize - q_reg.stop_burstsize, dvr_mosi.burstsize'length);
          v.dvr_mosi.address(c_adr_w - 1 downto 0)  := INCR_UVEC(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0), q_reg.stop_burstsize);
          v.read_adr            := TO_UINT(q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0)) + g_burstsize;
        end if;
        v.dvr_mosi.burstbegin   := '1';
        v.dvr_mosi.wr           := '0';
        v.dvr_mosi.rd           := '1';
        v.rd_burst_en           := '0';
      end if;

      -- makes sure the fifo is filled before asking for another rd request. to prevent 4 rd burst to happend directly after one another.
      if dvr_miso.done = '0' and q_reg.rd_burst_en = '0' then
        v.rd_burst_en := '1';
        v.state       := READING;
      end if;

    when READING =>
      v.ddrctrl_ctrl_state(c_high_state_ndx downto c_low_state_ndx) := TO_UVEC(9, c_state_ndx_w);
      v.wr_sosi.valid         := '0';
      -- rd_fifo needs a refil after rd_fifo_usedw <= 10 because of delays, if you wait until rd_fifo_usedw = 0 then you get an empty fifo which results in your outputs sosi.valid not being constatly valid.
      if TO_UINT(rd_fifo_usedw) <= g_burstsize and dvr_miso.done = '1' and q_reg.rd_burst_en = '1' then
        v.dvr_mosi.wr         := '0';
        v.dvr_mosi.rd         := '1';
        v.dvr_mosi.burstbegin := '1';
        v.rd_burst_en         := '0';
        if q_reg.read_adr > g_max_adr - g_burstsize then
          v.dvr_mosi.address    := TO_UVEC(q_reg.read_adr, dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_last_burstsize, dvr_mosi.burstsize'length);
          v.read_adr            := 0;
        else
          v.dvr_mosi.address    := TO_UVEC(q_reg.read_adr, dvr_mosi.address'length);
          v.dvr_mosi.burstsize  := TO_UVEC(g_burstsize, dvr_mosi.burstsize'length);
          v.read_adr            := q_reg.read_adr + g_burstsize;
        end if;
      else
        v.dvr_mosi.burstbegin := '0';
      end if;

      -- makes sure the fifo is filled before asking for another rd request. to prevent 4 rd burst to happend directly after one another.
      if dvr_miso.done = '0' then
        v.rd_burst_en := '1';
      end if;

      -- goes to STOP_reading when this read burst was from the burstblock before q_reg.stop_adr
      if q_reg.last_adr_to_write_to(c_adr_w - 1 downto 0) = TO_UVEC(q_reg.read_adr, c_adr_w) then
        v.state := STOP_READING;
      end if;
    end case;

    if rst = '1' then
      v.state := RESET;
    end if;

    if inp_sosi.eop = '1' then
      v.started := '1';
    end if;

    v.ddrctrl_ctrl_state(c_always_one_ndx)  := '1';
    v.ddrctrl_ctrl_state(c_rst_ndx)         := rst;
    v.ddrctrl_ctrl_state(c_done_ndx)        := dvr_miso.done;
    v.ddrctrl_ctrl_state(c_bb_ndx)          := q_reg.dvr_mosi.burstbegin;
    v.ddrctrl_ctrl_state(c_ben_ndx)         := q_reg.wr_burst_en;
    v.ddrctrl_ctrl_state(c_das_ndx)         := inp_data_stopped;
    v.ddrctrl_ctrl_state(c_high_bre_ndx downto c_low_bre_ndx) := TO_UVEC(q_reg.wr_bursts_ready, 32)(c_bre_ndx_w - 1 downto 0);

    d_reg     <= v;
  end process;

  -- fill outputs
  dvr_mosi              <= q_reg.dvr_mosi;
  wr_sosi               <= q_reg.wr_sosi;
  stop_out              <= q_reg.stopped;
  outp_bsn              <= q_reg.outp_bsn;
  rst_ddrctrl_input_ac  <= q_reg.rst_ddrctrl_input_ac or rst;
  ddrctrl_ctrl_state    <= q_reg.ddrctrl_ctrl_state;
end rtl;
