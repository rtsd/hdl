-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose:
--   Deinterleave g_nof_in inputs based on g_deint_block_size and re-interleave
--   these streams onto g_nof_out outputs based on g_inter_block_size.
-- Description:
--   The input and output streams are concatenated into one SLV.
-- Remarks:
-- . g_inter_block_size >= g_deint_block_size;
-- . One valid input applies to all input data streams;
-- . The user must take care of the correct valid/gap ratio on the inputs.

entity common_reinterleave is
  generic (
    g_nof_in         : natural;
    g_deint_block_size  : natural;
    g_nof_out        : natural;
    g_inter_block_size : natural;
    g_dat_w          : natural;
    g_align_out  : boolean := false
 );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    in_dat      : in  std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);
    in_val      : in  std_logic;

    out_dat     : out std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
    out_val     : out std_logic_vector(g_nof_out - 1 downto 0)
  );
end;

architecture rtl of common_reinterleave is
  type t_dat_arr is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);
  type t_val_arr is array (integer range <>) of std_logic;

  -----------------------------------------------------------------------------
  -- Constants to determine the number of instances. General rule:
  -- . Deinterleavers: one for each input  stream.
  -- . Interleavers:   one for each output stream.
  -- Exceptions:
  -- . Multi  in, single out = one interleaver   instance only
  -- . Single in, multi  out = one deinterleaver instance only
  -- . One in,    one out    = wires
  -----------------------------------------------------------------------------
  constant c_interleave_only   : boolean := g_nof_in > 1 and g_nof_out = 1;
  constant c_deinterleave_only : boolean := g_nof_in = 1 and g_nof_out > 1;
  constant c_wires_only        : boolean := g_nof_in = 1 and g_nof_out = 1;

  constant c_nof_deint : natural := sel_a_b( c_interleave_only,   0,
                                    sel_a_b( c_deinterleave_only, 1,
                                    sel_a_b( c_wires_only,        0,
                                                                  g_nof_in)));

  constant c_nof_inter : natural := sel_a_b( c_interleave_only,   1,
                                    sel_a_b( c_deinterleave_only, 0,
                                    sel_a_b( c_wires_only,        0,
                                                                  g_nof_out)));

  constant c_nof_deint_out : natural := g_nof_out;
  constant c_nof_inter_in  : natural := g_nof_in;

  -----------------------------------------------------------------------------
  -- in_dat array to the deinterleavers
  -----------------------------------------------------------------------------
  signal deint_in_dat_arr : t_dat_arr(c_nof_deint - 1 downto 0);
  signal deint_in_val     : std_logic;

  -----------------------------------------------------------------------------
  -- Array of concatenated deinterleaver outputs
  -----------------------------------------------------------------------------
  type t_deint_out_concat_dat_arr is array (c_nof_deint - 1 downto 0) of std_logic_vector(c_nof_deint_out * g_dat_w - 1 downto 0);
  type t_deint_out_concat_val_arr is array (c_nof_deint - 1 downto 0) of std_logic_vector(c_nof_deint_out - 1 downto 0);

  signal deint_out_concat_dat_arr : t_deint_out_concat_dat_arr;
  signal deint_out_concat_val_arr : t_deint_out_concat_val_arr;

  -----------------------------------------------------------------------------
  -- Array of de-concatenated deinterleaver outputs
  -----------------------------------------------------------------------------
  type t_deint_out_dat_2arr is array (c_nof_deint - 1 downto 0) of t_dat_arr(c_nof_deint_out - 1 downto 0);
  type t_deint_out_val_2arr is array (c_nof_deint - 1 downto 0) of t_val_arr(c_nof_deint_out - 1 downto 0);

  signal deint_out_dat_2arr : t_deint_out_dat_2arr;
  signal deint_out_val_2arr : t_deint_out_val_2arr;

  -----------------------------------------------------------------------------
  -- Array of de-concatenated interleaver inputs
  -----------------------------------------------------------------------------
  type t_inter_in_dat_2arr is array (c_nof_inter - 1 downto 0) of t_dat_arr(c_nof_inter_in - 1 downto 0);
  type t_inter_in_val_2arr is array (c_nof_inter - 1 downto 0) of t_val_arr(c_nof_inter_in - 1 downto 0);

  signal inter_in_dat_2arr : t_inter_in_dat_2arr;
  signal inter_in_val_2arr : t_inter_in_val_2arr;

  -----------------------------------------------------------------------------
  -- Array of concatenated interleaver inputs
  -----------------------------------------------------------------------------
  type t_inter_in_concat_dat_arr is array (c_nof_inter - 1 downto 0) of std_logic_vector(c_nof_inter_in * g_dat_w - 1 downto 0);
  type t_inter_in_concat_val_arr is array (c_nof_inter - 1 downto 0) of std_logic_vector(c_nof_inter_in - 1 downto 0);

  signal inter_in_concat_dat_arr : t_inter_in_concat_dat_arr;
  signal inter_in_concat_val_arr : t_inter_in_concat_val_arr;

  -----------------------------------------------------------------------------
  -- out_dat array from the interleavers
  -----------------------------------------------------------------------------
  signal inter_out_dat_arr : t_dat_arr(c_nof_inter - 1 downto 0);
  signal inter_out_val     : std_logic_vector(c_nof_inter - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Deinterleavers and their input wiring
  -----------------------------------------------------------------------------
  gen_deint: for i in 0 to c_nof_deint - 1 generate
    deint_in_dat_arr(i) <= in_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
    deint_in_val        <= in_val;

    u_deinterleave : entity work.common_deinterleave
    generic map (
      g_nof_out => c_nof_deint_out,
      g_dat_w   => g_dat_w,
      g_block_size => g_deint_block_size,
      g_align_out => g_align_out
    )
    port map (
      rst        => rst,
      clk        => clk,

      in_dat     => deint_in_dat_arr(i),
      in_val     => deint_in_val,

      out_dat    => deint_out_concat_dat_arr(i),
      out_val    => deint_out_concat_val_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Wire the array of concatenated deinterleaver outputs to an array
  -- of de-concatenated (so another array) deinterleaver outputs.
  -- Use the entity outputs if no interleavers are instantiated.
  -----------------------------------------------------------------------------
  gen_wire_deint_out: if c_deinterleave_only = false generate
    gen_wires_deint : for i in 0 to c_nof_deint - 1 generate
      gen_deint_out : for j in 0 to c_nof_deint_out - 1 generate
        deint_out_dat_2arr(i)(j) <= deint_out_concat_dat_arr(i)(j * g_dat_w + g_dat_w - 1 downto j * g_dat_w);
        deint_out_val_2arr(i)(j) <= deint_out_concat_val_arr(i)(j);
      end generate;
    end generate;
  end generate;

  gen_wire_deint_only: if c_deinterleave_only = true generate
    out_dat <= deint_out_concat_dat_arr(0);
    out_val <= deint_out_concat_val_arr(0);
  end generate;

  -----------------------------------------------------------------------------
  -- Deinterleavers -> Interleavers interconnections
  -----------------------------------------------------------------------------
  gen_interconnect: if c_deinterleave_only = false and c_interleave_only = false generate
    gen_wires_deint : for i in 0 to c_nof_deint - 1 generate
      gen_deint_out : for j in 0 to c_nof_deint_out - 1 generate
        inter_in_dat_2arr(j)(i) <= deint_out_dat_2arr(i)(j);
        inter_in_val_2arr(j)(i) <= deint_out_val_2arr(i)(j);
      end generate;
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- Wire the array of concatenated interleaver inputs to an array
  -- of de-concatenated (so another array) interleaver inputs.
  -- Use the entity inputs if no deinterleavers are instantiated.
  -----------------------------------------------------------------------------
  gen_wire_inter_arr: if c_interleave_only = false generate
    gen_nof_inter : for i in 0 to c_nof_inter - 1 generate
      gen_inter_in : for j in 0 to c_nof_inter_in - 1 generate
        inter_in_concat_dat_arr(i)(j * g_dat_w + g_dat_w - 1 downto j * g_dat_w) <= inter_in_dat_2arr(i)(j);
        inter_in_concat_val_arr(i)(j)                                     <= inter_in_val_2arr(i)(j);
      end generate;
    end generate;
  end generate;

  gen_wire_inter_only: if c_interleave_only = true generate
    inter_in_concat_dat_arr(0)    <= in_dat;

    gen_inter_in : for i in 0 to c_nof_inter_in - 1 generate
      inter_in_concat_val_arr(0)(i) <= in_val;
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- Interleavers and their output wiring
  -----------------------------------------------------------------------------
  gen_inter: for i in 0 to c_nof_inter - 1 generate
    u_interleave : entity work.common_interleave
    generic map (
      g_nof_in     => c_nof_inter_in,
      g_dat_w      => g_dat_w,
      g_block_size => g_inter_block_size
    )
    port map (
      rst        => rst,
      clk        => clk,

      in_dat     => inter_in_concat_dat_arr(i),
      in_val     => inter_in_concat_val_arr(i)(0),  -- All input streams are valid at the same time.

      out_dat    => inter_out_dat_arr(i),
      out_val    => inter_out_val(i)
    );

    out_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w) <= inter_out_dat_arr(i);
    out_val(i)                                      <= inter_out_val(i);
  end generate;
end rtl;
