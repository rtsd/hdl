-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Provide Ethernet control access to a node and some UDP ports for streaming
--   data.
-- Description:
--   Connect the 1GbE TSE to the microprocessor and to streaming UDP ports. The
--   packets for the streaming channels are directed based on the UDP port
--   number and all other packets are transfered to the default control channel.

library IEEE, common_lib, technology_lib, dp_lib, eth_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_select_pkg.all;

entity eth1g is
  generic (
    g_technology         : natural := c_tech_select_default;
    g_init_ip_address    : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := X"00000000";  -- 0.0.0.0
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_ETH_PHY            : string  := "LVDS";  -- "LVDS" (default): uses LVDS IOs for ctrl_unb_common, "XCVR": uses tranceiver PHY
    g_ihl20              : boolean := false;
    g_frm_discard_en     : boolean := false  -- when TRUE discard frame types that would otherwise have to be discarded by the Nios MM master
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk             : in  std_logic;  -- memory-mapped bus clock
    eth_clk            : in  std_logic;  -- ethernet phy reference clock
    st_rst             : in  std_logic;  -- reset synchronous with st_clk
    st_clk             : in  std_logic;  -- packet stream clock

    cal_rec_clk        : in  std_logic := '0';  -- Calibration & reconfig clock when using XCVR

    -- UDP transmit interface
    udp_tx_snk_in_arr  : in  t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_sosi_rst);  -- ST sinks, default not valid if not used
    udp_tx_snk_out_arr : out t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);
    -- UDP receive interface
    udp_rx_src_in_arr  : in  t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rdy);  -- ST sources, default ready if not used
    udp_rx_src_out_arr : out t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);

    -- Memory Mapped Slaves
    tse_sla_in         : in  t_mem_mosi;  -- ETH TSE MAC registers
    tse_sla_out        : out t_mem_miso;
    reg_sla_in         : in  t_mem_mosi;  -- ETH control and status registers
    reg_sla_out        : out t_mem_miso;
    reg_sla_interrupt  : out std_logic;  -- Interrupt
    ram_sla_in         : in  t_mem_mosi;  -- ETH rx frame and tx frame memory
    ram_sla_out        : out t_mem_miso;

    -- Monitoring
    rx_flushed_frm_cnt : out std_logic_vector(c_word_w - 1 downto 0);  -- only used in simulation, because it will get optimized away in synthesis

    -- PHY interface
    eth_txp            : out std_logic;
    eth_rxp            : in  std_logic;

    -- LED interface
    tse_led            : out t_tech_tse_led
  );
end eth1g;

architecture str of eth1g is
  ------------------------------------------------------------------------------
  -- ETH Rx packet buffer and Tx packet buffer
  ------------------------------------------------------------------------------

  -- Use MM bus data width = c_word_w = 32
  constant c_mm_ram  : t_c_mem := (latency  => c_mem_ram_rd_latency,
                                   adr_w    => c_eth_ram_addr_w,
                                   dat_w    => c_word_w,
                                   nof_dat  => c_eth_ram_nof_words,
                                   init_sl  => '0');

  signal mem_in         : t_mem_mosi;  -- big endian on ST and TSE MAC network side
  signal mem_out        : t_mem_miso;  -- big endian on ST and TSE MAC network side
  signal mem_in_endian  : t_mem_mosi;  -- keep big endian on MM side
  signal mem_out_endian : t_mem_miso;  -- keep big endian on MM side

  ------------------------------------------------------------------------------
  -- ETH stream
  ------------------------------------------------------------------------------

  -- Multiplex - demultiplex
  constant c_mux_nof_ports    : natural := 1 + c_eth_nof_udp_ports;  -- One for control + nof UDP ports
  constant c_demux_nof_ports  : natural := c_mux_nof_ports;
  constant c_demux_combined   : boolean := false;  -- when TRUE then all downstream sinks must be ready, when FALSE then only the
                                                   -- selected sink needs to be ready (see dp_demux for more explanation).
  -- All Rx (so UDP off-load and other ETH traffic)
  signal rx_adapt_siso        : t_dp_siso;
  signal rx_adapt_sosi        : t_dp_sosi;

  signal rx_crc_siso          : t_dp_siso;
  signal rx_crc_sosi          : t_dp_sosi;

  signal rx_ihl20_siso        : t_dp_siso;
  signal rx_ihl20_sosi        : t_dp_sosi;

  signal rx_hdr_siso          : t_dp_siso;
  signal rx_hdr_sosi          : t_dp_sosi;
  signal rx_hdr_status          : t_eth_hdr_status;
  signal rx_hdr_status_complete : std_logic;

  -- Rx demux
  signal rx_channel_siso      : t_dp_siso;
  signal rx_channel_sosi      : t_dp_sosi;

  signal demux_siso_arr       : t_dp_siso_arr(0 to c_mux_nof_ports - 1);
  signal demux_sosi_arr       : t_dp_sosi_arr(0 to c_mux_nof_ports - 1);

  -- ETH Rx
  signal eth_rx_siso          : t_dp_siso;
  signal eth_rx_sosi          : t_dp_sosi;

  signal rx_frm_discard         : std_logic;
  signal rx_frm_discard_val     : std_logic;
  signal rx_eth_discard         : std_logic;
  signal rx_eth_discard_val     : std_logic;

  signal rx_frame_rd            : std_logic;
  signal rx_frame_ack           : std_logic;
  signal rx_frame_done          : std_logic;
  signal rx_frame_sosi          : t_dp_sosi;
  signal rx_frame_hdr_words_arr : t_network_total_header_32b_arr;
  signal rx_frame_hdr_fields    : t_network_total_header;
  signal rx_frame_hdr_status    : t_eth_hdr_status;
  signal rx_frame_crc_word      : std_logic_vector(c_eth_data_w - 1 downto 0);

  -- ETH Tx
  signal eth_tx_siso          : t_dp_siso;
  signal eth_tx_sosi          : t_dp_sosi;

  -- Tx mux
  signal mux_siso_arr         : t_dp_siso_arr(0 to c_mux_nof_ports - 1);
  signal mux_sosi_arr         : t_dp_sosi_arr(0 to c_mux_nof_ports - 1);

  -- All Tx (so UDP off-load and other ETH traffic)
  signal tx_mux_siso          : t_dp_siso;
  signal tx_mux_sosi          : t_dp_sosi;

  signal tx_hdr_siso          : t_dp_siso;
  signal tx_hdr_sosi          : t_dp_sosi;

  ------------------------------------------------------------------------------
  -- MM registers (in st_clk domain)
  ------------------------------------------------------------------------------

  -- . write/read back
  signal reg_demux            : t_eth_mm_reg_demux;
  signal reg_config           : t_eth_mm_reg_config;
  signal reg_control          : t_eth_mm_reg_control;
  signal reg_continue_wr      : std_logic;
  -- . read only
  signal reg_frame            : t_eth_mm_reg_frame;
  signal reg_status           : t_eth_mm_reg_status;
  signal reg_status_wr        : std_logic;

  ------------------------------------------------------------------------------
  -- TSE MAC
  ------------------------------------------------------------------------------
  -- . MAC Transmit Stream
  signal tse_tx_siso        : t_dp_siso;
  signal tse_tx_sosi        : t_dp_sosi;
  -- . MAC specific
  signal tse_tx_mac_in      : t_tech_tse_tx_mac;
  signal tse_tx_mac_out     : t_tech_tse_tx_mac;
  -- . MAC Receive Stream
  signal tse_rx_sosi        : t_dp_sosi;
  signal tse_rx_siso        : t_dp_siso;
  -- . MAC specific
  signal tse_rx_mac_out     : t_tech_tse_rx_mac;
begin
  ------------------------------------------------------------------------------
  -- MM registers
  ------------------------------------------------------------------------------

  u_mm_registers : entity eth_lib.eth_mm_registers
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_init_ip_address    => g_init_ip_address
  )
  port map (
    -- Clocks and reset
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    st_rst             => st_rst,
    st_clk             => st_clk,
    -- Memory Mapped Slave
    sla_in             => reg_sla_in,
    sla_out            => reg_sla_out,
    sla_interrupt      => reg_sla_interrupt,
    -- MM registers in st_clk domain
    -- . write/read back
    st_reg_demux       => reg_demux,
    st_reg_config      => reg_config,
    st_reg_control     => reg_control,
    st_reg_continue_wr => reg_continue_wr,
    -- . read only
    st_reg_frame       => reg_frame,
    st_reg_status      => reg_status,
    st_reg_status_wr   => reg_status_wr
  );

  -- Packet buffer
  u_mm_ram : entity common_lib.common_ram_crw_crw
  generic map (
    g_technology => g_technology,
    g_ram        => c_mm_ram
  )
  port map (
    rst_a     => mm_rst,
    clk_a     => mm_clk,
    wr_en_a   => ram_sla_in.wr,
    adr_a     => ram_sla_in.address(c_mm_ram.adr_w - 1 downto 0),
    wr_dat_a  => ram_sla_in.wrdata(c_mm_ram.dat_w - 1 downto 0),
    rd_en_a   => ram_sla_in.rd,
    rd_dat_a  => ram_sla_out.rddata(c_mm_ram.dat_w - 1 downto 0),
    rd_val_a  => ram_sla_out.rdval,
    rst_b     => st_rst,
    clk_b     => st_clk,
    wr_en_b   => mem_in_endian.wr,
    adr_b     => mem_in_endian.address(c_mm_ram.adr_w - 1 downto 0),
    wr_dat_b  => mem_in_endian.wrdata(c_mm_ram.dat_w - 1 downto 0),
    rd_en_b   => mem_in_endian.rd,
    rd_dat_b  => mem_out_endian.rddata(c_mm_ram.dat_w - 1 downto 0),
    rd_val_b  => mem_out_endian.rdval
  );

  -- The Rx, Tx packet buffer is big-endian
  mem_in_endian <= func_mem_swap_endianess(mem_in, c_word_sz);
  mem_out       <= func_mem_swap_endianess(mem_out_endian, c_word_sz);

  ------------------------------------------------------------------------------
  -- RX : Adapt the TSE RX source ready latency from 2 to 1
  ------------------------------------------------------------------------------

  u_adapt : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => c_eth_rx_ready_latency,  -- = 2
    g_out_latency => c_eth_ready_latency  -- = 1
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    -- ST sink
    snk_out => tse_rx_siso,
    snk_in  => tse_rx_sosi,
    -- ST source
    src_in  => rx_adapt_siso,
    src_out => rx_adapt_sosi
  );

  ------------------------------------------------------------------------------
  -- RX : Replace the CRC word with the stream error field from the TSE MAC
  ------------------------------------------------------------------------------

  u_crc_ctrl : entity eth_lib.eth_crc_ctrl
  port map (
    rst            => st_rst,
    clk            => st_clk,

    -- Streaming Sink
    snk_in_err     => rx_adapt_sosi.err(c_tech_tse_error_w - 1 downto 0),  -- preserve error field from TSE MAC stream
    snk_in         => rx_adapt_sosi,
    snk_out        => rx_adapt_siso,

    -- Streaming Source
    src_in         => rx_crc_siso,
    src_out        => rx_crc_sosi,  -- replaced CRC word by snk_in_err, so CRC word /=0 indicates any TSE error
    src_out_err    => open  -- flag snk_in_err/=0 at src_out.eop
  );

  ------------------------------------------------------------------------------
  -- RX : Strip the option words from the IPv4 header
  ------------------------------------------------------------------------------
  gen_ihl20: if g_ihl20 generate
    u_ihl20 : entity eth_lib.eth_ihl_to_20
    port map (
      rst            => st_rst,
      clk            => st_clk,

      -- Streaming Sink
      snk_in         => rx_crc_sosi,
      snk_out        => rx_crc_siso,

      -- Streaming Source
      src_in         => rx_ihl20_siso,
      src_out        => rx_ihl20_sosi
    );
  end generate;

  no_ihl20: if not g_ihl20 generate
    rx_ihl20_sosi <= rx_crc_sosi;
    rx_crc_siso   <= rx_ihl20_siso;
  end generate;

  ------------------------------------------------------------------------------
  -- RX : For IP verify IP header checksum
  ------------------------------------------------------------------------------

  u_rx_frame : entity eth_lib.eth_hdr
  generic map (
    g_header_store_and_forward     => true,
    g_ip_header_checksum_calculate => true
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => rx_ihl20_sosi,
    snk_out         => rx_ihl20_siso,

    -- Streaming Source
    src_in          => rx_hdr_siso,
    src_out         => rx_hdr_sosi,

    -- Frame control
    frm_discard     => rx_eth_discard,
    frm_discard_val => rx_eth_discard_val,

    -- Header info
    hdr_status          => rx_hdr_status,
    hdr_status_complete => rx_hdr_status_complete
  );

  rx_eth_discard     <= rx_frm_discard      when g_frm_discard_en = true else '0';
  rx_eth_discard_val <= rx_frm_discard_val  when g_frm_discard_en = true else '1';

  u_frm_discard : entity eth_lib.eth_frm_discard
  generic map (
    g_support_dhcp       => true,
    g_support_udp_onload => false
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- MM control
    reg_config      => reg_config,
    reg_demux       => reg_demux,

    -- ST info
    hdr_status          => rx_hdr_status,
    hdr_status_complete => rx_hdr_status_complete,

    -- Frame discard decision
    frm_discard     => rx_frm_discard,
    frm_discard_val => rx_frm_discard_val
  );

  ------------------------------------------------------------------------------
  -- Demux the UDP off-load traffic and the keep the other ETH traffic
  ------------------------------------------------------------------------------

  -- Put UDP off-load traffic on channel > 0
  -- Put other ETH    traffic on channel = 0 for further internal processing
  u_udp_channel : entity eth_lib.eth_udp_channel
  port map (
    -- Clocks and reset
    rst            => st_rst,
    clk            => st_clk,

    -- Streaming Sink
    snk_in         => rx_hdr_sosi,
    snk_out        => rx_hdr_siso,

    -- Streaming Source with channel field
    src_in         => rx_channel_siso,
    src_out        => rx_channel_sosi,

    -- Demux control
    reg_demux      => reg_demux,
    hdr_status     => rx_hdr_status
  );

  -- Demultiplex channel 0 for internal handling and the other channels > 0 for external UDP off-load.
  u_rx_demux : entity dp_lib.dp_demux
  generic map (
    g_nof_output    => c_demux_nof_ports,
    g_combined      => c_demux_combined
  )
  port map (
    rst         => st_rst,
    clk         => st_clk,
    -- ST sinks
    snk_out     => rx_channel_siso,
    snk_in      => rx_channel_sosi,
    -- ST source
    src_in_arr  => demux_siso_arr,
    src_out_arr => demux_sosi_arr
  );

  -- Fixed local ETH port
  eth_rx_sosi       <= demux_sosi_arr(0);
  demux_siso_arr(0) <= eth_rx_siso;

  -- UDP offload ports
  gen_udp_rx_demux : for i in 1 to c_eth_nof_udp_ports generate
    udp_rx_src_out_arr(i - 1) <= demux_sosi_arr(i);
    demux_siso_arr(i)       <= udp_rx_src_in_arr(i - 1);
  end generate;

  ------------------------------------------------------------------------------
  -- ETH RX frame buffer
  ------------------------------------------------------------------------------

  u_rx_buffer : entity eth_lib.eth_buffer
  generic map (
    g_technology   => g_technology
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => eth_rx_sosi,
    snk_out         => eth_rx_siso,

    -- Streaming Source
    -- . The src_rd, src_ack and src_done act instead of src_in.ready to have src_in ready per frame instead of per data word
    src_rd          => rx_frame_rd,  -- request frame pulse
    src_ack         => rx_frame_ack,  -- acknowledge request
    src_done        => rx_frame_done,  -- signal frame received
    src_out         => rx_frame_sosi,

    -- Monitoring
    flushed_frm_cnt => rx_flushed_frm_cnt
  );

  ------------------------------------------------------------------------------
  -- ETH RX frame monitor
  ------------------------------------------------------------------------------

  -- Extract total header
  u_rx_hdr_info : entity eth_lib.eth_hdr
  generic map (
    g_header_store_and_forward     => false,
    g_ip_header_checksum_calculate => false
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => rx_frame_sosi,

    -- Header info
    hdr_words_arr   => rx_frame_hdr_words_arr,
    hdr_fields      => rx_frame_hdr_fields,
    hdr_status      => rx_frame_hdr_status
  );

  -- Extract CRC word (enumerate: 0=OK, >0 AND odd = Error)
  u_rx_crc_word : entity eth_lib.eth_crc_word
  port map (
    rst            => st_rst,
    clk            => st_clk,

    -- Streaming Sink
    snk_in         => rx_frame_sosi,

    -- CRC word
    crc_word       => rx_frame_crc_word,
    crc_word_val   => open
  );

  u_mm_reg_frame : entity eth_lib.eth_mm_reg_frame
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Inputs need for the frame register
    hdr_fields      => rx_frame_hdr_fields,
    hdr_status      => rx_frame_hdr_status,
    erc_word        => rx_frame_crc_word,
    reg_config      => reg_config,

    -- Frame register
    reg_frame       => reg_frame
  );

  ------------------------------------------------------------------------------
  -- ETH Control
  ------------------------------------------------------------------------------

  u_control : entity eth_lib.eth_control
  port map (
    -- Clocks and reset
    rst               => st_rst,
    clk               => st_clk,

    -- Control register
    reg_config        => reg_config,
    reg_control       => reg_control,
    reg_continue_wr   => reg_continue_wr,
    reg_status        => reg_status,
    reg_status_wr     => reg_status_wr,

    -- Streaming sink Rx frame
    rcv_rd            => rx_frame_rd,
    rcv_ack           => rx_frame_ack,
    rcv_done          => rx_frame_done,
    rcv_in            => rx_frame_sosi,
    rcv_hdr_words_arr => rx_frame_hdr_words_arr,
    rcv_hdr_status    => rx_frame_hdr_status,

    -- Streaming source Tx frame
    xmt_in            => eth_tx_siso,
    xmt_out           => eth_tx_sosi,

    -- MM frame memory
    mem_in            => mem_in,
    mem_out           => mem_out
  );

  ------------------------------------------------------------------------------
  -- TX : Mux UDP
  ------------------------------------------------------------------------------

  -- Fixed local ETH
  eth_tx_siso     <= mux_siso_arr(0);
  mux_sosi_arr(0) <= eth_tx_sosi;

  -- UDP offload ports
  gen_udp_tx_mux : for i in 1 to c_eth_nof_udp_ports generate
    udp_tx_snk_out_arr(i - 1) <= mux_siso_arr(i);
    mux_sosi_arr(i)         <= udp_tx_snk_in_arr(i - 1);
  end generate;

  -- Multiplex the two input streams on to the single ETH stream
  u_tx_mux : entity dp_lib.dp_mux
  generic map (
    g_technology      => g_technology,
    g_data_w          => c_eth_data_w,
    g_empty_w         => c_eth_empty_w,
    g_in_channel_w    => 1,
    g_error_w         => 1,
    g_use_empty       => true,
    g_use_in_channel  => false,
    g_use_error       => false,
    g_nof_input       => c_mux_nof_ports,
    g_use_fifo        => false,
    g_fifo_size       => array_init(1024, c_mux_nof_ports),  -- input FIFOs are not used, but generic must match g_nof_input
    g_fifo_fill       => array_init(   0, c_mux_nof_ports)  -- input FIFOs are not used, but generic must match g_nof_input
  )
  port map (
    rst         => st_rst,
    clk         => st_clk,
    -- ST sinks
    snk_out_arr => mux_siso_arr,  -- OUT = request to upstream ST source
    snk_in_arr  => mux_sosi_arr,
    -- ST source
    src_in      => tx_mux_siso,  -- IN  = request from downstream ST sink
    src_out     => tx_mux_sosi
  );

  ------------------------------------------------------------------------------
  -- TX : For IP insert IP header checksum
  ------------------------------------------------------------------------------

  u_tx_frame : entity eth_lib.eth_hdr
  generic map (
    g_header_store_and_forward     => true,
    g_ip_header_checksum_calculate => true
  )
  port map (
    -- Clocks and reset
    rst             => st_rst,
    clk             => st_clk,

    -- Streaming Sink
    snk_in          => tx_mux_sosi,
    snk_out         => tx_mux_siso,

    -- Streaming Source
    src_in          => tx_hdr_siso,
    src_out         => tx_hdr_sosi
  );

  ------------------------------------------------------------------------------
  -- TSE MAC
  ------------------------------------------------------------------------------
  tx_hdr_siso <= tse_tx_siso;
  tse_tx_sosi <= func_dp_stream_error_set(tx_hdr_sosi, 0);  -- set err field (value 0 for OK)

  tse_tx_mac_in.crc_fwd <= '0';  -- when '0' then TSE MAC generates the TX CRC field

  u_tech_tse : entity tech_tse_lib.tech_tse
  generic map (
    g_technology   => g_technology,
    g_ETH_PHY      => g_ETH_PHY
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    eth_clk        => eth_clk,
    tx_snk_clk     => st_clk,
    rx_src_clk     => st_clk,
    cal_rec_clk    => cal_rec_clk,
    -- Memory Mapped Slave
    mm_sla_in      => tse_sla_in,
    mm_sla_out     => tse_sla_out,
    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      => tse_tx_sosi,
    tx_snk_out     => tse_tx_siso,
    -- . MAC specific
    tx_mac_in      => tse_tx_mac_in,
    tx_mac_out     => tse_tx_mac_out,  -- OPEN
    -- MAC receive interface
    -- . ST Source
    rx_src_in      => tse_rx_siso,
    rx_src_out     => tse_rx_sosi,
    -- . MAC specific
    rx_mac_out     => tse_rx_mac_out,
    -- PHY interface
    eth_txp        => eth_txp,
    eth_rxp        => eth_rxp,
    -- LED interface
    tse_led        => tse_led
  );
end str;
