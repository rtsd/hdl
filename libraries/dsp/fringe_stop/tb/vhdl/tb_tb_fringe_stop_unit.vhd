-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the fringe_stop_unit.
--
--
-- Usage
--   > as 8
--   > run -all
--   > Testbench is self-checking
--   > Evalute the WAVE window.

library IEEE, common_lib, technology_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;

entity tb_tb_fringe_stop_unit is
end tb_tb_fringe_stop_unit;

architecture tb of tb_tb_fringe_stop_unit is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- Usage
--   > as 8
--   > run -all
--   > Testbenches are self-checking

--    g_sim_type             : NATURAL  := 0;   -- 0 = Increment, 1 = Increment over the maximum, 2 = Decrease, 3 = Decrease over the minimum
--    g_in_dat_w             : POSITIVE := 8;   -- Width of the incoming data.
--    g_fs_offset_w          : POSITIVE := 10;  -- Width of the offset of the linear coefficient
--    g_accu_w               : POSITIVE := 31;  -- Width of the accumulation register
--    g_fs_step_w            : POSITIVE := 17;  -- Width of the step of the linear coefficient
--    g_nof_channels         : POSITIVE := 4;   -- Number of serial channels for which the fringe stopping must be applied uniquely
--    g_phasor_w             : POSITIVE := 9    -- Width of the phasor values in the lookup table
--    g_phi_minus_sign       : BOOLEAN := TRUE; -- Apply exp(j*phi) or exp(-j*phi) based on phasor lookup table address phi
--

  gen_sim_types : for I in 0 to 3 generate
    sim_phi       : entity work.tb_fringe_stop_unit generic map (I, 8, 10, 31, 17, 4, 9, false);
    sim_minus_phi : entity work.tb_fringe_stop_unit generic map (I, 8, 10, 31, 17, 4, 9, true);
  end generate;
end tb;
