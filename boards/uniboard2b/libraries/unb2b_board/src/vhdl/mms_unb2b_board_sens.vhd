-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for unb2b_board_sens
-- Description: See unb2b_board_sens.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mms_unb2b_board_sens is
  generic (
    g_sim             : boolean := false;
    g_i2c_peripheral  : natural;
    g_sens_nof_result : natural;  -- Should match nof read bytes via I2C in the unb2b_board_sens_ctrl SEQUENCE list
    g_clk_freq        : natural := 100 * 10**6;  -- clk frequency in Hz
    g_temp_high       : natural := 85;
    g_comma_w         : natural := 0
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    mm_start          : in  std_logic;

    -- Memory-mapped clock domain
    reg_mosi          : in  t_mem_mosi := c_mem_mosi_rst;  -- actual ranges defined by c_mm_reg
    reg_miso          : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- i2c bus
    scl               : inout std_logic := 'Z';
    sda               : inout std_logic := 'Z';

    -- Temperature alarm output
    temp_alarm        : out std_logic
  );
end mms_unb2b_board_sens;

architecture str of mms_unb2b_board_sens is
  constant c_temp_high_w     : natural := 7;  -- Allow user to use only 7 (no sign, only positive) of 8 bits to set set max temp

  signal sens_err  : std_logic;
  signal sens_data : t_slv_8_arr(0 to g_sens_nof_result - 1);

  signal temp_high : std_logic_vector(c_temp_high_w - 1 downto 0);
begin
  u_unb2b_board_sens_reg : entity work.unb2b_board_sens_reg
  generic map (
    g_sens_nof_result => g_sens_nof_result,
    g_temp_high       => g_temp_high
  )
  port map (
    -- Clocks and reset
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in       => reg_mosi,
    sla_out      => reg_miso,

    -- MM registers
    sens_err     => sens_err,  -- using same protocol list for both node2 and all nodes implies that sens_err is only valid for node2.
    sens_data    => sens_data,

    -- Max temp threshold
    temp_high    => temp_high
  );

  u_unb2b_board_sens : entity work.unb2b_board_sens
  generic map (
    g_sim             => g_sim,
    g_i2c_peripheral  => g_i2c_peripheral,
    g_clk_freq        => g_clk_freq,
    g_temp_high       => g_temp_high,
    g_sens_nof_result => g_sens_nof_result,
    g_comma_w         => g_comma_w
  )
  port map (
    clk          => mm_clk,
    rst          => mm_rst,
    start        => mm_start,
    -- i2c bus
    scl          => scl,
    sda          => sda,
    -- read results
    sens_evt     => OPEN,
    sens_err     => sens_err,
    sens_data    => sens_data
  );

  -- Temperature: 7 bits (1 bit per degree) plus sign. A faulty readout (never pulled down = all ones)
  -- would produce -1 degrees so does not trigger a temperature alarm.
  -- temp_high is 7 bits, preceded by a '0' to allow only positive temps to be set.
  temp_alarm <= '1' when (signed(sens_data(0)) > signed('0' & temp_high)) else '0';
end str;
