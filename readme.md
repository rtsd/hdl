# Gitlab CI/CD

HDL linting is using *gitlab-runners* on *dop349*.

Linting tool used is *vhdl-style-guide* (VSG)

link: https://vhdl-style-guide.readthedocs.io/en/latest/index.html

##### Linting settings.

By default all rules are disabled in de *vsg_config.yaml* file.

The only active rule on this moment is *case.keyword* and is set to *upper*.





