module altjesd_ss_RX_xcvr_reset_control (
		input  wire       clock,              //              clock.clk
		output wire [0:0] pll_powerdown,      //      pll_powerdown.pll_powerdown
		input  wire       reset,              //              reset.reset
		output wire [0:0] rx_analogreset,     //     rx_analogreset.rx_analogreset
		input  wire [0:0] rx_cal_busy,        //        rx_cal_busy.rx_cal_busy
		output wire [0:0] rx_digitalreset,    //    rx_digitalreset.rx_digitalreset
		input  wire [0:0] rx_is_lockedtodata, // rx_is_lockedtodata.rx_is_lockedtodata
		output wire [0:0] rx_ready            //           rx_ready.rx_ready
	);
endmodule

