/* *************************************************************************
* Copyright 2021
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Reinier vd Walle
* Purpose:
* . Test the lofar2_unb2b_ring OpenCL application in emulator
* Description:
* . Run: -> make lofar2_unb2b_ring
* . Navigate to -> cd $HDL_WORK/unb2b/OpenCL/lofar2_unb2b_ring/bin
* . Execute -> CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 ./host
* *********************************************************************** */
#include <CL/cl_ext_intelfpga.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "common.h"
#include <unistd.h>

using namespace std;
int main(int argc, char **argv)
{
    if (argc > 2) {
        cerr << "usage: " << argv[0] << " [lofar2_unb2b_ring.aocx]" << endl;
        exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Get program
    string filename_bin = string(argc == 2 ? argv[1] : "lofar2_unb2b_ring.aocx");
    cl::Program program = get_program(context, device, filename_bin);


    // Setup command queues
    vector<cl::CommandQueue> queues(8);

    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    cl::Event computeDone[8];

    // Setup FPGA kernels
    cl::Kernel mmInController(program, "mm_in_controller");
    cl::Kernel mmOutController(program, "mm_out_controller");
    cl::Kernel interfaceSelect(program, "interface_select");
    cl::Kernel blockValidateDecode(program, "block_validate_decode");
    cl::Kernel rxSplit(program, "rx_split");
    cl::Kernel validateBsnAtSync(program, "validate_bsn_at_sync");
//    cl::Kernel noValidateBsnAtSync(program, "no_validate_bsn_at_sync");
    cl::Kernel validateChannel(program, "validate_channel");
//    cl::Kernel noValidateChannel(program, "no_validate_channel");
    cl::Kernel txEncode(program, "tx_encode");

    // Run FPGA kernels
    clog << ">>> Run fpga" << endl;
    try {
        queues[0].enqueueTask(txEncode, nullptr, &computeDone[0]);
        queues[1].enqueueTask(validateChannel, nullptr, &computeDone[1]);
        queues[2].enqueueTask(validateBsnAtSync, nullptr, &computeDone[2]);
        queues[3].enqueueTask(rxSplit, nullptr, &computeDone[3]);
        queues[4].enqueueTask(blockValidateDecode, nullptr, &computeDone[4]);
        queues[5].enqueueTask(interfaceSelect, nullptr, &computeDone[5]);
        queues[6].enqueueTask(mmOutController, nullptr, &computeDone[6]);
        queues[7].enqueueTask(mmInController, nullptr, &computeDone[7]);

    } catch (cl::Error &error) {
        cerr << "Error launching kernel: " << error.what() << endl;
        exit(EXIT_FAILURE);
    }

    // Write IO channel file
    // Input packet channel = 1
    vector<unsigned char> inputVecs[] = {{0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0xAA, 0xBB, 0x01, 0x00, 0x00, 0x00, 0x00}, //First
                                {0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x60, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00},
                                {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x02, 0x00, 0x00, 0x00, 0x00}}; // Last

    // dp lane input, channel = 0
    vector<unsigned char> dpVecs[] = {{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},//First
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}; // Last

    
    ofstream output_fileA("kernel_input_10GbE_ring_0");
    ostream_iterator<char> output_iteratorA(output_fileA, "");
    for (int i = 0; i < 11; i++)
      copy(inputVecs[i].begin(), inputVecs[i].end(), output_iteratorA);

    output_fileA.close();

    ofstream output_fileX("kernel_input_lane_0");
    ostream_iterator<char> output_iteratorX(output_fileX, "");
    for (int i = 0; i < 8; i++)
      copy(dpVecs[i].begin(), dpVecs[i].end(), output_iteratorX);

    output_fileX.close(); 

 
    clog << ">>> Written IO files" << endl;

    // wait for validate_bsn_at_sync to be finished
    computeDone[2].wait();

    // print output IO channel file
    const string inputFileB = "kernel_output_lane_0";
    ifstream fileB(inputFileB);
    clog << fileB.rdbuf() << endl;

    // wait for interface_select to be finished
    computeDone[5].wait();

    // print output IO channel file
    const string inputFileY = "kernel_output_10GbE_ring_0";
    ifstream fileY(inputFileY);
    clog << fileY.rdbuf() << endl;

    return EXIT_SUCCESS;
}
