-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench running two tech_eth_10g with ppm offset.
-- Description:
--   The two instances of tb_tech_eth_10g have a 100 ppm offset in their
--   tr_ref_clk_644.
--   The tb is self checking based on that tb_tech_eth_10g is self checking
--   and both tb_tech_eth_10g instances send the same and expect the same.
-- Remarks:
--   . For c_tech_arria10_proto the test fails when g_nof_10ppm /= 0 (erko, 21 nov 2014)
--   . For c_tech_stratixiv the test fails when g_nof_10ppm /= 0 (erko, 5 dec 2014)
-- Usage:
--   > as 16
--   > run -all

library IEEE, technology_lib, tech_pll_lib, tech_mac_10g_lib, common_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tb_tech_eth_10g_ppm is
  -- Test bench control parameters
  generic (
    g_technology : natural := c_tech_select_default;
    g_nof_10ppm  : integer := 1  -- use /= 0 to verify XO ppm offset between two devices
  );
end tb_tech_eth_10g_ppm;

architecture tb of tb_tech_eth_10g_ppm is
  -- PHY 10gbase_r
  signal serial_tx_0 : std_logic;  -- 1 lane
  signal serial_tx_1 : std_logic;

  -- PHY XAUI
  signal xaui_tx_0   : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);  -- 4 lanes
  signal xaui_tx_1   : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
begin
  u_tb_tech_eth_10g_0 : entity work.tb_tech_eth_10g
  generic map (
    g_technology              => g_technology,
    g_ref_clk_644_period      => tech_pll_clk_644_period,
    g_ref_clk_156_period      => 6.4 ns,
    g_verify_link_recovery    => false,
    g_use_serial_rx_in        => true
  )
  port map (
    -- PHY 10gbase_r
    serial_tx_out => serial_tx_0,
    serial_rx_in  => serial_tx_1,

    -- PHY XAUI
    xaui_tx_out   => xaui_tx_0,
    xaui_rx_in    => xaui_tx_1
  );

  u_tb_tech_eth_10g_1 : entity work.tb_tech_eth_10g
  generic map (
    g_technology              => g_technology,
    g_ref_clk_644_period      => tech_pll_clk_644_period + tech_pll_clk_644_10ppm * g_nof_10ppm,
    g_ref_clk_156_period      => 6.4 ns + 64 fs * g_nof_10ppm,
    g_verify_link_recovery    => false,
    g_use_serial_rx_in        => true
  )
  port map (
    -- PHY 10gbase_r
    serial_tx_out => serial_tx_1,
    serial_rx_in  => serial_tx_0,

    -- PHY XAUI
    xaui_tx_out   => xaui_tx_1,
    xaui_rx_in    => xaui_tx_0
  );
end tb;
