#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the wpfb_unit entity. 
   This testcase should be used in conjunction with the tb_mmf_wpfb_unit.vhd file. 
   the g_use_bg generic should be set to TRUE.
   
   This testcase writes data to the block generators that are part of the wpfb.
   Than it enables the block generators and the data is verified. 
   
   The block generator that is part of the testbench is NOT used. 

"""

###############################################################################
# System imports
import test_case
import node_io
import unb_apertif as apr
import pi_diag_block_gen
import pi_diag_data_buffer
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *

###############################################################################
# Setup
c_nof_points        = 128  
c_wb_factor         = 4
c_nof_channels      = c_wb_factor
c_bg_buf_size       = c_nof_points/c_wb_factor
c_blocks_per_sync   = 32  
c_fft_out_dat_w     = 16

# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for wpfb_unit entity')
tc.append_log(3, '>>>')
tc.append_log(3, '')

# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg_pfb = pi_diag_block_gen.PiDiagBlockGen(tc, io, instanceName = 'PFB', nofChannels = c_nof_channels, nodeNr = tc.nodeBnNrs, ramSizePerChannel=c_bg_buf_size)

# Create databuffer instances
db_re = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'REAL', nofStreams=c_nof_channels, nodeNr=tc.nodeBnNrs)
db_im = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'IMAG', nofStreams=c_nof_channels, nodeNr=tc.nodeBnNrs)

# After 100 ns mm_rst is deasserted for sure, so we can perform MM accesses.
if tc.sim==True:
    do_until_gt(io.simIO.getSimTime, 100, s_timeout=3600)

# Write setting for the block generator:
bg_pfb.write_block_gen_settings(samplesPerPacket=c_bg_buf_size, blocksPerSync=c_blocks_per_sync, gapSize=0, memLowAddr=0, memHighAddr=c_bg_buf_size-1, BSNInit=0)

# Prepare data stimuli
bg_data = []
bg_real = []
bg_imag = []
for i in range(c_nof_points/2): 
  real_a = 0
  real_a_mask = real_a & (2**c_fft_out_dat_w-1)  
  real_b = 1
  real_b_mask = real_b & (2**c_fft_out_dat_w-1)  
  
  imag_a = i
  imag_a_mask = imag_a & (2**c_fft_out_dat_w-1)  
  imag_b = i                                
  imag_b_mask = imag_b & (2**c_fft_out_dat_w-1)  
  
  bg_real.append(real_a)
  bg_real.append(real_b)
  bg_imag.append(imag_a)
  bg_imag.append(imag_b)
  bg_data.append((imag_a_mask << c_fft_out_dat_w) + real_a_mask)
  bg_data.append((imag_b_mask << c_fft_out_dat_w) + real_b_mask)

print max(bg_real)
print min(bg_real)
print len(bg_data)

# Write the stimuli to the block generator and enable the block generator         
for i in range(c_wb_factor):       
  send_data = []
  for j in range(c_bg_buf_size):
    send_data.append(bg_data[c_bg_buf_size*i+j])     #
  bg_pfb.write_waveform_ram(data=send_data, channelNr= i)
bg_pfb.write_enable()  

tc.sleep(1)

# Read the data back from the databuffer
buf_data_real = []
buf_data_imag = []
for i in range(c_wb_factor):       
  buf_data_real.append(db_re.read_data_buffer(streamNr=i, n=c_bg_buf_size))
  buf_data_imag.append(db_im.read_data_buffer(streamNr=i, n=c_bg_buf_size))

buf_data_real = flatten(buf_data_real)
buf_data_imag = flatten(buf_data_imag)

# Compare input data with output data
err_cnt = 0
for i in range(c_nof_points):                                                                                         
  if(buf_data_real[i] != bg_real[i]):
    err_cnt = err_cnt + 1
  if(buf_data_real[i] != bg_real[i]):
    err_cnt = err_cnt + 1

print "Number of readback errors is: " + str(err_cnt)

if err_cnt != 0:
  tc.set_result('FAILED') 
      
###############################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(3, '>>>')

        

