%
% Calculate golden FFT results for TCL signals
%
clear all;

% Prepare position and size on screen
close all;
h = get(0,'ScreenSize');
px = round(0.2*h(3));
py = round(0.4*h(4));
pw = round(0.5*h(3));
ph = round(0.5*h(4));
pdx = ceil(0.04*pw);
pdy = ceil(0.04*ph);


fnames = char('cosin_N2.sig',    ...
              'cosin_1.sig',     ...
              'cosin_39.sig',     ...
              'sinus_1.sig',     ...
              'sinus_13.sig',    ...
              'sinus_13s.sig',   ...
              'impulse_0.sig',   ...
              'impulse_1.sig',   ...
              'zeros.sig',       ...
              'dc.sig',          ...
              'block_1.sig',     ...
              'block_117.sig',   ...
              'u_noise.sig');
              
[r c]     = size(fnames);
nof_files = r;


for i = 1:nof_files,
  % Load signal file
  fname = deblank(fnames(i,:));
  s = load(fname);
  
  norm = max([max(s) -min(s)]);
  if norm == 0
    norm = 1;
  end
  fprintf('%20s, norm = %f\n',fname,norm);

  % Calculate FFT
  n = length(s);
  b = fft(s,n);
  re = real(b);
  im = imag(b);
  
  % Plot FFT
  f = 0:n-1;
  figure('position',[px py pw ph]); px = px + pdx; py = py - pdy;
  plot(f, re/norm, 'b', f, im/norm, 'r');
  title(fname)
  grid on;

  % Save FFT  
  fname = fname(1:length(fname)-4);  % strip '.sig'
  
  % Save Re part
  fname_re = strcat(fname, '.re');
  fid = fopen(fname_re,'w');
  fprintf(fid,'%f\n',re);
  fclose(fid);
  
  % Save Im part
  fname_im = strcat(fname, '.im');
  fid = fopen(fname_im,'w');
  fprintf(fid,'%f\n',im);
  fclose(fid);
end
