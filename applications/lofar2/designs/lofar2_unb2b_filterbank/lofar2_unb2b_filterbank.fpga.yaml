schema_name   : args
schema_version: 1.0
schema_type   : fpga

hdl_library_name: lofar2_unb2b_filterbank
fpga_name       : lofar2_unb2b_filterbank
fpga_description: "FPGA design lofar2_unb2b_filterbank"

peripherals:
  #############################################################################
  # Factory / minimal (see ctrl_unb2b_board.vhd)
  #############################################################################
  - peripheral_name: unb2b_board/system_info
    lock_base_address: 0x10000
    mm_port_names:
      - ROM_SYSTEM_INFO
      - PIO_SYSTEM_INFO

  - peripheral_name: unb2b_board/wdi
    mm_port_names:
      - REG_WDI

  - peripheral_name: unb2b_board/unb2_fpga_sens
    mm_port_names:
      - REG_FPGA_TEMP_SENS
      - REG_FPGA_VOLTAGE_SENS
    
  - peripheral_name: unb2b_board/ram_scrap
    mm_port_names:
      - RAM_SCRAP
      
  - peripheral_name: eth/eth
    mm_port_names:
      - AVS_ETH_0_TSE
      - AVS_ETH_0_REG
      - AVS_ETH_0_RAM
      
  - peripheral_name: ppsh/ppsh
    mm_port_names:
      - PIO_PPS
      
  - peripheral_name: epcs/epcs
    parameter_overrides:
      - { name: "g_epcs_addr_w", value: 32 }
    mm_port_names:
      - REG_EPCS
      
  - peripheral_name: dp/dpmm
    mm_port_names:
      - REG_DPMM_CTRL
      - REG_DPMM_DATA
      
  - peripheral_name: dp/mmdp
    mm_port_names:
      - REG_MMDP_CTRL
      - REG_MMDP_DATA
      
  - peripheral_name: remu/remu
    parameter_overrides:
      - { name: g_data_w, value: 32 }
    mm_port_names:
      - REG_REMU
 
  #############################################################################
  # AIT = ADC Input and Timing (see node_adc_input_and_timing.vhd)
  #############################################################################
  
  - peripheral_name: tech_jesd204b/jesd_ctrl
    mm_port_names:
      - PIO_JESD_CTRL
      
  - peripheral_name: tech_jesd204b/jesd204b_arria10
    mm_port_names:
      - JESD204B
  
  - peripheral_name: dp/dp_shiftram
    parameter_overrides:
      - { name: g_nof_streams, value: 12 }  # = S_pn
      - { name: g_nof_words, value: 4096 }
      - { name: g_data_w, value: 16 }
    mm_port_names:
      - REG_DP_SHIFTRAM

  - peripheral_name: dp/dp_bsn_source
    parameter_overrides:
      - { name: g_nof_block_per_sync, value: 390625 }  # EK: TODO temporarily use 390625 = 2 * 195312.5, to have integer number of blocks in 2 s sync interval, TODO: remove when REG_BSN_SOURCE_V2 is used
    mm_port_names:
      - REG_BSN_SOURCE
      
  # TODO: Use REG_BSN_SOURCE_V2 instead of REG_BSN_SOURCE
  #peripheral_name: dp/dp_bsn_source_v2
  #parameter_overrides:
  #  - { name: g_nof_clk_per_sync, value: 200000000 }  # = f_adc
  #  - { name: g_block_size, value: 1024 }       # = N_fft
  #  - { name: g_bsn_time_offset_w, value: 10 }  # note: g_bsn_time_offset_w = ceil_log2(g_block_size)
  #mm_port_names:
  #  - REG_BSN_SOURCE_V2
      
  - peripheral_name: dp/dp_bsn_scheduler
    mm_port_names:
      - REG_BSN_SCHEDULER
  
  - peripheral_name: dp/dp_bsn_monitor
    peripheral_group: input
    mm_port_names:
      - REG_BSN_MONITOR_INPUT
  
  - peripheral_name: diag/diag_wg_wideband
    parameter_overrides:
      - { name: g_nof_streams, value: 12 }  # = S_pn
    mm_port_names:
      - REG_WG
      - RAM_WG
      
  - peripheral_name: aduh/aduh_mon_dc_power
    parameter_overrides:
      - { name: g_nof_streams, value: 12 }  # = S_pn
    mm_port_names:
      - REG_ADUH_MONITOR

  # Commented RAM_ADUH_MON, because use RAM_DIAG_DATA_BUF_BSN instead
  #- peripheral_name: aduh/aduh_mon_data_buffer
  #  parameter_overrides:
  #    - { name: g_nof_streams, value: 12 }  # = S_pn
  #    - { name: g_symbol_w, value: 16 }
  #    - { name: g_nof_symbols_per_data, value: 1 }
  #    - { name: g_buffer_nof_symbols, value: 512 }
  #    - { name: g_buffer_use_sync, value: true }
  #  mm_port_names:
  #    - RAM_ADUH_MON

  - peripheral_name: diag/diag_data_buffer
    peripheral_group: bsn
    parameter_overrides:
      - { name: g_nof_streams, value: 12 }  # = S_pn
      - { name: g_data_w, value: 16 }
      - { name: g_nof_data, value: 1024 }
    mm_port_names:
      - REG_DIAG_DATA_BUF_BSN
      - RAM_DIAG_DATA_BUF_BSN
  
  #############################################################################
  # Fsub = Subband Filterbank (from node_sdp_filterbank.vhd)
  #############################################################################
  
  - peripheral_name: si/si
    mm_port_names:
      - REG_SI
      
  - peripheral_name: filter/fil_ppf_w
    parameter_overrides:
      - { name: g_fil_ppf.wb_factor, value: 1 } # process at sample rate
      - { name: g_fil_ppf.nof_chan, value: 0 } # process at sample rate
      - { name: g_fil_ppf.nof_bands, value: 1024 }  # = N_fft
      - { name: g_fil_ppf.nof_taps, value: 16 }  # = N_taps
      - { name: g_fil_ppf.nof_streams, value: 1 }
      - { name: g_fil_ppf.coef_dat_w, value: 16 }  # = W_fir_coef
    mm_port_names:
      - RAM_FIL_COEFS

  - peripheral_name: sdp/sdp_subband_equalizer
    mm_port_names:
      - RAM_EQUALIZER_GAINS
      
  - peripheral_name: dp/dp_selector
    mm_port_names:
      - REG_DP_SELECTOR   # input_select = 0 for weighted subbands, input_select = 1 for raw subbands
      
  - peripheral_name: st/st_sst_for_sdp
    mm_port_names:
      - RAM_ST_SST
      
  - peripheral_name: common/common_variable_delay
    mm_port_names:
      - REG_STAT_ENABLE

  - peripheral_name: sdp/sdp_statistics_offload_hdr_dat_sst
    mm_port_names:
      - REG_STAT_HDR_DAT
