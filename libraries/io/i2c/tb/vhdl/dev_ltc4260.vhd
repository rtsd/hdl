-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.i2c_dev_ltc4260_pkg.all;

entity dev_ltc4260 is
  generic(
    g_address : std_logic_vector(c_byte_w - 2 downto 0);  -- 7 bit address, so without R/Wn bit
    g_R_sense : real := 0.01
  );
  port(
    scl               : inout std_logic := 'H';  -- Default output 'H' (instead of 'X' !)
    sda               : inout std_logic;
    ana_current_sense : in    real;
    ana_volt_source   : in    real;
    ana_volt_adin     : in    real
  );
end dev_ltc4260;

architecture beh of dev_ltc4260 is
  -- Convert V sense into I sense
  constant c_I_unit_sense  : real := LTC4260_V_UNIT_SENSE / g_R_sense;  -- = 0.3 mV / 10 mOhm

  -- Digitized values
  signal dig_current_sense : integer;
  signal dig_volt_source   : integer;
  signal dig_volt_adin     : integer;

  -- I2C control
  signal enable      : std_logic;  -- new access, may be write command or write data
  signal stop        : std_logic;  -- end of access
  signal wr_dat      : std_logic_vector(c_byte_w - 1 downto 0);  -- I2C write data
  signal wr_val      : std_logic;
  signal rd_dat      : std_logic_vector(c_byte_w - 1 downto 0);  -- I2C read data
  signal rd_req      : std_logic;

  signal cmd_en      : std_logic := '0';
  signal cmd         : std_logic_vector(c_byte_w - 1 downto 0);  -- device command

  -- LTC4260 registers (with power up defaults)
  signal control_reg : std_logic_vector(c_byte_w - 1 downto 0) := std_logic_vector(to_unsigned(LTC4260_CONTROL_DEFAULT, c_byte_w));
begin
  -- Digitize the measured current and voltages
  dig_current_sense <= integer(ana_current_sense / c_I_unit_sense);  -- e.g. 5.0 A / 0.03 = 166
  dig_volt_source   <= integer(ana_volt_source / LTC4260_V_UNIT_SOURCE);  -- e.g. 48.0 V / 0.4 = 120
  dig_volt_adin     <= integer(ana_volt_adin / LTC4260_V_UNIT_ADIN);

  i2c_slv_device : entity work.i2c_slv_device
  generic map (
    g_address => g_address
  )
  port map (
    scl     => scl,
    sda     => sda,
    en      => enable,
    p       => stop,
    wr_dat  => wr_dat,
    wr_val  => wr_val,
    rd_req  => rd_req,
    rd_dat  => rd_dat
  );

  -- Support PROTOCOL_WRITE_BYTE
  p_write : process (enable, wr_val)  -- first write byte is treated as command
  begin
    if rising_edge(enable) then
      cmd_en <= '1';
    elsif falling_edge(enable) then
      cmd_en <= '0';
    end if;
    if rising_edge(wr_val) then
      cmd_en <= '0';
      if cmd_en = '1' then
        cmd <= wr_dat;
      else
        case to_integer(unsigned(cmd)) is
          when LTC4260_CMD_CONTROL => control_reg <= wr_dat;
          when others              => null;  -- no further model for write access
        end case;
      end if;
    end if;
  end process;

  -- Support PROTOCOL_READ_BYTE
  p_read : process (rd_req)
  begin
    if rising_edge(rd_req) then
      case to_integer(unsigned(cmd)) is  -- only model read I and V
        when LTC4260_CMD_CONTROL => rd_dat <= control_reg;
        when LTC4260_CMD_ALERT   => rd_dat <= (others => '1');
        when LTC4260_CMD_STATUS  => rd_dat <= (others => '1');
        when LTC4260_CMD_FAULT   => rd_dat <= (others => '1');
        when LTC4260_CMD_SENSE   => rd_dat <= std_logic_vector(to_unsigned(dig_current_sense, c_byte_w));
        when LTC4260_CMD_SOURCE  => rd_dat <= std_logic_vector(to_unsigned(dig_volt_source,   c_byte_w));
        when LTC4260_CMD_ADIN    => rd_dat <= std_logic_vector(to_unsigned(dig_volt_adin,     c_byte_w));
        when others              => rd_dat <= (others => '1');
      end case;
    end if;
  end process;
end beh;
