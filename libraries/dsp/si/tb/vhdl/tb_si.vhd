-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Spectral inversion.
-- Description:
--   Test bench for si.vhd.
-- Remark:
-- . Ported from LOFAR1 rsp. Made the tb self-stopping and self-checking.
-- Usage:
--   > as 5
--   > run -a
--   view out_dat in radix decimal format in Wave window to see + and - data value

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_si is
end tb_si;

architecture tb of tb_si is
  constant c_clk_period   : time := 10 ns;

  constant c_dat_w        : natural := 5;
  constant c_max          : integer :=  2**(c_dat_w - 1) - 1;
  constant c_min          : integer := -2**(c_dat_w - 1);
  constant c_block_size   : natural := 9;

  signal in_sosi        : t_dp_sosi;
  signal out_sosi       : t_dp_sosi;

  signal in_dat         : std_logic_vector(c_dat_w - 1 downto 0) := (others => '0');
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_sync        : std_logic;
  signal out_dat        : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_sync       : std_logic;
  signal si_en          : std_logic;
  signal clk            : std_logic := '1';
  signal rst            : std_logic;
  signal tb_end         : std_logic := '0';

  signal verify_en      : std_logic;
  signal toggle         : std_logic;
  signal clip_even      : std_logic;
  signal clip_odd       : std_logic;
  signal cnt_even       : natural;
  signal cnt_odd        : natural;
begin
  rst <= '1', '0' after c_clk_period;
  clk <= not(clk) or tb_end after c_clk_period / 2;

  u_si : entity work.si
  generic map (
    g_pipeline  => 0,
    g_dat_w     => c_dat_w
  )
  port map(
    in_sosi     => in_sosi,
    out_sosi    => out_sosi,
    si_en       => si_en,
    clk         => clk,
    rst         => rst
  );

  -- wires
  in_sosi.sync  <= in_sync;
  in_sosi.sop   <= in_sop;
  in_sosi.valid <= in_val;
  in_sosi.data  <= RESIZE_DP_SDATA(in_dat);
  in_sosi.re    <= TO_DP_DSP_DATA(0);
  in_sosi.im    <= TO_DP_DSP_DATA(0);

  out_sync <= out_sosi.sync;
  out_sop  <= out_sosi.sop;
  out_val  <= out_sosi.valid;
  out_dat  <= out_sosi.data(c_dat_w - 1 downto 0);

  -- Create in_dat with equal value per pair
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      in_dat   <= (others => '0');
      toggle   <= '0';
    elsif rising_edge(clk) then
      if in_val = '1' then
        if toggle = '1' then
          in_dat <= INCR_UVEC(in_dat, 1);
        end if;
        toggle <= not toggle;
      end if;
    end if;
  end process;

  p_stimuli : process
  begin
    verify_en <= '1';
    si_en   <= '1';
    in_sop  <= '0';
    in_sync <= '0';
    in_val  <= '0';
    wait for 10 * c_clk_period;

    ---------------------------------------------------------------------------
    -- First some blocks with +1 * even and -1 * odd index
    -- ==> only clip_odd will occur
    ----------------------------------------------------- ---------------------

    -- pulse in_sync, to check that it is passed on
    -- pulse sop and continue with valid data
    in_sync <= '1';
    in_sop  <= '1';
    in_val  <= '1';
    wait for c_clk_period;
    in_sync <= '0';
    in_sop  <= '0';
    wait for c_block_size * c_clk_period;

    -- insert some valid low cycles
    in_val  <= '0';
    wait for 3 * c_clk_period;
    in_val  <= '1';

    -- some more blocks
    for I in 0 to 15 loop
      in_sop <= '1';
      wait for c_clk_period;
      in_sop <= '0';
      wait for c_block_size * c_clk_period;
    end loop;

    ---------------------------------------------------------------------------
    -- Some blocks with -1 * even and +1 * odd index
    -- ==> only clip_even will occur
    ----------------------------------------------------- ---------------------
    -- insert one extra valid do let in_sop occur on odd index
    wait for c_clk_period;

    -- disable verification while using sop and two valids periods to transition to the new sop index phase
    verify_en <= '0';
    in_sop <= '1';
    wait for c_clk_period;
    in_sop <= '0';
    wait for c_clk_period;
    verify_en <= '1';

    -- some more blocks
    for I in 0 to 15 loop
      in_sop <= '1';
      wait for c_clk_period;
      in_sop <= '0';
      wait for c_block_size * c_clk_period;
    end loop;

    ---------------------------------------------------------------------------
    -- Some blocks with +1 * even and -1 * odd index
    -- ==> only clip_odd will occur
    ----------------------------------------------------- ---------------------
    -- insert one extra valid do let in_sop occur on even index
    wait for c_clk_period;

    -- disable verification while using sop and two valids periods to transition to the new sop index phase
    verify_en <= '0';
    in_sop <= '1';
    wait for c_clk_period;
    in_sop <= '0';
    wait for c_clk_period;
    verify_en <= '1';

    -- some more blocks
    for I in 0 to 15 loop
      in_sop <= '1';
      wait for c_clk_period;
      in_sop <= '0';
      wait for c_block_size * c_clk_period;
    end loop;

    tb_end <= '1';
    wait;
  end process;

  p_verify : process
    variable v_even      : integer;
    variable v_odd       : integer;
    variable v_clip_even : std_logic;
    variable v_clip_odd  : std_logic;
  begin
    -- verify per pair
    wait for c_clk_period;
    proc_common_wait_until_high(clk, out_val);
    v_even := TO_SINT(out_dat);
    wait for c_clk_period;
    proc_common_wait_until_high(clk, out_val);
    v_odd := TO_SINT(out_dat);
    -- identify clip wrap of -c_min to +c_max
    if v_even = c_max and v_odd = c_min then v_clip_even := '1'; else v_clip_even := '0'; end if;
    if v_even = c_min and v_odd = c_max then v_clip_odd  := '1'; else v_clip_odd  := '0'; end if;
    clip_even <= v_clip_even;  -- show in wave window (only cycle late)
    clip_odd  <= v_clip_odd;  -- show in wave window (only cycle late)
    -- compare pair
    if tb_end = '0' then
      if verify_en = '1' then
        if v_even /= -1 * v_odd then
          if not (v_clip_even = '1') then
            if not (v_clip_odd = '1') then
              report "Wrong negate value at valid (v_even = " & int_to_str(v_even) & " v_odd = " & int_to_str(v_odd)
                severity ERROR;
      end if;
          end if;
        end if;
      end if;
    else
      -- Verify expected number of clip_even (when in_sop is at even) and clip_odd (when in_sop is at odd)
      assert cnt_even = 4
        report "Wrong number of expected clipped c_min to c_max at even index"
        severity ERROR;
      assert cnt_odd = 12
        report "Wrong number of expected clipped c_min to c_max at odd index"
        severity ERROR;
      wait;
    end if;
  end process;

  -- Count number of clip_even and clip_odd
  cnt_even <= cnt_even + 1 when rising_edge(clk) and clip_even = '1';
  cnt_odd  <= cnt_odd  + 1 when rising_edge(clk) and clip_odd  = '1';
end tb;
