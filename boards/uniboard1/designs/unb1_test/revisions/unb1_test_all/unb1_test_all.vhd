-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, unb1_test_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity unb1_test_all is
  generic (
    g_design_name : string  := "unb1_test_all";  -- use revision name = entity name = design name
    g_design_note : string  := "Test Design with all";
    g_sim         : boolean := false;  -- Overridden by TB
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0;  -- FN0
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn   : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic;
    ETH_SGOUT    : out   std_logic;

    -- Transceiver clocks
    SA_CLK       : in  std_logic;  -- SerDes Clock BN-BI / SI_FN

    -- Serial I/O
    SI_FN_0_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_0_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_1_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_1_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_2_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_2_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_3_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_3_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

    SI_FN_0_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);  -- (0 = LASI; 1=MDC; 2=MDIO)
    SI_FN_1_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_2_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_3_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_RSTN    : out   std_logic := '1';  -- ResetN is pulled up in the Vitesse chip, but pulled down again by external 1k resistor.

    BN_BI_0_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_0_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_1_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_1_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_2_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_2_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_3_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    BN_BI_3_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

    -- SO-DIMM Memory Bank I
    MB_I_IN       : in    t_tech_ddr3_phy_in;
    MB_I_IO       : inout t_tech_ddr3_phy_io;
    MB_I_OU       : out   t_tech_ddr3_phy_ou;

    -- SO-DIMM Memory Bank II
    MB_II_IN      : in    t_tech_ddr3_phy_in;
    MB_II_IO      : inout t_tech_ddr3_phy_io;
    MB_II_OU      : out   t_tech_ddr3_phy_ou
  );
end unb1_test_all;

architecture str of unb1_test_all is
begin
  u_revision : entity unb1_test_lib.unb1_test
  generic map (
    g_design_name => g_design_name,
    g_design_note => g_design_note,
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn
  )
  port map (
    -- GENERAL
    CLK          => CLK,
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- I2C Interface to Sensors
    sens_sc      => sens_sc,
    sens_sd      => sens_sd,

    -- 1GbE Control Interface
    ETH_clk      => ETH_clk,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,

    -- Serial I/O
    SI_FN_0_TX    => SI_FN_0_TX,
    SI_FN_0_RX    => SI_FN_0_RX,
    SI_FN_1_TX    => SI_FN_1_TX,
    SI_FN_1_RX    => SI_FN_1_RX,
    SI_FN_2_TX    => SI_FN_2_TX,
    SI_FN_2_RX    => SI_FN_2_RX,
    SI_FN_3_TX    => SI_FN_3_TX,
    SI_FN_3_RX    => SI_FN_3_RX,

    SI_FN_0_CNTRL => SI_FN_0_CNTRL,
    SI_FN_1_CNTRL => SI_FN_1_CNTRL,
    SI_FN_2_CNTRL => SI_FN_2_CNTRL,
    SI_FN_3_CNTRL => SI_FN_3_CNTRL,
    SI_FN_RSTN    => SI_FN_RSTN,

    BN_BI_0_TX    => BN_BI_0_TX,
    BN_BI_0_RX    => BN_BI_0_RX,
    BN_BI_1_TX    => BN_BI_1_TX,
    BN_BI_1_RX    => BN_BI_1_RX,
    BN_BI_2_TX    => BN_BI_2_TX,
    BN_BI_2_RX    => BN_BI_2_RX,
    BN_BI_3_TX    => BN_BI_3_TX,
    BN_BI_3_RX    => BN_BI_3_RX,

    MB_I_IN => MB_I_IN,
    MB_I_IO => MB_I_IO,
    MB_I_OU => MB_I_OU,

    MB_II_IN => MB_II_IN,
    MB_II_IO => MB_II_IO,
    MB_II_OU => MB_II_OU
  );
end str;
