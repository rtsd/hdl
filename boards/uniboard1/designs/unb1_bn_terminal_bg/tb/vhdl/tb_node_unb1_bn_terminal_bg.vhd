-----------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify a node_unb1_bn_terminal_bg in a larger UniBoard simulation
-- Description:
-- . See designs/fn_terminal_bf/tb/vhdl/tb_node_bn_bg_terminal_fn_bf.vhd
-- . Standalone this tb can simulate but without verification. This tb should
--   be instantiated in a bigger multi-node UniBoard test bench.
-- . Standalone one can use c_bg_try_re_enable=TRUE to show the DIAG BG off/on
--   behaviour in the Wave window.

library IEEE, common_lib, dp_lib, diag_lib, bf_lib, unb1_board_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use bf_lib.bf_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use diagnostics_lib.tb_diagnostics_trnb_pkg.all;

entity tb_node_unb1_bn_terminal_bg is
  generic (
    -- Tb
    g_sim_level               : natural := 1;  -- 0 = simulate GX IP, 1 = use fast serial behavioural model
    g_nof_sync                : natural := 2;  -- use > c_bst_skip_nof_sync = 1 to also verify the BST once (more sync yield the same BST)
    g_chip_id                 : natural := 4;  -- BN chip ID 4, 5, 6, 7 used in case of multiple tb instances, default BN0 with chip ID 4
    g_bck_id                  : natural := 0;
    -- Application
    g_bf                      : t_c_bf  := c_bf;
    g_use_bg                  : boolean := false;
    g_bg_data_file_index_arr  : t_nat_natural_arr := array_init(0, 16, 1);
    g_bg_data_file_name       : string  := "data/bf_in_data";
    -- Diagnostics - TRNB
    g_diagnostics_en            : boolean := false;
    g_diagnostics_init_interval : real := 6.0;  -- unit us
    g_diagnostics_on_interval   : real := 1.0;  -- unit us
    g_diagnostics_off_interval  : real := 10.3;  -- unit us
    -- Terminals interface
    -- . back
    g_use_back                : boolean := false;

    -- . mesh
    g_mesh_nof_serial         : natural := 3;  -- default only use the 3 full featured transceivers in the mesh (out of maximal 4), using only 2 is not enough
    g_mesh_use_rx             : boolean := false;  -- can be FALSE for BG functionality, use TRUE to support for bidirectional TR diagnostics
    g_mesh_gx_mbps            : natural := 5000;
    g_mesh_mon_select         : natural := 0;  -- 0 = no Tx terminals monitor via MM, else see unb_terminals_mesh.vhd
    g_mesh_mon_nof_words      : natural := 1024;
    g_mesh_mon_use_sync       : boolean := true;
    g_mesh_ena_reorder        : boolean := false
  );
  port (
    tb_clk                        : out std_logic;
    tb_end                        : out std_logic;

    -- Timing
    dp_pps                        : in  std_logic;

    -- Mesh serial interface (tr_nonbonded)
    mesh_tx_serial_2arr           : out t_unb1_board_mesh_sl_2arr;  -- Tx
    mesh_rx_serial_2arr           : in  t_unb1_board_mesh_sl_2arr := (others => (others => '0'));  -- Rx support for diagnostics

    -- Back serial interface (tr_nonbonded)
    back_tx_serial_2arr           : out t_unb1_board_back_sl_2arr;  -- Tx
    back_rx_serial_2arr           : in  t_unb1_board_back_sl_2arr := (others => (others => '0'))
  );
end tb_node_unb1_bn_terminal_bg;

architecture tb of tb_node_unb1_bn_terminal_bg is
  constant c_sim                       : boolean := true;

  constant c_bg_data_file_dat          : string  := g_bg_data_file_name & ".dat";
  constant c_bg_try_re_enable          : boolean := false;

  constant c_mm_clk_ps                 : natural :=  1000;  -- default is e.g. 8 ns, but use faster mm_clk to speed up simulation by using less c_nof_accum_per_sync
  constant c_dp_clk_ps                 : natural :=  5000;  -- 200 MHz
  constant c_cal_clk_ps                : natural := 25000;  -- 40 MHz
  constant c_tr_clk_ps                 : natural :=  6400;  -- 156.25 MHz
  constant c_mm_clk_1us                : real := 1000000.0 / real(c_mm_clk_ps);
  constant c_mm_clk_period             : time := c_mm_clk_ps * 1 ps;
  constant c_dp_clk_period             : time := c_dp_clk_ps * 1 ps;
  constant c_cal_clk_period            : time := c_cal_clk_ps * 1 ps;
  constant c_tr_clk_period             : time := c_tr_clk_ps * 1 ps;

  -- BG derived constants
  constant c_nof_samples_in_packet     : natural := g_bf.nof_signal_paths * g_bf.nof_subbands / g_bf.nof_input_streams;
  constant c_block_size                : natural := g_bf.nof_weights;
  constant c_gap                       : natural := c_block_size - c_nof_samples_in_packet;
  constant c_nof_accum_per_sync        : natural := 20 * c_mm_clk_ps / c_dp_clk_ps + 1;  -- integration time, account for mm_clk speed versus the dp_clk speed
  constant c_dp_run_nof_cycles         : natural := (g_nof_sync * c_nof_accum_per_sync + 1) * c_block_size;
  constant c_bsn_init_hi               : natural :=  0;  -- 16#BA98#;
  constant c_bsn_init_lo               : natural := 32;  -- 16#76543210#;
  constant c_bg_buf_adr_w              : natural := ceil_log2(c_nof_samples_in_packet);

  -- TRNB diagnostics
  constant c_nof_gx                    : natural := g_mesh_nof_serial * c_unb1_board_nof_node;  -- = 12 = 3 * 4
  constant c_nof_gx_mask               : natural := 2**c_nof_gx - 1;
  constant c_gx_link_delay             : real := 3.0;  -- unit us

  signal i_tb_end                      : std_logic;
  signal mm_clk                        : std_logic := '0';
  signal mm_rst                        : std_logic;
  signal dp_rst                        : std_logic;
  signal dp_clk                        : std_logic := '0';
  signal cal_clk                       : std_logic := '0';
  signal tr_clk                        : std_logic := '0';

  signal reg_diag_bg_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_bg_miso              : t_mem_miso;
  signal ram_diag_bg_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_bg_miso              : t_mem_miso;

  signal reg_mesh_tr_nonbonded_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mesh_tr_nonbonded_miso    : t_mem_miso;
  signal reg_mesh_diagnostics_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mesh_diagnostics_miso     : t_mem_miso;

  signal reg_back_tr_nonbonded_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal reg_back_tr_nonbonded_miso    : t_mem_miso;
  signal reg_back_diagnostics_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal reg_back_diagnostics_miso     : t_mem_miso;

  signal ram_mesh_diag_data_buf_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal ram_mesh_diag_data_buf_miso   : t_mem_miso;

  signal in_sosi_arr                   : t_dp_sosi_arr(g_bf.nof_input_streams - 1 downto 0);
  signal in_siso_arr                   : t_dp_siso_arr(g_bf.nof_input_streams - 1 downto 0);

  signal init_waveforms_done           : std_logic;
begin
  tb_clk <= dp_clk;
  tb_end <= i_tb_end;

  -- CLock and reset generation
  mm_clk <= not mm_clk or i_tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk or i_tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  cal_clk <= not cal_clk or i_tb_end after c_cal_clk_period / 2;
  tr_clk <= not tr_clk or i_tb_end after c_tr_clk_period / 2;

  ------------------------------------------------------------------------------
  -- Use default waveforms in memory for each input stream.
  ------------------------------------------------------------------------------

  init_waveforms_done <= '1';

  ----------------------------------------------------------------------------
  -- Stimuli for BF input streams
  ----------------------------------------------------------------------------
  p_bg_control_input_streams : process
  begin
    i_tb_end <= '0';
    reg_diag_bg_mosi <= c_mem_mosi_rst;

    -- Wait until intitializations have been done
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_until_high(mm_clk, init_waveforms_done);

    -- Set and enable the waveform generators. All generators are controlled by the same registers
    proc_mem_mm_bus_wr(1, c_nof_samples_in_packet,   mm_clk, reg_diag_bg_mosi);  -- Set the number of samples per block
    proc_mem_mm_bus_wr(2, c_nof_accum_per_sync,      mm_clk, reg_diag_bg_mosi);  -- Set the number of blocks per sync
    proc_mem_mm_bus_wr(3, c_gap,                     mm_clk, reg_diag_bg_mosi);  -- Set the gapsize
    proc_mem_mm_bus_wr(4, 0,                         mm_clk, reg_diag_bg_mosi);  -- Set the start address of the memory
    proc_mem_mm_bus_wr(5, c_nof_samples_in_packet - 1, mm_clk, reg_diag_bg_mosi);  -- Set the end address of the memory
    proc_mem_mm_bus_wr(6, c_bsn_init_lo,             mm_clk, reg_diag_bg_mosi);  -- Set the BSNInit low  value
    proc_mem_mm_bus_wr(7, c_bsn_init_hi,             mm_clk, reg_diag_bg_mosi);  -- Set the BSNInit high value
    proc_mem_mm_bus_wr(0, 3,                         mm_clk, reg_diag_bg_mosi);  -- Enable the WG to start at PPS

    proc_common_wait_until_high(dp_clk, dp_pps);

    -- Optionally try disable and re-enable BG to verify that this occurs at block boundaries
    if c_bg_try_re_enable = true then
      proc_common_wait_some_cycles(mm_clk, 1000);
      proc_mem_mm_bus_wr(0, 0, mm_clk, reg_diag_bg_mosi);  -- Disable the BG
      proc_common_wait_some_cycles(mm_clk, 500);
      proc_mem_mm_bus_wr(0, 1, mm_clk, reg_diag_bg_mosi);  -- Re-enable the BG to start immediately
    end if;

    -- Run time
    proc_common_wait_some_cycles(dp_clk, c_dp_run_nof_cycles);

    -- The end
    proc_common_wait_some_cycles(dp_clk, 100);
    i_tb_end <= '1';
    wait;
  end process;

  ---------------------------------------------------------------
  -- GENERATE BLOCK GENERATOR FOR STIMULI ON SOSI PORT
  ---------------------------------------------------------------
  u_block_generator : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams      => g_bf.nof_input_streams,
    g_buf_dat_w        => c_nof_complex * g_bf.in_dat_w,
    g_buf_addr_w       => c_bg_buf_adr_w,  -- Waveform buffer size 2**g_buf_addr_w nof samples
    g_file_index_arr   => g_bg_data_file_index_arr,
    g_file_name_prefix => g_bg_data_file_name
  )
  port map(
   -- Clocks and Reset
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    en_sync          => '1',
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => open,
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => open,
    out_siso_arr     => in_siso_arr,
    out_sosi_arr     => in_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Stimuli for TRNB diagnostics
  ----------------------------------------------------------------------------

  p_trnb_control_diagnostics : process
  begin
    reg_mesh_diagnostics_mosi <= c_mem_mosi_rst;

    -- Wait until intitializations have been done
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    if g_diagnostics_en = true then
      -- Wait some initial time
      proc_common_wait_some_cycles(mm_clk, c_mm_clk_1us * g_diagnostics_init_interval);

      -- Set diagnostics data mode
      proc_diagnostics_trnb_tx_set_mode_prbs(mm_clk, reg_mesh_diagnostics_mosi);
      proc_diagnostics_trnb_rx_set_mode_prbs(mm_clk, reg_mesh_diagnostics_mosi);

      while true loop
        proc_diagnostics_trnb_run_and_verify(g_chip_id,
                                             c_nof_gx,
                                             c_nof_gx_mask,
                                             c_gx_link_delay,
                                             g_diagnostics_on_interval,
                                             g_diagnostics_off_interval,
                                             c_mm_clk_1us,
                                             mm_clk,
                                             reg_mesh_diagnostics_miso,
                                             reg_mesh_diagnostics_mosi);
      end loop;
    end if;

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  u_dut : entity work.node_unb1_bn_terminal_bg
  generic map (
    g_sim                     => c_sim,
    g_sim_level               => g_sim_level,
    -- Application interface
    g_use_bg                  => g_use_bg,
--    g_bg_data_file_index_arr  => g_bg_data_file_index_arr,
--    g_bg_data_file_prefix     => g_bg_data_file_name,
    g_use_back                => g_use_back,

    -- Terminals interface
    g_mesh_nof_serial         => g_mesh_nof_serial,
    g_mesh_use_rx             => g_mesh_use_rx,
    g_mesh_gx_mbps            => g_mesh_gx_mbps,
    g_mesh_mon_select         => g_mesh_mon_select,
    g_mesh_mon_nof_words      => g_mesh_mon_nof_words,
    g_mesh_mon_use_sync       => g_mesh_mon_use_sync,
    g_mesh_ena_reorder        => g_mesh_ena_reorder,
    -- Auxiliary Interface
    g_aux                     => c_unb1_board_aux
  )
  port map (
    -- System
    chip_id                     => TO_UVEC(g_chip_id, c_unb1_board_nof_chip_w),  -- BN chip ID 4, 5, 6, 7
    bck_id                      => TO_UVEC(g_bck_id, c_unb1_board_nof_uniboard_w),  -- Backplane ID 0,1,2,3

    mm_rst                      => mm_rst,
    mm_clk                      => mm_clk,
    dp_rst                      => dp_rst,
    dp_clk                      => dp_clk,
    dp_pps                      => dp_pps,
    tr_mesh_clk                 => tr_clk,
    tr_back_clk                 => tr_clk,
    cal_clk                     => cal_clk,

    -- Streaming data input
    in_sosi_arr                 => in_sosi_arr,
    in_siso_arr                 => in_siso_arr,

    -- MM registers
    -- . block generator
    reg_diag_bg_mosi            => reg_diag_bg_mosi,
    reg_diag_bg_miso            => reg_diag_bg_miso,
    ram_diag_bg_mosi            => ram_diag_bg_mosi,
    ram_diag_bg_miso            => ram_diag_bg_miso,
    -- . tr_nonbonded
    reg_mesh_tr_nonbonded_mosi  => reg_mesh_tr_nonbonded_mosi,
    reg_mesh_tr_nonbonded_miso  => reg_mesh_tr_nonbonded_miso,
    reg_mesh_diagnostics_mosi   => reg_mesh_diagnostics_mosi,
    reg_mesh_diagnostics_miso   => reg_mesh_diagnostics_miso,

    reg_back_tr_nonbonded_mosi  => reg_back_tr_nonbonded_mosi,
    reg_back_tr_nonbonded_miso  => reg_back_tr_nonbonded_miso,
    reg_back_diagnostics_mosi   => reg_back_diagnostics_mosi,
    reg_back_diagnostics_miso   => reg_back_diagnostics_miso,

    -- . rx terminals monitor buffers
    ram_mesh_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso => ram_mesh_diag_data_buf_miso,

    -- Mesh interface level
    -- . Serial (tr_nonbonded)
    mesh_tx_serial_2arr         => mesh_tx_serial_2arr,
    mesh_rx_serial_2arr         => mesh_rx_serial_2arr,

    -- Back interface level
    -- . Serial (tr_nonbonded)
    back_tx_serial_2arr         => back_tx_serial_2arr,
    back_rx_serial_2arr         => back_rx_serial_2arr
  );
end tb;
