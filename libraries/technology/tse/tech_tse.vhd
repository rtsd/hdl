-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_tse_pkg.all;

entity tech_tse is
  generic (
    g_technology   : natural := c_tech_select_default;
    g_ETH_PHY      : string  := "LVDS";  -- "LVDS" (default): uses LVDS IOs for ctrl_unb_common, "XCVR": uses tranceiver PHY
    g_sim          : boolean := false;
    g_sim_level    : natural := 0;  -- 0 = use IP model (equivalent to g_sim = FALSE); 1 = use fast serdes model;
    g_sim_tx       : boolean := true;
    g_sim_rx       : boolean := true
  );
  port (
    -- Clocks and reset
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;  -- MM
    eth_clk        : in  std_logic;  -- 125 MHz
    tx_snk_clk     : in  std_logic;  -- DP
    rx_src_clk     : in  std_logic;  -- DP

    -- Calibration & reconfig clock
    cal_rec_clk    : in  std_logic := '0';

    -- Memory Mapped Slave
    mm_sla_in      : in  t_mem_mosi;
    mm_sla_out     : out t_mem_miso;

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      : in  t_dp_sosi;
    tx_snk_out     : out t_dp_siso;
    -- . MAC specific
    tx_mac_in      : in  t_tech_tse_tx_mac;
    tx_mac_out     : out t_tech_tse_tx_mac;

    -- MAC receive interface
    -- . ST Source
    rx_src_in      : in  t_dp_siso;
    rx_src_out     : out t_dp_sosi;
    -- . MAC specific
    rx_mac_out     : out t_tech_tse_rx_mac;

    -- PHY interface
    eth_txp        : out std_logic;
    eth_rxp        : in  std_logic;

    tse_led        : out t_tech_tse_led
  );
end tech_tse;

architecture str of tech_tse is
  constant c_use_technology : boolean := g_sim = false or g_sim_level = 0;
  constant c_use_sim_model  : boolean := not c_use_technology;

  component sim_tse is
  generic(
    g_tx         : boolean;
    g_tx_crc     : boolean := true;  -- model append CRC by TSE MAC, CRC value = 0
    g_rx         : boolean
  );
  port(
    -- Clocks and reset
    mm_rst         : in  std_logic;  -- unused
    mm_clk         : in  std_logic;  -- unused
    eth_clk        : in  std_logic;  -- 125 MHz
    tx_snk_clk     : in  std_logic;  -- DP
    rx_src_clk     : in  std_logic;  -- DP

    -- Memory Mapped Slave
    mm_sla_in      : in  t_mem_mosi;
    mm_sla_out     : out t_mem_miso;

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      : in  t_dp_sosi;
    tx_snk_out     : out t_dp_siso;
    -- . MAC specific
    tx_mac_in      : in  t_tech_tse_tx_mac;
    tx_mac_out     : out t_tech_tse_tx_mac;

    -- MAC receive interface
    -- . ST Source
    rx_src_in      : in  t_dp_siso;
    rx_src_out     : out t_dp_sosi;
    -- . MAC specific
    rx_mac_out     : out t_tech_tse_rx_mac;

    -- PHY interface
    eth_txp        : out std_logic;
    eth_rxp        : in  std_logic;

    tse_led        : out t_tech_tse_led
  );
end component;
begin
  gen_ip_stratixiv : if c_use_technology = true and g_technology = c_tech_stratixiv generate
    u0 : entity work.tech_tse_stratixiv
    generic map (g_ETH_PHY)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              cal_rec_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;

  gen_ip_arria10 : if c_use_technology = true and g_technology = c_tech_arria10_proto generate
    u0 : entity work.tech_tse_arria10
    generic map (g_ETH_PHY)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;

  gen_ip_arria10_e3sge3 : if c_use_technology = true and g_technology = c_tech_arria10_e3sge3 generate
    u0 : entity work.tech_tse_arria10_e3sge3
    generic map (g_ETH_PHY)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;

  gen_ip_arria10_e1sg : if c_use_technology = true and g_technology = c_tech_arria10_e1sg generate
    u0 : entity work.tech_tse_arria10_e1sg
    generic map (g_ETH_PHY)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;

  gen_ip_arria10_e2sg : if c_use_technology = true and g_technology = c_tech_arria10_e2sg generate
    u0 : entity work.tech_tse_arria10_e2sg
    generic map (g_ETH_PHY)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;

  gen_sim_tse : if c_use_sim_model = true generate
    u_sim_tse : sim_tse
    generic map (g_sim_tx, true, g_sim_rx)
    port map (mm_rst, mm_clk, eth_clk, tx_snk_clk, rx_src_clk,
              mm_sla_in, mm_sla_out,
              tx_snk_in, tx_snk_out,
              tx_mac_in, tx_mac_out,
              rx_src_in, rx_src_out,
              rx_mac_out,
              eth_txp, eth_rxp,
              tse_led);
  end generate;
end architecture;
