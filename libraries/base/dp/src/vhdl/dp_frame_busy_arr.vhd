-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Output frame busy control signal for array of streams
-- Description:
--   See dp_frame_busy.

library IEEE;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

entity dp_frame_busy_arr is
  generic (
    g_nof_inputs : natural := 1;
    g_pipeline   : natural := 0
  );
  port (
    rst             : in  std_logic;
    clk             : in  std_logic;
    snk_in_arr      : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    snk_in_busy_arr : out std_logic_vector(g_nof_inputs - 1 downto 0)
  );
end dp_frame_busy_arr;

architecture str of dp_frame_busy_arr is
begin
  gen_nof_inputs : for I in 0 to g_nof_inputs - 1 generate
    u_dp_frame_busy : entity work.dp_frame_busy
    generic map (
      g_pipeline => g_pipeline
    )
    port map (
      rst         => rst,
      clk         => clk,
      snk_in      => snk_in_arr(I),
      snk_in_busy => snk_in_busy_arr(I)
    );
  end generate;
end str;
