-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_pipeline_ready for different RL
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct output

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_pipeline_ready is
  generic (
    g_in_en          : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
    g_out_ready      : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
    g_in_latency     : natural := 1;  -- >= 0
    g_out_latency    : natural := 0;  -- >= 0
    g_nof_repeat     : natural := 50
  );
end tb_dp_pipeline_ready;

architecture tb of tb_dp_pipeline_ready is
  constant c_data_w          : natural := 16;
  constant c_rl              : natural := 1;
  constant c_data_init       : integer := 0;
  constant c_frame_len_init  : natural := 1;  -- >= 1
  constant c_pulse_active    : natural := 1;
  constant c_pulse_period    : natural := 7;
  constant c_sync_period     : natural := 7;
  constant c_sync_offset     : natural := 2;

  signal tb_end              : std_logic := '0';
  signal clk                 : std_logic := '1';
  signal rst                 : std_logic := '1';

  -- Flow control
  signal random_0            : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1            : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0             : std_logic;
  signal pulse_1             : std_logic;
  signal pulse_en            : std_logic := '1';

  -- Stimuli
  signal in_en               : std_logic := '1';
  signal in_siso             : t_dp_siso;
  signal in_sosi             : t_dp_sosi;
  signal adapt_siso          : t_dp_siso;
  signal adapt_sosi          : t_dp_sosi;

  signal out_siso            : t_dp_siso := c_dp_siso_hold;  -- ready='0', xon='1'
  signal out_sosi            : t_dp_sosi;

  -- Verification
  signal verify_en           : std_logic := '0';
  signal verify_done         : std_logic := '0';
  signal count_eop           : natural := 0;

  signal prev_out_ready      : std_logic_vector(0 to g_out_latency);
  signal prev_out_data       : std_logic_vector(c_data_w - 1 downto 0) := TO_SVEC(c_data_init - 1, c_data_w);
  signal out_bsn             : std_logic_vector(c_data_w - 1 downto 0);
  signal out_data            : std_logic_vector(c_data_w - 1 downto 0);
  signal out_sync            : std_logic;
  signal out_val             : std_logic;
  signal out_sop             : std_logic;
  signal out_eop             : std_logic;
  signal hold_out_sop        : std_logic;
  signal expected_out_data   : std_logic_vector(c_data_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_data_init   : natural;
    variable v_frame_len   : natural;
    variable v_sync        : std_logic;
  begin
    v_data_init := c_data_init;
    v_frame_len := c_frame_len_init;
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Begin of stimuli
    for R in 0 to g_nof_repeat - 1 loop
      v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');
      proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, v_frame_len, 0, 0, v_sync, TO_DP_BSN(R), clk, in_en, in_siso, in_sosi);
      --proc_common_wait_some_cycles(clk, 10);
      v_data_init := v_data_init + v_frame_len;
      v_frame_len := v_frame_len + 1;
    end loop;

    -- End of stimuli
    expected_out_data <= TO_UVEC(v_data_init - 1, c_data_w);

    proc_common_wait_until_high(clk, verify_done);
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- proc_dp_gen_block_data() only supports RL=0 or 1, so use a latency adpater to support any g_in_latency
  u_input_adapt : entity work.dp_latency_adapter
  generic map (
    g_in_latency   => c_rl,
    g_out_latency  => g_in_latency
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => in_siso,
    snk_in       => in_sosi,
    -- ST source
    src_in       => adapt_siso,
    src_out      => adapt_sosi
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Verification logistics
  verify_en <= '1'          when rising_edge(clk) and out_sosi.sop = '1';  -- enable verify after first output sop
  count_eop <= count_eop + 1  when rising_edge(clk) and out_sosi.eop = '1' and((g_out_latency > 0) or
                                                                           (g_out_latency = 0 and out_siso.ready = '1'));  -- count number of output eop
  verify_done <= '1'        when rising_edge(clk) and count_eop = g_nof_repeat;  -- signal verify done after g_nof_repeat frames

  -- Actual verification of the output streams
  proc_dp_verify_data("out_sosi.data", g_out_latency, clk, verify_en, out_siso.ready, out_sosi.valid, out_data, prev_out_data);  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_valid(g_out_latency, clk, verify_en, out_siso.ready, prev_out_ready, out_sosi.valid);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_sop_and_eop(g_out_latency, clk, out_siso.ready, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_out_sop);  -- Verify that sop and eop come in pairs
  proc_dp_verify_value(e_equal, clk, verify_done, expected_out_data, prev_out_data);  -- Verify that the stimuli have been applied at all
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en, out_sosi.sync, out_sosi.sop, out_sosi.bsn);

  -- Monitoring
  out_bsn  <= out_sosi.bsn(c_data_w - 1 downto 0);
  out_data <= out_sosi.data(c_data_w - 1 downto 0);
  out_sync <= out_sosi.sync;
  out_val  <= out_sosi.valid;
  out_sop  <= out_sosi.sop;
  out_eop  <= out_sosi.eop;

  ------------------------------------------------------------------------------
  -- DUT dp_pipeline_ready
  ------------------------------------------------------------------------------

  pipeline : entity work.dp_pipeline_ready
  generic map (
    g_in_latency   => g_in_latency,
    g_out_latency  => g_out_latency
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => adapt_siso,
    snk_in       => adapt_sosi,
    -- ST source
    src_in       => out_siso,
    src_out      => out_sosi
  );
end tb;
