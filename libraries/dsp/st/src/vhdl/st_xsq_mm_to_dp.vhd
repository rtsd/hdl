-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author : R. vd Walle
-- Purpose:
-- . Read a block of xsq data with size (g_nof_crosslets * g_nof_signal_inputs)
--   from memory mapped (MM) location and stream it as a block of data of
--   size g_nof_crosslets * g_nof_signal_inputs **2.
-- Description:
-- After every in_sosi.sop the st_xsq_mm_to_dp.vhd reads blocks of data
-- via g_nof_streams mm and outputs it as g_nof_streams parallel streams via
-- out_sosi_arr. In_sosi.sync is passed on to out_sosi.
-- --------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity st_xsq_mm_to_dp is
  generic (
    g_nof_streams        : natural;
    g_nof_crosslets      : natural;
    g_nof_signal_inputs  : natural;
    g_dsp_data_w         : natural := 16
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    in_sosi       : in  t_dp_sosi;  -- sop used as start signal
    mm_mosi       : out t_mem_mosi;
    mm_miso_arr   : in  t_mem_miso_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end st_xsq_mm_to_dp;

architecture rtl of st_xsq_mm_to_dp is
  type t_reg is record
    in_sosi_strobe  : t_dp_sosi;
    out_sosi_ctrl   : t_dp_sosi;
    busy            : std_logic;
    crosslets_index : natural;
    in_a_index      : natural;
    in_b_index      : natural;
    mm_mosi         : t_mem_mosi;
  end record;

  constant c_reg_rst : t_reg := (c_dp_sosi_rst, c_dp_sosi_rst, '0', 0, 0, 0, c_mem_mosi_rst);

  signal r       : t_reg;
  signal nxt_r   : t_reg;
begin
  mm_mosi <= nxt_r.mm_mosi;

  u_sosi : process(r, mm_miso_arr)
  begin
    for I in 0 to g_nof_streams - 1 loop
      out_sosi_arr(I)       <= r.out_sosi_ctrl;
      out_sosi_arr(I).re    <= RESIZE_DP_DSP_DATA(mm_miso_arr(I).rddata(g_dsp_data_w - 1 downto 0));
      out_sosi_arr(I).im    <= RESIZE_DP_DSP_DATA(mm_miso_arr(I).rddata(c_nof_complex * g_dsp_data_w - 1 downto g_dsp_data_w));
      out_sosi_arr(I).valid <= mm_miso_arr(I).rdval;  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so same as the ready latency (RL = 1)
    end loop;
  end process;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, in_sosi)
    variable v : t_reg;
  begin
    v := r;
    v.out_sosi_ctrl := c_dp_sosi_rst;
    v.mm_mosi.rd := '0';

    -- initiate next block and capture in_sosi strobe
    if r.busy = '0' and in_sosi.sop = '1' then
      v.busy := '1';
      v.in_sosi_strobe := in_sosi;
    elsif r.busy = '1' then
      -- continue with block
      v.mm_mosi.rd := '1';
      v.mm_mosi.address := TO_MEM_ADDRESS(r.crosslets_index * g_nof_signal_inputs + r.in_b_index);  -- streams iterate over in_b_index

      -- Indices counters to select data order
      if r.in_b_index < g_nof_signal_inputs - 1 then
        v.in_b_index := r.in_b_index + 1;
      else
        v.in_b_index := 0;
        if r.in_a_index < g_nof_signal_inputs - 1 then
          v.in_a_index := r.in_a_index + 1;
        else
          v.in_a_index := 0;
          if r.crosslets_index < g_nof_crosslets - 1 then
            v.crosslets_index := r.crosslets_index + 1;
          else
            v.crosslets_index := 0;
          end if;
        end if;
      end if;

      -- check start of block
      if r.crosslets_index = 0 and r.in_a_index = 0 and r.in_b_index = 0 then
        v.out_sosi_ctrl.sop  := '1';
        v.out_sosi_ctrl.sync := r.in_sosi_strobe.sync;
        v.out_sosi_ctrl.bsn  := r.in_sosi_strobe.bsn;
      end if;

      -- check end of block
      if r.crosslets_index >= (g_nof_crosslets - 1) and r.in_a_index >= (g_nof_signal_inputs - 1)  and r.in_b_index >= (g_nof_signal_inputs - 1) then
        v.out_sosi_ctrl.eop := '1';
        v.out_sosi_ctrl.err  := r.in_sosi_strobe.err;
        v.busy := '0';
      end if;
    end if;
    nxt_r <= v;
  end process;
end rtl;
