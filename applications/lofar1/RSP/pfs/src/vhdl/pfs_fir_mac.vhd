library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_fir_mac is
  generic (
    g_a_in_w        : natural;
    g_b_in_w        : natural;
    g_out_w         : natural;
    g_taps_w        : natural;
    g_mult_pipeline : natural
  );
  port (
    data_a          : in std_logic_vector(g_a_in_w - 1 downto 0);
    data_b          : in std_logic_vector(g_b_in_w - 1 downto 0);
    res_clr         : in std_logic;
    res_val         : in std_logic;
    clk             : in std_logic;
    rst             : in std_logic;
    result          : out std_logic_vector(g_out_w - 1 downto 0)
  );
end pfs_fir_mac;
