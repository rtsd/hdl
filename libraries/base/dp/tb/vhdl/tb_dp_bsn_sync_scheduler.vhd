-------------------------------------------------------------------------------
-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: Eric Kooistra
-- Purpose: Test bench for dp_bsn_sync_scheduler.vhd
-- Description:
--   The tb automatically TODO verifies the following cases:
--   * when enabled the out_sosi = in_sosi, except for the output sync
--     interval
--   * disabling the output and re-enabling the output using
--     c_nof_test_intervals
--   * c_ctrl_start_bsn must be in the future, else output remains disabled
--   * gaps between input blocks using g_input_gap_size > 0
--   * output sync intervals that have fractional number of blocks using
--     ctrl_interval_size = g_nof_samples_per_output_sync
--   * output sync interval recovery in case of lost input blocks
-- Usage:
-- > as 4
-- > run -all
--   View in Wave window u_dut: r, nxt_r, and tb: in_sosi, out_sosi,
--   out_sync, out_start, out_start_interval
--
-- Development steps:
--  . Step    1 ~1 day work (idea started earlier, so requirements were clear),
--    steps 2,5 ~2 days work (fixing details and verifying all features),
--    steps 3,4 ~1 day work,
--    total ~4 days work spent in ~ 1 week.
-- 1a Draft design description of dp_bsn_sync_scheduler
--  b Initial implementation that compiles
-- 2a Initial tb using u_stimuli to generate in_sosi and using p_stimuli
--    to test basic functionality, i.e. to get out_sosi when ctrl_enable is
--    on and no output when off.
--  b Verify that g_input_gap_size > 0 also works
--  c Add verification of out_sosi = in sosi when enabled
--    (proc_dp_verify_sosi_equal)
--  d Add verification of out_sosi (p_verify_out_enable)
--  e Add verification of out_start and first out_sync (p_verify_out_start)
--  f Add verification of out_sosi.sync using proc_dp_verify_sync() for
--    fractional sync intervals
--  g Add verification of mon_output_sync_bsn (p_verify_mon_output_sync_bsn)
-- 3a Add tb generics and tb_tb_dp_bsn_sync_scheduler.vhd
-- 4a Add mmp_dp_bsn_sync_scheduler.vhd with MM register and
--    tb_mmp_dp_bsn_sync_scheduler.vhd that verifies only the MM part,
--    because the sync part is already verified by
--    tb_tb_dp_bsn_sync_scheduler.vhd.
-- 5a For all ASSERTs, verify that the ERROR or FAILURE can occur by e.g.
--    temporarily changing the ASSERT condition
--  b Initialy used LOOP in p_stimuli to repeat test. Later used list of
--    c_nof_test_intervals and tb_state to try different stimuli.
--
-- References:
-- [1] https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+Timing+in+Station
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_bsn_sync_scheduler is
  generic (
    -- Input sync period and sosi ctrl
    g_nof_input_sync               : natural := 10;  -- Use c_nof_input_sync > g_block_size, see tb_tb_dp_bsn_sync_scheduler
    g_nof_block_per_input_sync     : natural := 17;
    g_block_size                   : natural := 2;
    g_input_gap_size               : natural := 0;

    -- Output sync period
    g_nof_samples_per_output_sync  : natural := 45;  -- 45 / g_block_size = 4.5
    g_pipeline                     : natural := 0  -- 0 or 1
  );
end tb_dp_bsn_sync_scheduler;

architecture tb of tb_dp_bsn_sync_scheduler is
  constant c_clk_period                   : time    := 10 ns;
  constant c_bsn_w                        : natural := 31;
  constant c_dut_latency                  : natural := 1;

  -- Test intervals
  constant c_nof_test_intervals           : natural := 4;  -- nof should be large enough for p_stimuli
  constant c_nof_block_per_test_interval  : natural := g_nof_block_per_input_sync * g_nof_input_sync;
  constant c_nof_lost_input_blocks        : natural := g_nof_block_per_input_sync * 4 + 3;
  constant c_nof_clk_per_block            : natural := g_block_size + g_input_gap_size;
  constant c_nof_clk_per_test_interval    : natural := c_nof_clk_per_block * c_nof_block_per_test_interval;
  constant c_begin_of_test_interval       : natural := 0 + c_nof_clk_per_block * 4;  -- just after start
  constant c_early_in_test_interval       : natural := c_nof_clk_per_test_interval / 3;  -- just after begin and before mid
  constant c_mid_of_test_interval         : natural := c_nof_clk_per_test_interval / 2;  -- at mid
  constant c_end_of_test_interval         : natural := c_nof_clk_per_test_interval - c_nof_clk_per_block * 3;  -- just before end

  constant c_sim_nof_blocks               : natural := c_nof_block_per_test_interval * c_nof_test_intervals;

  constant c_output_nof_blocks_min        : natural := g_nof_samples_per_output_sync / g_block_size;
  constant c_enable_init_nof_bsn          : natural := ceil_value(c_output_nof_blocks_min / g_block_size + 10, g_nof_block_per_input_sync);

  constant c_out_enable_margin            : natural := g_block_size;

  signal clk                   : std_logic := '1';
  signal rst                   : std_logic := '1';
  signal cnt                   : integer := 0;
  signal test_interval         : integer := 0;
  signal tb_end                : std_logic := '0';

  type t_stimuli_state_enum is (  -- use short names to ease unzoomed view in Wave window
    e_dis,  -- disable
    e_en,  -- enable
    e_re,  -- re-enable
    e_lost,  -- lost input blocks
    e_old  -- start bsn in passed
  );
  signal stimuli_state         : t_stimuli_state_enum := e_dis;  -- to show what tb does in Wave window

  -- Stimuli
  signal ctrl_enable           : std_logic := '0';
  signal ctrl_enable_evt       : std_logic := '0';
  signal ctrl_interval_size    : natural := g_nof_samples_per_output_sync;
  signal ctrl_start_bsn        : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');
  signal mon_current_input_bsn : std_logic_vector(c_bsn_w - 1 downto 0);
  signal mon_input_bsn_at_sync : std_logic_vector(c_bsn_w - 1 downto 0);
  signal mon_output_enable     : std_logic;
  signal mon_output_sync_bsn   : std_logic_vector(c_bsn_w - 1 downto 0);
  signal out_sop_dly           : std_logic;

  signal stimuli_sosi          : t_dp_sosi := c_dp_sosi_init;
  signal stimuli_sync          : std_logic;  -- declared next to stimuli_sosi and out_sync for easier comparison in Wave window

  -- Input stimuli
  signal in_lost               : std_logic := '0';
  signal in_sosi               : t_dp_sosi := c_dp_sosi_init;  -- = stimuli_sosi, with option for lost blocks
  signal in_sync               : std_logic;

  -- Output
  signal out_sync              : std_logic;  -- declared next to in_sync, out_start and out_sosi for easier comparison in Wave window
  signal out_start             : std_logic;
  signal out_start_interval    : std_logic;
  signal exp_start_interval    : std_logic := '0';
  signal first_interval        : std_logic := '0';
  signal out_enable            : std_logic;
  signal expected_out_enable   : std_logic := '0';
  signal out_sosi              : t_dp_sosi := c_dp_sosi_init;

  -- Verify
  signal in_sosi_integer_comb  : t_dp_sosi_integer;
  signal in_sosi_integer_pipe  : t_dp_sosi_integer;
  signal in_sosi_integer       : t_dp_sosi_integer;
  signal out_sosi_integer      : t_dp_sosi_integer;

  signal verify_sosi_equal          : std_logic := '0';
  signal verify_sosi_equal_at_sop   : std_logic := '0';
  signal verify_sosi_equal_at_valid : std_logic := '0';
  signal verify_sync                : std_logic := '1';
  signal recover_from_in_lost       : std_logic := '0';

  signal verifying_sync_equal     : std_logic := '0';

  signal prev_ctrl_enable         : std_logic := '0';
  signal prev_out_enable          : std_logic := '0';
  signal out_enable_cnt           : natural := 0;
  signal nxt_out_enable_cnt       : natural := 0;
  signal expected_out_enable_comb : std_logic := '0';
  signal expected_out_enable_pipe : std_logic := '0';
  signal expecting_out_start      : std_logic := '0';
  signal hold_out_sop             : std_logic := '0';
  signal out_sop_cnt              : natural := 0;

  signal dbg_out_sosi_sync     : std_logic;
  signal dbg_out_sosi_sop      : std_logic;
  signal dbg_out_sosi_bsn      : natural;
  signal dbg_nof_blk           : natural := 0;
  signal dbg_accumulate        : natural := 0;
  signal dbg_expected_bsn      : natural := 0;

  -- Local procedures
  procedure proc_output_enable(signal clk                   : in std_logic;
                               signal cnt                   : in integer;
                               signal sync                  : in std_logic;
                               signal mon_input_bsn_at_sync : in std_logic_vector(c_bsn_w - 1 downto 0);
                               signal stimuli_state         : out t_stimuli_state_enum;
                               signal ctrl_start_bsn        : out std_logic_vector(c_bsn_w - 1 downto 0);
                               signal ctrl_enable           : out std_logic;
                               signal ctrl_enable_evt       : out std_logic) is
  begin
    proc_common_wait_until_hi_lo(clk, sync);  -- (re)enable at begin of sync interval
    stimuli_state <= e_en;
    ctrl_start_bsn <= ADD_UVEC(mon_input_bsn_at_sync, TO_UVEC(c_enable_init_nof_bsn, c_natural_w));  -- determine BSN in the future
    ctrl_enable <= '1';
    ctrl_enable_evt <= '1';
    proc_common_wait_some_cycles(clk, 1);
    ctrl_enable_evt <= '0';
  end proc_output_enable;

  procedure proc_output_re_enable(signal clk                   : in std_logic;
                                  signal cnt                   : in integer;
                                  signal mon_input_bsn_at_sync : in std_logic_vector(c_bsn_w - 1 downto 0);
                                  signal stimuli_state         : out t_stimuli_state_enum;
                                  signal ctrl_start_bsn        : out std_logic_vector(c_bsn_w - 1 downto 0);
                                  signal ctrl_enable           : out std_logic;
                                  signal ctrl_enable_evt       : out std_logic) is
  begin
    proc_output_enable(clk, cnt, in_sync, mon_input_bsn_at_sync, stimuli_state, ctrl_start_bsn, ctrl_enable, ctrl_enable_evt);
    stimuli_state <= e_re;
  end proc_output_re_enable;

  procedure proc_output_disable(signal stimuli_state   : out t_stimuli_state_enum;
                                signal ctrl_enable     : out std_logic;
                                signal ctrl_enable_evt : out std_logic) is
  begin
    stimuli_state <= e_dis;
    ctrl_enable <= '0';
    ctrl_enable_evt <= '1';
    proc_common_wait_some_cycles(clk, 1);
    ctrl_enable_evt <= '0';
  end proc_output_disable;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;
  cnt <= cnt + 1 when rising_edge(clk);

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  p_stimuli : process
  begin
    stimuli_state <= e_dis;
    proc_common_wait_until_low(clk, rst);

    ------------------------------------------------------------------------------
    -- Enable and disable output
    ------------------------------------------------------------------------------
    test_interval <= 0; proc_common_wait_some_cycles(clk, 1);

    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_begin_of_test_interval, clk, cnt);
    -- Start of test_interval: Enable output
    proc_output_enable(clk, cnt, in_sync, mon_input_bsn_at_sync, stimuli_state, ctrl_start_bsn, ctrl_enable, ctrl_enable_evt);

    -- End of test_interval: Disable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_end_of_test_interval, clk, cnt);
    proc_output_disable(stimuli_state, ctrl_enable, ctrl_enable_evt);

    ------------------------------------------------------------------------------
    -- Re enable output when already enabled
    ------------------------------------------------------------------------------
    test_interval <= test_interval + 1; proc_common_wait_some_cycles(clk, 1);

    -- Start of test_interval: Enable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_begin_of_test_interval, clk, cnt);
    proc_output_enable(clk, cnt, in_sync, mon_input_bsn_at_sync, stimuli_state, ctrl_start_bsn, ctrl_enable, ctrl_enable_evt);

    -- Mid of test_interval: Re-enable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_mid_of_test_interval, clk, cnt);
    proc_output_re_enable(clk, cnt, mon_input_bsn_at_sync, stimuli_state, ctrl_start_bsn, ctrl_enable, ctrl_enable_evt);

    -- End of test_interval: Disable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_end_of_test_interval, clk, cnt);
    proc_output_disable(stimuli_state, ctrl_enable, ctrl_enable_evt);

    ------------------------------------------------------------------------------
    -- Lost input blocks
    ------------------------------------------------------------------------------
    test_interval <= test_interval + 1; proc_common_wait_some_cycles(clk, 1);

    -- Start of test_interval: Enable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_begin_of_test_interval, clk, cnt);
    proc_output_enable(clk, cnt, in_sync, mon_input_bsn_at_sync, stimuli_state, ctrl_start_bsn, ctrl_enable, ctrl_enable_evt);

    -- Early in test_interval: Disable input to simulate lost blocks
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_early_in_test_interval, clk, cnt);
    stimuli_state <= e_lost;
    proc_common_wait_until_high(clk, stimuli_sosi.eop);
    in_lost <= '1';  -- high after eop, so high at next sop
    recover_from_in_lost <= '1';
    for I in 0 to c_nof_lost_input_blocks - 1 loop
      proc_common_wait_some_cycles(clk, 1);
      proc_common_wait_until_high(clk, stimuli_sosi.eop);
    end loop;
    in_lost <= '0';  -- low after eop, so low at next sop
    stimuli_state <= e_en;
    -- Wait for some cycles that DUT needs to catch up after lost input (see nxt_r.update_bsn in DUT)
    for I in 0 to c_nof_lost_input_blocks / c_output_nof_blocks_min + 5 loop  -- + for some extra margin
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    recover_from_in_lost <= '0';

    -- End of test_interval: Disable output
    proc_common_wait_until_value(test_interval * c_nof_clk_per_test_interval + c_end_of_test_interval, clk, cnt);
    proc_output_disable(stimuli_state, ctrl_enable, ctrl_enable_evt);

    wait;
  end process;

  -- Generate data blocks with input sync
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period  => g_nof_block_per_input_sync,
    g_err_init     => 0,
    g_err_incr     => 0,  -- do not increment, to not distract from viewing of BSN in Wave window
    g_channel_init => 0,
    g_channel_incr => 0,  -- do not increment, to not distract from viewing of BSN in Wave window
    g_nof_repeat   => c_sim_nof_blocks,
    g_pkt_len      => g_block_size,
    g_pkt_gap      => g_input_gap_size
  )
  port map (
    rst               => rst,
    clk               => clk,

    -- Generate stimuli
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => tb_end
  );

  -- Input with option to loose data blocks
  p_in_sosi : process(stimuli_sosi, in_lost)
  begin
    in_sosi <= stimuli_sosi;
    if in_lost = '1' then
      in_sosi.sync  <= '0';
      in_sosi.sop   <= '0';
      in_sosi.eop   <= '0';
      in_sosi.valid <= '0';
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Verification
  -----------------------------------------------------------------------------

  -- separate signal for easier viewing in Wave window
  stimuli_sync <= stimuli_sosi.sync;
  in_sync <= in_sosi.sync;
  out_sync <= out_sosi.sync;

  -----------------------------------------------------------------------------
  -- . Verify out_enable
  -----------------------------------------------------------------------------
  -- The expected out_enable is difficult to determine cycle exact, because
  -- it depends on g_block_size = 2 or > 2 and on g_pipeline. Therfore use
  -- expected_out_enable_comb = '-' to define dont care.
  -- * For g_block_size = 2 the use of r.enable (instead of v.enable) in
  --   dp_bsn_sync_scheduler.vhd causes that the output can stay enabled 2
  --   cycles longer, which is ok. Using v.enable does avoid these extra
  --   cycles, but for timing closure it is preferred to use r.enable.

  -- Verify out_enable
  p_expected_out_enable : process(ctrl_enable, in_sosi, ctrl_start_bsn, ctrl_enable_evt, out_enable_cnt)
  begin
    -- Default
    expected_out_enable_comb <= '0';

    -- Expect output enable '1' when ctrl_enable is active, but after ctrl_start_bsn
    if ctrl_enable = '1' then
      if unsigned(in_sosi.bsn) >= unsigned(ctrl_start_bsn) then
        expected_out_enable_comb <= '1';
      end if;
    end if;

    -- Introduce some dont care margin in case of ctrl_enable_evt when ctrl_enable was active and may change.
    -- The ctrl_enable may go inactive (= stop) or remain active (= restart).
    if ctrl_enable_evt = '1' and prev_ctrl_enable = '1' then
      nxt_out_enable_cnt <= 0;
      expected_out_enable_comb <= '-';
    elsif out_enable_cnt < c_out_enable_margin then
      nxt_out_enable_cnt <= out_enable_cnt + 1;
      expected_out_enable_comb <= '-';
    end if;
  end process;

  prev_ctrl_enable <= ctrl_enable when rising_edge(clk);

  out_enable_cnt <= nxt_out_enable_cnt when rising_edge(clk);

  expected_out_enable_pipe <= expected_out_enable_comb when rising_edge(clk);

  expected_out_enable <= expected_out_enable_comb when g_pipeline = 0 else expected_out_enable_pipe;

  p_verify_out_enable : process(clk)
  begin
    if rising_edge(clk) then
      if expected_out_enable = '1' then
        assert out_enable = '1'
          report "Wrong out_enable, should be active"
          severity ERROR;
      elsif expected_out_enable = '0' then
        assert out_enable = '0'
          report "Wrong out_enable, should be inactive"
          severity ERROR;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- . Verify that there was valid output
  -----------------------------------------------------------------------------

  out_sop_cnt <= out_sop_cnt + 1 when rising_edge(clk) and out_sosi.sop = '1';

  p_verify_output : process
  begin
    wait until tb_end = '1';
    assert out_sop_cnt > 0
      report "There was no output."
      severity ERROR;
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- . Verify out_start
  -----------------------------------------------------------------------------

  p_expecting_out_start : process(clk)
  begin
    if rising_edge(clk) then
      if ctrl_enable = '1' and ctrl_enable_evt = '1' then
        expecting_out_start <= '1';
      elsif out_start = '1' then
        expecting_out_start <= '0';
      end if;
    end if;
  end process;

  prev_out_enable <= out_enable when rising_edge(clk);

  p_verify_out_start : process(clk)
  begin
    if rising_edge(clk) then
      -- Check that out_sync is active at out_start
      if out_start = '1' then
        if out_sync = '0' then
          report "Missing out_sync at out_start"
            severity ERROR;
        end if;
      end if;

      -- Check unexpected out_start
      if out_start = '1' and expecting_out_start = '0' then
        report "Unexpected out_start"
          severity ERROR;
      end if;

      -- Check that out_start is active when out_enable goes high
      if out_start = '0' and out_enable = '1' and prev_out_enable = '0' then
        report "Missed out_start"
          severity ERROR;
      end if;
    end if;
  end process;

  exp_start_interval <= out_start or (first_interval and not out_sync) when rst = '0' else '0';

  p_verify_out_start_interval : process(clk)
  begin
    if rising_edge(clk) then
      -- Create first_interval for exp_start_interval
      if out_start = '1' then
        first_interval <= '1';
      elsif out_sync = '1' then
        first_interval <= '0';
      end if;

      -- Verify exp_start_interval
      assert exp_start_interval = out_start_interval
        report "Wrong out_start_interval"
        severity ERROR;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- . Verify out_sosi = in_sosi, for all fields except out_sosi.sync
  -----------------------------------------------------------------------------
  -- Use registered values for func_dp_stream_slv_to_integer(), to avoid
  -- Warning: NUMERIC_STD.TO_INTEGER: metavalue detected, returning 0 at
  -- Time: 0 fs in Transcript window. Using only c_dp_sosi_init at signal
  -- declaration is not sufficient.

  verify_sosi_equal <= out_enable when rising_edge(clk);
  verify_sosi_equal_at_sop <= verify_sosi_equal and in_sosi_integer.sop;
  verify_sosi_equal_at_valid <= verify_sosi_equal and in_sosi_integer.valid;

  in_sosi_integer_comb <= func_dp_stream_slv_to_integer(in_sosi, c_natural_w) when rising_edge(clk);
  in_sosi_integer_pipe <= in_sosi_integer_comb when rising_edge(clk);
  in_sosi_integer <= in_sosi_integer_comb when g_pipeline = 0 else in_sosi_integer_pipe;
  out_sosi_integer <= func_dp_stream_slv_to_integer(out_sosi, c_natural_w) when rising_edge(clk);

  proc_dp_verify_sosi_equal(  "bsn", clk, verify_sosi_equal_at_sop, out_sosi_integer, in_sosi_integer);
  proc_dp_verify_sosi_equal(  "sop", clk, verify_sosi_equal, out_sosi_integer, in_sosi_integer);
  proc_dp_verify_sosi_equal(  "eop", clk, verify_sosi_equal, out_sosi_integer, in_sosi_integer);
  proc_dp_verify_sosi_equal("valid", clk, verify_sosi_equal, out_sosi_integer, in_sosi_integer);
  proc_dp_verify_sosi_equal( "data", clk, verify_sosi_equal_at_valid, out_sosi_integer, in_sosi_integer);

  -- Verify that out_sosi blocks have sop and eop
  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_out_sop);

  -----------------------------------------------------------------------------
  -- . Verify out_sosi.sync = in_sosi.sync, when sync interval is not changed
  -----------------------------------------------------------------------------
  gen_verify_sync_equal : if g_nof_samples_per_output_sync = g_nof_block_per_input_sync * g_block_size generate
    verifying_sync_equal <= '1';  -- to show in Wave window that this check is active
    proc_dp_verify_sosi_equal( "sync", clk, verify_sosi_equal, out_sosi_integer, in_sosi_integer);
  end generate;

  -----------------------------------------------------------------------------
  -- . Verify out_sosi.sync interval
  -----------------------------------------------------------------------------
  verify_sync <= not recover_from_in_lost;

  -- Verify that sync is on PPS and BSN grid as defined in [1].
  proc_dp_verify_sync(TO_UINT(ctrl_start_bsn),
                      ctrl_interval_size,
                      g_block_size,
                      clk,
                      verify_sync,
                      out_sosi.sync,
                      out_sosi.sop,
                      out_sosi.bsn,
                      dbg_nof_blk,
                      dbg_accumulate,
                      dbg_expected_bsn);

  dbg_out_sosi_sync <= out_sosi.sync;
  dbg_out_sosi_sop  <= out_sosi.sop;
  dbg_out_sosi_bsn  <= TO_UINT(out_sosi.bsn);

  -----------------------------------------------------------------------------
  -- . Verify mon_output_sync_bsn
  -----------------------------------------------------------------------------

  out_sop_dly <= out_sosi.sop when rising_edge(clk);

  p_verify_mon_output_sync_bsn : process(clk)
    variable v_bsn_min : natural;
    variable v_bsn_max : natural;
  begin
    if rising_edge(clk) then
      if recover_from_in_lost = '0' then
        if out_sop_dly = '1' then
          v_bsn_min := TO_UINT(mon_current_input_bsn) - 1;
          v_bsn_max := TO_UINT(mon_current_input_bsn) + c_output_nof_blocks_min + 1;

          assert TO_UINT(mon_output_sync_bsn) >= v_bsn_min
            report "Wrong: mon_output_sync_bsn is behind (" & int_to_str(TO_UINT(mon_output_sync_bsn)) & " < " & int_to_str(v_bsn_min) & ")"
            severity ERROR;
          assert TO_UINT(mon_output_sync_bsn) <= v_bsn_max
            report "Wrong: mon_output_sync_bsn is too far ahead (" & int_to_str(TO_UINT(mon_output_sync_bsn)) & " > " & int_to_str(v_bsn_max) & ")"
            severity ERROR;

          --Debug report used to investigate v_bsn_min and v_bsn_max assert conditions
          --REPORT int_to_str(TO_UINT(mon_output_sync_bsn)) & " : " & int_to_str(TO_UINT(mon_current_input_bsn)) SEVERITY NOTE;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: dp_bsn_sync_scheduler
  -----------------------------------------------------------------------------

  dut : entity work.dp_bsn_sync_scheduler
  generic map (
    g_bsn_w           => c_bsn_w,
    g_block_size      => g_block_size,
    g_pipeline        => g_pipeline
  )
  port map (
    rst                   => rst,
    clk                   => clk,

    -- M&C
    ctrl_enable           => ctrl_enable,
    ctrl_enable_evt       => ctrl_enable_evt,
    ctrl_interval_size    => ctrl_interval_size,
    ctrl_start_bsn        => ctrl_start_bsn,
    mon_current_input_bsn => mon_current_input_bsn,
    mon_input_bsn_at_sync => mon_input_bsn_at_sync,
    mon_output_enable     => mon_output_enable,
    mon_output_sync_bsn   => mon_output_sync_bsn,

    -- Streaming
    in_sosi               => in_sosi,
    out_sosi              => out_sosi,
    out_start             => out_start,
    out_start_interval    => out_start_interval,
    out_enable            => out_enable
  );
end tb;
