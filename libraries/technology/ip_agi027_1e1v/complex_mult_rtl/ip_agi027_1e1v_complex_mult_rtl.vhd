-- --------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -------------------------------------------------------------------------
--
-- Author : D.F. Brouwer
-- Reference:
--   Copied from */technology/ip_arria10/complex_mult_rtl/ip_arria10_complex_mult_rtl.vhd.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

--
-- Function: Signed complex multiply
--   p = a * b       when g_conjugate_b = FALSE
--     = (ar + j ai) * (br + j bi)
--     =  ar*br - ai*bi + j ( ar*bi + ai*br)
--
--   p = a * conj(b) when g_conjugate_b = TRUE
--     = (ar + j ai) * (br - j bi)
--     =  ar*br + ai*bi + j (-ar*bi + ai*br)
--
-- Architectures:
-- . rtl          : uses RTL to have all registers in one clocked process
--

entity ip_agi027_1e1v_complex_mult_rtl is
  generic (
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    result_re  : out  std_logic_vector(g_out_p_w - 1 downto 0);
    result_im  : out  std_logic_vector(g_out_p_w - 1 downto 0)
  );
end ip_agi027_1e1v_complex_mult_rtl;

architecture str of ip_agi027_1e1v_complex_mult_rtl is
  function RESIZE_NUM(s : signed; w : natural) return signed is
  begin
    -- extend sign bit or keep LS part
    if w > s'length then
      return resize(s, w);  -- extend sign bit
    else
      return signed(resize(unsigned(s), w));  -- keep LSbits (= vec[w-1:0])
    end if;
  end;

  constant c_prod_w     : natural := g_in_a_w + g_in_b_w;
  constant c_sum_w      : natural := c_prod_w + 1;

--  CONSTANT c_re_add_sub : STRING := sel_a_b(g_conjugate_b, "ADD", "SUB");
--  CONSTANT c_im_add_sub : STRING := sel_a_b(g_conjugate_b, "SUB", "ADD");

  -- registers
  signal reg_ar         : signed(g_in_a_w - 1 downto 0);
  signal reg_ai         : signed(g_in_a_w - 1 downto 0);
  signal reg_br         : signed(g_in_b_w - 1 downto 0);
  signal reg_bi         : signed(g_in_b_w - 1 downto 0);
  signal reg_prod_ar_br : signed(c_prod_w - 1 downto 0);  -- re
  signal reg_prod_ai_bi : signed(c_prod_w - 1 downto 0);
  signal reg_prod_ai_br : signed(c_prod_w - 1 downto 0);  -- im
  signal reg_prod_ar_bi : signed(c_prod_w - 1 downto 0);
  signal reg_sum_re     : signed(c_sum_w - 1 downto 0);
  signal reg_sum_im     : signed(c_sum_w - 1 downto 0);
  signal reg_result_re  : signed(g_out_p_w - 1 downto 0);
  signal reg_result_im  : signed(g_out_p_w - 1 downto 0);

  -- combinatorial
  signal nxt_ar         : signed(g_in_a_w - 1 downto 0);
  signal nxt_ai         : signed(g_in_a_w - 1 downto 0);
  signal nxt_br         : signed(g_in_b_w - 1 downto 0);
  signal nxt_bi         : signed(g_in_b_w - 1 downto 0);
  signal nxt_prod_ar_br : signed(c_prod_w - 1 downto 0);  -- re
  signal nxt_prod_ai_bi : signed(c_prod_w - 1 downto 0);
  signal nxt_prod_ai_br : signed(c_prod_w - 1 downto 0);  -- im
  signal nxt_prod_ar_bi : signed(c_prod_w - 1 downto 0);
  signal nxt_sum_re     : signed(c_sum_w - 1 downto 0);
  signal nxt_sum_im     : signed(c_sum_w - 1 downto 0);
  signal nxt_result_re  : signed(g_out_p_w - 1 downto 0);
  signal nxt_result_im  : signed(g_out_p_w - 1 downto 0);

  -- the active signals
  signal ar             : signed(g_in_a_w - 1 downto 0);
  signal ai             : signed(g_in_a_w - 1 downto 0);
  signal br             : signed(g_in_b_w - 1 downto 0);
  signal bi             : signed(g_in_b_w - 1 downto 0);
  signal prod_ar_br     : signed(c_prod_w - 1 downto 0);  -- re
  signal prod_ai_bi     : signed(c_prod_w - 1 downto 0);
  signal prod_ai_br     : signed(c_prod_w - 1 downto 0);  -- im
  signal prod_ar_bi     : signed(c_prod_w - 1 downto 0);
  signal sum_re         : signed(c_sum_w - 1 downto 0);
  signal sum_im         : signed(c_sum_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------

  -- Put all potential registers in a single process for optimal DSP inferrence
  -- Use rst only if it is supported by the DSP primitive, else leave it at '0'
  p_reg : process (rst, clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        reg_ar         <= (others => '0');
        reg_ai         <= (others => '0');
        reg_br         <= (others => '0');
        reg_bi         <= (others => '0');
        reg_prod_ar_br <= (others => '0');
        reg_prod_ai_bi <= (others => '0');
        reg_prod_ai_br <= (others => '0');
        reg_prod_ar_bi <= (others => '0');
        reg_sum_re     <= (others => '0');
        reg_sum_im     <= (others => '0');
        reg_result_re  <= (others => '0');
        reg_result_im  <= (others => '0');
      elsif clken = '1' then
        reg_ar         <= nxt_ar;  -- inputs
        reg_ai         <= nxt_ai;
        reg_br         <= nxt_br;
        reg_bi         <= nxt_bi;
        reg_prod_ar_br <= nxt_prod_ar_br;  -- products for re
        reg_prod_ai_bi <= nxt_prod_ai_bi;
        reg_prod_ai_br <= nxt_prod_ai_br;  -- products for im
        reg_prod_ar_bi <= nxt_prod_ar_bi;
        reg_sum_re     <= nxt_sum_re;  -- sum
        reg_sum_im     <= nxt_sum_im;
        reg_result_re  <= nxt_result_re;  -- result sum after optional register stage
        reg_result_im  <= nxt_result_im;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Inputs
  ------------------------------------------------------------------------------

  nxt_ar <= signed(in_ar);
  nxt_ai <= signed(in_ai);
  nxt_br <= signed(in_br);
  nxt_bi <= signed(in_bi);

  no_input_reg : if g_pipeline_input = 0 generate  -- wired
    ar <= nxt_ar;
    ai <= nxt_ai;
    br <= nxt_br;
    bi <= nxt_bi;
  end generate;

  gen_input_reg : if g_pipeline_input > 0 generate  -- register input
    ar <= reg_ar;
    ai <= reg_ai;
    br <= reg_br;
    bi <= reg_bi;
  end generate;

  ------------------------------------------------------------------------------
  -- Products
  ------------------------------------------------------------------------------

  nxt_prod_ar_br <= ar * br;  -- products for re
  nxt_prod_ai_bi <= ai * bi;
  nxt_prod_ai_br <= ai * br;  -- products for im
  nxt_prod_ar_bi <= ar * bi;

  no_product_reg : if g_pipeline_product = 0 generate  -- wired
    prod_ar_br <= nxt_prod_ar_br;
    prod_ai_bi <= nxt_prod_ai_bi;
    prod_ai_br <= nxt_prod_ai_br;
    prod_ar_bi <= nxt_prod_ar_bi;
  end generate;
  gen_product_reg : if g_pipeline_product > 0 generate  -- register
    prod_ar_br <= reg_prod_ar_br;
    prod_ai_bi <= reg_prod_ai_bi;
    prod_ai_br <= reg_prod_ai_br;
    prod_ar_bi <= reg_prod_ar_bi;
  end generate;

  ------------------------------------------------------------------------------
  -- Sum
  ------------------------------------------------------------------------------

  -- Re
  -- . "ADD" for a*conj(b) : ar*br + ai*bi
  -- . "SUB" for a*b       : ar*br - ai*bi
  gen_re_add : if g_conjugate_b generate
    nxt_sum_re <= RESIZE_NUM(prod_ar_br, c_sum_w) + prod_ai_bi;
  end generate;

  gen_re_sub : if not g_conjugate_b generate
    nxt_sum_re <= RESIZE_NUM(prod_ar_br, c_sum_w) - prod_ai_bi;
  end generate;

  -- Im
  -- . "ADD" for a*b       : ai*br + ar*bi
  -- . "SUB" for a*conj(b) : ai*br - ar*bi
  gen_im_add : if not g_conjugate_b generate
    nxt_sum_im <= RESIZE_NUM(prod_ai_br, c_sum_w) + prod_ar_bi;
  end generate;

  gen_im_sub : if g_conjugate_b generate
    nxt_sum_im <= RESIZE_NUM(prod_ai_br, c_sum_w) - prod_ar_bi;
  end generate;

  no_adder_reg : if g_pipeline_adder = 0 generate  -- wired
    sum_re <= nxt_sum_re;
    sum_im <= nxt_sum_im;
  end generate;
  gen_adder_reg : if g_pipeline_adder > 0 generate  -- register
    sum_re <= reg_sum_re;
    sum_im <= reg_sum_im;
  end generate;

  ------------------------------------------------------------------------------
  -- Result sum after optional rounding
  ------------------------------------------------------------------------------

  nxt_result_re <= RESIZE_NUM(sum_re, g_out_p_w);
  nxt_result_im <= RESIZE_NUM(sum_im, g_out_p_w);

  no_result_reg : if g_pipeline_output = 0 generate  -- wired
    result_re <= std_logic_vector(nxt_result_re);
    result_im <= std_logic_vector(nxt_result_im);
  end generate;
  gen_result_reg : if g_pipeline_output > 0 generate  -- register
    result_re <= std_logic_vector(reg_result_re);
    result_im <= std_logic_vector(reg_result_im);
  end generate;
end architecture;
