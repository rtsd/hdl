-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Instantiates ta2_unb2b_1GbE component
library IEEE;
use IEEE.std_logic_1164.all;

entity ta2_unb2b_1GbE_ip_wrapper is
  port (
    st_clk             : in std_logic;
    st_rst             : in std_logic;

    -- eth1g UDP streaming ports
    udp_tx_sosi_data   : out std_logic_vector(39 downto 0);
    udp_tx_sosi_valid  : out std_logic;
    udp_tx_sosi_sop    : out std_logic;
    udp_tx_sosi_eop    : out std_logic;
    udp_tx_sosi_empty  : out std_logic_vector(1 downto 0);
    udp_tx_siso_ready  : in  std_logic;
    udp_tx_siso_xon    : in  std_logic;

    udp_rx_sosi_data   : in  std_logic_vector(39 downto 0);
    udp_rx_sosi_valid  : in  std_logic;
    udp_rx_sosi_sop    : in  std_logic;
    udp_rx_sosi_eop    : in  std_logic;
    udp_rx_sosi_empty  : in  std_logic_vector(1 downto 0);
    udp_rx_siso_ready  : out std_logic;
    udp_rx_siso_xon    : out std_logic;

    kernel_clk         : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset       : in  std_logic;

    kernel_src_data    : out std_logic_vector(39 downto 0);  -- RX Data to kernel
    kernel_src_valid   : out std_logic;  -- RX data valid signal to kernel
    kernel_src_ready   : in  std_logic;  -- Flow control from kernel

    kernel_snk_data    : in  std_logic_vector(39 downto 0);  -- TX Data from kernel
    kernel_snk_valid   : in  std_logic;  -- TX data valid signal from kernel
    kernel_snk_ready   : out std_logic  -- Flow control towards kernel
  );
end ta2_unb2b_1GbE_ip_wrapper;

architecture str of ta2_unb2b_1GbE_ip_wrapper is
  ----------------------------------------------------------------------------
  -- ta2_unb2b_1GbE Component
  ----------------------------------------------------------------------------
  component ta2_unb2b_1GbE is
  port (
    st_clk             : in std_logic;
    st_rst             : in std_logic;

    -- eth1g UDP streaming ports
    udp_tx_sosi_data   : out std_logic_vector(39 downto 0);
    udp_tx_sosi_valid  : out std_logic;
    udp_tx_sosi_sop    : out std_logic;
    udp_tx_sosi_eop    : out std_logic;
    udp_tx_sosi_empty  : out std_logic_vector(1 downto 0);
    udp_tx_siso_ready  : in  std_logic;
    udp_tx_siso_xon    : in  std_logic;

    udp_rx_sosi_data   : in  std_logic_vector(39 downto 0);
    udp_rx_sosi_valid  : in  std_logic;
    udp_rx_sosi_sop    : in  std_logic;
    udp_rx_sosi_eop    : in  std_logic;
    udp_rx_sosi_empty  : in  std_logic_vector(1 downto 0);
    udp_rx_siso_ready  : out std_logic;
    udp_rx_siso_xon    : out std_logic;

    kernel_clk         : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset       : in  std_logic;

    kernel_src_data    : out std_logic_vector(39 downto 0);  -- RX Data to kernel
    kernel_src_valid   : out std_logic;  -- RX data valid signal to kernel
    kernel_src_ready   : in  std_logic;  -- Flow control from kernel

    kernel_snk_data    : in  std_logic_vector(39 downto 0);  -- TX Data from kernel
    kernel_snk_valid   : in  std_logic;  -- TX data valid signal from kernel
    kernel_snk_ready   : out std_logic  -- Flow control towards kernel
  );
  end component ta2_unb2b_1GbE;
begin
  u_ta2_unb2b_1GbE : ta2_unb2b_1GbE
    port map (
      st_clk             => st_clk,
      st_rst             => st_rst,

      udp_tx_sosi_data   => udp_tx_sosi_data,
      udp_tx_sosi_valid  => udp_tx_sosi_valid,
      udp_tx_sosi_sop    => udp_tx_sosi_sop,
      udp_tx_sosi_eop    => udp_tx_sosi_eop,
      udp_tx_sosi_empty  => udp_tx_sosi_empty,
      udp_tx_siso_ready  => udp_tx_siso_ready,
      udp_tx_siso_xon    => udp_tx_siso_xon,

      udp_rx_sosi_data   => udp_rx_sosi_data,
      udp_rx_sosi_valid  => udp_rx_sosi_valid,
      udp_rx_sosi_sop    => udp_rx_sosi_sop,
      udp_rx_sosi_eop    => udp_rx_sosi_eop,
      udp_rx_sosi_empty  => udp_rx_sosi_empty,
      udp_rx_siso_ready  => udp_rx_siso_ready,
      udp_rx_siso_xon    => udp_rx_siso_xon,

      kernel_clk         => kernel_clk,
      kernel_reset       => kernel_reset,
      kernel_src_data    => kernel_src_data,
      kernel_src_valid   => kernel_src_valid,
      kernel_src_ready   => kernel_src_ready,
      kernel_snk_data    => kernel_snk_data,
      kernel_snk_valid   => kernel_snk_valid,
      kernel_snk_ready   => kernel_snk_ready
    );
end str;
