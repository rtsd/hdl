-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_clock_phase_detector
-- Usage:
--   > as 5
--   > run -all
--
-- Description:

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_clock_phase_detector is
  generic (
    --g_clk_factor     : REAL := 0.5     -- = clk rate / in_clk rate, must be integer or 1/integer
    g_clk_factor     : real := 2.0  -- = clk rate / in_clk rate, must be integer or 1/integer
  );
end tb_common_clock_phase_detector;

architecture tb of tb_common_clock_phase_detector is
  constant c_clk_factor_num : natural := sel_a_b(g_clk_factor >= 1.0, integer(g_clk_factor),     1);
  constant c_clk_factor_den : natural := sel_a_b(g_clk_factor < 1.0, integer(1.0 / g_clk_factor), 1);

  constant c_in_clk_period : time := c_clk_factor_num * 5 ns;

  constant in_clk_drift    : time := 6 ps;  -- must be 0 or even, use drift to model different clock phases
  constant in_clk_period   : time := c_in_clk_period + in_clk_drift;

  constant clk_period      : time := c_clk_factor_den * c_in_clk_period / c_clk_factor_num;

  constant c_on_interval   : natural := 5000;
  constant c_off_interval  : natural := 500;
  constant c_delay_len     : natural := 3;

  signal tb_end         : std_logic := '0';
  signal in_en          : std_logic := '1';
  signal in_clk         : std_logic := '0';

  signal rst            : std_logic;
  signal clk            : std_logic := '0';
  signal phase_r        : std_logic;
  signal phase_r_det    : std_logic;
  signal phase_f        : std_logic;
  signal phase_f_det    : std_logic;
begin
  in_clk <= not (in_clk and in_en) or tb_end after in_clk_period / 2;
  clk    <= not clk or tb_end after clk_period / 2;

  rst <= '1', '0' after clk_period * 10;

  p_in_stimuli : process
  begin
    in_en  <= '1';
    proc_common_wait_some_cycles(clk, c_on_interval);
    in_en  <= '0';
    proc_common_wait_some_cycles(clk, c_off_interval);
    in_en  <= '1';
    proc_common_wait_some_cycles(clk, c_on_interval);
    in_en  <= '0';
    proc_common_wait_some_cycles(clk, c_off_interval);
    in_en  <= '1';
    proc_common_wait_some_cycles(clk, c_on_interval);
    tb_end <= '1';
    wait;
  end process;

  u_common_clock_phase_detector_r : entity work.common_clock_phase_detector
  generic map (
    g_rising_edge    => true,
    g_meta_delay_len => c_delay_len,
    g_clk_factor     => c_clk_factor_num
  )
  port map (
    in_clk    => in_clk,  -- used as data input for clk domain
    rst       => rst,
    clk       => clk,
    phase     => phase_r,
    phase_det => phase_r_det
  );

  u_common_clock_phase_detector_f : entity work.common_clock_phase_detector
  generic map (
    g_rising_edge    => false,
    g_meta_delay_len => c_delay_len,
    g_clk_factor     => c_clk_factor_num
  )
  port map (
    in_clk    => in_clk,  -- used as data input for clk domain
    rst       => rst,
    clk       => clk,
    phase     => phase_f,
    phase_det => phase_f_det
  );
end tb;
