--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, mdio_lib, technology_lib, tech_xaui_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mdio_lib.mdio_pkg.all;
use mdio_lib.mdio_vitesse_vsc8486_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tr_xaui is
  generic (
    g_technology            : natural := c_tech_select_default;
    g_sim                   : boolean := false;
    g_sim_level             : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_use_xgmii             : boolean := false;  -- Don't use streaming I/O but XGMII (e.g. conenct to 10GbE MAC)
    g_nof_xaui              : natural := 1;  -- Up to 3 (hard XAUI only) supported
    g_use_mdio              : boolean := false;
    g_mdio_epcs_dis         : boolean := false  -- TRUE disables EPCS on init; e.g. to target a 10GbE card in PC that does not support it
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                  : in  std_logic;  -- 156.25 MHz
    tr_rst                  : in  std_logic;

    -- Calibration & reconfiguration clock
    cal_rec_clk             : in  std_logic;

    -- MM interface
    mm_clk                  : in  std_logic := '0';
    mm_rst                  : in  std_logic := '0';

    xaui_mosi               : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso               : out t_mem_miso;

    mdio_mosi_arr           : in  t_mem_mosi_arr(g_nof_xaui - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr           : out t_mem_miso_arr(g_nof_xaui - 1 downto 0);

    -- Streaming TX interfaces
    tx_clk_arr              : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- 156.25 MHz, externally connect tr_clk to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    tx_rst_arr              : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- tx_rst release depends on XAUI ready
    tx_sosi_arr             : in  t_dp_sosi_arr(g_nof_xaui - 1 downto 0) := (others => c_dp_sosi_rst);
    tx_siso_arr             : out t_dp_siso_arr(g_nof_xaui - 1 downto 0);

    -- Streaming RX interfaces
    rx_clk_arr_out          : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- recovered clock per XAUI
    rx_clk_arr_in           : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr              : out std_logic_vector(g_nof_xaui - 1 downto 0);
    rx_sosi_arr             : out t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
    rx_siso_arr             : in  t_dp_siso_arr(g_nof_xaui - 1 downto 0) := (others => c_dp_siso_rst);

    -- XGMII interface
    xgmii_tx_dc_arr         : in  t_xgmii_dc_arr(g_nof_xaui - 1 downto 0) := (others => (others => '0'));
    xgmii_rx_dc_arr         : out t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);

    -- Serial IO interface
    xaui_tx_arr             : out t_xaui_arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr             : in  t_xaui_arr(g_nof_xaui - 1 downto 0);

    -- MDIO interface
    mdio_rst                : out std_logic;
    mdio_mdc_arr            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_in_arr        : in  std_logic_vector(g_nof_xaui - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr       : out std_logic_vector(g_nof_xaui - 1 downto 0)
  );
end tr_xaui;

architecture str of tr_xaui is
  signal i_tx_rst_arr              : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal i_rx_rst_arr              : std_logic_vector(g_nof_xaui - 1 downto 0);

  --XGMII data and control combined:
  signal xgmii_tx_dc_in_arr        : t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);
  signal xgmii_rx_dc_out_arr       : t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);

  --XGMII control bits (one for each XGMII lane):
  signal xgmii_tx_c_arr            : t_xgmii_c_arr(g_nof_xaui - 1 downto 0);
  signal xgmii_rx_c_arr            : t_xgmii_c_arr(g_nof_xaui - 1 downto 0);

  --XGMII data
  signal xgmii_tx_d_arr            : t_xgmii_d_arr(g_nof_xaui - 1 downto 0);
  signal xgmii_rx_d_arr            : t_xgmii_d_arr(g_nof_xaui - 1 downto 0);

  signal txc_tx_ready_arr              : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal rxc_rx_ready_arr              : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal txc_rx_channelaligned_arr     : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal tx_framer_siso_arr        : t_dp_siso_arr(g_nof_xaui - 1 downto 0);
  signal tx_framer_sosi_arr        : t_dp_sosi_arr(g_nof_xaui - 1 downto 0);
begin
  tx_rst_arr <= i_tx_rst_arr;
  rx_rst_arr <= i_rx_rst_arr;

  -----------------------------------------------------------------------------
  -- XAUI PHY
  -----------------------------------------------------------------------------

  i_tx_rst_arr <= not txc_tx_ready_arr;
  i_rx_rst_arr <= not rxc_rx_ready_arr;

  u_tech_xaui : entity tech_xaui_lib.tech_xaui
  generic map (
    g_technology => g_technology,
    g_sim        => g_sim,
    g_sim_level  => g_sim_level,
    g_nof_xaui   => g_nof_xaui
  )
  port map (
    tr_clk                    => tr_clk,
    tr_rst                    => tr_rst,

    cal_rec_clk               => cal_rec_clk,

    mm_rst                    => mm_rst,
    mm_clk                    => mm_clk,

    xaui_mosi                 => xaui_mosi,
    xaui_miso                 => xaui_miso,

    tx_clk_arr                => tx_clk_arr,
    rx_clk_arr_out            => rx_clk_arr_out,
    rx_clk_arr_in             => rx_clk_arr_in,

    txc_tx_ready_arr          => txc_tx_ready_arr,
    rxc_rx_ready_arr          => rxc_rx_ready_arr,
    txc_rx_channelaligned_arr => txc_rx_channelaligned_arr,

    xgmii_tx_dc_arr           => xgmii_tx_dc_in_arr,
    xgmii_rx_dc_arr           => xgmii_rx_dc_out_arr,

    xaui_tx_arr               => xaui_tx_arr,
    xaui_rx_arr               => xaui_rx_arr
  );

  -----------------------------------------------------------------------------
  -- SOSI-XGMII user interface
  -----------------------------------------------------------------------------
  gen_sosi_io: if g_use_xgmii = false generate
    gen_nof_user : for i in g_nof_xaui - 1 downto 0 generate
      xgmii_tx_dc_in_arr(i) <= func_xgmii_dc(xgmii_tx_d_arr(i), xgmii_tx_c_arr(i));

      xgmii_rx_d_arr(i) <= func_xgmii_d(xgmii_rx_dc_out_arr(i));
      xgmii_rx_c_arr(i) <= func_xgmii_c(xgmii_rx_dc_out_arr(i));

      tx_siso_arr(i).ready <= '0' when txc_rx_channelaligned_arr(i) = '0' else tx_framer_siso_arr(i).ready;
      tx_siso_arr(i).xon   <= txc_rx_channelaligned_arr(i);

      tx_framer_sosi_arr(i) <= tx_sosi_arr(i);

      u_tx_framer : entity work.tr_xaui_framer
      generic map (
        g_dat_len => sel_a_b(g_sim, 100, 100000),
        g_gap_len => 10
       )
      port map (
        tx_rst     => i_tx_rst_arr(i),
        tx_clk     => tx_clk_arr(i),

        snk_out    => tx_framer_siso_arr(i),
        snk_in     => tx_framer_sosi_arr(i),

        xgmii_tx_d => xgmii_tx_d_arr(i),
        xgmii_tx_c => xgmii_tx_c_arr(i)
      );

      u_rx_deframer : entity work.tr_xaui_deframer
      port map (
        rx_rst     => i_rx_rst_arr(i),
        rx_clk     => rx_clk_arr_in(i),

        xgmii_rx_d => xgmii_rx_d_arr(i),
        xgmii_rx_c => xgmii_rx_c_arr(i),

        src_out    => rx_sosi_arr(i)
      );
    end generate;  -- g_nof_xaui
  end generate;

  ---------------------------------------------------------------------------
  -- Direct XGMII user interface
  ---------------------------------------------------------------------------
  gen_xgmii_io: if g_use_xgmii = true generate
    xgmii_tx_dc_in_arr <= xgmii_tx_dc_arr;
    xgmii_rx_dc_arr    <= xgmii_rx_dc_out_arr;
  end generate;

  ---------------------------------------------------------------------------
  -- MDIO
  ---------------------------------------------------------------------------
  gen_mdio: if g_use_mdio = true generate
    u_tr_xaui_mdio : entity work.tr_xaui_mdio
    generic map (
      g_sim           => g_sim,
      g_nof_xaui      => g_nof_xaui,
      g_mdio_epcs_dis => g_mdio_epcs_dis
    )
    port map (
      -- Transceiver PLL reference clock
      tr_clk            => tr_clk,
      tr_rst            => tr_rst,

      -- MM clock for register of optional MDIO master
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,

      -- MDIO master = mm slave
      mdio_mosi_arr     => mdio_mosi_arr,
      mdio_miso_arr     => mdio_miso_arr,

      -- MDIO External clock and serial data.
      mdio_rst          => mdio_rst,
      mdio_mdc_arr      => mdio_mdc_arr,
      mdio_mdat_in_arr  => mdio_mdat_in_arr,
      mdio_mdat_oen_arr => mdio_mdat_oen_arr
    );
  end generate;
end str;
