-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Monitor the nof valid clock cycles between two in_evt pulses
-- Description:
--   The in_evt pulses define the interval. Leave in_val not connected to count
--   every clock cycle.
-- Remarks:

entity common_interval_monitor is
  generic (
    g_interval_cnt_w  : natural := 20  -- wide enough to fit somewhat more than maximum nof valid clock cycles per interval
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- ST
    in_val        : in  std_logic := '1';
    in_evt        : in  std_logic;
    -- MM
    interval_cnt  : out std_logic_vector(g_interval_cnt_w - 1 downto 0);
    clk_cnt       : out std_logic_vector(g_interval_cnt_w - 1 downto 0)
  );
end common_interval_monitor;

architecture rtl of common_interval_monitor is
  signal i_clk_cnt        : std_logic_vector(interval_cnt'range);
  signal nxt_clk_cnt      : std_logic_vector(interval_cnt'range);
  signal i_interval_cnt   : std_logic_vector(interval_cnt'range);
  signal nxt_interval_cnt : std_logic_vector(interval_cnt'range);
begin
  interval_cnt <= i_interval_cnt;
  clk_cnt      <= i_clk_cnt;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      i_clk_cnt        <= (others => '1');
      i_interval_cnt <= (others => '1');
    elsif rising_edge(clk) then
      i_clk_cnt        <= nxt_clk_cnt;
      i_interval_cnt <= nxt_interval_cnt;
    end if;
  end process;

  p_counter : process(i_clk_cnt, i_interval_cnt, in_evt, in_val)
  begin
    nxt_clk_cnt      <= i_clk_cnt;
    nxt_interval_cnt <= i_interval_cnt;

    if in_evt = '1' then
      -- If there is an in_evt pulse, then capture the i_clk_cnt into interval_cnt and restart i_clk_cnt
      nxt_clk_cnt      <= (others => '0');
      nxt_interval_cnt <= INCR_UVEC(i_clk_cnt, 1);
    elsif signed(i_clk_cnt) = -1 then
      -- If there occur no in_evt pulses, then i_clk_cnt will eventually stop at maximum (= -1)
      nxt_clk_cnt      <= (others => '1');
      nxt_interval_cnt <= (others => '1');
    elsif in_val = '1' then
      -- Increment for valid clk cycles
      nxt_clk_cnt <= INCR_UVEC(i_clk_cnt, 1);
    end if;
  end process;
end rtl;
