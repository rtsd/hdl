library IEEE, pfs_lib;
use IEEE.std_logic_1164.all;
library common_lib;
use common_lib.common_pkg.all;

architecture str of pfs_fir is
  constant c_nof_taps_w      : natural := ceil_log2(g_nof_taps);
  constant c_nof_prefilter_w : natural := ceil_log2(g_nof_prefilter);
  constant c_nof_coefs       : natural := g_nof_prefilter * g_nof_taps;
  constant c_addr_w          : natural := ceil_log2(c_nof_coefs);
  constant c_mult_latency    : natural := 1;

  signal sample_data_hor     : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal sample_data_ver     : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal sample_addr         : std_logic_vector(c_addr_w - 1 downto 0);
  signal sample_wren         : std_logic;
  signal taps_data_hor       : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal taps_data_ver       : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal taps_addr           : std_logic_vector(c_addr_w - 1 downto 0);
  signal taps_rden           : std_logic;
  signal coefs_addr          : std_logic_vector(c_addr_w - 1 downto 0);
  signal coefs_data          : std_logic_vector(g_coef_dat_w - 1 downto 0);
  signal coefs_rden          : std_logic;
  signal res_clr             : std_logic;
  signal i_res_val           : std_logic;
begin
  ctrl : entity pfs_lib.pfs_fir_ctrl
  generic map (
    g_nof_prefilter   => g_nof_prefilter,
    g_nof_prefilter_w => c_nof_prefilter_w,
    g_nof_taps        => g_nof_taps,
    g_nof_taps_w      => c_nof_taps_w,
    g_sample_width    => g_in_dat_w
  )
  port map (
    clk               => clk,
    rst               => rst,
    input_hor         => in_hor,
    input_ver         => in_ver,
    input_val         => in_val,
    input_sync        => in_sync,
    coefs_addr        => coefs_addr,
    coefs_rden        => coefs_rden,
    sample_addr       => sample_addr,
    sample_data_hor   => sample_data_hor,
    sample_data_ver   => sample_data_ver,
    sample_wren       => sample_wren,
    taps_addr         => taps_addr,
    taps_rden         => taps_rden,
    res_clr           => res_clr,
    result_val        => i_res_val,
    result_sync       => res_sync
  );

  mac_hor : entity pfs_lib.pfs_fir_mac
  generic map (
    g_a_in_w          => g_in_dat_w,
    g_b_in_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => c_nof_taps_w,
    g_mult_pipeline   => c_mult_latency
  )
  port map (
    data_a            => taps_data_hor,
    data_b            => coefs_data,
    res_clr           => res_clr,
    res_val           => i_res_val,
    clk               => clk,
    rst               => rst,
    result            => res_hor
  );

  mac_ver : entity pfs_lib.pfs_fir_mac
  generic map (
    g_a_in_w          => g_in_dat_w,
    g_b_in_w          => g_coef_dat_w,
    g_out_w           => g_out_dat_w,
    g_taps_w          => c_nof_taps_w,
    g_mult_pipeline   => c_mult_latency
  )
  port map (
    data_a            => taps_data_ver,
    data_b            => coefs_data,
    res_clr           => res_clr,
    res_val           => i_res_val,
    clk               => clk,
    rst               => rst,
    result            => res_ver
  );

  coefsbuf_0 : if g_fir_nr = 0 generate
    coefsbuf : entity pfs_lib.pfs_fir_coefsbuf
    generic map (
      g_data_w        => g_coef_dat_w,
      g_coefs_w       => c_addr_w,
      g_nof_coefs     => c_nof_coefs,
      g_init_file     => "../../../../../EPA/pfs/src/data/pfs_fir_coeff_0_"
          & natural'image(g_nof_prefilter * g_nof_taps) & "pts.hex"
    )
    port map (
      addr            => coefs_addr,
      rden            => coefs_rden,
      data            => coefs_data,
      clk             => clk,
      rst             => rst
    );
  end generate;

  coefsbuf_N : if g_fir_nr > 0 generate
    coefsbuf : entity pfs_lib.pfs_fir_coefsbuf
    generic map (
      g_data_w        => g_coef_dat_w,
      g_coefs_w       => c_addr_w,
      g_nof_coefs     => c_nof_coefs,
      g_init_file     => "../../../../../EPA/pfs/src/data/pfs_fir_coeff_"
          & natural'image(g_fir_nr) & "_" & natural'image(g_nof_prefilter * g_nof_taps) & "pts.hex"
    )
    port map (
      addr            => coefs_addr,
      rden            => coefs_rden,
      data            => coefs_data,
      clk             => clk,
      rst             => rst
    );
  end generate;

  tapsbuf_hor : entity pfs_lib.pfs_fir_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w,
    g_nof_words       => c_nof_coefs,
    g_addr_w          => c_addr_w
  )
  port map (
    data_a            => sample_data_hor,
    wren_a            => sample_wren,
    addr_a            => sample_addr,
    addr_b            => taps_addr,
    rden_b            => taps_rden,
    data_b            => taps_data_hor,
    clk               => clk,
    rst               => rst
  );

  tapsbuf_ver : entity pfs_lib.pfs_fir_tapsbuf
  generic map (
    g_data_w          => g_in_dat_w,
    g_nof_words       => c_nof_coefs,
    g_addr_w          => c_addr_w
  )
  port map (
    data_a            => sample_data_ver,
    wren_a            => sample_wren,
    addr_a            => sample_addr,
    addr_b            => taps_addr,
    rden_b            => taps_rden,
    data_b            => taps_data_ver,
    clk               => clk,
    rst               => rst
  );

  res_val <= i_res_val;
end str;
