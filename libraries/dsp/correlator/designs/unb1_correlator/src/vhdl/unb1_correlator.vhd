-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, correlator_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity unb1_correlator is
  generic (
    g_design_name : string  := "unb1_correlator";
    g_sim         : boolean := false;  -- Overridden by TB
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0;
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn   : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc      : inout std_logic;
    sens_sd      : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk      : in    std_logic;
    ETH_SGIN     : in    std_logic;
    ETH_SGOUT    : out   std_logic
  );
end unb1_correlator;

architecture str of unb1_correlator is
  -- Firmware version x.y
  constant c_fw_version             : t_unb1_board_fw_version := (1, 1);
  -- In simulation we don't need the 1GbE core for MM control, deselect it in c_use_phy based on g_sim
  constant c_use_phy                : t_c_unb1_board_use_phy  := (sel_a_b(g_sim, 0, 1), 0, 0, 0, 0, 0, 0, 1);

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- Correlator
  constant c_nof_inputs             : natural := 24;
  constant c_nof_input_folds        : natural := 1;
  constant c_nof_input_streams      : natural := c_nof_inputs / pow2(c_nof_input_folds);
  constant c_nof_pre_mult_folds     : natural := 1;
  constant c_complex_data_w         : natural := 8;
  constant c_conjugate              : boolean := true;
  constant c_nof_channels           : natural := 64;
  constant c_integration_period     : natural := 12208;

  constant c_nof_visibilities       : natural := (c_nof_inputs * (c_nof_inputs + 1)) / 2;

  -- Gap size on the correlator input depends on the number of folds
  constant c_block_period           : natural := 1;  -- pow2(c_nof_pre_mult_folds);  -- To do: figure out the block period as function of both folding factors.

  -- Block generator
  constant c_bg_block_size          : natural := c_nof_channels * pow2(c_nof_input_folds);
  constant c_bg_gapsize             : natural := c_bg_block_size * (c_block_period - 1);

  -- Indicate the integration period with the sync. In the correlator, the
  -- integration period is forced to a minimum of c_nof_visibilities to
  -- allow folding the output onto one stream.
  -- . The sync pulse is only there for the human eye (wave window) -
  --   it is not used by the correlator.
  constant c_bg_blocks_per_sync     : natural := largest(c_integration_period, c_nof_visibilities);

  constant c_bg_ctrl                : t_diag_block_gen := ('1',  -- enable
                                                           '0',  -- enable_sync
                                                          TO_UVEC(     c_bg_block_size, c_diag_bg_samples_per_packet_w),
                                                          TO_UVEC(c_bg_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                          TO_UVEC(        c_bg_gapsize, c_diag_bg_gapsize_w),
                                                          TO_UVEC(                   0, c_diag_bg_mem_low_adrs_w),
                                                          TO_UVEC(   c_bg_block_size-1, c_diag_bg_mem_high_adrs_w),
                                                          TO_UVEC(                   0, c_diag_bg_bsn_init_w));

  signal block_gen_src_out_arr      : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);

  signal dp_fifo_sc_src_out_arr     : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);
  signal dp_fifo_sc_src_in_arr      : t_dp_siso_arr(c_nof_input_streams - 1 downto 0);

  signal correlator_snk_in_arr      : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);
  signal correlator_src_out_arr     : t_dp_sosi_arr(1 - 1 downto 0);

  signal ram_diag_data_buf_mosi     : t_mem_mosi;
  signal ram_diag_data_buf_miso     : t_mem_miso;
  signal reg_diag_data_buf_mosi     : t_mem_mosi;
  signal reg_diag_data_buf_miso     : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- Block generators
  -- . Each stream in block_gen_src_out_arr contains complex subband samples
  --   from one 'antenna'.
  -- . These complex subband samples are generated and converted to a HEX RAM
  --   initialization file using Python, see tb/python/gen_subband_hex_files.py
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen : entity diag_lib.mms_diag_block_gen
  generic map (
    g_nof_streams        => c_nof_input_streams,
    g_buf_dat_w          => 2 * c_complex_data_w,
    g_buf_addr_w         => ceil_log2(TO_UINT(c_bg_ctrl.samples_per_packet)),
    g_file_name_prefix   => "../../../libraries/dsp/correlator/src/hex/complex_subbands_" & natural'image(c_complex_data_w) & "b_" & "fold_" & natural'image(c_nof_input_folds),
    g_diag_block_gen_rst => c_bg_ctrl
  )
  port map (
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,

    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    out_sosi_arr     => block_gen_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Introduce gaps in the input stream
  -- . mms_diag_block_gen does not support gaps within blocks.
  -- . We'll use FIFO buffers and dp_gap to read out the FIFOs to introduce
  --   gaps.
  -----------------------------------------------------------------------------
  gen_dp_fifo_sc : for i in 0 to c_nof_input_streams - 1 generate
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
    generic map (
      g_data_w         => 2 * c_complex_data_w,
      g_use_ctrl       => false,
      g_use_complex    => true,
      g_fifo_size      => c_bg_block_size,
      g_fifo_af_margin => 0
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      wr_ful      => OPEN,
      usedw       => OPEN,
      rd_emp      => OPEN,

      snk_out     => OPEN,
      snk_in      => block_gen_src_out_arr(i),
      src_in      => dp_fifo_sc_src_in_arr(i),
      src_out     => dp_fifo_sc_src_out_arr(i)
    );
  end generate;

  gen_dp_src_out_timer : for i in 0 to c_nof_input_streams - 1 generate
    u_dp_src_out_timer : entity dp_lib.dp_src_out_timer
    generic map (
      g_block_period => c_block_period
    )
    port map (
      rst                  => dp_rst,
      clk                  => dp_clk,

      snk_in               => dp_fifo_sc_src_out_arr(i),
      snk_out              => dp_fifo_sc_src_in_arr(i)
    );
  end generate;

  correlator_snk_in_arr <= dp_fifo_sc_src_out_arr;

  -----------------------------------------------------------------------------
  -- Correlator
  -----------------------------------------------------------------------------
  u_correlator : entity correlator_lib.correlator
  generic map (
    g_nof_input_streams   => c_nof_input_streams,
    g_input_unfold_factor => c_nof_input_folds,
    g_nof_pre_mult_folds  => c_nof_pre_mult_folds,
    g_data_w              => c_complex_data_w,
    g_conjugate           => c_conjugate,
    g_nof_channels        => c_nof_channels,
    g_integration_period  => c_integration_period
  )
  port map (
    clk         => dp_clk,
    rst         => dp_rst,

    snk_in_arr  => correlator_snk_in_arr,
    src_out_arr => correlator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Data buffer to be read out by Python
  -----------------------------------------------------------------------------
  u_diag_data_buffer : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => 1,
    g_data_w       => 64,
    g_data_type    => e_complex,
    g_buf_nof_data => c_nof_visibilities,
    g_buf_use_sync => true
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,

    in_sync           => correlator_src_out_arr(0).sop,
    in_sosi_arr       => correlator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim         => g_sim,
    g_design_name => g_design_name,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn,
    g_fw_version  => c_fw_version,
    g_mm_clk_freq => c_unb1_board_mm_clk_freq_50M,
    g_use_phy     => c_use_phy,
    g_aux         => c_unb1_board_aux,
    g_dp_clk_use_pll => false  -- Use altpll_1 in QSYS
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    epcs_clk                 => '0',

    dp_rst                   => dp_rst,
    dp_clk                   => OPEN,  -- dp_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => '0',
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb1_correlator
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    xo_clk                   => xo_clk,
    xo_rst_n                 => xo_rst_n,
    xo_rst                   => xo_rst,

    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,

    clk_clk                  => CLK,  -- altpll_1 ref clk (200MHz)
    clk_clk_in_reset_reset_n => xo_rst_n,
    altpll_1_c0_clk          => dp_clk,  -- altpll_1 output clock

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- . Data buffers
    reg_diag_data_buf_mosi   => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso   => reg_diag_data_buf_miso,

    ram_diag_data_buf_mosi   => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso   => ram_diag_data_buf_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso
  );
end str;
