-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for common_stable_monitor
-- Description: See common_stable_monitor.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mms_common_stable_monitor is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_nof_input          : natural := 8
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock
    st_rst     : in  std_logic;  -- reset synchronous with st_clk
    st_clk     : in  std_logic;  -- other clock domain clock

    st_in_arr  : in  std_logic_vector(g_nof_input - 1 downto 0);

    -- Memory-mapped clock domain
    reg_mosi   : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    reg_miso   : out t_mem_miso  -- actual ranges defined by c_mm_reg
  );
end mms_common_stable_monitor;

architecture str of mms_common_stable_monitor is
  -- Define the actual size of the MM slave register
  constant c_nof_dat : natural := ceil_div(g_nof_input, c_word_w);
  constant c_adr_w   : natural := ceil_log2(c_nof_dat);
  constant c_dat_w   : natural := sel_a_b(c_nof_dat = 1, g_nof_input, c_word_w);

  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => c_adr_w,
                                  dat_w    => c_dat_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => c_nof_dat,
                                  init_sl  => '0');

  signal reg_rd_arr     : std_logic_vector(c_mm_reg.nof_dat - 1 downto 0);

  signal st_stable_arr  : std_logic_vector(g_nof_input - 1 downto 0);
  signal st_stable_ack  : std_logic;

  signal in_reg         : std_logic_vector(c_nof_dat * c_word_w - 1 downto 0);
begin
  u_mm_reg : entity work.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_readback           => false,
    g_reg                => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => st_rst,
    st_clk         => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_mosi,
    sla_out        => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => reg_rd_arr,
    in_reg         => in_reg,
    out_reg        => open
  );

  in_reg(g_nof_input - 1 downto 0) <= st_stable_arr;

  -- Use last part of register for stable read acknowledge
  st_stable_ack <= reg_rd_arr(c_mm_reg.nof_dat - 1);

  gen_mon : for I in g_nof_input - 1 downto 0 generate
    u_stable_monitor : entity work.common_stable_monitor
    port map (
      rst          => st_rst,
      clk          => st_clk,
      -- MM
      r_in         => st_in_arr(I),
      r_stable     => st_stable_arr(I),
      r_stable_ack => st_stable_ack
    );
  end generate;
end str;
