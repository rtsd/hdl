-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author :
--   D. van der Schuur  May 2012  Original for Python - file IO - VHDL
--   E. Kooistra        feb 2017  Added purpose and description
--                                Added procedures for external control in a
--                                pure VHDL test bench.
--
-- Purpose: Provide DUT access via MM bus through file IO per MM slave
-- Description:
--   This package provides file IO access to MM slaves and to the status of
--   the simulation:
--
-- 1) MM slave access
--   Access to MM slaves is provided by component mm_file.vhd that first calls
--   mmf_file_create() and loop forever calling mmf_mm_from_file(). Each MM
--   slave has a dedicated pair of request (.ctrl) and response (.stat) IO
--   files.
--   The mmf_file_create() creates the .ctrl file and mmf_mm_from_file() reads
--   it to check whether there is a WR or RD access request. For a WR request
--   the wr_data and wr_addr are read from the .ctrl and output on the MM bus
--   via mm_mosi. For a RD access request the rd_addr is read from the .ctrl
--   and output on the MM bus via mm_mosi. The after the read latency the
--   rd_data is written to the .stat file that is then created and closed.
--
--                    wr             rd  _________               __________
--   mmf_mm_bus_wr() ---> ctrl file --->|         |---mm_mosi-->|          |
--                                      | mm_file |             | MM slave |
--   mmf_mm_bus_rd() <--- stat file <---|___\_____|<--mm_miso---|__________|
--                    rd             wr      \
--                                            \--> loop: mmf_mm_from_file()
--
--   The ctrl file is created by mm_file at initialization and recreated by
--   every call of mmf_mm_from_file().
--   The stat file is recreated by every call of mmf_mm_bus_rd().
--
-- 2) Simulator access
--   External access to the simulation is provided via a .ctrl file that
--   supports GET_SIM_TIME and then report the NOW time via the .stat file.
--   The simulation access is provided via a procedure mmf_poll_sim_ctrl_file()
--   that works similar component mm_file.vhd.
--
--                      wr             rd
--                    |---> ctrl file --->|
--   mmf_sim_get_now()|                   |mmf_poll_sim_ctrl_file()
--                    |<--- stat file <---|  \
--                      rd             wr     \
--                                             \--> loop: mmf_sim_ctrl_from_file()
--
--   The ctrl file is created by mmf_poll_sim_ctrl_file at initialization and
--   recreated by every call of mmf_sim_ctrl_from_file().
--   The stat file is recreated by every call of mmf_sim_get_now().
--
-- A) External control by a Python script
--   A Python script can issue requests via the .ctrl files to control the
--   simulation and read the .stat files. This models the MM access via a
--   Monitoring and Control protocol via 1GbE.
--
--   Internal procedures:
--   . mmf_file_create(filename: IN STRING);
--   . mmf_mm_from_file(SIGNAL mm_clk  : IN STD_LOGIC;
--   . mmf_sim_ctrl_from_file(rd_filename: IN STRING;
--
--   External procedures (used in a VHDL design to provide access to the MM
--   slaves and simulation via file IO):
--   . mm_file.vhd --> instead of a procedure MM slave file IO uses a component
--   . mmf_poll_sim_ctrl_file()
--
-- B) External control by a VHDL process --> see tb_mm_file.vhd
--   Instead of a Python script the file IO access to the MM slaves can also
--   be used in a pure VHDL testbench. This is useful when the MM slave bus
--   signals (mm_mosi, mm_miso) are not available on the entity of the DUT
--   (device under test), which is typically the case when a complete FPGA
--   design needs to be simulated.
--
--   Internal procedures:
--   . mmf_wait_for_file_status()
--   . mmf_wait_for_file_empty()
--   . mmf_wait_for_file_not_empty()
--
--   External procedures (used in a VHDL test bench to provide access to the
--   MM slaves in a DUT VHDL design and simulation via file IO):
--   . mmf_mm_bus_wr()
--   . mmf_mm_bus_rd()
--   . mmf_sim_get_now()
--
--   External function to create unique sim.ctrl/sim.stat filename per test bench in a multi tb
--   . mmf_slave_prefix()
--
-- Remarks:
-- . The timing of the MM access in mmf_mm_bus_wr() and mmf_mm_bus_rd() and the
--   simulation access in mmf_sim_get_now() is not critical. The timing of the first
--   access depends on the tb. Due to falling_edge(mm_clk) in mmf_wait_for_file_*()
--   all subsequent accesses will start at falling_edge(mm_clk)

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use common_lib.common_str_pkg.all;

package mm_file_pkg is
  -- Constants used by mm_file.vhd
  constant c_mmf_mm_clk_period : time :=  100 ps;  -- Default mm_clk period in simulation. Set much faster than DP clock to speed up
                                                   -- simulation of MM access. Without file IO throttling 100 ps is a good balance
                                                   -- between simulation speed and file IO rate.
  constant c_mmf_mm_timeout    : time := 1000 ns;  -- Default MM file IO timeout period. Set large enough to account for MM-DP clock
                                                   -- domain crossing delays. Use 0 ns to disable file IO throttling, to have file IO
                                                   -- at the mm_clk rate.
  constant c_mmf_mm_pause      : time :=  100 ns;  -- Default MM file IO pause period after timeout. Balance between file IO rate
                                                   -- reduction and responsiveness to new MM access.

  -- Procedure to (re)create empty file
  procedure mmf_file_create(filename: in string);

  -- Procedure to perform an MM access from file
  procedure mmf_mm_from_file(signal mm_clk  : in std_logic;
                             signal mm_rst  : in std_logic;
                             signal mm_mosi : out t_mem_mosi;
                             signal mm_miso : in  t_mem_miso;
                             rd_filename: in string;
                             wr_filename: in string;
                             rd_latency: in natural);

  -- Procedure to process a simulation status request from the .ctrl file and provide response via the .stat file
  procedure mmf_sim_ctrl_from_file(rd_filename: in string;
                                   wr_filename: in string);

  -- Procedure to poll the simulation status
  procedure mmf_poll_sim_ctrl_file(rd_file_name: in string;
                                   wr_file_name: in string);

  -- Procedure to poll the simulation status
  procedure mmf_poll_sim_ctrl_file(signal mm_clk  : in std_logic;
                                   rd_file_name: in string;
                                   wr_file_name: in string);

  -- Procedures that keep reading the file until it has been made empty or not empty by some other program,
  -- to ensure the file is ready for a new write access
  procedure mmf_wait_for_file_status(rd_filename   : in string;  -- file name with extension
                                     exit_on_empty : in boolean;
                                     signal mm_clk : in std_logic);

  procedure mmf_wait_for_file_empty(rd_filename   : in string;  -- file name with extension
                                    signal mm_clk : in std_logic);
  procedure mmf_wait_for_file_not_empty(rd_filename   : in string;  -- file name with extension
                                        signal mm_clk : in std_logic);

  -- Procedure to issue a write access via the MM request .ctrl file
  procedure mmf_mm_bus_wr(filename      : in string;  -- file name without extension
                          wr_addr       : in integer;  -- use integer to support full 32 bit range
                          wr_data       : in integer;
                          signal mm_clk : in std_logic);

  -- Procedure to issue a read access via the MM request .ctrl file and get the read data from the MM response file
  procedure mmf_mm_bus_rd(filename       : in string;  -- file name without extension
                          rd_latency     : in natural;
                          rd_addr        : in integer;  -- use integer to support full 32 bit range
                          signal rd_data : out std_logic_vector(c_word_w - 1 downto 0);
                          signal mm_clk  : in std_logic);
  -- . rd_latency = 1
  procedure mmf_mm_bus_rd(filename       : in string;
                          rd_addr        : in integer;
                          signal rd_data : out std_logic_vector(c_word_w - 1 downto 0);
                          signal mm_clk  : in std_logic);

  -- Procedure that reads the rd_data every rd_interval until has the specified rd_value, the proc arguments can be understood as a sentence
  procedure mmf_mm_wait_until_value(filename         : in string;  -- file name without extension
                                    rd_addr          : in integer;
                                    c_representation : in string;  -- treat rd_data as "SIGNED" or "UNSIGNED" 32 bit word
                                    signal rd_data   : inout std_logic_vector(c_word_w - 1 downto 0);
                                    c_condition      : in string;  -- ">", ">=", "=", "<=", "<", "/="
                                    c_rd_value       : in integer;
                                    c_rd_interval    : in time;
                                    signal mm_clk    : in std_logic);

  -- Procedure to get NOW via simulator status
  procedure mmf_sim_get_now(filename       : in string;  -- file name without extension
                            signal rd_now  : out string;
                            signal mm_clk  : in std_logic);

  -- Functions to create prefixes for the mmf file filename
  function mmf_prefix(name : string; index : natural) return string;  -- generic prefix name with index to be used for a file IO filename
  function mmf_tb_prefix(tb : integer) return string;  -- fixed test bench prefix with index tb to allow file IO with multi tb
  function mmf_subrack_prefix(subrack : integer) return string;  -- fixed subrack prefix with index subrack to allow file IO with multi subracks that use same unb numbers

  -- Functions to create mmf file prefix that is unique per slave, for increasing number of hierarchy levels:
  -- . return "filepath/s0_i0_"
  -- . return "filepath/s0_i0_s1_i1_"
  -- . return "filepath/s0_i0_s1_i1_s2_i2_"
  -- . return "filepath/s0_i0_s1_i1_s2_i2_s3_i3_"
  -- . return "filepath/s0_i0_s1_i1_s2_i2_s3_i3_s4_i4_"
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural) return string;
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural) return string;
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural) return string;
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural) return string;
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural; s4 : string; i4 : natural) return string;

  constant c_mmf_local_dir_path : string := "mmfiles/";  -- local directory in project file build directory
  function mmf_slave_prefix(s0 : string; i0 : natural) return string;
  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural) return string;
  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural) return string;
  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural) return string;
  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural; s4 : string; i4 : natural) return string;

  ----------------------------------------------------------------------------
  -- Declare mm_file component to support positional generic and port mapping of many instances in a TB
  ----------------------------------------------------------------------------
  component mm_file
  generic(
    g_file_prefix       : string;
    g_file_enable       : std_logic := '1';
    g_mm_rd_latency     : natural := 2;
    g_mm_timeout        : time := c_mmf_mm_timeout;
    g_mm_pause          : time := c_mmf_mm_pause
  );
  port (
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;
    mm_master_out : out t_mem_mosi;
    mm_master_in  : in  t_mem_miso
  );
  end component;
end mm_file_pkg;

package body mm_file_pkg is
  procedure mmf_file_create(filename: in string) is
    file created_file : TEXT open write_mode is filename;
  begin
    -- Write the file with nothing in it
    write(created_file, "");
  end;

  procedure mmf_mm_from_file(signal mm_clk : in std_logic;
                             signal mm_rst : in std_logic;
                             signal mm_mosi : out t_mem_mosi;
                             signal mm_miso : in  t_mem_miso;
                             rd_filename: in string;
                             wr_filename: in string;
                             rd_latency: in natural) is
    file rd_file : TEXT;
    file wr_file : TEXT;

    variable open_status_rd: file_open_status;
    variable open_status_wr: file_open_status;

    variable rd_line : LINE;
    variable wr_line : LINE;

    -- Note: Both the address and the data are interpreted as 32-bit data!
    -- This means one has to use leading zeros in the file when either is
    -- less than 8 hex characters, e.g.:
    -- (address) 0000000A
    -- (data)    DEADBEEF
    -- ...as a hex address 'A' would fit in only 4 bits, causing an error in hread().
    variable v_addr_slv : std_logic_vector(c_word_w - 1 downto 0);
    variable v_data_slv : std_logic_vector(c_word_w - 1 downto 0);

    variable v_rd_wr_str : string(1 to 2);  -- Contains 'RD' or 'WR'

  begin

    proc_common_wait_until_low(mm_clk, mm_rst);

    -- We have to open the file explicitely so we can check the status
    file_open(open_status_rd, rd_file, rd_filename, read_mode);

    -- open_status may throw an error if the file is being written to by some other program
    if open_status_rd = open_ok then

      if not endfile(rd_file) then
        -- The file is not empty: process its contents

        -- Read a line from it, first line indicates RD or WR
        readline(rd_file, rd_line);
        read(rd_line, v_rd_wr_str);

        -- The second line represents the address offset:
        readline(rd_file, rd_line);
        hread(rd_line, v_addr_slv);  -- read the string as HEX and assign to SLV.

        -- Write only: The third line contains the data to write:
        if v_rd_wr_str = "WR" then
          readline(rd_file, rd_line);
          hread(rd_line, v_data_slv);  -- read the string as HEX and assign to SLV.
        end if;

        -- We're done reading MM request from the .ctrl file.
        -- Clear the .ctrl file by closing and recreating it, because we don't want to do the same
        -- MM request again the next time this procedure is called.
        file_close(rd_file);
        mmf_file_create(rd_filename);

        -- Execute the MM request to the MM slave
        if v_rd_wr_str = "WR" then
          print_str("[" & time_to_str(now) & "] " & rd_filename & ": Writing 0x" & slv_to_hex(v_data_slv) & " to address 0x" & slv_to_hex(v_addr_slv));
          -- Treat 32 bit hex data from file as 32 bit VHDL INTEGER, so need to use signed TO_SINT() to avoid out of NATURAL range
          -- warning in simulation due to '1' sign bit, because unsigned VHDL NATURAL only fits 31 bits
          proc_mem_mm_bus_wr(TO_UINT(v_addr_slv), TO_SINT(v_data_slv), mm_clk, mm_miso, mm_mosi);

        elsif v_rd_wr_str = "RD" then
          proc_mem_mm_bus_rd(TO_UINT(v_addr_slv), mm_clk, mm_miso, mm_mosi);
          if rd_latency > 0 then
            proc_mem_mm_bus_rd_latency(rd_latency, mm_clk);
          end if;
          v_data_slv := mm_miso.rddata(31 downto 0);
          print_str("[" & time_to_str(now) & "] " & rd_filename & ": Reading from address 0x" & slv_to_hex(v_addr_slv) & ": 0x" & slv_to_hex(v_data_slv));

          -- Write the RD response read data to the .stat file
          file_open(open_status_wr, wr_file, wr_filename, write_mode);
          hwrite(wr_line, v_data_slv);
          writeline(wr_file, wr_line);
          file_close(wr_file);
        end if;

      else
        -- Nothing to process; wait one MM clock cycle.
        proc_common_wait_some_cycles(mm_clk, 1);
      end if;

    else
      report "mmf_mm_from_file() could not open " & rd_filename & " at " & time_to_str(now)
        severity NOTE;
      -- Try again next time; wait one MM clock cycle.
      proc_common_wait_some_cycles(mm_clk, 1);
    end if;

    -- The END implicitely close the rd_file, if still necessary.
  end;

  procedure mmf_sim_ctrl_from_file(rd_filename: in string;
                                   wr_filename: in string) is

    file rd_file : TEXT;
    file wr_file : TEXT;

    variable open_status_rd: file_open_status;
    variable open_status_wr: file_open_status;

    variable rd_line : LINE;
    variable wr_line : LINE;

    variable v_rd_wr_str : string(1 to 12);  -- "GET_SIM_TIME"

  begin

    -- We have to open the file explicitely so we can check the status
    file_open(open_status_rd, rd_file, rd_filename, read_mode);

    -- open_status may throw an error if the file is being written to by some other program
    if open_status_rd = open_ok then

      if not endfile(rd_file) then
        -- The file is not empty: process its contents

        -- Read a line from it, interpret the simulation request
        readline(rd_file, rd_line);
        read(rd_line, v_rd_wr_str);

        -- We're done reading this simulation request .ctrl file. Clear the file by closing and recreating it.
        file_close(rd_file);
        mmf_file_create(rd_filename);

        -- Execute the simulation request
        if v_rd_wr_str = "GET_SIM_TIME" then
          -- Write the GET_SIM_TIME response time NOW to the .stat file
          file_open(open_status_wr, wr_file, wr_filename, write_mode);
          write(wr_line, time_to_str(now));
          writeline(wr_file, wr_line);
          file_close(wr_file);
        end if;

      else
        -- Nothing to process; wait in procedure mmf_poll_sim_ctrl_file
        null;
      end if;

    else
      report "mmf_mm_from_file() could not open " & rd_filename & " at " & time_to_str(now)
        severity NOTE;
      -- Try again next time; wait in procedure mmf_poll_sim_ctrl_file
    end if;

    -- The END implicitely close the rd_file, if still necessary.
  end;

  procedure mmf_poll_sim_ctrl_file(rd_file_name: in string; wr_file_name : in string) is
  begin
    -- Create the ctrl file that we're going to read from
    print_str("[" & time_to_str(now) & "] " & rd_file_name & ": Created" );
    mmf_file_create(rd_file_name);

    while true loop
      mmf_sim_ctrl_from_file(rd_file_name, wr_file_name);
      wait for 1 ns;
    end loop;

  end;

  procedure mmf_poll_sim_ctrl_file(signal mm_clk  : in std_logic;
                                   rd_file_name: in string; wr_file_name : in string) is
  begin
    -- Create the ctrl file that we're going to read from
    print_str("[" & time_to_str(now) & "] " & rd_file_name & ": Created" );
    mmf_file_create(rd_file_name);

    while true loop
      mmf_sim_ctrl_from_file(rd_file_name, wr_file_name);
      proc_common_wait_some_cycles(mm_clk, 1);
    end loop;

  end;

  procedure mmf_wait_for_file_status(rd_filename   : in string;  -- file name with extension
                                     exit_on_empty : in boolean;
                                     signal mm_clk : in std_logic) is
    file     rd_file        : TEXT;
    variable open_status_rd : file_open_status;
    variable v_endfile      : boolean;
  begin
    -- Check on falling_edge(mm_clk) because mmf_mm_from_file() operates on rising_edge(mm_clk)
    -- Note: In fact the file IO also works fine when rising_edge() is used, but then
    --       tb_tb_mm_file.vhd takes about 1% more mm_clk cycles
    wait until falling_edge(mm_clk);

    -- Keep reading the file until it has become empty by some other program
    while true loop
      -- Open the file in read mode to check whether it is empty
      file_open(open_status_rd, rd_file, rd_filename, read_mode);
      -- open_status may throw an error if the file is being written to by some other program
      if open_status_rd = open_ok then
        v_endfile := endfile(rd_file);
        file_close(rd_file);
        if exit_on_empty then
          if v_endfile then
            -- The file is empty; continue
            exit;
          else
            -- The file is not empty; wait one MM clock cycle.
            wait until falling_edge(mm_clk);
          end if;
        else
          if v_endfile then
            -- The file is empty; wait one MM clock cycle.
            wait until falling_edge(mm_clk);
          else
            -- The file is not empty; continue
            exit;
          end if;
        end if;
      else
        report "mmf_wait_for_file_status() could not open " & rd_filename & " at " & time_to_str(now)
          severity NOTE;
        wait until falling_edge(mm_clk);
      end if;
    end loop;
    -- The END implicitely close the file, if still necessary.
  end;

  procedure mmf_wait_for_file_empty(rd_filename   : in string;  -- file name with extension
                                    signal mm_clk : in std_logic) is
  begin
    mmf_wait_for_file_status(rd_filename, true, mm_clk);
  end;

  procedure mmf_wait_for_file_not_empty(rd_filename   : in string;  -- file name with extension
                                        signal mm_clk : in std_logic) is
  begin
    mmf_wait_for_file_status(rd_filename, false, mm_clk);
  end;

  procedure mmf_mm_bus_wr(filename      : in string;  -- file name without extension
                          wr_addr       : in integer;  -- use integer to support full 32 bit range
                          wr_data       : in integer;
                          signal mm_clk : in std_logic) is
    constant ctrl_filename  : string := filename & ".ctrl";
    file     ctrl_file      : TEXT;
    variable open_status_wr : file_open_status;
    variable wr_line        : LINE;

  begin
    -- Write MM WR access to the .ctrl file.
    -- The MM device is ready for a new MM request, because any previous MM request has finished at
    -- mmf_mm_bus_wr() or mmf_mm_bus_rd() procedure exit, therefore just overwrite the .ctrl file.
    file_open(open_status_wr, ctrl_file, ctrl_filename, write_mode);
    -- open_status may throw an error if the file is being written to by some other program
    if open_status_wr = open_ok then
      write(wr_line, string'("WR"));
      writeline(ctrl_file, wr_line);
      hwrite(wr_line, TO_SVEC(wr_addr, c_word_w));
      writeline(ctrl_file, wr_line);
      hwrite(wr_line, TO_SVEC(wr_data, c_word_w));
      writeline(ctrl_file, wr_line);
      file_close(ctrl_file);
    else
      report "mmf_mm_bus_wr() could not open " & ctrl_filename & " at " & time_to_str(now)
        severity NOTE;
    end if;

    -- Prepare for next MM request
    -- Keep reading the .ctrl file until it is empty, to ensure that the MM device is ready for a new MM request
    mmf_wait_for_file_empty(ctrl_filename, mm_clk);

    -- The END implicitely close the ctrl_file, if still necessary.
  end;

  procedure mmf_mm_bus_rd(filename       : in string;  -- file name without extension
                          rd_latency     : in natural;
                          rd_addr        : in integer;  -- use integer to support full 32 bit range
                          signal rd_data : out std_logic_vector(c_word_w - 1 downto 0);
                          signal mm_clk  : in std_logic) is
    constant ctrl_filename  : string := filename & ".ctrl";
    constant stat_filename  : string := filename & ".stat";
    file     ctrl_file      : TEXT;
    file     stat_file      : TEXT;
    variable open_status_wr : file_open_status;
    variable open_status_rd : file_open_status;
    variable wr_line        : LINE;
    variable rd_line        : LINE;
    variable v_rd_data      : std_logic_vector(c_word_w - 1 downto 0);

  begin
    -- Clear the .stat file by recreating it, because we don't want to do read old file data again
    mmf_file_create(stat_filename);

    -- Write MM RD access to the .ctrl file.
    -- The MM device is ready for a new MM request, because any previous MM request has finished at
    -- mmf_mm_bus_wr() or mmf_mm_bus_rd() procedure exit, therefore just overwrite the .ctrl file.
    file_open(open_status_wr, ctrl_file, ctrl_filename, write_mode);
    -- open_status may throw an error if the file is being written to by some other program
    if open_status_wr = open_ok then
      write(wr_line, string'("RD"));
      writeline(ctrl_file, wr_line);
      hwrite(wr_line, TO_SVEC(rd_addr, c_word_w));
      writeline(ctrl_file, wr_line);
      file_close(ctrl_file);
    else
      report "mmf_mm_bus_rd() could not open " & ctrl_filename & " at " & time_to_str(now)
        severity FAILURE;
    end if;

    -- Wait until the MM RD access has written the read data to the .stat file
    mmf_wait_for_file_not_empty(stat_filename, mm_clk);

    -- Read the MM RD access read data from the .stat file
    file_open(open_status_rd, stat_file, stat_filename, read_mode);
    -- open_status may throw an error if the file is being written to by some other program
    if open_status_rd = open_ok then
      readline(stat_file, rd_line);
      hread(rd_line, v_rd_data);
      file_close(stat_file);
      rd_data <= v_rd_data;
      -- wait to ensure rd_data has got v_rd_data, otherwise rd_data still holds the old data on procedure exit
      -- the wait should be < mm_clk period/2 to not affect the read rate
      wait for 1 fs;
    else
      report "mmf_mm_bus_rd() could not open " & stat_filename & " at " & time_to_str(now)
        severity FAILURE;
    end if;

    -- No need to prepare for next MM request, because:
    -- . the .ctrl file must already be empty because the .stat file was there
    -- . the .stat file will be cleared on this procedure entry

    -- The END implicitely closes the files, if still necessary
  end;

  -- rd_latency = 1
  procedure mmf_mm_bus_rd(filename       : in string;
                          rd_addr        : in integer;
                          signal rd_data : out std_logic_vector(c_word_w - 1 downto 0);
                          signal mm_clk  : in std_logic) is
  begin
    mmf_mm_bus_rd(filename, 1, rd_addr, rd_data, mm_clk);
  end;

  procedure mmf_mm_wait_until_value(filename         : in string;  -- file name without extension
                                    rd_addr          : in integer;
                                    c_representation : in string;  -- treat rd_data as "SIGNED" or "UNSIGNED" 32 bit word
                                    signal rd_data   : inout std_logic_vector(c_word_w - 1 downto 0);
                                    c_condition      : in string;  -- ">", ">=", "=", "<=", "<", "/="
                                    c_rd_value       : in integer;
                                    c_rd_interval    : in time;
                                    signal mm_clk    : in std_logic) is
  begin
    while true loop
      -- Read current
      mmf_mm_bus_rd(filename, rd_addr, rd_data, mm_clk);  -- only read low part
      if c_representation = "SIGNED" then
        if    c_condition = ">"  then if TO_SINT(rd_data) > c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = ">=" then if TO_SINT(rd_data) >= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "/=" then if TO_SINT(rd_data) /= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "<=" then if TO_SINT(rd_data) <= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "<"  then if TO_SINT(rd_data) < c_rd_value then exit; else wait for c_rd_interval; end if;
        else                        if TO_SINT(rd_data) = c_rd_value then exit; else wait for c_rd_interval; end if;  -- default: "="
        end if;
      else  -- default: UNSIGED
        if    c_condition = ">"  then if TO_UINT(rd_data) > c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = ">=" then if TO_UINT(rd_data) >= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "/=" then if TO_UINT(rd_data) /= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "<=" then if TO_UINT(rd_data) <= c_rd_value then exit; else wait for c_rd_interval; end if;
        elsif c_condition = "<"  then if TO_UINT(rd_data) < c_rd_value then exit; else wait for c_rd_interval; end if;
        else                        if TO_UINT(rd_data) = c_rd_value then exit; else wait for c_rd_interval; end if;  -- default: "="
        end if;
      end if;
    end loop;
  end mmf_mm_wait_until_value;

  procedure mmf_sim_get_now(filename       : in string;  -- file name without extension
                            signal rd_now  : out string;
                            signal mm_clk  : in std_logic) is
    constant ctrl_filename  : string := filename & ".ctrl";
    constant stat_filename  : string := filename & ".stat";
    file     ctrl_file      : TEXT;
    file     stat_file      : TEXT;
    variable open_status_wr : file_open_status;
    variable open_status_rd : file_open_status;
    variable wr_line        : LINE;
    variable rd_line        : LINE;
    variable v_rd_now       : string(rd_now'range);

  begin
    -- Clear the sim.stat file by recreating it, because we don't want to do read old simulator status again
    mmf_file_create(stat_filename);

    -- Write GET_SIM_TIME to the sim.ctrl file
    -- The simulation is ready for a new simulation status request, because any previous simulation status request has finished at
    -- mmf_sim_get_now() procedure exit, therefore just overwrite the .ctrl file.
    file_open(open_status_wr, ctrl_file, ctrl_filename, write_mode);
    -- open_status may throw an error if the file is being written to by some other program
    if open_status_wr = open_ok then
      write(wr_line, string'("GET_SIM_TIME"));
      writeline(ctrl_file, wr_line);
      file_close(ctrl_file);
    else
      report "mmf_sim_get_now() could not open " & ctrl_filename & " at " & time_to_str(now)
        severity FAILURE;
    end if;

    -- Wait until the simulation has written the simulation status to the sim.stat file
    mmf_wait_for_file_not_empty(stat_filename, mm_clk);

    -- Read the GET_SIM_TIME simulation status from the .stat file
    file_open(open_status_rd, stat_file, stat_filename, read_mode);
    -- open_status may throw an error if the file is being written to by some other program
    if open_status_rd = open_ok then
      readline(stat_file, rd_line);
      read(rd_line, v_rd_now);
      file_close(stat_file);
      rd_now <= v_rd_now;
      print_str("GET_SIM_TIME = " & v_rd_now & " at " & time_to_str(now));
    else
      report "mmf_sim_get_now() could not open " & stat_filename & " at " & time_to_str(now)
        severity FAILURE;
    end if;

    -- No need to prepare for next simulation status request, because:
    -- . the .ctrl file must already be empty because the .stat file was there
    -- . the .stat file will be cleared on this procedure entry

    -- The END implicitely closes the files, if still necessary
  end;

  -- Functions to create prefixes for the mmf file filename
  function mmf_prefix(name : string; index : natural) return string is
  begin
    return name & "_" & int_to_str(index) & "_";
  end;

  function mmf_tb_prefix(tb : integer) return string is
  begin
    return mmf_prefix("TB", tb);
  end;

  function mmf_subrack_prefix(subrack : integer) return string is
  begin
    return mmf_prefix("SUBRACK", subrack);
  end;

  -- Functions to create mmf file prefix that is unique per slave, for increasing number of hierarchy levels:
  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural) return string is
  begin
    return dir_path & mmf_prefix(s0, i0);
  end;

  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural) return string is
  begin
    return dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1);
  end;

  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural) return string is
  begin
    return dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2);
  end;

  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural) return string is
  begin
    return dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2) & mmf_prefix(s3, i3);
  end;

  function mmf_slave_prefix(dir_path, s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural; s4 : string; i4 : natural) return string is
  begin
    return dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2) & mmf_prefix(s3, i3) & mmf_prefix(s4, i4);
  end;

  -- Use local dir_path
  function mmf_slave_prefix(s0 : string; i0 : natural) return string is
  begin
    return c_mmf_local_dir_path & mmf_prefix(s0, i0);
  end;

  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural) return string is
  begin
    return c_mmf_local_dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1);
  end;

  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural) return string is
  begin
    return c_mmf_local_dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2);
  end;

  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural) return string is
  begin
    return c_mmf_local_dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2) & mmf_prefix(s3, i3);
  end;

  function mmf_slave_prefix(s0 : string; i0 : natural; s1 : string; i1 : natural; s2 : string; i2 : natural; s3 : string; i3 : natural; s4 : string; i4 : natural) return string is
  begin
    return c_mmf_local_dir_path & mmf_prefix(s0, i0) & mmf_prefix(s1, i1) & mmf_prefix(s2, i2) & mmf_prefix(s3, i3) & mmf_prefix(s4, i4);
  end;
end mm_file_pkg;
