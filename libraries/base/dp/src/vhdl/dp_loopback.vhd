--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- General function:
-- =================
--           ________________
--          |                |
-- 0 -----> | snk(0)->src(0) | -----> 0
--          |  ||       /\   |
--          |  \/       ||   |
-- 1 <----- | src(1)<-snk(1) | <----- 1
--          |________________|

-- 00 = Loop back neither
--      connects snk(0) to src(0)
--      connects snk(1) to src(1)
-- 01 = Loop back (0) to (1)
--      connects snk(0) to src(1)
--      connects snk(1) to dp_flush or c_siso_hold depending on g_flush_not_hold
-- 10 = Loop back (1) to (0)
--      connects snk(1) to src(0)
--      connects snk(0) to dp_flush or c_siso_hold depending on g_flush_not_hold
-- 11 = Loop back both
--      connects snk(0) to src(1)
--      connects snk(1) to src(0)

entity dp_loopback is
  generic (
    g_use_pkt        : boolean := true;
    g_flush_not_hold : boolean := true  -- Use TRUE for uninterruptable sources,
  );  -- for flow-controlled sourced FALSE can be used.
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_out_arr : out t_dp_siso_arr(1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(1 downto 0);

    src_in_arr  : in  t_dp_siso_arr(1 downto 0);
    src_out_arr : out t_dp_sosi_arr(1 downto 0);

    lpbk_ctrl   : in  std_logic_vector(1 downto 0);
    lpbk_stat   : out std_logic_vector(1 downto 0)
  );
end dp_loopback;

-- Structural architecture:
-- =======================
--
--              flush_siso(0)/hold_siso(0)
--          ____________     |          ____________
--         |u_dp_demux_0|    |         |u_dp_mux_0  |
--         |         (2)|----/         |            |
-- 0 ----->|         (0)|------------->|(0)         |------> 0
--         |         (1)|---\      /-->|(1)         |
--         |____________|   |      |   |____________|
--          ____________    |      |    ____________
--         |u_dp_mux_1  |   |      |   |u_dp_demux_1|
--         |         (0)|<--/      \---|(0)         |
-- 1 <-----|         (1)|<-------------|(1)         |<------ 1
--         |            |          /---|(2)         |
--         |____________|          |   |____________|
--                                 |
--                    flush_siso(1)/hold_siso(1)
--
-- When using packet boundaries, the muxes may only switch to a new input
-- on a packet boundary on the current input. The dp_muxes themselves do not
-- support this, but the dp_demuxes do (and the streams pass through the demuxes
-- first) - so if we select the new demux outputs before selecting a new mux
-- input, we will ensure that all switching is done on packet boundaries.
-- For example: User wants to loop back sink 0 to source 1:
--    1) u_dp_demux_0 selects new output (1) on the next packet boundary
--       (or when there is no packet present on snk(0) )
--    2) u_dp_demux_1 interrupts stream coming from snk(1) by connecting it to
--       (2) on a packet boundary
--    3) When both demuxes indicate they're done (sel_stat outputs), u_dp_mux_1
--       selects its new input (0) to complete the loopback.

architecture str of dp_loopback is
  constant c_nof_demux_out : natural := 3;
  constant c_nof_mux_in    : natural := 2;

  signal demux_0_siso_arr : t_dp_siso_arr(0 to c_nof_demux_out - 1);
  signal demux_0_sosi_arr : t_dp_sosi_arr(0 to c_nof_demux_out - 1);

  signal demux_1_siso_arr : t_dp_siso_arr(0 to c_nof_demux_out - 1);
  signal demux_1_sosi_arr : t_dp_sosi_arr(0 to c_nof_demux_out - 1);

  signal mux_0_siso_arr   : t_dp_siso_arr(0 to c_nof_mux_in - 1);
  signal mux_0_sosi_arr   : t_dp_sosi_arr(0 to c_nof_mux_in - 1);

  signal mux_1_siso_arr   : t_dp_siso_arr(0 to c_nof_mux_in - 1);
  signal mux_1_sosi_arr   : t_dp_sosi_arr(0 to c_nof_mux_in - 1);

  signal demux_0_sel_ctrl : natural range 0 to c_nof_demux_out - 1 := 0;
  signal demux_0_sel_stat : natural range 0 to c_nof_demux_out - 1;

  signal demux_1_sel_ctrl : natural range 0 to c_nof_demux_out - 1 := 1;
  signal demux_1_sel_stat : natural range 0 to c_nof_demux_out - 1;

  signal mux_0_sel_ctrl   : natural range 0 to c_nof_mux_in - 1 := 0;
  signal mux_1_sel_ctrl   : natural range 0 to c_nof_mux_in - 1 := 1;
begin
  -- The interconnections as shown in the above structure:
  mux_0_sosi_arr(0)   <= demux_0_sosi_arr(0);
  demux_0_siso_arr(0) <= mux_0_siso_arr(0);

  mux_0_sosi_arr(1)   <= demux_1_sosi_arr(0);
  demux_0_siso_arr(0) <= mux_0_siso_arr(1);

  mux_1_sosi_arr(1)   <= demux_1_sosi_arr(1);
  demux_1_siso_arr(1) <= mux_1_siso_arr(1);

  mux_1_sosi_arr(0)   <= demux_0_sosi_arr(1);
  demux_1_siso_arr(1) <= mux_1_siso_arr(0);

  demux_0_siso_arr(2) <= c_dp_siso_flush;
  demux_1_siso_arr(2) <= c_dp_siso_flush;

  u_dp_demux_0: entity work.dp_demux
  generic map (
    g_mode            => 2,
    g_nof_output      => c_nof_demux_out,
    g_combined        => false,
    g_sel_ctrl_invert => true,
    g_sel_ctrl_pkt    => true
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out     => snk_out_arr(0),
    snk_in      => snk_in_arr(0),

    src_in_arr  => demux_0_siso_arr,
    src_out_arr => demux_0_sosi_arr,

    sel_ctrl    => demux_0_sel_ctrl,
    sel_stat    => demux_0_sel_stat
  );

  u_dp_demux_1: entity work.dp_demux
  generic map (
    g_mode            => 2,
    g_nof_output      => c_nof_demux_out,
    g_combined        => false,
    g_sel_ctrl_invert => true,
    g_sel_ctrl_pkt    => true
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out     => snk_out_arr(1),
    snk_in      => snk_in_arr(1),

    src_in_arr  => demux_1_siso_arr,
    src_out_arr => demux_1_sosi_arr,

    sel_ctrl    => demux_1_sel_ctrl,
    sel_stat    => demux_1_sel_stat
  );

  u_dp_mux_0 : entity work.dp_mux
  generic map (
    g_mode            => 2,
    g_sel_ctrl_invert => true
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out_arr => mux_0_siso_arr,
    snk_in_arr  => mux_0_sosi_arr,

    src_in      => src_in_arr(0),
    src_out     => src_out_arr(0),

    sel_ctrl    => mux_0_sel_ctrl
  );

  u_dp_mux_1 : entity work.dp_mux
  generic map (
    g_mode            => 2,
    g_sel_ctrl_invert => true
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out_arr => mux_1_siso_arr,
    snk_in_arr  => mux_1_sosi_arr,

    src_in      => src_in_arr(1),
    src_out     => src_out_arr(1),

    sel_ctrl    => mux_1_sel_ctrl
  );
end str;
