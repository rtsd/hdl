-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the ss_reorder unit.
--           To be used in conjunction with python script: ../python/tc_mmf_ss_reorder.py
--
-- The testbench can be used in two modes: auto-mode and non-auto-mode. The mode
-- is determined by the constant c_modelsim_start in the tc_mmf_ss_reorder.py script.
--
-- Usage in auto-mode (c_modelsim_start = 1 in python):
--   > Run python script in separate terminal: "python tc_mmf_ss_reorder.py --unb 0 --bn 0 --sim"
--
-- Usage in non-auto-mode (c_modelsim_start = 0 in python):
--   > as 5
--   > run -all
--   > Run python script in separate terminal: "python tc_mmf_ss_reorder.py --unb 0 --bn 0 --sim"
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute the WAVE window.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;

entity tb_mmf_reorder_row is
  generic(
    g_nof_inputs    : natural := 8;
    g_nof_outputs   : natural := 16;
    g_dsp_data_w    : natural := 16;
    g_frame_size    : natural := 16;
    g_ram_init_file : string  := "../../../src/data/select_buf";  -- or "UNUSED"
    g_pipeline_in   : natural := 1;  -- pipeline in_data
    g_pipeline_in_m : natural := 1;  -- pipeline in_data for M-fold fan out
    g_pipeline_out  : natural := 1;  -- pipeline out_data
    g_nof_frames    : natural := 1
  );
end tb_mmf_reorder_row;

architecture tb of tb_mmf_reorder_row is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 100 ps;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_sclk_period        : time := 1250 ps;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  signal SCLK                   : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_re_mosi : t_mem_mosi;
  signal ram_diag_data_buf_re_miso : t_mem_miso;

  signal reg_diag_data_buf_re_mosi : t_mem_mosi;
  signal reg_diag_data_buf_re_miso : t_mem_miso;

  signal ram_diag_data_buf_im_mosi : t_mem_mosi;
  signal ram_diag_data_buf_im_miso : t_mem_miso;

  signal reg_diag_data_buf_im_mosi : t_mem_mosi;
  signal reg_diag_data_buf_im_miso : t_mem_miso;

  signal ram_ss_reorder_mosi       : t_mem_mosi;
  signal ram_ss_reorder_miso       : t_mem_miso;

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := g_frame_size * g_nof_frames;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := g_nof_inputs;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_dsp_data_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "UNUSED";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, g_nof_inputs, 1);

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := g_nof_outputs;
  constant c_db_data_w              : positive := g_dsp_data_w;
  constant c_db_buf_nof_data        : positive := c_bg_block_len;
  constant c_db_buf_use_sync        : boolean  := false;
  constant c_db_data_type_re        : t_diag_data_type_enum := e_real;
  constant c_db_data_type_im        : t_diag_data_type_enum := e_imag;

  -- Signal declarations to connect block generator, DUT and databuffers
  signal bg_siso_arr                : t_dp_siso_arr(g_nof_inputs - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr               : t_dp_siso_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_siso_rdy);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  SCLK   <= not SCLK after c_sclk_period / 2;
  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_re_mosi, ram_diag_data_buf_re_miso);

  u_mm_file_reg_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_re_mosi, reg_diag_data_buf_re_miso);

  u_mm_file_ram_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_im_mosi, ram_diag_data_buf_im_miso);

  u_mm_file_reg_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_im_mosi, reg_diag_data_buf_im_miso);

  u_mm_file_ram_ss_reorder       : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_SS_REORDER")
                                           port map(mm_rst, mm_clk, ram_ss_reorder_mosi, ram_ss_reorder_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.reorder_row
  generic map(
    g_nof_inputs        => g_nof_inputs,
    g_nof_outputs       => g_nof_outputs,
    g_dsp_data_w        => g_dsp_data_w,
    g_frame_size        => g_frame_size,
    g_ram_init_file     => g_ram_init_file,
    g_pipeline_in       => g_pipeline_in,
    g_pipeline_in_m     => g_pipeline_in_m,
    g_pipeline_out      => g_pipeline_out
  )
  port map (
    mm_rst              =>  mm_rst,
    mm_clk              =>  mm_clk,
    dp_rst              =>  dp_rst,
    dp_clk              =>  dp_clk,
    -- Memory Mapped
    ram_ss_reorder_mosi => ram_ss_reorder_mosi,
    ram_ss_reorder_miso => ram_ss_reorder_miso,
    -- Streaming
    input_sosi_arr      => bg_sosi_arr,
    output_sosi_arr     => out_sosi_arr,
    output_siso_arr     => out_siso_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_db_nof_streams,
    g_data_type    => c_db_data_type_re,
    g_data_w       => c_db_data_w,
    g_buf_nof_data => c_db_buf_nof_data,
    g_buf_use_sync => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,

    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,

    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_db_nof_streams,
    g_data_type    => c_db_data_type_im,
    g_data_w       => c_db_data_w,
    g_buf_nof_data => c_db_buf_nof_data,
    g_buf_use_sync => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,

    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,

    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
