-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_packetizing_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_packetizing is
  generic (
    -- Try different packing
    g_usr_nof_words  : natural := 4;  -- 1;   -- g_phy_dat_w/g_usr_dat_w = 16/16 = 1/1
    g_phy_nof_words  : natural := 1;  -- 1;
    g_usr_dat_w      : natural := 8;  -- 10;
    g_phy_dat_w      : natural := 32  -- 10  -- <  c_dp_crc_w
  );
end tb_dp_packetizing;

architecture tb of tb_dp_packetizing is
  -- > as 10
  -- > run 30 us                 --> OK
  -- > restart -f
  -- > do tb_dp_packetizing.do   --> to force error that will be reported

  constant c_phy_link_latency         : time := clk_period;
  --CONSTANT c_phy_link_valid_support   : BOOLEAN := TRUE;
  constant c_phy_link_valid_support   : boolean := false;

  constant c_fsn_w                    : natural := 8;  -- <= g_usr_dat_w

  ------------------------------------------------------------------------------
  -- Try different block sizes

  constant c_usr_block_size           : natural := 60;  -- = 2*2*3*5, so suitable for g_usr_nof_words = 1, 2, 3, 4, 5, 6 and with c_dp_gap_min=4 yields minimal c_interval_size
  --CONSTANT c_usr_block_size           : NATURAL := 20 * g_usr_nof_words;  -- dp_repack does not support padding, so the g_usr_nof_words must fit the block size
  constant c_usr_block_size_w         : natural := ceil_log2(c_usr_block_size + c_dp_gap_min);
  constant c_interval_size            : natural := 2**c_usr_block_size_w;
  constant c_gap_size                 : natural := c_interval_size - c_usr_block_size;

  constant c_phy_block_size           : natural := c_usr_block_size * g_phy_nof_words / g_usr_nof_words;

  ------------------------------------------------------------------------------

  constant c_nof_block_per_sync       : natural := 10;

  constant c_fifo_size                : natural := c_usr_block_size;
  constant c_fifo_fill                : integer := sel_a_b(g_usr_nof_words <= g_phy_nof_words,
                                                           0,  -- no_xmt_pacer (dummy value)
                                                           c_usr_block_size - c_phy_block_size);  -- gen_xmt_pacer

  constant c_rcv_fifo_nof_words       : natural := c_phy_block_size;

  signal sl0               : std_logic := '1';
  signal sl1               : std_logic := '1';

  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal sync_nr           : natural;
  signal block_nr          : natural;

  signal in_sync           : std_logic := '0';
  signal in_val            : std_logic := '0';
  signal in_dat            : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '0');

  signal xmt_fsn           : std_logic_vector(c_fsn_w - 1 downto 0) := (others => '0');
  signal xmt_dat           : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '0');
  signal xmt_val           : std_logic;
  signal xmt_sof           : std_logic;
  signal xmt_eof           : std_logic;
  signal xmt_err           : std_logic;

  signal xmt_frm_dat       : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '0');
  signal xmt_frm_val       : std_logic;
  signal xmt_frm_sof       : std_logic;
  signal xmt_frm_eof       : std_logic;

  signal xmt_pack_dat      : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal xmt_pack_val      : std_logic;
  signal xmt_pack_sof      : std_logic;
  signal xmt_pack_eof      : std_logic;

  signal xmt_pace_dat      : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal xmt_pace_val      : std_logic_vector(0 downto 0);
  signal xmt_pace_sof      : std_logic_vector(0 downto 0);
  signal xmt_pace_eof      : std_logic_vector(0 downto 0);

  signal xmt_pace_sosi     : t_dp_sosi;
  signal phy_tx_sosi       : t_dp_sosi;

  signal phy_tx_dat        : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal phy_tx_val        : std_logic;

  signal phy_rx_dat        : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal phy_rx_val        : std_logic;

  signal rcv_pack_dat      : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal rcv_pack_val      : std_logic;
  signal rcv_pack_sof      : std_logic;
  signal rcv_pack_eof      : std_logic;

  signal rcv_pack_sosi     : t_dp_sosi;
  signal rcv_fifo_rd_siso  : t_dp_siso;
  signal rcv_fifo_rd_sosi  : t_dp_sosi;

  signal rcv_fifo_rd_req   : std_logic;
  signal rcv_fifo_rd_dat   : std_logic_vector(g_phy_dat_w - 1 downto 0);
  signal rcv_fifo_rd_val   : std_logic;
  signal rcv_fifo_rd_sof   : std_logic;
  signal rcv_fifo_rd_eof   : std_logic;

  signal rcv_throttle_dat  : std_logic_vector(g_phy_dat_w - 1 downto 0) := (others => '0');
  signal rcv_throttle_val  : std_logic;
  signal rcv_throttle_sof  : std_logic;
  signal rcv_throttle_eof  : std_logic;

  signal rcv_frm_dat       : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '0');
  signal rcv_frm_val       : std_logic;
  signal rcv_frm_sof       : std_logic;
  signal rcv_frm_eof       : std_logic;

  signal rcv_sync          : std_logic;
  signal rcv_fsn           : std_logic_vector(c_fsn_w - 1 downto 0) := (others => '0');
  signal rcv_dat           : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '0');
  signal rcv_val           : std_logic;
  signal rcv_sof           : std_logic;
  signal rcv_eof           : std_logic;
  signal rcv_err           : std_logic := '0';

  signal prev_rcv_dat      : std_logic_vector(g_usr_dat_w - 1 downto 0) := (others => '1');  -- -1 to also verify first sample 0 correctly
begin
  clk <= not clk after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION and VERIFICATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  proc_dp_gen_block_data(c_nof_block_per_sync,
                         c_usr_block_size,
                         c_gap_size,
                         g_usr_nof_words,  -- apply throttle at generator
                         g_phy_nof_words,
                         rst,
                         clk,
                         sync_nr,
                         block_nr,
                         in_sync,
                         in_val,
                         in_dat);

  -- Verify the data path output data
  proc_dp_verify_data("rcv_dat",
                      1,  -- c_ready_latency, any value > 0 suits here
                      clk,
                      sl1,  -- verify_en
                      sl1,  -- out_ready
                      rcv_val,
                      rcv_dat,
                      prev_rcv_dat);

  -- Verify the data path output CRC (avoid error message at initialisation)
  assert NOW = 0 ps or rcv_err = '0'
    report "rcv_err = '1' indicating CRC error"
    severity ERROR;

  ------------------------------------------------------------------------------
  -- TRANSMITTER
  ------------------------------------------------------------------------------

  -- Determine the data block control signals: sof, eof, err and fsn
  u_frame_fsn : entity work.dp_frame_fsn
  generic map (
    g_fsn_w      => c_fsn_w,
    g_dat_w      => g_usr_dat_w,
    g_use_sync   => true,
    g_block_size => c_usr_block_size
  )
  port map (
    rst          => rst,
    clk          => clk,
    in_sync      => in_sync,
    in_val       => in_val,
    in_dat       => in_dat,
    out_fsn      => xmt_fsn,
    out_dat      => xmt_dat,
    out_val      => xmt_val,
    out_sof      => xmt_sof,
    out_eof      => xmt_eof,
    out_err      => xmt_err
  );

  -- Insert the fsn and err into the internal data path frame format
  u_frame : entity work.dp_frame
  generic map (
    g_fsn_w      => c_fsn_w,
    g_dat_w      => g_usr_dat_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_fsn       => xmt_fsn,
    in_dat       => xmt_dat,
    in_val       => xmt_val,
    in_sof       => xmt_sof,
    in_eof       => xmt_eof,
    in_err       => xmt_err,

    out_dat      => xmt_frm_dat,
    out_val      => xmt_frm_val,
    out_sof      => xmt_frm_sof,
    out_eof      => xmt_frm_eof
  );

  -- Pack the user data width frame data into the PHY data width frame data
  -- Note:
  -- . The proc_dp_gen_block_data appies throttling if g_usr_nof_words <
  --   g_phy_nof_words, so xmt_frm_val has a sufficiently slow pace.
  u_frame_pack : entity work.dp_frame_repack
  generic map (
    g_in_dat_w      => g_usr_dat_w,
    g_in_nof_words  => g_usr_nof_words,
    g_out_dat_w     => g_phy_dat_w,
    g_out_nof_words => g_phy_nof_words
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => xmt_frm_dat,
    in_val       => xmt_frm_val,
    in_sof       => xmt_frm_sof,
    in_eof       => xmt_frm_eof,

    out_dat      => xmt_pack_dat,
    out_val      => xmt_pack_val,
    out_sof      => xmt_pack_sof,
    out_eof      => xmt_pack_eof
  );

  -- Transmit the internal data path frame as a dp PHY frame with SFD and a true CRC
  u_frame_tx : entity work.dp_frame_tx
  generic map (
    g_sfd        => c_dp_sfd,
    g_dat_w      => g_phy_dat_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => xmt_pack_dat,
    in_val       => xmt_pack_val,
    in_sof       => xmt_pack_sof,
    in_eof       => xmt_pack_eof,

    out_dat      => xmt_pace_dat,
    out_val      => xmt_pace_val(0),
    out_sof      => xmt_pace_sof(0),
    out_eof      => xmt_pace_eof(0)
  );

  ------------------------------------------------------------------------------
  -- PHY LINK INTERFACE
  ------------------------------------------------------------------------------

  -- Model the transceiver link
  u_transceiver_link : entity work.dp_phy_link
  generic map (
    g_latency        => c_phy_link_latency,
    g_valid_support  => c_phy_link_valid_support
  )
  port map (
    in_dat       => phy_tx_dat,
    in_val       => phy_tx_val,

    out_dat      => phy_rx_dat,
    out_val      => phy_rx_val
  );

  -- Receive the dp PHY frame with SFD and a true CRC and extract the internal data path frame
  u_frame_rx : entity work.dp_frame_rx
  generic map (
    g_sfd        => c_dp_sfd,
    g_dat_w      => g_phy_dat_w,
    g_block_size => c_phy_block_size
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => phy_rx_dat,
    in_val       => phy_rx_val,

    out_dat      => rcv_pack_dat,
    out_val      => rcv_pack_val,
    out_sof      => rcv_pack_sof,
    out_eof      => rcv_pack_eof
  );

  -- Transceiver link interface control
  gen_valid_support : if c_phy_link_valid_support = true generate
    -- The u_transceiver_link will pass on the phy_tx_val to phy_rx_val
    -- ==> it is allowed to send a frame with invalid gaps
    phy_tx_dat <= xmt_pace_dat;
    phy_tx_val <= xmt_pace_val(0);

    -- ==> the rcv_pack is already appriopriately throttled by the valid rate of xmt_pack
    rcv_throttle_dat <= rcv_pack_dat;
    rcv_throttle_val <= rcv_pack_val;
    rcv_throttle_sof <= rcv_pack_sof;
    rcv_throttle_eof <= rcv_pack_eof;
  end generate;  -- gen_valid_support

  no_valid_support : if c_phy_link_valid_support = false generate
    -- The u_transceiver_link will not pass on the phy_tx_val to phy_rx_val.
    -- This has the following implications:
    -- 1) The xmt_pace frames need to be compacted without invalid dat between
    --    sof and eof. This is done by u_pace using a FIFO in dp_fifo_fill. The
    --    FIFO is filled sufficiently to ensure that it will not run empty
    --    before the eof is read.
    -- 2) The rcv_pack frames are also compacted, i.e. with no invalid between
    --    sof and eof. If then g_usr_nof_words > g_phy_nof_words then the
    --    unpack function needs a throttled input, because otherwise it does
    --    not have time to unpack the phy input words into more usr output
    --    words. This throttling is done by u_rcv_throttle that uses u_rcv_fifo
    --    to buffer the rcv_pack.
    --    Note that for c_phy_link_valid_support=TRUE there is no need to
    --    throttle the rcv_pack, because the xmt_pack will already have the
    --    appropriate rate and it will maintain it, because the valid then gets
    --    passed on via the transceiver link.

    -- ==> 1) Use the FIFO in dp_fifo_fill to avoid gaps during a frame
    -- Note:
    -- . The proc_dp_gen_block_data appies throttling if g_usr_nof_words <
    --   g_phy_nof_words, so xmt_frm_val has exactly the rigth pace so that
    --   phy_tx_val remain active during a frame.
    no_xmt_pacer : if g_usr_nof_words <= g_phy_nof_words generate
      phy_tx_dat <= xmt_pace_dat;
      phy_tx_val <= xmt_pace_val(0);
    end generate;

    gen_xmt_pacer : if g_usr_nof_words > g_phy_nof_words generate
      -- map sl, slv to record
      xmt_pace_sosi.data(g_phy_dat_w - 1 downto 0) <= xmt_pace_dat;  -- SOSI
      xmt_pace_sosi.valid                        <= xmt_pace_val(0);
      xmt_pace_sosi.sop                          <= xmt_pace_sof(0);
      xmt_pace_sosi.eop                          <= xmt_pace_eof(0);

      phy_tx_dat <= phy_tx_sosi.data(g_phy_dat_w - 1 downto 0);  -- SOSI
      phy_tx_val <= phy_tx_sosi.valid;

      u_fifo_fill : entity work.dp_fifo_fill
      generic map (
        g_data_w      => g_phy_dat_w,
        g_empty_w     => 1,
        g_channel_w   => 1,
        g_error_w     => 1,
        g_use_empty   => false,
        g_use_channel => false,
        g_use_error   => false,
        g_fifo_fill   => c_fifo_fill,
        g_fifo_size   => c_fifo_size,
        g_fifo_rl     => 1
      )
      port map (
        rst         => rst,
        clk         => clk,
        snk_out     => OPEN,  -- OUT = request to upstream ST source
        snk_in      => xmt_pace_sosi,
        src_in      => c_dp_siso_rdy,  -- IN  = request from downstream ST sink
        src_out     => phy_tx_sosi
      );
    end generate;

    -- ==> 2) Use FIFO and dp_frame_rd to throttle the output frame for unpack if necessary
    no_rcv_throttle : if g_usr_nof_words <= g_phy_nof_words generate
      rcv_throttle_dat <= rcv_pack_dat;
      rcv_throttle_val <= rcv_pack_val;
      rcv_throttle_sof <= rcv_pack_sof;
      rcv_throttle_eof <= rcv_pack_eof;
    end generate;  -- no_rcv_throttle

    gen_rcv_throttle : if g_usr_nof_words > g_phy_nof_words generate
      -- map sl, slv to record
      rcv_pack_sosi.data(g_phy_dat_w - 1 downto 0) <= rcv_pack_dat;  -- SOSI
      rcv_pack_sosi.valid                        <= rcv_pack_val;
      rcv_pack_sosi.sop                          <= rcv_pack_sof;
      rcv_pack_sosi.eop                          <= rcv_pack_eof;

      rcv_fifo_rd_siso.ready <= rcv_fifo_rd_req;  -- SISO
      rcv_fifo_rd_dat <= rcv_fifo_rd_sosi.data(g_phy_dat_w - 1 downto 0);  -- SOSI
      rcv_fifo_rd_val <= rcv_fifo_rd_sosi.valid;
      rcv_fifo_rd_sof <= rcv_fifo_rd_sosi.sop;
      rcv_fifo_rd_eof <= rcv_fifo_rd_sosi.eop;

      dut : entity work.dp_fifo_sc
      generic map (
        g_data_w      => g_phy_dat_w,
        g_empty_w     => 1,
        g_channel_w   => 1,
        g_error_w     => 1,
        g_use_empty   => false,
        g_use_channel => false,
        g_use_error   => false,
        g_use_ctrl    => true,
        g_fifo_size   => c_rcv_fifo_nof_words,
        g_fifo_rl     => 1
      )
      port map (
        rst         => rst,
        clk         => clk,
        snk_out     => OPEN,  -- OUT = request to upstream ST source
        snk_in      => rcv_pack_sosi,
        usedw       => OPEN,
        src_in      => rcv_fifo_rd_siso,  -- IN  = request from downstream ST sink
        src_out     => rcv_fifo_rd_sosi
      );

      u_rcv_throttle : entity work.dp_frame_rd
      generic map (
        g_dat_w          => g_phy_dat_w,
        g_throttle_num   => g_phy_nof_words,
        g_throttle_den   => g_usr_nof_words,
        g_throttle_sof   => false,
        g_throttle_eof   => false
      )
      port map (
        rst              => rst,
        clk              => clk,

        frm_req          => '1',
        frm_flush        => '0',
        frm_ack          => OPEN,
        frm_busy         => OPEN,
        frm_err          => OPEN,
        frm_done         => OPEN,

        rd_req           => rcv_fifo_rd_req,
        rd_dat           => rcv_fifo_rd_dat,
        rd_val           => rcv_fifo_rd_val,
        rd_sof           => rcv_fifo_rd_sof,
        rd_eof           => rcv_fifo_rd_eof,

        out_dat          => rcv_throttle_dat,
        out_val          => rcv_throttle_val,
        out_sof          => rcv_throttle_sof,
        out_eof          => rcv_throttle_eof
      );
    end generate;  -- gen_rcv_throttle
  end generate;  -- no_valid_support

  ------------------------------------------------------------------------------
  -- RECEIVER
  ------------------------------------------------------------------------------

  -- Unpack the PHY data width frame data into the user data width frame data
  u_frame_unpack : entity work.dp_frame_repack
  generic map (
    g_in_dat_w      => g_phy_dat_w,
    g_in_nof_words  => g_phy_nof_words,
    g_out_dat_w     => g_usr_dat_w,
    g_out_nof_words => g_usr_nof_words
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => rcv_throttle_dat,
    in_val       => rcv_throttle_val,
    in_sof       => rcv_throttle_sof,
    in_eof       => rcv_throttle_eof,

    out_dat      => rcv_frm_dat,
    out_val      => rcv_frm_val,
    out_sof      => rcv_frm_sof,
    out_eof      => rcv_frm_eof
  );

  -- Extract the dat, fsn and err from the internal data path frame format
  u_unframe : entity work.dp_unframe
  generic map (
    g_fsn_w      => c_fsn_w,
    g_dat_w      => g_usr_dat_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => rcv_frm_dat,
    in_val       => rcv_frm_val,
    in_sof       => rcv_frm_sof,
    in_eof       => rcv_frm_eof,

    out_fsn      => rcv_fsn,
    out_sync     => rcv_sync,
    out_dat      => rcv_dat,
    out_val      => rcv_val,
    out_sof      => rcv_sof,
    out_eof      => rcv_eof,
    out_err      => rcv_err
  );
end tb;
