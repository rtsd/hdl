library IEEE;
use IEEE.std_logic_1164.all;

entity pfs_top is
  generic (
    g_nof_bands         : natural := 1024;
    g_nof_taps          : natural := 16 * 1024;
    g_in_dat_w          : natural := 12;
    g_out_dat_w         : natural := 18;
    g_coef_dat_w        : natural := 16
  );
  port (
    in_dat_x            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_y            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val              : in  std_logic;
    in_sync             : in  std_logic;
    out_dat_x           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_dat_y           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val             : out std_logic;
    out_sync            : out std_logic;
    clk                 : in  std_logic;
    rst                 : in  std_logic
  );
end pfs_top;
