#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

set IPMODEL "SIM";
#set IPMODEL "PHY";

if {$IPMODEL=="PHY"} { 
    # OUTDATED AND NOT USED!!
    # This file is based on Qsys-generated file msim_setup.tcl.
    set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_ddio_in_1/sim"
        
    #vlib ./work/         ;# Assume library work already exists
    vmap ip_arria10_ddio_in_1_altera_gpio_core_191  ./work/
    vmap ip_arria10_ddio_in_1_altera_gpio_1930       ./work/
    
    vlog -sv "$IP_DIR/../altera_gpio_core_191/sim/mentor/altera_gpio.sv"                        -work ip_arria10_ddio_in_1_altera_gpio_core_191
    
    vcom     "$IP_DIR/../altera_gpio_1930/sim/ip_arria10_ddio_in_1_altera_gpio_1930_aac2boi.vhd"  -work ip_arria10_ddio_in_1_altera_gpio_1930     
    vcom     "$IP_DIR/ip_arria10_ddio_in_1.vhd"                                                                                               


    set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_ddio_out_1/sim"

    #vlib ./work/         ;# Assume library work already exists
    vmap ip_arria10_ddio_out_1_altera_gpio_core_191 ./work/
    vmap ip_arria10_ddio_out_1_altera_gpio_1930      ./work/    
    
    vlog -sv "$IP_DIR/../altera_gpio_core_191/sim/mentor/altera_gpio.sv"                        -work ip_arria10_ddio_out_1_altera_gpio_core_191
    
    vcom     "$IP_DIR/../altera_gpio_1930/sim/ip_arria10_ddio_out_1_altera_gpio_1930_qdroiey.vhd" -work ip_arria10_ddio_out_1_altera_gpio_1930     
    vcom     "$IP_DIR/ip_arria10_ddio_out_1.vhd"                                                                                                    

} else {

    # This file uses a behavioral model because the PHY model does not compile OK, see README.txt.
    set SIM_DIR "$env(HDL_WORK)/libraries/technology/ip_arria10_e2sg/ddio/sim/"
    
    vcom "$SIM_DIR/ip_arria10_e2sg_ddio_in_1.vhd"
    vcom "$SIM_DIR/ip_arria10_e2sg_ddio_out_1.vhd"
    vcom "$SIM_DIR/tb_ip_arria10_e2sg_ddio_1.vhd"
    
}
