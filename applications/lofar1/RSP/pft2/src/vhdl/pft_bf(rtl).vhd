library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library common_lib;
use common_lib.all;

architecture rtl of pft_bf is
  constant c_read_pipeline  : natural := 1;
  constant c_add_pipeline   : natural := 2;
  constant c_write_pipeline : natural := 1;
  constant c_pipeline       : natural := c_read_pipeline + c_add_pipeline + c_write_pipeline;
  constant c_cnt_w          : natural := g_index + 2;
  constant c_regbank_size_w : natural := g_index;
  constant c_regbank_size   : natural := 2**c_regbank_size_w;
  constant c_dat_w          : natural := g_in_dat_w + 1;

  type sig_rec is record
    val     : std_logic;
    sync    : std_logic;
    s0      : std_logic;
    s1      : std_logic;
    wr_req  : std_logic;
  end record;

  type sig_array is array (c_pipeline-1 downto 0) of sig_rec;

  subtype fifo_type is std_logic_vector(2 * c_dat_w - 1 downto 0);
  type fifo_arr is array(c_regbank_size - c_pipeline downto 0) of fifo_type;

  signal fifo_dat     : fifo_arr;
  signal nxt_fifo_dat : fifo_arr;

  signal sig : sig_array;
  signal nxt_sig : sig_array;

  signal init         : std_logic;
  signal nxt_init     : std_logic;

  signal cnt          : std_logic_vector(c_cnt_w - 1 downto 0);
  signal nxt_cnt      : std_logic_vector(cnt'range);

  signal s0           : std_logic;
  signal s1           : std_logic;
  signal sync         : std_logic;
  signal nxt_sync     : std_logic;

  signal add_ar       : std_logic_vector(c_dat_w - 1 downto 0);
  signal add_ai       : std_logic_vector(c_dat_w - 1 downto 0);
  signal add_br       : std_logic_vector(c_dat_w - 1 downto 0);
  signal add_bi       : std_logic_vector(c_dat_w - 1 downto 0);
  signal add_cr       : std_logic_vector(c_dat_w - 1 downto 0);
  signal add_ci       : std_logic_vector(c_dat_w - 1 downto 0);

  signal sub_ar       : std_logic_vector(c_dat_w - 1 downto 0);
  signal sub_ai       : std_logic_vector(c_dat_w - 1 downto 0);
  signal sub_br       : std_logic_vector(c_dat_w - 1 downto 0);
  signal sub_bi       : std_logic_vector(c_dat_w - 1 downto 0);
  signal sub_cr       : std_logic_vector(c_dat_w - 1 downto 0);
  signal sub_ci       : std_logic_vector(c_dat_w - 1 downto 0);

  signal nxt_out_re   : std_logic_vector(out_re'range);
  signal nxt_out_im   : std_logic_vector(out_re'range);

  signal reg_re       : std_logic_vector(c_dat_w - 1 downto 0);
  signal nxt_reg_re   : std_logic_vector(c_dat_w - 1 downto 0);

  signal reg_im       : std_logic_vector(c_dat_w - 1 downto 0);
  signal nxt_reg_im   : std_logic_vector(c_dat_w - 1 downto 0);

  signal rd_re        : std_logic_vector(c_dat_w - 1 downto 0);
  signal rd_im        : std_logic_vector(c_dat_w - 1 downto 0);
  signal rd_req       : std_logic;

  signal nxt_wr_re    : std_logic_vector(c_dat_w - 1 downto 0);
  signal nxt_wr_im    : std_logic_vector(c_dat_w - 1 downto 0);

  signal wr_req       : std_logic;

  signal wr_dat       : std_logic_vector(2 * c_dat_w - 1 downto 0);
  signal nxt_wr_dat   : std_logic_vector(2 * c_dat_w - 1 downto 0);
  signal rd_dat       : std_logic_vector(2 * c_dat_w - 1 downto 0);
begin
  out_val      <= sig(0).val;
  out_sync     <= sig(0).sync;
  wr_req       <= sig(0).wr_req;

  nxt_wr_dat   <= nxt_wr_re & nxt_wr_im;
  --wr_re        <= wr_dat(wr_dat'HIGH DOWNTO wr_im'LENGTH);
  --wr_im        <= wr_dat(wr_im'RANGE);

  s0  <= cnt(cnt'high - 1);
  s1  <= cnt(cnt'high  ) when g_bf_name = "bf2" else '0';

  registers : process (clk, rst)
  begin
    if rst = '1' then
      init     <= '0';
      cnt      <= (others => '0');
      sig      <= (others => (others => '0'));
      sync     <= '0';
      out_re   <= (others => '0');
      out_im   <= (others => '0');
      wr_dat   <= (others => '0');
      reg_re   <= (others => '0');
      reg_im   <= (others => '0');
    elsif rising_edge(clk) then
      init     <= nxt_init;
      cnt      <= nxt_cnt;
      sig      <= nxt_sig;
      sync     <= nxt_sync;
      out_re   <= nxt_out_re;
      out_im   <= nxt_out_im;
      wr_dat   <= nxt_wr_dat;
      reg_re   <= nxt_reg_re;
      reg_im   <= nxt_reg_im;
    end if;
  end process;

  sync_proc : process(cnt, in_sync, sync)
  begin
    nxt_sync               <= sync;
    nxt_sig(sig'high).sync <= '0';
    if in_sync = '1' or unsigned(cnt) = c_regbank_size-1 then
      nxt_sync               <= in_sync;
      nxt_sig(sig'high).sync <= sync;
    end if;
  end process;

  cnt_proc : process (cnt, in_val, in_sync)
  begin
    nxt_cnt <= cnt;
    if in_sync = '1' or signed(cnt) = -1 then
      nxt_cnt  <= (others => '0');
    elsif in_val = '1' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;

  init_proc : process(cnt, init, in_val)
  begin
    nxt_init <= init;
    if unsigned(cnt) = c_regbank_size-1 and in_val = '1' then
      nxt_init <= '1';
    end if;
  end process;

  rd_proc : process(rd_dat, sig)
  begin
    rd_re <= rd_dat(rd_dat'high downto rd_im'length);
    rd_im <= rd_dat(rd_im'range);
    --synthesis translate off
    if sig(sig'high).val = '0' then
      rd_re <= (others => '0');
      rd_im <= (others => '0');
    end if;
    --synthesis translate on
  end process;

  rd_req       <= in_val and init;

  nxt_reg_re <= std_logic_vector(resize(signed(in_re), c_dat_w));
  nxt_reg_im <= std_logic_vector(resize(signed(in_im), c_dat_w));

  nxt_sig(sig'high).val    <= rd_req;
  nxt_sig(sig'high).s0     <= s0;
  nxt_sig(sig'high).s1     <= s1;
  nxt_sig(sig'high).wr_req <= in_val;

  nxt_sig(c_pipeline-2 downto 0) <= sig(c_pipeline-1 downto 1);

  in_proc : process (sig, reg_re, reg_im, rd_re, rd_im)
  begin
    add_ar <= rd_re;
    add_ai <= rd_im;
    sub_ar <= reg_re;
    sub_ai <= reg_im;
    add_br <= (others => '0');
    add_bi <= (others => '0');
    sub_br <= (others => '0');
    sub_bi <= (others => '0');
    if sig(sig'high).s0 = '1' then
      sub_ar <= rd_re;
      sub_ai <= rd_im;
      add_br <= reg_re;
      add_bi <= reg_im;
      sub_br <= reg_re;
      sub_bi <= reg_im;
      if sig(sig'high).s1 = '1' then
        add_br <= reg_im;
        add_bi <= reg_re;
        sub_br <= reg_im;
        sub_bi <= reg_re;
      end if;
    end if;
  end process;

  out_proc : process (sig, add_cr, add_ci, sub_cr, sub_ci)
  begin
    nxt_out_re  <= std_logic_vector(resize(signed(add_cr), g_out_dat_w));
    nxt_out_im  <= std_logic_vector(resize(signed(add_ci), g_out_dat_w));
    nxt_wr_re   <= sub_cr;
    nxt_wr_im   <= sub_ci;
    if sig(1).s0 = '1' and sig(1).s1 = '1' then
      nxt_out_re  <= std_logic_vector(resize(signed(add_cr), g_out_dat_w));
      nxt_out_im  <= std_logic_vector(resize(signed(sub_ci), g_out_dat_w));
      nxt_wr_re   <= sub_cr;
      nxt_wr_im   <= add_ci;
    end if;
  end process;

-- Adds/ Subs ------------------------------------------------------------------

--  Intel Altera lmp_add_sub carry in:
--  ADD: out = a + b + cin      => cin = '0' to have out = a + b
--  SUB: out = a - b + cin - 1  => cin = '1' to have out = a - b

  --cadd : ENTITY common_lib.common_caddsub
  --GENERIC MAP (
  --  g_in_a_w   => c_dat_w,
  --  g_in_b_w   => c_dat_w,
  --  g_out_c_w  => c_dat_w,
  --  g_pipeline => c_add_pipeline,
  --  g_add_sub  => "ADD"
  --)
  --PORT MAP (
  --  in_ar  => add_ar,
  --  in_ai  => add_ai,
  --  in_br  => add_br,
  --  in_bi  => add_bi,
  --  in_cr  => '0',
  --  in_ci  => '0',
  --  out_cr => add_cr,
  --  out_ci => add_ci,
  --  clk    => clk,
  --  rst    => rst
  --);

  cadd : entity common_complex_add_sub
  generic map (
    g_direction       => "ADD",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_pipeline,  -- >= 0
    g_in_dat_w        => c_dat_w,
    g_out_dat_w       => c_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    in_ar    => add_ar,
    in_ai    => add_ai,
    in_br    => add_br,
    in_bi    => add_bi,
    out_re   => add_cr,
    out_im   => add_ci
  );

--  csub : ENTITY common_lib.common_caddsub
--  GENERIC MAP (
--    g_in_a_w   => c_dat_w,
--    g_in_b_w   => c_dat_w,
--    g_out_c_w  => c_dat_w,
--    g_pipeline => c_add_pipeline,
--    g_add_sub  => "SUB"
--  )
--  PORT MAP (
--    in_ar  => sub_ar,
--    in_ai  => sub_ai,
--    in_br  => sub_br,
--    in_bi  => sub_bi,
--    in_cr  => '1',
--    in_ci  => '1',
--    out_cr => sub_cr,
--    out_ci => sub_ci,
--    clk    => clk,
--    rst    => rst
--  );

  csub : entity common_complex_add_sub
  generic map (
    g_direction       => "SUB",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_pipeline,  -- >= 0
    g_in_dat_w        => c_dat_w,
    g_out_dat_w       => c_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    in_ar    => sub_ar,
    in_ai    => sub_ai,
    in_br    => sub_br,
    in_bi    => sub_bi,
    out_re   => sub_cr,
    out_im   => sub_ci
  );

-- regbank --------------------------------------------------------------------------
  fifo_gen: if c_regbank_size > 8 generate
  fifo : entity common_lib.common_fifo_sc
  generic map (
    g_dat_w     => wr_dat'LENGTH,
    g_nof_words => c_regbank_size
  )
  port map (
    wr_dat => wr_dat,
    wr_req => wr_req,
    rd_dat => rd_dat,
    rd_req => rd_req,
    clk    => clk,
    rst    => rst
  );
  end generate fifo_gen;

  fifo2_gen : if c_regbank_size > c_pipeline and c_regbank_size <= 8 generate
  fifo2_reg : process (clk, rst)
  begin
    if rst = '1' then
      fifo_dat <= (others => (others => '0'));
    elsif rising_edge(clk) then
      fifo_dat <= nxt_fifo_dat;
    end if;
  end process;

  fifo2_proc : process(fifo_dat, wr_req, wr_dat)
  begin
    nxt_fifo_dat <= fifo_dat;
    if wr_req = '1' then
      nxt_fifo_dat <= wr_dat & fifo_dat(fifo_dat'high downto 1);
    end if;
    rd_dat <= fifo_dat(0);
  end process;
  end generate;

  fifo3_gen : if c_regbank_size = c_pipeline generate
  fifo3_reg : process (clk, rst)
  begin
    if rst = '1' then
      fifo_dat <= (others => (others => '0'));
    elsif rising_edge(clk) then
      fifo_dat <= nxt_fifo_dat;
    end if;
  end process;

  fifo3_proc : process(fifo_dat, wr_req, wr_dat)
  begin
    nxt_fifo_dat <= fifo_dat;
    if wr_req = '1' then
      nxt_fifo_dat(0) <= wr_dat;
    end if;
    rd_dat <= fifo_dat(0);
  end process;
  end generate;

  assert c_regbank_size >= c_pipeline
    severity FAILURE;
end rtl;
