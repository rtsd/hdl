-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tech_eth_10g the 10G Ethernet IP technology wrapper.
-- Description:
--   The tb is self checking based on:
--   . proc_tech_mac_10g_rx_packet() for expected header and data type
--   . tx_pkt_cnt=rx_pkt_cnt > 0 must be true at the tb_end.
-- Usage:
--   > as 16
--   > run -all

library IEEE, technology_lib, tech_pll_lib, tech_mac_10g_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use tech_mac_10g_lib.tb_tech_mac_10g_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_tech_eth_10g is
  -- Test bench control parameters
  generic (
    g_technology              : natural := c_tech_select_default;
    g_tb_end                  : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    g_no_dut                  : boolean := false;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
    g_sim_level               : natural := 1;  -- 0 = use IP; 1 = use fast serdes model
    g_ref_clk_644_period      : time := tech_pll_clk_644_period;  -- for 10GBASE-R
    g_ref_clk_156_period      : time := 6.4 ns;  -- for XAUI
    g_data_type               : natural := c_tb_tech_mac_10g_data_type_symbols;
    g_verify_link_recovery    : boolean := true;
    g_use_serial_rx_in        : boolean := false  -- default FALSE when this tb is ran standalone, else use TRUE to simulate a link between two instances of this tb
  );
  port (
    tb_end          : out std_logic;
    -- PHY 10gbase_r
    serial_tx_out   : out std_logic;  -- 1 lane
    serial_rx_in    : in  std_logic := 'X';
    -- PHY XAUI
    xaui_tx_out     : out std_logic_vector(c_nof_xaui_lanes - 1 downto 0);  -- 4 lanes
    xaui_rx_in      : in  std_logic_vector(c_nof_xaui_lanes - 1 downto 0) := (others => 'X')
  );
end tb_tech_eth_10g;

architecture tb of tb_tech_eth_10g is
  constant c_sim                : boolean := true;  -- tr_xaui_align_dly has time delay ~ 1 sec, so not suitable for simulation
  constant c_nof_channels       : natural := 1;  -- fixed in this tb

  constant cal_clk_period       : time := 25 ns;  -- 40 MHz

  constant phy_delay            : time :=  0 ns;
  constant c_phy_loopback       : boolean := not g_use_serial_rx_in;

  constant c_pkt_length_arr1    : t_nat_natural_arr := array_init(0, 50, 1) & (1472, 1473) & 9000;  -- frame longer than 1518-46 = 1472 is received with rx_sosi.err = 8
                                                                                                    -- jumbo frame is 9018-46 = 8972
  constant c_pkt_length_arr2    : t_nat_natural_arr := array_init(46, 10, 139) & 1472;
  constant c_pkt_length_arr     : t_nat_natural_arr := c_pkt_length_arr1 & c_pkt_length_arr2;
  constant c_nof_pkt1           : natural := c_pkt_length_arr1'length;
  constant c_nof_pkt2           : natural := c_pkt_length_arr2'length;
  constant c_nof_pkt            : natural := sel_a_b(g_verify_link_recovery, c_nof_pkt1 + c_nof_pkt2, c_nof_pkt1);

  constant c_dst_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";
  constant c_src_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC";  -- = 12-34-56-78-9A-BC
  constant c_src_mac_tx         : std_logic_vector(c_network_eth_mac_slv'range) := c_src_mac;
  --CONSTANT c_src_mac_tx         : STD_LOGIC_VECTOR(c_network_eth_mac_slv'RANGE) := X"100056789ABC";  -- = 10-00-56-78-9A-BC
  constant c_ethertype          : std_logic_vector(c_network_eth_type_slv'range) := X"10FA";
  constant c_etherlen           : std_logic_vector(c_network_eth_type_slv'range) := "0000000000010000";

  -- Packet headers
  constant c_eth_header_ethertype : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_ethertype);
  constant c_eth_header_etherlen  : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_etherlen);

  signal total_header      : t_network_total_header := c_network_total_header_ones;  -- default fill all fields with value 1

  -- Clocks and reset
  signal tx_end            : std_logic := '0';
  signal rx_end            : std_logic := '0';
  signal cal_clk           : std_logic := '1';  -- calibration clock
  signal mm_clk            : std_logic := '0';  -- memory-mapped bus clock
  signal mm_rst            : std_logic;  -- reset synchronous with mm_clk

  -- External reference clocks
  signal tr_ref_clk_644    : std_logic := '1';  -- 10GBASE-R
  signal tr_ref_clk_312    : std_logic;  -- 10GBASE-R
  signal tr_ref_clk_156    : std_logic := '1';  -- 10GBASE-R or XAUI
  signal tr_ref_rst_156    : std_logic;  -- 10GBASE-R or XAUI

  -- XAUI clocks
  signal tx_rst_arr_out    : std_logic_vector(0 downto 0);
  signal rx_clk_arr_out    : std_logic_vector(0 downto 0);
  signal rx_rst_arr_out    : std_logic_vector(0 downto 0);

  -- . Test bench top level clocks
  signal tb_ref_clk_644    : std_logic;
  signal tb_ref_clk_312    : std_logic;
  signal tb_ref_clk_156    : std_logic;
  signal tb_ref_rst_156    : std_logic;
  signal tb_tx_clk         : std_logic;
  signal tb_tx_rst         : std_logic;
  signal tb_rx_clk         : std_logic;
  signal tb_rx_rst         : std_logic;

  -- 10G MAC control interface
  signal mm_init           : std_logic := '1';
  signal mac_mosi          : t_mem_mosi;
  signal mac_mosi_wrdata   : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit;
  signal mac_miso          : t_mem_miso;
  signal mac_miso_rdval    : std_logic;
  signal mac_miso_rddata   : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit;

  -- 10G MAC transmit interface
  -- . The tb is the ST source
  signal tx_siso           : t_dp_siso;
  signal tx_sosi           : t_dp_sosi;
  signal tx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit

  -- 10G MAC receive interface
  -- . The tb is the ST sink
  signal rx_siso           : t_dp_siso;
  signal rx_sosi           : t_dp_sosi;
  signal rx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit

  -- PHY serial IO
  -- . 10GBASE-R (single lane)
  signal serial_tx_arr     : std_logic_vector(0 downto 0);
  signal serial_rx_arr     : std_logic_vector(0 downto 0);

  -- . XAUI (four lanes)
  signal xaui_tx_arr       : t_xaui_arr(0 downto 0);
  signal xaui_rx_arr       : t_xaui_arr(0 downto 0);

  -- Model a serial link fault
  signal link_fault        : std_logic;

  -- Verification
  signal tx_pkt_cnt        : natural := 0;
  signal rx_pkt_cnt        : natural := 0;
  signal rx_toggle         : std_logic;  -- toggle after every received packet
begin
  cal_clk <= not cal_clk after cal_clk_period / 2;  -- Calibration clock

  -- debug signals to ease monitoring in wave window
  mac_mosi_wrdata <= mac_mosi.wrdata(c_word_w - 1 downto 0);
  mac_miso_rddata <= mac_miso.rddata(c_word_w - 1 downto 0);
  mac_miso_rdval <= '1' when mac_mosi.rd = '1' and mac_miso.waitrequest = '0' else '0';  -- c_rd_latency = 1

  tx_sosi_data <= tx_sosi.data(c_tech_mac_10g_data_w - 1 downto 0);
  rx_sosi_data <= rx_sosi.data(c_tech_mac_10g_data_w - 1 downto 0);

  -- Use signal to leave unused fields 'X'
  total_header.eth <= c_eth_header_ethertype;

  u_mm_setup : entity tech_mac_10g_lib.tb_tech_mac_10g_setup
  generic map (
    g_technology    => g_technology,
    g_src_mac       => c_src_mac
  )
  port map (
    tb_end    => rx_end,
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,
    mm_init   => mm_init,
    mac_mosi  => mac_mosi,
    mac_miso  => mac_miso
  );

  -- Packet transmitter
  u_transmitter : entity tech_mac_10g_lib.tb_tech_mac_10g_transmitter
  generic map (
    g_data_type            => g_data_type,
    g_pkt_length_arr1      => c_pkt_length_arr1,
    g_pkt_length_arr2      => c_pkt_length_arr2,
    g_verify_link_recovery => g_verify_link_recovery
  )
  port map (
    mm_init        => mm_init,
    total_header   => total_header,
    tx_clk         => tb_tx_clk,
    tx_siso        => tx_siso,
    tx_sosi        => tx_sosi,
    link_fault     => link_fault,
    tx_end         => tx_end
  );

  -- Generate reference clocks
  gen_ref_clocks_xaui : if g_technology = c_tech_stratixiv generate
    tr_ref_clk_644 <= 'X';
    tr_ref_clk_312 <= 'X';
    tr_ref_clk_156 <= not tr_ref_clk_156 after g_ref_clk_156_period / 2;
    tr_ref_rst_156 <= '1', '0' after g_ref_clk_156_period * 5;
  end generate;

  gen_ref_clocks_10gbase_r : if g_technology /= c_tech_stratixiv generate
    tr_ref_clk_644 <= not tr_ref_clk_644 after g_ref_clk_644_period / 2;

    pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    generic map (
      g_technology => g_technology
    )
    port map (
      refclk_644 => tr_ref_clk_644,
      rst_in     => mm_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );
  end generate;

  -- Apply the clocks from top level down such that they have their rising_edge() aligned without any delta-delay
  u_tech_eth_10g_clocks : entity work.tech_eth_10g_clocks
  generic map (
    g_technology     => g_technology,
    g_nof_channels   => c_nof_channels
  )
  port map (
    -- Input clocks
    -- . Reference
    tr_ref_clk_644    => tr_ref_clk_644,
    tr_ref_clk_312    => tr_ref_clk_312,
    tr_ref_clk_156    => tr_ref_clk_156,
    tr_ref_rst_156    => tr_ref_rst_156,

    -- . XAUI
    tx_rst_arr        => tx_rst_arr_out,
    rx_clk_arr        => rx_clk_arr_out,
    rx_rst_arr        => rx_rst_arr_out,

    -- Output clocks
    -- . Reference
    eth_ref_clk_644   => tb_ref_clk_644,
    eth_ref_clk_312   => tb_ref_clk_312,
    eth_ref_clk_156   => tb_ref_clk_156,
    eth_ref_rst_156   => tb_ref_rst_156,

    -- . Data
    eth_tx_clk_arr(0) => tb_tx_clk,
    eth_tx_rst_arr(0) => tb_tx_rst,

    eth_rx_clk_arr(0) => tb_rx_clk,
    eth_rx_rst_arr(0) => tb_rx_rst
  );

  no_dut : if g_no_dut = true generate
    tx_rst_arr_out <= (others => tr_ref_rst_156);
    rx_clk_arr_out <= (others => tr_ref_clk_156);
    rx_rst_arr_out <= (others => tr_ref_rst_156);

    rx_sosi <= tx_sosi;
    tx_siso <= rx_siso;
  end generate;

  gen_dut : if g_no_dut = false generate
    dut : entity work.tech_eth_10g
    generic map (
      g_technology          => g_technology,
      g_sim                 => c_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => c_nof_channels,
      g_pre_header_padding  => true
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644    => tb_ref_clk_644,
      tr_ref_clk_312    => tb_ref_clk_312,
      tr_ref_clk_156    => tb_ref_clk_156,
      tr_ref_rst_156    => tb_ref_rst_156,

      -- Calibration & reconfig clock
      cal_rec_clk       => cal_clk,  -- for XAUI

      -- MM
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,

      mac_mosi          => mac_mosi,  -- CSR = control status register
      mac_miso          => mac_miso,

      -- XAUI clocks
      tx_clk_arr_in(0)  => tb_tx_clk,  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
      tx_rst_arr_out    => tx_rst_arr_out,
      rx_clk_arr_out    => rx_clk_arr_out,
      rx_clk_arr_in(0)  => tb_rx_clk,  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
      rx_rst_arr_out    => rx_rst_arr_out,

      -- ST
      tx_snk_in_arr(0)  => tx_sosi,  -- 64 bit data @ 156 tb_tx_clk
      tx_snk_out_arr(0) => tx_siso,

      rx_src_out_arr(0) => rx_sosi,  -- 64 bit data @ 156 tb_rx_clk
      rx_src_in_arr(0)  => rx_siso,

      -- PHY serial IO
      -- . 10GBASE-R (single lane)
      serial_tx_arr     => serial_tx_arr,
      serial_rx_arr     => serial_rx_arr,

      -- . XAUI (four lanes)
      xaui_tx_arr       => xaui_tx_arr,
      xaui_rx_arr       => xaui_rx_arr
    );
  end generate;

  u_link_connect : entity tech_mac_10g_lib.tb_tech_mac_10g_link_connect
  generic map (
    g_loopback    => c_phy_loopback,  -- default TRUE for loopback tx to rx, else use FALSE to connect tx-tx, rx-rx between two tb devices
    g_link_delay  => phy_delay
  )
  port map (
    link_fault    => link_fault,  -- when '1' then forces rx_serial_arr(0)='0'

    -- 10GBASE-R serial layer connect
    serial_tx     => serial_tx_arr(0),
    serial_rx     => serial_rx_arr(0),  -- connects to delayed tx_serial_arr(0) when g_loopback=TRUE else to rx_serial_in
    serial_tx_out => serial_tx_out,  -- connects to delayed tx_serial_arr(0)
    serial_rx_in  => serial_rx_in,  -- used when g_loopback=FALSE

    -- XAUI serial layer connect
    xaui_tx       => xaui_tx_arr(0),
    xaui_rx       => xaui_rx_arr(0),  -- connects to delayed xaui_tx_arr(0) when g_loopback=TRUE else to xaui_rx_in
    xaui_tx_out   => xaui_tx_out,  -- connects to delayed xaui_tx_arr(0)
    xaui_rx_in    => xaui_rx_in  -- used when g_loopback=FALSE
  );

  -- Packet receiver
  u_receiver : entity tech_mac_10g_lib.tb_tech_mac_10_receiver
  generic map (
    g_data_type  => g_data_type
  )
  port map (
    mm_init        => mm_init,
    total_header   => total_header,
    rx_clk         => tb_rx_clk,
    rx_sosi        => rx_sosi,
    rx_siso        => rx_siso,
    rx_toggle      => rx_toggle
  );

  -- Verification
  u_verify_rx_at_eop : entity tech_mac_10g_lib.tb_tech_mac_10_verify_rx_at_eop
  generic map (
    g_no_padding     => g_no_dut,
    g_pkt_length_arr => c_pkt_length_arr
  )
  port map (
    tx_clk      => tb_tx_clk,
    tx_sosi     => tx_sosi,
    rx_clk      => tb_rx_clk,
    rx_sosi     => rx_sosi
  );

  u_verify_rx_pkt_cnt : entity tech_mac_10g_lib.tb_tech_mac_10g_verify_rx_pkt_cnt
  generic map (
    g_nof_pkt   => c_nof_pkt
  )
  port map (
    tx_clk      => tb_tx_clk,
    tx_sosi     => tx_sosi,
    rx_clk      => tb_rx_clk,
    rx_sosi     => rx_sosi,
    tx_pkt_cnt  => tx_pkt_cnt,
    rx_pkt_cnt  => rx_pkt_cnt,
    rx_end      => rx_end
  );

  -- Stop the simulation
  u_simulation_end : entity tech_mac_10g_lib.tb_tech_mac_10g_simulation_end
  generic map (
    g_tb_end            => g_tb_end,
    g_nof_clk_to_rx_end => 1000
  )
  port map (
    clk       => tb_tx_clk,
    tx_end    => tx_end,
    rx_end    => rx_end,
    tb_end    => tb_end
  );
end tb;
