-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;
use work.tb_common_pkg.all;
use work.tb_common_mem_pkg.all;

entity tb_common_mem_mux is
 generic (
    g_nof_mosi    : positive := 16;  -- Number of memory interfaces in the array.
    g_mult_addr_w : positive := 4  -- Address width of each memory-interface element in the array.
  );
end tb_common_mem_mux;

-- Usage:
--   > as 10
--   > run -all

architecture tb of tb_common_mem_mux is
  constant clk_period   : time    := 10 ns;

  constant c_data_w     : natural := 32;
  constant c_test_ram   : t_c_mem := (latency  => 1,
                                      adr_w    => g_mult_addr_w,
                                      dat_w    => c_data_w,
                                      nof_dat  => 2**g_mult_addr_w,
                                      init_sl  => '0');
  signal rst      : std_logic;
  signal clk      : std_logic := '1';
  signal tb_end   : std_logic;

  signal mosi_arr : t_mem_mosi_arr(g_nof_mosi - 1 downto 0);
  signal miso_arr : t_mem_miso_arr(g_nof_mosi - 1 downto 0);
  signal mosi     : t_mem_mosi;
  signal miso     : t_mem_miso;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 5;

  p_stimuli : process
    variable temp : integer;
  begin
    tb_end <= '0';
    mosi   <= c_mem_mosi_rst;

    -- Write the whole memory range
    for I in 0 to g_nof_mosi - 1 loop
      for J in 0 to 2**g_mult_addr_w - 1 loop
        proc_mem_mm_bus_wr(I * 2**g_mult_addr_w + J, I + J, clk, mosi);
      end loop;
    end loop;

    -- Read back the whole range and check if data is as expected
    for I in 0 to g_nof_mosi - 1 loop
      for J in 0 to 2**g_mult_addr_w - 1 loop
        proc_mem_mm_bus_rd(I * 2**g_mult_addr_w + J, clk, mosi);
        proc_common_wait_some_cycles(clk, 1);
        temp := TO_UINT(miso.rddata(31 downto 0));
        if(temp /= I + J) then
          report "Error! Readvalue is not as expected"
            severity ERROR;
        end if;
      end loop;
    end loop;
    tb_end <= '1';
    wait;
  end process;

  generation_of_test_rams : for I in 0 to g_nof_mosi - 1 generate
    u_test_rams : entity work.common_ram_r_w
    generic map (
      g_ram       => c_test_ram,
      g_init_file => "UNUSED"
    )
    port map (
      rst       => rst,
      clk       => clk,
      clken     => '1',
      wr_en     => mosi_arr(I).wr,
      wr_adr    => mosi_arr(I).address(g_mult_addr_w - 1 downto 0),
      wr_dat    => mosi_arr(I).wrdata(c_data_w - 1 downto 0),
      rd_en     => mosi_arr(I).rd,
      rd_adr    => mosi_arr(I).address(g_mult_addr_w - 1 downto 0),
      rd_dat    => miso_arr(I).rddata(c_data_w - 1 downto 0),
      rd_val    => miso_arr(I).rdval
    );
  end generate;

  d_dut : entity work.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_mosi,
    g_mult_addr_w => g_mult_addr_w
  )
  port map (
    mosi_arr => mosi_arr,
    miso_arr => miso_arr,
    mosi     => mosi,
    miso     => miso
  );
end tb;
