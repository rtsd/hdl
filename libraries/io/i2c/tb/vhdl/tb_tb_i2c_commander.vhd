-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi-testbench for i2c_commander
-- Description:
--   Verify i2c_commander for I2C slaves on different board busses
-- Usage:
--   > as 3
--   > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_i2c_commander is
end tb_tb_i2c_commander;

architecture tb of tb_tb_i2c_commander is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_board      : STRING := "adu"  -- else default to "unb"

  u_adu : entity work.tb_i2c_commander generic map ("adu");
  u_unb : entity work.tb_i2c_commander generic map ("unb");
end tb;
