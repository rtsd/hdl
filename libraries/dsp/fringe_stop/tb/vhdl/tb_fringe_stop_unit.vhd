-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the fringe_stop_unit.
--
--
-- Usage
--   > as 8
--   > run -all
--   > Testbench is self-checking
--   > Evalute the WAVE window.

library IEEE, common_lib, technology_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_math_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;

entity tb_fringe_stop_unit is
  generic(
    g_sim_type             : natural  := 2;  -- 0 = Increment, 1 = Increment over the maximum, 2 = Decrease, 3 = Decrease over the minimum
    g_in_dat_w             : positive := 8;  -- Width of the incoming data.
    g_fs_offset_w          : positive := 10;  -- Width of the offset of the linear coefficient
    g_accu_w               : positive := 31;  -- Width of the accumulation register
    g_fs_step_w            : positive := 17;  -- Width of the step of the linear coefficient
    g_nof_channels         : positive := 4;  -- Number of serial channels for which the fringe stopping must be applied uniquely
    g_phasor_w             : positive := 9;  -- Width of the phasor values in the lookup table
    g_phi_minus_sign       : boolean := true  -- Apply exp(j*phi) or exp(-j*phi) based on phasor lookup table address phi
  );
end tb_fringe_stop_unit;

architecture tb of tb_fringe_stop_unit is
  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  --CONSTANT c_mm_clk_period      : TIME := 10 ns;
  constant c_mm_clk_period      : time := 100 ps;
  constant c_dp_clk_period      : time := 5 ns;

  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  signal tb_end                 : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi            : t_mem_mosi;
  signal reg_diag_bg_miso            : t_mem_miso;

  signal ram_diag_bg_mosi            : t_mem_mosi;
  signal ram_diag_bg_miso            : t_mem_miso;

  -- DUT
  signal ram_fringe_stop_offset_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fringe_stop_offset_miso : t_mem_miso := c_mem_miso_rst;

  signal ram_fringe_stop_step_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fringe_stop_step_miso   : t_mem_miso := c_mem_miso_rst;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := 1;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_in_dat_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(g_nof_channels);
  constant c_bg_data_file_prefix    : string   := "UNUSED";  -- "/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, c_bg_nof_output_streams, 1);

  constant c_block_size             : natural := g_nof_channels;
  constant c_bg_gapsize             : natural := 0;
  constant c_bg_nof_blocks_per_sync : natural := 128;
  constant c_bg_mem_high_addr       : natural := g_nof_channels - 1;

  constant c_bg_ctrl                : t_diag_block_gen := ( '0',  -- enable: On by default in simulation; MM enable required on hardware.
                                                            '0',  -- enable_sync
                                                TO_UVEC(            c_block_size, c_diag_bg_samples_per_packet_w),
                                                TO_UVEC(c_bg_nof_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                TO_UVEC(            c_bg_gapsize, c_diag_bg_gapsize_w),
                                                TO_UVEC(                       0, c_diag_bg_mem_low_adrs_w),
                                                TO_UVEC(      c_bg_mem_high_addr, c_diag_bg_mem_high_adrs_w),
                                                TO_UVEC(                       0, c_diag_bg_bsn_init_w));

  -- Configuration of the databuffers:
  signal bg_sosi_arr                : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(1 - 1 downto 0) := (others => c_dp_sosi_rst);

  constant c_lookup_len             : positive := 2**g_fs_offset_w;  -- corresponds to 2*pi
  constant c_lookup_real            : t_nat_integer_arr := common_math_create_look_up_table_cos(c_lookup_len, g_phasor_w, false);
  constant c_lookup_imag            : t_nat_integer_arr := common_math_create_look_up_table_sin(c_lookup_len, g_phasor_w, g_phi_minus_sign);
  constant c_nof_sync_periods       : positive := 8;

  signal bg_data_arr_re             : t_integer_arr(g_nof_channels - 1 downto 0);
  signal bg_data_arr_im             : t_integer_arr(g_nof_channels - 1 downto 0);
  signal fs_offset_matrix           : t_integer_matrix(c_nof_sync_periods - 1 downto 0, g_nof_channels - 1 downto 0);
  signal fs_step_matrix             : t_integer_matrix(c_nof_sync_periods - 1 downto 0, g_nof_channels - 1 downto 0);

  type reg_type is record
    out_sosi_arr  : t_dp_sosi_arr(1 downto 0);
    accu_reg      : t_integer_arr(g_nof_channels - 1 downto 0);
    chn_cnt       : integer;
    sop_cnt       : integer;
    sync_cnt      : integer;
    loop_cnt      : integer;
    step          : integer;
    first_chn     : boolean;
    first_sop     : boolean;
    first_sync    : boolean;
    index         : natural;
    ref_re        : integer;
    ref_im        : integer;
  end record;

  constant c_reg_type_rst : reg_type := ((others => c_dp_sosi_rst), (others => 0), 0, 0, 0, 0, 0, true, true, true, 0, 0, 0);

  signal r    : reg_type := c_reg_type_rst;
  signal rin  : reg_type := c_reg_type_rst;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

 ----------------------------------------------------------------------------
  -- Stimuli: Create the stimuli values and calculate the reference array
  ----------------------------------------------------------------------------
  p_create_stimuli : process
    variable v_accu_reg : t_integer_arr(g_nof_channels - 1 downto 0);
    variable v_step     : t_integer_matrix(c_bg_nof_blocks_per_sync - 1 downto 0, g_nof_channels - 1 downto 0);
    variable v_index    : t_integer_matrix(c_bg_nof_blocks_per_sync - 1 downto 0, g_nof_channels - 1 downto 0);
    variable v_ref_re   : t_integer_matrix(c_bg_nof_blocks_per_sync - 1 downto 0, g_nof_channels - 1 downto 0);
    variable v_ref_im   : t_integer_matrix(c_bg_nof_blocks_per_sync - 1 downto 0, g_nof_channels - 1 downto 0);
  begin
    -- Init the values for the BG.
    for J in 0 to g_nof_channels - 1 loop
      bg_data_arr_re(J) <= -1 * g_nof_channels / 2 + J;
      bg_data_arr_im(J) <= g_nof_channels / 2 - J;
    end loop;

    -- init fs_offset_matrix
    for I in 0 to c_nof_sync_periods - 1 loop
      -- Normal increment, both offset and step.
      if g_sim_type = 0 then
        for J in 0 to g_nof_channels - 1 loop
          fs_offset_matrix(I, J) <= I + 10 + J;
          fs_step_matrix(I, J) <= 2**(g_fs_step_w - 1) - 1 - g_nof_channels + J;
        end loop;
      end if;

      -- Increment where sum reaches maximum and wraps.
      if g_sim_type = 1 then
        for J in 0 to g_nof_channels - 1 loop
          fs_offset_matrix(I, J) <= 2**g_fs_offset_w - 1;
          fs_step_matrix(I, J) <= 2**(g_fs_step_w - 1) - 1 - g_nof_channels + J;
        end loop;
      end if;

      -- Decrease the step and offset
      if g_sim_type = 2 then
        for J in 0 to g_nof_channels - 1 loop
          fs_offset_matrix(I, J) <= 2**(g_fs_offset_w / 2) + J - I;
          fs_step_matrix(I, J) <= -1 * 2**(g_fs_step_w - 1) + J;
        end loop;
      end if;

      -- Decrease where sum reaches minimum and wraps
      if g_sim_type = 3 then
        for J in 0 to g_nof_channels - 1 loop
          fs_offset_matrix(I, J) <= 1;
          fs_step_matrix(I, J) <= -1 * 2**(g_fs_step_w - 1);
        end loop;
      end if;
    end loop;
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Stimuli: Block gen data send to memory
  ----------------------------------------------------------------------------
  p_mm_block_gen_stimuli : process
  begin
    ram_diag_bg_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 10);
    for I in 0 to g_nof_channels - 1 loop
      proc_mem_mm_bus_wr( I, TO_SINT(TO_SVEC(bg_data_arr_im(I), g_in_dat_w) & TO_SVEC(bg_data_arr_re(I), g_in_dat_w)), mm_clk, ram_diag_bg_mosi);
    end loop;
    proc_common_wait_some_cycles(mm_clk, 80);
    proc_mem_mm_bus_wr( 0, 1, mm_clk, reg_diag_bg_mosi);

    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Stimuli: fringe stop step values send to memory
  ----------------------------------------------------------------------------
  p_mm_fs_step_stimuli : process
  begin
    ram_fringe_stop_step_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 10);

    for K in 0 to 20 loop
      for I in 0 to c_nof_sync_periods - 1 loop
        for J in 0 to g_nof_channels - 1 loop
          proc_mem_mm_bus_wr( J, fs_step_matrix(I, J), mm_clk, ram_fringe_stop_step_mosi);
        end loop;
        -- wait until the next sync interval
        proc_common_wait_some_pulses(dp_clk, bg_sosi_arr(0).sync, 1);
        proc_common_wait_some_cycles(dp_clk, 20);  -- account for clock domain crossing latency
        proc_common_wait_some_cycles(mm_clk, 20);  -- account for clock domain crossing latency
      end loop;
    end loop;
    proc_common_wait_some_cycles(mm_clk, 80);

    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Stimuli: fringe stop offset values send to memory
  ----------------------------------------------------------------------------
  p_mm_fs_offset_stimuli : process
  begin
    ram_fringe_stop_offset_mosi <= c_mem_mosi_rst;
    proc_common_wait_some_cycles(mm_clk, 10);

    for K in 0 to 20 loop
      for I in 0 to c_nof_sync_periods - 1 loop
        for J in 0 to g_nof_channels - 1 loop
          proc_mem_mm_bus_wr( J, fs_offset_matrix(I, J), mm_clk, ram_fringe_stop_offset_mosi);
        end loop;
        -- wait until the next sync interval
        proc_common_wait_some_pulses(dp_clk, bg_sosi_arr(0).sync, 1);
        proc_common_wait_some_cycles(dp_clk, 20);  -- account for clock domain crossing latency
        proc_common_wait_some_cycles(mm_clk, 20);  -- account for clock domain crossing latency
      end loop;
    end loop;
    proc_common_wait_some_cycles(mm_clk, 80);

    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_diag_block_gen_rst => c_bg_ctrl,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.fringe_stop_unit
  generic map (
    g_technology     => c_tech_select_default,
    g_in_dat_w       => g_in_dat_w,
    g_fs_offset_w    => g_fs_offset_w,
    g_fs_step_w      => g_fs_step_w,
    g_accu_w         => g_accu_w,
    g_phi_minus_sign => g_phi_minus_sign,
    g_phasor_w       => g_phasor_w,
    g_nof_channels   => g_nof_channels
  )
  port map (
    -- System
    dp_rst                 =>  dp_rst,
    dp_clk                 =>  dp_clk,
    mm_rst                 =>  mm_rst,
    mm_clk                 =>  mm_clk,

    -- MM interface
    ram_fringe_stop_offset_mosi => ram_fringe_stop_offset_mosi,
    ram_fringe_stop_offset_miso => ram_fringe_stop_offset_miso,
    ram_fringe_stop_step_mosi   => ram_fringe_stop_step_mosi,
    ram_fringe_stop_step_miso   => ram_fringe_stop_step_miso,

    -- ST interface
    in_sosi            => bg_sosi_arr(0),
    out_sosi           => out_sosi_arr(0)
  );

  p_reference_comb : process(r, dp_rst, out_sosi_arr, fs_offset_matrix, fs_step_matrix, bg_data_arr_re, bg_data_arr_im)
    variable v : reg_type;
    variable v_sum_int : integer;
    variable v_sum_vec : std_logic_vector(c_integer_w - 1 downto 0);
  begin
    v          := r;
    v.out_sosi_arr(0) := out_sosi_arr(0);

    -- Maintain counters for sync, sop and channels.
    if out_sosi_arr(0).valid = '1' then
      if r.first_chn = true then
          v.first_chn := false;
      elsif r.chn_cnt = g_nof_channels - 1 then
        v.chn_cnt := 0;
      else
        v.chn_cnt := r.chn_cnt + 1;
      end if;

      if out_sosi_arr(0).sop = '1' then
        if r.first_sop = true then
          v.first_sop := false;
        elsif r.sop_cnt = c_bg_nof_blocks_per_sync - 1 then
          v.sop_cnt := 0;
        else
          v.sop_cnt := r.sop_cnt + 1;
        end if;
      end if;

      if out_sosi_arr(0).sync = '1' then
        if r.first_sync then
          v.first_sync := false;
        else
          if r.sync_cnt = c_nof_sync_periods - 1 then
            v.sync_cnt := 0;
            v.loop_cnt := r.loop_cnt + 1;
          else
            v.sync_cnt := r.sync_cnt + 1;
          end if;
        end if;
      end if;
    end if;

    if out_sosi_arr(0).valid = '1' then
      if v.sop_cnt = 0 then
        -- Flush the accumulation registers
        for J in 0 to g_nof_channels - 1 loop
          v.accu_reg(J) := fs_step_matrix(r.sync_cnt, J);
        end loop;
      else
        -- Accumulate
        v.accu_reg(v.chn_cnt) := r.accu_reg(v.chn_cnt) + fs_step_matrix(r.sync_cnt, v.chn_cnt);
      end if;
      -- Calculate the next reference value.
      v.step    := TO_SINT(TO_SVEC(v.accu_reg(v.chn_cnt), g_accu_w)(g_accu_w - 1 downto g_accu_w - g_fs_offset_w));
      v_sum_int := fs_offset_matrix(v.sync_cnt, v.chn_cnt) + v.step;
      v_sum_vec := TO_SVEC(v_sum_int, c_integer_w);
      v.index   := TO_UINT(v_sum_vec(g_fs_offset_w - 1 downto 0));
--      v.index  := TO_UINT(TO_UVEC(temp, g_fs_offset_w+1)(g_fs_offset_w-1 DOWNTO 0));
--      v.index  := TO_UINT(TO_UVEC(fs_offset_matrix(v.sync_cnt, v.chn_cnt) + v.step, g_fs_offset_w+1)(g_fs_offset_w-1 DOWNTO 0));
      v.ref_re := COMPLEX_MULT_REAL( bg_data_arr_re(v.chn_cnt), bg_data_arr_im(v.chn_cnt), c_lookup_real(v.index), c_lookup_imag(v.index));
      v.ref_im := COMPLEX_MULT_IMAG( bg_data_arr_re(v.chn_cnt), bg_data_arr_im(v.chn_cnt), c_lookup_real(v.index), c_lookup_imag(v.index));
    end if;

    rin <= v;
  end process p_reference_comb;

  regs : process(dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_type_rst;
    elsif rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  p_verify : process(r.ref_re, r.ref_im, dp_clk, r.out_sosi_arr)
  begin
    if rising_edge(dp_clk) then
      if r.out_sosi_arr(0).valid = '1' then
        assert TO_SINT(r.out_sosi_arr(0).re) = r.ref_re
          report "Error: wrong result in real part DUT"
          severity ERROR;
        assert TO_SINT(r.out_sosi_arr(0).im) = r.ref_im
          report "Error: wrong result in imaginary part DUT"
          severity ERROR;
      end if;
    end if;
  end process;

  tb_end <= '1' when r.loop_cnt = 4 else '0';
end tb;
