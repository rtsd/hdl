-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Provide waitrequest stimuli to model a slave with MM flow control
-- Description:
--   The model applies random waitrequest stimuli for a MM slave that does not
--   need MM flow control. In this way the MM slave acts like a MM slave that
--   does need MM flow control.
--   * The model only controls the bus_miso.waitrequest. The other slave_miso
--     fields are wired to the bus_miso. The bus master will act upon the
--     waitrequest, so model can rely on that regarding the bus_mosi. However
--     towards the MM slave that has no flow control the model has to gate the
--     bus_mosi wr and rd with the waitrequest, so that the MM slave only gets
--     a ram_mosi rd or wr when it was acknowledged.
--   * When g_waitrequest = TRUE then the waitrequest model is applied to the
--     bus_miso. Use g_waitrequest = FALSE to bypass the waitrequest model,
--     so then bus_miso.waitrequest is fixed '0'.
--   * The g_seed is used to initalize the random PRSG, e.g use slave instance
--     index as g_seed to have different stimuli per instance.
--   * The maximum number of cycles that waitrequest depends on the period of
--     the LFSR random sequence generator and can be:
--     . '1' for g_prsg_w mm_clk cycles
--     . '0' for g_prsg_w-1 mm_clk cycles
-- Remarks:
-- . To some extend the ASSERTs check the flow control. The testbench has to
--   verify the rddata to ensure more test coverage.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;

entity mm_waitrequest_model is
  generic (
    g_waitrequest : boolean;
    g_seed        : natural := 0;
    g_prsg_w      : natural := 16
  );
  port (
    mm_clk      : in  std_logic;
    bus_mosi    : in  t_mem_mosi;
    bus_miso    : out t_mem_miso;
    slave_mosi  : out t_mem_mosi;
    slave_miso  : in  t_mem_miso
  );
end mm_waitrequest_model;

architecture rtl of mm_waitrequest_model is
  constant c_prsg_init     : natural := g_seed + 1;  -- PRSG init must be > 0

  signal prsg              : std_logic_vector(g_prsg_w - 1 downto 0) := TO_UVEC(c_prsg_init, g_prsg_w);

  signal waitrequest       : std_logic;

  signal prev_bus_mosi     : t_mem_mosi;
  signal prev_waitrequest  : std_logic;
begin
  no_waitrequest : if g_waitrequest = false generate
    slave_mosi <= bus_mosi;

    p_waitrequest : process(slave_miso)
    begin
      bus_miso <= slave_miso;
      bus_miso.waitrequest <= '0';
    end process;
  end generate;

  gen_waitrequest : if g_waitrequest = true generate
    -- Model MM flow control using random waitrequest
    p_reg : process(mm_clk)
    begin
      if rising_edge(mm_clk) then
        -- random waitrequest flow control
        prsg             <= func_common_random(prsg);
        -- check MM access
        prev_bus_mosi    <= bus_mosi;
        prev_waitrequest <= waitrequest;
      end if;
    end process;

    waitrequest <= prsg(0);

    -- Apply MM flow control to bus master using waitrequest
    p_bus_miso : process(waitrequest, slave_miso)
    begin
      bus_miso <= slave_miso;
      bus_miso.waitrequest <= waitrequest;
    end process;

    -- Gate MM rd and wr access to RAM slave that has no flow control
    p_slave_mosi : process(waitrequest, bus_mosi)
    begin
      slave_mosi <= bus_mosi;
      slave_mosi.wr <= bus_mosi.wr and not waitrequest;
      slave_mosi.rd <= bus_mosi.rd and not waitrequest;
    end process;

    -- Verify that MM access is not removed before it is acknowledged by waitrequest
    p_verify : process(bus_mosi, prev_bus_mosi, prev_waitrequest)
    begin
      if prev_waitrequest = '1' then
        if prev_bus_mosi.wr = '1' and bus_mosi.wr = '0' then report "Aborted slave write." severity ERROR; end if;
        if prev_bus_mosi.rd = '1' and bus_mosi.rd = '0' then report "Aborted slave read." severity ERROR; end if;
        if prev_bus_mosi.wr = '1' and bus_mosi.address /= prev_bus_mosi.address then report "Address change during pending slave write." severity ERROR; end if;
        if prev_bus_mosi.rd = '1' and bus_mosi.address /= prev_bus_mosi.address then report "Address change during pending slave read." severity ERROR; end if;
      end if;
    end process;

  end generate;

end rtl;
