-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle (original), E. Kooistra (updates)
-- Purpose: Self-checking testbench for simulating lofar2_unb2b_sdp_station_bf using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG on signal input g_sp via reg_diag_wg with:
--        g_subband = 102 --> WG freq = 19.921875MHz
--        g_ampl = 1.0 --> WG ampl = 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write
--      reg_bsn_scheduler_wg to trigger start of WG at BSN.
--
--   3) Read and verify subband statistics (SST)
--
--   4) Select subband g_subband for beamlet g_beamlet
--
--   5) Apply BF weight (g_bf_gain, g_bf_phase) to g_beamlet X beam and Y beam
--
--   6) Read and verify beamlet statistics (BST)
--        View sp_subband_sst in Wave window
--        View pol_beamlet_bst in Wave window
--
--   7) Verify 10GbE output header and output payload for g_beamlet.
--
-- Remark:
-- * Use g_beamlet_scale = 2**10, for full scale WG and N_ant = 1, see [1]
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   # Manually add missing signal
--   > add wave -position insertpoint  \
--     sim:/tb_lofar2_unb2b_sdp_station_bf/sp_subband_ssts_arr2 \
--     sim:/tb_lofar2_unb2b_sdp_station_bf/pol_beamlet_bsts_arr2
--   > run -a
--   Takes about   40 m when g_read_all_* = FALSE
--   Takes about 1h 5 m when g_read_all_* = TRUE
--
-- References:
-- [1] L4 SDPFW Decision: LOFAR2.0 SDP Firmware Quantization Model,
--     https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L4+SDPFW+Decision%3A+LOFAR2.0+SDP+Firmware+Quantization+Model
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, tech_pll_lib, tr_10GbE_lib, lofar2_unb2b_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use lofar2_sdp_lib.tb_sdp_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_lofar2_unb2b_sdp_station_bf is
  generic (
    g_sp                : natural := 0;  -- WG signal path index in range(S_pn = 12)
    g_wg_ampl           : real := 1.0;  -- WG normalized amplitude
    g_subband           : natural := 102;  -- select g_subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
    g_beamlet           : natural := 10;  -- map g_subband to g_beamlet index in beamset in range(c_sdp_S_sub_bf = 488)
    g_beamlet_scale     : real := 1.0 / 2.0**10;  -- g_beamlet output scale factor
    g_bf_gain           : real := 1.0;  -- g_beamlet BF weight normalized gain
    g_bf_phase          : real := 30.0;  -- g_beamlet BF weight phase rotation in degrees
    g_read_all_SST      : boolean := false;  -- when FALSE only read SST for g_subband, to save sim time
    g_read_all_BST      : boolean := false  -- when FALSE only read BST for g_beamlet, to save sim time
  );
end tb_lofar2_unb2b_sdp_station_bf;

architecture tb of tb_lofar2_unb2b_sdp_station_bf is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_gn_index        : natural := c_unb_nr * 4 + c_node_nr;  -- this node GN
  constant c_init_bsn        : natural := 17;  -- some recognizable value >= 0

  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_gn_index, 8);
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_mac_15_0        : std_logic_vector(15 downto 0) := TO_UVEC(c_unb_nr, 8) & TO_UVEC(c_node_nr, 8);
  constant c_ip_15_0         : std_logic_vector(15 downto 0) := TO_UVEC(c_unb_nr, 8) & TO_UVEC(c_node_nr + 1, 8);  -- +1 to avoid IP = *.*.*.0

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_mm_clk_period       : time := 10 ns;  -- 100 MHz internal mm_clk
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 16;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_stat_data_sz        : natural := c_wpfb_sim.stat_data_sz;  -- = 2

  constant c_stat_percentage     : real := 0.05;  -- +-percentage margin that actual value may differ from expected value
  constant c_stat_lo_factor      : real := 1.0 - c_stat_percentage;  -- lower boundary
  constant c_stat_hi_factor      : real := 1.0 + c_stat_percentage;  -- higher boundary

  constant c_beamlet_output_delta : integer := 2;  -- +-delta margin

  -- header fields
  constant c_cep_eth_src_mac     : std_logic_vector(47 downto 0) := c_sdp_cep_eth_src_mac_47_16 & c_mac_15_0;  -- x"00228608";  -- 47:16, 15:8 = backplane, 7:0 = node
  constant c_cep_ip_src_addr     : std_logic_vector(31 downto 0) := c_sdp_cep_ip_src_addr_31_16 & c_ip_15_0;  -- C0A80001 = '192.168.0.1' = DOP36-eth0
  constant c_cep_udp_src_port    : std_logic_vector(15 downto 0) := c_sdp_cep_udp_src_port_15_8 & c_id;  -- D0 & c_id

  constant c_exp_ip_header_checksum : natural := 16#5BDE#;  -- value obtained from rx_sdp_cep_header.ip.header_checksum in wave window

  constant c_exp_beamlet_scale   : natural := natural(g_beamlet_scale * real(c_sdp_unit_beamlet_scale));  -- c_sdp_unit_beamlet_scale = 2**15;
  constant c_exp_beamlet_index   : natural := 0;  -- depends on beamset bset * c_sdp_S_sub_bf
  constant c_beamlet_index_mod   : boolean := true;

  constant c_exp_sdp_info        : t_sdp_info := (
                                     TO_UVEC(3, 6),  -- antenna_field_index
                                     TO_UVEC(601, 10),  -- station_id
                                     '0',  -- antenna_band_index
                                     x"7FFFFFFF",  -- observation_id, use > 0 to avoid Warning: (vsim-151) NUMERIC_STD.TO_INTEGER: Value -2 is not in bounds of subtype NATURAL.
                                     b"01",  -- nyquist_zone_index, 0 = first, 1 = second, 2 = third
                                     '1',  -- f_adc, 0 = 160 MHz, 1 = 200 MHz
                                     '0',  -- fsub_type, 0 = critically sampled, 1 = oversampled
                                     '0',  -- beam_repositioning_flag
                                     x"1400"  -- block_period = 5120
                                   );

  -- WG
  constant c_bsn_start_wg         : natural := c_init_bsn + 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  -- .ampl
  constant c_wg_ampl              : natural := natural(g_wg_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_exp_sp_power         : real := real(c_wg_ampl**2) / 2.0;
  constant c_exp_sp_ast           : real := c_exp_sp_power * real(c_nof_clk_per_sync);
  -- . phase
  constant c_subband_phase        : real := 0.0;  -- wanted subband phase in degrees = WG phase at sop
  constant c_subband_freq         : real := real(g_subband) / real(c_sdp_N_fft);  -- normalized by fs = f_adc = 200 MHz = dp_clk rate
  constant c_wg_latency           : integer := c_diag_wg_latency + c_sdp_shiftram_latency - 0;  -- -0 to account for BSN scheduler start trigger latency
  constant c_wg_phase_offset      : real := 360.0 * real(c_wg_latency) * c_subband_freq;  -- c_diag_wg_latency is in dp_clk cycles
  constant c_wg_phase             : real := c_subband_phase + c_wg_phase_offset;  -- WG phase in degrees

  -- WPFB
  constant c_pol_index                      : natural := g_sp mod c_sdp_Q_fft;
  constant c_pfb_index                      : natural := g_sp / c_sdp_Q_fft;  -- only read used WPFB unit out of range(c_sdp_P_pfb = 6)
  constant c_subband_phase_offset           : real := -90.0;  -- WG with zero phase sinues yields subband with -90 degrees phase (negative Im, zero Re)
  constant c_subband_weight_gain            : real := 1.0;  -- use default unit subband weights
  constant c_subband_weight_phase           : real := 0.0;  -- use default unit subband weights
  constant c_exp_subband_ampl               : real := real(c_wg_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio * c_subband_weight_gain;
  constant c_exp_subband_power              : real := c_exp_subband_ampl**2.0;  -- complex, so no divide by 2
  constant c_exp_subband_sst                : real := c_exp_subband_power * real(c_nof_block_per_sync);

  type t_real_arr is array (integer range <>) of real;
  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_sub - 1);  -- 512
  type t_slv_64_beamlets_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_beamlets_sdp - 1);  -- 2*488 = 976

  -- BF
  -- . select
  constant c_exp_g_beamlet_index        : natural := g_beamlet * c_sdp_N_pol_bf;  -- in beamset 0
  -- . Beamlet weights for selected g_sp
  constant c_bf_weight_re             : integer := integer(g_bf_gain * real(c_sdp_unit_bf_weight) * COS(g_bf_phase * MATH_2_PI / 360.0));
  constant c_bf_weight_im             : integer := integer(g_bf_gain * real(c_sdp_unit_bf_weight) * SIN(g_bf_phase * MATH_2_PI / 360.0));
  -- . Beamlet internal
  constant c_exp_beamlet_ampl         : real := c_exp_subband_ampl * g_bf_gain;
  constant c_exp_beamlet_phase        : real := c_subband_phase_offset + c_subband_weight_phase + g_bf_phase;
  constant c_exp_beamlet_re           : real := c_exp_beamlet_ampl * COS(c_exp_beamlet_phase * MATH_2_PI / 360.0);
  constant c_exp_beamlet_im           : real := c_exp_beamlet_ampl * SIN(c_exp_beamlet_phase * MATH_2_PI / 360.0);
  -- . BST
  constant c_exp_beamlet_power        : real := c_exp_beamlet_ampl**2.0;  -- complex, so no divide by 2
  constant c_exp_beamlet_bst          : real := c_exp_subband_sst * g_bf_gain**2.0;  -- = c_exp_beamlet_power *  REAL(c_nof_block_per_sync)
  -- . Beamlet output
  constant c_exp_beamlet_output_ampl  : real := c_exp_beamlet_ampl * g_beamlet_scale;
  constant c_exp_beamlet_output_phase : real := c_exp_beamlet_phase;
  constant c_exp_beamlet_output_re    : real := c_exp_beamlet_re * g_beamlet_scale;
  constant c_exp_beamlet_output_im    : real := c_exp_beamlet_im * g_beamlet_scale;

  -- MM
  -- . Address widths of a single MM instance
  --   . c_sdp_S_pn = 12 instances
  constant c_addr_w_reg_diag_wg     : natural := 2;
  --   . c_sdp_N_beamsets = 2 instances
  constant c_addr_w_ram_ss_ss_wide  : natural := ceil_log2(                 c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_ram_bf_weights  : natural := ceil_log2(c_sdp_N_pol_bf * c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_reg_bf_scale    : natural := 1;
  constant c_addr_w_reg_hdr_dat     : natural := ceil_log2(field_nof_words(c_sdp_cep_hdr_field_arr, c_word_w));
  constant c_addr_w_reg_dp_xonoff   : natural := 1;
  constant c_addr_w_ram_st_bst      : natural := ceil_log2(c_sdp_S_sub_bf * c_sdp_N_pol_bf * c_stat_data_sz);
  -- . Address spans of a single MM instance
  --   . c_sdp_S_pn = 12 instances
  constant c_mm_span_reg_diag_wg    : natural := 2**c_addr_w_reg_diag_wg;
  --   . c_sdp_N_beamsets = 2 instances
  constant c_mm_span_ram_ss_ss_wide : natural := 2**c_addr_w_ram_ss_ss_wide;
  constant c_mm_span_ram_bf_weights : natural := 2**c_addr_w_ram_bf_weights;
  constant c_mm_span_reg_bf_scale   : natural := 2**c_addr_w_reg_bf_scale;
  constant c_mm_span_reg_hdr_dat    : natural := 2**c_addr_w_reg_hdr_dat;
  constant c_mm_span_reg_dp_xonoff  : natural := 2**c_addr_w_reg_dp_xonoff;
  constant c_mm_span_ram_st_bst     : natural := 2**c_addr_w_ram_st_bst;

  constant c_mm_file_reg_ppsh             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_bsn_source_v2    : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_ram_equalizer_gains  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_EQUALIZER_GAINS";
  constant c_mm_file_reg_dp_selector      : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_SELECTOR";
  constant c_mm_file_ram_st_sst           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_SST";
  constant c_mm_file_ram_st_bst           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_BST";
  constant c_mm_file_reg_dp_xonoff        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_XONOFF";
  constant c_mm_file_ram_ss_ss_wide       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_SS_SS_WIDE";
  constant c_mm_file_ram_bf_weights       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_BF_WEIGHTS";
  constant c_mm_file_reg_bf_scale         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BF_SCALE";
  constant c_mm_file_reg_sdp_info         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_SDP_INFO";
  constant c_mm_file_reg_hdr_dat          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_HDR_DAT";  -- c_sdp_N_beamsets = 2 beamsets

  -- Tb
  signal stimuli_done        : std_logic := '0';
  signal tb_almost_end       : std_logic := '0';
  signal tb_end              : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  signal dest_rst            : std_logic := '1';  -- use separate destination rst for Rx 10GbE in tb
  signal pps_rst             : std_logic := '1';  -- use separate reset to release the PPS generator
  signal gen_pps             : std_logic := '0';

  signal in_sync             : std_logic := '0';
  signal in_sync_cnt         : natural := 0;
  signal test_sync_cnt       : integer := 0;

  -- MM
  signal rd_sdp_info         : t_sdp_info := c_sdp_info_rst;
  signal rd_beamlet_scale    : std_logic_vector(15 downto 0);
  signal rd_cep_eth_src_mac  : std_logic_vector(47 downto 0);
  signal rd_cep_eth_dst_mac  : std_logic_vector(47 downto 0);
  signal rd_cep_ip_src_addr  : std_logic_vector(31 downto 0);
  signal rd_cep_ip_dst_addr  : std_logic_vector(31 downto 0);
  signal rd_cep_udp_src_port : std_logic_vector(15 downto 0);
  signal rd_cep_udp_dst_port : std_logic_vector(15 downto 0);

  -- WG
  signal current_bsn_wg      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- FSUB
  -- . Read sp_subband_ssts_arr2 = SST for one WPFB unit that processes g_sp
  signal sp_subband_ssts_arr2  : t_slv_64_subbands_arr(c_sdp_N_pol - 1 downto 0);  -- [pol][sub], for X,Y pair of A, B
  signal sp_subband_sst        : real := 0.0;
  signal stat_data               : std_logic_vector(c_longword_w - 1 downto 0);

  -- . Selector
  signal sst_offload_weighted_subbands : std_logic;

  -- . Subband equalizer
  signal sp_subband_weight_re    : integer := 0;
  signal sp_subband_weight_im    : integer := 0;
  signal sp_subband_weight_gain  : real := 0.0;
  signal sp_subband_weight_phase : real := 0.0;

  -- BF
  signal sp_subband_select       : natural :=  0;
  signal sp_subband_select_arr   : t_natural_arr(0 to c_sdp_S_sub_bf * c_sdp_N_pol - 1) := (others => 0);  -- Q_fft = N_pol = 2
  signal sp_bf_weights_re_arr    : t_integer_arr(0 to c_sdp_S_pn - 1) := (others => 0);
  signal sp_bf_weights_im_arr    : t_integer_arr(0 to c_sdp_S_pn - 1) := (others => 0);
  signal sp_bf_weights_gain_arr  : t_real_arr(0 to c_sdp_S_pn - 1) := (others => 0.0);
  signal sp_bf_weights_phase_arr : t_real_arr(0 to c_sdp_S_pn - 1) := (others => 0.0);

  signal pol_beamlet_bsts_arr2 : t_slv_64_beamlets_arr(c_sdp_N_pol_bf - 1 downto 0);  -- [pol_bf][blet]
  signal pol_beamlet_bst_X_arr : t_real_arr(0 to c_sdp_N_beamsets - 1) := (others => 0.0);  -- [bset]
  signal pol_beamlet_bst_Y_arr : t_real_arr(0 to c_sdp_N_beamsets - 1) := (others => 0.0);  -- [bset]

  -- 10GbE
  signal tr_10GbE_src_out        : t_dp_sosi;
  signal tr_10GbE_src_in         : t_dp_siso;
  signal tr_ref_clk_312          : std_logic := '0';
  signal tr_ref_clk_156          : std_logic := '0';
  signal tr_ref_rst_156          : std_logic := '0';

  -- dp_offload_rx
  signal offload_rx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal offload_rx_hdr_dat_miso : t_mem_miso;

  signal rx_hdr_fields_out       : std_logic_vector(1023 downto 0);
  signal rx_hdr_fields_raw       : std_logic_vector(1023 downto 0) := (others => '0');
  signal rx_sdp_cep_header       : t_sdp_cep_header;
  signal exp_sdp_cep_header      : t_sdp_cep_header;
  signal exp_dp_bsn              : natural;
  signal exp_payload_error       : std_logic := '0';

  signal rx_beamlet_en           : std_logic := '0';
  signal rx_beamlet_data         : std_logic_vector(c_longword_w - 1 downto 0);  -- 64 bit
  signal rx_beamlet_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal rx_beamlet_sop_cnt      : natural := 0;
  signal rx_beamlet_eop_cnt      : natural := 0;

  -- [0 : 3] =  X, Y, X, Y
  signal rx_beamlet_arr_re       : t_sdp_beamlet_part_arr;
  signal rx_beamlet_arr_im       : t_sdp_beamlet_part_arr;
  signal rx_beamlet_cnt          : natural;
  signal rx_beamlet_valid        : std_logic;
  -- [0 : 4 * 488 * 2 - 1] = [0 : 3903]
  signal rx_packet_list_re       : t_sdp_beamlet_packet_list;
  signal rx_packet_list_im       : t_sdp_beamlet_packet_list;

  -- DUT
  signal ext_clk              : std_logic := '0';
  signal ext_pps              : std_logic := '0';

  signal WDI                  : std_logic;
  signal INTA                 : std_logic;
  signal INTB                 : std_logic;

  signal eth_clk              : std_logic := '0';
  signal eth_txp              : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp              : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl             : std_logic;
  signal sens_sda             : std_logic;
  signal pmbus_scl            : std_logic;
  signal pmbus_sda            : std_logic;

  signal SA_CLK               : std_logic := '1';
  signal si_lpbk_0            : std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref      : std_logic;
  signal jesd204b_sync_n      : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)
  dest_rst <= '0' after c_ext_clk_period * 10;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, gen_pps);
  jesd204b_sysref <= gen_pps;
  ext_pps <= gen_pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2b_sdp_station_bf : entity lofar2_unb2b_sdp_station_lib.lofar2_unb2b_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2b_sdp_station_bf",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync,
    g_scope_selected_subband => g_subband
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => ext_pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,
    -- front transceivers
    QSFP_1_RX    => si_lpbk_0,
    QSFP_1_TX    => si_lpbk_0,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  u_unb2_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  port map (
    refclk_644 => SA_CLK,
    rst_in     => dest_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
  generic map (
    g_sim           => true,
    g_sim_level     => 1,
    g_nof_macs      => 1,
    g_use_mdio      => false
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644      => SA_CLK,
    tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

    -- MM interface
    mm_rst              => dest_rst,
    mm_clk              => tb_clk,

    -- DP interface
    dp_rst              => dest_rst,
    dp_clk              => ext_clk,

    serial_rx_arr(0)    => si_lpbk_0(0),

    src_out_arr(0)      => tr_10GbE_src_out,
    src_in_arr(0)       => tr_10GbE_src_in
  );

  u_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => c_longword_w,
    g_symbol_w            => c_octet_w,
    g_hdr_field_arr       => c_sdp_cep_hdr_field_arr,
    g_remove_crc          => false,
    g_crc_nof_words       => 0
  )
  port map (
    mm_rst                => dest_rst,
    mm_clk                => tb_clk,

    dp_rst                => dest_rst,
    dp_clk                => ext_clk,

    reg_hdr_dat_mosi      => offload_rx_hdr_dat_mosi,
    reg_hdr_dat_miso      => offload_rx_hdr_dat_miso,

    snk_in_arr(0)         => tr_10GbE_src_out,
    snk_out_arr(0)        => tr_10GbE_src_in,

    src_out_arr(0)        => rx_beamlet_sosi,

    hdr_fields_out_arr(0) => rx_hdr_fields_out,
    hdr_fields_raw_arr(0) => rx_hdr_fields_raw
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                                  : natural;
    variable v_sp_subband_sst                       : real := 0.0;
    variable v_pol_beamlet_bst                      : real := 0.0;
    variable v_data_lo, v_data_hi                   : std_logic_vector(c_word_w - 1 downto 0);
    variable v_stat_data                            : std_logic_vector(c_longword_w - 1 downto 0);
    variable v_len, v_span, v_offset, v_addr, v_sel : natural;  -- address ranges, indices
    variable v_W, v_P, v_S, v_A, v_B, v_G           : natural;  -- array indicies
    variable v_re, v_im, v_weight                   : integer;
    variable v_re_exp, v_im_exp                     : real := 0.0;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Set and check SDP info
    ----------------------------------------------------------------------------
    --     TYPE t_sdp_info IS RECORD
    --   8   antenna_field_index     : STD_LOGIC_VECTOR(5 DOWNTO 0);
    --   7   station_id              : STD_LOGIC_VECTOR(9 DOWNTO 0);
    --   6   antenna_band_index      : STD_LOGIC;
    --   5   observation_id          : STD_LOGIC_VECTOR(31 DOWNTO 0);
    --   4   nyquist_zone_index      : STD_LOGIC_VECTOR(1 DOWNTO 0);
    --   3   f_adc                   : STD_LOGIC;
    --   2   fsub_type               : STD_LOGIC;
    --   1   beam_repositioning_flag : STD_LOGIC;
    --   0   block_period            : STD_LOGIC_VECTOR(15 DOWNTO 0);
    --     END RECORD;
    -- . Write
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  8, TO_UINT(c_exp_sdp_info.antenna_field_index), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  7, TO_UINT(c_exp_sdp_info.station_id), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  6, TO_UINT(slv(c_exp_sdp_info.antenna_band_index)), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  5, TO_UINT(c_exp_sdp_info.observation_id), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  4, TO_UINT(c_exp_sdp_info.nyquist_zone_index), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  1, TO_UINT(slv(c_exp_sdp_info.beam_repositioning_flag)), tb_clk);
    -- . Read
    mmf_mm_bus_rd(c_mm_file_reg_sdp_info,  3, rd_data, tb_clk); rd_sdp_info.f_adc <= rd_data(0);
    mmf_mm_bus_rd(c_mm_file_reg_sdp_info,  2, rd_data, tb_clk); rd_sdp_info.fsub_type <= rd_data(0);
    mmf_mm_bus_rd(c_mm_file_reg_sdp_info,  0, rd_data, tb_clk); rd_sdp_info.block_period <= rd_data(15 downto 0);
    proc_common_wait_some_cycles(tb_clk, 1);
    -- . Verify read
    assert c_exp_sdp_info.f_adc = rd_sdp_info.f_adc
      report "Wrong MM read SDP info f_adc"
      severity ERROR;
    assert c_exp_sdp_info.fsub_type = rd_sdp_info.fsub_type
      report "Wrong MM read SDP info fsub_type"
      severity ERROR;
    assert c_exp_sdp_info.block_period = rd_sdp_info.block_period
      report "Wrong MM read SDP info block_period"
      severity ERROR;

    ------------------------------------------------------------------------------
    ---- Set and check BF per beamset
    ------------------------------------------------------------------------------
    for bset in 0 to c_sdp_N_beamsets - 1 loop
      -- MM beamlet_scale
      -- . write
      v_offset := bset * c_mm_span_reg_bf_scale;
      mmf_mm_bus_wr(c_mm_file_reg_bf_scale, v_offset + 0, c_exp_beamlet_scale, tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_ext_clk_period);

      -- . readback
      mmf_mm_bus_rd(c_mm_file_reg_bf_scale, v_offset + 0, rd_data, tb_clk);
      rd_beamlet_scale <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert TO_UINT(rd_beamlet_scale) = c_exp_beamlet_scale
        report "Wrong MM read beamlet_scale for beamset " & natural'image(bset)
        severity ERROR;

      -- CEP beamlet output header
      --     c_sdp_cep_hdr_field_arr : t_common_field_arr(c_sdp_cep_nof_hdr_fields-1 DOWNTO 0) := (
      --  40   "eth_dst_mac"                        ), "RW", 48, field_default(c_sdp_cep_eth_dst_mac) ),
      --  38   "eth_src_mac"                        ), "RW", 48, field_default(0) ),
      --  37   "eth_type"                           ), "RW", 16, field_default(x"0800") ),
      --
      --  36   "ip_version"                         ), "RW",  4, field_default(4) ),
      --  35   "ip_header_length"                   ), "RW",  4, field_default(5) ),
      --  34   "ip_services"                        ), "RW",  8, field_default(0) ),
      --  33   "ip_total_length"                    ), "RW", 16, field_default(c_sdp_cep_ip_total_length) ),
      --  32   "ip_identification"                  ), "RW", 16, field_default(0) ),
      --  31   "ip_flags"                           ), "RW",  3, field_default(2) ),
      --  30   "ip_fragment_offset"                 ), "RW", 13, field_default(0) ),
      --  29   "ip_time_to_live"                    ), "RW",  8, field_default(127) ),
      --  28   "ip_protocol"                        ), "RW",  8, field_default(17) ),
      --  27   "ip_header_checksum"                 ), "RW", 16, field_default(0) ),
      --  26   "ip_src_addr"                        ), "RW", 32, field_default(0) ),
      --  25   "ip_dst_addr"                        ), "RW", 32, field_default(c_sdp_cep_ip_dst_addr) ),
      --
      --  24   "udp_src_port"                       ), "RW", 16, field_default(0) ),
      --  23   "udp_dst_port"                       ), "RW", 16, field_default(c_sdp_cep_udp_dst_port) ),
      --  22   "udp_total_length"                   ), "RW", 16, field_default(c_sdp_cep_udp_total_length) ),
      --  21   "udp_checksum"                       ), "RW", 16, field_default(0) ),
      --
      --  20   "sdp_marker"                         ), "RW",  8, field_default(c_sdp_marker_beamlets) ),
      --  19   "sdp_version_id"                     ), "RW",  8, field_default(c_sdp_cep_version_id) ),
      --  18   "sdp_observation_id"                 ), "RW", 32, field_default(0) ),
      --  17   "sdp_station_info"                   ), "RW", 16, field_default(0) ),
      --
      --  16   "sdp_source_info_reserved"               ), "RW",  5, field_default(0) ),
      --  15   "sdp_source_info_antenna_band_id"        ), "RW",  1, field_default(0) ),
      --  14   "sdp_source_info_nyquist_zone_id"        ), "RW",  2, field_default(0) ),
      --  13   "sdp_source_info_f_adc"                  ), "RW",  1, field_default(0) ),
      --  12   "sdp_source_info_fsub_type"              ), "RW",  1, field_default(0) ),
      --  11   "sdp_source_info_payload_error"          ), "RW",  1, field_default(0) ),
      --  10   "sdp_source_info_beam_repositioning_flag"), "RW",  1, field_default(0) ),
      --   9   "sdp_source_info_beamlet_width"          ), "RW",  4, field_default(c_sdp_W_beamlet) ),
      --   8   "sdp_source_info_gn_id"                  ), "RW",  8, field_default(0) ),
      --
      --   7   "sdp_reserved"                       ), "RW", 32, field_default(0) ),
      --   6   "sdp_beamlet_scale"                  ), "RW", 16, field_default(c_sdp_unit_beamlet_scale) ),
      --   5   "sdp_beamlet_index"                  ), "RW", 16, field_default(0) ),
      --   4   "sdp_nof_blocks_per_packet"          ), "RW",  8, field_default(c_sdp_cep_nof_blocks_per_packet) ),
      --   3   "sdp_nof_beamlets_per_block"         ), "RW", 16, field_default(c_sdp_cep_nof_beamlets_per_block) ),
      --   2   "sdp_block_period"                   ), "RW", 16, field_default(c_sdp_block_period) ),
      --
      --   0   "dp_bsn"                             ), "RW", 64, field_default(0) )
      --     );

      v_offset := bset * c_mm_span_reg_hdr_dat;
      -- Default destination MAC/IP/UDP = 0
      -- . Read
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 39, rd_data, tb_clk); rd_cep_eth_src_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 38, rd_data, tb_clk); rd_cep_eth_src_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 26, rd_data, tb_clk); rd_cep_ip_src_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 24, rd_data, tb_clk); rd_cep_udp_src_port <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 41, rd_data, tb_clk); rd_cep_eth_dst_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 40, rd_data, tb_clk); rd_cep_eth_dst_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 25, rd_data, tb_clk); rd_cep_ip_dst_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 23, rd_data, tb_clk); rd_cep_udp_dst_port <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      -- . verify read
      assert unsigned(rd_cep_eth_src_mac) = 0
        report "Wrong MM read rd_cep_eth_src_mac != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_ip_src_addr) = 0
        report "Wrong MM read rd_cep_ip_src_addr != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_udp_src_port) = 0
        report "Wrong MM read rd_cep_udp_src_port != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_eth_dst_mac) = 0
        report "Wrong MM read rd_cep_eth_dst_mac != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_ip_dst_addr) = 0
        report "Wrong MM read rd_cep_ip_dst_addr != 0 for beamset " & natural'image(bset)
        severity ERROR;
      assert unsigned(rd_cep_udp_dst_port) = 0
        report "Wrong MM read rd_cep_udp_dst_port != 0 for beamset " & natural'image(bset)
        severity ERROR;

      -- Write tb defaults
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 39, TO_UINT(c_cep_eth_src_mac(47 downto 32)), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 38, TO_SINT(c_cep_eth_src_mac(31 downto 0)), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 26, TO_SINT(c_cep_ip_src_addr), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 24, TO_UINT(c_cep_udp_src_port), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 41, TO_UINT(c_sdp_cep_eth_dst_mac(47 downto 32)), tb_clk);
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 40, TO_SINT(c_sdp_cep_eth_dst_mac(31 downto 0)), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 25, TO_SINT(c_sdp_cep_ip_dst_addr), tb_clk);  -- use signed to fit 32 b in INTEGER
      mmf_mm_bus_wr(c_mm_file_reg_hdr_dat, v_offset + 23, TO_UINT(c_sdp_cep_udp_dst_port), tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);

      -- . Read back
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 39, rd_data, tb_clk); rd_cep_eth_src_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 38, rd_data, tb_clk); rd_cep_eth_src_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 26, rd_data, tb_clk); rd_cep_ip_src_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 24, rd_data, tb_clk); rd_cep_udp_src_port <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 41, rd_data, tb_clk); rd_cep_eth_dst_mac(47 downto 32) <= rd_data(15 downto 0);
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 40, rd_data, tb_clk); rd_cep_eth_dst_mac(31 downto  0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 25, rd_data, tb_clk); rd_cep_ip_dst_addr <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_hdr_dat, v_offset + 23, rd_data, tb_clk); rd_cep_udp_dst_port <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      -- . verify read back
      assert rd_cep_eth_src_mac = c_cep_eth_src_mac
        report "Wrong MM read rd_cep_eth_src_mac for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_ip_src_addr = c_cep_ip_src_addr
        report "Wrong MM read rd_cep_ip_src_addr for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_udp_src_port = c_cep_udp_src_port
        report "Wrong MM read rd_cep_udp_src_port for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_eth_dst_mac = c_sdp_cep_eth_dst_mac
        report "Wrong MM read rd_cep_eth_dst_mac for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_ip_dst_addr = c_sdp_cep_ip_dst_addr
        report "Wrong MM read rd_cep_ip_dst_addr for beamset " & natural'image(bset)
        severity ERROR;
      assert rd_cep_udp_dst_port = c_sdp_cep_udp_dst_port
        report "Wrong MM read rd_cep_udp_dst_port for beamset " & natural'image(bset)
        severity ERROR;

      ----------------------------------------------------------------------------
      -- Enable beamlet UDP offload (dp_xonoff)
      ----------------------------------------------------------------------------
      v_offset := bset * c_mm_span_reg_dp_xonoff;
      mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff, v_offset + 0, 1, tb_clk);
    end loop;

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,         c_init_bsn, tb_clk);  -- Init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                  0, tb_clk);  -- Write high part activates the init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1, c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,       16#00000003#, tb_clk);  -- Enable BS at PPS

    -- Release PPS pulser, to get first PPS now and to start BSN source
    wait for 1 us;
    pps_rst <= '0';

    ----------------------------------------------------------------------------
    -- Enable and start WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    -- . Put wanted signal on g_sp input
    v_offset := g_sp * c_mm_span_reg_diag_wg;
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 1, integer(c_wg_phase * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 2, integer(real(g_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 3, integer(real(c_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    v_bsn := c_bsn_start_wg;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

    ----------------------------------------------------------------------------
    -- Read weighted subband selector
    ----------------------------------------------------------------------------
    mmf_mm_bus_rd(c_mm_file_reg_dp_selector, 0, rd_data, tb_clk);
    sst_offload_weighted_subbands <= not rd_data(0);
    proc_common_wait_some_cycles(tb_clk, 1);

    ----------------------------------------------------------------------------
    -- Subband weight
    ----------------------------------------------------------------------------

    -- . MM format: (cint16)RAM_EQUALIZER_GAINS[S_pn/Q_fft]_[Q_fft][N_sub] = [S_pn][N_sub]
    v_addr := g_sp * c_sdp_N_sub + g_subband;
    -- . read
    mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains, v_addr, rd_data, tb_clk);
    v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
    v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
    sp_subband_weight_re <= v_re;
    sp_subband_weight_im <= v_im;
    sp_subband_weight_gain <= SQRT(real(v_re)**2.0 + real(v_im)**2.0) / real(c_sdp_unit_sub_weight);
    sp_subband_weight_phase <= atan2(Y => real(v_im), X => real(v_re)) * 360.0 / MATH_2_PI;

    -- No need to write subband weight, because default it is unit weight

    ----------------------------------------------------------------------------
    -- Subband select to map subband to beamlet
    ----------------------------------------------------------------------------
    -- . MM format: (uint16)RAM_SS_SS_WIDE[N_beamsets][A_pn]_[S_sub_bf][Q_fft], Q_fft = N_pol = 2

    -- . write selection, only for g_beamlet to save sim time
    v_span := true_log_pow2(c_sdp_N_pol * c_sdp_S_sub_bf);  -- = 1024
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- Same selection for both beamsets
      -- Select beamlet g_beamlet to subband g_subband
      for A in 0 to c_sdp_A_pn - 1 loop
        -- Same selection to all SP
        for P in 0 to c_sdp_N_pol - 1 loop
          v_addr := P + g_beamlet * c_sdp_N_pol + A * v_span + U * c_mm_span_ram_ss_ss_wide;
          v_sel := P + g_subband * c_sdp_N_pol;
          mmf_mm_bus_wr(c_mm_file_ram_ss_ss_wide, v_addr, v_sel, tb_clk);
        end loop;
      end loop;
    end loop;

    -- . read back selection for g_sp = c_pfb_index * c_sdp_N_pol + c_pol_index
    v_P := c_pol_index;
    v_A := c_pfb_index;
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- Same selection for both beamsets, so fine to use only one sp_subband_select_arr()
      for B in 0 to c_sdp_S_sub_bf - 1 loop
        -- Same selection for all SP, so fine to only read subband selection for g_sp
        v_addr := v_P + B * c_sdp_N_pol + v_A * v_span + U * c_mm_span_ram_ss_ss_wide;
        mmf_mm_bus_rd(c_mm_file_ram_ss_ss_wide, v_addr, rd_data, tb_clk);
        v_sel := (TO_UINT(rd_data) - v_P) / c_sdp_N_pol;
        sp_subband_select_arr(B) <= v_sel;
        sp_subband_select <= v_sel;  -- for time series view in Wave window
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ----------------------------------------------------------------------------
    -- Write beamlet weight for g_beamlet
    ----------------------------------------------------------------------------
    -- . MM format: (cint16)RAM_BF_WEIGHTS[N_beamsets][N_pol_bf][A_pn]_[N_pol][S_sub_bf]

    -- . write BF weights, only for g_beamlet to save sim time
    v_span := true_log_pow2(c_sdp_N_pol * c_sdp_S_sub_bf);  -- = 1024
    for U in 0 to c_sdp_N_beamsets - 1 loop
      -- Same BF weights for both beamsets
      for A in 0 to c_sdp_A_pn - 1 loop
        for P in 0 to c_sdp_N_pol - 1 loop
          v_S := A * c_sdp_N_pol + P;
          if v_S = g_sp then
            -- use generic BF weight for g_sp in g_beamlet
            v_weight := pack_complex(re => c_bf_weight_re, im => c_bf_weight_im, w => c_sdp_W_bf_weight);
          else
            -- default set all weights to zero
            v_weight := 0;
          end if;
          v_addr := g_beamlet + A * v_span + U * c_mm_span_ram_bf_weights;
          v_addr := v_addr + P * c_sdp_S_sub_bf;
          mmf_mm_bus_wr(c_mm_file_ram_bf_weights, v_addr, v_weight, tb_clk);
        end loop;
      end loop;
    end loop;

    -- . read back BF weights for g_beamlet
    for U in 0 to c_sdp_N_beamsets - 1 loop
      for A in 0 to c_sdp_A_pn - 1 loop
        for P in 0 to c_sdp_N_pol - 1 loop
          v_addr := g_beamlet + A * v_span + U * c_mm_span_ram_bf_weights;
          v_addr := v_addr + P * c_sdp_S_sub_bf;
          mmf_mm_bus_rd(c_mm_file_ram_bf_weights, v_addr, rd_data, tb_clk);
          v_re := unpack_complex_re(rd_data, c_sdp_W_bf_weight);
          v_im := unpack_complex_im(rd_data, c_sdp_W_bf_weight);
          -- same BF weights for both beamsets, so fine to use only one sp_bf_weights_*_arr()
          v_S := A * c_sdp_N_pol + P;
          sp_bf_weights_re_arr(v_S) <= v_re;
          sp_bf_weights_im_arr(v_S) <= v_im;
          sp_bf_weights_gain_arr(v_S) <= SQRT(real(v_re)**2.0 + real(v_im)**2.0) / real(c_sdp_unit_bf_weight);
          sp_bf_weights_phase_arr(v_S) <= atan2(Y => real(v_im), X => real(v_re)) * 360.0 / MATH_2_PI;
        end loop;
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ----------------------------------------------------------------------------
    -- Wait for enough WG data and start of sync interval
    ----------------------------------------------------------------------------
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_init_bsn + c_nof_block_per_sync * 3,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    -- Stimuli done, now verify results at end of test
    stimuli_done <= '1';

    ---------------------------------------------------------------------------
    -- Read subband statistics
    ---------------------------------------------------------------------------
    -- . the subband statistics are c_stat_data_sz = 2 word power values.
    -- . there are c_sdp_S_pn = 12 signal inputs A, B, C, D, E, F, G, H, I, J, K, L
    -- . there are c_sdp_N_sub = 512 subbands per signal input (SI, = signal path, SP)
    -- . one complex WPFB can process two real inputs A, B, so there are c_sdp_P_pfb = 6 WPFB units,
    --   but only read for the 1 WPFB unit of the selected g_sp, to save sim time
    -- . the outputs for A, B are time multiplexed, c_sdp_Q_fft = 2, assume that they
    --   correspond to the c_sdp_N_pol = 2 signal polarizations
    -- . the subbands are output alternately so A0 B0 A1 B1 ... A511 B511 for input A, B
    -- . the subband statistics multiple WPFB units appear in order in the ram_st_sst address map
    -- . the subband statistics are stored first lo word 0 then hi word 1
    v_len := c_sdp_N_sub * c_sdp_N_pol * c_stat_data_sz;  -- 2048 = 512 * 2 * 64/32
    v_span := true_log_pow2(v_len);  -- = 2048
    for I in 0 to v_len - 1 loop
      v_W := I mod c_stat_data_sz;  -- 0, 1 per statistics word, word index
      v_P := (I / c_stat_data_sz) mod c_sdp_N_pol;  -- 0, 1 per SP pol, polarization index
      v_B := I / (c_sdp_N_pol * c_stat_data_sz);  -- subband index, range(N_sub = 512) per dual pol
      v_addr := I + c_pfb_index * v_span;  -- MM address
      -- Only read SST for g_subband for dual pol SP, to save sim time
      if g_read_all_SST = true or v_B = g_subband then
        if v_W = 0 then
          -- low part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_lo := rd_data;
        else
          -- high part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_hi := rd_data;
          v_stat_data := v_data_hi & v_data_lo;

          sp_subband_ssts_arr2(v_P)(v_B) <= v_stat_data;
          stat_data <= v_stat_data;  -- for time series view in Wave window
        end if;
      end if;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Subband power of g_subband in g_sp
    -- . For the selected g_subband in g_sp the sp_subband_sst will be close
    --   to sp_subband_sst_sum_arr(c_pol_index), because the input is a
    --   sinus, so most power will be in 1 subband.
    sp_subband_sst <= TO_UREAL(sp_subband_ssts_arr2(c_pol_index)(g_subband));
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ---------------------------------------------------------------------------
    -- Read beamlet statistics
    ---------------------------------------------------------------------------
    -- . the beamlet statistics are c_stat_data_sz = 2 word power values.
    -- . there are c_sdp_S_sub_bf = 488 dual pol beamlets per beamset
    -- . the beamlets are output alternately so X0 Y0 X1 Y1 ... X487 Y487 for polarizations X, Y
    -- . the beamlet statistics for multiple beamsets appear in order in the ram_st_bst address map
    -- . the beamlet statistics are stored first lo word 0 then hi word 1
    v_len := c_sdp_S_sub_bf * c_sdp_N_pol_bf * c_stat_data_sz;  -- = 1952 = 488 * 2 * 64/32
    v_span := true_log_pow2(v_len);  -- = 2048
    for U in 0 to c_sdp_N_beamsets - 1 loop
      for I in 0 to v_len - 1 loop
        v_W := I mod c_stat_data_sz;  -- 0, 1 per statistics word, word index
        v_P := (I / c_stat_data_sz) mod c_sdp_N_pol_bf;  -- 0, 1 per BF pol, polarization index
        v_B := I / (c_sdp_N_pol_bf * c_stat_data_sz);  -- beamlet index in beamset, range(S_sub_bf = 488) per dual pol
        v_G := v_B + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
        v_addr := I + U * v_span;  -- MM address
        --Only read BST for g_beamlet and dual pol_bf 0 and 1 and for both beamsets, to save sim time
        if g_read_all_BST = true or v_B = g_beamlet then
          if v_W = 0 then
            -- low part
            mmf_mm_bus_rd(c_mm_file_ram_st_bst, v_addr, rd_data, tb_clk);
            v_data_lo := rd_data;
          else
            -- high part
            mmf_mm_bus_rd(c_mm_file_ram_st_bst, v_addr, rd_data, tb_clk);
            v_data_hi := rd_data;
            v_stat_data := v_data_hi & v_data_lo;

            pol_beamlet_bsts_arr2(v_P)(v_G) <= v_stat_data;
            stat_data <= v_stat_data;  -- for time series view in Wave window
          end if;
        end if;
      end loop;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Beamlet power of g_beamlet X and Y, same for both beamsets
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      pol_beamlet_bst_X_arr(U) <= TO_UREAL(pol_beamlet_bsts_arr2(0)(v_G));  -- X pol beamlet
      pol_beamlet_bst_Y_arr(U) <= TO_UREAL(pol_beamlet_bsts_arr2(1)(v_G));  -- Y pol beamlet
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window

    ---------------------------------------------------------------------------
    -- Log WG, subband and beamlet statistics
    ---------------------------------------------------------------------------

    print_str("");
    print_str("WG:");
    print_str(". c_wg_ampl                            = " & int_to_str(c_wg_ampl));
    print_str(". c_exp_sp_power                       = " & real_to_str(c_exp_sp_power, 20, 1));
    print_str(". c_exp_sp_ast                         = " & real_to_str(c_exp_sp_ast, 20, 1));

    print_str("");
    print_str("Subband selector:");
    print_str(". sst_offload_weighted_subbands        = " & sl_to_str(sst_offload_weighted_subbands));

    print_str("");
    print_str("Subband weight:");
    print_str(". sp_subband_weight_gain               = " & real_to_str(sp_subband_weight_gain, 20, 6));
    print_str(". sp_subband_weight_phase              = " & real_to_str(sp_subband_weight_phase, 20, 6));

    print_str("");
    print_str("SST results:");
    print_str(". c_exp_subband_ampl                   = " & int_to_str(natural(c_exp_subband_ampl)));
    print_str(". c_exp_subband_power                  = " & real_to_str(c_exp_subband_power, 20, 1));
    print_str(". c_exp_subband_sst                    = " & real_to_str(c_exp_subband_sst, 20, 1));
    print_str("");
    print_str(". sp_subband_sst                       = " & real_to_str(sp_subband_sst, 20, 1));
    print_str(". sp_subband_sst / c_exp_subband_sst   = " & real_to_str(sp_subband_sst / c_exp_subband_sst, 20, 6));

    print_str("");
    print_str("BST results:");
    print_str(". c_exp_beamlet_ampl                   = " & int_to_str(natural(c_exp_beamlet_ampl)));
    print_str(". c_exp_beamlet_power                  = " & real_to_str(c_exp_beamlet_power, 20, 1));
    print_str(". c_exp_beamlet_bst                    = " & real_to_str(c_exp_beamlet_bst, 20, 1));
    print_str("");
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      print_str(". pol_beamlet_bst_X beamlet(" & integer'image(v_G) & ") = " & real_to_str(pol_beamlet_bst_X_arr(U), 20, 1));
      print_str(". pol_beamlet_bst_Y beamlet(" & integer'image(v_G) & ") = " & real_to_str(pol_beamlet_bst_Y_arr(U), 20, 1));
    end loop;
    for U in 0 to c_sdp_N_beamsets - 1 loop
      v_G := g_beamlet + U * c_sdp_S_sub_bf;  -- global beamlet index, range(c_sdp_N_beamlets_sdp)
      print_str(". pol_beamlet_bst_X beamlet(" & integer'image(v_G) & ") / c_exp_beamlet_bst = " & real_to_str(pol_beamlet_bst_X_arr(U) / c_exp_beamlet_bst, 20, 6));
      print_str(". pol_beamlet_bst_Y beamlet(" & integer'image(v_G) & ") / c_exp_beamlet_bst = " & real_to_str(pol_beamlet_bst_Y_arr(U) / c_exp_beamlet_bst, 20, 6));
    end loop;

    print_str("");
    print_str("Beamlet output:");
    print_str(". rd_beamlet_scale                     = " & int_to_str(TO_UINT(rd_beamlet_scale)));
    print_str(". c_exp_beamlet_output_ampl            = " & int_to_str(natural(c_exp_beamlet_output_ampl)));

    ---------------------------------------------------------------------------
    -- Verify SST and BST
    ---------------------------------------------------------------------------

    -- verify expected subband power based on WG power
    assert sp_subband_sst > c_stat_lo_factor * c_exp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_sp)
      severity ERROR;
    assert sp_subband_sst < c_stat_hi_factor * c_exp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_sp)
      severity ERROR;

    -- verify expected beamlet power based on WG power and BF weigths
    --
    -- All co and cross polarization weights are equal: w = w_xx = w_xy = w_yx = w_yy
    -- With one g_sp either SP X or SP Y is used, so the other antenna polarization is 0
    -- Hence the c_exp_beamlet_bst will be the same for beamlet X and Y independent of whether g_sp is an X or Y signal input:
    --   g_sp = X --> w_xx --> beamlet X = c_exp_beamlet_bst
    --   g_sp = Y --> w_xy --> beamlet X = c_exp_beamlet_bst
    --   g_sp = X --> w_yx --> beamlet Y = c_exp_beamlet_bst
    --   g_sp = Y --> w_yy --> beamlet Y = c_exp_beamlet_bst
    --
    for U in 0 to c_sdp_N_beamsets - 1 loop
      assert pol_beamlet_bst_X_arr(U) > c_stat_lo_factor * c_exp_beamlet_bst
        report "Wrong beamlet power for X in beamset " & natural'image(U)
        severity ERROR;
      assert pol_beamlet_bst_X_arr(U) < c_stat_hi_factor * c_exp_beamlet_bst
        report "Wrong beamlet power for X in beamset " & natural'image(U)
        severity ERROR;
      assert pol_beamlet_bst_Y_arr(U) > c_stat_lo_factor * c_exp_beamlet_bst
        report "Wrong beamlet power for Y in beamset " & natural'image(U)
        severity ERROR;
      assert pol_beamlet_bst_Y_arr(U) < c_stat_hi_factor * c_exp_beamlet_bst
        report "Wrong beamlet power for Y in beamset " & natural'image(U)
        severity ERROR;
    end loop;

    ---------------------------------------------------------------------------
    -- Verify beamlet output in 10GbE UDP offload
    ---------------------------------------------------------------------------
    v_re := TO_SINT(rx_packet_list_re(c_exp_g_beamlet_index)); v_re_exp := c_exp_beamlet_output_re;
    v_im := TO_SINT(rx_packet_list_im(c_exp_g_beamlet_index)); v_im_exp := c_exp_beamlet_output_im;
    assert v_re > integer(v_re_exp) - c_beamlet_output_delta
      report "Wrong 10GbE output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_re < integer(v_re_exp) + c_beamlet_output_delta
      report "Wrong 10GbE output (re) " & integer'image(v_re) & " != " & real'image(v_re_exp)
      severity ERROR;
    assert v_im > integer(v_im_exp) - c_beamlet_output_delta
      report "Wrong 10GbE output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;
    assert v_im < integer(v_im_exp) + c_beamlet_output_delta
      report "Wrong 10GbE output (im) " & integer'image(v_im) & " != " & real'image(v_im_exp)
      severity ERROR;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    tb_almost_end <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);  -- delay for ease of view in Wave window
    proc_common_stop_simulation(true, ext_clk, tb_almost_end, tb_end);
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verify beamlet offload packet header
  -----------------------------------------------------------------------------

  -- Counters to time expected exp_sdp_cep_header fields per offload packet
  p_test_counters : process(ext_clk)
  begin
    if rising_edge(ext_clk) then
      -- Count rx_beamlet_sosi packets
      if rx_beamlet_sosi.sop = '1' then
        rx_beamlet_sop_cnt <= rx_beamlet_sop_cnt + 1;  -- early count
      end if;
      if rx_beamlet_sosi.eop = '1' then
        rx_beamlet_eop_cnt <= rx_beamlet_eop_cnt + 1;  -- after count
      end if;
    end if;
  end process;

  -- Count sync intervals using in_sosi.sync, because there is no rx_beamlet_sosi.sync
  in_sync_cnt <= in_sync_cnt + 1 when rising_edge(ext_clk) and in_sync = '1';
  test_sync_cnt <= in_sync_cnt - 1;  -- optionally adjust to fit rx_beamlet_sosi

  -- Prepare exp_sdp_cep_header before rx_beamlet_sosi.eop, so that
  -- p_exp_sdp_cep_header can verify it at rx_beamlet_sosi.eop.
  exp_sdp_cep_header <= func_sdp_compose_cep_header(c_exp_ip_header_checksum,
                                                    c_exp_sdp_info,
                                                    c_gn_index,
                                                    exp_payload_error,
                                                    c_exp_beamlet_scale,
                                                    c_exp_beamlet_index,
                                                    exp_dp_bsn);

  rx_sdp_cep_header <= func_sdp_map_cep_header(rx_hdr_fields_raw);

  p_verify_cep_header : process
    variable v_pkt_cnt : natural;
    variable v_new_pkt : boolean;
    variable v_error   : std_logic := '0';
    variable v_bsn     : natural := 0;
    variable v_bool    : boolean;
  begin
    wait until rising_edge(ext_clk);
    -- Count packets per beamset
    v_pkt_cnt := rx_beamlet_sop_cnt / c_sdp_N_beamsets;
    v_new_pkt := rx_beamlet_sop_cnt mod c_sdp_N_beamsets = 0;

    -- Prepare exp_sdp_cep_header at sop, so that it can be verified at eop
    if rx_beamlet_sosi.sop = '1' then
      -- Expected BSN increments by c_sdp_cep_nof_blocks_per_packet = 4 blocks per packet,
      -- both beamsets are outputting packets.
      if v_new_pkt then
        -- Default expected
        v_error := '0';
        v_bsn := c_init_bsn + v_pkt_cnt * c_sdp_cep_nof_blocks_per_packet;

        -- Expected due to bsn and payload_error stimuli in sdp_beamformer_output.vhd.
        if v_pkt_cnt = 1 then
          v_error := '1';
        elsif v_pkt_cnt = 2 or v_pkt_cnt = 3 then
          v_bsn := v_bsn + 1;
        elsif v_pkt_cnt = 4 then
          v_bsn := v_bsn + 1;
          v_error := '1';
        end if;

        -- Apply expected values
        exp_payload_error <= v_error;
        exp_dp_bsn <= v_bsn;
      end if;
    end if;

    -- Verify header at eop
    -- . The expected beamlet_index 0 or S_sub_bf = 488 depends on beamset 0
    --   or 1, but the order in which the packets arrive is undetermined.
    --   Therefore accept any beamlet_index MOD c_sdp_S_sub_bf = 0 as correct
    --   in func_sdp_verify_cep_header().
    if rx_beamlet_sosi.eop = '1' then
      v_bool := func_sdp_verify_cep_header(rx_sdp_cep_header,
                                           exp_sdp_cep_header,
                                           c_beamlet_index_mod);
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- CEP Read Rx 10GbE Stream
  -----------------------------------------------------------------------------
  -- Show received beamlets from 10GbE stream in Wave Window
  -- . The packet header is 9.25 longwords wide. The dp_offload_rx has stripped
  --   the header and has realigned the payload at a longword boundary.
  -- . expect c_nof_block_per_sync / c_sdp_cep_nof_blocks_per_packet *
  --   c_sdp_N_beamsets = 16 / 4 * 2 = 4 * 2 = 8 packets per sync interval
  -- . expect c_sdp_cep_nof_beamlets_per_block = c_sdp_S_sub_bf = 488 dual pol
  --   and complex beamlets per packet, so 2 dual pol beamlets/64b data word.
  -- . Beamlets array is stored big endian in the data, so X.real index 0 first
  --   in MSByte of rx_beamlet_sosi.data.
  proc_sdp_rx_beamlet_octets(ext_clk,
                             rx_beamlet_sosi,
                             rx_beamlet_cnt,
                             rx_beamlet_valid,
                             rx_beamlet_arr_re,
                             rx_beamlet_arr_im,
                             rx_packet_list_re,
                             rx_packet_list_im);

  -- To view the 64 bit 10GbE offload data more easily in the Wave window
  rx_beamlet_data <= rx_beamlet_sosi.data(c_longword_w - 1 downto 0);
end tb;
