--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Future note:
--   The dp_bsn_align aligns at BSN level. In retrospect it is probably
--   sufficient and easier to align at sync level. This can be done in a new
--   component dp_sync_align. The dp_sync_align then waits for the sync in
--   all streams and then declares alignment. The sync interval is longer than
--   a BSN interval so sync alignment will take longer to achieve. However this
--   is no disadvantage, because typically the system remains aligned after the
--   initial alignment.
--   Per stream it or for all streams together it may be necessary to have
--   monitors to detect misalignment. These can be used to detect:
--   . corrupted blocks due to input FIFO overflow by means of valid cnt from
--     sop to eop = g_block_size and valid cnt from eop to sop = 0.
--   . missing blocks or out of order blocks by means of the incrementing BSN.
--   . sync timeout (similar to sop timeout in dp_bsn_align).
--
-- Purpose:
--   Align frames from multiple input streams
--
-- Description:
--
--   BSN alignment:
--   The frame alignment is based on the BSN of the enabled inputs. Initially
--   and when the BSN differ too much then the inputs get flushed via s_xoff.
--   The inputs are then flushed for g_xoff_timeout cycles. Using a preset
--   timeout is easier and more robust dynamically determining the flush time
--   based on monitoring the inputs states.
--   When the BSN differ less than g_bsn_latency, then the maximum of all BSN
--   is determined and the inputs that have not yet reached this BSN are read
--   until they have. When all inputs have the same BSN pending, then they are
--   output per frame. After every frame the BSN alignment is checked and the
--   frame output continues if the alignment is still OK, else the function
--   tries to recover the BSN aligmnent.
--   The assumption is that the data blocks arrive almost at the same time at
--   the inputs. The g_bsn_latency defines the maximum number of blocks that
--   their arrival may differ. Therefor each input stream needs an external
--   FIFO that can at least buffer g_bsn_latency number of blocks, so
--   c_input_fifo_size >= g_bsn_latency * g_block_size. Typically if all inputs
--   are transmitted at the same instant then they will arrive with at within
--   +-1 BSN difference, but it can be more dependent on the buffering and
--   multiplexing that occurs for each input from tx to rx. Default using +-3
--   seems a save marging so g_bsn_latency = 3. Choosing g_bsn_latency larger
--   than needed costs extra logic for the BSN comparison and extra RAM for
--   the input FIFO buffering.
--   The data blocks arrive with gaps in between them, the block size + average
--   gap size is equal to the g_block_period with which the data blocks were
--   generated. This g_block_period is e.g. 256 cycles and is used as a unit
--   for the g_xoff_timeout and the g_sop_timeout.
--
--   Input FIFO size and sop and xoff timeouts:
--
--   For example for:
--     g_bsn_latency      = 3
--     g_block_size       = 32
--     g_block_period     = 256
--   use:
--     c_input_fifo_size >= (g_bsn_latency + 2) * g_block_size  -- of external FIFO
--     g_sop_timeout      = (g_bsn_latency + 1) * g_block_period
--     g_xoff_timeout     =  g_bsn_latency * 2  * g_block_period
--
--   The g_sop_timeout makes sure that an attempt to align the input stream is
--   ended if it does not succeed within g_sop_timeout clock cycles. The
--   worst case time to align is about g_block_period * g_bsn_latency, because
--   more time difference between the input streams can not occur during
--   normal operation. Choose g_sop_timeout +1 period larger.
--   The g_sop_timeout also avoids that the input FIFOs will get full.
--   Choosing g_sop_timeout too large may cause some input FIFOs to get full,
--   but that is probably no problem because the BSN aligner will then still
--   recover anyway. Therefore choose the input FIFO size a little larger
--   to fit all blocks that can arrive during a sop timeout, hence choose
--   c_input_fifo_size +2 blocks larger.
--   To recover from a failed alignment the xoff is used. The xoff makes sure
--   that the rx of new input frames is stopped and that the input FIFOs get
--   flushed. The g_xoff_timeout must be long enough to ensure that all input
--   FIFOs are empty. Hence typically g_xoff_timeout must be equal to the input
--   FIFO size, so about g_bsn_latency*g_block_size. However it is bad to flush
--   to short and fine to flush longer than needed. Therefor add a factor 2
--   extra to g_xoff_timeout and use g_block_period instead of g_block_size.
--
--   External input fill FIFO:
--   The BSN aligner can only cope with gaps in the snk_in_arr valid if they
--   occur the same for all inputs. Otherwise the inputs need to be put through
--   a dp_fifo_fill that fills >= 1 * g_block_period to ensure that after the
--   alignment at the sop the inputs deliver valid data for the whole frame,
--   so for every src_in ready there then is valid output.
--
-- Remark:
-- . Using g_nof_input=1 is transparent.
-- . The xoff is used for coarse flow control over blocks of data to recover
--   from start up mis alignment or an rx input that briefly misses a burst of
--   data. The ready is used for the fine flow per clock cycle to align on the
--   inputs at their sop's.
-- . The assumption is that data from the upstream inputs can be stopped via
--   xoff or have a dp_flush that can be enabled via xoff to empty all input
--   streams.
-- . The number of outputs is also g_nof_input. All outputs, including those
--   for the inputs that are not enabled, have the same sosi control signalling
--   (sync, valid, sop and eop). Therefore a downstream sink only needs to use
--   the control from one output, typically output 0. For the inputs that are
--   not enabled the sosi data fields are forced to 0.
-- . The input block sizes must match the g_block_size. Using g_block_size an
--   internally generated reference block is used to set the sosi control
--   during the frame output. The advantage of using this internal reference
--   block generator is that it does not depend on the in_en_arr settings.
-- . It was also considered to have a state machine per input, but later on it
--   was though easier to use only a single state machine and vectored input
--   control.
--

entity dp_bsn_align is
  generic (
    g_block_size           : natural := 32;  -- > 1, g_block_size=1 is not supported, g_block_size <= g_block_period
    g_block_period         : natural := 256;  -- number of clock cycles per block period or block interval (not used in the architecture, used in the comment only)
    g_nof_input            : natural := 2;  -- >= 1
    g_xoff_timeout         : natural := 1536;  -- e.g.  g_bsn_latency * 2  * g_block_period
    g_sop_timeout          : natural := 1024;  -- e.g. (g_bsn_latency + 1) * g_block_period
    g_bsn_latency          : natural := 3;
    g_bsn_request_pipeline : natural := 2  -- = 1, 2, ..., c_bsn_nof_stages, requested total pipelining of the BSN max and BSN min operation
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(g_nof_input - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_input - 1 downto 0);
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(g_nof_input - 1 downto 0) := (others => c_dp_siso_rdy);  -- only src_in_arr(0) is used for src_out flow control because all src_out need to stay aligned
    src_out_arr : out t_dp_sosi_arr(g_nof_input - 1 downto 0);
    -- MM
    in_en_evt   : in  std_logic := '0';  -- pulse '1' indicates that the in_en_arr user input enables have been updated
    in_en_arr   : in  std_logic_vector(g_nof_input - 1 downto 0) := (others => '1')  -- default all user inputs are enabled
  );
end dp_bsn_align;

architecture rtl of dp_bsn_align is
  constant c_timeout_max            : natural := largest(g_xoff_timeout, g_sop_timeout);
  constant c_timeout_w              : natural := ceil_log2(c_timeout_max + 1);

  -- Use as much BSN range as necessary. At the end of the range the limited range BSN will wrap. This will cause g_bsn_latency out of 2**c_bsn_align_w
  -- possible limited BSN values to fail alignment initially, but for these instants the alignment will be possible some BSN later. Using +2 provides
  -- sufficient opportunity for BSN alignment at the first pend_sop_all attempt and certainly at the next.
  constant c_bsn_align_w            : natural := ceil_log2(g_bsn_latency) + 2;

  -- Control pipelining of pend_bsn_max, pend_bsn_max and pend_sop_all to ease achieving timing closure for comparing the BSN of g_nof_input.
  -- . Use g_bsn_request_pipeline=1 for fastest alignment detection with only 1 cycle lost
  -- . Use g_bsn_request_pipeline>1 to more easily achieve timing closure at expense of more cycles lost
  -- . The maximum g_bsn_request_pipeline = c_bsn_nof_stages provided that there is sufficient data not valid gap between the input packets to allow for these lost cycles
  constant c_bsn_nof_stages         : natural := ceil_log2(g_nof_input);  -- = nof operation stages for determining max BSN and min BSN
  constant c_bsn_stage_pipeline     : natural := 1;  -- = fixed 1, always use at least one register
  constant c_bsn_request_pipeline   : natural := smallest(c_bsn_nof_stages, g_bsn_request_pipeline);
  constant c_bsn_stage_pipeline_mod : natural := c_bsn_nof_stages / c_bsn_request_pipeline;
  constant c_bsn_actual_pipeline    : natural := c_bsn_nof_stages / c_bsn_stage_pipeline_mod;  -- = 1, 2, ..., c_bsn_nof_stages, actual nof operation stages that will get a register

  type t_state is (s_xoff, s_align, s_data);

  -- Define the local registers in t_reg record to more clearly show the single registered process, conform the two process method of Gaisler. However
  -- still do use multiple combinatorial processes, instead of the only one combinatiorial process of the Gaisler method that uses variables.
  type t_reg is record  -- local registers
    in_en_arr          : std_logic_vector(g_nof_input - 1 downto 0);
    timeout_cnt        : std_logic_vector(c_timeout_w - 1 downto 0);
    pend_sop_all       : std_logic_vector(c_bsn_actual_pipeline-1 downto 0);
    state              : t_state;
    blk_buf            : t_dp_sosi;
    src_buf_arr        : t_dp_sosi_arr(g_nof_input - 1 downto 0);
    src_out_arr        : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  end record;

  constant c_reg_rst        : t_reg := ((others => '0'), (others => '0'), (others => '0'), s_xoff, c_dp_sosi_rst, (others => c_dp_sosi_rst), (others => c_dp_sosi_rst));

  signal r                  : t_reg;
  signal nxt_r              : t_reg;

  signal in_en_ack          : std_logic;
  signal in_en_new          : std_logic;

  signal blk_en             : std_logic;
  signal blk_siso           : t_dp_siso;
  signal blk_sosi           : t_dp_sosi;
  signal next_blk_buf       : t_dp_sosi;
  signal hold_blk_in        : t_dp_siso;

  signal pend_bsn_arr       : t_natural_arr(g_nof_input - 1 downto 0);
  signal pend_bsn_vec       : std_logic_vector(g_nof_input * c_bsn_align_w - 1 downto 0);
  signal pend_bsn_max       : std_logic_vector(c_bsn_align_w - 1 downto 0);
  signal pend_bsn_min       : std_logic_vector(c_bsn_align_w - 1 downto 0);

  signal i_snk_out_arr      : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal next_src_buf_arr   : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal pend_src_buf_arr   : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal hold_src_in_arr    : t_dp_siso_arr(g_nof_input - 1 downto 0);

  signal src_in             : t_dp_siso;
begin
  snk_out_arr <= i_snk_out_arr;
  src_out_arr <= r.src_out_arr;

  src_in <= src_in_arr(0);  -- all src_in_arr are identical, so use element (0)

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  -- control user input enable updates to not occur during a block
  u_in_en_new : entity common_lib.common_switch
  generic map (
    g_rst_level    => '0',
    g_priority_lo  => false,
    g_or_high      => false,
    g_and_low      => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => in_en_evt,
    switch_low  => in_en_ack,
    out_level   => in_en_new
  );

  -- block reference
  u_block_gen : entity work.dp_block_gen
  generic map (
    g_nof_data => g_block_size,
    g_empty    => 0,
    g_channel  => 0,
    g_error    => 0
  )
  port map (
    rst        => rst,
    clk        => clk,
    -- Streaming source
    src_in     => blk_siso,
    src_out    => blk_sosi,
    -- MM control
    en         => blk_en
  );

  -- Hold the sink input to be able to register the source output
  u_block_hold : entity work.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => blk_siso,  -- wired blk_siso = hold_blk_in
    snk_in       => blk_sosi,
    -- ST source
    src_in       => hold_blk_in,
    next_src_out => next_blk_buf,
    pend_src_out => OPEN,
    src_out_reg  => r.blk_buf
  );

  gen_inputs : for I in g_nof_input - 1 downto 0 generate
    u_hold : entity work.dp_hold_input
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => OPEN,
      snk_in       => snk_in_arr(I),
      -- ST source
      src_in       => hold_src_in_arr(I),
      next_src_out => next_src_buf_arr(I),
      pend_src_out => pend_src_buf_arr(I),
      src_out_reg  => r.src_buf_arr(I)
    );
  end generate;

  -- This is not Erlang or Haskell, but that does not mean that we can not do some functional programming in VHDL as well.
  -- By using functions that operate on the t_dp_siso_arr and t_dp_sosi_arr we avoid FOR-LOOPs here. However using
  -- functions can obscure the understanding of the code. Therefore it is important to define the functions such that they
  -- are generic so that they can be reused.

  -- User input enable control
  nxt_r.in_en_arr <= in_en_arr when in_en_ack = '1' else r.in_en_arr;

  -- Detect whether the sop is pending at the enabled user inputs, yields '1' when all enabled user inputs show a sop pending
  gen_reg : if c_bsn_actual_pipeline = 1 generate
    nxt_r.pend_sop_all(0) <= func_dp_stream_arr_and(pend_src_buf_arr, r.in_en_arr, "SOP");
  end generate;

  gen_pipe : if c_bsn_actual_pipeline > 1 generate
    nxt_r.pend_sop_all    <= func_dp_stream_arr_and(pend_src_buf_arr, r.in_en_arr, "SOP") & r.pend_sop_all(c_bsn_actual_pipeline-1 downto 1);
  end generate;

  -- Determine pending bsn range for the enabled user inputs, it is valid at the r.pend_sop_all(0)
  p_pend_bsn_arr : process(pend_src_buf_arr)
  begin
    for I in g_nof_input - 1 downto 0 loop
      pend_bsn_vec((I + 1) * c_bsn_align_w - 1 downto I * c_bsn_align_w) <=         pend_src_buf_arr(I).bsn(c_bsn_align_w - 1 downto 0);
      pend_bsn_arr(I)                                            <= TO_UINT(pend_src_buf_arr(I).bsn(c_bsn_align_w - 1 downto 0));  -- for debugging purposes in wave window
    end loop;
  end process;

  -- Use tree instead of folding to ease timing closure:
  --   pend_bsn_max <= func_dp_stream_arr_bsn_max(pend_src_buf_arr, r.in_en_arr, c_bsn_align_w);
  u_pend_bsn_max : entity common_lib.common_operation_tree
  generic map (
    g_operation      => "MAX",
    g_representation => "UNSIGNED",
    g_pipeline       => c_bsn_stage_pipeline,  -- amount of output pipelining per stage
    g_pipeline_mod   => c_bsn_stage_pipeline_mod,  -- only pipeline the stage output by g_pipeline when the stage number MOD g_pipeline_mod = 0
    g_nof_inputs     => g_nof_input,
    g_dat_w          => c_bsn_align_w
  )
  port map (
    clk         => clk,
    in_data_vec => pend_bsn_vec,
    in_en_vec   => r.in_en_arr,
    result      => pend_bsn_max
  );

  -- Use tree instead of folding to ease timing closure:
  --   pend_bsn_min <= func_dp_stream_arr_bsn_min(pend_src_buf_arr, r.in_en_arr, c_bsn_align_w);
  u_pend_bsn_min : entity common_lib.common_operation_tree
  generic map (
    g_operation      => "MIN",
    g_representation => "UNSIGNED",
    g_pipeline       => c_bsn_stage_pipeline,  -- amount of output pipelining per stage
    g_pipeline_mod   => c_bsn_stage_pipeline_mod,  -- only pipeline the stage output by g_pipeline when the stage number MOD g_pipeline_mod = 0
    g_nof_inputs     => g_nof_input,
    g_dat_w          => c_bsn_align_w
  )
  port map (
    clk         => clk,
    in_data_vec => pend_bsn_vec,
    in_en_vec   => r.in_en_arr,
    result      => pend_bsn_min
  );

  -- Hold input registers
  nxt_r.blk_buf     <= next_blk_buf;
  nxt_r.src_buf_arr <= next_src_buf_arr;

  p_state : process(r, src_in, pend_src_buf_arr, pend_bsn_max, pend_bsn_min, next_src_buf_arr, next_blk_buf, in_en_new)
    variable v_siso_arr_hold  : t_dp_siso_arr(g_nof_input - 1 downto 0);
    variable v_siso_arr_rdy   : t_dp_siso_arr(g_nof_input - 1 downto 0);
    variable v_siso_arr_flush : t_dp_siso_arr(g_nof_input - 1 downto 0);
    variable v_siso_arr_src   : t_dp_siso_arr(g_nof_input - 1 downto 0);
    variable v_sosi_arr       : t_dp_sosi_arr(g_nof_input - 1 downto 0);  -- auxiliary sosi variable
    variable v_blk_buf        : t_dp_sosi;  -- auxiliary sosi variable
    constant c_init_all_zeros : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0) := array_init('0', c_dp_stream_dsp_data_w);
  begin
    -- default semi-constant v_siso_arr controls taking account of the dynamic r.in_en_arr
    v_siso_arr_hold  := func_dp_stream_arr_select(r.in_en_arr, c_dp_siso_hold,  c_dp_siso_flush);
    v_siso_arr_rdy   := func_dp_stream_arr_select(r.in_en_arr, c_dp_siso_rdy,   c_dp_siso_flush);
    v_siso_arr_flush := func_dp_stream_arr_select(r.in_en_arr, c_dp_siso_flush, c_dp_siso_flush);  -- = (OTHERS=>c_dp_siso_flush)
    v_siso_arr_src   := func_dp_stream_arr_select(r.in_en_arr, src_in,          c_dp_siso_flush);

    -- assume if any input has a sync then all enabled inputs will have the sync
    v_blk_buf      := next_blk_buf;
    v_blk_buf.sync := vector_or(func_dp_stream_arr_get(next_src_buf_arr, "SYNC"));

    -- control user input enable
    in_en_ack <= '0';

    -- control reference block input
    blk_en <= '0';

    -- default reset timeout
    nxt_r.timeout_cnt <= (others => '0');

    -- default flow control flush reference block input and all user inputs
    hold_blk_in     <= c_dp_siso_flush;
    i_snk_out_arr   <= v_siso_arr_flush;
    hold_src_in_arr <= v_siso_arr_flush;

    nxt_r.state       <= r.state;
    nxt_r.src_out_arr <= func_dp_stream_arr_reset_control(r.src_out_arr);  -- default hold sosi data and reset sosi control

    case r.state is
      when s_xoff =>
        in_en_ack <= '1';  -- accept any in_en_arr updates
        -- remain in s_xoff for g_xoff_timeout cycles
        if unsigned(r.timeout_cnt) = g_xoff_timeout then
          hold_blk_in     <= c_dp_siso_rdy;
          i_snk_out_arr   <= v_siso_arr_rdy;
          hold_src_in_arr <= v_siso_arr_rdy;
          blk_en          <= '1';  -- enable reference block input
          nxt_r.state     <= s_align;
        else
          nxt_r.timeout_cnt <= INCR_UVEC(r.timeout_cnt, 1);
        end if;

      when s_align =>
        blk_en <= '1';  -- keep reference block input enabled

        hold_blk_in     <= c_dp_siso_hold;  -- the reference block sop is already there, because blk_siso.ready was '1' in previous state and dp_block_gen can always deliver valid data
        i_snk_out_arr   <= v_siso_arr_rdy;  -- wait for sop at the user inputs and then hold the sop of the enabled user inputs
        hold_src_in_arr <= v_siso_arr_hold;
        for I in g_nof_input - 1 downto 0 loop
          if pend_src_buf_arr(I).sop = '1' then
            i_snk_out_arr(I) <= c_dp_siso_hold;  -- flush user input until sop, then stop further input and hold this sosi
          end if;
        end loop;

        -- remain in s_align until all enabled user inputs have a sop pending
        nxt_r.timeout_cnt <= INCR_UVEC(r.timeout_cnt, 1);
        if r.pend_sop_all(0) = '1' then
          -- now i_snk_out_arr = v_siso_arr_hold
          -- use registered r.pend_sop_all to be able to use registered pend_bsn_max and pend_bsn_min to ease timing closure at the expense of an idle clock cycle
          if unsigned(pend_bsn_max) = unsigned(pend_bsn_min) then
            -- bsn are aligned, so start outputing the frames

            -- >>> Begin of same code as in s_data
            -- let downstream flow control the enabled inputs
            hold_blk_in     <= src_in;
            i_snk_out_arr   <= v_siso_arr_src;
            hold_src_in_arr <= v_siso_arr_src;

            -- output the aligned user input frames
            v_sosi_arr := (others => c_dp_sosi_rst);
            v_sosi_arr := func_dp_stream_arr_select(r.in_en_arr, next_src_buf_arr, v_sosi_arr);
            v_sosi_arr := func_dp_stream_arr_set_control(v_sosi_arr, v_blk_buf);
            v_sosi_arr := func_dp_stream_arr_copy_valid_bsn(v_sosi_arr, r.in_en_arr);  -- Copy the BSN of one of the valid streams to all other streams
            v_sosi_arr := func_dp_stream_set_data(v_sosi_arr, c_init_all_zeros, "ALL", r.in_en_arr);  -- Fill the RE and IM fields of the disabled channels with ones.
            nxt_r.src_out_arr <= v_sosi_arr;
            -- <<< End of same code as in s_data

            nxt_r.state     <= s_data;
          elsif unsigned(pend_bsn_max) <= unsigned(pend_bsn_min) + g_bsn_latency then
            -- bsn differ but are within alignment range
            for I in g_nof_input - 1 downto 0 loop
              if unsigned(pend_src_buf_arr(I).bsn(c_bsn_align_w - 1 downto 0)) < unsigned(pend_bsn_max) then
                i_snk_out_arr(I)   <= c_dp_siso_rdy;  -- flush the pending sop and remain in s_align to wait for the next sop, then stop further input and hold this sosi
                hold_src_in_arr(I) <= c_dp_siso_rdy;
              end if;
            end loop;
          else
            -- bsn differ too much, so flush all inputs and try to align again
            nxt_r.timeout_cnt <= (others => '0');
            nxt_r.state <= s_xoff;
          end if;
        -- check user input enable control (must use 'ELSIF' and not 'END IF; IF', to properly handle case where both conditions are TRUE at same clock cycle)
        elsif in_en_new = '1' then
          -- need to use updated user input enable settings, so flush all inputs and try to align again
          nxt_r.timeout_cnt <= (others => '0');
          nxt_r.state <= s_xoff;
        elsif unsigned(r.timeout_cnt) = g_sop_timeout then
          -- input sop timeout, so flush all inputs and try to align again
          nxt_r.timeout_cnt <= (others => '0');
          nxt_r.state <= s_xoff;
        end if;

      when others =>  -- s_data
        blk_en <= '1';  -- keep reference block input enabled

        -- let downstream flow control the enabled inputs
        hold_blk_in     <= src_in;
        i_snk_out_arr   <= v_siso_arr_src;
        hold_src_in_arr <= v_siso_arr_src;

        -- output the aligned user input frames
        v_sosi_arr := (others => c_dp_sosi_rst);  -- default force the sosi data of unused user inputs to 0
        v_sosi_arr := func_dp_stream_arr_select(r.in_en_arr, next_src_buf_arr, v_sosi_arr);  -- output the pending input frame of all enabled user inputs
        v_sosi_arr := func_dp_stream_arr_set_control(v_sosi_arr, v_blk_buf);  -- use the same reference block sosi control for all outputs
        v_sosi_arr := func_dp_stream_arr_copy_valid_bsn(v_sosi_arr, r.in_en_arr);  -- Copy the BSN of one of the valid streams to all other streams
        v_sosi_arr := func_dp_stream_set_data(v_sosi_arr, c_init_all_zeros, "ALL", r.in_en_arr);  -- Fill the RE and IM fields of the disabled channels with ones.
        nxt_r.src_out_arr <= v_sosi_arr;

        -- at end of every block
        if v_blk_buf.eop = '1' then
          -- check bsn alignment
          nxt_r.state <= s_align;

          -- check user input enable control
          if in_en_new = '1' then
            -- need to use updated user input enable settings, so flush all inputs and try to align again
            nxt_r.state <= s_xoff;
          end if;
        end if;
    end case;
  end process;
end rtl;
