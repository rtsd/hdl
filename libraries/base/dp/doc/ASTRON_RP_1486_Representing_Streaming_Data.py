###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""
Purpose:
. Numpy implementations corresponding to the examples in the document
Description:
. Similar to this file, firmware design documents could contain corresponding
  executable specifications.
Usage:
. python ASTRON_RP_1486_Representing_Streaming_Data.py
"""

import numpy as np

print '###############################################################################'
print '# Data: S[a][b][n][m]'
print '# . A=2, B=3, N=2, M=4'
print '###############################################################################'
S_abnm = np.arange(48).reshape((2,3,2,4))
print S_abnm, '\n'

print '###############################################################################'
print '# Data: S[p][t]  (in table format)' 
print '# . p = a*B+b'
print '# . t = n*M+m'
print '###############################################################################'
S_pt = S_abnm.reshape((2*3,2*4))
print S_pt, '\n'

print '###############################################################################'
print '# Data transposed : T[m][b][n][a]  (swapped index a and m)'
print '# . A=2, B=3, N=2, M=4'
print '###############################################################################'
T_mbna = S_abnm.transpose(3,1,2,0)
print T_mbna, '\n'

print '###############################################################################'
print '# Data transposed : T[p][t]  (in table format)' 
print '###############################################################################'
T_pt = T_mbna.reshape(4*3,2*2)
print T_pt, '\n'

