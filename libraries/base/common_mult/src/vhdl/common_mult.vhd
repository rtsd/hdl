-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, common_lib, tech_mult_lib, technology_lib;
use ieee.std_logic_1164.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;

-- Function: Default one or more independent products dependent on g_nof_mult
--
--   If g_nof_mult = 2 then the input vectors are
--     a = a(1) & a(0) and b = b(1) & b(0)
--   and the independent products in the product vector will be:
--     p = a(1)*b(1) & a(0)*b(0)
--
-- Remarks:
-- . When g_out_p_w < g_in_a_w+g_in_b_w then the common_mult truncates the
--   MSbit of the product.
-- . For c_prod_w = g_in_a_w+g_in_b_w the full product range is preserved. Use
--   g_out_p_w = c_prod_w-1 to skip the double sign bit that is only needed
--   when the maximum positive product -2**(g_in_a_w-1) * -2**(g_in_b_w-1) has
--   to be represented, which is typically not needed in DSP.

entity common_mult is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string   := "IP";
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    in_val     : in  std_logic := '1';  -- only propagate valid, not used internally
    out_p      : out std_logic_vector(g_nof_mult * g_out_p_w - 1 downto 0);
    out_val    : out std_logic
  );
end common_mult;

architecture str of common_mult is
  constant c_pipeline        : natural := g_pipeline_input + g_pipeline_product + g_pipeline_output;

  -- Extra output pipelining using common_pipeline is only needed when g_pipeline_output > 1
  constant c_pipeline_output : natural := sel_a_b(g_pipeline_output > 0, g_pipeline_output - 1, 0);

  signal result        : std_logic_vector(out_p'range);  -- stage dependent on g_pipeline_output  being 0 or 1
begin
  u_mult : entity tech_mult_lib.tech_mult
  generic map(
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_a_w,
    g_in_b_w           => g_in_b_w,
    g_out_p_w          => g_out_p_w,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_output  => g_pipeline_output,
    g_representation   => g_representation
  )
  port map(
    rst        => rst,
    clk        => clk,
    clken      => clken,
    in_a       => in_a,
    in_b       => in_b,
    out_p      => result
  );

  -- Propagate in_val with c_pipeline latency
  u_out_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_dat  => in_val,
    out_dat => out_val
  );

  ------------------------------------------------------------------------------
  -- Extra output pipelining
  ------------------------------------------------------------------------------

  u_output_pipe : entity common_lib.common_pipeline  -- pipeline output
  generic map (
    g_representation => g_representation,
    g_pipeline       => c_pipeline_output,
    g_in_dat_w       => result'LENGTH,
    g_out_dat_w      => result'length
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_dat  => std_logic_vector(result),
    out_dat => out_p
  );
end str;
