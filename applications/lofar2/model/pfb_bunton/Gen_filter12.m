c=fircls1(191,1/15.3,.02,.0003);
subplot(3,1,1)
k=length(c)-1;
plot((0:k)*12/k,c);grid
fcl=fft([c c*0 c*0 c*0]);
xlabel ('Time in units of FFT length','FontSize',10)
%title ('Filter impulse response')
subplot(3,1,2)
%plot((0:48)/48,db(fcl(1:49)))
plot(((0:96)-48)/48,[db(fcl(49:-1:2)),db(fcl(1:49))])
hold on
%plot((48:-1:0)/48,db(fcl(1:49)),'.')
plot(((0:96)-48)/48,db(fcl(97:-1:1)),'.')
xlabel ('Frequency (FFT bins)','FontSize',10)
ylabel ('Magnitude (dB)','FontSize',10)
%title ('Response of Bin 0 and 1 to monochromatic source')
hold off
axis([-1     1  -80    10])
grid
subplot(3,1,3)
fc=fft(c);
plot(0:16,db(fc(1:17)))
plot((0:48)/48,abs(fcl(1:49).^2)+abs(fcl(49:-1:1).^2))
grid
%axis([0     1  .98    1.04])
xlabel ('Frequency (FFT bins)','FontSize',10)
ylabel ('Magnitude','FontSize',10)
title ('Sum of correlator responses for Bin 0 and 1')
cl=interpft(c,length(c)*12);

%db(fc(7))
%clf
cstart=c;