-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2a_board_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2a_board_lib.unb2_board_pkg.all;

entity unb2a_led is
  generic (
    g_design_name   : string  := "unb2a_led";
    g_design_note   : string  := "UNUSED";
    g_technology    : natural := c_tech_arria10_e3sge3;
    g_sim           : boolean := false;  -- Overridden by TB
    g_sim_unb_nr    : natural := 0;
    g_sim_node_nr   : natural := 0;
    g_stamp_date    : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time    : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn     : natural := 0;  -- SVN revision    -- set by QSF
    g_factory_image : boolean := true
  );
  port (
    ETH_CLK      : in    std_logic;  -- 125 MHz
    TESTIO       : inout std_logic_vector(c_unb2_board_aux.testio_w - 1 downto 0);
    QSFP_LED     : out   std_logic_vector(c_unb2_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2a_led;

architecture str of unb2a_led is
  -- Firmware version x.y
  constant c_fw_version         : t_unb2_board_fw_version := (1, 1);
  constant c_reset_len          : natural := 4;  -- >= c_meta_delay_len from common_pkg
  constant c_mm_clk_freq        : natural := c_unb2_board_mm_clk_freq_50M;

  -- System
  signal i_xo_ethclk            : std_logic;
  signal i_xo_rst               : std_logic;
  signal i_mm_rst               : std_logic;
  signal i_mm_clk               : std_logic;
  signal mm_locked              : std_logic;

  signal clk125                 : std_logic := '1';
  signal clk100                 : std_logic := '1';
  signal clk50                  : std_logic := '1';

  signal cs_sim                 : std_logic;
  signal xo_ethclk              : std_logic;
  signal xo_rst                 : std_logic;
  signal xo_rst_n               : std_logic;
  signal mm_clk                 : std_logic;
  signal mm_rst                 : std_logic;

  signal pulse_10Hz             : std_logic;
  signal pulse_10Hz_extended    : std_logic;
  signal mm_pulse_ms            : std_logic;
  signal mm_pulse_s             : std_logic;

  signal led_toggle             : std_logic;
  signal led_flash              : std_logic;
  signal led_flash_red          : std_logic;
  signal led_flash_green        : std_logic;

  -- QSFP leds
  signal qsfp_green_led_arr     : std_logic_vector(c_unb2_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr       : std_logic_vector(c_unb2_board_tr_qsfp.nof_bus - 1 downto 0);
begin
  xo_ethclk  <= i_xo_ethclk;
  xo_rst     <=     i_xo_rst;
  xo_rst_n   <= not i_xo_rst;
  mm_clk     <= i_mm_clk;
  mm_rst     <= i_mm_rst;

  -----------------------------------------------------------------------------
  -- xo_ethclk = ETH_CLK
  -----------------------------------------------------------------------------

  i_xo_ethclk <= ETH_CLK;  -- use the ETH_CLK pin as xo_clk

  u_common_areset_xo : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => i_xo_ethclk,
    out_rst   => i_xo_rst
  );

  -----------------------------------------------------------------------------
  -- mm_clk
  -- . use mm_sim_clk in sim
  -- . derived from ETH_CLK via PLL on hardware
  -----------------------------------------------------------------------------

  i_mm_clk <= clk50;

  gen_mm_clk_sim: if g_sim = true generate
      clk50       <= not clk50 after 10 ns;  -- 50 MHz, 20ns/2
      mm_locked   <= '0', '1' after 70 ns;
  end generate;

  gen_mm_clk_hardware: if g_sim = false generate
    u_unb2a_board_clk125_pll : entity unb2a_board_lib.unb2_board_clk125_pll
    generic map (
      g_use_fpll   => true,
      g_technology => g_technology
    )
    port map (
      arst       => i_xo_rst,
      clk125     => i_xo_ethclk,
      c1_clk50   => clk50,
      pll_locked => mm_locked
    );
  end generate;

  u_unb2a_board_node_ctrl : entity unb2a_board_lib.unb2_board_node_ctrl
  generic map (
    g_pulse_us => c_mm_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
  )
  port map (
    -- MM clock domain reset
    mm_clk      => i_mm_clk,
    mm_locked   => mm_locked,
    mm_rst      => i_mm_rst,
    -- WDI extend
    mm_wdi_in   => mm_pulse_s,
    -- Pulses
    mm_pulse_us => OPEN,
    mm_pulse_ms => mm_pulse_ms,
    mm_pulse_s  => mm_pulse_s  -- could be used to toggle a LED
  );

  ------------------------------------------------------------------------------
  -- Toggle red LED when unb2a_minimal is running, green LED for other designs.
  ------------------------------------------------------------------------------
  led_flash_red   <= sel_a_b(g_factory_image = true,  led_flash, '0');
  led_flash_green <= sel_a_b(g_factory_image = false, led_flash, '0');

  u_extend : entity common_lib.common_pulse_extend
  generic map (
    g_extend_w => 22  -- (2^22) / 50e6 = 0.083886 th of 1 sec
  )
  port map (
    rst     => i_mm_rst,
    clk     => i_mm_clk,
    p_in    => mm_pulse_s,
    ep_out  => led_flash
  );

  -- Red LED control
  TESTIO(c_unb2_board_testio_led_red)   <= led_flash_red;

  -- Green LED control
  TESTIO(c_unb2_board_testio_led_green) <= led_flash_green;

  u_common_pulser_10Hz : entity common_lib.common_pulser
  generic map (
    g_pulse_period => 100,
    g_pulse_phase  => 100 - 1
  )
  port map (
    rst            => i_mm_rst,
    clk            => i_mm_clk,
    clken          => '1',
    pulse_en       => mm_pulse_ms,
    pulse_out      => pulse_10Hz
  );

  u_extend_10Hz : entity common_lib.common_pulse_extend
  generic map (
    g_extend_w => 21  -- (2^21) / 50e6 = 0.041943 th of 1 sec
  )
  port map (
    rst     => i_mm_rst,
    clk     => i_mm_clk,
    p_in    => pulse_10Hz,
    ep_out  => pulse_10Hz_extended
  );

  u_toggle : entity common_lib.common_toggle
  port map (
    rst         => i_mm_rst,
    clk         => i_mm_clk,
    in_dat      => mm_pulse_s,
    out_dat     => led_toggle
  );

  QSFP_LED(2)  <= pulse_10Hz_extended;
  QSFP_LED(6)  <= led_toggle;
  QSFP_LED(7)  <= not led_toggle;
  QSFP_LED(10) <= led_toggle;
  QSFP_LED(11) <= not led_toggle;

  -- red LEDs on bottom
  QSFP_LED(1)  <= '1';
  QSFP_LED(5)  <= '1';
  QSFP_LED(9)  <= '1';
end str;
