library ieee, pfs_lib, pft2_lib, tst_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use pft2_lib.pft_pkg.all;

entity tb_pft is
  generic (
    g_fft_size_w         : natural := 10;
    g_in_w               : natural := 12;
    g_pfs_w              : natural := 18;
    g_pfs_coef_w         : natural := 16;
    g_out_w              : natural := 18;
    g_clk_period         : time    := 10 ns;
    g_rst_period         : time    := 25 ns;
    g_tst_data_dir       : string  := "../../../tb/data/"
  );
end tb_pft;

architecture tb of tb_pft is
  constant c_pft_in_file   : string  := g_tst_data_dir & "input.txt";
  constant c_pft_out_file  : string  := g_tst_data_dir & "output.txt";
  constant c_pft_ref_file  : string  := g_tst_data_dir & "reference.txt";

  -- Signals
  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal val            : std_logic := '0';

  signal in_x           : std_logic_vector(g_in_w - 1 downto 0);
  signal in_y           : std_logic_vector(g_in_w - 1 downto 0);
  signal in_val         : std_logic;
  signal in_sync        : std_logic;

  signal pfs_x          : std_logic_vector(g_pfs_w - 1 downto 0);
  signal pfs_y          : std_logic_vector(g_pfs_w - 1 downto 0);
  signal pfs_val        : std_logic;
  signal pfs_sync       : std_logic;

  signal out_re         : std_logic_vector(g_out_w - 1 downto 0);
  signal out_im         : std_logic_vector(g_out_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_sync       : std_logic;

--   SIGNAL ref_re         : STD_LOGIC_VECTOR(g_out_w-1 DOWNTO 0);
--   SIGNAL ref_im         : STD_LOGIC_VECTOR(g_out_w-1 DOWNTO 0);
--   SIGNAL ref_val        : STD_LOGIC;
begin
  rst <= '0' after g_rst_period;
  val <= '1' after g_rst_period + g_clk_period / 2;

  clk <= not clk after g_clk_period / 2;

  in_dat: entity tst_lib.tst_input
  generic map (
    g_file_name  => c_pft_in_file,
    g_data_width => g_in_w
  )
  port map (
    clk      => clk,
    rst      => rst,
    en       => '1',
    out_dat1  => in_x,
    out_dat2  => in_y,
    out_val  => open  -- in_val
  );

  in_val  <= val;
  in_sync <= '0';

  pfs : entity pfs_lib.pfs
  generic map (
    g_nof_bands              => 2**g_fft_size_w,  -- 2*g_nof_subbands,
    g_nof_taps               => 2**g_fft_size_w * 16,  -- 2*16*g_nof_subbands,
    g_in_dat_w               => g_in_w,
    g_out_dat_w              => g_pfs_w,
    g_coef_dat_w             => g_pfs_coef_w
  )
  port map (
    in_dat_x                 => in_x,
    in_dat_y                 => in_y,
    in_val                   => in_val,
    in_sync                  => in_sync,
    out_dat_x                => pfs_x,
    out_dat_y                => pfs_y,
    out_val                  => pfs_val,
    out_sync                 => pfs_sync,
    clk                      => clk,
    rst                      => rst,
    restart                  => '0'
  );

  pft : entity pft2_lib.pft
  generic map (
    g_fft_size_w   => g_fft_size_w,
    g_in_dat_w     => g_pfs_w,
    g_out_dat_w    => g_out_w,
    g_mode         => PFT_MODE_REAL2
  )
  port map (
    in_re          => pfs_x,
    in_im          => pfs_y,
    in_val         => pfs_val,
    in_sync        => pfs_sync,
    switch_en      => '1',
    out_re         => out_re,
    out_im         => out_im,
    out_val        => out_val,
    out_sync       => out_sync,
    clk            => clk,
    rst            => rst
  );

--   out_dat: ENTITY tst_lib.tst_output
--   GENERIC MAP (
--     g_file_name  => c_pft_out_file,
--     g_data_width => out_re'LENGTH
--   )
--   PORT MAP (
--     clk      => clk,
--     rst      => rst,
--     in_dat1  => out_re,
--     in_dat2  => out_im,
--     in_val   => out_val
--   );

--   ref_dat: ENTITY tst_lib.tst_input
--   GENERIC MAP (
--     g_file_name  => c_pft_ref_file,
--     g_data_width => ref_re'LENGTH
--   )
--   PORT MAP (
--     clk      => clk,
--     rst      => rst,
--     en       => out_val,
--     out_dat1 => ref_re,
--     out_dat2 => ref_im,
--     out_val  => ref_val
--   );
end tb;
