------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.unb1_board_pkg.all;

entity unb1_board_mesh_io is
  generic (
    g_bus_w : natural := 3  -- use 4 to include the CMU transceives at port (3) else use 3 to only use the HW transceivers on port (2:0)
  );
  port (
    tx_serial_2arr  : in  t_unb1_board_mesh_sl_2arr := (others => (others => '0'));
    rx_serial_2arr  : out t_unb1_board_mesh_sl_2arr;

    -- Serial I/O
    FN_BN_0_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    FN_BN_0_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    FN_BN_1_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    FN_BN_1_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    FN_BN_2_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    FN_BN_2_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    FN_BN_3_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    FN_BN_3_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0')
  );
end unb1_board_mesh_io;

architecture str of unb1_board_mesh_io is
begin
  -- Map the serial streams to the mesh
  wires : for I in 0 to g_bus_w - 1 generate
    FN_BN_0_TX(I)        <= tx_serial_2arr(0)(I);
    FN_BN_1_TX(I)        <= tx_serial_2arr(1)(I);
    FN_BN_2_TX(I)        <= tx_serial_2arr(2)(I);
    FN_BN_3_TX(I)        <= tx_serial_2arr(3)(I);

    rx_serial_2arr(0)(I) <= FN_BN_0_RX(I);
    rx_serial_2arr(1)(I) <= FN_BN_1_RX(I);
    rx_serial_2arr(2)(I) <= FN_BN_2_RX(I);
    rx_serial_2arr(3)(I) <= FN_BN_3_RX(I);
  end generate;
end;
