// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip", encrypt_agent_info= "Riviera-PRO 2015.06.92"
`pragma protect data_method= "aes128-cbc"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC15_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64", line_length= 76, bytes= 256)
KmmBA/OMHXG5rkt4ZaBP9sQ3xzE9AgKLZJwa88Syy7yNaYd8adSYVptrn4scJFUBNPqebGlFVms0
yh52g89RKLRBLRQ8+SbOgR+Y5J9YR4OMv+VfSh92wEb1HYps98dcpWhjisrd1GdQAk+jmMoU19A3
sRQ+u9odVNC2GvtfIwkMjcZw/F8Wrt/vvSylS8D8bzEe+kA8Fz9xUMAMbOV9xHPKOFK6qDYd0lri
iqjjltzLmD9pDxXo2dmGkN/N0kbpkg109VLBUM7W2mvsbIhrRHhe7xp6yg6628aJCC2O3XRznlKq
z5hqA7ZglmIOjxz5+NB32ptCA12DMgfywTYtpQ==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_block encoding= (enctype="base64", line_length= 76, bytes= 7568)
n8XoQ337ZsMvsJMtQA0PTldsiT8SVtA6YUgPPMnGY0LEp23ulZZGofTKAjwG5BotcGX9tjVNvpMQ
7q/shwsOBoZ0IZVAUuZ58N5RS3kkdb1KDvGRvlkD/dzBozBbUQ8agiL26aCZb+ER8DuDu6RAgkei
m8JeslK/+mYdWcLGjlGhLNVy/QqxaODL9huMa6xzGAZ2fx6rwk9+4A/e0VI8aYNnjd7H/vCzGWs4
bCf/HMHNTMJLtiG8FILxHITUuXAW2KCTz20oy0h7u7h1zOZZgDvFo5Q5bfYGZXd0Xd0lnoDWO4ar
qftwmSjXM9Nggf0D3qDwoP/6+BkJv/swhUfe5u+VoTCsqZKvedtLoU/y8XqP1NFl8GaW/hpV8WNm
7lBMfXKA4vBj99XkmqHbXg/V0+5Tv6eCATXBGgJrpSUNni0B2ls5ZNZyGkz/pNejKJpuRmTZrCGU
6b3v/tdylX3Hn6piDb9IBoeOcoSDFbV3illFlegRnw0LRV0FeoaWxIyLww8NV2WpjPrK83Ys4V/g
w1WZtqwOUCZ5OE3I0Pe0illFdPsTeDh2lCxpANwk3igKwpDY4eEb0uWL5Qh6PZVc3XEZe7NWKWok
UjphBRrCsXAo8r/SjnBAJPKy4OHJ1tpnp1ePg0OlV18ALVay9ZPsUOSdZlMpbhFzWnx6b3Q3fODM
1rSRy9BXahUI4I4P9A9BHobJ39EB3fvDAA08sE5BOCN5ih7DbWaTczcdQINsz4J8GoKnvy6HchOu
RMQnD9OnZUWIyfI4j6jQiXV0h9V1/4pV8p67uyp0FFDIr8aRiOQYf9qWFplsJQ9N/k3B1hQoMVc/
CGxqHC1kocP/HX9+Qx3FkRYaKdssfbkjPmRi2k82ziydCGxlwXHRXBKQ8ZNXlgDvbYtUa3eeBzE5
nCt7pFudayF8XB2CB+ubFXKoaXB/C7dMVsL5n5uBOAVhSAKI/pTactVTionBlPyv278wViFyaeDR
2PCZlHJYLs04q2xwN3tt1Cv0h4TrJ8Lus8qETt9VcLBlGb+vdDhNbzX/C61pGrvPud0HxcX+p6PV
6flWbdApBWZXRWqpQniZ51+s0cui1wAiuZKmoqxBbUappIU5hITvthNCh9wNCplBZFi7aWgM4+t9
S2ly6cEsI9HC5orHQMymXFK0B72iXhP68/+LggLab4bmiIog0RjWTkEq+Uw6vtgUTcAFc6I981Bn
QTtuI703c5xQuYXn/p3cPVMP4GOxLYzp8nLyZoMqnPDhmvhG32q30qck3pCMS7WcDYM/EEOR9aWG
P5p0eIr27yKyCiAR+9AilyUzHFWGFpinrJBpksqlsgt+7SVJX70qmRqgg3h61On+pzq5/MA+BN06
yBV5P7XKmhR5/cNDoiwib7bJNy2w24hLkMi7BIU2zfiCmQIUQYNfBD1QZ+dfU6GOxEIoEICpTEBd
1GYl069VCLRg6HjsO9k7LJfBrrTs1CEH9PprX1YjkDFm/BaOLp8ELU46nehpwk1P52lXuo/+nMiw
PcePVv4xxZykUWVBXPCgXn4SWdgKWPlGyHxByjwP26CPjuCd4CYMTJ61ok3RRUmKPvaALVwl2Dlo
LfIU+nkhQ4vFW5E8aJgFsKa8NWIpSTTz3OjwxJQZzsiMW+6rRtHLSjxnpugYtDKVnh3RwU0Cik9s
5PC9onq7p6xziEI2rl7CMwdLhDz5rwwbq7vARRgoukJublqT7PH5Qj4UWoD3QX2oDeLNqFXgFKq2
aAE9+GEyP15OZJJDTYkzueiJ3lOZzP4o/r6yymHNnAzHgrZtWNGpVOv1joJ75iw/Zy1MnuaA00U/
aqUFwdN2QLGgORYbqUFAg3V/rZwDtCxc1ma6gZ8X2UM0BzjFeQDSWOe3yns66kR3lY2ZN9/U4E1q
Hqi/Na6cRFLEsXWat/5mkawUYXkXAq00fEU5tLhClrOYLSELxRIm1kRbp64irhBfN8LoGNXaVosw
EnaiAo9y6GzIUqhrnddHF3gPjHsIY5pYxtX+9qyNejrpO0bGPreKKIWCzaGs3VqF9K7ot/EPClLn
u4UhTRZCTryvbx7tUSJG1DAJf/tN9OI5L+O1TXh+LtUDw7JCNNbJOC1SKBJ4ek6UQf3X87IU9sLR
oyvq0+oTzl+vfMlBuVgXVucmVZyX8ApVOWf+1VPRvyGbFUih1HBU155V6RCWz6KdTDBoMBDYZvEz
DVZPGT+0W7SnrUZS25DihWnWc7WskFEyUXFMqh5q+CINZ5zg0Jyl47FAqHv0frEaVFlVwuf764ds
1UoGohTYyTMPlQFKbp4Y4q66JWGYcanCSLRVvxggSN0fC0VpvlzaEuetbUF6JhmCBAnqXhhrptAX
Brkmp5LHJK6mr+iIarcMzgFdUlak/H2qYfCFMaXDfaHQ900wKhMNF9F1S51aPNHYSlxpVsFGQ7gO
QEUBoCsRrBJOOm7UHdqPuEitKAsG/VxympNeltRT6FCEUfL8mKi7WECO4G6eEfch3wDGia7MMfg3
EaiiGZuOd05341vuF/JqaV7BqZD1dqhDup6LyaQ9nyYJ0cpN/XACzvy1Rm/jEpMShMt7+ExAWvjm
+3eXcE/6zlBXmVLG4KTxPv90fisXcp8fxeXaEY0nXEBQGrxY5/90TPf1Noo2gls1htBxAbLH/dss
WF7mhq2cRf7MmY/ZTkAyOoSTIkvfhskHdXtwaB5cm2gdYxKrhmtZ0F0ZkCiQ+75Jgjypqsqx3y1M
M9xoMVzLqHjU6Hideftz2jPn35H55fCyDqzmOtrKHTHu/TrD4FTnCQzjnJuWQ2cHYFKgGdBdKLfr
ARCCjhp4kVGfEXg6UPK38GxEPMy1rLcajs1p+0svT/inv7SxTzXAEELjwu1RU3r6xEfh71SypFer
Gvu2iHxIUlzXfb76Mo3/fQKB4f8tDyDBdgqdmicC4bxMaDHNnetaQ1ubOUqZ51/0oJ4yZQWSMABX
H7bqdiSAFILTi6u17JkS79GMUaY0YwV+rhPCfU7AnGSe7nOii5pw/IrK7iZAHPE2lHQv9f1V/tI5
z7d+f8mHkR+rgsRfPYOFHT4gbYFeWranDOKUlQ11ntK1LLdYeb38lYA6EelB0HZopz6PJRo8oKIq
Fc5XiAmUeEQBUUa+6Ku2Y9h+lKyJqZkoiJYSUIXJDtujabunPXE7RvsXwmgGbcONovr/NvonrH67
a9ciKr0jwideYruEmA4bpvf5vqNhuyND2ati3PmhZfHdRT+AszFnD88OV5iPySAGCaBCfynSW/F3
MiexLeBrahSNqabq0WL+V6M0ax+o9Yh2nU0l0sJJQce8+2PDIP/ePmnuoDv3zk5qPixS+K+4xJSm
rMY5YRxYmFD0D1omGz4epRToC73gMZruU22E3Z7nQQnO9d1vByrMEDHcERPUrBHAO54K1MZW38In
aTDS9Pq0sjp4HOPl53WCFlFyJxE+bH6aW4W4u9OGOQCl2xBR+IAdAbeH6sJpOkCSgnnBZUpUIa7Z
71jvHtI+9jlvahQReL0obb52FpZhQZLa0zMcYTlWjek3HjDLSW8uk/7f2GpUUm40IZm44T01GLc5
rB35eTQXRG7o43ukP53cplQnwpG3BabHcj6r03OF1OOV0yPZ/7dctTH9/PFgyn2Phj03VD8m0O6/
l9pN/AMRkfDQpQ8EdbnTwS3g1cihU2t07LI74Bsr+gBxp8EcIAtDkWV+QyytmzHhz/OrCMPIb696
qlEjlAm9nWJ7EhOscpbaGUpiwUWcUIdfVam9fMoEC+CIWghGeYoDNn0em6mBTyp8WkrlEiX0juH0
YHBeNmnj6SX03/u12lae6+sX9rVRTvGYXQBSfzjH1ZPLBezI7DtgDF6ZKQ88OpY6lUfXPLnPUeRy
2Q5gABV6+D++vXDhTe6qhyVnYRKRO0CAyBtGnZYY548SNC3FDFwtmB4iRAcE+N8m+Fk01/vzrgVT
/v7kxIu/D2U819Yr/u2wCHPmxjzr+/iVe8YcQycQP6EJItptDe6Nl2JoLlvOrBo5RnfQMJMs1jRd
SkNCw4d6C++0ldrKDD25kR5vpDoE2AEI/yo4gWibCW19tF3QOpj/wjabQYj4VBnPTC4ypE9ZEDxP
Hi3qbybRB0nm1135lqpXSwUgVy2hvC3yTZzo7gIF0ptUdHAK2zucmnl1g4aF9FvOlPBac8A17FDU
Mnhyw6P1Jb+TzeKl5GrkvuZg++Oaxo1VM3vYhkgbAg5iYUHyICuKrBTZj1oInWfv1Es1UtMMESz4
AdNqbgqXde87RI/aqC7yb23xq2DpKAYzr+pI8eIHFuKCQSQvmw46Irtia/jDjwT6F+GbQmnQaY6X
NMQObOhAU8m0oEkcfJ0TWARxyTzE+RJeAsmQQ12Ar9xxWgGcSzFkcyxraDnbFqcVp0wt99GCEuvJ
daBjdtY6HPE60jLHJJaoOmxFgtNSLRRG+uGfLfmjdNv8jWC7xj8mc6L9C60JSgvzfWU5A74Av+Z+
VECPr8w7/juPEb1XKSpypEiz8wgpPYYXhNhYh1o857AV3ADXOBrsRz+EvPXMJfJbCiQFTnZCE1I3
qtbk00d0/k2FPyek04Kl0eQ474Fhh3SG4ncSskEq7fL9CaBT+mAAOiLsp5S465Oy5h4sP/Q9B3UV
HRVdApe2JbhQMDCBk5a0CG+pL6Gy209MivzBUvXnerQxacwjTNLTZhn9rEqb1czyAKDdCtbHUAiS
0P27YjI/btXsMpTM5m98d1sl06TH62+4bnhVDJVO4MiPDfc1N/bF8q6VRUZ+bliKR4fZEmyEA9FU
sr2DtLkXssQvZgLFL3eBQXXVtTFx1Qj320fxUjzf9h0jlFw3RmS6kniOgei4dk/v1fRnlp96Dgdo
D/fgHBJ0Fa2xeUsJdtA3JR5z533m2JmedRogH3KdXaV9TwK0tW69ZbpGoU9WzjHjl4UxFBralPCJ
tzlLtIjA08zfZ/8/vs1q9HfPnCdMgwZGp9OnVC55DiC5bY6B9dCE6ILa6mAVIJ22L/X1j1mkSYDs
VKOp2/NJUwoIOVuwKcKeuepISWr4EjoHUJ2gtQkFFw4gyrZ+26SlkI1wqp9t5nBCWt+c4rxu0mRD
2hESEVy4tz7Yei9eq26/rK6Mvs8LG6NCQ5XtQywYKLnRqawPDZLIEXHgYyCiRZM1WA7KF1wYlrx/
FSPGxsyY8woZ5b+I0zeUIIbYTWvXnFzNkixhHy5wQPZtO/d7I91YhmDFTznceZ43zcQX1BApJXpE
ZnX6LFnB96s+2nSkr0rE9V9aQ8wxUMNmut8dxhwaEQklu6gJYcOyqddywt+za7OXPOnIJKgrD1Pn
WTzcfZKsrSVvrcUgvf6Xbd70YYDl3sDL3uNFbvtK/QDwW+TPf69bXpi6mkjXmg7pODiOjEsDHR1E
ZU8MRt9UqWP07h2c0Hvs0DJ878qeKUxzoCkVgVzuxVihgE/yEZMQTWpXLsjC9VVVX9zMmy3g4aJt
Ec5NN/FbI0xsbf8cWlzrMjTU1M90S6SDV1sQ0XO8VR4UswogMSaFYIaku9Vw7a9aRowhZA67RGlb
tQuab83jG0YYyeiKurGTyWa7Inwag/ajPEwXPpt7fEuFIZQ464wW++Yhr9bKoIqNfYzBLdIrqhzw
z8JEjMQkgv7qnc3h/wHELzZC8sLMf5gzbzLKcUtSEQN4tv/65qviSu6uDgGL3rmjWOXGjrP85Aw2
WkxGYLnwlemSnPkzwDyowK9KHicN3q4qHH6htezmV41aGzyX0uIX3Or5zOwCxjPoFnAv72T9jdfX
x1JqIomaPG94HrKS0LQzqV2GdyAZweOnfbpmRAKTGkADy0/O+WrIKzqjFJiRaFLyQfGcPeUs52+R
dCQ1J1K+CQx8kpU3pjlRgXY5UTANJJrIRoIyz/mficywDSPYeWnOm2kBY282DoKcBJFvp/duR40D
8MhwxAqkCi3L3sAQYbLb83gpX6PS+8fd9RX6J8k7wJRqBLq87eCWpTobXxX58bSzAmtBY0d2SVFp
vKlUNk9y7bjrP06BHlRONUoMQqwiMqg5wAD8ohXuad+bv0BmuF3k/gwnf2OpUMXYHcKppnnCdjtA
sNkx3/PTkYuu+F0n5De0s+JZrR86t9MVZpn88HZd5KZGIiCKr5dU+aZpop1dyrVfb4szB5PbYoYs
uD6mfs/qdg8D+cK68MpW6iuhY8oLVGc7sUK6ukvkI3uTrlRhB9ONCjN3LX2sDmha3aZjBqkOZziC
wWvi8Hz2IosXKyArgj55P988JJufNBQce/Je4+hWEVOgbjOhEXWU+qbavpDLbzlqExxtyxDjdQLf
qZnSlCaEWZG3XS7eCysrIWvqNEb0JLPZTLZeMcroMrOLpbTwIc5FAw3/mXKtGm7u9IYs1pwd6ZDL
cITO54oDzB8tq4lGvGvJL2CUflMFc2fu9UEey//nPx7u5c59pM2IMph2pf/OzuF9QRof0aPmJ4Qo
W39aF4thc6Grg3KreRP2RKGbDbhnSWEqARuiByZaNq9HPVSkzAwbXCoLSvzRZ1YGcaqYmKxFdGHX
nlCDI9b/6K56PLxqn+4ruDd6ZQawk7WVLHc14K6Vrhos/n9vV1P3klyaDWt14wRs83pkM+z+ZHDT
MSs/dK7NbLzXziTG4R9S07dxjQ2QygxjUlFpMcAYirwhXwfWzDhH0aWlrnyNCMR6wSl0Wk5VR3A3
ZgT8Rw3mqQOLSawPXJQ3KIVgXKdvIPWJWjL/MXj8bFy8Ily6SeDbRtEDoiNeoW/p618lkCJBa/2K
q0koiqoqiFHvJ5BAhYU5NVCc2uP1CKLoy/QoaZImmMsupyVw1BDp9ix832dcb1ZcjzG7+JzCefTQ
eqGe9WCSzJgO36PhmrP6lAb5g6rPKHh+HT5xvsCQ5odaTrpeRryhMyI0qoj97/rfZCj9EdGZt0lE
GpvS5rjpw05dWjAr4jwAqHUcCddq2gaJUs6UfxWkHGZOJQ7b1CEaLTyXioIRdbhEz0kYhccrrkgu
vs9L7ik+7UEB0epXPMeiufON2qhf1KkOjSJY+/B4j5FoIxxKFbIlHDJuucgzAaTOnODWVnZY1kFM
nB6yzYF3gQqabHczSDCIp8ZkA0Sax1k4/AJvTGpRQg0L0O18ThPJPFsxPD7y6xs6XLmIPcZZ06VD
Jt2tKDQUKJZupqDjiGHB9gy/fafEw3U+/1UJGL1t4a2c5Gkd2D2xWdMrwtZoJHeWi6uMlC80+Xds
ZH+lyxpw5Yfb0y04r6/3qa0B8VOeMUQnJuKYVcenVQvGDC+QJWudbPkMjBrCgonwCsmySGCQfS4a
JYaTTxCGlFOLC/VUBmfuomdpKKlKb0bdhHQiJW4DOfL5D4aiE9iGYe0bwuJ2XZerZlkRAP/MS7T6
nS2cCN7iCNVGklLeNWUK3iY6Embh4efOobn/hOzMMVta9OENhaJO07dS9zxUqJ2wlBrzGQWfiF/2
7Sc9NE4YCEoGklSwobSJPhzYT9OkMJl+ZJ13AIUmSFozoSYGGsUkIp/0fBL/HNAnEZTKXFKsDYBF
4PlFiJd2QwxlvZTGF/s1bdz6kfNoHYLrVZ+V03SRXOnrBTd7Cots19u/6IQX0uY4Isa+W2+hVdRB
6X1jeNI+ozyWEAQU/q1BhA/XyKYMBxjbWdjiKhXPuejWRB5JfEORSeSb7Bm7f9Bs1Ngx2/LrFi38
fs6Uo2q114pp8xVZH9lyBMyOa95RhRJl5U1cDfTQTZaEsv/+KnUMc8Jx1cR9UzN6O9p5fTc1tQzl
vF1+njtqnJ4C9soakBqrnNiCLp2VBDPXYTGhZMkxJP/smqzXb4i47JVoxXFPTevydYLwIN1leg27
cHCGba22HPuW7ht6QdesHDX7zI/Tq+xqmMK0Dn5v5LB5wOtaWWwNljBLlhVgBXa1gJPH6cnpUnqs
nuFEU5VSTwMNZ1LBCp8jOmdMa1r2uZhsnFhmCYDSPmR0dgzIoqFPSfAKlEY3R/tdFZxTcoDYnZ7T
BW7hll1TIKzMZC1x0vFneOmKKwm5qPVcLhcYbp2pxxOZ+xRCkrRTiEf9XMH0kUeEHmeltHGJK9T4
okk4TroiOXpWuA7HHgz0eWxRv+4pRDIwQtlLRkbOQe/vFbXhUvt1Qr8iDFnmtSzyGJo7GAFT8CzP
uWQhFSwWw8ox1PXYvJ37Eir/4NUmDkWdPSI84cRfk5E7cya+sqvMEnp2sLPxXyYoAnmpZMdnsP/z
QnB5bPGyFadgdXKrrORUMNr890z41c5cBRvJRIbdTXXY5AR7yIHrG3VFF0UjRVaZjubG2Y3l6ARu
+53KJVsYtMhTVPf0CwDOlobFiqHiFH2QnBoYFJTOMR41un5ri9m4Sa8siF1PFcZoTAY514SqZCBF
LQR8MhCeHZgSIhbvHVx+ds9d/ffF9NFqoWh5VIaS4t0z0fHhlRS/nxPBCt2Zdv//4mXHg8xWmP/t
S0DwO99z9rPTDBYq6KvgMBuIeyJg0Q9ODz+9lCpJpxM/WP7sfHXPiRQi2oRHyGLQJlaBLIySB677
2VqLJ/fe6WohnWcipTwzGV45vqEGVZZupXF4L2ED8rdNiahDwBLVKJXAfCHnRNaHtLE8tSAy6coW
Qp5kWt8EY2zL6tMIzLXWANeaqHiEUJyYRY4Y7/T/8Vxg8wtu8vqLioCP+XUKY68SJEmCugNH7Pqq
wAW6P3rNuyRS+sCfOXvrNr3dJWA25JWU78VVLZngxFaPfjqQjejNBglwiw7qZljPBNdIjND/EsSO
sFpPtDSwRcXBuXZRquQmEUUXWY7T4KNhWmA+2oD4b0wJI/7o9Z7kqeNK5XjGKBRubjrRMtHloBtD
8tZshBBMgdGhBdL6xkzUMcYupKUDpu8I6wJfwIcLiVoeuN3kjXqOeIyx7NPrqXD0nqFTLaD/v2Wf
GmWO/ZaC5NDAKaxRPTkxyZlEW9xVxUhpNXfytYrQ1qd1Srv6W12hFPLpfXal4H/CMTVoysyisndh
nadaDeWvxIhgFv6nG7u++wVc2dh2oEV/iSTmhrfGRT1LKXmCs4sNChAJ4knedb9qEvl0qG3ZUBPV
7H0YzFzzTLVClfOgYt7tEXHzn1fzu/0lAiEsQK6Iy8/R84czpZ6Oyh7zK8zzvvHsKWGV7VtZAsgV
grAVeZWyqgz96OOdBkVlohy1cwnUugf2SAkJ0o5FSiY7noAJwqkC4Q4FCyw7oHU+N0wfD/N/O8yy
GQ3lrd+ZI5GqpHzHnQqFJslTC9Qq6pmAccGhNjJ58MinlEQN/gi9BX0mkyN7T3TiSRIpHnF3Z2hv
sg0FeMNVNybqUQKOAtrn76rwI4chZA+KfNs2IKLSm2GDe7BZ939E0ePMgHUULK4G0RAsQgj36R20
7q2qnaEQIPjkHNFenVJynFdE/naL4TkNPEyKPppx4Gb+moGYGQLkkKXjJ2XtUyPKdPq+X73xG/C8
maW/kGAZH6LVZg+3OnzRkBZQP/Ln7xSW8H9mZzyMqz/ETvqRckaAxn3Bg5breOM/8nDt+1TbhOS0
uNDAJf66sirViJZ3AVVU0IQK1H+h1luObtaospW1aI2lg0szIUi7sNHbQKPxAi9R3ojfwq6dJP4i
ZizEjL48e95ZHlHY+A9+SryboPFzII6F8rs68bUlhftGqJPJBrwMG3ArZDUqAWF+WsdLuLBV1g7n
q9gNF2rnYt0uaBTzpouXqZ3/4RFxgoIst5ybERwEQSa5O1kzpom4pFGJHLRy1NCcNFtuvnc1gS2n
OJQdTTiWX/iJ2RfzdL5ZKok+U+oH0pjyTWSnkxQySdP2qwx6BcY8TD4htARy2gAB+axkLFvrE3My
Ir/Syt30ejih4zXS01gGccpDf3quYug5fcO2FFwGDFeetLnQ7YCENNLDj3YXf6W877xiQt4hg2Ax
r/UTb8S+Fp0UodAQbm9xtfdhhOsy4q2tk2IuNm6ZcD+kW5KXWzgHIL+QFr6DqyLqHKQqatJ1LDP7
6r7xP33GOqUiZDcHcAn0Pbb05+EVRfbqtMorkW/O55M5NkTtATV4EAj1U3EK0XQYhjK8Q561tcC5
+gF+byGMvz8nhXV8W8zZ2PNncgE+9/PEeDq8rGl+3OX6XBo/Q1m/Wulcaw4=
`pragma protect end_protected
