-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- (AVS Wrapper)

-- Derived from LOFAR cfg_single_reg
--
-- Usage:
-- 1) Connect out_reg to in_reg for write and readback register.
-- 2) Do not connect out_reg to in_reg for seperate write only register and
--    read only register at the same address.
-- 3) Leave out_reg OPEN for read only register.
-- 4) Connect wr_adr and rd_adr to have a shared address bus register.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

entity avs_common_reg_r_w is
  generic (
    g_latency   : natural := 1;  -- read latency
    g_adr_w     : natural := 5;
    g_dat_w     : natural := 32;
    g_nof_dat   : natural := 32;  -- optional, nof dat words <= 2**adr_w
    g_init_sl   : std_logic := '0';  -- optional, init all dat words to std_logic '0', '1' or 'X'
    g_init_reg  : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0')
  );
  port (
    csi_system_reset        : in  std_logic := '0';
    csi_system_clk          : in  std_logic;

    -- MM side
    avs_register_address     : in  std_logic_vector(g_adr_w - 1 downto 0);
    avs_register_write       : in  std_logic;
    avs_register_writedata   : in  std_logic_vector(g_dat_w - 1 downto 0);
    avs_register_read        : in  std_logic;
    avs_register_readdata    : out std_logic_vector(g_dat_w - 1 downto 0);

    -- user side
    coe_out_reg_export      : out std_logic_vector(g_dat_w * g_nof_dat - 1 downto 0);
    coe_in_reg_export       : in std_logic_vector(g_dat_w * g_nof_dat - 1 downto 0)
  );
end avs_common_reg_r_w;

architecture wrap of avs_common_reg_r_w is
  constant c_avs_memrec  : t_c_mem  := (g_latency, g_adr_w, g_dat_w, g_nof_dat, g_init_sl);
begin
  common_reg_r_w : entity work.common_reg_r_w
    generic map(
      g_reg       => c_avs_memrec,
      g_init_reg  => g_init_reg
    )
    port map(
      rst             => csi_system_reset,
      clk             => csi_system_clk,
      clken           => '1',
      -- control side
      wr_en           => avs_register_write,
      wr_adr          => avs_register_address,
      wr_dat          => avs_register_writedata,
      rd_en           => avs_register_read,
      rd_adr          => avs_register_address,
      rd_dat          => avs_register_readdata,
      rd_val          => OPEN,
      -- data side
      out_reg         => coe_out_reg_export,
      in_reg          => coe_in_reg_export
    );
end wrap;
