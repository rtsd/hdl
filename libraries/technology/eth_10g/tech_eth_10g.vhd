--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Combine mac_10g and 10gbase_r
-- Description:
-- . For c_tech_stratixiv:
--
--     cal_rec_clk    -->
--     tr_ref_clk_156 -->
--     tr_ref_rst_156 -->
--
--     tx_clk_arr_in  -->
--     rx_clk_arr_out <--
--     rx_clk_arr_in  -->
--                ___________________
--                |                 |
--     tx_snk --->|tech_            |---> xaui_tx_arr
--     rx_src <---|eth_10g_stratixiv|<--- xaui_rx_arr
--                |_________________|
--                    |
--                    |
--                  mac_mm
--
-- . For c_tech_arria10:
--
--     tr_ref_clk_644 -->
--     tr_ref_clk_312 -->
--     tr_ref_clk_156 -->
--     tr_ref_rst_156 -->
--                ___________________
--                |                 |
--     tx_snk --->|tech_            |---> serial_tx
--     rx_src <---|eth_10g_arria10  |<--- serial_rx
--                |_________________|
--                    |
--                    |
--                  mac_mm
--
-- . g_pre_header_padding: See tech_mac10g.vhd
--

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity tech_eth_10g is
  generic (
    g_technology          : natural := c_tech_select_default;
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_use_loopback        : boolean := false;
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R
    tr_ref_clk_312   : in  std_logic := '0';  -- 312.5      MHz for 10GBASE-R
    tr_ref_clk_156   : in  std_logic := '0';  -- 156.25     MHz for 10GBASE-R or for XAUI
    tr_ref_rst_156   : in  std_logic := '0';  -- for 10GBASE-R or for XAUI

    -- Calibration & reconfig clock
    cal_rec_clk      : in  std_logic := '0';  -- for XAUI;

    -- XAUI clocks
    tx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr_in or connect rx_clk_arr to tx_clk_arr_in
    tx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi := c_mem_mosi_rst;  -- MAG_10G (CSR), aggregated for all g_nof_channels
    mac_miso         : out t_mem_miso;

    xaui_mosi        : in  t_mem_mosi := c_mem_mosi_rst;  -- XAUI control
    xaui_miso        : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi := c_mem_mosi_rst;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    reg_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_10gbase_r_24_miso   : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- PHY serial IO
    -- . 10GBASE-R (single lane)
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');

    -- . XAUI (four lanes)
    xaui_tx_arr      : out t_xaui_arr(g_nof_channels - 1 downto 0);
    xaui_rx_arr      : in  t_xaui_arr(g_nof_channels - 1 downto 0) := (others => (others => '0'))
  );
end tech_eth_10g;

architecture str of tech_eth_10g is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : entity work.tech_eth_10g_stratixiv
    generic map (
      g_sim                 => g_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => g_nof_channels,
      g_direction           => g_direction,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_156   => tr_ref_clk_156,
      tr_ref_rst_156   => tr_ref_rst_156,

      -- Calibration & reconfig clock
      cal_rec_clk      => cal_rec_clk,

      -- Data clocks
      tx_clk_arr_in    => tx_clk_arr_in,
      tx_rst_arr_out   => tx_rst_arr_out,
      rx_clk_arr_out   => rx_clk_arr_out,
      rx_clk_arr_in    => rx_clk_arr_in,
      rx_rst_arr_out   => rx_rst_arr_out,

      -- MM
      mm_clk           => mm_clk,
      mm_rst           => mm_rst,

      mac_mosi         => mac_mosi,
      mac_miso         => mac_miso,

      xaui_mosi        => xaui_mosi,
      xaui_miso        => xaui_miso,

      -- ST
      tx_snk_in_arr    => tx_snk_in_arr,  -- 64 bit data @ tx_clk_arr_in (156.25 MHz)
      tx_snk_out_arr   => tx_snk_out_arr,

      rx_src_out_arr   => rx_src_out_arr,  -- 64 bit data @ rx_clk_arr (156.25 MHz)
      rx_src_in_arr    => rx_src_in_arr,

      -- Serial IO
      xaui_tx_arr      => xaui_tx_arr,
      xaui_rx_arr      => xaui_rx_arr
    );
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : entity work.tech_eth_10g_arria10
    generic map (
      g_sim                 => g_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => g_nof_channels,
      g_direction           => g_direction,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644   => tr_ref_clk_644,

      -- Data clocks
      clk_312          => tr_ref_clk_312,
      clk_156          => tr_ref_clk_156,
      rst_156          => tr_ref_rst_156,

      -- MM
      mm_clk           => mm_clk,
      mm_rst           => mm_rst,

      mac_mosi         => mac_mosi,
      mac_miso         => mac_miso,

      reg_eth10g_mosi  => reg_eth10g_mosi,
      reg_eth10g_miso  => reg_eth10g_miso,

      -- ST
      tx_snk_in_arr    => tx_snk_in_arr,  -- 64 bit data @ tr_ref_clk_156
      tx_snk_out_arr   => tx_snk_out_arr,

      rx_src_out_arr   => rx_src_out_arr,  -- 64 bit data @ tr_ref_clk_156
      rx_src_in_arr    => rx_src_in_arr,

      -- Serial
      serial_tx_arr    => serial_tx_arr,
      serial_rx_arr    => serial_rx_arr
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : entity work.tech_eth_10g_arria10_e3sge3
    generic map (
      g_sim                 => g_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => g_nof_channels,
      g_direction           => g_direction,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644   => tr_ref_clk_644,

      -- Data clocks
      clk_312          => tr_ref_clk_312,
      clk_156          => tr_ref_clk_156,
      rst_156          => tr_ref_rst_156,

      -- MM
      mm_clk           => mm_clk,
      mm_rst           => mm_rst,

      mac_mosi         => mac_mosi,
      mac_miso         => mac_miso,

      reg_eth10g_mosi  => reg_eth10g_mosi,
      reg_eth10g_miso  => reg_eth10g_miso,

      reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
      reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso => reg_10gbase_r_24_miso,

      -- ST
      tx_snk_in_arr    => tx_snk_in_arr,  -- 64 bit data @ tr_ref_clk_156
      tx_snk_out_arr   => tx_snk_out_arr,

      rx_src_out_arr   => rx_src_out_arr,  -- 64 bit data @ tr_ref_clk_156
      rx_src_in_arr    => rx_src_in_arr,

      -- Serial
      serial_tx_arr    => serial_tx_arr,
      serial_rx_arr    => serial_rx_arr
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : entity work.tech_eth_10g_arria10_e1sg
    generic map (
      g_sim                 => g_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => g_nof_channels,
      g_direction           => g_direction,
      g_use_loopback        => g_use_loopback,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644   => tr_ref_clk_644,

      -- Data clocks
      clk_312          => tr_ref_clk_312,
      clk_156          => tr_ref_clk_156,
      rst_156          => tr_ref_rst_156,

      -- MM
      mm_clk           => mm_clk,
      mm_rst           => mm_rst,

      mac_mosi         => mac_mosi,
      mac_miso         => mac_miso,

      reg_eth10g_mosi  => reg_eth10g_mosi,
      reg_eth10g_miso  => reg_eth10g_miso,

      reg_ip_arria10_e1sg_phy_10gbase_r_24_mosi   => reg_10gbase_r_24_mosi,
      reg_ip_arria10_e1sg_phy_10gbase_r_24_miso   => reg_10gbase_r_24_miso,

      -- ST
      tx_snk_in_arr    => tx_snk_in_arr,  -- 64 bit data @ tr_ref_clk_156
      tx_snk_out_arr   => tx_snk_out_arr,

      rx_src_out_arr   => rx_src_out_arr,  -- 64 bit data @ tr_ref_clk_156
      rx_src_in_arr    => rx_src_in_arr,

      -- Serial
      serial_tx_arr    => serial_tx_arr,
      serial_rx_arr    => serial_rx_arr
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : entity work.tech_eth_10g_arria10_e2sg
    generic map (
      g_sim                 => g_sim,
      g_sim_level           => g_sim_level,
      g_nof_channels        => g_nof_channels,
      g_direction           => g_direction,
      g_use_loopback        => g_use_loopback,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644   => tr_ref_clk_644,

      -- Data clocks
      clk_312          => tr_ref_clk_312,
      clk_156          => tr_ref_clk_156,
      rst_156          => tr_ref_rst_156,

      -- MM
      mm_clk           => mm_clk,
      mm_rst           => mm_rst,

      mac_mosi         => mac_mosi,
      mac_miso         => mac_miso,

      reg_eth10g_mosi  => reg_eth10g_mosi,
      reg_eth10g_miso  => reg_eth10g_miso,

      reg_ip_arria10_e2sg_phy_10gbase_r_24_mosi   => reg_10gbase_r_24_mosi,
      reg_ip_arria10_e2sg_phy_10gbase_r_24_miso   => reg_10gbase_r_24_miso,

      -- ST
      tx_snk_in_arr    => tx_snk_in_arr,  -- 64 bit data @ tr_ref_clk_156
      tx_snk_out_arr   => tx_snk_out_arr,

      rx_src_out_arr   => rx_src_out_arr,  -- 64 bit data @ tr_ref_clk_156
      rx_src_in_arr    => rx_src_in_arr,

      -- Serial
      serial_tx_arr    => serial_tx_arr,
      serial_rx_arr    => serial_rx_arr
    );
  end generate;
end str;
