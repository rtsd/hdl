-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Define the fields of network headers

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

package common_network_layers_pkg is
  -- All *_len constants are in nof octets = nof bytes = c_8 bits

  ------------------------------------------------------------------------------
  -- Ethernet Packet (with payload 32b word alignment!)
  --
  --  0                               15 16                               31  wi
  -- |----------------------------------------------------------------------|
  -- |     32b Word Align               |    Destination MAC Address        |  0
  -- |-----------------------------------                                   |
  -- |                                                                      |  1
  -- |----------------------------------------------------------------------|
  -- |                Source MAC Address                                    |  2
  -- |                                  ------------------------------------|
  -- |                                  |    EtherType                      |  3
  -- |----------------------------------|-----------------------------------|
  -- |                                                                      |
  -- |                Ethernet Payload                                      |
  -- |                                                                      |
  -- |------------------------------------------------------------ // ------|
  -- |                Frame Check Sequence                                  |
  -- |------------------------------------------------------------ // ------|
  --

  -- field widths in bits '_w' or in bytes '_len', '_min', '_max', '_sz'
  constant c_network_eth_preamble_len      : natural := 8;
  constant c_network_eth_mac_addr_len      : natural := 6;
  constant c_network_eth_mac_addr_w        : natural := c_network_eth_mac_addr_len * c_8;
  constant c_network_eth_type_len          : natural := 2;
  constant c_network_eth_type_w            : natural := c_network_eth_type_len * c_8;
  constant c_network_eth_header_len        : natural := 2 * c_network_eth_mac_addr_len + c_network_eth_type_len;  -- = 14
  constant c_network_eth_payload_min       : natural := 46;
  constant c_network_eth_payload_max       : natural := 1500;
  constant c_network_eth_payload_jumbo_max : natural := 9000;
  constant c_network_eth_crc_len           : natural := 4;
  constant c_network_eth_crc_w             : natural := c_network_eth_crc_len * c_8;
  constant c_network_eth_gap_len           : natural := 12;  -- IPG = interpacket gap, minimum idle period between transmission of Ethernet packets
  constant c_network_eth_frame_max         : natural := c_network_eth_header_len + c_network_eth_payload_max       + c_network_eth_crc_len;  -- = 1518
  constant c_network_eth_frame_jumbo_max   : natural := c_network_eth_header_len + c_network_eth_payload_jumbo_max + c_network_eth_crc_len;  -- = 9018

  -- default field values
  constant c_network_eth_preamble          : natural := 5;  -- nibble "0101"
  constant c_network_eth_frame_delimiter   : natural := 13;  -- nibble "1101"

  -- useful field values
  constant c_network_eth_mac_slv           : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := (others => 'X');  -- Ethernet MAC slv RANGE
  constant c_network_eth_bc_mac            : std_logic_vector(c_network_eth_mac_slv'range) := (others => '1');  -- Broadcast destination MAC

  constant c_network_eth_type_slv          : std_logic_vector(c_network_eth_type_w - 1 downto 0) := (others => 'X');  -- Ethernet TYPE slv RANGE
  constant c_network_eth_type_arp          : natural := 16#0806#;  -- ARP = Address Resolution Prorotol
  constant c_network_eth_type_ip           : natural := 16#0800#;  -- IPv4 = Internet Protocol, Version 4

  type t_network_eth_header is record
    dst_mac    : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    src_mac    : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    eth_type   : std_logic_vector(c_network_eth_type_w - 1 downto 0);
  end record;

  constant c_network_eth_header_ones : t_network_eth_header := ("000000000000000000000000000000000000000000000001",
                                                                "000000000000000000000000000000000000000000000001",
                                                                "0000000000000001");

  ------------------------------------------------------------------------------
  -- IPv4 Packet
  --
  --  0       3 4     7 8            15 16   18 19                        31  wi
  -- |----------------------------------------------------------------------|
  -- | Version |  HLEN |    Services    |      Total Length                 |  4
  -- |----------------------------------------------------------------------|
  -- |       Identification             | Flags |    Fragment Offset        |  5
  -- |----------------------------------------------------------------------|
  -- |     TTL         |    Protocol    |      Header Checksum              |  6
  -- |----------------------------------------------------------------------|
  -- |              Source IP Address                                       |  7
  -- |----------------------------------------------------------------------|
  -- |              Destination IP Address                                  |  8
  -- |----------------------------------------------------------------------|
  -- |                                                                      |
  -- |              IP Payload                                              |
  -- |                                                                      |
  -- |------------------------------------------------------------ // ------|
  --

  -- field widths in bits '_w' or in bytes '_len'
  constant c_network_ip_version_w           : natural := 4;  -- 4-bit field
  constant c_network_ip_header_length_w     : natural := 4;  -- 4-bit field
  constant c_network_ip_version_header_len  : natural := 1;
  constant c_network_ip_version_header_w    : natural := c_network_ip_version_header_len * c_8;
  constant c_network_ip_services_len        : natural := 1;
  constant c_network_ip_services_w          : natural := c_network_ip_services_len * c_8;
  constant c_network_ip_total_length_len    : natural := 2;
  constant c_network_ip_total_length_w      : natural := c_network_ip_total_length_len * c_8;
  constant c_network_ip_identification_len  : natural := 2;
  constant c_network_ip_identification_w    : natural := c_network_ip_identification_len * c_8;
  constant c_network_ip_flags_w             : natural := 3;  -- 3-bit field
  constant c_network_ip_fragment_offset_w   : natural := 13;  -- 13-bit field
  constant c_network_ip_flags_fragment_len  : natural := 2;
  constant c_network_ip_flags_fragment_w    : natural := c_network_ip_flags_fragment_len * c_8;
  constant c_network_ip_time_to_live_len    : natural := 1;
  constant c_network_ip_time_to_live_w      : natural := c_network_ip_time_to_live_len * c_8;
  constant c_network_ip_protocol_len        : natural := 1;
  constant c_network_ip_protocol_w          : natural := c_network_ip_protocol_len * c_8;
  constant c_network_ip_header_checksum_len : natural := 2;
  constant c_network_ip_header_checksum_w   : natural := c_network_ip_header_checksum_len * c_8;
  constant c_network_ip_addr_len            : natural := 4;
  constant c_network_ip_addr_w              : natural := c_network_ip_addr_len * c_8;

                                                      -- [0:7]                             [8:15]                      [16:31]
  constant c_network_ip_header_len          : natural := c_network_ip_version_header_len + c_network_ip_services_len + c_network_ip_total_length_len +
                                                         c_network_ip_identification_len +                             c_network_ip_flags_fragment_len +
                                                         c_network_ip_time_to_live_len   + c_network_ip_protocol_len + c_network_ip_header_checksum_len +
                                                         c_network_ip_addr_len +
                                                         c_network_ip_addr_len;
                                                    -- = c_network_ip_header_length * c_word_sz = 20
  -- default field values
  constant c_network_ip_version             : natural := 4;  -- 4 = IPv4,
  constant c_network_ip_header_length       : natural := 5;  -- 5 = nof words in the header, no options field support
  constant c_network_ip_services            : natural := 0;  -- 0 = default, use default on transmit, ignore on receive, copy on reply
  constant c_network_ip_total_length        : natural := 20;  -- >= 20, nof bytes in entire datagram including header and data
  constant c_network_ip_identification      : natural := 0;  -- identification number, copy on reply
  constant c_network_ip_flags               : natural := 2;  -- 2 = don't fragment and this is the last fragment
  constant c_network_ip_fragment_offset     : natural := 0;  -- 0 = first fragment
  constant c_network_ip_time_to_live        : natural := 127;  -- number of hops until the packet will be discarded
  constant c_network_ip_header_checksum     : natural := 0;  -- init value

  -- useful field values
  constant c_network_ip_protocol_slv        : std_logic_vector(c_network_ip_protocol_w - 1 downto 0) := (others => 'X');  -- IP protocol slv RANGE
  constant c_network_ip_protocol_udp        : natural := 17;  -- UDP = User Datagram Protocol (for board control and streaming data)
  constant c_network_ip_protocol_icmp       : natural := 1;  -- ICMP = Internet Control Message Protocol (for ping)

  constant c_network_ip_addr_slv            : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := (others => 'X');  -- IP address slv RANGE

  type t_network_ip_header is record
    version             : std_logic_vector(c_network_ip_version_w - 1 downto 0);  -- 4 bit
    header_length       : std_logic_vector(c_network_ip_header_length_w - 1 downto 0);  -- 4 bit
    services            : std_logic_vector(c_network_ip_services_w - 1 downto 0);  -- 1 octet
    total_length        : std_logic_vector(c_network_ip_total_length_w - 1 downto 0);  -- 2 octet
    identification      : std_logic_vector(c_network_ip_identification_w - 1 downto 0);  -- 2 octet
    flags               : std_logic_vector(c_network_ip_flags_w - 1 downto 0);  -- 3 bit
    fragment_offset     : std_logic_vector(c_network_ip_fragment_offset_w - 1 downto 0);  -- 13 bit
    time_to_live        : std_logic_vector(c_network_ip_time_to_live_w - 1 downto 0);  -- 1 octet
    protocol            : std_logic_vector(c_network_ip_protocol_w - 1 downto 0);  -- 1 octet
    header_checksum     : std_logic_vector(c_network_ip_header_checksum_w - 1 downto 0);  -- 2 octet
    src_ip_addr         : std_logic_vector(c_network_ip_addr_w - 1 downto 0);  -- 4 octet
    dst_ip_addr         : std_logic_vector(c_network_ip_addr_w - 1 downto 0);  -- 4 octet
  end record;

  constant c_network_ip_header_ones : t_network_ip_header := ("0001", "0001", "00000001", "0000000000000001",
                                                              "0000000000000001", "001", "0000000000001",
                                                              "00000001", "00000001", "0000000000000001",
                                                              "00000000000000000000000000000001",
                                                              "00000000000000000000000000000001");

  ------------------------------------------------------------------------------
  -- ARP Packet
  --
  --  0               7 8             15 16                               31  wi
  -- |----------------------------------------------------------------------|
  -- |       Hardware Type              |      Protocol Type                |  4
  -- |----------------------------------------------------------------------|
  -- |  HW Addr Len    |  Prot Addr Len |      Operation                    |  5
  -- |----------------------------------------------------------------------|
  -- |         Sender Hardware Address                                      |  6
  -- |                                  ------------------------------------|
  -- |                                  |                                   |  7
  -- |---------------------------------/ /----------------------------------|
  -- |         Sender Protocol Address  |                                   |  8
  -- |-----------------------------------                                   |
  -- |         Target Hardware Address                                      |  9
  -- |----------------------------------------------------------------------|
  -- |         Target Protocol Address                                      | 10
  -- |----------------------------------------------------------------------|
  --
  -- Note that ARP header = ARP packet, because ARP has no payload
  --

  -- field widths in bits '_w' or in bytes '_len'
  constant c_network_arp_htype_len          : natural := 2;
  constant c_network_arp_htype_w            : natural := c_network_arp_htype_len * c_8;
  constant c_network_arp_ptype_len          : natural := 2;
  constant c_network_arp_ptype_w            : natural := c_network_arp_ptype_len * c_8;
  constant c_network_arp_hlen_len           : natural := 1;
  constant c_network_arp_hlen_w             : natural := c_network_arp_hlen_len * c_8;
  constant c_network_arp_plen_len           : natural := 1;
  constant c_network_arp_plen_w             : natural := c_network_arp_plen_len * c_8;
  constant c_network_arp_oper_len           : natural := 2;
  constant c_network_arp_oper_w             : natural := c_network_arp_oper_len * c_8;

                                                      -- [0:15]                       [16:31]
  constant c_network_arp_data_len           : natural := c_network_arp_htype_len    + c_network_arp_ptype_len +
                                                         c_network_arp_hlen_len     + c_network_arp_plen_len  + c_network_arp_oper_len +
                                                         c_network_eth_mac_addr_len + c_network_ip_addr_len   +
                                                         c_network_eth_mac_addr_len + c_network_ip_addr_len;
                                                      -- [0:47]                       [0:31]                  = 8 + 2*(6+4) = 28

  -- default field values
  constant c_network_arp_htype              : natural := 1;  -- Hardware type, 1=ethernet
  constant c_network_arp_ptype              : natural := c_network_eth_type_ip;  -- Protocol type, do ARP for IPv4
  constant c_network_arp_hlen               : natural := c_network_eth_mac_addr_len;  -- Hardware length = 6
  constant c_network_arp_plen               : natural := c_network_ip_addr_len;  -- Protocol length = 4
  constant c_network_arp_oper_request       : natural := 1;  -- Operator, 1=request
  constant c_network_arp_oper_reply         : natural := 2;  -- Operator, 2=reply

  -- useful field values
  constant c_network_arp_dst_mac            : std_logic_vector(c_network_eth_mac_slv'range) := c_network_eth_bc_mac;  -- Broadcast destination MAC
  constant c_network_arp_tha                : std_logic_vector(c_network_eth_mac_slv'range) := c_network_eth_bc_mac;  -- Broadcast target hardware address

  type t_network_arp_packet is record
    htype   : std_logic_vector(c_network_arp_htype_w - 1 downto 0);  -- 2 octet
    ptype   : std_logic_vector(c_network_arp_ptype_w - 1 downto 0);  -- 2 octet
    hlen    : std_logic_vector(c_network_arp_hlen_w - 1 downto 0);  -- 1 octet
    plen    : std_logic_vector(c_network_arp_plen_w - 1 downto 0);  -- 1 octet
    oper    : std_logic_vector(c_network_arp_oper_w - 1 downto 0);  -- 2 octet
    sha     : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);  -- 6 octet, Sender Hardware Address
    spa     : std_logic_vector(c_network_ip_addr_w - 1 downto 0);  -- 4 octet, Sender Protocol Address
    tha     : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);  -- 6 octet, Target Hardware Address
    tpa     : std_logic_vector(c_network_ip_addr_w - 1 downto 0);  -- 4 octet, Target Protocol Address
  end record;

  constant c_network_arp_packet_ones : t_network_arp_packet := ("0000000000000001", "0000000000000001",
                                                                "00000001", "00000001", "0000000000000001",
                                                                "000000000000000000000000000000000000000000000001",
                                                                "00000000000000000000000000000001",
                                                                "000000000000000000000000000000000000000000000001",
                                                                "00000000000000000000000000000001");

  ------------------------------------------------------------------------------
  -- ICMP (for ping)
  --
  --  0               7 8             15 16                               31  wi
  -- |----------------------------------------------------------------------|
  -- |    Type         |    Code        |      Checksum                     |  9
  -- |----------------------------------------------------------------------|
  -- |    ID                            |      Sequence                     | 10
  -- |----------------------------------------------------------------------|
  -- |                                                                      |
  -- |              ICMP Payload (padding data)                             |
  -- |                                                                      |
  -- |------------------------------------------------------------ // ------|
  --

  -- field widths in bits '_w' or in bytes '_len'
  constant c_network_icmp_msg_type_len      : natural := 1;
  constant c_network_icmp_msg_type_w        : natural := c_network_icmp_msg_type_len * c_8;
  constant c_network_icmp_code_len          : natural := 1;
  constant c_network_icmp_code_w            : natural := c_network_icmp_code_len * c_8;
  constant c_network_icmp_checksum_len      : natural := 2;
  constant c_network_icmp_checksum_w        : natural := c_network_icmp_checksum_len * c_8;
  constant c_network_icmp_id_len            : natural := 2;
  constant c_network_icmp_id_w              : natural := c_network_icmp_id_len * c_8;
  constant c_network_icmp_sequence_len      : natural := 2;
  constant c_network_icmp_sequence_w        : natural := c_network_icmp_sequence_len * c_8;
  constant c_network_icmp_header_len        : natural := c_network_icmp_msg_type_len + c_network_icmp_code_len + c_network_icmp_checksum_len +
                                                         c_network_icmp_id_len                                 + c_network_icmp_sequence_len;

  -- default field values
  constant c_network_icmp_msg_type_request   : natural := 8;  -- 8 = echo request
  constant c_network_icmp_msg_type_reply     : natural := 0;  -- 8 = echo reply (ping)
  constant c_network_icmp_checksum           : natural := 0;  -- init value

  -- useful field values
  constant c_network_icmp_code               : natural := 0;  -- default
  constant c_network_icmp_id                 : natural := 3;  -- arbitrary value
  constant c_network_icmp_sequence           : natural := 4;  -- arbitrary value

  type t_network_icmp_header is record
    msg_type   : std_logic_vector(c_network_icmp_msg_type_w - 1 downto 0);  -- 1 octet
    code       : std_logic_vector(c_network_icmp_code_w - 1 downto 0);  -- 1 octet
    checksum   : std_logic_vector(c_network_icmp_checksum_w - 1 downto 0);  -- 2 octet
    id         : std_logic_vector(c_network_icmp_id_w - 1 downto 0);  -- 2 octet
    sequence   : std_logic_vector(c_network_icmp_sequence_w - 1 downto 0);  -- 2 octet
  end record;

  constant c_network_icmp_header_ones : t_network_icmp_header := ("00000001", "00000001", "0000000000000001",
                                                                  "0000000000000001", "0000000000000001");

  ------------------------------------------------------------------------------
  -- UDP Packet
  --
  --  0                               15 16                               31  wi
  -- |----------------------------------------------------------------------|
  -- |      Source Port                 |      Destination Port             |  9
  -- |----------------------------------------------------------------------|
  -- |      Total Length                |      Checksum                     | 10
  -- |----------------------------------------------------------------------|
  -- |                                                                      |
  -- |                      UDP Payload                                     |
  -- |                                                                      |
  -- |----------------------------------------------------------- // -------|
  --

  -- field widths in bits '_w' or in bytes '_len'
  constant c_network_udp_port_len           : natural := 2;
  constant c_network_udp_port_w             : natural := c_network_udp_port_len * c_8;
  constant c_network_udp_total_length_len   : natural := 2;
  constant c_network_udp_total_length_w     : natural := c_network_udp_total_length_len * c_8;
  constant c_network_udp_checksum_len       : natural := 2;
  constant c_network_udp_checksum_w         : natural := c_network_udp_checksum_len * c_8;

                                                      -- [0:15]                           [16:31]
  constant c_network_udp_header_len         : natural := c_network_udp_port_len         + c_network_udp_port_len +
                                                         c_network_udp_total_length_len + c_network_udp_checksum_len;  -- 8

  -- default field values
  constant c_network_udp_total_length       : natural := 8;  -- >= 8, nof bytes in entire datagram including header and data
  constant c_network_udp_checksum           : natural := 0;  -- init value

  -- useful field values  -- Note that ARP header = ARP packet, because ARP has no payload

  constant c_network_udp_port_dhcp_in       : natural := 68;  -- DHCP to client = Dynamic Host Configuration Protocol (for IP address assignment)
  constant c_network_udp_port_dhcp_out      : natural := 67;  -- DHCP to server
  constant c_network_udp_port_slv           : std_logic_vector(c_network_udp_port_w - 1 downto 0) := (others => 'X');  -- UDP port slv RANGE

  type t_network_udp_header is record
    src_port     : std_logic_vector(c_network_udp_port_w - 1 downto 0);  -- 2 octet
    dst_port     : std_logic_vector(c_network_udp_port_w - 1 downto 0);  -- 2 octet
    total_length : std_logic_vector(c_network_udp_total_length_w - 1 downto 0);  -- 2 octet
    checksum     : std_logic_vector(c_network_udp_checksum_w - 1 downto 0);  -- 2 octet
  end record;

  constant c_network_udp_header_ones : t_network_udp_header := ("0000000000000001", "0000000000000001",
                                                                "0000000000000001", "0000000000000001");
end common_network_layers_pkg;

package body common_network_layers_pkg is
end common_network_layers_pkg;
