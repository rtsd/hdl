-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Provide monitoring and control of dp_fifo_*
-- Description:
-- . Instantiate this dp_fifo_monitor next to dp_fifo_* and connect the
--   desired monitoring inputs and control outputs.

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

entity dp_fifo_monitor is
  port (
    mm_clk       : in  std_logic;
    mm_rst       : in  std_logic;

    dp_clk       : in  std_logic;
    dp_rst       : in  std_logic;

    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;
    -- Status inputs
    rd_usedw_32b : in  std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
    wr_usedw_32b : in  std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
    rd_emp       : in  std_logic := '0';
    wr_full      : in  std_logic := '0';
    -- Control output
    rd_fill_32b  : out std_logic_vector(c_word_w - 1 downto 0)
  );
end dp_fifo_monitor;

architecture str of dp_fifo_monitor is
  constant c_field_arr : t_common_field_arr(4 downto 0) := ( (field_name_pad("rd_usedw"), "RO", 32, field_default(0) ),
                                                             (field_name_pad("wr_usedw"), "RO", 32, field_default(0) ),
                                                             (field_name_pad("rd_empty"), "RO",  1, field_default(0) ),
                                                             (field_name_pad("wr_full" ), "RO",  1, field_default(0) ),
                                                             (field_name_pad("rd_fill" ), "RW", 32, field_default(0) ));

  signal mm_fields_in  : std_logic_vector(field_slv_in_len(c_field_arr) - 1 downto 0);
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);
begin
  mm_fields_in(field_hi(c_field_arr, "rd_usedw") downto field_lo(c_field_arr, "rd_usedw")) <= rd_usedw_32b;
  mm_fields_in(field_hi(c_field_arr, "wr_usedw") downto field_lo(c_field_arr, "wr_usedw")) <= wr_usedw_32b;
  mm_fields_in(field_hi(c_field_arr, "rd_empty") downto field_lo(c_field_arr, "rd_empty")) <= slv(rd_emp);
  mm_fields_in(field_hi(c_field_arr, "wr_full")  downto field_lo(c_field_arr, "wr_full"))  <= slv(wr_full);

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_use_slv_in_val  => false,  -- use FALSE to save logic when always slv_in_val='1'
    g_field_arr       => c_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_mosi,
    mm_miso    => reg_miso,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_in,
    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  rd_fill_32b <= mm_fields_out(field_hi(c_field_arr, "rd_fill") downto field_lo(c_field_arr, "rd_fill"));
end str;
