--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Unfold n input streams into n*2, n*2*2, n*2*2*2, .. output streams
-- Description:
-- . Reversed operation of dp_folder.
-- Remark:
-- . Use dp_deinterleave_one_to_n.vhd when:
--   - for 1 to N, so any N >= 1 (so not only powers of 2)
--   - when all sosi fields need to be preserved (so not only the valid)
--   - if flow control is needed

entity dp_unfolder is
  generic (
    g_nof_inputs        : natural;  -- Number of inputs
    g_nof_unfolds       : natural := 0;  -- Number of times to unfold
    g_output_block_size : natural := 0;  -- >0: Create SOP/EOP tagged output blocks of this size.
    g_fwd_sync_bsn      : boolean := false;  -- TRUE: forwards (stored) input Sync+BSN (from snk_in_arr(0)) to all output streams
    g_use_channel       : boolean := false;  -- TRUE: Also fold the channel field
    g_output_align      : boolean := true  -- TRUE: Use pipeline stages to align the outputs
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

    src_out_arr    : out t_dp_sosi_arr(g_nof_inputs * pow2(g_nof_unfolds) - 1 downto 0)
  );
end dp_unfolder;

architecture str of dp_unfolder is
  component dp_unfolder is
    generic (
      g_nof_inputs   : natural;
      g_nof_unfolds  : natural := 0;
      g_output_block_size : natural := 0;
      g_fwd_sync_bsn      : boolean := false;
      g_use_channel       : boolean := false;
      g_output_align : boolean := true
    );
    port (
      rst            : in  std_logic;
      clk            : in  std_logic;

      snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

      src_out_arr    : out t_dp_sosi_arr(g_nof_inputs * pow2(g_nof_unfolds) - 1 downto 0)
    );
  end component;

  constant c_nof_demuxes : natural := g_nof_inputs;
  constant c_nof_outputs : natural := g_nof_inputs * pow2(g_nof_unfolds);

  signal output_sel_arr           : std_logic_vector(c_nof_demuxes - 1 downto 0);
  signal nxt_output_sel_arr       : std_logic_vector(c_nof_demuxes - 1 downto 0);

  signal demux_src_out_2arr_2     : t_dp_sosi_2arr_2(c_nof_demuxes - 1 downto 0);
  signal nxt_demux_src_out_2arr_2 : t_dp_sosi_2arr_2(c_nof_demuxes - 1 downto 0);
  signal demux_src_out_arr        : t_dp_sosi_arr(2 * c_nof_demuxes - 1 downto 0);

  signal dp_pipeline_snk_in_arr   : t_dp_sosi_arr(c_nof_outputs - 1 downto 0);

  signal dp_block_gen_snk_in_arr  : t_dp_sosi_arr(c_nof_outputs - 1 downto 0);
  signal dp_block_gen_src_out_arr : t_dp_sosi_arr(c_nof_outputs - 1 downto 0);
begin
  gen_arch: if g_nof_unfolds /= 0 generate
    -----------------------------------------------------------------------------
    -- Simple 2-output demux logic
    -----------------------------------------------------------------------------
    gen_demux_comb: for i in 0 to c_nof_demuxes - 1 generate
      nxt_output_sel_arr(i) <= not output_sel_arr(i) when snk_in_arr(i).valid = '1' else output_sel_arr(i);
    end generate;

    p_demux : process(snk_in_arr, output_sel_arr)
    begin
      for i in 0 to c_nof_demuxes - 1 loop
        nxt_demux_src_out_2arr_2(i)(0) <= c_dp_sosi_rst;
        nxt_demux_src_out_2arr_2(i)(1) <= c_dp_sosi_rst;
        if snk_in_arr(i).valid = '1' then
          if output_sel_arr(i) = '0' then
            nxt_demux_src_out_2arr_2(i)(0).data  <= snk_in_arr(i).data;
            nxt_demux_src_out_2arr_2(i)(0).re    <= snk_in_arr(i).re;
            nxt_demux_src_out_2arr_2(i)(0).im    <= snk_in_arr(i).im;
            nxt_demux_src_out_2arr_2(i)(0).valid <= '1';
            if g_use_channel = true then
              nxt_demux_src_out_2arr_2(i)(0).channel <= snk_in_arr(i).channel;
            end if;
          else
            nxt_demux_src_out_2arr_2(i)(1).data  <= snk_in_arr(i).data;
            nxt_demux_src_out_2arr_2(i)(1).re    <= snk_in_arr(i).re;
            nxt_demux_src_out_2arr_2(i)(1).im    <= snk_in_arr(i).im;
            nxt_demux_src_out_2arr_2(i)(1).valid <= '1';
            if g_use_channel = true then
              nxt_demux_src_out_2arr_2(i)(1).channel <= snk_in_arr(i).channel;
            end if;
          end if;
        end if;
      end loop;
    end process;

    -- Registers
    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        demux_src_out_2arr_2 <= (others => (others => c_dp_sosi_rst));
        output_sel_arr       <= (others => '0');
      elsif rising_edge(clk) then
        demux_src_out_2arr_2 <= nxt_demux_src_out_2arr_2;
        output_sel_arr       <= nxt_output_sel_arr;
      end if;
    end process;

    -----------------------------------------------------------------------------
    -- Wire the 2D demux output array to 1D array to match entity I/O type
    -----------------------------------------------------------------------------
    gen_demux_inputs_0: for i in 0 to c_nof_demuxes - 1 generate
       demux_src_out_arr(2 * i)   <= demux_src_out_2arr_2(i)(0);
       demux_src_out_arr(2 * i + 1) <= demux_src_out_2arr_2(i)(1);
    end generate;

    -----------------------------------------------------------------------------
    -- g_nof_unfolds>1, so add an unfolder stage.
    -----------------------------------------------------------------------------
    gen_dp_unfolder: if g_nof_unfolds > 1 generate
      u_dp_unfolder : dp_unfolder
      generic map (
        g_nof_inputs        => c_nof_demuxes * 2,  -- Next stage has all our demux outputs as inputs
        g_nof_unfolds       => g_nof_unfolds - 1,
        g_output_block_size => g_output_block_size,
        g_fwd_sync_bsn      => g_fwd_sync_bsn,
        g_use_channel       => g_use_channel,
        g_output_align      => g_output_align
      )
      port map (
        rst         => rst,
        clk         => clk,

        snk_in_arr  => demux_src_out_arr,
        src_out_arr => src_out_arr
      );
    end generate;

    -----------------------------------------------------------------------------
    -- g_nof_unfolds=1, so this is the last stage. Wire up the outputs.
    -----------------------------------------------------------------------------
    gen_src_out_arr: if g_nof_unfolds = 1 generate
      gen_output_align : if g_output_align = true generate
        dp_pipeline_snk_in_arr <= demux_src_out_arr;

        -----------------------------------------------------------------------------
        -- Pipeline to re-align the unfolder output
        -----------------------------------------------------------------------------
        gen_dp_pipeline : for i in 0 to c_nof_outputs - 1 generate
          u_dp_pipeline : entity dp_lib.dp_pipeline
          generic map (
            g_pipeline => 0 + (pow2(g_nof_unfolds) - i rem pow2(g_nof_unfolds) - 1)
          )
          port map (
            rst         => rst,
            clk         => clk,

            snk_in      => dp_pipeline_snk_in_arr(i),
            src_out     => dp_block_gen_snk_in_arr(i)
          );
        end generate;
      end generate;

      no_output_align : if g_output_align = false generate
        dp_block_gen_snk_in_arr <= demux_src_out_arr;
      end generate;

      -----------------------------------------------------------------------------
      -- Add SOP and EOP to the outputs
      -----------------------------------------------------------------------------
      gen_ctrl : if g_output_block_size > 0 generate
        gen_dp_block_gen : for i in 0 to c_nof_outputs - 1 generate
          u_dp_block_gen : entity work.dp_block_gen
          generic map (
            g_use_src_in       => false,
            g_nof_data         => g_output_block_size,
            g_preserve_sync    => true,
            g_preserve_bsn     => true,
            g_preserve_channel => g_use_channel
          )
          port map(
            rst        => rst,
            clk        => clk,

            snk_in     => dp_block_gen_snk_in_arr(i),
            src_out    => dp_block_gen_src_out_arr(i)
          );
        end generate;
      end generate;

      no_ctrl : if g_output_block_size = 0 generate
        dp_block_gen_src_out_arr <= dp_block_gen_snk_in_arr;
      end generate;

      -----------------------------------------------------------------------------
      -- Re-add input sync + BSN to all output streams
      -----------------------------------------------------------------------------
      gen_sync_bsn : if g_fwd_sync_bsn = true generate
        gen_dp_fifo_info: for i in 0 to c_nof_outputs - 1 generate
          u_dp_fifo_info : entity work.dp_fifo_info
          generic map (
            g_use_sync => true,
            g_use_bsn  => true
          )
          port map (
            rst          => rst,
            clk          => clk,

            data_snk_in  => dp_block_gen_src_out_arr(i),  -- delayed snk_in data
            info_snk_in  => snk_in_arr(0),  -- original snk_in info

            src_in       => c_dp_siso_rdy,
            src_out      => src_out_arr(i)
          );
        end generate;
      end generate;

      no_sync_bsn : if g_fwd_sync_bsn = false generate
        src_out_arr <= dp_block_gen_src_out_arr;
      end generate;

        end generate;
      end generate;

  -----------------------------------------------------------------------------
  -- Wire output to input if g_nof_unfolds=0
  -----------------------------------------------------------------------------
  gen_wire_out_to_in: if g_nof_unfolds = 0 generate
    dp_block_gen_snk_in_arr <= snk_in_arr;
  end generate;
end str;
