  component qsys_unb2b_minimal_avs_eth_0 is
    port (
      coe_clk_export             : out std_logic;  -- export
      ins_interrupt_irq          : out std_logic;  -- irq
      coe_irq_export             : in  std_logic                     := 'X';  -- export
      csi_mm_clk                 : in  std_logic                     := 'X';  -- clk
      csi_mm_reset               : in  std_logic                     := 'X';  -- reset
      mms_ram_address            : in  std_logic_vector(9 downto 0)  := (others => 'X');  -- address
      mms_ram_write              : in  std_logic                     := 'X';  -- write
      mms_ram_read               : in  std_logic                     := 'X';  -- read
      mms_ram_writedata          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
      mms_ram_readdata           : out std_logic_vector(31 downto 0);  -- readdata
      mms_reg_address            : in  std_logic_vector(3 downto 0)  := (others => 'X');  -- address
      mms_reg_write              : in  std_logic                     := 'X';  -- write
      mms_reg_read               : in  std_logic                     := 'X';  -- read
      mms_reg_writedata          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
      mms_reg_readdata           : out std_logic_vector(31 downto 0);  -- readdata
      mms_tse_address            : in  std_logic_vector(9 downto 0)  := (others => 'X');  -- address
      mms_tse_write              : in  std_logic                     := 'X';  -- write
      mms_tse_read               : in  std_logic                     := 'X';  -- read
      mms_tse_writedata          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
      mms_tse_readdata           : out std_logic_vector(31 downto 0);  -- readdata
      mms_tse_waitrequest        : out std_logic;  -- waitrequest
      coe_ram_address_export     : out std_logic_vector(9 downto 0);  -- export
      coe_ram_read_export        : out std_logic;  -- export
      coe_ram_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
      coe_ram_write_export       : out std_logic;  -- export
      coe_ram_writedata_export   : out std_logic_vector(31 downto 0);  -- export
      coe_reg_address_export     : out std_logic_vector(3 downto 0);  -- export
      coe_reg_read_export        : out std_logic;  -- export
      coe_reg_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
      coe_reg_write_export       : out std_logic;  -- export
      coe_reg_writedata_export   : out std_logic_vector(31 downto 0);  -- export
      coe_reset_export           : out std_logic;  -- export
      coe_tse_address_export     : out std_logic_vector(9 downto 0);  -- export
      coe_tse_read_export        : out std_logic;  -- export
      coe_tse_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
      coe_tse_waitrequest_export : in  std_logic                     := 'X';  -- export
      coe_tse_write_export       : out std_logic;  -- export
      coe_tse_writedata_export   : out std_logic_vector(31 downto 0)  -- export
    );
  end component qsys_unb2b_minimal_avs_eth_0;

  u0 : component qsys_unb2b_minimal_avs_eth_0
    port map (
      coe_clk_export             => CONNECTED_TO_coe_clk_export,  -- clk.export
      ins_interrupt_irq          => CONNECTED_TO_ins_interrupt_irq,  -- interrupt.irq
      coe_irq_export             => CONNECTED_TO_coe_irq_export,  -- irq.export
      csi_mm_clk                 => CONNECTED_TO_csi_mm_clk,  -- mm.clk
      csi_mm_reset               => CONNECTED_TO_csi_mm_reset,  -- mm_reset.reset
      mms_ram_address            => CONNECTED_TO_mms_ram_address,  -- mms_ram.address
      mms_ram_write              => CONNECTED_TO_mms_ram_write,  -- .write
      mms_ram_read               => CONNECTED_TO_mms_ram_read,  -- .read
      mms_ram_writedata          => CONNECTED_TO_mms_ram_writedata,  -- .writedata
      mms_ram_readdata           => CONNECTED_TO_mms_ram_readdata,  -- .readdata
      mms_reg_address            => CONNECTED_TO_mms_reg_address,  -- mms_reg.address
      mms_reg_write              => CONNECTED_TO_mms_reg_write,  -- .write
      mms_reg_read               => CONNECTED_TO_mms_reg_read,  -- .read
      mms_reg_writedata          => CONNECTED_TO_mms_reg_writedata,  -- .writedata
      mms_reg_readdata           => CONNECTED_TO_mms_reg_readdata,  -- .readdata
      mms_tse_address            => CONNECTED_TO_mms_tse_address,  -- mms_tse.address
      mms_tse_write              => CONNECTED_TO_mms_tse_write,  -- .write
      mms_tse_read               => CONNECTED_TO_mms_tse_read,  -- .read
      mms_tse_writedata          => CONNECTED_TO_mms_tse_writedata,  -- .writedata
      mms_tse_readdata           => CONNECTED_TO_mms_tse_readdata,  -- .readdata
      mms_tse_waitrequest        => CONNECTED_TO_mms_tse_waitrequest,  -- .waitrequest
      coe_ram_address_export     => CONNECTED_TO_coe_ram_address_export,  -- ram_address.export
      coe_ram_read_export        => CONNECTED_TO_coe_ram_read_export,  -- ram_read.export
      coe_ram_readdata_export    => CONNECTED_TO_coe_ram_readdata_export,  -- ram_readdata.export
      coe_ram_write_export       => CONNECTED_TO_coe_ram_write_export,  -- ram_write.export
      coe_ram_writedata_export   => CONNECTED_TO_coe_ram_writedata_export,  -- ram_writedata.export
      coe_reg_address_export     => CONNECTED_TO_coe_reg_address_export,  -- reg_address.export
      coe_reg_read_export        => CONNECTED_TO_coe_reg_read_export,  -- reg_read.export
      coe_reg_readdata_export    => CONNECTED_TO_coe_reg_readdata_export,  -- reg_readdata.export
      coe_reg_write_export       => CONNECTED_TO_coe_reg_write_export,  -- reg_write.export
      coe_reg_writedata_export   => CONNECTED_TO_coe_reg_writedata_export,  -- reg_writedata.export
      coe_reset_export           => CONNECTED_TO_coe_reset_export,  -- reset.export
      coe_tse_address_export     => CONNECTED_TO_coe_tse_address_export,  -- tse_address.export
      coe_tse_read_export        => CONNECTED_TO_coe_tse_read_export,  -- tse_read.export
      coe_tse_readdata_export    => CONNECTED_TO_coe_tse_readdata_export,  -- tse_readdata.export
      coe_tse_waitrequest_export => CONNECTED_TO_coe_tse_waitrequest_export,  -- tse_waitrequest.export
      coe_tse_write_export       => CONNECTED_TO_coe_tse_write_export,  -- tse_write.export
      coe_tse_writedata_export   => CONNECTED_TO_coe_tse_writedata_export  -- tse_writedata.export
    );
