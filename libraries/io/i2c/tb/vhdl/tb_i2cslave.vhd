-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--23-7-2005
--file i2cslave_tb.vhd
--testbench for i2cslave function
--written by A.W. Gunst

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity tb_i2cslave is
end tb_i2cslave;

architecture tb of tb_i2cslave is
  constant c_period         : time := 25 ns;
  constant c_address        : std_logic_vector(6 downto 0) := "0000001";  -- Slave I2C address
  constant c_nof_ctrl_bytes : integer := 3;

  component i2cslave
    generic (
      g_rx_filter       : boolean := true;
      g_address         : std_logic_vector(6 downto 0) := "0000001";
      g_nof_ctrl_bytes  : natural := 3
    );
    port(
      clk         : in  std_logic;
      SDA         : inout  std_logic;
      SCL         : in  std_logic;
      RST         : in  std_logic;
      CTRL_REG    : out std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0)
    );
  end component;

   signal SDA      : std_logic;  -- I2C Serial Data Line
   signal SCL      : std_logic;  -- I2C Serial Clock Line
   signal RST      : std_logic;  -- optional reset bit
   signal CTRL_REG : std_logic_vector(8 * c_nof_ctrl_bytes - 1 downto 0);  -- ctrl for RCU control
begin
  uut: i2cslave
  generic map (
    g_rx_filter        => false,
    g_address          => c_address,
    g_nof_ctrl_bytes   => c_nof_ctrl_bytes
  )
  port map(
    clk => '0',
    SDA => SDA,
    SCL => SCL,
    RST => RST,
    CTRL_REG => CTRL_REG
  );

tbctrl : process
  variable cnt : std_logic_vector(11 downto 0) := "000000000000";
  begin
    while (true) loop
      SCL <= '0';
      cnt := cnt + 1;
      wait for c_period;
      SCL <= 'H';
      wait for c_period;
    end loop;
    wait;
  end process;

--SCL low: 0-25 ns
--SCL high: 25-50 ns

tbsda : process
  begin
    SDA <= 'Z';
    wait for 200 ns;  -- initial time to let the fpga set up things
    RST <= '1', '0' after 100 ns;
    wait for 200 ns;  -- initial time to let the fpga set up things

     --I2C write sequence (from master to slave)
     SDA <= 'H', '0' after 30 ns, '0' after 60 ns, '0' after 110 ns, '0' after 160 ns, '0' after 210 ns, '0' after 260 ns, '0' after 310 ns, 'H' after 360 ns, '0' after 410 ns;
     --     start                     sent address: 0000001,                                                                                              sent rw=0
    --the previous is evaluated at time = 0
    wait for 452 ns;  -- next lines are evaluated 450 ns later
    SDA <= 'Z';  -- time for slave to acknowledge
    wait for 50 ns;  -- WAIT 1 clk cycle
    SDA <= 'Z', 'H' after 10 ns, '0' after 60 ns, 'H' after 110 ns, '0' after 160 ns, 'H' after 210 ns, 'H' after 260 ns, 'H' after 310 ns, '0' after 360 ns;  -- sent first data byte
    wait for 400 ns;
    SDA <= 'Z';  -- time for slave to acknowledge
    wait for 50 ns;  -- WAIT 1 clk cycle
    SDA <= 'Z', '0' after 10 ns, 'H' after 60 ns, 'H' after 110 ns, '0' after 160 ns, 'H' after 210 ns, '0' after 260 ns, 'H' after 310 ns, 'H' after 360 ns;  -- sent second data byte

    wait for 400 ns;
    SDA <= 'Z';  -- time for slave to acknowledge
    wait for 50 ns;  -- WAIT 1 clk cycle
    SDA <= 'Z', '0' after 10 ns, 'H' after 60 ns, 'H' after 110 ns, '0' after 160 ns, 'H' after 210 ns, '0' after 260 ns, 'H' after 310 ns, 'H' after 360 ns;  -- sent third data byte

    wait for 400 ns;
    SDA <= 'Z';  -- time for slave to nacknowledge
    wait for 70 ns;  -- WAIT 1.5 clk cycle
    SDA <= '0';  -- stop
    wait for 30 ns;  -- to get in line with falling clk edge

    --reset slave
    RST <= '1';
    wait for 50 ns;
    RST <= '0';

    --time is 1500ns + 450ns
    --I2C read sequence (from slave to master)
    SDA <= 'H', '0' after 30 ns, '0' after 60 ns, '0' after 110 ns, '0' after 160 ns, '0' after 210 ns, '0' after 260 ns, '0' after 310 ns, 'H' after 360 ns, 'H' after 410 ns;
    --     start                     sent address: 0000001                                                                                             sent rw=1
    wait for 450 ns;  -- next lines are evaluated 450 ns later
    SDA <= 'Z';  -- time for slave to acknowledge address
    wait for 505 ns;
    SDA <= '0', 'Z' after 30 ns;  -- acknowledge first byte

    wait for 500 ns;
    SDA <= '0', 'Z' after 30 ns;  -- acknowledge second byte

    --time is 2455ns + 450ns
    wait for 505 ns;  -- on purpose the nack is given 100 ns later
    SDA <= 'H', 'Z' after 50 ns;  -- nacknowledge third byte
    wait for 125 ns;  -- WAIT 2.5 clk to give stop command
    SDA <= 'H';  -- stop
    wait for 15 ns;  -- to get in line with falling clk edge
    --time is 3115

    wait for 200 ns;
    --reset slave
    RST <= '0';
    wait for 50 ns;
    RST <= '1';
    SDA <= '0';

    wait for 50 ns;

    --I2C sequence for another slave
    SDA <= 'H', '0' after 30 ns, 'H' after 60 ns, '0' after 110 ns, '0' after 160 ns, '0' after 210 ns, '0' after 260 ns, '0' after 310 ns, '0' after 360 ns, '0' after 410 ns;
    --     start                     sent address: 0000001,                                                                                              sent rw=0
    --the previous is evaluated at time = 0
    wait for 450 ns;  -- next lines are evaluated 450 ns later
    SDA <= 'Z';  -- time for slave to acknowledge
    wait for 50 ns;  -- WAIT 1 clk cycle
    SDA <= 'Z', '0' after 10 ns, '0' after 60 ns, 'H' after 110 ns, '0' after 160 ns, 'H' after 210 ns, 'H' after 260 ns, 'H' after 310 ns, '0' after 360 ns;  -- sent first data byte
    wait for 400 ns;
    SDA <= 'Z';  -- time for slave to acknowledge
    wait for 50 ns;  -- WAIT 1 clk cycle
    SDA <= 'Z', '0' after 10 ns, 'H' after 60 ns, 'H' after 110 ns, '0' after 160 ns, 'H' after 210 ns, '0' after 260 ns, 'H' after 310 ns, 'H' after 360 ns;  -- sent second data byte
    wait for 400 ns;
    SDA <= 'Z';  -- time for slave to nacknowledge
    wait for 80 ns;  -- WAIT 1.5 clk cycle
    SDA <= '0';  -- stop
    wait for 20 ns;  -- to get in line with falling clk edge
  end process;
end;
