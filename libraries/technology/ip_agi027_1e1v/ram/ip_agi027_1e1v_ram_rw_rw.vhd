-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author: D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper / Instantiate RAM IP with generics
-- Description:
--   Copied component declaration and instance example from
--   generated/ram_2port_2040/sim/ip_agi027_1e1v_ram_rw_rw_ram_2port_2040_uxwhvmq.vhd
-- Remark:
--   The outcome of the synthesis is that the parameter
--   read_during_write_mode_mixed_ports cannot be set to the
--   value OLD_DATA for the Agilex 7, otherwise an error occurs.

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use technology_lib.technology_pkg.all;

library altera_lnsim;
use altera_lnsim.altera_lnsim_components.all;

entity ip_agi027_1e1v_ram_rw_rw is
  generic (
    g_inferred   : boolean := false;
    g_adr_w      : natural := 5;
    g_dat_w      : natural := 8;
    g_nof_words  : natural := 2**5;
    g_rd_latency : natural := 1;  -- choose 1 or 2
    g_init_file  : string  := "UNUSED"
  );
  port (
    address_a : in std_logic_vector(g_adr_w - 1 downto 0);
    address_b : in std_logic_vector(g_adr_w - 1 downto 0);
    clk       : in std_logic  := '1';
    data_a    : in std_logic_vector(g_dat_w - 1 downto 0);
    data_b    : in std_logic_vector(g_dat_w - 1 downto 0);
    wren_a    : in std_logic  := '0';
    wren_b    : in std_logic  := '0';
    q_a       : out std_logic_vector(g_dat_w - 1 downto 0);
    q_b       : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end ip_agi027_1e1v_ram_rw_rw;

architecture SYN of ip_agi027_1e1v_ram_rw_rw is
  constant c_outdata_reg : string := tech_sel_a_b(g_rd_latency = 1, "UNREGISTERED", "CLOCK0");

  component altera_syncram
  generic (
          address_reg_b  : string;
          clock_enable_input_a  : string;
          clock_enable_input_b  : string;
          clock_enable_output_a  : string;
          clock_enable_output_b  : string;
          indata_reg_b  : string;
          init_file  : string;
          -- enable_force_to_zero : string;
          intended_device_family  : string;
          lpm_type  : string;
          numwords_a  : integer;
          numwords_b  : integer;
          operation_mode  : string;
          outdata_aclr_a  : string;
          -- outdata_sclr_a  : string;
          outdata_aclr_b  : string;
          -- outdata_sclr_b  : string;
          outdata_reg_a  : string;
          outdata_reg_b  : string;
          power_up_uninitialized  : string;
          read_during_write_mode_port_a  : string;
          read_during_write_mode_port_b  : string;
          read_during_write_mode_mixed_ports : string;
          widthad_a  : integer;
          widthad_b  : integer;
          width_a  : integer;
          width_b  : integer;
          width_byteena_a  : integer;
          width_byteena_b  : integer
  );
  port (
      address_a : in std_logic_vector(g_adr_w - 1 downto 0);
      address_b : in std_logic_vector(g_adr_w - 1 downto 0);
      clock0 : in std_logic;
      data_a : in std_logic_vector(g_dat_w - 1 downto 0);
      data_b : in std_logic_vector(g_dat_w - 1 downto 0);
      wren_a : in std_logic;
      wren_b : in std_logic;
      q_a : out std_logic_vector(g_dat_w - 1 downto 0);
      q_b : out std_logic_vector(g_dat_w - 1 downto 0)
  );
  end component;

  -- Is used for gen_inferred:
  signal addr_a : natural range 0 to g_nof_words - 1;
  signal addr_b : natural range 0 to g_nof_words - 1;

  signal out_a  : std_logic_vector(g_dat_w - 1 downto 0);
  signal out_b  : std_logic_vector(g_dat_w - 1 downto 0);

  signal reg_a  : std_logic_vector(g_dat_w - 1 downto 0);
  signal reg_b  : std_logic_vector(g_dat_w - 1 downto 0);
begin
  assert g_rd_latency = 1 or g_rd_latency = 2
    report "ip_agi027_1e1v_ram_rw_rw : read latency must be 1 (default) or 2"
    severity FAILURE;

  gen_ip : if g_inferred = false generate
    u_altera_syncram : altera_syncram
    generic map (
          address_reg_b  => "CLOCK0",
          clock_enable_input_a  => "BYPASS",
          clock_enable_input_b  => "BYPASS",
          clock_enable_output_a  => "BYPASS",
          clock_enable_output_b  => "BYPASS",
          indata_reg_b  => "CLOCK0",
          init_file  => g_init_file,
          --enable_force_to_zero  => "FALSE",
          intended_device_family  => "Agilex 7",
          lpm_type  => "altera_syncram",
          numwords_a  => g_nof_words,
          numwords_b  => g_nof_words,
          operation_mode  => "BIDIR_DUAL_PORT",
          outdata_aclr_a  => "NONE",
          --outdata_sclr_a  => "NONE",
          outdata_aclr_b  => "NONE",
          --outdata_sclr_b  => "NONE",
          outdata_reg_a  => c_outdata_reg,
          outdata_reg_b  => c_outdata_reg,
          power_up_uninitialized  => "FALSE",
          read_during_write_mode_port_a  => "NEW_DATA_NO_NBE_READ",
          read_during_write_mode_port_b  => "NEW_DATA_NO_NBE_READ",
          read_during_write_mode_mixed_ports  => "DONT_CARE",
          widthad_a  => g_adr_w,
          widthad_b  => g_adr_w,
          width_a  => g_dat_w,
          width_b  => g_dat_w,
          width_byteena_a  => 1,
          width_byteena_b  => 1
    )
    port map (
        address_a => address_a,
        address_b => address_b,
        clock0 => clk,
        data_a => data_a,
        data_b => data_b,
        wren_a => wren_a,
        wren_b => wren_b,
        q_a => q_a,
        q_b => q_b
    );

  end generate;

  gen_inferred : if g_inferred = true generate
    addr_a <= to_integer(unsigned(address_a));
    addr_b <= to_integer(unsigned(address_b));

    u_mem : entity work.ip_agi027_1e1v_true_dual_port_ram_single_clock
    generic map (
      DATA_WIDTH => g_dat_w,
      ADDR_WIDTH => g_adr_w
    )
    port map (
      clk    => clk,
      addr_a => addr_a,
      addr_b => addr_b,
      data_a => data_a,
      data_b => data_b,
      we_a   => wren_a,
      we_b   => wren_b,
      q_a    => out_a,
      q_b    => out_b
    );

    reg_a <= out_a when rising_edge(clk);
    reg_b <= out_b when rising_edge(clk);

    q_a <= out_a when g_rd_latency = 1 else reg_a;
    q_b <= out_b when g_rd_latency = 1 else reg_b;
  end generate;
end SYN;
