--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Distribute the frames from n input streams on to m output streams.
-- Description:
-- * First the n input streams are demultiplexed in n*m internal streams and
--   then these are multiplexed on to the m output streams.
-- * The input streams can have an input FIFO dependent on g_use_fifo.
-- * The dp_distribute has two operation modes transmitter and receiver that
--   are set by g_tx.
--   When g_tx=TRUE then transmit:
--   . demux g_mode=1 selects next output when eop is there to distribute the
--     incomming frames evenly over all outputs.
--   . demux uses c_demux_remove_channel_lo=FALSE because the input frames all
--     have the same channel, they are demuxed in order.
--   . default non-blocking mux g_tx_mux_mode = 0 selects the next input when
--     the input has had a fair chance. Alternatively mux g_tx_mux_mode = 1
--     selects the next input when the eop is there. This causes that all
--     inputs will get to send equal number of frames, so this input traffic
--     must fit that and no input frames must get lost.
--   . mux uses c_mux_append_channel_lo=TRUE because the frames that will be
--     output to the serial link come from different the input sources. The
--     channel_lo field allows the other side of the link to reliably demux
--     these frames even when a frame gets lost on the link and disturbs the
--     order.
--   When g_tx=FALSE then receive:
--   . demux g_mode=0 uses the channel_lo field to select the output.
--   . demux uses c_demux_remove_channel_lo=TRUE because it uses the channel_lo
--     field for the output selection.
--   . default non-blocking mux g_rx_mux_mode = 0 selects the next input when
--     the input has had a fair chance.
--   . mux uses c_mux_append_channel_lo=FALSE because the frames that will be
--     output are in time order from the same source.
-- * The input streams and output streams can have DP packet coding to ease
--   transporting the other sosi fields over the link. If the snk_in stream
--   uses DP packets then g_code_channel_lo=TRUE may be used to also transport
--   the dp_distribute mux channel_lo bits via the CHAN field of the DP packet.
--   For g_tx=TRUE the mux channel_lo bits get inserted at the output and for
--   g_tx=FALSE the mux channel_lo bits get removed at the input. The removal
--   is done after the FIFO to avoid having to transport the channel_lo field
--   in parallel through the FIFO.
-- * When g_transpose = TRUE then the dp_distribute still does the demux-mux
--   transpose of frames in time over multiple serial links when g_nof_input
--   = g_nof_output. When g_transpose = FALSE then the inputs are wired on to
--   the corresponding serial link if g_nof_input = g_nof_output.
-- Data flow:
--      snk--> FIFO      -- when g_use_fifo
--       in--> DECODE    -- when NOT g_tx AND g_code_channel_lo
--       rx--> DEMUX
--   demux2--> rewire
--     mux2--> MUX
--       tx--> ENCODE    -- when     g_tx AND g_code_channel_lo
--         -->src
-- Remark:
-- . The assumption is that all links are equaly fast so the time order of the
--   frames at the input is preserved at the output.
-- . Thanks to the non-blocking dp_mux the distribution continuous when a frame
--   gets lost or an input is not used.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_distribute is
  generic (
    g_technology      : natural := c_tech_select_default;
    -- Distribution IO
    g_tx              : boolean;
    g_nof_input       : natural := 4;  -- >= 1
    g_nof_output      : natural := 3;  -- >= 1
    g_transpose       : boolean := false;
    g_code_channel_lo : boolean := false;  -- for DP packet optionaly transport the mux channel_lo bits via the CHAN field
    g_data_w          : natural := 16;  -- actual sosi data width, used for FIFO and for dp_packet_en_channel_lo
    -- Scheduling
    g_tx_mux_mode     : natural := 0;  -- default 0 for non-blocking mux, but 1 also works in simulation and on hardware provided that the inputs are garantueed to arrive
    g_rx_mux_mode     : natural := 0;  -- default 0 for non-blocking mux, but 1 also works in simulation and on hardware provided that the inputs are garantueed to arrive
    -- Input FIFO
    g_use_fifo        : boolean := false;
    g_bsn_w           : natural := 1;
    g_empty_w         : natural := 1;
    g_channel_w       : natural := 1;
    g_error_w         : natural := 1;
    g_use_bsn         : boolean := false;
    g_use_empty       : boolean := false;
    g_use_channel     : boolean := false;
    g_use_error       : boolean := false;
    g_use_sync        : boolean := false;
    g_fifo_af_margin  : natural := 4;  -- Nof words below max (full) at which fifo is considered almost full
    g_fifo_fill       : natural := 0;
    g_fifo_size       : natural := 256  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    -- ST sinks
    snk_out_arr : out t_dp_siso_arr(0 to g_nof_input - 1);
    snk_in_arr  : in  t_dp_sosi_arr(0 to g_nof_input - 1);
    -- ST source
    src_in_arr  : in  t_dp_siso_arr(0 to g_nof_output - 1) := (others => c_dp_siso_rdy);
    src_out_arr : out t_dp_sosi_arr(0 to g_nof_output - 1)
  );
end dp_distribute;

architecture str of dp_distribute is
  constant c_demux_mode              : natural := sel_a_b(g_tx, 1, 0);
  constant c_demux_remove_channel_lo : boolean := not g_tx;

  constant c_mux_mode                : natural := sel_a_b(g_tx, g_tx_mux_mode, g_rx_mux_mode);
  constant c_mux_append_channel_lo   : boolean := g_tx;

  constant g_encode_channel_lo       : boolean :=     g_tx and g_code_channel_lo;  -- true only for transmit to link
  constant g_decode_channel_lo       : boolean := not g_tx and g_code_channel_lo;  -- true only for receive from link
  constant c_link_channel_lo         : natural := sel_a_b(g_tx, ceil_log2(g_nof_input), ceil_log2(g_nof_output));

  type t_demux_siso_2arr is array (integer range <>) of t_dp_siso_arr(0 to g_nof_output - 1);
  type t_demux_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(0 to g_nof_output - 1);
  type t_mux_siso_2arr   is array (integer range <>) of t_dp_siso_arr(0 to g_nof_input - 1);
  type t_mux_sosi_2arr   is array (integer range <>) of t_dp_sosi_arr(0 to g_nof_input - 1);

  signal in_siso_arr     : t_dp_siso_arr(0 to g_nof_input - 1);  -- from snk_* for ST after optional input FIFO
  signal in_sosi_arr     : t_dp_sosi_arr(0 to g_nof_input - 1);
  signal rx_siso_arr     : t_dp_siso_arr(0 to g_nof_input - 1);  -- ST after optional decoding
  signal rx_sosi_arr     : t_dp_sosi_arr(0 to g_nof_input - 1);
  signal demux_siso_2arr : t_demux_siso_2arr(0 to g_nof_input - 1);  -- ST at output of demux
  signal demux_sosi_2arr : t_demux_sosi_2arr(0 to g_nof_input - 1);
  signal mux_siso_2arr   : t_mux_siso_2arr(0 to g_nof_output - 1);  -- ST rewired for input to mux
  signal mux_sosi_2arr   : t_mux_sosi_2arr(0 to g_nof_output - 1);
  signal tx_siso_arr     : t_dp_siso_arr(0 to g_nof_output - 1);  -- ST at output of mux, to src_* for ST output after optional encoding
  signal tx_sosi_arr     : t_dp_sosi_arr(0 to g_nof_output - 1);
begin
  no_fifo : if g_use_fifo = false generate
    snk_out_arr <= in_siso_arr;
    in_sosi_arr <= snk_in_arr;
  end generate;

  gen_fifo : if g_use_fifo = true generate
    gen_input : for I in 0 to g_nof_input - 1 generate
      u_fifo : entity work.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_bsn_w          => g_bsn_w,
        g_data_w         => g_data_w,
        g_empty_w        => g_empty_w,
        g_channel_w      => g_channel_w,
        g_error_w        => g_error_w,
        g_use_bsn        => g_use_bsn,
        g_use_empty      => g_use_empty,
        g_use_channel    => g_use_channel,
        g_use_error      => g_use_error,
        g_use_sync       => g_use_sync,
        g_fifo_fill      => g_fifo_fill,
        g_fifo_size      => g_fifo_size,
        g_fifo_af_margin => g_fifo_af_margin,
        g_fifo_rl        => 1
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sink
        snk_out   => snk_out_arr(I),
        snk_in    => snk_in_arr(I),
        -- ST source
        src_in    => in_siso_arr(I),
        src_out   => in_sosi_arr(I)
      );
    end generate;
  end generate;

  no_dec : if g_decode_channel_lo = false generate
    in_siso_arr <= rx_siso_arr;
    rx_sosi_arr <= in_sosi_arr;
  end generate;

  gen_dec : if g_decode_channel_lo = true generate
    gen_i : for I in 0 to g_nof_input - 1 generate
      u_dec : entity work.dp_packet_dec_channel_lo
      generic map (
        g_data_w      => g_data_w,
        g_channel_lo  => c_link_channel_lo
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sinks
        snk_out   => in_siso_arr(I),
        snk_in    => in_sosi_arr(I),
        -- ST source
        src_in    => rx_siso_arr(I),
        src_out   => rx_sosi_arr(I)
      );
    end generate;
  end generate;

  no_transpose : if g_nof_input = g_nof_output and g_transpose = false generate
    rx_siso_arr <= tx_siso_arr;
    tx_sosi_arr <= rx_sosi_arr;
  end generate;

  gen_transpose : if g_nof_input /= g_nof_output or g_transpose = true generate
    gen_demux : for I in 0 to g_nof_input - 1 generate
      u_demux : entity work.dp_demux
      generic map (
        g_mode              => c_demux_mode,
        g_nof_output        => g_nof_output,
        g_remove_channel_lo => c_demux_remove_channel_lo,
        g_combined          => false
      )
      port map (
        rst         => rst,
        clk         => clk,
        -- ST sinks
        snk_out     => rx_siso_arr(I),
        snk_in      => rx_sosi_arr(I),
        -- ST source
        src_in_arr  => demux_siso_2arr(I),
        src_out_arr => demux_sosi_2arr(I)
      );
    end generate;

    -- Rewire to distribute
    gen_in : for I in 0 to g_nof_input - 1 generate
      gen_out : for J in 0 to g_nof_output - 1 generate
        demux_siso_2arr(I)(J) <= mux_siso_2arr(J)(I);
        mux_sosi_2arr(J)(I) <= demux_sosi_2arr(I)(J);
      end generate;
    end generate;

    gen_mux : for J in 0 to g_nof_output - 1 generate
      u_mux : entity work.dp_mux
      generic map (
        -- MUX
        g_mode              => c_mux_mode,
        g_nof_input         => g_nof_input,
        g_append_channel_lo => c_mux_append_channel_lo,
        -- Input FIFO
        g_use_fifo          => false,
        g_fifo_size         => array_init(1024, g_nof_input),  -- must match g_nof_input, even when g_use_fifo=FALSE
        g_fifo_fill         => array_init(   0, g_nof_input)  -- must match g_nof_input, even when g_use_fifo=FALSE
      )
      port map (
        rst         => rst,
        clk         => clk,
        -- ST sinks
        snk_out_arr => mux_siso_2arr(J),
        snk_in_arr  => mux_sosi_2arr(J),
        -- ST source
        src_in      => tx_siso_arr(J),
        src_out     => tx_sosi_arr(J)
      );
    end generate;
  end generate;

  no_enc : if g_encode_channel_lo = false generate
    tx_siso_arr <= src_in_arr;
    src_out_arr <= tx_sosi_arr;
  end generate;

  gen_enc : if g_encode_channel_lo = true generate
    gen_j : for J in 0 to g_nof_output - 1 generate
      u_enc : entity work.dp_packet_enc_channel_lo
      generic map (
        g_data_w      => g_data_w,
        g_channel_lo  => c_link_channel_lo
      )
      port map (
        -- ST sinks
        snk_out   => tx_siso_arr(J),
        snk_in    => tx_sosi_arr(J),
        -- ST source
        src_in    => src_in_arr(J),
        src_out   => src_out_arr(J)
      );
    end generate;
  end generate;
end str;
